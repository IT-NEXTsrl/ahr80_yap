* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsor_kft                                                        *
*              Dati testata ordine                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30][VRS_198]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-04-19                                                      *
* Last revis.: 2017-11-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsor_kft",oParentObject))

* --- Class definition
define class tgsor_kft as StdForm
  Top    = 104
  Left   = 194

  * --- Standard Properties
  Width  = 534
  Height = 466
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2017-11-09"
  HelpContextID=48851305
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=98

  * --- Constant Properties
  _IDX = 0
  VOCIIVA_IDX = 0
  CAU_CONT_IDX = 0
  AGENTI_IDX = 0
  DOC_MAST_IDX = 0
  CAM_AGAZ_IDX = 0
  DIC_INTE_IDX = 0
  cPrg = "gsor_kft"
  cComment = "Dati testata ordine"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_TIPCON = space(1)
  w_TIPDOC = space(2)
  w_MVDATREG = ctod('  /  /  ')
  w_MVCLADOC = space(2)
  w_MVFLVEAC = space(1)
  w_FLINTR = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_BOLIVE = space(1)
  w_PERIVE = 0
  w_DTOBSO = ctod('  /  /  ')
  w_TIPIVA = space(1)
  w_DESAPP = space(35)
  w_TIPREG = space(1)
  w_CFLPP = space(1)
  w_MVRIFDIC = space(10)
  w_DATOBSO = ctod('  /  /  ')
  w_MVCODVAL = space(3)
  w_CAOVAL = 0
  w_FLGEFA = space(1)
  w_MVDATCIV = ctod('  /  /  ')
  w_LIVRAGG = space(1)
  w_MVCODAGE = space(5)
  w_CODAG2 = space(5)
  w_MVCODAG2 = space(5)
  w_DESAG2 = space(35)
  w_MVCODIVE = space(5)
  w_MVTFRAGG = space(1)
  w_MVCAUCON = space(5)
  w_DESCAC = space(35)
  w_TIPAG2 = space(1)
  w_MVTINCOM = ctod('  /  /  ')
  w_MVTFICOM = ctod('  /  /  ')
  w_MVFLVABD = space(1)
  w_MVNUMEST = 0
  w_MVALFEST = space(10)
  w_MVDATEST = ctod('  /  /  ')
  w_MVSEREST = space(30)
  w_MVFLSEND = space(1)
  w_MVTIPCON = space(1)
  w_DESAGE = space(35)
  w_PARCAU = space(1)
  w_MVRIFESP = space(10)
  w_INDIVE = 0
  w_MVCODCOM = space(15)
  w_MVFLEVAS = space(1)
  w_MVVALNAZ = space(3)
  w_COCODVAL = space(3)
  w_CAONAZ = 0
  w_MVCAOVAL = 0
  w_MVDATDOC = ctod('  /  /  ')
  w_MVTIPDOC = space(5)
  o_MVTIPDOC = space(5)
  w_MVCODCON = space(15)
  w_MVNUMDOC = 0
  w_MVALFDOC = space(10)
  w_MVSERIAL = space(10)
  w_MVTIPCON = space(1)
  w_AGSCOPA2 = space(1)
  w_AGEPRO = space(5)
  w_AGSCOPAG = space(1)
  w_AGEPR2 = space(5)
  w_FLCCAU = space(1)
  w_MVTCAMAG = space(5)
  w_DESCAU = space(35)
  w_FLAVAL = space(1)
  w_OBSCAU = ctod('  /  /  ')
  w_CAUCOL = space(5)
  w_VARVAL = space(1)
  w_OLDAGE = space(5)
  w_OLDCAP = space(5)
  w_MVFLSCOM = space(1)
  w_NUMRIG = 0
  w_SERORI = space(10)
  w_DICSN = space(10)
  w_NUMDIC = 0
  w_SERDOC = space(3)
  w_ANNDIC = space(4)
  w_DATDIC = ctod('  /  /  ')
  w_OPEDIC = space(1)
  w_TIPDIC = space(1)
  w_CODIVE = space(5)
  o_CODIVE = space(5)
  w_UTIDIC = 0
  w_MVAGG_01 = space(15)
  w_MVAGG_02 = space(15)
  w_MVAGG_03 = space(15)
  w_MVAGG_04 = space(15)
  w_MVAGG_05 = ctod('  /  /  ')
  w_MVAGG_06 = ctod('  /  /  ')
  w_DACAM_01 = space(30)
  w_DACAM_02 = space(30)
  w_DACAM_03 = space(30)
  w_DACAM_04 = space(30)
  w_DACAM_05 = space(30)
  w_DACAM_06 = space(30)
  w_DICINTE_INDIRETTA = space(15)
  w_NUMDIC_COLL = 0
  w_SERDOC_COLL = space(3)
  w_ANNDIC_COLL = space(4)
  w_DATDIC_COLL = ctod('  /  /  ')
  w_CAMAGG01 = .NULL.
  w_CAMAGG02 = .NULL.
  w_CAMAGG03 = .NULL.
  w_CAMAGG04 = .NULL.
  w_CAMAGG05 = .NULL.
  w_CAMAGG06 = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsor_kft
  *--- Conta le righe di detaglio che hanno MVCODICE e MVPREZZO pieni
  *--- La procedura valorizza la variabile w_NUMRIG
  Proc CountRig()
     with this
       .oParentObject.Exec_Select('TMP_KFT','Count(*) As Conta','NOT DELETED() AND ( NOT EMPTY(t_MVCODICE) OR t_MVPREZZO > 0 )','','')
       .w_NUMRIG=Nvl(TMP_KFT.Conta, 0 )
       Use In Tmp_Kft
     endwith
  EndProc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsor_kftPag1","gsor_kft",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMVCODAGE_1_22
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_CAMAGG01 = this.oPgFrm.Pages(1).oPag.CAMAGG01
    this.w_CAMAGG02 = this.oPgFrm.Pages(1).oPag.CAMAGG02
    this.w_CAMAGG03 = this.oPgFrm.Pages(1).oPag.CAMAGG03
    this.w_CAMAGG04 = this.oPgFrm.Pages(1).oPag.CAMAGG04
    this.w_CAMAGG05 = this.oPgFrm.Pages(1).oPag.CAMAGG05
    this.w_CAMAGG06 = this.oPgFrm.Pages(1).oPag.CAMAGG06
    DoDefault()
    proc Destroy()
      this.w_CAMAGG01 = .NULL.
      this.w_CAMAGG02 = .NULL.
      this.w_CAMAGG03 = .NULL.
      this.w_CAMAGG04 = .NULL.
      this.w_CAMAGG05 = .NULL.
      this.w_CAMAGG06 = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='VOCIIVA'
    this.cWorkTables[2]='CAU_CONT'
    this.cWorkTables[3]='AGENTI'
    this.cWorkTables[4]='DOC_MAST'
    this.cWorkTables[5]='CAM_AGAZ'
    this.cWorkTables[6]='DIC_INTE'
    return(this.OpenAllTables(6))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPCON=space(1)
      .w_TIPDOC=space(2)
      .w_MVDATREG=ctod("  /  /  ")
      .w_MVCLADOC=space(2)
      .w_MVFLVEAC=space(1)
      .w_FLINTR=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_BOLIVE=space(1)
      .w_PERIVE=0
      .w_DTOBSO=ctod("  /  /  ")
      .w_TIPIVA=space(1)
      .w_DESAPP=space(35)
      .w_TIPREG=space(1)
      .w_CFLPP=space(1)
      .w_MVRIFDIC=space(10)
      .w_DATOBSO=ctod("  /  /  ")
      .w_MVCODVAL=space(3)
      .w_CAOVAL=0
      .w_FLGEFA=space(1)
      .w_MVDATCIV=ctod("  /  /  ")
      .w_LIVRAGG=space(1)
      .w_MVCODAGE=space(5)
      .w_CODAG2=space(5)
      .w_MVCODAG2=space(5)
      .w_DESAG2=space(35)
      .w_MVCODIVE=space(5)
      .w_MVTFRAGG=space(1)
      .w_MVCAUCON=space(5)
      .w_DESCAC=space(35)
      .w_TIPAG2=space(1)
      .w_MVTINCOM=ctod("  /  /  ")
      .w_MVTFICOM=ctod("  /  /  ")
      .w_MVFLVABD=space(1)
      .w_MVNUMEST=0
      .w_MVALFEST=space(10)
      .w_MVDATEST=ctod("  /  /  ")
      .w_MVSEREST=space(30)
      .w_MVFLSEND=space(1)
      .w_MVTIPCON=space(1)
      .w_DESAGE=space(35)
      .w_PARCAU=space(1)
      .w_MVRIFESP=space(10)
      .w_INDIVE=0
      .w_MVCODCOM=space(15)
      .w_MVFLEVAS=space(1)
      .w_MVVALNAZ=space(3)
      .w_COCODVAL=space(3)
      .w_CAONAZ=0
      .w_MVCAOVAL=0
      .w_MVDATDOC=ctod("  /  /  ")
      .w_MVTIPDOC=space(5)
      .w_MVCODCON=space(15)
      .w_MVNUMDOC=0
      .w_MVALFDOC=space(10)
      .w_MVSERIAL=space(10)
      .w_MVTIPCON=space(1)
      .w_AGSCOPA2=space(1)
      .w_AGEPRO=space(5)
      .w_AGSCOPAG=space(1)
      .w_AGEPR2=space(5)
      .w_FLCCAU=space(1)
      .w_MVTCAMAG=space(5)
      .w_DESCAU=space(35)
      .w_FLAVAL=space(1)
      .w_OBSCAU=ctod("  /  /  ")
      .w_CAUCOL=space(5)
      .w_VARVAL=space(1)
      .w_OLDAGE=space(5)
      .w_OLDCAP=space(5)
      .w_MVFLSCOM=space(1)
      .w_NUMRIG=0
      .w_SERORI=space(10)
      .w_DICSN=space(10)
      .w_NUMDIC=0
      .w_SERDOC=space(3)
      .w_ANNDIC=space(4)
      .w_DATDIC=ctod("  /  /  ")
      .w_OPEDIC=space(1)
      .w_TIPDIC=space(1)
      .w_CODIVE=space(5)
      .w_UTIDIC=0
      .w_MVAGG_01=space(15)
      .w_MVAGG_02=space(15)
      .w_MVAGG_03=space(15)
      .w_MVAGG_04=space(15)
      .w_MVAGG_05=ctod("  /  /  ")
      .w_MVAGG_06=ctod("  /  /  ")
      .w_DACAM_01=space(30)
      .w_DACAM_02=space(30)
      .w_DACAM_03=space(30)
      .w_DACAM_04=space(30)
      .w_DACAM_05=space(30)
      .w_DACAM_06=space(30)
      .w_DICINTE_INDIRETTA=space(15)
      .w_NUMDIC_COLL=0
      .w_SERDOC_COLL=space(3)
      .w_ANNDIC_COLL=space(4)
      .w_DATDIC_COLL=ctod("  /  /  ")
      .w_TIPDOC=oParentObject.w_TIPDOC
      .w_MVDATREG=oParentObject.w_MVDATREG
      .w_MVCLADOC=oParentObject.w_MVCLADOC
      .w_MVFLVEAC=oParentObject.w_MVFLVEAC
      .w_FLINTR=oParentObject.w_FLINTR
      .w_BOLIVE=oParentObject.w_BOLIVE
      .w_PERIVE=oParentObject.w_PERIVE
      .w_TIPIVA=oParentObject.w_TIPIVA
      .w_TIPREG=oParentObject.w_TIPREG
      .w_CFLPP=oParentObject.w_CFLPP
      .w_MVRIFDIC=oParentObject.w_MVRIFDIC
      .w_MVCODVAL=oParentObject.w_MVCODVAL
      .w_CAOVAL=oParentObject.w_CAOVAL
      .w_FLGEFA=oParentObject.w_FLGEFA
      .w_MVDATCIV=oParentObject.w_MVDATCIV
      .w_MVCODAGE=oParentObject.w_MVCODAGE
      .w_MVCODAG2=oParentObject.w_MVCODAG2
      .w_MVCODIVE=oParentObject.w_MVCODIVE
      .w_MVTFRAGG=oParentObject.w_MVTFRAGG
      .w_MVCAUCON=oParentObject.w_MVCAUCON
      .w_MVTINCOM=oParentObject.w_MVTINCOM
      .w_MVTFICOM=oParentObject.w_MVTFICOM
      .w_MVFLVABD=oParentObject.w_MVFLVABD
      .w_MVNUMEST=oParentObject.w_MVNUMEST
      .w_MVALFEST=oParentObject.w_MVALFEST
      .w_MVDATEST=oParentObject.w_MVDATEST
      .w_MVSEREST=oParentObject.w_MVSEREST
      .w_MVFLSEND=oParentObject.w_MVFLSEND
      .w_MVTIPCON=oParentObject.w_MVTIPCON
      .w_PARCAU=oParentObject.w_PARCAU
      .w_MVRIFESP=oParentObject.w_MVRIFESP
      .w_INDIVE=oParentObject.w_INDIVE
      .w_MVCODCOM=oParentObject.w_MVCODCOM
      .w_MVFLEVAS=oParentObject.w_MVFLEVAS
      .w_MVVALNAZ=oParentObject.w_MVVALNAZ
      .w_COCODVAL=oParentObject.w_COCODVAL
      .w_CAONAZ=oParentObject.w_CAONAZ
      .w_MVCAOVAL=oParentObject.w_MVCAOVAL
      .w_MVDATDOC=oParentObject.w_MVDATDOC
      .w_MVTIPDOC=oParentObject.w_MVTIPDOC
      .w_MVCODCON=oParentObject.w_MVCODCON
      .w_MVNUMDOC=oParentObject.w_MVNUMDOC
      .w_MVALFDOC=oParentObject.w_MVALFDOC
      .w_MVSERIAL=oParentObject.w_MVSERIAL
      .w_MVTIPCON=oParentObject.w_MVTIPCON
      .w_AGSCOPA2=oParentObject.w_AGSCOPA2
      .w_AGEPRO=oParentObject.w_AGEPRO
      .w_AGSCOPAG=oParentObject.w_AGSCOPAG
      .w_AGEPR2=oParentObject.w_AGEPR2
      .w_FLCCAU=oParentObject.w_FLCCAU
      .w_MVTCAMAG=oParentObject.w_MVTCAMAG
      .w_CAUCOL=oParentObject.w_CAUCOL
      .w_VARVAL=oParentObject.w_VARVAL
      .w_MVFLSCOM=oParentObject.w_MVFLSCOM
      .w_NUMDIC=oParentObject.w_NUMDIC
      .w_ANNDIC=oParentObject.w_ANNDIC
      .w_DATDIC=oParentObject.w_DATDIC
      .w_OPEDIC=oParentObject.w_OPEDIC
      .w_TIPDIC=oParentObject.w_TIPDIC
      .w_UTIDIC=oParentObject.w_UTIDIC
      .w_MVAGG_01=oParentObject.w_MVAGG_01
      .w_MVAGG_02=oParentObject.w_MVAGG_02
      .w_MVAGG_03=oParentObject.w_MVAGG_03
      .w_MVAGG_04=oParentObject.w_MVAGG_04
      .w_MVAGG_05=oParentObject.w_MVAGG_05
      .w_MVAGG_06=oParentObject.w_MVAGG_06
      .w_DACAM_01=oParentObject.w_DACAM_01
      .w_DACAM_02=oParentObject.w_DACAM_02
      .w_DACAM_03=oParentObject.w_DACAM_03
      .w_DACAM_04=oParentObject.w_DACAM_04
      .w_DACAM_05=oParentObject.w_DACAM_05
      .w_DACAM_06=oParentObject.w_DACAM_06
      .w_NUMDIC_COLL=oParentObject.w_NUMDIC_COLL
      .w_SERDOC_COLL=oParentObject.w_SERDOC_COLL
      .w_ANNDIC_COLL=oParentObject.w_ANNDIC_COLL
      .w_DATDIC_COLL=oParentObject.w_DATDIC_COLL
        .w_TIPCON = 'G'
          .DoRTCalc(2,6,.f.)
        .w_OBTEST = .w_MVDATREG
        .DoRTCalc(8,15,.f.)
        if not(empty(.w_MVRIFDIC))
          .link_1_15('Full')
        endif
          .DoRTCalc(16,20,.f.)
        .w_LIVRAGG = .w_MVTFRAGG
        .w_MVCODAGE = .w_MVCODAGE
        .DoRTCalc(22,22,.f.)
        if not(empty(.w_MVCODAGE))
          .link_1_22('Full')
        endif
        .DoRTCalc(23,24,.f.)
        if not(empty(.w_MVCODAG2))
          .link_1_24('Full')
        endif
          .DoRTCalc(25,25,.f.)
        .w_MVCODIVE = .w_MVCODIVE
        .DoRTCalc(26,26,.f.)
        if not(empty(.w_MVCODIVE))
          .link_1_26('Full')
        endif
          .DoRTCalc(27,27,.f.)
        .w_MVCAUCON = .w_MVCAUCON
        .DoRTCalc(28,28,.f.)
        if not(empty(.w_MVCAUCON))
          .link_1_28('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_46.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_58.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_60.Calculate()
        .DoRTCalc(29,62,.f.)
        if not(empty(.w_MVTCAMAG))
          .link_1_80('Full')
        endif
          .DoRTCalc(63,67,.f.)
        .w_OLDAGE = .w_MVCODAGE
        .w_OLDCAP = .w_MVCODAG2
          .DoRTCalc(70,71,.f.)
        .w_SERORI = CHKDOCESP(.w_MVSERIAL)
        .w_DICSN = IIF(EMPTY(.w_MVRIFDIC), ' ', 'S')
      .oPgFrm.Page1.oPag.CAMAGG01.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_01, " "), "Campo 1" )) + IIF( g_COLON, ":", ""))
      .oPgFrm.Page1.oPag.CAMAGG02.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_02, " "), "Campo 2" )) + IIF( g_COLON, ":", ""))
      .oPgFrm.Page1.oPag.CAMAGG03.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_03, " "), "Campo 3" )) + IIF( g_COLON, ":", ""))
      .oPgFrm.Page1.oPag.CAMAGG04.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_04, " "), "Campo 4" )) + IIF( g_COLON, ":", ""))
      .oPgFrm.Page1.oPag.CAMAGG05.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_05, " "), "Campo 5" )) + IIF( g_COLON, ":", ""))
      .oPgFrm.Page1.oPag.CAMAGG06.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_06, " "), "Campo 6" )) + IIF( g_COLON, ":", ""))
    endwith
    this.DoRTCalc(74,98,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_49.enabled = this.oPgFrm.Page1.oPag.oBtn_1_49.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_68.enabled = this.oPgFrm.Page1.oPag.oBtn_1_68.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_92.enabled = this.oPgFrm.Page1.oPag.oBtn_1_92.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_99.enabled = this.oPgFrm.Page1.oPag.oBtn_1_99.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsor_kft
    *--- Valorizzo w_NUMRIG
    this.CountRig()
    * --- Fine Area Manuale
  endproc

  proc SetStatus()
    if type('this.oParentObject.cFunction')='C'
      this.cFunction=this.oParentObject.cFunction
      if this.cFunction="Load"
        this.cFunction="Edit"
      endif
    endif
    DoDefault()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oMVCODAGE_1_22.enabled = i_bVal
      .Page1.oPag.oMVCODAG2_1_24.enabled = i_bVal
      .Page1.oPag.oMVCODIVE_1_26.enabled = i_bVal
      .Page1.oPag.oMVTFRAGG_1_27.enabled = i_bVal
      .Page1.oPag.oMVTINCOM_1_37.enabled = i_bVal
      .Page1.oPag.oMVTFICOM_1_39.enabled = i_bVal
      .Page1.oPag.oMVFLVABD_1_40.enabled = i_bVal
      .Page1.oPag.oMVNUMEST_1_41.enabled = i_bVal
      .Page1.oPag.oMVALFEST_1_42.enabled = i_bVal
      .Page1.oPag.oMVDATEST_1_43.enabled = i_bVal
      .Page1.oPag.oMVTCAMAG_1_80.enabled = i_bVal
      .Page1.oPag.oMVFLSCOM_1_89.enabled = i_bVal
      .Page1.oPag.oDICSN_1_94.enabled = i_bVal
      .Page1.oPag.oMVAGG_01_1_107.enabled = i_bVal
      .Page1.oPag.oMVAGG_02_1_108.enabled = i_bVal
      .Page1.oPag.oMVAGG_03_1_109.enabled = i_bVal
      .Page1.oPag.oMVAGG_04_1_110.enabled = i_bVal
      .Page1.oPag.oMVAGG_05_1_111.enabled = i_bVal
      .Page1.oPag.oMVAGG_06_1_112.enabled = i_bVal
      .Page1.oPag.oBtn_1_47.enabled = i_bVal
      .Page1.oPag.oBtn_1_49.enabled = .Page1.oPag.oBtn_1_49.mCond()
      .Page1.oPag.oBtn_1_68.enabled = .Page1.oPag.oBtn_1_68.mCond()
      .Page1.oPag.oBtn_1_92.enabled = .Page1.oPag.oBtn_1_92.mCond()
      .Page1.oPag.oBtn_1_99.enabled = .Page1.oPag.oBtn_1_99.mCond()
      .Page1.oPag.oObj_1_31.enabled = i_bVal
      .Page1.oPag.oObj_1_38.enabled = i_bVal
      .Page1.oPag.oObj_1_46.enabled = i_bVal
      .Page1.oPag.oObj_1_58.enabled = i_bVal
      .Page1.oPag.oObj_1_60.enabled = i_bVal
      .Page1.oPag.CAMAGG01.enabled = i_bVal
      .Page1.oPag.CAMAGG02.enabled = i_bVal
      .Page1.oPag.CAMAGG03.enabled = i_bVal
      .Page1.oPag.CAMAGG04.enabled = i_bVal
      .Page1.oPag.CAMAGG05.enabled = i_bVal
      .Page1.oPag.CAMAGG06.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_TIPDOC=.w_TIPDOC
      .oParentObject.w_MVDATREG=.w_MVDATREG
      .oParentObject.w_MVCLADOC=.w_MVCLADOC
      .oParentObject.w_MVFLVEAC=.w_MVFLVEAC
      .oParentObject.w_FLINTR=.w_FLINTR
      .oParentObject.w_BOLIVE=.w_BOLIVE
      .oParentObject.w_PERIVE=.w_PERIVE
      .oParentObject.w_TIPIVA=.w_TIPIVA
      .oParentObject.w_TIPREG=.w_TIPREG
      .oParentObject.w_CFLPP=.w_CFLPP
      .oParentObject.w_MVRIFDIC=.w_MVRIFDIC
      .oParentObject.w_MVCODVAL=.w_MVCODVAL
      .oParentObject.w_CAOVAL=.w_CAOVAL
      .oParentObject.w_FLGEFA=.w_FLGEFA
      .oParentObject.w_MVDATCIV=.w_MVDATCIV
      .oParentObject.w_MVCODAGE=.w_MVCODAGE
      .oParentObject.w_MVCODAG2=.w_MVCODAG2
      .oParentObject.w_MVCODIVE=.w_MVCODIVE
      .oParentObject.w_MVTFRAGG=.w_MVTFRAGG
      .oParentObject.w_MVCAUCON=.w_MVCAUCON
      .oParentObject.w_MVTINCOM=.w_MVTINCOM
      .oParentObject.w_MVTFICOM=.w_MVTFICOM
      .oParentObject.w_MVFLVABD=.w_MVFLVABD
      .oParentObject.w_MVNUMEST=.w_MVNUMEST
      .oParentObject.w_MVALFEST=.w_MVALFEST
      .oParentObject.w_MVDATEST=.w_MVDATEST
      .oParentObject.w_MVSEREST=.w_MVSEREST
      .oParentObject.w_MVFLSEND=.w_MVFLSEND
      .oParentObject.w_MVTIPCON=.w_MVTIPCON
      .oParentObject.w_PARCAU=.w_PARCAU
      .oParentObject.w_MVRIFESP=.w_MVRIFESP
      .oParentObject.w_INDIVE=.w_INDIVE
      .oParentObject.w_MVCODCOM=.w_MVCODCOM
      .oParentObject.w_MVFLEVAS=.w_MVFLEVAS
      .oParentObject.w_MVVALNAZ=.w_MVVALNAZ
      .oParentObject.w_COCODVAL=.w_COCODVAL
      .oParentObject.w_CAONAZ=.w_CAONAZ
      .oParentObject.w_MVCAOVAL=.w_MVCAOVAL
      .oParentObject.w_MVDATDOC=.w_MVDATDOC
      .oParentObject.w_MVTIPDOC=.w_MVTIPDOC
      .oParentObject.w_MVCODCON=.w_MVCODCON
      .oParentObject.w_MVNUMDOC=.w_MVNUMDOC
      .oParentObject.w_MVALFDOC=.w_MVALFDOC
      .oParentObject.w_MVSERIAL=.w_MVSERIAL
      .oParentObject.w_MVTIPCON=.w_MVTIPCON
      .oParentObject.w_AGSCOPA2=.w_AGSCOPA2
      .oParentObject.w_AGEPRO=.w_AGEPRO
      .oParentObject.w_AGSCOPAG=.w_AGSCOPAG
      .oParentObject.w_AGEPR2=.w_AGEPR2
      .oParentObject.w_FLCCAU=.w_FLCCAU
      .oParentObject.w_MVTCAMAG=.w_MVTCAMAG
      .oParentObject.w_CAUCOL=.w_CAUCOL
      .oParentObject.w_VARVAL=.w_VARVAL
      .oParentObject.w_MVFLSCOM=.w_MVFLSCOM
      .oParentObject.w_NUMDIC=.w_NUMDIC
      .oParentObject.w_ANNDIC=.w_ANNDIC
      .oParentObject.w_DATDIC=.w_DATDIC
      .oParentObject.w_OPEDIC=.w_OPEDIC
      .oParentObject.w_TIPDIC=.w_TIPDIC
      .oParentObject.w_UTIDIC=.w_UTIDIC
      .oParentObject.w_MVAGG_01=.w_MVAGG_01
      .oParentObject.w_MVAGG_02=.w_MVAGG_02
      .oParentObject.w_MVAGG_03=.w_MVAGG_03
      .oParentObject.w_MVAGG_04=.w_MVAGG_04
      .oParentObject.w_MVAGG_05=.w_MVAGG_05
      .oParentObject.w_MVAGG_06=.w_MVAGG_06
      .oParentObject.w_DACAM_01=.w_DACAM_01
      .oParentObject.w_DACAM_02=.w_DACAM_02
      .oParentObject.w_DACAM_03=.w_DACAM_03
      .oParentObject.w_DACAM_04=.w_DACAM_04
      .oParentObject.w_DACAM_05=.w_DACAM_05
      .oParentObject.w_DACAM_06=.w_DACAM_06
      .oParentObject.w_NUMDIC_COLL=.w_NUMDIC_COLL
      .oParentObject.w_SERDOC_COLL=.w_SERDOC_COLL
      .oParentObject.w_ANNDIC_COLL=.w_ANNDIC_COLL
      .oParentObject.w_DATDIC_COLL=.w_DATDIC_COLL
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- gsor_kft
    *Se modifico i dati agente o capoarea rilancio il calcolo percentuali provvigioni
    If this.w_OLDAGE<> this.w_MVCODAGE OR this.w_OLDCAP<> this.w_MVCODAG2
     this.oParentObject.NotifyEvent('CalcprovAllRow')
    Endif
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,6,.t.)
            .w_OBTEST = .w_MVDATREG
        .DoRTCalc(8,14,.t.)
        if .o_CODIVE<>.w_CODIVE
          .link_1_15('Full')
        endif
        .DoRTCalc(16,27,.t.)
          .link_1_28('Full')
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_58.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_60.Calculate()
        .DoRTCalc(29,61,.t.)
        if .o_MVTIPDOC<>.w_MVTIPDOC
          .link_1_80('Full')
        endif
        .oPgFrm.Page1.oPag.CAMAGG01.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_01, " "), "Campo 1" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page1.oPag.CAMAGG02.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_02, " "), "Campo 2" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page1.oPag.CAMAGG03.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_03, " "), "Campo 3" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page1.oPag.CAMAGG04.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_04, " "), "Campo 4" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page1.oPag.CAMAGG05.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_05, " "), "Campo 5" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page1.oPag.CAMAGG06.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_06, " "), "Campo 6" )) + IIF( g_COLON, ":", ""))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(63,98,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_58.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_60.Calculate()
        .oPgFrm.Page1.oPag.CAMAGG01.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_01, " "), "Campo 1" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page1.oPag.CAMAGG02.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_02, " "), "Campo 2" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page1.oPag.CAMAGG03.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_03, " "), "Campo 3" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page1.oPag.CAMAGG04.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_04, " "), "Campo 4" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page1.oPag.CAMAGG05.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_05, " "), "Campo 5" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page1.oPag.CAMAGG06.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_06, " "), "Campo 6" )) + IIF( g_COLON, ":", ""))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oMVCODAGE_1_22.enabled = this.oPgFrm.Page1.oPag.oMVCODAGE_1_22.mCond()
    this.oPgFrm.Page1.oPag.oMVCODAG2_1_24.enabled = this.oPgFrm.Page1.oPag.oMVCODAG2_1_24.mCond()
    this.oPgFrm.Page1.oPag.oMVTCAMAG_1_80.enabled = this.oPgFrm.Page1.oPag.oMVTCAMAG_1_80.mCond()
    this.oPgFrm.Page1.oPag.oMVFLSCOM_1_89.enabled = this.oPgFrm.Page1.oPag.oMVFLSCOM_1_89.mCond()
    this.oPgFrm.Page1.oPag.oDICSN_1_94.enabled = this.oPgFrm.Page1.oPag.oDICSN_1_94.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_92.enabled = this.oPgFrm.Page1.oPag.oBtn_1_92.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oMVCODAGE_1_22.visible=!this.oPgFrm.Page1.oPag.oMVCODAGE_1_22.mHide()
    this.oPgFrm.Page1.oPag.oMVCODAG2_1_24.visible=!this.oPgFrm.Page1.oPag.oMVCODAG2_1_24.mHide()
    this.oPgFrm.Page1.oPag.oDESAG2_1_25.visible=!this.oPgFrm.Page1.oPag.oDESAG2_1_25.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_34.visible=!this.oPgFrm.Page1.oPag.oStr_1_34.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_35.visible=!this.oPgFrm.Page1.oPag.oStr_1_35.mHide()
    this.oPgFrm.Page1.oPag.oMVSEREST_1_44.visible=!this.oPgFrm.Page1.oPag.oMVSEREST_1_44.mHide()
    this.oPgFrm.Page1.oPag.oMVFLSEND_1_45.visible=!this.oPgFrm.Page1.oPag.oMVFLSEND_1_45.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_50.visible=!this.oPgFrm.Page1.oPag.oStr_1_50.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_54.visible=!this.oPgFrm.Page1.oPag.oStr_1_54.mHide()
    this.oPgFrm.Page1.oPag.oDESAGE_1_55.visible=!this.oPgFrm.Page1.oPag.oDESAGE_1_55.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_68.visible=!this.oPgFrm.Page1.oPag.oBtn_1_68.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_92.visible=!this.oPgFrm.Page1.oPag.oBtn_1_92.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_93.visible=!this.oPgFrm.Page1.oPag.oStr_1_93.mHide()
    this.oPgFrm.Page1.oPag.oDICSN_1_94.visible=!this.oPgFrm.Page1.oPag.oDICSN_1_94.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_31.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_38.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_46.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_58.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_60.Event(cEvent)
      .oPgFrm.Page1.oPag.CAMAGG01.Event(cEvent)
      .oPgFrm.Page1.oPag.CAMAGG02.Event(cEvent)
      .oPgFrm.Page1.oPag.CAMAGG03.Event(cEvent)
      .oPgFrm.Page1.oPag.CAMAGG04.Event(cEvent)
      .oPgFrm.Page1.oPag.CAMAGG05.Event(cEvent)
      .oPgFrm.Page1.oPag.CAMAGG06.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MVRIFDIC
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIC_INTE_IDX,3]
    i_lTable = "DIC_INTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIC_INTE_IDX,2], .t., this.DIC_INTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIC_INTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVRIFDIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVRIFDIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DISERIAL,DI__ANNO,DIDATLET,DICODIVA,DITIPCON,DITIPOPE,DIIMPUTI,DITIPIVA,DINUMDOC,DISERDOC";
                   +" from "+i_cTable+" "+i_lTable+" where DISERIAL="+cp_ToStrODBC(this.w_MVRIFDIC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DISERIAL',this.w_MVRIFDIC)
            select DISERIAL,DI__ANNO,DIDATLET,DICODIVA,DITIPCON,DITIPOPE,DIIMPUTI,DITIPIVA,DINUMDOC,DISERDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVRIFDIC = NVL(_Link_.DISERIAL,space(10))
      this.w_ANNDIC = NVL(_Link_.DI__ANNO,space(4))
      this.w_DATDIC = NVL(cp_ToDate(_Link_.DIDATLET),ctod("  /  /  "))
      this.w_CODIVE = NVL(_Link_.DICODIVA,space(5))
      this.w_TIPDIC = NVL(_Link_.DITIPCON,space(1))
      this.w_OPEDIC = NVL(_Link_.DITIPOPE,space(1))
      this.w_UTIDIC = NVL(_Link_.DIIMPUTI,0)
      this.w_TIPIVA = NVL(_Link_.DITIPIVA,space(1))
      this.w_NUMDIC = NVL(_Link_.DINUMDOC,0)
      this.w_SERDOC = NVL(_Link_.DISERDOC,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_MVRIFDIC = space(10)
      endif
      this.w_ANNDIC = space(4)
      this.w_DATDIC = ctod("  /  /  ")
      this.w_CODIVE = space(5)
      this.w_TIPDIC = space(1)
      this.w_OPEDIC = space(1)
      this.w_UTIDIC = 0
      this.w_TIPIVA = space(1)
      this.w_NUMDIC = 0
      this.w_SERDOC = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIC_INTE_IDX,2])+'\'+cp_ToStr(_Link_.DISERIAL,1)
      cp_ShowWarn(i_cKey,this.DIC_INTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVRIFDIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCODAGE
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCODAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_MVCODAGE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO,AGCZOAGE,AGCATPRO,AGSCOPAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_MVCODAGE))
          select AGCODAGE,AGDESAGE,AGDTOBSO,AGCZOAGE,AGCATPRO,AGSCOPAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCODAGE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVCODAGE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oMVCODAGE_1_22'),i_cWhere,'GSAR_AGE',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO,AGCZOAGE,AGCATPRO,AGSCOPAG";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO,AGCZOAGE,AGCATPRO,AGSCOPAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCODAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO,AGCZOAGE,AGCATPRO,AGSCOPAG";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_MVCODAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_MVCODAGE)
            select AGCODAGE,AGDESAGE,AGDTOBSO,AGCZOAGE,AGCATPRO,AGSCOPAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCODAGE = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGE = NVL(_Link_.AGDESAGE,space(35))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
      this.w_CODAG2 = NVL(_Link_.AGCZOAGE,space(5))
      this.w_AGEPRO = NVL(_Link_.AGCATPRO,space(5))
      this.w_AGSCOPAG = NVL(_Link_.AGSCOPAG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVCODAGE = space(5)
      endif
      this.w_DESAGE = space(35)
      this.w_DTOBSO = ctod("  /  /  ")
      this.w_CODAG2 = space(5)
      this.w_AGEPRO = space(5)
      this.w_AGSCOPAG = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        endif
        this.w_MVCODAGE = space(5)
        this.w_DESAGE = space(35)
        this.w_DTOBSO = ctod("  /  /  ")
        this.w_CODAG2 = space(5)
        this.w_AGEPRO = space(5)
        this.w_AGSCOPAG = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCODAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCODAG2
  func Link_1_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCODAG2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_MVCODAG2)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE,AGCATPRO,AGSCOPAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_MVCODAG2))
          select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE,AGCATPRO,AGSCOPAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCODAG2)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVCODAG2) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oMVCODAG2_1_24'),i_cWhere,'GSAR_AGE',"",'GSVE1KFT.AGENTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE,AGCATPRO,AGSCOPAG";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE,AGCATPRO,AGSCOPAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCODAG2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE,AGCATPRO,AGSCOPAG";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_MVCODAG2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_MVCODAG2)
            select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE,AGCATPRO,AGSCOPAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCODAG2 = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAG2 = NVL(_Link_.AGDESAGE,space(35))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
      this.w_TIPAG2 = NVL(_Link_.AGTIPAGE,space(1))
      this.w_AGEPR2 = NVL(_Link_.AGCATPRO,space(5))
      this.w_AGSCOPA2 = NVL(_Link_.AGSCOPAG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVCODAG2 = space(5)
      endif
      this.w_DESAG2 = space(35)
      this.w_DTOBSO = ctod("  /  /  ")
      this.w_TIPAG2 = space(1)
      this.w_AGEPR2 = space(5)
      this.w_AGSCOPA2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPAG2 $ ' C' AND (EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto oppure non capoarea")
        endif
        this.w_MVCODAG2 = space(5)
        this.w_DESAG2 = space(35)
        this.w_DTOBSO = ctod("  /  /  ")
        this.w_TIPAG2 = space(1)
        this.w_AGEPR2 = space(5)
        this.w_AGSCOPA2 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCODAG2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCODIVE
  func Link_1_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCODIVE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_MVCODIVE)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVBOLIVA,IVPERIVA,IVDTOBSO,IVPERIND";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_MVCODIVE))
          select IVCODIVA,IVDESIVA,IVBOLIVA,IVPERIVA,IVDTOBSO,IVPERIND;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCODIVE)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVCODIVE) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oMVCODIVE_1_26'),i_cWhere,'GSAR_AIV',"",'GSVE0MDV.VOCIIVA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVBOLIVA,IVPERIVA,IVDTOBSO,IVPERIND";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDESIVA,IVBOLIVA,IVPERIVA,IVDTOBSO,IVPERIND;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCODIVE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVBOLIVA,IVPERIVA,IVDTOBSO,IVPERIND";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_MVCODIVE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_MVCODIVE)
            select IVCODIVA,IVDESIVA,IVBOLIVA,IVPERIVA,IVDTOBSO,IVPERIND;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCODIVE = NVL(_Link_.IVCODIVA,space(5))
      this.w_DESAPP = NVL(_Link_.IVDESIVA,space(35))
      this.w_BOLIVE = NVL(_Link_.IVBOLIVA,space(1))
      this.w_PERIVE = NVL(_Link_.IVPERIVA,0)
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.IVDTOBSO),ctod("  /  /  "))
      this.w_INDIVE = NVL(_Link_.IVPERIND,0)
    else
      if i_cCtrl<>'Load'
        this.w_MVCODIVE = space(5)
      endif
      this.w_DESAPP = space(35)
      this.w_BOLIVE = space(1)
      this.w_PERIVE = 0
      this.w_DTOBSO = ctod("  /  /  ")
      this.w_INDIVE = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice IVA esente incongruente o obsoleto")
        endif
        this.w_MVCODIVE = space(5)
        this.w_DESAPP = space(35)
        this.w_BOLIVE = space(1)
        this.w_PERIVE = 0
        this.w_DTOBSO = ctod("  /  /  ")
        this.w_INDIVE = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCODIVE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCAUCON
  func Link_1_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCAUCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCAUCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPDOC,CCFLPPRO,CCTIPREG,CCFLPART";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_MVCAUCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_MVCAUCON)
            select CCCODICE,CCDESCRI,CCTIPDOC,CCFLPPRO,CCTIPREG,CCFLPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCAUCON = NVL(_Link_.CCCODICE,space(5))
      this.w_DESCAC = NVL(_Link_.CCDESCRI,space(35))
      this.w_TIPDOC = NVL(_Link_.CCTIPDOC,space(2))
      this.w_CFLPP = NVL(_Link_.CCFLPPRO,space(1))
      this.w_TIPREG = NVL(_Link_.CCTIPREG,space(1))
      this.w_PARCAU = NVL(_Link_.CCFLPART,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVCAUCON = space(5)
      endif
      this.w_DESCAC = space(35)
      this.w_TIPDOC = space(2)
      this.w_CFLPP = space(1)
      this.w_TIPREG = space(1)
      this.w_PARCAU = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCAUCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVTCAMAG
  func Link_1_80(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVTCAMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACM',True,'CAM_AGAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_MVTCAMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMCAUCOL,CMVARVAL,CMDESCRI,CMDTOBSO,CMFLAVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_MVTCAMAG))
          select CMCODICE,CMCAUCOL,CMVARVAL,CMDESCRI,CMDTOBSO,CMFLAVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVTCAMAG)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CMCAUCOL like "+cp_ToStrODBC(trim(this.w_MVTCAMAG)+"%");

            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMCAUCOL,CMVARVAL,CMDESCRI,CMDTOBSO,CMFLAVAL";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CMCAUCOL like "+cp_ToStr(trim(this.w_MVTCAMAG)+"%");

            select CMCODICE,CMCAUCOL,CMVARVAL,CMDESCRI,CMDTOBSO,CMFLAVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MVTCAMAG) and !this.bDontReportError
            deferred_cp_zoom('CAM_AGAZ','*','CMCODICE',cp_AbsName(oSource.parent,'oMVTCAMAG_1_80'),i_cWhere,'GSMA_ACM',"Causali magazzino",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMCAUCOL,CMVARVAL,CMDESCRI,CMDTOBSO,CMFLAVAL";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMCAUCOL,CMVARVAL,CMDESCRI,CMDTOBSO,CMFLAVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVTCAMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMCAUCOL,CMVARVAL,CMDESCRI,CMDTOBSO,CMFLAVAL";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_MVTCAMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_MVTCAMAG)
            select CMCODICE,CMCAUCOL,CMVARVAL,CMDESCRI,CMDTOBSO,CMFLAVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVTCAMAG = NVL(_Link_.CMCODICE,space(5))
      this.w_CAUCOL = NVL(_Link_.CMCAUCOL,space(5))
      this.w_VARVAL = NVL(_Link_.CMVARVAL,space(1))
      this.w_DESCAU = NVL(_Link_.CMDESCRI,space(35))
      this.w_OBSCAU = NVL(cp_ToDate(_Link_.CMDTOBSO),ctod("  /  /  "))
      this.w_FLAVAL = NVL(_Link_.CMFLAVAL,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVTCAMAG = space(5)
      endif
      this.w_CAUCOL = space(5)
      this.w_VARVAL = space(1)
      this.w_DESCAU = space(35)
      this.w_OBSCAU = ctod("  /  /  ")
      this.w_FLAVAL = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKCAUMA(.w_MVTCAMAG,.w_CAUCOL,.w_FLGEFA,.w_FLAVAL,.w_OBSCAU,.w_OBTEST,'V', .w_MVFLVEAC, .w_MVCLADOC)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MVTCAMAG = space(5)
        this.w_CAUCOL = space(5)
        this.w_VARVAL = space(1)
        this.w_DESCAU = space(35)
        this.w_OBSCAU = ctod("  /  /  ")
        this.w_FLAVAL = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVTCAMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMVCODAGE_1_22.value==this.w_MVCODAGE)
      this.oPgFrm.Page1.oPag.oMVCODAGE_1_22.value=this.w_MVCODAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oMVCODAG2_1_24.value==this.w_MVCODAG2)
      this.oPgFrm.Page1.oPag.oMVCODAG2_1_24.value=this.w_MVCODAG2
    endif
    if not(this.oPgFrm.Page1.oPag.oDESAG2_1_25.value==this.w_DESAG2)
      this.oPgFrm.Page1.oPag.oDESAG2_1_25.value=this.w_DESAG2
    endif
    if not(this.oPgFrm.Page1.oPag.oMVCODIVE_1_26.value==this.w_MVCODIVE)
      this.oPgFrm.Page1.oPag.oMVCODIVE_1_26.value=this.w_MVCODIVE
    endif
    if not(this.oPgFrm.Page1.oPag.oMVTFRAGG_1_27.value==this.w_MVTFRAGG)
      this.oPgFrm.Page1.oPag.oMVTFRAGG_1_27.value=this.w_MVTFRAGG
    endif
    if not(this.oPgFrm.Page1.oPag.oMVTINCOM_1_37.value==this.w_MVTINCOM)
      this.oPgFrm.Page1.oPag.oMVTINCOM_1_37.value=this.w_MVTINCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oMVTFICOM_1_39.value==this.w_MVTFICOM)
      this.oPgFrm.Page1.oPag.oMVTFICOM_1_39.value=this.w_MVTFICOM
    endif
    if not(this.oPgFrm.Page1.oPag.oMVFLVABD_1_40.RadioValue()==this.w_MVFLVABD)
      this.oPgFrm.Page1.oPag.oMVFLVABD_1_40.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMVNUMEST_1_41.value==this.w_MVNUMEST)
      this.oPgFrm.Page1.oPag.oMVNUMEST_1_41.value=this.w_MVNUMEST
    endif
    if not(this.oPgFrm.Page1.oPag.oMVALFEST_1_42.value==this.w_MVALFEST)
      this.oPgFrm.Page1.oPag.oMVALFEST_1_42.value=this.w_MVALFEST
    endif
    if not(this.oPgFrm.Page1.oPag.oMVDATEST_1_43.value==this.w_MVDATEST)
      this.oPgFrm.Page1.oPag.oMVDATEST_1_43.value=this.w_MVDATEST
    endif
    if not(this.oPgFrm.Page1.oPag.oMVSEREST_1_44.value==this.w_MVSEREST)
      this.oPgFrm.Page1.oPag.oMVSEREST_1_44.value=this.w_MVSEREST
    endif
    if not(this.oPgFrm.Page1.oPag.oMVFLSEND_1_45.RadioValue()==this.w_MVFLSEND)
      this.oPgFrm.Page1.oPag.oMVFLSEND_1_45.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESAGE_1_55.value==this.w_DESAGE)
      this.oPgFrm.Page1.oPag.oDESAGE_1_55.value=this.w_DESAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oMVTCAMAG_1_80.value==this.w_MVTCAMAG)
      this.oPgFrm.Page1.oPag.oMVTCAMAG_1_80.value=this.w_MVTCAMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAU_1_81.value==this.w_DESCAU)
      this.oPgFrm.Page1.oPag.oDESCAU_1_81.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oMVFLSCOM_1_89.RadioValue()==this.w_MVFLSCOM)
      this.oPgFrm.Page1.oPag.oMVFLSCOM_1_89.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDICSN_1_94.RadioValue()==this.w_DICSN)
      this.oPgFrm.Page1.oPag.oDICSN_1_94.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMDIC_1_95.value==this.w_NUMDIC)
      this.oPgFrm.Page1.oPag.oNUMDIC_1_95.value=this.w_NUMDIC
    endif
    if not(this.oPgFrm.Page1.oPag.oSERDOC_1_96.value==this.w_SERDOC)
      this.oPgFrm.Page1.oPag.oSERDOC_1_96.value=this.w_SERDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oANNDIC_1_97.value==this.w_ANNDIC)
      this.oPgFrm.Page1.oPag.oANNDIC_1_97.value=this.w_ANNDIC
    endif
    if not(this.oPgFrm.Page1.oPag.oDATDIC_1_98.value==this.w_DATDIC)
      this.oPgFrm.Page1.oPag.oDATDIC_1_98.value=this.w_DATDIC
    endif
    if not(this.oPgFrm.Page1.oPag.oMVAGG_01_1_107.value==this.w_MVAGG_01)
      this.oPgFrm.Page1.oPag.oMVAGG_01_1_107.value=this.w_MVAGG_01
    endif
    if not(this.oPgFrm.Page1.oPag.oMVAGG_02_1_108.value==this.w_MVAGG_02)
      this.oPgFrm.Page1.oPag.oMVAGG_02_1_108.value=this.w_MVAGG_02
    endif
    if not(this.oPgFrm.Page1.oPag.oMVAGG_03_1_109.value==this.w_MVAGG_03)
      this.oPgFrm.Page1.oPag.oMVAGG_03_1_109.value=this.w_MVAGG_03
    endif
    if not(this.oPgFrm.Page1.oPag.oMVAGG_04_1_110.value==this.w_MVAGG_04)
      this.oPgFrm.Page1.oPag.oMVAGG_04_1_110.value=this.w_MVAGG_04
    endif
    if not(this.oPgFrm.Page1.oPag.oMVAGG_05_1_111.value==this.w_MVAGG_05)
      this.oPgFrm.Page1.oPag.oMVAGG_05_1_111.value=this.w_MVAGG_05
    endif
    if not(this.oPgFrm.Page1.oPag.oMVAGG_06_1_112.value==this.w_MVAGG_06)
      this.oPgFrm.Page1.oPag.oMVAGG_06_1_112.value=this.w_MVAGG_06
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST)  and not(.w_MVTIPCON<>'C')  and (g_PERAGE='S')  and not(empty(.w_MVCODAGE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVCODAGE_1_22.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure obsoleto")
          case   not(.w_TIPAG2 $ ' C' AND (EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST))  and not(.w_MVTIPCON<>'C')  and (g_PERAGE='S' AND NOT EMPTY(.w_MVCODAGE))  and not(empty(.w_MVCODAG2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVCODAG2_1_24.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure obsoleto oppure non capoarea")
          case   not(EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST)  and not(empty(.w_MVCODIVE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVCODIVE_1_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice IVA esente incongruente o obsoleto")
          case   not(EMPTY(.w_MVTINCOM) OR .w_MVTINCOM<=.w_MVTFICOM)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVTFICOM_1_39.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(CHKCAUMA(.w_MVTCAMAG,.w_CAUCOL,.w_FLGEFA,.w_FLAVAL,.w_OBSCAU,.w_OBTEST,'V', .w_MVFLVEAC, .w_MVCLADOC))  and (.w_FLCCAU = 'S')  and not(empty(.w_MVTCAMAG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVTCAMAG_1_80.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MVTIPDOC = this.w_MVTIPDOC
    this.o_CODIVE = this.w_CODIVE
    return

enddefine

* --- Define pages as container
define class tgsor_kftPag1 as StdContainer
  Width  = 530
  height = 466
  stdWidth  = 530
  stdheight = 466
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMVCODAGE_1_22 as StdField with uid="OCSJILINDX",rtseq=22,rtrep=.f.,;
    cFormVar = "w_MVCODAGE", cQueryName = "MVCODAGE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
    ToolTipText = "Codice agente",;
    HelpContextID = 44704267,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=144, Top=42, cSayPict='"XXXXX"', cGetPict='"XXXXX"', InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_MVCODAGE"

  func oMVCODAGE_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERAGE='S')
    endwith
   endif
  endfunc

  func oMVCODAGE_1_22.mHide()
    with this.Parent.oContained
      return (.w_MVTIPCON<>'C')
    endwith
  endfunc

  func oMVCODAGE_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCODAGE_1_22.ecpDrop(oSource)
    this.Parent.oContained.link_1_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVCODAGE_1_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oMVCODAGE_1_22'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"",'',this.parent.oContained
  endproc
  proc oMVCODAGE_1_22.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_MVCODAGE
     i_obj.ecpSave()
  endproc

  add object oMVCODAG2_1_24 as StdField with uid="VUNUNJWQVD",rtseq=24,rtrep=.f.,;
    cFormVar = "w_MVCODAG2", cQueryName = "MVCODAG2",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto oppure non capoarea",;
    ToolTipText = "Eventuale codice capoarea",;
    HelpContextID = 44704248,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=144, Top=71, cSayPict='"XXXXX"', cGetPict='"XXXXX"', InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_MVCODAG2"

  func oMVCODAG2_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERAGE='S' AND NOT EMPTY(.w_MVCODAGE))
    endwith
   endif
  endfunc

  func oMVCODAG2_1_24.mHide()
    with this.Parent.oContained
      return (.w_MVTIPCON<>'C')
    endwith
  endfunc

  func oMVCODAG2_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCODAG2_1_24.ecpDrop(oSource)
    this.Parent.oContained.link_1_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVCODAG2_1_24.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oMVCODAG2_1_24'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"",'GSVE1KFT.AGENTI_VZM',this.parent.oContained
  endproc
  proc oMVCODAG2_1_24.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_MVCODAG2
     i_obj.ecpSave()
  endproc

  add object oDESAG2_1_25 as StdField with uid="SQQHGFMWDH",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESAG2", cQueryName = "DESAG2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 204664778,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=212, Top=71, InputMask=replicate('X',35)

  func oDESAG2_1_25.mHide()
    with this.Parent.oContained
      return (.w_MVTIPCON<>'C')
    endwith
  endfunc

  add object oMVCODIVE_1_26 as StdField with uid="BYCBTUVBNC",rtseq=26,rtrep=.f.,;
    cFormVar = "w_MVCODIVE", cQueryName = "MVCODIVE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice IVA esente incongruente o obsoleto",;
    ToolTipText = "Codice IVA applicato in caso di documento esente o non imponibile o IVA agevolata",;
    HelpContextID = 178921995,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=144, Top=100, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_MVCODIVE"

  func oMVCODIVE_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_26('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCODIVE_1_26.ecpDrop(oSource)
    this.Parent.oContained.link_1_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVCODIVE_1_26.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oMVCODIVE_1_26'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"",'GSVE0MDV.VOCIIVA_VZM',this.parent.oContained
  endproc
  proc oMVCODIVE_1_26.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_MVCODIVE
     i_obj.ecpSave()
  endproc

  add object oMVTFRAGG_1_27 as StdField with uid="NJJDUWDCLO",rtseq=27,rtrep=.f.,;
    cFormVar = "w_MVTFRAGG", cQueryName = "MVTFRAGG",;
    bObbl = .f. , nPag = 1, value=space(1), bMultilanguage =  .f.,;
    ToolTipText = "Livello di raggruppamento righe documento",;
    HelpContextID = 58864141,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=394, Top=100, cSayPict="'9'", cGetPict="'9'", InputMask=replicate('X',1), tabstop=.f.


  add object oObj_1_31 as cp_runprogram with uid="WFSDNNKFEA",left=9, top=499, width=237,height=20,;
    caption='GSVE_BED(D)',;
   bGlobalFont=.t.,;
    prg='GSVE_BED("D")',;
    cEvent = "w_DICSN Changed",;
    nPag=1;
    , HelpContextID = 89400618

  add object oMVTINCOM_1_37 as StdField with uid="MMGEAQYRTX",rtseq=31,rtrep=.f.,;
    cFormVar = "w_MVTINCOM", cQueryName = "MVTINCOM",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio competenza contabile",;
    HelpContextID = 180014573,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=144, Top=129


  add object oObj_1_38 as cp_runprogram with uid="JUSZDWHEXH",left=9, top=518, width=237,height=20,;
    caption='GSVE_BED(I)',;
   bGlobalFont=.t.,;
    prg='GSVE_BED("I")',;
    cEvent = "w_MVTINCOM Changed",;
    nPag=1;
    , HelpContextID = 89401898

  add object oMVTFICOM_1_39 as StdField with uid="DGMZQLJSDW",rtseq=32,rtrep=.f.,;
    cFormVar = "w_MVTFICOM", cQueryName = "MVTFICOM",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di fine competenza contabile",;
    HelpContextID = 185454061,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=291, Top=129

  func oMVTFICOM_1_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_MVTINCOM) OR .w_MVTINCOM<=.w_MVTFICOM)
    endwith
    return bRes
  endfunc

  add object oMVFLVABD_1_40 as StdCheck with uid="BJRLERBNOE",rtseq=33,rtrep=.f.,left=396, top=129, caption="Beni deperibili",;
    ToolTipText = "Flag beni deperibili",;
    HelpContextID = 63394314,;
    cFormVar="w_MVFLVABD", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMVFLVABD_1_40.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oMVFLVABD_1_40.GetRadio()
    this.Parent.oContained.w_MVFLVABD = this.RadioValue()
    return .t.
  endfunc

  func oMVFLVABD_1_40.SetRadio()
    this.Parent.oContained.w_MVFLVABD=trim(this.Parent.oContained.w_MVFLVABD)
    this.value = ;
      iif(this.Parent.oContained.w_MVFLVABD=='S',1,;
      0)
  endfunc

  add object oMVNUMEST_1_41 as StdField with uid="ZFDIZSTERM",rtseq=34,rtrep=.f.,;
    cFormVar = "w_MVNUMEST", cQueryName = "MVNUMEST",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero del documento di riferimento",;
    HelpContextID = 121688602,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=144, Top=158, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue =999999999999999

  add object oMVALFEST_1_42 as StdField with uid="BDMLPUTXDM",rtseq=35,rtrep=.f.,;
    cFormVar = "w_MVALFEST", cQueryName = "MVALFEST",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Parte alfanumerica del documento di riferimento",;
    HelpContextID = 113705498,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=284, Top=158, cSayPict="'!!!!!!!!!!'", cGetPict="'!!!!!!!!!!'", InputMask=replicate('X',10)

  add object oMVDATEST_1_43 as StdField with uid="FGVIQKWEIW",rtseq=36,rtrep=.f.,;
    cFormVar = "w_MVDATEST", cQueryName = "MVDATEST",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data del documento di riferimento",;
    HelpContextID = 127676954,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=394, Top=158

  add object oMVSEREST_1_44 as StdField with uid="CUQZQDKSRH",rtseq=37,rtrep=.f.,;
    cFormVar = "w_MVSEREST", cQueryName = "MVSEREST",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "riferimento esterno EDI",;
    HelpContextID = 125903386,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=144, Top=183, InputMask=replicate('X',30)

  func oMVSEREST_1_44.mHide()
    with this.Parent.oContained
      return (g_VEFA<>'S')
    endwith
  endfunc

  add object oMVFLSEND_1_45 as StdCheck with uid="YYLHDBQJED",rtseq=38,rtrep=.f.,left=396, top=182, caption="Generato file EDI", enabled=.f.,;
    HelpContextID = 141078006,;
    cFormVar="w_MVFLSEND", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMVFLSEND_1_45.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oMVFLSEND_1_45.GetRadio()
    this.Parent.oContained.w_MVFLSEND = this.RadioValue()
    return .t.
  endfunc

  func oMVFLSEND_1_45.SetRadio()
    this.Parent.oContained.w_MVFLSEND=trim(this.Parent.oContained.w_MVFLSEND)
    this.value = ;
      iif(this.Parent.oContained.w_MVFLSEND=='S',1,;
      0)
  endfunc

  func oMVFLSEND_1_45.mHide()
    with this.Parent.oContained
      return (g_VEFA<>'S')
    endwith
  endfunc


  add object oObj_1_46 as cp_runprogram with uid="GNVPFIAFZN",left=9, top=537, width=237,height=20,;
    caption='GSVE_BED(F)',;
   bGlobalFont=.t.,;
    prg='GSVE_BED("F")',;
    cEvent = "w_MVTFICOM Changed",;
    nPag=1;
    , HelpContextID = 89401130


  add object oBtn_1_47 as StdButton with uid="OHUIXPVXJY",left=423, top=415, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare le scelte impostate";
    , HelpContextID = 48822554;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_47.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_49 as StdButton with uid="PLSPMOJOMK",left=474, top=415, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire senza confermare le scelte";
    , HelpContextID = 41533882;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_49.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESAGE_1_55 as StdField with uid="ODBNJFMJVL",rtseq=40,rtrep=.f.,;
    cFormVar = "w_DESAGE", cQueryName = "DESAGE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 154333130,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=212, Top=42, InputMask=replicate('X',35)

  func oDESAGE_1_55.mHide()
    with this.Parent.oContained
      return (.w_MVTIPCON<>'C')
    endwith
  endfunc


  add object oObj_1_58 as cp_runprogram with uid="LMFFPFDEQR",left=9, top=480, width=237,height=20,;
    caption='GSVE_BED(A)',;
   bGlobalFont=.t.,;
    prg="GSVE_BED('A')",;
    cEvent = "w_MVCODAGE Changed",;
    nPag=1;
    , HelpContextID = 89399850


  add object oObj_1_60 as cp_runprogram with uid="UUFHSMHLAX",left=9, top=556, width=237,height=20,;
    caption='GSVE_BED(C)',;
   bGlobalFont=.t.,;
    prg="GSVE_BED('C')",;
    cEvent = "Update end",;
    nPag=1;
    , HelpContextID = 89400362


  add object oBtn_1_68 as StdButton with uid="GHNRXKQGVK",left=240, top=415, width=48,height=45,;
    CpPicture="BMP\tracciabilita.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere alla funzione di tracciabilità del documento";
    , HelpContextID = 187375912;
    , TabStop=.f.,Caption='\<Tracciab.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_68.Click()
      with this.Parent.oContained
        do GSVE_BDT with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_68.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_ORDI<>'S')
     endwith
    endif
  endfunc

  add object oMVTCAMAG_1_80 as StdField with uid="GXKZJANOVX",rtseq=62,rtrep=.f.,;
    cFormVar = "w_MVTCAMAG", cQueryName = "MVTCAMAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice della causale magazzino",;
    HelpContextID = 242168333,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=144, Top=208, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAM_AGAZ", cZoomOnZoom="GSMA_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_MVTCAMAG"

  func oMVTCAMAG_1_80.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLCCAU = 'S')
    endwith
   endif
  endfunc

  func oMVTCAMAG_1_80.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_80('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVTCAMAG_1_80.ecpDrop(oSource)
    this.Parent.oContained.link_1_80('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVTCAMAG_1_80.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAM_AGAZ','*','CMCODICE',cp_AbsName(this.parent,'oMVTCAMAG_1_80'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACM',"Causali magazzino",'',this.parent.oContained
  endproc
  proc oMVTCAMAG_1_80.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_MVTCAMAG
     i_obj.ecpSave()
  endproc

  add object oDESCAU_1_81 as StdField with uid="LXEOTEPTNR",rtseq=63,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 107941942,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=212, Top=208, InputMask=replicate('X',35)

  add object oMVFLSCOM_1_89 as StdCheck with uid="WCZPARSCRJ",rtseq=70,rtrep=.f.,left=144, top=235, caption="Calcola sconti su omaggi",;
    ToolTipText = "Se attivo: calcola sconti su omaggio",;
    HelpContextID = 174632429,;
    cFormVar="w_MVFLSCOM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMVFLSCOM_1_89.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oMVFLSCOM_1_89.GetRadio()
    this.Parent.oContained.w_MVFLSCOM = this.RadioValue()
    return .t.
  endfunc

  func oMVFLSCOM_1_89.SetRadio()
    this.Parent.oContained.w_MVFLSCOM=trim(this.Parent.oContained.w_MVFLSCOM)
    this.value = ;
      iif(this.Parent.oContained.w_MVFLSCOM=='S',1,;
      0)
  endfunc

  func oMVFLSCOM_1_89.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.oParentObject.cFunction = 'Load' AND .w_NUMRIG < 1)
    endwith
   endif
  endfunc


  add object oBtn_1_92 as StdButton with uid="RBCWUHZZNH",left=192, top=415, width=48,height=45,;
    CpPicture="BMP\doc1.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere al documento generato dalla evasione prodotti finiti";
    , HelpContextID = 141688415;
    , Caption='\<Prodotti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_92.Click()
      with this.Parent.oContained
        GSAR_BZM(this.Parent.oContained,IIF(Empty(.w_SERORI),.w_MVRIFESP, .w_SERORI), -20)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_92.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ((NOT EMPTY(.w_MVRIFESP) Or Not Empty(.w_SERORI)) AND g_EACD='S')
      endwith
    endif
  endfunc

  func oBtn_1_92.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return ((EMPTY(.w_MVRIFESP) And Empty(.w_SERORI)) OR g_EACD<>'S')
     endwith
    endif
  endfunc

  add object oDICSN_1_94 as StdCheck with uid="VWTJVNOTNY",rtseq=73,rtrep=.f.,left=27, top=13, caption="Rif. dichiarazione:",;
    HelpContextID = 229764042,;
    cFormVar="w_DICSN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDICSN_1_94.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oDICSN_1_94.GetRadio()
    this.Parent.oContained.w_DICSN = this.RadioValue()
    return .t.
  endfunc

  func oDICSN_1_94.SetRadio()
    this.Parent.oContained.w_DICSN=trim(this.Parent.oContained.w_DICSN)
    this.value = ;
      iif(this.Parent.oContained.w_DICSN=='S',1,;
      0)
  endfunc

  func oDICSN_1_94.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_MVRIFDIC))
    endwith
   endif
  endfunc

  func oDICSN_1_94.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_MVRIFDIC))
    endwith
  endfunc

  add object oNUMDIC_1_95 as StdField with uid="XAAHAAWCIG",rtseq=74,rtrep=.f.,;
    cFormVar = "w_NUMDIC", cQueryName = "NUMDIC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero della eventuale dichiarazione di esenzione",;
    HelpContextID = 185614122,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=144, Top=13

  add object oSERDOC_1_96 as StdField with uid="JCUUFQTVFB",rtseq=75,rtrep=.f.,;
    cFormVar = "w_SERDOC", cQueryName = "SERDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 179306202,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=216, Top=13, InputMask=replicate('X',3)

  add object oANNDIC_1_97 as StdField with uid="VRNLSZZRRL",rtseq=76,rtrep=.f.,;
    cFormVar = "w_ANNDIC", cQueryName = "ANNDIC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno della eventuale dichiarazione di esenzione",;
    HelpContextID = 185612026,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=260, Top=13, InputMask=replicate('X',4)

  add object oDATDIC_1_98 as StdField with uid="MUZRFQLKAF",rtseq=77,rtrep=.f.,;
    cFormVar = "w_DATDIC", cQueryName = "DATDIC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data della eventuale dichiarazione di esenzione",;
    HelpContextID = 185590730,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=337, Top=13


  add object oBtn_1_99 as StdButton with uid="PKKSUWHQWC",left=416, top=13, width=20,height=19,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare la dichiarazione di intento";
    , HelpContextID = 48650282;
  , bGlobalFont=.t.

    proc oBtn_1_99.Click()
      do GSVE_KDI with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oMVAGG_01_1_107 as StdField with uid="NLDMPNYWUJ",rtseq=82,rtrep=.f.,;
    cFormVar = "w_MVAGG_01", cQueryName = "MVAGG_01",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 13763063,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=212, Top=264, InputMask=replicate('X',15)

  add object oMVAGG_02_1_108 as StdField with uid="FVGOOVMXGE",rtseq=83,rtrep=.f.,;
    cFormVar = "w_MVAGG_02", cQueryName = "MVAGG_02",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 13763064,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=212, Top=289, InputMask=replicate('X',15)

  add object oMVAGG_03_1_109 as StdField with uid="QFPECASIYR",rtseq=84,rtrep=.f.,;
    cFormVar = "w_MVAGG_03", cQueryName = "MVAGG_03",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 13763065,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=212, Top=314, InputMask=replicate('X',15)

  add object oMVAGG_04_1_110 as StdField with uid="NUIJEEJKYX",rtseq=85,rtrep=.f.,;
    cFormVar = "w_MVAGG_04", cQueryName = "MVAGG_04",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 13763066,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=212, Top=339, InputMask=replicate('X',15)

  add object oMVAGG_05_1_111 as StdField with uid="CAKNRPLCGH",rtseq=86,rtrep=.f.,;
    cFormVar = "w_MVAGG_05", cQueryName = "MVAGG_05",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 13763067,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=212, Top=364

  add object oMVAGG_06_1_112 as StdField with uid="UGTDQKRBLY",rtseq=87,rtrep=.f.,;
    cFormVar = "w_MVAGG_06", cQueryName = "MVAGG_06",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 13763068,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=212, Top=389


  add object CAMAGG01 as cp_calclbl with uid="JZCKXLHDHV",left=9, top=265, width=202,height=18,;
    caption='Campo aggiuntivo 1',;
   bGlobalFont=.t.,;
    caption="Campo 1",Alignment =1,;
    nPag=1;
    , HelpContextID = 181158036


  add object CAMAGG02 as cp_calclbl with uid="YBFIFIXNYF",left=9, top=291, width=202,height=18,;
    caption='Campo aggiuntivo 2',;
   bGlobalFont=.t.,;
    caption="Campo 2",Alignment =1,;
    nPag=1;
    , HelpContextID = 181157780


  add object CAMAGG03 as cp_calclbl with uid="EELMGPXNWQ",left=9, top=316, width=202,height=18,;
    caption='Campo aggiuntivo 3',;
   bGlobalFont=.t.,;
    caption="Campo 3",alignment =1,;
    nPag=1;
    , HelpContextID = 181157524


  add object CAMAGG04 as cp_calclbl with uid="WWUGRPNAOG",left=9, top=341, width=202,height=18,;
    caption='Campo aggiuntivo 4',;
   bGlobalFont=.t.,;
    caption="Campo 4",alignment =1,;
    nPag=1;
    , HelpContextID = 181157268


  add object CAMAGG05 as cp_calclbl with uid="BLEZPMMBEJ",left=9, top=366, width=202,height=18,;
    caption='Campo aggiuntivo 5',;
   bGlobalFont=.t.,;
    caption="Campo 5",alignment =1,;
    nPag=1;
    , HelpContextID = 181157012


  add object CAMAGG06 as cp_calclbl with uid="ORVYGKKIKW",left=9, top=391, width=202,height=18,;
    caption='Campo aggiuntivo 6',;
   bGlobalFont=.t.,;
    caption="Campo 6",alignment =1,;
    nPag=1;
    , HelpContextID = 181156756

  add object oStr_1_30 as StdString with uid="AZZYXUPBMS",Visible=.t., Left=15, Top=100,;
    Alignment=1, Width=127, Height=15,;
    Caption="Codice IVA esenzione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="SMSSENJGOW",Visible=.t., Left=16, Top=131,;
    Alignment=1, Width=126, Height=15,;
    Caption="Compet.contabile da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="HKPNJLDIYY",Visible=.t., Left=270, Top=129,;
    Alignment=1, Width=19, Height=15,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="RTOTUWRRHM",Visible=.t., Left=15, Top=42,;
    Alignment=1, Width=127, Height=15,;
    Caption="Agente:"  ;
  , bGlobalFont=.t.

  func oStr_1_34.mHide()
    with this.Parent.oContained
      return (.w_MVTIPCON<>'C')
    endwith
  endfunc

  add object oStr_1_35 as StdString with uid="DSSWKIVOGH",Visible=.t., Left=14, Top=71,;
    Alignment=1, Width=128, Height=15,;
    Caption="Capo area:"  ;
  , bGlobalFont=.t.

  func oStr_1_35.mHide()
    with this.Parent.oContained
      return (.w_MVTIPCON<>'C')
    endwith
  endfunc

  add object oStr_1_48 as StdString with uid="SRYTRRQOCP",Visible=.t., Left=248, Top=100,;
    Alignment=1, Width=143, Height=15,;
    Caption="Liv. raggruppamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="GKGOADKRRB",Visible=.t., Left=14, Top=159,;
    Alignment=1, Width=126, Height=18,;
    Caption="Rif.N.cliente:"  ;
  , bGlobalFont=.t.

  func oStr_1_50.mHide()
    with this.Parent.oContained
      return (.w_MVTIPCON<>'C')
    endwith
  endfunc

  add object oStr_1_51 as StdString with uid="LYZIESWVTB",Visible=.t., Left=265, Top=159,;
    Alignment=2, Width=15, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="IREERMHBUJ",Visible=.t., Left=369, Top=159,;
    Alignment=1, Width=22, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="SQRLPQSQMV",Visible=.t., Left=16, Top=159,;
    Alignment=1, Width=126, Height=18,;
    Caption="Rif.N.fornitore:"  ;
  , bGlobalFont=.t.

  func oStr_1_54.mHide()
    with this.Parent.oContained
      return (.w_MVTIPCON<>'F')
    endwith
  endfunc

  add object oStr_1_82 as StdString with uid="TPBTTIGVHB",Visible=.t., Left=7, Top=208,;
    Alignment=1, Width=135, Height=18,;
    Caption="Causale di magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_93 as StdString with uid="UXUVIYEKFH",Visible=.t., Left=22, Top=184,;
    Alignment=1, Width=120, Height=18,;
    Caption="Rif.esterno EDI:"  ;
  , bGlobalFont=.t.

  func oStr_1_93.mHide()
    with this.Parent.oContained
      return (g_VEFA<>'S')
    endwith
  endfunc

  add object oStr_1_100 as StdString with uid="THYYFUEPJR",Visible=.t., Left=253, Top=13,;
    Alignment=0, Width=8, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_101 as StdString with uid="MYVZGHLMUE",Visible=.t., Left=310, Top=13,;
    Alignment=1, Width=22, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_102 as StdString with uid="YUKQSOUSSG",Visible=.t., Left=209, Top=13,;
    Alignment=0, Width=8, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsor_kft','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
