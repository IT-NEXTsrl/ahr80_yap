* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_aum                                                        *
*              Unit� di misura                                                 *
*                                                                              *
*      Author: Zucchetti S.p.a.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1997-06-06                                                      *
* Last revis.: 2015-10-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_aum"))

* --- Class definition
define class tgsar_aum as StdForm
  Top    = 60
  Left   = 29

  * --- Standard Properties
  Width  = 418
  Height = 145+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-10-23"
  HelpContextID=192317591
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=8

  * --- Constant Properties
  UNIMIS_IDX = 0
  cFile = "UNIMIS"
  cKeySelect = "UMCODICE"
  cKeyWhere  = "UMCODICE=this.w_UMCODICE"
  cKeyWhereODBC = '"UMCODICE="+cp_ToStrODBC(this.w_UMCODICE)';

  cKeyWhereODBCqualified = '"UNIMIS.UMCODICE="+cp_ToStrODBC(this.w_UMCODICE)';

  cPrg = "gsar_aum"
  cComment = "Unit� di misura"
  icon = "anag.ico"
  cAutoZoom = 'GSAR0AUM'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_UMCODICE = space(3)
  w_UMDESCRI = space(35)
  w_UMFLFRAZ = space(1)
  w_UMMODUM2 = space(1)
  w_CANC = .F.
  w_UMFLTEMP = space(1)
  o_UMFLTEMP = space(1)
  w_UMDURORE = 0
  w_UMDURSEC = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'UNIMIS','gsar_aum')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_aumPag1","gsar_aum",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Unit� di misura")
      .Pages(1).HelpContextID = 62622639
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oUMCODICE_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='UNIMIS'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.UNIMIS_IDX,5],7]
    this.nPostItConn=i_TableProp[this.UNIMIS_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_UMCODICE = NVL(UMCODICE,space(3))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from UNIMIS where UMCODICE=KeySet.UMCODICE
    *
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('UNIMIS')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "UNIMIS.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' UNIMIS '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'UMCODICE',this.w_UMCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CANC = .f.
        .oPgFrm.Page1.oPag.oObj_1_1.Calculate()
        .w_UMCODICE = NVL(UMCODICE,space(3))
        .w_UMDESCRI = NVL(UMDESCRI,space(35))
        .w_UMFLFRAZ = NVL(UMFLFRAZ,space(1))
        .w_UMMODUM2 = NVL(UMMODUM2,space(1))
        .w_UMFLTEMP = NVL(UMFLTEMP,space(1))
        .w_UMDURORE = NVL(UMDURORE,0)
        .w_UMDURSEC = NVL(UMDURSEC,0)
        cp_LoadRecExtFlds(this,'UNIMIS')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_UMCODICE = space(3)
      .w_UMDESCRI = space(35)
      .w_UMFLFRAZ = space(1)
      .w_UMMODUM2 = space(1)
      .w_CANC = .f.
      .w_UMFLTEMP = space(1)
      .w_UMDURORE = 0
      .w_UMDURSEC = 0
      if .cFunction<>"Filter"
        .oPgFrm.Page1.oPag.oObj_1_1.Calculate()
          .DoRTCalc(1,3,.f.)
        .w_UMMODUM2 = ' '
          .DoRTCalc(5,5,.f.)
        .w_UMFLTEMP = 'N'
      endif
    endwith
    cp_BlankRecExtFlds(this,'UNIMIS')
    this.DoRTCalc(7,8,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oUMCODICE_1_2.enabled = i_bVal
      .Page1.oPag.oUMDESCRI_1_3.enabled = i_bVal
      .Page1.oPag.oUMFLFRAZ_1_6.enabled = i_bVal
      .Page1.oPag.oUMMODUM2_1_7.enabled = i_bVal
      .Page1.oPag.oUMFLTEMP_1_9.enabled = i_bVal
      .Page1.oPag.oUMDURORE_1_10.enabled = i_bVal
      .Page1.oPag.oUMDURSEC_1_13.enabled = i_bVal
      .Page1.oPag.oObj_1_1.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oUMCODICE_1_2.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oUMCODICE_1_2.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'UNIMIS',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UMCODICE,"UMCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UMDESCRI,"UMDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UMFLFRAZ,"UMFLFRAZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UMMODUM2,"UMMODUM2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UMFLTEMP,"UMFLTEMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UMDURORE,"UMDURORE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UMDURSEC,"UMDURSEC",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    i_lTable = "UNIMIS"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.UNIMIS_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      vx_exec("QUERY\GSMA_QM1.VQR,QUERY\GSMA_SAU.FRX",this)
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.UNIMIS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.UNIMIS_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into UNIMIS
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'UNIMIS')
        i_extval=cp_InsertValODBCExtFlds(this,'UNIMIS')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(UMCODICE,UMDESCRI,UMFLFRAZ,UMMODUM2,UMFLTEMP"+;
                  ",UMDURORE,UMDURSEC "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_UMCODICE)+;
                  ","+cp_ToStrODBC(this.w_UMDESCRI)+;
                  ","+cp_ToStrODBC(this.w_UMFLFRAZ)+;
                  ","+cp_ToStrODBC(this.w_UMMODUM2)+;
                  ","+cp_ToStrODBC(this.w_UMFLTEMP)+;
                  ","+cp_ToStrODBC(this.w_UMDURORE)+;
                  ","+cp_ToStrODBC(this.w_UMDURSEC)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'UNIMIS')
        i_extval=cp_InsertValVFPExtFlds(this,'UNIMIS')
        cp_CheckDeletedKey(i_cTable,0,'UMCODICE',this.w_UMCODICE)
        INSERT INTO (i_cTable);
              (UMCODICE,UMDESCRI,UMFLFRAZ,UMMODUM2,UMFLTEMP,UMDURORE,UMDURSEC  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_UMCODICE;
                  ,this.w_UMDESCRI;
                  ,this.w_UMFLFRAZ;
                  ,this.w_UMMODUM2;
                  ,this.w_UMFLTEMP;
                  ,this.w_UMDURORE;
                  ,this.w_UMDURSEC;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.UNIMIS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.UNIMIS_IDX,i_nConn)
      *
      * update UNIMIS
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'UNIMIS')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " UMDESCRI="+cp_ToStrODBC(this.w_UMDESCRI)+;
             ",UMFLFRAZ="+cp_ToStrODBC(this.w_UMFLFRAZ)+;
             ",UMMODUM2="+cp_ToStrODBC(this.w_UMMODUM2)+;
             ",UMFLTEMP="+cp_ToStrODBC(this.w_UMFLTEMP)+;
             ",UMDURORE="+cp_ToStrODBC(this.w_UMDURORE)+;
             ",UMDURSEC="+cp_ToStrODBC(this.w_UMDURSEC)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'UNIMIS')
        i_cWhere = cp_PKFox(i_cTable  ,'UMCODICE',this.w_UMCODICE  )
        UPDATE (i_cTable) SET;
              UMDESCRI=this.w_UMDESCRI;
             ,UMFLFRAZ=this.w_UMFLFRAZ;
             ,UMMODUM2=this.w_UMMODUM2;
             ,UMFLTEMP=this.w_UMFLTEMP;
             ,UMDURORE=this.w_UMDURORE;
             ,UMDURSEC=this.w_UMDURSEC;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.UNIMIS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.UNIMIS_IDX,i_nConn)
      *
      * delete UNIMIS
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'UMCODICE',this.w_UMCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_1.Calculate()
        if .o_UMFLTEMP<>.w_UMFLTEMP
          .Calculate_LEOETOHJGX()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,8,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_1.Calculate()
    endwith
  return

  proc Calculate_LEOETOHJGX()
    with this
          * --- Azzeramento durata
          .w_UMDURORE = IIF(.w_UMFLTEMP='N', 0, .w_UMDURORE)
          .w_UMDURSEC = IIF(.w_UMFLTEMP='N', 0, .w_UMDURSEC)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oUMMODUM2_1_7.visible=!this.oPgFrm.Page1.oPag.oUMMODUM2_1_7.mHide()
    this.oPgFrm.Page1.oPag.oUMFLTEMP_1_9.visible=!this.oPgFrm.Page1.oPag.oUMFLTEMP_1_9.mHide()
    this.oPgFrm.Page1.oPag.oUMDURORE_1_10.visible=!this.oPgFrm.Page1.oPag.oUMDURORE_1_10.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_11.visible=!this.oPgFrm.Page1.oPag.oStr_1_11.mHide()
    this.oPgFrm.Page1.oPag.oUMDURSEC_1_13.visible=!this.oPgFrm.Page1.oPag.oUMDURSEC_1_13.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_14.visible=!this.oPgFrm.Page1.oPag.oStr_1_14.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_1.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oUMCODICE_1_2.value==this.w_UMCODICE)
      this.oPgFrm.Page1.oPag.oUMCODICE_1_2.value=this.w_UMCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oUMDESCRI_1_3.value==this.w_UMDESCRI)
      this.oPgFrm.Page1.oPag.oUMDESCRI_1_3.value=this.w_UMDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oUMFLFRAZ_1_6.RadioValue()==this.w_UMFLFRAZ)
      this.oPgFrm.Page1.oPag.oUMFLFRAZ_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oUMMODUM2_1_7.RadioValue()==this.w_UMMODUM2)
      this.oPgFrm.Page1.oPag.oUMMODUM2_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oUMFLTEMP_1_9.RadioValue()==this.w_UMFLTEMP)
      this.oPgFrm.Page1.oPag.oUMFLTEMP_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oUMDURORE_1_10.value==this.w_UMDURORE)
      this.oPgFrm.Page1.oPag.oUMDURORE_1_10.value=this.w_UMDURORE
    endif
    if not(this.oPgFrm.Page1.oPag.oUMDURSEC_1_13.value==this.w_UMDURSEC)
      this.oPgFrm.Page1.oPag.oUMDURSEC_1_13.value=this.w_UMDURSEC
    endif
    cp_SetControlsValueExtFlds(this,'UNIMIS')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_UMCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oUMCODICE_1_2.SetFocus()
            i_bnoObbl = !empty(.w_UMCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_UMFLTEMP = this.w_UMFLTEMP
    return

enddefine

* --- Define pages as container
define class tgsar_aumPag1 as StdContainer
  Width  = 414
  height = 145
  stdWidth  = 414
  stdheight = 145
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_1_1 as cp_runprogram with uid="ROAAWEFSEL",left=242, top=178, width=137,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSAR_BUM",;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 166555674

  add object oUMCODICE_1_2 as StdField with uid="YKPVVDHHBE",rtseq=1,rtrep=.f.,;
    cFormVar = "w_UMCODICE", cQueryName = "UMCODICE",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice unit� di misura",;
    HelpContextID = 116782197,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=84, Top=14, InputMask=replicate('X',3)

  add object oUMDESCRI_1_3 as StdField with uid="RNEWOUJDPZ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_UMDESCRI", cQueryName = "UMDESCRI",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione unit� di misura",;
    HelpContextID = 202368113,;
   bGlobalFont=.t.,;
    Height=21, Width=323, Left=84, Top=42, InputMask=replicate('X',35)

  add object oUMFLFRAZ_1_6 as StdCheck with uid="DSEPPOJUFG",rtseq=3,rtrep=.f.,left=84, top=75, caption="Non frazionabile",;
    ToolTipText = "Se attivo: le quantit� movimentate non potranno avere decimali",;
    HelpContextID = 232309856,;
    cFormVar="w_UMFLFRAZ", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oUMFLFRAZ_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oUMFLFRAZ_1_6.GetRadio()
    this.Parent.oContained.w_UMFLFRAZ = this.RadioValue()
    return .t.
  endfunc

  func oUMFLFRAZ_1_6.SetRadio()
    this.Parent.oContained.w_UMFLFRAZ=trim(this.Parent.oContained.w_UMFLFRAZ)
    this.value = ;
      iif(this.Parent.oContained.w_UMFLFRAZ=='S',1,;
      0)
  endfunc

  add object oUMMODUM2_1_7 as StdCheck with uid="ZYRSDHTTUY",rtseq=4,rtrep=.f.,left=245, top=75, caption="Forza U.M. secondarie",;
    ToolTipText = "Se attivo: arrotonda la qt� se movimentata la 2^ U.M. frazionabile",;
    HelpContextID = 84585336,;
    cFormVar="w_UMMODUM2", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oUMMODUM2_1_7.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oUMMODUM2_1_7.GetRadio()
    this.Parent.oContained.w_UMMODUM2 = this.RadioValue()
    return .t.
  endfunc

  func oUMMODUM2_1_7.SetRadio()
    this.Parent.oContained.w_UMMODUM2=trim(this.Parent.oContained.w_UMMODUM2)
    this.value = ;
      iif(this.Parent.oContained.w_UMMODUM2=='S',1,;
      0)
  endfunc

  func oUMMODUM2_1_7.mHide()
    with this.Parent.oContained
      return (.w_UMFLFRAZ<>'S')
    endwith
  endfunc

  add object oUMFLTEMP_1_9 as StdCheck with uid="DAUTOEPGZM",rtseq=6,rtrep=.f.,left=84, top=97, caption="Gestita a tempo",;
    ToolTipText = "Se attivo, l'unit� di misura � convertibile in una quantit� di tempo  ",;
    HelpContextID = 101137302,;
    cFormVar="w_UMFLTEMP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oUMFLTEMP_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oUMFLTEMP_1_9.GetRadio()
    this.Parent.oContained.w_UMFLTEMP = this.RadioValue()
    return .t.
  endfunc

  func oUMFLTEMP_1_9.SetRadio()
    this.Parent.oContained.w_UMFLTEMP=trim(this.Parent.oContained.w_UMFLTEMP)
    this.value = ;
      iif(this.Parent.oContained.w_UMFLTEMP=='S',1,;
      0)
  endfunc

  func oUMFLTEMP_1_9.mHide()
    with this.Parent.oContained
      return (g_Agen = 'N' and g_prfa='N')
    endwith
  endfunc

  add object oUMDURORE_1_10 as StdField with uid="GGELKIIERJ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_UMDURORE", cQueryName = "UMDURORE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Rappresenta la quantit� di ore corrispondente ad una unit� dell'U.M. (per calcolo durata effettiva prestazioni sulle attivit�)",;
    HelpContextID = 1041525,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=331, Top=96, cSayPict='"999.99999"', cGetPict='"999.99999"'

  func oUMDURORE_1_10.mHide()
    with this.Parent.oContained
      return (g_Agen='N' OR .w_UMFLTEMP<>'S')
    endwith
  endfunc

  add object oUMDURSEC_1_13 as StdField with uid="RISOKJCBGZ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_UMDURSEC", cQueryName = "UMDURSEC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Fattore di conversione in secondi (utilizzato nel modulo produzione funzioni avanzate)",;
    HelpContextID = 202368119,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=310, Top=121, cSayPict='"99999999.999"', cGetPict='"99999999.999"'

  func oUMDURSEC_1_13.mHide()
    with this.Parent.oContained
      return (g_PRFA='N' OR .w_UMFLTEMP<>'S')
    endwith
  endfunc

  add object oStr_1_4 as StdString with uid="RMLNGKUVBC",Visible=.t., Left=41, Top=14,;
    Alignment=1, Width=42, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_5 as StdString with uid="LAHZHMJVDZ",Visible=.t., Left=0, Top=42,;
    Alignment=1, Width=82, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="SBBAKZPVML",Visible=.t., Left=230, Top=98,;
    Alignment=1, Width=101, Height=18,;
    Caption="Durata in ore:"  ;
  , bGlobalFont=.t.

  func oStr_1_11.mHide()
    with this.Parent.oContained
      return (g_Agen='N' OR .w_UMFLTEMP<>'S')
    endwith
  endfunc

  add object oStr_1_14 as StdString with uid="ZNJHVIQCRW",Visible=.t., Left=180, Top=124,;
    Alignment=1, Width=127, Height=18,;
    Caption="Fatt. conv. in secondi:"  ;
  , bGlobalFont=.t.

  func oStr_1_14.mHide()
    with this.Parent.oContained
      return (g_PRFA='N' OR .w_UMFLTEMP<>'S')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_aum','UNIMIS','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".UMCODICE=UNIMIS.UMCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
