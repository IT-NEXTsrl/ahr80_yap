* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bsa                                                        *
*              Controllo documenti collegati in ristampa                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_296]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-11-07                                                      *
* Last revis.: 2007-11-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pNomeChiam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bsa",oParentObject,m.pNomeChiam)
return(i_retval)

define class tgsve_bsa as StdBatch
  * --- Local variables
  pNomeChiam = space(1)
  w_HASEVCOP = space(50)
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_RESCHK = 0
  w_HASEVENT = .f.
  w_MVFLCONT = space(1)
  w_MVRIFACC = space(10)
  w_MVFLINTR = space(1)
  w_MVFLBLOC = space(1)
  w_MVFLVEAC = space(1)
  w_MVMOVCOM = space(10)
  w_MVSERDDT = space(10)
  w_MVCLADOC = space(2)
  w_MVSERIAL = space(10)
  w_MoviCaller = .NULL.
  w_MVFLGIOM = space(1)
  w_MVRIFDCO = space(10)
  w_MVGENPRO = space(1)
  w_MVNUMEST = 0
  w_MVNUMDOC = 0
  w_MVANNPRO = space(4)
  w_MVRIFODL = space(15)
  w_MVFLOFFE = space(1)
  w_MVPRD = space(2)
  w_FLGEFA = space(1)
  w_FLEDIT = space(1)
  w_MVFLPROV = space(1)
  w_MVSERRIF = space(10)
  w_MVROWRIF = 0
  w_MVNUMRIF = 0
  w_MVQTAIMP = 0
  w_MVQTAIM1 = 0
  w_MVFLARIF = space(1)
  w_MOLTIP = 0
  w_MVTIPRIG = space(1)
  w_UNMIS1 = space(3)
  w_MVUNIMIS = space(3)
  w_MVFLELGM = space(1)
  w_MVDATREG = ctod("  /  /  ")
  w_MVCODMAG = space(5)
  w_MVCODMAT = space(5)
  w_MVCODESE = space(4)
  w_FLBLEV = space(1)
  w_MVQTAEVA = 0
  w_MVIMPEVA = 0
  w_ASSCES = space(1)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Prima di ristampare un documento dobbiamo controllare se qualche elemento importato � gi� stato utilizzato.
    *     Non possiamo chiamare direttamente GSVE_BNR dato che utilizza variabili caller a loro volta caller in GSVE_BSD,
    *     quindi abbiamo definito in GSVE_BSD delle variabili di appoggio che rinominiamo opportunamente qui in modo che siano corrette in GSVE_BNR
    * --- pNomeChiam='S' (ossia facciamo una comune stampa) da GSVE_MDV
    *     pNomeChiam='R' tasto destro da GSAL_KRD
    this.w_HASEVCOP = "ECPEDIT"
    this.w_CPROWNUM = 0
    this.w_CPROWORD = 0
    this.w_RESCHK = 0
    this.w_HASEVENT = this.oParentObject.w_w_HASEVENT
    this.w_MVFLCONT = this.oParentObject.w_w_MVFLCONT
    this.w_MVRIFACC = this.oParentObject.w_w_MVRIFACC
    this.w_MVFLINTR = this.oParentObject.w_w_MVFLINTR
    this.w_MVFLBLOC = this.oParentObject.w_w_MVFLBLOC
    this.w_MVFLVEAC = this.oParentObject.w_w_MVFLVEAC
    this.w_MVMOVCOM = this.oParentObject.w_w_MVMOVCOM
    this.w_MVSERDDT = this.oParentObject.w_w_MVSERDDT
    this.w_MVCLADOC = this.oParentObject.w_w_MVCLADOC
    this.w_MVSERIAL = this.oParentObject.w_w_MVSERIAL
    if this.pNomeChiam="S"
      this.w_MoviCaller = this.oParentObject.w_PADRE
    endif
    * --- adfbadfbadfbadfbadfbadfbadfba
    this.w_MVFLGIOM = this.oParentObject.w_w_MVFLGIOM
    this.w_MVRIFDCO = this.oParentObject.w_w_MVRIFDCO
    this.w_MVGENPRO = this.oParentObject.w_w_MVGENPRO
    this.w_MVNUMEST = this.oParentObject.w_w_MVNUMEST
    this.w_MVNUMDOC = this.oParentObject.w_w_MVNUMDOC
    this.w_MVANNPRO = this.oParentObject.w_w_MVANNPRO
    this.w_MVRIFODL = this.oParentObject.w_w_MVRIFODL
    this.w_MVFLOFFE = this.oParentObject.w_w_MVFLOFFE
    this.w_MVPRD = this.oParentObject.w_w_MVPRD
    this.w_FLGEFA = this.oParentObject.w_w_FLGEFA
    this.w_FLEDIT = this.oParentObject.w_w_FLEDIT
    this.w_MVFLPROV = this.oParentObject.w_w_MVFLPROV
    this.w_MVSERRIF = this.oParentObject.w_w_MVSERRIF
    this.w_MVROWRIF = this.oParentObject.w_w_MVROWRIF
    this.w_MVNUMRIF = this.oParentObject.w_w_MVNUMRIF
    this.w_MVQTAIMP = this.oParentObject.w_w_MVQTAIMP
    this.w_MVQTAIM1 = this.oParentObject.w_w_MVQTAIM1
    this.w_MVFLARIF = this.oParentObject.w_w_MVFLARIF
    this.w_MOLTIP = this.oParentObject.w_w_MOLTIP
    this.w_MVTIPRIG = this.oParentObject.w_w_MVTIPRIG
    this.w_UNMIS1 = this.oParentObject.w_w_UNMIS1
    this.w_MVUNIMIS = this.oParentObject.w_w_MVUNIMIS
    this.w_MVFLELGM = this.oParentObject.w_w_MVFLELGM
    this.w_MVDATREG = this.oParentObject.w_w_MVDATREG
    this.w_MVCODMAG = this.oParentObject.w_w_MVCODMAG
    this.w_MVCODMAT = this.oParentObject.w_w_MVCODMAT
    this.w_MVCODESE = this.oParentObject.w_w_MVCODESE
    this.w_FLBLEV = this.oParentObject.w_w_FLBLEV
    this.w_MVQTAEVA = this.oParentObject.w_w_MVQTAEVA
    this.w_MVIMPEVA = this.oParentObject.w_w_MVIMPEVA
    this.w_ASSCES = this.oParentObject.w_w_ASSCES
    GSVE_BNR(this, this.pNomeChiam )
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  proc Init(oParentObject,pNomeChiam)
    this.pNomeChiam=pNomeChiam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pNomeChiam"
endproc
