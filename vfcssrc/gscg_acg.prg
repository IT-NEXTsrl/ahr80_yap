* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_acg                                                        *
*              Modello F24 contribuente - 2007                                 *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-06-14                                                      *
* Last revis.: 2018-03-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgscg_acg")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgscg_acg")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgscg_acg")
  return

* --- Class definition
define class tgscg_acg as StdPCForm
  Width  = 853
  Height = 358
  Top    = 8
  Left   = 44
  cComment = "Modello F24 contribuente - 2007"
  cPrg = "gscg_acg"
  HelpContextID=109716841
  add object cnt as tcgscg_acg
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgscg_acg as PCContext
  w_CFSERIAL = space(10)
  w_COD_SOC = space(5)
  w_COD_PRE = space(5)
  w_NOME_PER = space(40)
  w_INDIRSOC = space(35)
  w_SEDE = space(2)
  w_COD_FIS = space(5)
  w_NOME_SOC = space(40)
  w_INDIRPER = space(30)
  w_CFDOMFIS2 = space(10)
  w_CFPRDFIS2 = space(10)
  w_COMU_SOC = space(30)
  w_CFINDFIS2 = space(10)
  w_CFCAPFIS2 = space(10)
  w_PROV_SOC = space(2)
  w_PROV_PER = space(2)
  w_COMU_PER = space(30)
  w_UNO = 0
  w_PERAZ = space(1)
  w_FISINDIR = space(35)
  w_FISPROV = space(2)
  w_FISLOCAL = space(30)
  w_FISCODES = space(5)
  w_CFCAPCON2 = space(10)
  w_SETIPRIF = space(10)
  w_CFTELCON2 = space(10)
  w_SEDI = space(5)
  w_CFDELEGT = space(50)
  w_CFDELEGB = space(50)
  w_CFCODAGE = space(40)
  w_CFAGPROV = space(2)
  w_CFCODFIS = space(16)
  w_CFCODFIS = space(16)
  w_CFPERFIS = space(1)
  w_CFRAGSOC = space(80)
  w_CF__NOME = space(25)
  w_CF_SESSO = space(1)
  w_CFDATNAS = space(8)
  w_CFLOCNAS = space(30)
  w_CFPRONAS = space(2)
  w_CF__NOME = space(25)
  w_CFPERFIS = space(1)
  w_CFRAGSOC = space(80)
  w_CF_SESSO = space(1)
  w_CFDATNAS = space(8)
  w_CFLOCNAS = space(30)
  w_CFPRONAS = space(2)
  w_RESCHK = 0
  w_DATAPP = space(8)
  w_CFINDIRI2 = space(30)
  w_CFLOCALI2 = space(30)
  w_CFPROVIN2 = space(2)
  w_TIPSL = space(2)
  w_SEDILEG = space(5)
  w_CFINDFIS = space(35)
  w_CFDOMFIS = space(40)
  w_CFPRDFIS = space(2)
  w_CFCAPFIS = space(5)
  w_CFINDIRI = space(30)
  w_CFLOCALI = space(30)
  w_CFPROVIN = space(2)
  w_CFCAPCON = space(5)
  w_CFTELCON = space(12)
  w_CFMAIVER = space(60)
  w_CFCODIDE = space(2)
  w_CFSECCOD = space(16)
  w_FISCAP = space(10)
  w_CAP_PER = space(10)
  w_CAP_SOC = space(10)
  w_SETEL = space(10)
  w_TEL_PER = space(10)
  w_TEL_SOC = space(10)
  w_CFCODFIR = space(16)
  w_CFCOGFIR = space(60)
  w_CFNOMFIR = space(20)
  w_CFSESFIR = space(1)
  w_CFDATFIR = space(8)
  w_CFCOMFIR = space(40)
  w_CFPROFIR = space(2)
  w_CFDOMFIR = space(40)
  w_CFPRDFIR = space(2)
  w_CFCAPFIR = space(5)
  w_CFINDFIR = space(35)
  w_EMAILSEDE = space(60)
  w_EMAILAZI = space(60)
  proc Save(oFrom)
    this.w_CFSERIAL = oFrom.w_CFSERIAL
    this.w_COD_SOC = oFrom.w_COD_SOC
    this.w_COD_PRE = oFrom.w_COD_PRE
    this.w_NOME_PER = oFrom.w_NOME_PER
    this.w_INDIRSOC = oFrom.w_INDIRSOC
    this.w_SEDE = oFrom.w_SEDE
    this.w_COD_FIS = oFrom.w_COD_FIS
    this.w_NOME_SOC = oFrom.w_NOME_SOC
    this.w_INDIRPER = oFrom.w_INDIRPER
    this.w_CFDOMFIS2 = oFrom.w_CFDOMFIS2
    this.w_CFPRDFIS2 = oFrom.w_CFPRDFIS2
    this.w_COMU_SOC = oFrom.w_COMU_SOC
    this.w_CFINDFIS2 = oFrom.w_CFINDFIS2
    this.w_CFCAPFIS2 = oFrom.w_CFCAPFIS2
    this.w_PROV_SOC = oFrom.w_PROV_SOC
    this.w_PROV_PER = oFrom.w_PROV_PER
    this.w_COMU_PER = oFrom.w_COMU_PER
    this.w_UNO = oFrom.w_UNO
    this.w_PERAZ = oFrom.w_PERAZ
    this.w_FISINDIR = oFrom.w_FISINDIR
    this.w_FISPROV = oFrom.w_FISPROV
    this.w_FISLOCAL = oFrom.w_FISLOCAL
    this.w_FISCODES = oFrom.w_FISCODES
    this.w_CFCAPCON2 = oFrom.w_CFCAPCON2
    this.w_SETIPRIF = oFrom.w_SETIPRIF
    this.w_CFTELCON2 = oFrom.w_CFTELCON2
    this.w_SEDI = oFrom.w_SEDI
    this.w_CFDELEGT = oFrom.w_CFDELEGT
    this.w_CFDELEGB = oFrom.w_CFDELEGB
    this.w_CFCODAGE = oFrom.w_CFCODAGE
    this.w_CFAGPROV = oFrom.w_CFAGPROV
    this.w_CFCODFIS = oFrom.w_CFCODFIS
    this.w_CFCODFIS = oFrom.w_CFCODFIS
    this.w_CFPERFIS = oFrom.w_CFPERFIS
    this.w_CFRAGSOC = oFrom.w_CFRAGSOC
    this.w_CF__NOME = oFrom.w_CF__NOME
    this.w_CF_SESSO = oFrom.w_CF_SESSO
    this.w_CFDATNAS = oFrom.w_CFDATNAS
    this.w_CFLOCNAS = oFrom.w_CFLOCNAS
    this.w_CFPRONAS = oFrom.w_CFPRONAS
    this.w_CF__NOME = oFrom.w_CF__NOME
    this.w_CFPERFIS = oFrom.w_CFPERFIS
    this.w_CFRAGSOC = oFrom.w_CFRAGSOC
    this.w_CF_SESSO = oFrom.w_CF_SESSO
    this.w_CFDATNAS = oFrom.w_CFDATNAS
    this.w_CFLOCNAS = oFrom.w_CFLOCNAS
    this.w_CFPRONAS = oFrom.w_CFPRONAS
    this.w_RESCHK = oFrom.w_RESCHK
    this.w_DATAPP = oFrom.w_DATAPP
    this.w_CFINDIRI2 = oFrom.w_CFINDIRI2
    this.w_CFLOCALI2 = oFrom.w_CFLOCALI2
    this.w_CFPROVIN2 = oFrom.w_CFPROVIN2
    this.w_TIPSL = oFrom.w_TIPSL
    this.w_SEDILEG = oFrom.w_SEDILEG
    this.w_CFINDFIS = oFrom.w_CFINDFIS
    this.w_CFDOMFIS = oFrom.w_CFDOMFIS
    this.w_CFPRDFIS = oFrom.w_CFPRDFIS
    this.w_CFCAPFIS = oFrom.w_CFCAPFIS
    this.w_CFINDIRI = oFrom.w_CFINDIRI
    this.w_CFLOCALI = oFrom.w_CFLOCALI
    this.w_CFPROVIN = oFrom.w_CFPROVIN
    this.w_CFCAPCON = oFrom.w_CFCAPCON
    this.w_CFTELCON = oFrom.w_CFTELCON
    this.w_CFMAIVER = oFrom.w_CFMAIVER
    this.w_CFCODIDE = oFrom.w_CFCODIDE
    this.w_CFSECCOD = oFrom.w_CFSECCOD
    this.w_FISCAP = oFrom.w_FISCAP
    this.w_CAP_PER = oFrom.w_CAP_PER
    this.w_CAP_SOC = oFrom.w_CAP_SOC
    this.w_SETEL = oFrom.w_SETEL
    this.w_TEL_PER = oFrom.w_TEL_PER
    this.w_TEL_SOC = oFrom.w_TEL_SOC
    this.w_CFCODFIR = oFrom.w_CFCODFIR
    this.w_CFCOGFIR = oFrom.w_CFCOGFIR
    this.w_CFNOMFIR = oFrom.w_CFNOMFIR
    this.w_CFSESFIR = oFrom.w_CFSESFIR
    this.w_CFDATFIR = oFrom.w_CFDATFIR
    this.w_CFCOMFIR = oFrom.w_CFCOMFIR
    this.w_CFPROFIR = oFrom.w_CFPROFIR
    this.w_CFDOMFIR = oFrom.w_CFDOMFIR
    this.w_CFPRDFIR = oFrom.w_CFPRDFIR
    this.w_CFCAPFIR = oFrom.w_CFCAPFIR
    this.w_CFINDFIR = oFrom.w_CFINDFIR
    this.w_EMAILSEDE = oFrom.w_EMAILSEDE
    this.w_EMAILAZI = oFrom.w_EMAILAZI
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_CFSERIAL = this.w_CFSERIAL
    oTo.w_COD_SOC = this.w_COD_SOC
    oTo.w_COD_PRE = this.w_COD_PRE
    oTo.w_NOME_PER = this.w_NOME_PER
    oTo.w_INDIRSOC = this.w_INDIRSOC
    oTo.w_SEDE = this.w_SEDE
    oTo.w_COD_FIS = this.w_COD_FIS
    oTo.w_NOME_SOC = this.w_NOME_SOC
    oTo.w_INDIRPER = this.w_INDIRPER
    oTo.w_CFDOMFIS2 = this.w_CFDOMFIS2
    oTo.w_CFPRDFIS2 = this.w_CFPRDFIS2
    oTo.w_COMU_SOC = this.w_COMU_SOC
    oTo.w_CFINDFIS2 = this.w_CFINDFIS2
    oTo.w_CFCAPFIS2 = this.w_CFCAPFIS2
    oTo.w_PROV_SOC = this.w_PROV_SOC
    oTo.w_PROV_PER = this.w_PROV_PER
    oTo.w_COMU_PER = this.w_COMU_PER
    oTo.w_UNO = this.w_UNO
    oTo.w_PERAZ = this.w_PERAZ
    oTo.w_FISINDIR = this.w_FISINDIR
    oTo.w_FISPROV = this.w_FISPROV
    oTo.w_FISLOCAL = this.w_FISLOCAL
    oTo.w_FISCODES = this.w_FISCODES
    oTo.w_CFCAPCON2 = this.w_CFCAPCON2
    oTo.w_SETIPRIF = this.w_SETIPRIF
    oTo.w_CFTELCON2 = this.w_CFTELCON2
    oTo.w_SEDI = this.w_SEDI
    oTo.w_CFDELEGT = this.w_CFDELEGT
    oTo.w_CFDELEGB = this.w_CFDELEGB
    oTo.w_CFCODAGE = this.w_CFCODAGE
    oTo.w_CFAGPROV = this.w_CFAGPROV
    oTo.w_CFCODFIS = this.w_CFCODFIS
    oTo.w_CFCODFIS = this.w_CFCODFIS
    oTo.w_CFPERFIS = this.w_CFPERFIS
    oTo.w_CFRAGSOC = this.w_CFRAGSOC
    oTo.w_CF__NOME = this.w_CF__NOME
    oTo.w_CF_SESSO = this.w_CF_SESSO
    oTo.w_CFDATNAS = this.w_CFDATNAS
    oTo.w_CFLOCNAS = this.w_CFLOCNAS
    oTo.w_CFPRONAS = this.w_CFPRONAS
    oTo.w_CF__NOME = this.w_CF__NOME
    oTo.w_CFPERFIS = this.w_CFPERFIS
    oTo.w_CFRAGSOC = this.w_CFRAGSOC
    oTo.w_CF_SESSO = this.w_CF_SESSO
    oTo.w_CFDATNAS = this.w_CFDATNAS
    oTo.w_CFLOCNAS = this.w_CFLOCNAS
    oTo.w_CFPRONAS = this.w_CFPRONAS
    oTo.w_RESCHK = this.w_RESCHK
    oTo.w_DATAPP = this.w_DATAPP
    oTo.w_CFINDIRI2 = this.w_CFINDIRI2
    oTo.w_CFLOCALI2 = this.w_CFLOCALI2
    oTo.w_CFPROVIN2 = this.w_CFPROVIN2
    oTo.w_TIPSL = this.w_TIPSL
    oTo.w_SEDILEG = this.w_SEDILEG
    oTo.w_CFINDFIS = this.w_CFINDFIS
    oTo.w_CFDOMFIS = this.w_CFDOMFIS
    oTo.w_CFPRDFIS = this.w_CFPRDFIS
    oTo.w_CFCAPFIS = this.w_CFCAPFIS
    oTo.w_CFINDIRI = this.w_CFINDIRI
    oTo.w_CFLOCALI = this.w_CFLOCALI
    oTo.w_CFPROVIN = this.w_CFPROVIN
    oTo.w_CFCAPCON = this.w_CFCAPCON
    oTo.w_CFTELCON = this.w_CFTELCON
    oTo.w_CFMAIVER = this.w_CFMAIVER
    oTo.w_CFCODIDE = this.w_CFCODIDE
    oTo.w_CFSECCOD = this.w_CFSECCOD
    oTo.w_FISCAP = this.w_FISCAP
    oTo.w_CAP_PER = this.w_CAP_PER
    oTo.w_CAP_SOC = this.w_CAP_SOC
    oTo.w_SETEL = this.w_SETEL
    oTo.w_TEL_PER = this.w_TEL_PER
    oTo.w_TEL_SOC = this.w_TEL_SOC
    oTo.w_CFCODFIR = this.w_CFCODFIR
    oTo.w_CFCOGFIR = this.w_CFCOGFIR
    oTo.w_CFNOMFIR = this.w_CFNOMFIR
    oTo.w_CFSESFIR = this.w_CFSESFIR
    oTo.w_CFDATFIR = this.w_CFDATFIR
    oTo.w_CFCOMFIR = this.w_CFCOMFIR
    oTo.w_CFPROFIR = this.w_CFPROFIR
    oTo.w_CFDOMFIR = this.w_CFDOMFIR
    oTo.w_CFPRDFIR = this.w_CFPRDFIR
    oTo.w_CFCAPFIR = this.w_CFCAPFIR
    oTo.w_CFINDFIR = this.w_CFINDFIR
    oTo.w_EMAILSEDE = this.w_EMAILSEDE
    oTo.w_EMAILAZI = this.w_EMAILAZI
    PCContext::Load(oTo)
enddefine

define class tcgscg_acg as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 853
  Height = 358
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-03-12"
  HelpContextID=109716841
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=85

  * --- Constant Properties
  MODCPAG_IDX = 0
  TITOLARI_IDX = 0
  AZIENDA_IDX = 0
  cpazi_IDX = 0
  SEDIAZIE_IDX = 0
  anag_pro_IDX = 0
  ANAG_PRO_IDX = 0
  cFile = "MODCPAG"
  cKeySelect = "CFSERIAL"
  cKeyWhere  = "CFSERIAL=this.w_CFSERIAL"
  cKeyWhereODBC = '"CFSERIAL="+cp_ToStrODBC(this.w_CFSERIAL)';

  cKeyWhereODBCqualified = '"MODCPAG.CFSERIAL="+cp_ToStrODBC(this.w_CFSERIAL)';

  cPrg = "gscg_acg"
  cComment = "Modello F24 contribuente - 2007"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CFSERIAL = space(10)
  w_COD_SOC = space(5)
  o_COD_SOC = space(5)
  w_COD_PRE = space(5)
  o_COD_PRE = space(5)
  w_NOME_PER = space(40)
  o_NOME_PER = space(40)
  w_INDIRSOC = space(35)
  o_INDIRSOC = space(35)
  w_SEDE = space(2)
  w_COD_FIS = space(5)
  o_COD_FIS = space(5)
  w_NOME_SOC = space(40)
  o_NOME_SOC = space(40)
  w_INDIRPER = space(30)
  o_INDIRPER = space(30)
  w_CFDOMFIS2 = space(10)
  w_CFPRDFIS2 = space(10)
  w_COMU_SOC = space(30)
  o_COMU_SOC = space(30)
  w_CFINDFIS2 = space(10)
  w_CFCAPFIS2 = space(10)
  w_PROV_SOC = space(2)
  o_PROV_SOC = space(2)
  w_PROV_PER = space(2)
  o_PROV_PER = space(2)
  w_COMU_PER = space(30)
  o_COMU_PER = space(30)
  w_UNO = 0
  w_PERAZ = space(1)
  o_PERAZ = space(1)
  w_FISINDIR = space(35)
  w_FISPROV = space(2)
  w_FISLOCAL = space(30)
  w_FISCODES = space(5)
  w_CFCAPCON2 = space(10)
  w_SETIPRIF = space(10)
  w_CFTELCON2 = space(10)
  w_SEDI = space(5)
  o_SEDI = space(5)
  w_CFDELEGT = space(50)
  w_CFDELEGB = space(50)
  w_CFCODAGE = space(40)
  o_CFCODAGE = space(40)
  w_CFAGPROV = space(2)
  w_CFCODFIS = space(16)
  w_CFCODFIS = space(16)
  w_CFPERFIS = space(1)
  o_CFPERFIS = space(1)
  w_CFRAGSOC = space(80)
  w_CF__NOME = space(25)
  w_CF_SESSO = space(1)
  w_CFDATNAS = ctod('  /  /  ')
  w_CFLOCNAS = space(30)
  w_CFPRONAS = space(2)
  w_CF__NOME = space(25)
  w_CFPERFIS = space(1)
  w_CFRAGSOC = space(80)
  w_CF_SESSO = space(1)
  w_CFDATNAS = ctod('  /  /  ')
  w_CFLOCNAS = space(30)
  w_CFPRONAS = space(2)
  w_RESCHK = 0
  w_DATAPP = ctod('  /  /  ')
  o_DATAPP = ctod('  /  /  ')
  w_CFINDIRI2 = space(30)
  w_CFLOCALI2 = space(30)
  w_CFPROVIN2 = space(2)
  w_TIPSL = space(2)
  w_SEDILEG = space(5)
  o_SEDILEG = space(5)
  w_CFINDFIS = space(35)
  o_CFINDFIS = space(35)
  w_CFDOMFIS = space(40)
  w_CFPRDFIS = space(2)
  w_CFCAPFIS = space(5)
  w_CFINDIRI = space(30)
  w_CFLOCALI = space(30)
  w_CFPROVIN = space(2)
  w_CFCAPCON = space(5)
  w_CFTELCON = space(12)
  w_CFMAIVER = space(60)
  w_CFCODIDE = space(2)
  o_CFCODIDE = space(2)
  w_CFSECCOD = space(16)
  w_FISCAP = space(10)
  w_CAP_PER = space(10)
  w_CAP_SOC = space(10)
  w_SETEL = space(10)
  w_TEL_PER = space(10)
  w_TEL_SOC = space(10)
  w_CFCODFIR = space(16)
  w_CFCOGFIR = space(60)
  w_CFNOMFIR = space(20)
  w_CFSESFIR = space(1)
  w_CFDATFIR = ctod('  /  /  ')
  w_CFCOMFIR = space(40)
  w_CFPROFIR = space(2)
  w_CFDOMFIR = space(40)
  w_CFPRDFIR = space(2)
  w_CFCAPFIR = space(5)
  w_CFINDFIR = space(35)
  w_EMAILSEDE = space(60)
  w_EMAILAZI = space(60)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_acgPag1","gscg_acg",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).HelpContextID = 51269386
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCFDELEGT_1_28
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='TITOLARI'
    this.cWorkTables[2]='AZIENDA'
    this.cWorkTables[3]='cpazi'
    this.cWorkTables[4]='SEDIAZIE'
    this.cWorkTables[5]='anag_pro'
    this.cWorkTables[6]='ANAG_PRO'
    this.cWorkTables[7]='MODCPAG'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(7))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MODCPAG_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MODCPAG_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgscg_acg'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from MODCPAG where CFSERIAL=KeySet.CFSERIAL
    *
    i_nConn = i_TableProp[this.MODCPAG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MODCPAG_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MODCPAG')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MODCPAG.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' MODCPAG '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CFSERIAL',this.w_CFSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_COD_SOC = i_CODAZI
        .w_NOME_PER = space(40)
        .w_INDIRSOC = space(35)
        .w_SEDE = 'DF'
        .w_NOME_SOC = space(40)
        .w_INDIRPER = space(30)
        .w_CFDOMFIS2 = space(10)
        .w_CFPRDFIS2 = space(10)
        .w_COMU_SOC = space(30)
        .w_CFINDFIS2 = space(10)
        .w_CFCAPFIS2 = space(10)
        .w_PROV_SOC = space(2)
        .w_PROV_PER = space(2)
        .w_COMU_PER = space(30)
        .w_UNO = 1
        .w_PERAZ = space(1)
        .w_FISINDIR = space(35)
        .w_FISPROV = space(2)
        .w_FISLOCAL = space(30)
        .w_FISCODES = space(5)
        .w_CFCAPCON2 = space(10)
        .w_SETIPRIF = 'DF'
        .w_CFTELCON2 = space(10)
        .w_SEDI = i_CODAZI
        .w_RESCHK = 0
        .w_DATAPP = ctod("  /  /  ")
        .w_CFINDIRI2 = space(30)
        .w_CFLOCALI2 = space(30)
        .w_CFPROVIN2 = space(2)
        .w_TIPSL = 'SL'
        .w_FISCAP = space(10)
        .w_CAP_PER = space(10)
        .w_CAP_SOC = space(10)
        .w_SETEL = space(10)
        .w_TEL_PER = space(10)
        .w_TEL_SOC = space(10)
        .w_EMAILSEDE = space(60)
        .w_EMAILAZI = space(60)
        .w_CFSERIAL = NVL(CFSERIAL,space(10))
          .link_1_2('Load')
        .w_COD_PRE = i_CODAZI
          .link_1_3('Load')
        .w_COD_FIS = i_CODAZI
          .link_1_7('Load')
          .link_1_27('Load')
        .w_CFDELEGT = NVL(CFDELEGT,space(50))
        .w_CFDELEGB = NVL(CFDELEGB,space(50))
        .w_CFCODAGE = NVL(CFCODAGE,space(40))
        .w_CFAGPROV = NVL(CFAGPROV,space(2))
        .w_CFCODFIS = NVL(CFCODFIS,space(16))
        .w_CFCODFIS = NVL(CFCODFIS,space(16))
        .w_CFPERFIS = NVL(CFPERFIS,space(1))
        .w_CFRAGSOC = NVL(CFRAGSOC,space(80))
        .w_CF__NOME = NVL(CF__NOME,space(25))
        .w_CF_SESSO = NVL(CF_SESSO,space(1))
        .w_CFDATNAS = NVL(cp_ToDate(CFDATNAS),ctod("  /  /  "))
        .w_CFLOCNAS = NVL(CFLOCNAS,space(30))
        .w_CFPRONAS = NVL(CFPRONAS,space(2))
        .w_CF__NOME = NVL(CF__NOME,space(25))
        .w_CFPERFIS = NVL(CFPERFIS,space(1))
        .w_CFRAGSOC = NVL(CFRAGSOC,space(80))
        .w_CF_SESSO = NVL(CF_SESSO,space(1))
        .w_CFDATNAS = NVL(cp_ToDate(CFDATNAS),ctod("  /  /  "))
        .w_CFLOCNAS = NVL(CFLOCNAS,space(30))
        .w_CFPRONAS = NVL(CFPRONAS,space(2))
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate()
        .w_SEDILEG = iif(.w_CFPERFIS<>'S',i_CODAZI,' ')
          .link_1_90('Load')
        .w_CFINDFIS = NVL(CFINDFIS,space(35))
        .w_CFDOMFIS = NVL(CFDOMFIS,space(40))
        .w_CFPRDFIS = NVL(CFPRDFIS,space(2))
        .w_CFCAPFIS = NVL(CFCAPFIS,space(5))
        .w_CFINDIRI = NVL(CFINDIRI,space(30))
        .w_CFLOCALI = NVL(CFLOCALI,space(30))
        .w_CFPROVIN = NVL(CFPROVIN,space(2))
        .w_CFCAPCON = NVL(CFCAPCON,space(5))
        .w_CFTELCON = NVL(CFTELCON,space(12))
        .w_CFMAIVER = NVL(CFMAIVER,space(60))
        .w_CFCODIDE = NVL(CFCODIDE,space(2))
        .w_CFSECCOD = NVL(CFSECCOD,space(16))
        .w_CFCODFIR = NVL(CFCODFIR,space(16))
        .w_CFCOGFIR = NVL(CFCOGFIR,space(60))
        .w_CFNOMFIR = NVL(CFNOMFIR,space(20))
        .w_CFSESFIR = NVL(CFSESFIR,space(1))
        .w_CFDATFIR = NVL(cp_ToDate(CFDATFIR),ctod("  /  /  "))
        .w_CFCOMFIR = NVL(CFCOMFIR,space(40))
        .w_CFPROFIR = NVL(CFPROFIR,space(2))
        .w_CFDOMFIR = NVL(CFDOMFIR,space(40))
        .w_CFPRDFIR = NVL(CFPRDFIR,space(2))
        .w_CFCAPFIR = NVL(CFCAPFIR,space(5))
        .w_CFINDFIR = NVL(CFINDFIR,space(35))
        cp_LoadRecExtFlds(this,'MODCPAG')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_125.enabled = this.oPgFrm.Page1.oPag.oBtn_1_125.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_CFSERIAL = space(10)
      .w_COD_SOC = space(5)
      .w_COD_PRE = space(5)
      .w_NOME_PER = space(40)
      .w_INDIRSOC = space(35)
      .w_SEDE = space(2)
      .w_COD_FIS = space(5)
      .w_NOME_SOC = space(40)
      .w_INDIRPER = space(30)
      .w_CFDOMFIS2 = space(10)
      .w_CFPRDFIS2 = space(10)
      .w_COMU_SOC = space(30)
      .w_CFINDFIS2 = space(10)
      .w_CFCAPFIS2 = space(10)
      .w_PROV_SOC = space(2)
      .w_PROV_PER = space(2)
      .w_COMU_PER = space(30)
      .w_UNO = 0
      .w_PERAZ = space(1)
      .w_FISINDIR = space(35)
      .w_FISPROV = space(2)
      .w_FISLOCAL = space(30)
      .w_FISCODES = space(5)
      .w_CFCAPCON2 = space(10)
      .w_SETIPRIF = space(10)
      .w_CFTELCON2 = space(10)
      .w_SEDI = space(5)
      .w_CFDELEGT = space(50)
      .w_CFDELEGB = space(50)
      .w_CFCODAGE = space(40)
      .w_CFAGPROV = space(2)
      .w_CFCODFIS = space(16)
      .w_CFCODFIS = space(16)
      .w_CFPERFIS = space(1)
      .w_CFRAGSOC = space(80)
      .w_CF__NOME = space(25)
      .w_CF_SESSO = space(1)
      .w_CFDATNAS = ctod("  /  /  ")
      .w_CFLOCNAS = space(30)
      .w_CFPRONAS = space(2)
      .w_CF__NOME = space(25)
      .w_CFPERFIS = space(1)
      .w_CFRAGSOC = space(80)
      .w_CF_SESSO = space(1)
      .w_CFDATNAS = ctod("  /  /  ")
      .w_CFLOCNAS = space(30)
      .w_CFPRONAS = space(2)
      .w_RESCHK = 0
      .w_DATAPP = ctod("  /  /  ")
      .w_CFINDIRI2 = space(30)
      .w_CFLOCALI2 = space(30)
      .w_CFPROVIN2 = space(2)
      .w_TIPSL = space(2)
      .w_SEDILEG = space(5)
      .w_CFINDFIS = space(35)
      .w_CFDOMFIS = space(40)
      .w_CFPRDFIS = space(2)
      .w_CFCAPFIS = space(5)
      .w_CFINDIRI = space(30)
      .w_CFLOCALI = space(30)
      .w_CFPROVIN = space(2)
      .w_CFCAPCON = space(5)
      .w_CFTELCON = space(12)
      .w_CFMAIVER = space(60)
      .w_CFCODIDE = space(2)
      .w_CFSECCOD = space(16)
      .w_FISCAP = space(10)
      .w_CAP_PER = space(10)
      .w_CAP_SOC = space(10)
      .w_SETEL = space(10)
      .w_TEL_PER = space(10)
      .w_TEL_SOC = space(10)
      .w_CFCODFIR = space(16)
      .w_CFCOGFIR = space(60)
      .w_CFNOMFIR = space(20)
      .w_CFSESFIR = space(1)
      .w_CFDATFIR = ctod("  /  /  ")
      .w_CFCOMFIR = space(40)
      .w_CFPROFIR = space(2)
      .w_CFDOMFIR = space(40)
      .w_CFPRDFIR = space(2)
      .w_CFCAPFIR = space(5)
      .w_CFINDFIR = space(35)
      .w_EMAILSEDE = space(60)
      .w_EMAILAZI = space(60)
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_COD_SOC = i_CODAZI
        .DoRTCalc(2,2,.f.)
          if not(empty(.w_COD_SOC))
          .link_1_2('Full')
          endif
        .w_COD_PRE = i_CODAZI
        .DoRTCalc(3,3,.f.)
          if not(empty(.w_COD_PRE))
          .link_1_3('Full')
          endif
          .DoRTCalc(4,5,.f.)
        .w_SEDE = 'DF'
        .w_COD_FIS = i_CODAZI
        .DoRTCalc(7,7,.f.)
          if not(empty(.w_COD_FIS))
          .link_1_7('Full')
          endif
          .DoRTCalc(8,17,.f.)
        .w_UNO = 1
          .DoRTCalc(19,24,.f.)
        .w_SETIPRIF = 'DF'
          .DoRTCalc(26,26,.f.)
        .w_SEDI = i_CODAZI
        .DoRTCalc(27,27,.f.)
          if not(empty(.w_SEDI))
          .link_1_27('Full')
          endif
          .DoRTCalc(28,30,.f.)
        .w_CFAGPROV = iif(empty(.w_CFCODAGE),'  ',.w_CFAGPROV)
          .DoRTCalc(32,33,.f.)
        .w_CFPERFIS = iif(.w_PERAZ='S','S','N')
        .w_CFRAGSOC = iif(.w_PERAZ='S',upper(.w_NOME_PER),upper(.w_NOME_SOC))
        .w_CF__NOME = iif(.w_CFPERFIS='S',upper(.w_CF__NOME),'')
          .DoRTCalc(37,37,.f.)
        .w_CFDATNAS = iif(.w_CFPERFIS='S',.w_CFDATNAS,.w_DATAPP)
        .w_CFLOCNAS = iif(.w_CFPERFIS='S',upper(.w_CFLOCNAS),'')
        .w_CFPRONAS = iif(.w_CFPERFIS='S',upper(.w_CFPRONAS),'  ')
        .w_CF__NOME = iif(.w_CFPERFIS='S',upper(.w_CF__NOME),'')
        .w_CFPERFIS = iif(.w_PERAZ='S','S','N')
        .w_CFRAGSOC = iif(.w_PERAZ='S',upper(.w_NOME_PER),upper(.w_NOME_SOC))
          .DoRTCalc(44,44,.f.)
        .w_CFDATNAS = iif(.w_CFPERFIS='S',.w_CFDATNAS,.w_DATAPP)
        .w_CFLOCNAS = iif(.w_CFPERFIS='S',upper(.w_CFLOCNAS),'')
        .w_CFPRONAS = iif(.w_CFPERFIS='S',upper(.w_CFPRONAS),'  ')
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate()
          .DoRTCalc(48,52,.f.)
        .w_TIPSL = 'SL'
        .w_SEDILEG = iif(.w_CFPERFIS<>'S',i_CODAZI,' ')
        .DoRTCalc(54,54,.f.)
          if not(empty(.w_SEDILEG))
          .link_1_90('Full')
          endif
        .w_CFINDFIS = IIF (.w_CFPERFIS='N' , IIF (NOT EMPTY (.w_CFINDFIS2),.w_CFINDFIS2,.w_INDIRSOC)  , '')
        .w_CFDOMFIS = IIF (.w_CFPERFIS='N' ,IIF (NOT EMPTY (.w_CFDOMFIS2),.w_CFDOMFIS2,.w_COMU_SOC),'')
        .w_CFPRDFIS = IIF (.w_CFPERFIS='N' , IIF (NOT EMPTY (.w_CFPRDFIS2), left (.w_CFPRDFIS2,2), left (.w_PROV_SOC,2))   ,'')
        .w_CFCAPFIS = IIF (.w_CFPERFIS='N',IIF (NOT EMPTY (.w_CFCAPFIS2), left (.w_CFCAPFIS2,5), left (.w_CAP_SOC,5))  ,'')
        .w_CFINDIRI = iif(not empty(.w_FISCODES),left(upper(.w_FISINDIR),30),iif(.w_PERAZ='S',iif(empty(upper(.w_INDIRPER)),upper(left(.w_INDIRSOC,30)),left(upper(.w_INDIRPER),30)),upper(left(.w_INDIRSOC,30))))
        .w_CFLOCALI = iif(not empty(.w_FISCODES),left(upper(.w_FISLOCAL),30),iif(.w_PERAZ='S',iif(empty(upper(.w_COMU_PER)),left(upper(.w_COMU_SOC),30),left(upper(.w_COMU_PER),30)),left(upper(.w_COMU_SOC),30)))
        .w_CFPROVIN = iif(not empty(.w_FISCODES),upper(.w_FISPROV),iif(.w_PERAZ='S',iif(empty(upper(.w_PROV_PER)),upper(.w_PROV_SOC),upper(.w_PROV_PER)),upper(.w_PROV_SOC)))
        .w_CFCAPCON = iif(not empty(.w_FISCODES),      left (alltrim (.w_FISCAP),5) ,iif(.w_PERAZ='S',iif(empty(upper ( left (.w_CAP_PER,5))),upper( left (.w_CAP_SOC,5)),upper(left (.w_CAP_PER,5))),upper(left (.w_CAP_SOC,5))))
        .w_CFTELCON = iif(not empty(.w_FISCODES),      left (alltrim (.w_SETEL),12),iif(.w_PERAZ='S',iif(empty( upper(.w_TEL_PER)), left(upper(.w_TEL_SOC),12), left (upper(.w_TEL_PER),12)),left (upper(.w_TEL_SOC),12)))
        .w_CFMAIVER = iif(not empty(.w_FISCODES),left(upper(.w_EMAILSEDE),30),.w_EMAILAZI)
      endif
    endwith
    cp_BlankRecExtFlds(this,'MODCPAG')
    this.DoRTCalc(65,85,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_125.enabled = this.oPgFrm.Page1.oPag.oBtn_1_125.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gscg_acg
    * Forza aggiornamento del database
    this.bupdated=.t.
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oCFDELEGT_1_28.enabled = i_bVal
      .Page1.oPag.oCFDELEGB_1_29.enabled = i_bVal
      .Page1.oPag.oCFCODAGE_1_30.enabled = i_bVal
      .Page1.oPag.oCFAGPROV_1_31.enabled = i_bVal
      .Page1.oPag.oCFCODFIS_1_32.enabled = i_bVal
      .Page1.oPag.oCFCODFIS_1_33.enabled = i_bVal
      .Page1.oPag.oCFPERFIS_1_34.enabled_(i_bVal)
      .Page1.oPag.oCFRAGSOC_1_35.enabled = i_bVal
      .Page1.oPag.oCF__NOME_1_36.enabled = i_bVal
      .Page1.oPag.oCF_SESSO_1_37.enabled = i_bVal
      .Page1.oPag.oCFDATNAS_1_38.enabled = i_bVal
      .Page1.oPag.oCFLOCNAS_1_39.enabled = i_bVal
      .Page1.oPag.oCFPRONAS_1_40.enabled = i_bVal
      .Page1.oPag.oCF__NOME_1_41.enabled = i_bVal
      .Page1.oPag.oCFPERFIS_1_42.enabled_(i_bVal)
      .Page1.oPag.oCFRAGSOC_1_43.enabled = i_bVal
      .Page1.oPag.oCF_SESSO_1_44.enabled = i_bVal
      .Page1.oPag.oCFDATNAS_1_45.enabled = i_bVal
      .Page1.oPag.oCFLOCNAS_1_46.enabled = i_bVal
      .Page1.oPag.oCFPRONAS_1_47.enabled = i_bVal
      .Page1.oPag.oCFINDFIS_1_91.enabled = i_bVal
      .Page1.oPag.oCFDOMFIS_1_92.enabled = i_bVal
      .Page1.oPag.oCFPRDFIS_1_94.enabled = i_bVal
      .Page1.oPag.oCFCAPFIS_1_96.enabled = i_bVal
      .Page1.oPag.oCFINDIRI_1_99.enabled = i_bVal
      .Page1.oPag.oCFLOCALI_1_101.enabled = i_bVal
      .Page1.oPag.oCFPROVIN_1_102.enabled = i_bVal
      .Page1.oPag.oCFCAPCON_1_103.enabled = i_bVal
      .Page1.oPag.oCFTELCON_1_104.enabled = i_bVal
      .Page1.oPag.oCFMAIVER_1_105.enabled = i_bVal
      .Page1.oPag.oCFCODIDE_1_106.enabled = i_bVal
      .Page1.oPag.oCFSECCOD_1_107.enabled = i_bVal
      .Page1.oPag.oBtn_1_125.enabled = .Page1.oPag.oBtn_1_125.mCond()
      .Page1.oPag.oObj_1_65.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'MODCPAG',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MODCPAG_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFSERIAL,"CFSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFDELEGT,"CFDELEGT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFDELEGB,"CFDELEGB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFCODAGE,"CFCODAGE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFAGPROV,"CFAGPROV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFCODFIS,"CFCODFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFCODFIS,"CFCODFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFPERFIS,"CFPERFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFRAGSOC,"CFRAGSOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CF__NOME,"CF__NOME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CF_SESSO,"CF_SESSO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFDATNAS,"CFDATNAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFLOCNAS,"CFLOCNAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFPRONAS,"CFPRONAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CF__NOME,"CF__NOME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFPERFIS,"CFPERFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFRAGSOC,"CFRAGSOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CF_SESSO,"CF_SESSO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFDATNAS,"CFDATNAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFLOCNAS,"CFLOCNAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFPRONAS,"CFPRONAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFINDFIS,"CFINDFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFDOMFIS,"CFDOMFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFPRDFIS,"CFPRDFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFCAPFIS,"CFCAPFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFINDIRI,"CFINDIRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFLOCALI,"CFLOCALI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFPROVIN,"CFPROVIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFCAPCON,"CFCAPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFTELCON,"CFTELCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFMAIVER,"CFMAIVER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFCODIDE,"CFCODIDE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFSECCOD,"CFSECCOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFCODFIR,"CFCODFIR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFCOGFIR,"CFCOGFIR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFNOMFIR,"CFNOMFIR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFSESFIR,"CFSESFIR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFDATFIR,"CFDATFIR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFCOMFIR,"CFCOMFIR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFPROFIR,"CFPROFIR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFDOMFIR,"CFDOMFIR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFPRDFIR,"CFPRDFIR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFCAPFIR,"CFCAPFIR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFINDFIR,"CFINDFIR",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MODCPAG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MODCPAG_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.MODCPAG_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into MODCPAG
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MODCPAG')
        i_extval=cp_InsertValODBCExtFlds(this,'MODCPAG')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(CFSERIAL,CFDELEGT,CFDELEGB,CFCODAGE,CFAGPROV"+;
                  ",CFCODFIS,CFPERFIS,CFRAGSOC,CF__NOME,CF_SESSO"+;
                  ",CFDATNAS,CFLOCNAS,CFPRONAS,CFINDFIS,CFDOMFIS"+;
                  ",CFPRDFIS,CFCAPFIS,CFINDIRI,CFLOCALI,CFPROVIN"+;
                  ",CFCAPCON,CFTELCON,CFMAIVER,CFCODIDE,CFSECCOD"+;
                  ",CFCODFIR,CFCOGFIR,CFNOMFIR,CFSESFIR,CFDATFIR"+;
                  ",CFCOMFIR,CFPROFIR,CFDOMFIR,CFPRDFIR,CFCAPFIR"+;
                  ",CFINDFIR "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_CFSERIAL)+;
                  ","+cp_ToStrODBC(this.w_CFDELEGT)+;
                  ","+cp_ToStrODBC(this.w_CFDELEGB)+;
                  ","+cp_ToStrODBC(this.w_CFCODAGE)+;
                  ","+cp_ToStrODBC(this.w_CFAGPROV)+;
                  ","+cp_ToStrODBC(this.w_CFCODFIS)+;
                  ","+cp_ToStrODBC(this.w_CFPERFIS)+;
                  ","+cp_ToStrODBC(this.w_CFRAGSOC)+;
                  ","+cp_ToStrODBC(this.w_CF__NOME)+;
                  ","+cp_ToStrODBC(this.w_CF_SESSO)+;
                  ","+cp_ToStrODBC(this.w_CFDATNAS)+;
                  ","+cp_ToStrODBC(this.w_CFLOCNAS)+;
                  ","+cp_ToStrODBC(this.w_CFPRONAS)+;
                  ","+cp_ToStrODBC(this.w_CFINDFIS)+;
                  ","+cp_ToStrODBC(this.w_CFDOMFIS)+;
                  ","+cp_ToStrODBC(this.w_CFPRDFIS)+;
                  ","+cp_ToStrODBC(this.w_CFCAPFIS)+;
                  ","+cp_ToStrODBC(this.w_CFINDIRI)+;
                  ","+cp_ToStrODBC(this.w_CFLOCALI)+;
                  ","+cp_ToStrODBC(this.w_CFPROVIN)+;
                  ","+cp_ToStrODBC(this.w_CFCAPCON)+;
                  ","+cp_ToStrODBC(this.w_CFTELCON)+;
                  ","+cp_ToStrODBC(this.w_CFMAIVER)+;
                  ","+cp_ToStrODBC(this.w_CFCODIDE)+;
                  ","+cp_ToStrODBC(this.w_CFSECCOD)+;
                  ","+cp_ToStrODBC(this.w_CFCODFIR)+;
                  ","+cp_ToStrODBC(this.w_CFCOGFIR)+;
                  ","+cp_ToStrODBC(this.w_CFNOMFIR)+;
                  ","+cp_ToStrODBC(this.w_CFSESFIR)+;
                  ","+cp_ToStrODBC(this.w_CFDATFIR)+;
                  ","+cp_ToStrODBC(this.w_CFCOMFIR)+;
                  ","+cp_ToStrODBC(this.w_CFPROFIR)+;
                  ","+cp_ToStrODBC(this.w_CFDOMFIR)+;
                  ","+cp_ToStrODBC(this.w_CFPRDFIR)+;
                  ","+cp_ToStrODBC(this.w_CFCAPFIR)+;
                  ","+cp_ToStrODBC(this.w_CFINDFIR)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MODCPAG')
        i_extval=cp_InsertValVFPExtFlds(this,'MODCPAG')
        cp_CheckDeletedKey(i_cTable,0,'CFSERIAL',this.w_CFSERIAL)
        INSERT INTO (i_cTable);
              (CFSERIAL,CFDELEGT,CFDELEGB,CFCODAGE,CFAGPROV,CFCODFIS,CFPERFIS,CFRAGSOC,CF__NOME,CF_SESSO,CFDATNAS,CFLOCNAS,CFPRONAS,CFINDFIS,CFDOMFIS,CFPRDFIS,CFCAPFIS,CFINDIRI,CFLOCALI,CFPROVIN,CFCAPCON,CFTELCON,CFMAIVER,CFCODIDE,CFSECCOD,CFCODFIR,CFCOGFIR,CFNOMFIR,CFSESFIR,CFDATFIR,CFCOMFIR,CFPROFIR,CFDOMFIR,CFPRDFIR,CFCAPFIR,CFINDFIR  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_CFSERIAL;
                  ,this.w_CFDELEGT;
                  ,this.w_CFDELEGB;
                  ,this.w_CFCODAGE;
                  ,this.w_CFAGPROV;
                  ,this.w_CFCODFIS;
                  ,this.w_CFPERFIS;
                  ,this.w_CFRAGSOC;
                  ,this.w_CF__NOME;
                  ,this.w_CF_SESSO;
                  ,this.w_CFDATNAS;
                  ,this.w_CFLOCNAS;
                  ,this.w_CFPRONAS;
                  ,this.w_CFINDFIS;
                  ,this.w_CFDOMFIS;
                  ,this.w_CFPRDFIS;
                  ,this.w_CFCAPFIS;
                  ,this.w_CFINDIRI;
                  ,this.w_CFLOCALI;
                  ,this.w_CFPROVIN;
                  ,this.w_CFCAPCON;
                  ,this.w_CFTELCON;
                  ,this.w_CFMAIVER;
                  ,this.w_CFCODIDE;
                  ,this.w_CFSECCOD;
                  ,this.w_CFCODFIR;
                  ,this.w_CFCOGFIR;
                  ,this.w_CFNOMFIR;
                  ,this.w_CFSESFIR;
                  ,this.w_CFDATFIR;
                  ,this.w_CFCOMFIR;
                  ,this.w_CFPROFIR;
                  ,this.w_CFDOMFIR;
                  ,this.w_CFPRDFIR;
                  ,this.w_CFCAPFIR;
                  ,this.w_CFINDFIR;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.MODCPAG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MODCPAG_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.MODCPAG_IDX,i_nConn)
      *
      * update MODCPAG
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'MODCPAG')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " CFDELEGT="+cp_ToStrODBC(this.w_CFDELEGT)+;
             ",CFDELEGB="+cp_ToStrODBC(this.w_CFDELEGB)+;
             ",CFCODAGE="+cp_ToStrODBC(this.w_CFCODAGE)+;
             ",CFAGPROV="+cp_ToStrODBC(this.w_CFAGPROV)+;
             ",CFCODFIS="+cp_ToStrODBC(this.w_CFCODFIS)+;
             ",CFPERFIS="+cp_ToStrODBC(this.w_CFPERFIS)+;
             ",CFRAGSOC="+cp_ToStrODBC(this.w_CFRAGSOC)+;
             ",CF__NOME="+cp_ToStrODBC(this.w_CF__NOME)+;
             ",CF_SESSO="+cp_ToStrODBC(this.w_CF_SESSO)+;
             ",CFDATNAS="+cp_ToStrODBC(this.w_CFDATNAS)+;
             ",CFLOCNAS="+cp_ToStrODBC(this.w_CFLOCNAS)+;
             ",CFPRONAS="+cp_ToStrODBC(this.w_CFPRONAS)+;
             ",CFINDFIS="+cp_ToStrODBC(this.w_CFINDFIS)+;
             ",CFDOMFIS="+cp_ToStrODBC(this.w_CFDOMFIS)+;
             ",CFPRDFIS="+cp_ToStrODBC(this.w_CFPRDFIS)+;
             ",CFCAPFIS="+cp_ToStrODBC(this.w_CFCAPFIS)+;
             ",CFINDIRI="+cp_ToStrODBC(this.w_CFINDIRI)+;
             ",CFLOCALI="+cp_ToStrODBC(this.w_CFLOCALI)+;
             ",CFPROVIN="+cp_ToStrODBC(this.w_CFPROVIN)+;
             ",CFCAPCON="+cp_ToStrODBC(this.w_CFCAPCON)+;
             ",CFTELCON="+cp_ToStrODBC(this.w_CFTELCON)+;
             ",CFMAIVER="+cp_ToStrODBC(this.w_CFMAIVER)+;
             ",CFCODIDE="+cp_ToStrODBC(this.w_CFCODIDE)+;
             ",CFSECCOD="+cp_ToStrODBC(this.w_CFSECCOD)+;
             ",CFCODFIR="+cp_ToStrODBC(this.w_CFCODFIR)+;
             ",CFCOGFIR="+cp_ToStrODBC(this.w_CFCOGFIR)+;
             ",CFNOMFIR="+cp_ToStrODBC(this.w_CFNOMFIR)+;
             ",CFSESFIR="+cp_ToStrODBC(this.w_CFSESFIR)+;
             ",CFDATFIR="+cp_ToStrODBC(this.w_CFDATFIR)+;
             ",CFCOMFIR="+cp_ToStrODBC(this.w_CFCOMFIR)+;
             ",CFPROFIR="+cp_ToStrODBC(this.w_CFPROFIR)+;
             ",CFDOMFIR="+cp_ToStrODBC(this.w_CFDOMFIR)+;
             ",CFPRDFIR="+cp_ToStrODBC(this.w_CFPRDFIR)+;
             ",CFCAPFIR="+cp_ToStrODBC(this.w_CFCAPFIR)+;
             ",CFINDFIR="+cp_ToStrODBC(this.w_CFINDFIR)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'MODCPAG')
        i_cWhere = cp_PKFox(i_cTable  ,'CFSERIAL',this.w_CFSERIAL  )
        UPDATE (i_cTable) SET;
              CFDELEGT=this.w_CFDELEGT;
             ,CFDELEGB=this.w_CFDELEGB;
             ,CFCODAGE=this.w_CFCODAGE;
             ,CFAGPROV=this.w_CFAGPROV;
             ,CFCODFIS=this.w_CFCODFIS;
             ,CFPERFIS=this.w_CFPERFIS;
             ,CFRAGSOC=this.w_CFRAGSOC;
             ,CF__NOME=this.w_CF__NOME;
             ,CF_SESSO=this.w_CF_SESSO;
             ,CFDATNAS=this.w_CFDATNAS;
             ,CFLOCNAS=this.w_CFLOCNAS;
             ,CFPRONAS=this.w_CFPRONAS;
             ,CFINDFIS=this.w_CFINDFIS;
             ,CFDOMFIS=this.w_CFDOMFIS;
             ,CFPRDFIS=this.w_CFPRDFIS;
             ,CFCAPFIS=this.w_CFCAPFIS;
             ,CFINDIRI=this.w_CFINDIRI;
             ,CFLOCALI=this.w_CFLOCALI;
             ,CFPROVIN=this.w_CFPROVIN;
             ,CFCAPCON=this.w_CFCAPCON;
             ,CFTELCON=this.w_CFTELCON;
             ,CFMAIVER=this.w_CFMAIVER;
             ,CFCODIDE=this.w_CFCODIDE;
             ,CFSECCOD=this.w_CFSECCOD;
             ,CFCODFIR=this.w_CFCODFIR;
             ,CFCOGFIR=this.w_CFCOGFIR;
             ,CFNOMFIR=this.w_CFNOMFIR;
             ,CFSESFIR=this.w_CFSESFIR;
             ,CFDATFIR=this.w_CFDATFIR;
             ,CFCOMFIR=this.w_CFCOMFIR;
             ,CFPROFIR=this.w_CFPROFIR;
             ,CFDOMFIR=this.w_CFDOMFIR;
             ,CFPRDFIR=this.w_CFPRDFIR;
             ,CFCAPFIR=this.w_CFCAPFIR;
             ,CFINDFIR=this.w_CFINDFIR;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.MODCPAG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MODCPAG_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.MODCPAG_IDX,i_nConn)
      *
      * delete MODCPAG
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'CFSERIAL',this.w_CFSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MODCPAG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MODCPAG_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_COD_SOC<>.w_COD_SOC
          .link_1_2('Full')
        endif
        if .o_COD_PRE<>.w_COD_PRE
            .w_COD_PRE = i_CODAZI
          .link_1_3('Full')
        endif
        .DoRTCalc(4,6,.t.)
        if .o_COD_FIS<>.w_COD_FIS
            .w_COD_FIS = i_CODAZI
          .link_1_7('Full')
        endif
        .DoRTCalc(8,26,.t.)
        if .o_SEDI<>.w_SEDI
          .link_1_27('Full')
        endif
        .DoRTCalc(28,30,.t.)
        if .o_CFCODAGE<>.w_CFCODAGE
            .w_CFAGPROV = iif(empty(.w_CFCODAGE),'  ',.w_CFAGPROV)
        endif
        .DoRTCalc(32,33,.t.)
        if .o_PERAZ<>.w_PERAZ
            .w_CFPERFIS = iif(.w_PERAZ='S','S','N')
        endif
        if .o_NOME_SOC<>.w_NOME_SOC.or. .o_NOME_PER<>.w_NOME_PER.or. .o_PERAZ<>.w_PERAZ
            .w_CFRAGSOC = iif(.w_PERAZ='S',upper(.w_NOME_PER),upper(.w_NOME_SOC))
        endif
        if .o_CFPERFIS<>.w_CFPERFIS
            .w_CF__NOME = iif(.w_CFPERFIS='S',upper(.w_CF__NOME),'')
        endif
        .DoRTCalc(37,37,.t.)
        if .o_CFPERFIS<>.w_CFPERFIS.or. .o_DATAPP<>.w_DATAPP
            .w_CFDATNAS = iif(.w_CFPERFIS='S',.w_CFDATNAS,.w_DATAPP)
        endif
        if .o_CFPERFIS<>.w_CFPERFIS
            .w_CFLOCNAS = iif(.w_CFPERFIS='S',upper(.w_CFLOCNAS),'')
        endif
        if .o_CFPERFIS<>.w_CFPERFIS
            .w_CFPRONAS = iif(.w_CFPERFIS='S',upper(.w_CFPRONAS),'  ')
        endif
        if .o_CFPERFIS<>.w_CFPERFIS
            .w_CF__NOME = iif(.w_CFPERFIS='S',upper(.w_CF__NOME),'')
        endif
        if .o_PERAZ<>.w_PERAZ
            .w_CFPERFIS = iif(.w_PERAZ='S','S','N')
        endif
        if .o_NOME_SOC<>.w_NOME_SOC.or. .o_NOME_PER<>.w_NOME_PER.or. .o_PERAZ<>.w_PERAZ
            .w_CFRAGSOC = iif(.w_PERAZ='S',upper(.w_NOME_PER),upper(.w_NOME_SOC))
        endif
        .DoRTCalc(44,44,.t.)
        if .o_CFPERFIS<>.w_CFPERFIS.or. .o_DATAPP<>.w_DATAPP
            .w_CFDATNAS = iif(.w_CFPERFIS='S',.w_CFDATNAS,.w_DATAPP)
        endif
        if .o_CFPERFIS<>.w_CFPERFIS
            .w_CFLOCNAS = iif(.w_CFPERFIS='S',upper(.w_CFLOCNAS),'')
        endif
        if .o_CFPERFIS<>.w_CFPERFIS
            .w_CFPRONAS = iif(.w_CFPERFIS='S',upper(.w_CFPRONAS),'  ')
        endif
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate()
        if .o_CFPERFIS<>.w_CFPERFIS
          .Calculate_MQWQCOFKFW()
        endif
        .DoRTCalc(48,53,.t.)
        if .o_SEDILEG<>.w_SEDILEG
            .w_SEDILEG = iif(.w_CFPERFIS<>'S',i_CODAZI,' ')
          .link_1_90('Full')
        endif
        .DoRTCalc(55,55,.t.)
        if .o_CFINDFIS<>.w_CFINDFIS
            .w_CFDOMFIS = IIF (.w_CFPERFIS='N' ,IIF (NOT EMPTY (.w_CFDOMFIS2),.w_CFDOMFIS2,.w_COMU_SOC),'')
        endif
        .DoRTCalc(57,58,.t.)
        if .o_PERAZ<>.w_PERAZ.or. .o_INDIRPER<>.w_INDIRPER.or. .o_INDIRSOC<>.w_INDIRSOC
            .w_CFINDIRI = iif(not empty(.w_FISCODES),left(upper(.w_FISINDIR),30),iif(.w_PERAZ='S',iif(empty(upper(.w_INDIRPER)),upper(left(.w_INDIRSOC,30)),left(upper(.w_INDIRPER),30)),upper(left(.w_INDIRSOC,30))))
        endif
        if .o_COMU_SOC<>.w_COMU_SOC.or. .o_COMU_PER<>.w_COMU_PER.or. .o_PERAZ<>.w_PERAZ
            .w_CFLOCALI = iif(not empty(.w_FISCODES),left(upper(.w_FISLOCAL),30),iif(.w_PERAZ='S',iif(empty(upper(.w_COMU_PER)),left(upper(.w_COMU_SOC),30),left(upper(.w_COMU_PER),30)),left(upper(.w_COMU_SOC),30)))
        endif
        if .o_PROV_SOC<>.w_PROV_SOC.or. .o_PROV_PER<>.w_PROV_PER.or. .o_PERAZ<>.w_PERAZ
            .w_CFPROVIN = iif(not empty(.w_FISCODES),upper(.w_FISPROV),iif(.w_PERAZ='S',iif(empty(upper(.w_PROV_PER)),upper(.w_PROV_SOC),upper(.w_PROV_PER)),upper(.w_PROV_SOC)))
        endif
        if .o_CFCODIDE<>.w_CFCODIDE
          .Calculate_IFQQKFGWPY()
        endif
        if .o_CFCODIDE<>.w_CFCODIDE
          .Calculate_EHOLEYRHBI()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(62,85,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate()
    endwith
  return

  proc Calculate_MQWQCOFKFW()
    with this
          * --- Aggiorna tipo contribuente sul padre
          AggiornaTipo(this;
             )
          .w_CFCODIDE = IIF (.oparentobject.w_MFENTRAT<>'E' OR  ( .w_CFPERFIS='S' AND .oparentobject.w_MFCHKFIR=' ') OR   ( .w_CFPERFIS='N' AND .oparentobject.w_MFCHKFIR='S' AND .oparentobject.w_MFCARFIR='1' ) , .w_CFCODIDE , ' ')
          .w_CFSECCOD = IIF (.oparentobject.w_MFENTRAT<>'E' OR  ( .w_CFPERFIS='S' AND .oparentobject.w_MFCHKFIR=' ') OR   ( .w_CFPERFIS='N' AND .oparentobject.w_MFCHKFIR='S' AND .oparentobject.w_MFCARFIR='1' ) , .w_CFSECCOD , ' ')
    endwith
  endproc
  proc Calculate_IFQQKFGWPY()
    with this
          * --- Sbianca Secondo codice fiscale
          .w_CFSECCOD = IIF (EMPTY ( .w_CFCODIDE),   ' ',.w_CFSECCOD)
    endwith
  endproc
  proc Calculate_EHOLEYRHBI()
    with this
          * --- Sbianca Secondo codice fiscale
          .w_CFSECCOD = IIF (EMPTY ( .w_CFCODIDE),   ' ',.w_CFSECCOD)
          .w_CFCOGFIR = ''
          .w_CFDATFIR = ctod('  /  /  ')
          .w_CFCOMFIR = ''
          .w_CFPROFIR = ''
          .w_CFINDFIR = ''
          .w_CFPRDFIR = ''
          .w_CFCAPFIR = ''
          .w_CFDOMFIR = ''
          .w_CFNOMFIR = ''
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCFAGPROV_1_31.enabled = this.oPgFrm.Page1.oPag.oCFAGPROV_1_31.mCond()
    this.oPgFrm.Page1.oPag.oCF__NOME_1_36.enabled = this.oPgFrm.Page1.oPag.oCF__NOME_1_36.mCond()
    this.oPgFrm.Page1.oPag.oCFDATNAS_1_38.enabled = this.oPgFrm.Page1.oPag.oCFDATNAS_1_38.mCond()
    this.oPgFrm.Page1.oPag.oCFLOCNAS_1_39.enabled = this.oPgFrm.Page1.oPag.oCFLOCNAS_1_39.mCond()
    this.oPgFrm.Page1.oPag.oCFPRONAS_1_40.enabled = this.oPgFrm.Page1.oPag.oCFPRONAS_1_40.mCond()
    this.oPgFrm.Page1.oPag.oCF__NOME_1_41.enabled = this.oPgFrm.Page1.oPag.oCF__NOME_1_41.mCond()
    this.oPgFrm.Page1.oPag.oCFDATNAS_1_45.enabled = this.oPgFrm.Page1.oPag.oCFDATNAS_1_45.mCond()
    this.oPgFrm.Page1.oPag.oCFLOCNAS_1_46.enabled = this.oPgFrm.Page1.oPag.oCFLOCNAS_1_46.mCond()
    this.oPgFrm.Page1.oPag.oCFPRONAS_1_47.enabled = this.oPgFrm.Page1.oPag.oCFPRONAS_1_47.mCond()
    this.oPgFrm.Page1.oPag.oCFINDFIS_1_91.enabled = this.oPgFrm.Page1.oPag.oCFINDFIS_1_91.mCond()
    this.oPgFrm.Page1.oPag.oCFDOMFIS_1_92.enabled = this.oPgFrm.Page1.oPag.oCFDOMFIS_1_92.mCond()
    this.oPgFrm.Page1.oPag.oCFPRDFIS_1_94.enabled = this.oPgFrm.Page1.oPag.oCFPRDFIS_1_94.mCond()
    this.oPgFrm.Page1.oPag.oCFCAPFIS_1_96.enabled = this.oPgFrm.Page1.oPag.oCFCAPFIS_1_96.mCond()
    this.oPgFrm.Page1.oPag.oCFCODIDE_1_106.enabled = this.oPgFrm.Page1.oPag.oCFCODIDE_1_106.mCond()
    this.oPgFrm.Page1.oPag.oCFSECCOD_1_107.enabled = this.oPgFrm.Page1.oPag.oCFSECCOD_1_107.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_125.enabled = this.oPgFrm.Page1.oPag.oBtn_1_125.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCFDELEGT_1_28.visible=!this.oPgFrm.Page1.oPag.oCFDELEGT_1_28.mHide()
    this.oPgFrm.Page1.oPag.oCFDELEGB_1_29.visible=!this.oPgFrm.Page1.oPag.oCFDELEGB_1_29.mHide()
    this.oPgFrm.Page1.oPag.oCFCODAGE_1_30.visible=!this.oPgFrm.Page1.oPag.oCFCODAGE_1_30.mHide()
    this.oPgFrm.Page1.oPag.oCFAGPROV_1_31.visible=!this.oPgFrm.Page1.oPag.oCFAGPROV_1_31.mHide()
    this.oPgFrm.Page1.oPag.oCFCODFIS_1_32.visible=!this.oPgFrm.Page1.oPag.oCFCODFIS_1_32.mHide()
    this.oPgFrm.Page1.oPag.oCFCODFIS_1_33.visible=!this.oPgFrm.Page1.oPag.oCFCODFIS_1_33.mHide()
    this.oPgFrm.Page1.oPag.oCFPERFIS_1_34.visible=!this.oPgFrm.Page1.oPag.oCFPERFIS_1_34.mHide()
    this.oPgFrm.Page1.oPag.oCFRAGSOC_1_35.visible=!this.oPgFrm.Page1.oPag.oCFRAGSOC_1_35.mHide()
    this.oPgFrm.Page1.oPag.oCF__NOME_1_36.visible=!this.oPgFrm.Page1.oPag.oCF__NOME_1_36.mHide()
    this.oPgFrm.Page1.oPag.oCF_SESSO_1_37.visible=!this.oPgFrm.Page1.oPag.oCF_SESSO_1_37.mHide()
    this.oPgFrm.Page1.oPag.oCFDATNAS_1_38.visible=!this.oPgFrm.Page1.oPag.oCFDATNAS_1_38.mHide()
    this.oPgFrm.Page1.oPag.oCFLOCNAS_1_39.visible=!this.oPgFrm.Page1.oPag.oCFLOCNAS_1_39.mHide()
    this.oPgFrm.Page1.oPag.oCFPRONAS_1_40.visible=!this.oPgFrm.Page1.oPag.oCFPRONAS_1_40.mHide()
    this.oPgFrm.Page1.oPag.oCF__NOME_1_41.visible=!this.oPgFrm.Page1.oPag.oCF__NOME_1_41.mHide()
    this.oPgFrm.Page1.oPag.oCFPERFIS_1_42.visible=!this.oPgFrm.Page1.oPag.oCFPERFIS_1_42.mHide()
    this.oPgFrm.Page1.oPag.oCFRAGSOC_1_43.visible=!this.oPgFrm.Page1.oPag.oCFRAGSOC_1_43.mHide()
    this.oPgFrm.Page1.oPag.oCF_SESSO_1_44.visible=!this.oPgFrm.Page1.oPag.oCF_SESSO_1_44.mHide()
    this.oPgFrm.Page1.oPag.oCFDATNAS_1_45.visible=!this.oPgFrm.Page1.oPag.oCFDATNAS_1_45.mHide()
    this.oPgFrm.Page1.oPag.oCFLOCNAS_1_46.visible=!this.oPgFrm.Page1.oPag.oCFLOCNAS_1_46.mHide()
    this.oPgFrm.Page1.oPag.oCFPRONAS_1_47.visible=!this.oPgFrm.Page1.oPag.oCFPRONAS_1_47.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_48.visible=!this.oPgFrm.Page1.oPag.oStr_1_48.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_49.visible=!this.oPgFrm.Page1.oPag.oStr_1_49.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_50.visible=!this.oPgFrm.Page1.oPag.oStr_1_50.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_51.visible=!this.oPgFrm.Page1.oPag.oStr_1_51.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_52.visible=!this.oPgFrm.Page1.oPag.oStr_1_52.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_53.visible=!this.oPgFrm.Page1.oPag.oStr_1_53.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_54.visible=!this.oPgFrm.Page1.oPag.oStr_1_54.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_55.visible=!this.oPgFrm.Page1.oPag.oStr_1_55.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_56.visible=!this.oPgFrm.Page1.oPag.oStr_1_56.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_61.visible=!this.oPgFrm.Page1.oPag.oStr_1_61.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_62.visible=!this.oPgFrm.Page1.oPag.oStr_1_62.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_73.visible=!this.oPgFrm.Page1.oPag.oStr_1_73.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_74.visible=!this.oPgFrm.Page1.oPag.oStr_1_74.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_77.visible=!this.oPgFrm.Page1.oPag.oStr_1_77.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_78.visible=!this.oPgFrm.Page1.oPag.oStr_1_78.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_79.visible=!this.oPgFrm.Page1.oPag.oStr_1_79.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_80.visible=!this.oPgFrm.Page1.oPag.oStr_1_80.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_81.visible=!this.oPgFrm.Page1.oPag.oStr_1_81.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_82.visible=!this.oPgFrm.Page1.oPag.oStr_1_82.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_83.visible=!this.oPgFrm.Page1.oPag.oStr_1_83.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_84.visible=!this.oPgFrm.Page1.oPag.oStr_1_84.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_85.visible=!this.oPgFrm.Page1.oPag.oStr_1_85.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_86.visible=!this.oPgFrm.Page1.oPag.oStr_1_86.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_87.visible=!this.oPgFrm.Page1.oPag.oStr_1_87.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_88.visible=!this.oPgFrm.Page1.oPag.oStr_1_88.mHide()
    this.oPgFrm.Page1.oPag.oCFINDFIS_1_91.visible=!this.oPgFrm.Page1.oPag.oCFINDFIS_1_91.mHide()
    this.oPgFrm.Page1.oPag.oCFDOMFIS_1_92.visible=!this.oPgFrm.Page1.oPag.oCFDOMFIS_1_92.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_93.visible=!this.oPgFrm.Page1.oPag.oStr_1_93.mHide()
    this.oPgFrm.Page1.oPag.oCFPRDFIS_1_94.visible=!this.oPgFrm.Page1.oPag.oCFPRDFIS_1_94.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_95.visible=!this.oPgFrm.Page1.oPag.oStr_1_95.mHide()
    this.oPgFrm.Page1.oPag.oCFCAPFIS_1_96.visible=!this.oPgFrm.Page1.oPag.oCFCAPFIS_1_96.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_97.visible=!this.oPgFrm.Page1.oPag.oStr_1_97.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_98.visible=!this.oPgFrm.Page1.oPag.oStr_1_98.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_100.visible=!this.oPgFrm.Page1.oPag.oStr_1_100.mHide()
    this.oPgFrm.Page1.oPag.oCFCAPCON_1_103.visible=!this.oPgFrm.Page1.oPag.oCFCAPCON_1_103.mHide()
    this.oPgFrm.Page1.oPag.oCFTELCON_1_104.visible=!this.oPgFrm.Page1.oPag.oCFTELCON_1_104.mHide()
    this.oPgFrm.Page1.oPag.oCFMAIVER_1_105.visible=!this.oPgFrm.Page1.oPag.oCFMAIVER_1_105.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_125.visible=!this.oPgFrm.Page1.oPag.oBtn_1_125.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_128.visible=!this.oPgFrm.Page1.oPag.oStr_1_128.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_131.visible=!this.oPgFrm.Page1.oPag.oStr_1_131.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_132.visible=!this.oPgFrm.Page1.oPag.oStr_1_132.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_133.visible=!this.oPgFrm.Page1.oPag.oStr_1_133.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_65.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=COD_SOC
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COD_SOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COD_SOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI,AZPERAZI,AZCOFAZI,AZINDAZI,AZLOCAZI,AZPROAZI,AZDESTES,AZDESBAN,AZCAPAZI,AZTELEFO,AZ_EMAIL";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_COD_SOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_COD_SOC)
            select AZCODAZI,AZRAGAZI,AZPERAZI,AZCOFAZI,AZINDAZI,AZLOCAZI,AZPROAZI,AZDESTES,AZDESBAN,AZCAPAZI,AZTELEFO,AZ_EMAIL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COD_SOC = NVL(_Link_.AZCODAZI,space(5))
      this.w_NOME_SOC = NVL(_Link_.AZRAGAZI,space(40))
      this.w_PERAZ = NVL(_Link_.AZPERAZI,space(1))
      this.w_CFCODFIS = NVL(_Link_.AZCOFAZI,space(16))
      this.w_INDIRSOC = NVL(_Link_.AZINDAZI,space(35))
      this.w_COMU_SOC = NVL(_Link_.AZLOCAZI,space(30))
      this.w_PROV_SOC = NVL(_Link_.AZPROAZI,space(2))
      this.w_CFDELEGT = NVL(_Link_.AZDESTES,space(50))
      this.w_CFDELEGB = NVL(_Link_.AZDESBAN,space(50))
      this.w_CAP_SOC = NVL(_Link_.AZCAPAZI,space(10))
      this.w_TEL_SOC = NVL(_Link_.AZTELEFO,space(10))
      this.w_EMAILAZI = NVL(_Link_.AZ_EMAIL,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_COD_SOC = space(5)
      endif
      this.w_NOME_SOC = space(40)
      this.w_PERAZ = space(1)
      this.w_CFCODFIS = space(16)
      this.w_INDIRSOC = space(35)
      this.w_COMU_SOC = space(30)
      this.w_PROV_SOC = space(2)
      this.w_CFDELEGT = space(50)
      this.w_CFDELEGB = space(50)
      this.w_CAP_SOC = space(10)
      this.w_TEL_SOC = space(10)
      this.w_EMAILAZI = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COD_SOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COD_PRE
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TITOLARI_IDX,3]
    i_lTable = "TITOLARI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2], .t., this.TITOLARI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COD_PRE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COD_PRE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TTCODAZI,TTCOGTIT,TTNOMTIT,TTDATNAS,TTLUONAS,TTPRONAS,TT_SESSO,TTINDIRI,TTLOCTIT,TTPROTIT,TTCAPTIT,TTTELEFO";
                   +" from "+i_cTable+" "+i_lTable+" where TTCODAZI="+cp_ToStrODBC(this.w_COD_PRE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TTCODAZI',this.w_COD_PRE)
            select TTCODAZI,TTCOGTIT,TTNOMTIT,TTDATNAS,TTLUONAS,TTPRONAS,TT_SESSO,TTINDIRI,TTLOCTIT,TTPROTIT,TTCAPTIT,TTTELEFO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COD_PRE = NVL(_Link_.TTCODAZI,space(5))
      this.w_NOME_PER = NVL(_Link_.TTCOGTIT,space(40))
      this.w_CF__NOME = NVL(_Link_.TTNOMTIT,space(25))
      this.w_CFDATNAS = NVL(cp_ToDate(_Link_.TTDATNAS),ctod("  /  /  "))
      this.w_CFLOCNAS = NVL(_Link_.TTLUONAS,space(30))
      this.w_CFPRONAS = NVL(_Link_.TTPRONAS,space(2))
      this.w_CF_SESSO = NVL(_Link_.TT_SESSO,space(1))
      this.w_INDIRPER = NVL(_Link_.TTINDIRI,space(30))
      this.w_COMU_PER = NVL(_Link_.TTLOCTIT,space(30))
      this.w_PROV_PER = NVL(_Link_.TTPROTIT,space(2))
      this.w_CAP_PER = NVL(_Link_.TTCAPTIT,space(10))
      this.w_TEL_PER = NVL(_Link_.TTTELEFO,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_COD_PRE = space(5)
      endif
      this.w_NOME_PER = space(40)
      this.w_CF__NOME = space(25)
      this.w_CFDATNAS = ctod("  /  /  ")
      this.w_CFLOCNAS = space(30)
      this.w_CFPRONAS = space(2)
      this.w_CF_SESSO = space(1)
      this.w_INDIRPER = space(30)
      this.w_COMU_PER = space(30)
      this.w_PROV_PER = space(2)
      this.w_CAP_PER = space(10)
      this.w_TEL_PER = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2])+'\'+cp_ToStr(_Link_.TTCODAZI,1)
      cp_ShowWarn(i_cKey,this.TITOLARI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COD_PRE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COD_FIS
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SEDIAZIE_IDX,3]
    i_lTable = "SEDIAZIE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2], .t., this.SEDIAZIE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COD_FIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COD_FIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SETIPRIF,SECODAZI,SEINDIRI,SELOCALI,SEPROVIN,SECODDES,SE___CAP,SETELEFO,SE_EMAIL";
                   +" from "+i_cTable+" "+i_lTable+" where SECODAZI="+cp_ToStrODBC(this.w_COD_FIS);
                   +" and SETIPRIF="+cp_ToStrODBC(this.w_SEDE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SETIPRIF',this.w_SEDE;
                       ,'SECODAZI',this.w_COD_FIS)
            select SETIPRIF,SECODAZI,SEINDIRI,SELOCALI,SEPROVIN,SECODDES,SE___CAP,SETELEFO,SE_EMAIL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COD_FIS = NVL(_Link_.SECODAZI,space(5))
      this.w_FISINDIR = NVL(_Link_.SEINDIRI,space(35))
      this.w_FISLOCAL = NVL(_Link_.SELOCALI,space(30))
      this.w_FISPROV = NVL(_Link_.SEPROVIN,space(2))
      this.w_FISCODES = NVL(_Link_.SECODDES,space(5))
      this.w_FISCAP = NVL(_Link_.SE___CAP,space(10))
      this.w_SETEL = NVL(_Link_.SETELEFO,space(10))
      this.w_EMAILSEDE = NVL(_Link_.SE_EMAIL,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_COD_FIS = space(5)
      endif
      this.w_FISINDIR = space(35)
      this.w_FISLOCAL = space(30)
      this.w_FISPROV = space(2)
      this.w_FISCODES = space(5)
      this.w_FISCAP = space(10)
      this.w_SETEL = space(10)
      this.w_EMAILSEDE = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])+'\'+cp_ToStr(_Link_.SETIPRIF,1)+'\'+cp_ToStr(_Link_.SECODAZI,1)
      cp_ShowWarn(i_cKey,this.SEDIAZIE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COD_FIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SEDI
  func Link_1_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SEDIAZIE_IDX,3]
    i_lTable = "SEDIAZIE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2], .t., this.SEDIAZIE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SEDI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SEDI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SETIPRIF,SECODAZI,SEINDIRI,SELOCALI,SEPROVIN,SE___CAP,SETELEFO";
                   +" from "+i_cTable+" "+i_lTable+" where SECODAZI="+cp_ToStrODBC(this.w_SEDI);
                   +" and SETIPRIF="+cp_ToStrODBC(this.w_SETIPRIF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SETIPRIF',this.w_SETIPRIF;
                       ,'SECODAZI',this.w_SEDI)
            select SETIPRIF,SECODAZI,SEINDIRI,SELOCALI,SEPROVIN,SE___CAP,SETELEFO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SEDI = NVL(_Link_.SECODAZI,space(5))
      this.w_CFINDIRI2 = NVL(_Link_.SEINDIRI,space(30))
      this.w_CFLOCALI2 = NVL(_Link_.SELOCALI,space(30))
      this.w_CFPROVIN2 = NVL(_Link_.SEPROVIN,space(2))
      this.w_CFCAPCON2 = NVL(_Link_.SE___CAP,space(10))
      this.w_CFTELCON2 = NVL(_Link_.SETELEFO,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_SEDI = space(5)
      endif
      this.w_CFINDIRI2 = space(30)
      this.w_CFLOCALI2 = space(30)
      this.w_CFPROVIN2 = space(2)
      this.w_CFCAPCON2 = space(10)
      this.w_CFTELCON2 = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])+'\'+cp_ToStr(_Link_.SETIPRIF,1)+'\'+cp_ToStr(_Link_.SECODAZI,1)
      cp_ShowWarn(i_cKey,this.SEDIAZIE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SEDI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SEDILEG
  func Link_1_90(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SEDIAZIE_IDX,3]
    i_lTable = "SEDIAZIE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2], .t., this.SEDIAZIE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SEDILEG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SEDILEG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SETIPRIF,SECODAZI,SELOCALI,SEPROVIN,SEINDIRI,SE___CAP";
                   +" from "+i_cTable+" "+i_lTable+" where SECODAZI="+cp_ToStrODBC(this.w_SEDILEG);
                   +" and SETIPRIF="+cp_ToStrODBC(this.w_TIPSL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SETIPRIF',this.w_TIPSL;
                       ,'SECODAZI',this.w_SEDILEG)
            select SETIPRIF,SECODAZI,SELOCALI,SEPROVIN,SEINDIRI,SE___CAP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SEDILEG = NVL(_Link_.SECODAZI,space(5))
      this.w_CFDOMFIS2 = NVL(_Link_.SELOCALI,space(10))
      this.w_CFPRDFIS2 = NVL(_Link_.SEPROVIN,space(10))
      this.w_CFINDFIS2 = NVL(_Link_.SEINDIRI,space(10))
      this.w_CFCAPFIS2 = NVL(_Link_.SE___CAP,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_SEDILEG = space(5)
      endif
      this.w_CFDOMFIS2 = space(10)
      this.w_CFPRDFIS2 = space(10)
      this.w_CFINDFIS2 = space(10)
      this.w_CFCAPFIS2 = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])+'\'+cp_ToStr(_Link_.SETIPRIF,1)+'\'+cp_ToStr(_Link_.SECODAZI,1)
      cp_ShowWarn(i_cKey,this.SEDIAZIE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SEDILEG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCFDELEGT_1_28.value==this.w_CFDELEGT)
      this.oPgFrm.Page1.oPag.oCFDELEGT_1_28.value=this.w_CFDELEGT
    endif
    if not(this.oPgFrm.Page1.oPag.oCFDELEGB_1_29.value==this.w_CFDELEGB)
      this.oPgFrm.Page1.oPag.oCFDELEGB_1_29.value=this.w_CFDELEGB
    endif
    if not(this.oPgFrm.Page1.oPag.oCFCODAGE_1_30.value==this.w_CFCODAGE)
      this.oPgFrm.Page1.oPag.oCFCODAGE_1_30.value=this.w_CFCODAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oCFAGPROV_1_31.value==this.w_CFAGPROV)
      this.oPgFrm.Page1.oPag.oCFAGPROV_1_31.value=this.w_CFAGPROV
    endif
    if not(this.oPgFrm.Page1.oPag.oCFCODFIS_1_32.value==this.w_CFCODFIS)
      this.oPgFrm.Page1.oPag.oCFCODFIS_1_32.value=this.w_CFCODFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oCFCODFIS_1_33.value==this.w_CFCODFIS)
      this.oPgFrm.Page1.oPag.oCFCODFIS_1_33.value=this.w_CFCODFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oCFPERFIS_1_34.RadioValue()==this.w_CFPERFIS)
      this.oPgFrm.Page1.oPag.oCFPERFIS_1_34.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCFRAGSOC_1_35.value==this.w_CFRAGSOC)
      this.oPgFrm.Page1.oPag.oCFRAGSOC_1_35.value=this.w_CFRAGSOC
    endif
    if not(this.oPgFrm.Page1.oPag.oCF__NOME_1_36.value==this.w_CF__NOME)
      this.oPgFrm.Page1.oPag.oCF__NOME_1_36.value=this.w_CF__NOME
    endif
    if not(this.oPgFrm.Page1.oPag.oCF_SESSO_1_37.RadioValue()==this.w_CF_SESSO)
      this.oPgFrm.Page1.oPag.oCF_SESSO_1_37.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCFDATNAS_1_38.value==this.w_CFDATNAS)
      this.oPgFrm.Page1.oPag.oCFDATNAS_1_38.value=this.w_CFDATNAS
    endif
    if not(this.oPgFrm.Page1.oPag.oCFLOCNAS_1_39.value==this.w_CFLOCNAS)
      this.oPgFrm.Page1.oPag.oCFLOCNAS_1_39.value=this.w_CFLOCNAS
    endif
    if not(this.oPgFrm.Page1.oPag.oCFPRONAS_1_40.value==this.w_CFPRONAS)
      this.oPgFrm.Page1.oPag.oCFPRONAS_1_40.value=this.w_CFPRONAS
    endif
    if not(this.oPgFrm.Page1.oPag.oCF__NOME_1_41.value==this.w_CF__NOME)
      this.oPgFrm.Page1.oPag.oCF__NOME_1_41.value=this.w_CF__NOME
    endif
    if not(this.oPgFrm.Page1.oPag.oCFPERFIS_1_42.RadioValue()==this.w_CFPERFIS)
      this.oPgFrm.Page1.oPag.oCFPERFIS_1_42.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCFRAGSOC_1_43.value==this.w_CFRAGSOC)
      this.oPgFrm.Page1.oPag.oCFRAGSOC_1_43.value=this.w_CFRAGSOC
    endif
    if not(this.oPgFrm.Page1.oPag.oCF_SESSO_1_44.RadioValue()==this.w_CF_SESSO)
      this.oPgFrm.Page1.oPag.oCF_SESSO_1_44.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCFDATNAS_1_45.value==this.w_CFDATNAS)
      this.oPgFrm.Page1.oPag.oCFDATNAS_1_45.value=this.w_CFDATNAS
    endif
    if not(this.oPgFrm.Page1.oPag.oCFLOCNAS_1_46.value==this.w_CFLOCNAS)
      this.oPgFrm.Page1.oPag.oCFLOCNAS_1_46.value=this.w_CFLOCNAS
    endif
    if not(this.oPgFrm.Page1.oPag.oCFPRONAS_1_47.value==this.w_CFPRONAS)
      this.oPgFrm.Page1.oPag.oCFPRONAS_1_47.value=this.w_CFPRONAS
    endif
    if not(this.oPgFrm.Page1.oPag.oCFINDFIS_1_91.value==this.w_CFINDFIS)
      this.oPgFrm.Page1.oPag.oCFINDFIS_1_91.value=this.w_CFINDFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oCFDOMFIS_1_92.value==this.w_CFDOMFIS)
      this.oPgFrm.Page1.oPag.oCFDOMFIS_1_92.value=this.w_CFDOMFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oCFPRDFIS_1_94.value==this.w_CFPRDFIS)
      this.oPgFrm.Page1.oPag.oCFPRDFIS_1_94.value=this.w_CFPRDFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oCFCAPFIS_1_96.value==this.w_CFCAPFIS)
      this.oPgFrm.Page1.oPag.oCFCAPFIS_1_96.value=this.w_CFCAPFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oCFINDIRI_1_99.value==this.w_CFINDIRI)
      this.oPgFrm.Page1.oPag.oCFINDIRI_1_99.value=this.w_CFINDIRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCFLOCALI_1_101.value==this.w_CFLOCALI)
      this.oPgFrm.Page1.oPag.oCFLOCALI_1_101.value=this.w_CFLOCALI
    endif
    if not(this.oPgFrm.Page1.oPag.oCFPROVIN_1_102.value==this.w_CFPROVIN)
      this.oPgFrm.Page1.oPag.oCFPROVIN_1_102.value=this.w_CFPROVIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCFCAPCON_1_103.value==this.w_CFCAPCON)
      this.oPgFrm.Page1.oPag.oCFCAPCON_1_103.value=this.w_CFCAPCON
    endif
    if not(this.oPgFrm.Page1.oPag.oCFTELCON_1_104.value==this.w_CFTELCON)
      this.oPgFrm.Page1.oPag.oCFTELCON_1_104.value=this.w_CFTELCON
    endif
    if not(this.oPgFrm.Page1.oPag.oCFMAIVER_1_105.value==this.w_CFMAIVER)
      this.oPgFrm.Page1.oPag.oCFMAIVER_1_105.value=this.w_CFMAIVER
    endif
    if not(this.oPgFrm.Page1.oPag.oCFCODIDE_1_106.value==this.w_CFCODIDE)
      this.oPgFrm.Page1.oPag.oCFCODIDE_1_106.value=this.w_CFCODIDE
    endif
    if not(this.oPgFrm.Page1.oPag.oCFSECCOD_1_107.value==this.w_CFSECCOD)
      this.oPgFrm.Page1.oPag.oCFSECCOD_1_107.value=this.w_CFSECCOD
    endif
    cp_SetControlsValueExtFlds(this,'MODCPAG')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_CFAGPROV='EE' or LOOKTAB('ANAG_PRO','PRCODPRO','PRCODPRO',.w_CFAGPROV)=.w_CFAGPROV)  and not(.oparentobject.w_MFENTRAT='E')  and (!empty(.w_CFCODAGE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCFAGPROV_1_31.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(iif(EMPTY(.w_CFCODFIS),.T.,iif(.w_CFPERFIS='S',chkcfp(.w_CFCODFIS,'CF'),chkcfp(.w_CFCODFIS,'PI'))))  and not(.oparentobject.w_MFENTRAT='E')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCFCODFIS_1_32.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(iif(EMPTY(.w_CFCODFIS),.T.,iif(.w_CFPERFIS='S',chkcfp(.w_CFCODFIS,'CF'),chkcfp(.w_CFCODFIS,'PI'))))  and not(.oparentobject.w_MFENTRAT<>'E')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCFCODFIS_1_33.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CFPRONAS='EE' or LOOKTAB('ANAG_PRO','PRCODPRO','PRCODPRO',.w_CFPRONAS)=.w_CFPRONAS)  and not(.oparentobject.w_MFENTRAT<>'E')  and (.w_CFPERFIS='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCFPRONAS_1_40.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CFPRONAS='EE' or LOOKTAB('ANAG_PRO','PRCODPRO','PRCODPRO',.w_CFPRONAS)=.w_CFPRONAS)  and not(.oparentobject.w_MFENTRAT='E')  and (.w_CFPERFIS='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCFPRONAS_1_47.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CFPROVIN='EE' or LOOKTAB('ANAG_PRO','PRCODPRO','PRCODPRO',.w_CFPROVIN)=.w_CFPROVIN)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCFPROVIN_1_102.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY (.w_CFTELCON) OR at(' ',alltrim(.w_CFTELCON))=0 and at('/',alltrim(.w_CFTELCON))=0 and at('-',alltrim(.w_CFTELCON))=0)  and not(.oparentobject.w_MFENTRAT<>'E')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCFTELCON_1_104.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non inserire caratteri separatori tra prefisso e numero")
          case   not((not empty(.w_CFSECCOD) and chkcfp(.w_CFSECCOD,'CF'))  or ((not empty(.w_CFSECCOD) and not empty(.w_CFCODIDE)) or empty(.w_CFCODIDE)))  and (.oparentobject.w_MFENTRAT<>'E' OR (  ( .w_CFPERFIS='S' AND  .oparentobject.w_MFCHKFIR=' ' ) OR   ( .w_CFPERFIS='N' AND .oparentobject.w_MFCHKFIR='S' AND .oparentobject.w_MFCARFIR='1' )  AND NOT EMPTY ( .w_CFCODIDE) ))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCFSECCOD_1_107.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione dati altri soggetti: in presenza del codice identificativo, occorre specificare obbligatoriamente il secondo codice fiscale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gscg_acg
      if i_bRes
        .w_RESCHK=0
        .NotifyEvent('ControllaDati')
        if .w_RESCHK<>0
          i_bRes=.f.
        endif
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_COD_SOC = this.w_COD_SOC
    this.o_COD_PRE = this.w_COD_PRE
    this.o_NOME_PER = this.w_NOME_PER
    this.o_INDIRSOC = this.w_INDIRSOC
    this.o_COD_FIS = this.w_COD_FIS
    this.o_NOME_SOC = this.w_NOME_SOC
    this.o_INDIRPER = this.w_INDIRPER
    this.o_COMU_SOC = this.w_COMU_SOC
    this.o_PROV_SOC = this.w_PROV_SOC
    this.o_PROV_PER = this.w_PROV_PER
    this.o_COMU_PER = this.w_COMU_PER
    this.o_PERAZ = this.w_PERAZ
    this.o_SEDI = this.w_SEDI
    this.o_CFCODAGE = this.w_CFCODAGE
    this.o_CFPERFIS = this.w_CFPERFIS
    this.o_DATAPP = this.w_DATAPP
    this.o_SEDILEG = this.w_SEDILEG
    this.o_CFINDFIS = this.w_CFINDFIS
    this.o_CFCODIDE = this.w_CFCODIDE
    return

enddefine

* --- Define pages as container
define class tgscg_acgPag1 as StdContainer
  Width  = 849
  height = 358
  stdWidth  = 849
  stdheight = 358
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCFDELEGT_1_28 as StdField with uid="GSTRGJXMCG",rtseq=28,rtrep=.f.,;
    cFormVar = "w_CFDELEGT", cQueryName = "CFDELEGT",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 209754758,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=178, Top=7, InputMask=replicate('X',50)

  func oCFDELEGT_1_28.mHide()
    with this.Parent.oContained
      return (g_TESO<>'S' OR .oparentobject.w_MFENTRAT='E')
    endwith
  endfunc

  add object oCFDELEGB_1_29 as StdField with uid="ZKGPXZIFZP",rtseq=29,rtrep=.f.,;
    cFormVar = "w_CFDELEGB", cQueryName = "CFDELEGB",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 209754776,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=178, Top=7, cSayPict='repl("!",50)', cGetPict='repl("!",50)', InputMask=replicate('X',50)

  func oCFDELEGB_1_29.mHide()
    with this.Parent.oContained
      return (g_TESO='S' OR .oparentobject.w_MFENTRAT='E')
    endwith
  endfunc

  add object oCFCODAGE_1_30 as StdField with uid="NYMGDMOIWI",rtseq=30,rtrep=.f.,;
    cFormVar = "w_CFCODAGE", cQueryName = "CFCODAGE",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Nome dell'agenzia di riferimento",;
    HelpContextID = 16165525,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=178, Top=31, cSayPict='repl("!",40)', cGetPict='repl("!",40)', InputMask=replicate('X',40)

  func oCFCODAGE_1_30.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT='E')
    endwith
  endfunc

  add object oCFAGPROV_1_31 as StdField with uid="KTITZQKIXI",rtseq=31,rtrep=.f.,;
    cFormVar = "w_CFAGPROV", cQueryName = "CFAGPROV",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia",;
    HelpContextID = 12662140,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=506, Top=31, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. 

  func oCFAGPROV_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_CFCODAGE))
    endwith
   endif
  endfunc

  func oCFAGPROV_1_31.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT='E')
    endwith
  endfunc

  func oCFAGPROV_1_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CFAGPROV='EE' or LOOKTAB('ANAG_PRO','PRCODPRO','PRCODPRO',.w_CFAGPROV)=.w_CFAGPROV)
    endwith
    return bRes
  endfunc

  proc oCFAGPROV_1_31.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_CFAGPROV")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oCFCODFIS_1_32 as StdField with uid="OQOQLRIUDC",rtseq=32,rtrep=.f.,;
    cFormVar = "w_CFCODFIS", cQueryName = "CFCODFIS",;
    bObbl = .f. , nPag = 1, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale dell'azienda",;
    HelpContextID = 200714887,;
   bGlobalFont=.t.,;
    Height=21, Width=136, Left=148, Top=110, cSayPict='repl("!",20)', cGetPict='repl("!",20)', InputMask=replicate('X',16)

  func oCFCODFIS_1_32.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT='E')
    endwith
  endfunc

  func oCFCODFIS_1_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(EMPTY(.w_CFCODFIS),.T.,iif(.w_CFPERFIS='S',chkcfp(.w_CFCODFIS,'CF'),chkcfp(.w_CFCODFIS,'PI'))))
    endwith
    return bRes
  endfunc

  add object oCFCODFIS_1_33 as StdField with uid="HADDVWZVKM",rtseq=33,rtrep=.f.,;
    cFormVar = "w_CFCODFIS", cQueryName = "CFCODFIS",;
    bObbl = .f. , nPag = 1, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale dell'azienda",;
    HelpContextID = 200714887,;
   bGlobalFont=.t.,;
    Height=21, Width=136, Left=148, Top=52, cSayPict='repl("!",20)', cGetPict='repl("!",20)', InputMask=replicate('X',16)

  func oCFCODFIS_1_33.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT<>'E')
    endwith
  endfunc

  func oCFCODFIS_1_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(EMPTY(.w_CFCODFIS),.T.,iif(.w_CFPERFIS='S',chkcfp(.w_CFCODFIS,'CF'),chkcfp(.w_CFCODFIS,'PI'))))
    endwith
    return bRes
  endfunc

  add object oCFPERFIS_1_34 as StdRadio with uid="DFKELAQCOQ",rtseq=34,rtrep=.f.,left=392, top=46, width=205,height=34;
    , cFormVar="w_CFPERFIS", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oCFPERFIS_1_34.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Persona fisica"
      this.Buttons(1).HelpContextID = 186636935
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Societ�"
      this.Buttons(2).HelpContextID = 186636935
      this.Buttons(2).Top=16
      this.SetAll("Width",203)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oCFPERFIS_1_34.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oCFPERFIS_1_34.GetRadio()
    this.Parent.oContained.w_CFPERFIS = this.RadioValue()
    return .t.
  endfunc

  func oCFPERFIS_1_34.SetRadio()
    this.Parent.oContained.w_CFPERFIS=trim(this.Parent.oContained.w_CFPERFIS)
    this.value = ;
      iif(this.Parent.oContained.w_CFPERFIS=='S',1,;
      iif(this.Parent.oContained.w_CFPERFIS=='N',2,;
      0))
  endfunc

  func oCFPERFIS_1_34.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT<>'E')
    endwith
  endfunc

  add object oCFRAGSOC_1_35 as StdField with uid="UDUMBGVJPG",rtseq=35,rtrep=.f.,;
    cFormVar = "w_CFRAGSOC", cQueryName = "CFRAGSOC",;
    bObbl = .f. , nPag = 1, value=space(80), bMultilanguage =  .f.,;
    ToolTipText = "Ragione sociale dell'azienda",;
    HelpContextID = 19678569,;
   bGlobalFont=.t.,;
    Height=21, Width=525, Left=148, Top=81, cSayPict='repl("!",80)', cGetPict='repl("!",80)', InputMask=replicate('X',80)

  func oCFRAGSOC_1_35.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT<>'E')
    endwith
  endfunc

  add object oCF__NOME_1_36 as StdField with uid="XNOWYOLCIM",rtseq=36,rtrep=.f.,;
    cFormVar = "w_CF__NOME", cQueryName = "CF__NOME",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Nome del titolare in caso di persona fisica",;
    HelpContextID = 38070933,;
   bGlobalFont=.t.,;
    Height=21, Width=208, Left=148, Top=109, cSayPict='repl("!",25)', cGetPict='repl("!",25)', InputMask=replicate('X',25)

  func oCF__NOME_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFPERFIS='S')
    endwith
   endif
  endfunc

  func oCF__NOME_1_36.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT<>'E')
    endwith
  endfunc


  add object oCF_SESSO_1_37 as StdCombo with uid="XAFYAHYJLV",rtseq=37,rtrep=.f.,left=439,top=109,width=110,height=21;
    , ToolTipText = "Sesso del titolare in caso di persona fisica";
    , HelpContextID = 18814325;
    , cFormVar="w_CF_SESSO",RowSource=""+"Maschio,"+"Femmina", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCF_SESSO_1_37.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oCF_SESSO_1_37.GetRadio()
    this.Parent.oContained.w_CF_SESSO = this.RadioValue()
    return .t.
  endfunc

  func oCF_SESSO_1_37.SetRadio()
    this.Parent.oContained.w_CF_SESSO=trim(this.Parent.oContained.w_CF_SESSO)
    this.value = ;
      iif(this.Parent.oContained.w_CF_SESSO=='M',1,;
      iif(this.Parent.oContained.w_CF_SESSO=='F',2,;
      0))
  endfunc

  func oCF_SESSO_1_37.mHide()
    with this.Parent.oContained
      return (.w_CFPERFIS='N' OR .oparentobject.w_MFENTRAT<>'E')
    endwith
  endfunc

  add object oCFDATNAS_1_38 as StdField with uid="ZJKBIQTXWV",rtseq=38,rtrep=.f.,;
    cFormVar = "w_CFDATNAS", cQueryName = "CFDATNAS",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita del titolare in caso di persona fisica",;
    HelpContextID = 217802105,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=148, Top=143

  func oCFDATNAS_1_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFPERFIS='S' )
    endwith
   endif
  endfunc

  func oCFDATNAS_1_38.mHide()
    with this.Parent.oContained
      return ( .oparentobject.w_MFENTRAT<>'E')
    endwith
  endfunc

  add object oCFLOCNAS_1_39 as StdField with uid="OKBVFPJIIP",rtseq=39,rtrep=.f.,;
    cFormVar = "w_CFLOCNAS", cQueryName = "CFLOCNAS",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Luogo di nascita del titolare in caso di persona fisica",;
    HelpContextID = 200926585,;
   bGlobalFont=.t.,;
    Height=21, Width=248, Left=339, Top=143, cSayPict='repl("!",30)', cGetPict='repl("!",30)', InputMask=replicate('X',30)

  func oCFLOCNAS_1_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFPERFIS='S')
    endwith
   endif
  endfunc

  func oCFLOCNAS_1_39.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT<>'E')
    endwith
  endfunc

  add object oCFPRONAS_1_40 as StdField with uid="APOJQXAUAT",rtseq=40,rtrep=.f.,;
    cFormVar = "w_CFPRONAS", cQueryName = "CFPRONAS",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia",;
    HelpContextID = 213722489,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=634, Top=143, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. 

  func oCFPRONAS_1_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFPERFIS='S')
    endwith
   endif
  endfunc

  func oCFPRONAS_1_40.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT<>'E')
    endwith
  endfunc

  func oCFPRONAS_1_40.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CFPRONAS='EE' or LOOKTAB('ANAG_PRO','PRCODPRO','PRCODPRO',.w_CFPRONAS)=.w_CFPRONAS)
    endwith
    return bRes
  endfunc

  proc oCFPRONAS_1_40.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_CFPRONAS")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oCF__NOME_1_41 as StdField with uid="OHKKXDENHK",rtseq=41,rtrep=.f.,;
    cFormVar = "w_CF__NOME", cQueryName = "CF__NOME",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Nome del titolare in caso di persona fisica",;
    HelpContextID = 38070933,;
   bGlobalFont=.t.,;
    Height=21, Width=208, Left=148, Top=167, cSayPict='repl("!",25)', cGetPict='repl("!",25)', InputMask=replicate('X',25)

  func oCF__NOME_1_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFPERFIS='S')
    endwith
   endif
  endfunc

  func oCF__NOME_1_41.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT='E')
    endwith
  endfunc

  add object oCFPERFIS_1_42 as StdRadio with uid="ZRRZQSXKBU",rtseq=42,rtrep=.f.,left=392, top=104, width=205,height=34;
    , cFormVar="w_CFPERFIS", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oCFPERFIS_1_42.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Persona fisica"
      this.Buttons(1).HelpContextID = 186636935
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Societ�"
      this.Buttons(2).HelpContextID = 186636935
      this.Buttons(2).Top=16
      this.SetAll("Width",203)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oCFPERFIS_1_42.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oCFPERFIS_1_42.GetRadio()
    this.Parent.oContained.w_CFPERFIS = this.RadioValue()
    return .t.
  endfunc

  func oCFPERFIS_1_42.SetRadio()
    this.Parent.oContained.w_CFPERFIS=trim(this.Parent.oContained.w_CFPERFIS)
    this.value = ;
      iif(this.Parent.oContained.w_CFPERFIS=='S',1,;
      iif(this.Parent.oContained.w_CFPERFIS=='N',2,;
      0))
  endfunc

  func oCFPERFIS_1_42.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT='E')
    endwith
  endfunc

  add object oCFRAGSOC_1_43 as StdField with uid="QZOCJXVTIQ",rtseq=43,rtrep=.f.,;
    cFormVar = "w_CFRAGSOC", cQueryName = "CFRAGSOC",;
    bObbl = .f. , nPag = 1, value=space(80), bMultilanguage =  .f.,;
    ToolTipText = "Ragione sociale dell'azienda",;
    HelpContextID = 19678569,;
   bGlobalFont=.t.,;
    Height=21, Width=525, Left=148, Top=139, cSayPict='repl("!",80)', cGetPict='repl("!",80)', InputMask=replicate('X',80)

  func oCFRAGSOC_1_43.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT='E')
    endwith
  endfunc


  add object oCF_SESSO_1_44 as StdCombo with uid="YQOYAUSGMU",rtseq=44,rtrep=.f.,left=439,top=167,width=110,height=21;
    , ToolTipText = "Sesso del titolare in caso di persona fisica";
    , HelpContextID = 18814325;
    , cFormVar="w_CF_SESSO",RowSource=""+"Maschio,"+"Femmina", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCF_SESSO_1_44.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oCF_SESSO_1_44.GetRadio()
    this.Parent.oContained.w_CF_SESSO = this.RadioValue()
    return .t.
  endfunc

  func oCF_SESSO_1_44.SetRadio()
    this.Parent.oContained.w_CF_SESSO=trim(this.Parent.oContained.w_CF_SESSO)
    this.value = ;
      iif(this.Parent.oContained.w_CF_SESSO=='M',1,;
      iif(this.Parent.oContained.w_CF_SESSO=='F',2,;
      0))
  endfunc

  func oCF_SESSO_1_44.mHide()
    with this.Parent.oContained
      return (.w_CFPERFIS='N' OR .oparentobject.w_MFENTRAT='E')
    endwith
  endfunc

  add object oCFDATNAS_1_45 as StdField with uid="ZKMJUZHLUD",rtseq=45,rtrep=.f.,;
    cFormVar = "w_CFDATNAS", cQueryName = "CFDATNAS",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita del titolare in caso di persona fisica",;
    HelpContextID = 217802105,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=148, Top=201

  func oCFDATNAS_1_45.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFPERFIS='S')
    endwith
   endif
  endfunc

  func oCFDATNAS_1_45.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT='E')
    endwith
  endfunc

  add object oCFLOCNAS_1_46 as StdField with uid="OJHABIUMBJ",rtseq=46,rtrep=.f.,;
    cFormVar = "w_CFLOCNAS", cQueryName = "CFLOCNAS",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Luogo di nascita del titolare in caso di persona fisica",;
    HelpContextID = 200926585,;
   bGlobalFont=.t.,;
    Height=21, Width=248, Left=339, Top=201, cSayPict='repl("!",30)', cGetPict='repl("!",30)', InputMask=replicate('X',30)

  func oCFLOCNAS_1_46.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFPERFIS='S')
    endwith
   endif
  endfunc

  func oCFLOCNAS_1_46.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT='E')
    endwith
  endfunc

  add object oCFPRONAS_1_47 as StdField with uid="OJSFKTMRTI",rtseq=47,rtrep=.f.,;
    cFormVar = "w_CFPRONAS", cQueryName = "CFPRONAS",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia",;
    HelpContextID = 213722489,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=634, Top=201, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. 

  func oCFPRONAS_1_47.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFPERFIS='S')
    endwith
   endif
  endfunc

  func oCFPRONAS_1_47.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT='E')
    endwith
  endfunc

  func oCFPRONAS_1_47.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CFPRONAS='EE' or LOOKTAB('ANAG_PRO','PRCODPRO','PRCODPRO',.w_CFPRONAS)=.w_CFPRONAS)
    endwith
    return bRes
  endfunc

  proc oCFPRONAS_1_47.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_CFPRONAS")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc


  add object oObj_1_65 as cp_runprogram with uid="IMVHWAQFRK",left=605, top=396, width=132,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSTE_BFC('DC')",;
    cEvent = "ControllaDati",;
    nPag=1;
    , HelpContextID = 68280806

  add object oCFINDFIS_1_91 as StdField with uid="PAXCUXSDXG",rtseq=55,rtrep=.f.,;
    cFormVar = "w_CFINDFIS", cQueryName = "CFINDFIS",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo della sede legale",;
    HelpContextID = 200755847,;
   bGlobalFont=.t.,;
    Height=21, Width=248, Left=78, Top=201, cSayPict='repl("!",35)', cGetPict='repl("!",35)', InputMask=replicate('X',35)

  func oCFINDFIS_1_91.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFPERFIS='N' )
    endwith
   endif
  endfunc

  func oCFINDFIS_1_91.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT<>'E')
    endwith
  endfunc

  add object oCFDOMFIS_1_92 as StdField with uid="LCRXSKTRYW",rtseq=56,rtrep=.f.,;
    cFormVar = "w_CFDOMFIS", cQueryName = "CFDOMFIS",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune della sede legale",;
    HelpContextID = 191273607,;
   bGlobalFont=.t.,;
    Height=21, Width=248, Left=401, Top=201, cSayPict='repl("!",40)', cGetPict='repl("!",40)', InputMask=replicate('X',40)

  func oCFDOMFIS_1_92.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFPERFIS='N')
    endwith
   endif
  endfunc

  func oCFDOMFIS_1_92.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT<>'E')
    endwith
  endfunc

  add object oCFPRDFIS_1_94 as StdField with uid="ZGSOQWAYTA",rtseq=57,rtrep=.f.,;
    cFormVar = "w_CFPRDFIS", cQueryName = "CFPRDFIS",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia della sede legale",;
    HelpContextID = 200465031,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=692, Top=201, cSayPict='repl("!",2)', cGetPict='repl("!",2)', InputMask=replicate('X',2), bHasZoom = .t. 

  func oCFPRDFIS_1_94.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFPERFIS='N' )
    endwith
   endif
  endfunc

  func oCFPRDFIS_1_94.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT<>'E')
    endwith
  endfunc

  proc oCFPRDFIS_1_94.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_CFPRDFIS")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oCFCAPFIS_1_96 as StdField with uid="MOBWPGOMRL",rtseq=58,rtrep=.f.,;
    cFormVar = "w_CFCAPFIS", cQueryName = "CFCAPFIS",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "CAP della sede legale",;
    HelpContextID = 189049479,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=780, Top=201, cSayPict='repl("!",5)', cGetPict='repl("!",5)', InputMask=replicate('X',5)

  func oCFCAPFIS_1_96.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFPERFIS='N' )
    endwith
   endif
  endfunc

  func oCFCAPFIS_1_96.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT<>'E')
    endwith
  endfunc

  add object oCFINDIRI_1_99 as StdField with uid="TSCKIMKLHK",rtseq=59,rtrep=.f.,;
    cFormVar = "w_CFINDIRI", cQueryName = "CFINDIRI",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo del domicilio fiscale azienda (se assente, indirizzo principale azienda)",;
    HelpContextID = 118011247,;
   bGlobalFont=.t.,;
    Height=21, Width=248, Left=78, Top=254, cSayPict='repl("!",30)', cGetPict='repl("!",30)', InputMask=replicate('X',30)

  add object oCFLOCALI_1_101 as StdField with uid="MCBXRCTIER",rtseq=60,rtrep=.f.,;
    cFormVar = "w_CFLOCALI", cQueryName = "CFLOCALI",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Comune del domicilio fiscale azienda (se assente, comune dell'indirizzo principale azienda)",;
    HelpContextID = 17177233,;
   bGlobalFont=.t.,;
    Height=21, Width=248, Left=401, Top=254, cSayPict='repl("!",30)', cGetPict='repl("!",30)', InputMask=replicate('X',30)

  add object oCFPROVIN_1_102 as StdField with uid="TNTIRIDZXM",rtseq=61,rtrep=.f.,;
    cFormVar = "w_CFPROVIN", cQueryName = "CFPROVIN",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia",;
    HelpContextID = 188930700,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=692, Top=254, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. 

  func oCFPROVIN_1_102.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CFPROVIN='EE' or LOOKTAB('ANAG_PRO','PRCODPRO','PRCODPRO',.w_CFPROVIN)=.w_CFPROVIN)
    endwith
    return bRes
  endfunc

  proc oCFPROVIN_1_102.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_CFPROVIN")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oCFCAPCON_1_103 as StdField with uid="BSQTQLRUAW",rtseq=62,rtrep=.f.,;
    cFormVar = "w_CFCAPCON", cQueryName = "CFCAPCON",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "CAP del contribuente",;
    HelpContextID = 29054324,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=780, Top=252, cSayPict="'99999'", cGetPict="'99999'", InputMask=replicate('X',5)

  func oCFCAPCON_1_103.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT<>'E')
    endwith
  endfunc

  add object oCFTELCON_1_104 as StdField with uid="OZZMKEQFZZ",rtseq=63,rtrep=.f.,;
    cFormVar = "w_CFTELCON", cQueryName = "CFTELCON",;
    bObbl = .f. , nPag = 1, value=space(12), bMultilanguage =  .f.,;
    sErrorMsg = "Non inserire caratteri separatori tra prefisso e numero",;
    ToolTipText = "Telefono del Contribuente",;
    HelpContextID = 25191796,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=78, Top=279, cSayPict="repl('!',12)", cGetPict="repl('!',12)", InputMask=replicate('X',12)

  func oCFTELCON_1_104.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT<>'E')
    endwith
  endfunc

  func oCFTELCON_1_104.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY (.w_CFTELCON) OR at(' ',alltrim(.w_CFTELCON))=0 and at('/',alltrim(.w_CFTELCON))=0 and at('-',alltrim(.w_CFTELCON))=0)
    endwith
    return bRes
  endfunc

  add object oCFMAIVER_1_105 as StdField with uid="PAHSGYGHXO",rtseq=64,rtrep=.f.,;
    cFormVar = "w_CFMAIVER", cQueryName = "CFMAIVER",;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo di posta elettronica del contribuente",;
    HelpContextID = 196348552,;
   bGlobalFont=.t.,;
    Height=21, Width=427, Left=401, Top=279, cSayPict='repl("!",60)', cGetPict='repl("!",60)', InputMask=replicate('X',60)

  func oCFMAIVER_1_105.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT<>'E')
    endwith
  endfunc

  add object oCFCODIDE_1_106 as StdField with uid="SLDZLOCJNP",rtseq=65,rtrep=.f.,;
    cFormVar = "w_CFCODIDE", cQueryName = "CFCODIDE",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione dati altri soggetti: in presenza del secondo codice fiscale, occorre specificare obbligatoriamente il codice identificativo",;
    ToolTipText = "E' il codice identificativo del coobbligato, erede, genitore, tutore o curatore fallimentare",;
    HelpContextID = 118052203,;
   bGlobalFont=.t.,;
    Height=21, Width=26, Left=137, Top=327, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oCFCODIDE_1_106.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT<>'E' OR  ( .w_CFPERFIS='S'  AND  .oparentobject.w_MFCHKFIR=' ' ) OR   ( .w_CFPERFIS='N' AND .oparentobject.w_MFCHKFIR='S' AND .oparentobject.w_MFCARFIR='1' ) )
    endwith
   endif
  endfunc

  add object oCFSECCOD_1_107 as StdField with uid="GVYIGOFFJV",rtseq=66,rtrep=.f.,;
    cFormVar = "w_CFSECCOD", cQueryName = "CFSECCOD",;
    bObbl = .f. , nPag = 1, value=space(16), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione dati altri soggetti: in presenza del codice identificativo, occorre specificare obbligatoriamente il secondo codice fiscale",;
    ToolTipText = "E' il codice fiscale del coobbligato, erede, genitore, tutore o curatore fallimentare ecc.",;
    HelpContextID = 15750506,;
   bGlobalFont=.t.,;
    Height=21, Width=150, Left=390, Top=327, cSayPict="repl('!',16)", cGetPict="repl('!',16)", InputMask=replicate('X',16)

  func oCFSECCOD_1_107.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT<>'E' OR (  ( .w_CFPERFIS='S' AND  .oparentobject.w_MFCHKFIR=' ' ) OR   ( .w_CFPERFIS='N' AND .oparentobject.w_MFCHKFIR='S' AND .oparentobject.w_MFCARFIR='1' )  AND NOT EMPTY ( .w_CFCODIDE) ))
    endwith
   endif
  endfunc

  func oCFSECCOD_1_107.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((not empty(.w_CFSECCOD) and chkcfp(.w_CFSECCOD,'CF'))  or ((not empty(.w_CFSECCOD) and not empty(.w_CFCODIDE)) or empty(.w_CFCODIDE)))
    endwith
    return bRes
  endfunc


  add object oBtn_1_125 as StdButton with uid="GJQKCBDMMT",left=555, top=309, width=48,height=45,;
    CpPicture="bmp\manuale.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere ai dati di dettaglio del terzo datore";
    , HelpContextID = 102334410;
    , TabStop=.f., Caption='\<Terzo Dat.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_125.Click()
      do gscg_kcg with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_125.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CFCODIDE='60' AND NOT EMPTY (.w_CFSECCOD) )
      endwith
    endif
  endfunc

  func oBtn_1_125.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT<>'E')
     endwith
    endif
  endfunc

  add object oStr_1_48 as StdString with uid="UTQYIKDWQR",Visible=.t., Left=10, Top=141,;
    Alignment=1, Width=134, Height=15,;
    Caption="Rag.soc/cognome:"  ;
  , bGlobalFont=.t.

  func oStr_1_48.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT='E')
    endwith
  endfunc

  add object oStr_1_49 as StdString with uid="OCNBQAJRIU",Visible=.t., Left=44, Top=167,;
    Alignment=1, Width=101, Height=15,;
    Caption="Nome:"  ;
  , bGlobalFont=.t.

  func oStr_1_49.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT='E')
    endwith
  endfunc

  add object oStr_1_50 as StdString with uid="PNYMIDQAZR",Visible=.t., Left=11, Top=7,;
    Alignment=1, Width=163, Height=15,;
    Caption="Delega irrevocabile a:"  ;
  , bGlobalFont=.t.

  func oStr_1_50.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT='E')
    endwith
  endfunc

  add object oStr_1_51 as StdString with uid="TWXOFZNBWK",Visible=.t., Left=96, Top=31,;
    Alignment=1, Width=78, Height=15,;
    Caption="Agenzia:"  ;
  , bGlobalFont=.t.

  func oStr_1_51.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT='E')
    endwith
  endfunc

  add object oStr_1_52 as StdString with uid="PKMBKAAABE",Visible=.t., Left=474, Top=31,;
    Alignment=1, Width=30, Height=15,;
    Caption="Prov.:"  ;
  , bGlobalFont=.t.

  func oStr_1_52.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT='E')
    endwith
  endfunc

  add object oStr_1_53 as StdString with uid="CLLGBCCFFF",Visible=.t., Left=69, Top=59,;
    Alignment=0, Width=319, Height=15,;
    Caption="per l'accredito alla tesoreria competente."  ;
  , bGlobalFont=.t.

  func oStr_1_53.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT='E')
    endwith
  endfunc

  add object oStr_1_54 as StdString with uid="BEDGEVXSCJ",Visible=.t., Left=44, Top=203,;
    Alignment=1, Width=101, Height=15,;
    Caption="Data di nascita:"  ;
  , bGlobalFont=.t.

  func oStr_1_54.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT='E')
    endwith
  endfunc

  add object oStr_1_55 as StdString with uid="RQOMMJHESN",Visible=.t., Left=228, Top=203,;
    Alignment=1, Width=107, Height=15,;
    Caption="Luogo di nascita:"  ;
  , bGlobalFont=.t.

  func oStr_1_55.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT='E')
    endwith
  endfunc

  add object oStr_1_56 as StdString with uid="VBDRBEUWDW",Visible=.t., Left=601, Top=204,;
    Alignment=1, Width=30, Height=15,;
    Caption="Prov.:"  ;
  , bGlobalFont=.t.

  func oStr_1_56.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT='E')
    endwith
  endfunc

  add object oStr_1_58 as StdString with uid="QXEIBFXISO",Visible=.t., Left=333, Top=256,;
    Alignment=1, Width=64, Height=15,;
    Caption="Comune:"  ;
  , bGlobalFont=.t.

  add object oStr_1_59 as StdString with uid="VBCZZFZNIW",Visible=.t., Left=17, Top=254,;
    Alignment=1, Width=56, Height=15,;
    Caption="Indirizzo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_60 as StdString with uid="OVBXHFPXVC",Visible=.t., Left=659, Top=256,;
    Alignment=1, Width=30, Height=15,;
    Caption="Prov.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="ZLWBMYUWHU",Visible=.t., Left=44, Top=110,;
    Alignment=1, Width=101, Height=15,;
    Caption="Codice fiscale:"  ;
  , bGlobalFont=.t.

  func oStr_1_61.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT='E')
    endwith
  endfunc

  add object oStr_1_62 as StdString with uid="SCGDXRHZHJ",Visible=.t., Left=376, Top=167,;
    Alignment=1, Width=55, Height=15,;
    Caption="Sesso:"  ;
  , bGlobalFont=.t.

  func oStr_1_62.mHide()
    with this.Parent.oContained
      return (.w_CFPERFIS='N' OR .oparentobject.w_MFENTRAT='E')
    endwith
  endfunc

  add object oStr_1_69 as StdString with uid="BIWJFKEAUC",Visible=.t., Left=17, Top=298,;
    Alignment=0, Width=141, Height=18,;
    Caption="Dati altri soggetti"  ;
  , bGlobalFont=.t.

  add object oStr_1_71 as StdString with uid="CPDLFGWXVK",Visible=.t., Left=13, Top=329,;
    Alignment=1, Width=121, Height=18,;
    Caption="Codice identificativo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_72 as StdString with uid="MHKZNZPBNA",Visible=.t., Left=224, Top=329,;
    Alignment=1, Width=162, Height=18,;
    Caption="Secondo codice fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_73 as StdString with uid="NBSTCOKDWM",Visible=.t., Left=740, Top=256,;
    Alignment=1, Width=40, Height=18,;
    Caption="CAP: "  ;
  , bGlobalFont=.t.

  func oStr_1_73.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT<>'E')
    endwith
  endfunc

  add object oStr_1_74 as StdString with uid="KFUHCETOGV",Visible=.t., Left=6, Top=283,;
    Alignment=1, Width=70, Height=18,;
    Caption="Telefono: "  ;
  , bGlobalFont=.t.

  func oStr_1_74.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT<>'E')
    endwith
  endfunc

  add object oStr_1_76 as StdString with uid="BGNBNAAQGE",Visible=.t., Left=3, Top=227,;
    Alignment=0, Width=149, Height=18,;
    Caption="Domicilio fiscale"  ;
  , bGlobalFont=.t.

  add object oStr_1_77 as StdString with uid="TZHFYCNCGJ",Visible=.t., Left=6, Top=24,;
    Alignment=0, Width=839, Height=18,;
    Caption="Contribuente                                                                                                                                    "  ;
  , bGlobalFont=.t.

  func oStr_1_77.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT<>'E')
    endwith
  endfunc

  add object oStr_1_78 as StdString with uid="QXTOTQOMJF",Visible=.t., Left=376, Top=109,;
    Alignment=1, Width=55, Height=15,;
    Caption="Sesso:"  ;
  , bGlobalFont=.t.

  func oStr_1_78.mHide()
    with this.Parent.oContained
      return (.w_CFPERFIS='N' OR .oparentobject.w_MFENTRAT<>'E')
    endwith
  endfunc

  add object oStr_1_79 as StdString with uid="WJCWRKBFKV",Visible=.t., Left=44, Top=52,;
    Alignment=1, Width=101, Height=15,;
    Caption="Codice fiscale:"  ;
  , bGlobalFont=.t.

  func oStr_1_79.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT<>'E')
    endwith
  endfunc

  add object oStr_1_80 as StdString with uid="SLIAHMCMNK",Visible=.t., Left=600, Top=145,;
    Alignment=1, Width=30, Height=15,;
    Caption="Prov.:"  ;
  , bGlobalFont=.t.

  func oStr_1_80.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT<>'E')
    endwith
  endfunc

  add object oStr_1_81 as StdString with uid="WSWGISDNML",Visible=.t., Left=228, Top=145,;
    Alignment=1, Width=107, Height=15,;
    Caption="Luogo di nascita:"  ;
  , bGlobalFont=.t.

  func oStr_1_81.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT<>'E')
    endwith
  endfunc

  add object oStr_1_82 as StdString with uid="UVQUWJBAOH",Visible=.t., Left=44, Top=145,;
    Alignment=1, Width=101, Height=15,;
    Caption="Data di nascita:"  ;
  , bGlobalFont=.t.

  func oStr_1_82.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT<>'E')
    endwith
  endfunc

  add object oStr_1_83 as StdString with uid="WJGOSQBCAA",Visible=.t., Left=44, Top=109,;
    Alignment=1, Width=101, Height=15,;
    Caption="Nome:"  ;
  , bGlobalFont=.t.

  func oStr_1_83.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT<>'E')
    endwith
  endfunc

  add object oStr_1_84 as StdString with uid="NBSUKJIEJO",Visible=.t., Left=11, Top=83,;
    Alignment=1, Width=134, Height=15,;
    Caption="Rag.soc/cognome:"  ;
  , bGlobalFont=.t.

  func oStr_1_84.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT<>'E')
    endwith
  endfunc

  add object oStr_1_85 as StdString with uid="COPPLWVJEK",Visible=.t., Left=13, Top=201,;
    Alignment=1, Width=60, Height=18,;
    Caption="Indirizzo: "  ;
  , bGlobalFont=.t.

  func oStr_1_85.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT<>'E')
    endwith
  endfunc

  add object oStr_1_86 as StdString with uid="DOYRKWNURL",Visible=.t., Left=744, Top=201,;
    Alignment=1, Width=36, Height=18,;
    Caption="CAP: "  ;
  , bGlobalFont=.t.

  func oStr_1_86.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT<>'E')
    endwith
  endfunc

  add object oStr_1_87 as StdString with uid="TUSPQMZWKV",Visible=.t., Left=643, Top=201,;
    Alignment=1, Width=46, Height=18,;
    Caption="Prov.: "  ;
  , bGlobalFont=.t.

  func oStr_1_87.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT<>'E')
    endwith
  endfunc

  add object oStr_1_88 as StdString with uid="SQDPTIFTNP",Visible=.t., Left=335, Top=201,;
    Alignment=1, Width=62, Height=18,;
    Caption="Comune: "  ;
  , bGlobalFont=.t.

  func oStr_1_88.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT<>'E')
    endwith
  endfunc

  add object oStr_1_93 as StdString with uid="LGGMPFORXT",Visible=.t., Left=437, Top=24,;
    Alignment=0, Width=404, Height=18,;
    Caption="                                                                                                                                        "  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.t., FontStrikeThru=.f.

  func oStr_1_93.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT<>'E')
    endwith
  endfunc

  add object oStr_1_95 as StdString with uid="UUKKFRTVJR",Visible=.t., Left=5, Top=82,;
    Alignment=0, Width=839, Height=18,;
    Caption="Contribuente                                                                                                                                    "  ;
  , bGlobalFont=.t.

  func oStr_1_95.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT='E')
    endwith
  endfunc

  add object oStr_1_97 as StdString with uid="FEYWDSZBLB",Visible=.t., Left=436, Top=82,;
    Alignment=0, Width=401, Height=18,;
    Caption="                                                                                                                                        "  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.t., FontStrikeThru=.f.

  func oStr_1_97.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT='E')
    endwith
  endfunc

  add object oStr_1_98 as StdString with uid="CAWCHFWFNG",Visible=.t., Left=6, Top=179,;
    Alignment=0, Width=839, Height=18,;
    Caption="Sede legale                                                                                                                                   "  ;
  , bGlobalFont=.t.

  func oStr_1_98.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT<>'E')
    endwith
  endfunc

  add object oStr_1_100 as StdString with uid="LLXOHNUUPT",Visible=.t., Left=437, Top=179,;
    Alignment=0, Width=400, Height=18,;
    Caption="                                                                                                                                        "  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.t., FontStrikeThru=.f.

  func oStr_1_100.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT<>'E')
    endwith
  endfunc

  add object oStr_1_128 as StdString with uid="OPLZGITBIW",Visible=.t., Left=237, Top=279,;
    Alignment=1, Width=160, Height=18,;
    Caption="Indirizzo di posta elettronica:"  ;
  , bGlobalFont=.t.

  func oStr_1_128.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT<>'E')
    endwith
  endfunc

  add object oStr_1_131 as StdString with uid="SGMGAPWWJN",Visible=.t., Left=9, Top=24,;
    Alignment=0, Width=498, Height=18,;
    Caption="                                                                                                                                                                      "  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.t., FontStrikeThru=.f.

  func oStr_1_131.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT<>'E')
    endwith
  endfunc

  add object oStr_1_132 as StdString with uid="XVSUATVIUF",Visible=.t., Left=9, Top=82,;
    Alignment=0, Width=498, Height=18,;
    Caption="                                                                                                                                                                      "  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.t., FontStrikeThru=.f.

  func oStr_1_132.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT='E')
    endwith
  endfunc

  add object oStr_1_133 as StdString with uid="ZULIXBVVOP",Visible=.t., Left=9, Top=179,;
    Alignment=0, Width=498, Height=18,;
    Caption="                                                                                                                                                                      "  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.t., FontStrikeThru=.f.

  func oStr_1_133.mHide()
    with this.Parent.oContained
      return (.oparentobject.w_MFENTRAT<>'E')
    endwith
  endfunc

  add object oBox_1_57 as StdBox with uid="XIFBMCUPKY",left=9, top=245, width=826,height=1

  add object oBox_1_70 as StdBox with uid="YSFLQHRYQC",left=9, top=316, width=536,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_acg','MODCPAG','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CFSERIAL=MODCPAG.CFSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gscg_acg
Procedure AggiornaTipo (oparent)
 oparent.oparentobject.w_CFPERFIS=oparent.w_CFPERFIS
   IF oparent.w_CFPERFIS='N'
     oparent.oparentobject.w_MFCHKFIR='S'
   ENDIF 
   
oparent.oparentobject.mEnableControls()
oparent.oparentobject.mcalc(.t.)
ENDPROC
* --- Fine Area Manuale
