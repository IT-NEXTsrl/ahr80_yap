* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsca_bsu                                                        *
*              Batch per stampe                                                *
*                                                                              *
*      Author: ZUCCHETTI SPA: CS                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-07-13                                                      *
* Last revis.: 2010-07-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCOMPET,pDATA1,pDATA2,pFLGBUDG,pSCRASS,pFLGCON,pFLGDIRRIP,pFLGDARIP,pFLPROV,pCODBUN,pCDC1,pCDC2,pVOCE1,pVOCE2,pCOMM1,pCOMM2,pLIVELLO1,pLIVELLO2,pFLGCOMP,pFLGMOV,pFLGPRINT,pSEZBIL,pFLGORI,pESSTMORI
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsca_bsu",oParentObject,m.pCOMPET,m.pDATA1,m.pDATA2,m.pFLGBUDG,m.pSCRASS,m.pFLGCON,m.pFLGDIRRIP,m.pFLGDARIP,m.pFLPROV,m.pCODBUN,m.pCDC1,m.pCDC2,m.pVOCE1,m.pVOCE2,m.pCOMM1,m.pCOMM2,m.pLIVELLO1,m.pLIVELLO2,m.pFLGCOMP,m.pFLGMOV,m.pFLGPRINT,m.pSEZBIL,m.pFLGORI,m.pESSTMORI)
return(i_retval)

define class tgsca_bsu as StdBatch
  * --- Local variables
  pCOMPET = space(4)
  pDATA1 = ctod("  /  /  ")
  pDATA2 = ctod("  /  /  ")
  pFLGBUDG = space(1)
  pSCRASS = space(1)
  pFLGCON = space(1)
  pFLGDIRRIP = space(1)
  pFLGDARIP = space(1)
  pFLPROV = space(1)
  pCODBUN = space(3)
  pCDC1 = space(15)
  pCDC2 = space(15)
  pVOCE1 = space(15)
  pVOCE2 = space(15)
  pCOMM1 = space(15)
  pCOMM2 = space(15)
  pLIVELLO1 = 0
  pLIVELLO2 = 0
  pFLGCOMP = space(1)
  pFLGMOV = space(1)
  pFLGPRINT = space(1)
  pSEZBIL = space(1)
  pFLGORI = space(1)
  pESSTMORI = space(1)
  w_COMPET = space(4)
  w_DATA1 = ctod("  /  /  ")
  w_DATA2 = ctod("  /  /  ")
  w_FLGBUDG = space(1)
  w_SCRASS = space(1)
  w_FLGCON = space(1)
  w_FLGDIRRIP = space(1)
  w_FLGDARIP = space(1)
  w_FLPROV = space(1)
  w_CODBUN = space(3)
  w_CDC1 = space(15)
  w_CDC2 = space(15)
  w_VOCE1 = space(15)
  w_VOCE2 = space(15)
  w_COMM1 = space(15)
  w_COMM2 = space(15)
  w_LIVELLO1 = 0
  w_LIVELLO2 = 0
  w_FLGCOMP = space(1)
  w_FLGMOV = space(1)
  w_FLSEZBIL = space(1)
  w_PROVE = space(1)
  w_FLGORI = space(1)
  w_FLGPRINT = space(1)
  w_QUERYP = space(50)
  w_ESSTMORI = space(1)
  w_CODAZI = space(5)
  w_INIESE = ctod("  /  /  ")
  w_FINESE = ctod("  /  /  ")
  * --- WorkFile variables
  ESERCIZI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch utilizzato per la gestione delle stampe di analitica
    *     Chiama il Batch GSCA_QUN che crea l atemporanea TMPMOVANAL
    * --- Competenza (Esercizio)
    * --- Data inizio elaborazione
    * --- Data fine elaborazione
    * --- Flag budget: 'S', 'N', 'T'
    * --- Flag scritture di assestamento: 'S', 'N', 'T'
    * --- Flag consuntivo/impegno: 'S', 'I', 'T'
    * --- Flag diretto/ripartito: 'S', 'N', 'T'
    * --- Flag ripartito/da ripartire: 'S', 'N', 'T'
    * --- Flag rprovvisorio/confermato: 'S', 'N', 'T'
    * --- Codice Business Unit
    * --- Filtri su CDC, VOCE e COMMESSA
    * --- Filtro sul livello
    * --- Flag competenza ('S', 'N')
    *     Si applica ai soli movimenti extracontabili
    * --- Tipo di movimenti: 'E' effettivi, 'P' previsionali, 'T' tutti
    * --- Flag stampa
    * --- Flag sezione di bilancio: 'P'=patrimoniali, 'E'=reddituali ('C' costi,'R' ricavi), 'T'=tutti
    * --- Flag origine: 'T'=tutti, 'O'=originari, 'V'=Costo del venduto, 'C'=collegati
    * --- Flag per l'esclusione dello storno dei movimenti da ripartizione
    * --- Competenza (Esercizio)
    * --- Data inizio elaborazione
    * --- Data fine elaborazione
    * --- Flag budget: 'S', 'N', 'T'
    * --- Flag scritture di assestamento: 'S', 'N', 'T'
    * --- Flag consuntivo/impegno: 'S', 'I', 'T'
    * --- Flag diretto/ripartito: 'S', 'N', 'T'
    * --- Flag ripartito/da ripartire: 'S', 'N', 'T'
    * --- Flag rprovvisorio/confermato: 'S', 'N', 'T'
    * --- Codice Business Unit
    * --- Filtri su CDC, VOCE e COMMESSA
    * --- Filtro sul livello
    * --- Flag competenza ('S', 'N')
    *     Si applica ai soli movimenti extracontabili
    * --- Tipo di movimenti: 'E' effettivi, 'P' previsionali, 'T' tutti
    * --- Flag sezione di bilancio: 'P'=patrimoniali, 'E'=reddituali ('C' costi,'R' ricavi), 'T'=tutti
    * --- Flag provenienza
    * --- Flag origine: 'T'=tutti, 'O'=originari, 'V'=Costo del venduto, 'C'=collegati
    * --- Flag stampa
    * --- Per report
    * --- Flag per l'esclusione dello storno dei movimenti da ripartizione
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Calcolo tabella temporanea
    do gsca_bun with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.pFLGPRINT <> "N"
      L_COD1=this.w_CDC1
      L_COD2=this.w_CDC2
      L_VOCE1=this.w_VOCE1
      L_VOCE2=this.w_VOCE2
      L_CODCOM=this.w_COMM1
      L_CODCOM1=this.w_COMM2
      L_FLGMOV=this.w_FLGMOV
      L_FLGDIR=this.w_FLGDIRRIP 
      L_FLCOIM=this.w_FLGCON 
      L_DATA1=this.w_DATA1
      L_DATA2=this.w_DATA2
      L_LIVELLO1=this.w_LIVELLO1
      L_LIVELLO2=this.w_LIVELLO2
      L_BUNIT=this.w_CODBUN
      L_PROVCONF=this.w_FLPROV
      L_ESE=this.w_COMPET
      L_SIMBOLO=This.oParentObject.w_SIMVAL
      L_VALUTA=This.oParentObject.w_VALUTA
      L_DECIM=This.oParentObject.w_DECTOT
      L_CAOVAL=This.oParentObject.w_CAOVAL
      L_COMPET=this.w_FLGCOMP
      L_SEZBIL=this.w_FLSEZBIL
      L_ESSTMORI=this.w_ESSTMORI
      if TYPE("This.oParentObject.w_CODCLFO")="C"
        L_CODCLFO=This.oParentObject.w_CODCLFO
      endif
      if TYPE("This.oParentObject.w_CODCLFO1")="C"
        L_CODCLFO1=This.oParentObject.w_CODCLFO1
      endif
      if TYPE("This.oParentObject.w_TIPCLFO")="C"
        L_TIPCLFO=This.oParentObject.w_TIPCLFO
      endif
      if TYPE("This.oParentObject.w_FLGFILCF")="C"
        L_FLGFILCF=This.oParentObject.w_FLGFILCF
      endif
      * --- Elaborazione query
      this.w_QUERYP = ALLTRIM(this.oParentObject.w_OQRY)
      vq_exec( this.w_QUERYP ,this,"__TMP__")
      * --- Cancellazione temporanea
      * --- Drop temporary table TMPMOVANAL
      i_nIdx=cp_GetTableDefIdx('TMPMOVANAL')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPMOVANAL')
      endif
      * --- FASE 3 - Lancio il report 
      CP_CHPRN( ALLTRIM(this.oParentObject.w_OREP),"",this.oParentObject)
      if USED("__TMP__")
        SELECT __TMP__
        USE
      endif
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Competenza (Esercizio)
    this.w_COMPET = IIF(VARTYPE(this.pCOMPET) = "L", "", this.pCOMPET)
    this.w_CODAZI = i_CODAZI
    if !(EMPTY(this.w_COMPET))
      * --- Read from ESERCIZI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ESERCIZI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ESINIESE,ESFINESE"+;
          " from "+i_cTable+" ESERCIZI where ";
              +"ESCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
              +" and ESCODESE = "+cp_ToStrODBC(this.w_COMPET);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ESINIESE,ESFINESE;
          from (i_cTable) where;
              ESCODAZI = this.w_CODAZI;
              and ESCODESE = this.w_COMPET;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_INIESE = NVL(cp_ToDate(_read_.ESINIESE),cp_NullValue(_read_.ESINIESE))
        this.w_FINESE = NVL(cp_ToDate(_read_.ESFINESE),cp_NullValue(_read_.ESFINESE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    * --- Data inizio elaborazione
    this.w_DATA1 = IIF(VARTYPE(this.pDATA1) = "L", IIF(EMPTY(this.w_COMPET), CTOD("  -  -  "), this.w_INIESE), this.pDATA1)
    * --- Data fine elaborazione
    this.w_DATA2 = IIF(VARTYPE(this.pDATA2) = "L", IIF(EMPTY(this.w_COMPET), CTOD("  -  -  "), this.w_FINESE), this.pDATA2)
    * --- Flag budget: 'S', 'N', 'T'
    this.w_FLGBUDG = IIF(VARTYPE(this.pFLGBUDG) = "L", "T", IIF(this.pFLGBUDG="E", "N", IIF(this.pFLGBUDG="P", "S", "T")))
    * --- Flag scritture di assestamento: 'S', 'N', 'T'
    this.w_SCRASS = IIF(VARTYPE(this.pSCRASS) = "L", "T", this.pSCRASS)
    * --- Flag consuntivo/impegno: 'S', 'I', 'T'
    this.w_FLGCON = IIF(VARTYPE(this.pFLGCON)="L", "T", this.pFLGCON)
    * --- Flag diretto/ripartito: 'S', 'N', 'T'
    this.w_FLGDIRRIP = IIF(VARTYPE(this.pFLGDIRRIP) = "L", "T", this.pFLGDIRRIP)
    * --- Flag ripartito/da ripartire: 'S', 'N', 'T'
    this.w_FLGDARIP = IIF(VARTYPE(this.pFLGDARIP)= "L", "T", this.pFLGDARIP)
    * --- Flag sezione di bilancio: 'P'=patrimoniali, 'E'=reddituali ('C' costi,'R' ricavi), 'T'=tutti
    this.w_FLSEZBIL = IIF(VARTYPE(this.pSEZBIL) = "L" OR this.pSEZBIL = " ", "E", this.pSEZBIL )
    * --- Flag provenienza
    this.w_PROVE = "T"
    * --- Flag provvisorio/confermato: 'S', 'N', 'T'
    this.w_FLPROV = IIF(VARTYPE(this.pFLPROV) = "L" OR this.pFLPROV = " ", "T", this.pFLPROV )
    * --- Codice Business Unit
    this.w_CODBUN = IIF(VARTYPE(this.pCODBUN) = "L", "", this.pCODBUN)
    * --- Filtri su CDC, VOCE e COMMESSA
    this.w_CDC1 = IIF(VARTYPE(pCDC) = "L", "", this.pCDC1)
    this.w_CDC2 = IIF(VARTYPE(this.pCDC2) = "L", IIF(EMPTY(this.w_CDC1), "", this.w_CDC1), this.pCDC2)
    this.w_VOCE1 = IIF(VARTYPE(this.pVOCE1) = "L", "", this.pVOCE1)
    this.w_VOCE2 = IIF(VARTYPE(this.pVOCE2) = "L", IIF(EMPTY(this.w_VOCE1), "", this.w_VOCE1), this.pVOCE2)
    this.w_COMM1 = IIF(VARTYPE(this.pCOMM1) = "L", "", this.pCOMM1)
    this.w_COMM2 = IIF(VARTYPE(this.pCOMM2) = "L", IIF(EMPTY(this.w_COMM1), "", this.w_COMM1), this.pCOMM2)
    * --- Filtro sul livello
    this.w_LIVELLO1 = IIF(VARTYPE(this.pLIVELLO1) = "L", 0, this.pLIVELLO1 )
    this.w_LIVELLO2 = IIF(VARTYPE(this.pLIVELLO2) = "L", 0, this.pLIVELLO2)
    * --- Flag competenza ('S', 'N')
    *     Si applica ai soli movimenti extracontabili
    this.w_FLGCOMP = IIF(VARTYPE(this.pFLGCOMP) = "L", "N",this.pFLGCOMP)
    * --- Tipo movimenti
    this.w_FLGMOV = IIF(VARTYPE(this.pFLGMOV) = "L" or EMPTY(this.pFLGMOV),"T",this.pFLGMOV)
    * --- Flag stampa
    this.w_FLGPRINT = IIF(VARTYPE(this.pFLGPRINT) = "L", "N", this.pFLGPRINT)
    * --- Flag origine: 'T'=tutti, 'O'=originari, 'V'=Costo del venduto, 'C'=collegati
    this.w_FLGORI = IIF(VARTYPE(this.pFLGORI) = "L", "T", this.pFLGORI)
    * --- Flag per l'esclusione dello storno dei movimenti da ripartizione
    this.w_ESSTMORI = IIF(VARTYPE(this.pESSTMORI) = "L", "N", this.pESSTMORI)
  endproc


  proc Init(oParentObject,pCOMPET,pDATA1,pDATA2,pFLGBUDG,pSCRASS,pFLGCON,pFLGDIRRIP,pFLGDARIP,pFLPROV,pCODBUN,pCDC1,pCDC2,pVOCE1,pVOCE2,pCOMM1,pCOMM2,pLIVELLO1,pLIVELLO2,pFLGCOMP,pFLGMOV,pFLGPRINT,pSEZBIL,pFLGORI,pESSTMORI)
    this.pCOMPET=pCOMPET
    this.pDATA1=pDATA1
    this.pDATA2=pDATA2
    this.pFLGBUDG=pFLGBUDG
    this.pSCRASS=pSCRASS
    this.pFLGCON=pFLGCON
    this.pFLGDIRRIP=pFLGDIRRIP
    this.pFLGDARIP=pFLGDARIP
    this.pFLPROV=pFLPROV
    this.pCODBUN=pCODBUN
    this.pCDC1=pCDC1
    this.pCDC2=pCDC2
    this.pVOCE1=pVOCE1
    this.pVOCE2=pVOCE2
    this.pCOMM1=pCOMM1
    this.pCOMM2=pCOMM2
    this.pLIVELLO1=pLIVELLO1
    this.pLIVELLO2=pLIVELLO2
    this.pFLGCOMP=pFLGCOMP
    this.pFLGMOV=pFLGMOV
    this.pFLGPRINT=pFLGPRINT
    this.pSEZBIL=pSEZBIL
    this.pFLGORI=pFLGORI
    this.pESSTMORI=pESSTMORI
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ESERCIZI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCOMPET,pDATA1,pDATA2,pFLGBUDG,pSCRASS,pFLGCON,pFLGDIRRIP,pFLGDARIP,pFLPROV,pCODBUN,pCDC1,pCDC2,pVOCE1,pVOCE2,pCOMM1,pCOMM2,pLIVELLO1,pLIVELLO2,pFLGCOMP,pFLGMOV,pFLGPRINT,pSEZBIL,pFLGORI,pESSTMORI"
endproc
