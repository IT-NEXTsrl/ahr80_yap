* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gssr_ksp                                                        *
*              Storicizzazione primanota                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_63]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-06                                                      *
* Last revis.: 2009-06-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gssr_ksp
* Controllo che il sistema sia BLOCCATO
if !g_LOCKALL
  Ah_errormsg("La storicizzazione della primanota pu� essere eseguita%0solo con il sistema in manutenzione%0Portare il sistema in manutenzione e ripetere l'operazione","STOP")
  return
endif

* --- Fine Area Manuale
return(createobject("tgssr_ksp",oParentObject))

* --- Class definition
define class tgssr_ksp as StdForm
  Top    = 13
  Left   = 7

  * --- Standard Properties
  Width  = 729
  Height = 372
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-06-24"
  HelpContextID=169253527
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=8

  * --- Constant Properties
  _IDX = 0
  ESERCIZI_IDX = 0
  AZIENDA_IDX = 0
  cPrg = "gssr_ksp"
  cComment = "Storicizzazione primanota"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CodAzi = space(5)
  w_CodEse = space(4)
  o_CodEse = space(4)
  w_Dat_Al = ctod('  /  /  ')
  w_Msg = space(0)
  w_xesiniese = ctod('  /  /  ')
  w_xesfinese = ctod('  /  /  ')
  w_esesucc = space(4)
  w_xazesstco = space(4)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgssr_kspPag1","gssr_ksp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCodEse_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='AZIENDA'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CodAzi=space(5)
      .w_CodEse=space(4)
      .w_Dat_Al=ctod("  /  /  ")
      .w_Msg=space(0)
      .w_xesiniese=ctod("  /  /  ")
      .w_xesfinese=ctod("  /  /  ")
      .w_esesucc=space(4)
      .w_xazesstco=space(4)
        .w_CodAzi = i_codazi
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CodAzi))
          .link_1_1('Full')
        endif
        .w_CodEse = str(val(.w_xazesstco)+1,4,0)
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CodEse))
          .link_1_2('Full')
        endif
        .w_Dat_Al = cp_CharToDate('31-12-'+alltrim(Str(year(.w_xesfinese))))
          .DoRTCalc(4,6,.f.)
        .w_esesucc = CALCESER(.w_xesfinese+1)
      .oPgFrm.Page1.oPag.oObj_1_19.Calculate(ah_MsgFormat("La storicizzazione della primanota � una%0operazione non reversibile da effettuarsi%0a seguito di Backup del database."),0)
      .oPgFrm.Page1.oPag.oObj_1_20.Calculate(ah_MsgFormat("Verranno storicizzati tutti i movimenti di%0primanota con codice esercizio uguale a%0quello selezionato"),0)
    endwith
    this.DoRTCalc(8,8,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_CodAzi = i_codazi
          .link_1_1('Full')
        .DoRTCalc(2,2,.t.)
        if .o_CodEse<>.w_CodEse
            .w_Dat_Al = cp_CharToDate('31-12-'+alltrim(Str(year(.w_xesfinese))))
        endif
        .DoRTCalc(4,6,.t.)
            .w_esesucc = CALCESER(.w_xesfinese+1)
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate(ah_MsgFormat("La storicizzazione della primanota � una%0operazione non reversibile da effettuarsi%0a seguito di Backup del database."),0)
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate(ah_MsgFormat("Verranno storicizzati tutti i movimenti di%0primanota con codice esercizio uguale a%0quello selezionato"),0)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(8,8,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate(ah_MsgFormat("La storicizzazione della primanota � una%0operazione non reversibile da effettuarsi%0a seguito di Backup del database."),0)
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate(ah_MsgFormat("Verranno storicizzati tutti i movimenti di%0primanota con codice esercizio uguale a%0quello selezionato"),0)
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_16.visible=!this.oPgFrm.Page1.oPag.oStr_1_16.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_19.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_20.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CodAzi
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CodAzi) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CodAzi)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZESSTCO";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CodAzi);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CodAzi)
            select AZCODAZI,AZESSTCO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CodAzi = NVL(_Link_.AZCODAZI,space(5))
      this.w_xazesstco = NVL(_Link_.AZESSTCO,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_CodAzi = space(5)
      endif
      this.w_xazesstco = space(4)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CodAzi Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CodEse
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CodEse) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_CodEse)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CodAzi);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CodAzi;
                     ,'ESCODESE',trim(this.w_CodEse))
          select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CodEse)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CodEse) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oCodEse_1_2'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CodAzi<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CodAzi);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CodEse)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_CodEse);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CodAzi);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CodAzi;
                       ,'ESCODESE',this.w_CodEse)
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CodEse = NVL(_Link_.ESCODESE,space(4))
      this.w_xesiniese = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_xesfinese = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CodEse = space(4)
      endif
      this.w_xesiniese = ctod("  /  /  ")
      this.w_xesfinese = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CodEse Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCodEse_1_2.value==this.w_CodEse)
      this.oPgFrm.Page1.oPag.oCodEse_1_2.value=this.w_CodEse
    endif
    if not(this.oPgFrm.Page1.oPag.oMsg_1_5.value==this.w_Msg)
      this.oPgFrm.Page1.oPag.oMsg_1_5.value=this.w_Msg
    endif
    if not(this.oPgFrm.Page1.oPag.oxesiniese_1_9.value==this.w_xesiniese)
      this.oPgFrm.Page1.oPag.oxesiniese_1_9.value=this.w_xesiniese
    endif
    if not(this.oPgFrm.Page1.oPag.oxesfinese_1_10.value==this.w_xesfinese)
      this.oPgFrm.Page1.oPag.oxesfinese_1_10.value=this.w_xesfinese
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CodEse))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCodEse_1_2.SetFocus()
            i_bnoObbl = !empty(.w_CodEse)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CodEse = this.w_CodEse
    return

enddefine

* --- Define pages as container
define class tgssr_kspPag1 as StdContainer
  Width  = 725
  height = 372
  stdWidth  = 725
  stdheight = 372
  resizeXpos=571
  resizeYpos=280
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCodEse_1_2 as StdField with uid="UZKGKBYPNV",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CodEse", cQueryName = "CodEse",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio da storicizzare",;
    HelpContextID = 158183898,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=60, Top=179, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CodAzi", oKey_2_1="ESCODESE", oKey_2_2="this.w_CodEse"

  func oCodEse_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCodEse_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCodEse_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CodAzi)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CodAzi)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oCodEse_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oMsg_1_5 as StdMemo with uid="ATBBCHAJSX",rtseq=4,rtrep=.f.,;
    cFormVar = "w_Msg", cQueryName = "Msg",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 169706182,;
    FontName = "Courier New", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=281, Width=468, Left=251, Top=36, tabstop = .f., readonly = .t.


  add object oBtn_1_6 as StdButton with uid="VFSEADJQCS",left=137, top=322, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare la storicizzazione";
    , HelpContextID = 44026858;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      with this.Parent.oContained
        do GSSR_BSP with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_7 as StdButton with uid="URECPKNNNL",left=189, top=322, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 176570950;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oxesiniese_1_9 as StdField with uid="XEFRKYYSTC",rtseq=5,rtrep=.f.,;
    cFormVar = "w_xesiniese", cQueryName = "xesiniese",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Intervallo esercizio",;
    HelpContextID = 174538297,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=60, Top=207

  add object oxesfinese_1_10 as StdField with uid="DQJFHSTLSL",rtseq=6,rtrep=.f.,;
    cFormVar = "w_xesfinese", cQueryName = "xesfinese",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Intervallo esercizio",;
    HelpContextID = 252984889,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=163, Top=207


  add object oBtn_1_11 as StdButton with uid="SXPPIHAKJF",left=671, top=322, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la stampa";
    , HelpContextID = 225827802;
    , tabstop = .f., caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      with this.Parent.oContained
        do PrintMsg with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_11.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!empty(.w_Msg))
      endwith
    endif
  endfunc


  add object oObj_1_19 as cp_calclbl with uid="GCJFWUCMVR",left=11, top=42, width=224,height=57,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=1;
    , HelpContextID = 189619738


  add object oObj_1_20 as cp_calclbl with uid="YURAPMHYYL",left=11, top=105, width=224,height=57,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=1;
    , HelpContextID = 189619738

  add object oStr_1_3 as StdString with uid="ICYYTXIFDV",Visible=.t., Left=5, Top=183,;
    Alignment=1, Width=53, Height=18,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="MNNAMJJFUG",Visible=.t., Left=451, Top=18,;
    Alignment=0, Width=63, Height=18,;
    Caption="Messaggi"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="RGZUEVVJFR",Visible=.t., Left=11, Top=18,;
    Alignment=2, Width=224, Height=19,;
    Caption="Attenzione"    , backstyle = 1, backcolor = 8454143, borderstyle = 1;
  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_14 as StdString with uid="OZXNAPJDAR",Visible=.t., Left=24, Top=211,;
    Alignment=1, Width=34, Height=18,;
    Caption="Dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="FEJUHCPVDO",Visible=.t., Left=140, Top=211,;
    Alignment=1, Width=23, Height=18,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="TSYFSBJAZP",Visible=.t., Left=-15, Top=426,;
    Alignment=1, Width=122, Height=18,;
    Caption="Storicizzare fino al:"  ;
  , bGlobalFont=.t.

  func oStr_1_16.mHide()
    with this.Parent.oContained
      return (.t.)
    endwith
  endfunc

  add object oBox_1_8 as StdBox with uid="IEHDGTHVHI",left=243, top=14, width=2,height=345
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gssr_ksp','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gssr_ksp
proc PrintMsg(parent)
  set console off
  strtofile(parent.w_Msg,'tempMsg.txt')
  type 'tempMsg.txt' to printer prompt
endproc
* --- Fine Area Manuale
