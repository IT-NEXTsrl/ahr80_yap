* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_sd2                                                        *
*              Riepilogo liquidazione IVA periodica                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_402]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-21                                                      *
* Last revis.: 2011-02-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_sd2",oParentObject))

* --- Class definition
define class tgscg_sd2 as StdForm
  Top    = 3
  Left   = 94

  * --- Standard Properties
  Width  = 615
  Height = 466+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-02-10"
  HelpContextID=74065257
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=83

  * --- Constant Properties
  _IDX = 0
  AZIENDA_IDX = 0
  VALUTE_IDX = 0
  COC_MAST_IDX = 0
  COD_ABI_IDX = 0
  COD_CAB_IDX = 0
  cPrg = "gscg_sd2"
  cComment = "Riepilogo liquidazione IVA periodica"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_VP__ANNO = space(4)
  w_VPPERIOD = 0
  w_VPCODVAL = space(1)
  w_COLSHOW = 0
  w_COLEDIT = 0
  w_MAGTRI = 0
  w_MINACC = 0
  w_MINVER = 0
  w_F2DEFI = space(1)
  w_IMPVAL = space(3)
  w_RAGAZI = space(30)
  w_TIPDIC = space(1)
  w_VPDICGRU = space(1)
  w_VPDICSOC = space(1)
  w_FLTEST = space(1)
  w_VISPDF = space(1)
  w_CONFER = space(10)
  w_COFAZI = space(16)
  w_PIVAZI = space(12)
  w_TIPLIQ = space(1)
  w_CONCES = space(3)
  w_VPDATPRE = ctod('  /  /  ')
  w_TITOLO = space(100)
  w_VPIMPOR1 = 0
  w_VPCESINT = 0
  w_VPIMPOR2 = 0
  w_VPACQINT = 0
  w_VPIMPON3 = 0
  w_VPIMPOS3 = 0
  w_VPIMPOR5 = 0
  w_VPIMPOR6 = 0
  w_VPIMPOR7 = 0
  w_IMPDEB7 = 0
  w_IMPCRE7 = 0
  w_IMPDEB8 = 0
  w_IMPCRE8 = 0
  w_VPIMPOR8 = 0
  w_IMPDEB9 = 0
  w_IMPCRE9 = 0
  w_VPIMPOR9 = 0
  w_IMPDEB10 = 0
  o_IMPDEB10 = 0
  w_IMPCRE10 = 0
  w_VPIMPO10 = 0
  w_TOTALE = 0
  w_TOTDEB = 0
  w_TOTCRE = 0
  w_VPIMPO11 = 0
  o_VPIMPO11 = 0
  w_VPIMPO12 = 0
  o_VPIMPO12 = 0
  w_IMPDEB12 = 0
  w_IMPCRE12 = 0
  w_VPIMPO13 = 0
  o_VPIMPO13 = 0
  w_VPIMPO14 = 0
  w_VPIMPO15 = 0
  w_VPIMPO16 = 0
  o_VPIMPO16 = 0
  w_VPIMPVER = 0
  o_VPIMPVER = 0
  w_VPVEREUR = space(1)
  w_CRERES = 0
  w_VPIMPOR6C = 0
  w_OBTEST = ctod('  /  /  ')
  w_VPVARIMP = space(1)
  w_VPCORTER = space(1)
  w_VPVERNEF = space(1)
  w_VPAR74C5 = space(1)
  w_VPAR74C4 = space(1)
  w_VPOPMEDI = space(1)
  w_VPOPNOIM = space(1)
  w_VPACQBEA = space(1)
  w_VPCRERIM = 0
  w_VPEURO19 = space(1)
  o_VPEURO19 = space(1)
  w_VPCREUTI = 0
  w_VPCODCAR = space(1)
  w_VPCODFIS = space(16)
  w_VPDATVER = ctod('  /  /  ')
  w_CODBAN = space(15)
  w_VPCODAZI = space(5)
  w_VPCODCAB = space(5)
  w_DESBAN = space(35)
  w_CODAZI = space(5)
  w_DESABI = space(80)
  w_DESFIL = space(40)
  w_INDIRI = space(50)
  w_CAP = space(5)
  w_VPSTAREG = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_sd2Pag1","gscg_sd2",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dati IVA")
      .Pages(2).addobject("oPag","tgscg_sd2Pag2","gscg_sd2",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Dati versamento")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oVPIMPOR1_1_27
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='COC_MAST'
    this.cWorkTables[4]='COD_ABI'
    this.cWorkTables[5]='COD_CAB'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gscg_sd2
    *This.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_VP__ANNO=space(4)
      .w_VPPERIOD=0
      .w_VPCODVAL=space(1)
      .w_COLSHOW=0
      .w_COLEDIT=0
      .w_MAGTRI=0
      .w_MINACC=0
      .w_MINVER=0
      .w_F2DEFI=space(1)
      .w_IMPVAL=space(3)
      .w_RAGAZI=space(30)
      .w_TIPDIC=space(1)
      .w_VPDICGRU=space(1)
      .w_VPDICSOC=space(1)
      .w_FLTEST=space(1)
      .w_VISPDF=space(1)
      .w_CONFER=space(10)
      .w_COFAZI=space(16)
      .w_PIVAZI=space(12)
      .w_TIPLIQ=space(1)
      .w_CONCES=space(3)
      .w_VPDATPRE=ctod("  /  /  ")
      .w_TITOLO=space(100)
      .w_VPIMPOR1=0
      .w_VPCESINT=0
      .w_VPIMPOR2=0
      .w_VPACQINT=0
      .w_VPIMPON3=0
      .w_VPIMPOS3=0
      .w_VPIMPOR5=0
      .w_VPIMPOR6=0
      .w_VPIMPOR7=0
      .w_IMPDEB7=0
      .w_IMPCRE7=0
      .w_IMPDEB8=0
      .w_IMPCRE8=0
      .w_VPIMPOR8=0
      .w_IMPDEB9=0
      .w_IMPCRE9=0
      .w_VPIMPOR9=0
      .w_IMPDEB10=0
      .w_IMPCRE10=0
      .w_VPIMPO10=0
      .w_TOTALE=0
      .w_TOTDEB=0
      .w_TOTCRE=0
      .w_VPIMPO11=0
      .w_VPIMPO12=0
      .w_IMPDEB12=0
      .w_IMPCRE12=0
      .w_VPIMPO13=0
      .w_VPIMPO14=0
      .w_VPIMPO15=0
      .w_VPIMPO16=0
      .w_VPIMPVER=0
      .w_VPVEREUR=space(1)
      .w_CRERES=0
      .w_VPIMPOR6C=0
      .w_OBTEST=ctod("  /  /  ")
      .w_VPVARIMP=space(1)
      .w_VPCORTER=space(1)
      .w_VPVERNEF=space(1)
      .w_VPAR74C5=space(1)
      .w_VPAR74C4=space(1)
      .w_VPOPMEDI=space(1)
      .w_VPOPNOIM=space(1)
      .w_VPACQBEA=space(1)
      .w_VPCRERIM=0
      .w_VPEURO19=space(1)
      .w_VPCREUTI=0
      .w_VPCODCAR=space(1)
      .w_VPCODFIS=space(16)
      .w_VPDATVER=ctod("  /  /  ")
      .w_CODBAN=space(15)
      .w_VPCODAZI=space(5)
      .w_VPCODCAB=space(5)
      .w_DESBAN=space(35)
      .w_CODAZI=space(5)
      .w_DESABI=space(80)
      .w_DESFIL=space(40)
      .w_INDIRI=space(50)
      .w_CAP=space(5)
      .w_VPSTAREG=space(1)
      .w_VP__ANNO=oParentObject.w_VP__ANNO
      .w_VPPERIOD=oParentObject.w_VPPERIOD
      .w_VPCODVAL=oParentObject.w_VPCODVAL
      .w_MAGTRI=oParentObject.w_MAGTRI
      .w_MINACC=oParentObject.w_MINACC
      .w_MINVER=oParentObject.w_MINVER
      .w_F2DEFI=oParentObject.w_F2DEFI
      .w_TIPDIC=oParentObject.w_TIPDIC
      .w_VPDICGRU=oParentObject.w_VPDICGRU
      .w_VPDICSOC=oParentObject.w_VPDICSOC
      .w_FLTEST=oParentObject.w_FLTEST
      .w_CONFER=oParentObject.w_CONFER
      .w_COFAZI=oParentObject.w_COFAZI
      .w_PIVAZI=oParentObject.w_PIVAZI
      .w_TIPLIQ=oParentObject.w_TIPLIQ
      .w_CONCES=oParentObject.w_CONCES
      .w_VPIMPOR1=oParentObject.w_VPIMPOR1
      .w_VPCESINT=oParentObject.w_VPCESINT
      .w_VPIMPOR2=oParentObject.w_VPIMPOR2
      .w_VPACQINT=oParentObject.w_VPACQINT
      .w_VPIMPON3=oParentObject.w_VPIMPON3
      .w_VPIMPOS3=oParentObject.w_VPIMPOS3
      .w_VPIMPOR5=oParentObject.w_VPIMPOR5
      .w_VPIMPOR6=oParentObject.w_VPIMPOR6
      .w_VPIMPOR7=oParentObject.w_VPIMPOR7
      .w_VPIMPOR8=oParentObject.w_VPIMPOR8
      .w_VPIMPOR9=oParentObject.w_VPIMPOR9
      .w_VPIMPO10=oParentObject.w_VPIMPO10
      .w_VPIMPO11=oParentObject.w_VPIMPO11
      .w_VPIMPO12=oParentObject.w_VPIMPO12
      .w_VPIMPO13=oParentObject.w_VPIMPO13
      .w_VPIMPO14=oParentObject.w_VPIMPO14
      .w_VPIMPO15=oParentObject.w_VPIMPO15
      .w_VPIMPO16=oParentObject.w_VPIMPO16
      .w_VPIMPVER=oParentObject.w_VPIMPVER
      .w_VPVEREUR=oParentObject.w_VPVEREUR
      .w_CRERES=oParentObject.w_CRERES
      .w_VPIMPOR6C=oParentObject.w_VPIMPOR6C
      .w_VPVARIMP=oParentObject.w_VPVARIMP
      .w_VPCORTER=oParentObject.w_VPCORTER
      .w_VPVERNEF=oParentObject.w_VPVERNEF
      .w_VPAR74C5=oParentObject.w_VPAR74C5
      .w_VPAR74C4=oParentObject.w_VPAR74C4
      .w_VPOPMEDI=oParentObject.w_VPOPMEDI
      .w_VPOPNOIM=oParentObject.w_VPOPNOIM
      .w_VPACQBEA=oParentObject.w_VPACQBEA
      .w_VPCRERIM=oParentObject.w_VPCRERIM
      .w_VPEURO19=oParentObject.w_VPEURO19
      .w_VPCREUTI=oParentObject.w_VPCREUTI
      .w_VPCODCAR=oParentObject.w_VPCODCAR
      .w_VPCODFIS=oParentObject.w_VPCODFIS
      .w_VPDATVER=oParentObject.w_VPDATVER
      .w_VPCODAZI=oParentObject.w_VPCODAZI
      .w_VPCODCAB=oParentObject.w_VPCODCAB
      .w_VPSTAREG=oParentObject.w_VPSTAREG
          .DoRTCalc(1,3,.f.)
        .w_COLSHOW = 0
        .w_COLEDIT = RGB(27,80,180)
          .DoRTCalc(6,9,.f.)
        .w_IMPVAL = IIF(.w_VPCODVAL='S', g_CODEUR, g_CODLIR)
        .w_RAGAZI = g_RAGAZI
          .DoRTCalc(12,15,.f.)
        .w_VISPDF = ' '
        .w_CONFER = 'S'
          .DoRTCalc(18,21,.f.)
        .w_VPDATPRE = cp_CharToDate('  -  -  ')
        .w_TITOLO = 'Liquidazione Iva '+IIF(.w_F2DEFI = 'S' ,'Definitiva','In Prova')+' del '+.w_VP__ANNO+iif(g_TIPDEN='T',' Trimestre ',' Mese di ')+CALCPER(.w_VPPERIOD, g_TIPDEN)
      .oPgFrm.Page1.oPag.oObj_1_26.Calculate()
          .DoRTCalc(24,32,.f.)
        .w_IMPDEB7 = IIF( .w_VPIMPOR7>0 ,  .w_VPIMPOR7, 0)
        .w_IMPCRE7 = IIF( .w_VPIMPOR7<0 ,  ABS(.w_VPIMPOR7) ,0 )
          .DoRTCalc(35,36,.f.)
        .w_VPIMPOR8 = .w_IMPDEB8-.w_IMPCRE8
          .DoRTCalc(38,39,.f.)
        .w_VPIMPOR9 = .w_IMPDEB9-.w_IMPCRE9
        .w_IMPDEB10 = IIF( .w_VPIMPO10>0,  .w_VPIMPO10 ,0)
        .w_IMPCRE10 = IIF( .w_VPIMPO10<0 ,  ABS(.w_VPIMPO10) ,0)
        .w_VPIMPO10 = .w_IMPDEB10-.w_IMPCRE10
        .w_TOTALE = .w_VPIMPOR7+.w_VPIMPOR8+.w_VPIMPOR9+.w_VPIMPO10
        .w_TOTDEB = .w_TOTALE
        .w_TOTCRE = .w_TOTALE
          .DoRTCalc(47,47,.f.)
        .w_VPIMPO12 = (.w_VPIMPOR7+.w_VPIMPOR8+.w_VPIMPOR9+.w_VPIMPO10)-.w_VPIMPO11
        .w_IMPDEB12 = IIF( .w_VPIMPO12>0,  .w_VPIMPO12 ,0)
        .w_IMPCRE12 = IIF( .w_VPIMPO12<0 ,  ABS(.w_VPIMPO12) ,0)
          .DoRTCalc(51,53,.f.)
        .w_VPIMPO16 = IIF((g_TIPDEN<>'M' AND .w_VPPERIOD=4) OR ((.w_VPIMPO12 + .w_VPIMPO14) - (.w_VPIMPO13 + .w_VPIMPO15))<0, 0, (.w_VPIMPO12 + .w_VPIMPO14) - (.w_VPIMPO13 + .w_VPIMPO15))
      .oPgFrm.Page1.oPag.oObj_1_73.Calculate('Op.Attive',IIF(.w_VPVARIMP='S' And .w_VPDICGRU<>'S',.w_COLEDIT,.w_COLSHOW))
      .oPgFrm.Page1.oPag.oObj_1_74.Calculate('Op.Passive',IIF(.w_VPVARIMP='S' And .w_VPDICGRU<>'S',.w_COLEDIT,.w_COLSHOW))
      .oPgFrm.Page1.oPag.oObj_1_75.Calculate('di cui Cessioni Intra',IIF(.w_VPVARIMP='S' And .w_VPDICGRU<>'S',.w_COLEDIT,.w_COLSHOW))
      .oPgFrm.Page1.oPag.oObj_1_76.Calculate('di cui Acquisti Intra',IIF(.w_VPVARIMP='S' And .w_VPDICGRU<>'S',.w_COLEDIT,.w_COLSHOW))
      .oPgFrm.Page1.oPag.oObj_1_77.Calculate('Iva Esigibile per il Periodo',IIF(.w_VPDICGRU='S',.w_COLEDIT,.w_COLSHOW))
      .oPgFrm.Page1.oPag.oObj_1_78.Calculate('Iva che si detrae per il Periodo',IIF(.w_VPDICGRU='S',.w_COLEDIT,.w_COLSHOW))
      .oPgFrm.Page1.oPag.oObj_1_79.Calculate('Variazioni di Imposta',IIF(1=1,.w_COLEDIT,.w_COLSHOW))
      .oPgFrm.Page1.oPag.oObj_1_80.Calculate('Credito Iva Compensabile',IIF(1=1,.w_COLEDIT,.w_COLSHOW))
      .oPgFrm.Page1.oPag.oObj_1_81.Calculate('Versamento',IIF(.w_VPDICSOC<>'S' AND .w_VPIMPO16<>0 AND NOT (g_TIPDEN='T' AND .w_VPPERIOD=4), .w_COLEDIT,.w_COLSHOW))
      .oPgFrm.Page1.oPag.oObj_1_83.Calculate('Imponibile',IIF(.w_VPVARIMP='S' And .w_VPDICGRU<>'S',.w_COLEDIT,.w_COLSHOW))
      .oPgFrm.Page1.oPag.oObj_1_84.Calculate('Imposta',IIF(.w_VPVARIMP='S' And .w_VPDICGRU<>'S',.w_COLEDIT,.w_COLSHOW))
      .oPgFrm.Page1.oPag.oObj_1_85.Calculate('Iva non versata/in eccesso',IIF(1=1,.w_COLEDIT,.w_COLSHOW))
          .DoRTCalc(55,58,.f.)
        .w_OBTEST = i_DATSYS
      .oPgFrm.Page2.oPag.oObj_2_16.Calculate('Credito chiesto a rimborso:',IIF(.w_VPDICSOC<>'S' AND .w_VPIMPO12<0 AND ((G_TIPDEN='M' AND (.w_VPPERIOD=3 or .w_VPPERIOD=6  or .w_VPPERIOD=9)) OR (G_TIPDEN='T' AND .w_VPPERIOD<4)),.w_COLEDIT,.w_COLSHOW))
      .oPgFrm.Page2.oPag.oObj_2_17.Calculate('Credito da utilizzare in compensazione mod. F24',IIF(.w_VPDICSOC<>'S' AND .w_VPIMPO12<0 AND ((G_TIPDEN='M' AND (.w_VPPERIOD=3 or .w_VPPERIOD=6  or .w_VPPERIOD=9)) OR (G_TIPDEN='T' AND .w_VPPERIOD<4)),.w_COLEDIT,.w_COLSHOW))
        .DoRTCalc(60,74,.f.)
        if not(empty(.w_CODBAN))
          .link_2_19('Full')
        endif
        .DoRTCalc(75,75,.f.)
        if not(empty(.w_VPCODAZI))
          .link_2_20('Full')
        endif
        .DoRTCalc(76,76,.f.)
        if not(empty(.w_VPCODCAB))
          .link_2_21('Full')
        endif
          .DoRTCalc(77,77,.f.)
        .w_CODAZI = I_CODAZI
    endwith
    this.DoRTCalc(79,83,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_VP__ANNO=.w_VP__ANNO
      .oParentObject.w_VPPERIOD=.w_VPPERIOD
      .oParentObject.w_VPCODVAL=.w_VPCODVAL
      .oParentObject.w_MAGTRI=.w_MAGTRI
      .oParentObject.w_MINACC=.w_MINACC
      .oParentObject.w_MINVER=.w_MINVER
      .oParentObject.w_F2DEFI=.w_F2DEFI
      .oParentObject.w_TIPDIC=.w_TIPDIC
      .oParentObject.w_VPDICGRU=.w_VPDICGRU
      .oParentObject.w_VPDICSOC=.w_VPDICSOC
      .oParentObject.w_FLTEST=.w_FLTEST
      .oParentObject.w_CONFER=.w_CONFER
      .oParentObject.w_COFAZI=.w_COFAZI
      .oParentObject.w_PIVAZI=.w_PIVAZI
      .oParentObject.w_TIPLIQ=.w_TIPLIQ
      .oParentObject.w_CONCES=.w_CONCES
      .oParentObject.w_VPIMPOR1=.w_VPIMPOR1
      .oParentObject.w_VPCESINT=.w_VPCESINT
      .oParentObject.w_VPIMPOR2=.w_VPIMPOR2
      .oParentObject.w_VPACQINT=.w_VPACQINT
      .oParentObject.w_VPIMPON3=.w_VPIMPON3
      .oParentObject.w_VPIMPOS3=.w_VPIMPOS3
      .oParentObject.w_VPIMPOR5=.w_VPIMPOR5
      .oParentObject.w_VPIMPOR6=.w_VPIMPOR6
      .oParentObject.w_VPIMPOR7=.w_VPIMPOR7
      .oParentObject.w_VPIMPOR8=.w_VPIMPOR8
      .oParentObject.w_VPIMPOR9=.w_VPIMPOR9
      .oParentObject.w_VPIMPO10=.w_VPIMPO10
      .oParentObject.w_VPIMPO11=.w_VPIMPO11
      .oParentObject.w_VPIMPO12=.w_VPIMPO12
      .oParentObject.w_VPIMPO13=.w_VPIMPO13
      .oParentObject.w_VPIMPO14=.w_VPIMPO14
      .oParentObject.w_VPIMPO15=.w_VPIMPO15
      .oParentObject.w_VPIMPO16=.w_VPIMPO16
      .oParentObject.w_VPIMPVER=.w_VPIMPVER
      .oParentObject.w_VPVEREUR=.w_VPVEREUR
      .oParentObject.w_CRERES=.w_CRERES
      .oParentObject.w_VPIMPOR6C=.w_VPIMPOR6C
      .oParentObject.w_VPVARIMP=.w_VPVARIMP
      .oParentObject.w_VPCORTER=.w_VPCORTER
      .oParentObject.w_VPVERNEF=.w_VPVERNEF
      .oParentObject.w_VPAR74C5=.w_VPAR74C5
      .oParentObject.w_VPAR74C4=.w_VPAR74C4
      .oParentObject.w_VPOPMEDI=.w_VPOPMEDI
      .oParentObject.w_VPOPNOIM=.w_VPOPNOIM
      .oParentObject.w_VPACQBEA=.w_VPACQBEA
      .oParentObject.w_VPCRERIM=.w_VPCRERIM
      .oParentObject.w_VPEURO19=.w_VPEURO19
      .oParentObject.w_VPCREUTI=.w_VPCREUTI
      .oParentObject.w_VPCODCAR=.w_VPCODCAR
      .oParentObject.w_VPCODFIS=.w_VPCODFIS
      .oParentObject.w_VPDATVER=.w_VPDATVER
      .oParentObject.w_VPCODAZI=.w_VPCODAZI
      .oParentObject.w_VPCODCAB=.w_VPCODCAB
      .oParentObject.w_VPSTAREG=.w_VPSTAREG
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,22,.t.)
            .w_TITOLO = 'Liquidazione Iva '+IIF(.w_F2DEFI = 'S' ,'Definitiva','In Prova')+' del '+.w_VP__ANNO+iif(g_TIPDEN='T',' Trimestre ',' Mese di ')+CALCPER(.w_VPPERIOD, g_TIPDEN)
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate()
        .DoRTCalc(24,32,.t.)
            .w_IMPDEB7 = IIF( .w_VPIMPOR7>0 ,  .w_VPIMPOR7, 0)
            .w_IMPCRE7 = IIF( .w_VPIMPOR7<0 ,  ABS(.w_VPIMPOR7) ,0 )
        .DoRTCalc(35,36,.t.)
            .w_VPIMPOR8 = .w_IMPDEB8-.w_IMPCRE8
        .DoRTCalc(38,39,.t.)
            .w_VPIMPOR9 = .w_IMPDEB9-.w_IMPCRE9
        .DoRTCalc(41,42,.t.)
            .w_VPIMPO10 = .w_IMPDEB10-.w_IMPCRE10
            .w_TOTALE = .w_VPIMPOR7+.w_VPIMPOR8+.w_VPIMPOR9+.w_VPIMPO10
            .w_TOTDEB = .w_TOTALE
            .w_TOTCRE = .w_TOTALE
        .DoRTCalc(47,47,.t.)
            .w_VPIMPO12 = (.w_VPIMPOR7+.w_VPIMPOR8+.w_VPIMPOR9+.w_VPIMPO10)-.w_VPIMPO11
            .w_IMPDEB12 = IIF( .w_VPIMPO12>0,  .w_VPIMPO12 ,0)
            .w_IMPCRE12 = IIF( .w_VPIMPO12<0 ,  ABS(.w_VPIMPO12) ,0)
        .DoRTCalc(51,51,.t.)
        if .o_VPIMPO11<>.w_VPIMPO11.or. .o_VPIMPO12<>.w_VPIMPO12.or. .o_VPIMPO13<>.w_VPIMPO13.or. .o_IMPDEB10<>.w_IMPDEB10
            .w_VPIMPO14 = IIF(g_TIPDEN<>'M' AND .w_VPPERIOD<>4 AND (.w_VPIMPO12-(.w_VPIMPO13+.w_IMPDEB10))>0, cp_ROUND(((.w_VPIMPO12-(.w_VPIMPO13+.w_IMPDEB10)) * .w_MAGTRI) / 100, IIF(VAL(.w_VP__ANNO)>2001, 2, 0)), 0)
        endif
        .DoRTCalc(53,53,.t.)
            .w_VPIMPO16 = IIF((g_TIPDEN<>'M' AND .w_VPPERIOD=4) OR ((.w_VPIMPO12 + .w_VPIMPO14) - (.w_VPIMPO13 + .w_VPIMPO15))<0, 0, (.w_VPIMPO12 + .w_VPIMPO14) - (.w_VPIMPO13 + .w_VPIMPO15))
        if .o_VPIMPO16<>.w_VPIMPO16
            .w_VPIMPVER = IIF(.w_VPDICSOC<>'S' AND .w_VPIMPO16>.w_MINVER,.w_VPIMPO16,0)
        endif
        .oPgFrm.Page1.oPag.oObj_1_73.Calculate('Op.Attive',IIF(.w_VPVARIMP='S' And .w_VPDICGRU<>'S',.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page1.oPag.oObj_1_74.Calculate('Op.Passive',IIF(.w_VPVARIMP='S' And .w_VPDICGRU<>'S',.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page1.oPag.oObj_1_75.Calculate('di cui Cessioni Intra',IIF(.w_VPVARIMP='S' And .w_VPDICGRU<>'S',.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page1.oPag.oObj_1_76.Calculate('di cui Acquisti Intra',IIF(.w_VPVARIMP='S' And .w_VPDICGRU<>'S',.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page1.oPag.oObj_1_77.Calculate('Iva Esigibile per il Periodo',IIF(.w_VPDICGRU='S',.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page1.oPag.oObj_1_78.Calculate('Iva che si detrae per il Periodo',IIF(.w_VPDICGRU='S',.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page1.oPag.oObj_1_79.Calculate('Variazioni di Imposta',IIF(1=1,.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page1.oPag.oObj_1_80.Calculate('Credito Iva Compensabile',IIF(1=1,.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page1.oPag.oObj_1_81.Calculate('Versamento',IIF(.w_VPDICSOC<>'S' AND .w_VPIMPO16<>0 AND NOT (g_TIPDEN='T' AND .w_VPPERIOD=4), .w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page1.oPag.oObj_1_83.Calculate('Imponibile',IIF(.w_VPVARIMP='S' And .w_VPDICGRU<>'S',.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page1.oPag.oObj_1_84.Calculate('Imposta',IIF(.w_VPVARIMP='S' And .w_VPDICGRU<>'S',.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page1.oPag.oObj_1_85.Calculate('Iva non versata/in eccesso',IIF(1=1,.w_COLEDIT,.w_COLSHOW))
        .DoRTCalc(56,61,.t.)
        if .o_VPIMPVER<>.w_VPIMPVER
            .w_VPVERNEF = ' '
        endif
        .DoRTCalc(63,67,.t.)
        if .o_VPEURO19<>.w_VPEURO19
            .w_VPCRERIM = 0
        endif
        .DoRTCalc(69,69,.t.)
        if .o_VPEURO19<>.w_VPEURO19
            .w_VPCREUTI = 0
        endif
        .oPgFrm.Page2.oPag.oObj_2_16.Calculate('Credito chiesto a rimborso:',IIF(.w_VPDICSOC<>'S' AND .w_VPIMPO12<0 AND ((G_TIPDEN='M' AND (.w_VPPERIOD=3 or .w_VPPERIOD=6  or .w_VPPERIOD=9)) OR (G_TIPDEN='T' AND .w_VPPERIOD<4)),.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page2.oPag.oObj_2_17.Calculate('Credito da utilizzare in compensazione mod. F24',IIF(.w_VPDICSOC<>'S' AND .w_VPIMPO12<0 AND ((G_TIPDEN='M' AND (.w_VPPERIOD=3 or .w_VPPERIOD=6  or .w_VPPERIOD=9)) OR (G_TIPDEN='T' AND .w_VPPERIOD<4)),.w_COLEDIT,.w_COLSHOW))
        .DoRTCalc(71,74,.t.)
          .link_2_20('Full')
          .link_2_21('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(77,83,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_73.Calculate('Op.Attive',IIF(.w_VPVARIMP='S' And .w_VPDICGRU<>'S',.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page1.oPag.oObj_1_74.Calculate('Op.Passive',IIF(.w_VPVARIMP='S' And .w_VPDICGRU<>'S',.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page1.oPag.oObj_1_75.Calculate('di cui Cessioni Intra',IIF(.w_VPVARIMP='S' And .w_VPDICGRU<>'S',.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page1.oPag.oObj_1_76.Calculate('di cui Acquisti Intra',IIF(.w_VPVARIMP='S' And .w_VPDICGRU<>'S',.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page1.oPag.oObj_1_77.Calculate('Iva Esigibile per il Periodo',IIF(.w_VPDICGRU='S',.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page1.oPag.oObj_1_78.Calculate('Iva che si detrae per il Periodo',IIF(.w_VPDICGRU='S',.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page1.oPag.oObj_1_79.Calculate('Variazioni di Imposta',IIF(1=1,.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page1.oPag.oObj_1_80.Calculate('Credito Iva Compensabile',IIF(1=1,.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page1.oPag.oObj_1_81.Calculate('Versamento',IIF(.w_VPDICSOC<>'S' AND .w_VPIMPO16<>0 AND NOT (g_TIPDEN='T' AND .w_VPPERIOD=4), .w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page1.oPag.oObj_1_83.Calculate('Imponibile',IIF(.w_VPVARIMP='S' And .w_VPDICGRU<>'S',.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page1.oPag.oObj_1_84.Calculate('Imposta',IIF(.w_VPVARIMP='S' And .w_VPDICGRU<>'S',.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page1.oPag.oObj_1_85.Calculate('Iva non versata/in eccesso',IIF(1=1,.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page2.oPag.oObj_2_16.Calculate('Credito chiesto a rimborso:',IIF(.w_VPDICSOC<>'S' AND .w_VPIMPO12<0 AND ((G_TIPDEN='M' AND (.w_VPPERIOD=3 or .w_VPPERIOD=6  or .w_VPPERIOD=9)) OR (G_TIPDEN='T' AND .w_VPPERIOD<4)),.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page2.oPag.oObj_2_17.Calculate('Credito da utilizzare in compensazione mod. F24',IIF(.w_VPDICSOC<>'S' AND .w_VPIMPO12<0 AND ((G_TIPDEN='M' AND (.w_VPPERIOD=3 or .w_VPPERIOD=6  or .w_VPPERIOD=9)) OR (G_TIPDEN='T' AND .w_VPPERIOD<4)),.w_COLEDIT,.w_COLSHOW))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oVPIMPOR1_1_27.enabled = this.oPgFrm.Page1.oPag.oVPIMPOR1_1_27.mCond()
    this.oPgFrm.Page1.oPag.oVPCESINT_1_28.enabled = this.oPgFrm.Page1.oPag.oVPCESINT_1_28.mCond()
    this.oPgFrm.Page1.oPag.oVPIMPOR2_1_29.enabled = this.oPgFrm.Page1.oPag.oVPIMPOR2_1_29.mCond()
    this.oPgFrm.Page1.oPag.oVPACQINT_1_30.enabled = this.oPgFrm.Page1.oPag.oVPACQINT_1_30.mCond()
    this.oPgFrm.Page1.oPag.oVPIMPON3_1_31.enabled = this.oPgFrm.Page1.oPag.oVPIMPON3_1_31.mCond()
    this.oPgFrm.Page1.oPag.oVPIMPOS3_1_32.enabled = this.oPgFrm.Page1.oPag.oVPIMPOS3_1_32.mCond()
    this.oPgFrm.Page1.oPag.oVPIMPOR5_1_33.enabled = this.oPgFrm.Page1.oPag.oVPIMPOR5_1_33.mCond()
    this.oPgFrm.Page1.oPag.oVPIMPOR6_1_34.enabled = this.oPgFrm.Page1.oPag.oVPIMPOR6_1_34.mCond()
    this.oPgFrm.Page1.oPag.oIMPDEB8_1_41.enabled = this.oPgFrm.Page1.oPag.oIMPDEB8_1_41.mCond()
    this.oPgFrm.Page1.oPag.oIMPCRE8_1_42.enabled = this.oPgFrm.Page1.oPag.oIMPCRE8_1_42.mCond()
    this.oPgFrm.Page1.oPag.oIMPDEB9_1_44.enabled = this.oPgFrm.Page1.oPag.oIMPDEB9_1_44.mCond()
    this.oPgFrm.Page1.oPag.oIMPCRE9_1_45.enabled = this.oPgFrm.Page1.oPag.oIMPCRE9_1_45.mCond()
    this.oPgFrm.Page1.oPag.oIMPDEB10_1_47.enabled = this.oPgFrm.Page1.oPag.oIMPDEB10_1_47.mCond()
    this.oPgFrm.Page1.oPag.oIMPCRE10_1_48.enabled = this.oPgFrm.Page1.oPag.oIMPCRE10_1_48.mCond()
    this.oPgFrm.Page1.oPag.oVPIMPO13_1_61.enabled = this.oPgFrm.Page1.oPag.oVPIMPO13_1_61.mCond()
    this.oPgFrm.Page1.oPag.oVPIMPO14_1_63.enabled = this.oPgFrm.Page1.oPag.oVPIMPO14_1_63.mCond()
    this.oPgFrm.Page1.oPag.oVPIMPVER_1_67.enabled = this.oPgFrm.Page1.oPag.oVPIMPVER_1_67.mCond()
    this.oPgFrm.Page2.oPag.oVPVARIMP_2_2.enabled = this.oPgFrm.Page2.oPag.oVPVARIMP_2_2.mCond()
    this.oPgFrm.Page2.oPag.oVPVERNEF_2_4.enabled = this.oPgFrm.Page2.oPag.oVPVERNEF_2_4.mCond()
    this.oPgFrm.Page2.oPag.oVPAR74C5_2_5.enabled = this.oPgFrm.Page2.oPag.oVPAR74C5_2_5.mCond()
    this.oPgFrm.Page2.oPag.oVPAR74C4_2_6.enabled = this.oPgFrm.Page2.oPag.oVPAR74C4_2_6.mCond()
    this.oPgFrm.Page2.oPag.oVPCRERIM_2_11.enabled = this.oPgFrm.Page2.oPag.oVPCRERIM_2_11.mCond()
    this.oPgFrm.Page2.oPag.oVPCREUTI_2_13.enabled = this.oPgFrm.Page2.oPag.oVPCREUTI_2_13.mCond()
    this.oPgFrm.Page2.oPag.oVPDATVER_2_18.enabled = this.oPgFrm.Page2.oPag.oVPDATVER_2_18.mCond()
    this.oPgFrm.Page2.oPag.oCODBAN_2_19.enabled = this.oPgFrm.Page2.oPag.oCODBAN_2_19.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oIMPDEB7_1_38.visible=!this.oPgFrm.Page1.oPag.oIMPDEB7_1_38.mHide()
    this.oPgFrm.Page1.oPag.oIMPCRE7_1_40.visible=!this.oPgFrm.Page1.oPag.oIMPCRE7_1_40.mHide()
    this.oPgFrm.Page1.oPag.oIMPDEB12_1_58.visible=!this.oPgFrm.Page1.oPag.oIMPDEB12_1_58.mHide()
    this.oPgFrm.Page1.oPag.oIMPCRE12_1_59.visible=!this.oPgFrm.Page1.oPag.oIMPCRE12_1_59.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_26.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_73.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_74.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_75.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_76.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_77.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_78.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_79.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_80.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_81.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_83.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_84.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_85.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_16.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_17.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODBAN
  func Link_2_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODBAN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_CODBAN)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BACODABI,BACODCAB";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_CODBAN))
          select BACODBAN,BADESCRI,BACODABI,BACODCAB;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODBAN)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStrODBC(trim(this.w_CODBAN)+"%");

            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BACODABI,BACODCAB";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStr(trim(this.w_CODBAN)+"%");

            select BACODBAN,BADESCRI,BACODABI,BACODCAB;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODBAN) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oCODBAN_2_19'),i_cWhere,'',"Conti correnti",'GSAR_ACL.COC_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BACODABI,BACODCAB";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESCRI,BACODABI,BACODCAB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODBAN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BACODABI,BACODCAB";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_CODBAN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_CODBAN)
            select BACODBAN,BADESCRI,BACODABI,BACODCAB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODBAN = NVL(_Link_.BACODBAN,space(15))
      this.w_DESBAN = NVL(_Link_.BADESCRI,space(35))
      this.w_VPCODAZI = NVL(_Link_.BACODABI,space(5))
      this.w_VPCODCAB = NVL(_Link_.BACODCAB,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CODBAN = space(15)
      endif
      this.w_DESBAN = space(35)
      this.w_VPCODAZI = space(5)
      this.w_VPCODCAB = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODBAN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VPCODAZI
  func Link_2_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_ABI_IDX,3]
    i_lTable = "COD_ABI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_ABI_IDX,2], .t., this.COD_ABI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_ABI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VPCODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VPCODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ABCODABI,ABDESABI";
                   +" from "+i_cTable+" "+i_lTable+" where ABCODABI="+cp_ToStrODBC(this.w_VPCODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ABCODABI',this.w_VPCODAZI)
            select ABCODABI,ABDESABI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VPCODAZI = NVL(_Link_.ABCODABI,space(5))
      this.w_DESABI = NVL(_Link_.ABDESABI,space(80))
    else
      if i_cCtrl<>'Load'
        this.w_VPCODAZI = space(5)
      endif
      this.w_DESABI = space(80)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_ABI_IDX,2])+'\'+cp_ToStr(_Link_.ABCODABI,1)
      cp_ShowWarn(i_cKey,this.COD_ABI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VPCODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VPCODCAB
  func Link_2_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_CAB_IDX,3]
    i_lTable = "COD_CAB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_CAB_IDX,2], .t., this.COD_CAB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_CAB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VPCODCAB) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VPCODCAB)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FICODABI,FICODCAB,FIDESFIL,FIINDIRI,FI___CAP";
                   +" from "+i_cTable+" "+i_lTable+" where FICODCAB="+cp_ToStrODBC(this.w_VPCODCAB);
                   +" and FICODABI="+cp_ToStrODBC(this.w_VPCODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FICODABI',this.w_VPCODAZI;
                       ,'FICODCAB',this.w_VPCODCAB)
            select FICODABI,FICODCAB,FIDESFIL,FIINDIRI,FI___CAP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VPCODCAB = NVL(_Link_.FICODCAB,space(5))
      this.w_DESFIL = NVL(_Link_.FIDESFIL,space(40))
      this.w_INDIRI = NVL(_Link_.FIINDIRI,space(50))
      this.w_CAP = NVL(_Link_.FI___CAP,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_VPCODCAB = space(5)
      endif
      this.w_DESFIL = space(40)
      this.w_INDIRI = space(50)
      this.w_CAP = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_CAB_IDX,2])+'\'+cp_ToStr(_Link_.FICODABI,1)+'\'+cp_ToStr(_Link_.FICODCAB,1)
      cp_ShowWarn(i_cKey,this.COD_CAB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VPCODCAB Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTITOLO_1_25.value==this.w_TITOLO)
      this.oPgFrm.Page1.oPag.oTITOLO_1_25.value=this.w_TITOLO
    endif
    if not(this.oPgFrm.Page1.oPag.oVPIMPOR1_1_27.value==this.w_VPIMPOR1)
      this.oPgFrm.Page1.oPag.oVPIMPOR1_1_27.value=this.w_VPIMPOR1
    endif
    if not(this.oPgFrm.Page1.oPag.oVPCESINT_1_28.value==this.w_VPCESINT)
      this.oPgFrm.Page1.oPag.oVPCESINT_1_28.value=this.w_VPCESINT
    endif
    if not(this.oPgFrm.Page1.oPag.oVPIMPOR2_1_29.value==this.w_VPIMPOR2)
      this.oPgFrm.Page1.oPag.oVPIMPOR2_1_29.value=this.w_VPIMPOR2
    endif
    if not(this.oPgFrm.Page1.oPag.oVPACQINT_1_30.value==this.w_VPACQINT)
      this.oPgFrm.Page1.oPag.oVPACQINT_1_30.value=this.w_VPACQINT
    endif
    if not(this.oPgFrm.Page1.oPag.oVPIMPON3_1_31.value==this.w_VPIMPON3)
      this.oPgFrm.Page1.oPag.oVPIMPON3_1_31.value=this.w_VPIMPON3
    endif
    if not(this.oPgFrm.Page1.oPag.oVPIMPOS3_1_32.value==this.w_VPIMPOS3)
      this.oPgFrm.Page1.oPag.oVPIMPOS3_1_32.value=this.w_VPIMPOS3
    endif
    if not(this.oPgFrm.Page1.oPag.oVPIMPOR5_1_33.value==this.w_VPIMPOR5)
      this.oPgFrm.Page1.oPag.oVPIMPOR5_1_33.value=this.w_VPIMPOR5
    endif
    if not(this.oPgFrm.Page1.oPag.oVPIMPOR6_1_34.value==this.w_VPIMPOR6)
      this.oPgFrm.Page1.oPag.oVPIMPOR6_1_34.value=this.w_VPIMPOR6
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPDEB7_1_38.value==this.w_IMPDEB7)
      this.oPgFrm.Page1.oPag.oIMPDEB7_1_38.value=this.w_IMPDEB7
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPCRE7_1_40.value==this.w_IMPCRE7)
      this.oPgFrm.Page1.oPag.oIMPCRE7_1_40.value=this.w_IMPCRE7
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPDEB8_1_41.value==this.w_IMPDEB8)
      this.oPgFrm.Page1.oPag.oIMPDEB8_1_41.value=this.w_IMPDEB8
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPCRE8_1_42.value==this.w_IMPCRE8)
      this.oPgFrm.Page1.oPag.oIMPCRE8_1_42.value=this.w_IMPCRE8
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPDEB9_1_44.value==this.w_IMPDEB9)
      this.oPgFrm.Page1.oPag.oIMPDEB9_1_44.value=this.w_IMPDEB9
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPCRE9_1_45.value==this.w_IMPCRE9)
      this.oPgFrm.Page1.oPag.oIMPCRE9_1_45.value=this.w_IMPCRE9
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPDEB10_1_47.value==this.w_IMPDEB10)
      this.oPgFrm.Page1.oPag.oIMPDEB10_1_47.value=this.w_IMPDEB10
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPCRE10_1_48.value==this.w_IMPCRE10)
      this.oPgFrm.Page1.oPag.oIMPCRE10_1_48.value=this.w_IMPCRE10
    endif
    if not(this.oPgFrm.Page1.oPag.oVPIMPO11_1_55.value==this.w_VPIMPO11)
      this.oPgFrm.Page1.oPag.oVPIMPO11_1_55.value=this.w_VPIMPO11
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPDEB12_1_58.value==this.w_IMPDEB12)
      this.oPgFrm.Page1.oPag.oIMPDEB12_1_58.value=this.w_IMPDEB12
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPCRE12_1_59.value==this.w_IMPCRE12)
      this.oPgFrm.Page1.oPag.oIMPCRE12_1_59.value=this.w_IMPCRE12
    endif
    if not(this.oPgFrm.Page1.oPag.oVPIMPO13_1_61.value==this.w_VPIMPO13)
      this.oPgFrm.Page1.oPag.oVPIMPO13_1_61.value=this.w_VPIMPO13
    endif
    if not(this.oPgFrm.Page1.oPag.oVPIMPO14_1_63.value==this.w_VPIMPO14)
      this.oPgFrm.Page1.oPag.oVPIMPO14_1_63.value=this.w_VPIMPO14
    endif
    if not(this.oPgFrm.Page1.oPag.oVPIMPO15_1_64.value==this.w_VPIMPO15)
      this.oPgFrm.Page1.oPag.oVPIMPO15_1_64.value=this.w_VPIMPO15
    endif
    if not(this.oPgFrm.Page1.oPag.oVPIMPO16_1_65.value==this.w_VPIMPO16)
      this.oPgFrm.Page1.oPag.oVPIMPO16_1_65.value=this.w_VPIMPO16
    endif
    if not(this.oPgFrm.Page1.oPag.oVPIMPVER_1_67.value==this.w_VPIMPVER)
      this.oPgFrm.Page1.oPag.oVPIMPVER_1_67.value=this.w_VPIMPVER
    endif
    if not(this.oPgFrm.Page1.oPag.oCRERES_1_69.value==this.w_CRERES)
      this.oPgFrm.Page1.oPag.oCRERES_1_69.value=this.w_CRERES
    endif
    if not(this.oPgFrm.Page2.oPag.oVPVARIMP_2_2.RadioValue()==this.w_VPVARIMP)
      this.oPgFrm.Page2.oPag.oVPVARIMP_2_2.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oVPCORTER_2_3.RadioValue()==this.w_VPCORTER)
      this.oPgFrm.Page2.oPag.oVPCORTER_2_3.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oVPVERNEF_2_4.RadioValue()==this.w_VPVERNEF)
      this.oPgFrm.Page2.oPag.oVPVERNEF_2_4.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oVPAR74C5_2_5.RadioValue()==this.w_VPAR74C5)
      this.oPgFrm.Page2.oPag.oVPAR74C5_2_5.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oVPAR74C4_2_6.RadioValue()==this.w_VPAR74C4)
      this.oPgFrm.Page2.oPag.oVPAR74C4_2_6.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oVPOPMEDI_2_8.RadioValue()==this.w_VPOPMEDI)
      this.oPgFrm.Page2.oPag.oVPOPMEDI_2_8.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oVPOPNOIM_2_9.RadioValue()==this.w_VPOPNOIM)
      this.oPgFrm.Page2.oPag.oVPOPNOIM_2_9.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oVPACQBEA_2_10.RadioValue()==this.w_VPACQBEA)
      this.oPgFrm.Page2.oPag.oVPACQBEA_2_10.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oVPCRERIM_2_11.value==this.w_VPCRERIM)
      this.oPgFrm.Page2.oPag.oVPCRERIM_2_11.value=this.w_VPCRERIM
    endif
    if not(this.oPgFrm.Page2.oPag.oVPCREUTI_2_13.value==this.w_VPCREUTI)
      this.oPgFrm.Page2.oPag.oVPCREUTI_2_13.value=this.w_VPCREUTI
    endif
    if not(this.oPgFrm.Page2.oPag.oVPDATVER_2_18.value==this.w_VPDATVER)
      this.oPgFrm.Page2.oPag.oVPDATVER_2_18.value=this.w_VPDATVER
    endif
    if not(this.oPgFrm.Page2.oPag.oCODBAN_2_19.value==this.w_CODBAN)
      this.oPgFrm.Page2.oPag.oCODBAN_2_19.value=this.w_CODBAN
    endif
    if not(this.oPgFrm.Page2.oPag.oVPCODAZI_2_20.value==this.w_VPCODAZI)
      this.oPgFrm.Page2.oPag.oVPCODAZI_2_20.value=this.w_VPCODAZI
    endif
    if not(this.oPgFrm.Page2.oPag.oVPCODCAB_2_21.value==this.w_VPCODCAB)
      this.oPgFrm.Page2.oPag.oVPCODCAB_2_21.value=this.w_VPCODCAB
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_IMPDEB8>=0)  and (.w_IMPCRE8=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIMPDEB8_1_41.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_IMPCRE8>=0)  and (.w_IMPDEB8=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIMPCRE8_1_42.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_IMPDEB9>=0)  and (.w_IMPCRE9=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIMPDEB9_1_44.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_IMPCRE9>=0)  and (.w_IMPDEB9=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIMPCRE9_1_45.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_IMPDEB10>=0)  and (.w_F2DEFI<>'S' AND .w_IMPCRE10=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIMPDEB10_1_47.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_IMPCRE10>=0)  and (.w_F2DEFI<>'S' AND .w_IMPDEB10=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIMPCRE10_1_48.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_VPIMPO11<=.w_CRERES And .w_VPIMPO11>=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oVPIMPO11_1_55.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il credito deve essere minore del credito disponibile e maggiore di zero")
          case   not(.w_VPIMPO13>=0 And .w_VPIMPO13<=.w_IMPDEB12)  and (.w_IMPDEB12>0 And Not (g_TIPDEN='T' And .w_VPPERIOD=4) And .w_VPDICSOC<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oVPIMPO13_1_61.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il credito non pu� superare il debito per il periodo")
          case   not(.w_VPIMPO14=0 OR (.w_VPIMPO14>0 AND (.w_VPIMPO12-(.w_VPIMPO13+.w_IMPDEB10))>0))  and (g_TIPDEN<>'M')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oVPIMPO14_1_63.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Importo interessi per le liquidazioni trimestrali non congruente")
          case   not(.w_VPIMPVER>=0)  and (.w_VPDICSOC<>'S' AND .w_VPIMPO16<>0 AND NOT (g_TIPDEN='T' AND .w_VPPERIOD=4))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oVPIMPVER_1_67.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("L'importo versato non pu� essere negativo")
          case   not(.w_VPCRERIM+.w_VPCREUTI<=cp_ROUND(ABS(.w_VPIMPO12),g_PERPVL))  and (.w_VPDICSOC<>'S' AND .w_VPIMPO12<0 AND (G_TIPDEN='M' OR (G_TIPDEN='T' AND .w_VPPERIOD<4)))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oVPCRERIM_2_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La somma chiesta a rimborso non deve superare il credito del periodo")
          case   not(.w_VPCRERIM+.w_VPCREUTI<=cp_ROUND(ABS(.w_VPIMPO12),g_PERPVL))  and (.w_VPDICSOC<>'S' AND .w_VPIMPO12<0 AND (G_TIPDEN='M' OR (G_TIPDEN='T' AND .w_VPPERIOD<4)))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oVPCREUTI_2_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La somma chiesta a rimborso non deve superare il credito del periodo")
          case   (empty(.w_VPDATVER))  and (.w_VPDICSOC<>'S' AND .w_VPIMPVER>0)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oVPDATVER_2_18.SetFocus()
            i_bnoObbl = !empty(.w_VPDATVER)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gscg_sd2
      * --- Esegue la Stampa
      if i_bRes = .t.
         this.NotifyEvent('Conferma')
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_IMPDEB10 = this.w_IMPDEB10
    this.o_VPIMPO11 = this.w_VPIMPO11
    this.o_VPIMPO12 = this.w_VPIMPO12
    this.o_VPIMPO13 = this.w_VPIMPO13
    this.o_VPIMPO16 = this.w_VPIMPO16
    this.o_VPIMPVER = this.w_VPIMPVER
    this.o_VPEURO19 = this.w_VPEURO19
    return

enddefine

* --- Define pages as container
define class tgscg_sd2Pag1 as StdContainer
  Width  = 611
  height = 467
  stdWidth  = 611
  stdheight = 467
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_17 as StdButton with uid="OBZRZEORUH",left=509, top=422, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare i risultati";
    , HelpContextID = 74036506;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_22 as StdButton with uid="FWTZHDZJVZ",left=560, top=422, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare l'elaborazione";
    , HelpContextID = 66747834;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oTITOLO_1_25 as StdField with uid="XDPXPBCAQQ",rtseq=23,rtrep=.f.,;
    cFormVar = "w_TITOLO", cQueryName = "TITOLO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 5609162,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=604, Left=4, Top=6, InputMask=replicate('X',100)


  add object oObj_1_26 as cp_runprogram with uid="UJDUSKJAVW",left=314, top=478, width=161,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCG_BDV('L')",;
    cEvent = "Conferma",;
    nPag=1;
    , HelpContextID = 103932390

  add object oVPIMPOR1_1_27 as StdField with uid="YCEHNWHNGR",rtseq=24,rtrep=.f.,;
    cFormVar = "w_VPIMPOR1", cQueryName = "VPIMPOR1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Operazioni attive al netto d'IVA",;
    HelpContextID = 1589113,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=116, Top=35, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  func oVPIMPOR1_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPVARIMP='S' And .w_VPDICGRU<>'S')
    endwith
   endif
  endfunc

  add object oVPCESINT_1_28 as StdField with uid="NXGSJKTQLA",rtseq=25,rtrep=.f.,;
    cFormVar = "w_VPCESINT", cQueryName = "VPCESINT",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Operazioni attive derivanti da cessioni intracomunitarie",;
    HelpContextID = 99655510,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=415, Top=35, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  func oVPCESINT_1_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPVARIMP='S' And .w_VPDICGRU<>'S')
    endwith
   endif
  endfunc

  add object oVPIMPOR2_1_29 as StdField with uid="JERCBUZYIT",rtseq=26,rtrep=.f.,;
    cFormVar = "w_VPIMPOR2", cQueryName = "VPIMPOR2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Operazioni passive al netto d'IVA",;
    HelpContextID = 1589112,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=116, Top=55, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  func oVPIMPOR2_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPVARIMP='S' And .w_VPDICGRU<>'S')
    endwith
   endif
  endfunc

  add object oVPACQINT_1_30 as StdField with uid="GFBRSCZXTY",rtseq=27,rtrep=.f.,;
    cFormVar = "w_VPACQINT", cQueryName = "VPACQINT",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Operazioni passive derivanti da acquisti intracomunitari",;
    HelpContextID = 101891926,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=415, Top=55, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  func oVPACQINT_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPVARIMP='S' And .w_VPDICGRU<>'S')
    endwith
   endif
  endfunc

  add object oVPIMPON3_1_31 as StdField with uid="BDMALWJPUT",rtseq=28,rtrep=.f.,;
    cFormVar = "w_VPIMPON3", cQueryName = "VPIMPON3",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Imponibile delle importazioni di oro e argento",;
    HelpContextID = 1589111,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=116, Top=91, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  func oVPIMPON3_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPVARIMP='S' And .w_VPDICGRU<>'S')
    endwith
   endif
  endfunc

  add object oVPIMPOS3_1_32 as StdField with uid="GMQWPTAUJT",rtseq=29,rtrep=.f.,;
    cFormVar = "w_VPIMPOS3", cQueryName = "VPIMPOS3",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Imposta delle importazioni di oro e argento",;
    HelpContextID = 266846345,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=415, Top=91, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  func oVPIMPOS3_1_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPVARIMP='S' And .w_VPDICGRU<>'S')
    endwith
   endif
  endfunc

  add object oVPIMPOR5_1_33 as StdField with uid="DDJORJMKPR",rtseq=30,rtrep=.f.,;
    cFormVar = "w_VPIMPOR5", cQueryName = "VPIMPOR5",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ammontare dell'IVA sulle vendite",;
    HelpContextID = 1589109,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=271, Top=136, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  func oVPIMPOR5_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPDICGRU='S')
    endwith
   endif
  endfunc

  add object oVPIMPOR6_1_34 as StdField with uid="LGFISEIKFP",rtseq=31,rtrep=.f.,;
    cFormVar = "w_VPIMPOR6", cQueryName = "VPIMPOR6",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ammontare complessivo dell'IVA sugli acquisti",;
    HelpContextID = 1589108,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=439, Top=156, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  func oVPIMPOR6_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPDICGRU='S')
    endwith
   endif
  endfunc

  add object oIMPDEB7_1_38 as StdField with uid="SPFZLMSAKN",rtseq=33,rtrep=.f.,;
    cFormVar = "w_IMPDEB7", cQueryName = "IMPDEB7",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Differenza tra IVA esigibile e IVA detraibile",;
    HelpContextID = 36646022,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=271, Top=176, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  func oIMPDEB7_1_38.mHide()
    with this.Parent.oContained
      return (.w_VPIMPOR7<0)
    endwith
  endfunc

  add object oIMPCRE7_1_40 as StdField with uid="SEAPHSAMZQ",rtseq=34,rtrep=.f.,;
    cFormVar = "w_IMPCRE7", cQueryName = "IMPCRE7",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Differenza tra IVA esigibile e IVA detraibile",;
    HelpContextID = 100543622,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=439, Top=176, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  func oIMPCRE7_1_40.mHide()
    with this.Parent.oContained
      return (.w_VPIMPOR7>=0)
    endwith
  endfunc

  add object oIMPDEB8_1_41 as StdField with uid="CSFNYRMAWZ",rtseq=35,rtrep=.f.,;
    cFormVar = "w_IMPDEB8", cQueryName = "IMPDEB8",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Variazioni d'imposta non comprese nell'IVA esigibile/detraibile sopra indicata",;
    HelpContextID = 36646022,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=271, Top=196, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  func oIMPDEB8_1_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IMPCRE8=0)
    endwith
   endif
  endfunc

  func oIMPDEB8_1_41.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_IMPDEB8>=0)
    endwith
    return bRes
  endfunc

  add object oIMPCRE8_1_42 as StdField with uid="EHOLMASDFC",rtseq=36,rtrep=.f.,;
    cFormVar = "w_IMPCRE8", cQueryName = "IMPCRE8",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Variazioni d'imposta non comprese nei righi VP10 e VP11",;
    HelpContextID = 100543622,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=439, Top=196, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  func oIMPCRE8_1_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IMPDEB8=0)
    endwith
   endif
  endfunc

  func oIMPCRE8_1_42.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_IMPCRE8>=0)
    endwith
    return bRes
  endfunc

  add object oIMPDEB9_1_44 as StdField with uid="HWBRXQYOHA",rtseq=38,rtrep=.f.,;
    cFormVar = "w_IMPDEB9", cQueryName = "IMPDEB9",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "IVA non versata gi� indicata nell'importo da versare di una dichiarazione precedente",;
    HelpContextID = 36646022,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=271, Top=216, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  func oIMPDEB9_1_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IMPCRE9=0)
    endwith
   endif
  endfunc

  func oIMPDEB9_1_44.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_IMPDEB9>=0)
    endwith
    return bRes
  endfunc

  add object oIMPCRE9_1_45 as StdField with uid="WNWUBHSMJK",rtseq=39,rtrep=.f.,;
    cFormVar = "w_IMPCRE9", cQueryName = "IMPCRE9",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "IVA versata in eccesso gi� indicata nell'importo da versare di una dichiarazione precedente",;
    HelpContextID = 100543622,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=439, Top=216, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  func oIMPCRE9_1_45.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IMPDEB9=0)
    endwith
   endif
  endfunc

  func oIMPCRE9_1_45.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_IMPCRE9>=0)
    endwith
    return bRes
  endfunc

  add object oIMPDEB10_1_47 as StdField with uid="KRAZPMOFLH",rtseq=41,rtrep=.f.,;
    cFormVar = "w_IMPDEB10", cQueryName = "IMPDEB10",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Debito riportato dal periodo precedente",;
    HelpContextID = 231789386,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=271, Top=237, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  func oIMPDEB10_1_47.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_F2DEFI<>'S' AND .w_IMPCRE10=0)
    endwith
   endif
  endfunc

  func oIMPDEB10_1_47.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_IMPDEB10>=0)
    endwith
    return bRes
  endfunc

  add object oIMPCRE10_1_48 as StdField with uid="KTOBCQHFAQ",rtseq=42,rtrep=.f.,;
    cFormVar = "w_IMPCRE10", cQueryName = "IMPCRE10",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Credito riportato dal periodo precedente",;
    HelpContextID = 167891786,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=439, Top=237, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  func oIMPCRE10_1_48.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_F2DEFI<>'S' AND .w_IMPDEB10=0)
    endwith
   endif
  endfunc

  func oIMPCRE10_1_48.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_IMPCRE10>=0)
    endwith
    return bRes
  endfunc

  add object oVPIMPO11_1_55 as StdField with uid="GRJCBCYOES",rtseq=47,rtrep=.f.,;
    cFormVar = "w_VPIMPO11", cQueryName = "VPIMPO11",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il credito deve essere minore del credito disponibile e maggiore di zero",;
    ToolTipText = "Credito IVA dell'anno precedente compensato nel periodo",;
    HelpContextID = 1589113,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=439, Top=258, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  proc oVPIMPO11_1_55.mBefore
      with this.Parent.oContained
        GSCG_BDV(this.Parent.oContained,"_BEFOREVP11")
      endwith
  endproc

  proc oVPIMPO11_1_55.mAfter
      with this.Parent.oContained
        GSCG_BDV(this.Parent.oContained,"_AFTERVP11")
      endwith
  endproc

  func oVPIMPO11_1_55.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_VPIMPO11<=.w_CRERES And .w_VPIMPO11>=0)
    endwith
    return bRes
  endfunc

  add object oIMPDEB12_1_58 as StdField with uid="YLYANSOVPA",rtseq=49,rtrep=.f.,;
    cFormVar = "w_IMPDEB12", cQueryName = "IMPDEB12",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Somma algebrica dei righi precedenti",;
    HelpContextID = 231789384,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=271, Top=278, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  func oIMPDEB12_1_58.mHide()
    with this.Parent.oContained
      return (.w_VPIMPO12<0)
    endwith
  endfunc

  add object oIMPCRE12_1_59 as StdField with uid="EZZRCODZHX",rtseq=50,rtrep=.f.,;
    cFormVar = "w_IMPCRE12", cQueryName = "IMPCRE12",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Somma algebrica dei righi precedenti",;
    HelpContextID = 167891784,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=439, Top=278, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  func oIMPCRE12_1_59.mHide()
    with this.Parent.oContained
      return (.w_VPIMPO12>=0)
    endwith
  endfunc

  add object oVPIMPO13_1_61 as StdField with uid="ALXFZKRLSF",rtseq=51,rtrep=.f.,;
    cFormVar = "w_VPIMPO13", cQueryName = "VPIMPO13",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il credito non pu� superare il debito per il periodo",;
    ToolTipText = "Ammontare dei particolari crediti di imposta utilizzati nel periodo",;
    HelpContextID = 1589111,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=439, Top=298, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  func oVPIMPO13_1_61.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IMPDEB12>0 And Not (g_TIPDEN='T' And .w_VPPERIOD=4) And .w_VPDICSOC<>'S')
    endwith
   endif
  endfunc

  func oVPIMPO13_1_61.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_VPIMPO13>=0 And .w_VPIMPO13<=.w_IMPDEB12)
    endwith
    return bRes
  endfunc

  add object oVPIMPO14_1_63 as StdField with uid="VAHMDBVVQB",rtseq=52,rtrep=.f.,;
    cFormVar = "w_VPIMPO14", cQueryName = "VPIMPO14",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Importo interessi per le liquidazioni trimestrali non congruente",;
    ToolTipText = "Interessi dovuti per liquidazioni trimestrali",;
    HelpContextID = 1589110,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=271, Top=318, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  func oVPIMPO14_1_63.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_TIPDEN<>'M')
    endwith
   endif
  endfunc

  func oVPIMPO14_1_63.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_VPIMPO14=0 OR (.w_VPIMPO14>0 AND (.w_VPIMPO12-(.w_VPIMPO13+.w_IMPDEB10))>0))
    endwith
    return bRes
  endfunc

  add object oVPIMPO15_1_64 as StdField with uid="ONJGXRIWMI",rtseq=53,rtrep=.f.,;
    cFormVar = "w_VPIMPO15", cQueryName = "VPIMPO15",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Acconto versato relativo alla liquidazione dell'ultimo periodo dell'anno",;
    HelpContextID = 1589109,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=439, Top=338, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  add object oVPIMPO16_1_65 as StdField with uid="IZWVTDCWON",rtseq=54,rtrep=.f.,;
    cFormVar = "w_VPIMPO16", cQueryName = "VPIMPO16",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo da versare o da trasferire",;
    HelpContextID = 1589108,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=225, Top=368, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  add object oVPIMPVER_1_67 as StdField with uid="IPSCJIFUTX",rtseq=55,rtrep=.f.,;
    cFormVar = "w_VPIMPVER", cQueryName = "VPIMPVER",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "L'importo versato non pu� essere negativo",;
    ToolTipText = "Importo del versamento riportato sul modello F24",;
    HelpContextID = 115851432,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=125, Top=397, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  func oVPIMPVER_1_67.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPDICSOC<>'S' AND .w_VPIMPO16<>0 AND NOT (g_TIPDEN='T' AND .w_VPPERIOD=4))
    endwith
   endif
  endfunc

  func oVPIMPVER_1_67.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_VPIMPVER>=0)
    endwith
    return bRes
  endfunc

  add object oCRERES_1_69 as StdField with uid="BOEQBQAHYJ",rtseq=57,rtrep=.f.,;
    cFormVar = "w_CRERES", cQueryName = "CRERES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Credito IVA dell'anno precedente ancora utilizzabile",;
    HelpContextID = 214138586,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=415, Top=397, cSayPict="v_PV(20)", cGetPict="v_GV(20)"


  add object oObj_1_73 as cp_calclbl with uid="LPMLOQSWLH",left=9, top=35, width=100,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="Op.Attive",Fontbold=.t.,;
    nPag=1;
    , HelpContextID = 103932390


  add object oObj_1_74 as cp_calclbl with uid="URUAFHFTOZ",left=9, top=55, width=100,height=15,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="Op.Passive",Fontbold=.t.,;
    nPag=1;
    , HelpContextID = 103932390


  add object oObj_1_75 as cp_calclbl with uid="KMAMMMQTPJ",left=297, top=35, width=116,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="di cui Cessioni Intra",Fontbold=.t.,;
    nPag=1;
    , HelpContextID = 103932390


  add object oObj_1_76 as cp_calclbl with uid="ZCFPYDSKNS",left=297, top=55, width=113,height=17,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="di cui Acquisti Intra",Fontbold=.t.,;
    nPag=1;
    , HelpContextID = 103932390


  add object oObj_1_77 as cp_calclbl with uid="KIUYFSPXAZ",left=9, top=136, width=203,height=13,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="Iva Esigibile per il Periodo",Fontbold=.t.,;
    nPag=1;
    , HelpContextID = 103932390


  add object oObj_1_78 as cp_calclbl with uid="ZSMRFWQNFZ",left=9, top=156, width=203,height=13,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="Iva che si detrae per il Periodo",Fontbold=.t.,;
    nPag=1;
    , HelpContextID = 103932390


  add object oObj_1_79 as cp_calclbl with uid="UAXEQEIZSK",left=9, top=196, width=203,height=15,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="Variazioni di Imposta",Fontbold=.t.,;
    nPag=1;
    , HelpContextID = 103932390


  add object oObj_1_80 as cp_calclbl with uid="NTRPYVCLYF",left=9, top=258, width=208,height=13,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="Credito Iva Compensabile",Fontbold=.t.,;
    nPag=1;
    , HelpContextID = 103932390


  add object oObj_1_81 as cp_calclbl with uid="TUDVQIAFYW",left=9, top=397, width=119,height=15,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="Versamento",Fontbold=.t.,;
    nPag=1;
    , HelpContextID = 103932390


  add object oObj_1_83 as cp_calclbl with uid="ENBQUAESLN",left=9, top=91, width=100,height=15,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="Op.Passive",Fontbold=.t.,;
    nPag=1;
    , HelpContextID = 103932390


  add object oObj_1_84 as cp_calclbl with uid="XSOCYONAUC",left=333, top=91, width=78,height=15,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="Op.Passive",Fontbold=.t.,;
    nPag=1;
    , HelpContextID = 103932390


  add object oObj_1_85 as cp_calclbl with uid="TZJBAKOIBK",left=9, top=216, width=208,height=15,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="Iva non versata/in eccesso",Fontbold=.t.,;
    nPag=1;
    , HelpContextID = 103932390

  add object oStr_1_36 as StdString with uid="GMZESXGIRV",Visible=.t., Left=271, Top=113,;
    Alignment=0, Width=163, Height=15,;
    Caption="DEBITI"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_37 as StdString with uid="KFQADRSQUB",Visible=.t., Left=439, Top=112,;
    Alignment=0, Width=160, Height=15,;
    Caption="CREDITI"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_39 as StdString with uid="REXHGNVSAQ",Visible=.t., Left=9, Top=176,;
    Alignment=0, Width=227, Height=18,;
    Caption="Debito o credito per il periodo"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_50 as StdString with uid="ZBMNXRNWAF",Visible=.t., Left=9, Top=237,;
    Alignment=0, Width=213, Height=18,;
    Caption="Debito/credito riportato"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_52 as StdString with uid="URZOGZDSTS",Visible=.t., Left=9, Top=278,;
    Alignment=0, Width=260, Height=18,;
    Caption="IVA dovuta/credito per il periodo"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_57 as StdString with uid="UVPQJQHTSV",Visible=.t., Left=9, Top=298,;
    Alignment=0, Width=212, Height=18,;
    Caption="Crediti speciali"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_60 as StdString with uid="HSEQFDOYWY",Visible=.t., Left=9, Top=318,;
    Alignment=0, Width=212, Height=18,;
    Caption="Interessi dovuti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_62 as StdString with uid="HHCDJUDDZE",Visible=.t., Left=9, Top=338,;
    Alignment=0, Width=212, Height=18,;
    Caption="Acconto versato"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_66 as StdString with uid="WKEWYLEUTZ",Visible=.t., Left=9, Top=368,;
    Alignment=0, Width=212, Height=18,;
    Caption="Importo da versare"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_70 as StdString with uid="HUYVXDWZNZ",Visible=.t., Left=463, Top=379,;
    Alignment=0, Width=114, Height=15,;
    Caption="Credito utilizzabile"  ;
  , bGlobalFont=.t.

  add object oStr_1_82 as StdString with uid="UBHQNKODBV",Visible=.t., Left=8, Top=423,;
    Alignment=0, Width=303, Height=13,;
    Caption="In blue le descrizioni associate ad importi editabili."  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_71 as StdBox with uid="ZIMOQCIOTB",left=10, top=361, width=584,height=2

  add object oBox_1_72 as StdBox with uid="COOYXIQIBU",left=12, top=130, width=593,height=1
enddefine
define class tgscg_sd2Pag2 as StdContainer
  Width  = 611
  height = 467
  stdWidth  = 611
  stdheight = 467
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oVPVARIMP_2_2 as StdCheck with uid="IRWVKWSNAW",rtseq=60,rtrep=.f.,left=11, top=26, caption="Nel periodo sono comprese variazioni di imponibile relative a periodi precedenti",;
    ToolTipText = "Variazione di imponibile periodi precedenti comprese nel periodo",;
    HelpContextID = 100888410,;
    cFormVar="w_VPVARIMP", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oVPVARIMP_2_2.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oVPVARIMP_2_2.GetRadio()
    this.Parent.oContained.w_VPVARIMP = this.RadioValue()
    return .t.
  endfunc

  func oVPVARIMP_2_2.SetRadio()
    this.Parent.oContained.w_VPVARIMP=trim(this.Parent.oContained.w_VPVARIMP)
    this.value = ;
      iif(this.Parent.oContained.w_VPVARIMP=='S',1,;
      0)
  endfunc

  func oVPVARIMP_2_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPDICGRU<>'S')
    endwith
   endif
  endfunc

  add object oVPCORTER_2_3 as StdCheck with uid="WGLLEOVLFL",rtseq=61,rtrep=.f.,left=11, top=47, caption="Correttiva nei termini",;
    ToolTipText = "Se attivo: dichiarazione correttiva nei termini",;
    HelpContextID = 84500648,;
    cFormVar="w_VPCORTER", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oVPCORTER_2_3.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oVPCORTER_2_3.GetRadio()
    this.Parent.oContained.w_VPCORTER = this.RadioValue()
    return .t.
  endfunc

  func oVPCORTER_2_3.SetRadio()
    this.Parent.oContained.w_VPCORTER=trim(this.Parent.oContained.w_VPCORTER)
    this.value = ;
      iif(this.Parent.oContained.w_VPCORTER=='S',1,;
      0)
  endfunc

  add object oVPVERNEF_2_4 as StdCheck with uid="XWETPYPOMX",rtseq=62,rtrep=.f.,left=11, top=68, caption="Versamento non effettuato a seguito di agevolazioni per eventi eccezionali",;
    HelpContextID = 251695260,;
    cFormVar="w_VPVERNEF", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oVPVERNEF_2_4.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oVPVERNEF_2_4.GetRadio()
    this.Parent.oContained.w_VPVERNEF = this.RadioValue()
    return .t.
  endfunc

  func oVPVERNEF_2_4.SetRadio()
    this.Parent.oContained.w_VPVERNEF=trim(this.Parent.oContained.w_VPVERNEF)
    this.value = ;
      iif(this.Parent.oContained.w_VPVERNEF=='S',1,;
      0)
  endfunc

  func oVPVERNEF_2_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPDICSOC<>'S' And .w_VPIMPVER=0)
    endwith
   endif
  endfunc

  add object oVPAR74C5_2_5 as StdCheck with uid="UATPFQYAGN",rtseq=63,rtrep=.f.,left=11, top=89, caption="Subfornitori che si sono avvalsi delle agevolazioni di cui all'art.74, comma 5",;
    HelpContextID = 212057973,;
    cFormVar="w_VPAR74C5", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oVPAR74C5_2_5.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oVPAR74C5_2_5.GetRadio()
    this.Parent.oContained.w_VPAR74C5 = this.RadioValue()
    return .t.
  endfunc

  func oVPAR74C5_2_5.SetRadio()
    this.Parent.oContained.w_VPAR74C5=trim(this.Parent.oContained.w_VPAR74C5)
    this.value = ;
      iif(this.Parent.oContained.w_VPAR74C5=='S',1,;
      0)
  endfunc

  func oVPAR74C5_2_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPDICSOC<>'S')
    endwith
   endif
  endfunc

  add object oVPAR74C4_2_6 as StdCheck with uid="CZFHSSAWDI",rtseq=64,rtrep=.f.,left=11, top=110, caption="Contribuenti con liquidazioni trimestrali (art.74, comma 4)",;
    HelpContextID = 212057974,;
    cFormVar="w_VPAR74C4", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oVPAR74C4_2_6.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oVPAR74C4_2_6.GetRadio()
    this.Parent.oContained.w_VPAR74C4 = this.RadioValue()
    return .t.
  endfunc

  func oVPAR74C4_2_6.SetRadio()
    this.Parent.oContained.w_VPAR74C4=trim(this.Parent.oContained.w_VPAR74C4)
    this.value = ;
      iif(this.Parent.oContained.w_VPAR74C4=='S',1,;
      0)
  endfunc

  func oVPAR74C4_2_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPDICSOC<>'S')
    endwith
   endif
  endfunc

  add object oVPOPMEDI_2_8 as StdCheck with uid="YFAOBVTFPB",rtseq=65,rtrep=.f.,left=11, top=166, caption="Aliquota media",;
    HelpContextID = 96149663,;
    cFormVar="w_VPOPMEDI", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oVPOPMEDI_2_8.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oVPOPMEDI_2_8.GetRadio()
    this.Parent.oContained.w_VPOPMEDI = this.RadioValue()
    return .t.
  endfunc

  func oVPOPMEDI_2_8.SetRadio()
    this.Parent.oContained.w_VPOPMEDI=trim(this.Parent.oContained.w_VPOPMEDI)
    this.value = ;
      iif(this.Parent.oContained.w_VPOPMEDI=='S',1,;
      0)
  endfunc

  add object oVPOPNOIM_2_9 as StdCheck with uid="KUTTQVXLCF",rtseq=66,rtrep=.f.,left=139, top=166, caption="Operazioni non imponibili",;
    HelpContextID = 264970403,;
    cFormVar="w_VPOPNOIM", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oVPOPNOIM_2_9.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oVPOPNOIM_2_9.GetRadio()
    this.Parent.oContained.w_VPOPNOIM = this.RadioValue()
    return .t.
  endfunc

  func oVPOPNOIM_2_9.SetRadio()
    this.Parent.oContained.w_VPOPNOIM=trim(this.Parent.oContained.w_VPOPNOIM)
    this.value = ;
      iif(this.Parent.oContained.w_VPOPNOIM=='S',1,;
      0)
  endfunc

  add object oVPACQBEA_2_10 as StdCheck with uid="PGQTKYERVP",rtseq=67,rtrep=.f.,left=324, top=166, caption="Acquisto di beni ammortizzabili",;
    HelpContextID = 49102999,;
    cFormVar="w_VPACQBEA", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oVPACQBEA_2_10.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oVPACQBEA_2_10.GetRadio()
    this.Parent.oContained.w_VPACQBEA = this.RadioValue()
    return .t.
  endfunc

  func oVPACQBEA_2_10.SetRadio()
    this.Parent.oContained.w_VPACQBEA=trim(this.Parent.oContained.w_VPACQBEA)
    this.value = ;
      iif(this.Parent.oContained.w_VPACQBEA=='S',1,;
      0)
  endfunc

  add object oVPCRERIM_2_11 as StdField with uid="EIKUPNXXDW",rtseq=68,rtrep=.f.,;
    cFormVar = "w_VPCRERIM", cQueryName = "VPCRERIM",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "La somma chiesta a rimborso non deve superare il credito del periodo",;
    ToolTipText = "Importo rimborsato",;
    HelpContextID = 37511331,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=231, Top=205, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  func oVPCRERIM_2_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPDICSOC<>'S' AND .w_VPIMPO12<0 AND (G_TIPDEN='M' OR (G_TIPDEN='T' AND .w_VPPERIOD<4)))
    endwith
   endif
  endfunc

  func oVPCRERIM_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_VPCRERIM+.w_VPCREUTI<=cp_ROUND(ABS(.w_VPIMPO12),g_PERPVL))
    endwith
    return bRes
  endfunc

  add object oVPCREUTI_2_13 as StdField with uid="MGUUQKOBGU",rtseq=70,rtrep=.f.,;
    cFormVar = "w_VPCREUTI", cQueryName = "VPCREUTI",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "La somma chiesta a rimborso non deve superare il credito del periodo",;
    ToolTipText = "Credito da utilizzare in compensazione con mod. F24",;
    HelpContextID = 87842975,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=373, Top=235, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  func oVPCREUTI_2_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPDICSOC<>'S' AND .w_VPIMPO12<0 AND (G_TIPDEN='M' OR (G_TIPDEN='T' AND .w_VPPERIOD<4)))
    endwith
   endif
  endfunc

  func oVPCREUTI_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_VPCRERIM+.w_VPCREUTI<=cp_ROUND(ABS(.w_VPIMPO12),g_PERPVL))
    endwith
    return bRes
  endfunc


  add object oObj_2_16 as cp_calclbl with uid="RFYWSQPBKK",left=17, top=209, width=209,height=15,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="Credito chiesto a rimborso:",Fontbold=.t.,;
    nPag=2;
    , HelpContextID = 103932390


  add object oObj_2_17 as cp_calclbl with uid="EMTDOYLAAL",left=17, top=239, width=351,height=15,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="Credito da utilizzare in compensazione mod. F24",Fontbold=.t.,;
    nPag=2;
    , HelpContextID = 103932390

  add object oVPDATVER_2_18 as StdField with uid="OKHRTOXIAR",rtseq=73,rtrep=.f.,;
    cFormVar = "w_VPDATVER", cQueryName = "VPDATVER",;
    bObbl = .t. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data del versamento",;
    HelpContextID = 119238824,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=117, Top=304

  func oVPDATVER_2_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPDICSOC<>'S' AND .w_VPIMPVER>0)
    endwith
   endif
  endfunc

  add object oCODBAN_2_19 as StdField with uid="PEPLDOWAVR",rtseq=74,rtrep=.f.,;
    cFormVar = "w_CODBAN", cQueryName = "CODBAN",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "C.corrente su cui appoggiare il versamento",;
    HelpContextID = 34836954,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=346, Top=304, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", oKey_1_1="BACODBAN", oKey_1_2="this.w_CODBAN"

  func oCODBAN_2_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPDICSOC<>'S' AND .w_VPIMPVER>0)
    endwith
   endif
  endfunc

  func oCODBAN_2_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODBAN_2_19.ecpDrop(oSource)
    this.Parent.oContained.link_2_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODBAN_2_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oCODBAN_2_19'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Conti correnti",'GSAR_ACL.COC_MAST_VZM',this.parent.oContained
  endproc

  add object oVPCODAZI_2_20 as StdField with uid="VKIDGSGODN",rtseq=75,rtrep=.f.,;
    cFormVar = "w_VPCODAZI", cQueryName = "VPCODAZI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice ABI del conto",;
    HelpContextID = 19488927,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=273, Top=339, InputMask=replicate('X',5), cLinkFile="COD_ABI", cZoomOnZoom="GSAR_ABI", oKey_1_1="ABCODABI", oKey_1_2="this.w_VPCODAZI"

  func oVPCODAZI_2_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_VPCODCAB)
        bRes2=.link_2_21('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oVPCODCAB_2_21 as StdField with uid="PMFZTAFFHO",rtseq=76,rtrep=.f.,;
    cFormVar = "w_VPCODCAB", cQueryName = "VPCODCAB",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice CAB del conto",;
    HelpContextID = 215392104,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=408, Top=339, InputMask=replicate('X',5), cLinkFile="COD_CAB", cZoomOnZoom="GSAR_AFI", oKey_1_1="FICODABI", oKey_1_2="this.w_VPCODAZI", oKey_2_1="FICODCAB", oKey_2_2="this.w_VPCODCAB"

  func oVPCODCAB_2_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oStr_2_7 as StdString with uid="WEPSJZJNXM",Visible=.t., Left=11, Top=142,;
    Alignment=0, Width=535, Height=15,;
    Caption="Dati per rimborso o compensazione infrannuale (art.30, 3)"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_22 as StdString with uid="SXAFPUPNED",Visible=.t., Left=220, Top=304,;
    Alignment=1, Width=123, Height=18,;
    Caption="Conto corrente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_23 as StdString with uid="XFZRNSKFYY",Visible=.t., Left=111, Top=339,;
    Alignment=1, Width=159, Height=18,;
    Caption="Codice azienda:"  ;
  , bGlobalFont=.t.

  add object oStr_2_24 as StdString with uid="VPPWPKUTCR",Visible=.t., Left=355, Top=339,;
    Alignment=1, Width=50, Height=18,;
    Caption="CAB:"  ;
  , bGlobalFont=.t.

  add object oStr_2_25 as StdString with uid="DCECZMFIBK",Visible=.t., Left=5, Top=304,;
    Alignment=1, Width=109, Height=18,;
    Caption="Data versamento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_26 as StdString with uid="PEAEHKDLJV",Visible=.t., Left=17, Top=272,;
    Alignment=0, Width=570, Height=18,;
    Caption="Estremi del versamento"  ;
  , bGlobalFont=.t.

  add object oBox_2_27 as StdBox with uid="KJUOOZNRVU",left=14, top=291, width=575,height=0
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_sd2','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
