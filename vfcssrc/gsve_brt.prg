* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_brt                                                        *
*              Carica rate scadenze                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_63]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-08                                                      *
* Last revis.: 2012-07-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_brt",oParentObject)
return(i_retval)

define class tgsve_brt as StdBatch
  * --- Local variables
  w_IMPRAT = 0
  w_APPO = 0
  w_AGGRAT = .f.
  w_MESS = space(10)
  w_OK = .f.
  w_GSVE_MRT = .NULL.
  w_GEST = .NULL.
  w_OKRATE = .f.
  w_LOOP = 0
  * --- WorkFile variables
  COC_MAST_idx=0
  BAN_CHE_idx=0
  MOD_PAGA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carica temporaneo Rate Scadenze da Documenti (da GSVE_MRT)
    this.w_GSVE_MRT = this.oParentObject
    this.w_GEST = this.oParentObject.oParentObject
    this.w_OK = .T.
    * --- OKRATE: Se .F. significa che sono state create delle rate altrimenti devo fare comunque la InitRow
    this.w_OKRATE = .T.
    this.w_AGGRAT = .F.
    this.w_GSVE_MRT.MarkPos()     
     
 Select ( this.w_GSVE_MRT.cTrsName ) 
 Go Top
    * --- Prima Pero' verifica che non siano state Scritte Rate
    this.w_IMPRAT = 0
    SCAN FOR t_RSNUMRAT<>0 AND (NOT EMPTY(t_RSDATRAT)) AND NOT DELETED()
    this.w_OK = .F.
    this.w_IMPRAT = this.w_IMPRAT + t_RSIMPRAT
    ENDSCAN
    Go Top
    * --- Occore testare oltre che il totale delle scadenze non cambi anche che il pagamento non cambi !!
    if ( Not this.w_OK AND this.w_IMPRAT <> RA[1000, 2] ) OR this.oParentObject.w_AGGIORNO
      if RA[1000, 5] = "D" OR this.oParentObject.w_AGGIORNO
        * --- Se i Totali delle Rate non coincidono con il Documento Ricalcola Sempre
        this.w_OK = .T.
      else
        * --- Scadenze Confermate, non ricalcolabili
        if this.w_GEST.w_MVFLSCAF="S"
          this.w_MESS = "Importi rate scadenze incongruenti con il totale documento%0Allineare manualmente gli importi scadenze con il totale documento"
          ah_ErrorMsg(this.w_MESS)
          this.w_GEST.w_RESCHK = -1
          this.w_OK = .F.
        else
          this.w_OK = ah_YesNo("Importi rate scadenze incongruenti con il totale documento%0Aggiorno le rate scadenze?")
          this.w_AGGRAT = this.w_OK
          if Not this.w_OK AND this.w_GEST.w_MVFLVEAC="V"
            this.w_MESS = "Allineare manualmente gli importi scadenze con il totale documento"
            ah_ErrorMsg(this.w_MESS)
            this.w_GEST.w_RESCHK = -1
          endif
        endif
      endif
      this.oParentObject.w_AGGIORNO = .F.
    endif
    * --- Azzera il Transitorio
    this.w_APPO = 0
    if this.w_OK
      * --- Azzera il Transitorio
      SELECT ( this.w_GSVE_MRT.cTrsName )
      if this.w_GSVE_MRT.cFunction="Load"
        if RecCount( this.w_GSVE_MRT.cTrsName )<>0
          * --- In caricamento posso eliminare direttamente i record
          Zap
          * --- Se elimino tutto ricreo un record vuoto e mi ci posiziono...
          this.w_GSVE_MRT.InitRow()     
          this.w_GSVE_MRT.MarkPos()     
        endif
      else
        Delete All
        this.w_GSVE_MRT.InitRow()     
      endif
      * --- Cicla Sul Vettore delle Rate (definito in GSAR_BFA)
      this.w_LOOP = 1
      do while this.w_LOOP<=999
        if NOT EMPTY(RA[ this.w_LOOP , 1]) AND NOT EMPTY(RA[this.w_LOOP, 2])
          this.w_OKRATE = .F.
          * --- Append
          this.w_GSVE_MRT.AddRow()     
          this.oParentObject.w_RSNUMRAT = this.w_LOOP
          this.oParentObject.w_RSDATRAT = RA[this.w_LOOP, 1]
          this.oParentObject.w_RSIMPRAT = RA[this.w_LOOP, 2]
          this.oParentObject.w_RSMODPAG = RA[this.w_LOOP, 5]
          this.oParentObject.w_RSDESRIG = RA[this.w_LOOP, 11]
          this.oParentObject.w_RSBANNOS = RA[this.w_LOOP, 7]
          if NOT EMPTY(this.oParentObject.w_RSBANNOS)
            * --- Read from COC_MAST
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.COC_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2],.t.,this.COC_MAST_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "BADESCRI,BACONSBF"+;
                " from "+i_cTable+" COC_MAST where ";
                    +"BACODBAN = "+cp_ToStrODBC(this.oParentObject.w_RSBANNOS);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                BADESCRI,BACONSBF;
                from (i_cTable) where;
                    BACODBAN = this.oParentObject.w_RSBANNOS;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_DESBAN = NVL(cp_ToDate(_read_.BADESCRI),cp_NullValue(_read_.BADESCRI))
              this.oParentObject.w_CONSBF = NVL(cp_ToDate(_read_.BACONSBF),cp_NullValue(_read_.BACONSBF))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          this.oParentObject.w_RSBANAPP = RA[this.w_LOOP, 8]
          this.oParentObject.w_RSFLSOSP = RA[this.w_LOOP, 9]
          this.oParentObject.w_RSCONCOR = RA[this.w_LOOP, 10]
          if NOT EMPTY(this.oParentObject.w_RSBANAPP)
            * --- Read from BAN_CHE
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.BAN_CHE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.BAN_CHE_idx,2],.t.,this.BAN_CHE_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "BADESBAN"+;
                " from "+i_cTable+" BAN_CHE where ";
                    +"BACODBAN = "+cp_ToStrODBC(this.oParentObject.w_RSBANAPP);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                BADESBAN;
                from (i_cTable) where;
                    BACODBAN = this.oParentObject.w_RSBANAPP;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_DESBAN1 = NVL(cp_ToDate(_read_.BADESBAN),cp_NullValue(_read_.BADESBAN))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          if NOT EMPTY(this.oParentObject.w_RSMODPAG)
            * --- Read from MOD_PAGA
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.MOD_PAGA_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MOD_PAGA_idx,2],.t.,this.MOD_PAGA_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "MPTIPPAG"+;
                " from "+i_cTable+" MOD_PAGA where ";
                    +"MPCODICE = "+cp_ToStrODBC(this.oParentObject.w_RSMODPAG);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                MPTIPPAG;
                from (i_cTable) where;
                    MPCODICE = this.oParentObject.w_RSMODPAG;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_TIPPAG = NVL(cp_ToDate(_read_.MPTIPPAG),cp_NullValue(_read_.MPTIPPAG))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          this.oParentObject.w_RSFLPROV = RA[this.w_LOOP, 6]
          this.w_APPO = this.w_APPO + this.oParentObject.w_RSIMPRAT
          * --- Carica il Temporaneo dei Dati e skippa al record successivo
          this.w_GSVE_MRT.TrsFromWork()     
        endif
        this.w_LOOP = this.w_LOOP + 1
      enddo
       
 SELECT ( this.w_GSVE_MRT.cTrsName) 
 Go Top
      if this.w_AGGRAT AND this.w_APPO<>RA[1000, 2] AND NOT EMPTY(RA[1, 1]) AND NOT EMPTY(RA[1, 2])
        * --- Se Aggiorno rate riporta la differenza Castelletto sulla prima
        this.w_GSVE_MRT.WorkFromTrs()     
        this.oParentObject.w_RSIMPRAT = RA[1000, 2] - (this.w_APPO - this.oParentObject.w_RSIMPRAT)
        this.w_GSVE_MRT.TrsFromWork()     
        this.w_APPO = RA[1000, 2]
      endif
    endif
    this.w_GSVE_MRT.RePos()     
    * --- Controllo di Congruita' tranne che nell'Import
    if this.w_APPO<>0 AND this.w_APPO<>RA[1000, 2] AND RA[1000, 2]<>-999
      ah_ErrorMsg("Totale documento incongruente con le rate scadenze")
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='COC_MAST'
    this.cWorkTables[2]='BAN_CHE'
    this.cWorkTables[3]='MOD_PAGA'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
