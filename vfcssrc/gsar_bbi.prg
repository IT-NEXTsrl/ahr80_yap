* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bbi                                                        *
*              Stampa brogliaccio INTRA                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_711]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-03-14                                                      *
* Last revis.: 2010-04-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bbi",oParentObject)
return(i_retval)

define class tgsar_bbi as StdBatch
  * --- Local variables
  NumRiga = 0
  UltimoTipo = space(2)
  w_DECOR = 0
  w_Sez1RigC = 0
  w_Sez1RigA = 0
  w_Sez2RigC = 0
  w_Sez2RigA = 0
  w_Sez3RigC = 0
  w_Sez3RigA = 0
  w_Sez4RigC = 0
  w_Sez4RigA = 0
  w_VALUTA = space(3)
  Gruppo = space(10)
  w_ANPARIVA = space(10)
  GruppoExpr = space(10)
  w_NAISOCLF = space(10)
  Record = 0
  w_NAISODES = space(10)
  w_PIIMPNAZ = 0
  w_NAISOPRO = space(10)
  w_PIIMPVAL = 0
  w_NAISOORI = space(10)
  w_PIMASNET = 0
  w_SPMODSPE = space(10)
  w_PIQTASUP = 0
  w_PICONDCO = space(1)
  w_PIVALSTA = 0
  w_TIPOMOV = space(2)
  w_PINOMENC = space(8)
  w_PINATTRA = space(3)
  w_TIPAC = space(1)
  w_DTFINEAC = ctod("  /  /  ")
  w_TOTNAZ = 0
  w_IESELSTA = 0
  w_IESENAZ = 0
  w_PIPAEPAG = space(2)
  w_PITIPRET = space(1)
  w_Rigavalida = 0
  w_NUMFATT = 0
  w_ALFAFATT = space(10)
  w_DATAFATT = ctod("  /  /  ")
  w_PIPERSER = space(6)
  w_PIMODINC = space(1)
  w_PAEPAG = space(3)
  w_PIPAEPAG = space(2)
  w_PISEZDOG = space(6)
  w_PIANNREG = space(4)
  w_PIPRORET = space(6)
  w_PIPROGSE = space(5)
  w_PIDATREG = ctod("  /  /  ")
  * --- WorkFile variables
  AZIENDA_idx=0
  CONTI_idx=0
  ESERCIZI_idx=0
  VALUTE_idx=0
  NOMENCLA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa Brogliaccio INTRA (da GSAR_SBI)
    * --- Variabili della Maschera
    * --- Variabili locali
    this.w_Rigavalida = 0
    this.UltimoTipo = "  "
    this.NumRiga = 0
    this.w_Sez1RigC = 0
    this.w_Sez1RigA = 0
    this.w_Sez2RigC = 0
    this.w_Sez2RigA = 0
    this.w_Sez3RigC = 0
    this.w_Sez3RigA = 0
    this.w_Sez4RigC = 0
    this.w_Sez4RigA = 0
    this.w_VALUTA = g_PERVAL
    * --- Controllo selezioni obbligatorie
    * --- Controllo selezioni obbligatorie
    if (this.oParentObject.w_TIPOGENE $ "EN-CE-EB-CS-ES" .and. empty(this.oParentObject.w_PERICESS)) .or. (this.oParentObject.w_TIPOGENE $ "EN-AC-EB-AS-ES" .and. empty(this.oParentObject.w_PERIACQU))
      ah_errormsg("Specificare il periodo di riferimento",48)
      i_retcode = 'stop'
      return
    endif
    * --- Cursore per la stampa delle righe di dettaglio
    create cursor RIGDETT (PROG N(10), TIPOMOV C(2), STATO C(10), CODIVA C(12),IMPEUR N(18,4),IMPVAL N(18,4),DEC N(1),; 
 NATRA C(3),PERET N(2), ANNRET C(4),SEGNO C(1),NOMENCL C(8), MASSNET N(12,3), UNITSUP N(12,3),; 
 VALSTAT N(18,4), CODCONS C(1), MODTRAS C(1),PAEPRO C(2), PAEORI C(2), PAEDEST C(2),PROVORI C(2), PROVDES C(2),PITIPPER C(1),; 
 NUMFATT N(15,0),ALFAFATT C(10),DATAFATT D(8),PIPERSER C(1),PIMODINC C(1),PIPAEPAG C(2) ,PAEPAG C(3),; 
 PISEZDOG C(6),PIANNREG C(4),PIPRORET C(6),PIPROGSE C(6))
    * --- Crea un cursore temporaneo ordinato per tipo operazione (Ordine: AC - RA - CE -RE)
    vq_exec("query\GSAR2BEF",this,"EleIntra")
    SELECT ELEINTRA
    if Reccount()=0
      ah_ErrorMsg("Per la selezione effettuata non esistono dati da elaborare",,"")
      use
      i_retcode = 'stop'
      return
    endif
    * --- Le rettifiche nello stesso periodo sono da considerare Acquisti/Cessioni (se in meno, in negativo)
    SELECT IIF(PITIPMOV $ "AC-CE-AS-CS", LEFT(PITIPMOV, 1), IIF(PITIPMOV$"RC-RS", "C", "A")) AS SEZIONE, ; 
 iif( PITIPMOV$"AC-CE-RC-RA","1","2") as SEZIONE1,; 
 IIF(PITIPMOV $ "RC-RA" AND NVL(PIANNRET,"    ")=this.oParentObject.w_ANNORIFE AND ( PIPERRET=IIF(PITIPMOV="RA", this.oParentObject.w_PERIACQU, this.oParentObject.w_PERICESS) OR NVL(PITIPPER," ")="A" ), IIF(PITIPMOV="RA", "AC", "CE"), PITIPMOV) AS PITIPMOV, ; 
 IIF(PITIPMOV $ "AC-CE" OR (PITIPMOV $ "RC-RA" AND NVL(PIANNRET,"    ")=this.oParentObject.w_ANNORIFE AND ( PIPERRET=IIF(PITIPMOV="RA", this.oParentObject.w_PERIACQU, this.oParentObject.w_PERICESS) OR NVL(PITIPPER," ")="A" )), "    ", PIANNRET) AS PIANNRET, ; 
 IIF(PITIPMOV $ "AC-CE" OR (PITIPMOV $ "RC-RA" AND NVL(PIANNRET,"    ")=this.oParentObject.w_ANNORIFE AND ( PIPERRET=IIF(PITIPMOV="RA", this.oParentObject.w_PERIACQU, this.oParentObject.w_PERICESS) OR NVL(PITIPPER," ")="A" )), 99*0, PIPERRET) AS PIPERRET, ; 
 ANPARIVA, PINATTRA, PINOMENC, IIF(PICONDCO="N", " ", PICONDCO) AS PICONDCO, PIMODTRA, NAISOPRO, NAISODES, NAISOORI, PIPROORI, PIPRODES, ; 
 PISERIAL, PIDATREG, PI__ANNO, PINUMREG, PIDATCOM, PIDATDOC, PINUMDOC, PIALFDOC, ; 
 PITIPCON, PICODCON, PIVALORI, PIVALNAZ, CPROWORD, PITIPRET, ; 
 PIIMPNAZ * IIF(PITIPMOV $ "RC-RA" AND NVL(PITIPRET," ")="-" AND NVL(PIANNRET,"    ")=this.oParentObject.w_ANNORIFE AND ( PIPERRET=IIF(PITIPMOV="RA", this.oParentObject.w_PERIACQU, this.oParentObject.w_PERICESS) OR NVL(PITIPPER," ")="A" ), -1, 1) AS PIIMPNAZ, ; 
 PIIMPVAL * IIF(PITIPMOV $ "RC-RA" AND NVL(PITIPRET," ")="-" AND NVL(PIANNRET,"    ")=this.oParentObject.w_ANNORIFE AND ( PIPERRET=IIF(PITIPMOV="RA", this.oParentObject.w_PERIACQU, this.oParentObject.w_PERICESS) OR NVL(PITIPPER," ")="A" ), -1, 1) AS PIIMPVAL, ; 
 PIMASNET * IIF(PITIPMOV $ "RC-RA" AND NVL(PITIPRET," ")="-" AND NVL(PIANNRET,"    ")=this.oParentObject.w_ANNORIFE AND ( PIPERRET=IIF(PITIPMOV="RA", this.oParentObject.w_PERIACQU, this.oParentObject.w_PERICESS) OR NVL(PITIPPER," ")="A" ), -1, 1) AS PIMASNET, ; 
 PIQTASUP * IIF(PITIPMOV $ "RC-RA" AND NVL(PITIPRET," ")="-" AND NVL(PIANNRET,"    ")=this.oParentObject.w_ANNORIFE AND ( PIPERRET=IIF(PITIPMOV="RA", this.oParentObject.w_PERIACQU, this.oParentObject.w_PERICESS) OR NVL(PITIPPER," ")="A" ), -1, 1) AS PIQTASUP, ; 
 PIVALSTA * IIF(PITIPMOV $ "RC-RA" AND NVL(PITIPRET," ")="-" AND NVL(PIANNRET,"    ")=this.oParentObject.w_ANNORIFE AND ( PIPERRET=IIF(PITIPMOV="RA", this.oParentObject.w_PERIACQU, this.oParentObject.w_PERICESS) OR NVL(PITIPPER," ")="A" ), -1, 1) AS PIVALSTA, ; 
 PINAZDES, ANNAZION, NAISOCLF, SPMODSPE, SPCODSPE, ; 
 NACODNAZ, NACODNAZ1, NACODNAZ2, NACODNAZ3, NVL(PITIPPER, " ") AS PITIPPER,; 
 PIPERSER,PIMODINC,PIPAEPAG,DATAFATT,NUMFATT,ALFAFATT,VADECTOT,PAEPAG,; 
 PISEZDOG,PIANNREG,PIPRORET,PIPROGSE; 
 FROM EleIntra ; 
 INTO CURSOR EleIntra ORDER BY 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,29,53,54,56,55,49,50,48,45,46,47
    SELECT EleIntra
    GO TOP
    * --- Scrittura righe di dettaglio
    do while .not. eof()
      * --- Quando cambia il tipo movimento azzera il contatore di riga
      if PITIPMOV <> this.UltimoTipo
        this.UltimoTipo = PITIPMOV
        this.NumRiga = 0
      endif
      this.w_PIDATREG = PIDATREG
      this.w_DECOR = Nvl(VADECTOT,0)
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      select EleIntra
      skip
    enddo
    SELECT RIGDETT
    if Reccount()=0
      AH_ERRORMSG("Non � possibile presentare righe a importo nullo",48)
    else
      * --- Stampa Riepilogo Cessioni \ Acquisti
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if used( "ELEINTRA" )
      select eleintra
      use
    endif
    if used("__TMP__")
      select __TMP__
      use
    endif
    if used("RIGDETT")
      select RIGDETT
      use
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elabora Dettaglio
    this.w_TIPOMOV = NVL(PITIPMOV,SPACE(2))
    * --- Gestione dei campi che possono assumere il valore NULL
    this.w_ANPARIVA = iif( isnull( ANPARIVA ), space(12) , alltrim(ANPARIVA) )
    this.w_NAISOCLF = iif( isnull( NAISOCLF ), space(2) , alltrim(NAISOCLF) )
    this.w_NAISODES = iif( isnull( NAISODES ), space(2) , alltrim(NAISODES) )
    this.w_NAISOPRO = iif( isnull( NAISOPRO ), space(2) , alltrim(NAISOPRO) )
    this.w_NAISOORI = iif( isnull( NAISOORI ), space(2) , alltrim(NAISOORI) )
    this.w_PIPAEPAG = iif( isnull( PIPAEPAG ), space(2) , alltrim(PIPAEPAG) )
    this.w_SPMODSPE = iif( isnull( SPMODSPE ), space(2) , alltrim(SPMODSPE) )
    this.w_PICONDCO = iif( isnull( PICONDCO ), space(1) , alltrim(PICONDCO) )
    * --- Raggruppamento righe
    * --- Non � stato gestito a livello di query perch�:
    * --- 1) Varia in base alla periodicit� di presentazione degli elenchi.
    * --- 2) Un'azienda pu� avere periodicit� differenti fra acquisti e cessioni.
    * --- 3) Anche Le rettifiche devono essere raggruppate
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    do case
      case PITIPMOV $ "RC-RA-AC-CE"
        this.w_PINOMENC = alltrim( NVL(PINOMENC,SPACE(8)))
        this.w_PINATTRA = alltrim( NVL(PINATTRA,SPACE(3)))
      case PITIPMOV $ "AS-CS-RS-RX" 
        this.w_PISEZDOG = right( repl("0",6) + alltrim(PISEZDOG) ,6)
        this.w_PIANNREG = PIANNREG
        this.w_PIPRORET = right( repl("0",6) + alltrim(PIPRORET) ,6)
        this.w_PIPROGSE = right( repl("0",5) + alltrim(PIPROGSE) ,5)
        if PITIPMOV $ "RS-RX" And this.w_PIIMPNAZ=0
          this.w_PINOMENC = SPACE(6)
          this.w_NUMFATT = 0
          this.w_ALFAFATT = space(10)
          this.w_DATAFATT = cp_CharToDate("  -  -  ")
          this.w_PIPERSER = space(1)
          this.w_PIMODINC = space(1)
          this.w_PAEPAG = space(3)
          this.w_PIPAEPAG = space(2)
        else
          this.w_PINOMENC = alltrim( NVL(PINOMENC,SPACE(6)))
          this.w_NUMFATT = NUMFATT
          this.w_ALFAFATT = ALFAFATT
          this.w_DATAFATT = CP_TODATE(PIDATDOC)
          this.w_PIPERSER = PIPERSER
          this.w_PIMODINC = PIMODINC
          this.w_PAEPAG = PAEPAG
        endif
    endcase
    * --- Carico cursore per la stampa delle righe di dettaglio
    if this.w_PIIMPNAZ <> 0 or this.w_PIVALSTA<>0 or (this.w_RIGAVALIDA>0 And PITIPMOV $ "RX-RS")
      if this.w_VALUTA = g_PERVAL
        this.w_PIIMPNAZ = cp_Round(iif(cp_round(this.w_PIIMPNAZ,0)=0 And this.w_PIIMPNAZ<>0,1,this.w_PIIMPNAZ),0)
        this.w_PIIMPVAL = cp_Round(iif(cp_round(this.w_PIIMPVAL,0)=0 And this.w_PIIMPVAL<>0,1,this.w_PIIMPVAL),0)
        this.w_PIVALSTA = cp_Round(iif(cp_round(this.w_PIVALSTA,0)=0 And this.w_PIVALSTA<>0,1,this.w_PIVALSTA),0)
        this.w_PIMASNET = cp_Round(iif(cp_round(this.w_PIMASNET,0)=0 And this.w_PIMASNET<>0,1,this.w_PIMASNET),0)
        this.w_PIQTASUP = cp_Round(iif(cp_round(this.w_PIQTASUP,0)=0 And this.w_PIQTASUP<>0,1,this.w_PIQTASUP),0)
      endif
      * --- Incrementa il contatore delle righe della sezione corrente
      this.NumRiga = this.NumRiga + 1
      * --- Carico cursore per la stampa delle righe di dettaglio
      insert into RIGDETT values (this.NumRiga,this.w_TIPOMOV,this.w_NAISOCLF,this.w_ANPARIVA,this.w_PIIMPNAZ,this.w_PIIMPVAL,; 
 this.w_DECOR,this.w_PINATTRA,NVL(ELEINTRA.PIPERRET,0),; 
 NVL(ELEINTRA.PIANNRET," "),NVL(ELEINTRA.PITIPRET,"+"),this.w_PINOMENC,this.w_PIMASNET,this.w_PIQTASUP,this.w_PIVALSTA,; 
 this.w_PICONDCO,this.w_SPMODSPE,this.w_NAISOPRO,this.w_NAISOORI,this.w_NAISODES,; 
 NVL(ELEINTRA.PIPROORI,SPACE(2)),NVL(ELEINTRA.PIPRODES,SPACE(2)), NVL(ELEINTRA.PITIPPER," "),; 
 this.w_NUMFATT,this.w_ALFAFATT,this.w_DATAFATT,this.w_PIPERSER,this.w_PIMODINC,this.w_PIPAEPAG,this.w_PAEPAG,; 
 this.w_PISEZDOG,this.w_PIANNREG,this.w_PIPRORET,this.w_PIPROGSE)
      do case
        case this.w_TIPAC = "CE"
          this.w_Sez1RigC = this.w_Sez1RigC + 1
        case this.w_TIPAC = "RC"
          this.w_Sez2RigC = this.w_Sez2RigC + 1
        case this.w_TIPAC = "AC"
          this.w_Sez1RigA = this.w_Sez1RigA + 1
        case this.w_TIPAC = "RA"
          this.w_Sez2RigA = this.w_Sez2RigA + 1
        case this.w_TIPAC = "CS"
          this.w_Sez3RigC = this.w_Sez3RigC + 1
        case this.w_TIPAC = "AS"
          this.w_Sez3RigA = this.w_Sez3RigA + 1
        case this.w_TIPAC = "RX"
          this.w_Sez4RigA = this.w_Sez4RigA + 1
        case this.w_TIPAC = "RS"
          this.w_Sez4RigC = this.w_Sez4RigC + 1
      endcase
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    L_PIVAZI=this.oParentObject.w_PIVAPRES
    L_ANNO=this.oParentObject.w_AnnoRife
    L_DEC=g_PERPVL
    if this.oParentObject.w_TIPOGENE$"EN-CE-EB" AND (this.w_Sez1RigC<>0 OR this.w_Sez2RigC<>0)
      L_TIPOREPO=AH_MsgFormat("RIEPILOGO DELLE CESSIONI INTRACOMUNITARIE DI BENI E DEI SERVIZI RESI")
      L_TIPOPER=this.oParentObject.w_TIPOCESS
      L_PERIODO=this.oParentObject.w_PERICESS
      * --- Stampa Cessioni
      if this.w_Sez1RigC<>0
        L_TIPOSEZ = AH_MsgFormat("SEZIONE 1 - CESSIONI DI BENI REGISTRATE NEL PERIODO")
        SELECT * FROM RIGDETT INTO CURSOR __TMP__;
        WHERE TIPOMOV = "CE" ORDER BY PROG
        cp_chprn("QUERY\GSAR_SEF","",this.oParentObject)
      endif
      * --- Stampa Rettifiche
      if this.w_Sez2RigC<>0
        L_TIPOSEZ = AH_MsgFormat("SEZIONE 2 - RETTIFICHE ALLE CESSIONI DI BENI RELATIVE A PERIODI PRECEDENTI")
        SELECT * FROM RIGDETT INTO CURSOR __TMP__;
        WHERE TIPOMOV = "RC" ORDER BY PROG
        cp_chprn("QUERY\GSAR1SEF","",this.oParentObject)
      endif
    endif
    if this.oParentObject.w_TIPOGENE$"EN-CS-ES" AND (this.w_Sez3RigC<>0 OR this.w_Sez4RigC<>0)
      L_TIPOREPO=AH_MsgFormat("RIEPILOGO DELLE CESSIONI INTRACOMUNITARIE DI BENI E DEI SERVIZI RESI")
      L_TIPOPER=this.oParentObject.w_TIPOCESS_S
      L_PERIODO=this.oParentObject.w_PERICESS_S
      * --- Stampa Servizi resi
      if this.w_Sez3RigC<>0
        L_TIPOSEZ=AH_MsgFormat("SEZIONE 3 - SERVIZI RESI REGISTRATI NEL PERIODO")
        SELECT * FROM RIGDETT INTO CURSOR __TMP__;
        WHERE TIPOMOV = "CS" ORDER BY PROG
        cp_chprn("QUERY\GSAR3SEF","",this.oParentObject)
      endif
      * --- Stampa Rettifiche Servizi resi
      if this.w_Sez4RigC<>0
        L_TIPOSEZ=AH_MsgFormat("SEZIONE 4 - RETTIFICHE AI SERVIZI RESI INDICATI IN SEZIONE 3 DI PERIODI PRECEDENTI")
        SELECT * FROM RIGDETT INTO CURSOR __TMP__;
        WHERE TIPOMOV = "RS" ORDER BY PROG
        cp_chprn("QUERY\GSAR4SEF","",this.oParentObject)
      endif
    endif
    * --- Stampa Riepilogo Acquisti
    if this.oParentObject.w_TIPOGENE$"EN-AC-EB" AND (this.w_Sez1RigA<>0 OR this.w_Sez2RigA<>0)
      L_TIPOREPO = AH_MsgFormat("RIEPILOGO DEGLI ACQUISTI INTRACOMUNITARI DI BENI E DEI SERVIZI RICEVUTI")
      L_TIPOPER=this.oParentObject.w_TIPOACQU
      L_PERIODO=this.oParentObject.w_PERIACQU
      * --- Stampa Acquisti
      if this.w_Sez1RigA<>0
        L_TIPOSEZ = AH_MsgFormat("SEZIONE 1 - ACQUISTI DI BENI REGISTRATI NEL PERIODO")
        SELECT * FROM RIGDETT INTO CURSOR __TMP__;
        WHERE TIPOMOV = "AC" ORDER BY PROG
        cp_chprn("QUERY\GSARASEF","",this.oParentObject)
      endif
      * --- Stampa Rettifiche
      if this.w_Sez2RigA<>0
        L_TIPOSEZ = AH_MsgFormat("SEZIONE 2 - RETTIFICHE AGLI ACQUISTI DI BENI RELATIVE A PERIODI PRECEDENTI")
        SELECT * FROM RIGDETT INTO CURSOR __TMP__;
        WHERE TIPOMOV = "RA" ORDER BY PROG
        cp_chprn("QUERY\GSAR1ASEF","",this.oParentObject)
      endif
    endif
    if this.oParentObject.w_TIPOGENE$"EN-AS-ES" AND (this.w_Sez3RigA<>0 OR this.w_Sez4RigA<>0)
      L_TIPOREPO = AH_MsgFormat("RIEPILOGO DEGLI ACQUISTI INTRACOMUNITARI DI BENI E DEI SERVIZI RICEVUTI")
      L_TIPOPER=this.oParentObject.w_TIPOACQU_S
      L_PERIODO=this.oParentObject.w_PERIACQU_S
      * --- Stampa Servizi ricevuti
      if this.w_Sez3RigA<>0
        L_TIPOSEZ=AH_MsgFormat("SEZIONE 3 - SERVIZI RICEVUTI REGISTRATI NEL PERIODO")
        SELECT * FROM RIGDETT INTO CURSOR __TMP__;
        WHERE TIPOMOV = "AS" ORDER BY PROG
        cp_chprn("QUERY\GSAR3ASEF","",this.oParentObject)
      endif
      * --- Stampa Rettifica Servizi ricevuti
      if this.w_Sez4RigA<>0
        L_TIPOSEZ=AH_MsgFormat("SEZIONE 4 - RETTIFICHE AI SERVIZI RICEVUTI INDICATI IN SEZIONE 3 DI PERIODI PRECEDENTI")
        SELECT * FROM RIGDETT INTO CURSOR __TMP__;
        WHERE TIPOMOV = "RX" ORDER BY PROG
        cp_chprn("QUERY\GSAR4ASEF","",this.oParentObject)
      endif
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.Gruppo = ""
    do case
      case PITIPMOV $"CE-RC"
        if this.oParentObject.w_TIPOCESS = "M"
          * --- Cessioni mensili
          this.Gruppo = this.w_ANPARIVA+NVL(PINATTRA,SPACE(3))+NVL(PINOMENC,SPACE(8))+NVL(PICONDCO,SPACE(1))
          this.Gruppo = this.Gruppo+NVL(SPMODSPE,SPACE(2))+this.w_NAISODES+NVL(PIPROORI,SPACE(2)) 
          this.Gruppo = this.Gruppo+IIF(PITIPMOV="CE"," ", NVL(PITIPRET," ") + NVL(PIANNRET," ") + STR(NVL(PIPERRET,0)))
          this.GruppoExpr = "this.w_ANPARIVA+NVL(PINATTRA,SPACE(3))+NVL(PINOMENC,SPACE(8))"
          this.GruppoExpr = this.GruppoExpr+"+NVL(PICONDCO,SPACE(1))+NVL(SPMODSPE,SPACE(2))"
          this.GruppoExpr = this.GruppoExpr+"+this.w_NAISODES+NVL(PIPROORI,SPACE(2))"
          this.GruppoExpr = this.GruppoExpr+"+IIF(PITIPMOV='CE',' ', NVL(PITIPRET,' ') + NVL(PIANNRET,' ') + STR(NVL(PIPERRET,0)))"
        else
          * --- Cessioni / Rettifiche trimestrali 
          this.Gruppo = this.w_ANPARIVA+NVL(PINATTRA,SPACE(3))+NVL(PINOMENC,SPACE(8)) 
          this.Gruppo = this.Gruppo+IIF(PITIPMOV="CE"," ", NVL(PITIPRET," ") + NVL(PIANNRET," ") + STR(NVL(PIPERRET,0)))
          this.GruppoExpr = "this.w_ANPARIVA+NVL(PINATTRA,SPACE(3))+NVL(PINOMENC,SPACE(8))"
          this.GruppoExpr = this.GruppoExpr+"+IIF(PITIPMOV='CE',' ', NVL(PITIPRET,' ') + NVL(PIANNRET,' ') + STR(NVL(PIPERRET,0)))"
        endif
      case PITIPMOV $ "AC-RA"
        if this.oParentObject.w_TIPOACQU = "M"
          this.Gruppo = this.w_ANPARIVA+NVL(PINATTRA,SPACE(3))+NVL(PINOMENC,SPACE(8))+NVL(PICONDCO,SPACE(1))
          this.Gruppo = this.Gruppo+NVL(SPMODSPE,SPACE(2))+this.w_NAISOPRO+this.w_NAISOORI+NVL(PIPRODES,SPACE(2)) 
          this.Gruppo = this.Gruppo+IIF(PITIPMOV="AC"," ", NVL(PITIPRET," ") + NVL(PIANNRET," ") + STR(NVL(PIPERRET,0)))
          this.GruppoExpr = "this.w_ANPARIVA+NVL(PINATTRA,SPACE(3))+NVL(PINOMENC,SPACE(8))"
          this.GruppoExpr = this.GruppoExpr+"+NVL(PICONDCO,SPACE(1))+NVL(SPMODSPE,SPACE(2))"
          this.GruppoExpr = this.GruppoExpr+"+this.w_NAISOPRO+this.w_NAISOORI+NVL(PIPRODES,SPACE(2))"
          this.GruppoExpr = this.GruppoExpr+"+IIF(PITIPMOV='AC',' ', NVL(PITIPRET,' ') + NVL(PIANNRET,' ') + STR(NVL(PIPERRET,0)))"
        else
          this.Gruppo = this.w_ANPARIVA+NVL(PINATTRA,SPACE(3))+NVL(PINOMENC,SPACE(8)) 
          this.Gruppo = this.Gruppo+IIF(PITIPMOV="AC"," ", NVL(PITIPRET," ") + NVL(PIANNRET," ") + STR(NVL(PIPERRET,0)))
          this.GruppoExpr = "this.w_ANPARIVA+NVL(PINATTRA,SPACE(3))+NVL(PINOMENC,SPACE(8))"
          this.GruppoExpr = this.GruppoExpr+"+IIF(PITIPMOV='AC',' ', NVL(PITIPRET,' ') + NVL(PIANNRET,' ') + STR(NVL(PIPERRET,0)))"
        endif
      case PITIPMOV $ "CS-AS"
        * --- Cessioni / Acquisti di servizi mensili / trimestrali
        this.Gruppo = this.w_ANPARIVA+STR(NUMFATT)+IIF(EMPTY(ALFAFATT),SPACE(10),ALFAFATT)+IIF(EMPTY(DATAFATT),SPACE(6),DATAFATT)
        this.Gruppo = this.Gruppo+NVL(PINOMENC,SPACE(8))+NVL(PIPERSER,SPACE(1))+NVL(PIMODINC,SPACE(1))+this.w_PIPAEPAG
        this.GruppoExpr = "this.w_ANPARIVA+STR(NUMFATT)+IIF(EMPTY(ALFAFATT),SPACE(10),ALFAFATT)+IIF(EMPTY(DATAFATT),SPACE(6),DATAFATT)"
        this.GruppoExpr = this.GruppoExpr+"+NVL(PINOMENC,SPACE(8))+NVL(PIPERSER,SPACE(1))+NVL(PIMODINC,SPACE(1))+this.w_PIPAEPAG"
      case PITIPMOV $ "RS-RX"
        * --- Rettifica di cessioni / acquisti di servizi mensili / trimestrali
        this.Gruppo = PISEZDOG+PIANNREG+PIPRORET+PIPROGSE
        this.GruppoExpr = "PISEZDOG+PIANNREG+PIPRORET+PIPROGSE"
    endcase
    * --- Azzero le variabili
    STORE 0 TO this.w_PIIMPNAZ,this.w_PIIMPVAL,this.w_PIMASNET,this.w_PIQTASUP,this.w_PIVALSTA,this.w_IESENAZ,this.w_IESELSTA,this.w_RIGAVALIDA
    this.w_TIPAC = PITIPMOV
    * --- Raggruppamento
    this.Record = recno()
    do while (.not. eof()) .and. this.Gruppo = eval( this.GruppoExpr ) and this.w_TIPAC=PITIPMOV
      * --- Totalizza i dati del record corrente
      this.Record = recno()
      * --- Se la valuta naz. della registrazione � LIRE ma la valuta naz. di fine periodo � EURO converto l'importo
      if this.w_VALUTA <> PIVALNAZ and this.w_VALUTA = g_PERVAL
        this.w_IESENAZ = VAL2MON(NVL(PIIMPNAZ,0), GETCAM(PIVALNAZ,this.w_PIDATREG), 1,this.w_PIDATREG,this.w_VALUTA,GETVALUT(this.w_VALUTA,"VADECTOT"))
        this.w_IESELSTA = VAL2MON(NVL(PIVALSTA,0),GETCAM(PIVALNAZ,this.w_PIDATREG),1,this.w_PIDATREG,this.w_VALUTA,GETVALUT(this.w_VALUTA,"VADECTOT"))
        this.w_PIIMPNAZ = this.w_PIIMPNAZ + this.w_IESENAZ
        this.w_PIVALSTA = this.w_PIVALSTA + this.w_IESELSTA
      else
        this.w_PIIMPNAZ = this.w_PIIMPNAZ + NVL(PIIMPNAZ, 0)
        this.w_PIVALSTA = this.w_PIVALSTA + NVL(PIVALSTA, 0)
      endif
      this.w_PIIMPVAL = this.w_PIIMPVAL + NVL(PIIMPVAL, 0)
      this.w_PIMASNET = this.w_PIMASNET + NVL(PIMASNET, 0)
      this.w_PIQTASUP = this.w_PIQTASUP + NVL(PIQTASUP, 0)
      * --- Nel caso di rettifica di servizi considero solo le righe dove � presente la 
      *     sezione doganale,l'anno di registrazione,il protocollo della dichiarazione e
      *     il progressivo della sezione 3
      if PITIPMOV $ "RS-RX" 
        this.w_Rigavalida = this.w_Rigavalida+IIF(NOT EMPTY(PISEZDOG) AND NOT EMPTY(PIANNREG) AND NOT EMPTY(PIPROGSE) AND NOT EMPTY(PIPRORET),1,0)
      endif
      skip
      this.w_ANPARIVA = iif( isnull( ANPARIVA ), space(12) , alltrim(ANPARIVA) )
      this.w_NAISODES = iif( isnull( NAISODES ), space(2) , alltrim(NAISODES) )
      this.w_NAISOPRO = iif( isnull( NAISOPRO ), space(2) , alltrim(NAISOPRO) )
      this.w_NAISOORI = iif( isnull( NAISOORI ), space(2) , alltrim(NAISOORI) )
      this.w_PIPAEPAG = iif( isnull( PIPAEPAG ), space(2) , alltrim(PIPAEPAG) )
    enddo
    * --- Si posiziona sull'ultimo record che apparteneva all'intervallo di raggruppamento
    goto this.Record
    this.w_ANPARIVA = iif( isnull( ANPARIVA ), space(12) , alltrim(ANPARIVA) )
    this.w_NAISODES = iif( isnull( NAISODES ), space(2) , alltrim(NAISODES) )
    this.w_NAISOPRO = iif( isnull( NAISOPRO ), space(2) , alltrim(NAISOPRO) )
    this.w_NAISOORI = iif( isnull( NAISOORI ), space(2) , alltrim(NAISOORI) )
    this.w_PIPAEPAG = iif( isnull( PIPAEPAG ), space(2) , alltrim(PIPAEPAG) )
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='ESERCIZI'
    this.cWorkTables[4]='VALUTE'
    this.cWorkTables[5]='NOMENCLA'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
