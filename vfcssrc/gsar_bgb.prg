* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bgb                                                        *
*              Determina saldi contabili bilancio                              *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_8]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-04-23                                                      *
* Last revis.: 2008-06-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_ESE,w_TIPSTA,w_ESSALDI,w_ESPREC,w_DATA1,w_DATA2,w_PROVVI ,w_SUPERBU,w_CODBUN,w_MASABI,w_INFRAAN
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bgb",oParentObject,m.w_ESE,m.w_TIPSTA,m.w_ESSALDI,m.w_ESPREC,m.w_DATA1,m.w_DATA2,m.w_PROVVI ,m.w_SUPERBU,m.w_CODBUN,m.w_MASABI,m.w_INFRAAN)
return(i_retval)

define class tgsar_bgb as StdBatch
  * --- Local variables
  w_ESE = space(4)
  w_TIPSTA = space(1)
  w_ESSALDI = space(1)
  w_ESPREC = space(1)
  w_DATA1 = ctod("  /  /  ")
  w_DATA2 = ctod("  /  /  ")
  w_PROVVI  = space(1)
  w_SUPERBU = space(15)
  w_CODBUN = space(3)
  w_MASABI = space(1)
  w_INFRAAN = space(1)
  w_VALAPP = space(3)
  w_DATINIESE = ctod("  /  /  ")
  w_CAONAZ = 0
  w_DECNAZ = 0
  w_ESEPRE = space(4)
  w_PRVALNAZ = space(3)
  w_PRCAOVAL = 0
  w_APERTURA = .f.
  * --- WorkFile variables
  VALUTE_idx=0
  ESERCIZI_idx=0
  TMPMOVIMAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Costruisce e popola il temporaneo SQL [TMPMOVIMAST] che contiene il resoconto
    *     contabile necessario per Bilancio UE ed esportazione per Basilea2.
    *     
    *     In generale da utilizzare per elaborazione simil Bilancio...
    * --- Parametri
    *     w_ESE = Esercizio di Stampa
    *     w_TIPSTA = Tipo Stampa (T/M=Tutto C Solo Clienti , F Solo Fornitori)
    *     w_ESSALDI = Se 'S' legge dati da saldi altrimrenti da prima nota
    *     w_ESPREC = Se 'S'  recupera saldi esercizio precedente
    *     w_DATA1  = Data inziio selezione reg. di prima nota
    *     w_DATA2  = Data fine selezione reg. di prima nota
    *     w_PROVVI = 'N' Confermati , 'S' Provvisori '' Tutti
    *     w_CODBUN = Codice B.Unit
    *     w_SUPERBU= Cod. Gruppo B.Unit
    *     w_MASABI = 'S' se gestito Mastro in Prima Nota (non gestito)
    *     w_INFRANN = escludi chiusure infrannuali
    * --- Determino la valuta di conto per l'esercizio...
    *     w_VALAPP = Valuta di Conto Esercizio di stampa
    * --- Read from ESERCIZI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ESERCIZI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ESVALNAZ,ESINIESE"+;
        " from "+i_cTable+" ESERCIZI where ";
            +"ESCODAZI = "+cp_ToStrODBC(i_CODAZI);
            +" and ESCODESE = "+cp_ToStrODBC(this.w_ESE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ESVALNAZ,ESINIESE;
        from (i_cTable) where;
            ESCODAZI = i_CODAZI;
            and ESCODESE = this.w_ESE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_VALAPP = NVL(cp_ToDate(_read_.ESVALNAZ),cp_NullValue(_read_.ESVALNAZ))
      this.w_DATINIESE = NVL(cp_ToDate(_read_.ESINIESE),cp_NullValue(_read_.ESINIESE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VACAOVAL,VADECTOT"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_VALAPP);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VACAOVAL,VADECTOT;
        from (i_cTable) where;
            VACODVAL = this.w_VALAPP;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CAONAZ = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
      this.w_DECNAZ = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Determino l'esercizio precedente
    *     w_PRVALNAZ = Valuta di Conto Es. Precedente
    *     w_ESEPRE = Esercizio Precedente
    this.w_ESEPRE = CALCESPR(i_CODAZI,this.w_DATINIESE,.T.)
    * --- Read from ESERCIZI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ESERCIZI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ESVALNAZ"+;
        " from "+i_cTable+" ESERCIZI where ";
            +"ESCODAZI = "+cp_ToStrODBC(i_CODAZI);
            +" and ESCODESE = "+cp_ToStrODBC(this.w_ESEPRE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ESVALNAZ;
        from (i_cTable) where;
            ESCODAZI = i_CODAZI;
            and ESCODESE = this.w_ESEPRE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PRVALNAZ = NVL(cp_ToDate(_read_.ESVALNAZ),cp_NullValue(_read_.ESVALNAZ))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Leggo informazioni relativi alle valute...
    *     Recupero decimali e cambio valuta di conto
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VACAOVAL"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_PRVALNAZ);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VACAOVAL;
        from (i_cTable) where;
            VACODVAL = this.w_PRVALNAZ;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PRCAOVAL = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- ============================================================
    *     LEGGO I MOVIMENTI COMPRESI FRA LE DATE DI STAMPA E DI COMPETENZA DELL'ESERCIZIO
    *     SE SPECIFICATI, APPLICO I FILTRI PER B.U. E MOVIMENTI PROVVISORI
    * --- LEGGO I MOVIMENTI PRECEDENTI ALLA DATA DI INIZIO STAMPA CON COMPETENZA DELL'ESERCIZIO IN CORSO
    *     QUESTA QUERY SI PUO' EVITARE SE LA DATA DI INIZIO STAMPA COINCIDE CON L'INIZIO DELL'ESERCIZIO
    ah_Msg("Lettura movimenti compresi fra le date di stampa",.T.,.T.)
    if this.w_ESSALDI="S"
      * --- Create temporary table TMPMOVIMAST
      i_nIdx=cp_AddTableDef('TMPMOVIMAST') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      declare indexes_PEDIHWPIYY[2]
      indexes_PEDIHWPIYY[1]='PNTIPCON,PNCODCON'
      indexes_PEDIHWPIYY[2]='MCCODICE'
      vq_exec('query\b_movimenti1',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_PEDIHWPIYY,.f.)
      this.TMPMOVIMAST_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    else
      if this.w_MASABI="S"
        * --- Create temporary table TMPMOVIMAST
        i_nIdx=cp_AddTableDef('TMPMOVIMAST') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        declare indexes_ZYMMBPUCHZ[2]
        indexes_ZYMMBPUCHZ[1]='PNTIPCON,PNCODCON'
        indexes_ZYMMBPUCHZ[2]='MCCODICE'
        vq_exec('query\b_movimenti2',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_ZYMMBPUCHZ,.f.)
        this.TMPMOVIMAST_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      else
        * --- Create temporary table TMPMOVIMAST
        i_nIdx=cp_AddTableDef('TMPMOVIMAST') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        declare indexes_HUJBPLGDDF[2]
        indexes_HUJBPLGDDF[1]='PNTIPCON,PNCODCON'
        indexes_HUJBPLGDDF[2]='MCCODICE'
        vq_exec('query\b_movimenti',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_HUJBPLGDDF,.f.)
        this.TMPMOVIMAST_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      endif
    endif
    if this.w_ESPREC="S"
      * --- INIZIO LA LETTURA DEI SALDI DELL'ESERCIZIO PRECEDENTE
      *     1- VERIFICO SE PER L'ESERCIZIO IN CORSO E' STATA FATTA L'APERTURA
      this.w_APERTURA = .F.
      if w_PROVVI<>"S"
        * --- Entro a verificare se c'� apertura anche nel caso che sto 
        *     stampando tutti i mvimenti ma non esiste esercizio precedente
        *     QUESTO CASO SI VERIFICA SOLO NELLA STAMPA CONFRONTO
        ah_Msg("Verifica effettuazione apertura conti",.T.,.T.)
        if this.w_MASABI="S"
          * --- Insert into TMPMOVIMAST
          i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\b_apertura1",this.TMPMOVIMAST_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        else
          * --- Insert into TMPMOVIMAST
          i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\b_apertura",this.TMPMOVIMAST_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
        this.w_APERTURA = i_Rows > 0
      endif
      if NOT this.w_APERTURA 
        * --- 2- SE NON E' STATA FATTA L'APERTURA DEI CONTI, LEGGIAMO I SALDI DAI MOVIMENTI DELL'ESERCIZIO PRECEDENDE
        *     DALLA LETTURA ESCLUDIAMO I MOVIMENTI DI CHIUSURA
        ah_Msg("Lettura movimenti dell'esercizio precedente",.T.,.T.)
        if this.w_MASABI="S"
          if this.w_VALAPP=this.w_PRVALNAZ
            * --- Insert into TMPMOVIMAST
            i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\b_saldo2",this.TMPMOVIMAST_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          else
            * --- Insert into TMPMOVIMAST
            i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\b_saldo3",this.TMPMOVIMAST_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
        else
          if this.w_VALAPP=this.w_PRVALNAZ
            * --- Insert into TMPMOVIMAST
            i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\b_saldo",this.TMPMOVIMAST_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          else
            * --- Insert into TMPMOVIMAST
            i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\b_saldo1",this.TMPMOVIMAST_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
        endif
        if i_Rows=0 AND w_PROVVI<>"S"
          * --- non ci sono movimenti nell'esercizio precedente per cui provo a leggere il movimento di apertura
          ah_Msg("Verifica effettuazione apertura conti",.T.,.T.)
          if this.w_MASABI="S"
            * --- Insert into TMPMOVIMAST
            i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\b_apertura1",this.TMPMOVIMAST_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          else
            * --- Insert into TMPMOVIMAST
            i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\b_apertura",this.TMPMOVIMAST_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
        endif
        if w_PROVVI<>"S" and EMPTY(this.w_CODBUN) AND EMPTY(this.w_SUPERBU)
          * --- LEGGIAMO ANCHE I MOVIMENTI FUORI LINEA CHE DEVONO POI ESSERE SOMMATI.
          ah_Msg("Lettura saldi fuori linea",.T.,.T.)
          if this.w_VALAPP=this.w_PRVALNAZ
            * --- Insert into TMPMOVIMAST
            i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\b_movfuori",this.TMPMOVIMAST_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          else
            * --- Insert into TMPMOVIMAST
            i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\b_movfuori1",this.TMPMOVIMAST_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
        endif
      endif
    endif
  endproc


  proc Init(oParentObject,w_ESE,w_TIPSTA,w_ESSALDI,w_ESPREC,w_DATA1,w_DATA2,w_PROVVI ,w_SUPERBU,w_CODBUN,w_MASABI,w_INFRAAN)
    this.w_ESE=w_ESE
    this.w_TIPSTA=w_TIPSTA
    this.w_ESSALDI=w_ESSALDI
    this.w_ESPREC=w_ESPREC
    this.w_DATA1=w_DATA1
    this.w_DATA2=w_DATA2
    this.w_PROVVI =w_PROVVI 
    this.w_SUPERBU=w_SUPERBU
    this.w_CODBUN=w_CODBUN
    this.w_MASABI=w_MASABI
    this.w_INFRAAN=w_INFRAAN
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='VALUTE'
    this.cWorkTables[2]='ESERCIZI'
    this.cWorkTables[3]='*TMPMOVIMAST'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_ESE,w_TIPSTA,w_ESSALDI,w_ESPREC,w_DATA1,w_DATA2,w_PROVVI ,w_SUPERBU,w_CODBUN,w_MASABI,w_INFRAAN"
endproc
