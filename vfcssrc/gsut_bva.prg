* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bva                                                        *
*              Visualizza allegati                                             *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][22]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-08-29                                                      *
* Last revis.: 2005-08-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bva",oParentObject)
return(i_retval)

define class tgsut_bva as StdBatch
  * --- Local variables
  w_Archivio = space(2)
  w_Categoria = space(2)
  w_Chiave = space(20)
  w_Descrizione = space(250)
  w_FILE = space(255)
  w_GESTIONE = space(50)
  w_Periodo = ctod("  /  /  ")
  w_Programma = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato da Men� Sistema->Gestione Allegati Immagini->Visualizza Allegati
    if !cp_IsAdministrator(.t.) 
 
       ah_ErrorMsg("Funzionalit� consentita solo ad un utente amministratore","!")
      i_retcode = 'stop'
      return
    endif
    this.w_Archivio = "AR"
    this.w_Chiave = Space(10)
    this.w_Descrizione = ""
    this.w_FILE = ""
    this.w_GESTIONE = "Visualizzazione"
    this.w_Periodo = i_DATSYS
    this.w_Programma = "MENU"
    do gsut_kgf with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
