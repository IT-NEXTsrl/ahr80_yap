* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut1bsa                                                        *
*              Creazione collegamento accesso silente schedulatore             *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][4]                                                       *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-10-06                                                      *
* Last revis.: 2016-05-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAZIONE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut1bsa",oParentObject,m.pAZIONE)
return(i_retval)

define class tgsut1bsa as StdBatch
  * --- Local variables
  pAZIONE = space(2)
  w_LETTURA = space(254)
  w_MAKELNK = .f.
  w_Shell = .NULL.
  w_PATHLINK = space(254)
  w_Link = .NULL.
  w_STRINGHK = space(50)
  w_VARCNFOK = .f.
  w_MESSAGGI = space(254)
  w_EXETOLAUNCH = space(10)
  w_RD___PWD = space(20)
  w_SENDTOPRG = space(254)
  w_SENDTOPRG = space(254)
  w_FILESENDTO = space(0)
  w_HANDLE = 0
  w_PATH_MAINSERVICE = space(254)
  w_PATH_SCHSERVICE = space(254)
  w_HKEY_CLASSES_ROOT = 0
  w_HKEY_CURRENT_USER = 0
  w_HKEY_LOCAL_MACHINE = 0
  w_LINE = space(254)
  w_FOUND = .f.
  w_ERROR = 0
  w_Service = .NULL.
  w_Folder = .NULL.
  w_Task = .NULL.
  w_Trigger = .NULL.
  w_Action = .NULL.
  w_TaskName = space(100)
  w_ZSACCOUN = space(50)
  w_ZSPSWD = space(10)
  w_ZSCOPSWD = space(10)
  w_objmask = .NULL.
  w_PVARDEF = .f.
  w_PVARON = .f.
  w_TaskList = .NULL.
  w_TaskDeleted = .f.
  w_TaskIndex = 0
  * --- WorkFile variables
  CPUSRGRP_idx=0
  GROUPSJ_idx=0
  AZIENDA_idx=0
  UTE_AZI_idx=0
  BUSIUNIT_idx=0
  ESERCIZI_idx=0
  CPUSERS_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --Variabili caller servizio per funzionalit� windows
    do case
      case this.pAZIONE=="UC" And this.oParentObject.w_TIPO="J"
        this.oParentObject.w_GRPUSER = 0
        * --- Select from CPUSRGRP
        i_nConn=i_TableProp[this.CPUSRGRP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CPUSRGRP_idx,2],.t.,this.CPUSRGRP_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select GROUPCODE  from "+i_cTable+" CPUSRGRP ";
              +" where USERCODE="+cp_ToStrODBC(this.oParentObject.w_USERIN)+"";
               ,"_Curs_CPUSRGRP")
        else
          select GROUPCODE from (i_cTable);
           where USERCODE=this.oParentObject.w_USERIN;
            into cursor _Curs_CPUSRGRP
        endif
        if used('_Curs_CPUSRGRP')
          select _Curs_CPUSRGRP
          locate for 1=1
          do while not(eof())
          if this.oParentObject.w_SCHEDGRP<>0
            if this.oParentObject.w_SCHEDGRP=_Curs_CPUSRGRP.GROUPCODE
              this.oParentObject.w_GRPUSER = _Curs_CPUSRGRP.GROUPCODE
            endif
          endif
            select _Curs_CPUSRGRP
            continue
          enddo
          use
        endif
        if this.oParentObject.w_GRPUSER=0
          ah_errormsg("Utente selezionato non appartenente al gruppo schedulatore")
          this.oParentObject.w_USERIN = 0
          this.oParentObject.w_DESCUSER = SPACE(20)
        else
          if !EMPTY(this.oParentObject.w_CODAZI)
            * --- Read from UTE_AZI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.UTE_AZI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.UTE_AZI_idx,2],.t.,this.UTE_AZI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "*"+;
                " from "+i_cTable+" UTE_AZI where ";
                    +"UACODUTE = "+cp_ToStrODBC(this.oParentObject.w_USERIN);
                    +" and UACODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                *;
                from (i_cTable) where;
                    UACODUTE = this.oParentObject.w_USERIN;
                    and UACODAZI = this.oParentObject.w_CODAZI;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              use
              if i_Rows=0
                ah_errormsg("Utente non abilitato sull'azienda selezionata")
                this.oParentObject.w_USERIN = 0
                this.oParentObject.w_DESCUSER = SPACE(20)
              endif
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
        endif
        this.oParentObject.NotifyEvent("Aggiorna")
      case this.pAZIONE=="CM" And InList(this.oParentObject.w_TIPO,"J","R")
        if this.oParentObject.w_TIPO="J"
          * --- Read from AZIENDA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.AZIENDA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "AZFLBUNI"+;
              " from "+i_cTable+" AZIENDA where ";
                  +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              AZFLBUNI;
              from (i_cTable) where;
                  AZCODAZI = this.oParentObject.w_CODAZI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_AZFLBUNI = NVL(cp_ToDate(_read_.AZFLBUNI),cp_NullValue(_read_.AZFLBUNI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from GROUPSJ
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.GROUPSJ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.GROUPSJ_idx,2],.t.,this.GROUPSJ_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "GJCODICE"+;
              " from "+i_cTable+" GROUPSJ where ";
                  +"1 = "+cp_ToStrODBC(1);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              GJCODICE;
              from (i_cTable) where;
                  1 = 1;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_SCHEDGRP = NVL(cp_ToDate(_read_.GJCODICE),cp_NullValue(_read_.GJCODICE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.oParentObject.w_SCHEDGRP=0
            ah_errormsg("Non � stato specificato il gruppo schedulatore. Non sar� possibile creare l'icona di collegamento")
            this.oParentObject.w_SCHEDGRP = -1
          endif
        endif
        * --- Se non esiste il file di configurazione del Service Manager lo creo
        if !cp_fileExist(ForceExt(this.oParentObject.w_ZSRVMNG,"ini"))
          this.w_HANDLE = FCREATE(ForceExt(this.oParentObject.w_ZSRVMNG,"ini"))
          if this.w_HANDLE=-1
            ah_errormsg("Errore creazione file:%0%1",,,ForcePath(this.oParentObject.w_ZSRVINI,JustPath(this.oParentObject.w_ZSRVMNG)))
            i_retcode = 'stop'
            return
          endif
          FPUTS(this.w_HANDLE, "* ---------------------------------------------"+Chr(9)+"*") 
 FPUTS(this.w_HANDLE, "* Percorso zMainService.exe"+Chr(9)+Chr(9)+Chr(9)+"*") 
 FPUTS(this.w_HANDLE, "* Elenco dei servizi, descritti come segue:"+Chr(9)+"*") 
 FPUTS(this.w_HANDLE, "* descrizione;file ini;file mutex (*.log)"+Chr(9)+"*") 
 FPUTS(this.w_HANDLE, "* ---------------------------------------------"+Chr(9)+"*") 
 FPUTS(this.w_HANDLE,"*") 
 FPUTS(this.w_HANDLE,this.oParentObject.w_ZMAINSER) 
 FPUTS(this.w_HANDLE,"*")
          FCLOSE(this.w_HANDLE)
        endif
      case this.pAZIONE=="CP"
        if this.oParentObject.w_USERIN<>0
          this.w_MAKELNK = .F.
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          this.oParentObject.w_PARACSIL = ""
        endif
      case this.pAZIONE=="CI"
        * --- Variabili locali per creazione shortcut
        * --- contiene nome eseguibile da lanciare
        * --- --Determinazione dell'eseguibile da lanciare
        this.w_EXETOLAUNCH = iCase(isAhr() And this.oParentObject.w_TIPO="R", "AH_SERVICE.EXE", isAhr(), "AHR.EXE", isAhe(), "AHE.EXE", isAlt(), "ALTEREGOTOP.EXE", "AHPA.EXE")
        if this.oParentObject.w_FLHOTKEY AND (this.oParentObject.w_FUNC__HK="C" AND EMPTY(this.oParentObject.w_CHAR__HK))
          ah_errormsg("Disattivare hot key o definirla correttamente")
          i_retcode = 'stop'
          return
        endif
        * --- Read from CPUSERS
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CPUSERS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CPUSERS_idx,2],.t.,this.CPUSERS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PASSWD"+;
            " from "+i_cTable+" CPUSERS where ";
                +"CODE = "+cp_ToStrODBC(this.oParentObject.w_USERIN);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PASSWD;
            from (i_cTable) where;
                CODE = this.oParentObject.w_USERIN;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_RD___PWD = NVL(cp_ToDate(_read_.PASSWD),cp_NullValue(_read_.PASSWD))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if !cp_CheckPwd(this.oParentObject.w_USERIN,this.oParentObject.w_PWDUSER, this.w_RD___PWD)
          ah_errormsg("Password errata")
          i_retcode = 'stop'
          return
        endif
        if this.oParentObject.w_FLASCRYP="S"
          this.w_VARCNFOK = .F.
          this.w_MESSAGGI = "� stata attivata la criptazione della stringa di accesso silente ma nel CNF selezionato la funzionalit� non � abilitata o definita correttamente. Procedere comunque alla creazione dell'icona?"
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if !this.w_VARCNFOK
            if !ah_yesno(this.w_MESSAGGI)
              i_retcode = 'stop'
              return
            endif
          endif
        endif
        * --- Creazione dell'icona
        if this.oParentObject.w_TIPOCONF="ICO"
          * --- --Controllo caratteri speciali su nome icona 
          if Len(Alltrim(this.oParentObject.w_ICONAME))=Len(Alltrim(ChrTran(this.oParentObject.w_ICONAME, '*\/:?"<>', "")))
            this.w_Shell = .NULL.
            this.w_Shell = CreateObject("WScript.Shell")
            if this.w_SHELL=.NULL.
              ah_errormsg("Servizio non disponibile. Impossibile creare l'icona")
            else
              do case
                case this.oParentObject.w_TIPO = "S"
                  * --- SendTo
                  this.w_PATHLINK = this.w_Shell.SpecialFolders("SendTo")
                  if this.oParentObject.w_FLSUBFOLDER = "S"
                    do case
                      case IsAlt()
                        this.w_PATHLINK = ADDBS(this.w_PATHLINK) + "Alterego Top"
                      case IsAhr()
                        this.w_PATHLINK = ADDBS(this.w_PATHLINK) + "Ad hoc Revolution"
                      case IsAhe()
                        this.w_PATHLINK = ADDBS(this.w_PATHLINK) + "Ad hoc Enterprise"
                      otherwise
                        this.w_PATHLINK = ADDBS(this.w_PATHLINK) + "Ad hoc PA"
                    endcase
                    MakeDir(this.w_PATHLINK)
                  endif
                case this.oParentObject.w_TIPO = "C"
                  * --- Centralino
                  this.w_PATHLINK = this.oParentObject.w_PATHVBS
                  if !Empty(this.w_PATHLINK)
                    MakeDir(this.w_PATHLINK)
                  endif
                otherwise
                  * --- Schedulatore e iRevolution
                  this.w_PATHLINK = this.w_Shell.SpecialFolders("Desktop")
              endcase
              if FILE(ADDBS(this.w_PATHLINK)+ALLTRIM(this.oParentObject.w_ICONAME)+".lnk")
                if ah_YESNO("Icona gi� esistente. Sovrascriverla?")
                  DELETE FILE ADDBS(this.w_PATHLINK)+ALLTRIM(this.oParentObject.w_ICONAME)+".lnk"
                else
                  i_retcode = 'stop'
                  return
                endif
              endif
              this.w_Link = this.w_Shell.CreateShortcut(ADDBS(this.w_PATHLINK)+ALLTRIM(this.oParentObject.w_ICONAME)+".lnk")
              * --- --Occorre determinare il path giusto da dove lanciare l'eseguibile
              do case
                case this.oParentObject.w_TIPO = "S"
                  * --- SendTo
                  this.w_SENDTOPRG = ADDBS(tempadhoc())+"SendTo"+SYS(2015)+".vbs"
                  this.w_Link.TargetPath = this.w_SENDTOPRG
                case this.oParentObject.w_TIPO = "C"
                  * --- Centralino
                  this.w_SENDTOPRG = ADDBS(tempadhoc())+"Phone"+SYS(2015)+".vbs"
                  this.w_Link.TargetPath = this.w_SENDTOPRG
                otherwise
                  * --- Schedulatore e iRevolution
                  this.w_Link.TargetPath = ADDBS(this.oParentObject.w_PATHEXE)+this.w_EXETOLAUNCH
              endcase
              this.w_MAKELNK = .T.
              this.Page_3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              do case
                case this.oParentObject.w_TIPO = "S" Or this.oParentObject.w_TIPO = "C"
                  * --- SendTo/Centralino
                  if this.oParentObject.w_TIPO="S"
                    this.w_FILESENDTO = FileToStr("SendTo.vbs")
                  else
                    this.w_FILESENDTO = FileToStr("Phone.vbs")
                  endif
                  this.w_FILESENDTO = STRTRAN(this.w_FILESENDTO, "#cExe#", '"'+ADDBS(ALLTRIM(this.oParentObject.w_PATHEXE))+this.w_EXETOLAUNCH+'"')
                  this.w_FILESENDTO = STRTRAN(this.w_FILESENDTO, "#cCNF#", '"' + ALLTRIM(this.oParentObject.w_FILECNF) + '"')
                  this.w_FILESENDTO = STRTRAN(this.w_FILESENDTO, "#cSilent#", '"' + ALLTRIM(this.oParentObject.w_PARACSIL) + '"')
                  this.w_FILESENDTO = STRTRAN(this.w_FILESENDTO, "#cApplication#", '"'+iif(isAhr(),"AHR",iif(isAhe(),"AHE",IIF(isAlt(),"ALT","AHP")))+'"')
                  this.w_FILESENDTO = STRTRAN(this.w_FILESENDTO, "#ClassName#", '"'+Left(Lower(Juststem(this.w_EXETOLAUNCH)),9)+"9c000000"+'"')
                  STRTOFILE(this.w_FILESENDTO, this.w_SENDTOPRG)
                otherwise
                  * --- Schedulatore e iRevolution
                  this.w_Link.Arguments = ALLTRIM(this.oParentObject.w_FILECNF)+" "+'"'+ALLTRIM(this.oParentObject.w_PARACSIL)+'"'
                  if this.oParentObject.w_TIPO = "R"
                    this.w_Link.Arguments = this.w_Link.Arguments+' UNI "GSRV_BEC(' + iif(this.oParentObject.w_ZSRVFLAZI="S", ".null.,'','"+Alltrim(this.oParentObject.w_ZSRVAZI)+"'", "") + ')" '+Alltrim(p_Application)
                  endif
              endcase
              this.w_Link.WorkingDirectory = ALLTRIM(this.oParentObject.w_PATHEXE)
              this.w_Link.IconLocation = ADDBS(this.oParentObject.w_PATHEXE)+this.w_EXETOLAUNCH+", 0"
              if this.oParentObject.w_FLHOTKEY
                do case
                  case this.oParentObject.w_FLHKSPEC=0
                    this.w_STRINGHK = ALLTRIM(this.oParentObject.w_FUNC__HK)
                  case this.oParentObject.w_FLHKSPEC=1
                    this.w_STRINGHK = "CTRL+"+ALLTRIM(this.oParentObject.w_FUNC__HK)
                  case this.oParentObject.w_FLHKSPEC=2
                    this.w_STRINGHK = "ALT+"+ALLTRIM(this.oParentObject.w_FUNC__HK)
                  case this.oParentObject.w_FLHKSPEC=3
                    this.w_STRINGHK = "SHIFT+"+ALLTRIM(this.oParentObject.w_FUNC__HK)
                  case this.oParentObject.w_FLHKSPEC=4
                    this.w_STRINGHK = "CTRL+ALT+"+IIF(this.oParentObject.w_FUNC__HK="C",ALLTRIM(this.oParentObject.w_CHAR__HK),ALLTRIM(this.oParentObject.w_FUNC__HK))
                  case this.oParentObject.w_FLHKSPEC=5
                    this.w_STRINGHK = "CTRL+SHIFT+"+IIF(this.oParentObject.w_FUNC__HK="C",ALLTRIM(this.oParentObject.w_CHAR__HK),ALLTRIM(this.oParentObject.w_FUNC__HK))
                  case this.oParentObject.w_FLHKSPEC=6
                    this.w_STRINGHK = "ALT+SHIFT+"+IIF(this.oParentObject.w_FUNC__HK="C",ALLTRIM(this.oParentObject.w_CHAR__HK),ALLTRIM(this.oParentObject.w_FUNC__HK))
                  case this.oParentObject.w_FLHKSPEC=7
                    this.w_STRINGHK = "CTRL+ALT+SHIFT+"+IIF(this.oParentObject.w_FUNC__HK="C",ALLTRIM(this.oParentObject.w_CHAR__HK),ALLTRIM(this.oParentObject.w_FUNC__HK))
                endcase
                this.w_Link.HotKey = this.w_STRINGHK
              endif
              this.w_Link.WindowStyle = this.oParentObject.w_FLWNDSTA
              if NOT EMPTY(this.oParentObject.w_COMMENTO)
                this.w_Link.Description = this.oParentObject.w_COMMENTO
              endif
              this.w_Link.Save()     
              this.w_Link = .NULL.
              this.w_Shell = .NULL.
              ah_errormsg("Collegamento creato con successo", 64) 
            endif
          else
            ah_errormsg("I nomi dei file non possono contenere i seguenti caratteri: /:*?<>\") 
            i_retcode = 'stop'
            return
          endif
        else
          * --- --Gestione nuovo servizio
          * --- --Variabili per gestione nuovo servizio
          this.w_HKEY_CLASSES_ROOT = 0x80000000
          this.w_HKEY_CURRENT_USER = 0x80000001
          this.w_HKEY_LOCAL_MACHINE = 0x80000002
          this.w_PATH_MAINSERVICE = alltrim(upper(this.oParentObject.w_ZMAINSER))
          this.w_PATH_SCHSERVICE = STRTRAN(this.w_PATH_MAINSERVICE,"ZSRVMNG.EXE")
          if file(this.w_PATH_MAINSERVICE)
            if !file(this.w_PATH_SCHSERVICE)
              ah_errormsg("Attenzione: i file ZMainService e ZSrvMng devono risiedere nella medesima cartella")
              i_retcode = 'stop'
              return
            endif
          else
            ah_errormsg("Attenzione: il file ZMainService non � stato trovato nel percorso indicato: specificare un percorso di rete")
            i_retcode = 'stop'
            return
          endif
          this.w_MAKELNK = .T.
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_HANDLE = FCREATE(ForcePath(this.oParentObject.w_ZSRVINI,JustPath(this.oParentObject.w_ZSRVMNG)))
          if this.w_HANDLE=-1
            ah_errormsg("Errore creazione file:%0%1",,,ForcePath(this.oParentObject.w_ZSRVINI,JustPath(this.oParentObject.w_ZSRVMNG)))
            i_retcode = 'stop'
            return
          endif
          * --- Scriviamo dei commenti per la struttura del file
          FPUTS(this.w_HANDLE,"* La struttura del file DEVE essere la seguente:")
          FPUTS(this.w_HANDLE,"* File CNF *")
          FPUTS(this.w_HANDLE,"* Path eseguibile applicazione *")
          FPUTS(this.w_HANDLE,"* Directory eseguibile applicazione *")
          FPUTS(this.w_HANDLE,"* Parametri accesso silente *")
          FPUTS(this.w_HANDLE,"* Mappatura unit� di rete *")
          FPUTS(this.w_HANDLE,"* Path eseguibile ZMainService *")
          FPUTS(this.w_HANDLE,"* Path eseguibile ZSRVMNG *")
          FPUTS(this.w_HANDLE,"* ")
          FPUTS(this.w_HANDLE,"* "+Replicate("-",30)+" *")
          FPUTS(this.w_HANDLE,"* ")
          FFLUSH(this.w_HANDLE)
          * --- Da qui iniziamo a scrivere i veri dati
          FPUTS(this.w_HANDLE,ALLTRIM(this.oParentObject.w_FILECNF))
          FPUTS(this.w_HANDLE,ALLTRIM(this.oParentObject.w_PATHEXE+"\"+this.w_EXETOLAUNCH))
          FPUTS(this.w_HANDLE,ALLTRIM(this.oParentObject.w_PATHEXE))
          do case
            case this.oParentObject.w_TIPO="J"
              FPUTS(this.w_HANDLE,ALLTRIM(this.oParentObject.w_FILECNF)+" "+'"'+ALLTRIM(this.oParentObject.w_PARACSIL)+";PUBLIC G_ISSERVICE"+";G_ISSERVICE=.T."+'"')
            case this.oParentObject.w_TIPO="R"
              FPUTS(this.w_HANDLE,ALLTRIM(this.oParentObject.w_FILECNF)+' "'+ALLTRIM(this.oParentObject.w_PARACSIL)+'" UNI "GSRV_BEC(' + iif(this.oParentObject.w_ZSRVFLAZI="S", ".null.,'','"+Alltrim(this.oParentObject.w_ZSRVAZI)+"'", "") + ')" '+Alltrim(p_Application))
          endcase
          FPUTS(this.w_HANDLE,ALLTRIM(this.oParentObject.w_ZSDEVMAP))
          FPUTS(this.w_HANDLE,ALLTRIM(this.oParentObject.w_ZMAINSER))
          FPUTS(this.w_HANDLE,ALLTRIM(this.oParentObject.w_ZSRVMNG))
          FFLUSH(this.w_HANDLE)
          FCLOSE(this.w_HANDLE)
          * --- Aggiorno il file di configurazione del Manager dei servizi, altrimenti lo creo
          this.w_HANDLE = FOPEN(ForceExt(this.oParentObject.w_ZSRVMNG,"ini"), 12)
          if this.w_HANDLE=-1
            ah_errormsg("Errore apertura file:%0%1",,,ForceExt(this.oParentObject.w_ZSRVMNG,"ini"))
            i_retcode = 'stop'
            return
          endif
          this.w_FOUND = .F.
          do while !this.w_FOUND And !FEOF(this.w_HANDLE)
            this.w_LINE = Alltrim(FGETS(this.w_HANDLE))
            this.w_FOUND = Lower(Left(this.w_LINE,At(";",this.w_LINE)-1))==Lower(this.oParentObject.w_ZSRVNAME)
          enddo
          if !this.w_FOUND
            FPUTS(this.w_HANDLE,this.oParentObject.w_ZSRVNAME+";"+this.oParentObject.w_ZSRVINI+";"+this.oParentObject.w_ZSRVMTX)
          endif
          FCLOSE(this.w_HANDLE)
          * --- Scrittura chiavi nel registro di sistema per l'esecuzione automatica
          this.w_ERROR = 0
          * --- Se � richiesto l'avvio allo startup vado ad inserireun'attivit� pianificata, altrimenti la rimuovo
          this.w_Service = CreateObject("Schedule.Service")
          this.w_Service.Connect()     
          this.w_Folder = this.w_Service.GetFolder("\")
          this.w_TaskName = "Zucchetti Service Manager"
          if this.oParentObject.w_STARTUP
            this.Page_4()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            this.Page_5()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          * --- Se Il servizo � stato impostato come servizio di rete e se devo creare il servizio 
          *     lancio maschera per configurazioni utente di rete.
          if this.oParentObject.w_ZSSERLOC="R" and this.oParentObject.w_ZSCREASE
            this.w_objmask = gsjb_kas()
            this.w_ZSACCOUN = ALLTRIM(this.w_objmask.w_ZSACCOUN)
            this.w_ZSPSWD = ALLTRIM(this.w_objmask.w_ZSPSWD)
            this.w_objmask = NULL
          endif
          * --- Se � richiesta la creazione del servizio lo creo.
          if this.oParentObject.w_ZSCREASE
            this.Page_6()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          * --- Se � richiesta la creazione dell'icona, la creo
          if this.oParentObject.w_ZSCREAIC
            this.w_Shell = .NULL.
            this.w_Shell = CreateObject("WScript.Shell")
            if this.w_SHELL=.NULL.
              ah_errormsg("Servizio non disponibile. Impossibile creare l'icona")
            else
              this.w_PATHLINK = this.w_Shell.SpecialFolders("Desktop")
              if FILE(this.w_PATHLINK+"\Zucchetti Service Manager.lnk")
                DELETE FILE this.w_PATHLINK+"\Zucchetti Service Manager.lnk"
              endif
              this.w_Link = this.w_Shell.CreateShortcut(this.w_PATHLINK+"\Zucchetti Service Manager.lnk")
              this.w_Link.TargetPath = ALLTRIM(this.oParentObject.w_ZSRVMNG)
              this.w_Link.Arguments = ""
              this.w_Link.WorkingDirectory = STRTRAN(alltrim(upper(this.oParentObject.w_ZSRVMNG)),"ZSRVMNG.EXE")
              this.w_Link.WindowStyle = 1
              this.w_Link.IconLocation = SUBSTR(this.oParentObject.w_ZSRVMNG,1,RAT("\",this.oParentObject.w_ZSRVMNG))+"clock_big.ico"+", 0"
              this.w_Link.Save()     
              this.w_Link = .NULL.
              this.w_Shell = .NULL.
            endif
          endif
          * --- Scrittura chiavi nel registro di sistema per l'esecuzione come amministratore
          this.Page_7()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Se � richiesto l'avvio del programma alla chiusura, lo eseguo
          if this.oParentObject.w_ZSLAUNCH
            ah_shellex("open",this.oParentObject.w_ZSRVMNG,"",JustPath(this.oParentObject.w_ZSRVMNG))
          endif
        endif
        this.oParentObject.ecpQuit()
      case this.pAZIONE=="AC"
        * --- Read from AZIENDA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.AZIENDA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "AZFLBUNI"+;
            " from "+i_cTable+" AZIENDA where ";
                +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            AZFLBUNI;
            from (i_cTable) where;
                AZCODAZI = this.oParentObject.w_CODAZI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_AZFLBUNI = NVL(cp_ToDate(_read_.AZFLBUNI),cp_NullValue(_read_.AZFLBUNI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from UTE_AZI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.UTE_AZI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.UTE_AZI_idx,2],.t.,this.UTE_AZI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "*"+;
            " from "+i_cTable+" UTE_AZI where ";
                +"UACODUTE = "+cp_ToStrODBC(this.oParentObject.w_USERIN);
                +" and UACODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            *;
            from (i_cTable) where;
                UACODUTE = this.oParentObject.w_USERIN;
                and UACODAZI = this.oParentObject.w_CODAZI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          use
          if i_Rows=0
            this.oParentObject.w_USERIN = 0
            this.oParentObject.w_DESCUSER = SPACE(20)
            this.oParentObject.w_PWDUSER = SPACE(50)
          endif
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.oParentObject.w_AZFLBUNI $ "ET"
          * --- Read from BUSIUNIT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.BUSIUNIT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.BUSIUNIT_idx,2],.t.,this.BUSIUNIT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "BUCODICE,BUDESCRI"+;
              " from "+i_cTable+" BUSIUNIT where ";
                  +"BUCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              BUCODICE,BUDESCRI;
              from (i_cTable) where;
                  BUCODAZI = this.oParentObject.w_CODAZI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_CODBUN = NVL(cp_ToDate(_read_.BUCODICE),cp_NullValue(_read_.BUCODICE))
            this.oParentObject.w_DESBUN = NVL(cp_ToDate(_read_.BUDESCRI),cp_NullValue(_read_.BUDESCRI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        else
          this.oParentObject.w_DESBUN = SPACE(40)
          this.oParentObject.w_CODBUN = SPACE(3)
        endif
        * --- Read from ESERCIZI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ESERCIZI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "*"+;
            " from "+i_cTable+" ESERCIZI where ";
                +"ESCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
                +" and ESCODESE = "+cp_ToStrODBC(this.oParentObject.w_CODESE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            *;
            from (i_cTable) where;
                ESCODAZI = this.oParentObject.w_CODAZI;
                and ESCODESE = this.oParentObject.w_CODESE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          use
          if i_Rows=0
            * --- Read from ESERCIZI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ESERCIZI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ESCODESE,ESINIESE,ESFINESE"+;
                " from "+i_cTable+" ESERCIZI where ";
                    +"ESCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ESCODESE,ESINIESE,ESFINESE;
                from (i_cTable) where;
                    ESCODAZI = this.oParentObject.w_CODAZI;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_CODESE = NVL(cp_ToDate(_read_.ESCODESE),cp_NullValue(_read_.ESCODESE))
              this.oParentObject.w_INIESE = NVL(cp_ToDate(_read_.ESINIESE),cp_NullValue(_read_.ESINIESE))
              this.oParentObject.w_FINESE = NVL(cp_ToDate(_read_.ESFINESE),cp_NullValue(_read_.ESFINESE))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.oParentObject.NotifyEvent("Aggiorna")
      case this.pAZIONE=="CC"
        if !cp_IsAdminWin()
          if ah_YesNo(MSG_RESTART_AS_ADMIN_QP)
            AdHocRunAs()
          endif
          * --- N.B. in caso di richiesta di riavvio come amministratore la chiususra di questo processo non � immediata quindi devo decidere sempre cosa fare dopo la richiesta:
          *     in questo caso, qualsiasi scelta sia stata fatta dall'utente non � possibile procedere con la configurazione del servizio, quindi forzo l'opzione per l'icona di accesso silente
          this.oParentObject.w_TIPOCONF = "ICO"
        endif
    endcase
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    FH=FOPEN(this.oParentObject.w_FILECNF, 0)
    if FH>0
      this.w_PVARDEF = .F.
      this.w_PVARON = .F.
      do while !FEOF(FH)
        this.w_LETTURA = FGETS(FH, 8192)
        if "PUBLIC" $ UPPER(ALLTRIM(this.w_LETTURA)) AND "G_CRYPTSILENTCONNECT" $ UPPER(ALLTRIM(this.w_LETTURA)) AND !LEFT(ALLTRIM(this.w_LETTURA),1)="*"
          this.w_PVARDEF = .T.
        endif
        if UPPER(ALLTRIM(STRTRAN(this.w_LETTURA," ","")))="G_CRYPTSILENTCONNECT=.T." AND !LEFT(ALLTRIM(this.w_LETTURA),1)="*"
          this.w_PVARON = .T.
        endif
      enddo
      =FCLOSE(FH)
      if this.w_PVARDEF and this.w_PVARON
        this.w_VARCNFOK = .T.
      else
        this.w_VARCNFOK = .F.
      endif
    else
      this.w_MESSAGGI = "� stata attivata la criptazione della stringa di accesso silente ma non � possibile effettuare le verifiche per il CNF specificato. Procedere comunque alla creazione dell'icona?"
      this.w_VARCNFOK = .F.
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.oParentObject.w_PARACSIL = "i_CODUTE="+ALLTRIM(STR(this.oParentObject.w_USERIN))
    if !EMPTY(this.oParentObject.w_CODAZI)
      this.oParentObject.w_PARACSIL = this.oParentObject.w_PARACSIL+";i_CODAZI='"+ALLTRIM(this.oParentObject.w_CODAZI)+"'"
    endif
    if !EMPTY(this.oParentObject.w_PWDUSER)
      if this.w_MAKELNK
        this.oParentObject.w_PARACSIL = this.oParentObject.w_PARACSIL+";PWD='"+ALLTRIM(this.oParentObject.w_PWDUSER)+"'"
      else
        this.oParentObject.w_PARACSIL = this.oParentObject.w_PARACSIL+";PWD='"+REPLICATE("*",LEN(ALLTRIM(this.oParentObject.w_PWDUSER)))+"'"
      endif
    endif
    if !EMPTY(this.oParentObject.w_CODBUN)
      this.oParentObject.w_PARACSIL = this.oParentObject.w_PARACSIL+";g_CODBUN='"+ALLTRIM(this.oParentObject.w_CODBUN)+"'"
    endif
    if !EMPTY(this.oParentObject.w_CODESE)
      this.oParentObject.w_PARACSIL = this.oParentObject.w_PARACSIL+";g_CODESE='"+ALLTRIM(this.oParentObject.w_CODESE)+"'"
    endif
    if this.oParentObject.w_TIPO="M"
      this.oParentObject.w_PARACSIL = this.oParentObject.w_PARACSIL+";i_bMobileMode=.T."
    endif
    if this.oParentObject.w_FLASCRYP="S"
      this.oParentObject.w_PARACSIL = CifraCnf( ALLTRIM(this.oParentObject.w_PARACSIL) , "C" )
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserimento attivit� programmata
    *     Inserendo l'esecuzione all'avvio nei registri di sistema non si riesce ad ottenere l'esecuzione come amministratore
    * --- --
    * --- Devo cancellare la vecchia chiave di registro per evitare che parta prima dell'attivit� pianificata e senza diritti di amministratore
    this.w_ERROR = 0
    this.Page_8()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.w_ERROR=-1
      ah_ErrorMsg("Non � stato possibile cancellare la vecchia chiave di registro per l'esecuzione all'avvio, sar� necessario provvedere manualmente")
    endif
    * --- --
    Local l_oldErr, l_ActionErr 
 m.l_ActionError = 0 
 m.l_oldErr = On("Error") 
 On Error l_ActionError = -1
    this.w_Task = this.w_Service.NewTask(0)
    this.w_Task.RegistrationInfo.Description = "Zucchetti Service Manager. Copyright (C) by Zucchetti S.p.a."
    this.w_Task.RegistrationInfo.Author = "Zucchetti S.p.a."
    this.w_Task.Principal.LogonType = 3
    this.w_Task.Principal.RunLevel = 1
    this.w_Task.Settings.IdleSettings.WaitTimeout = ""
    this.w_Task.Settings.IdleSettings.IdleDuration = ""
    this.w_Task.Settings.Enabled = .T.
    this.w_Task.Settings.StartWhenAvailable = .F.
    this.w_Task.Settings.Hidden = .F.
    this.w_Task.Settings.ExecutionTimeLimit = "P3D"
    this.w_Trigger = this.w_Task.Triggers.Create(9)
    this.w_Trigger.Id = "ZSrvMng_Boot"
    this.w_Trigger.Enabled = .T.
    this.w_Action = this.w_Task.Actions.Create(0)
    this.w_Action.Path = '"' + this.oParentObject.w_ZSRVMNG + '"'
    this.w_Action.Arguments = '"' + addbs(JustPath(this.oParentObject.w_ZSRVMNG)) + '"'
    this.w_Folder.RegisterTaskDefinition(this.w_TaskName, this.w_Task, 6, "", "", 3)     
    * --- Verifico se c'� stato un errore
    if m.l_ActionError=-1
      ah_ErrorMsg("Non � stato possibile inserire l'attivit� programmata per l'esecuzione all'avvio, sar� necessario avviare Zucchetti Service Manager manualmente")
    endif
    On Error &l_oldErr
    this.w_ERROR = m.l_ActionError
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Cancellamento attivit� programmata
    * --- --
    * --- Devo cancellare la vecchia chiave di registro per evitare che parta prima dell'attivit� pianificata e senza diritti di amministratore
    this.w_ERROR = 0
    this.Page_8()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.w_ERROR=-1
      ah_ErrorMsg("Non � stato possibile cancellare la vecchia chiave di registro per l'esecuzione all'avvio, sar� necessario provvedere manualmente")
    endif
    * --- --
    Local l_oldErr, l_ActionErr 
 m.l_ActionError = 0 
 m.l_oldErr = On("Error") 
 On Error l_ActionError = -1
    this.w_TaskList = this.w_Folder.GetTasks(0)
    this.w_TaskDeleted = .F.
    this.w_TaskIndex = 1
    do while !this.w_TaskDeleted And this.w_TaskIndex<=this.w_TaskList.Count
      if this.w_TaskList.Item(this.w_TaskIndex).Name==this.w_TaskName
        this.w_Folder.DeleteTask(this.w_TaskName, 0)     
        this.w_TaskDeleted = .T.
      endif
      this.w_TaskIndex = this.w_TaskIndex + 1
    enddo
    * --- Verifico se c'� stato un errore
    if m.l_ActionError=-1
      ah_errormsg("Non � stato possibile eliminare l'attivit� programmata per l'esecuzione all'avvio, sar� necessario provvedere manualmente tramite l'utilit� di pianificazione")
    endif
    On Error &l_oldErr
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --Creazione servizio ZSRVMNG
    *-- define per gestione del servizio 
 #DEFINE SC_MANAGER_CREATE_SERVICE 2 && necessario al Manager per creare il servizio 
 #DEFINE SC_MANAGER_ALL_ACCESS 0xF003F 
 #DEFINE SERVICE_ALL_ACCESS 0xF01FF 
 #DEFINE SERVICE_WIN32_OWN_PROCESS 0x00000010 
 #DEFINE SERVICE_AUTO_START 0x00000002 
 #DEFINE SERVICE_ERROR_NORMAL 0x00000001 && settaggio messaggi di errore "normale" 
 #DEFINE SERVICE_QUERY_STATUS 0x0004
     
 DECLARE INTEGER CloseServiceHandle IN advapi32 INTEGER hSCObject 
 
 DECLARE INTEGER CreateService IN advapi32; 
 INTEGER hSCManager, STRING lpServiceName, STRING lpDisplayName,; 
 LONG dwDesiredAccess, LONG dwServiceType, LONG dwStartType,; 
 LONG dwErrorControl, STRING lpBinaryPathName,; 
 STRING lpLoadOrderGroup, LONG lpdwTagId, STRING lpDependencies,; 
 STRING lpServiceStartName, STRING lpPassword 
 
 DECLARE INTEGER OpenSCManager IN advapi32; 
 STRING lpMachineName, STRING lpDatabaseName,; 
 LONG dwDesiredAccess 
 
 DECLARE INTEGER OpenService IN advapi32; 
 INTEGER hSCManager, STRING lpServiceName,; 
 LONG dwDesiredAccess
    LOCAL hManager, hService, PATH_TO_EXE, PATH_TO_EXE_PARAM,cName,cPswd 
 hManager=0 
 hService=0 
 cName=IIF(EMPTY(this.w_ZSACCOUN),NULL,this.w_ZSACCOUN) 
 cPswd=IIF(EMPTY(this.w_ZSPSWD),NULL,this.w_ZSPSWD) 
 PATH_TO_EXE = alltrim(this.oParentObject.w_ZMAINSER) 
 PATH_TO_EXE_PARAM = alltrim(this.oParentObject.w_ZMAINSER)+' "'+strtran(alltrim(upper(this.oParentObject.w_ZMAINSER)),"ZMAINSERVICE.EXE",this.oParentObject.w_ZSRVINI)+'"'
    if not(file(strtran(alltrim(this.oParentObject.w_ZMAINSER),"ZAMINSERVICE.EXE",this.oParentObject.w_ZSRVINI)))
      ah_errormsg("Errore nella lettura del file di configurazione. Impossibile creare il servizio")
      i_retcode = 'stop'
      return
    endif
    * --- Controllo esistenza servizio
    hManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS)
    hService = OpenService(hManager, this.oParentObject.w_ZSRVNAME, SERVICE_QUERY_STATUS)
    CloseServiceHandle(hService) 
 CloseServiceHandle(hManager)
    * --- Preparazione alla creazione del servizio
    * --- Se hService � uguale a 0 significa che il servizio non esiste, quindi devo crearlo
    if hService=0
      hManager=0 
 hService=0
      hManager = OpenSCManager(NULL, NULL, SC_MANAGER_CREATE_SERVICE)
      if hManager = 0
        ah_errormsg("Errore nell'apertura del SCManager")
        = CloseServiceHandle(hManager)
        i_retcode = 'stop'
        return
      endif
      hService = CreateService(hManager, this.oParentObject.w_ZSRVNAME, this.oParentObject.w_ZSRVNAME, SERVICE_ALL_ACCESS, SERVICE_WIN32_OWN_PROCESS, SERVICE_AUTO_START, SERVICE_ERROR_NORMAL, PATH_TO_EXE_PARAM, NULL, 0, NULL, cName, cPswd)
      if hService = 0
        ah_errormsg("Errore nella creazione del servizio")
      else
        = CloseServiceHandle(hService)
      endif
      = CloseServiceHandle(hManager)
    endif
  endproc


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Il service manager deve essere sempre eseguito come amministratore, altrimenti non sarebbe
    *     in grado di eseguire operazioni sui servizi che deve gestire
    Local l_oldErr, l_RegErr 
 m.l_RegErr = 0 
 m.l_oldErr = On("Error") 
 On Error l_RegErr = -1
    this.w_HANDLE = OpenRegistryKey(this.w_HKEY_CURRENT_USER, "Software\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Layers")
    if m.l_RegErr=-1
      m.l_RegErr = 0
      this.w_HANDLE = CreateRegistryKey(this.w_HKEY_CURRENT_USER, "Software\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Layers")
    endif
    WriteRegistryKey(this.w_Handle, "~ RUNASADMIN", this.oParentObject.w_ZSRVMNG)
    CloseRegistryKey(this.w_Handle)
    On Error &l_oldErr
    this.w_ERROR = m.l_RegErr
  endproc


  procedure Page_8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --Cancellazione key da registro di sistema
     
 #DEFINE HKEY_LOCAL_MACHINE 2147483650 && (HKEY) 0x80000002 
 #DEFINE SECURITY_ACCESS_MASK 983103 
 
 DECLARE INTEGER RegCreateKeyEx IN advapi32; 
 INTEGER hKey,; 
 STRING lpSubKey,; 
 INTEGER Reserved,; 
 STRING lpClass,; 
 INTEGER dwOptions,; 
 INTEGER samDesired,; 
 INTEGER lpSecurityAttributes,; 
 INTEGER @ phkResult,; 
 INTEGER @ lpdwDisposition 
 
 DECLARE INTEGER RegDeleteValue IN advapi32; 
 INTEGER hKey,; 
 STRING lpValueName 
 
 PUBLIC nRESULT,nDISPLAY 
 nRESULT=0 
 nDISPLAY=0
    if (RegCreateKeyEx(HKEY_LOCAL_MACHINE,"Software\Microsoft\Windows\CurrentVersion\Run",0,"REG_SZ",0,SECURITY_ACCESS_MASK,0,@nRESULT,@nDISPLAY))<>0
      this.w_ERROR = -1
    else
      nDISPLAY=RegDeleteValue(nRESULT,"ZSRVMNG")
      * --- -- Se nDISPLAY = 2 siginifica che la chiave gi� non esiste.
      if nDISPLAY<>0 and nDISPLAY<>2
        this.w_ERROR = -1
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pAZIONE)
    this.pAZIONE=pAZIONE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='CPUSRGRP'
    this.cWorkTables[2]='GROUPSJ'
    this.cWorkTables[3]='AZIENDA'
    this.cWorkTables[4]='UTE_AZI'
    this.cWorkTables[5]='BUSIUNIT'
    this.cWorkTables[6]='ESERCIZI'
    this.cWorkTables[7]='CPUSERS'
    return(this.OpenAllTables(7))

  proc CloseCursors()
    if used('_Curs_CPUSRGRP')
      use in _Curs_CPUSRGRP
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAZIONE"
endproc
