* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_brl                                                        *
*              Controllo dati e dettaglio rilevazioni                          *
*                                                                              *
*      Author: Zucchetti tam Spa                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_344]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-04-17                                                      *
* Last revis.: 2011-07-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTIPO
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_brl",oParentObject,m.pTIPO)
return(i_retval)

define class tgsma_brl as StdBatch
  * --- Local variables
  pTIPO = space(1)
  w_OBJMOV = .NULL.
  w_PUNPAD = .NULL.
  w_MESS = space(10)
  w_KEY = space(254)
  w_SERIALE = space(10)
  w_CODRIL = space(10)
  w_DATRIL = ctod("  /  /  ")
  w_DATASTAM = ctod("  /  /  ")
  w_DRARTIC = space(20)
  w_MGUBIC = space(1)
  w_FLLOTT = space(1)
  w_CODMAG = space(5)
  w_ADDRIL = space(5)
  w_CODUBI = space(20)
  w_NUMDOC = 0
  w_NUMSER = space(15)
  w_TMPC = space(40)
  w_KEYSAL = space(40)
  w_KEYULO = space(40)
  w_ZOOM = .NULL.
  w_CODRIC = space(41)
  w_CODLOT = space(5)
  w_CODMAT = space(40)
  w_GESMAT = space(1)
  w_CODART = space(20)
  w_CODVAR = space(20)
  w_APPSER = space(15)
  w_DTOBS1 = ctod("  /  /  ")
  w_OBTEST = ctod("  /  /  ")
  w_ARTVALIDO = space(2)
  w_APPCODART = space(20)
  w_APPCAL = space(12)
  w_QTAUM1 = 0
  w_CODCOM = space(15)
  w_ORECO = 0
  w_TYPERIL = space(1)
  w_CODUTE = 0
  w_DRSERIAL = space(15)
  w_LOTART = space(20)
  w_UNIMIS = space(2)
  w_MATRMAG = space(5)
  w_MATRUBI = space(20)
  w_MATRLOT = space(20)
  w_MATRLAR = space(20)
  w_MATRULO = space(40)
  w_NOME = space(20)
  w_LOOP = 0
  * --- WorkFile variables
  ADDERILE_idx=0
  ART_ICOL_idx=0
  CODIRILE_idx=0
  MAGAZZIN_idx=0
  RILEVAZI_idx=0
  UBICAZIO_idx=0
  KEY_ARTI_idx=0
  UNIMIS_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- RETTIFICHE INVENTARIALI . CONTROLLI GENERALI
    *     
    *     Se pTIPO='Z' carica lo zoom GSMA_KDR (Init)
    *     Se pTIPO='D' carica il record dell'anagrafica Dati rilevati  da GSMA_KDR (Bottone Dettagli)
    *     se pTIPO='A'  avvalora in fase di caricamento di un record il campo DRNUMDOC da GSMA_ADR
    *     Se pTIPO='C' Verifica se tutte le rilevazioni associate sono gi� state confermate da GSMA_SDR
    *     Se pTIPO='E' GSMA_SDR - Selezi Changed 
    *     Se pTIPO='U' GSMA_ADR -  Changed DRADDRIL (Addetto alla rilevazione)
    *     Se pTIPO='N' GSMA_ADR -  CheckForm Controlla Univocit� documento associato all'utente ed obbligatoriet� Articolo
    *     Se pTIPO='M' da GSMA_ADR - Bottone di visualizzazione Movimento di Magazzino collegato (lancia la gestione) 
    *     Se pTIPO='R' da GSMA_KDR rieseguo la ricerca dati nel confronto dati
    *     Se pTIPO='I' da GSMA_SDR Campo Matricola
    *     Se pTIPO='O' ZOOM Campo Matricola (GSMA_ADR, GSMA_SRT)
    *     Se pTIPO='S' Changed DRCODRIC controllo obsolescenza Codice di Ricerca
    this.w_PUNPAD = this.oParentObject
    do case
      case this.pTIPO="D"
        * --- Carica il record selezionato da GSMA_KDR
        this.w_SERIALE = this.w_PUNPAD.w_SERIALE
        * --- Oggetto nagrafica Dati rilevati
        this.w_OBJMOV = GSMA_ADR()
        * --- Locale per sringa da passare alla querykeyset
        this.w_KEY = "DRSERIAL='"+this.w_SERIALE+"'"
        * --- Testo se l'utente pu� aprire la gestione
        if not (this.w_OBJMOV.bSec1)
          Ah_ErrorMsg("Impossibile aprire la funzionalit� selezionata!",48,"")
          i_retcode = 'stop'
          return
        endif
        * --- Lancio l'oggetto in interrogazione
        this.w_OBJMOV.cFunction = "Query"
        this.w_OBJMOV.QueryKeySet(this.w_KEY,"")     
        this.w_OBJMOV.LoadRecWarn()     
        oCpToolBar.SetQuery()
      case this.pTIPO="A"
        * --- Legge il massimo seriale
        * --- Select from RILEVAZI
        i_nConn=i_TableProp[this.RILEVAZI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.RILEVAZI_idx,2],.t.,this.RILEVAZI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select MAX(DRNUMDOC) AS NUMERO,MAX(DRSERIAL) AS NUMSER  from "+i_cTable+" RILEVAZI ";
               ,"_Curs_RILEVAZI")
        else
          select MAX(DRNUMDOC) AS NUMERO,MAX(DRSERIAL) AS NUMSER from (i_cTable);
            into cursor _Curs_RILEVAZI
        endif
        if used('_Curs_RILEVAZI')
          select _Curs_RILEVAZI
          locate for 1=1
          do while not(eof())
          this.w_NUMSER = NVL(NUMSER,SPACE(15))
            select _Curs_RILEVAZI
            continue
          enddo
          use
        endif
        * --- Leggo il magazzino (eventuale ubicazione) il codice rilevazione , l'addetto alla rilevazione e il numero documento
        * --- Read from RILEVAZI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.RILEVAZI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.RILEVAZI_idx,2],.t.,this.RILEVAZI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DRCODMAG,DRADDRIL,DRUBICAZ,DRCODRIL,DRNUMDOC"+;
            " from "+i_cTable+" RILEVAZI where ";
                +"DRSERIAL = "+cp_ToStrODBC(this.w_NUMSER);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DRCODMAG,DRADDRIL,DRUBICAZ,DRCODRIL,DRNUMDOC;
            from (i_cTable) where;
                DRSERIAL = this.w_NUMSER;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CODMAG = NVL(cp_ToDate(_read_.DRCODMAG),cp_NullValue(_read_.DRCODMAG))
          this.w_ADDRIL = NVL(cp_ToDate(_read_.DRADDRIL),cp_NullValue(_read_.DRADDRIL))
          this.w_CODUBI = NVL(cp_ToDate(_read_.DRUBICAZ),cp_NullValue(_read_.DRUBICAZ))
          this.w_CODRIL = NVL(cp_ToDate(_read_.DRCODRIL),cp_NullValue(_read_.DRCODRIL))
          this.w_NUMDOC = NVL(cp_ToDate(_read_.DRNUMDOC),cp_NullValue(_read_.DRNUMDOC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_PUNPAD.w_DRCODRIL= this.w_CODRIL
        this.w_PUNPAD.w_DRADDRIL= this.w_ADDRIL
        this.w_PUNPAD.w_DRCODMAG=this.w_CODMAG
        * --- Quando carico un nuovo record aumenta di uno il numero del documento
        this.w_PUNPAD.w_DRNUMDOC=this.w_NUMDOC + 1
        * --- Read from CODIRILE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CODIRILE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CODIRILE_idx,2],.t.,this.CODIRILE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "RIDATRIL"+;
            " from "+i_cTable+" CODIRILE where ";
                +"RICODICE = "+cp_ToStrODBC(this.w_CODRIL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            RIDATRIL;
            from (i_cTable) where;
                RICODICE = this.w_CODRIL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DATRIL = NVL(cp_ToDate(_read_.RIDATRIL),cp_NullValue(_read_.RIDATRIL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Data Rilevazione
        this.w_PUNPAD.w_DATRIL= this.w_DATRIL
        * --- Read from ADDERILE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ADDERILE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ADDERILE_idx,2],.t.,this.ADDERILE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARDESCRI"+;
            " from "+i_cTable+" ADDERILE where ";
                +"ARCODICE = "+cp_ToStrODBC(this.w_ADDRIL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARDESCRI;
            from (i_cTable) where;
                ARCODICE = this.w_ADDRIL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_TMPC = NVL(cp_ToDate(_read_.ARDESCRI),cp_NullValue(_read_.ARDESCRI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Descrizione addetto alla rilevazione
        this.w_PUNPAD.w_DESADD=this.w_TMPC
        this.w_TMPC = SPACE(40)
        * --- Read from MAGAZZIN
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MGDESMAG,MGFLUBIC"+;
            " from "+i_cTable+" MAGAZZIN where ";
                +"MGCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MGDESMAG,MGFLUBIC;
            from (i_cTable) where;
                MGCODMAG = this.w_CODMAG;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_TMPC = NVL(cp_ToDate(_read_.MGDESMAG),cp_NullValue(_read_.MGDESMAG))
          this.w_MGUBIC = NVL(cp_ToDate(_read_.MGFLUBIC),cp_NullValue(_read_.MGFLUBIC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Descrizione Magazzino
        this.w_PUNPAD.w_DESMAG=this.w_TMPC
        if this.w_MGUBIC="S"
          * --- Dati relativi all'Ubicazione
          this.w_PUNPAD.w_MGUBIC=this.w_MGUBIC
          this.w_PUNPAD.w_DRUBICAZ=this.w_CODUBI
          this.w_TMPC = SPACE(40)
          * --- Read from UBICAZIO
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.UBICAZIO_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.UBICAZIO_idx,2],.t.,this.UBICAZIO_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "UBDESCRI"+;
              " from "+i_cTable+" UBICAZIO where ";
                  +"UBCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                  +" and UBCODICE = "+cp_ToStrODBC(this.w_CODUBI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              UBDESCRI;
              from (i_cTable) where;
                  UBCODMAG = this.w_CODMAG;
                  and UBCODICE = this.w_CODUBI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_TMPC = NVL(cp_ToDate(_read_.UBDESCRI),cp_NullValue(_read_.UBDESCRI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_PUNPAD.w_UBIDESCRI=this.w_TMPC
        endif
      case this.pTIPO="Z"
        this.w_ZOOM = this.w_PUNPAD.w_ZoomDet
        if g_MATR<>"S"
          this.w_NOME = "DRCODMAT"
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if g_APPLICATION = "ADHOC REVOLUTION"
          * --- Nascondo campo variante
          this.w_NOME = "DRCODVAR"
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- Estrazione Dati relativi al Magazzino, Codice Rivelazione  e Riga selezionata su GSMA_SDR
        this.w_CODMAG = this.w_PUNPAD.w_CODMAG
        this.w_CODRIL = this.w_PUNPAD.w_CODRIL
        this.w_KEYSAL = this.w_PUNPAD.w_KEYSAL
        this.w_KEYULO = this.w_PUNPAD.w_KEYULO
        this.w_CODMAT = this.w_PUNPAD.w_CODMAT
        if UPPER(g_APPLICATION) = "AD HOC ENTERPRISE"
          this.w_CODCOM = this.w_PUNPAD.w_DRCODCOM
        endif
        * --- Query per reperire i dati rilevati
        VQ_EXEC("QUERY\GSMA_KDR.VQR",this,"__TMP__")
        * --- Inserisco il risultato del cursore  nello zoom
        Select __TMP__
        Go Top
        Scan
        Scatter Memvar
        Select (this.w_ZOOM.cCursor)
        Append blank
        Gather memvar
        Endscan
        SELECT __TMP__
        USE
        SELECT ( this.w_ZOOM.cCursor )
        GO TOP
        * --- Refresh della griglia
        this.w_zoom.grd.refresh
      case this.pTIPO="C"
        this.w_CODMAG = this.w_PUNPAD.w_CODMAG
        this.w_CODRIL = this.w_PUNPAD.w_CODRIL
        MM = this.w_PUNPAD.w_ZoomConf.cCursor
        SELECT (MM)
        this.w_KEYSAL = DRKEYSAL
        this.w_KEYULO = DRKEYULO
        this.w_CODMAT = NVL(DRCODMAT,SPACE(40))
        this.w_TYPERIL = FLNONRIL
        if XCHK=1 
          if EMPTY(this.w_CODMAT)
            * --- Read from RILEVAZI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.RILEVAZI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.RILEVAZI_idx,2],.t.,this.RILEVAZI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "DRSERIAL"+;
                " from "+i_cTable+" RILEVAZI where ";
                    +"DRCODRIL = "+cp_ToStrODBC(this.w_CODRIL);
                    +" and DRCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                    +" and DRKEYSAL = "+cp_ToStrODBC(this.w_KEYSAL);
                    +" and DRKEYULO = "+cp_ToStrODBC(this.w_KEYULO);
                    +" and DRCONFER = "+cp_ToStrODBC(" ");
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                DRSERIAL;
                from (i_cTable) where;
                    DRCODRIL = this.w_CODRIL;
                    and DRCODMAG = this.w_CODMAG;
                    and DRKEYSAL = this.w_KEYSAL;
                    and DRKEYULO = this.w_KEYULO;
                    and DRCONFER = " ";
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_SERIALE = NVL(cp_ToDate(_read_.DRSERIAL),cp_NullValue(_read_.DRSERIAL))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          else
            * --- Read from RILEVAZI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.RILEVAZI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.RILEVAZI_idx,2],.t.,this.RILEVAZI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "DRSERIAL"+;
                " from "+i_cTable+" RILEVAZI where ";
                    +"DRCODRIL = "+cp_ToStrODBC(this.w_CODRIL);
                    +" and DRCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                    +" and DRKEYSAL = "+cp_ToStrODBC(this.w_KEYSAL);
                    +" and DRKEYULO = "+cp_ToStrODBC(this.w_KEYULO);
                    +" and DRCODMAT = "+cp_ToStrODBC(this.w_CODMAT);
                    +" and DRCONFER = "+cp_ToStrODBC(" ");
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                DRSERIAL;
                from (i_cTable) where;
                    DRCODRIL = this.w_CODRIL;
                    and DRCODMAG = this.w_CODMAG;
                    and DRKEYSAL = this.w_KEYSAL;
                    and DRKEYULO = this.w_KEYULO;
                    and DRCODMAT = this.w_CODMAT;
                    and DRCONFER = " ";
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_SERIALE = NVL(cp_ToDate(_read_.DRSERIAL),cp_NullValue(_read_.DRSERIAL))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          do case
            case i_ROWS=0 AND this.oParentObject.w_FLNONRIL<>"S"
              AA=ALLTRIM(STR(this.w_PUNPAD.w_ZoomConf.grd.ColumnCount))
              this.w_PUNPAD.w_ZoomConf.grd.Column&AA..chk.Value = 0
              this.w_MESS = "Impossibile selezionare%0Le dichiarazioni associate sono gi� processate"
              ah_ErrorMsg(this.w_MESS,"!")
            case i_ROWS>0 AND this.oParentObject.w_FLNONRIL="S" AND this.w_TYPERIL="S"
              AA=ALLTRIM(STR(this.w_PUNPAD.w_ZoomConf.grd.ColumnCount))
              this.w_PUNPAD.w_ZoomConf.grd.Column&AA..chk.Value = 0
              this.w_MESS = "Impossibile selezionare%0Rilevazione gi� caricata. Aggiornare i dati"
              ah_ErrorMsg(this.w_MESS,"!")
            otherwise
              if UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE"
                if Not EMPTY(this.w_CODMAT)
                  * --- Se presente Rettifica = 1 con Matricola, controllo la presenza di un eventuale rettifica = 0
                  *     della stessa matricola, stesso Keysal. Se presente, prima devo GENERARE il movimento 
                  *     di scarico. Successivamente il movimento di Carico
                  SELECT (MM)
                  this.w_ORECO = RECNO()
                  Locate for Drkeysal=this.w_KeySal and Drcodmat = this.w_CodMat and Drqtaes1 = 0 and nvl(Procedur,0) = 1and Recno() <> this.w_ORECO
                  if found()
                    ah_ErrorMsg("Impossibile selezionare%0� presente una rettifica a zero della stessa matricola%0Occorre prima generare il movimento di scarico di magazzino",48,"" )
                    AA=ALLTRIM(STR(this.w_PUNPAD.w_ZoomConf.grd.ColumnCount))
                    this.w_PUNPAD.w_ZoomConf.grd.Column&AA..chk.Value = 1
                    GOTO this.w_ORECO
                    AA=ALLTRIM(STR(this.w_PUNPAD.w_ZoomConf.grd.ColumnCount))
                    this.w_PUNPAD.w_ZoomConf.grd.Column&AA..chk.Value = 0
                  else
                    * --- Mi riposiziono sulla Riga corrente
                    GOTO this.w_ORECO
                  endif
                endif
              endif
          endcase
        endif
      case this.pTIPO="E"
        * --- Evento w_SELEZI Changed
        * --- Seleziona/Deselezione la Righe
        this.w_CODMAG = this.w_PUNPAD.w_CODMAG
        this.w_CODRIL = this.w_PUNPAD.w_CODRIL
        if USED(this.w_PUNPAD.w_ZoomConf.cCursor)
          MM = this.w_PUNPAD.w_ZoomConf.cCursor
          if this.w_PUNPAD.w_SELEZI = "S"
            * --- Seleziona Tutto 
            if Upper(This.oParentObject.Class )="TGSMA_KIR"
              UPDATE (MM) SET XCHK=1
            else
              SELECT (MM)
              GO TOP
              SCAN
              this.w_KEYSAL = DRKEYSAL
              this.w_KEYULO = DRKEYULO
              this.w_CODMAT = NVL(DRCODMAT,SPACE(40))
              this.w_TYPERIL = FLNONRIL
              if EMPTY(this.w_CODMAT)
                * --- Read from RILEVAZI
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.RILEVAZI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.RILEVAZI_idx,2],.t.,this.RILEVAZI_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "DRSERIAL"+;
                    " from "+i_cTable+" RILEVAZI where ";
                        +"DRCODRIL = "+cp_ToStrODBC(this.w_CODRIL);
                        +" and DRCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                        +" and DRKEYSAL = "+cp_ToStrODBC(this.w_KEYSAL);
                        +" and DRKEYULO = "+cp_ToStrODBC(this.w_KEYULO);
                        +" and DRCONFER = "+cp_ToStrODBC(" ");
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    DRSERIAL;
                    from (i_cTable) where;
                        DRCODRIL = this.w_CODRIL;
                        and DRCODMAG = this.w_CODMAG;
                        and DRKEYSAL = this.w_KEYSAL;
                        and DRKEYULO = this.w_KEYULO;
                        and DRCONFER = " ";
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_SERIALE = NVL(cp_ToDate(_read_.DRSERIAL),cp_NullValue(_read_.DRSERIAL))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                if i_ROWS <> 0 OR this.w_TYPERIL="S"
                  SELECT (MM)
                  REPLACE XCHK WITH 1
                endif
              else
                if g_APPLICATION = "ADHOC REVOLUTION"
                  * --- Read from RILEVAZI
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.RILEVAZI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.RILEVAZI_idx,2],.t.,this.RILEVAZI_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "DRSERIAL"+;
                      " from "+i_cTable+" RILEVAZI where ";
                          +"DRCODRIL = "+cp_ToStrODBC(this.w_CODRIL);
                          +" and DRCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                          +" and DRKEYSAL = "+cp_ToStrODBC(this.w_KEYSAL);
                          +" and DRKEYULO = "+cp_ToStrODBC(this.w_KEYULO);
                          +" and DRCODMAT = "+cp_ToStrODBC(this.w_CODMAT);
                          +" and DRCONFER = "+cp_ToStrODBC(" ");
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      DRSERIAL;
                      from (i_cTable) where;
                          DRCODRIL = this.w_CODRIL;
                          and DRCODMAG = this.w_CODMAG;
                          and DRKEYSAL = this.w_KEYSAL;
                          and DRKEYULO = this.w_KEYULO;
                          and DRCODMAT = this.w_CODMAT;
                          and DRCONFER = " ";
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_SERIALE = NVL(cp_ToDate(_read_.DRSERIAL),cp_NullValue(_read_.DRSERIAL))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                  if i_ROWS <> 0 OR this.w_TYPERIL="S"
                    SELECT (MM)
                    REPLACE XCHK WITH 1
                  endif
                else
                  * --- Se presente Rettifica = 1 con Matricola, controllo la presenza di un eventuale rettifica = 0
                  *     della stessa matricola, stesso Keysal. Se presente, prima devo GENERARE il movimento 
                  *     di scarico. Successivamente il movimento di Carico
                  this.w_ORECO = RECNO()
                  Locate for Drkeysal=this.w_KeySal and Drcodmat = this.w_CodMat and Drqtaes1 = 0 and nvl(Procedur,0) = 1and Recno() <> this.w_ORECO
                  if found()
                    * --- Mi riposiziono sulla Riga corrente e non setto il flag = 1
                    GOTO this.w_ORECO
                  else
                    * --- Mi posiziono sulla riga
                    GOTO this.w_ORECO
                    * --- Se non processata attivo il flag 
                    * --- Read from RILEVAZI
                    i_nOldArea=select()
                    if used('_read_')
                      select _read_
                      use
                    endif
                    i_nConn=i_TableProp[this.RILEVAZI_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.RILEVAZI_idx,2],.t.,this.RILEVAZI_idx)
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select "+;
                        "DRSERIAL"+;
                        " from "+i_cTable+" RILEVAZI where ";
                            +"DRCODRIL = "+cp_ToStrODBC(this.w_CODRIL);
                            +" and DRCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                            +" and DRKEYSAL = "+cp_ToStrODBC(this.w_KEYSAL);
                            +" and DRKEYULO = "+cp_ToStrODBC(this.w_KEYULO);
                            +" and DRCODMAT = "+cp_ToStrODBC(this.w_CODMAT);
                            +" and DRCONFER = "+cp_ToStrODBC(" ");
                             ,"_read_")
                      i_Rows=iif(used('_read_'),reccount(),0)
                    else
                      select;
                        DRSERIAL;
                        from (i_cTable) where;
                            DRCODRIL = this.w_CODRIL;
                            and DRCODMAG = this.w_CODMAG;
                            and DRKEYSAL = this.w_KEYSAL;
                            and DRKEYULO = this.w_KEYULO;
                            and DRCODMAT = this.w_CODMAT;
                            and DRCONFER = " ";
                         into cursor _read_
                      i_Rows=_tally
                    endif
                    if used('_read_')
                      locate for 1=1
                      this.w_SERIALE = NVL(cp_ToDate(_read_.DRSERIAL),cp_NullValue(_read_.DRSERIAL))
                      use
                    else
                      * --- Error: sql sentence error.
                      i_Error = MSG_READ_ERROR
                      return
                    endif
                    select (i_nOldArea)
                    if i_ROWS <> 0 Or this.w_TYPERIL="S"
                      REPLACE XCHK WITH 1
                    endif
                  endif
                endif
              endif
              SELECT (MM)
              ENDSCAN
            endif
          else
            * --- deseleziona Tutto
            UPDATE (MM) SET XCHK=0 
          endif
          * --- Si riposiziona all'inizio
          SELECT (MM)
          GO TOP
        endif
      case this.pTIPO="U"
        * --- Legge il massimo seriale legato a quell'utente
        this.w_ADDRIL = this.w_PUNPAD.w_DRADDRIL
        * --- Select from RILEVAZI
        i_nConn=i_TableProp[this.RILEVAZI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.RILEVAZI_idx,2],.t.,this.RILEVAZI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select MAX(DRSERIAL) AS NUMSER  from "+i_cTable+" RILEVAZI ";
              +" where DRADDRIL="+cp_ToStrODBC(this.w_ADDRIL)+"";
               ,"_Curs_RILEVAZI")
        else
          select MAX(DRSERIAL) AS NUMSER from (i_cTable);
           where DRADDRIL=this.w_ADDRIL;
            into cursor _Curs_RILEVAZI
        endif
        if used('_Curs_RILEVAZI')
          select _Curs_RILEVAZI
          locate for 1=1
          do while not(eof())
          this.w_NUMSER = NVL(NUMSER,SPACE(15))
            select _Curs_RILEVAZI
            continue
          enddo
          use
        endif
        * --- Legge il Num.Documento legato all'ultima rilevazione dell'utente indicato
        * --- Read from RILEVAZI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.RILEVAZI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.RILEVAZI_idx,2],.t.,this.RILEVAZI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DRNUMDOC"+;
            " from "+i_cTable+" RILEVAZI where ";
                +"DRSERIAL = "+cp_ToStrODBC(this.w_NUMSER);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DRNUMDOC;
            from (i_cTable) where;
                DRSERIAL = this.w_NUMSER;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_NUMDOC = NVL(cp_ToDate(_read_.DRNUMDOC),cp_NullValue(_read_.DRNUMDOC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Aumenta di uno il numero del documento
        this.w_PUNPAD.w_DRNUMDOC=this.w_NUMDOC + 1
      case this.pTIPO="N"
        this.w_NUMDOC = this.w_PUNPAD.w_DRNUMDOC
        this.w_SERIALE = this.w_PUNPAD.w_DRSERIAL
        this.w_ADDRIL = this.w_PUNPAD.w_DRADDRIL
        * --- Select from RILEVAZI
        i_nConn=i_TableProp[this.RILEVAZI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.RILEVAZI_idx,2],.t.,this.RILEVAZI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select DRSERIAL  from "+i_cTable+" RILEVAZI ";
              +" where DRADDRIL = "+cp_ToStrODBC(this.w_ADDRIL)+" AND DRNUMDOC = "+cp_ToStrODBC(this.w_NUMDOC)+" AND DRSERIAL<>"+cp_ToStrODBC(this.w_SERIALE)+"";
               ,"_Curs_RILEVAZI")
        else
          select DRSERIAL from (i_cTable);
           where DRADDRIL = this.w_ADDRIL AND DRNUMDOC = this.w_NUMDOC AND DRSERIAL<>this.w_SERIALE;
            into cursor _Curs_RILEVAZI
        endif
        if used('_Curs_RILEVAZI')
          select _Curs_RILEVAZI
          locate for 1=1
          do while not(eof())
          this.w_NUMSER = NVL(_Curs_RILEVAZI.DRSERIAL,SPACE(15))
            select _Curs_RILEVAZI
            continue
          enddo
          use
        endif
        if NOT (EMPTY(this.w_NUMSER))
          this.w_MESS = "Impossibile confermare%0Il documento n. %1 associato all'utente %2 � gi� presente"
          ah_ErrorMsg(this.w_MESS,"!","", alltrim(str(this.w_NUMDOC,15)), alltrim(this.w_ADDRIL) )
          this.OparentObject.w_RESCHK=-1
          i_retcode = 'stop'
          return
        endif
        this.w_CODMAG = NVL(this.w_PUNPAD.w_DRCODMAG," ")
        if EMPTY(this.w_CODMAG)
          this.w_MESS = "Impossibile confermare. Magazzino non inserito"
          ah_ErrorMsg(this.w_MESS,"!")
          this.OparentObject.w_RESCHK=-1
          i_retcode = 'stop'
          return
        endif
        this.w_CODRIC = NVL(this.w_PUNPAD.w_DRCODRIC," ")
        if EMPTY(this.w_CODRIC)
          this.w_MESS = "Impossibile confermare. Articolo non inserito"
          ah_ErrorMsg(this.w_MESS,"!")
          this.OparentObject.w_RESCHK=-1
          i_retcode = 'stop'
          return
        endif
        this.w_CODUBI = NVL(this.w_PUNPAD.w_DRUBICAZ," ")
        this.w_MGUBIC = NVL(this.w_PUNPAD.w_MGUBIC," ")
        if EMPTY(this.w_CODUBI) AND g_PERUBI="S" AND this.w_MGUBIC="S"
          this.w_MESS = "Impossibile confermare. Ubicazione non inserita"
          ah_ErrorMsg(this.w_MESS,"!")
          this.OparentObject.w_RESCHK=-1
          i_retcode = 'stop'
          return
        endif
        this.w_CODLOT = NVL(this.w_PUNPAD.w_DR_LOTTO," ")
        this.w_FLLOTT = NVL(this.w_PUNPAD.w_FLLOTT," ")
        if EMPTY(this.w_CODLOT) AND g_PERLOT="S" AND this.w_FLLOTT="S"
          this.w_MESS = "Impossibile confermare. Lotto non inserito"
          ah_ErrorMsg(this.w_MESS,"!")
          this.OparentObject.w_RESCHK=-1
          i_retcode = 'stop'
          return
        endif
        * --- Gestione Matricole: Se l'articolo � gestito a Matricole � obbligatorio
        *     inserire la Matricola, che non pu� essere rilevata pi� di una volta
        this.w_DATRIL = this.w_PUNPAD.w_DATRIL
        if g_MATR="S" And this.w_DATRIL>=Nvl(g_DATMAT,cp_CharToDate("  /  /    "))
          this.w_CODMAT = NVL(this.w_PUNPAD.w_DRCODMAT," ")
          this.w_GESMAT = NVL(this.w_PUNPAD.w_GESMAT," ")
          if EMPTY(this.w_CODMAT) AND this.w_GESMAT="S"
            this.w_MESS = "Impossibile confermare. Matricola non inserita"
            ah_ErrorMsg(this.w_MESS,"!")
            this.OparentObject.w_RESCHK=-1
            i_retcode = 'stop'
            return
          endif
          * --- Verifica se la matricola con magazzino + lotto + ubicazione + articolo sia gi� stata rilevata in questa tornata di rilevazioni
          if !Empty(this.w_CODMAT) 
            this.w_KEYSAL = this.w_PUNPAD.w_DRKEYSAL
            this.w_CODRIL = this.w_PUNPAD.w_DRCODRIL
            this.w_KEYULO = this.w_PUNPAD.w_DRKEYULO
            this.w_APPSER = this.w_PUNPAD.w_DRSERIAL
            if this.w_PUNPAD.cFunction <> "Edit" and UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE"
              * --- Se non sono in Modifica rileggo il primo seriale libero
              i_Conn=i_TableProp[this.RILEVAZI_IDX, 3]
              cp_AskTableProg(this, i_Conn, "RILSER", "i_codazi,w_APPSER")
            endif
            * --- Select from RILEVAZI
            i_nConn=i_TableProp[this.RILEVAZI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.RILEVAZI_idx,2],.t.,this.RILEVAZI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select DRSERIAL  from "+i_cTable+" RILEVAZI ";
                  +" where DRCODRIL="+cp_ToStrODBC(this.w_CODRIL)+" And DRCODMAT = "+cp_ToStrODBC(this.w_CODMAT)+" And DRKEYSAL= "+cp_ToStrODBC(this.w_KEYSAL)+" And DRCODMAG= "+cp_ToStrODBC(this.w_CODMAG)+" And DRKEYULO="+cp_ToStrODBC(this.w_KEYULO)+" And DRSERIAL<>"+cp_ToStrODBC(this.w_APPSER)+"";
                   ,"_Curs_RILEVAZI")
            else
              select DRSERIAL from (i_cTable);
               where DRCODRIL=this.w_CODRIL And DRCODMAT = this.w_CODMAT And DRKEYSAL= this.w_KEYSAL And DRCODMAG= this.w_CODMAG And DRKEYULO=this.w_KEYULO And DRSERIAL<>this.w_APPSER;
                into cursor _Curs_RILEVAZI
            endif
            if used('_Curs_RILEVAZI')
              select _Curs_RILEVAZI
              locate for 1=1
              do while not(eof())
              this.w_NUMSER = NVL(_Curs_RILEVAZI.DRSERIAL,SPACE(15))
                select _Curs_RILEVAZI
                continue
              enddo
              use
            endif
            if NOT (EMPTY(this.w_NUMSER))
              this.w_MESS = "Impossibile confermare%0La matricola %1 � gi� stata rilevata"
              ah_ErrorMsg(this.w_MESS,"!","", alltrim(this.w_CODMAT) )
              this.OparentObject.w_RESCHK=-1
              i_retcode = 'stop'
              return
            else
              if UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE"
                * --- Verifico l'esistenza di una Rilevazione senza Mov.Magazzino associato con q.t� uguale a 1
                if this.oParentObject.w_DRQTAESI = 1
                  * --- Select from RILEVAZI
                  i_nConn=i_TableProp[this.RILEVAZI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.RILEVAZI_idx,2],.t.,this.RILEVAZI_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select DRSERIAL,DRSERMOV,DRCODMAG,DRCODRIL  from "+i_cTable+" RILEVAZI ";
                        +" where DRCODMAT = "+cp_ToStrODBC(this.w_CODMAT)+" And DRKEYSAL= "+cp_ToStrODBC(this.w_KEYSAL)+" And DRSERIAL<>"+cp_ToStrODBC(this.w_APPSER)+"";
                         ,"_Curs_RILEVAZI")
                  else
                    select DRSERIAL,DRSERMOV,DRCODMAG,DRCODRIL from (i_cTable);
                     where DRCODMAT = this.w_CODMAT And DRKEYSAL= this.w_KEYSAL And DRSERIAL<>this.w_APPSER;
                      into cursor _Curs_RILEVAZI
                  endif
                  if used('_Curs_RILEVAZI')
                    select _Curs_RILEVAZI
                    locate for 1=1
                    do while not(eof())
                    if Empty(nvl(_Curs_RILEVAZI.DRSERMOV,""))
                      this.w_MESS = "Impossibile confermare%0La matricola %1 � gi� stata rilevata%0Codice rilevazione %2 magazzino %3"
                      AH_ERRORMSG(this.w_MESS,48,"",alltrim(this.w_CODMAT),alltrim(_Curs_RILEVAZI.DRCODRIL),alltrim(_Curs_RILEVAZI.DRCODMAG))
                      this.OparentObject.w_RESCHK=-1
                      i_retcode = 'stop'
                      return
                    endif
                      select _Curs_RILEVAZI
                      continue
                    enddo
                    use
                  endif
                endif
              endif
            endif
            if UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE"
              * --- Controllo Congruenza Movimento Carico Scarico Matricola
              VQ_EXEC("QUERY\GSMA_MATR.VQR",this,"MATR")
              do case
                case this.oParentObject.w_DRQTAESI = 1 And RECCOUNT("MATR") > 0 
                  * --- Se Rettifico quantit� 1 - Carico - Matricola � presente su un altro Magazzino
                  this.w_MATRMAG = MATR.CODMAG
                  this.w_MATRUBI = NVL(MATR.CODUBI,SPACE(20))
                  this.w_MATRLOT = NVL(MATR.CODLOT,SPACE(20))
                  this.w_MATRLAR = NVL(MATR.LOTART, SPACE(20))
                  this.w_MATRULO = iif(empty(this.w_MATRUBI),repl("#",20),this.w_MATRUBI)+iif(empty(this.w_MATRLOT),repl("#",20),this.w_MATRLOT)
                  if EMPTY(this.w_MATRUBI)
                    this.w_MESS = "La matricola %1 � gi� presente sul magazzino %2%0Si vuole procedere ugualmente?"
                  else
                    this.w_MESS = "La matricola %1 � gi� presente sul magazzino %2 ubicazione %3%0Si vuole procedere ugualmente?"
                  endif
                  if AH_YESNO( this.w_MESS,"",alltrim(this.w_CODMAT),alltrim(this.w_MATRMAG), alltrim(this.w_MATRUBI))
                    * --- Creo un Movimento di Rettifica di Scarico dal Magazzino su cui � presente la Matricola
                    ah_msg("Generazione rettifica di scarico su %1",.t.,.f.,.f.,alltrim(this.w_MATRMAG))
                    this.w_CODUTE = IIF(g_MAGUTE="S", 0, i_CODUTE)
                    this.w_CODVAR = this.w_PUNPAD.w_DRCODVAR
                    this.w_CODART = this.w_PUNPAD.w_DRCODART
                    this.w_UNIMIS = this.w_PUNPAD.w_DRUNIMIS
                    * --- Prima di generare il movimento di rettifica di scarico devo verificare se � gi� 
                    *     presente una rilevazione con gli stessi parametri (NB adesso il magazzino di riferimento � MATR.CODMAG)
                    this.w_NUMSER = ""
                    * --- Select from RILEVAZI
                    i_nConn=i_TableProp[this.RILEVAZI_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.RILEVAZI_idx,2],.t.,this.RILEVAZI_idx)
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select DRSERIAL  from "+i_cTable+" RILEVAZI ";
                          +" where DRCODRIL="+cp_ToStrODBC(this.w_CODRIL)+" And DRCODMAT = "+cp_ToStrODBC(this.w_CODMAT)+" And DRKEYSAL= "+cp_ToStrODBC(this.w_KEYSAL)+" And DRCODMAG= "+cp_ToStrODBC(this.w_MATRMAG)+" And DRKEYULO="+cp_ToStrODBC(this.w_MATRULO)+" And DRSERIAL<>"+cp_ToStrODBC(this.w_APPSER)+"";
                           ,"_Curs_RILEVAZI")
                    else
                      select DRSERIAL from (i_cTable);
                       where DRCODRIL=this.w_CODRIL And DRCODMAT = this.w_CODMAT And DRKEYSAL= this.w_KEYSAL And DRCODMAG= this.w_MATRMAG And DRKEYULO=this.w_MATRULO And DRSERIAL<>this.w_APPSER;
                        into cursor _Curs_RILEVAZI
                    endif
                    if used('_Curs_RILEVAZI')
                      select _Curs_RILEVAZI
                      locate for 1=1
                      do while not(eof())
                      this.w_NUMSER = NVL(_Curs_RILEVAZI.DRSERIAL,SPACE(15))
                        select _Curs_RILEVAZI
                        continue
                      enddo
                      use
                    endif
                    if !Empty (this.w_NUMSER)
                      this.w_MESS = "Impossibile generare rettifica di scarico%0� gi� presente una rilevazione della matricola %1 con lo stesso codice rilevazione %2 e magazzino %3%0Occorre impostare un nuovo codice rilevazione"
                      AH_ERRORMSG(this.w_MESS,48,"",alltrim(this.w_CODMAT),alltrim(this.w_CODRIL),alltrim(this.w_MATRMAG))
                      this.OparentObject.w_RESCHK=-1
                      i_retcode = 'stop'
                      return
                    endif
                    * --- Try
                    local bErr_039B7230
                    bErr_039B7230=bTrsErr
                    this.Try_039B7230()
                    * --- Catch
                    if !empty(i_Error)
                      i_ErrMsg=i_Error
                      i_Error=''
                      * --- rollback
                      bTrsErr=.t.
                      cp_EndTrs(.t.)
                      ah_ErrorMsg("Impossibile generare dati rilevati: rettifica su %1%0%2" , 48, "",alltrim(MATR.CODMAG), message())
                      this.OparentObject.w_RESCHK=-1
                      i_retcode = 'stop'
                      return
                    endif
                    bTrsErr=bTrsErr or bErr_039B7230
                    * --- End
                  else
                    this.OparentObject.w_RESCHK=-1
                    i_retcode = 'stop'
                    return
                  endif
                case this.oParentObject.w_DRQTAESI = 0 And RECCOUNT("MATR") = 0 
                  * --- Se Rettifico quantit� 0 - Scarico -Matricola  non � presente sul Magazzino selezionato
                  AH_ERRORMSG("Impossibile confermare:%0La matricola %1 non � presente sul magazzino %2",48,"",alltrim(this.w_CODMAT),alltrim(this.w_CODMAG))
                  this.OparentObject.w_RESCHK=-1
                  i_retcode = 'stop'
                  return
              endcase
              if USED("MATR") 
 
                Select MATR 
 USE
              endif
            endif
            * --- Controllo obsolescenza nel caso sia stato imputato prima la matricola
            this.w_DTOBS1 = this.w_PUNPAD.w_DTOBS1
            * --- Read from KEY_ARTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CADTOBSO"+;
                " from "+i_cTable+" KEY_ARTI where ";
                    +"CACODICE = "+cp_ToStrODBC(this.w_CODRIC);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CADTOBSO;
                from (i_cTable) where;
                    CACODICE = this.w_CODRIC;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_DTOBS1 = NVL(cp_ToDate(_read_.CADTOBSO),cp_NullValue(_read_.CADTOBSO))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Visualizzo un messaggio se articolo obsoleto
            if Not ( this.w_DTOBS1>this.w_DATRIL OR EMPTY(this.w_DTOBS1) )
              ah_ErrorMsg("Articolo obsoleto")
            endif
          endif
        endif
      case this.pTIPO="M"
        * --- Movimenti Magazzino
        if UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE"
          * --- Movimenti Magazzino
          this.w_OBJMOV = GSMA_MVM()
          * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
          if !(this.w_OBJMOV.bSec1)
            i_retcode = 'stop'
            return
          endif
          this.w_OBJMOV.w_MMSERIAL = this.w_PUNPAD.w_DRSERMOV
          this.w_OBJMOV.QueryKeySet("MMSERIAL='"+this.w_PUNPAD.w_DRSERMOV+ "'","")     
          * --- carico il record
          this.w_OBJMOV.LoadRecWarn()     
        else
          gsar_bzm(this,this.w_PUNPAD.w_DRSERMOV,-10)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.pTIPO="R"
        * --- Alla chiusura della Maschera dettaglio rieseguo la ricerca nel Confronto Dati
        this.oParentObject.oParentObject.NotifyEvent("Ricerca")
      case this.pTIPO="I"
        this.w_ZOOM = this.oParentObject.w_ZoomConf
        if Upper(This.oParentObject.Class )="TGSMA_KIR"
          this.w_ZOOM.grd.Columns[ 7 ].Format = "KZR"
          this.w_ZOOM.grd.Columns[ 7 ].Enabled = .T.
          this.w_ZOOM.grd.Columns[ 7 ].DynamicForeColor = "RGB(0,0,255)"
          this.w_NOME = "MTCODMAT"
        else
          this.w_NOME = "DRCODMAT"
        endif
        * --- Se matricole non attive nascondo la colonna Matricole
        if g_MATR<>"S"
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if g_APPLICATION = "ADHOC REVOLUTION"
          * --- Nascondo campo variante
          this.w_NOME = "DRCODVAR"
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.pTIPO="O"
        vx_exec("QUERY\GSMA_ZMA.VZM",this)
        if Not Empty(this.w_CODMAT)
          do case
            case UPPER(This.OparentObject.Class)="TGSMA_ADR" 
              this.w_PUNPAD.w_DRCODMAT= this.w_CODMAT
              this.w_PUNPAD.w_DRCODART= this.w_CODART
              this.w_PUNPAD.w_DRCODVAR= this.w_CODVAR
              this.w_PUNPAD.w_DRKEYSAL=this.w_KEYSAL
              this.w_PUNPAD.w_GESMAT="S"
            case UPPER(This.OparentObject.Class)="TGSMA_ARG"
              this.w_PUNPAD.w_TCMATRIC = this.w_CODMAT
              this.w_PUNPAD.w_DRCODART= this.w_CODART
              this.w_PUNPAD.w_DRCODVAR= this.w_CODVAR
              this.w_PUNPAD.w_DRKEYSAL=this.w_KEYSAL
              this.w_PUNPAD.w_GESMAT="S"
            otherwise
              this.w_PUNPAD.w_CODMAT= this.w_CODMAT
              this.w_PUNPAD.w_CODART= this.w_CODART
              this.w_PUNPAD.w_CODVAR= this.w_CODVAR
              this.w_PUNPAD.w_DRKEYSAL=this.w_KEYSAL
              this.w_PUNPAD.w_GESMAT="S"
          endcase
        endif
      case this.pTIPO="S"
        if Upper(This.oParentObject.Class )="TGSMA_ADR"
          this.w_APPCODART = this.oParentObject.w_DRCODART
        else
          this.w_APPCODART = this.w_PUNPAD.w_CODART
        endif
        if NOT EMPTY(this.w_APPCODART)
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARTIPART"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.w_APPCODART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARTIPART;
              from (i_cTable) where;
                  ARCODART = this.w_APPCODART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_ARTVALIDO = NVL(cp_ToDate(_read_.ARTIPART),cp_NullValue(_read_.ARTIPART))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_ARTVALIDO $ "DC-PM-FA-PS-RI"
            ah_ErrorMsg("L'articolo inserito non � valido","!")
            this.oParentObject.w_DRCODRIC = ""
            this.oParentObject.w_DESART = ""
            this.oParentObject.w_DRCODART = ""
            i_retcode = 'stop'
            return
          else
            this.w_OBTEST = this.w_PUNPAD.w_OBTEST
            this.w_DTOBS1 = this.w_PUNPAD.w_DTOBS1
            * --- Visualizzo un messaggio se articolo obsoleto
            if Not ( this.w_DTOBS1>this.w_OBTEST OR EMPTY(this.w_DTOBS1) )
              ah_ErrorMsg("Articolo obsoleto")
            endif
          endif
        endif
      case this.pTIPO="Q"
        this.w_APPCAL = this.oParentObject.w_UNMIS1+this.oParentObject.w_UNMIS2+this.oParentObject.w_UNMIS3+this.oParentObject.w_DRUNIMIS+this.oParentObject.w_OPERAT+this.oParentObject.w_OPERAT3+" "+"Q"
        * --- Forza U.M. Secondarie o Controllo UM non frazionabile
        if this.oParentObject.w_DRQTAESI <> 0 And this.oParentObject.w_DRUNIMIS <> this.oParentObject.w_UNMIS1
          * --- Lettura Flag non Frazionabile e Forza UM secondaria (mi serve anche per la CALMMLIS)
          this.w_QTAUM1 = CALQTAADV(this.oParentObject.w_DRQTAESI,this.oParentObject.w_DRUNIMIS,this.oParentObject.w_UNMIS2,this.oParentObject.w_OPERAT, this.oParentObject.w_MOLTIP, this.oParentObject.w_FLUSEP, this.oParentObject.w_NOFRAZ1, this.oParentObject.w_MODUM2, , this.oParentObject.w_UNMIS3, this.oParentObject.w_OPERAT3, this.oParentObject.w_MOLTI3,,,This.oparentobject, "DRQTAESI", .T. )
          this.oParentObject.w_DRQTAES1 = cp_Round(this.w_QTAUM1,g_PERPQT)
          * --- Lettura Flag Forza U.M. Secondarie
        else
          this.w_QTAUM1 = CALQTA(this.oParentObject.w_DRQTAESI, this.oParentObject.w_DRUNIMIS ,this.oParentObject.w_UNMIS2,this.oParentObject.w_OPERAT, this.oParentObject.w_MOLTIP, this.oParentObject.w_FLUSEP, this.oParentObject.w_NOFRAZ1, this.oParentObject.w_MODUM2, "", this.oParentObject.w_UNMIS3, this.oParentObject.w_OPERAT3, this.oParentObject.w_MOLTI3)
        endif
      case this.pTIPO="V"
        * --- Elimino Movimento di Scarico Matricola collegato
        this.w_APPSER = this.w_PUNPAD.w_DRSERIAL
        * --- Delete from RILEVAZI
        i_nConn=i_TableProp[this.RILEVAZI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.RILEVAZI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"DRORDICH = "+cp_ToStrODBC(this.w_APPSER);
                 )
        else
          delete from (i_cTable) where;
                DRORDICH = this.w_APPSER;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
    endcase
  endproc
  proc Try_039B7230()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Genero nuovo Movimento Dati Rilevati di Scarico  (q.t� a Zero) 
    this.w_DRSERIAL = cp_GetProg("RILEVAZI","RILSER",this.w_DRSERIAL,i_CODAZI)
    this.w_MATRUBI = IIF(EMPTY(this.w_MATRUBI), NULL, this.w_MATRUBI)
    this.w_MATRLOT = IIF(EMPTY(this.w_MATRLOT), NULL, this.w_MATRLOT)
    * --- Rileggo il seriale libero per valorizzare DRORIDICH
    this.w_APPSER = SPACE(15)
    i_Conn=i_TableProp[this.RILEVAZI_IDX, 3]
    cp_AskTableProg(this, i_Conn, "RILSER", "i_codazi,w_APPSER")
    * --- Insert into RILEVAZI
    i_nConn=i_TableProp[this.RILEVAZI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RILEVAZI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.RILEVAZI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"DRSERIAL"+",DRCODRIL"+",DRCODMAG"+",DRCODUTE"+",DRADDRIL"+",DRCODRIC"+",DRCODART"+",DRCODVAR"+",DRKEYSAL"+",DRUBICAZ"+",DR_LOTTO"+",DRKEYULO"+",DRUNIMIS"+",DRQTAESI"+",DRQTAES1"+",DRCONFER"+",UTCC"+",UTDC"+",UTCV"+",UTDV"+",DRCODMAT"+",DRLOTART"+",DRNUMDOC"+",DRORDICH"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_DRSERIAL),'RILEVAZI','DRSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODRIL),'RILEVAZI','DRCODRIL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MATRMAG),'RILEVAZI','DRCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODUTE),'RILEVAZI','DRCODUTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ADDRIL),'RILEVAZI','DRADDRIL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODRIC),'RILEVAZI','DRCODRIC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODART),'RILEVAZI','DRCODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODVAR),'RILEVAZI','DRCODVAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_KEYSAL),'RILEVAZI','DRKEYSAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MATRUBI),'RILEVAZI','DRUBICAZ');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MATRLOT),'RILEVAZI','DR_LOTTO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MATRULO),'RILEVAZI','DRKEYULO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UNIMIS),'RILEVAZI','DRUNIMIS');
      +","+cp_NullLink(cp_ToStrODBC(0),'RILEVAZI','DRQTAESI');
      +","+cp_NullLink(cp_ToStrODBC(0),'RILEVAZI','DRQTAES1');
      +","+cp_NullLink(cp_ToStrODBC(" "),'RILEVAZI','DRCONFER');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'RILEVAZI','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'RILEVAZI','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(0),'RILEVAZI','UTCV');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'RILEVAZI','UTDV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODMAT),'RILEVAZI','DRCODMAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MATRLAR),'RILEVAZI','DRLOTART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMDOC +1),'RILEVAZI','DRNUMDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_APPSER),'RILEVAZI','DRORDICH');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'DRSERIAL',this.w_DRSERIAL,'DRCODRIL',this.w_CODRIL,'DRCODMAG',this.w_MATRMAG,'DRCODUTE',this.w_CODUTE,'DRADDRIL',this.w_ADDRIL,'DRCODRIC',this.w_CODRIC,'DRCODART',this.w_CODART,'DRCODVAR',this.w_CODVAR,'DRKEYSAL',this.w_KEYSAL,'DRUBICAZ',this.w_MATRUBI,'DR_LOTTO',this.w_MATRLOT,'DRKEYULO',this.w_MATRULO)
      insert into (i_cTable) (DRSERIAL,DRCODRIL,DRCODMAG,DRCODUTE,DRADDRIL,DRCODRIC,DRCODART,DRCODVAR,DRKEYSAL,DRUBICAZ,DR_LOTTO,DRKEYULO,DRUNIMIS,DRQTAESI,DRQTAES1,DRCONFER,UTCC,UTDC,UTCV,UTDV,DRCODMAT,DRLOTART,DRNUMDOC,DRORDICH &i_ccchkf. );
         values (;
           this.w_DRSERIAL;
           ,this.w_CODRIL;
           ,this.w_MATRMAG;
           ,this.w_CODUTE;
           ,this.w_ADDRIL;
           ,this.w_CODRIC;
           ,this.w_CODART;
           ,this.w_CODVAR;
           ,this.w_KEYSAL;
           ,this.w_MATRUBI;
           ,this.w_MATRLOT;
           ,this.w_MATRULO;
           ,this.w_UNIMIS;
           ,0;
           ,0;
           ," ";
           ,i_CODUTE;
           ,SetInfoDate( g_CALUTD );
           ,0;
           ,cp_CharToDate("  -  -    ");
           ,this.w_CODMAT;
           ,this.w_MATRLAR;
           ,this.w_NUMDOC +1;
           ,this.w_APPSER;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    ah_ErrorMsg("Archivio dati rilevati: generata rettifica di scarico su %1", 64, "",alltrim(MATR.CODMAG))
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Nome del campo da disabilitare...
    this.w_LOOP = 1
    do while this.w_LOOP<= this.w_ZOOM.grd.ColumnCount
      if this.w_ZOOM.grd.Columns[ This.w_LOOP ].ControlSource= this.w_NOME
        this.w_ZOOM.grd.Columns[ This.w_LOOP ].Visible = .F.
        * --- Nel caso della variante nasconde la caption del titolo ma la colonna rimane.
        this.w_ZOOM.grd.Columns[ This.w_LOOP ].Width = 0
        this.w_LOOP = this.w_ZOOM.grd.ColumnCount+1
      else
        this.w_LOOP = this.w_LOOP + 1
      endif
    enddo
    this.w_ZOOM.Refresh()     
  endproc


  proc Init(oParentObject,pTIPO)
    this.pTIPO=pTIPO
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,8)]
    this.cWorkTables[1]='ADDERILE'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='CODIRILE'
    this.cWorkTables[4]='MAGAZZIN'
    this.cWorkTables[5]='RILEVAZI'
    this.cWorkTables[6]='UBICAZIO'
    this.cWorkTables[7]='KEY_ARTI'
    this.cWorkTables[8]='UNIMIS'
    return(this.OpenAllTables(8))

  proc CloseCursors()
    if used('_Curs_RILEVAZI')
      use in _Curs_RILEVAZI
    endif
    if used('_Curs_RILEVAZI')
      use in _Curs_RILEVAZI
    endif
    if used('_Curs_RILEVAZI')
      use in _Curs_RILEVAZI
    endif
    if used('_Curs_RILEVAZI')
      use in _Curs_RILEVAZI
    endif
    if used('_Curs_RILEVAZI')
      use in _Curs_RILEVAZI
    endif
    if used('_Curs_RILEVAZI')
      use in _Curs_RILEVAZI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTIPO"
endproc
