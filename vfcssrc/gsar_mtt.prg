* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_mtt                                                        *
*              Titolari                                                        *
*                                                                              *
*      Author: ZUCCHETTI TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_21]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-10                                                      *
* Last revis.: 2011-02-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsar_mtt")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsar_mtt")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsar_mtt")
  return

* --- Class definition
define class tgsar_mtt as StdPCForm
  Width  = 615
  Height = 310
  Top    = 12
  Left   = 60
  cComment = "Titolari"
  cPrg = "gsar_mtt"
  HelpContextID=80312169
  add object cnt as tcgsar_mtt
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsar_mtt as PCContext
  w_TTCODAZI = space(5)
  w_TTCOGTIT = space(25)
  w_TTNOMTIT = space(25)
  w_TTTELEFO = space(18)
  w_TTLUONAS = space(30)
  w_TTPRONAS = space(2)
  w_TTDATNAS = space(8)
  w_TTINDIRI = space(30)
  w_TTCAPTIT = space(8)
  w_TTLOCTIT = space(30)
  w_TT_SESSO = space(1)
  w_TTPROTIT = space(2)
  proc Save(i_oFrom)
    this.w_TTCODAZI = i_oFrom.w_TTCODAZI
    this.w_TTCOGTIT = i_oFrom.w_TTCOGTIT
    this.w_TTNOMTIT = i_oFrom.w_TTNOMTIT
    this.w_TTTELEFO = i_oFrom.w_TTTELEFO
    this.w_TTLUONAS = i_oFrom.w_TTLUONAS
    this.w_TTPRONAS = i_oFrom.w_TTPRONAS
    this.w_TTDATNAS = i_oFrom.w_TTDATNAS
    this.w_TTINDIRI = i_oFrom.w_TTINDIRI
    this.w_TTCAPTIT = i_oFrom.w_TTCAPTIT
    this.w_TTLOCTIT = i_oFrom.w_TTLOCTIT
    this.w_TT_SESSO = i_oFrom.w_TT_SESSO
    this.w_TTPROTIT = i_oFrom.w_TTPROTIT
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_TTCODAZI = this.w_TTCODAZI
    i_oTo.w_TTCOGTIT = this.w_TTCOGTIT
    i_oTo.w_TTNOMTIT = this.w_TTNOMTIT
    i_oTo.w_TTTELEFO = this.w_TTTELEFO
    i_oTo.w_TTLUONAS = this.w_TTLUONAS
    i_oTo.w_TTPRONAS = this.w_TTPRONAS
    i_oTo.w_TTDATNAS = this.w_TTDATNAS
    i_oTo.w_TTINDIRI = this.w_TTINDIRI
    i_oTo.w_TTCAPTIT = this.w_TTCAPTIT
    i_oTo.w_TTLOCTIT = this.w_TTLOCTIT
    i_oTo.w_TT_SESSO = this.w_TT_SESSO
    i_oTo.w_TTPROTIT = this.w_TTPROTIT
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsar_mtt as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 615
  Height = 310
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-02-25"
  HelpContextID=80312169
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=12

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  TITOLARI_IDX = 0
  cFile = "TITOLARI"
  cKeySelect = "TTCODAZI"
  cKeyWhere  = "TTCODAZI=this.w_TTCODAZI"
  cKeyDetail  = "TTCODAZI=this.w_TTCODAZI"
  cKeyWhereODBC = '"TTCODAZI="+cp_ToStrODBC(this.w_TTCODAZI)';

  cKeyDetailWhereODBC = '"TTCODAZI="+cp_ToStrODBC(this.w_TTCODAZI)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"TITOLARI.TTCODAZI="+cp_ToStrODBC(this.w_TTCODAZI)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'TITOLARI.CPROWNUM '
  cPrg = "gsar_mtt"
  cComment = "Titolari"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 9
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TTCODAZI = space(5)
  w_TTCOGTIT = space(25)
  w_TTNOMTIT = space(25)
  w_TTTELEFO = space(18)
  w_TTLUONAS = space(30)
  w_TTPRONAS = space(2)
  w_TTDATNAS = ctod('  /  /  ')
  w_TTINDIRI = space(30)
  w_TTCAPTIT = space(8)
  w_TTLOCTIT = space(30)
  w_TT_SESSO = space(1)
  w_TTPROTIT = space(2)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_mttPag1","gsar_mtt",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='TITOLARI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.TITOLARI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.TITOLARI_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsar_mtt'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from TITOLARI where TTCODAZI=KeySet.TTCODAZI
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.TITOLARI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2],this.bLoadRecFilter,this.TITOLARI_IDX,"gsar_mtt")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('TITOLARI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "TITOLARI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' TITOLARI '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'TTCODAZI',this.w_TTCODAZI  )
      select * from (i_cTable) TITOLARI where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TTCODAZI = NVL(TTCODAZI,space(5))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'TITOLARI')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_TTCOGTIT = NVL(TTCOGTIT,space(25))
          .w_TTNOMTIT = NVL(TTNOMTIT,space(25))
          .w_TTTELEFO = NVL(TTTELEFO,space(18))
          .w_TTLUONAS = NVL(TTLUONAS,space(30))
          .w_TTPRONAS = NVL(TTPRONAS,space(2))
          .w_TTDATNAS = NVL(cp_ToDate(TTDATNAS),ctod("  /  /  "))
          .w_TTINDIRI = NVL(TTINDIRI,space(30))
          .w_TTCAPTIT = NVL(TTCAPTIT,space(8))
          .w_TTLOCTIT = NVL(TTLOCTIT,space(30))
          .w_TT_SESSO = NVL(TT_SESSO,space(1))
          .w_TTPROTIT = NVL(TTPROTIT,space(2))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_TTCODAZI=space(5)
      .w_TTCOGTIT=space(25)
      .w_TTNOMTIT=space(25)
      .w_TTTELEFO=space(18)
      .w_TTLUONAS=space(30)
      .w_TTPRONAS=space(2)
      .w_TTDATNAS=ctod("  /  /  ")
      .w_TTINDIRI=space(30)
      .w_TTCAPTIT=space(8)
      .w_TTLOCTIT=space(30)
      .w_TT_SESSO=space(1)
      .w_TTPROTIT=space(2)
      if .cFunction<>"Filter"
        .DoRTCalc(1,10,.f.)
        .w_TT_SESSO = 'M'
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'TITOLARI')
    this.DoRTCalc(12,12,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oTTLUONAS_2_4.enabled = i_bVal
      .Page1.oPag.oTTPRONAS_2_5.enabled = i_bVal
      .Page1.oPag.oTTDATNAS_2_6.enabled = i_bVal
      .Page1.oPag.oTTINDIRI_2_7.enabled = i_bVal
      .Page1.oPag.oTTCAPTIT_2_8.enabled = i_bVal
      .Page1.oPag.oTTLOCTIT_2_9.enabled = i_bVal
      .Page1.oPag.oTT_SESSO_2_10.enabled_(i_bVal)
      .Page1.oPag.oTTPROTIT_2_11.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'TITOLARI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.TITOLARI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TTCODAZI,"TTCODAZI",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_TTCOGTIT C(25);
      ,t_TTNOMTIT C(25);
      ,t_TTTELEFO C(18);
      ,t_TTLUONAS C(30);
      ,t_TTPRONAS C(2);
      ,t_TTDATNAS D(8);
      ,t_TTINDIRI C(30);
      ,t_TTCAPTIT C(8);
      ,t_TTLOCTIT C(30);
      ,t_TT_SESSO N(3);
      ,t_TTPROTIT C(2);
      ,CPROWNUM N(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsar_mttbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTTCOGTIT_2_1.controlsource=this.cTrsName+'.t_TTCOGTIT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTTNOMTIT_2_2.controlsource=this.cTrsName+'.t_TTNOMTIT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTTTELEFO_2_3.controlsource=this.cTrsName+'.t_TTTELEFO'
    this.oPgFRm.Page1.oPag.oTTLUONAS_2_4.controlsource=this.cTrsName+'.t_TTLUONAS'
    this.oPgFRm.Page1.oPag.oTTPRONAS_2_5.controlsource=this.cTrsName+'.t_TTPRONAS'
    this.oPgFRm.Page1.oPag.oTTDATNAS_2_6.controlsource=this.cTrsName+'.t_TTDATNAS'
    this.oPgFRm.Page1.oPag.oTTINDIRI_2_7.controlsource=this.cTrsName+'.t_TTINDIRI'
    this.oPgFRm.Page1.oPag.oTTCAPTIT_2_8.controlsource=this.cTrsName+'.t_TTCAPTIT'
    this.oPgFRm.Page1.oPag.oTTLOCTIT_2_9.controlsource=this.cTrsName+'.t_TTLOCTIT'
    this.oPgFRm.Page1.oPag.oTT_SESSO_2_10.controlsource=this.cTrsName+'.t_TT_SESSO'
    this.oPgFRm.Page1.oPag.oTTPROTIT_2_11.controlsource=this.cTrsName+'.t_TTPROTIT'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(218)
    this.AddVLine(433)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTTCOGTIT_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.TITOLARI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.TITOLARI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2])
      *
      * insert into TITOLARI
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'TITOLARI')
        i_extval=cp_InsertValODBCExtFlds(this,'TITOLARI')
        i_cFldBody=" "+;
                  "(TTCODAZI,TTCOGTIT,TTNOMTIT,TTTELEFO,TTLUONAS"+;
                  ",TTPRONAS,TTDATNAS,TTINDIRI,TTCAPTIT,TTLOCTIT"+;
                  ",TT_SESSO,TTPROTIT,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_TTCODAZI)+","+cp_ToStrODBC(this.w_TTCOGTIT)+","+cp_ToStrODBC(this.w_TTNOMTIT)+","+cp_ToStrODBC(this.w_TTTELEFO)+","+cp_ToStrODBC(this.w_TTLUONAS)+;
             ","+cp_ToStrODBC(this.w_TTPRONAS)+","+cp_ToStrODBC(this.w_TTDATNAS)+","+cp_ToStrODBC(this.w_TTINDIRI)+","+cp_ToStrODBC(this.w_TTCAPTIT)+","+cp_ToStrODBC(this.w_TTLOCTIT)+;
             ","+cp_ToStrODBC(this.w_TT_SESSO)+","+cp_ToStrODBC(this.w_TTPROTIT)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'TITOLARI')
        i_extval=cp_InsertValVFPExtFlds(this,'TITOLARI')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'TTCODAZI',this.w_TTCODAZI)
        INSERT INTO (i_cTable) (;
                   TTCODAZI;
                  ,TTCOGTIT;
                  ,TTNOMTIT;
                  ,TTTELEFO;
                  ,TTLUONAS;
                  ,TTPRONAS;
                  ,TTDATNAS;
                  ,TTINDIRI;
                  ,TTCAPTIT;
                  ,TTLOCTIT;
                  ,TT_SESSO;
                  ,TTPROTIT;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_TTCODAZI;
                  ,this.w_TTCOGTIT;
                  ,this.w_TTNOMTIT;
                  ,this.w_TTTELEFO;
                  ,this.w_TTLUONAS;
                  ,this.w_TTPRONAS;
                  ,this.w_TTDATNAS;
                  ,this.w_TTINDIRI;
                  ,this.w_TTCAPTIT;
                  ,this.w_TTLOCTIT;
                  ,this.w_TT_SESSO;
                  ,this.w_TTPROTIT;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.TITOLARI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (t_TTCOGTIT<>space(25)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'TITOLARI')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'TITOLARI')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_TTCOGTIT<>space(25)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update TITOLARI
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'TITOLARI')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " TTCOGTIT="+cp_ToStrODBC(this.w_TTCOGTIT)+;
                     ",TTNOMTIT="+cp_ToStrODBC(this.w_TTNOMTIT)+;
                     ",TTTELEFO="+cp_ToStrODBC(this.w_TTTELEFO)+;
                     ",TTLUONAS="+cp_ToStrODBC(this.w_TTLUONAS)+;
                     ",TTPRONAS="+cp_ToStrODBC(this.w_TTPRONAS)+;
                     ",TTDATNAS="+cp_ToStrODBC(this.w_TTDATNAS)+;
                     ",TTINDIRI="+cp_ToStrODBC(this.w_TTINDIRI)+;
                     ",TTCAPTIT="+cp_ToStrODBC(this.w_TTCAPTIT)+;
                     ",TTLOCTIT="+cp_ToStrODBC(this.w_TTLOCTIT)+;
                     ",TT_SESSO="+cp_ToStrODBC(this.w_TT_SESSO)+;
                     ",TTPROTIT="+cp_ToStrODBC(this.w_TTPROTIT)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'TITOLARI')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      TTCOGTIT=this.w_TTCOGTIT;
                     ,TTNOMTIT=this.w_TTNOMTIT;
                     ,TTTELEFO=this.w_TTTELEFO;
                     ,TTLUONAS=this.w_TTLUONAS;
                     ,TTPRONAS=this.w_TTPRONAS;
                     ,TTDATNAS=this.w_TTDATNAS;
                     ,TTINDIRI=this.w_TTINDIRI;
                     ,TTCAPTIT=this.w_TTCAPTIT;
                     ,TTLOCTIT=this.w_TTLOCTIT;
                     ,TT_SESSO=this.w_TT_SESSO;
                     ,TTPROTIT=this.w_TTPROTIT;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.TITOLARI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_TTCOGTIT<>space(25)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete TITOLARI
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_TTCOGTIT<>space(25)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.TITOLARI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,12,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oTTLUONAS_2_4.value==this.w_TTLUONAS)
      this.oPgFrm.Page1.oPag.oTTLUONAS_2_4.value=this.w_TTLUONAS
      replace t_TTLUONAS with this.oPgFrm.Page1.oPag.oTTLUONAS_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTTPRONAS_2_5.value==this.w_TTPRONAS)
      this.oPgFrm.Page1.oPag.oTTPRONAS_2_5.value=this.w_TTPRONAS
      replace t_TTPRONAS with this.oPgFrm.Page1.oPag.oTTPRONAS_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTTDATNAS_2_6.value==this.w_TTDATNAS)
      this.oPgFrm.Page1.oPag.oTTDATNAS_2_6.value=this.w_TTDATNAS
      replace t_TTDATNAS with this.oPgFrm.Page1.oPag.oTTDATNAS_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTTINDIRI_2_7.value==this.w_TTINDIRI)
      this.oPgFrm.Page1.oPag.oTTINDIRI_2_7.value=this.w_TTINDIRI
      replace t_TTINDIRI with this.oPgFrm.Page1.oPag.oTTINDIRI_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTTCAPTIT_2_8.value==this.w_TTCAPTIT)
      this.oPgFrm.Page1.oPag.oTTCAPTIT_2_8.value=this.w_TTCAPTIT
      replace t_TTCAPTIT with this.oPgFrm.Page1.oPag.oTTCAPTIT_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTTLOCTIT_2_9.value==this.w_TTLOCTIT)
      this.oPgFrm.Page1.oPag.oTTLOCTIT_2_9.value=this.w_TTLOCTIT
      replace t_TTLOCTIT with this.oPgFrm.Page1.oPag.oTTLOCTIT_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTT_SESSO_2_10.RadioValue()==this.w_TT_SESSO)
      this.oPgFrm.Page1.oPag.oTT_SESSO_2_10.SetRadio()
      replace t_TT_SESSO with this.oPgFrm.Page1.oPag.oTT_SESSO_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTTPROTIT_2_11.value==this.w_TTPROTIT)
      this.oPgFrm.Page1.oPag.oTTPROTIT_2_11.value=this.w_TTPROTIT
      replace t_TTPROTIT with this.oPgFrm.Page1.oPag.oTTPROTIT_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTTCOGTIT_2_1.value==this.w_TTCOGTIT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTTCOGTIT_2_1.value=this.w_TTCOGTIT
      replace t_TTCOGTIT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTTCOGTIT_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTTNOMTIT_2_2.value==this.w_TTNOMTIT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTTNOMTIT_2_2.value=this.w_TTNOMTIT
      replace t_TTNOMTIT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTTNOMTIT_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTTTELEFO_2_3.value==this.w_TTTELEFO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTTTELEFO_2_3.value=this.w_TTTELEFO
      replace t_TTTELEFO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTTTELEFO_2_3.value
    endif
    cp_SetControlsValueExtFlds(this,'TITOLARI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .w_TTCOGTIT<>space(25)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_TTCOGTIT<>space(25))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_TTCOGTIT=space(25)
      .w_TTNOMTIT=space(25)
      .w_TTTELEFO=space(18)
      .w_TTLUONAS=space(30)
      .w_TTPRONAS=space(2)
      .w_TTDATNAS=ctod("  /  /  ")
      .w_TTINDIRI=space(30)
      .w_TTCAPTIT=space(8)
      .w_TTLOCTIT=space(30)
      .w_TT_SESSO=space(1)
      .w_TTPROTIT=space(2)
      .DoRTCalc(1,10,.f.)
        .w_TT_SESSO = 'M'
    endwith
    this.DoRTCalc(12,12,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_TTCOGTIT = t_TTCOGTIT
    this.w_TTNOMTIT = t_TTNOMTIT
    this.w_TTTELEFO = t_TTTELEFO
    this.w_TTLUONAS = t_TTLUONAS
    this.w_TTPRONAS = t_TTPRONAS
    this.w_TTDATNAS = t_TTDATNAS
    this.w_TTINDIRI = t_TTINDIRI
    this.w_TTCAPTIT = t_TTCAPTIT
    this.w_TTLOCTIT = t_TTLOCTIT
    this.w_TT_SESSO = this.oPgFrm.Page1.oPag.oTT_SESSO_2_10.RadioValue(.t.)
    this.w_TTPROTIT = t_TTPROTIT
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_TTCOGTIT with this.w_TTCOGTIT
    replace t_TTNOMTIT with this.w_TTNOMTIT
    replace t_TTTELEFO with this.w_TTTELEFO
    replace t_TTLUONAS with this.w_TTLUONAS
    replace t_TTPRONAS with this.w_TTPRONAS
    replace t_TTDATNAS with this.w_TTDATNAS
    replace t_TTINDIRI with this.w_TTINDIRI
    replace t_TTCAPTIT with this.w_TTCAPTIT
    replace t_TTLOCTIT with this.w_TTLOCTIT
    replace t_TT_SESSO with this.oPgFrm.Page1.oPag.oTT_SESSO_2_10.ToRadio()
    replace t_TTPROTIT with this.w_TTPROTIT
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsar_mttPag1 as StdContainer
  Width  = 611
  height = 310
  stdWidth  = 611
  stdheight = 310
  resizeXpos=306
  resizeYpos=138
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=7, top=2, width=600,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=3,Field1="TTCOGTIT",Label1="Cognome",Field2="TTNOMTIT",Label2="Nome",Field3="TTTELEFO",Label3="Telefono",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 235778682

  add object oStr_1_2 as StdString with uid="VBGMIHDSJX",Visible=.t., Left=13, Top=218,;
    Alignment=1, Width=87, Height=15,;
    Caption="Nato a:"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="AYHROSNADE",Visible=.t., Left=457, Top=218,;
    Alignment=1, Width=61, Height=15,;
    Caption="Il:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="AVYOLXJFFD",Visible=.t., Left=353, Top=219,;
    Alignment=1, Width=46, Height=15,;
    Caption="Prov.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="KUYHAXAGHL",Visible=.t., Left=8, Top=277,;
    Alignment=1, Width=92, Height=15,;
    Caption="CAP - localit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="PCKMOEOYBR",Visible=.t., Left=425, Top=277,;
    Alignment=1, Width=41, Height=15,;
    Caption="Prov.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="LYZBZSLIJV",Visible=.t., Left=13, Top=247,;
    Alignment=1, Width=87, Height=15,;
    Caption="Indirizzo:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-3,top=21,;
    width=596+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*9*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-2,top=22,width=595+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*9*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oTTLUONAS_2_4.Refresh()
      this.Parent.oTTPRONAS_2_5.Refresh()
      this.Parent.oTTDATNAS_2_6.Refresh()
      this.Parent.oTTINDIRI_2_7.Refresh()
      this.Parent.oTTCAPTIT_2_8.Refresh()
      this.Parent.oTTLOCTIT_2_9.Refresh()
      this.Parent.oTT_SESSO_2_10.Refresh()
      this.Parent.oTTPROTIT_2_11.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oTTLUONAS_2_4 as StdTrsField with uid="AKCEAKQYAW",rtseq=5,rtrep=.t.,;
    cFormVar="w_TTLUONAS",value=space(30),;
    ToolTipText = "Luogo di nascita del titolare",;
    HelpContextID = 243311241,;
    cTotal="", bFixedPos=.t., cQueryName = "TTLUONAS",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=248, Left=103, Top=218, InputMask=replicate('X',30)

  add object oTTPRONAS_2_5 as StdTrsField with uid="BTUMKYREBP",rtseq=6,rtrep=.t.,;
    cFormVar="w_TTPRONAS",value=space(2),;
    ToolTipText = "Sigla della provincia del luogo di nascita",;
    HelpContextID = 243131017,;
    cTotal="", bFixedPos=.t., cQueryName = "TTPRONAS",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=402, Top=219, cSayPict=['!!'], cGetPict=['!!'], InputMask=replicate('X',2), bHasZoom = .t. 

  proc oTTPRONAS_2_5.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P"," "," ",".w_TTPRONAS")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oTTDATNAS_2_6 as StdTrsField with uid="ZKPHOTAVKL",rtseq=7,rtrep=.t.,;
    cFormVar="w_TTDATNAS",value=ctod("  /  /  "),;
    ToolTipText = "Data di nascita del titolare",;
    HelpContextID = 247210633,;
    cTotal="", bFixedPos=.t., cQueryName = "TTDATNAS",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=519, Top=219

  add object oTTINDIRI_2_7 as StdTrsField with uid="SURBBQHLML",rtseq=8,rtrep=.t.,;
    cFormVar="w_TTINDIRI",value=space(30),;
    ToolTipText = "Indirizzo di residenza",;
    HelpContextID = 147419775,;
    cTotal="", bFixedPos=.t., cQueryName = "TTINDIRI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=248, Left=103, Top=247, InputMask=replicate('X',30)

  add object oTTCAPTIT_2_8 as StdTrsField with uid="QFPWORCZVA",rtseq=9,rtrep=.t.,;
    cFormVar="w_TTCAPTIT",value=space(8),;
    ToolTipText = "Codice di avviamento postale di residenza",;
    HelpContextID = 193195382,;
    cTotal="", bFixedPos=.t., cQueryName = "TTCAPTIT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=103, Top=276, cSayPict=['99999999'], cGetPict=['99999999'], InputMask=replicate('X',8), bHasZoom = .t. 

  proc oTTCAPTIT_2_8.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_TTCAPTIT",".w_TTLOCTIT",".w_TTPROTIT")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oTTLOCTIT_2_9 as StdTrsField with uid="MPKRZRNEZG",rtseq=10,rtrep=.t.,;
    cFormVar="w_TTLOCTIT",value=space(30),;
    ToolTipText = "Localit� di residenza",;
    HelpContextID = 205872502,;
    cTotal="", bFixedPos=.t., cQueryName = "TTLOCTIT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=243, Left=180, Top=276, InputMask=replicate('X',30), bHasZoom = .t. 

  proc oTTLOCTIT_2_9.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_TTCAPTIT",".w_TTLOCTIT",".w_TTPROTIT")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oTT_SESSO_2_10 as StdTrsRadio with uid="WUVWRQZCUC",rtrep=.t.,;
    cFormVar="w_TT_SESSO", ButtonCount=2,;
    Height=32, Width=77, Left=531, Top=276,;
    cTotal="", cQueryName = "TT_SESSO",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
  , bGlobalFont=.t.

    proc oTT_SESSO_2_10.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Maschio"
      this.Buttons(1).HelpContextID = 48222853
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Femmina"
      this.Buttons(2).HelpContextID = 48222853
      this.Buttons(2).Top=15
      this.SetAll("Width",75)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Sesso")
      StdRadio::init()
    endproc

  func oTT_SESSO_2_10.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..TT_SESSO,&i_cF..t_TT_SESSO),this.value)
    return(iif(xVal =1,'M',;
    iif(xVal =2,'F',;
    space(1))))
  endfunc
  func oTT_SESSO_2_10.GetRadio()
    this.Parent.oContained.w_TT_SESSO = this.RadioValue()
    return .t.
  endfunc

  func oTT_SESSO_2_10.ToRadio()
    this.Parent.oContained.w_TT_SESSO=trim(this.Parent.oContained.w_TT_SESSO)
    return(;
      iif(this.Parent.oContained.w_TT_SESSO=='M',1,;
      iif(this.Parent.oContained.w_TT_SESSO=='F',2,;
      0)))
  endfunc

  func oTT_SESSO_2_10.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oTTPROTIT_2_11 as StdTrsField with uid="UGSOPBUPSL",rtseq=12,rtrep=.t.,;
    cFormVar="w_TTPROTIT",value=space(2),;
    ToolTipText = "Sigla della provincia di residenza",;
    HelpContextID = 193076598,;
    cTotal="", bFixedPos=.t., cQueryName = "TTPROTIT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=469, Top=276, cSayPict=['!!'], cGetPict=['!!'], InputMask=replicate('X',2), bHasZoom = .t. 

  proc oTTPROTIT_2_11.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P"," "," ",".w_TTPROTIT")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsar_mttBodyRow as CPBodyRowCnt
  Width=586
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oTTCOGTIT_2_1 as StdTrsField with uid="RAHAOLGYFQ",rtseq=2,rtrep=.t.,;
    cFormVar="w_TTCOGTIT",value=space(25),;
    ToolTipText = "Cognome del titolare",;
    HelpContextID = 201715062,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=208, Left=-2, Top=0, InputMask=replicate('X',25)

  add object oTTNOMTIT_2_2 as StdTrsField with uid="MWWSFLGLNX",rtseq=3,rtrep=.t.,;
    cFormVar="w_TTNOMTIT",value=space(25),;
    ToolTipText = "Nome del titolare",;
    HelpContextID = 195378550,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=208, Left=212, Top=0, InputMask=replicate('X',25)

  add object oTTTELEFO_2_3 as StdTrsField with uid="VKWQBDTUSH",rtseq=4,rtrep=.t.,;
    cFormVar="w_TTTELEFO",value=space(18),;
    ToolTipText = "Numero telefonico del titolare",;
    HelpContextID = 88154757,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=152, Left=429, Top=0, InputMask=replicate('X',18)
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oTTCOGTIT_2_1.When()
    return(.t.)
  proc oTTCOGTIT_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oTTCOGTIT_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=8
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_mtt','TITOLARI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".TTCODAZI=TITOLARI.TTCODAZI";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
