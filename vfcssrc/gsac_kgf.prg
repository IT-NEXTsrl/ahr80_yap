* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsac_kgf                                                        *
*              Generazione PDA                                                 *
*                                                                              *
*      Author: Zucchetti TAM Srl                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_56]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-18                                                      *
* Last revis.: 2017-06-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsac_kgf",oParentObject))

* --- Class definition
define class tgsac_kgf as StdForm
  Top    = 3
  Left   = 4

  * --- Standard Properties
  Width  = 857
  Height = 536+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2017-06-21"
  HelpContextID=32139113
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=61

  * --- Constant Properties
  _IDX = 0
  PAR_PROD_IDX = 0
  KEY_ARTI_IDX = 0
  FAM_ARTI_IDX = 0
  GRUMERC_IDX = 0
  CATEGOMO_IDX = 0
  MARCHI_IDX = 0
  MAGAZZIN_IDX = 0
  CAN_TIER_IDX = 0
  cPrg = "gsac_kgf"
  cComment = "Generazione PDA"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_PPCODICE = space(2)
  w_ELAMPS = ctod('  /  /  ')
  w_ORAMPS = space(8)
  w_UCRIFR = space(1)
  w_ELAODL = ctod('  /  /  ')
  w_ORAODL = space(8)
  w_DATREG = ctod('  /  /  ')
  w_ORAREG = space(8)
  w_PPCODICE = space(2)
  w_GEN_PDA = .F.
  w_EDITREC = .F.
  w_TIPATT = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_KEYRIF = space(10)
  w_PDAMSG = .F.
  w_CRIELAB = space(1)
  o_CRIELAB = space(1)
  w_CRIFORN = space(1)
  w_FLAROB = space(1)
  w_PDCODINI = space(20)
  w_PDCODFIN = space(20)
  w_PDFAMAIN = space(5)
  w_PDFAMAFI = space(5)
  w_PDGRUINI = space(5)
  w_PDGRUFIN = space(5)
  w_PDCATINI = space(5)
  w_PDCATFIN = space(5)
  w_PDMARINI = space(5)
  w_PDMARFIN = space(5)
  w_PDMAGINI = space(5)
  w_DESMAGI = space(30)
  w_PDMAGFIN = space(5)
  w_PDCOMINI = space(15)
  o_PDCOMINI = space(15)
  w_PDCOMFIN = space(15)
  w_DESCOMINI = space(30)
  w_DESFAMAI = space(35)
  w_DESGRUI = space(35)
  w_DESCATI = space(35)
  w_DEMARINI = space(35)
  w_DESINI = space(40)
  w_DESFIN = space(40)
  w_ELAPDF = space(1)
  w_ELAPDS = space(1)
  w_PPFLAROB = space(1)
  w_DESCOMFIN = space(30)
  w_CRIELA = space(1)
  o_CRIELA = space(1)
  w_MCRIFOR = space(1)
  w_STAORD = space(1)
  w_SELEZM = space(1)
  o_SELEZM = space(1)
  w_DESMAGF = space(30)
  w_DESFAMAF = space(35)
  w_DESGRUF = space(35)
  w_DESCATF = space(35)
  w_DEMARFIN = space(35)
  w_SELEZA = space(1)
  o_SELEZA = space(1)
  w_RIGALIS = 0
  w_DATOBSO = ctod('  /  /  ')
  w_DATOBS1 = ctod('  /  /  ')
  w_ARCODINI = space(10)
  w_ARCODFIN = space(10)
  w_NULLA = space(10)
  w_RET = .F.
  w_ZOOMMAGA = .NULL.
  w_LBLMAGA = .NULL.
  w_ZOOMART = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsac_kgfPag1","gsac_kgf",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(2).addobject("oPag","tgsac_kgfPag2","gsac_kgf",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni aggiuntive")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPDCODINI_1_29
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOMMAGA = this.oPgFrm.Pages(1).oPag.ZOOMMAGA
    this.w_LBLMAGA = this.oPgFrm.Pages(1).oPag.LBLMAGA
    this.w_ZOOMART = this.oPgFrm.Pages(2).oPag.ZOOMART
    DoDefault()
    proc Destroy()
      this.w_ZOOMMAGA = .NULL.
      this.w_LBLMAGA = .NULL.
      this.w_ZOOMART = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[8]
    this.cWorkTables[1]='PAR_PROD'
    this.cWorkTables[2]='KEY_ARTI'
    this.cWorkTables[3]='FAM_ARTI'
    this.cWorkTables[4]='GRUMERC'
    this.cWorkTables[5]='CATEGOMO'
    this.cWorkTables[6]='MARCHI'
    this.cWorkTables[7]='MAGAZZIN'
    this.cWorkTables[8]='CAN_TIER'
    return(this.OpenAllTables(8))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSAC_BFB(this,"A")
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PPCODICE=space(2)
      .w_ELAMPS=ctod("  /  /  ")
      .w_ORAMPS=space(8)
      .w_UCRIFR=space(1)
      .w_ELAODL=ctod("  /  /  ")
      .w_ORAODL=space(8)
      .w_DATREG=ctod("  /  /  ")
      .w_ORAREG=space(8)
      .w_PPCODICE=space(2)
      .w_GEN_PDA=.f.
      .w_EDITREC=.f.
      .w_TIPATT=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_KEYRIF=space(10)
      .w_PDAMSG=.f.
      .w_CRIELAB=space(1)
      .w_CRIFORN=space(1)
      .w_FLAROB=space(1)
      .w_PDCODINI=space(20)
      .w_PDCODFIN=space(20)
      .w_PDFAMAIN=space(5)
      .w_PDFAMAFI=space(5)
      .w_PDGRUINI=space(5)
      .w_PDGRUFIN=space(5)
      .w_PDCATINI=space(5)
      .w_PDCATFIN=space(5)
      .w_PDMARINI=space(5)
      .w_PDMARFIN=space(5)
      .w_PDMAGINI=space(5)
      .w_DESMAGI=space(30)
      .w_PDMAGFIN=space(5)
      .w_PDCOMINI=space(15)
      .w_PDCOMFIN=space(15)
      .w_DESCOMINI=space(30)
      .w_DESFAMAI=space(35)
      .w_DESGRUI=space(35)
      .w_DESCATI=space(35)
      .w_DEMARINI=space(35)
      .w_DESINI=space(40)
      .w_DESFIN=space(40)
      .w_ELAPDF=space(1)
      .w_ELAPDS=space(1)
      .w_PPFLAROB=space(1)
      .w_DESCOMFIN=space(30)
      .w_CRIELA=space(1)
      .w_MCRIFOR=space(1)
      .w_STAORD=space(1)
      .w_SELEZM=space(1)
      .w_DESMAGF=space(30)
      .w_DESFAMAF=space(35)
      .w_DESGRUF=space(35)
      .w_DESCATF=space(35)
      .w_DEMARFIN=space(35)
      .w_SELEZA=space(1)
      .w_RIGALIS=0
      .w_DATOBSO=ctod("  /  /  ")
      .w_DATOBS1=ctod("  /  /  ")
      .w_ARCODINI=space(10)
      .w_ARCODFIN=space(10)
      .w_NULLA=space(10)
      .w_RET=.f.
        .w_PPCODICE = "AA"
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_PPCODICE))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,6,.f.)
        .w_DATREG = i_datsys
        .w_ORAREG = time()
        .w_PPCODICE = "AA"
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_PPCODICE))
          .link_1_13('Full')
        endif
        .w_GEN_PDA = .T.
        .w_EDITREC = .T.
        .w_TIPATT = 'A'
        .w_OBTEST = i_DATSYS
        .w_KEYRIF = SYS(2015)
        .w_PDAMSG = .T.
        .DoRTCalc(16,19,.f.)
        if not(empty(.w_PDCODINI))
          .link_1_29('Full')
        endif
        .DoRTCalc(20,20,.f.)
        if not(empty(.w_PDCODFIN))
          .link_1_30('Full')
        endif
        .DoRTCalc(21,21,.f.)
        if not(empty(.w_PDFAMAIN))
          .link_1_32('Full')
        endif
        .DoRTCalc(22,22,.f.)
        if not(empty(.w_PDFAMAFI))
          .link_1_33('Full')
        endif
        .DoRTCalc(23,23,.f.)
        if not(empty(.w_PDGRUINI))
          .link_1_34('Full')
        endif
        .DoRTCalc(24,24,.f.)
        if not(empty(.w_PDGRUFIN))
          .link_1_35('Full')
        endif
        .DoRTCalc(25,25,.f.)
        if not(empty(.w_PDCATINI))
          .link_1_36('Full')
        endif
        .DoRTCalc(26,26,.f.)
        if not(empty(.w_PDCATFIN))
          .link_1_37('Full')
        endif
        .DoRTCalc(27,27,.f.)
        if not(empty(.w_PDMARINI))
          .link_1_38('Full')
        endif
        .DoRTCalc(28,28,.f.)
        if not(empty(.w_PDMARFIN))
          .link_1_39('Full')
        endif
        .DoRTCalc(29,29,.f.)
        if not(empty(.w_PDMAGINI))
          .link_1_42('Full')
        endif
        .DoRTCalc(30,31,.f.)
        if not(empty(.w_PDMAGFIN))
          .link_1_45('Full')
        endif
        .w_PDCOMINI = SPACE(15)
        .DoRTCalc(32,32,.f.)
        if not(empty(.w_PDCOMINI))
          .link_1_49('Full')
        endif
        .w_PDCOMFIN = .w_PDCOMINI
        .DoRTCalc(33,33,.f.)
        if not(empty(.w_PDCOMFIN))
          .link_1_50('Full')
        endif
          .DoRTCalc(34,40,.f.)
        .w_ELAPDF = 'F'
        .w_ELAPDS = 'S'
        .w_PPFLAROB = .w_FLAROB
          .DoRTCalc(44,44,.f.)
        .w_CRIELA = .w_CRIELAB
        .w_MCRIFOR = .w_CRIFORN
        .w_STAORD = 'S'
      .oPgFrm.Page1.oPag.ZOOMMAGA.Calculate()
        .w_SELEZM = "S"
      .oPgFrm.Page1.oPag.LBLMAGA.Calculate(AH_msgFormat(iif(.w_CRIELA='G', "Gruppi di Magazzino", "Magazzini di impegno")))
      .oPgFrm.Page2.oPag.ZOOMART.Calculate()
          .DoRTCalc(49,53,.f.)
        .w_SELEZA = "D"
    endwith
    this.DoRTCalc(55,61,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_77.enabled = this.oPgFrm.Page1.oPag.oBtn_1_77.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_78.enabled = this.oPgFrm.Page1.oPag.oBtn_1_78.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_79.enabled = this.oPgFrm.Page1.oPag.oBtn_1_79.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_1.enabled = this.oPgFrm.Page2.oPag.oBtn_2_1.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_2.enabled = this.oPgFrm.Page2.oPag.oBtn_2_2.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,8,.t.)
          .link_1_13('Full')
        if .o_CRIELA<>.w_CRIELA
          .Calculate_VASGIKTUPU()
        endif
        if .o_SELEZM<>.w_SELEZM
          .Calculate_LOGQKNSSGF()
        endif
        .DoRTCalc(10,32,.t.)
        if .o_PDCOMINI<>.w_PDCOMINI
            .w_PDCOMFIN = .w_PDCOMINI
          .link_1_50('Full')
        endif
        .DoRTCalc(34,46,.t.)
        if .o_CRIELA<>.w_CRIELA
            .w_STAORD = 'S'
        endif
        .oPgFrm.Page1.oPag.ZOOMMAGA.Calculate()
        .oPgFrm.Page1.oPag.LBLMAGA.Calculate(AH_msgFormat(iif(.w_CRIELA='G', "Gruppi di Magazzino", "Magazzini di impegno")))
        .oPgFrm.Page2.oPag.ZOOMART.Calculate()
        if .o_SELEZA<>.w_SELEZA
          .Calculate_IOMYBPQFVI()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(48,61,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOMMAGA.Calculate()
        .oPgFrm.Page1.oPag.LBLMAGA.Calculate(AH_msgFormat(iif(.w_CRIELA='G', "Gruppi di Magazzino", "Magazzini di impegno")))
        .oPgFrm.Page2.oPag.ZOOMART.Calculate()
    endwith
  return

  proc Calculate_GGOBXLBQED()
    with this
          * --- Condizione di editing Label e zoom
          .w_ZOOMMAGA.cZoomFile = IIF(.w_CRIELA = 'G', "GSVEGKGF" , "GSVEMKGF")
          .w_ZOOMMAGA.cCpQueryName = IIF(.w_CRIELA = 'G', "QUERY\GSVEGKGF" , "QUERY\GSVEFKGF")
    endwith
  endproc
  proc Calculate_XOHCHPGRKK()
    with this
          * --- Condizione di editing Label e zoom
          .w_LBLMAGA.Enabled = IIF(.w_CRIELA $ 'G-M', .T., .F.)
          .w_ZOOMMAGA.Enabled = IIF(.w_CRIELA $ 'G-M', .T., .F.)
          .w_ZOOMMAGA.GRD.Enabled = .w_ZOOMMAGA.Enabled
    endwith
  endproc
  proc Calculate_LRBMCECHHD()
    with this
          * --- Gestione filtri magazzino MAGA_TEMP
          GSAR_BFM(this;
              ,"AGGIORNA";
              ,.w_KEYRIF;
              ,.w_CRIELA;
              ,'N';
             )
    endwith
  endproc
  proc Calculate_LYAVVYYQCV()
    with this
          * --- Gestione filtri magazzino MAGA_TEMP
          GSAR_BFM(this;
              ,"ESCI";
              ,.w_KEYRIF;
              ,.w_CRIELA;
              ,'N';
             )
    endwith
  endproc
  proc Calculate_VASGIKTUPU()
    with this
          * --- Gestione filtri magazzino ZOOMMAGA
          GSAR_BFM(this;
              ,"APERTURA";
              ,.w_KEYRIF;
              ,.w_CRIELA;
              ,"N";
             )
          .w_ZOOMMAGA.enabled = .w_CRIELA $ 'G-M'
          .w_ZOOMMAGA.grd.enabled = .w_CRIELA $ 'G-M'
          .w_RET = .NotifyEvent("InterrogaMaga")
          .w_SELEZM = 'S' && IIF(.w_CRIELA $ 'G-M' , 'S', SPACE(1) )
          .o_SELEZM = .w_SELEZM
          .w_RET = .w_ZOOMMAGA.CheckAll()
    endwith
  endproc
  proc Calculate_LOGQKNSSGF()
    with this
          * --- Gestione filtri magazzino ZOOMMAGA
      if .w_SELEZM='S'
          .w_RET = .w_ZOOMMAGA.checkall()
      endif
      if .w_SELEZM='D'
          .w_RET = .w_ZOOMMAGA.uncheckall()
      endif
    endwith
  endproc
  proc Calculate_FYQVKGRZSL()
    with this
          * --- ActivatePage 2
          .oPgFrm.ActivePage = 2
    endwith
  endproc
  proc Calculate_ZPESTHEAQS()
    with this
          * --- Gestione filtri magazzino ZOOMART
          .w_RET = .NotifyEvent("Interroga")
          .w_SELEZA = 'S'
          .o_SELEZA = 'S'
          .w_RET = .w_ZOOMART.CheckAll()
    endwith
  endproc
  proc Calculate_IOMYBPQFVI()
    with this
          * --- Gestione filtri magazzino ZOOMART
      if .w_SELEZA='S'
          .w_RET = .w_ZOOMART.CheckAll()
      endif
      if .w_SELEZA<>'S'
          .w_RET = .w_ZOOMART.UnCheckAll()
      endif
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPDFAMAIN_1_32.enabled = this.oPgFrm.Page1.oPag.oPDFAMAIN_1_32.mCond()
    this.oPgFrm.Page1.oPag.oPDFAMAFI_1_33.enabled = this.oPgFrm.Page1.oPag.oPDFAMAFI_1_33.mCond()
    this.oPgFrm.Page1.oPag.oPDGRUINI_1_34.enabled = this.oPgFrm.Page1.oPag.oPDGRUINI_1_34.mCond()
    this.oPgFrm.Page1.oPag.oPDGRUFIN_1_35.enabled = this.oPgFrm.Page1.oPag.oPDGRUFIN_1_35.mCond()
    this.oPgFrm.Page1.oPag.oPDCATINI_1_36.enabled = this.oPgFrm.Page1.oPag.oPDCATINI_1_36.mCond()
    this.oPgFrm.Page1.oPag.oPDCATFIN_1_37.enabled = this.oPgFrm.Page1.oPag.oPDCATFIN_1_37.mCond()
    this.oPgFrm.Page1.oPag.oPDMARINI_1_38.enabled = this.oPgFrm.Page1.oPag.oPDMARINI_1_38.mCond()
    this.oPgFrm.Page1.oPag.oPDMARFIN_1_39.enabled = this.oPgFrm.Page1.oPag.oPDMARFIN_1_39.mCond()
    this.oPgFrm.Page1.oPag.oPDMAGINI_1_42.enabled = this.oPgFrm.Page1.oPag.oPDMAGINI_1_42.mCond()
    this.oPgFrm.Page1.oPag.oPDMAGFIN_1_45.enabled = this.oPgFrm.Page1.oPag.oPDMAGFIN_1_45.mCond()
    this.oPgFrm.Page1.oPag.oPDCOMINI_1_49.enabled = this.oPgFrm.Page1.oPag.oPDCOMINI_1_49.mCond()
    this.oPgFrm.Page1.oPag.oPDCOMFIN_1_50.enabled = this.oPgFrm.Page1.oPag.oPDCOMFIN_1_50.mCond()
    this.oPgFrm.Page1.oPag.oSELEZM_1_75.enabled_(this.oPgFrm.Page1.oPag.oSELEZM_1_75.mCond())
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Blank") or lower(cEvent)==lower("w_CRIELA Changed")
          .Calculate_GGOBXLBQED()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank") or lower(cEvent)==lower("w_CRIELA Changed") or lower(cEvent)==lower("w_ZOOMMAGA after query")
          .Calculate_XOHCHPGRKK()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_ZOOMMAGA menucheck") or lower(cEvent)==lower("w_ZOOMMAGA row checked") or lower(cEvent)==lower("w_ZOOMMAGA row unchecked") or lower(cEvent)==lower("w_zoommaga rowcheckall") or lower(cEvent)==lower("w_zoommaga rowuncheckall")
          .Calculate_LRBMCECHHD()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Done")
          .Calculate_LYAVVYYQCV()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank") or lower(cEvent)==lower("Init")
          .Calculate_VASGIKTUPU()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.ZOOMMAGA.Event(cEvent)
      .oPgFrm.Page1.oPag.LBLMAGA.Event(cEvent)
        if lower(cEvent)==lower("Interroga")
          .Calculate_FYQVKGRZSL()
          bRefresh=.t.
        endif
      .oPgFrm.Page2.oPag.ZOOMART.Event(cEvent)
        if lower(cEvent)==lower("ActivatePage 2")
          .Calculate_ZPESTHEAQS()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsac_kgf
    if upper(cEvent)='FORMLOAD'
      this.w_ZOOMMAGA.GRD.SCROLLBARS=2
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PPCODICE
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_PROD_IDX,3]
    i_lTable = "PAR_PROD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2], .t., this.PAR_PROD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PPCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PPCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPCODICE,PPELAMPS,PPORAMPS,PPELAODL,PPORAODL,PPFLDIGE";
                   +" from "+i_cTable+" "+i_lTable+" where PPCODICE="+cp_ToStrODBC(this.w_PPCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPCODICE',this.w_PPCODICE)
            select PPCODICE,PPELAMPS,PPORAMPS,PPELAODL,PPORAODL,PPFLDIGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PPCODICE = NVL(_Link_.PPCODICE,space(2))
      this.w_ELAMPS = NVL(cp_ToDate(_Link_.PPELAMPS),ctod("  /  /  "))
      this.w_ORAMPS = NVL(_Link_.PPORAMPS,space(8))
      this.w_ELAODL = NVL(cp_ToDate(_Link_.PPELAODL),ctod("  /  /  "))
      this.w_ORAODL = NVL(_Link_.PPORAODL,space(8))
      this.w_UCRIFR = NVL(_Link_.PPFLDIGE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PPCODICE = space(2)
      endif
      this.w_ELAMPS = ctod("  /  /  ")
      this.w_ORAMPS = space(8)
      this.w_ELAODL = ctod("  /  /  ")
      this.w_ORAODL = space(8)
      this.w_UCRIFR = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])+'\'+cp_ToStr(_Link_.PPCODICE,1)
      cp_ShowWarn(i_cKey,this.PAR_PROD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PPCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PPCODICE
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_PROD_IDX,3]
    i_lTable = "PAR_PROD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2], .t., this.PAR_PROD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PPCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PPCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPCODICE,PPCRIFOR,PPCRIELA,PPFLAROB";
                   +" from "+i_cTable+" "+i_lTable+" where PPCODICE="+cp_ToStrODBC(this.w_PPCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPCODICE',this.w_PPCODICE)
            select PPCODICE,PPCRIFOR,PPCRIELA,PPFLAROB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PPCODICE = NVL(_Link_.PPCODICE,space(2))
      this.w_CRIFORN = NVL(_Link_.PPCRIFOR,space(1))
      this.w_CRIELAB = NVL(_Link_.PPCRIELA,space(1))
      this.w_FLAROB = NVL(_Link_.PPFLAROB,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PPCODICE = space(2)
      endif
      this.w_CRIFORN = space(1)
      this.w_CRIELAB = space(1)
      this.w_FLAROB = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])+'\'+cp_ToStr(_Link_.PPCODICE,1)
      cp_ShowWarn(i_cKey,this.PAR_PROD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PPCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PDCODINI
  func Link_1_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDCODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_PDCODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO,CACODART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_PDCODINI))
          select CACODICE,CADESART,CADTOBSO,CACODART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDCODINI)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_PDCODINI)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_PDCODINI)+"%");

            select CACODICE,CADESART,CADTOBSO,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PDCODINI) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oPDCODINI_1_29'),i_cWhere,'',"Articoli",'gsac_zpd.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO,CACODART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CADTOBSO,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDCODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_PDCODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_PDCODINI)
            select CACODICE,CADESART,CADTOBSO,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDCODINI = NVL(_Link_.CACODICE,space(20))
      this.w_DESINI = NVL(_Link_.CADESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
      this.w_ARCODINI = NVL(_Link_.CACODART,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_PDCODINI = space(20)
      endif
      this.w_DESINI = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_ARCODINI = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_PDCODFIN)) OR  (.w_PDCODINI <= .w_PDCODFIN)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST or .w_PPFLAROB='S' )
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("L'articolo iniziale � maggiore di quello finale, obsoleto oppure non di provenienza esterna")
        endif
        this.w_PDCODINI = space(20)
        this.w_DESINI = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_ARCODINI = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDCODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PDCODFIN
  func Link_1_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDCODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_PDCODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO,CACODART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_PDCODFIN))
          select CACODICE,CADESART,CADTOBSO,CACODART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDCODFIN)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_PDCODFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_PDCODFIN)+"%");

            select CACODICE,CADESART,CADTOBSO,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PDCODFIN) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oPDCODFIN_1_30'),i_cWhere,'',"Articoli",'gsac_zpd.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO,CACODART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CADTOBSO,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDCODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_PDCODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_PDCODFIN)
            select CACODICE,CADESART,CADTOBSO,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDCODFIN = NVL(_Link_.CACODICE,space(20))
      this.w_DESFIN = NVL(_Link_.CADESART,space(40))
      this.w_DATOBS1 = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
      this.w_ARCODFIN = NVL(_Link_.CACODART,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_PDCODFIN = space(20)
      endif
      this.w_DESFIN = space(40)
      this.w_DATOBS1 = ctod("  /  /  ")
      this.w_ARCODFIN = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_PDCODINI <= .w_PDCODFIN) or (empty(.w_PDCODFIN))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST or .w_PPFLAROB='S' )
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("L'articolo iniziale � maggiore di quello finale, obsoleto oppure non di provenienza esterna")
        endif
        this.w_PDCODFIN = space(20)
        this.w_DESFIN = space(40)
        this.w_DATOBS1 = ctod("  /  /  ")
        this.w_ARCODFIN = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDCODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PDFAMAIN
  func Link_1_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDFAMAIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_PDFAMAIN)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_PDFAMAIN))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDFAMAIN)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDFAMAIN) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oPDFAMAIN_1_32'),i_cWhere,'',"FAMIGLIE ARTICOLI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDFAMAIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_PDFAMAIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_PDFAMAIN)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDFAMAIN = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAI = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PDFAMAIN = space(5)
      endif
      this.w_DESFAMAI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PDFAMAIN <= .w_PDFAMAFI OR EMPTY(.w_PDFAMAFI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PDFAMAIN = space(5)
        this.w_DESFAMAI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDFAMAIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PDFAMAFI
  func Link_1_33(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDFAMAFI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_PDFAMAFI)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_PDFAMAFI))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDFAMAFI)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDFAMAFI) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oPDFAMAFI_1_33'),i_cWhere,'',"FAMIGLIE ARTICOLI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDFAMAFI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_PDFAMAFI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_PDFAMAFI)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDFAMAFI = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAF = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PDFAMAFI = space(5)
      endif
      this.w_DESFAMAF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PDFAMAIN <= .w_PDFAMAFI
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PDFAMAFI = space(5)
        this.w_DESFAMAF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDFAMAFI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PDGRUINI
  func Link_1_34(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDGRUINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_PDGRUINI)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_PDGRUINI))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDGRUINI)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDGRUINI) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oPDGRUINI_1_34'),i_cWhere,'',"GRUPPI MERCEOLOGICI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDGRUINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_PDGRUINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_PDGRUINI)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDGRUINI = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUI = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PDGRUINI = space(5)
      endif
      this.w_DESGRUI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PDGRUINI <= .w_PDGRUFIN OR EMPTY(.w_PDGRUFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PDGRUINI = space(5)
        this.w_DESGRUI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDGRUINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PDGRUFIN
  func Link_1_35(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDGRUFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_PDGRUFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_PDGRUFIN))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDGRUFIN)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDGRUFIN) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oPDGRUFIN_1_35'),i_cWhere,'',"GRUPPI MERCEOLOGICI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDGRUFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_PDGRUFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_PDGRUFIN)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDGRUFIN = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUF = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PDGRUFIN = space(5)
      endif
      this.w_DESGRUF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PDGRUINI <= .w_PDGRUFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PDGRUFIN = space(5)
        this.w_DESGRUF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDGRUFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PDCATINI
  func Link_1_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDCATINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_PDCATINI)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_PDCATINI))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDCATINI)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDCATINI) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oPDCATINI_1_36'),i_cWhere,'',"CATEGORIE OMOGENEE",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDCATINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_PDCATINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_PDCATINI)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDCATINI = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATI = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PDCATINI = space(5)
      endif
      this.w_DESCATI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PDCATINI <= .w_PDCATFIN OR EMPTY(.w_PDCATFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PDCATINI = space(5)
        this.w_DESCATI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDCATINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PDCATFIN
  func Link_1_37(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDCATFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_PDCATFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_PDCATFIN))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDCATFIN)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDCATFIN) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oPDCATFIN_1_37'),i_cWhere,'',"CATEGORIE OMOGENEE",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDCATFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_PDCATFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_PDCATFIN)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDCATFIN = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATF = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PDCATFIN = space(5)
      endif
      this.w_DESCATF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PDCATINI <= .w_PDCATFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PDCATFIN = space(5)
        this.w_DESCATF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDCATFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PDMARINI
  func Link_1_38(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_lTable = "MARCHI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2], .t., this.MARCHI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDMARINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MARCHI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MACODICE like "+cp_ToStrODBC(trim(this.w_PDMARINI)+"%");

          i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MACODICE',trim(this.w_PDMARINI))
          select MACODICE,MADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDMARINI)==trim(_Link_.MACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDMARINI) and !this.bDontReportError
            deferred_cp_zoom('MARCHI','*','MACODICE',cp_AbsName(oSource.parent,'oPDMARINI_1_38'),i_cWhere,'',"MARCHI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',oSource.xKey(1))
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDMARINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(this.w_PDMARINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_PDMARINI)
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDMARINI = NVL(_Link_.MACODICE,space(5))
      this.w_DEMARINI = NVL(_Link_.MADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PDMARINI = space(5)
      endif
      this.w_DEMARINI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PDMARINI <= .w_PDMARFIN OR EMPTY(.w_PDMARFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PDMARINI = space(5)
        this.w_DEMARINI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)
      cp_ShowWarn(i_cKey,this.MARCHI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDMARINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PDMARFIN
  func Link_1_39(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_lTable = "MARCHI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2], .t., this.MARCHI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDMARFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MARCHI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MACODICE like "+cp_ToStrODBC(trim(this.w_PDMARFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MACODICE',trim(this.w_PDMARFIN))
          select MACODICE,MADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDMARFIN)==trim(_Link_.MACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDMARFIN) and !this.bDontReportError
            deferred_cp_zoom('MARCHI','*','MACODICE',cp_AbsName(oSource.parent,'oPDMARFIN_1_39'),i_cWhere,'',"MARCHI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',oSource.xKey(1))
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDMARFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(this.w_PDMARFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_PDMARFIN)
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDMARFIN = NVL(_Link_.MACODICE,space(5))
      this.w_DEMARFIN = NVL(_Link_.MADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PDMARFIN = space(5)
      endif
      this.w_DEMARFIN = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PDMARINI <= .w_PDMARFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PDMARFIN = space(5)
        this.w_DEMARFIN = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)
      cp_ShowWarn(i_cKey,this.MARCHI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDMARFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PDMAGINI
  func Link_1_42(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDMAGINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_PDMAGINI)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_PDMAGINI))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDMAGINI)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDMAGINI) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oPDMAGINI_1_42'),i_cWhere,'',"MAGAZZINI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDMAGINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_PDMAGINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_PDMAGINI)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDMAGINI = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAGI = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_PDMAGINI = space(5)
      endif
      this.w_DESMAGI = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PDMAGINI <= .w_PDMAGFIN OR EMPTY(.w_PDMAGFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PDMAGINI = space(5)
        this.w_DESMAGI = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDMAGINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PDMAGFIN
  func Link_1_45(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDMAGFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_PDMAGFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_PDMAGFIN))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDMAGFIN)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDMAGFIN) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oPDMAGFIN_1_45'),i_cWhere,'',"MAGAZZINI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDMAGFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_PDMAGFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_PDMAGFIN)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDMAGFIN = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAGF = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_PDMAGFIN = space(5)
      endif
      this.w_DESMAGF = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PDMAGINI <= .w_PDMAGFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PDMAGFIN = space(5)
        this.w_DESMAGF = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDMAGFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PDCOMINI
  func Link_1_49(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDCOMINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_PDCOMINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_PDCOMINI))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDCOMINI)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDCOMINI) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oPDCOMINI_1_49'),i_cWhere,'',"COMMESSE",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDCOMINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_PDCOMINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_PDCOMINI)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDCOMINI = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOMINI = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_PDCOMINI = space(15)
      endif
      this.w_DESCOMINI = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PDCOMINI <= .w_PDCOMFIN OR EMPTY(.w_PDCOMFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PDCOMINI = space(15)
        this.w_DESCOMINI = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDCOMINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PDCOMFIN
  func Link_1_50(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDCOMFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_PDCOMFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_PDCOMFIN))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDCOMFIN)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDCOMFIN) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oPDCOMFIN_1_50'),i_cWhere,'',"COMMESSE",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDCOMFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_PDCOMFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_PDCOMFIN)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDCOMFIN = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOMFIN = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_PDCOMFIN = space(15)
      endif
      this.w_DESCOMFIN = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PDCOMINI <= .w_PDCOMFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PDCOMFIN = space(15)
        this.w_DESCOMFIN = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDCOMFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oUCRIFR_1_4.RadioValue()==this.w_UCRIFR)
      this.oPgFrm.Page1.oPag.oUCRIFR_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oELAODL_1_5.value==this.w_ELAODL)
      this.oPgFrm.Page1.oPag.oELAODL_1_5.value=this.w_ELAODL
    endif
    if not(this.oPgFrm.Page1.oPag.oORAODL_1_6.value==this.w_ORAODL)
      this.oPgFrm.Page1.oPag.oORAODL_1_6.value=this.w_ORAODL
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCODINI_1_29.value==this.w_PDCODINI)
      this.oPgFrm.Page1.oPag.oPDCODINI_1_29.value=this.w_PDCODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCODFIN_1_30.value==this.w_PDCODFIN)
      this.oPgFrm.Page1.oPag.oPDCODFIN_1_30.value=this.w_PDCODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPDFAMAIN_1_32.value==this.w_PDFAMAIN)
      this.oPgFrm.Page1.oPag.oPDFAMAIN_1_32.value=this.w_PDFAMAIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPDFAMAFI_1_33.value==this.w_PDFAMAFI)
      this.oPgFrm.Page1.oPag.oPDFAMAFI_1_33.value=this.w_PDFAMAFI
    endif
    if not(this.oPgFrm.Page1.oPag.oPDGRUINI_1_34.value==this.w_PDGRUINI)
      this.oPgFrm.Page1.oPag.oPDGRUINI_1_34.value=this.w_PDGRUINI
    endif
    if not(this.oPgFrm.Page1.oPag.oPDGRUFIN_1_35.value==this.w_PDGRUFIN)
      this.oPgFrm.Page1.oPag.oPDGRUFIN_1_35.value=this.w_PDGRUFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCATINI_1_36.value==this.w_PDCATINI)
      this.oPgFrm.Page1.oPag.oPDCATINI_1_36.value=this.w_PDCATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCATFIN_1_37.value==this.w_PDCATFIN)
      this.oPgFrm.Page1.oPag.oPDCATFIN_1_37.value=this.w_PDCATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPDMARINI_1_38.value==this.w_PDMARINI)
      this.oPgFrm.Page1.oPag.oPDMARINI_1_38.value=this.w_PDMARINI
    endif
    if not(this.oPgFrm.Page1.oPag.oPDMARFIN_1_39.value==this.w_PDMARFIN)
      this.oPgFrm.Page1.oPag.oPDMARFIN_1_39.value=this.w_PDMARFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPDMAGINI_1_42.value==this.w_PDMAGINI)
      this.oPgFrm.Page1.oPag.oPDMAGINI_1_42.value=this.w_PDMAGINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAGI_1_43.value==this.w_DESMAGI)
      this.oPgFrm.Page1.oPag.oDESMAGI_1_43.value=this.w_DESMAGI
    endif
    if not(this.oPgFrm.Page1.oPag.oPDMAGFIN_1_45.value==this.w_PDMAGFIN)
      this.oPgFrm.Page1.oPag.oPDMAGFIN_1_45.value=this.w_PDMAGFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCOMINI_1_49.value==this.w_PDCOMINI)
      this.oPgFrm.Page1.oPag.oPDCOMINI_1_49.value=this.w_PDCOMINI
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCOMFIN_1_50.value==this.w_PDCOMFIN)
      this.oPgFrm.Page1.oPag.oPDCOMFIN_1_50.value=this.w_PDCOMFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOMINI_1_51.value==this.w_DESCOMINI)
      this.oPgFrm.Page1.oPag.oDESCOMINI_1_51.value=this.w_DESCOMINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAMAI_1_52.value==this.w_DESFAMAI)
      this.oPgFrm.Page1.oPag.oDESFAMAI_1_52.value=this.w_DESFAMAI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRUI_1_53.value==this.w_DESGRUI)
      this.oPgFrm.Page1.oPag.oDESGRUI_1_53.value=this.w_DESGRUI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCATI_1_54.value==this.w_DESCATI)
      this.oPgFrm.Page1.oPag.oDESCATI_1_54.value=this.w_DESCATI
    endif
    if not(this.oPgFrm.Page1.oPag.oDEMARINI_1_61.value==this.w_DEMARINI)
      this.oPgFrm.Page1.oPag.oDEMARINI_1_61.value=this.w_DEMARINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINI_1_64.value==this.w_DESINI)
      this.oPgFrm.Page1.oPag.oDESINI_1_64.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIN_1_65.value==this.w_DESFIN)
      this.oPgFrm.Page1.oPag.oDESFIN_1_65.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oELAPDF_1_67.RadioValue()==this.w_ELAPDF)
      this.oPgFrm.Page1.oPag.oELAPDF_1_67.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oELAPDS_1_68.RadioValue()==this.w_ELAPDS)
      this.oPgFrm.Page1.oPag.oELAPDS_1_68.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPPFLAROB_1_69.RadioValue()==this.w_PPFLAROB)
      this.oPgFrm.Page1.oPag.oPPFLAROB_1_69.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOMFIN_1_70.value==this.w_DESCOMFIN)
      this.oPgFrm.Page1.oPag.oDESCOMFIN_1_70.value=this.w_DESCOMFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCRIELA_1_71.RadioValue()==this.w_CRIELA)
      this.oPgFrm.Page1.oPag.oCRIELA_1_71.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMCRIFOR_1_72.RadioValue()==this.w_MCRIFOR)
      this.oPgFrm.Page1.oPag.oMCRIFOR_1_72.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTAORD_1_73.RadioValue()==this.w_STAORD)
      this.oPgFrm.Page1.oPag.oSTAORD_1_73.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZM_1_75.RadioValue()==this.w_SELEZM)
      this.oPgFrm.Page1.oPag.oSELEZM_1_75.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAGF_1_86.value==this.w_DESMAGF)
      this.oPgFrm.Page1.oPag.oDESMAGF_1_86.value=this.w_DESMAGF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAMAF_1_87.value==this.w_DESFAMAF)
      this.oPgFrm.Page1.oPag.oDESFAMAF_1_87.value=this.w_DESFAMAF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRUF_1_88.value==this.w_DESGRUF)
      this.oPgFrm.Page1.oPag.oDESGRUF_1_88.value=this.w_DESGRUF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCATF_1_89.value==this.w_DESCATF)
      this.oPgFrm.Page1.oPag.oDESCATF_1_89.value=this.w_DESCATF
    endif
    if not(this.oPgFrm.Page1.oPag.oDEMARFIN_1_90.value==this.w_DEMARFIN)
      this.oPgFrm.Page1.oPag.oDEMARFIN_1_90.value=this.w_DEMARFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oSELEZA_2_5.RadioValue()==this.w_SELEZA)
      this.oPgFrm.Page2.oPag.oSELEZA_2_5.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(((empty(.w_PDCODFIN)) OR  (.w_PDCODINI <= .w_PDCODFIN)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST or .w_PPFLAROB='S' ))  and not(empty(.w_PDCODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDCODINI_1_29.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("L'articolo iniziale � maggiore di quello finale, obsoleto oppure non di provenienza esterna")
          case   not(((.w_PDCODINI <= .w_PDCODFIN) or (empty(.w_PDCODFIN))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST or .w_PPFLAROB='S' ))  and not(empty(.w_PDCODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDCODFIN_1_30.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("L'articolo iniziale � maggiore di quello finale, obsoleto oppure non di provenienza esterna")
          case   not(.w_PDFAMAIN <= .w_PDFAMAFI OR EMPTY(.w_PDFAMAFI))  and (.w_EDITREC)  and not(empty(.w_PDFAMAIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDFAMAIN_1_32.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PDFAMAIN <= .w_PDFAMAFI)  and (.w_EDITREC)  and not(empty(.w_PDFAMAFI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDFAMAFI_1_33.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PDGRUINI <= .w_PDGRUFIN OR EMPTY(.w_PDGRUFIN))  and (.w_EDITREC)  and not(empty(.w_PDGRUINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDGRUINI_1_34.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PDGRUINI <= .w_PDGRUFIN)  and (.w_EDITREC)  and not(empty(.w_PDGRUFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDGRUFIN_1_35.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PDCATINI <= .w_PDCATFIN OR EMPTY(.w_PDCATFIN))  and (.w_EDITREC)  and not(empty(.w_PDCATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDCATINI_1_36.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PDCATINI <= .w_PDCATFIN)  and (.w_EDITREC)  and not(empty(.w_PDCATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDCATFIN_1_37.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PDMARINI <= .w_PDMARFIN OR EMPTY(.w_PDMARFIN))  and (.w_EDITREC)  and not(empty(.w_PDMARINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDMARINI_1_38.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PDMARINI <= .w_PDMARFIN)  and (.w_EDITREC)  and not(empty(.w_PDMARFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDMARFIN_1_39.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PDMAGINI <= .w_PDMAGFIN OR EMPTY(.w_PDMAGFIN))  and (.w_EDITREC)  and not(empty(.w_PDMAGINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDMAGINI_1_42.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PDMAGINI <= .w_PDMAGFIN)  and (.w_EDITREC)  and not(empty(.w_PDMAGFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDMAGFIN_1_45.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PDCOMINI <= .w_PDCOMFIN OR EMPTY(.w_PDCOMFIN))  and (g_PERCAN='S' AND .w_EDITREC)  and not(empty(.w_PDCOMINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDCOMINI_1_49.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PDCOMINI <= .w_PDCOMFIN)  and (g_PERCAN='S' AND .w_EDITREC)  and not(empty(.w_PDCOMFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDCOMFIN_1_50.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CRIELAB = this.w_CRIELAB
    this.o_PDCOMINI = this.w_PDCOMINI
    this.o_CRIELA = this.w_CRIELA
    this.o_SELEZM = this.w_SELEZM
    this.o_SELEZA = this.w_SELEZA
    return

enddefine

* --- Define pages as container
define class tgsac_kgfPag1 as StdContainer
  Width  = 853
  height = 536
  stdWidth  = 853
  stdheight = 536
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oUCRIFR_1_4 as StdCombo with uid="LGEJZLCJTZ",rtseq=4,rtrep=.f.,left=660,top=451,width=167,height=21, enabled=.f.;
    , ToolTipText = "Criterio di scelta fornitore";
    , HelpContextID = 79954246;
    , cFormVar="w_UCRIFR",RowSource=""+"Proporzione,"+"Tempo,"+"Prezzo,"+"Affidabilit�,"+"Priorit�,"+"Ripartizione", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oUCRIFR_1_4.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'T',;
    iif(this.value =3,'Z',;
    iif(this.value =4,'A',;
    iif(this.value =5,'I',;
    iif(this.value =6,'R',;
    space(1))))))))
  endfunc
  func oUCRIFR_1_4.GetRadio()
    this.Parent.oContained.w_UCRIFR = this.RadioValue()
    return .t.
  endfunc

  func oUCRIFR_1_4.SetRadio()
    this.Parent.oContained.w_UCRIFR=trim(this.Parent.oContained.w_UCRIFR)
    this.value = ;
      iif(this.Parent.oContained.w_UCRIFR=='P',1,;
      iif(this.Parent.oContained.w_UCRIFR=='T',2,;
      iif(this.Parent.oContained.w_UCRIFR=='Z',3,;
      iif(this.Parent.oContained.w_UCRIFR=='A',4,;
      iif(this.Parent.oContained.w_UCRIFR=='I',5,;
      iif(this.Parent.oContained.w_UCRIFR=='R',6,;
      0))))))
  endfunc

  add object oELAODL_1_5 as StdField with uid="NRICKUCHHY",rtseq=5,rtrep=.f.,;
    cFormVar = "w_ELAODL", cQueryName = "ELAODL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 245954886,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=660, Top=423

  add object oORAODL_1_6 as StdField with uid="BDPAHZQVUD",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ORAODL", cQueryName = "ORAODL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    HelpContextID = 245956582,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=768, Top=423, InputMask=replicate('X',8)

  add object oPDCODINI_1_29 as StdField with uid="CWVZMPJNZR",rtseq=19,rtrep=.f.,;
    cFormVar = "w_PDCODINI", cQueryName = "PDCODINI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "L'articolo iniziale � maggiore di quello finale, obsoleto oppure non di provenienza esterna",;
    ToolTipText = "Codice articolo iniziale",;
    HelpContextID = 72805825,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=123, Top=33, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_PDCODINI"

  func oPDCODINI_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_29('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDCODINI_1_29.ecpDrop(oSource)
    this.Parent.oContained.link_1_29('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDCODINI_1_29.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oPDCODINI_1_29'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Articoli",'gsac_zpd.KEY_ARTI_VZM',this.parent.oContained
  endproc

  add object oPDCODFIN_1_30 as StdField with uid="HPPPNNCKLL",rtseq=20,rtrep=.f.,;
    cFormVar = "w_PDCODFIN", cQueryName = "PDCODFIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "L'articolo iniziale � maggiore di quello finale, obsoleto oppure non di provenienza esterna",;
    ToolTipText = "Codice articolo finale",;
    HelpContextID = 123137468,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=123, Top=57, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_PDCODFIN"

  func oPDCODFIN_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_30('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDCODFIN_1_30.ecpDrop(oSource)
    this.Parent.oContained.link_1_30('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDCODFIN_1_30.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oPDCODFIN_1_30'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Articoli",'gsac_zpd.KEY_ARTI_VZM',this.parent.oContained
  endproc

  add object oPDFAMAIN_1_32 as StdField with uid="BAUZFWPTWC",rtseq=21,rtrep=.f.,;
    cFormVar = "w_PDFAMAIN", cQueryName = "PDFAMAIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia articolo di inizio selezione",;
    HelpContextID = 198491580,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=123, Top=81, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_PDFAMAIN"

  func oPDFAMAIN_1_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EDITREC)
    endwith
   endif
  endfunc

  func oPDFAMAIN_1_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_32('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDFAMAIN_1_32.ecpDrop(oSource)
    this.Parent.oContained.link_1_32('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDFAMAIN_1_32.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oPDFAMAIN_1_32'),iif(empty(i_cWhere),.f.,i_cWhere),'',"FAMIGLIE ARTICOLI",'',this.parent.oContained
  endproc

  add object oPDFAMAFI_1_33 as StdField with uid="GHDDKOKVIG",rtseq=22,rtrep=.f.,;
    cFormVar = "w_PDFAMAFI", cQueryName = "PDFAMAFI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia articolo di fine selezione",;
    HelpContextID = 69943871,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=523, Top=81, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_PDFAMAFI"

  proc oPDFAMAFI_1_33.mDefault
    with this.Parent.oContained
      if empty(.w_PDFAMAFI)
        .w_PDFAMAFI = .w_PDFAMAIN
      endif
    endwith
  endproc

  func oPDFAMAFI_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EDITREC)
    endwith
   endif
  endfunc

  func oPDFAMAFI_1_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_33('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDFAMAFI_1_33.ecpDrop(oSource)
    this.Parent.oContained.link_1_33('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDFAMAFI_1_33.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oPDFAMAFI_1_33'),iif(empty(i_cWhere),.f.,i_cWhere),'',"FAMIGLIE ARTICOLI",'',this.parent.oContained
  endproc

  add object oPDGRUINI_1_34 as StdField with uid="VYQYSTJFCH",rtseq=23,rtrep=.f.,;
    cFormVar = "w_PDGRUINI", cQueryName = "PDGRUINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo merceologico di inizio selezione",;
    HelpContextID = 54767041,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=123, Top=104, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_PDGRUINI"

  func oPDGRUINI_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EDITREC)
    endwith
   endif
  endfunc

  func oPDGRUINI_1_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_34('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDGRUINI_1_34.ecpDrop(oSource)
    this.Parent.oContained.link_1_34('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDGRUINI_1_34.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oPDGRUINI_1_34'),iif(empty(i_cWhere),.f.,i_cWhere),'',"GRUPPI MERCEOLOGICI",'',this.parent.oContained
  endproc

  add object oPDGRUFIN_1_35 as StdField with uid="BKTBROMCMD",rtseq=24,rtrep=.f.,;
    cFormVar = "w_PDGRUFIN", cQueryName = "PDGRUFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo merceologico di fine selezione",;
    HelpContextID = 105098684,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=523, Top=104, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_PDGRUFIN"

  proc oPDGRUFIN_1_35.mDefault
    with this.Parent.oContained
      if empty(.w_PDGRUFIN)
        .w_PDGRUFIN = .w_PDGRUINI
      endif
    endwith
  endproc

  func oPDGRUFIN_1_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EDITREC)
    endwith
   endif
  endfunc

  func oPDGRUFIN_1_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_35('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDGRUFIN_1_35.ecpDrop(oSource)
    this.Parent.oContained.link_1_35('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDGRUFIN_1_35.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oPDGRUFIN_1_35'),iif(empty(i_cWhere),.f.,i_cWhere),'',"GRUPPI MERCEOLOGICI",'',this.parent.oContained
  endproc

  add object oPDCATINI_1_36 as StdField with uid="WQNLFQEXDD",rtseq=25,rtrep=.f.,;
    cFormVar = "w_PDCATINI", cQueryName = "PDCATINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria omogenea di inizio selezione",;
    HelpContextID = 56946113,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=123, Top=127, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_PDCATINI"

  func oPDCATINI_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EDITREC)
    endwith
   endif
  endfunc

  func oPDCATINI_1_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_36('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDCATINI_1_36.ecpDrop(oSource)
    this.Parent.oContained.link_1_36('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDCATINI_1_36.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oPDCATINI_1_36'),iif(empty(i_cWhere),.f.,i_cWhere),'',"CATEGORIE OMOGENEE",'',this.parent.oContained
  endproc

  add object oPDCATFIN_1_37 as StdField with uid="WHQQEZYTLE",rtseq=26,rtrep=.f.,;
    cFormVar = "w_PDCATFIN", cQueryName = "PDCATFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria omogenea di fine selezione",;
    HelpContextID = 107277756,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=523, Top=127, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_PDCATFIN"

  proc oPDCATFIN_1_37.mDefault
    with this.Parent.oContained
      if empty(.w_PDCATFIN)
        .w_PDCATFIN = .w_PDCATINI
      endif
    endwith
  endproc

  func oPDCATFIN_1_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EDITREC)
    endwith
   endif
  endfunc

  func oPDCATFIN_1_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_37('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDCATFIN_1_37.ecpDrop(oSource)
    this.Parent.oContained.link_1_37('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDCATFIN_1_37.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oPDCATFIN_1_37'),iif(empty(i_cWhere),.f.,i_cWhere),'',"CATEGORIE OMOGENEE",'',this.parent.oContained
  endproc

  add object oPDMARINI_1_38 as StdField with uid="WBGKIJDARW",rtseq=27,rtrep=.f.,;
    cFormVar = "w_PDMARINI", cQueryName = "PDMARINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice marchio di inizio selezione",;
    HelpContextID = 59002305,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=123, Top=150, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MARCHI", oKey_1_1="MACODICE", oKey_1_2="this.w_PDMARINI"

  func oPDMARINI_1_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EDITREC)
    endwith
   endif
  endfunc

  func oPDMARINI_1_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_38('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDMARINI_1_38.ecpDrop(oSource)
    this.Parent.oContained.link_1_38('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDMARINI_1_38.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MARCHI','*','MACODICE',cp_AbsName(this.parent,'oPDMARINI_1_38'),iif(empty(i_cWhere),.f.,i_cWhere),'',"MARCHI",'',this.parent.oContained
  endproc

  add object oPDMARFIN_1_39 as StdField with uid="LATPHZOTCT",rtseq=28,rtrep=.f.,;
    cFormVar = "w_PDMARFIN", cQueryName = "PDMARFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice marchio di fine selezione",;
    HelpContextID = 109333948,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=523, Top=150, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MARCHI", oKey_1_1="MACODICE", oKey_1_2="this.w_PDMARFIN"

  proc oPDMARFIN_1_39.mDefault
    with this.Parent.oContained
      if empty(.w_PDMARFIN)
        .w_PDMARFIN = .w_PDMARINI
      endif
    endwith
  endproc

  func oPDMARFIN_1_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EDITREC)
    endwith
   endif
  endfunc

  func oPDMARFIN_1_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_39('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDMARFIN_1_39.ecpDrop(oSource)
    this.Parent.oContained.link_1_39('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDMARFIN_1_39.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MARCHI','*','MACODICE',cp_AbsName(this.parent,'oPDMARFIN_1_39'),iif(empty(i_cWhere),.f.,i_cWhere),'',"MARCHI",'',this.parent.oContained
  endproc

  add object oPDMAGINI_1_42 as StdField with uid="WWAXPPIMPZ",rtseq=29,rtrep=.f.,;
    cFormVar = "w_PDMAGINI", cQueryName = "PDMAGINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 70536641,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=124, Top=173, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_PDMAGINI"

  func oPDMAGINI_1_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EDITREC)
    endwith
   endif
  endfunc

  func oPDMAGINI_1_42.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_42('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDMAGINI_1_42.ecpDrop(oSource)
    this.Parent.oContained.link_1_42('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDMAGINI_1_42.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oPDMAGINI_1_42'),iif(empty(i_cWhere),.f.,i_cWhere),'',"MAGAZZINI",'',this.parent.oContained
  endproc

  add object oDESMAGI_1_43 as StdField with uid="QONUNYBHAK",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DESMAGI", cQueryName = "DESMAGI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 109571530,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=196, Top=173, InputMask=replicate('X',30)

  add object oPDMAGFIN_1_45 as StdField with uid="IPBLQAUROZ",rtseq=31,rtrep=.f.,;
    cFormVar = "w_PDMAGFIN", cQueryName = "PDMAGFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 120868284,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=523, Top=173, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_PDMAGFIN"

  proc oPDMAGFIN_1_45.mDefault
    with this.Parent.oContained
      if empty(.w_PDMAGFIN)
        .w_PDMAGFIN = .w_PDMAGINI
      endif
    endwith
  endproc

  func oPDMAGFIN_1_45.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EDITREC)
    endwith
   endif
  endfunc

  func oPDMAGFIN_1_45.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_45('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDMAGFIN_1_45.ecpDrop(oSource)
    this.Parent.oContained.link_1_45('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDMAGFIN_1_45.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oPDMAGFIN_1_45'),iif(empty(i_cWhere),.f.,i_cWhere),'',"MAGAZZINI",'',this.parent.oContained
  endproc

  add object oPDCOMINI_1_49 as StdField with uid="GGQGKFAWGY",rtseq=32,rtrep=.f.,;
    cFormVar = "w_PDCOMINI", cQueryName = "PDCOMINI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa",;
    HelpContextID = 63368641,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=124, Top=200, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_PDCOMINI"

  func oPDCOMINI_1_49.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCAN='S' AND .w_EDITREC)
    endwith
   endif
  endfunc

  func oPDCOMINI_1_49.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_49('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDCOMINI_1_49.ecpDrop(oSource)
    this.Parent.oContained.link_1_49('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDCOMINI_1_49.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oPDCOMINI_1_49'),iif(empty(i_cWhere),.f.,i_cWhere),'',"COMMESSE",'',this.parent.oContained
  endproc

  add object oPDCOMFIN_1_50 as StdField with uid="TNWJSVTIPV",rtseq=33,rtrep=.f.,;
    cFormVar = "w_PDCOMFIN", cQueryName = "PDCOMFIN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa",;
    HelpContextID = 113700284,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=124, Top=224, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_PDCOMFIN"

  func oPDCOMFIN_1_50.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCAN='S' AND .w_EDITREC)
    endwith
   endif
  endfunc

  func oPDCOMFIN_1_50.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_50('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDCOMFIN_1_50.ecpDrop(oSource)
    this.Parent.oContained.link_1_50('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDCOMFIN_1_50.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oPDCOMFIN_1_50'),iif(empty(i_cWhere),.f.,i_cWhere),'',"COMMESSE",'',this.parent.oContained
  endproc

  add object oDESCOMINI_1_51 as StdField with uid="BCJHQPAVGJ",rtseq=34,rtrep=.f.,;
    cFormVar = "w_DESCOMINI", cQueryName = "DESCOMINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 263317740,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=258, Top=200, InputMask=replicate('X',30)

  add object oDESFAMAI_1_52 as StdField with uid="AOYBYAMKXI",rtseq=35,rtrep=.f.,;
    cFormVar = "w_DESFAMAI", cQueryName = "DESFAMAI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 259068543,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=196, Top=81, InputMask=replicate('X',35)

  add object oDESGRUI_1_53 as StdField with uid="ZABORREMDH",rtseq=36,rtrep=.f.,;
    cFormVar = "w_DESGRUI", cQueryName = "DESGRUI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 125693386,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=196, Top=104, InputMask=replicate('X',35)

  add object oDESCATI_1_54 as StdField with uid="ZFWYARIWSP",rtseq=37,rtrep=.f.,;
    cFormVar = "w_DESCATI", cQueryName = "DESCATI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 160558538,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=196, Top=127, InputMask=replicate('X',35)

  add object oDEMARINI_1_61 as StdField with uid="YGJVDCJIRK",rtseq=38,rtrep=.f.,;
    cFormVar = "w_DEMARINI", cQueryName = "DEMARINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 59002241,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=196, Top=150, InputMask=replicate('X',35)

  add object oDESINI_1_64 as StdField with uid="LIJOMNDAQG",rtseq=39,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 205787702,;
   bGlobalFont=.t.,;
    Height=21, Width=389, Left=288, Top=33, InputMask=replicate('X',40)

  add object oDESFIN_1_65 as StdField with uid="GTOXOUJFXY",rtseq=40,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 15798838,;
   bGlobalFont=.t.,;
    Height=21, Width=389, Left=288, Top=57, InputMask=replicate('X',40)

  add object oELAPDF_1_67 as StdCheck with uid="HKUREFTMVX",rtseq=41,rtrep=.f.,left=660, top=219, caption="a fabbisogno",;
    ToolTipText = "Elabora articoli a fabbisogno",;
    HelpContextID = 145357126,;
    cFormVar="w_ELAPDF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oELAPDF_1_67.RadioValue()
    return(iif(this.value =1,'F',;
    '.'))
  endfunc
  func oELAPDF_1_67.GetRadio()
    this.Parent.oContained.w_ELAPDF = this.RadioValue()
    return .t.
  endfunc

  func oELAPDF_1_67.SetRadio()
    this.Parent.oContained.w_ELAPDF=trim(this.Parent.oContained.w_ELAPDF)
    this.value = ;
      iif(this.Parent.oContained.w_ELAPDF=='F',1,;
      0)
  endfunc

  add object oELAPDS_1_68 as StdCheck with uid="FGYUVIBBUI",rtseq=42,rtrep=.f.,left=660, top=244, caption="a scorta",;
    ToolTipText = "Elabora articoli a quantit� costante",;
    HelpContextID = 95025478,;
    cFormVar="w_ELAPDS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oELAPDS_1_68.RadioValue()
    return(iif(this.value =1,'S',;
    '.'))
  endfunc
  func oELAPDS_1_68.GetRadio()
    this.Parent.oContained.w_ELAPDS = this.RadioValue()
    return .t.
  endfunc

  func oELAPDS_1_68.SetRadio()
    this.Parent.oContained.w_ELAPDS=trim(this.Parent.oContained.w_ELAPDS)
    this.value = ;
      iif(this.Parent.oContained.w_ELAPDS=='S',1,;
      0)
  endfunc

  add object oPPFLAROB_1_69 as StdCheck with uid="TITLJSWEZG",rtseq=43,rtrep=.f.,left=660, top=273, caption="Pianifica articoli obsoleti",;
    ToolTipText = "Pianifica proposte di acquisto per articoli obsoleti",;
    HelpContextID = 74862136,;
    cFormVar="w_PPFLAROB", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPPFLAROB_1_69.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oPPFLAROB_1_69.GetRadio()
    this.Parent.oContained.w_PPFLAROB = this.RadioValue()
    return .t.
  endfunc

  func oPPFLAROB_1_69.SetRadio()
    this.Parent.oContained.w_PPFLAROB=trim(this.Parent.oContained.w_PPFLAROB)
    this.value = ;
      iif(this.Parent.oContained.w_PPFLAROB=='S',1,;
      0)
  endfunc

  add object oDESCOMFIN_1_70 as StdField with uid="DXEBQBAGFD",rtseq=44,rtrep=.f.,;
    cFormVar = "w_DESCOMFIN", cQueryName = "DESCOMFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 263317665,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=258, Top=224, InputMask=replicate('X',30)


  add object oCRIELA_1_71 as StdCombo with uid="VSDAZQQBWZ",rtseq=45,rtrep=.f.,left=660,top=312,width=168,height=21;
    , ToolTipText = "Criterio di pianificazione delle proposte di acquisto";
    , HelpContextID = 69173030;
    , cFormVar="w_CRIELA",RowSource=""+"Aggregata,"+"Per Magazzino,"+"Per gruppi di magazzini", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCRIELA_1_71.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'M',;
    iif(this.value =3,'G',;
    space(1)))))
  endfunc
  func oCRIELA_1_71.GetRadio()
    this.Parent.oContained.w_CRIELA = this.RadioValue()
    return .t.
  endfunc

  func oCRIELA_1_71.SetRadio()
    this.Parent.oContained.w_CRIELA=trim(this.Parent.oContained.w_CRIELA)
    this.value = ;
      iif(this.Parent.oContained.w_CRIELA=='A',1,;
      iif(this.Parent.oContained.w_CRIELA=='M',2,;
      iif(this.Parent.oContained.w_CRIELA=='G',3,;
      0)))
  endfunc


  add object oMCRIFOR_1_72 as StdCombo with uid="SSUXEZZSTH",rtseq=46,rtrep=.f.,left=660,top=351,width=168,height=21;
    , ToolTipText = "Criterio di scelta del miglior fornitore in base ai contratti in essere";
    , HelpContextID = 29622470;
    , cFormVar="w_MCRIFOR",RowSource=""+"Tempo,"+"Prezzo,"+"Affidabilit�,"+"Priorit�", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMCRIFOR_1_72.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'Z',;
    iif(this.value =3,'A',;
    iif(this.value =4,'I',;
    space(1))))))
  endfunc
  func oMCRIFOR_1_72.GetRadio()
    this.Parent.oContained.w_MCRIFOR = this.RadioValue()
    return .t.
  endfunc

  func oMCRIFOR_1_72.SetRadio()
    this.Parent.oContained.w_MCRIFOR=trim(this.Parent.oContained.w_MCRIFOR)
    this.value = ;
      iif(this.Parent.oContained.w_MCRIFOR=='T',1,;
      iif(this.Parent.oContained.w_MCRIFOR=='Z',2,;
      iif(this.Parent.oContained.w_MCRIFOR=='A',3,;
      iif(this.Parent.oContained.w_MCRIFOR=='I',4,;
      0))))
  endfunc


  add object oSTAORD_1_73 as StdCombo with uid="THMCRRBBQC",rtseq=47,rtrep=.f.,left=660,top=389,width=168,height=21;
    , ToolTipText = "Stato ordini generati da PDA";
    , HelpContextID = 126419494;
    , cFormVar="w_STAORD",RowSource=""+"Suggerita,"+"Confermata", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTAORD_1_73.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'C',;
    space(1))))
  endfunc
  func oSTAORD_1_73.GetRadio()
    this.Parent.oContained.w_STAORD = this.RadioValue()
    return .t.
  endfunc

  func oSTAORD_1_73.SetRadio()
    this.Parent.oContained.w_STAORD=trim(this.Parent.oContained.w_STAORD)
    this.value = ;
      iif(this.Parent.oContained.w_STAORD=='S',1,;
      iif(this.Parent.oContained.w_STAORD=='C',2,;
      0))
  endfunc


  add object ZOOMMAGA as cp_szoombox with uid="VPMEOXAFHG",left=10, top=255, width=373,height=238,;
    caption='Object',;
   bGlobalFont=.t.,;
    bRetriveAllRows=.t.,cZoomFile="GSVEMKGF",bOptions=.f.,bAdvOptions=.f.,cZoomOnZoom="",cTable="MAGAZZIN",bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "InterrogaMaga",;
    nPag=1;
    , HelpContextID = 145858534

  add object oSELEZM_1_75 as StdRadio with uid="LTQWYYFJNE",rtseq=48,rtrep=.f.,left=15, top=495, width=155,height=32;
    , cFormVar="w_SELEZM", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZM_1_75.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 16753446
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 16753446
      this.Buttons(2).Top=15
      this.SetAll("Width",153)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZM_1_75.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"D",;
    space(1))))
  endfunc
  func oSELEZM_1_75.GetRadio()
    this.Parent.oContained.w_SELEZM = this.RadioValue()
    return .t.
  endfunc

  func oSELEZM_1_75.SetRadio()
    this.Parent.oContained.w_SELEZM=trim(this.Parent.oContained.w_SELEZM)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZM=="S",1,;
      iif(this.Parent.oContained.w_SELEZM=="D",2,;
      0))
  endfunc

  func oSELEZM_1_75.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CRIELA $ 'G-M')
    endwith
   endif
  endfunc


  add object LBLMAGA as cp_calclbl with uid="UIGUTRTDRQ",left=20, top=248, width=222,height=15,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",FontBold=.f.,;
    nPag=1;
    , HelpContextID = 145858534


  add object oBtn_1_77 as StdButton with uid="QSKFOUORYR",left=796, top=489, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 58806806;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_77.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_78 as StdButton with uid="ALAMDEZKHR",left=745, top=489, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedre alle selezioni aggiuntive";
    , HelpContextID = 58806806;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_78.Click()
      with this.Parent.oContained
        .notifyevent('ActivatePage 2')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_79 as StdButton with uid="AOQBFJLGSU",left=690, top=489, width=48,height=45,;
    CpPicture="BMP\AUTO.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per iniziare l'elaborazione";
    , HelpContextID = 58806806;
    , Caption='\<Elabora';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_79.Click()
      with this.Parent.oContained
        GSAC_BFB(this.Parent.oContained,"A")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESMAGF_1_86 as StdField with uid="EBXVFVWORM",rtseq=49,rtrep=.f.,;
    cFormVar = "w_DESMAGF", cQueryName = "DESMAGF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 158863926,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=596, Top=173, InputMask=replicate('X',30)

  add object oDESFAMAF_1_87 as StdField with uid="WOCDSQXKSA",rtseq=50,rtrep=.f.,;
    cFormVar = "w_DESFAMAF", cQueryName = "DESFAMAF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 259068540,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=596, Top=81, InputMask=replicate('X',35)

  add object oDESGRUF_1_88 as StdField with uid="PYGQUHDJMJ",rtseq=51,rtrep=.f.,;
    cFormVar = "w_DESGRUF", cQueryName = "DESGRUF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 125693386,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=596, Top=104, InputMask=replicate('X',35)

  add object oDESCATF_1_89 as StdField with uid="SXSGGSHCII",rtseq=52,rtrep=.f.,;
    cFormVar = "w_DESCATF", cQueryName = "DESCATF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 160558538,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=596, Top=127, InputMask=replicate('X',35)

  add object oDEMARFIN_1_90 as StdField with uid="DJYWVFQILW",rtseq=53,rtrep=.f.,;
    cFormVar = "w_DEMARFIN", cQueryName = "DEMARFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 109333884,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=596, Top=150, InputMask=replicate('X',35)

  add object oStr_1_7 as StdString with uid="YHXVSTJWJF",Visible=.t., Left=454, Top=426,;
    Alignment=1, Width=198, Height=15,;
    Caption="Ultima generazione PDA eseguita il:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="NSOUGWYGTI",Visible=.t., Left=733, Top=424,;
    Alignment=1, Width=32, Height=15,;
    Caption="alle:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="NJKHJOINRO",Visible=.t., Left=454, Top=453,;
    Alignment=1, Width=198, Height=15,;
    Caption="Criterio di scelta selezionato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="DFDAZGHWTK",Visible=.t., Left=60, Top=35,;
    Alignment=1, Width=61, Height=15,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="TWCCZXZZKY",Visible=.t., Left=69, Top=58,;
    Alignment=1, Width=52, Height=15,;
    Caption="A codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="KBYTQWVWFL",Visible=.t., Left=39, Top=174,;
    Alignment=1, Width=83, Height=15,;
    Caption="Da magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="QPBYNHFHPK",Visible=.t., Left=437, Top=174,;
    Alignment=1, Width=83, Height=15,;
    Caption="A magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="WHGWHEWFXM",Visible=.t., Left=39, Top=10,;
    Alignment=0, Width=85, Height=15,;
    Caption="Codice articolo"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="ODMTRHWNGJ",Visible=.t., Left=37, Top=203,;
    Alignment=1, Width=85, Height=15,;
    Caption="Da commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="AKMDEUWFDE",Visible=.t., Left=51, Top=83,;
    Alignment=1, Width=70, Height=15,;
    Caption="Da famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_56 as StdString with uid="EWRTNGDSDH",Visible=.t., Left=38, Top=104,;
    Alignment=1, Width=83, Height=15,;
    Caption="Da gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_57 as StdString with uid="RWXHDLRPLM",Visible=.t., Left=31, Top=127,;
    Alignment=1, Width=90, Height=15,;
    Caption="Da cat. omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_58 as StdString with uid="BETNLUNOYI",Visible=.t., Left=450, Top=81,;
    Alignment=1, Width=70, Height=15,;
    Caption="A famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_59 as StdString with uid="DDVVDZKRKO",Visible=.t., Left=433, Top=104,;
    Alignment=1, Width=87, Height=15,;
    Caption="A gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_60 as StdString with uid="VUGHKWWVJN",Visible=.t., Left=440, Top=127,;
    Alignment=1, Width=80, Height=15,;
    Caption="A cat. omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_62 as StdString with uid="PQHJWMLKLX",Visible=.t., Left=31, Top=152,;
    Alignment=1, Width=90, Height=17,;
    Caption="Da marchio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_63 as StdString with uid="RGIGIXXNHK",Visible=.t., Left=440, Top=152,;
    Alignment=1, Width=80, Height=17,;
    Caption="A marchio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_66 as StdString with uid="CXXKRKNNJP",Visible=.t., Left=47, Top=226,;
    Alignment=1, Width=75, Height=15,;
    Caption="A commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_81 as StdString with uid="PNTFZPMMPZ",Visible=.t., Left=506, Top=390,;
    Alignment=1, Width=146, Height=18,;
    Caption="Stato proposte di acquisto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_84 as StdString with uid="TJZDCQSUZV",Visible=.t., Left=470, Top=313,;
    Alignment=1, Width=182, Height=15,;
    Caption="Criterio di pianificazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_85 as StdString with uid="HIUKTRYGLF",Visible=.t., Left=470, Top=353,;
    Alignment=1, Width=182, Height=15,;
    Caption="Criterio di scelta fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_91 as StdString with uid="YIGIVNUWEN",Visible=.t., Left=557, Top=221,;
    Alignment=1, Width=95, Height=18,;
    Caption="Elabora articoli:"  ;
  , bGlobalFont=.t.

  add object oBox_1_10 as StdBox with uid="YMGTSRTJXP",left=447, top=417, width=396,height=1

  add object oBox_1_47 as StdBox with uid="AVMMROXYXX",left=39, top=27, width=789,height=1

  add object oBox_1_80 as StdBox with uid="MGLTDTEJTQ",left=447, top=380, width=396,height=1

  add object oBox_1_82 as StdBox with uid="LJRBBOWUTB",left=447, top=298, width=396,height=1

  add object oBox_1_83 as StdBox with uid="VOEIFYMCVR",left=447, top=339, width=396,height=1

  add object oBox_1_98 as StdBox with uid="QDOWILHSOU",left=447, top=267, width=396,height=1
enddefine
define class tgsac_kgfPag2 as StdContainer
  Width  = 853
  height = 536
  stdWidth  = 853
  stdheight = 536
  resizeXpos=217
  resizeYpos=237
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_2_1 as StdButton with uid="PIZIPOFWEI",left=745, top=489, width=48,height=45,;
    CpPicture="BMP\AUTO.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per iniziare l'elaborazione";
    , HelpContextID = 58806806;
    , Caption='\<Elabora';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_1.Click()
      with this.Parent.oContained
        GSAC_BFB(this.Parent.oContained,"A")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_2 as StdButton with uid="RUBTYBZKTF",left=796, top=489, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 58806806;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_2.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZOOMART as cp_szoombox with uid="XCNTAMVCAJ",left=6, top=6, width=838,height=480,;
    caption='Object',;
   bGlobalFont=.t.,;
    bAdvOptions=.t.,cZoomOnZoom="GSMA_ACA",bQueryOnLoad=.f.,bReadOnly=.t.,cMenuFile="",cTable="KEY_ARTI",cZoomFile="GSACAKGF",bOptions=.t.,bNoZoomGridShape=.f.,bQueryOnDblClick=.t.,;
    cEvent = "Interroga",;
    nPag=2;
    , HelpContextID = 145858534

  add object oSELEZA_2_5 as StdRadio with uid="FMOKZITWYP",rtseq=54,rtrep=.f.,left=6, top=489, width=155,height=32;
    , cFormVar="w_SELEZA", ButtonCount=2, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oSELEZA_2_5.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 83862310
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 83862310
      this.Buttons(2).Top=15
      this.SetAll("Width",153)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZA_2_5.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"D",;
    space(1))))
  endfunc
  func oSELEZA_2_5.GetRadio()
    this.Parent.oContained.w_SELEZA = this.RadioValue()
    return .t.
  endfunc

  func oSELEZA_2_5.SetRadio()
    this.Parent.oContained.w_SELEZA=trim(this.Parent.oContained.w_SELEZA)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZA=="S",1,;
      iif(this.Parent.oContained.w_SELEZA=="D",2,;
      0))
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsac_kgf','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
