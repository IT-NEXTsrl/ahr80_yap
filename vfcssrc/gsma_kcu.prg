* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_kcu                                                        *
*              Lista articoli                                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-12-05                                                      *
* Last revis.: 2018-09-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_kcu",oParentObject))

* --- Class definition
define class tgsma_kcu as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 674
  Height = 228
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-09-25"
  HelpContextID=99253097
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsma_kcu"
  cComment = "Lista articoli"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_LOG = space(0)
  w_TOTRECORD = 0
  w_TOTARTI = 0
  w_TOTMAGA = 0
  w_RET = space(10)
  * --- Area Manuale = Declare Variables
  * --- gsma_kcu
  closable=.f.
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_kcuPag1","gsma_kcu",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oLOG_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_LOG=space(0)
      .w_TOTRECORD=0
      .w_TOTARTI=0
      .w_TOTMAGA=0
      .w_RET=space(10)
      .w_LOG=oParentObject.w_LOG
      .w_TOTRECORD=oParentObject.w_TOTRECORD
      .w_TOTARTI=oParentObject.w_TOTARTI
      .w_TOTMAGA=oParentObject.w_TOTMAGA
    endwith
    this.DoRTCalc(1,5,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_3.enabled = this.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_LOG=.w_LOG
      .oParentObject.w_TOTRECORD=.w_TOTRECORD
      .oParentObject.w_TOTARTI=.w_TOTARTI
      .oParentObject.w_TOTMAGA=.w_TOTMAGA
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,5,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_AXUNRDMHSC()
    with this
          * --- Caption
          .cComment = Ah_MsgFormat(IIF(.w_TOTARTI>0 And .w_TOTMAGA>0,"Lista Articoli\Magazzini",IIF(.w_TOTMAGA>0,"Lista Magazzini","Lista Articoli")))
          .w_RET = .SetCaption()
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oTOTRECORD_1_4.visible=!this.oPgFrm.Page1.oPag.oTOTRECORD_1_4.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_5.visible=!this.oPgFrm.Page1.oPag.oStr_1_5.mHide()
    this.oPgFrm.Page1.oPag.oTOTARTI_1_6.visible=!this.oPgFrm.Page1.oPag.oTOTARTI_1_6.mHide()
    this.oPgFrm.Page1.oPag.oTOTMAGA_1_7.visible=!this.oPgFrm.Page1.oPag.oTOTMAGA_1_7.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_10.visible=!this.oPgFrm.Page1.oPag.oStr_1_10.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Blank")
          .Calculate_AXUNRDMHSC()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oLOG_1_1.value==this.w_LOG)
      this.oPgFrm.Page1.oPag.oLOG_1_1.value=this.w_LOG
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTRECORD_1_4.value==this.w_TOTRECORD)
      this.oPgFrm.Page1.oPag.oTOTRECORD_1_4.value=this.w_TOTRECORD
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTARTI_1_6.value==this.w_TOTARTI)
      this.oPgFrm.Page1.oPag.oTOTARTI_1_6.value=this.w_TOTARTI
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTMAGA_1_7.value==this.w_TOTMAGA)
      this.oPgFrm.Page1.oPag.oTOTMAGA_1_7.value=this.w_TOTMAGA
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsma_kcuPag1 as StdContainer
  Width  = 670
  height = 228
  stdWidth  = 670
  stdheight = 228
  resizeXpos=237
  resizeYpos=117
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oLOG_1_1 as StdMemo with uid="YMOLNPNTXF",rtseq=1,rtrep=.f.,;
    cFormVar = "w_LOG", cQueryName = "LOG",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 98940746,;
   bGlobalFont=.t.,;
    Height=169, Width=661, Left=5, Top=3, readonly=.t.,tabstop=.f.


  add object oBtn_1_2 as StdButton with uid="JNPYZMNJQP",left=558, top=177, width=48,height=45,;
    CpPicture="bmp\doc.ico", caption="", nPag=1;
    , ToolTipText = "Copia la lista nella clipboard";
    , HelpContextID = 258607066;
    , Caption='\<Copia';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_2.Click()
      with this.Parent.oContained
        SELEZIONE(this.Parent.oContained,"Vuoto")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_3 as StdButton with uid="OFLKLPHFIY",left=611, top=177, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire ";
    , HelpContextID = 91935674;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_3.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oTOTRECORD_1_4 as StdField with uid="ILRPNKYOXE",rtseq=2,rtrep=.f.,;
    cFormVar = "w_TOTRECORD", cQueryName = "TOTRECORD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero totale articoli non movimentati nel periodo selezionato",;
    HelpContextID = 239264312,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=96, Top=178

  func oTOTRECORD_1_4.mHide()
    with this.Parent.oContained
      return (.w_TOTARTI <> 0 OR .w_TOTMAGA <> 0)
    endwith
  endfunc

  add object oTOTARTI_1_6 as StdField with uid="AEGREPOFHS",rtseq=3,rtrep=.f.,;
    cFormVar = "w_TOTARTI", cQueryName = "TOTARTI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero articoli non presenti in anagrafica",;
    HelpContextID = 209970890,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=96, Top=178

  func oTOTARTI_1_6.mHide()
    with this.Parent.oContained
      return (.w_TOTARTI = 0)
    endwith
  endfunc

  add object oTOTMAGA_1_7 as StdField with uid="BQVFWVJGBJ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_TOTMAGA", cQueryName = "TOTMAGA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero magazzini non presenti in anagrafica",;
    HelpContextID = 91756854,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=96, Top=201

  func oTOTMAGA_1_7.mHide()
    with this.Parent.oContained
      return (.w_TOTMAGA = 0)
    endwith
  endfunc

  add object oStr_1_5 as StdString with uid="EBQCERSCMJ",Visible=.t., Left=6, Top=180,;
    Alignment=1, Width=86, Height=18,;
    Caption="N. Articoli:"  ;
  , bGlobalFont=.t.

  func oStr_1_5.mHide()
    with this.Parent.oContained
      return (.w_TOTARTI = 0 And .w_TOTMAGA <> 0)
    endwith
  endfunc

  add object oStr_1_10 as StdString with uid="JAOROGLGDR",Visible=.t., Left=6, Top=203,;
    Alignment=1, Width=86, Height=18,;
    Caption="N. Magazzini:"  ;
  , bGlobalFont=.t.

  func oStr_1_10.mHide()
    with this.Parent.oContained
      return (.w_TOTMAGA = 0)
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_kcu','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsma_kcu
procedure SELEZIONE()
  parameters obj, pVuoto
  local memot  
  memot = obj.getctrl('w_LOG')
  memot.Selstart=0
  memot.SelLength = len(obj.w_LOG)
  _cliptext=memot.Seltext
endproc

* --- Fine Area Manuale
