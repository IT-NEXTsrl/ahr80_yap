* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_afi                                                        *
*              Codici CAB                                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-05-14                                                      *
* Last revis.: 2009-04-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_afi"))

* --- Class definition
define class tgsar_afi as StdForm
  Top    = 7
  Left   = 13

  * --- Standard Properties
  Width  = 512
  Height = 130+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-04-08"
  HelpContextID=59340649
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Constant Properties
  COD_CAB_IDX = 0
  COD_ABI_IDX = 0
  cFile = "COD_CAB"
  cKeySelect = "FICODABI,FICODCAB"
  cKeyWhere  = "FICODABI=this.w_FICODABI and FICODCAB=this.w_FICODCAB"
  cKeyWhereODBC = '"FICODABI="+cp_ToStrODBC(this.w_FICODABI)';
      +'+" and FICODCAB="+cp_ToStrODBC(this.w_FICODCAB)';

  cKeyWhereODBCqualified = '"COD_CAB.FICODABI="+cp_ToStrODBC(this.w_FICODABI)';
      +'+" and COD_CAB.FICODCAB="+cp_ToStrODBC(this.w_FICODCAB)';

  cPrg = "gsar_afi"
  cComment = "Codici CAB"
  icon = "anag.ico"
  cAutoZoom = 'GSAR_AFI'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_FICODABI = space(5)
  w_DESABI = space(80)
  w_FICODCAB = space(5)
  w_FIDESFIL = space(40)
  w_FIINDIRI = space(50)
  w_FI___CAP = space(9)
  w_FIDTOBSO = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'COD_CAB','gsar_afi')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_afiPag1","gsar_afi",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("CAB")
      .Pages(1).HelpContextID = 59052506
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFICODABI_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='COD_ABI'
    this.cWorkTables[2]='COD_CAB'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.COD_CAB_IDX,5],7]
    this.nPostItConn=i_TableProp[this.COD_CAB_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_FICODABI = NVL(FICODABI,space(5))
      .w_FICODCAB = NVL(FICODCAB,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_1_joined
    link_1_1_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from COD_CAB where FICODABI=KeySet.FICODABI
    *                            and FICODCAB=KeySet.FICODCAB
    *
    i_nConn = i_TableProp[this.COD_CAB_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.COD_CAB_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('COD_CAB')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "COD_CAB.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' COD_CAB '
      link_1_1_joined=this.AddJoinedLink_1_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'FICODABI',this.w_FICODABI  ,'FICODCAB',this.w_FICODCAB  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESABI = space(80)
        .w_FICODABI = NVL(FICODABI,space(5))
          if link_1_1_joined
            this.w_FICODABI = NVL(ABCODABI101,NVL(this.w_FICODABI,space(5)))
            this.w_DESABI = NVL(ABDESABI101,space(80))
          else
          .link_1_1('Load')
          endif
        .w_FICODCAB = NVL(FICODCAB,space(5))
        .w_FIDESFIL = NVL(FIDESFIL,space(40))
        .w_FIINDIRI = NVL(FIINDIRI,space(50))
        .w_FI___CAP = NVL(FI___CAP,space(9))
        .w_FIDTOBSO = NVL(cp_ToDate(FIDTOBSO),ctod("  /  /  "))
        cp_LoadRecExtFlds(this,'COD_CAB')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
    this.Calculate_FFNCSZAOBJ()
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_FICODABI = space(5)
      .w_DESABI = space(80)
      .w_FICODCAB = space(5)
      .w_FIDESFIL = space(40)
      .w_FIINDIRI = space(50)
      .w_FI___CAP = space(9)
      .w_FIDTOBSO = ctod("  /  /  ")
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_FICODABI))
          .link_1_1('Full')
          endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'COD_CAB')
    this.DoRTCalc(2,7,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
    this.Calculate_FFNCSZAOBJ()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oFICODABI_1_1.enabled = i_bVal
      .Page1.oPag.oFICODCAB_1_3.enabled = i_bVal
      .Page1.oPag.oFIDESFIL_1_4.enabled = i_bVal
      .Page1.oPag.oFIINDIRI_1_5.enabled = i_bVal
      .Page1.oPag.oFI___CAP_1_6.enabled = i_bVal
      .Page1.oPag.oFIDTOBSO_1_12.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oFICODABI_1_1.enabled = .f.
        .Page1.oPag.oFICODCAB_1_3.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oFICODABI_1_1.enabled = .t.
        .Page1.oPag.oFICODCAB_1_3.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'COD_CAB',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.COD_CAB_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FICODABI,"FICODABI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FICODCAB,"FICODCAB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FIDESFIL,"FIDESFIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FIINDIRI,"FIINDIRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FI___CAP,"FI___CAP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FIDTOBSO,"FIDTOBSO",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.COD_CAB_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.COD_CAB_IDX,2])
    i_lTable = "COD_CAB"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.COD_CAB_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    this.Calculate_FFNCSZAOBJ()
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.COD_CAB_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.COD_CAB_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.COD_CAB_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into COD_CAB
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'COD_CAB')
        i_extval=cp_InsertValODBCExtFlds(this,'COD_CAB')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(FICODABI,FICODCAB,FIDESFIL,FIINDIRI,FI___CAP"+;
                  ",FIDTOBSO "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_FICODABI)+;
                  ","+cp_ToStrODBC(this.w_FICODCAB)+;
                  ","+cp_ToStrODBC(this.w_FIDESFIL)+;
                  ","+cp_ToStrODBC(this.w_FIINDIRI)+;
                  ","+cp_ToStrODBC(this.w_FI___CAP)+;
                  ","+cp_ToStrODBC(this.w_FIDTOBSO)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'COD_CAB')
        i_extval=cp_InsertValVFPExtFlds(this,'COD_CAB')
        cp_CheckDeletedKey(i_cTable,0,'FICODABI',this.w_FICODABI,'FICODCAB',this.w_FICODCAB)
        INSERT INTO (i_cTable);
              (FICODABI,FICODCAB,FIDESFIL,FIINDIRI,FI___CAP,FIDTOBSO  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_FICODABI;
                  ,this.w_FICODCAB;
                  ,this.w_FIDESFIL;
                  ,this.w_FIINDIRI;
                  ,this.w_FI___CAP;
                  ,this.w_FIDTOBSO;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.Calculate_FFNCSZAOBJ()
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.COD_CAB_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.COD_CAB_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.COD_CAB_IDX,i_nConn)
      *
      * update COD_CAB
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'COD_CAB')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " FIDESFIL="+cp_ToStrODBC(this.w_FIDESFIL)+;
             ",FIINDIRI="+cp_ToStrODBC(this.w_FIINDIRI)+;
             ",FI___CAP="+cp_ToStrODBC(this.w_FI___CAP)+;
             ",FIDTOBSO="+cp_ToStrODBC(this.w_FIDTOBSO)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'COD_CAB')
        i_cWhere = cp_PKFox(i_cTable  ,'FICODABI',this.w_FICODABI  ,'FICODCAB',this.w_FICODCAB  )
        UPDATE (i_cTable) SET;
              FIDESFIL=this.w_FIDESFIL;
             ,FIINDIRI=this.w_FIINDIRI;
             ,FI___CAP=this.w_FI___CAP;
             ,FIDTOBSO=this.w_FIDTOBSO;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.COD_CAB_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.COD_CAB_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.COD_CAB_IDX,i_nConn)
      *
      * delete COD_CAB
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'FICODABI',this.w_FICODABI  ,'FICODCAB',this.w_FICODCAB  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.COD_CAB_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.COD_CAB_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,7,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_WTBSFWEPUN()
    with this
          * --- Gsar_be1 delete start
          gsar_be1(this;
              ,.w_FICODABI;
              ,.w_FICODCAB;
             )
    endwith
  endproc
  proc Calculate_FFNCSZAOBJ()
    with this
          * --- Dimensionamento CAP
          .w_FI___CAP = LEFT( .w_FI___CAP , IIF(g_APPLICATION = "ad hoc ENTERPRISE",9,8 ))
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Delete start")
          .Calculate_WTBSFWEPUN()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=FICODABI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_ABI_IDX,3]
    i_lTable = "COD_ABI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_ABI_IDX,2], .t., this.COD_ABI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_ABI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FICODABI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ABI',True,'COD_ABI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ABCODABI like "+cp_ToStrODBC(trim(this.w_FICODABI)+"%");

          i_ret=cp_SQL(i_nConn,"select ABCODABI,ABDESABI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ABCODABI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ABCODABI',trim(this.w_FICODABI))
          select ABCODABI,ABDESABI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ABCODABI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FICODABI)==trim(_Link_.ABCODABI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FICODABI) and !this.bDontReportError
            deferred_cp_zoom('COD_ABI','*','ABCODABI',cp_AbsName(oSource.parent,'oFICODABI_1_1'),i_cWhere,'GSAR_ABI',"CODICI ABI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ABCODABI,ABDESABI";
                     +" from "+i_cTable+" "+i_lTable+" where ABCODABI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ABCODABI',oSource.xKey(1))
            select ABCODABI,ABDESABI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FICODABI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ABCODABI,ABDESABI";
                   +" from "+i_cTable+" "+i_lTable+" where ABCODABI="+cp_ToStrODBC(this.w_FICODABI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ABCODABI',this.w_FICODABI)
            select ABCODABI,ABDESABI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FICODABI = NVL(_Link_.ABCODABI,space(5))
      this.w_DESABI = NVL(_Link_.ABDESABI,space(80))
    else
      if i_cCtrl<>'Load'
        this.w_FICODABI = space(5)
      endif
      this.w_DESABI = space(80)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_ABI_IDX,2])+'\'+cp_ToStr(_Link_.ABCODABI,1)
      cp_ShowWarn(i_cKey,this.COD_ABI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FICODABI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COD_ABI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COD_ABI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_1.ABCODABI as ABCODABI101"+ ",link_1_1.ABDESABI as ABDESABI101"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_1 on COD_CAB.FICODABI=link_1_1.ABCODABI"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_1"
          i_cKey=i_cKey+'+" and COD_CAB.FICODABI=link_1_1.ABCODABI(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFICODABI_1_1.value==this.w_FICODABI)
      this.oPgFrm.Page1.oPag.oFICODABI_1_1.value=this.w_FICODABI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESABI_1_2.value==this.w_DESABI)
      this.oPgFrm.Page1.oPag.oDESABI_1_2.value=this.w_DESABI
    endif
    if not(this.oPgFrm.Page1.oPag.oFICODCAB_1_3.value==this.w_FICODCAB)
      this.oPgFrm.Page1.oPag.oFICODCAB_1_3.value=this.w_FICODCAB
    endif
    if not(this.oPgFrm.Page1.oPag.oFIDESFIL_1_4.value==this.w_FIDESFIL)
      this.oPgFrm.Page1.oPag.oFIDESFIL_1_4.value=this.w_FIDESFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oFIINDIRI_1_5.value==this.w_FIINDIRI)
      this.oPgFrm.Page1.oPag.oFIINDIRI_1_5.value=this.w_FIINDIRI
    endif
    if not(this.oPgFrm.Page1.oPag.oFI___CAP_1_6.value==this.w_FI___CAP)
      this.oPgFrm.Page1.oPag.oFI___CAP_1_6.value=this.w_FI___CAP
    endif
    if not(this.oPgFrm.Page1.oPag.oFIDTOBSO_1_12.value==this.w_FIDTOBSO)
      this.oPgFrm.Page1.oPag.oFIDTOBSO_1_12.value=this.w_FIDTOBSO
    endif
    cp_SetControlsValueExtFlds(this,'COD_CAB')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_FICODABI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFICODABI_1_1.SetFocus()
            i_bnoObbl = !empty(.w_FICODABI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_FICODCAB))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFICODCAB_1_3.SetFocus()
            i_bnoObbl = !empty(.w_FICODCAB)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_afiPag1 as StdContainer
  Width  = 508
  height = 130
  stdWidth  = 508
  stdheight = 130
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oFICODABI_1_1 as StdField with uid="ONYXZBMBEG",rtseq=1,rtrep=.f.,;
    cFormVar = "w_FICODABI", cQueryName = "FICODABI",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice ABI della banca",;
    HelpContextID = 34211487,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=91, Top=12, cSayPict="'99999'", cGetPict="'99999'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_ABI", cZoomOnZoom="GSAR_ABI", oKey_1_1="ABCODABI", oKey_1_2="this.w_FICODABI"

  func oFICODABI_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oFICODABI_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFICODABI_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_ABI','*','ABCODABI',cp_AbsName(this.parent,'oFICODABI_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ABI',"CODICI ABI",'',this.parent.oContained
  endproc
  proc oFICODABI_1_1.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ABI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ABCODABI=this.parent.oContained.w_FICODABI
     i_obj.ecpSave()
  endproc

  add object oDESABI_1_2 as StdField with uid="SLHIOWXTJN",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DESABI", cQueryName = "DESABI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(80), bMultilanguage =  .f.,;
    HelpContextID = 165478966,;
   bGlobalFont=.t.,;
    Height=21, Width=345, Left=157, Top=12, InputMask=replicate('X',80)

  add object oFICODCAB_1_3 as StdField with uid="VVHMPSSKIP",rtseq=3,rtrep=.f.,;
    cFormVar = "w_FICODCAB", cQueryName = "FICODABI,FICODCAB",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice CAB della",;
    HelpContextID = 67765912,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=91, Top=42, cSayPict="'99999'", cGetPict="'99999'", InputMask=replicate('X',5)

  add object oFIDESFIL_1_4 as StdField with uid="EUEAVHEOUK",rtseq=4,rtrep=.f.,;
    cFormVar = "w_FIDESFIL", cQueryName = "FIDESFIL",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione completa dello sportello/filiale",;
    HelpContextID = 135260510,;
   bGlobalFont=.t.,;
    Height=21, Width=345, Left=157, Top=42, InputMask=replicate('X',40)

  add object oFIINDIRI_1_5 as StdField with uid="VGNSWFJEIP",rtseq=5,rtrep=.f.,;
    cFormVar = "w_FIINDIRI", cQueryName = "FIINDIRI",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo dello dello sportello/filiale",;
    HelpContextID = 168388255,;
   bGlobalFont=.t.,;
    Height=21, Width=411, Left=91, Top=72, InputMask=replicate('X',50)

  add object oFI___CAP_1_6 as StdField with uid="HVDHHMMNDB",rtseq=6,rtrep=.f.,;
    cFormVar = "w_FI___CAP", cQueryName = "FI___CAP",;
    bObbl = .f. , nPag = 1, value=space(9), bMultilanguage =  .f.,;
    ToolTipText = "CAP della localit� relativa allo sportello/filiale",;
    HelpContextID = 97240742,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=91, Top=102, InputMask=replicate('X',9), bHasZoom = .t. , InputMask=replicate('X', IIF(g_APPLICATION = "ad hoc ENTERPRISE",9,8 ) )

  proc oFI___CAP_1_6.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_FI___CAP")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oFIDTOBSO_1_12 as StdField with uid="QFRFCERXAV",rtseq=7,rtrep=.f.,;
    cFormVar = "w_FIDTOBSO", cQueryName = "FIDTOBSO",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Ultima data di validit�",;
    HelpContextID = 62854821,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=420, Top=102, tabstop=.f.

  add object oStr_1_7 as StdString with uid="NAYJITNFVT",Visible=.t., Left=6, Top=42,;
    Alignment=1, Width=80, Height=15,;
    Caption="Codice CAB:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_8 as StdString with uid="COUVDOQVDD",Visible=.t., Left=29, Top=72,;
    Alignment=1, Width=57, Height=15,;
    Caption="Indirizzo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="LPEYIWTSHW",Visible=.t., Left=59, Top=102,;
    Alignment=1, Width=27, Height=15,;
    Caption="CAP:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="IZKLTOYINQ",Visible=.t., Left=16, Top=12,;
    Alignment=1, Width=70, Height=15,;
    Caption="Codice ABI:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_13 as StdString with uid="CVMTTHKVWA",Visible=.t., Left=293, Top=102,;
    Alignment=1, Width=123, Height=15,;
    Caption="Data obsolescenza:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_afi','COD_CAB','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".FICODABI=COD_CAB.FICODABI";
  +" and "+i_cAliasName2+".FICODCAB=COD_CAB.FICODCAB";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
