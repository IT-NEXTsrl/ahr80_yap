* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bcl                                                        *
*              Batch calcola prov liquidate                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_63]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-12-14                                                      *
* Last revis.: 2008-02-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bcl",oParentObject)
return(i_retval)

define class tgsve_bcl as StdBatch
  * --- Local variables
  w_OLDCAOVAL = 0
  w_OLDCAOVAC = 0
  w_PLDATFIN = ctod("  /  /  ")
  w_DATI = .f.
  w_PLCODVAC = space(3)
  w_PLTOTIMP = 0
  w_PLCODVAL = space(3)
  w_PLDATREG = ctod("  /  /  ")
  w_PLCAOVAL = 0
  w_PLTOTZON = 0
  w_PL__ANNO = space(4)
  w_ROWNUM = 0
  w_OLDVALC = space(3)
  w_PLAGENTE = 0
  w_PLTOTPRO = 0
  w_SERIAL = space(10)
  w_PLNUMREG = 0
  w_CONFER = space(1)
  w_PLCAPO = 0
  w_OLDCAP = space(5)
  w_OLDAGE = space(5)
  w_DATSLIQ = ctod("  /  /  ")
  w_PASSO = .f.
  w_PLTOTIM2 = 0
  w_PLCODAGE = space(5)
  w_PLDATINI = ctod("  /  /  ")
  w_PLCODCAP = space(8)
  w_OLDVALU = space(3)
  w_PLRIFPRO = space(10)
  w_PLRIFAGE = space(5)
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_DATDOC = ctod("  /  /  ")
  w_CODVAL = space(3)
  w_CODAGE = space(5)
  w_TOTIMP = 0
  w_PERPRA = 0
  w_TOTAGE = 0
  w_DESAGE = space(35)
  w_FLSOSP = space(1)
  w_DECTOT = 0
  w_CODCON = space(15)
  w_DESCRI = space(40)
  w_DATMAT = ctod("  /  /  ")
  w_SERIALE = space(10)
  w_CPROWNUM = 0
  w_CODCAP = space(8)
  w_TOTZON = 0
  w_CAPDESC = space(40)
  w_CAPPERC = 0
  w_PROVCAP = 0
  w_TIPO = space(1)
  w_TIPAGE = space(1)
  * --- WorkFile variables
  MOP_DETT_idx=0
  PRO_LIQU_idx=0
  MOP_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato dal calcolo provvigioni liquidate (GSVE_KPL)
    * --- La variabile DATSLIQ viene utilizzata per poter visualizzare
    * --- nella maschera di aggiornamento la data di liquidazione
    * --- l'agente � sempre valorizzato nel cursore
    this.w_OLDCAOVAL = 0
    this.w_OLDCAOVAC = 0
    this.w_OLDAGE = ""
    this.w_OLDCAP = ""
    this.w_OLDVALU = ""
    this.w_OLDVALC = ""
    * --- Controllo che le date siano congruenti
    if this.oParentObject.w_DATA1>this.oParentObject.w_DATA2
      ah_ErrorMsg("Intervallo di date errato")
      i_retcode = 'stop'
      return
    endif
    L_AGEINI=this.oParentObject.w_AGEINI
    L_AGEFIN=this.oParentObject.w_AGEFIN
    L_STAMPATO=0
    L_VALUTA=this.oParentObject.w_VALUTA
    L_DATA1=this.oParentObject.w_DATA1
    L_DATA2=this.oParentObject.w_DATA2
    this.w_DATSLIQ = this.oParentObject.w_DATALIQ
    this.w_DATI = .T.
    this.w_PASSO = .T.
    CREATE CURSOR __TMP__ ; 
 (MPNUMDOC N(15), MPALFDOC C(10), MPDATDOC D(8), MPCODVAL C(3),MPCODAGE C(5), MPTOTIMP N(18,4),MPPERPRA N(5,2),; 
 MPTOTAGE N(18,4), AGDESAGE C(35), MPFLSOSP C(1), VADECTOT N(1), MPCODCON C(15), ANDESCRI C(40), MPDATMAT D(8), MPSERIAL C(10),; 
 CPROWNUM N(4), MPCODCAP C(5), MPTOTZON N(18,4), CAPDESC C(40), CAPPERC N(5,2),PROVCAP N(18,4),TIPO C(1),TIPAGE C(1))
    * --- Eseguo la query per la stampa 
    vq_exec("query\GSVE1KPL.vqr",this,"AGENTI")
    if RECCOUNT("AGENTI")=0
      this.w_DATI = .F.
      ah_ErrorMsg("Non ci sono dati da stampare")
    else
      vq_exec("query\GSVE3KPL.vqr",this,"CAPI")
      SELECT AGENTI
      GO TOP
      SCAN
      this.w_NUMDOC = NVL(MPNUMDOC,0)
      this.w_ALFDOC = NVL(MPALFDOC,SPACE(10))
      this.w_DATDOC = NVL(CP_TODATE(MPDATDOC), cp_CharToDate("  -  -  "))
      this.w_CODVAL = NVL(MPCODVAL,SPACE(3))
      this.w_CODAGE = NVL(MPCODAGE,SPACE(5))
      this.w_TOTIMP = NVL(MPTOTIMP,0)
      this.w_PERPRA = NVL(MPPERPRA ,0)
      this.w_TOTAGE = NVL(MPTOTAGE,0)
      this.w_DESAGE = NVL(AGDESAGE,SPACE(40))
      this.w_FLSOSP = NVL(MPFLSOSP," ")
      this.w_DECTOT = NVL(VADECTOT,0)
      this.w_CODCON = NVL(MPCODCON,SPACE(15))
      this.w_DESCRI = NVL(ANDESCRI,SPACE(40))
      this.w_DATMAT = NVL(CP_TODATE(MPDATMAT), cp_CharToDate("  -  -  "))
      this.w_SERIALE = NVL(MPSERIAL,SPACE(10))
      this.w_CPROWNUM = NVL(CPROWNUM,0)
      this.w_CODCAP = NVL(MPCODCAP,SPACE(8))
      this.w_TOTZON = NVL(MPTOTZON,0)
      this.w_TIPO = NVL(TIPO," ")
      this.w_TIPAGE = NVL(AGTIPAGE," ")
      this.w_CAPDESC = SPACE(40)
      this.w_CAPPERC = 0
      this.w_PROVCAP = 0
      SELECT CAPI
      GO TOP
      LOCATE FOR ALLTRIM(NVL(MPSERIAL, SPACE(10)))= this.w_SERIALE AND NVL(CPROWNUM,0)=this.w_CPROWNUM
      if  FOUND()
        this.w_CAPDESC = NVL(AGDESAGE,SPACE(40))
        this.w_CAPPERC = NVL(MPPERPRA,0)
        this.w_PROVCAP = NVL(MPTOTAGE,0)
      endif
      INSERT INTO __TMP__ ;
      (MPNUMDOC,MPALFDOC, MPDATDOC, MPCODVAL, MPCODAGE, MPTOTIMP, MPPERPRA,MPTOTAGE, AGDESAGE, MPFLSOSP,VADECTOT, ;
      MPCODCON, ANDESCRI, MPDATMAT, MPSERIAL, CPROWNUM, MPCODCAP,MPTOTZON,CAPDESC,CAPPERC,PROVCAP,TIPO,TIPAGE);
      VALUES (this.w_NUMDOC, this.w_ALFDOC, this.w_DATDOC, this.w_CODVAL, this.w_CODAGE, this.w_TOTIMP, this.w_PERPRA, this.w_TOTAGE,this.w_DESAGE,this.w_FLSOSP, this.w_DECTOT, ;
      this.w_CODCON,this.w_DESCRI,this.w_DATMAT,this.w_SERIALE,this.w_CPROWNUM,this.w_CODCAP,this.w_TOTZON,this.w_CAPDESC, this.w_CAPPERC,this.w_PROVCAP,this.w_TIPO,this.w_TIPAGE)
      SELECT AGENTI
      ENDSCAN
      * --- lancio il report della stampa per agenti
      CP_CHPRN("query\GSVE_BCL.frx", " ", this)
      if used("__tmp__")
        select __tmp__
        use
      endif
      if used("AGENTI")
        select AGENTI
        use
      endif
      if used("CAPI")
        select CAPI
        use
      endif
      * --- Eseguo la query per l'aggiornamento di PRO_LIQU
      vq_exec("query\GSVE_KPL.vqr",this,"__TMP__")
      if RECCOUNT("__TMP__")=0
        ah_ErrorMsg("Non ci sono provvigioni da liquidare in qualit� di agente")
        select __TMP__
        use
      else
        if L_STAMPATO=1 AND this.w_PASSO=.T.
          do GSVE_KP2 with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_PL__ANNO = STR(YEAR(this.oParentObject.w_DATALIQ),4,0)
          this.w_PLDATREG = this.oParentObject.w_DATALIQ
          this.w_PLDATINI = this.oParentObject.w_DATA1
          this.w_PLDATFIN = this.oParentObject.w_DATA2
          if this.w_confer = "S"
            * --- Try
            local bErr_040D8A80
            bErr_040D8A80=bTrsErr
            this.Try_040D8A80()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- rollback
              bTrsErr=.t.
              cp_EndTrs(.t.)
              ah_ErrorMsg(i_errmsg,,"")
            endif
            bTrsErr=bTrsErr or bErr_040D8A80
            * --- End
          endif
        endif
      endif
    endif
  endproc
  proc Try_040D8A80()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    select __TMP__
    go top
    scan
    * --- Try
    local bErr_040D9200
    bErr_040D9200=bTrsErr
    this.Try_040D9200()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Raise
      i_Error="Non � possibile effettuare l'aggiornamento dei mov. di provvigione"
      return
    endif
    bTrsErr=bTrsErr or bErr_040D9200
    * --- End
    select __TMP__
    this.w_PLTOTIMP = __TMP__.MPTOTIMP
    this.w_PLTOTPRO = __TMP__.MPTOTAGE
    this.w_PLCODAGE = __TMP__.MPCODAGE
    this.w_PLCODVAL = __TMP__.MPCODVAL
    this.w_PLCAOVAL = __TMP__.MPCAOVAL
    if this.w_PLTOTIMP<>0
      if this.w_OLDAGE<>this.w_PLCODAGE OR this.w_OLDVALU<>this.w_PLCODVAL OR this.w_OLDCAOVAL<>this.w_PLCAOVAL
        * --- Try
        local bErr_0420B230
        bErr_0420B230=bTrsErr
        this.Try_0420B230()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Write into PRO_LIQU
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PRO_LIQU_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PRO_LIQU_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PRO_LIQU_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PLTOTIMP =PLTOTIMP+ "+cp_ToStrODBC(this.w_PLTOTIMP);
            +",PLTOTPRO =PLTOTPRO+ "+cp_ToStrODBC(this.w_PLTOTPRO);
                +i_ccchkf ;
            +" where ";
                +"PLNUMREG = "+cp_ToStrODBC(this.w_PLAGENTE);
                +" and PL__ANNO = "+cp_ToStrODBC(this.w_PL__ANNO);
                   )
          else
            update (i_cTable) set;
                PLTOTIMP = PLTOTIMP + this.w_PLTOTIMP;
                ,PLTOTPRO = PLTOTPRO + this.w_PLTOTPRO;
                &i_ccchkf. ;
             where;
                PLNUMREG = this.w_PLAGENTE;
                and PL__ANNO = this.w_PL__ANNO;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_0420B230
        * --- End
        Wait Clear
      else
        * --- Write into PRO_LIQU
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PRO_LIQU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRO_LIQU_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PRO_LIQU_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PLTOTIMP =PLTOTIMP+ "+cp_ToStrODBC(this.w_PLTOTIMP);
          +",PLTOTPRO =PLTOTPRO+ "+cp_ToStrODBC(this.w_PLTOTPRO);
              +i_ccchkf ;
          +" where ";
              +"PLNUMREG = "+cp_ToStrODBC(this.w_PLAGENTE);
              +" and PL__ANNO = "+cp_ToStrODBC(this.w_PL__ANNO);
                 )
        else
          update (i_cTable) set;
              PLTOTIMP = PLTOTIMP + this.w_PLTOTIMP;
              ,PLTOTPRO = PLTOTPRO + this.w_PLTOTPRO;
              &i_ccchkf. ;
           where;
              PLNUMREG = this.w_PLAGENTE;
              and PL__ANNO = this.w_PL__ANNO;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      * --- Scrive riferimento Liquidazione per l'agente
      * --- Write into MOP_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MOP_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOP_DETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MOP_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MPLIQAGE ="+cp_NullLink(cp_ToStrODBC(this.w_PLAGENTE),'MOP_DETT','MPLIQAGE');
        +",MPANNLIQ ="+cp_NullLink(cp_ToStrODBC(this.w_PL__ANNO),'MOP_DETT','MPANNLIQ');
            +i_ccchkf ;
        +" where ";
            +"MPSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
               )
      else
        update (i_cTable) set;
            MPLIQAGE = this.w_PLAGENTE;
            ,MPANNLIQ = this.w_PL__ANNO;
            &i_ccchkf. ;
         where;
            MPSERIAL = this.w_SERIAL;
            and CPROWNUM = this.w_ROWNUM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    select __TMP__
    if NOT EMPTY(NVL(__TMP__.MPCODCAP,""))
      this.w_PLTOTIM2 = __TMP__.MPTOTIM2
      this.w_PLTOTZON = __TMP__.MPTOTZON
      this.w_PLCODCAP = __TMP__.MPCODCAP
      this.w_PLRIFAGE = NVL(__TMP__.MPCODAGE,SPACE(5))
      if this.w_PLTOTIM2<>0
        if this.w_OLDCAP<>this.w_PLCODCAP OR this.w_OLDVALC<>this.w_PLCODVAL OR this.w_OLDCAOVAC<>this.w_PLCAOVAL
          * --- Try
          local bErr_040DEC00
          bErr_040DEC00=bTrsErr
          this.Try_040DEC00()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            * --- Write into PRO_LIQU
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.PRO_LIQU_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PRO_LIQU_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.PRO_LIQU_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PLTOTIMP =PLTOTIMP+ "+cp_ToStrODBC(this.w_PLTOTIM2);
              +",PLTOTPRO =PLTOTPRO+ "+cp_ToStrODBC(this.w_PLTOTZON);
                  +i_ccchkf ;
              +" where ";
                  +"PLNUMREG = "+cp_ToStrODBC(this.w_PLCAPO);
                  +" and PL__ANNO = "+cp_ToStrODBC(this.w_PL__ANNO);
                     )
            else
              update (i_cTable) set;
                  PLTOTIMP = PLTOTIMP + this.w_PLTOTIM2;
                  ,PLTOTPRO = PLTOTPRO + this.w_PLTOTZON;
                  &i_ccchkf. ;
               where;
                  PLNUMREG = this.w_PLCAPO;
                  and PL__ANNO = this.w_PL__ANNO;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          bTrsErr=bTrsErr or bErr_040DEC00
          * --- End
          Wait Clear
        else
          * --- Write into PRO_LIQU
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PRO_LIQU_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PRO_LIQU_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PRO_LIQU_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PLTOTIMP =PLTOTIMP+ "+cp_ToStrODBC(this.w_PLTOTIM2);
            +",PLTOTPRO =PLTOTPRO+ "+cp_ToStrODBC(this.w_PLTOTZON);
                +i_ccchkf ;
            +" where ";
                +"PLNUMREG = "+cp_ToStrODBC(this.w_PLCAPO);
                +" and PL__ANNO = "+cp_ToStrODBC(this.w_PL__ANNO);
                   )
          else
            update (i_cTable) set;
                PLTOTIMP = PLTOTIMP + this.w_PLTOTIM2;
                ,PLTOTPRO = PLTOTPRO + this.w_PLTOTZON;
                &i_ccchkf. ;
             where;
                PLNUMREG = this.w_PLCAPO;
                and PL__ANNO = this.w_PL__ANNO;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        * --- Scrive riferimento Liquidazione per il Capo Area
        * --- Write into MOP_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.MOP_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MOP_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.MOP_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MPLIQCAP ="+cp_NullLink(cp_ToStrODBC(this.w_PLCAPO),'MOP_DETT','MPLIQCAP');
          +",MPANNLIQ ="+cp_NullLink(cp_ToStrODBC(this.w_PL__ANNO),'MOP_DETT','MPANNLIQ');
              +i_ccchkf ;
          +" where ";
              +"MPSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
                 )
        else
          update (i_cTable) set;
              MPLIQCAP = this.w_PLCAPO;
              ,MPANNLIQ = this.w_PL__ANNO;
              &i_ccchkf. ;
           where;
              MPSERIAL = this.w_SERIAL;
              and CPROWNUM = this.w_ROWNUM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    endif
    select __TMP__
    endscan
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_040D9200()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_SERIAL = __TMP__.MPSERIAL
    this.w_ROWNUM = __TMP__.CPROWNUM
    * --- Write into MOP_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MOP_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOP_DETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.MOP_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MPDATLIQ ="+cp_NullLink(cp_ToStrODBC(this.w_DATSLIQ),'MOP_DETT','MPDATLIQ');
          +i_ccchkf ;
      +" where ";
          +"MPSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
          +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
             )
    else
      update (i_cTable) set;
          MPDATLIQ = this.w_DATSLIQ;
          &i_ccchkf. ;
       where;
          MPSERIAL = this.w_SERIAL;
          and CPROWNUM = this.w_ROWNUM;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_0420B230()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_PLNUMREG = 0
    i_Conn=i_TableProp[this.PRO_LIQU_IDX, 3]
    cp_NextTableProg(this, i_Conn, "PRLIQ", "i_codazi,w_PL__ANNO,w_PLNUMREG")
    this.w_PLAGENTE = this.w_PLNUMREG
    * --- Insert into PRO_LIQU
    i_nConn=i_TableProp[this.PRO_LIQU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRO_LIQU_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRO_LIQU_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PLNUMREG"+",PL__ANNO"+",PLDATREG"+",PLDATINI"+",PLDATFIN"+",PLCODVAL"+",PLCODAGE"+",PLTOTIMP"+",PLTOTPRO"+",PLCAOVAL"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PLAGENTE),'PRO_LIQU','PLNUMREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PL__ANNO),'PRO_LIQU','PL__ANNO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PLDATREG),'PRO_LIQU','PLDATREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PLDATINI),'PRO_LIQU','PLDATINI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PLDATFIN),'PRO_LIQU','PLDATFIN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PLCODVAL),'PRO_LIQU','PLCODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PLCODAGE),'PRO_LIQU','PLCODAGE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PLTOTIMP),'PRO_LIQU','PLTOTIMP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PLTOTPRO),'PRO_LIQU','PLTOTPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PLCAOVAL),'PRO_LIQU','PLCAOVAL');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PLNUMREG',this.w_PLAGENTE,'PL__ANNO',this.w_PL__ANNO,'PLDATREG',this.w_PLDATREG,'PLDATINI',this.w_PLDATINI,'PLDATFIN',this.w_PLDATFIN,'PLCODVAL',this.w_PLCODVAL,'PLCODAGE',this.w_PLCODAGE,'PLTOTIMP',this.w_PLTOTIMP,'PLTOTPRO',this.w_PLTOTPRO,'PLCAOVAL',this.w_PLCAOVAL)
      insert into (i_cTable) (PLNUMREG,PL__ANNO,PLDATREG,PLDATINI,PLDATFIN,PLCODVAL,PLCODAGE,PLTOTIMP,PLTOTPRO,PLCAOVAL &i_ccchkf. );
         values (;
           this.w_PLAGENTE;
           ,this.w_PL__ANNO;
           ,this.w_PLDATREG;
           ,this.w_PLDATINI;
           ,this.w_PLDATFIN;
           ,this.w_PLCODVAL;
           ,this.w_PLCODAGE;
           ,this.w_PLTOTIMP;
           ,this.w_PLTOTPRO;
           ,this.w_PLCAOVAL;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.w_OLDAGE = this.w_PLCODAGE
    this.w_OLDVALU = this.w_PLCODVAL
    this.w_OLDCAOVAL = this.w_PLCAOVAL
    return
  proc Try_040DEC00()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_PLNUMREG = 0
    i_Conn=i_TableProp[this.PRO_LIQU_IDX, 3]
    cp_NextTableProg(this, i_Conn, "PRLIQ", "i_codazi,w_PL__ANNO,w_PLNUMREG")
    this.w_PLCAPO = this.w_PLNUMREG
    this.w_OLDCAP = this.w_PLCODCAP
    this.w_OLDVALC = this.w_PLCODVAL
    this.w_OLDCAOVAC = this.w_PLCAOVAL
    * --- Insert into PRO_LIQU
    i_nConn=i_TableProp[this.PRO_LIQU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRO_LIQU_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRO_LIQU_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PLNUMREG"+",PL__ANNO"+",PLDATREG"+",PLDATINI"+",PLDATFIN"+",PLCODVAL"+",PLCODAGE"+",PLTOTIMP"+",PLTOTPRO"+",PLCAOVAL"+",PLRIFAGE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PLCAPO),'PRO_LIQU','PLNUMREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PL__ANNO),'PRO_LIQU','PL__ANNO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PLDATREG),'PRO_LIQU','PLDATREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PLDATINI),'PRO_LIQU','PLDATINI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PLDATFIN),'PRO_LIQU','PLDATFIN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PLCODVAL),'PRO_LIQU','PLCODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PLCODCAP),'PRO_LIQU','PLCODAGE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PLTOTIM2),'PRO_LIQU','PLTOTIMP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PLTOTZON),'PRO_LIQU','PLTOTPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PLCAOVAL),'PRO_LIQU','PLCAOVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PLRIFAGE),'PRO_LIQU','PLRIFAGE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PLNUMREG',this.w_PLCAPO,'PL__ANNO',this.w_PL__ANNO,'PLDATREG',this.w_PLDATREG,'PLDATINI',this.w_PLDATINI,'PLDATFIN',this.w_PLDATFIN,'PLCODVAL',this.w_PLCODVAL,'PLCODAGE',this.w_PLCODCAP,'PLTOTIMP',this.w_PLTOTIM2,'PLTOTPRO',this.w_PLTOTZON,'PLCAOVAL',this.w_PLCAOVAL,'PLRIFAGE',this.w_PLRIFAGE)
      insert into (i_cTable) (PLNUMREG,PL__ANNO,PLDATREG,PLDATINI,PLDATFIN,PLCODVAL,PLCODAGE,PLTOTIMP,PLTOTPRO,PLCAOVAL,PLRIFAGE &i_ccchkf. );
         values (;
           this.w_PLCAPO;
           ,this.w_PL__ANNO;
           ,this.w_PLDATREG;
           ,this.w_PLDATINI;
           ,this.w_PLDATFIN;
           ,this.w_PLCODVAL;
           ,this.w_PLCODCAP;
           ,this.w_PLTOTIM2;
           ,this.w_PLTOTZON;
           ,this.w_PLCAOVAL;
           ,this.w_PLRIFAGE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='MOP_DETT'
    this.cWorkTables[2]='PRO_LIQU'
    this.cWorkTables[3]='MOP_MAST'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
