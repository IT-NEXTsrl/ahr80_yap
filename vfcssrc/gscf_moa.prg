* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscf_moa                                                        *
*              Operazioni aggiuntive regola                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-10-06                                                      *
* Last revis.: 2012-02-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgscf_moa")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgscf_moa")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgscf_moa")
  return

* --- Class definition
define class tgscf_moa as StdPCForm
  Width  = 785
  Height = 549
  Top    = 10
  Left   = 10
  cComment = "Operazioni aggiuntive regola"
  cPrg = "gscf_moa"
  HelpContextID=164246889
  add object cnt as tcgscf_moa
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgscf_moa as PCContext
  w_OASERIAL = space(10)
  w_CPROWORD = 0
  w_OACODUTE = 0
  w_OACODGRU = 0
  w_OAPOSTIN = space(1)
  w_OA__MAIL = space(1)
  w_OANAMUTE = space(20)
  w_OANAMGRU = space(20)
  w_OAOBJMSG = space(250)
  w_OACCADDR = space(10)
  w_OATXTMSG = space(10)
  w_OAPIEMSG = space(10)
  proc Save(i_oFrom)
    this.w_OASERIAL = i_oFrom.w_OASERIAL
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_OACODUTE = i_oFrom.w_OACODUTE
    this.w_OACODGRU = i_oFrom.w_OACODGRU
    this.w_OAPOSTIN = i_oFrom.w_OAPOSTIN
    this.w_OA__MAIL = i_oFrom.w_OA__MAIL
    this.w_OANAMUTE = i_oFrom.w_OANAMUTE
    this.w_OANAMGRU = i_oFrom.w_OANAMGRU
    this.w_OAOBJMSG = i_oFrom.w_OAOBJMSG
    this.w_OACCADDR = i_oFrom.w_OACCADDR
    this.w_OATXTMSG = i_oFrom.w_OATXTMSG
    this.w_OAPIEMSG = i_oFrom.w_OAPIEMSG
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_OASERIAL = this.w_OASERIAL
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_OACODUTE = this.w_OACODUTE
    i_oTo.w_OACODGRU = this.w_OACODGRU
    i_oTo.w_OAPOSTIN = this.w_OAPOSTIN
    i_oTo.w_OA__MAIL = this.w_OA__MAIL
    i_oTo.w_OANAMUTE = this.w_OANAMUTE
    i_oTo.w_OANAMGRU = this.w_OANAMGRU
    i_oTo.w_OAOBJMSG = this.w_OAOBJMSG
    i_oTo.w_OACCADDR = this.w_OACCADDR
    i_oTo.w_OATXTMSG = this.w_OATXTMSG
    i_oTo.w_OAPIEMSG = this.w_OAPIEMSG
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgscf_moa as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 785
  Height = 549
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-02-23"
  HelpContextID=164246889
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=12

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  OPAGGREG_IDX = 0
  cpusers_IDX = 0
  cpgroups_IDX = 0
  cFile = "OPAGGREG"
  cKeySelect = "OASERIAL"
  cKeyWhere  = "OASERIAL=this.w_OASERIAL"
  cKeyDetail  = "OASERIAL=this.w_OASERIAL"
  cKeyWhereODBC = '"OASERIAL="+cp_ToStrODBC(this.w_OASERIAL)';

  cKeyDetailWhereODBC = '"OASERIAL="+cp_ToStrODBC(this.w_OASERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"OPAGGREG.OASERIAL="+cp_ToStrODBC(this.w_OASERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'OPAGGREG.CPROWORD '
  cPrg = "gscf_moa"
  cComment = "Operazioni aggiuntive regola"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_OASERIAL = space(10)
  w_CPROWORD = 0
  w_OACODUTE = 0
  o_OACODUTE = 0
  w_OACODGRU = 0
  o_OACODGRU = 0
  w_OAPOSTIN = space(1)
  w_OA__MAIL = space(1)
  w_OANAMUTE = space(20)
  w_OANAMGRU = space(20)
  w_OAOBJMSG = space(250)
  w_OACCADDR = space(0)
  w_OATXTMSG = space(0)
  w_OAPIEMSG = space(0)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscf_moaPag1","gscf_moa",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='cpusers'
    this.cWorkTables[2]='cpgroups'
    this.cWorkTables[3]='OPAGGREG'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.OPAGGREG_IDX,5],7]
    this.nPostItConn=i_TableProp[this.OPAGGREG_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgscf_moa'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from OPAGGREG where OASERIAL=KeySet.OASERIAL
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.OPAGGREG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.OPAGGREG_IDX,2],this.bLoadRecFilter,this.OPAGGREG_IDX,"gscf_moa")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('OPAGGREG')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "OPAGGREG.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' OPAGGREG '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'OASERIAL',this.w_OASERIAL  )
      select * from (i_cTable) OPAGGREG where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_OASERIAL = NVL(OASERIAL,space(10))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'OPAGGREG')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_OANAMUTE = space(20)
          .w_OANAMGRU = space(20)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_OACODUTE = NVL(OACODUTE,0)
          .link_2_2('Load')
          .w_OACODGRU = NVL(OACODGRU,0)
          .link_2_3('Load')
          .w_OAPOSTIN = NVL(OAPOSTIN,space(1))
          .w_OA__MAIL = NVL(OA__MAIL,space(1))
          .w_OAOBJMSG = NVL(OAOBJMSG,space(250))
          .w_OACCADDR = NVL(OACCADDR,space(0))
          .w_OATXTMSG = NVL(OATXTMSG,space(0))
          .w_OAPIEMSG = NVL(OAPIEMSG,space(0))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_OASERIAL=space(10)
      .w_CPROWORD=10
      .w_OACODUTE=0
      .w_OACODGRU=0
      .w_OAPOSTIN=space(1)
      .w_OA__MAIL=space(1)
      .w_OANAMUTE=space(20)
      .w_OANAMGRU=space(20)
      .w_OAOBJMSG=space(250)
      .w_OACCADDR=space(0)
      .w_OATXTMSG=space(0)
      .w_OAPIEMSG=space(0)
      if .cFunction<>"Filter"
        .DoRTCalc(1,3,.f.)
        if not(empty(.w_OACODUTE))
         .link_2_2('Full')
        endif
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_OACODGRU))
         .link_2_3('Full')
        endif
        .w_OAPOSTIN = IIF( (EMPTY(.w_OACODGRU) AND EMPTY(.w_OACODUTE) ) OR EMPTY(.w_OAPOSTIN), 'N', .w_OAPOSTIN )
        .w_OA__MAIL = IIF( (EMPTY(.w_OACODGRU) AND EMPTY(.w_OACODUTE) ) OR EMPTY(.w_OA__MAIL), 'N', .w_OA__MAIL )
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'OPAGGREG')
    this.DoRTCalc(7,12,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oOAOBJMSG_2_8.enabled = i_bVal
      .Page1.oPag.oOACCADDR_2_9.enabled = i_bVal
      .Page1.oPag.oOATXTMSG_2_10.enabled = i_bVal
      .Page1.oPag.oOAPIEMSG_2_11.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'OPAGGREG',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.OPAGGREG_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OASERIAL,"OASERIAL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(6);
      ,t_OACODUTE N(4);
      ,t_OACODGRU N(4);
      ,t_OAPOSTIN N(3);
      ,t_OA__MAIL N(3);
      ,t_OANAMUTE C(20);
      ,t_OANAMGRU C(20);
      ,t_OAOBJMSG C(250);
      ,t_OACCADDR M(10);
      ,t_OATXTMSG M(10);
      ,t_OAPIEMSG M(10);
      ,CPROWNUM N(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgscf_moabodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oOACODUTE_2_2.controlsource=this.cTrsName+'.t_OACODUTE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oOACODGRU_2_3.controlsource=this.cTrsName+'.t_OACODGRU'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oOAPOSTIN_2_4.controlsource=this.cTrsName+'.t_OAPOSTIN'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oOA__MAIL_2_5.controlsource=this.cTrsName+'.t_OA__MAIL'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oOANAMUTE_2_6.controlsource=this.cTrsName+'.t_OANAMUTE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oOANAMGRU_2_7.controlsource=this.cTrsName+'.t_OANAMGRU'
    this.oPgFRm.Page1.oPag.oOAOBJMSG_2_8.controlsource=this.cTrsName+'.t_OAOBJMSG'
    this.oPgFRm.Page1.oPag.oOACCADDR_2_9.controlsource=this.cTrsName+'.t_OACCADDR'
    this.oPgFRm.Page1.oPag.oOATXTMSG_2_10.controlsource=this.cTrsName+'.t_OATXTMSG'
    this.oPgFRm.Page1.oPag.oOAPIEMSG_2_11.controlsource=this.cTrsName+'.t_OAPIEMSG'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(177)
    this.AddVLine(234)
    this.AddVLine(391)
    this.AddVLine(449)
    this.AddVLine(606)
    this.AddVLine(688)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.OPAGGREG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.OPAGGREG_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.OPAGGREG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.OPAGGREG_IDX,2])
      *
      * insert into OPAGGREG
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'OPAGGREG')
        i_extval=cp_InsertValODBCExtFlds(this,'OPAGGREG')
        i_cFldBody=" "+;
                  "(OASERIAL,CPROWORD,OACODUTE,OACODGRU,OAPOSTIN"+;
                  ",OA__MAIL,OAOBJMSG,OACCADDR,OATXTMSG,OAPIEMSG,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_OASERIAL)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBCNull(this.w_OACODUTE)+","+cp_ToStrODBCNull(this.w_OACODGRU)+","+cp_ToStrODBC(this.w_OAPOSTIN)+;
             ","+cp_ToStrODBC(this.w_OA__MAIL)+","+cp_ToStrODBC(this.w_OAOBJMSG)+","+cp_ToStrODBC(this.w_OACCADDR)+","+cp_ToStrODBC(this.w_OATXTMSG)+","+cp_ToStrODBC(this.w_OAPIEMSG)+;
             ","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'OPAGGREG')
        i_extval=cp_InsertValVFPExtFlds(this,'OPAGGREG')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'OASERIAL',this.w_OASERIAL)
        INSERT INTO (i_cTable) (;
                   OASERIAL;
                  ,CPROWORD;
                  ,OACODUTE;
                  ,OACODGRU;
                  ,OAPOSTIN;
                  ,OA__MAIL;
                  ,OAOBJMSG;
                  ,OACCADDR;
                  ,OATXTMSG;
                  ,OAPIEMSG;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_OASERIAL;
                  ,this.w_CPROWORD;
                  ,this.w_OACODUTE;
                  ,this.w_OACODGRU;
                  ,this.w_OAPOSTIN;
                  ,this.w_OA__MAIL;
                  ,this.w_OAOBJMSG;
                  ,this.w_OACCADDR;
                  ,this.w_OATXTMSG;
                  ,this.w_OAPIEMSG;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.OPAGGREG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.OPAGGREG_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_OACODUTE)) OR not(Empty(t_OACODGRU))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'OPAGGREG')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'OPAGGREG')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_OACODUTE)) OR not(Empty(t_OACODGRU))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update OPAGGREG
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'OPAGGREG')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",OACODUTE="+cp_ToStrODBCNull(this.w_OACODUTE)+;
                     ",OACODGRU="+cp_ToStrODBCNull(this.w_OACODGRU)+;
                     ",OAPOSTIN="+cp_ToStrODBC(this.w_OAPOSTIN)+;
                     ",OA__MAIL="+cp_ToStrODBC(this.w_OA__MAIL)+;
                     ",OAOBJMSG="+cp_ToStrODBC(this.w_OAOBJMSG)+;
                     ",OACCADDR="+cp_ToStrODBC(this.w_OACCADDR)+;
                     ",OATXTMSG="+cp_ToStrODBC(this.w_OATXTMSG)+;
                     ",OAPIEMSG="+cp_ToStrODBC(this.w_OAPIEMSG)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'OPAGGREG')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,OACODUTE=this.w_OACODUTE;
                     ,OACODGRU=this.w_OACODGRU;
                     ,OAPOSTIN=this.w_OAPOSTIN;
                     ,OA__MAIL=this.w_OA__MAIL;
                     ,OAOBJMSG=this.w_OAOBJMSG;
                     ,OACCADDR=this.w_OACCADDR;
                     ,OATXTMSG=this.w_OATXTMSG;
                     ,OAPIEMSG=this.w_OAPIEMSG;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.OPAGGREG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.OPAGGREG_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_OACODUTE)) OR not(Empty(t_OACODGRU))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete OPAGGREG
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_OACODUTE)) OR not(Empty(t_OACODGRU))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.OPAGGREG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.OPAGGREG_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
        if .o_OACODGRU<>.w_OACODGRU.or. .o_OACODUTE<>.w_OACODUTE
          .w_OAPOSTIN = IIF( (EMPTY(.w_OACODGRU) AND EMPTY(.w_OACODUTE) ) OR EMPTY(.w_OAPOSTIN), 'N', .w_OAPOSTIN )
        endif
        if .o_OACODGRU<>.w_OACODGRU.or. .o_OACODUTE<>.w_OACODUTE
          .w_OA__MAIL = IIF( (EMPTY(.w_OACODGRU) AND EMPTY(.w_OACODUTE) ) OR EMPTY(.w_OA__MAIL), 'N', .w_OA__MAIL )
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(7,12,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oOACODUTE_2_2.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oOACODUTE_2_2.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oOACODGRU_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oOACODGRU_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oOAPOSTIN_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oOAPOSTIN_2_4.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oOA__MAIL_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oOA__MAIL_2_5.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=OACODUTE
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OACODUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpusers')
        if i_nConn<>0
          i_cWhere = " code="+cp_ToStrODBC(this.w_OACODUTE);

          i_ret=cp_SQL(i_nConn,"select code,name";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'code',this.w_OACODUTE)
          select code,name;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_OACODUTE) and !this.bDontReportError
            deferred_cp_zoom('cpusers','*','code',cp_AbsName(oSource.parent,'oOACODUTE_2_2'),i_cWhere,'',"Utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                     +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',oSource.xKey(1))
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OACODUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_OACODUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_OACODUTE)
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OACODUTE = NVL(_Link_.code,0)
      this.w_OANAMUTE = NVL(_Link_.name,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_OACODUTE = 0
      endif
      this.w_OANAMUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OACODUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OACODGRU
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpgroups_IDX,3]
    i_lTable = "cpgroups"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpgroups_IDX,2], .t., this.cpgroups_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpgroups_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OACODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpgroups')
        if i_nConn<>0
          i_cWhere = " code="+cp_ToStrODBC(this.w_OACODGRU);

          i_ret=cp_SQL(i_nConn,"select code,name";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'code',this.w_OACODGRU)
          select code,name;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_OACODGRU) and !this.bDontReportError
            deferred_cp_zoom('cpgroups','*','code',cp_AbsName(oSource.parent,'oOACODGRU_2_3'),i_cWhere,'',"Gruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                     +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',oSource.xKey(1))
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OACODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_OACODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_OACODGRU)
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OACODGRU = NVL(_Link_.code,0)
      this.w_OANAMGRU = NVL(_Link_.name,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_OACODGRU = 0
      endif
      this.w_OANAMGRU = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpgroups_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.cpgroups_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OACODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oOAOBJMSG_2_8.value==this.w_OAOBJMSG)
      this.oPgFrm.Page1.oPag.oOAOBJMSG_2_8.value=this.w_OAOBJMSG
      replace t_OAOBJMSG with this.oPgFrm.Page1.oPag.oOAOBJMSG_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oOACCADDR_2_9.value==this.w_OACCADDR)
      this.oPgFrm.Page1.oPag.oOACCADDR_2_9.value=this.w_OACCADDR
      replace t_OACCADDR with this.oPgFrm.Page1.oPag.oOACCADDR_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oOATXTMSG_2_10.value==this.w_OATXTMSG)
      this.oPgFrm.Page1.oPag.oOATXTMSG_2_10.value=this.w_OATXTMSG
      replace t_OATXTMSG with this.oPgFrm.Page1.oPag.oOATXTMSG_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oOAPIEMSG_2_11.value==this.w_OAPIEMSG)
      this.oPgFrm.Page1.oPag.oOAPIEMSG_2_11.value=this.w_OAPIEMSG
      replace t_OAPIEMSG with this.oPgFrm.Page1.oPag.oOAPIEMSG_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOACODUTE_2_2.value==this.w_OACODUTE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOACODUTE_2_2.value=this.w_OACODUTE
      replace t_OACODUTE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOACODUTE_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOACODGRU_2_3.value==this.w_OACODGRU)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOACODGRU_2_3.value=this.w_OACODGRU
      replace t_OACODGRU with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOACODGRU_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOAPOSTIN_2_4.RadioValue()==this.w_OAPOSTIN)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOAPOSTIN_2_4.SetRadio()
      replace t_OAPOSTIN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOAPOSTIN_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOA__MAIL_2_5.RadioValue()==this.w_OA__MAIL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOA__MAIL_2_5.SetRadio()
      replace t_OA__MAIL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOA__MAIL_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOANAMUTE_2_6.value==this.w_OANAMUTE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOANAMUTE_2_6.value=this.w_OANAMUTE
      replace t_OANAMUTE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOANAMUTE_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOANAMGRU_2_7.value==this.w_OANAMGRU)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOANAMGRU_2_7.value=this.w_OANAMGRU
      replace t_OANAMGRU with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOANAMGRU_2_7.value
    endif
    cp_SetControlsValueExtFlds(this,'OPAGGREG')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_OACODUTE)) OR not(Empty(.w_OACODGRU))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_OACODUTE = this.w_OACODUTE
    this.o_OACODGRU = this.w_OACODGRU
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_OACODUTE)) OR not(Empty(t_OACODGRU)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(999999,cp_maxroword()+10)
      .w_OACODUTE=0
      .w_OACODGRU=0
      .w_OAPOSTIN=space(1)
      .w_OA__MAIL=space(1)
      .w_OANAMUTE=space(20)
      .w_OANAMGRU=space(20)
      .w_OAOBJMSG=space(250)
      .w_OACCADDR=space(0)
      .w_OATXTMSG=space(0)
      .w_OAPIEMSG=space(0)
      .DoRTCalc(1,3,.f.)
      if not(empty(.w_OACODUTE))
        .link_2_2('Full')
      endif
      .DoRTCalc(4,4,.f.)
      if not(empty(.w_OACODGRU))
        .link_2_3('Full')
      endif
        .w_OAPOSTIN = IIF( (EMPTY(.w_OACODGRU) AND EMPTY(.w_OACODUTE) ) OR EMPTY(.w_OAPOSTIN), 'N', .w_OAPOSTIN )
        .w_OA__MAIL = IIF( (EMPTY(.w_OACODGRU) AND EMPTY(.w_OACODUTE) ) OR EMPTY(.w_OA__MAIL), 'N', .w_OA__MAIL )
    endwith
    this.DoRTCalc(7,12,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_OACODUTE = t_OACODUTE
    this.w_OACODGRU = t_OACODGRU
    this.w_OAPOSTIN = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOAPOSTIN_2_4.RadioValue(.t.)
    this.w_OA__MAIL = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOA__MAIL_2_5.RadioValue(.t.)
    this.w_OANAMUTE = t_OANAMUTE
    this.w_OANAMGRU = t_OANAMGRU
    this.w_OAOBJMSG = t_OAOBJMSG
    this.w_OACCADDR = t_OACCADDR
    this.w_OATXTMSG = t_OATXTMSG
    this.w_OAPIEMSG = t_OAPIEMSG
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_OACODUTE with this.w_OACODUTE
    replace t_OACODGRU with this.w_OACODGRU
    replace t_OAPOSTIN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOAPOSTIN_2_4.ToRadio()
    replace t_OA__MAIL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOA__MAIL_2_5.ToRadio()
    replace t_OANAMUTE with this.w_OANAMUTE
    replace t_OANAMGRU with this.w_OANAMGRU
    replace t_OAOBJMSG with this.w_OAOBJMSG
    replace t_OACCADDR with this.w_OACCADDR
    replace t_OATXTMSG with this.w_OATXTMSG
    replace t_OAPIEMSG with this.w_OAPIEMSG
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgscf_moaPag1 as StdContainer
  Width  = 781
  height = 549
  stdWidth  = 781
  stdheight = 549
  resizeXpos=246
  resizeYpos=97
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=132, top=25, width=632,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=7,Field1="CPROWORD",Label1="Riga",Field2="OACODUTE",Label2="Utente",Field3="OANAMUTE",Label3="Nome",Field4="OACODGRU",Label4="Gruppo",Field5="OANAMGRU",Label5="Nome",Field6="OAPOSTIN",Label6="Notifica",Field7="OA__MAIL",Label7="Notifica",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 151843962

  add object oStr_1_3 as StdString with uid="ZMTQQGNNIN",Visible=.t., Left=20, Top=6,;
    Alignment=0, Width=203, Height=18,;
    Caption="Configurazione notifiche utenti/gruppi"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="DZPDQCRALK",Visible=.t., Left=11, Top=240,;
    Alignment=1, Width=119, Height=18,;
    Caption="Oggetto mail:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="REMRUNWZQB",Visible=.t., Left=11, Top=313,;
    Alignment=1, Width=119, Height=18,;
    Caption="Intestazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="SQYVPVDGNW",Visible=.t., Left=11, Top=429,;
    Alignment=1, Width=119, Height=18,;
    Caption="Piede:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="UOSNKKOPIP",Visible=.t., Left=11, Top=264,;
    Alignment=1, Width=119, Height=18,;
    Caption="Indirizzo mail per CC"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=124,top=44,;
    width=628+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=125,top=45,width=627+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='cpusers|cpgroups|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oOAOBJMSG_2_8.Refresh()
      this.Parent.oOACCADDR_2_9.Refresh()
      this.Parent.oOATXTMSG_2_10.Refresh()
      this.Parent.oOAPIEMSG_2_11.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='cpusers'
        oDropInto=this.oBodyCol.oRow.oOACODUTE_2_2
      case cFile='cpgroups'
        oDropInto=this.oBodyCol.oRow.oOACODGRU_2_3
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oOAOBJMSG_2_8 as StdTrsField with uid="RNRIKWULMM",rtseq=9,rtrep=.t.,;
    cFormVar="w_OAOBJMSG",value=space(250),;
    ToolTipText = "Oggetto e-mail (se vuoto: creato automaticamente da procedura)",;
    HelpContextID = 136118573,;
    cTotal="", bFixedPos=.t., cQueryName = "OAOBJMSG",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=630, Left=135, Top=239, InputMask=replicate('X',250)

  add object oOACCADDR_2_9 as StdTrsMemo with uid="WTSSEVLUJF",rtseq=10,rtrep=.t.,;
    cFormVar="w_OACCADDR",value=space(0),;
    HelpContextID = 244138296,;
    cTotal="", bFixedPos=.t., cQueryName = "OACCADDR",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=43, Width=630, Left=135, Top=264

  add object oOATXTMSG_2_10 as StdTrsMemo with uid="QCLHULRKLD",rtseq=11,rtrep=.t.,;
    cFormVar="w_OATXTMSG",value=space(0),;
    ToolTipText = "Testo del messaggio da aggiungere a quello generato automaticamente",;
    HelpContextID = 148066605,;
    cTotal="", bFixedPos=.t., cQueryName = "OATXTMSG",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=114, Width=630, Left=135, Top=312

  add object oOAPIEMSG_2_11 as StdTrsMemo with uid="DUDCOFLDNK",rtseq=12,rtrep=.t.,;
    cFormVar="w_OAPIEMSG",value=space(0),;
    ToolTipText = "Testo del messaggio da aggiungere a quello generato automaticamente",;
    HelpContextID = 131338541,;
    cTotal="", bFixedPos=.t., cQueryName = "OAPIEMSG",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=114, Width=630, Left=135, Top=429

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgscf_moaBodyRow as CPBodyRowCnt
  Width=618
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="PZQCWESBPE",rtseq=2,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 84263062,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=41, Left=-2, Top=0

  add object oOACODUTE_2_2 as StdTrsField with uid="XRNWIFCQOK",rtseq=3,rtrep=.t.,;
    cFormVar="w_OACODUTE",value=0,;
    HelpContextID = 264847659,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=43, Top=0, cSayPict=["9999"], cGetPict=["9999"], bHasZoom = .t. , cLinkFile="cpusers", oKey_1_1="code", oKey_1_2="this.w_OACODUTE"

  func oOACODUTE_2_2.mCond()
    with this.Parent.oContained
      return (EMPTY(.w_OACODGRU))
    endwith
  endfunc

  func oOACODUTE_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oOACODUTE_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oOACODUTE_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'cpusers','*','code',cp_AbsName(this.parent,'oOACODUTE_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Utenti",'',this.parent.oContained
  endproc

  add object oOACODGRU_2_3 as StdTrsField with uid="BPKGJQZRSZ",rtseq=4,rtrep=.t.,;
    cFormVar="w_OACODGRU",value=0,;
    HelpContextID = 238468805,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=258, Top=0, cSayPict=["9999"], cGetPict=["9999"], bHasZoom = .t. , cLinkFile="cpgroups", oKey_1_1="code", oKey_1_2="this.w_OACODGRU"

  func oOACODGRU_2_3.mCond()
    with this.Parent.oContained
      return (EMPTY(.w_OACODUTE))
    endwith
  endfunc

  func oOACODGRU_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oOACODGRU_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oOACODGRU_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'cpgroups','*','code',cp_AbsName(this.parent,'oOACODGRU_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi",'',this.parent.oContained
  endproc

  add object oOAPOSTIN_2_4 as StdTrsCheck with uid="MFUFKKUDAI",rtrep=.t.,;
    cFormVar="w_OAPOSTIN",  caption="Post-IN",;
    HelpContextID = 263852340,;
    Left=474, Top=0, Width=76,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oOAPOSTIN_2_4.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..OAPOSTIN,&i_cF..t_OAPOSTIN),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oOAPOSTIN_2_4.GetRadio()
    this.Parent.oContained.w_OAPOSTIN = this.RadioValue()
    return .t.
  endfunc

  func oOAPOSTIN_2_4.ToRadio()
    this.Parent.oContained.w_OAPOSTIN=trim(this.Parent.oContained.w_OAPOSTIN)
    return(;
      iif(this.Parent.oContained.w_OAPOSTIN=='S',1,;
      0))
  endfunc

  func oOAPOSTIN_2_4.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oOAPOSTIN_2_4.mCond()
    with this.Parent.oContained
      return (!EMPTY(.w_OACODGRU) OR !EMPTY(.w_OACODUTE))
    endwith
  endfunc

  add object oOA__MAIL_2_5 as StdTrsCheck with uid="PTTVGWPRDI",rtrep=.t.,;
    cFormVar="w_OA__MAIL",  caption="Mail",;
    HelpContextID = 208339250,;
    Left=555, Top=0, Width=58,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oOA__MAIL_2_5.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..OA__MAIL,&i_cF..t_OA__MAIL),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oOA__MAIL_2_5.GetRadio()
    this.Parent.oContained.w_OA__MAIL = this.RadioValue()
    return .t.
  endfunc

  func oOA__MAIL_2_5.ToRadio()
    this.Parent.oContained.w_OA__MAIL=trim(this.Parent.oContained.w_OA__MAIL)
    return(;
      iif(this.Parent.oContained.w_OA__MAIL=='S',1,;
      0))
  endfunc

  func oOA__MAIL_2_5.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oOA__MAIL_2_5.mCond()
    with this.Parent.oContained
      return (!EMPTY(.w_OACODGRU) OR !EMPTY(.w_OACODUTE))
    endwith
  endfunc

  add object oOANAMUTE_2_6 as StdTrsField with uid="WQKYBPFAYO",rtseq=7,rtrep=.t.,;
    cFormVar="w_OANAMUTE",value=space(20),enabled=.f.,;
    HelpContextID = 4976939,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=153, Left=99, Top=0, InputMask=replicate('X',20)

  add object oOANAMGRU_2_7 as StdTrsField with uid="BGUNGIOZVR",rtseq=8,rtrep=.t.,;
    cFormVar="w_OANAMGRU",value=space(20),enabled=.f.,;
    HelpContextID = 229904069,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=153, Left=314, Top=0, InputMask=replicate('X',20)
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscf_moa','OPAGGREG','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".OASERIAL=OPAGGREG.OASERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
