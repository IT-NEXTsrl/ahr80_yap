* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bda                                                        *
*              Eliminazione azienda                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_21]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-10                                                      *
* Last revis.: 2016-05-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bda",oParentObject)
return(i_retval)

define class tgsut_bda as StdBatch
  * --- Local variables
  w_OK = .f.
  w_MESS = space(10)
  w_OLDMODULES = space(0)
  w_ADDONMOD = space(4)
  w_TABNAME = space(35)
  w_ROWS = 0
  w_ROWS2 = 0
  w_ROWS3 = 0
  w_AZIENDA = space(5)
  w_FRASE = space(1)
  w_Filtro = space(50)
  w_Ret = space(1)
  w_conn = 0
  w_Numc = 0
  w_oMess = .NULL.
  w_oPart = .NULL.
  w_COMANDO = space(254)
  w_CONTA = 0
  w_NAME = space(35)
  w_OKCANC = .f.
  w_CONTCANC = 0
  * --- WorkFile variables
  AZIENDA_idx=0
  CONTROPA_idx=0
  ESERCIZI_idx=0
  FILDTREG_idx=0
  PAR_PROV_idx=0
  SEDIAZIE_idx=0
  TITOLARI_idx=0
  UTE_AZI_idx=0
  PAR_PARC_idx=0
  PAR_AGEN_idx=0
  PAR_PRAT_idx=0
  TAB_RITE_idx=0
  PAR_OFFE_idx=0
  PAR_ATTI_idx=0
  AZBACKUP_idx=0
  ADDON_idx=0
  PAR_ANTP_idx=0
  CAU_DOCU_idx=0
  RAPP_ANA_idx=0
  RAPPANAD_idx=0
  PRO_NUME_idx=0
  MAP_SINC_idx=0
  BUSIUNIT_idx=0
  PAR_INFO_idx=0
  PARA_EDS_idx=0
  DMPARAM_idx=0
  PES_RACC_idx=0
  NUMAUT_C_idx=0
  NUMAUT_D_idx=0
  NUMAUT_M_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimina Azienda (Lanciato da GSUT_KCA)
    if UPPER(ALLTRIM(this.oParentObject.w_CODAZI))=="XXX"
      ah_ErrorMsg("IMPOSSIBILE ELIMINARE AZIENDA MODELLO XXX",,"")
      i_retcode = 'stop'
      return
    endif
    * --- Oggetto per messaggi incrementali
    this.w_MESS = ""
    this.w_oMess=createobject("Ah_Message")
    this.w_AZIENDA = "'%"+ALLTRIM(this.oParentObject.w_CODAZI)+"%'"
    * --- Seleziona l'Azienda
    * --- Select from TABNOTXDC
    do vq_exec with 'TABNOTXDC',this,'_Curs_TABNOTXDC','',.f.,.t.
    if used('_Curs_TABNOTXDC')
      select _Curs_TABNOTXDC
      locate for 1=1
      do while not(eof())
      this.w_ROWS = 0
      this.w_ROWS2 = 0
      this.w_ROWS3 = 0
      this.w_TABNAME = ALLTRIM(this.oParentObject.w_CODAZI) +ALLTRIM(NVL(FILENAME,SPACE(35)))
      * --- Elimina gli eventuali constraint
      * --- Attivo una connessione utilizzando altra tabella perch� la tabella non appartiene al dizionario dati e quindi il metodo restituirebbe 0
      i_nConn=i_TableProp[this.AZIENDA_idx,3]
      if i_nConn<>0
        this.w_ROWS = cp_SQLExec(i_nConn,"select * from  "+ this.w_TABNAME)
        * --- Try
        local bErr_042E64F0
        bErr_042E64F0=bTrsErr
        this.Try_042E64F0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_042E64F0
        * --- End
      endif
        select _Curs_TABNOTXDC
        continue
      enddo
      use
    endif
    if upper(CP_DBTYPE)="SQLSERVER"
      * --- tali CONSTRAINT potrebbero impedire la cancellazione delle tabelle del dizionario dati se legati a tabelle non pi� presenti nel dizionario.
      this.w_ROWS = 0
      * --- Aggiungo all'XDC il nome della tabella di sistema SYSOBJECTS
      i_nIdx=cp_AddTableDef("SYSOBJECTS") && aggiunge la definizione nella lista delle tabelle
      i_TableProp[i_nIdx,2] = "SYSOBJECTS" && cambio il nome della tabella non � una tabella temporanea
      i_TableProp[i_nIdx,4]=1
      * --- Aggiungo all'XDC il nome della tabella di sistema SYSCOLUMNS
      i_nIdx=cp_AddTableDef("SYSCOLUMNS") && aggiunge la definizione nella lista delle tabelle
      i_TableProp[i_nIdx,2] = "SYSCOLUMNS" && cambio il nome della tabella non � una tabella temporanea
      i_TableProp[i_nIdx,4]=1
      * --- Se esiste la devo filtrare anche per l'azienda xxx
      * --- Select from GSCV1BDC
      do vq_exec with 'GSCV1BDC',this,'_Curs_GSCV1BDC','',.f.,.t.
      if used('_Curs_GSCV1BDC')
        select _Curs_GSCV1BDC
        locate for 1=1
        do while not(eof())
        * --- Try
        local bErr_042E8A10
        bErr_042E8A10=bTrsErr
        this.Try_042E8A10()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_042E8A10
        * --- End
          select _Curs_GSCV1BDC
          continue
        enddo
        use
      endif
      cp_RemoveTableDef("SYSOBJECTS")
      cp_RemoveTableDef("SYSCOLUMNS") 
 cp_CloseTable("SYSCOLUMNS") 
 cp_CloseTable("SYSOBJECTS")
    endif
    * --- memorizza il vecchio i_cmodules
    this.w_OLDMODULES = ALLTRIM(i_cModules)
    * --- * --- Setta i moduli da precaricare sempre...
     i_cModules ="VFCSIM,PCON" 
 do case 
 case IsAlt() 
 i_cModules = i_cModules + ",DISB,ALTE," 
 case IsAhr() 
 i_cModules = i_cModules + ",DISB," 
 endcase
    * --- rilegge un "finto" i_cmodules per cancellare anche le tabelle di moduli disattivati successivamente
    *     i_cModules "completo"
    * --- Select from ADDON
    i_nConn=i_TableProp[this.ADDON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ADDON_idx,2],.t.,this.ADDON_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select ADNOMMOD  from "+i_cTable+" ADDON ";
           ,"_Curs_ADDON")
    else
      select ADNOMMOD from (i_cTable);
        into cursor _Curs_ADDON
    endif
    if used('_Curs_ADDON')
      select _Curs_ADDON
      locate for 1=1
      do while not(eof())
      this.w_ADDONMOD = NVL(ADNOMMOD,SPACE(4) )
      if Not Empty( this.w_ADDONMOD ) And At(this.w_ADDONMOD, i_cModules)=0
        i_cModules = i_cModules + ALLTRIM(this.w_ADDONMOD)+","
      endif
        select _Curs_ADDON
        continue
      enddo
      use
    endif
    if Right(i_cModules, 1) = ","
      * --- tolgo la virgola finale
      i_cModules = LEFT(i_cModules, LEN(i_cModules)-1)
    endif
    this.w_AZIENDA = ALLTRIM(this.oParentObject.w_CODAZI)
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    this.w_COMANDO = ""
    do case
      case upper(CP_DBTYPE)="SQLSERVER"
        this.w_COMANDO = "select name as table_name from sys.tables where  name like "
      case upper(CP_DBTYPE)="ORACLE"
        this.w_COMANDO = "select table_name from user_tables where table_name like "
      case upper(CP_DBTYPE)="POSTGRESQL"
        this.w_COMANDO = "select table_name from information_schema.tables where table_type='BASE TABLE' and table_schema='public' and table_name ilike "
    endcase
    this.w_ROWS = cp_SQLExec(i_nConn,this.w_comando+cp_ToStrODBC(trim(this.w_AZIENDA)+"%") , "curscommand")
    * --- Seleziona l'Azienda
    cp_ReadXdc(.T.)
    this.w_OK = CP_DELETEAZI(this.oParentObject.w_CODAZI)
    USE IN SELECT("curscommand")
    * --- Riassegna l'i_cmodules corretto e rilegge il dizionario dati corretto
    i_cModules = this.w_OLDMODULES
    cp_ReadXdc(.T.)
    this.w_CONTCANC = 0
    this.w_OKCANC = .f.
    this.w_CONTA = 0
    do while this.w_CONTA<=20
      i_nConn=i_TableProp[this.AZIENDA_idx,3]
      do case
        case upper(CP_DBTYPE)="SQLSERVER"
          this.w_COMANDO = "select name as table_name from sys.tables where  len(name)>9 and name like "
        case upper(CP_DBTYPE)="ORACLE"
          this.w_COMANDO = "select table_name from user_tables where len(table_name)>9 and table_name like "
        case upper(CP_DBTYPE)="POSTGRESQL"
          this.w_COMANDO = "select table_name from information_schema.tables where char_length(table_name)>9 and table_type='BASE TABLE' and table_schema='public' and table_name ilike "
      endcase
      this.w_ROWS = cp_SQLExec(i_nConn,this.w_comando+cp_ToStrODBC(trim(this.w_AZIENDA)+"%") , "curscommand")
      if used("curscommand") and reccount("curscommand")>0 and this.w_CONTCANC=0
        this.w_CONTCANC = 1
        SELECT curscommand 
 GO TOP 
 SCAN
        this.w_NAME = ALLTRIM(this.w_NAME)+IIF(!EMPTY(NVL(TABLE_NAME, SPACE(35))),IIF(!EMPTY(this.w_NAME), ", ","")+ALLTRIM(NVL(TABLE_NAME, SPACE(35))),"")
        ENDSCAN
        this.w_OKCANC = ah_yesno("Ci sono tabelle che contengono il nome azienda che non sono state cancellate: %0%1%0Si vuole proseguire con la cancellazione di tali tabelle?","",this.w_NAME)
      endif
      if used("curscommand") and this.w_OKCANC
        if reccount("curscommand")>0
          this.w_CONTA = this.w_CONTA+1
        else
          this.w_CONTA = 21
        endif
        SELECT curscommand 
 GO TOP 
 SCAN
        this.w_NAME = ALLTRIM(NVL(TABLE_NAME, SPACE(35)))
        * --- Try
        local bErr_042EE380
        bErr_042EE380=bTrsErr
        this.Try_042EE380()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_042EE380
        * --- End
        ENDSCAN
        USE IN SELECT("curscommand")
      else
        this.w_CONTA = 21
      endif
    enddo
    if this.w_OK
      * --- Se OK; elimina Azienda da Archivi Comuni
      * --- Try
      local bErr_043524E8
      bErr_043524E8=bTrsErr
      this.Try_043524E8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_oMess.AddMsgPartNL("fildtreg - filtri data registrazione")     
      endif
      bTrsErr=bTrsErr or bErr_043524E8
      * --- End
      * --- Try
      local bErr_04355FF8
      bErr_04355FF8=bTrsErr
      this.Try_04355FF8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_oMess.AddMsgPartNL("Contropa - contropartite")     
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_04355FF8
      * --- End
      * --- Try
      local bErr_043521E8
      bErr_043521E8=bTrsErr
      this.Try_043521E8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_oMess.AddMsgPartNL("Sediazie - sedi azienda")     
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_043521E8
      * --- End
      * --- Try
      local bErr_04353AD8
      bErr_04353AD8=bTrsErr
      this.Try_04353AD8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_oMess.AddMsgPartNL("Titolari - titolari azienda")     
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_04353AD8
      * --- End
      * --- Try
      local bErr_043511C8
      bErr_043511C8=bTrsErr
      this.Try_043511C8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_oMess.AddMsgPartNL("Esercizi - esercizi azienda")     
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_043511C8
      * --- End
      * --- Try
      local bErr_04337140
      bErr_04337140=bTrsErr
      this.Try_04337140()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_oMess.AddMsgPartNL("Par_prov - parametri provvigioni")     
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_04337140
      * --- End
      * --- Try
      local bErr_04336750
      bErr_04336750=bTrsErr
      this.Try_04336750()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_oMess.AddMsgPartNL("Ute_azi - utenti / azienda")     
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_04336750
      * --- End
      * --- Try
      local bErr_04350628
      bErr_04350628=bTrsErr
      this.Try_04350628()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_oMess.AddMsgPartNL("Dati backup")     
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_04350628
      * --- End
      * --- Try
      local bErr_0435FDD8
      bErr_0435FDD8=bTrsErr
      this.Try_0435FDD8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_oMess.AddMsgPartNL("Azienda - dati azienda")     
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_0435FDD8
      * --- End
      * --- Try
      local bErr_04360078
      bErr_04360078=bTrsErr
      this.Try_04360078()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_oMess.AddMsgPartNL("Par_parc - parametri parcellazione")     
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_04360078
      * --- End
      if IsAlt()
        * --- Eliminazione tabelle PAR_ALTE e PAR_ANTI (presenti solo se installato il modulo Alte)
        GSUT_BD1(this,this.oParentObject.w_CODAZI, this.w_oMess)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Try
      local bErr_04360CA8
      bErr_04360CA8=bTrsErr
      this.Try_04360CA8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_oMess.AddMsgPartNL("Par_agen - parametri attivit�")     
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_04360CA8
      * --- End
      * --- Try
      local bErr_04351B28
      bErr_04351B28=bTrsErr
      this.Try_04351B28()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_oMess.AddMsgPartNL("Par_atti - altri parametri attivit�")     
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_04351B28
      * --- End
      * --- Try
      local bErr_043601F8
      bErr_043601F8=bTrsErr
      this.Try_043601F8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_oMess.AddMsgPartNL("Par_prat - parametri pratiche")     
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_043601F8
      * --- End
      * --- Try
      local bErr_043515E8
      bErr_043515E8=bTrsErr
      this.Try_043515E8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_oMess.AddMsgPartNL("Tab_rite - tabella ritenute")     
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_043515E8
      * --- End
      * --- Try
      local bErr_04351348
      bErr_04351348=bTrsErr
      this.Try_04351348()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_oMess.AddMsgPartNL("Par_offe - parametri offerte/nominativi")     
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_04351348
      * --- End
      * --- Try
      local bErr_04351318
      bErr_04351318=bTrsErr
      this.Try_04351318()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_oMess.AddMsgPartNL("Par_coat - parametri contratti")     
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_04351318
      * --- End
      * --- Try
      local bErr_04307128
      bErr_04307128=bTrsErr
      this.Try_04307128()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_oMess.AddMsgPartNL("Contbilc - contropartite contabili")     
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_04307128
      * --- End
      * --- Try
      local bErr_04307398
      bErr_04307398=bTrsErr
      this.Try_04307398()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_oMess.AddMsgPartNL("Par_cost - parametri costanti")     
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_04307398
      * --- End
      * --- Try
      local bErr_04308658
      bErr_04308658=bTrsErr
      this.Try_04308658()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_oMess.AddMsgPartNL("Par_cesp - parametri cespiti")     
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_04308658
      * --- End
      * --- Try
      local bErr_04309DF8
      bErr_04309DF8=bTrsErr
      this.Try_04309DF8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_oMess.AddMsgPartNL("Par_disb - parametri distinte")     
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_04309DF8
      * --- End
      * --- Try
      local bErr_0433BF10
      bErr_0433BF10=bTrsErr
      this.Try_0433BF10()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_oMess.AddMsgPartNL("Par_antp - parametri anticipazioni")     
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_0433BF10
      * --- End
      * --- Try
      local bErr_0433B9A0
      bErr_0433B9A0=bTrsErr
      this.Try_0433B9A0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_oMess.AddMsgPartNL("Cau_docu - causali documenti agenda")     
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_0433B9A0
      * --- End
      * --- Try
      local bErr_0433BAC0
      bErr_0433BAC0=bTrsErr
      this.Try_0433BAC0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_oMess.AddMsgPartNL("Rappanad - rapporti dettagliati")     
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_0433BAC0
      * --- End
      * --- Try
      local bErr_0433B7C0
      bErr_0433B7C0=bTrsErr
      this.Try_0433B7C0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_oMess.AddMsgPartNL("Rapp_ana - rapporti")     
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_0433B7C0
      * --- End
      * --- Try
      local bErr_0433B4F0
      bErr_0433B4F0=bTrsErr
      this.Try_0433B4F0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_oMess.AddMsgPartNL("Pro_nume - progressivi numerazioni")     
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_0433B4F0
      * --- End
      * --- Try
      local bErr_0433AF80
      bErr_0433AF80=bTrsErr
      this.Try_0433AF80()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_oMess.AddMsgPartNL("Titolari - titolari")     
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_0433AF80
      * --- End
      * --- Try
      local bErr_0433B2B0
      bErr_0433B2B0=bTrsErr
      this.Try_0433B2B0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_oMess.AddMsgPartNL("Map_sinc - mappatura campi per sincronizzazione")     
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_0433B2B0
      * --- End
      * --- Try
      local bErr_0433B1F0
      bErr_0433B1F0=bTrsErr
      this.Try_0433B1F0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_oMess.AddMsgPartNL("Busiunit - business unit")     
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_0433B1F0
      * --- End
      * --- Try
      local bErr_0433B040
      bErr_0433B040=bTrsErr
      this.Try_0433B040()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_oMess.AddMsgPartNL("Par_info - parametri infoLink")     
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_0433B040
      * --- End
      * --- Try
      local bErr_0433A920
      bErr_0433A920=bTrsErr
      this.Try_0433A920()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_oMess.AddMsgPartNL("Para_eds - parametri E.D.S. / Infinity D.M.S")     
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_0433A920
      * --- End
      * --- Try
      local bErr_0433ADA0
      bErr_0433ADA0=bTrsErr
      this.Try_0433ADA0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_oMess.AddMsgPartNL("Dmparam - parametri document management")     
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_0433ADA0
      * --- End
      * --- Try
      local bErr_0433A950
      bErr_0433A950=bTrsErr
      this.Try_0433A950()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_oMess.AddMsgPartNL("Pes_racc- Pesi regole acquisizione clienti CPZ")     
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_0433A950
      * --- End
      * --- Try
      local bErr_0433AC80
      bErr_0433AC80=bTrsErr
      this.Try_0433AC80()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_oMess.AddMsgPartNL("Numaut_c - Elenco campi tabella")     
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_0433AC80
      * --- End
      * --- Try
      local bErr_0433A620
      bErr_0433A620=bTrsErr
      this.Try_0433A620()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_oMess.AddMsgPartNL("Numaut_d - numerazione automatica detail")     
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_0433A620
      * --- End
      * --- Try
      local bErr_0433A410
      bErr_0433A410=bTrsErr
      this.Try_0433A410()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_oMess.AddMsgPartNL("Numaut_m - Numerazione automatica master")     
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_0433A410
      * --- End
      * --- Try
      local bErr_0433CF90
      bErr_0433CF90=bTrsErr
      this.Try_0433CF90()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_oMess.AddMsgPartNL("Stu_para - parametri trasferimento studio")     
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_0433CF90
      * --- End
      * --- Try
      local bErr_0433E3A0
      bErr_0433E3A0=bTrsErr
      this.Try_0433E3A0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_oMess.AddMsgPartNL("Paralore - parametri logistica remota")     
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_0433E3A0
      * --- End
      * --- Try
      local bErr_0433FC00
      bErr_0433FC00=bTrsErr
      this.Try_0433FC00()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_oMess.AddMsgPartNL("Par_cont - parametri contenzioso")     
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_0433FC00
      * --- End
      * --- Try
      local bErr_04330720
      bErr_04330720=bTrsErr
      this.Try_04330720()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_oMess.AddMsgPartNL("Par_vefa - parametri vendite funzioni avanzate")     
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_04330720
      * --- End
      * --- Try
      local bErr_04336F90
      bErr_04336F90=bTrsErr
      this.Try_04336F90()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_oMess.AddMsgPartNL("Antivapo - dettagli parametri operazioni non soggette")     
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_04336F90
      * --- End
      * --- Try
      local bErr_04335F70
      bErr_04335F70=bTrsErr
      this.Try_04335F70()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_oMess.AddMsgPartNL("Antivapi - dettagli parametri eccezioni codici IVA")     
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_04335F70
      * --- End
      * --- Try
      local bErr_04335730
      bErr_04335730=bTrsErr
      this.Try_04335730()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_oMess.AddMsgPartNL("Antivaps - dettagli parametri soggetti da non raggruppare")     
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_04335730
      * --- End
      * --- Try
      local bErr_04336900
      bErr_04336900=bTrsErr
      this.Try_04336900()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_oMess.AddMsgPartNL("Antivapc - Dettagli parametri eccezioni causali fatture")     
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_04336900
      * --- End
      * --- Try
      local bErr_04335D30
      bErr_04335D30=bTrsErr
      this.Try_04335D30()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_oMess.AddMsgPartNL("Antivapa - parametri antievasione IVA")     
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_04335D30
      * --- End
      * --- Cancello postit
      if this.oParentObject.w_DELPOSTIT="S"
        ah_msg("ELIMINO I POST-IN DELL'AZIENDA %1",.t.,.f.,.f.,this.oParentObject.w_CODAZI)
        GSUT1BDA(this,this.oParentObject.w_CODAZI)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      this.w_MESS = this.w_oMess.ComposeMessage()
      if EMPTY(this.w_MESS)
        ah_Msg("AZIENDA: %1 ELIMINATA",.T.,.F.,.F., this.oParentObject.w_CODAZI)
      else
        ah_ErrorMsg("Non � stato possibile eliminare l'azienda %1 dalle seguenti tabelle:%0%2%0%0Per queste � necessario procedere manualmente",,"", this.oParentObject.w_CODAZI, this.w_MESS )
      endif
      this.oParentObject.w_CODAZI = SPACE(5)
      this.oParentObject.w_RAGAZI = SPACE(40)
    else
      * --- Abbandona...
      ah_ErrorMsg("ALTRI UTENTI CONNESSI AL DATABASE, IMPOSSIBILE ELIMINARE L'AZIENDA",,"")
    endif
  endproc
  proc Try_042E64F0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if this.w_ROWS>0
      i_nConn=i_TableProp[this.AZIENDA_idx,3]
      this.w_ROWS2 = cp_SQLExec(i_nConn,"DROP TABLE "+ this.w_TABNAME )
    endif
    return
  proc Try_042E8A10()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_FRASE = ALLTRIM(_Curs_GSCV1BDC.FRASE)
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    if i_nConn>0
      this.w_Rows = CP_SQLEXEC(i_nConn, rtrim(this.w_FRASE))
    endif
    return
  proc Try_042EE380()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    if i_nConn<>0
      this.w_ROWS2 = cp_SQLExec(i_nConn,"DROP TABLE "+ this.w_NAME )
    endif
    return
  proc Try_043524E8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("ELIMINO: %1 DA TABELLA: FILDTREG",.T.,.F.,.F., this.oParentObject.w_CODAZI)
    * --- Delete from FILDTREG
    i_nConn=i_TableProp[this.FILDTREG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.FILDTREG_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"FLCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      delete from (i_cTable) where;
            FLCODAZI = this.oParentObject.w_CODAZI;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_04355FF8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("ELIMINO: %1 DA TABELLA: AZIENDA",.T.,.F.,.F., this.oParentObject.w_CODAZI )
    * --- Delete from CONTROPA
    i_nConn=i_TableProp[this.CONTROPA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"COCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      delete from (i_cTable) where;
            COCODAZI = this.oParentObject.w_CODAZI;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_043521E8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("ELIMINO: %1 DA TABELLA: SEDIAZIE",.T.,.F.,.F., this.oParentObject.w_CODAZI)
    * --- Delete from SEDIAZIE
    i_nConn=i_TableProp[this.SEDIAZIE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SEDIAZIE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"SECODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      delete from (i_cTable) where;
            SECODAZI = this.oParentObject.w_CODAZI;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_04353AD8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("ELIMINO: %1 DA TABELLA: TITOLARI",.T.,.F.,.F., this.oParentObject.w_CODAZI)
    * --- Delete from TITOLARI
    i_nConn=i_TableProp[this.TITOLARI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TITOLARI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"TTCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      delete from (i_cTable) where;
            TTCODAZI = this.oParentObject.w_CODAZI;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_043511C8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("ELIMINO: %1 DA TABELLA: ESERCIZI",.T.,.F.,.F., this.oParentObject.w_CODAZI)
    * --- Delete from ESERCIZI
    i_nConn=i_TableProp[this.ESERCIZI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"ESCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      delete from (i_cTable) where;
            ESCODAZI = this.oParentObject.w_CODAZI;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_04337140()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("ELIMINO: %1 DA TABELLA: PROVVIGIONI",.T.,.F.,.F., this.oParentObject.w_CODAZI)
    * --- Delete from PAR_PROV
    i_nConn=i_TableProp[this.PAR_PROV_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROV_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"PPCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      delete from (i_cTable) where;
            PPCODAZI = this.oParentObject.w_CODAZI;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_04336750()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("ELIMINO: %1 DA TABELLA: UTENTI/AZIENDA",.T.,.F.,.F., this.oParentObject.w_CODAZI)
    * --- Delete from UTE_AZI
    i_nConn=i_TableProp[this.UTE_AZI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.UTE_AZI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"UACODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      delete from (i_cTable) where;
            UACODAZI = this.oParentObject.w_CODAZI;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_04350628()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("ELIMINO: %1 TABELLA BACKUP",.T.,.F.,.F., this.oParentObject.w_CODAZI)
    * --- Delete from AZBACKUP
    i_nConn=i_TableProp[this.AZBACKUP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZBACKUP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      delete from (i_cTable) where;
            AZCODAZI = this.oParentObject.w_CODAZI;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_0435FDD8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("ELIMINO: %1 DA TABELLA: AZIENDA",.T.,.F.,.F., this.oParentObject.w_CODAZI)
    * --- Delete from AZIENDA
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      delete from (i_cTable) where;
            AZCODAZI = this.oParentObject.w_CODAZI;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_04360078()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("ELIMINO: %1 DA TABELLA: PAR_PARC",.T.,.F.,.F., this.oParentObject.w_CODAZI)
    * --- Delete from PAR_PARC
    i_nConn=i_TableProp[this.PAR_PARC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PARC_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"PRCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      delete from (i_cTable) where;
            PRCODAZI = this.oParentObject.w_CODAZI;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_04360CA8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("ELIMINO: %1 DA TABELLA: PAR_AGEN",.T.,.F.,.F., this.oParentObject.w_CODAZI)
    * --- Delete from PAR_AGEN
    i_nConn=i_TableProp[this.PAR_AGEN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_AGEN_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"PACODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      delete from (i_cTable) where;
            PACODAZI = this.oParentObject.w_CODAZI;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_04351B28()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("ELIMINO: %1 DA TABELLA: PAR_ATTI",.T.,.F.,.F., this.oParentObject.w_CODAZI)
    * --- Delete from PAR_ATTI
    i_nConn=i_TableProp[this.PAR_ATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_ATTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"PTCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      delete from (i_cTable) where;
            PTCODAZI = this.oParentObject.w_CODAZI;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_043601F8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("ELIMINO: %1 DA TABELLA: PAR_PRAT",.T.,.F.,.F., this.oParentObject.w_CODAZI)
    * --- Delete from PAR_PRAT
    i_nConn=i_TableProp[this.PAR_PRAT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PRAT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"PACODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      delete from (i_cTable) where;
            PACODAZI = this.oParentObject.w_CODAZI;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_043515E8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("ELIMINO: %1 DA TABELLA: TAB_RITE",.T.,.F.,.F., this.oParentObject.w_CODAZI)
    * --- Delete from TAB_RITE
    i_nConn=i_TableProp[this.TAB_RITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TAB_RITE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"TRCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      delete from (i_cTable) where;
            TRCODAZI = this.oParentObject.w_CODAZI;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_04351348()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("ELIMINO: %1 DA TABELLA: PAR_OFFE",.T.,.F.,.F., this.oParentObject.w_CODAZI)
    * --- Delete from PAR_OFFE
    i_nConn=i_TableProp[this.PAR_OFFE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_OFFE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"POCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      delete from (i_cTable) where;
            POCODAZI = this.oParentObject.w_CODAZI;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_04351318()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- obsoleta ma ancora in analisi
    ah_Msg("ELIMINO: %1 DA TABELLA: PAR_COAT",.T.,.F.,.F., this.oParentObject.w_CODAZI)
    this.w_conn = i_serverconn[1,2]
    this.w_Numc = SqlExec(this.w_conn,"SELECT * FROM PAR_COAT")
    if this.w_numc>0
      this.w_Filtro = "PCCODAZI = '"+trim(this.OPARENTOBJECT.w_CODAZI)+"'"
      this.w_Ret = DeleteTable("PAR_COAT","",.F.,.F.,this.w_Filtro)
    endif
    return
  proc Try_04307128()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("ELIMINO: %1 DA TABELLA: CONTBILC",.T.,.F.,.F., this.oParentObject.w_CODAZI)
    this.w_conn = i_serverconn[1,2]
    this.w_Numc = SqlExec(this.w_conn,"SELECT * FROM CONTBILC")
    if this.w_numc>0
      this.w_Filtro = "COCODAZI = '"+trim(this.OPARENTOBJECT.w_CODAZI)+"'"
      this.w_Ret = DeleteTable("CONTBILC","",.F.,.F.,this.w_Filtro)
    endif
    return
  proc Try_04307398()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("ELIMINO: %1 DA TABELLA: PAR_COST",.T.,.F.,.F., this.oParentObject.w_CODAZI)
    this.w_conn = i_serverconn[1,2]
    this.w_Numc = SqlExec(this.w_conn,"SELECT * FROM PAR_COST")
    if this.w_numc>0
      this.w_Filtro = "PCCODAZI = '"+trim(this.OPARENTOBJECT.w_CODAZI)+"'"
      this.w_Ret = DeleteTable("PAR_COST","",.F.,.F.,this.w_Filtro)
    endif
    return
  proc Try_04308658()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("ELIMINO: %1 DA TABELLA: PAR_CESP",.T.,.F.,.F., this.oParentObject.w_CODAZI)
    this.w_conn = i_serverconn[1,2]
    this.w_Numc = SqlExec(this.w_conn,"SELECT * FROM PAR_CESP")
    if this.w_numc>0
      this.w_Filtro = "PCCODAZI = '"+trim(this.OPARENTOBJECT.w_CODAZI)+"'"
      this.w_Ret = DeleteTable("PAR_CESP","",.F.,.F.,this.w_Filtro)
    endif
    return
  proc Try_04309DF8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("ELIMINO: %1 DA TABELLA: PAR_DISB",.T.,.F.,.F., this.oParentObject.w_CODAZI)
    this.w_conn = i_serverconn[1,2]
    this.w_Numc = SqlExec(this.w_conn,"SELECT * FROM PAR_DISB")
    if this.w_numc>0
      this.w_Filtro = "PDCODAZI = '"+trim(this.OPARENTOBJECT.w_CODAZI)+"'"
      this.w_Ret = DeleteTable("PAR_DISB","",.F.,.F.,this.w_Filtro)
    endif
    return
  proc Try_0433BF10()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("ELIMINO: %1 DA TABELLA: PAR_ANTP",.T.,.F.,.F., this.oParentObject.w_CODAZI)
    * --- Delete from PAR_ANTP
    i_nConn=i_TableProp[this.PAR_ANTP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_ANTP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"PACODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      delete from (i_cTable) where;
            PACODAZI = this.oParentObject.w_CODAZI;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_0433B9A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("ELIMINO: %1 DA TABELLA: CAU_DOCU",.T.,.F.,.F., this.oParentObject.w_CODAZI)
    * --- Delete from CAU_DOCU
    i_nConn=i_TableProp[this.CAU_DOCU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAU_DOCU_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"CDCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      delete from (i_cTable) where;
            CDCODAZI = this.oParentObject.w_CODAZI;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_0433BAC0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("ELIMINO: %1 DA TABELLA: RAPPANAD",.T.,.F.,.F., this.oParentObject.w_CODAZI)
    * --- Delete from RAPPANAD
    i_nConn=i_TableProp[this.RAPPANAD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RAPPANAD_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"ADCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      delete from (i_cTable) where;
            ADCODAZI = this.oParentObject.w_CODAZI;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_0433B7C0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("ELIMINO: %1 DA TABELLA: RAPP_ANA",.T.,.F.,.F., this.oParentObject.w_CODAZI)
    * --- Delete from RAPP_ANA
    i_nConn=i_TableProp[this.RAPP_ANA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RAPP_ANA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"DRCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      delete from (i_cTable) where;
            DRCODAZI = this.oParentObject.w_CODAZI;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_0433B4F0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("ELIMINO: %1 DA TABELLA: PRO_NUME",.T.,.F.,.F., this.oParentObject.w_CODAZI)
    * --- Delete from PRO_NUME
    i_nConn=i_TableProp[this.PRO_NUME_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRO_NUME_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"PRCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      delete from (i_cTable) where;
            PRCODAZI = this.oParentObject.w_CODAZI;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_0433AF80()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("ELIMINO: %1 DA TABELLA: TITOLARI",.T.,.F.,.F., this.oParentObject.w_CODAZI)
    * --- Delete from TITOLARI
    i_nConn=i_TableProp[this.TITOLARI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TITOLARI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"TTCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      delete from (i_cTable) where;
            TTCODAZI = this.oParentObject.w_CODAZI;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_0433B2B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("ELIMINO: %1 DA TABELLA: MAP_SINC",.T.,.F.,.F., this.oParentObject.w_CODAZI)
    * --- Delete from MAP_SINC
    i_nConn=i_TableProp[this.MAP_SINC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MAP_SINC_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"MCCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      delete from (i_cTable) where;
            MCCODAZI = this.oParentObject.w_CODAZI;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_0433B1F0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("ELIMINO: %1 DA TABELLA: BUSIUNIT",.T.,.F.,.F., this.oParentObject.w_CODAZI)
    * --- Delete from BUSIUNIT
    i_nConn=i_TableProp[this.BUSIUNIT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.BUSIUNIT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"BUCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      delete from (i_cTable) where;
            BUCODAZI = this.oParentObject.w_CODAZI;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_0433B040()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("ELIMINO: %1 DA TABELLA: PAR_INFO",.T.,.F.,.F., this.oParentObject.w_CODAZI)
    * --- Delete from PAR_INFO
    i_nConn=i_TableProp[this.PAR_INFO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_INFO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"INCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      delete from (i_cTable) where;
            INCODAZI = this.oParentObject.w_CODAZI;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_0433A920()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("ELIMINO: %1 DA TABELLA: PARA_EDS",.T.,.F.,.F., this.oParentObject.w_CODAZI)
    * --- Delete from PARA_EDS
    i_nConn=i_TableProp[this.PARA_EDS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PARA_EDS_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"CDCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      delete from (i_cTable) where;
            CDCODAZI = this.oParentObject.w_CODAZI;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_0433ADA0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("ELIMINO: %1 DA TABELLA: DMPARAM",.T.,.F.,.F., this.oParentObject.w_CODAZI)
    * --- Delete from DMPARAM
    i_nConn=i_TableProp[this.DMPARAM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DMPARAM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"PACODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      delete from (i_cTable) where;
            PACODAZI = this.oParentObject.w_CODAZI;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_0433A950()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("ELIMINO: %1 DA TABELLA: PES_RACC",.T.,.F.,.F., this.oParentObject.w_CODAZI)
    * --- Delete from PES_RACC
    i_nConn=i_TableProp[this.PES_RACC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PES_RACC_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"PRCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      delete from (i_cTable) where;
            PRCODAZI = this.oParentObject.w_CODAZI;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_0433AC80()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("ELIMINO: %1 DA TABELLA: NUMAUT_C",.T.,.F.,.F., this.oParentObject.w_CODAZI)
    * --- Delete from NUMAUT_C
    i_nConn=i_TableProp[this.NUMAUT_C_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.NUMAUT_C_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"NACODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      delete from (i_cTable) where;
            NACODAZI = this.oParentObject.w_CODAZI;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_0433A620()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("ELIMINO: %1 DA TABELLA: NUMAUT_D",.T.,.F.,.F., this.oParentObject.w_CODAZI)
    * --- Delete from NUMAUT_D
    i_nConn=i_TableProp[this.NUMAUT_D_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.NUMAUT_D_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"NACODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      delete from (i_cTable) where;
            NACODAZI = this.oParentObject.w_CODAZI;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_0433A410()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("ELIMINO: %1 DA TABELLA: NUMAUT_M",.T.,.F.,.F., this.oParentObject.w_CODAZI)
    * --- Delete from NUMAUT_M
    i_nConn=i_TableProp[this.NUMAUT_M_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.NUMAUT_M_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"NACODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      delete from (i_cTable) where;
            NACODAZI = this.oParentObject.w_CODAZI;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_0433CF90()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("ELIMINO: %1 DA TABELLA: STU_PARA",.T.,.F.,.F., this.oParentObject.w_CODAZI)
    this.w_conn = i_serverconn[1,2]
    this.w_Numc = SqlExec(this.w_conn,"SELECT * FROM STU_PARA")
    if this.w_numc>0
      this.w_Filtro = "LMCODAZI = '"+trim(this.OPARENTOBJECT.w_CODAZI)+"'"
      this.w_Ret = DeleteTable("STU_PARA","",.F.,.F.,this.w_Filtro)
    endif
    return
  proc Try_0433E3A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("ELIMINO: %1 DA TABELLA: PARALORE",.T.,.F.,.F., this.oParentObject.w_CODAZI)
    this.w_conn = i_serverconn[1,2]
    this.w_Numc = SqlExec(this.w_conn,"SELECT * FROM PARALORE")
    if this.w_numc>0
      this.w_Filtro = "PLCODAZI = '"+trim(this.OPARENTOBJECT.w_CODAZI)+"'"
      this.w_Ret = DeleteTable("PARALORE","",.F.,.F.,this.w_Filtro)
    endif
    return
  proc Try_0433FC00()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("ELIMINO: %1 DA TABELLA: PAR_CONT",.T.,.F.,.F., this.oParentObject.w_CODAZI)
    this.w_conn = i_serverconn[1,2]
    this.w_Numc = SqlExec(this.w_conn,"SELECT * FROM PAR_CONT")
    if this.w_numc>0
      this.w_Filtro = "PCCODAZI = '"+trim(this.OPARENTOBJECT.w_CODAZI)+"'"
      this.w_Ret = DeleteTable("PAR_CONT","",.F.,.F.,this.w_Filtro)
    endif
    return
  proc Try_04330720()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("ELIMINO: %1 DA TABELLA: PAR_VEFA",.T.,.F.,.F., this.oParentObject.w_CODAZI)
    this.w_conn = i_serverconn[1,2]
    this.w_Numc = SqlExec(this.w_conn,"SELECT * FROM PAR_VEFA")
    if this.w_numc>0
      this.w_Filtro = "PACODAZI = '"+trim(this.OPARENTOBJECT.w_CODAZI)+"'"
      this.w_Ret = DeleteTable("PAR_VEFA","",.F.,.F.,this.w_Filtro)
    endif
    return
  proc Try_04336F90()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("ELIMINO: %1 DA TABELLA: ANTIVAPO",.T.,.F.,.F., this.oParentObject.w_CODAZI)
    this.w_conn = i_serverconn[1,2]
    this.w_Numc = SqlExec(this.w_conn,"SELECT * FROM ANTIVAPO")
    if this.w_numc>0
      this.w_Filtro = "PACODAZI = '"+trim(this.OPARENTOBJECT.w_CODAZI)+"'"
      this.w_Ret = DeleteTable("ANTIVAPO","",.F.,.F.,this.w_Filtro)
    endif
    return
  proc Try_04335F70()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("ELIMINO: %1 DA TABELLA: ANTIVAPI",.T.,.F.,.F., this.oParentObject.w_CODAZI)
    this.w_conn = i_serverconn[1,2]
    this.w_Numc = SqlExec(this.w_conn,"SELECT * FROM ANTIVAPI")
    if this.w_numc>0
      this.w_Filtro = "PACODAZI = '"+trim(this.OPARENTOBJECT.w_CODAZI)+"'"
      this.w_Ret = DeleteTable("ANTIVAPI","",.F.,.F.,this.w_Filtro)
    endif
    return
  proc Try_04335730()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("ELIMINO: %1 DA TABELLA: ANTIVAPS",.T.,.F.,.F., this.oParentObject.w_CODAZI)
    this.w_conn = i_serverconn[1,2]
    this.w_Numc = SqlExec(this.w_conn,"SELECT * FROM ANTIVAPS")
    if this.w_numc>0
      this.w_Filtro = "PACODAZI = '"+trim(this.OPARENTOBJECT.w_CODAZI)+"'"
      this.w_Ret = DeleteTable("ANTIVAPS","",.F.,.F.,this.w_Filtro)
    endif
    return
  proc Try_04336900()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("ELIMINO: %1 DA TABELLA: ANTIVAPC",.T.,.F.,.F., this.oParentObject.w_CODAZI)
    this.w_conn = i_serverconn[1,2]
    this.w_Numc = SqlExec(this.w_conn,"SELECT * FROM ANTIVAPC")
    if this.w_numc>0
      this.w_Filtro = "PACODAZI = '"+trim(this.OPARENTOBJECT.w_CODAZI)+"'"
      this.w_Ret = DeleteTable("ANTIVAPC","",.F.,.F.,this.w_Filtro)
    endif
    return
  proc Try_04335D30()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("ELIMINO: %1 DA TABELLA: ANTIVAPA",.T.,.F.,.F., this.oParentObject.w_CODAZI)
    this.w_conn = i_serverconn[1,2]
    this.w_Numc = SqlExec(this.w_conn,"SELECT * FROM ANTIVAPA")
    if this.w_numc>0
      this.w_Filtro = "PACODAZI = '"+trim(this.OPARENTOBJECT.w_CODAZI)+"'"
      this.w_Ret = DeleteTable("ANTIVAPA","",.F.,.F.,this.w_Filtro)
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,30)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='CONTROPA'
    this.cWorkTables[3]='ESERCIZI'
    this.cWorkTables[4]='FILDTREG'
    this.cWorkTables[5]='PAR_PROV'
    this.cWorkTables[6]='SEDIAZIE'
    this.cWorkTables[7]='TITOLARI'
    this.cWorkTables[8]='UTE_AZI'
    this.cWorkTables[9]='PAR_PARC'
    this.cWorkTables[10]='PAR_AGEN'
    this.cWorkTables[11]='PAR_PRAT'
    this.cWorkTables[12]='TAB_RITE'
    this.cWorkTables[13]='PAR_OFFE'
    this.cWorkTables[14]='PAR_ATTI'
    this.cWorkTables[15]='AZBACKUP'
    this.cWorkTables[16]='ADDON'
    this.cWorkTables[17]='PAR_ANTP'
    this.cWorkTables[18]='CAU_DOCU'
    this.cWorkTables[19]='RAPP_ANA'
    this.cWorkTables[20]='RAPPANAD'
    this.cWorkTables[21]='PRO_NUME'
    this.cWorkTables[22]='MAP_SINC'
    this.cWorkTables[23]='BUSIUNIT'
    this.cWorkTables[24]='PAR_INFO'
    this.cWorkTables[25]='PARA_EDS'
    this.cWorkTables[26]='DMPARAM'
    this.cWorkTables[27]='PES_RACC'
    this.cWorkTables[28]='NUMAUT_C'
    this.cWorkTables[29]='NUMAUT_D'
    this.cWorkTables[30]='NUMAUT_M'
    return(this.OpenAllTables(30))

  proc CloseCursors()
    if used('_Curs_TABNOTXDC')
      use in _Curs_TABNOTXDC
    endif
    if used('_Curs_GSCV1BDC')
      use in _Curs_GSCV1BDC
    endif
    if used('_Curs_ADDON')
      use in _Curs_ADDON
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
