* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bwi                                                        *
*              Wizard integrazione Infinity D.M.S.                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-04-08                                                      *
* Last revis.: 2011-08-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bwi",oParentObject)
return(i_retval)

define class tgsut_bwi as StdBatch
  * --- Local variables
  w_CDINFURL = space(254)
  w_COPATHDO = space(254)
  w_CREATO = .f.
  w_CODSER = 0
  w_CDCODCLA = space(15)
  w_CDATTINF = space(15)
  w_CDTABKEY = space(15)
  w_CDDESATT = space(40)
  w_CDATTOPE = space(10)
  w_GSUT_KII = .NULL.
  w_GSAR1KGH = .NULL.
  w_GSUT_AUT = .NULL.
  w_MSGOPE = space(0)
  w_MSGOPE = space(0)
  w_GSUT_KFG = .NULL.
  * --- WorkFile variables
  PARA_EDS_idx=0
  PRODINDI_idx=0
  PRODCLAS_idx=0
  PROMCLAS_idx=0
  CONTROPA_idx=0
  PROMINDI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Procedura per configurare Infinity D.M.S. easy
    if this.oParentObject.w_INSTDLL="S"
      AddMsgNL("Fase 1: Installazione dll",this)
      * --- Installa dll
      do GSUT_BIL with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.oParentObject.w_CONFVAR="S"
      * --- parametri infinity dms
      AddMsgNL("Fase 2.1: Configurazione parametri Infinity",this)
      SET PROCEDURE TO GSUT_KII ADDITIVE
      this.w_GSUT_KII = createobject("w_tGSUT_KII",this)
      this.w_GSUT_KII = null
      RELEASE PROCEDURE GSUT_KII 
 i_curform=this.oParentObject
      * --- Path documenti
      AddMsgNL("Fase 2.2: Impostazione path documenti",this)
      SET PROCEDURE TO GSAR1KGH ADDITIVE
      this.w_GSAR1KGH = createobject("w_tGSAR1KGH",this)
      this.w_GSAR1KGH = null
      RELEASE PROCEDURE GSAR1KGH 
 i_curform=this.oParentObject
    endif
    if this.oParentObject.w_CONFUSER="S"
      AddMsgNL("Fase 3: Configurazione utenti",this)
      SET PROCEDURE TO GSUT_AUT ADDITIVE
      this.w_GSUT_AUT = createobject("w_tGSUT_AUT")
      this.w_GSUT_AUT = null
      RELEASE PROCEDURE GSUT_AUT 
 i_curform=this.oParentObject
    endif
    if NVL(this.oParentObject.w_CDWIZSEC, " ")<>"S"
      AddMsgNL("Fase 4: Import classi documentali come prima volta",this)
      this.w_MSGOPE = InfinitySyncroClass("IMP")
      if NOT EMPTY(this.w_MSGOPE)
        AddMsgNL("%1", this, this.w_MSGOPE)
      endif
      * --- Write into PARA_EDS
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PARA_EDS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PARA_EDS_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PARA_EDS_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CDWIZSEC ="+cp_NullLink(cp_ToStrODBC("S"),'PARA_EDS','CDWIZSEC');
            +i_ccchkf ;
        +" where ";
            +"CDCODAZI = "+cp_ToStrODBC(i_CODAZI);
               )
      else
        update (i_cTable) set;
            CDWIZSEC = "S";
            &i_ccchkf. ;
         where;
            CDCODAZI = i_CODAZI;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    if this.oParentObject.w_VALIDATECLASS="S"
      if NVL(this.oParentObject.w_CDWIZSEC, " ")<>"S"
        AddMsgNL("Fase 4.0: Trasformo tutte le classi per Infinity D.M.S.",this)
      else
        AddMsgNL("Fase 4: Trasformo tutte le classi per Infinity D.M.S.",this)
      endif
      * --- tutte le classi diventano per archiviazione infinity e pubblica su web
      * --- Write into PROMCLAS
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PROMCLAS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMCLAS_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CDMODALL ="+cp_NullLink(cp_ToStrODBC("I"),'PROMCLAS','CDMODALL');
        +",CDPUBWEB ="+cp_NullLink(cp_ToStrODBC("S"),'PROMCLAS','CDPUBWEB');
            +i_ccchkf ;
               )
      else
        update (i_cTable) set;
            CDMODALL = "I";
            ,CDPUBWEB = "S";
            &i_ccchkf. ;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      AddMsgNL("Fase 4.1: Calcolo attributi per Infinity D.M.S. per attributi legati a tabelle",this)
      this.w_CDATTOPE = cp_NewCCChk()
      * --- Select from gsut1bwi
      do vq_exec with 'gsut1bwi',this,'_Curs_gsut1bwi','',.f.,.t.
      if used('_Curs_gsut1bwi')
        select _Curs_gsut1bwi
        locate for 1=1
        do while not(eof())
        this.w_CDTABKEY = _Curs_gsut1bwi.CDTABKEY
        this.w_CDATTINF = ""
        this.w_CDDESATT = ""
        * --- Select from gsut1bwi1
        do vq_exec with 'gsut1bwi1',this,'_Curs_gsut1bwi1','',.f.,.t.
        if used('_Curs_gsut1bwi1')
          select _Curs_gsut1bwi1
          locate for 1=1
          do while not(eof())
          this.w_CDATTINF = _Curs_gsut1bwi1.CDATTINF
          this.w_CDDESATT = _Curs_gsut1bwi1.CDDESATT
            select _Curs_gsut1bwi1
            continue
          enddo
          use
        endif
        if EMPTY(NVL(this.w_CDATTINF,""))
          this.w_CDATTINF = _Curs_gsut1bwi.CDATTINF
          this.w_CDDESATT = _Curs_gsut1bwi.CDDESATT
          if UPPER(this.w_CDTABKEY)="CONTI" AND ("CLIENT"$UPPER(this.w_CDATTINF) OR "FORNITOR"$UPPER(this.w_CDATTINF))
            this.w_CDATTINF = "Intestatario"
            this.w_CDDESATT = "Intestatario"
          endif
          * --- Select from gsut1bwi2
          do vq_exec with 'gsut1bwi2',this,'_Curs_gsut1bwi2','',.f.,.t.
          if used('_Curs_gsut1bwi2')
            select _Curs_gsut1bwi2
            locate for 1=1
            do while not(eof())
            this.w_CDCODCLA = _Curs_gsut1bwi2.CDCODCLA
              select _Curs_gsut1bwi2
              continue
            enddo
            use
          endif
          if NOT EMPTY(NVL(this.w_CDCODCLA,""))
            this.w_CREATO = .F.
            this.w_CODSER = 0
            do while NOT this.w_CREATO
              this.w_CODSER = this.w_CODSER + 1
              this.w_CDATTINF = LEFT(this.w_CDATTINF+space(15), 15-len(alltrim(str(this.w_CODSER))))+alltrim(str(this.w_CODSER))
              * --- Select from gsut1bwi2
              do vq_exec with 'gsut1bwi2',this,'_Curs_gsut1bwi2','',.f.,.t.
              if used('_Curs_gsut1bwi2')
                select _Curs_gsut1bwi2
                locate for 1=1
                do while not(eof())
                this.w_CDCODCLA = _Curs_gsut1bwi2.CDCODCLA
                  select _Curs_gsut1bwi2
                  continue
                enddo
                use
              endif
              if EMPTY(NVL(this.w_CDCODCLA,""))
                this.w_CREATO = .T.
              endif
            enddo
          endif
        endif
        * --- Write into PRODCLAS
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PRODCLAS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRODCLAS_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PRODCLAS_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CDATTINF ="+cp_NullLink(cp_ToStrODBC(this.w_CDATTINF),'PRODCLAS','CDATTINF');
          +",CDDESATT ="+cp_NullLink(cp_ToStrODBC(this.w_CDDESATT),'PRODCLAS','CDDESATT');
          +",CDATTOPE ="+cp_NullLink(cp_ToStrODBC(this.w_CDATTOPE),'PRODCLAS','CDATTOPE');
              +i_ccchkf ;
          +" where ";
              +"CDCODCLA = "+cp_ToStrODBC(_Curs_gsut1bwi.CDCODCLA);
              +" and CPROWNUM = "+cp_ToStrODBC(_Curs_gsut1bwi.CPROWNUM);
                 )
        else
          update (i_cTable) set;
              CDATTINF = this.w_CDATTINF;
              ,CDDESATT = this.w_CDDESATT;
              ,CDATTOPE = this.w_CDATTOPE;
              &i_ccchkf. ;
           where;
              CDCODCLA = _Curs_gsut1bwi.CDCODCLA;
              and CPROWNUM = _Curs_gsut1bwi.CPROWNUM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
          select _Curs_gsut1bwi
          continue
        enddo
        use
      endif
      * --- aggiorno attributi riferiti a tabelle del DB impostando lo stesso codice attributo
      AddMsgNL("Fase 4.2: Calcolo attributi per Infinity D.M.S.",this)
      * --- Select from gsut2bwi
      do vq_exec with 'gsut2bwi',this,'_Curs_gsut2bwi','',.f.,.t.
      if used('_Curs_gsut2bwi')
        select _Curs_gsut2bwi
        locate for 1=1
        do while not(eof())
        this.w_CDATTINF = _Curs_gsut2bwi.CDATTINF
        this.w_CDDESATT = _Curs_gsut2bwi.CDDESATT
        this.w_CDCODCLA = space(15)
        * --- Select from gsut2bwi1
        do vq_exec with 'gsut2bwi1',this,'_Curs_gsut2bwi1','',.f.,.t.
        if used('_Curs_gsut2bwi1')
          select _Curs_gsut2bwi1
          locate for 1=1
          do while not(eof())
          this.w_CDCODCLA = _Curs_gsut2bwi1.CDCODCLA
          this.w_CDATTINF = _Curs_gsut2bwi1.CDATTINF
            select _Curs_gsut2bwi1
            continue
          enddo
          use
        endif
        if EMPTY(NVL(this.w_CDCODCLA,""))
          this.w_CDATTINF = _Curs_gsut2bwi.CDATTINF
          this.w_CDDESATT = _Curs_gsut2bwi.CDDESATT
          * --- Read from PRODCLAS
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PRODCLAS_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PRODCLAS_idx,2],.t.,this.PRODCLAS_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CDCODCLA"+;
              " from "+i_cTable+" PRODCLAS where ";
                  +"CDATTINF = "+cp_ToStrODBC(this.w_CDATTINF);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CDCODCLA;
              from (i_cTable) where;
                  CDATTINF = this.w_CDATTINF;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CDCODCLA = NVL(cp_ToDate(_read_.CDCODCLA),cp_NullValue(_read_.CDCODCLA))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if NOT EMPTY(NVL(this.w_CDCODCLA,""))
            this.w_CDATTINF = LEFT(this.w_CDDESATT, 15)
            * --- Read from PRODCLAS
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PRODCLAS_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PRODCLAS_idx,2],.t.,this.PRODCLAS_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CDCODCLA"+;
                " from "+i_cTable+" PRODCLAS where ";
                    +"CDATTINF = "+cp_ToStrODBC(this.w_CDATTINF);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CDCODCLA;
                from (i_cTable) where;
                    CDATTINF = this.w_CDATTINF;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CDCODCLA = NVL(cp_ToDate(_read_.CDCODCLA),cp_NullValue(_read_.CDCODCLA))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if NOT EMPTY(NVL(this.w_CDCODCLA,""))
              this.w_CREATO = .F.
              this.w_CODSER = 0
              do while NOT this.w_CREATO
                this.w_CODSER = this.w_CODSER + 1
                this.w_CDATTINF = LEFT(this.w_CDATTINF+space(15), 15-len(alltrim(str(this.w_CODSER))))+alltrim(str(this.w_CODSER))
                * --- Read from PRODCLAS
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.PRODCLAS_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PRODCLAS_idx,2],.t.,this.PRODCLAS_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "CDCODCLA"+;
                    " from "+i_cTable+" PRODCLAS where ";
                        +"CDATTINF = "+cp_ToStrODBC(this.w_CDATTINF);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    CDCODCLA;
                    from (i_cTable) where;
                        CDATTINF = this.w_CDATTINF;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_CDCODCLA = NVL(cp_ToDate(_read_.CDCODCLA),cp_NullValue(_read_.CDCODCLA))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                if EMPTY(this.w_CDCODCLA)
                  this.w_CREATO = .T.
                endif
              enddo
            endif
          endif
        endif
        * --- Write into PRODCLAS
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PRODCLAS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRODCLAS_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PRODCLAS_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CDATTINF ="+cp_NullLink(cp_ToStrODBC(this.w_CDATTINF),'PRODCLAS','CDATTINF');
          +",CDDESATT ="+cp_NullLink(cp_ToStrODBC(this.w_CDDESATT),'PRODCLAS','CDDESATT');
          +",CDATTOPE ="+cp_NullLink(cp_ToStrODBC(this.w_CDATTOPE),'PRODCLAS','CDATTOPE');
              +i_ccchkf ;
          +" where ";
              +"CDCODCLA = "+cp_ToStrODBC(_Curs_gsut2bwi.CDCODCLA);
              +" and CPROWNUM = "+cp_ToStrODBC(_Curs_gsut2bwi.CPROWNUM);
                 )
        else
          update (i_cTable) set;
              CDATTINF = this.w_CDATTINF;
              ,CDDESATT = this.w_CDDESATT;
              ,CDATTOPE = this.w_CDATTOPE;
              &i_ccchkf. ;
           where;
              CDCODCLA = _Curs_gsut2bwi.CDCODCLA;
              and CPROWNUM = _Curs_gsut2bwi.CPROWNUM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
          select _Curs_gsut2bwi
          continue
        enddo
        use
      endif
      * --- Apertura maschera verifica assegnamento codici attributi
      AddMsgNL("Fase 4.3: Verifica assegnamento codice attributo",this)
      do GSUT_KWB with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.oParentObject.w_EXPORTCLASS="S"
      AddMsgNL("Fase 5: Export classi documentali",this)
      this.w_MSGOPE = InfinitySyncroClass("EXP")
      if NOT EMPTY(this.w_MSGOPE)
        AddMsgNL("%1", this, this.w_MSGOPE)
      endif
    endif
    if this.oParentObject.w_IMPORTCLASS="S" AND NVL(this.oParentObject.w_CDWIZSEC, " ")="S"
      AddMsgNL("Fase 6: Import classi documentali",this)
      this.w_MSGOPE = InfinitySyncroClass("IMP")
      if NOT EMPTY(this.w_MSGOPE)
        AddMsgNL("%1", this, this.w_MSGOPE)
      endif
    endif
    if this.oParentObject.w_CONVERTIND="S"
      AddMsgNL("Fase 7.1: Invio indici documentali a Infinity D.M.S.",this)
      * --- Lancio maschera per gestione indici con disabilitate tutte le funzionalitą
      SET PROCEDURE TO GSUT_KFG ADDITIVE
      this.w_GSUT_KFG = createobject("w_tGSUT_KFG",this)
      this.w_GSUT_KFG = null
      RELEASE PROCEDURE GSUT_KFG 
 i_curform=this.oParentObject
    endif
    Ah_ErrorMsg("Elaborazione terminata")
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='PARA_EDS'
    this.cWorkTables[2]='PRODINDI'
    this.cWorkTables[3]='PRODCLAS'
    this.cWorkTables[4]='PROMCLAS'
    this.cWorkTables[5]='CONTROPA'
    this.cWorkTables[6]='PROMINDI'
    return(this.OpenAllTables(6))

  proc CloseCursors()
    if used('_Curs_gsut1bwi')
      use in _Curs_gsut1bwi
    endif
    if used('_Curs_gsut1bwi1')
      use in _Curs_gsut1bwi1
    endif
    if used('_Curs_gsut1bwi2')
      use in _Curs_gsut1bwi2
    endif
    if used('_Curs_gsut1bwi2')
      use in _Curs_gsut1bwi2
    endif
    if used('_Curs_gsut2bwi')
      use in _Curs_gsut2bwi
    endif
    if used('_Curs_gsut2bwi1')
      use in _Curs_gsut2bwi1
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- gsut_bwi
  enddefine
  
  define class w_tGSUT_KII as tGSUT_KII
  windowtype=1
  enddefine
  
  define class w_tgsut_kfg as tgsut_kfg
  windowtype=1
  enddefine
  
  define class w_tgsut_aut as tgsut_aut
  windowtype=1
  enddefine
  
  define class w_tGSAR1KGH as tGSAR1KGH
  windowtype=1
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
