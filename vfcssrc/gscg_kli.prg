* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_kli                                                        *
*              Elenco dichiarazioni                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-02-09                                                      *
* Last revis.: 2012-03-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_kli",oParentObject))

* --- Class definition
define class tgscg_kli as StdForm
  Top    = 59
  Left   = 107

  * --- Standard Properties
  Width  = 687
  Height = 381
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-03-20"
  HelpContextID=51763863
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Constant Properties
  _IDX = 0
  VOCIIVA_IDX = 0
  cPrg = "gscg_kli"
  cComment = "Elenco dichiarazioni"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_RIFDIC = space(10)
  w_ANNDIC = space(4)
  w_DATDIC = ctod('  /  /  ')
  w_NUMDIC = 0
  w_MVTIPCON = space(1)
  w_MVCODCON = space(10)
  w_PADRE = space(10)
  w_MVDATDOC = ctod('  /  /  ')
  w_SERDOC = space(3)
  w_ZOOM = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_kliPag1","gscg_kli",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOM = this.oPgFrm.Pages(1).oPag.ZOOM
    DoDefault()
    proc Destroy()
      this.w_ZOOM = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='VOCIIVA'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_RIFDIC=space(10)
      .w_ANNDIC=space(4)
      .w_DATDIC=ctod("  /  /  ")
      .w_NUMDIC=0
      .w_MVTIPCON=space(1)
      .w_MVCODCON=space(10)
      .w_PADRE=space(10)
      .w_MVDATDOC=ctod("  /  /  ")
      .w_SERDOC=space(3)
      .w_RIFDIC=oParentObject.w_RIFDIC
      .w_ANNDIC=oParentObject.w_ANNDIC
      .w_DATDIC=oParentObject.w_DATDIC
      .w_NUMDIC=oParentObject.w_NUMDIC
      .w_SERDOC=oParentObject.w_SERDOC
        .w_RIFDIC = .w_Zoom.getVar('DISERIAL')
      .oPgFrm.Page1.oPag.ZOOM.Calculate()
        .w_ANNDIC = .w_Zoom.getVar('DI__ANNO')
        .w_DATDIC = CP_TODATE(.w_Zoom.getVar('DIDATLET'))
      .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .w_NUMDIC = .w_Zoom.getVar('DINUMDOC')
        .w_MVTIPCON = this.oParentObject.w_TIPCON
        .w_MVCODCON = this.oParentObject.w_CODCON
        .w_PADRE = upper(this.oParentObject.class)
        .w_MVDATDOC = IIF(.w_PADRE = "TGSCG_KDT" OR .w_PADRE = "TGSCG_KAD", cp_CharToDate("  -  -  "), this.oParentObject.w_DOCFIN)
        .w_SERDOC = .w_Zoom.getVar('DISERDOC')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_RIFDIC=.w_RIFDIC
      .oParentObject.w_ANNDIC=.w_ANNDIC
      .oParentObject.w_DATDIC=.w_DATDIC
      .oParentObject.w_NUMDIC=.w_NUMDIC
      .oParentObject.w_SERDOC=.w_SERDOC
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- gscg_kli
    this.oparentobject.Notifyevent('CambiaDicinte')
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_RIFDIC = .w_Zoom.getVar('DISERIAL')
        .oPgFrm.Page1.oPag.ZOOM.Calculate()
            .w_ANNDIC = .w_Zoom.getVar('DI__ANNO')
            .w_DATDIC = CP_TODATE(.w_Zoom.getVar('DIDATLET'))
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
            .w_NUMDIC = .w_Zoom.getVar('DINUMDOC')
            .w_MVTIPCON = this.oParentObject.w_TIPCON
            .w_MVCODCON = this.oParentObject.w_CODCON
        .DoRTCalc(7,7,.t.)
            .w_MVDATDOC = IIF(.w_PADRE = "TGSCG_KDT" OR .w_PADRE = "TGSCG_KAD", cp_CharToDate("  -  -  "), this.oParentObject.w_DOCFIN)
            .w_SERDOC = .w_Zoom.getVar('DISERDOC')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOM.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOM.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_5.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgscg_kliPag1 as StdContainer
  Width  = 683
  height = 381
  stdWidth  = 683
  stdheight = 381
  resizeXpos=424
  resizeYpos=174
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZOOM as cp_zoombox with uid="VPVPECORZC",left=-3, top=2, width=686,height=317,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="DIC_INTE",cZoomFile="GSVE_KFA",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bRetriveAllRows=.f.,bQueryOnLoad=.t.,cMenuFile="",cZoomOnZoom="",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 229761510


  add object oObj_1_5 as cp_runprogram with uid="BGGJEIEVOT",left=-1, top=387, width=166,height=19,;
    caption='GSVE_BOO',;
   bGlobalFont=.t.,;
    prg="GSVE_BOO",;
    cEvent = "w_zoom selected",;
    nPag=1;
    , ToolTipText = "Chiude la maschera al doppio click sullo zoom";
    , HelpContextID = 189829813


  add object oBtn_1_12 as StdButton with uid="VMPFRPKBFV",left=575, top=328, width=48,height=45,;
    CpPicture="BMP\SAVE.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per salvare le modifiche";
    , HelpContextID = 51792614;
    , Caption='\<Salva';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_13 as StdButton with uid="MTXNONHMHL",left=627, top=328, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 59081286;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_kli','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
