* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_kwc                                                        *
*              Nuovo nominativo                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-06-01                                                      *
* Last revis.: 2014-10-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_kwc",oParentObject))

* --- Class definition
define class tgsar_kwc as StdForm
  Top    = 4
  Left   = 13

  * --- Standard Properties
  Width  = 754
  Height = 281+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-10-30"
  HelpContextID=32077673
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=36

  * --- Constant Properties
  _IDX = 0
  NAZIONI_IDX = 0
  cPrg = "gsar_kwc"
  cComment = "Nuovo nominativo"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_NOCODICE = space(15)
  w_NODESCRI = space(60)
  w_NOSOGGET = space(2)
  o_NOSOGGET = space(2)
  w_NOINDIRI = space(35)
  w_NOINDI_2 = space(35)
  w_NO___CAP = space(9)
  w_NO___CAP = space(8)
  w_NOLOCALI = space(30)
  w_NOLOCALI = space(30)
  w_NOPROVIN = space(2)
  w_NOPROVIN = space(2)
  w_NONAZION = space(3)
  w_NOTELEFO = space(18)
  w_NOTELFAX = space(18)
  w_NONUMCEL = space(18)
  w_NO_SKYPE = space(50)
  w_NO_EMAIL = space(254)
  w_NOINDWEB = space(254)
  w_NOTIPPER = space(1)
  w_CODCOM = space(4)
  w_ERR = .F.
  w_NOCOGNOM = space(50)
  w_NOSOGGET = space(2)
  w_NO__NOME = space(50)
  w_NOLOCNAS = space(30)
  w_NOPRONAS = space(2)
  w_NODATNAS = ctod('  /  /  ')
  w_NO_SESSO = space(1)
  w_NOCODFIS = space(16)
  o_NOCODFIS = space(16)
  w_NOPARIVA = space(12)
  w_NO__NOTE = space(0)
  w_CUR_STEP = 0
  w_TOT_STEP = 0
  w_OGGETTO = space(10)
  w_OGG_FIELD = space(10)
  w_NONATGIU = space(3)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_kwcPag1","gsar_kwc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(2).addobject("oPag","tgsar_kwcPag2","gsar_kwc",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Pag.2")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oNOCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='NAZIONI'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSAR_BWC(this,"SAVE")
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_NOCODICE=space(15)
      .w_NODESCRI=space(60)
      .w_NOSOGGET=space(2)
      .w_NOINDIRI=space(35)
      .w_NOINDI_2=space(35)
      .w_NO___CAP=space(9)
      .w_NO___CAP=space(8)
      .w_NOLOCALI=space(30)
      .w_NOLOCALI=space(30)
      .w_NOPROVIN=space(2)
      .w_NOPROVIN=space(2)
      .w_NONAZION=space(3)
      .w_NOTELEFO=space(18)
      .w_NOTELFAX=space(18)
      .w_NONUMCEL=space(18)
      .w_NO_SKYPE=space(50)
      .w_NO_EMAIL=space(254)
      .w_NOINDWEB=space(254)
      .w_NOTIPPER=space(1)
      .w_CODCOM=space(4)
      .w_ERR=.f.
      .w_NOCOGNOM=space(50)
      .w_NOSOGGET=space(2)
      .w_NO__NOME=space(50)
      .w_NOLOCNAS=space(30)
      .w_NOPRONAS=space(2)
      .w_NODATNAS=ctod("  /  /  ")
      .w_NO_SESSO=space(1)
      .w_NOCODFIS=space(16)
      .w_NOPARIVA=space(12)
      .w_NO__NOTE=space(0)
      .w_CUR_STEP=0
      .w_TOT_STEP=0
      .w_OGGETTO=space(10)
      .w_OGG_FIELD=space(10)
      .w_NONATGIU=space(3)
          .DoRTCalc(1,2,.f.)
        .w_NOSOGGET = iif(Isalt(),'PF','EN')
        .DoRTCalc(4,12,.f.)
        if not(empty(.w_NONAZION))
          .link_1_18('Full')
        endif
          .DoRTCalc(13,18,.f.)
        .w_NOTIPPER = 'A'
          .DoRTCalc(20,21,.f.)
        .w_NOCOGNOM = iif(.w_NOSOGGET='EN',' ',.w_NOCOGNOM)
        .w_NOSOGGET = 'EN'
        .w_NO__NOME = iif(.w_NOSOGGET='EN',' ',.w_NO__NOME)
          .DoRTCalc(25,27,.f.)
        .w_NO_SESSO = 'M'
          .DoRTCalc(29,31,.f.)
        .w_CUR_STEP = 1
        .w_TOT_STEP = this.oPGFRM.PageCount
        .w_OGGETTO = g_OMENU.OPARENTOBJECT
        .w_OGG_FIELD = g_OMENU.OCTRL
        .w_NONATGIU = IIF(.w_NOSOGGET='PF', 'PFI', '')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_39.enabled = this.oPgFrm.Page1.oPag.oBtn_1_39.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_23.enabled = this.oPgFrm.Page2.oPag.oBtn_2_23.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsar_kwc
    * Nascondo le tab
    IF i_cMenuTab="T" and i_VisualTheme<>-1 and Type('This.oTabMenu')='O'
       This.oTabMenu.Visible=.F.
       This.oTabMenu.Enabled=.F.
    ELSE
       This.oPGFRM.Tabs=.F.
       this.refresh()
    ENDIF
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        if .o_NOCODFIS<>.w_NOCODFIS
          .Calculate_XNLBETFNTC()
        endif
        .DoRTCalc(1,21,.t.)
        if .o_NOSOGGET<>.w_NOSOGGET
            .w_NOCOGNOM = iif(.w_NOSOGGET='EN',' ',.w_NOCOGNOM)
        endif
        .DoRTCalc(23,23,.t.)
        if .o_NOSOGGET<>.w_NOSOGGET
            .w_NO__NOME = iif(.w_NOSOGGET='EN',' ',.w_NO__NOME)
        endif
        if .o_NOSOGGET<>.w_NOSOGGET
          .Calculate_WWPJDQWIRN()
        endif
        .DoRTCalc(25,35,.t.)
        if .o_NOSOGGET<>.w_NOSOGGET
            .w_NONATGIU = IIF(.w_NOSOGGET='PF', 'PFI', '')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_XNLBETFNTC()
    with this
          * --- Cambio codice fiscale aggiornamento campi
          .w_ERR = cfanag(.w_NOCODFIS,'V')
          .w_NOSOGGET = iif(.w_ERR, .w_NOSOGGET , 'PF' )
          .w_NODATNAS = iif(.w_ERR, .w_NODATNAS, cfanag(.w_NOCODFIS,'D') )
          .w_NO_SESSO = iif(.w_ERR,  .w_NO_SESSO , cfanag(.w_NOCODFIS,'S'))
          .w_NOLOCNAS = iif(.w_ERR OR upper(.w_CODCOM)==upper(SUBSTR(.w_NOCODFIS,12,4)), .w_NOLOCNAS , cfanag(.w_NOCODFIS,'L',.w_NOLOCNAS,.w_NOPRONAS,.w_CODCOM))
          .w_NOPRONAS = iif(.w_ERR OR upper(.w_CODCOM)==upper(SUBSTR(.w_NOCODFIS,12,4)),  .w_NOPRONAS , cfanag(.w_NOCODFIS,'P',.w_NOLOCNAS,.w_NOPRONAS,.w_CODCOM))
          .w_CODCOM = iif(.w_ERR, .w_CODCOM , SUBSTR(.w_NOCODFIS,12,4))
    endwith
  endproc
  proc Calculate_KAJERGUIVD()
    with this
          * --- Inizializzazione campi
          GSAR_bwc(this;
              ,"INITFIELDS";
             )
    endwith
  endproc
  proc Calculate_WWPJDQWIRN()
    with this
          * --- Divide ragione sociale
          GSAR_BDR(this;
              ,'N';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oNODESCRI_1_3.enabled = this.oPgFrm.Page1.oPag.oNODESCRI_1_3.mCond()
    this.oPgFrm.Page2.oPag.oNOLOCNAS_2_9.enabled = this.oPgFrm.Page2.oPag.oNOLOCNAS_2_9.mCond()
    this.oPgFrm.Page2.oPag.oNOPRONAS_2_11.enabled = this.oPgFrm.Page2.oPag.oNOPRONAS_2_11.mCond()
    this.oPgFrm.Page2.oPag.oNODATNAS_2_13.enabled = this.oPgFrm.Page2.oPag.oNODATNAS_2_13.mCond()
    this.oPgFrm.Page2.oPag.oNO_SESSO_2_15.enabled = this.oPgFrm.Page2.oPag.oNO_SESSO_2_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_36.enabled = this.oPgFrm.Page1.oPag.oBtn_1_36.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_37.enabled = this.oPgFrm.Page1.oPag.oBtn_1_37.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_38.enabled = this.oPgFrm.Page1.oPag.oBtn_1_38.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_20.enabled = this.oPgFrm.Page2.oPag.oBtn_2_20.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_21.enabled = this.oPgFrm.Page2.oPag.oBtn_2_21.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_22.enabled = this.oPgFrm.Page2.oPag.oBtn_2_22.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    oField= this.oPgFrm.Page2.oPag.oNOCOGNOM_2_3
    cCShape='TTxOb'+m.oField.Name
    If PemStatus(oField.Parent, cCShape, 5) 
      oField.Parent.&cCShape..visible= oField.visible And oField.enabled And oField.CondObbl()
    EndIf
    oField= this.oPgFrm.Page2.oPag.oNO__NOME_2_6
    cCShape='TTxOb'+m.oField.Name
    If PemStatus(oField.Parent, cCShape, 5) 
      oField.Parent.&cCShape..visible= oField.visible And oField.enabled And oField.CondObbl()
    EndIf
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oNOSOGGET_1_4.visible=!this.oPgFrm.Page1.oPag.oNOSOGGET_1_4.mHide()
    this.oPgFrm.Page1.oPag.oNO___CAP_1_10.visible=!this.oPgFrm.Page1.oPag.oNO___CAP_1_10.mHide()
    this.oPgFrm.Page1.oPag.oNO___CAP_1_11.visible=!this.oPgFrm.Page1.oPag.oNO___CAP_1_11.mHide()
    this.oPgFrm.Page1.oPag.oNOLOCALI_1_12.visible=!this.oPgFrm.Page1.oPag.oNOLOCALI_1_12.mHide()
    this.oPgFrm.Page1.oPag.oNOLOCALI_1_13.visible=!this.oPgFrm.Page1.oPag.oNOLOCALI_1_13.mHide()
    this.oPgFrm.Page1.oPag.oNOPROVIN_1_15.visible=!this.oPgFrm.Page1.oPag.oNOPROVIN_1_15.mHide()
    this.oPgFrm.Page1.oPag.oNOPROVIN_1_16.visible=!this.oPgFrm.Page1.oPag.oNOPROVIN_1_16.mHide()
    this.oPgFrm.Page2.oPag.oNOSOGGET_2_4.visible=!this.oPgFrm.Page2.oPag.oNOSOGGET_2_4.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_38.visible=!this.oPgFrm.Page1.oPag.oBtn_1_38.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_22.visible=!this.oPgFrm.Page2.oPag.oBtn_2_22.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_45.visible=!this.oPgFrm.Page1.oPag.oStr_1_45.mHide()
    this.oPgFrm.Page2.oPag.oNONATGIU_2_26.visible=!this.oPgFrm.Page2.oPag.oNONATGIU_2_26.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_27.visible=!this.oPgFrm.Page2.oPag.oStr_2_27.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("AggiornaCF")
          .Calculate_XNLBETFNTC()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank")
          .Calculate_KAJERGUIVD()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=NONAZION
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NAZIONI_IDX,3]
    i_lTable = "NAZIONI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2], .t., this.NAZIONI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NONAZION) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'NAZIONI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NACODNAZ like "+cp_ToStrODBC(trim(this.w_NONAZION)+"%");

          i_ret=cp_SQL(i_nConn,"select NACODNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NACODNAZ","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NACODNAZ',trim(this.w_NONAZION))
          select NACODNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NACODNAZ into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NONAZION)==trim(_Link_.NACODNAZ) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NONAZION) and !this.bDontReportError
            deferred_cp_zoom('NAZIONI','*','NACODNAZ',cp_AbsName(oSource.parent,'oNONAZION_1_18'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',oSource.xKey(1))
            select NACODNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NONAZION)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(this.w_NONAZION);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',this.w_NONAZION)
            select NACODNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NONAZION = NVL(_Link_.NACODNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_NONAZION = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])+'\'+cp_ToStr(_Link_.NACODNAZ,1)
      cp_ShowWarn(i_cKey,this.NAZIONI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NONAZION Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oNOCODICE_1_1.value==this.w_NOCODICE)
      this.oPgFrm.Page1.oPag.oNOCODICE_1_1.value=this.w_NOCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oNODESCRI_1_3.value==this.w_NODESCRI)
      this.oPgFrm.Page1.oPag.oNODESCRI_1_3.value=this.w_NODESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oNOSOGGET_1_4.RadioValue()==this.w_NOSOGGET)
      this.oPgFrm.Page1.oPag.oNOSOGGET_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNOINDIRI_1_6.value==this.w_NOINDIRI)
      this.oPgFrm.Page1.oPag.oNOINDIRI_1_6.value=this.w_NOINDIRI
    endif
    if not(this.oPgFrm.Page1.oPag.oNOINDI_2_1_7.value==this.w_NOINDI_2)
      this.oPgFrm.Page1.oPag.oNOINDI_2_1_7.value=this.w_NOINDI_2
    endif
    if not(this.oPgFrm.Page1.oPag.oNO___CAP_1_10.value==this.w_NO___CAP)
      this.oPgFrm.Page1.oPag.oNO___CAP_1_10.value=this.w_NO___CAP
    endif
    if not(this.oPgFrm.Page1.oPag.oNO___CAP_1_11.value==this.w_NO___CAP)
      this.oPgFrm.Page1.oPag.oNO___CAP_1_11.value=this.w_NO___CAP
    endif
    if not(this.oPgFrm.Page1.oPag.oNOLOCALI_1_12.value==this.w_NOLOCALI)
      this.oPgFrm.Page1.oPag.oNOLOCALI_1_12.value=this.w_NOLOCALI
    endif
    if not(this.oPgFrm.Page1.oPag.oNOLOCALI_1_13.value==this.w_NOLOCALI)
      this.oPgFrm.Page1.oPag.oNOLOCALI_1_13.value=this.w_NOLOCALI
    endif
    if not(this.oPgFrm.Page1.oPag.oNOPROVIN_1_15.value==this.w_NOPROVIN)
      this.oPgFrm.Page1.oPag.oNOPROVIN_1_15.value=this.w_NOPROVIN
    endif
    if not(this.oPgFrm.Page1.oPag.oNOPROVIN_1_16.value==this.w_NOPROVIN)
      this.oPgFrm.Page1.oPag.oNOPROVIN_1_16.value=this.w_NOPROVIN
    endif
    if not(this.oPgFrm.Page1.oPag.oNONAZION_1_18.value==this.w_NONAZION)
      this.oPgFrm.Page1.oPag.oNONAZION_1_18.value=this.w_NONAZION
    endif
    if not(this.oPgFrm.Page1.oPag.oNOTELEFO_1_19.value==this.w_NOTELEFO)
      this.oPgFrm.Page1.oPag.oNOTELEFO_1_19.value=this.w_NOTELEFO
    endif
    if not(this.oPgFrm.Page1.oPag.oNOTELFAX_1_21.value==this.w_NOTELFAX)
      this.oPgFrm.Page1.oPag.oNOTELFAX_1_21.value=this.w_NOTELFAX
    endif
    if not(this.oPgFrm.Page1.oPag.oNONUMCEL_1_24.value==this.w_NONUMCEL)
      this.oPgFrm.Page1.oPag.oNONUMCEL_1_24.value=this.w_NONUMCEL
    endif
    if not(this.oPgFrm.Page1.oPag.oNO_SKYPE_1_25.value==this.w_NO_SKYPE)
      this.oPgFrm.Page1.oPag.oNO_SKYPE_1_25.value=this.w_NO_SKYPE
    endif
    if not(this.oPgFrm.Page1.oPag.oNO_EMAIL_1_26.value==this.w_NO_EMAIL)
      this.oPgFrm.Page1.oPag.oNO_EMAIL_1_26.value=this.w_NO_EMAIL
    endif
    if not(this.oPgFrm.Page1.oPag.oNOINDWEB_1_28.value==this.w_NOINDWEB)
      this.oPgFrm.Page1.oPag.oNOINDWEB_1_28.value=this.w_NOINDWEB
    endif
    if not(this.oPgFrm.Page2.oPag.oNOCOGNOM_2_3.value==this.w_NOCOGNOM)
      this.oPgFrm.Page2.oPag.oNOCOGNOM_2_3.value=this.w_NOCOGNOM
    endif
    if not(this.oPgFrm.Page2.oPag.oNOSOGGET_2_4.RadioValue()==this.w_NOSOGGET)
      this.oPgFrm.Page2.oPag.oNOSOGGET_2_4.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNO__NOME_2_6.value==this.w_NO__NOME)
      this.oPgFrm.Page2.oPag.oNO__NOME_2_6.value=this.w_NO__NOME
    endif
    if not(this.oPgFrm.Page2.oPag.oNOLOCNAS_2_9.value==this.w_NOLOCNAS)
      this.oPgFrm.Page2.oPag.oNOLOCNAS_2_9.value=this.w_NOLOCNAS
    endif
    if not(this.oPgFrm.Page2.oPag.oNOPRONAS_2_11.value==this.w_NOPRONAS)
      this.oPgFrm.Page2.oPag.oNOPRONAS_2_11.value=this.w_NOPRONAS
    endif
    if not(this.oPgFrm.Page2.oPag.oNODATNAS_2_13.value==this.w_NODATNAS)
      this.oPgFrm.Page2.oPag.oNODATNAS_2_13.value=this.w_NODATNAS
    endif
    if not(this.oPgFrm.Page2.oPag.oNO_SESSO_2_15.RadioValue()==this.w_NO_SESSO)
      this.oPgFrm.Page2.oPag.oNO_SESSO_2_15.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNOCODFIS_2_16.value==this.w_NOCODFIS)
      this.oPgFrm.Page2.oPag.oNOCODFIS_2_16.value=this.w_NOCODFIS
    endif
    if not(this.oPgFrm.Page2.oPag.oNOPARIVA_2_18.value==this.w_NOPARIVA)
      this.oPgFrm.Page2.oPag.oNOPARIVA_2_18.value=this.w_NOPARIVA
    endif
    if not(this.oPgFrm.Page2.oPag.oNO__NOTE_2_19.value==this.w_NO__NOTE)
      this.oPgFrm.Page2.oPag.oNO__NOTE_2_19.value=this.w_NO__NOTE
    endif
    if not(this.oPgFrm.Page2.oPag.oNONATGIU_2_26.RadioValue()==this.w_NONATGIU)
      this.oPgFrm.Page2.oPag.oNONATGIU_2_26.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_NOCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNOCODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_NOCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_NODESCRI)) or not(IIF(.w_NOSOGGET<>'PF' ,CHKCFP(.w_NODESCRI, "RA", "N",1, .w_NOCODICE),.T.)))  and (NOT EMPTY(.w_NOCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNODESCRI_1_3.SetFocus()
            i_bnoObbl = !empty(.w_NODESCRI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_NOCOGNOM) and (.w_NOSOGGET='PF' and !Isalt()))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNOCOGNOM_2_3.SetFocus()
            i_bnoObbl = !empty(.w_NOCOGNOM) or !(.w_NOSOGGET='PF' and !Isalt())
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_NO__NOME) and (.w_NOSOGGET='PF' and !Isalt()))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNO__NOME_2_6.SetFocus()
            i_bnoObbl = !empty(.w_NO__NOME) or !(.w_NOSOGGET='PF' and !Isalt())
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(CHKCFP(.w_NOCODFIS, 'CF', 'N',0,.w_NOCODICE, .w_NONAZION))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNOCODFIS_2_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(CHKCFP(.w_NOPARIVA, "PI",'N',0,.w_NOCODICE, .w_NONAZION))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNOPARIVA_2_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsar_kwc
      if i_bRes and i_bNoChk
        local OldMessage
        OldMessage = i_cErrorMsg
        i_cErrorMsg = GSAR_bwc(this, "CHECK_FORM")
        if !EMPTY(i_cErrorMsg)
          i_bRes = .F.
          i_bNoChk = .F.
        else
          i_cErrorMsg = OldMessage
        endif
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_NOSOGGET = this.w_NOSOGGET
    this.o_NOCODFIS = this.w_NOCODFIS
    return

enddefine

* --- Define pages as container
define class tgsar_kwcPag1 as StdContainer
  Width  = 750
  height = 281
  stdWidth  = 750
  stdheight = 281
  resizeXpos=282
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oNOCODICE_1_1 as StdField with uid="RNZSVYDFYR",rtseq=1,rtrep=.f.,;
    cFormVar = "w_NOCODICE", cQueryName = "NOCODICE",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice",;
    HelpContextID = 195693851,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=116, Top=7, cSayPict="p_NOM", cGetPict="p_NOM", InputMask=replicate('X',15)

  proc oNOCODICE_1_1.mAfter
    with this.Parent.oContained
      .w_NOCODICE=CALCZER(.w_NOCODICE, 'OFF_NOMI')
    endwith
  endproc

  add object oNODESCRI_1_3 as StdField with uid="NGPZHRNSCX",rtseq=2,rtrep=.f.,;
    cFormVar = "w_NODESCRI", cQueryName = "NODESCRI",;
    bObbl = .t. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 110107935,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=116, Top=34, InputMask=replicate('X',60)

  func oNODESCRI_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_NOCODICE))
    endwith
   endif
  endfunc

  proc oNODESCRI_1_3.mAfter
    with this.Parent.oContained
      .w_NODESCRI=Normragsoc(.w_NODESCRI)
    endwith
  endproc

  func oNODESCRI_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (IIF(.w_NOSOGGET<>'PF' ,CHKCFP(.w_NODESCRI, "RA", "N",1, .w_NOCODICE),.T.))
    endwith
    return bRes
  endfunc


  add object oNOSOGGET_1_4 as StdCombo with uid="SIKLPEILWV",rtseq=3,rtrep=.f.,left=355,top=7,width=107,height=22;
    , ToolTipText = "Soggetto: ente\persona fisica";
    , HelpContextID = 165350698;
    , cFormVar="w_NOSOGGET",RowSource=""+"Persona fisica,"+"Ente", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oNOSOGGET_1_4.RadioValue()
    return(iif(this.value =1,'PF',;
    iif(this.value =2,'EN',;
    space(2))))
  endfunc
  func oNOSOGGET_1_4.GetRadio()
    this.Parent.oContained.w_NOSOGGET = this.RadioValue()
    return .t.
  endfunc

  func oNOSOGGET_1_4.SetRadio()
    this.Parent.oContained.w_NOSOGGET=trim(this.Parent.oContained.w_NOSOGGET)
    this.value = ;
      iif(this.Parent.oContained.w_NOSOGGET=='PF',1,;
      iif(this.Parent.oContained.w_NOSOGGET=='EN',2,;
      0))
  endfunc

  func oNOSOGGET_1_4.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oNOINDIRI_1_6 as StdField with uid="YKHJVSMYWY",rtseq=4,rtrep=.f.,;
    cFormVar = "w_NOINDIRI", cQueryName = "NOINDIRI",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 195652895,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=116, Top=61, InputMask=replicate('X',35)

  add object oNOINDI_2_1_7 as StdField with uid="CINWHOGMWX",rtseq=5,rtrep=.f.,;
    cFormVar = "w_NOINDI_2", cQueryName = "NOINDI_2",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 72782584,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=376, Top=61, InputMask=replicate('X',35)

  add object oNO___CAP_1_10 as StdField with uid="BXEHVLTEGQ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_NO___CAP", cQueryName = "NO___CAP",;
    bObbl = .f. , nPag = 1, value=space(9), bMultilanguage =  .f.,;
    ToolTipText = "Codice di avviamento postale",;
    HelpContextID = 124505382,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=116, Top=88, InputMask=replicate('X',9), bHasZoom = .t. 

  func oNO___CAP_1_10.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) = "ADHOC REVOLUTION")
    endwith
  endfunc

  proc oNO___CAP_1_10.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_NO___CAP",".w_NOLOCALI",".w_NOPROVIN")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oNO___CAP_1_11 as StdField with uid="CCCWXBHSEV",rtseq=7,rtrep=.f.,;
    cFormVar = "w_NO___CAP", cQueryName = "NO___CAP",;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Codice di avviamento postale",;
    HelpContextID = 124505382,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=116, Top=88, InputMask=replicate('X',8), bHasZoom = .t. 

  func oNO___CAP_1_11.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) = "AD HOC ENTERPRISE")
    endwith
  endfunc

  proc oNO___CAP_1_11.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_NO___CAP",".w_NOLOCALI",".w_NOPROVIN")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oNOLOCALI_1_12 as StdField with uid="JWFWPYUEYX",rtseq=8,rtrep=.f.,;
    cFormVar = "w_NOLOCALI", cQueryName = "NOLOCALI",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Localit� di residenza",;
    HelpContextID = 207971041,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=203, Top=88, InputMask=replicate('X',30), bHasZoom = .t. 

  func oNOLOCALI_1_12.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "ADHOC REVOLUTION")
    endwith
  endfunc

  proc oNOLOCALI_1_12.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_NO___CAP",".w_NOLOCALI",".w_NOPROVIN")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oNOLOCALI_1_13 as StdField with uid="EXRQQZLDNM",rtseq=9,rtrep=.f.,;
    cFormVar = "w_NOLOCALI", cQueryName = "NOLOCALI",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Localit� di residenza",;
    HelpContextID = 207971041,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=203, Top=88, InputMask=replicate('X',30), bHasZoom = .t. 

  func oNOLOCALI_1_13.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) = "ADHOC REVOLUTION")
    endwith
  endfunc

  proc oNOLOCALI_1_13.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_NO___CAP",".w_NOLOCALI",".w_NOPROVIN")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oNOPROVIN_1_15 as StdField with uid="HZGKWBSESH",rtseq=10,rtrep=.f.,;
    cFormVar = "w_NOPROVIN", cQueryName = "NOPROVIN",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di residenza",;
    HelpContextID = 111289052,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=499, Top=88, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. 

  func oNOPROVIN_1_15.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "ADHOC REVOLUTION")
    endwith
  endfunc

  proc oNOPROVIN_1_15.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_NOPROVIN")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oNOPROVIN_1_16 as StdField with uid="MUXCAXNXRG",rtseq=11,rtrep=.f.,;
    cFormVar = "w_NOPROVIN", cQueryName = "NOPROVIN",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di residenza",;
    HelpContextID = 111289052,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=499, Top=88, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. 

  func oNOPROVIN_1_16.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) = "ADHOC REVOLUTION")
    endwith
  endfunc

  proc oNOPROVIN_1_16.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_NOPROVIN")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oNONAZION_1_18 as StdField with uid="VXJHEYZNQX",rtseq=12,rtrep=.f.,;
    cFormVar = "w_NONAZION", cQueryName = "NONAZION",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice della nazione di appartenenza",;
    HelpContextID = 50545372,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=631, Top=88, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="NAZIONI", oKey_1_1="NACODNAZ", oKey_1_2="this.w_NONAZION"

  func oNONAZION_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oNONAZION_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNONAZION_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'NAZIONI','*','NACODNAZ',cp_AbsName(this.parent,'oNONAZION_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oNOTELEFO_1_19 as StdField with uid="SHCZOLLPBT",rtseq=13,rtrep=.f.,;
    cFormVar = "w_NOTELEFO", cQueryName = "NOTELEFO",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Numero telefonico principale",;
    HelpContextID = 136387877,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=116, Top=115, InputMask=replicate('X',18)

  add object oNOTELFAX_1_21 as StdField with uid="DDYGEOYDNJ",rtseq=14,rtrep=.f.,;
    cFormVar = "w_NOTELFAX", cQueryName = "NOTELFAX",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Numero di TELEX o FAX",;
    HelpContextID = 153165102,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=355, Top=115, InputMask=replicate('X',18)

  add object oNONUMCEL_1_24 as StdField with uid="QBSFSELWGL",rtseq=15,rtrep=.f.,;
    cFormVar = "w_NONUMCEL", cQueryName = "NONUMCEL",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Numero del telefono cellulare",;
    HelpContextID = 104906018,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=601, Top=115, InputMask=replicate('X',18)

  add object oNO_SKYPE_1_25 as StdField with uid="ERYEMWYSMR",rtseq=16,rtrep=.f.,;
    cFormVar = "w_NO_SKYPE", cQueryName = "NO_SKYPE",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Codice utente Skype",;
    HelpContextID = 203410715,;
   bGlobalFont=.t.,;
    Height=21, Width=378, Left=116, Top=142, InputMask=replicate('X',50)

  add object oNO_EMAIL_1_26 as StdField with uid="TLZOMAVMCO",rtseq=17,rtrep=.f.,;
    cFormVar = "w_NO_EMAIL", cQueryName = "NO_EMAIL",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo di posta elettronica",;
    HelpContextID = 198062814,;
   bGlobalFont=.t.,;
    Height=21, Width=624, Left=116, Top=169, InputMask=replicate('X',254)

  add object oNOINDWEB_1_28 as StdField with uid="WFSOYCKVKP",rtseq=18,rtrep=.f.,;
    cFormVar = "w_NOINDWEB", cQueryName = "NOINDWEB",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo sito Internet",;
    HelpContextID = 162098456,;
   bGlobalFont=.t.,;
    Height=21, Width=624, Left=116, Top=196, InputMask=replicate('X',254)


  add object oBtn_1_36 as StdButton with uid="IBRDXZOPMJ",left=576, top=232, width=48,height=45,;
    CpPicture="..\EXE\BMP\RIGHT.BMP", caption="", nPag=1;
    , ToolTipText = "Avanza al passo successivo";
    , HelpContextID = 248189702;
    , Caption='A\<vanti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_36.Click()
      with this.Parent.oContained
        GSAR_bwc(this.Parent.oContained,"NEXT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_36.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP<.w_TOT_STEP AND NOT EMPTY(.w_NOCODICE))
      endwith
    endif
  endfunc


  add object oBtn_1_37 as StdButton with uid="JUAVHGMTDZ",left=523, top=232, width=48,height=45,;
    CpPicture="..\EXE\BMP\LEFT.BMP", caption="", nPag=1;
    , ToolTipText = "Torna al passo precedente";
    , HelpContextID = 148257781;
    , Caption='\<Indietro';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_37.Click()
      with this.Parent.oContained
        GSAR_bwc(this.Parent.oContained,"PREV")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_37.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP>1)
      endwith
    endif
  endfunc


  add object oBtn_1_38 as StdButton with uid="NLRIOAGGKF",left=629, top=232, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per salvare il nuovo nominativo";
    , HelpContextID = 32057114;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_38.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_38.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP=.w_TOT_STEP)
      endwith
    endif
  endfunc

  func oBtn_1_38.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_CUR_STEP<.w_TOT_STEP)
     endwith
    endif
  endfunc


  add object oBtn_1_39 as StdButton with uid="ABKYYMEJLO",left=682, top=232, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 22207238;
    , Caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_39.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_2 as StdString with uid="VHBXUGMVHR",Visible=.t., Left=10, Top=9,;
    Alignment=1, Width=102, Height=18,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_5 as StdString with uid="OMBRZPRYNR",Visible=.t., Left=6, Top=36,;
    Alignment=1, Width=106, Height=18,;
    Caption="Denominazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="SLYNWOXAVV",Visible=.t., Left=6, Top=63,;
    Alignment=1, Width=106, Height=18,;
    Caption="Indirizzo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="QIYXCLYZCQ",Visible=.t., Left=6, Top=90,;
    Alignment=1, Width=106, Height=18,;
    Caption="CAP - localit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="JXXVOQXOTX",Visible=.t., Left=432, Top=90,;
    Alignment=1, Width=62, Height=18,;
    Caption="Prov.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="UEQFMUWXLP",Visible=.t., Left=543, Top=91,;
    Alignment=1, Width=79, Height=18,;
    Caption="Nazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="YGWJHKYKLW",Visible=.t., Left=6, Top=117,;
    Alignment=1, Width=106, Height=18,;
    Caption="Telefono:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="LOAFDKDUEJ",Visible=.t., Left=260, Top=117,;
    Alignment=1, Width=92, Height=18,;
    Caption="Telefax:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="ZNLPVFEFLZ",Visible=.t., Left=499, Top=117,;
    Alignment=1, Width=96, Height=18,;
    Caption="Cellulare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="FAACLRDHMZ",Visible=.t., Left=6, Top=170,;
    Alignment=1, Width=106, Height=18,;
    Caption="E-mail:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="DLDCLSYQOD",Visible=.t., Left=6, Top=197,;
    Alignment=1, Width=106, Height=18,;
    Caption="Internet web:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="EHBINSTDXG",Visible=.t., Left=6, Top=145,;
    Alignment=1, Width=106, Height=18,;
    Caption="Skype:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="VRSBHYCUEF",Visible=.t., Left=289, Top=9,;
    Alignment=1, Width=63, Height=18,;
    Caption="Soggetto:"  ;
  , bGlobalFont=.t.

  func oStr_1_45.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oBox_1_40 as StdBox with uid="URYVCGNFAN",left=514, top=225, width=223,height=2
enddefine
define class tgsar_kwcPag2 as StdContainer
  Width  = 750
  height = 281
  stdWidth  = 750
  stdheight = 281
  resizeXpos=236
  resizeYpos=185
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oNOCOGNOM_2_3 as StdField with uid="XAFUGUJOJM",rtseq=22,rtrep=.f.,;
    cFormVar = "w_NOCOGNOM", cQueryName = "NOCOGNOM",;
    bObbl = .t. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Cognome",;
    HelpContextID = 254145245,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=119, Top=14, InputMask=replicate('X',50)

  func oNOCOGNOM_2_3.CondObbl()
    local i_bRes
    i_bRes = .t.
    with this.Parent.oContained
      i_bres=.w_NOSOGGET='PF' and !Isalt()
    endwith
    return i_bres
  endfunc

  add object oNOSOGGET_2_4 as StdCheck with uid="CYCSNYRQCP",rtseq=23,rtrep=.f.,left=491, top=14, caption="Persona fisica",;
    ToolTipText = "Soggetto: ente\persona fisica",;
    HelpContextID = 165350698,;
    cFormVar="w_NOSOGGET", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oNOSOGGET_2_4.RadioValue()
    return(iif(this.value =1,'PF',;
    'EN'))
  endfunc
  func oNOSOGGET_2_4.GetRadio()
    this.Parent.oContained.w_NOSOGGET = this.RadioValue()
    return .t.
  endfunc

  func oNOSOGGET_2_4.SetRadio()
    this.Parent.oContained.w_NOSOGGET=trim(this.Parent.oContained.w_NOSOGGET)
    this.value = ;
      iif(this.Parent.oContained.w_NOSOGGET=='PF',1,;
      0)
  endfunc

  func oNOSOGGET_2_4.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc

  add object oNO__NOME_2_6 as StdField with uid="LSAFYIOXLB",rtseq=24,rtrep=.f.,;
    cFormVar = "w_NO__NOME", cQueryName = "NO__NOME",;
    bObbl = .t. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Nome",;
    HelpContextID = 228864741,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=119, Top=41, InputMask=replicate('X',50)

  func oNO__NOME_2_6.CondObbl()
    local i_bRes
    i_bRes = .t.
    with this.Parent.oContained
      i_bres=.w_NOSOGGET='PF' and !Isalt()
    endwith
    return i_bres
  endfunc

  add object oNOLOCNAS_2_9 as StdField with uid="AERLKTKRON",rtseq=25,rtrep=.f.,;
    cFormVar = "w_NOLOCNAS", cQueryName = "NOLOCNAS",;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Luogo di nascita",;
    HelpContextID = 10132777,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=119, Top=68, InputMask=replicate('X',30), bHasZoom = .t. 

  func oNOLOCNAS_2_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NOSOGGET<>'EN')
    endwith
   endif
  endfunc

  proc oNOLOCNAS_2_9.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C","",".w_NOLOCNAS",".w_NOPRONAS",,,".w_CODCOM")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oNOPRONAS_2_11 as StdField with uid="GEJPRPYENZ",rtseq=26,rtrep=.f.,;
    cFormVar = "w_NOPRONAS", cQueryName = "NOPRONAS",;
    bObbl = .f. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia del luogo di nascita",;
    HelpContextID = 22928681,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=410, Top=68, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. 

  func oNOPRONAS_2_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NOSOGGET<>'EN')
    endwith
   endif
  endfunc

  proc oNOPRONAS_2_11.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_NOPRONAS")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oNODATNAS_2_13 as StdField with uid="ZTSDEOZBDX",rtseq=27,rtrep=.f.,;
    cFormVar = "w_NODATNAS", cQueryName = "NODATNAS",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita",;
    HelpContextID = 27008297,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=491, Top=68

  func oNODATNAS_2_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NOSOGGET<>'EN')
    endwith
   endif
  endfunc


  add object oNO_SESSO_2_15 as StdCombo with uid="XXAZSJMBTB",rtseq=28,rtrep=.f.,left=653,top=68,width=90,height=21;
    , ToolTipText = "Sesso";
    , HelpContextID = 96455973;
    , cFormVar="w_NO_SESSO",RowSource=""+"Maschio,"+"Femmina", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oNO_SESSO_2_15.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oNO_SESSO_2_15.GetRadio()
    this.Parent.oContained.w_NO_SESSO = this.RadioValue()
    return .t.
  endfunc

  func oNO_SESSO_2_15.SetRadio()
    this.Parent.oContained.w_NO_SESSO=trim(this.Parent.oContained.w_NO_SESSO)
    this.value = ;
      iif(this.Parent.oContained.w_NO_SESSO=='M',1,;
      iif(this.Parent.oContained.w_NO_SESSO=='F',2,;
      0))
  endfunc

  func oNO_SESSO_2_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NOSOGGET<>'EN')
    endwith
   endif
  endfunc

  add object oNOCODFIS_2_16 as StdField with uid="PYQSSFDNFK",rtseq=29,rtrep=.f.,;
    cFormVar = "w_NOCODFIS", cQueryName = "NOCODFIS",;
    bObbl = .f. , nPag = 2, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale - Doppio click per eseguire il calcolo del codice fiscale",;
    HelpContextID = 123073239,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=119, Top=95, cSayPict='REPL("!",16)', cGetPict='REPL("!",16)', InputMask=replicate('X',16), bHasZoom = .t. 

  func oNOCODFIS_2_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CHKCFP(.w_NOCODFIS, 'CF', 'N',0,.w_NOCODICE, .w_NONAZION))
    endwith
    return bRes
  endfunc

  proc oNOCODFIS_2_16.mZoom
      with this.Parent.oContained
        GSAR_BFC(this.Parent.oContained, ".w_NOCODFIS"  , .w_NOCOGNOM,.w_NO__NOME,.w_NOLOCNAS,.w_NOPRONAS,.w_NODATNAS,.w_NO_SESSO,,,.w_CODCOM)
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oNOPARIVA_2_18 as StdField with uid="BRAEXIUAIA",rtseq=30,rtrep=.f.,;
    cFormVar = "w_NOPARIVA", cQueryName = "NOPARIVA",;
    bObbl = .f. , nPag = 2, value=space(12), bMultilanguage =  .f.,;
    ToolTipText = "Partita IVA",;
    HelpContextID = 209509655,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=410, Top=95, InputMask=replicate('X',12)

  func oNOPARIVA_2_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CHKCFP(.w_NOPARIVA, "PI",'N',0,.w_NOCODICE, .w_NONAZION))
    endwith
    return bRes
  endfunc

  add object oNO__NOTE_2_19 as StdMemo with uid="MPCGZQUTAZ",rtseq=31,rtrep=.f.,;
    cFormVar = "w_NO__NOTE", cQueryName = "NO__NOTE",;
    bObbl = .f. , nPag = 2, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 39570715,;
   bGlobalFont=.t.,;
    Height=96, Width=622, Left=119, Top=122


  add object oBtn_2_20 as StdButton with uid="ERGMNHIPVD",left=576, top=232, width=48,height=45,;
    CpPicture="..\EXE\BMP\RIGHT.BMP", caption="", nPag=2;
    , ToolTipText = "Avanza al passo successivo";
    , HelpContextID = 248189702;
    , Caption='A\<vanti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_20.Click()
      with this.Parent.oContained
        GSAR_bwc(this.Parent.oContained,"NEXT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_20.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP<.w_TOT_STEP)
      endwith
    endif
  endfunc


  add object oBtn_2_21 as StdButton with uid="TAMAHPKNYE",left=523, top=232, width=48,height=45,;
    CpPicture="..\EXE\BMP\LEFT.BMP", caption="", nPag=2;
    , ToolTipText = "Torna al passo precedente";
    , HelpContextID = 148257781;
    , Caption='\<Indietro';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_21.Click()
      with this.Parent.oContained
        GSAR_bwc(this.Parent.oContained,"PREV")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_21.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP>1)
      endwith
    endif
  endfunc


  add object oBtn_2_22 as StdButton with uid="YITUWBQMSU",left=629, top=232, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per salvare il nuovo nominativo";
    , HelpContextID = 32057114;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_22.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_22.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP=.w_TOT_STEP)
      endwith
    endif
  endfunc

  func oBtn_2_22.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_CUR_STEP<.w_TOT_STEP)
     endwith
    endif
  endfunc


  add object oBtn_2_23 as StdButton with uid="JYJQAIVGOQ",left=682, top=232, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 22207238;
    , Caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_23.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oNONATGIU_2_26 as StdCombo with uid="MHZGWJNXXW",rtseq=36,rtrep=.f.,left=119,top=229,width=251,height=22;
    , ToolTipText = "Natura giuridica del nominativo. Richiesto al momento del deposito telematico se questo nominativo sar� inserito fra le parti di un procedimento";
    , HelpContextID = 90391253;
    , cFormVar="w_NONATGIU",RowSource=""+"Persona fisica,"+"Societ�,"+"Societ� di persone,"+"Cooperative,"+"Consorzio,"+"Condominio,"+"Associazione,"+"Comitato,"+"Enti di gestione,"+"Ente pubblico,"+"Ente religioso,"+"Partito o Sindacato,"+"Stato/Organizzazione estera,"+"Ditta Individuale,"+"Persona giuridica,"+"Pubblica amminstrazione,"+"Istituto di credito,"+"Non specificata,"+"Pubblico Ministero", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oNONATGIU_2_26.RadioValue()
    return(iif(this.value =1,'PFI',;
    iif(this.value =2,'SOC',;
    iif(this.value =3,'SOP',;
    iif(this.value =4,'COP',;
    iif(this.value =5,'CON',;
    iif(this.value =6,'CND',;
    iif(this.value =7,'ASS',;
    iif(this.value =8,'COM',;
    iif(this.value =9,'EDG',;
    iif(this.value =10,'ENP',;
    iif(this.value =11,'EIS',;
    iif(this.value =12,'PAS',;
    iif(this.value =13,'OSE',;
    iif(this.value =14,'DIM',;
    iif(this.value =15,'PGI',;
    iif(this.value =16,'PAM',;
    iif(this.value =17,'ISC',;
    iif(this.value =18,'NA',;
    iif(this.value =19,'PUM',;
    space(3)))))))))))))))))))))
  endfunc
  func oNONATGIU_2_26.GetRadio()
    this.Parent.oContained.w_NONATGIU = this.RadioValue()
    return .t.
  endfunc

  func oNONATGIU_2_26.SetRadio()
    this.Parent.oContained.w_NONATGIU=trim(this.Parent.oContained.w_NONATGIU)
    this.value = ;
      iif(this.Parent.oContained.w_NONATGIU=='PFI',1,;
      iif(this.Parent.oContained.w_NONATGIU=='SOC',2,;
      iif(this.Parent.oContained.w_NONATGIU=='SOP',3,;
      iif(this.Parent.oContained.w_NONATGIU=='COP',4,;
      iif(this.Parent.oContained.w_NONATGIU=='CON',5,;
      iif(this.Parent.oContained.w_NONATGIU=='CND',6,;
      iif(this.Parent.oContained.w_NONATGIU=='ASS',7,;
      iif(this.Parent.oContained.w_NONATGIU=='COM',8,;
      iif(this.Parent.oContained.w_NONATGIU=='EDG',9,;
      iif(this.Parent.oContained.w_NONATGIU=='ENP',10,;
      iif(this.Parent.oContained.w_NONATGIU=='EIS',11,;
      iif(this.Parent.oContained.w_NONATGIU=='PAS',12,;
      iif(this.Parent.oContained.w_NONATGIU=='OSE',13,;
      iif(this.Parent.oContained.w_NONATGIU=='DIM',14,;
      iif(this.Parent.oContained.w_NONATGIU=='PGI',15,;
      iif(this.Parent.oContained.w_NONATGIU=='PAM',16,;
      iif(this.Parent.oContained.w_NONATGIU=='ISC',17,;
      iif(this.Parent.oContained.w_NONATGIU=='NA',18,;
      iif(this.Parent.oContained.w_NONATGIU=='PUM',19,;
      0)))))))))))))))))))
  endfunc

  func oNONATGIU_2_26.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oStr_2_1 as StdString with uid="GRLMRZDMHW",Visible=.t., Left=10, Top=97,;
    Alignment=1, Width=106, Height=18,;
    Caption="Cod. fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_2 as StdString with uid="GPRMURSOGL",Visible=.t., Left=300, Top=97,;
    Alignment=1, Width=106, Height=18,;
    Caption="Partita IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_2_5 as StdString with uid="GIQPXWBUWL",Visible=.t., Left=9, Top=16,;
    Alignment=1, Width=106, Height=18,;
    Caption="Cognome:"  ;
  , bGlobalFont=.t.

  add object oStr_2_7 as StdString with uid="NQAGLJHGSA",Visible=.t., Left=9, Top=44,;
    Alignment=1, Width=106, Height=18,;
    Caption="Nome:"  ;
  , bGlobalFont=.t.

  add object oStr_2_8 as StdString with uid="ZPRSSXFVWX",Visible=.t., Left=9, Top=69,;
    Alignment=1, Width=106, Height=18,;
    Caption="Nato a:"  ;
  , bGlobalFont=.t.

  add object oStr_2_10 as StdString with uid="GOAVKPDDJZ",Visible=.t., Left=345, Top=69,;
    Alignment=1, Width=61, Height=18,;
    Caption="Prov.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_12 as StdString with uid="XMJRYPGNSE",Visible=.t., Left=454, Top=69,;
    Alignment=1, Width=32, Height=18,;
    Caption="il:"  ;
  , bGlobalFont=.t.

  add object oStr_2_14 as StdString with uid="GYFBVZOTAV",Visible=.t., Left=591, Top=69,;
    Alignment=1, Width=60, Height=18,;
    Caption="Sesso:"  ;
  , bGlobalFont=.t.

  add object oStr_2_17 as StdString with uid="MCGPSBXLOE",Visible=.t., Left=10, Top=122,;
    Alignment=1, Width=106, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_2_27 as StdString with uid="LIVJITKWXK",Visible=.t., Left=3, Top=229,;
    Alignment=1, Width=113, Height=18,;
    Caption="Natura giuridica Pct:"  ;
  , bGlobalFont=.t.

  func oStr_2_27.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oBox_2_24 as StdBox with uid="ITNLVTINWU",left=514, top=225, width=223,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_kwc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
