* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscf_mre                                                        *
*              Regole di log                                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-10-06                                                      *
* Last revis.: 2013-11-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscf_mre"))

* --- Class definition
define class tgscf_mre as StdTrsForm
  Top    = 1
  Left   = 10

  * --- Standard Properties
  Width  = 781
  Height = 543+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-11-26"
  HelpContextID=113915241
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=48

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  REF_MAST_IDX = 0
  REF_DETT_IDX = 0
  XDC_TABLE_IDX = 0
  XDC_FIELDS_IDX = 0
  cFile = "REF_MAST"
  cFileDetail = "REF_DETT"
  cKeySelect = "RESERIAL"
  cKeyWhere  = "RESERIAL=this.w_RESERIAL"
  cKeyDetail  = "RESERIAL=this.w_RESERIAL and RE_TABLE=this.w_RE_TABLE and CPROWNUM=this.w_CPROWNUM"
  cKeyWhereODBC = '"RESERIAL="+cp_ToStrODBC(this.w_RESERIAL)';

  cKeyDetailWhereODBC = '"RESERIAL="+cp_ToStrODBC(this.w_RESERIAL)';
      +'+" and RE_TABLE="+cp_ToStrODBC(this.w_RE_TABLE)';
      +'+" and CPROWNUM="+cp_ToStrODBC(this.w_CPROWNUM)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"REF_DETT.RESERIAL="+cp_ToStrODBC(this.w_RESERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'REF_DETT.CPROWORD '
  cPrg = "gscf_mre"
  cComment = "Regole di log"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 8
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_RESERIAL = space(10)
  w_REFLENAB = space(1)
  w_REPRIORI = 0
  w_REREGSUC = space(1)
  w_REDESCRI = space(50)
  w_REBUSOBJ = space(15)
  o_REBUSOBJ = space(15)
  w_REBUSOBJ = space(15)
  w_REPSTBTN = space(1)
  w_RETABMST = space(20)
  o_RETABMST = space(20)
  w_RETABDTL = space(20)
  o_RETABDTL = space(20)
  w_RETFLTEX = space(254)
  w_RETDELEX = space(254)
  w_REMSGERC = space(254)
  w_REGG_LOG = 0
  w_RESAVEDB = space(1)
  w_REUSEROP = space(30)
  w_TBCOMMENT = space(60)
  w_TBCOMMDTL = space(60)
  w_CPROWORD = 0
  w_RE_TABLE = space(20)
  o_RE_TABLE = space(20)
  w_REFLDNAM = space(30)
  w_MSTDET = space(1)
  o_MSTDET = space(1)
  w_REKEYREC = space(1)
  o_REKEYREC = space(1)
  w_RERIFREC = space(1)
  w_REMONINS = space(1)
  w_REMONMOD = space(1)
  o_REMONMOD = space(1)
  w_REMONDEL = space(1)
  w_REFLTEXP = space(254)
  w_NPKM = 0
  w_NPKD = 0
  w_REBLKINS = space(1)
  o_REBLKINS = space(1)
  w_REOBBINS = space(1)
  o_REOBBINS = space(1)
  w_REFLDCOM = space(254)
  w_CPROWNUM = 0
  w_REKEYGES = space(1)
  w_REKEYSEL = space(60)
  w_REMSGERR = space(254)
  w_TABLE1 = space(20)
  w_TABLE2 = space(20)
  w_WARNROWS = 0
  w_TOTPKM = 0
  w_TOTPKD = 0
  w_TOTROW = 0
  w_CTOTROW = 0
  w_MSGERR = space(10)
  w_SHOWALERT = .F.
  w_VOID = .F.
  w_RECCNADR = space(0)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_RESERIAL = this.W_RESERIAL

  * --- Children pointers
  GSCF_MRU = .NULL.
  GSCF_MOA = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gscf_mre
  cMyFilter = ""
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'REF_MAST','gscf_mre')
    stdPageFrame::Init()
    *set procedure to GSCF_MRU additive
    with this
      .Pages(1).addobject("oPag","tgscf_mrePag1","gscf_mre",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Regola")
      .Pages(1).HelpContextID = 23831830
      .Pages(2).addobject("oPag","tgscf_mrePag2")
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Notifiche")
      .Pages(2).HelpContextID = 151422350
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oRESERIAL_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSCF_MRU
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='XDC_TABLE'
    this.cWorkTables[2]='XDC_FIELDS'
    this.cWorkTables[3]='REF_MAST'
    this.cWorkTables[4]='REF_DETT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.REF_MAST_IDX,5],7]
    this.nPostItConn=i_TableProp[this.REF_MAST_IDX,3]
  return

  function CreateChildren()
    this.GSCF_MRU = CREATEOBJECT('stdDynamicChild',this,'GSCF_MRU',this.oPgFrm.Page1.oPag.oLinkPC_1_17)
    this.GSCF_MRU.createrealchild()
    this.GSCF_MOA = CREATEOBJECT('stdDynamicChild',this,'GSCF_MOA',this.oPgFrm.Page2.oPag.oLinkPC_4_1)
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSCF_MRU)
      this.GSCF_MRU.DestroyChildrenChain()
      this.GSCF_MRU=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_17')
    if !ISNULL(this.GSCF_MOA)
      this.GSCF_MOA.DestroyChildrenChain()
      this.GSCF_MOA=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_4_1')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSCF_MRU.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCF_MOA.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSCF_MRU.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCF_MOA.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSCF_MRU.NewDocument()
    this.GSCF_MOA.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .GSCF_MRU.ChangeRow(this.cRowID+'      1',1;
             ,.w_RESERIAL,"RUSERIAL";
             )
      .GSCF_MOA.ChangeRow(this.cRowID+'      1',1;
             ,.w_RESERIAL,"OASERIAL";
             )
    endwith
    select (i_cOldSel)
    return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_RESERIAL = NVL(RESERIAL,space(10))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_9_joined
    link_1_9_joined=.f.
    local link_1_10_joined
    link_1_10_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from REF_MAST where RESERIAL=KeySet.RESERIAL
    *
    i_nConn = i_TableProp[this.REF_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.REF_MAST_IDX,2],this.bLoadRecFilter,this.REF_MAST_IDX,"gscf_mre")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('REF_MAST')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "REF_MAST.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"REF_DETT.","REF_MAST.")
      i_cTable = i_cTable+' REF_MAST '
      link_1_9_joined=this.AddJoinedLink_1_9(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_10_joined=this.AddJoinedLink_1_10(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'RESERIAL',this.w_RESERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_TBCOMMENT = space(60)
        .w_TBCOMMDTL = space(60)
        .w_WARNROWS = iif(vartype(g_NROWS_CF_WARN)='N' AND g_NROWS_CF_WARN>=30 , int(g_NROWS_CF_WARN), 100)
        .w_TOTPKM = 0
        .w_TOTPKD = 0
        .w_SHOWALERT = True
        .w_VOID = .f.
        .w_RESERIAL = NVL(RESERIAL,space(10))
        .op_RESERIAL = .w_RESERIAL
        .w_REFLENAB = NVL(REFLENAB,space(1))
        .w_REPRIORI = NVL(REPRIORI,0)
        .w_REREGSUC = NVL(REREGSUC,space(1))
        .w_REDESCRI = NVL(REDESCRI,space(50))
        .w_REBUSOBJ = NVL(REBUSOBJ,space(15))
        .w_REBUSOBJ = NVL(REBUSOBJ,space(15))
        .w_REPSTBTN = NVL(REPSTBTN,space(1))
        .w_RETABMST = NVL(RETABMST,space(20))
          if link_1_9_joined
            this.w_RETABMST = NVL(TBNAME109,NVL(this.w_RETABMST,space(20)))
            this.w_TBCOMMENT = NVL(TBCOMMENT109,space(60))
          else
          .link_1_9('Load')
          endif
        .w_RETABDTL = NVL(RETABDTL,space(20))
          if link_1_10_joined
            this.w_RETABDTL = NVL(TBNAME110,NVL(this.w_RETABDTL,space(20)))
            this.w_TBCOMMDTL = NVL(TBCOMMENT110,space(60))
          else
          .link_1_10('Load')
          endif
        .w_RETFLTEX = NVL(RETFLTEX,space(254))
        .w_RETDELEX = NVL(RETDELEX,space(254))
        .w_REMSGERC = NVL(REMSGERC,space(254))
        .w_REGG_LOG = NVL(REGG_LOG,0)
        .w_RESAVEDB = NVL(RESAVEDB,space(1))
        .w_REUSEROP = NVL(REUSEROP,space(30))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_REKEYGES = NVL(REKEYGES,space(1))
        .w_REKEYSEL = NVL(REKEYSEL,space(60))
        .w_TABLE1 = .w_RETABMST
        .w_TABLE2 = .w_RETABDTL
        .w_TOTROW = .NumRow()
        .w_CTOTROW = alltrim(str(.w_TOTROW,4,0))
        .w_MSGERR = ah_msgformat("Attenzione: sono state inserite %1 righe, l'elaborazione della regola potrebbe provocare %2", .w_CTOTROW, iif(.w_TOTROW>255, "errori", "rallentamenti"))
        .w_RECCNADR = NVL(RECCNADR,space(0))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'REF_MAST')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from REF_DETT where RESERIAL=KeySet.RESERIAL
      *                            and RE_TABLE=KeySet.RE_TABLE
      *                            and CPROWNUM=KeySet.CPROWNUM
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.REF_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.REF_DETT_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('REF_DETT')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "REF_DETT.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" REF_DETT"
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'RESERIAL',this.w_RESERIAL  )
        select * from (i_cTable) REF_DETT where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      this.w_TOTPKM = 0
      this.w_TOTPKD = 0
      scan
        with this
          .w_CPROWNUM = CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_RE_TABLE = NVL(RE_TABLE,space(20))
          .w_REFLDNAM = NVL(REFLDNAM,space(30))
          * evitabile
          *.link_2_3('Load')
        .w_MSTDET = IIF(.w_RE_TABLE=.w_RETABDTL, "D", "M")
          .w_REKEYREC = NVL(REKEYREC,space(1))
          .w_RERIFREC = NVL(RERIFREC,space(1))
          .w_REMONINS = NVL(REMONINS,space(1))
          .w_REMONMOD = NVL(REMONMOD,space(1))
          .w_REMONDEL = NVL(REMONDEL,space(1))
          .w_REFLTEXP = NVL(REFLTEXP,space(254))
        .w_NPKM = IIF(.w_REKEYREC="S" and ALLTRIM(.w_RE_TABLE)==ALLTRIM(.w_RETABMST), 1, 0)
        .w_NPKD = IIF(.w_REKEYREC="S" and ALLTRIM(.w_RE_TABLE)==ALLTRIM(.w_RETABDTL), 1, 0)
          .w_REBLKINS = NVL(REBLKINS,space(1))
          .w_REOBBINS = NVL(REOBBINS,space(1))
          .w_REFLDCOM = NVL(REFLDCOM,space(254))
          .w_CPROWNUM = NVL(CPROWNUM,0)
          .w_REMSGERR = NVL(REMSGERR,space(254))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTPKM = .w_TOTPKM+.w_NPKM
          .w_TOTPKD = .w_TOTPKD+.w_NPKD
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_TABLE1 = .w_RETABMST
        .w_TABLE2 = .w_RETABDTL
        .w_TOTROW = .NumRow()
        .w_CTOTROW = alltrim(str(.w_TOTROW,4,0))
        .w_MSGERR = ah_msgformat("Attenzione: sono state inserite %1 righe, l'elaborazione della regola potrebbe provocare %2", .w_CTOTROW, iif(.w_TOTROW>255, "errori", "rallentamenti"))
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_1_47.enabled = .oPgFrm.Page1.oPag.oBtn_1_47.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_RESERIAL=space(10)
      .w_REFLENAB=space(1)
      .w_REPRIORI=0
      .w_REREGSUC=space(1)
      .w_REDESCRI=space(50)
      .w_REBUSOBJ=space(15)
      .w_REBUSOBJ=space(15)
      .w_REPSTBTN=space(1)
      .w_RETABMST=space(20)
      .w_RETABDTL=space(20)
      .w_RETFLTEX=space(254)
      .w_RETDELEX=space(254)
      .w_REMSGERC=space(254)
      .w_REGG_LOG=0
      .w_RESAVEDB=space(1)
      .w_REUSEROP=space(30)
      .w_TBCOMMENT=space(60)
      .w_TBCOMMDTL=space(60)
      .w_CPROWORD=10
      .w_RE_TABLE=space(20)
      .w_REFLDNAM=space(30)
      .w_MSTDET=space(1)
      .w_REKEYREC=space(1)
      .w_RERIFREC=space(1)
      .w_REMONINS=space(1)
      .w_REMONMOD=space(1)
      .w_REMONDEL=space(1)
      .w_REFLTEXP=space(254)
      .w_NPKM=0
      .w_NPKD=0
      .w_REBLKINS=space(1)
      .w_REOBBINS=space(1)
      .w_REFLDCOM=space(254)
      .w_CPROWNUM=0
      .w_REKEYGES=space(1)
      .w_REKEYSEL=space(60)
      .w_REMSGERR=space(254)
      .w_TABLE1=space(20)
      .w_TABLE2=space(20)
      .w_WARNROWS=0
      .w_TOTPKM=0
      .w_TOTPKD=0
      .w_TOTROW=0
      .w_CTOTROW=0
      .w_MSGERR=space(10)
      .w_SHOWALERT=.f.
      .w_VOID=.f.
      .w_RECCNADR=space(0)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        .w_REFLENAB = 'S'
        .DoRTCalc(3,3,.f.)
        .w_REREGSUC = 'S'
        .DoRTCalc(5,7,.f.)
        .w_REPSTBTN = 'N'
        .w_RETABMST = IIF(vartype(g_CTFL)="C" AND g_CTFL="S", .w_RETABMST, ICASE(.w_REBUSOBJ="gspr_acn","CAN_TIER",.w_REBUSOBJ="gsut_aid","PROMINDI",""))
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_RETABMST))
         .link_1_9('Full')
        endif
        .w_RETABDTL = IIF(vartype(g_CTFL)="C" AND g_CTFL="S", .w_RETABDTL, ICASE(.w_REBUSOBJ="gspr_mri","RIS_INTE",.w_REBUSOBJ="gspr_msr","RIS_ESTR",.w_REBUSOBJ="gspr_mse","RIS_ESTE",.w_REBUSOBJ="gsut_maa","PRODINDI",""))
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_RETABDTL))
         .link_1_10('Full')
        endif
        .DoRTCalc(11,14,.f.)
        .w_RESAVEDB = 'S'
        .DoRTCalc(16,19,.f.)
        .w_RE_TABLE = IIF(EMPTY(.w_RETABMST), .w_RETABDTL, .w_RETABMST)
        .DoRTCalc(21,21,.f.)
        if not(empty(.w_REFLDNAM))
         .link_2_3('Full')
        endif
        .w_MSTDET = IIF(.w_RE_TABLE=.w_RETABDTL, "D", "M")
        .w_REKEYREC = 'N'
        .w_RERIFREC = 'N'
        .w_REMONINS = 'N'
        .w_REMONMOD = 'N'
        .w_REMONDEL = 'N'
        .w_REFLTEXP = IIF(.w_REMONMOD='S', .w_REFLTEXP , '')
        .w_NPKM = IIF(.w_REKEYREC="S" and ALLTRIM(.w_RE_TABLE)==ALLTRIM(.w_RETABMST), 1, 0)
        .w_NPKD = IIF(.w_REKEYREC="S" and ALLTRIM(.w_RE_TABLE)==ALLTRIM(.w_RETABDTL), 1, 0)
        .w_REBLKINS = 'N'
        .w_REOBBINS = 'N'
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(33,37,.f.)
        .w_TABLE1 = .w_RETABMST
        .w_TABLE2 = .w_RETABDTL
        .w_WARNROWS = iif(vartype(g_NROWS_CF_WARN)='N' AND g_NROWS_CF_WARN>=30 , int(g_NROWS_CF_WARN), 100)
        .DoRTCalc(41,42,.f.)
        .w_TOTROW = .NumRow()
        .w_CTOTROW = alltrim(str(.w_TOTROW,4,0))
        .w_MSGERR = ah_msgformat("Attenzione: sono state inserite %1 righe, l'elaborazione della regola potrebbe provocare %2", .w_CTOTROW, iif(.w_TOTROW>255, "errori", "rallentamenti"))
        .w_SHOWALERT = True
      endif
    endwith
    cp_BlankRecExtFlds(this,'REF_MAST')
    this.DoRTCalc(47,48,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_1_47.enabled = this.oPgFrm.Page1.oPag.oBtn_1_47.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oRESERIAL_1_1.enabled = i_bVal
      .Page1.oPag.oREFLENAB_1_2.enabled = i_bVal
      .Page1.oPag.oREPRIORI_1_3.enabled = i_bVal
      .Page1.oPag.oREREGSUC_1_4.enabled = i_bVal
      .Page1.oPag.oREDESCRI_1_5.enabled = i_bVal
      .Page1.oPag.oREBUSOBJ_1_6.enabled = i_bVal
      .Page1.oPag.oREBUSOBJ_1_7.enabled = i_bVal
      .Page1.oPag.oREPSTBTN_1_8.enabled = i_bVal
      .Page1.oPag.oRETABMST_1_9.enabled = i_bVal
      .Page1.oPag.oRETABDTL_1_10.enabled = i_bVal
      .Page1.oPag.oRETFLTEX_1_11.enabled = i_bVal
      .Page1.oPag.oRETDELEX_1_12.enabled = i_bVal
      .Page1.oPag.oREMSGERC_1_13.enabled = i_bVal
      .Page1.oPag.oREGG_LOG_1_14.enabled = i_bVal
      .Page1.oPag.oRESAVEDB_1_15.enabled = i_bVal
      .Page1.oPag.oREUSEROP_1_16.enabled = i_bVal
      .Page1.oPag.oREFLTEXP_2_10.enabled = i_bVal
      .Page1.oPag.oREFLDCOM_2_19.enabled = i_bVal
      .Page1.oPag.oREKEYSEL_1_42.enabled = i_bVal
      .Page1.oPag.oREMSGERR_2_21.enabled = i_bVal
      .Page1.oPag.oBtn_1_18.enabled = i_bVal
      .Page1.oPag.oBtn_1_38.enabled = i_bVal
      .Page1.oPag.oBtn_1_47.enabled = .Page1.oPag.oBtn_1_47.mCond()
      .Page2.oPag.oBtn_4_2.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oRESERIAL_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oRESERIAL_1_1.enabled = .t.
      endif
    endwith
    this.GSCF_MRU.SetStatus(i_cOp)
    this.GSCF_MOA.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'REF_MAST',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.REF_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.REF_MAST_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SEREG","i_codazi,w_RESERIAL")
      .op_codazi = .w_codazi
      .op_RESERIAL = .w_RESERIAL
    endwith
    this.SetControlsValue()
  endproc

  *procedure SetChildrenStatus
  *  this.GSCF_MRU.SetChildrenStatus(i_cOp)
  *  this.GSCF_MOA.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.REF_MAST_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RESERIAL,"RESERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_REFLENAB,"REFLENAB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_REPRIORI,"REPRIORI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_REREGSUC,"REREGSUC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_REDESCRI,"REDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_REBUSOBJ,"REBUSOBJ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_REBUSOBJ,"REBUSOBJ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_REPSTBTN,"REPSTBTN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RETABMST,"RETABMST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RETABDTL,"RETABDTL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RETFLTEX,"RETFLTEX",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RETDELEX,"RETDELEX",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_REMSGERC,"REMSGERC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_REGG_LOG,"REGG_LOG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RESAVEDB,"RESAVEDB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_REUSEROP,"REUSEROP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_REKEYGES,"REKEYGES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_REKEYSEL,"REKEYSEL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RECCNADR,"RECCNADR",i_nConn)
    * --- Area Manuale = Build Filter
    * --- gscf_mre
    i_cFlt = IIF(EMPTY(this.cMyFilter), i_cFlt , this.cMyFilter)
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.REF_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.REF_MAST_IDX,2])
    i_lTable = "REF_MAST"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.REF_MAST_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(6);
      ,t_RE_TABLE C(20);
      ,t_REFLDNAM C(30);
      ,t_MSTDET N(3);
      ,t_REKEYREC N(3);
      ,t_RERIFREC N(3);
      ,t_REMONINS N(3);
      ,t_REMONMOD N(3);
      ,t_REMONDEL N(3);
      ,t_REFLTEXP C(254);
      ,t_REBLKINS N(3);
      ,t_REOBBINS N(3);
      ,t_REFLDCOM C(254);
      ,t_REMSGERR C(254);
      ,CPROWNUM N(10);
      ,t_NPKM N(2);
      ,t_NPKD N(2);
      ,t_CPROWNUM N(5);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgscf_mrebodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oRE_TABLE_2_2.controlsource=this.cTrsName+'.t_RE_TABLE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oREFLDNAM_2_3.controlsource=this.cTrsName+'.t_REFLDNAM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMSTDET_2_4.controlsource=this.cTrsName+'.t_MSTDET'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oREKEYREC_2_5.controlsource=this.cTrsName+'.t_REKEYREC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRERIFREC_2_6.controlsource=this.cTrsName+'.t_RERIFREC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oREMONINS_2_7.controlsource=this.cTrsName+'.t_REMONINS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oREMONMOD_2_8.controlsource=this.cTrsName+'.t_REMONMOD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oREMONDEL_2_9.controlsource=this.cTrsName+'.t_REMONDEL'
    this.oPgFRm.Page1.oPag.oREFLTEXP_2_10.controlsource=this.cTrsName+'.t_REFLTEXP'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oREBLKINS_2_13.controlsource=this.cTrsName+'.t_REBLKINS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oREOBBINS_2_14.controlsource=this.cTrsName+'.t_REOBBINS'
    this.oPgFRm.Page1.oPag.oREFLDCOM_2_19.controlsource=this.cTrsName+'.t_REFLDCOM'
    this.oPgFRm.Page1.oPag.oREMSGERR_2_21.controlsource=this.cTrsName+'.t_REMSGERR'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(58)
    this.AddVLine(134)
    this.AddVLine(250)
    this.AddVLine(310)
    this.AddVLine(377)
    this.AddVLine(450)
    this.AddVLine(549)
    this.AddVLine(616)
    this.AddVLine(689)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.REF_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.REF_MAST_IDX,2])
      with this
          cp_NextTableProg(this,i_nConn,"SEREG","i_codazi,w_RESERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into REF_MAST
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'REF_MAST')
        i_extval=cp_InsertValODBCExtFlds(this,'REF_MAST')
        local i_cFld
        i_cFld=" "+;
                  "(RESERIAL,REFLENAB,REPRIORI,REREGSUC,REDESCRI"+;
                  ",REBUSOBJ,REPSTBTN,RETABMST,RETABDTL,RETFLTEX"+;
                  ",RETDELEX,REMSGERC,REGG_LOG,RESAVEDB,REUSEROP"+;
                  ",REKEYGES,REKEYSEL,RECCNADR"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_RESERIAL)+;
                    ","+cp_ToStrODBC(this.w_REFLENAB)+;
                    ","+cp_ToStrODBC(this.w_REPRIORI)+;
                    ","+cp_ToStrODBC(this.w_REREGSUC)+;
                    ","+cp_ToStrODBC(this.w_REDESCRI)+;
                    ","+cp_ToStrODBC(this.w_REBUSOBJ)+;
                    ","+cp_ToStrODBC(this.w_REPSTBTN)+;
                    ","+cp_ToStrODBCNull(this.w_RETABMST)+;
                    ","+cp_ToStrODBCNull(this.w_RETABDTL)+;
                    ","+cp_ToStrODBC(this.w_RETFLTEX)+;
                    ","+cp_ToStrODBC(this.w_RETDELEX)+;
                    ","+cp_ToStrODBC(this.w_REMSGERC)+;
                    ","+cp_ToStrODBC(this.w_REGG_LOG)+;
                    ","+cp_ToStrODBC(this.w_RESAVEDB)+;
                    ","+cp_ToStrODBC(this.w_REUSEROP)+;
                    ","+cp_ToStrODBC(this.w_REKEYGES)+;
                    ","+cp_ToStrODBC(this.w_REKEYSEL)+;
                    ","+cp_ToStrODBC(this.w_RECCNADR)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'REF_MAST')
        i_extval=cp_InsertValVFPExtFlds(this,'REF_MAST')
        cp_CheckDeletedKey(i_cTable,0,'RESERIAL',this.w_RESERIAL)
        INSERT INTO (i_cTable);
              (RESERIAL,REFLENAB,REPRIORI,REREGSUC,REDESCRI,REBUSOBJ,REPSTBTN,RETABMST,RETABDTL,RETFLTEX,RETDELEX,REMSGERC,REGG_LOG,RESAVEDB,REUSEROP,REKEYGES,REKEYSEL,RECCNADR &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_RESERIAL;
                  ,this.w_REFLENAB;
                  ,this.w_REPRIORI;
                  ,this.w_REREGSUC;
                  ,this.w_REDESCRI;
                  ,this.w_REBUSOBJ;
                  ,this.w_REPSTBTN;
                  ,this.w_RETABMST;
                  ,this.w_RETABDTL;
                  ,this.w_RETFLTEX;
                  ,this.w_RETDELEX;
                  ,this.w_REMSGERC;
                  ,this.w_REGG_LOG;
                  ,this.w_RESAVEDB;
                  ,this.w_REUSEROP;
                  ,this.w_REKEYGES;
                  ,this.w_REKEYSEL;
                  ,this.w_RECCNADR;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.REF_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.REF_DETT_IDX,2])
      *
      * insert into REF_DETT
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(RESERIAL,CPROWORD,RE_TABLE,REFLDNAM,REKEYREC"+;
                  ",RERIFREC,REMONINS,REMONMOD,REMONDEL,REFLTEXP"+;
                  ",REBLKINS,REOBBINS,REFLDCOM,REMSGERR,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_RESERIAL)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_RE_TABLE)+","+cp_ToStrODBCNull(this.w_REFLDNAM)+","+cp_ToStrODBC(this.w_REKEYREC)+;
             ","+cp_ToStrODBC(this.w_RERIFREC)+","+cp_ToStrODBC(this.w_REMONINS)+","+cp_ToStrODBC(this.w_REMONMOD)+","+cp_ToStrODBC(this.w_REMONDEL)+","+cp_ToStrODBC(this.w_REFLTEXP)+;
             ","+cp_ToStrODBC(this.w_REBLKINS)+","+cp_ToStrODBC(this.w_REOBBINS)+","+cp_ToStrODBC(this.w_REFLDCOM)+","+cp_ToStrODBC(this.w_REMSGERR)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'RESERIAL',this.w_RESERIAL,'RE_TABLE',this.w_RE_TABLE,'CPROWNUM',this.w_CPROWNUM)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_RESERIAL,this.w_CPROWORD,this.w_RE_TABLE,this.w_REFLDNAM,this.w_REKEYREC"+;
                ",this.w_RERIFREC,this.w_REMONINS,this.w_REMONMOD,this.w_REMONDEL,this.w_REFLTEXP"+;
                ",this.w_REBLKINS,this.w_REOBBINS,this.w_REFLDCOM,this.w_REMSGERR,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.REF_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.REF_MAST_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update REF_MAST
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'REF_MAST')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " REFLENAB="+cp_ToStrODBC(this.w_REFLENAB)+;
             ",REPRIORI="+cp_ToStrODBC(this.w_REPRIORI)+;
             ",REREGSUC="+cp_ToStrODBC(this.w_REREGSUC)+;
             ",REDESCRI="+cp_ToStrODBC(this.w_REDESCRI)+;
             ",REBUSOBJ="+cp_ToStrODBC(this.w_REBUSOBJ)+;
             ",REPSTBTN="+cp_ToStrODBC(this.w_REPSTBTN)+;
             ",RETABMST="+cp_ToStrODBCNull(this.w_RETABMST)+;
             ",RETABDTL="+cp_ToStrODBCNull(this.w_RETABDTL)+;
             ",RETFLTEX="+cp_ToStrODBC(this.w_RETFLTEX)+;
             ",RETDELEX="+cp_ToStrODBC(this.w_RETDELEX)+;
             ",REMSGERC="+cp_ToStrODBC(this.w_REMSGERC)+;
             ",REGG_LOG="+cp_ToStrODBC(this.w_REGG_LOG)+;
             ",RESAVEDB="+cp_ToStrODBC(this.w_RESAVEDB)+;
             ",REUSEROP="+cp_ToStrODBC(this.w_REUSEROP)+;
             ",REKEYGES="+cp_ToStrODBC(this.w_REKEYGES)+;
             ",REKEYSEL="+cp_ToStrODBC(this.w_REKEYSEL)+;
             ",RECCNADR="+cp_ToStrODBC(this.w_RECCNADR)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'REF_MAST')
          i_cWhere = cp_PKFox(i_cTable  ,'RESERIAL',this.w_RESERIAL  )
          UPDATE (i_cTable) SET;
              REFLENAB=this.w_REFLENAB;
             ,REPRIORI=this.w_REPRIORI;
             ,REREGSUC=this.w_REREGSUC;
             ,REDESCRI=this.w_REDESCRI;
             ,REBUSOBJ=this.w_REBUSOBJ;
             ,REPSTBTN=this.w_REPSTBTN;
             ,RETABMST=this.w_RETABMST;
             ,RETABDTL=this.w_RETABDTL;
             ,RETFLTEX=this.w_RETFLTEX;
             ,RETDELEX=this.w_RETDELEX;
             ,REMSGERC=this.w_REMSGERC;
             ,REGG_LOG=this.w_REGG_LOG;
             ,RESAVEDB=this.w_RESAVEDB;
             ,REUSEROP=this.w_REUSEROP;
             ,REKEYGES=this.w_REKEYGES;
             ,REKEYSEL=this.w_REKEYSEL;
             ,RECCNADR=this.w_RECCNADR;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_REFLDNAM))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.REF_DETT_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.REF_DETT_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from REF_DETT
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update REF_DETT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",REFLDNAM="+cp_ToStrODBCNull(this.w_REFLDNAM)+;
                     ",REKEYREC="+cp_ToStrODBC(this.w_REKEYREC)+;
                     ",RERIFREC="+cp_ToStrODBC(this.w_RERIFREC)+;
                     ",REMONINS="+cp_ToStrODBC(this.w_REMONINS)+;
                     ",REMONMOD="+cp_ToStrODBC(this.w_REMONMOD)+;
                     ",REMONDEL="+cp_ToStrODBC(this.w_REMONDEL)+;
                     ",REFLTEXP="+cp_ToStrODBC(this.w_REFLTEXP)+;
                     ",REBLKINS="+cp_ToStrODBC(this.w_REBLKINS)+;
                     ",REOBBINS="+cp_ToStrODBC(this.w_REOBBINS)+;
                     ",REFLDCOM="+cp_ToStrODBC(this.w_REFLDCOM)+;
                     ",REMSGERR="+cp_ToStrODBC(this.w_REMSGERR)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,REFLDNAM=this.w_REFLDNAM;
                     ,REKEYREC=this.w_REKEYREC;
                     ,RERIFREC=this.w_RERIFREC;
                     ,REMONINS=this.w_REMONINS;
                     ,REMONMOD=this.w_REMONMOD;
                     ,REMONDEL=this.w_REMONDEL;
                     ,REFLTEXP=this.w_REFLTEXP;
                     ,REBLKINS=this.w_REBLKINS;
                     ,REOBBINS=this.w_REOBBINS;
                     ,REFLDCOM=this.w_REFLDCOM;
                     ,REMSGERR=this.w_REMSGERR;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask header and footer children to save themselves
      * --- GSCF_MRU : Saving
      this.GSCF_MRU.ChangeRow(this.cRowID+'      1',0;
             ,this.w_RESERIAL,"RUSERIAL";
             )
      this.GSCF_MRU.mReplace()
      * --- GSCF_MOA : Saving
      this.GSCF_MOA.ChangeRow(this.cRowID+'      1',0;
             ,this.w_RESERIAL,"OASERIAL";
             )
      this.GSCF_MOA.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSCF_MRU : Deleting
    this.GSCF_MRU.ChangeRow(this.cRowID+'      1',0;
           ,this.w_RESERIAL,"RUSERIAL";
           )
    this.GSCF_MRU.mDelete()
    * --- GSCF_MOA : Deleting
    this.GSCF_MOA.ChangeRow(this.cRowID+'      1',0;
           ,this.w_RESERIAL,"OASERIAL";
           )
    this.GSCF_MOA.mDelete()
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_REFLDNAM))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.REF_DETT_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.REF_DETT_IDX,2])
        *
        * delete REF_DETT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.REF_MAST_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.REF_MAST_IDX,2])
        *
        * delete REF_MAST
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_REFLDNAM))) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.REF_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.REF_MAST_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,7,.t.)
        if .o_REBUSOBJ<>.w_REBUSOBJ
          .w_REPSTBTN = 'N'
        endif
        if .o_REBUSOBJ<>.w_REBUSOBJ
          .w_RETABMST = IIF(vartype(g_CTFL)="C" AND g_CTFL="S", .w_RETABMST, ICASE(.w_REBUSOBJ="gspr_acn","CAN_TIER",.w_REBUSOBJ="gsut_aid","PROMINDI",""))
          .link_1_9('Full')
        endif
        if .o_REBUSOBJ<>.w_REBUSOBJ
          .w_RETABDTL = IIF(vartype(g_CTFL)="C" AND g_CTFL="S", .w_RETABDTL, ICASE(.w_REBUSOBJ="gspr_mri","RIS_INTE",.w_REBUSOBJ="gspr_msr","RIS_ESTR",.w_REBUSOBJ="gspr_mse","RIS_ESTE",.w_REBUSOBJ="gsut_maa","PRODINDI",""))
          .link_1_10('Full')
        endif
        .DoRTCalc(11,21,.t.)
        if .o_RE_TABLE<>.w_RE_TABLE
          .w_MSTDET = IIF(.w_RE_TABLE=.w_RETABDTL, "D", "M")
        endif
        .DoRTCalc(23,27,.t.)
        if .o_REMONMOD<>.w_REMONMOD
          .w_REFLTEXP = IIF(.w_REMONMOD='S', .w_REFLTEXP , '')
        endif
          .w_TOTPKM = .w_TOTPKM-.w_npkm
          .w_NPKM = IIF(.w_REKEYREC="S" and ALLTRIM(.w_RE_TABLE)==ALLTRIM(.w_RETABMST), 1, 0)
          .w_TOTPKM = .w_TOTPKM+.w_npkm
          .w_TOTPKD = .w_TOTPKD-.w_npkd
          .w_NPKD = IIF(.w_REKEYREC="S" and ALLTRIM(.w_RE_TABLE)==ALLTRIM(.w_RETABDTL), 1, 0)
          .w_TOTPKD = .w_TOTPKD+.w_npkd
        if .o_REBLKINS<>.w_REBLKINS
          .Calculate_ECCVSOTZGW()
        endif
        if .o_REOBBINS<>.w_REOBBINS.or. .o_REKEYREC<>.w_REKEYREC
          .Calculate_QMUDNFLJTA()
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        if .o_REKEYREC<>.w_REKEYREC
          .Calculate_GZCJTBFBIF()
        endif
        if .o_MSTDET<>.w_MSTDET
          .Calculate_MJGVNTAHAZ()
        endif
        .DoRTCalc(31,37,.t.)
          .w_TABLE1 = .w_RETABMST
          .w_TABLE2 = .w_RETABDTL
        if .o_RETABMST<>.w_RETABMST
          .Calculate_BLYXVOHUQE()
        endif
        if .o_RETABDTL<>.w_RETABDTL
          .Calculate_VPQPETJDSR()
        endif
        .DoRTCalc(40,42,.t.)
          .w_TOTROW = .NumRow()
          .w_CTOTROW = alltrim(str(.w_TOTROW,4,0))
          .w_MSGERR = ah_msgformat("Attenzione: sono state inserite %1 righe, l'elaborazione della regola potrebbe provocare %2", .w_CTOTROW, iif(.w_TOTROW>255, "errori", "rallentamenti"))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"SEREG","i_codazi,w_RESERIAL")
          .op_RESERIAL = .w_RESERIAL
        endif
        .op_codazi = .w_codazi
      endwith
      this.DoRTCalc(46,48,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_NPKM with this.w_NPKM
      replace t_NPKD with this.w_NPKD
      replace t_CPROWNUM with this.w_CPROWNUM
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_ECCVSOTZGW()
    with this
          * --- REOBBINS
          .w_REOBBINS = iif(.w_REBLKINS="S", "N", .w_REOBBINS)
    endwith
  endproc
  proc Calculate_QMUDNFLJTA()
    with this
          * --- REBLKINS
          .w_REBLKINS = iif(.w_REOBBINS="S" OR .w_REKEYREC="S", "N", .w_REBLKINS)
          .w_REKEYGES = " "
    endwith
  endproc
  proc Calculate_VOHEAAUIHV()
    with this
          * --- GSCF_BRE(UTE_GRU)
          GSCF_BRE(this;
              ,"UTE_GRU";
             )
    endwith
  endproc
  proc Calculate_GZCJTBFBIF()
    with this
          * --- REMONMOD
          .w_REMONMOD = iif(.w_REKEYREC="S", "N", .w_REMONMOD)
    endwith
  endproc
  proc Calculate_MJGVNTAHAZ()
    with this
          * --- RE_TABLE
          .w_RE_TABLE = iif(.w_MSTDET="D", .w_RETABDTL, .w_RETABMST)
          .w_REFLDNAM = " "
          .w_REFLDCOM = " "
    endwith
  endproc
  proc Calculate_WACXLNCAQX()
    with this
          * --- GSCF_BRE(DELST)
          GSCF_BRE(this;
              ,"DELST";
             )
    endwith
  endproc
  proc Calculate_TVIAIQPHDY()
    with this
          * --- GSCF_BRE(DELRST)
          GSCF_BRE(this;
              ,"DELRST";
             )
    endwith
  endproc
  proc Calculate_BLYXVOHUQE()
    with this
          * --- GSCF_BRE(TABMST)
          GSCF_BRE(this;
              ,"TABMST";
             )
    endwith
  endproc
  proc Calculate_VPQPETJDSR()
    with this
          * --- GSCF_BRE(TABDTL)
          GSCF_BRE(this;
              ,"TABDTL";
             )
    endwith
  endproc
  proc Calculate_ZWFAASOQBK()
    with this
          * --- GSCF_BRE(NEWROW)
     if .w_SHOWALERT
      if .w_TOTROW=.w_WARNROWS and .w_WARNROWS<255
          .w_VOID = ah_errormsg("Attenzione: sono state superate le %1 righe inserite, l'elaborazione della regola potrebbe provocare rallentamenti",,,alltrim(str(.w_WARNROWS-1,4,0)))
      endif
      if .w_TOTROW=255
          .w_VOID = ah_errormsg("Attenzione: sono state superate le 255 righe inserite, l'elaborazione della regola potrebbe provocare errori")
      endif
     endif
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oRETABMST_1_9.enabled = this.oPgFrm.Page1.oPag.oRETABMST_1_9.mCond()
    this.oPgFrm.Page1.oPag.oRETABDTL_1_10.enabled = this.oPgFrm.Page1.oPag.oRETABDTL_1_10.mCond()
    this.oPgFrm.Page1.oPag.oREUSEROP_1_16.enabled = this.oPgFrm.Page1.oPag.oREUSEROP_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_38.enabled = this.oPgFrm.Page1.oPag.oBtn_1_38.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_47.enabled = this.oPgFrm.Page1.oPag.oBtn_1_47.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oREFLDNAM_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oREFLDNAM_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMSTDET_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMSTDET_2_4.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oREMONINS_2_7.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oREMONINS_2_7.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oREMONMOD_2_8.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oREMONMOD_2_8.mCond()
    this.oPgFrm.Page1.oPag.oREFLTEXP_2_10.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oREFLTEXP_2_10.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oREBLKINS_2_13.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oREBLKINS_2_13.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oREBUSOBJ_1_6.visible=!this.oPgFrm.Page1.oPag.oREBUSOBJ_1_6.mHide()
    this.oPgFrm.Page1.oPag.oREBUSOBJ_1_7.visible=!this.oPgFrm.Page1.oPag.oREBUSOBJ_1_7.mHide()
    this.oPgFrm.Page1.oPag.oREPSTBTN_1_8.visible=!this.oPgFrm.Page1.oPag.oREPSTBTN_1_8.mHide()
    this.oPgFrm.Page1.oPag.oREKEYSEL_1_42.visible=!this.oPgFrm.Page1.oPag.oREKEYSEL_1_42.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_43.visible=!this.oPgFrm.Page1.oPag.oStr_1_43.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_47.visible=!this.oPgFrm.Page1.oPag.oBtn_1_47.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Insert children start") or lower(cEvent)==lower("Record Inserted")
          .Calculate_VOHEAAUIHV()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
        if lower(cEvent)==lower("Delete start")
          .Calculate_WACXLNCAQX()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Delete row start")
          .Calculate_TVIAIQPHDY()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init Row")
          .Calculate_ZWFAASOQBK()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=RETABMST
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.XDC_TABLE_IDX,3]
    i_lTable = "XDC_TABLE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2], .t., this.XDC_TABLE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RETABMST) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'XDC_TABLE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TBNAME like "+cp_ToStrODBC(trim(this.w_RETABMST)+"%");

          i_ret=cp_SQL(i_nConn,"select TBNAME,TBCOMMENT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TBNAME","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TBNAME',trim(this.w_RETABMST))
          select TBNAME,TBCOMMENT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TBNAME into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RETABMST)==trim(_Link_.TBNAME) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TBCOMMENT like "+cp_ToStrODBC(trim(this.w_RETABMST)+"%");

            i_ret=cp_SQL(i_nConn,"select TBNAME,TBCOMMENT";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TBCOMMENT like "+cp_ToStr(trim(this.w_RETABMST)+"%");

            select TBNAME,TBCOMMENT;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_RETABMST) and !this.bDontReportError
            deferred_cp_zoom('XDC_TABLE','*','TBNAME',cp_AbsName(oSource.parent,'oRETABMST_1_9'),i_cWhere,'',"Tabelle",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,TBCOMMENT";
                     +" from "+i_cTable+" "+i_lTable+" where TBNAME="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',oSource.xKey(1))
            select TBNAME,TBCOMMENT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RETABMST)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,TBCOMMENT";
                   +" from "+i_cTable+" "+i_lTable+" where TBNAME="+cp_ToStrODBC(this.w_RETABMST);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',this.w_RETABMST)
            select TBNAME,TBCOMMENT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RETABMST = NVL(_Link_.TBNAME,space(20))
      this.w_TBCOMMENT = NVL(_Link_.TBCOMMENT,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_RETABMST = space(20)
      endif
      this.w_TBCOMMENT = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])+'\'+cp_ToStr(_Link_.TBNAME,1)
      cp_ShowWarn(i_cKey,this.XDC_TABLE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RETABMST Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_9(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.XDC_TABLE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_9.TBNAME as TBNAME109"+ ",link_1_9.TBCOMMENT as TBCOMMENT109"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_9 on REF_MAST.RETABMST=link_1_9.TBNAME"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_9"
          i_cKey=i_cKey+'+" and REF_MAST.RETABMST=link_1_9.TBNAME(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=RETABDTL
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.XDC_TABLE_IDX,3]
    i_lTable = "XDC_TABLE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2], .t., this.XDC_TABLE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RETABDTL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'XDC_TABLE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TBNAME like "+cp_ToStrODBC(trim(this.w_RETABDTL)+"%");

          i_ret=cp_SQL(i_nConn,"select TBNAME,TBCOMMENT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TBNAME","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TBNAME',trim(this.w_RETABDTL))
          select TBNAME,TBCOMMENT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TBNAME into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RETABDTL)==trim(_Link_.TBNAME) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TBCOMMENT like "+cp_ToStrODBC(trim(this.w_RETABDTL)+"%");

            i_ret=cp_SQL(i_nConn,"select TBNAME,TBCOMMENT";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TBCOMMENT like "+cp_ToStr(trim(this.w_RETABDTL)+"%");

            select TBNAME,TBCOMMENT;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_RETABDTL) and !this.bDontReportError
            deferred_cp_zoom('XDC_TABLE','*','TBNAME',cp_AbsName(oSource.parent,'oRETABDTL_1_10'),i_cWhere,'',"Tabelle",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,TBCOMMENT";
                     +" from "+i_cTable+" "+i_lTable+" where TBNAME="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',oSource.xKey(1))
            select TBNAME,TBCOMMENT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RETABDTL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,TBCOMMENT";
                   +" from "+i_cTable+" "+i_lTable+" where TBNAME="+cp_ToStrODBC(this.w_RETABDTL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',this.w_RETABDTL)
            select TBNAME,TBCOMMENT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RETABDTL = NVL(_Link_.TBNAME,space(20))
      this.w_TBCOMMDTL = NVL(_Link_.TBCOMMENT,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_RETABDTL = space(20)
      endif
      this.w_TBCOMMDTL = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])+'\'+cp_ToStr(_Link_.TBNAME,1)
      cp_ShowWarn(i_cKey,this.XDC_TABLE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RETABDTL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_10(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.XDC_TABLE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_10.TBNAME as TBNAME110"+ ",link_1_10.TBCOMMENT as TBCOMMENT110"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_10 on REF_MAST.RETABDTL=link_1_10.TBNAME"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_10"
          i_cKey=i_cKey+'+" and REF_MAST.RETABDTL=link_1_10.TBNAME(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=REFLDNAM
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.XDC_FIELDS_IDX,3]
    i_lTable = "XDC_FIELDS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2], .t., this.XDC_FIELDS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_REFLDNAM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'XDC_FIELDS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FLNAME like "+cp_ToStrODBC(trim(this.w_REFLDNAM)+"%");
                   +" and TBNAME="+cp_ToStrODBC(this.w_RE_TABLE);

          i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLCOMMEN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TBNAME,FLNAME","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TBNAME',this.w_RE_TABLE;
                     ,'FLNAME',trim(this.w_REFLDNAM))
          select TBNAME,FLNAME,FLCOMMEN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TBNAME,FLNAME into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_REFLDNAM)==trim(_Link_.FLNAME) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" FLCOMMEN like "+cp_ToStrODBC(trim(this.w_REFLDNAM)+"%");
                   +" and TBNAME="+cp_ToStrODBC(this.w_RE_TABLE);

            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLCOMMEN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" FLCOMMEN like "+cp_ToStr(trim(this.w_REFLDNAM)+"%");
                   +" and TBNAME="+cp_ToStr(this.w_RE_TABLE);

            select TBNAME,FLNAME,FLCOMMEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_REFLDNAM) and !this.bDontReportError
            deferred_cp_zoom('XDC_FIELDS','*','TBNAME,FLNAME',cp_AbsName(oSource.parent,'oREFLDNAM_2_3'),i_cWhere,'',"Campi",'gscf_mre.XDC_FIELDS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_RE_TABLE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLCOMMEN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TBNAME,FLNAME,FLCOMMEN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLCOMMEN";
                     +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TBNAME="+cp_ToStrODBC(this.w_RE_TABLE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',oSource.xKey(1);
                       ,'FLNAME',oSource.xKey(2))
            select TBNAME,FLNAME,FLCOMMEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_REFLDNAM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLCOMMEN";
                   +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(this.w_REFLDNAM);
                   +" and TBNAME="+cp_ToStrODBC(this.w_RE_TABLE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',this.w_RE_TABLE;
                       ,'FLNAME',this.w_REFLDNAM)
            select TBNAME,FLNAME,FLCOMMEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_REFLDNAM = NVL(_Link_.FLNAME,space(30))
      this.w_REFLDCOM = NVL(_Link_.FLCOMMEN,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_REFLDNAM = space(30)
      endif
      this.w_REFLDCOM = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])+'\'+cp_ToStr(_Link_.TBNAME,1)+'\'+cp_ToStr(_Link_.FLNAME,1)
      cp_ShowWarn(i_cKey,this.XDC_FIELDS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_REFLDNAM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oRESERIAL_1_1.value==this.w_RESERIAL)
      this.oPgFrm.Page1.oPag.oRESERIAL_1_1.value=this.w_RESERIAL
    endif
    if not(this.oPgFrm.Page1.oPag.oREFLENAB_1_2.RadioValue()==this.w_REFLENAB)
      this.oPgFrm.Page1.oPag.oREFLENAB_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oREPRIORI_1_3.value==this.w_REPRIORI)
      this.oPgFrm.Page1.oPag.oREPRIORI_1_3.value=this.w_REPRIORI
    endif
    if not(this.oPgFrm.Page1.oPag.oREREGSUC_1_4.RadioValue()==this.w_REREGSUC)
      this.oPgFrm.Page1.oPag.oREREGSUC_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oREDESCRI_1_5.value==this.w_REDESCRI)
      this.oPgFrm.Page1.oPag.oREDESCRI_1_5.value=this.w_REDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oREBUSOBJ_1_6.RadioValue()==this.w_REBUSOBJ)
      this.oPgFrm.Page1.oPag.oREBUSOBJ_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oREBUSOBJ_1_7.value==this.w_REBUSOBJ)
      this.oPgFrm.Page1.oPag.oREBUSOBJ_1_7.value=this.w_REBUSOBJ
    endif
    if not(this.oPgFrm.Page1.oPag.oREPSTBTN_1_8.RadioValue()==this.w_REPSTBTN)
      this.oPgFrm.Page1.oPag.oREPSTBTN_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRETABMST_1_9.value==this.w_RETABMST)
      this.oPgFrm.Page1.oPag.oRETABMST_1_9.value=this.w_RETABMST
    endif
    if not(this.oPgFrm.Page1.oPag.oRETABDTL_1_10.value==this.w_RETABDTL)
      this.oPgFrm.Page1.oPag.oRETABDTL_1_10.value=this.w_RETABDTL
    endif
    if not(this.oPgFrm.Page1.oPag.oRETFLTEX_1_11.value==this.w_RETFLTEX)
      this.oPgFrm.Page1.oPag.oRETFLTEX_1_11.value=this.w_RETFLTEX
    endif
    if not(this.oPgFrm.Page1.oPag.oRETDELEX_1_12.value==this.w_RETDELEX)
      this.oPgFrm.Page1.oPag.oRETDELEX_1_12.value=this.w_RETDELEX
    endif
    if not(this.oPgFrm.Page1.oPag.oREMSGERC_1_13.value==this.w_REMSGERC)
      this.oPgFrm.Page1.oPag.oREMSGERC_1_13.value=this.w_REMSGERC
    endif
    if not(this.oPgFrm.Page1.oPag.oREGG_LOG_1_14.value==this.w_REGG_LOG)
      this.oPgFrm.Page1.oPag.oREGG_LOG_1_14.value=this.w_REGG_LOG
    endif
    if not(this.oPgFrm.Page1.oPag.oRESAVEDB_1_15.RadioValue()==this.w_RESAVEDB)
      this.oPgFrm.Page1.oPag.oRESAVEDB_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oREUSEROP_1_16.value==this.w_REUSEROP)
      this.oPgFrm.Page1.oPag.oREUSEROP_1_16.value=this.w_REUSEROP
    endif
    if not(this.oPgFrm.Page1.oPag.oRE_TABLE_2_2.value==this.w_RE_TABLE)
      this.oPgFrm.Page1.oPag.oRE_TABLE_2_2.value=this.w_RE_TABLE
      replace t_RE_TABLE with this.oPgFrm.Page1.oPag.oRE_TABLE_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oREFLTEXP_2_10.value==this.w_REFLTEXP)
      this.oPgFrm.Page1.oPag.oREFLTEXP_2_10.value=this.w_REFLTEXP
      replace t_REFLTEXP with this.oPgFrm.Page1.oPag.oREFLTEXP_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oREFLDCOM_2_19.value==this.w_REFLDCOM)
      this.oPgFrm.Page1.oPag.oREFLDCOM_2_19.value=this.w_REFLDCOM
      replace t_REFLDCOM with this.oPgFrm.Page1.oPag.oREFLDCOM_2_19.value
    endif
    if not(this.oPgFrm.Page1.oPag.oREKEYSEL_1_42.value==this.w_REKEYSEL)
      this.oPgFrm.Page1.oPag.oREKEYSEL_1_42.value=this.w_REKEYSEL
    endif
    if not(this.oPgFrm.Page1.oPag.oREMSGERR_2_21.value==this.w_REMSGERR)
      this.oPgFrm.Page1.oPag.oREMSGERR_2_21.value=this.w_REMSGERR
      replace t_REMSGERR with this.oPgFrm.Page1.oPag.oREMSGERR_2_21.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREFLDNAM_2_3.value==this.w_REFLDNAM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREFLDNAM_2_3.value=this.w_REFLDNAM
      replace t_REFLDNAM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREFLDNAM_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMSTDET_2_4.RadioValue()==this.w_MSTDET)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMSTDET_2_4.SetRadio()
      replace t_MSTDET with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMSTDET_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREKEYREC_2_5.RadioValue()==this.w_REKEYREC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREKEYREC_2_5.SetRadio()
      replace t_REKEYREC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREKEYREC_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRERIFREC_2_6.RadioValue()==this.w_RERIFREC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRERIFREC_2_6.SetRadio()
      replace t_RERIFREC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRERIFREC_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREMONINS_2_7.RadioValue()==this.w_REMONINS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREMONINS_2_7.SetRadio()
      replace t_REMONINS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREMONINS_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREMONMOD_2_8.RadioValue()==this.w_REMONMOD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREMONMOD_2_8.SetRadio()
      replace t_REMONMOD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREMONMOD_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREMONDEL_2_9.RadioValue()==this.w_REMONDEL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREMONDEL_2_9.SetRadio()
      replace t_REMONDEL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREMONDEL_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREBLKINS_2_13.RadioValue()==this.w_REBLKINS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREBLKINS_2_13.SetRadio()
      replace t_REBLKINS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREBLKINS_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREOBBINS_2_14.RadioValue()==this.w_REOBBINS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREOBBINS_2_14.SetRadio()
      replace t_REOBBINS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREOBBINS_2_14.value
    endif
    cp_SetControlsValueExtFlds(this,'REF_MAST')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        if not(.w_TOTROW<min(.w_WARNROWS,255))
           if (!cp_WarningMsg(""+.w_MSGERR+""))
             return .f.
           endif
        endif
        do case
          case not(!EMPTY(.w_RETABMST) and .w_TOTPKM>0 or !EMPTY(.w_RETABDTL) and .w_TOTPKD>0)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = cp_Translate(thisform.msgFmt("Impostare almeno una tabella ed un campo chiave"))
          case   (empty(.w_RESERIAL))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oRESERIAL_1_1.SetFocus()
            i_bnoObbl = !empty(.w_RESERIAL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_REBUSOBJ))  and not(vartype(g_CTFL)="C" AND g_CTFL="S")
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oREBUSOBJ_1_6.SetFocus()
            i_bnoObbl = !empty(.w_REBUSOBJ)
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      *i_bRes = i_bRes .and. .GSCF_MRU.CheckForm()
      if i_bres
        i_bres=  .GSCF_MRU.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      *i_bRes = i_bRes .and. .GSCF_MOA.CheckForm()
      if i_bres
        i_bres=  .GSCF_MOA.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gscf_mre
      if i_bres
         i_bres=GSCF_BRE(THIS,"CHKFORM")
      endif
      
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_REFLDNAM))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_REBUSOBJ = this.w_REBUSOBJ
    this.o_RETABMST = this.w_RETABMST
    this.o_RETABDTL = this.w_RETABDTL
    this.o_RE_TABLE = this.w_RE_TABLE
    this.o_MSTDET = this.w_MSTDET
    this.o_REKEYREC = this.w_REKEYREC
    this.o_REMONMOD = this.w_REMONMOD
    this.o_REBLKINS = this.w_REBLKINS
    this.o_REOBBINS = this.w_REOBBINS
    * --- GSCF_MRU : Depends On
    this.GSCF_MRU.SaveDependsOn()
    * --- GSCF_MOA : Depends On
    this.GSCF_MOA.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_REFLDNAM)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(999999,cp_maxroword()+10)
      .w_RE_TABLE=space(20)
      .w_REFLDNAM=space(30)
      .w_MSTDET=space(1)
      .w_REKEYREC=space(1)
      .w_RERIFREC=space(1)
      .w_REMONINS=space(1)
      .w_REMONMOD=space(1)
      .w_REMONDEL=space(1)
      .w_REFLTEXP=space(254)
      .w_NPKM=0
      .w_NPKD=0
      .w_REBLKINS=space(1)
      .w_REOBBINS=space(1)
      .w_REFLDCOM=space(254)
      .w_REMSGERR=space(254)
      .DoRTCalc(1,19,.f.)
        .w_RE_TABLE = IIF(EMPTY(.w_RETABMST), .w_RETABDTL, .w_RETABMST)
      .DoRTCalc(21,21,.f.)
      if not(empty(.w_REFLDNAM))
        .link_2_3('Full')
      endif
        .w_MSTDET = IIF(.w_RE_TABLE=.w_RETABDTL, "D", "M")
        .w_REKEYREC = 'N'
        .w_RERIFREC = 'N'
        .w_REMONINS = 'N'
        .w_REMONMOD = 'N'
        .w_REMONDEL = 'N'
        .w_REFLTEXP = IIF(.w_REMONMOD='S', .w_REFLTEXP , '')
        .w_NPKM = IIF(.w_REKEYREC="S" and ALLTRIM(.w_RE_TABLE)==ALLTRIM(.w_RETABMST), 1, 0)
        .w_NPKD = IIF(.w_REKEYREC="S" and ALLTRIM(.w_RE_TABLE)==ALLTRIM(.w_RETABDTL), 1, 0)
        .w_REBLKINS = 'N'
        .w_REOBBINS = 'N'
    endwith
    this.DoRTCalc(33,48,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_RE_TABLE = t_RE_TABLE
    this.w_REFLDNAM = t_REFLDNAM
    this.w_MSTDET = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMSTDET_2_4.RadioValue(.t.)
    this.w_REKEYREC = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREKEYREC_2_5.RadioValue(.t.)
    this.w_RERIFREC = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRERIFREC_2_6.RadioValue(.t.)
    this.w_REMONINS = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREMONINS_2_7.RadioValue(.t.)
    this.w_REMONMOD = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREMONMOD_2_8.RadioValue(.t.)
    this.w_REMONDEL = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREMONDEL_2_9.RadioValue(.t.)
    this.w_REFLTEXP = t_REFLTEXP
    this.w_NPKM = t_NPKM
    this.w_NPKD = t_NPKD
    this.w_REBLKINS = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREBLKINS_2_13.RadioValue(.t.)
    this.w_REOBBINS = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREOBBINS_2_14.RadioValue(.t.)
    this.w_REFLDCOM = t_REFLDCOM
    this.w_CPROWNUM = t_CPROWNUM
    this.w_REMSGERR = t_REMSGERR
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_RE_TABLE with this.w_RE_TABLE
    replace t_REFLDNAM with this.w_REFLDNAM
    replace t_MSTDET with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMSTDET_2_4.ToRadio()
    replace t_REKEYREC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREKEYREC_2_5.ToRadio()
    replace t_RERIFREC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRERIFREC_2_6.ToRadio()
    replace t_REMONINS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREMONINS_2_7.ToRadio()
    replace t_REMONMOD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREMONMOD_2_8.ToRadio()
    replace t_REMONDEL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREMONDEL_2_9.ToRadio()
    replace t_REFLTEXP with this.w_REFLTEXP
    replace t_NPKM with this.w_NPKM
    replace t_NPKD with this.w_NPKD
    replace t_REBLKINS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREBLKINS_2_13.ToRadio()
    replace t_REOBBINS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREOBBINS_2_14.ToRadio()
    replace t_REFLDCOM with this.w_REFLDCOM
    replace t_CPROWNUM with this.w_CPROWNUM
    replace t_REMSGERR with this.w_REMSGERR
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTPKM = .w_TOTPKM-.w_npkm
        .w_TOTPKD = .w_TOTPKD-.w_npkd
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgscf_mrePag1 as StdContainer
  Width  = 777
  height = 543
  stdWidth  = 777
  stdheight = 543
  resizeXpos=535
  resizeYpos=428
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oRESERIAL_1_1 as StdField with uid="OLIYDTZNCX",rtseq=1,rtrep=.f.,;
    cFormVar = "w_RESERIAL", cQueryName = "RESERIAL",nZero=10,;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Seriale regola",;
    HelpContextID = 127944034,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=87, Top=14, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10)

  add object oREFLENAB_1_2 as StdCheck with uid="IDVBKNNOTW",rtseq=2,rtrep=.f.,left=167, top=16, caption="Regola attiva",;
    ToolTipText = "Se attivo: regola abilitata",;
    HelpContextID = 198604120,;
    cFormVar="w_REFLENAB", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oREFLENAB_1_2.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..REFLENAB,&i_cF..t_REFLENAB),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oREFLENAB_1_2.GetRadio()
    this.Parent.oContained.w_REFLENAB = this.RadioValue()
    return .t.
  endfunc

  func oREFLENAB_1_2.ToRadio()
    this.Parent.oContained.w_REFLENAB=trim(this.Parent.oContained.w_REFLENAB)
    return(;
      iif(this.Parent.oContained.w_REFLENAB=='S',1,;
      0))
  endfunc

  func oREFLENAB_1_2.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oREPRIORI_1_3 as StdField with uid="IXRURWKODX",rtseq=3,rtrep=.f.,;
    cFormVar = "w_REPRIORI", cQueryName = "REPRIORI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Priorit� di elaborazione delle regole (9999 = prima regola elaborata)",;
    HelpContextID = 220009823,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=352, Top=14, cSayPict='"9999"', cGetPict='"9999"'

  add object oREREGSUC_1_4 as StdCheck with uid="JQZLLRDBWC",rtseq=4,rtrep=.f.,left=396, top=16, caption="Esamina regola successiva",;
    ToolTipText = "Se attivo esamina regola successiva, altrimenti non esamina ulteriori regole",;
    HelpContextID = 15742297,;
    cFormVar="w_REREGSUC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oREREGSUC_1_4.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..REREGSUC,&i_cF..t_REREGSUC),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oREREGSUC_1_4.GetRadio()
    this.Parent.oContained.w_REREGSUC = this.RadioValue()
    return .t.
  endfunc

  func oREREGSUC_1_4.ToRadio()
    this.Parent.oContained.w_REREGSUC=trim(this.Parent.oContained.w_REREGSUC)
    return(;
      iif(this.Parent.oContained.w_REREGSUC=='S',1,;
      0))
  endfunc

  func oREREGSUC_1_4.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oREDESCRI_1_5 as StdField with uid="BTUXOWCUWO",rtseq=5,rtrep=.f.,;
    cFormVar = "w_REDESCRI", cQueryName = "REDESCRI",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione regola",;
    HelpContextID = 28267871,;
   bGlobalFont=.t.,;
    Height=21, Width=468, Left=87, Top=39, InputMask=replicate('X',50)


  add object oREBUSOBJ_1_6 as StdCombo with uid="YKHQJPXYNL",rtseq=6,rtrep=.f.,left=657,top=13,width=118,height=21;
    , ToolTipText = "Programma associato alla regola";
    , HelpContextID = 230634848;
    , cFormVar="w_REBUSOBJ",RowSource=""+"GSPR_ACN,"+"GSPR_MSE,"+"GSPR_MRI,"+"GSPR_MSR,"+"GSUT_AID,"+"GSUT_MAA", bObbl = .t. , nPag = 1;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oREBUSOBJ_1_6.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..REBUSOBJ,&i_cF..t_REBUSOBJ),this.value)
    return(iif(xVal =1,"gspr_acn",;
    iif(xVal =2,"gspr_mse",;
    iif(xVal =3,"gspr_mri",;
    iif(xVal =4,"gspr_msr",;
    iif(xVal =5,"gsut_aid",;
    iif(xVal =6,"gsut_maa",;
    space(15))))))))
  endfunc
  func oREBUSOBJ_1_6.GetRadio()
    this.Parent.oContained.w_REBUSOBJ = this.RadioValue()
    return .t.
  endfunc

  func oREBUSOBJ_1_6.ToRadio()
    this.Parent.oContained.w_REBUSOBJ=trim(this.Parent.oContained.w_REBUSOBJ)
    return(;
      iif(this.Parent.oContained.w_REBUSOBJ=="gspr_acn",1,;
      iif(this.Parent.oContained.w_REBUSOBJ=="gspr_mse",2,;
      iif(this.Parent.oContained.w_REBUSOBJ=="gspr_mri",3,;
      iif(this.Parent.oContained.w_REBUSOBJ=="gspr_msr",4,;
      iif(this.Parent.oContained.w_REBUSOBJ=="gsut_aid",5,;
      iif(this.Parent.oContained.w_REBUSOBJ=="gsut_maa",6,;
      0)))))))
  endfunc

  func oREBUSOBJ_1_6.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oREBUSOBJ_1_6.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (vartype(g_CTFL)="C" AND g_CTFL="S")
    endwith
    endif
  endfunc

  add object oREBUSOBJ_1_7 as StdField with uid="CCPPRYQMQP",rtseq=7,rtrep=.f.,;
    cFormVar = "w_REBUSOBJ", cQueryName = "REBUSOBJ",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Programma associato alla regola",;
    HelpContextID = 230634848,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=657, Top=13, InputMask=replicate('X',15)

  func oREBUSOBJ_1_7.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (vartype(g_CTFL)<>"C" or g_CTFL<>"S")
    endwith
    endif
  endfunc

  add object oREPSTBTN_1_8 as StdCheck with uid="VVZQOYMXHR",rtseq=8,rtrep=.f.,left=652, top=36, caption="Bottone su post-IN",;
    ToolTipText = "Se attivo: aggiunge il bottone <Visualizza> su post-IN",;
    HelpContextID = 13505892,;
    cFormVar="w_REPSTBTN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oREPSTBTN_1_8.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..REPSTBTN,&i_cF..t_REPSTBTN),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oREPSTBTN_1_8.GetRadio()
    this.Parent.oContained.w_REPSTBTN = this.RadioValue()
    return .t.
  endfunc

  func oREPSTBTN_1_8.ToRadio()
    this.Parent.oContained.w_REPSTBTN=trim(this.Parent.oContained.w_REPSTBTN)
    return(;
      iif(this.Parent.oContained.w_REPSTBTN=='S',1,;
      0))
  endfunc

  func oREPSTBTN_1_8.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oREPSTBTN_1_8.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_REBUSOBJ))
    endwith
    endif
  endfunc

  add object oRETABMST_1_9 as StdField with uid="OXASHYPOPI",rtseq=9,rtrep=.f.,;
    cFormVar = "w_RETABMST", cQueryName = "RETABMST",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Tabella master",;
    HelpContextID = 178017642,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=656, Top=64, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="XDC_TABLE", oKey_1_1="TBNAME", oKey_1_2="this.w_RETABMST"

  func oRETABMST_1_9.mCond()
    with this.Parent.oContained
      return (vartype(g_CTFL)="C" AND g_CTFL="S"  and .cFunction<>"Edit")
    endwith
  endfunc

  func oRETABMST_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oRETABMST_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRETABMST_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'XDC_TABLE','*','TBNAME',cp_AbsName(this.parent,'oRETABMST_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Tabelle",'',this.parent.oContained
  endproc

  add object oRETABDTL_1_10 as StdField with uid="NAYWHBASAP",rtseq=10,rtrep=.f.,;
    cFormVar = "w_RETABDTL", cQueryName = "RETABDTL",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Tabella detail",;
    HelpContextID = 27022690,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=656, Top=89, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="XDC_TABLE", oKey_1_1="TBNAME", oKey_1_2="this.w_RETABDTL"

  func oRETABDTL_1_10.mCond()
    with this.Parent.oContained
      return (vartype(g_CTFL)="C" AND g_CTFL="S" and .cFunction<>"Edit")
    endwith
  endfunc

  func oRETABDTL_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oRETABDTL_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRETABDTL_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'XDC_TABLE','*','TBNAME',cp_AbsName(this.parent,'oRETABDTL_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Tabelle",'',this.parent.oContained
  endproc

  add object oRETFLTEX_1_11 as StdField with uid="LZPGBXWYLG",rtseq=11,rtrep=.f.,;
    cFormVar = "w_RETFLTEX", cQueryName = "RETFLTEX",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Espressione di filtro regola",;
    HelpContextID = 37836142,;
   bGlobalFont=.t.,;
    Height=21, Width=468, Left=87, Top=64, InputMask=replicate('X',254), bHasZoom = .t. 

  proc oRETFLTEX_1_11.mZoom
      with this.Parent.oContained
        GSCF_BRE(this.Parent.oContained,"ZOOMFILTER", "RETFLTEX")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oRETDELEX_1_12 as StdField with uid="JLKUQEMHCC",rtseq=12,rtrep=.f.,;
    cFormVar = "w_RETDELEX", cQueryName = "RETDELEX",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Espressione di blocco cancellazione: se verificata impedisce  l'eliminazione",;
    HelpContextID = 164582766,;
   bGlobalFont=.t.,;
    Height=21, Width=468, Left=87, Top=89, InputMask=replicate('X',254), bHasZoom = .t. 

  proc oRETDELEX_1_12.mZoom
      with this.Parent.oContained
        GSCF_BRE(this.Parent.oContained,"ZOOMFILTER", "RETDELEX")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oREMSGERC_1_13 as StdField with uid="XVHUQAZKBU",rtseq=13,rtrep=.f.,;
    cFormVar = "w_REMSGERC", cQueryName = "REMSGERC",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Messaggio di errore generico della regola",;
    HelpContextID = 50193753,;
   bGlobalFont=.t.,;
    Height=21, Width=687, Left=87, Top=114, InputMask=replicate('X',254)

  add object oREGG_LOG_1_14 as StdField with uid="ATFNBKOPDJ",rtseq=14,rtrep=.f.,;
    cFormVar = "w_REGG_LOG", cQueryName = "REGG_LOG",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorni di permanenza nel log",;
    HelpContextID = 191989085,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=87, Top=139, cSayPict='"9999"', cGetPict='"9999"'

  add object oRESAVEDB_1_15 as StdCheck with uid="MJNCEODAAN",rtseq=15,rtrep=.f.,left=16, top=184, caption="Database",;
    ToolTipText = "Salva informazioni su database",;
    HelpContextID = 64767320,;
    cFormVar="w_RESAVEDB", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oRESAVEDB_1_15.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..RESAVEDB,&i_cF..t_RESAVEDB),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oRESAVEDB_1_15.GetRadio()
    this.Parent.oContained.w_RESAVEDB = this.RadioValue()
    return .t.
  endfunc

  func oRESAVEDB_1_15.ToRadio()
    this.Parent.oContained.w_RESAVEDB=trim(this.Parent.oContained.w_RESAVEDB)
    return(;
      iif(this.Parent.oContained.w_RESAVEDB=='S',1,;
      0))
  endfunc

  func oRESAVEDB_1_15.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oREUSEROP_1_16 as StdField with uid="DHOWMZBGRT",rtseq=16,rtrep=.f.,;
    cFormVar = "w_REUSEROP", cQueryName = "REUSEROP",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "esegue una procedura personalizzata",;
    HelpContextID = 266233190,;
   bGlobalFont=.t.,;
    Height=21, Width=209, Left=10, Top=222, InputMask=replicate('X',30)

  func oREUSEROP_1_16.mCond()
    with this.Parent.oContained
      return (.w_RESAVEDB<>"S")
    endwith
  endfunc


  add object oLinkPC_1_17 as stdDynamicChildContainer with uid="WMFGDFPUWR",left=224, top=163, width=552, height=141, bOnScreen=.t.;



  add object oBtn_1_18 as StdButton with uid="IYPJCAFGSD",left=20, top=254, width=48,height=45,;
    CpPicture="BMP\INS_RAP.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per avviare il caricamento rapido delle righe";
    , HelpContextID = 21118504;
    , Caption='Car.\<Rap.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      do GSCF_KCR with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_18.mCond()
    with this.Parent.oContained
      return (!EMPTY(.w_RETABMST) or !EMPTY(.w_RETABDTL))
    endwith
  endfunc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=7, top=305, width=762,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=10,Field1="CPROWORD",Label1="Riga",Field2="MSTDET",Label2="Tabella",Field3="REFLDNAM",Label3="Nome campo",Field4="REKEYREC",Label4="Chiave",Field5="RERIFREC",Label5="Riferimento",Field6="REMONINS",Label6="Inserimento",Field7="REMONMOD",Label7="Modifica",Field8="REMONDEL",Label8="Cancellaz.",Field9="REBLKINS",Label9="Blocca inser.",Field10="REOBBINS",Label10="Inser. obblig.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 202175610


  add object oBtn_1_38 as StdButton with uid="ENBYYXBQNT",left=75, top=254, width=48,height=45,;
    CpPicture="bmp\IMP_LIC.ico", caption="", nPag=1;
    , ToolTipText = "Attiva il flag Chiave in relazione alla chiave primaria della tabella";
    , HelpContextID = 113885962;
    , Caption='Sel. \<ch.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_38.Click()
      with this.Parent.oContained
        GSCF_BRE(this.Parent.oContained,"CHIAVE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_38.mCond()
    with this.Parent.oContained
      return (!EMPTY(.w_RETABMST) or !EMPTY(.w_RETABDTL))
    endwith
  endfunc

  add object oREKEYSEL_1_42 as StdField with uid="YVECBXZOOE",rtseq=36,rtrep=.f.,;
    cFormVar = "w_REKEYSEL", cQueryName = "REKEYSEL",;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Sequenza campi chiave per apertura post-IN",;
    HelpContextID = 34588002,;
   bGlobalFont=.t.,;
    Height=21, Width=249, Left=525, Top=139, InputMask=replicate('X',60), bHasZoom = .t. 

  func oREKEYSEL_1_42.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_REPSTBTN<>"S" OR NVL(.w_REKEYGES="S"," "))
    endwith
    endif
  endfunc

  proc oREKEYSEL_1_42.mZoom
      with this.Parent.oContained
        GSCF_BRE(this.Parent.oContained,"KEYSEL")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc


  add object oBtn_1_47 as StdButton with uid="TPVPWTNJXN",left=130, top=254, width=48,height=45,;
    CpPicture="copy.bmp", caption="", nPag=1;
    , ToolTipText = "Duplica la regola";
    , HelpContextID = 54085174;
    , Caption='\<Duplica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_47.Click()
      with this.Parent.oContained
        GSCF_BRE(this.Parent.oContained,"DUPLICA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_47.mCond()
    with this.Parent.oContained
      return (not empty(.w_RESERIAL))
    endwith
  endfunc

  func oBtn_1_47.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.bsec2  or .cFunction<>"Query")
    endwith
   endif
  endfunc

  add object oStr_1_19 as StdString with uid="GKJHQXYHSK",Visible=.t., Left=7, Top=18,;
    Alignment=1, Width=77, Height=18,;
    Caption="Seriale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="JVWBAZKJLG",Visible=.t., Left=574, Top=16,;
    Alignment=1, Width=80, Height=18,;
    Caption="Programma:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="ZWABZSBCPU",Visible=.t., Left=574, Top=66,;
    Alignment=1, Width=80, Height=18,;
    Caption="Tabella:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="STDZHILZKG",Visible=.t., Left=10, Top=205,;
    Alignment=0, Width=209, Height=18,;
    Caption="Operazione personalizzata"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="HDVRGWRRCD",Visible=.t., Left=7, Top=43,;
    Alignment=1, Width=77, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="ENIDHUQZCC",Visible=.t., Left=7, Top=166,;
    Alignment=0, Width=158, Height=18,;
    Caption="Attiva log su"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="ZSSKRNFUIP",Visible=.t., Left=253, Top=145,;
    Alignment=0, Width=169, Height=18,;
    Caption="Utenti/Gruppi associati"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="KHPIRKCWIE",Visible=.t., Left=556, Top=90,;
    Alignment=1, Width=98, Height=18,;
    Caption="Tabella dettaglio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="CNYVAMIJBT",Visible=.t., Left=270, Top=18,;
    Alignment=1, Width=80, Height=18,;
    Caption="Priorit� regola:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="SJAMUWCTMI",Visible=.t., Left=7, Top=68,;
    Alignment=1, Width=77, Height=18,;
    Caption="Filtro regola:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="BFGAGKZWVR",Visible=.t., Left=7, Top=143,;
    Alignment=1, Width=77, Height=18,;
    Caption="GG validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="JRZGVSRJRI",Visible=.t., Left=7, Top=93,;
    Alignment=1, Width=77, Height=18,;
    Caption="Blocco canc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="RQWPPKBYLL",Visible=.t., Left=8, Top=482,;
    Alignment=1, Width=118, Height=18,;
    Caption="Espressione di filtro:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="WIEDJJWDRW",Visible=.t., Left=8, Top=503,;
    Alignment=1, Width=118, Height=18,;
    Caption="Descrizione campo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="QVCSBCRJFU",Visible=.t., Left=425, Top=143,;
    Alignment=1, Width=98, Height=18,;
    Caption="Sequenza PK:"  ;
  , bGlobalFont=.t.

  func oStr_1_43.mHide()
    with this.Parent.oContained
      return (.w_REPSTBTN<>"S" OR NVL(.w_REKEYGES="S"," "))
    endwith
  endfunc

  add object oStr_1_44 as StdString with uid="QCDNPQKLXL",Visible=.t., Left=8, Top=524,;
    Alignment=1, Width=118, Height=18,;
    Caption="Messaggio di errore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="JKCVQIKYQX",Visible=.t., Left=7, Top=118,;
    Alignment=1, Width=77, Height=18,;
    Caption="Msg. errore:"  ;
  , bGlobalFont=.t.

  add object oBox_1_25 as StdBox with uid="IXIAWRVZHA",left=7, top=183, width=217,height=68
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gscf_mru",lower(this.oContained.GSCF_MRU.class))=0
        this.oContained.GSCF_MRU.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-3,top=323,;
    width=764+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*8*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-2,top=324,width=763+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*8*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='XDC_FIELDS|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oRE_TABLE_2_2.Refresh()
      this.Parent.oREFLTEXP_2_10.Refresh()
      this.Parent.oREFLDCOM_2_19.Refresh()
      this.Parent.oREMSGERR_2_21.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='XDC_FIELDS'
        oDropInto=this.oBodyCol.oRow.oREFLDNAM_2_3
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oRE_TABLE_2_2 as StdTrsField with uid="GEXGJYVCTO",rtseq=20,rtrep=.t.,;
    cFormVar="w_RE_TABLE",value=space(20),enabled=.f.,isprimarykey=.t.,;
    HelpContextID = 6290085,;
    cTotal="", bFixedPos=.t., cQueryName = "RE_TABLE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=656, Top=500, InputMask=replicate('X',20)

  func oRE_TABLE_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_REFLDNAM)
        bRes2=.link_2_3('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oREFLTEXP_2_10 as StdTrsField with uid="GYMOLHEQNT",rtseq=28,rtrep=.t.,;
    cFormVar="w_REFLTEXP",value=space(254),;
    ToolTipText = "Se l'espressione � vera o vuota allora le operazioni sono applicate",;
    HelpContextID = 205097626,;
    cTotal="", bFixedPos=.t., cQueryName = "REFLTEXP",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=645, Left=129, Top=479, InputMask=replicate('X',254), bHasZoom = .t. 

  func oREFLTEXP_2_10.mCond()
    with this.Parent.oContained
      return (.w_REMONMOD $ 'SIO' or 'S' $ .w_REMONINS+.w_REMONDEL+.w_REBLKINS+.w_REOBBINS)
    endwith
  endfunc

  proc oREFLTEXP_2_10.mZoom
      with this.Parent.oContained
        GSCF_BRE(this.Parent.oContained,"ZOOMFILTER", "REFLTEXP")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oREFLDCOM_2_19 as StdTrsField with uid="BKOZUSBMRB",rtseq=33,rtrep=.t.,;
    cFormVar="w_REFLDCOM",value=space(254),;
    ToolTipText = "Descrizione del campo nel log",;
    HelpContextID = 13006179,;
    cTotal="", bFixedPos=.t., cQueryName = "REFLDCOM",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=523, Left=129, Top=500, InputMask=replicate('X',254)

  add object oREMSGERR_2_21 as StdTrsField with uid="LYEUQRNVMB",rtseq=37,rtrep=.t.,;
    cFormVar="w_REMSGERR",value=space(254),;
    ToolTipText = "Messaggio di errore in caso di dati non ammessi",;
    HelpContextID = 50193768,;
    cTotal="", bFixedPos=.t., cQueryName = "REMSGERR",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=645, Left=129, Top=521, InputMask=replicate('X',254)

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

  define class tgscf_mrePag2 as StdContainer
    Width  = 777
    height = 543
    stdWidth  = 777
    stdheight = 543
  resizeXpos=234
  resizeYpos=111
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_4_1 as stdDynamicChildContainer with uid="ZQYZAMKJYI",left=0, top=0, width=779, height=547, bOnScreen=.t.;



  add object oBtn_4_2 as StdButton with uid="WPNJNVSWMQ",left=6, top=498, width=48,height=45,;
    CpPicture="bmp\mail.ico", caption="", nPag=4,tabstop=.f.;
    , ToolTipText = "Premere per aggiungere un indirizzo a cui inviare una e-mail in CCN";
    , HelpContextID = 254229818;
    , Caption='\<mail CCN';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_2.Click()
      do GSCF_KCN with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gscf_moa",lower(this.oContained.GSCF_MOA.class))=0
        this.oContained.GSCF_MOA.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine

* --- Defining Body row
define class tgscf_mreBodyRow as CPBodyRowCnt
  Width=754
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="IIQABDPIEN",rtseq=19,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    ToolTipText = "Numero riga",;
    HelpContextID = 234504042,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=49, Left=-2, Top=0, cSayPict=["999999"], cGetPict=["999999"]

  add object oREFLDNAM_2_3 as StdTrsField with uid="FXTHPQZPCJ",rtseq=21,rtrep=.t.,;
    cFormVar="w_REFLDNAM",value=space(30),;
    ToolTipText = "Nome campo da monitorare",;
    HelpContextID = 197555555,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=111, Left=127, Top=0, InputMask=replicate('X',30), bHasZoom = .t. , cLinkFile="XDC_FIELDS", oKey_1_1="TBNAME", oKey_1_2="this.w_RE_TABLE", oKey_2_1="FLNAME", oKey_2_2="this.w_REFLDNAM"

  func oREFLDNAM_2_3.mCond()
    with this.Parent.oContained
      return ((not EMPTY(.w_RETABMST) or not EMPTY(.w_RETABDTL)) AND .RowStatus()='A')
    endwith
  endfunc

  func oREFLDNAM_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oREFLDNAM_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oREFLDNAM_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.XDC_FIELDS_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TBNAME="+cp_ToStrODBC(this.Parent.oContained.w_RE_TABLE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TBNAME="+cp_ToStr(this.Parent.oContained.w_RE_TABLE)
    endif
    do cp_zoom with 'XDC_FIELDS','*','TBNAME,FLNAME',cp_AbsName(this.parent,'oREFLDNAM_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Campi",'gscf_mre.XDC_FIELDS_VZM',this.parent.oContained
  endproc

  add object oMSTDET_2_4 as StdTrsCombo with uid="ZWPLZIUMRG",rtrep=.t.,;
    cFormVar="w_MSTDET", RowSource=""+"Master,"+"Detail" ,  tabstop=.f.,;
    HelpContextID = 30368454,;
    Height=22, Width=72, Left=51, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oMSTDET_2_4.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..MSTDET,&i_cF..t_MSTDET),this.value)
    return(iif(xVal =1,"M",;
    iif(xVal =2,"D",;
    space(1))))
  endfunc
  func oMSTDET_2_4.GetRadio()
    this.Parent.oContained.w_MSTDET = this.RadioValue()
    return .t.
  endfunc

  func oMSTDET_2_4.ToRadio()
    this.Parent.oContained.w_MSTDET=trim(this.Parent.oContained.w_MSTDET)
    return(;
      iif(this.Parent.oContained.w_MSTDET=="M",1,;
      iif(this.Parent.oContained.w_MSTDET=="D",2,;
      0)))
  endfunc

  func oMSTDET_2_4.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oMSTDET_2_4.mCond()
    with this.Parent.oContained
      return (not EMPTY(.w_RETABMST) and not EMPTY(.w_RETABDTL) and type("i_srv")="C" and i_srv="A")
    endwith
  endfunc

  add object oREKEYREC_2_5 as StdTrsCheck with uid="QFVUEYHEWH",rtrep=.t.,;
    cFormVar="w_REKEYREC",  caption=" ",;
    ToolTipText = "Chiave di identificazione del record (reale o candidata)",;
    HelpContextID = 17810777,;
    Left=244, Top=0, Width=53,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oREKEYREC_2_5.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..REKEYREC,&i_cF..t_REKEYREC),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oREKEYREC_2_5.GetRadio()
    this.Parent.oContained.w_REKEYREC = this.RadioValue()
    return .t.
  endfunc

  func oREKEYREC_2_5.ToRadio()
    this.Parent.oContained.w_REKEYREC=trim(this.Parent.oContained.w_REKEYREC)
    return(;
      iif(this.Parent.oContained.w_REKEYREC=='S',1,;
      0))
  endfunc

  func oREKEYREC_2_5.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oRERIFREC_2_6 as StdTrsCheck with uid="YSQXRLKENV",rtrep=.t.,;
    cFormVar="w_RERIFREC",  caption=" ",;
    ToolTipText = "Riferimento descrittivo del record",;
    HelpContextID = 266614105,;
    Left=305, Top=0, Width=59,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oRERIFREC_2_6.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..RERIFREC,&i_cF..t_RERIFREC),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oRERIFREC_2_6.GetRadio()
    this.Parent.oContained.w_RERIFREC = this.RadioValue()
    return .t.
  endfunc

  func oRERIFREC_2_6.ToRadio()
    this.Parent.oContained.w_RERIFREC=trim(this.Parent.oContained.w_RERIFREC)
    return(;
      iif(this.Parent.oContained.w_RERIFREC=='S',1,;
      0))
  endfunc

  func oRERIFREC_2_6.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oREMONINS_2_7 as StdTrsCheck with uid="NTAJBPYIBH",rtrep=.t.,;
    cFormVar="w_REMONINS",  caption="",;
    ToolTipText = "Se attivo: registra l'operazione di inserimento del dato",;
    HelpContextID = 144054935,;
    Left=372, Top=0, Width=65,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oREMONINS_2_7.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..REMONINS,&i_cF..t_REMONINS),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oREMONINS_2_7.GetRadio()
    this.Parent.oContained.w_REMONINS = this.RadioValue()
    return .t.
  endfunc

  func oREMONINS_2_7.ToRadio()
    this.Parent.oContained.w_REMONINS=trim(this.Parent.oContained.w_REMONINS)
    return(;
      iif(this.Parent.oContained.w_REMONINS=='S',1,;
      0))
  endfunc

  func oREMONINS_2_7.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oREMONINS_2_7.mCond()
    with this.Parent.oContained
      return (.w_REBLKINS='N')
    endwith
  endfunc

  add object oREMONMOD_2_8 as StdTrsCombo with uid="ZJTDSDIPSI",rtrep=.t.,;
    cFormVar="w_REMONMOD", RowSource=""+"Ignora,"+"Rileva,"+"Blocca,"+"Obbligatorio" , ;
    ToolTipText = "Regola di modifica del dato",;
    HelpContextID = 191489370,;
    Height=21, Width=92, Left=445, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oREMONMOD_2_8.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..REMONMOD,&i_cF..t_REMONMOD),this.value)
    return(iif(xVal =1,'N',;
    iif(xVal =2,'S',;
    iif(xVal =3,'I',;
    iif(xVal =4,'O',;
    space(1))))))
  endfunc
  func oREMONMOD_2_8.GetRadio()
    this.Parent.oContained.w_REMONMOD = this.RadioValue()
    return .t.
  endfunc

  func oREMONMOD_2_8.ToRadio()
    this.Parent.oContained.w_REMONMOD=trim(this.Parent.oContained.w_REMONMOD)
    return(;
      iif(this.Parent.oContained.w_REMONMOD=='N',1,;
      iif(this.Parent.oContained.w_REMONMOD=='S',2,;
      iif(this.Parent.oContained.w_REMONMOD=='I',3,;
      iif(this.Parent.oContained.w_REMONMOD=='O',4,;
      0)))))
  endfunc

  func oREMONMOD_2_8.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oREMONMOD_2_8.mCond()
    with this.Parent.oContained
      return (.w_REKEYREC='N' or .w_RETABDTL=.w_RE_TABLE)
    endwith
  endfunc

  add object oREMONDEL_2_9 as StdTrsCheck with uid="YOHDEOEJCP",rtrep=.t.,;
    cFormVar="w_REMONDEL",  caption="",;
    ToolTipText = "Se attivo: registra l'operazione di cancellazione del dato",;
    HelpContextID = 40494434,;
    Left=544, Top=0, Width=60,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oREMONDEL_2_9.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..REMONDEL,&i_cF..t_REMONDEL),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oREMONDEL_2_9.GetRadio()
    this.Parent.oContained.w_REMONDEL = this.RadioValue()
    return .t.
  endfunc

  func oREMONDEL_2_9.ToRadio()
    this.Parent.oContained.w_REMONDEL=trim(this.Parent.oContained.w_REMONDEL)
    return(;
      iif(this.Parent.oContained.w_REMONDEL=='S',1,;
      0))
  endfunc

  func oREMONDEL_2_9.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oREBLKINS_2_13 as StdTrsCheck with uid="SQKSIHRDPZ",rtrep=.t.,;
    cFormVar="w_REBLKINS",  caption="",;
    ToolTipText = "Dato bloccato in caricamento: il campo deve restare vuoto",;
    HelpContextID = 147442327,;
    Left=611, Top=0, Width=65,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oREBLKINS_2_13.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..REBLKINS,&i_cF..t_REBLKINS),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oREBLKINS_2_13.GetRadio()
    this.Parent.oContained.w_REBLKINS = this.RadioValue()
    return .t.
  endfunc

  func oREBLKINS_2_13.ToRadio()
    this.Parent.oContained.w_REBLKINS=trim(this.Parent.oContained.w_REBLKINS)
    return(;
      iif(this.Parent.oContained.w_REBLKINS=='S',1,;
      0))
  endfunc

  func oREBLKINS_2_13.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oREBLKINS_2_13.mCond()
    with this.Parent.oContained
      return (.w_REMONINS='N' AND .w_REKEYREC='N')
    endwith
  endfunc

  add object oREOBBINS_2_14 as StdTrsCheck with uid="NRPFCQUAUR",rtrep=.t.,;
    cFormVar="w_REOBBINS",  caption="",;
    ToolTipText = "Dato obbligatorio in caricamento",;
    HelpContextID = 157481623,;
    Left=684, Top=0, Width=65,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oREOBBINS_2_14.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..REOBBINS,&i_cF..t_REOBBINS),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oREOBBINS_2_14.GetRadio()
    this.Parent.oContained.w_REOBBINS = this.RadioValue()
    return .t.
  endfunc

  func oREOBBINS_2_14.ToRadio()
    this.Parent.oContained.w_REOBBINS=trim(this.Parent.oContained.w_REOBBINS)
    return(;
      iif(this.Parent.oContained.w_REOBBINS=='S',1,;
      0))
  endfunc

  func oREOBBINS_2_14.SetRadio()
    this.value=this.ToRadio()
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=7
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscf_mre','REF_MAST','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".RESERIAL=REF_MAST.RESERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
