* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_btd                                                        *
*              Gestione To Do                                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-10-20                                                      *
* Last revis.: 2015-03-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_btd",oParentObject,m.pParam)
return(i_retval)

define class tgsut_btd as StdBatch
  * --- Local variables
  pParam = space(4)
  w_UserCode = space(4)
  w_CodePost = space(10)
  w_Memo = space(0)
  w_Cursor = space(10)
  w_Parent = space(10)
  w_ParentCode = space(10)
  w_ColorPostin = 0
  w_DateStart = ctod("  /  /  ")
  w_DateStop = ctod("  /  /  ")
  w_CRLF = space(8)
  * --- WorkFile variables
  cpusers_idx=0
  cpusrgrp_idx=0
  postit_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Invio Postit da Creazione Appuntamento e ToDo
    do case
      case this.pParam="Init"
        if !Empty(this.oParentObject.w_Code)
          * --- Read from postit
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.postit_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.postit_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "datestart,datestop,postit,status,subject,createdby"+;
              " from "+i_cTable+" postit where ";
                  +"code = "+cp_ToStrODBC(this.oParentObject.w_CODE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              datestart,datestop,postit,status,subject,createdby;
              from (i_cTable) where;
                  code = this.oParentObject.w_CODE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_DATINI = NVL((_read_.datestart),cp_NullValue(_read_.datestart))
            this.oParentObject.w_DATFIN = NVL((_read_.datestop),cp_NullValue(_read_.datestop))
            this.w_Memo = NVL(cp_ToDate(_read_.postit),cp_NullValue(_read_.postit))
            this.oParentObject.w_STATUS = NVL(cp_ToDate(_read_.status),cp_NullValue(_read_.status))
            this.oParentObject.w_SUBJECT = NVL(cp_ToDate(_read_.subject),cp_NullValue(_read_.subject))
            this.w_UserCode = NVL(cp_ToDate(_read_.createdby),cp_NullValue(_read_.createdby))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_CRLF = chr(13)+chr(10)
          this.oParentObject.oPgFrm.Pages(2).enabled = (this.w_UserCode=i_Codute)
          this.oParentObject.w_TEXT = SubStr(this.w_Memo, At('"',this.w_Memo)+1, At(Chr(13),this.w_Memo)-3 )
          * --- Trascodifica valori del testo
          this.oParentObject.w_TEXT = Strtran(this.oParentObject.w_TEXT, Chr(253), "'")
          this.oParentObject.w_TEXT = Strtran(this.oParentObject.w_TEXT, Chr(254), '"')
          this.oParentObject.w_TEXT = Strtran(this.oParentObject.w_TEXT, Chr(255), this.w_CRLF)
          * --- Il colore � definito all'interno del memo
          this.oParentObject.w_COLOR = Val( SubStr( this.w_Memo, Rat("]",this.w_Memo)+3, Rat(this.w_CRLF,this.w_Memo,6)-3))
          this.oParentObject.w_ORAINI = Right("00"+Alltrim(Transform(Hour(this.oParentObject.w_DATINI))),2)+":"+Right("00"+Alltrim(Transform(Minute(this.oParentObject.w_DATINI))),2)
          this.oParentObject.w_ORAFIN = Right("00"+Alltrim(Transform(Hour(this.oParentObject.w_DATFIN))),2)+":"+Right("00"+Alltrim(Transform(Minute(this.oParentObject.w_DATFIN))),2)
          this.oParentObject.w_DATINI = Date(Year(this.oParentObject.w_DATINI),Month(this.oParentObject.w_DATINI),Day(this.oParentObject.w_DATINI))
          this.oParentObject.w_DATFIN = Date(Year(this.oParentObject.w_DATFIN),Month(this.oParentObject.w_DATFIN),Day(this.oParentObject.w_DATFIN))
          * --- Select from postit
          i_nConn=i_TableProp[this.postit_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.postit_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select usercode  from "+i_cTable+" postit ";
                +" where parent="+cp_ToStrODBC(this.oParentObject.w_Code)+" Or code="+cp_ToStrODBC(this.oParentObject.w_Code)+"";
                 ,"_Curs_postit")
          else
            select usercode from (i_cTable);
             where parent=this.oParentObject.w_Code Or code=this.oParentObject.w_Code;
              into cursor _Curs_postit
          endif
          if used('_Curs_postit')
            select _Curs_postit
            locate for 1=1
            do while not(eof())
            Update (this.oParentObject.w_USERZOOM.cCursor) set xchk=1 where Code=_Curs_postit.usercode Or Code=i_codute
              select _Curs_postit
              continue
            enddo
            use
          endif
          Select (this.oParentObject.w_USERZOOM.cCursor)
          Go Top
        else
          Select (this.oParentObject.w_USERZOOM.cCursor)
          Update (this.oParentObject.w_USERZOOM.cCursor) set xchk=1 where Code=i_codute
          Go Top
        endif
      case this.pParam="FormLoad"
        if ! (This.oParentObject.oParentObject$"ATD")
          this.oParentObject.w_CODE = This.oParentObject.oParentObject
          * --- Read from postit
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.postit_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.postit_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "status"+;
              " from "+i_cTable+" postit where ";
                  +"code = "+cp_ToStrODBC(this.oParentObject.w_CODE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              status;
              from (i_cTable) where;
                  code = this.oParentObject.w_CODE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_STATUS = NVL(cp_ToDate(_read_.status),cp_NullValue(_read_.status))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        This.bUpdateParentObject = .F.
      case this.pParam="Save"
        this.w_Cursor = this.oParentObject.w_USERZOOM.cCursor
        Select (this.w_Cursor)
        Go Top
        * --- Seleziono il codice del padre (necessariamente l'utente corrente)
        Scan For xchk=1 And Code == i_Codute
        this.w_ParentCode = SYS(2015)
        EndScan
        Select (this.w_Cursor)
        Go Top
        Scan For xchk=1
        this.w_UserCode = Code
        this.w_DateStart = DATETIME( Year(this.oParentObject.w_DATINI), Month(this.oParentObject.w_DATINI), Day(this.oParentObject.w_DATINI), Val(Left(this.oParentObject.w_ORAINI,At(":",this.oParentObject.w_ORAINI)-1)), Val(Right(this.oParentObject.w_ORAINI,At(":",this.oParentObject.w_ORAINI)-1)))
        this.w_DateStop = DATETIME( Year(this.oParentObject.w_DATFIN), Month(this.oParentObject.w_DATFIN), Day(this.oParentObject.w_DATFIN), Val(Left(this.oParentObject.w_ORAFIN,At(":",this.oParentObject.w_ORAFIN)-1)), Val(Right(this.oParentObject.w_ORAFIN,At(":",this.oParentObject.w_ORAFIN)-1)))
        this.w_CRLF = chr(13)+chr(10)
        * --- Trascodifica valori del testo
        this.oParentObject.w_TEXT = Strtran(this.oParentObject.w_TEXT,"'",Chr(253))
        this.oParentObject.w_TEXT = Strtran(this.oParentObject.w_TEXT,'"',Chr(254))
        this.oParentObject.w_TEXT = Chrtran(this.oParentObject.w_TEXT,Chr(13)+Chr(10),Chr(255))
        * --- Costruzione del Memo per la serializzazione corretta (colore incluso)
        this.w_Memo = '"'+this.oParentObject.w_TEXT+'"'+this.w_CRLF
        this.w_Memo = this.w_Memo+"0"+this.w_CRLF+"0"+this.w_CRLF+"0"+this.w_CRLF
        this.w_Memo = this.w_Memo+"["+this.w_CRLF+"]"+this.w_CRLF
        this.w_Memo = this.w_Memo+Alltrim(Str( this.oParentObject.w_COLOR ))+this.w_CRLF
        this.w_Memo = this.w_Memo+'""'+this.w_CRLF+"*"
        if Empty(this.oParentObject.w_Code)
          this.w_CodePost = iif( this.w_UserCode == i_Codute, this.w_ParentCode, SYS(2015))
          this.w_Parent = iif( this.w_UserCode == i_Codute, "", this.w_ParentCode)
          * --- Insert into postit
          i_nConn=i_TableProp[this.postit_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.postit_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.postit_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"code"+",usercode"+",created"+",createdby"+",postit"+",status"+",datestart"+",datestop"+",px"+",py"+",wi"+",he"+",subject"+",parent"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_CodePost),'postit','code');
            +","+cp_NullLink(cp_ToStrODBC(this.w_UserCode),'postit','usercode');
            +","+cp_NullLink(cp_ToStrODBC(DATETIME()),'postit','created');
            +","+cp_NullLink(cp_ToStrODBC(i_Codute),'postit','createdby');
            +","+cp_NullLink(cp_ToStrODBC(this.w_Memo),'postit','postit');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_STATUS),'postit','status');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DateStart),'postit','datestart');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DateStop),'postit','datestop');
            +","+cp_NullLink(cp_ToStrODBC(0),'postit','px');
            +","+cp_NullLink(cp_ToStrODBC(0),'postit','py');
            +","+cp_NullLink(cp_ToStrODBC(150),'postit','wi');
            +","+cp_NullLink(cp_ToStrODBC(150),'postit','he');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_SUBJECT),'postit','subject');
            +","+cp_NullLink(cp_ToStrODBC(this.w_Parent),'postit','parent');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'code',this.w_CodePost,'usercode',this.w_UserCode,'created',DATETIME(),'createdby',i_Codute,'postit',this.w_Memo,'status',this.oParentObject.w_STATUS,'datestart',this.w_DateStart,'datestop',this.w_DateStop,'px',0,'py',0,'wi',150,'he',150)
            insert into (i_cTable) (code,usercode,created,createdby,postit,status,datestart,datestop,px,py,wi,he,subject,parent &i_ccchkf. );
               values (;
                 this.w_CodePost;
                 ,this.w_UserCode;
                 ,DATETIME();
                 ,i_Codute;
                 ,this.w_Memo;
                 ,this.oParentObject.w_STATUS;
                 ,this.w_DateStart;
                 ,this.w_DateStop;
                 ,0;
                 ,0;
                 ,150;
                 ,150;
                 ,this.oParentObject.w_SUBJECT;
                 ,this.w_Parent;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        else
          if this.w_UserCode == i_Codute
            this.w_CodePost = this.oParentObject.w_Code
            * --- Read from postit
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.postit_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.postit_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "parent"+;
                " from "+i_cTable+" postit where ";
                    +"usercode = "+cp_ToStrODBC(this.w_UserCode);
                    +" and code = "+cp_ToStrODBC(this.oParentObject.w_Code);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                parent;
                from (i_cTable) where;
                    usercode = this.w_UserCode;
                    and code = this.oParentObject.w_Code;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_Parent = NVL(cp_ToDate(_read_.parent),cp_NullValue(_read_.parent))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if !Empty(this.w_Parent)
              if cp_YesNo("Post-it legato ad un altro, togliere il vincolo?",32,.f.)
                this.w_Parent = ""
              else
                cp_ErrorMsg( cp_Translate("Modifica annullata") ,48)
                i_retcode = 'stop'
                return
              endif
            endif
            * --- Write into postit
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.postit_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.postit_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.postit_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"usercode ="+cp_NullLink(cp_ToStrODBC(this.w_UserCode),'postit','usercode');
              +",created ="+cp_NullLink(cp_ToStrODBC(DATETIME()),'postit','created');
              +",createdby ="+cp_NullLink(cp_ToStrODBC(i_Codute),'postit','createdby');
              +",postit ="+cp_NullLink(cp_ToStrODBC(this.w_Memo),'postit','postit');
              +",status ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_STATUS),'postit','status');
              +",datestart ="+cp_NullLink(cp_ToStrODBC(this.w_DateStart),'postit','datestart');
              +",datestop ="+cp_NullLink(cp_ToStrODBC(this.w_DateStop),'postit','datestop');
              +",subject ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_SUBJECT),'postit','subject');
              +",parent ="+cp_NullLink(cp_ToStrODBC(this.w_Parent),'postit','parent');
                  +i_ccchkf ;
              +" where ";
                  +"code = "+cp_ToStrODBC(this.w_CodePost);
                     )
            else
              update (i_cTable) set;
                  usercode = this.w_UserCode;
                  ,created = DATETIME();
                  ,createdby = i_Codute;
                  ,postit = this.w_Memo;
                  ,status = this.oParentObject.w_STATUS;
                  ,datestart = this.w_DateStart;
                  ,datestop = this.w_DateStop;
                  ,subject = this.oParentObject.w_SUBJECT;
                  ,parent = this.w_Parent;
                  &i_ccchkf. ;
               where;
                  code = this.w_CodePost;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          else
            * --- Read from postit
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.postit_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.postit_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "Code"+;
                " from "+i_cTable+" postit where ";
                    +"usercode = "+cp_ToStrODBC(this.w_UserCode);
                    +" and parent = "+cp_ToStrODBC(this.oParentObject.w_Code);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                Code;
                from (i_cTable) where;
                    usercode = this.w_UserCode;
                    and parent = this.oParentObject.w_Code;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CodePost = NVL(cp_ToDate(_read_.Code),cp_NullValue(_read_.Code))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if Empty(this.w_CodePost)
              this.w_CodePost = SYS(2015)
              * --- Insert into postit
              i_nConn=i_TableProp[this.postit_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.postit_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.postit_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"code"+",usercode"+",created"+",createdby"+",postit"+",status"+",datestart"+",datestop"+",px"+",py"+",wi"+",he"+",subject"+",parent"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.w_CodePost),'postit','code');
                +","+cp_NullLink(cp_ToStrODBC(this.w_UserCode),'postit','usercode');
                +","+cp_NullLink(cp_ToStrODBC(DATETIME()),'postit','created');
                +","+cp_NullLink(cp_ToStrODBC(i_Codute),'postit','createdby');
                +","+cp_NullLink(cp_ToStrODBC(this.w_Memo),'postit','postit');
                +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_STATUS),'postit','status');
                +","+cp_NullLink(cp_ToStrODBC(this.w_DateStart),'postit','datestart');
                +","+cp_NullLink(cp_ToStrODBC(this.w_DateStop),'postit','datestop');
                +","+cp_NullLink(cp_ToStrODBC(0),'postit','px');
                +","+cp_NullLink(cp_ToStrODBC(0),'postit','py');
                +","+cp_NullLink(cp_ToStrODBC(150),'postit','wi');
                +","+cp_NullLink(cp_ToStrODBC(150),'postit','he');
                +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_SUBJECT),'postit','subject');
                +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_Code),'postit','parent');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'code',this.w_CodePost,'usercode',this.w_UserCode,'created',DATETIME(),'createdby',i_Codute,'postit',this.w_Memo,'status',this.oParentObject.w_STATUS,'datestart',this.w_DateStart,'datestop',this.w_DateStop,'px',0,'py',0,'wi',150,'he',150)
                insert into (i_cTable) (code,usercode,created,createdby,postit,status,datestart,datestop,px,py,wi,he,subject,parent &i_ccchkf. );
                   values (;
                     this.w_CodePost;
                     ,this.w_UserCode;
                     ,DATETIME();
                     ,i_Codute;
                     ,this.w_Memo;
                     ,this.oParentObject.w_STATUS;
                     ,this.w_DateStart;
                     ,this.w_DateStop;
                     ,0;
                     ,0;
                     ,150;
                     ,150;
                     ,this.oParentObject.w_SUBJECT;
                     ,this.oParentObject.w_Code;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
            else
              * --- Write into postit
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.postit_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.postit_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.postit_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"usercode ="+cp_NullLink(cp_ToStrODBC(this.w_UserCode),'postit','usercode');
                +",created ="+cp_NullLink(cp_ToStrODBC(DATETIME()),'postit','created');
                +",createdby ="+cp_NullLink(cp_ToStrODBC(i_Codute),'postit','createdby');
                +",postit ="+cp_NullLink(cp_ToStrODBC(this.w_Memo),'postit','postit');
                +",status ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_STATUS),'postit','status');
                +",datestart ="+cp_NullLink(cp_ToStrODBC(this.w_DateStart),'postit','datestart');
                +",datestop ="+cp_NullLink(cp_ToStrODBC(this.w_DateStop),'postit','datestop');
                +",subject ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_SUBJECT),'postit','subject');
                    +i_ccchkf ;
                +" where ";
                    +"code = "+cp_ToStrODBC(this.w_CodePost);
                       )
              else
                update (i_cTable) set;
                    usercode = this.w_UserCode;
                    ,created = DATETIME();
                    ,createdby = i_Codute;
                    ,postit = this.w_Memo;
                    ,status = this.oParentObject.w_STATUS;
                    ,datestart = this.w_DateStart;
                    ,datestop = this.w_DateStop;
                    ,subject = this.oParentObject.w_SUBJECT;
                    &i_ccchkf. ;
                 where;
                    code = this.w_CodePost;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
          endif
        endif
        EndScan
        if Type("i_PopupCurForm")="O"
          i_PopupCurForm.w_calendar.CalGrid.CleanGrid()
          i_PopupCurForm.NotifyEvent("ZoomRefresh")
          Release i_PopupCurForm
        endif
      case this.pParam="Delete"
        if cp_YesNo(MSG_CONFIRM_DELETING_QP,32,.f.)
          * --- Delete from postit
          i_nConn=i_TableProp[this.postit_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.postit_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"code = "+cp_ToStrODBC(this.oParentObject.w_Code);
                   )
          else
            delete from (i_cTable) where;
                  code = this.oParentObject.w_Code;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          * --- Read from postit
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.postit_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.postit_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "Code"+;
              " from "+i_cTable+" postit where ";
                  +"parent = "+cp_ToStrODBC(this.oParentObject.w_Code);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              Code;
              from (i_cTable) where;
                  parent = this.oParentObject.w_Code;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CodePost = NVL(cp_ToDate(_read_.Code),cp_NullValue(_read_.Code))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if !Empty(this.w_CodePost) And cp_YesNo("Sono presenti post-it associati, si desidera eliminarli?",32,.f.)
            * --- Delete from postit
            i_nConn=i_TableProp[this.postit_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.postit_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"parent = "+cp_ToStrODBC(this.oParentObject.w_Code);
                     )
            else
              delete from (i_cTable) where;
                    parent = this.oParentObject.w_Code;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
          endif
          This.oParentObject.ecpQuit()
        endif
    endcase
  endproc


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='cpusers'
    this.cWorkTables[2]='cpusrgrp'
    this.cWorkTables[3]='postit'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_postit')
      use in _Curs_postit
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
