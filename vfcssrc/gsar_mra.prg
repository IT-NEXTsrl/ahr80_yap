* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_mra                                                        *
*              Associazione record attributo                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-12-06                                                      *
* Last revis.: 2015-07-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsar_mra")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsar_mra")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsar_mra")
  return

* --- Class definition
define class tgsar_mra as StdPCForm
  Width  = 524
  Height = 190
  Top    = 7
  Left   = 9
  cComment = "Associazione record attributo"
  cPrg = "gsar_mra"
  HelpContextID=113866601
  add object cnt as tcgsar_mra
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsar_mra as PCContext
  w_CPROWORD = 0
  w_CODGRU = space(10)
  w_ASMODFAM = space(20)
  w_ASCODATT = space(10)
  w_CODATT = space(10)
  w_ASCODFAM = space(10)
  w_CODFAM = space(10)
  w_INPUTA = space(1)
  w_CALCPIC = 0
  w_TIPO = space(1)
  w_DIME = 0
  w_DEC = 0
  w_CALCPIC = 0
  w_ASTIPDAT = space(8)
  w_ASTIPNUM = 0
  w_ASTIPCAR = space(41)
  w_ASVALATT = space(20)
  w_GESTPROG = space(10)
  w_GEST = space(10)
  w_RESCHK = 0
  w_RIGA = 0
  w_AUTOMA = space(1)
  w_GUID = space(14)
  w_TABLE = space(30)
  w_CAMPO = space(50)
  w_ZOOM = space(254)
  w_ZOOMOZ = space(20)
  w_OBTEST = space(8)
  w_ATTRIBUTO = space(10)
  w_ATTCOL = space(30)
  w_MODATTR = space(20)
  w_FRDIMENS = 0
  w_FRNUMDEC = 0
  w_FR_TABLE = space(30)
  w_FR__ZOOM = space(254)
  w_FR_CAMPO = space(50)
  w_GSAR_BIA_FLAG = space(10)
  proc Save(i_oFrom)
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_CODGRU = i_oFrom.w_CODGRU
    this.w_ASMODFAM = i_oFrom.w_ASMODFAM
    this.w_ASCODATT = i_oFrom.w_ASCODATT
    this.w_CODATT = i_oFrom.w_CODATT
    this.w_ASCODFAM = i_oFrom.w_ASCODFAM
    this.w_CODFAM = i_oFrom.w_CODFAM
    this.w_INPUTA = i_oFrom.w_INPUTA
    this.w_CALCPIC = i_oFrom.w_CALCPIC
    this.w_TIPO = i_oFrom.w_TIPO
    this.w_DIME = i_oFrom.w_DIME
    this.w_DEC = i_oFrom.w_DEC
    this.w_CALCPIC = i_oFrom.w_CALCPIC
    this.w_ASTIPDAT = i_oFrom.w_ASTIPDAT
    this.w_ASTIPNUM = i_oFrom.w_ASTIPNUM
    this.w_ASTIPCAR = i_oFrom.w_ASTIPCAR
    this.w_ASVALATT = i_oFrom.w_ASVALATT
    this.w_GESTPROG = i_oFrom.w_GESTPROG
    this.w_GEST = i_oFrom.w_GEST
    this.w_RESCHK = i_oFrom.w_RESCHK
    this.w_RIGA = i_oFrom.w_RIGA
    this.w_AUTOMA = i_oFrom.w_AUTOMA
    this.w_GUID = i_oFrom.w_GUID
    this.w_TABLE = i_oFrom.w_TABLE
    this.w_CAMPO = i_oFrom.w_CAMPO
    this.w_ZOOM = i_oFrom.w_ZOOM
    this.w_ZOOMOZ = i_oFrom.w_ZOOMOZ
    this.w_OBTEST = i_oFrom.w_OBTEST
    this.w_ATTRIBUTO = i_oFrom.w_ATTRIBUTO
    this.w_ATTCOL = i_oFrom.w_ATTCOL
    this.w_MODATTR = i_oFrom.w_MODATTR
    this.w_FRDIMENS = i_oFrom.w_FRDIMENS
    this.w_FRNUMDEC = i_oFrom.w_FRNUMDEC
    this.w_FR_TABLE = i_oFrom.w_FR_TABLE
    this.w_FR__ZOOM = i_oFrom.w_FR__ZOOM
    this.w_FR_CAMPO = i_oFrom.w_FR_CAMPO
    this.w_GSAR_BIA_FLAG = i_oFrom.w_GSAR_BIA_FLAG
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_CODGRU = this.w_CODGRU
    i_oTo.w_ASMODFAM = this.w_ASMODFAM
    i_oTo.w_ASCODATT = this.w_ASCODATT
    i_oTo.w_CODATT = this.w_CODATT
    i_oTo.w_ASCODFAM = this.w_ASCODFAM
    i_oTo.w_CODFAM = this.w_CODFAM
    i_oTo.w_INPUTA = this.w_INPUTA
    i_oTo.w_CALCPIC = this.w_CALCPIC
    i_oTo.w_TIPO = this.w_TIPO
    i_oTo.w_DIME = this.w_DIME
    i_oTo.w_DEC = this.w_DEC
    i_oTo.w_CALCPIC = this.w_CALCPIC
    i_oTo.w_ASTIPDAT = this.w_ASTIPDAT
    i_oTo.w_ASTIPNUM = this.w_ASTIPNUM
    i_oTo.w_ASTIPCAR = this.w_ASTIPCAR
    i_oTo.w_ASVALATT = this.w_ASVALATT
    i_oTo.w_GESTPROG = this.w_GESTPROG
    i_oTo.w_GEST = this.w_GEST
    i_oTo.w_RESCHK = this.w_RESCHK
    i_oTo.w_RIGA = this.w_RIGA
    i_oTo.w_AUTOMA = this.w_AUTOMA
    i_oTo.w_GUID = this.w_GUID
    i_oTo.w_TABLE = this.w_TABLE
    i_oTo.w_CAMPO = this.w_CAMPO
    i_oTo.w_ZOOM = this.w_ZOOM
    i_oTo.w_ZOOMOZ = this.w_ZOOMOZ
    i_oTo.w_OBTEST = this.w_OBTEST
    i_oTo.w_ATTRIBUTO = this.w_ATTRIBUTO
    i_oTo.w_ATTCOL = this.w_ATTCOL
    i_oTo.w_MODATTR = this.w_MODATTR
    i_oTo.w_FRDIMENS = this.w_FRDIMENS
    i_oTo.w_FRNUMDEC = this.w_FRNUMDEC
    i_oTo.w_FR_TABLE = this.w_FR_TABLE
    i_oTo.w_FR__ZOOM = this.w_FR__ZOOM
    i_oTo.w_FR_CAMPO = this.w_FR_CAMPO
    i_oTo.w_GSAR_BIA_FLAG = this.w_GSAR_BIA_FLAG
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsar_mra as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 524
  Height = 190
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-07-31"
  HelpContextID=113866601
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=37

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  ASS_ATTR_IDX = 0
  MODDATTR_IDX = 0
  MODMATTR_IDX = 0
  FAM_ATTR_IDX = 0
  GRU_COCE_IDX = 0
  GRU_ATTR_IDX = 0
  GRUDATTR_IDX = 0
  FAMDATTR_IDX = 0
  cFile = "ASS_ATTR"
  cKeySelect = "GUID"
  cKeyWhere  = "GUID=this.w_GUID"
  cKeyDetail  = "GUID=this.w_GUID"
  cKeyWhereODBC = '"GUID="+cp_ToStrODBC(this.w_GUID)';

  cKeyDetailWhereODBC = '"GUID="+cp_ToStrODBC(this.w_GUID)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"ASS_ATTR.GUID="+cp_ToStrODBC(this.w_GUID)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'ASS_ATTR.CPROWORD '
  cPrg = "gsar_mra"
  cComment = "Associazione record attributo"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 7
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CPROWORD = 0
  w_CODGRU = space(10)
  o_CODGRU = space(10)
  w_ASMODFAM = space(20)
  w_ASCODATT = space(10)
  o_ASCODATT = space(10)
  w_CODATT = space(10)
  w_ASCODFAM = space(10)
  o_ASCODFAM = space(10)
  w_CODFAM = space(10)
  w_INPUTA = space(1)
  o_INPUTA = space(1)
  w_CALCPIC = 0
  w_TIPO = space(1)
  w_DIME = 0
  o_DIME = 0
  w_DEC = 0
  o_DEC = 0
  w_CALCPIC = 0
  w_ASTIPDAT = ctod('  /  /  ')
  o_ASTIPDAT = ctod('  /  /  ')
  w_ASTIPNUM = 0
  o_ASTIPNUM = 0
  w_ASTIPCAR = space(41)
  o_ASTIPCAR = space(41)
  w_ASVALATT = space(20)
  w_GESTPROG = space(10)
  w_GEST = space(10)
  w_RESCHK = 0
  w_RIGA = 0
  w_AUTOMA = space(1)
  w_GUID = space(14)
  w_TABLE = space(30)
  w_CAMPO = space(50)
  w_ZOOM = space(254)
  w_ZOOMOZ = space(20)
  w_OBTEST = ctod('  /  /  ')
  w_ATTRIBUTO = space(10)
  w_ATTCOL = space(30)
  w_MODATTR = space(20)
  w_FRDIMENS = 0
  w_FRNUMDEC = 0
  w_FR_TABLE = space(30)
  w_FR__ZOOM = space(254)
  w_FR_CAMPO = space(50)
  w_GSAR_BIA_FLAG = space(10)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_mraPag1","gsar_mra",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[8]
    this.cWorkTables[1]='MODDATTR'
    this.cWorkTables[2]='MODMATTR'
    this.cWorkTables[3]='FAM_ATTR'
    this.cWorkTables[4]='GRU_COCE'
    this.cWorkTables[5]='GRU_ATTR'
    this.cWorkTables[6]='GRUDATTR'
    this.cWorkTables[7]='FAMDATTR'
    this.cWorkTables[8]='ASS_ATTR'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(8))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ASS_ATTR_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ASS_ATTR_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsar_mra'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_6_joined
    link_2_6_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from ASS_ATTR where GUID=KeySet.GUID
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.ASS_ATTR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ASS_ATTR_IDX,2],this.bLoadRecFilter,this.ASS_ATTR_IDX,"gsar_mra")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ASS_ATTR')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ASS_ATTR.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ASS_ATTR '
      link_2_6_joined=this.AddJoinedLink_2_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'GUID',this.w_GUID  )
      select * from (i_cTable) ASS_ATTR where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_GESTPROG = space(10)
        .w_RESCHK = 0
        .w_OBTEST = i_DATSYS
        .w_CALCPIC = IIF(.w_INPUTA='N',DEFPICAT(.w_DIME,.w_DEC),0)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_GEST = THIS.OPARENTOBJECT.w_GEST
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .w_GUID = NVL(GUID,space(14))
        cp_LoadRecExtFlds(this,'ASS_ATTR')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_INPUTA = space(1)
          .w_TIPO = space(1)
          .w_DIME = 0
          .w_DEC = 0
          .w_AUTOMA = space(1)
          .w_TABLE = space(30)
          .w_CAMPO = space(50)
          .w_ZOOM = space(254)
          .w_ZOOMOZ = space(20)
          .w_ATTRIBUTO = space(10)
          .w_ATTCOL = space(30)
          .w_FRDIMENS = 0
          .w_FRNUMDEC = 0
          .w_FR_TABLE = space(30)
          .w_FR__ZOOM = space(254)
          .w_FR_CAMPO = space(50)
        .w_GSAR_BIA_FLAG = "N"
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
        .w_CODGRU = IIF(TYPE('this.oparentobject.w_CODATT')='C', this.oparentobject.w_CODATT, '          ')
          .w_ASMODFAM = NVL(ASMODFAM,space(20))
          .w_ASCODATT = NVL(ASCODATT,space(10))
          * evitabile
          *.link_2_4('Load')
        .w_CODATT = .w_ASCODATT
          .link_2_5('Load')
          .w_ASCODFAM = NVL(ASCODFAM,space(10))
          if link_2_6_joined
            this.w_ASCODFAM = NVL(FRCODICE206,NVL(this.w_ASCODFAM,space(10)))
            this.w_FRDIMENS = NVL(FRDIMENS206,0)
            this.w_FRNUMDEC = NVL(FRNUMDEC206,0)
            this.w_INPUTA = NVL(FRINPUTA206,space(1))
          else
          .link_2_6('Load')
          endif
        .w_CODFAM = .w_ASCODFAM
          .link_2_7('Load')
        .w_CALCPIC = iif(.w_inputa='N',DEFPICAT(.w_FRDIMENS,.w_FRNUMDEC),0)
          .w_ASTIPDAT = NVL(cp_ToDate(ASTIPDAT),ctod("  /  /  "))
          .w_ASTIPNUM = NVL(ASTIPNUM,0)
          .w_ASTIPCAR = NVL(ASTIPCAR,space(41))
          .w_ASVALATT = NVL(ASVALATT,space(20))
        .w_RIGA = .w_CPROWNUM
        .w_MODATTR = IIF(TYPE('this.oparentobject.w_IMMODATR')='C', this.oparentobject.w_IMMODATR, .w_GEST)
          .link_2_28('Load')
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_GEST = THIS.OPARENTOBJECT.w_GEST
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_CPROWORD=10
      .w_CODGRU=space(10)
      .w_ASMODFAM=space(20)
      .w_ASCODATT=space(10)
      .w_CODATT=space(10)
      .w_ASCODFAM=space(10)
      .w_CODFAM=space(10)
      .w_INPUTA=space(1)
      .w_CALCPIC=0
      .w_TIPO=space(1)
      .w_DIME=0
      .w_DEC=0
      .w_CALCPIC=0
      .w_ASTIPDAT=ctod("  /  /  ")
      .w_ASTIPNUM=0
      .w_ASTIPCAR=space(41)
      .w_ASVALATT=space(20)
      .w_GESTPROG=space(10)
      .w_GEST=space(10)
      .w_RESCHK=0
      .w_RIGA=0
      .w_AUTOMA=space(1)
      .w_GUID=space(14)
      .w_TABLE=space(30)
      .w_CAMPO=space(50)
      .w_ZOOM=space(254)
      .w_ZOOMOZ=space(20)
      .w_OBTEST=ctod("  /  /  ")
      .w_ATTRIBUTO=space(10)
      .w_ATTCOL=space(30)
      .w_MODATTR=space(20)
      .w_FRDIMENS=0
      .w_FRNUMDEC=0
      .w_FR_TABLE=space(30)
      .w_FR__ZOOM=space(254)
      .w_FR_CAMPO=space(50)
      .w_GSAR_BIA_FLAG=space(10)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        .w_CODGRU = IIF(TYPE('this.oparentobject.w_CODATT')='C', this.oparentobject.w_CODATT, '          ')
        .w_ASMODFAM = nvl(this.oparentobject.w_GEST,space(20))
        .w_ASCODATT = .w_CODGRU
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_ASCODATT))
         .link_2_4('Full')
        endif
        .w_CODATT = .w_ASCODATT
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_CODATT))
         .link_2_5('Full')
        endif
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_ASCODFAM))
         .link_2_6('Full')
        endif
        .w_CODFAM = .w_ASCODFAM
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_CODFAM))
         .link_2_7('Full')
        endif
        .DoRTCalc(8,8,.f.)
        .w_CALCPIC = iif(.w_inputa='N',DEFPICAT(.w_FRDIMENS,.w_FRNUMDEC),0)
        .DoRTCalc(10,12,.f.)
        .w_CALCPIC = IIF(.w_INPUTA='N',DEFPICAT(.w_DIME,.w_DEC),0)
        .w_ASTIPDAT = cp_chartodate ('  -  -  ')
        .w_ASTIPNUM = 0
        .w_ASTIPCAR = SPACE(20)
        .w_ASVALATT = IIF(.w_INPUTA='D',DTOC(.w_ASTIPDAT),IIF(.w_INPUTA='C',ALLTRIM(.w_ASTIPCAR),IIF(.w_INPUTA='N',alltrim(STR(.w_ASTIPNUM,20,3)),space(20))))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(18,18,.f.)
        .w_GEST = THIS.OPARENTOBJECT.w_GEST
        .w_RESCHK = 0
        .w_RIGA = .w_CPROWNUM
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .DoRTCalc(22,27,.f.)
        .w_OBTEST = i_DATSYS
        .DoRTCalc(29,30,.f.)
        .w_MODATTR = IIF(TYPE('this.oparentobject.w_IMMODATR')='C', this.oparentobject.w_IMMODATR, .w_GEST)
        .DoRTCalc(31,31,.f.)
        if not(empty(.w_MODATTR))
         .link_2_28('Full')
        endif
        .DoRTCalc(32,36,.f.)
        .w_GSAR_BIA_FLAG = "N"
      endif
    endwith
    cp_BlankRecExtFlds(this,'ASS_ATTR')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oASTIPDAT_2_14.enabled = i_bVal
      .Page1.oPag.oASTIPNUM_2_15.enabled = i_bVal
      .Page1.oPag.oASTIPCAR_2_16.enabled = i_bVal
      .Page1.oPag.oBtn_2_21.enabled = i_bVal
      .Page1.oPag.oObj_1_4.enabled = i_bVal
      .Page1.oPag.oObj_1_5.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'ASS_ATTR',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ASS_ATTR_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GUID,"GUID",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(4);
      ,t_ASCODATT C(10);
      ,t_ASCODFAM C(10);
      ,t_ASTIPDAT D(8);
      ,t_ASTIPNUM N(20,3);
      ,t_ASTIPCAR C(41);
      ,t_ASVALATT C(20);
      ,CPROWNUM N(10);
      ,t_CODGRU C(10);
      ,t_ASMODFAM C(20);
      ,t_CODATT C(10);
      ,t_CODFAM C(10);
      ,t_INPUTA C(1);
      ,t_CALCPIC N(20,3);
      ,t_TIPO C(1);
      ,t_DIME N(2);
      ,t_DEC N(3);
      ,t_RIGA N(5);
      ,t_AUTOMA C(1);
      ,t_TABLE C(30);
      ,t_CAMPO C(50);
      ,t_ZOOM C(254);
      ,t_ZOOMOZ C(20);
      ,t_ATTRIBUTO C(10);
      ,t_ATTCOL C(30);
      ,t_MODATTR C(20);
      ,t_FRDIMENS N(10);
      ,t_FRNUMDEC N(10);
      ,t_FR_TABLE C(30);
      ,t_FR__ZOOM C(254);
      ,t_FR_CAMPO C(50);
      ,t_GSAR_BIA_FLAG C(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsar_mrabodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oASCODATT_2_4.controlsource=this.cTrsName+'.t_ASCODATT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oASCODFAM_2_6.controlsource=this.cTrsName+'.t_ASCODFAM'
    this.oPgFRm.Page1.oPag.oASTIPDAT_2_14.controlsource=this.cTrsName+'.t_ASTIPDAT'
    this.oPgFRm.Page1.oPag.oASTIPNUM_2_15.controlsource=this.cTrsName+'.t_ASTIPNUM'
    this.oPgFRm.Page1.oPag.oASTIPCAR_2_16.controlsource=this.cTrsName+'.t_ASTIPCAR'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oASVALATT_2_17.controlsource=this.cTrsName+'.t_ASVALATT'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(42)
    this.AddVLine(142)
    this.AddVLine(274)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ASS_ATTR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ASS_ATTR_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ASS_ATTR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ASS_ATTR_IDX,2])
      *
      * insert into ASS_ATTR
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ASS_ATTR')
        i_extval=cp_InsertValODBCExtFlds(this,'ASS_ATTR')
        i_cFldBody=" "+;
                  "(CPROWORD,ASMODFAM,ASCODATT,ASCODFAM,ASTIPDAT"+;
                  ",ASTIPNUM,ASTIPCAR,ASVALATT,GUID,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_ASMODFAM)+","+cp_ToStrODBCNull(this.w_ASCODATT)+","+cp_ToStrODBCNull(this.w_ASCODFAM)+","+cp_ToStrODBC(this.w_ASTIPDAT)+;
             ","+cp_ToStrODBC(this.w_ASTIPNUM)+","+cp_ToStrODBC(this.w_ASTIPCAR)+","+cp_ToStrODBC(this.w_ASVALATT)+","+cp_ToStrODBC(this.w_GUID)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ASS_ATTR')
        i_extval=cp_InsertValVFPExtFlds(this,'ASS_ATTR')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'GUID',this.w_GUID)
        INSERT INTO (i_cTable) (;
                   CPROWORD;
                  ,ASMODFAM;
                  ,ASCODATT;
                  ,ASCODFAM;
                  ,ASTIPDAT;
                  ,ASTIPNUM;
                  ,ASTIPCAR;
                  ,ASVALATT;
                  ,GUID;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_CPROWORD;
                  ,this.w_ASMODFAM;
                  ,this.w_ASCODATT;
                  ,this.w_ASCODFAM;
                  ,this.w_ASTIPDAT;
                  ,this.w_ASTIPNUM;
                  ,this.w_ASTIPCAR;
                  ,this.w_ASVALATT;
                  ,this.w_GUID;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.ASS_ATTR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ASS_ATTR_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (t_CPROWORD<>0 and not empty (t_ASCODATT) and not empty(t_ASCODFAM)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'ASS_ATTR')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'ASS_ATTR')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_CPROWORD<>0 and not empty (t_ASCODATT) and not empty(t_ASCODFAM)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update ASS_ATTR
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'ASS_ATTR')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",ASMODFAM="+cp_ToStrODBC(this.w_ASMODFAM)+;
                     ",ASCODATT="+cp_ToStrODBCNull(this.w_ASCODATT)+;
                     ",ASCODFAM="+cp_ToStrODBCNull(this.w_ASCODFAM)+;
                     ",ASTIPDAT="+cp_ToStrODBC(this.w_ASTIPDAT)+;
                     ",ASTIPNUM="+cp_ToStrODBC(this.w_ASTIPNUM)+;
                     ",ASTIPCAR="+cp_ToStrODBC(this.w_ASTIPCAR)+;
                     ",ASVALATT="+cp_ToStrODBC(this.w_ASVALATT)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'ASS_ATTR')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,ASMODFAM=this.w_ASMODFAM;
                     ,ASCODATT=this.w_ASCODATT;
                     ,ASCODFAM=this.w_ASCODFAM;
                     ,ASTIPDAT=this.w_ASTIPDAT;
                     ,ASTIPNUM=this.w_ASTIPNUM;
                     ,ASTIPCAR=this.w_ASTIPCAR;
                     ,ASVALATT=this.w_ASVALATT;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ASS_ATTR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ASS_ATTR_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_CPROWORD<>0 and not empty (t_ASCODATT) and not empty(t_ASCODFAM)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete ASS_ATTR
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_CPROWORD<>0 and not empty (t_ASCODATT) and not empty(t_ASCODFAM)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ASS_ATTR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ASS_ATTR_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
          .w_CODGRU = IIF(TYPE('this.oparentobject.w_CODATT')='C', this.oparentobject.w_CODATT, '          ')
          .w_ASMODFAM = nvl(this.oparentobject.w_GEST,space(20))
        if .o_ASCODATT<>.w_ASCODATT.or. .o_ASCODFAM<>.w_ASCODFAM.or. .o_CODGRU<>.w_CODGRU
          .link_2_4('Full')
        endif
        if .o_ASCODATT<>.w_ASCODATT
          .w_CODATT = .w_ASCODATT
          .link_2_5('Full')
        endif
        .DoRTCalc(6,6,.t.)
        if .o_ASCODFAM<>.w_ASCODFAM
          .w_CODFAM = .w_ASCODFAM
          .link_2_7('Full')
        endif
        .DoRTCalc(8,8,.t.)
        if .o_ASCODFAM<>.w_ASCODFAM
          .w_CALCPIC = iif(.w_inputa='N',DEFPICAT(.w_FRDIMENS,.w_FRNUMDEC),0)
        endif
        .DoRTCalc(10,12,.t.)
        if .o_DEC<>.w_DEC.or. .o_DIME<>.w_DIME
          .w_CALCPIC = IIF(.w_INPUTA='N',DEFPICAT(.w_DIME,.w_DEC),0)
        endif
        .DoRTCalc(14,16,.t.)
        if .o_ASTIPCAR<>.w_ASTIPCAR.or. .o_ASTIPDAT<>.w_ASTIPDAT.or. .o_ASTIPNUM<>.w_ASTIPNUM
          .w_ASVALATT = IIF(.w_INPUTA='D',DTOC(.w_ASTIPDAT),IIF(.w_INPUTA='C',ALLTRIM(.w_ASTIPCAR),IIF(.w_INPUTA='N',alltrim(STR(.w_ASTIPNUM,20,3)),space(20))))
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(18,18,.t.)
          .w_GEST = THIS.OPARENTOBJECT.w_GEST
        .DoRTCalc(20,20,.t.)
          .w_RIGA = .w_CPROWNUM
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .DoRTCalc(22,30,.t.)
          .w_MODATTR = IIF(TYPE('this.oparentobject.w_IMMODATR')='C', this.oparentobject.w_IMMODATR, .w_GEST)
          .link_2_28('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(32,37,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_CODGRU with this.w_CODGRU
      replace t_ASMODFAM with this.w_ASMODFAM
      replace t_CODATT with this.w_CODATT
      replace t_CODFAM with this.w_CODFAM
      replace t_INPUTA with this.w_INPUTA
      replace t_CALCPIC with this.w_CALCPIC
      replace t_TIPO with this.w_TIPO
      replace t_DIME with this.w_DIME
      replace t_DEC with this.w_DEC
      replace t_RIGA with this.w_RIGA
      replace t_AUTOMA with this.w_AUTOMA
      replace t_TABLE with this.w_TABLE
      replace t_CAMPO with this.w_CAMPO
      replace t_ZOOM with this.w_ZOOM
      replace t_ZOOMOZ with this.w_ZOOMOZ
      replace t_ATTRIBUTO with this.w_ATTRIBUTO
      replace t_ATTCOL with this.w_ATTCOL
      replace t_MODATTR with this.w_MODATTR
      replace t_FRDIMENS with this.w_FRDIMENS
      replace t_FRNUMDEC with this.w_FRNUMDEC
      replace t_FR_TABLE with this.w_FR_TABLE
      replace t_FR__ZOOM with this.w_FR__ZOOM
      replace t_FR_CAMPO with this.w_FR_CAMPO
      replace t_GSAR_BIA_FLAG with this.w_GSAR_BIA_FLAG
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_TINIUZILQW()
    with this
          * --- Sbianco il valore attributo
          .w_ASTIPNUM = 0
          .w_ASTIPCAR = space(41)
          .w_ASTIPDAT = cp_chartodate ('  -  -  ')
          .w_ASVALATT = IIF(.w_INPUTA='D',DTOC(.w_ASTIPDAT),IIF(.w_INPUTA='C',ALLTRIM(.w_ASTIPCAR),IIF(.w_INPUTA='N',alltrim(STR(.w_ASTIPNUM,20,3)),space(20))))
    endwith
  endproc
  proc Calculate_WIOBAANQOZ()
    with this
          * --- Sbianco ascodfam
          .w_ASCODFAM = SPACE(10)
    endwith
  endproc
  proc Calculate_NCNRSKREUY()
    with this
          * --- Sbianco il valore attributo
          .w_ASTIPCAR = Left(.w_ASTIPCAR,.w_DIME)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oASCODATT_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oASCODATT_2_4.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oASCODFAM_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oASCODFAM_2_6.mCond()
    this.oPgFrm.Page1.oPag.oASTIPDAT_2_14.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oASTIPDAT_2_14.mCond()
    this.oPgFrm.Page1.oPag.oASTIPNUM_2_15.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oASTIPNUM_2_15.mCond()
    this.oPgFrm.Page1.oPag.oASTIPCAR_2_16.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oASTIPCAR_2_16.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oASTIPDAT_2_14.visible=!this.oPgFrm.Page1.oPag.oASTIPDAT_2_14.mHide()
    this.oPgFrm.Page1.oPag.oASTIPNUM_2_15.visible=!this.oPgFrm.Page1.oPag.oASTIPNUM_2_15.mHide()
    this.oPgFrm.Page1.oPag.oASTIPCAR_2_16.visible=!this.oPgFrm.Page1.oPag.oASTIPCAR_2_16.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_4.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_5.Event(cEvent)
        if lower(cEvent)==lower("w_ASCODATT Changed") or lower(cEvent)==lower("w_ASCODFAM Changed")
          .Calculate_TINIUZILQW()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_ASCODATT Changed")
          .Calculate_WIOBAANQOZ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_ASTIPCAR SelectedFromZoom")
          .Calculate_NCNRSKREUY()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ASCODATT
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRU_ATTR_IDX,3]
    i_lTable = "GRU_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRU_ATTR_IDX,2], .t., this.GRU_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRU_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ASCODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MGA',True,'GRU_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GRCODICE like "+cp_ToStrODBC(trim(this.w_ASCODATT)+"%");

          i_ret=cp_SQL(i_nConn,"select GRCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GRCODICE',trim(this.w_ASCODATT))
          select GRCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ASCODATT)==trim(_Link_.GRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ASCODATT) and !this.bDontReportError
            deferred_cp_zoom('GRU_ATTR','*','GRCODICE',cp_AbsName(oSource.parent,'oASCODATT_2_4'),i_cWhere,'GSAR_MGA',"Gruppo attributi",'GRUDEF.GRU_ATTR_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GRCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where GRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GRCODICE',oSource.xKey(1))
            select GRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ASCODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GRCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where GRCODICE="+cp_ToStrODBC(this.w_ASCODATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GRCODICE',this.w_ASCODATT)
            select GRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ASCODATT = NVL(_Link_.GRCODICE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_ASCODATT = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRU_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.GRCODICE,1)
      cp_ShowWarn(i_cKey,this.GRU_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ASCODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATT
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MODDATTR_IDX,3]
    i_lTable = "MODDATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MODDATTR_IDX,2], .t., this.MODDATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MODDATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODATT,MAAUTOMA";
                   +" from "+i_cTable+" "+i_lTable+" where MACODATT="+cp_ToStrODBC(this.w_CODATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODATT',this.w_CODATT)
            select MACODATT,MAAUTOMA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATT = NVL(_Link_.MACODATT,space(10))
      this.w_AUTOMA = NVL(_Link_.MAAUTOMA,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODATT = space(10)
      endif
      this.w_AUTOMA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MODDATTR_IDX,2])+'\'+cp_ToStr(_Link_.MACODATT,1)
      cp_ShowWarn(i_cKey,this.MODDATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ASCODFAM
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ATTR_IDX,3]
    i_lTable = "FAM_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ATTR_IDX,2], .t., this.FAM_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ASCODFAM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AAT',True,'FAM_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FRCODICE like "+cp_ToStrODBC(trim(this.w_ASCODFAM)+"%");

          i_ret=cp_SQL(i_nConn,"select FRCODICE,FRDIMENS,FRNUMDEC,FRINPUTA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FRCODICE',trim(this.w_ASCODFAM))
          select FRCODICE,FRDIMENS,FRNUMDEC,FRINPUTA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ASCODFAM)==trim(_Link_.FRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ASCODFAM) and !this.bDontReportError
            deferred_cp_zoom('FAM_ATTR','*','FRCODICE',cp_AbsName(oSource.parent,'oASCODFAM_2_6'),i_cWhere,'GSAR_AAT',"Attributi",'GSAR_AAT.FAM_ATTR_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FRCODICE,FRDIMENS,FRNUMDEC,FRINPUTA";
                     +" from "+i_cTable+" "+i_lTable+" where FRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FRCODICE',oSource.xKey(1))
            select FRCODICE,FRDIMENS,FRNUMDEC,FRINPUTA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ASCODFAM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FRCODICE,FRDIMENS,FRNUMDEC,FRINPUTA";
                   +" from "+i_cTable+" "+i_lTable+" where FRCODICE="+cp_ToStrODBC(this.w_ASCODFAM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FRCODICE',this.w_ASCODFAM)
            select FRCODICE,FRDIMENS,FRNUMDEC,FRINPUTA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ASCODFAM = NVL(_Link_.FRCODICE,space(10))
      this.w_FRDIMENS = NVL(_Link_.FRDIMENS,0)
      this.w_FRNUMDEC = NVL(_Link_.FRNUMDEC,0)
      this.w_INPUTA = NVL(_Link_.FRINPUTA,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ASCODFAM = space(10)
      endif
      this.w_FRDIMENS = 0
      this.w_FRNUMDEC = 0
      this.w_INPUTA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.FRCODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ASCODFAM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.FAM_ATTR_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.FAM_ATTR_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_6.FRCODICE as FRCODICE206"+ ",link_2_6.FRDIMENS as FRDIMENS206"+ ",link_2_6.FRNUMDEC as FRNUMDEC206"+ ",link_2_6.FRINPUTA as FRINPUTA206"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_6 on ASS_ATTR.ASCODFAM=link_2_6.FRCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_6"
          i_cKey=i_cKey+'+" and ASS_ATTR.ASCODFAM=link_2_6.FRCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CODFAM
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ATTR_IDX,3]
    i_lTable = "FAM_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ATTR_IDX,2], .t., this.FAM_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFAM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFAM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FRCODICE,FRTIPOLO,FRNUMDEC,FRDIMENS,FR_TABLE,FR_CAMPO,FR__ZOOM,FRZOOMOZ,FRCODFAM,FRCAMCOL";
                   +" from "+i_cTable+" "+i_lTable+" where FRCODICE="+cp_ToStrODBC(this.w_CODFAM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FRCODICE',this.w_CODFAM)
            select FRCODICE,FRTIPOLO,FRNUMDEC,FRDIMENS,FR_TABLE,FR_CAMPO,FR__ZOOM,FRZOOMOZ,FRCODFAM,FRCAMCOL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFAM = NVL(_Link_.FRCODICE,space(10))
      this.w_TIPO = NVL(_Link_.FRTIPOLO,space(1))
      this.w_DEC = NVL(_Link_.FRNUMDEC,0)
      this.w_DIME = NVL(_Link_.FRDIMENS,0)
      this.w_TABLE = NVL(_Link_.FR_TABLE,space(30))
      this.w_CAMPO = NVL(_Link_.FR_CAMPO,space(50))
      this.w_ZOOM = NVL(_Link_.FR__ZOOM,space(254))
      this.w_ZOOMOZ = NVL(_Link_.FRZOOMOZ,space(20))
      this.w_ATTRIBUTO = NVL(_Link_.FRCODFAM,space(10))
      this.w_ATTCOL = NVL(_Link_.FRCAMCOL,space(30))
      this.w_FR_TABLE = NVL(_Link_.FR_TABLE,space(30))
      this.w_FR__ZOOM = NVL(_Link_.FR__ZOOM,space(254))
      this.w_FR_CAMPO = NVL(_Link_.FR_CAMPO,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_CODFAM = space(10)
      endif
      this.w_TIPO = space(1)
      this.w_DEC = 0
      this.w_DIME = 0
      this.w_TABLE = space(30)
      this.w_CAMPO = space(50)
      this.w_ZOOM = space(254)
      this.w_ZOOMOZ = space(20)
      this.w_ATTRIBUTO = space(10)
      this.w_ATTCOL = space(30)
      this.w_FR_TABLE = space(30)
      this.w_FR__ZOOM = space(254)
      this.w_FR_CAMPO = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.FRCODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFAM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MODATTR
  func Link_2_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MODDATTR_IDX,3]
    i_lTable = "MODDATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MODDATTR_IDX,2], .t., this.MODDATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MODDATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MODATTR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MODATTR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE";
                   +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(this.w_MODATTR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_MODATTR)
            select MACODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MODATTR = NVL(_Link_.MACODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_MODATTR = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MODDATTR_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)
      cp_ShowWarn(i_cKey,this.MODDATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MODATTR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oASTIPDAT_2_14.value==this.w_ASTIPDAT)
      this.oPgFrm.Page1.oPag.oASTIPDAT_2_14.value=this.w_ASTIPDAT
      replace t_ASTIPDAT with this.oPgFrm.Page1.oPag.oASTIPDAT_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oASTIPNUM_2_15.value==this.w_ASTIPNUM)
      this.oPgFrm.Page1.oPag.oASTIPNUM_2_15.value=this.w_ASTIPNUM
      replace t_ASTIPNUM with this.oPgFrm.Page1.oPag.oASTIPNUM_2_15.value
    endif
    if not(this.oPgFrm.Page1.oPag.oASTIPCAR_2_16.value==this.w_ASTIPCAR)
      this.oPgFrm.Page1.oPag.oASTIPCAR_2_16.value=this.w_ASTIPCAR
      replace t_ASTIPCAR with this.oPgFrm.Page1.oPag.oASTIPCAR_2_16.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oASCODATT_2_4.value==this.w_ASCODATT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oASCODATT_2_4.value=this.w_ASCODATT
      replace t_ASCODATT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oASCODATT_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oASCODFAM_2_6.value==this.w_ASCODFAM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oASCODFAM_2_6.value=this.w_ASCODFAM
      replace t_ASCODFAM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oASCODFAM_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oASVALATT_2_17.value==this.w_ASVALATT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oASVALATT_2_17.value=this.w_ASVALATT
      replace t_ASVALATT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oASVALATT_2_17.value
    endif
    cp_SetControlsValueExtFlds(this,'ASS_ATTR')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsar_mra
      LOCAL Prog,L_MESSAGE
      If i_bres And (.cFunction='Edit' Or .cFunction='Load')
          *--- GSAG_MIM fa il controllo degli attributi da GSAR_BCN
          If This.numrow()>0 And Vartype(This.oparentobject)='O' AND UPPER(this.oparentobject.CLASS)<>'TGSAG_MIM'
              L_MESSAGE = CHK_ELE_ATTR( "D", This)
              If Not Empty (L_MESSAGE)
                  i_bnoChk = .F.
                  i_bres = .F.
                  i_cErrorMsg = L_MESSAGE
              Endif
          Endif
           
          If i_bres And Vartype(This.oparentobject.oparentobject)='O'
              Prog=This.oparentobject.oparentobject
              Prog.w_gestguid=This.w_guid
              Prog.bUpdated=.T.
              Prog.EcpSave()
              *--- Attivo nuovamente la maschera in modo tale da far scattare l'evento
              *--- activate delle gestione padre (prog) e riabilitare le toolbar
              This.oparentobject.Show()
          Endif
      Endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not((.w_TIPO$'FA' AND (GSAR2BCZ(.w_TABLE, this, .w_ATTCOL, .w_CAMPO, .w_ATTRIBUTO,.w_INPUTA,.w_ASCODFAM,.w_TIPO))) OR .w_TIPO$'LMB') and (.w_AUTOMA <> 'S') and (.w_CPROWORD<>0 and not empty (.w_ASCODATT) and not empty(.w_ASCODFAM))
          .oNewFocus=.oPgFrm.Page1.oPag.oASTIPDAT_2_14
          i_bRes = .f.
          i_bnoChk = .f.
        case   not((.w_TIPO$'FA' AND (GSAR2BCZ(.w_TABLE, this, .w_ATTCOL, .w_CAMPO, .w_ATTRIBUTO,.w_INPUTA,.w_ASCODFAM,.w_TIPO))) OR .w_TIPO$'LMB') and (.w_AUTOMA <> 'S') and (.w_CPROWORD<>0 and not empty (.w_ASCODATT) and not empty(.w_ASCODFAM))
          .oNewFocus=.oPgFrm.Page1.oPag.oASTIPNUM_2_15
          i_bRes = .f.
          i_bnoChk = .f.
        case   not(((.w_TIPO$'FA' AND (GSAR2BCZ(.w_TABLE, this, .w_ATTCOL, .w_CAMPO, .w_ATTRIBUTO,.w_INPUTA,.w_ASCODFAM,.w_TIPO))) OR .w_TIPO$'LMB') AND LEN(ALLTRIM(.w_ASTIPCAR))<=.w_DIME) and (.w_AUTOMA <> 'S') and (.w_CPROWORD<>0 and not empty (.w_ASCODATT) and not empty(.w_ASCODFAM))
          .oNewFocus=.oPgFrm.Page1.oPag.oASTIPCAR_2_16
          i_bRes = .f.
          i_bnoChk = .f.
      endcase
      if .w_CPROWORD<>0 and not empty (.w_ASCODATT) and not empty(.w_ASCODFAM)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODGRU = this.w_CODGRU
    this.o_ASCODATT = this.w_ASCODATT
    this.o_ASCODFAM = this.w_ASCODFAM
    this.o_INPUTA = this.w_INPUTA
    this.o_DIME = this.w_DIME
    this.o_DEC = this.w_DEC
    this.o_ASTIPDAT = this.w_ASTIPDAT
    this.o_ASTIPNUM = this.w_ASTIPNUM
    this.o_ASTIPCAR = this.w_ASTIPCAR
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_CPROWORD<>0 and not empty (t_ASCODATT) and not empty(t_ASCODFAM))
    select(i_nArea)
    return(i_bRes)
  Endfunc
  Func CanDeleteRow()
    local i_bRes
    i_bRes=t_AUTOMA<>'S'
    if !i_bRes
      cp_ErrorMsg("Impossibile eliminare un gruppo ZZ","stop","")
    endif
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(9999,cp_maxroword()+10)
      .w_CODGRU=space(10)
      .w_ASMODFAM=space(20)
      .w_ASCODATT=space(10)
      .w_CODATT=space(10)
      .w_ASCODFAM=space(10)
      .w_CODFAM=space(10)
      .w_INPUTA=space(1)
      .w_CALCPIC=0
      .w_TIPO=space(1)
      .w_DIME=0
      .w_DEC=0
      .w_ASTIPDAT=ctod("  /  /  ")
      .w_ASTIPNUM=0
      .w_ASTIPCAR=space(41)
      .w_ASVALATT=space(20)
      .w_RIGA=0
      .w_AUTOMA=space(1)
      .w_TABLE=space(30)
      .w_CAMPO=space(50)
      .w_ZOOM=space(254)
      .w_ZOOMOZ=space(20)
      .w_ATTRIBUTO=space(10)
      .w_ATTCOL=space(30)
      .w_MODATTR=space(20)
      .w_FRDIMENS=0
      .w_FRNUMDEC=0
      .w_FR_TABLE=space(30)
      .w_FR__ZOOM=space(254)
      .w_FR_CAMPO=space(50)
      .w_GSAR_BIA_FLAG=space(10)
      .DoRTCalc(1,1,.f.)
        .w_CODGRU = IIF(TYPE('this.oparentobject.w_CODATT')='C', this.oparentobject.w_CODATT, '          ')
        .w_ASMODFAM = nvl(this.oparentobject.w_GEST,space(20))
        .w_ASCODATT = .w_CODGRU
      .DoRTCalc(4,4,.f.)
      if not(empty(.w_ASCODATT))
        .link_2_4('Full')
      endif
        .w_CODATT = .w_ASCODATT
      .DoRTCalc(5,5,.f.)
      if not(empty(.w_CODATT))
        .link_2_5('Full')
      endif
      .DoRTCalc(6,6,.f.)
      if not(empty(.w_ASCODFAM))
        .link_2_6('Full')
      endif
        .w_CODFAM = .w_ASCODFAM
      .DoRTCalc(7,7,.f.)
      if not(empty(.w_CODFAM))
        .link_2_7('Full')
      endif
      .DoRTCalc(8,8,.f.)
        .w_CALCPIC = iif(.w_inputa='N',DEFPICAT(.w_FRDIMENS,.w_FRNUMDEC),0)
      .DoRTCalc(10,13,.f.)
        .w_ASTIPDAT = cp_chartodate ('  -  -  ')
        .w_ASTIPNUM = 0
        .w_ASTIPCAR = SPACE(20)
        .w_ASVALATT = IIF(.w_INPUTA='D',DTOC(.w_ASTIPDAT),IIF(.w_INPUTA='C',ALLTRIM(.w_ASTIPCAR),IIF(.w_INPUTA='N',alltrim(STR(.w_ASTIPNUM,20,3)),space(20))))
      .DoRTCalc(18,20,.f.)
        .w_RIGA = .w_CPROWNUM
      .DoRTCalc(22,30,.f.)
        .w_MODATTR = IIF(TYPE('this.oparentobject.w_IMMODATR')='C', this.oparentobject.w_IMMODATR, .w_GEST)
      .DoRTCalc(31,31,.f.)
      if not(empty(.w_MODATTR))
        .link_2_28('Full')
      endif
      .DoRTCalc(32,36,.f.)
        .w_GSAR_BIA_FLAG = "N"
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_CODGRU = t_CODGRU
    this.w_ASMODFAM = t_ASMODFAM
    this.w_ASCODATT = t_ASCODATT
    this.w_CODATT = t_CODATT
    this.w_ASCODFAM = t_ASCODFAM
    this.w_CODFAM = t_CODFAM
    this.w_INPUTA = t_INPUTA
    this.w_CALCPIC = t_CALCPIC
    this.w_TIPO = t_TIPO
    this.w_DIME = t_DIME
    this.w_DEC = t_DEC
    this.w_ASTIPDAT = t_ASTIPDAT
    this.w_ASTIPNUM = t_ASTIPNUM
    this.w_ASTIPCAR = t_ASTIPCAR
    this.w_ASVALATT = t_ASVALATT
    this.w_RIGA = t_RIGA
    this.w_AUTOMA = t_AUTOMA
    this.w_TABLE = t_TABLE
    this.w_CAMPO = t_CAMPO
    this.w_ZOOM = t_ZOOM
    this.w_ZOOMOZ = t_ZOOMOZ
    this.w_ATTRIBUTO = t_ATTRIBUTO
    this.w_ATTCOL = t_ATTCOL
    this.w_MODATTR = t_MODATTR
    this.w_FRDIMENS = t_FRDIMENS
    this.w_FRNUMDEC = t_FRNUMDEC
    this.w_FR_TABLE = t_FR_TABLE
    this.w_FR__ZOOM = t_FR__ZOOM
    this.w_FR_CAMPO = t_FR_CAMPO
    this.w_GSAR_BIA_FLAG = t_GSAR_BIA_FLAG
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_CODGRU with this.w_CODGRU
    replace t_ASMODFAM with this.w_ASMODFAM
    replace t_ASCODATT with this.w_ASCODATT
    replace t_CODATT with this.w_CODATT
    replace t_ASCODFAM with this.w_ASCODFAM
    replace t_CODFAM with this.w_CODFAM
    replace t_INPUTA with this.w_INPUTA
    replace t_CALCPIC with this.w_CALCPIC
    replace t_TIPO with this.w_TIPO
    replace t_DIME with this.w_DIME
    replace t_DEC with this.w_DEC
    replace t_ASTIPDAT with this.w_ASTIPDAT
    replace t_ASTIPNUM with this.w_ASTIPNUM
    replace t_ASTIPCAR with this.w_ASTIPCAR
    replace t_ASVALATT with this.w_ASVALATT
    replace t_RIGA with this.w_RIGA
    replace t_AUTOMA with this.w_AUTOMA
    replace t_TABLE with this.w_TABLE
    replace t_CAMPO with this.w_CAMPO
    replace t_ZOOM with this.w_ZOOM
    replace t_ZOOMOZ with this.w_ZOOMOZ
    replace t_ATTRIBUTO with this.w_ATTRIBUTO
    replace t_ATTCOL with this.w_ATTCOL
    replace t_MODATTR with this.w_MODATTR
    replace t_FRDIMENS with this.w_FRDIMENS
    replace t_FRNUMDEC with this.w_FRNUMDEC
    replace t_FR_TABLE with this.w_FR_TABLE
    replace t_FR__ZOOM with this.w_FR__ZOOM
    replace t_FR_CAMPO with this.w_FR_CAMPO
    replace t_GSAR_BIA_FLAG with this.w_GSAR_BIA_FLAG
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsar_mraPag1 as StdContainer
  Width  = 520
  height = 190
  stdWidth  = 520
  stdheight = 190
  resizeXpos=182
  resizeYpos=175
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="HBNWIGPNUX",left=4, top=2, width=455,height=21,;
    caption='Object',;
   bGlobalFont=.t.,;
    nNumberColumn=4,Field1="CPROWORD",Label1="Riga",Field2="ASCODATT",Label2="Gruppi",Field3="ASCODFAM",Label3="Ah_MsgFormat(IIF(Isahe(),'Famiglia','Attributi'))",Field4="ASVALATT",Label4="Valore",Field5="IVNUMREG",Label5="N.",Field6="IVPERIND",Label6="%Ind.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 64131046


  add object oObj_1_4 as cp_runprogram with uid="PKWTBMWQTA",left=2, top=257, width=443,height=22,;
    caption='GSAR_BIA',;
   bGlobalFont=.t.,;
    prg="gsar_bia('C','VARCHNG')",;
    cEvent = "w_ASTIPCAR Changed,w_ASTIPDAT Changed, w_ASTIPNUM Changed",;
    nPag=1;
    , HelpContextID = 24965287


  add object oObj_1_5 as cp_runprogram with uid="MPQLWKRSTU",left=2, top=236, width=266,height=22,;
    caption='GSAR1BCA',;
   bGlobalFont=.t.,;
    prg="gsar1bca('VIS')",;
    cEvent = "Interroga",;
    nPag=1;
    , HelpContextID = 23269209

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-6,top=23,;
    width=452+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*7*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-5,top=24,width=451+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*7*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='GRU_ATTR|FAM_ATTR|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oASTIPDAT_2_14.Refresh()
      this.Parent.oASTIPNUM_2_15.Refresh()
      this.Parent.oASTIPCAR_2_16.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='GRU_ATTR'
        oDropInto=this.oBodyCol.oRow.oASCODATT_2_4
      case cFile='FAM_ATTR'
        oDropInto=this.oBodyCol.oRow.oASCODFAM_2_6
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oASTIPDAT_2_14 as STDATTRFIELD with uid="QEZQRSWCKH",rtseq=14,rtrep=.t.,;
    cFormVar="w_ASTIPDAT",value=ctod("  /  /  "),;
    ToolTipText = "Valore attributo",;
    HelpContextID = 226156454,;
    cTotal="", bFixedPos=.t., cQueryName = "ASTIPDAT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=186, Left=272, Top=162, bHasZoom = .t. 

  func oASTIPDAT_2_14.mCond()
    with this.Parent.oContained
      return (.w_AUTOMA <> 'S')
    endwith
  endfunc

  func oASTIPDAT_2_14.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_inputa='C' OR .w_inputa='N'))
    endwith
    endif
  endfunc

  func oASTIPDAT_2_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_TIPO$'FA' AND (GSAR2BCZ(.w_TABLE, this, .w_ATTCOL, .w_CAMPO, .w_ATTRIBUTO,.w_INPUTA,.w_ASCODFAM,.w_TIPO))) OR .w_TIPO$'LMB')
    endwith
    return bRes
  endfunc

  proc oASTIPDAT_2_14.mZoom
      with this.Parent.oContained
        GSAR_BIA(this.Parent.oContained, this, "DBLCLICK")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oASTIPNUM_2_15 as STDATTRFIELD with uid="OMDJEIYUEE",rtseq=15,rtrep=.t.,;
    cFormVar="w_ASTIPNUM",value=0,;
    ToolTipText = "Valore attributo",;
    HelpContextID = 210051155,;
    cTotal="", bFixedPos=.t., cQueryName = "ASTIPNUM",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=187, Left=272, Top=162, cSayPict=[v_PU(1+VVP)], cGetPict=[v_GU(1+VVP)], bHasZoom = .t. 

  func oASTIPNUM_2_15.mCond()
    with this.Parent.oContained
      return (.w_AUTOMA <> 'S')
    endwith
  endfunc

  func oASTIPNUM_2_15.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_inputa='C' OR .w_inputa='D'))
    endwith
    endif
  endfunc

  func oASTIPNUM_2_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_TIPO$'FA' AND (GSAR2BCZ(.w_TABLE, this, .w_ATTCOL, .w_CAMPO, .w_ATTRIBUTO,.w_INPUTA,.w_ASCODFAM,.w_TIPO))) OR .w_TIPO$'LMB')
    endwith
    return bRes
  endfunc

  proc oASTIPNUM_2_15.mZoom
      with this.Parent.oContained
        GSAR_BIA(this.Parent.oContained, this, "DBLCLICK" )
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oASTIPCAR_2_16 as STDATTRFIELD with uid="MSPQJCPSRU",rtseq=16,rtrep=.t.,;
    cFormVar="w_ASTIPCAR",value=space(41),;
    ToolTipText = "Valore attributo",;
    HelpContextID = 242933672,;
    cTotal="", bFixedPos=.t., cQueryName = "ASTIPCAR",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=187, Left=272, Top=162, InputMask=replicate('X',41), bHasZoom = .t. 

  func oASTIPCAR_2_16.mCond()
    with this.Parent.oContained
      return (.w_AUTOMA <> 'S')
    endwith
  endfunc

  func oASTIPCAR_2_16.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_inputa='N' OR .w_inputa='D'))
    endwith
    endif
  endfunc

  func oASTIPCAR_2_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (((.w_TIPO$'FA' AND (GSAR2BCZ(.w_TABLE, this, .w_ATTCOL, .w_CAMPO, .w_ATTRIBUTO,.w_INPUTA,.w_ASCODFAM,.w_TIPO))) OR .w_TIPO$'LMB') AND LEN(ALLTRIM(.w_ASTIPCAR))<=.w_DIME)
    endwith
    return bRes
  endfunc

  proc oASTIPCAR_2_16.mZoom
      with this.Parent.oContained
        GSAR_BIA(this.Parent.oContained, this, "DBLCLICK")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oBtn_2_21 as StdButton with uid="WCYTVTPWJY",width=48,height=45,;
   left=465, top=24,;
    CpPicture="bmp\legami.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per importare gli attributi obbligatori del modello";
    , HelpContextID = 80542342;
    , TabStop=.f., Caption='\<Importa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_21.Click()
      with this.Parent.oContained
        gsar1bca(this.Parent.oContained,"OBB")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oStr_3_1 as StdString with uid="EBGDUJNVET",Visible=.t., Left=172, Top=162,;
    Alignment=1, Width=98, Height=18,;
    Caption="Valore attributo"  ;
  , bGlobalFont=.t.
enddefine

* --- Defining Body row
define class tgsar_mraBodyRow as CPBodyRowCnt
  Width=442
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="LVDZBUGIZA",rtseq=1,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 33882774,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=37, Left=-2, Top=0, cSayPict=["9999"], cGetPict=["9999"]

  add object oASCODATT_2_4 as StdTrsField with uid="HFPYRNFAKL",rtseq=4,rtrep=.t.,;
    cFormVar="w_ASCODATT",value=space(10),;
    HelpContextID = 248123482,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=97, Left=38, Top=0, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="GRU_ATTR", cZoomOnZoom="GSAR_MGA", oKey_1_1="GRCODICE", oKey_1_2="this.w_ASCODATT"

  func oASCODATT_2_4.mCond()
    with this.Parent.oContained
      return (.w_ASCODATT<>'ZZAUTOART' And .w_ASCODATT<>'ZZAUTOCLI' And .w_ASCODATT<>'ZZAUTOFOR')
    endwith
  endfunc

  func oASCODATT_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oASCODATT_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oASCODATT_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRU_ATTR','*','GRCODICE',cp_AbsName(this.parent,'oASCODATT_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MGA',"Gruppo attributi",'GRUDEF.GRU_ATTR_VZM',this.parent.oContained
  endproc
  proc oASCODATT_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MGA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GRCODICE=this.parent.oContained.w_ASCODATT
    i_obj.ecpSave()
  endproc

  add object oASCODFAM_2_6 as StdTrsField with uid="MXGQQBTOAP",rtseq=6,rtrep=.t.,;
    cFormVar="w_ASCODFAM",value=space(10),;
    HelpContextID = 204861357,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=129, Left=138, Top=0, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="FAM_ATTR", cZoomOnZoom="GSAR_AAT", oKey_1_1="FRCODICE", oKey_1_2="this.w_ASCODFAM"

  func oASCODFAM_2_6.mCond()
    with this.Parent.oContained
      return (.w_AUTOMA <> 'S')
    endwith
  endfunc

  func oASCODFAM_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oASCODFAM_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oASCODFAM_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ATTR','*','FRCODICE',cp_AbsName(this.parent,'oASCODFAM_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AAT',"Attributi",'GSAR_AAT.FAM_ATTR_VZM',this.parent.oContained
  endproc
  proc oASCODFAM_2_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AAT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FRCODICE=this.parent.oContained.w_ASCODFAM
    i_obj.ecpSave()
  endproc

  add object oASVALATT_2_17 as StdTrsField with uid="EFWJGKAAUR",rtseq=17,rtrep=.t.,;
    cFormVar="w_ASVALATT",value=space(20),enabled=.f.,;
    HelpContextID = 255672410,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=167, Left=270, Top=0, InputMask=replicate('X',20)
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=6
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_mra','ASS_ATTR','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".GUID=ASS_ATTR.GUID";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsar_mra
define class STDATTRFIELD as stdFIELD
PROCEDURE Ecpdrop(oSource)
 * --- Recupero dallo zoom il valore lo scrivo sul tansitorio
 * --- ed invoco una mcalc per eventuali calcoli collegati
 this.value=CP_TODATE(oSource.xKey(1))
 * --- Sarebbe una Set, ma a causa del fatto che lo zoom ha gia valorizzato
 * --- il valore all'interno del control dobbiamo eseguire le operazioni
 * --- a mano
 This.Parent.oContained.Set(this.cFormVar,THIS.VALUE,.F.,.T.)
 This.Parent.oContained.mcalc(.t.)
endproc
enddefine

* --- Fine Area Manuale
