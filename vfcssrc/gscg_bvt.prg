* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bvt                                                        *
*              Creazione separata indic. operaz. effettuate                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][39][VRS_50]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-12-07                                                      *
* Last revis.: 2005-12-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEXEC
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bvt",oParentObject,m.pEXEC)
return(i_retval)

define class tgscg_bvt as StdBatch
  * --- Local variables
  pEXEC = space(1)
  w_CODAZI = space(5)
  w_CODATT = space(5)
  w_FLAGATT = space(1)
  w_DATAINIP = ctod("  /  /  ")
  w_DATAFINP = ctod("  /  /  ")
  w_IMPIVA = 0
  w_IMPON = 0
  w_FLPROV = space(1)
  w_IVACORR = 0
  w_IMPCORR = 0
  w_IVAVEN = 0
  w_IMPVEN = 0
  w_TOTIMP = 0
  w_TOTIVA = 0
  * --- WorkFile variables
  MAN_QUVT_idx=0
  AZIENDA_idx=0
  GSCG1BVT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eseguito da GSCG_SVT popola tabella figli GSCG_MVT 
    * --- S: esegue stampa Quadro VT
    *     I: esegue caricamento dati per regione
    this.w_CODAZI = i_CODAZI
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZATTIVI,AZCATAZI"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZATTIVI,AZCATAZI;
        from (i_cTable) where;
            AZCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_FLAGATT = NVL(cp_ToDate(_read_.AZATTIVI),cp_NullValue(_read_.AZATTIVI))
      this.w_CODATT = NVL(cp_ToDate(_read_.AZCATAZI),cp_NullValue(_read_.AZCATAZI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    do case
      case this.pEXEC="I"
        this.w_DATAINIP = MIN(Cp_chartodate("01-10-"+ALLTRIM(STR(YEAR(this.oParentObject.w_FINANNO)-1))),this.oParentObject.w_INIANNO)
        this.w_DATAFINP = MIN(Cp_chartodate("30-09-"+ALLTRIM(STR(YEAR(this.oParentObject.w_FINANNO)))),this.oParentObject.w_FINANNO)
        * --- Elimino Record gi� presenti
        * --- Delete from MAN_QUVT
        i_nConn=i_TableProp[this.MAN_QUVT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MAN_QUVT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"VTCODREG is not null ";
                 )
        else
          delete from (i_cTable) where;
                VTCODREG is not null ;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        this.w_FLPROV = "S"
        this.w_CODATT = IIF(this.w_FLAGATT="S",Space(5),this.w_CODATT)
        * --- Select from GSCG_SVT
        do vq_exec with 'GSCG_SVT',this,'_Curs_GSCG_SVT','',.f.,.t.
        if used('_Curs_GSCG_SVT')
          select _Curs_GSCG_SVT
          locate for 1=1
          do while not(eof())
          AH_ERRORMSG("Esistono registrazioni provvisorie che non verranno considerate nel calcolo")
          Exit
            select _Curs_GSCG_SVT
            continue
          enddo
          use
        endif
        this.w_FLPROV = "N"
        this.w_IMPIVA = 0
        this.w_IMPON = 0
        this.oParentObject.GSCG_MVT.MarkPos()     
        * --- Select from GSCGRSVT
        do vq_exec with 'GSCGRSVT',this,'_Curs_GSCGRSVT','',.f.,.t.
        if used('_Curs_GSCGRSVT')
          select _Curs_GSCGRSVT
          locate for 1=1
          do while not(eof())
           
 Select (this.oParentObject.GSCG_MVT.ctrsname)
          this.oParentObject.GSCG_MVT.AddRow()     
          this.w_IMPIVA = this.w_IMPIVA + Nvl(_Curs_GSCGRSVT.IVA,0)-cp_Round(Nvl(_Curs_GSCGRSVT.IVA,0),0)
          this.w_IMPON = this.w_IMPON + Nvl(_Curs_GSCGRSVT.IMPONIBILE,0)-cp_Round(Nvl(_Curs_GSCGRSVT.IMPONIBILE,0),0)
          this.oParentObject.GSCG_MVT.w_VTIMPONI = cp_Round(Nvl(_Curs_GSCGRSVT.IMPONIBILE,0),0)
          this.oParentObject.GSCG_MVT.w_VTIMPIVA = cp_Round(Nvl(_Curs_GSCGRSVT.IVA,0),0)
          this.oParentObject.GSCG_MVT.w_VTCODREG = _Curs_GSCGRSVT.REGIONE
          this.oParentObject.GSCG_MVT.w_VTDESREG = _Curs_GSCGRSVT.RPDESCRI
          this.oParentObject.GSCG_MVT.SaveRow()     
            select _Curs_GSCGRSVT
            continue
          enddo
          use
        endif
        * --- Aggiungo sull'utlima riga l'eventuale resto
        if this.w_IMPON<>0
          this.oParentObject.GSCG_MVT.w_VTIMPONI = cp_Round(this.oParentObject.GSCG_MVT.w_VTIMPONI+this.w_IMPON,0)
        endif
        if this.w_IMPIVA<>0
          this.oParentObject.GSCG_MVT.w_VTIMPIVA = cp_Round(this.oParentObject.GSCG_MVT.w_VTIMPIVA+this.w_IMPIVA,0)
        endif
        this.oParentObject.GSCG_MVT.SaveRow()     
        this.oParentObject.GSCG_MVT.RePos()     
        * --- Select from GSCGTSVT
        do vq_exec with 'GSCGTSVT',this,'_Curs_GSCGTSVT','',.f.,.t.
        if used('_Curs_GSCGTSVT')
          select _Curs_GSCGTSVT
          locate for 1=1
          do while not(eof())
          this.oParentObject.w_VT1IMPIVA = cp_Round(Nvl(_Curs_GSCGTSVT.IVACORR,0),0)
          this.oParentObject.w_VT1IMPON = cp_Round(Nvl(_Curs_GSCGTSVT.IMPCORR,0),0)
          this.oParentObject.w_VT2IMPIVA = cp_Round(Nvl(_Curs_GSCGTSVT.IVAVEN,0),0)
          this.oParentObject.w_VT2IMPON = cp_Round(Nvl(_Curs_GSCGTSVT.IMPVEN,0),0)
          this.oParentObject.w_VTIMPON = cp_Round(Nvl(_Curs_GSCGTSVT.TOTIMP,0),0)
          this.oParentObject.w_VTIMPIVA = cp_Round(Nvl(_Curs_GSCGTSVT.TOTIVA,0),0)
          EXIT
            select _Curs_GSCGTSVT
            continue
          enddo
          use
        endif
      case this.pEXEC="S"
        * --- Esegue stampa quadro VT
        this.oParentObject.GSCG_MVT.Exec_Select("__Tmp__","Nvl(t_VTIMPONI,0) As VTIMPONI,Nvl(t_VTIMPIVA,0) As VTIMPIVA,t_VTCODREG AS VTCODREG,t_VTDESREG AS VTDESREG"," Not Deleted()  ","t_VTDESREG","","")     
         
 L_VT1IMPIVA=this.oParentObject.w_VT1IMPIVA 
 L_VT1IMPON=this.oParentObject.w_VT1IMPON 
 L_VT2IMPIVA=this.oParentObject.w_VT2IMPIVA 
 L_VT2IMPON=this.oParentObject.w_VT2IMPON 
 L_VTIMPIVA=this.oParentObject.w_VTIMPIVA 
 L_VTIMPON=this.oParentObject.w_VTIMPON 
 L_INIANNO=this.oParentObject.w_INIANNO 
 L_FINANNO=this.oParentObject.w_FINANNO 
 Cp_ChPrn("QUERY\GSCG2SVT.FRX", " ", this)
        if USED("__TMP__")
           
 Select __TMP__ 
 Use
        endif
      case this.pEXEC="F"
        * --- Esegue elaborazione per fisco Azienda
        this.w_FLPROV = "N"
        this.oParentObject.w_ANNDIC = IIF(Not Empty(this.oParentObject.w_ANNDIC),this.oParentObject.w_ANNDIC,ALLTRIM(STR(YEAR(i_datsys))))
        this.oParentObject.w_INIANNO = iCASE(Not Empty(this.oParentObject.w_INIANNO),this.oParentObject.w_INIANNO,Not Empty(this.oParentObject.w_ANNDIC),Cp_chartodate("01-01-"+ALLTRIM(this.oParentObject.w_ANNDIC)),Cp_chartodate("01-01-"+ALLTRIM(STR(YEAR(i_datsys)))))
        this.oParentObject.w_FINANNO = iCASE(Not Empty(this.oParentObject.w_FINANNO),this.oParentObject.w_FINANNO,Not Empty(this.oParentObject.w_ANNDIC),Cp_chartodate("31-12-"+ALLTRIM(this.oParentObject.w_ANNDIC)),Cp_chartodate("31-12-"+ALLTRIM(STR(YEAR(i_datsys)))))
        this.w_DATAINIP = MIN(Cp_chartodate("01-10-"+ALLTRIM(STR(YEAR(this.oParentObject.w_FINANNO)-1))),this.oParentObject.w_INIANNO)
        this.w_DATAFINP = MIN(Cp_chartodate("30-09-"+ALLTRIM(STR(YEAR(this.oParentObject.w_FINANNO)))),this.oParentObject.w_FINANNO)
        this.w_CODATT = IIF(this.w_FLAGATT="S",Space(5),this.w_CODATT)
        * --- Create temporary table GSCG1BVT
        i_nIdx=cp_AddTableDef('GSCG1BVT') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('QUERY\GSCGFBVT',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.GSCG1BVT_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- Select from GSCGTSVT
        do vq_exec with 'GSCGTSVT',this,'_Curs_GSCGTSVT','',.f.,.t.
        if used('_Curs_GSCGTSVT')
          select _Curs_GSCGTSVT
          locate for 1=1
          do while not(eof())
          this.w_IVACORR = cp_Round(Nvl(_Curs_GSCGTSVT.IVACORR,0),0)
          this.w_IMPCORR = cp_Round(Nvl(_Curs_GSCGTSVT.IMPCORR,0),0)
          this.w_IVAVEN = cp_Round(Nvl(_Curs_GSCGTSVT.IVAVEN,0),0)
          this.w_IMPVEN = cp_Round(Nvl(_Curs_GSCGTSVT.IMPVEN,0),0)
          this.w_TOTIMP = cp_Round(Nvl(_Curs_GSCGTSVT.TOTIMP,0),0)
          this.w_TOTIVA = cp_Round(Nvl(_Curs_GSCGTSVT.TOTIVA,0),0)
          * --- Insert into GSCG1BVT
          i_nConn=i_TableProp[this.GSCG1BVT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.GSCG1BVT_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.GSCG1BVT_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"IVACORR"+",TOTIVA"+",IMPCORR"+",IVAVEN"+",IMPVEN"+",TOTIMP"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_IVACORR),'GSCG1BVT','IVACORR');
            +","+cp_NullLink(cp_ToStrODBC(this.w_TOTIVA),'GSCG1BVT','TOTIVA');
            +","+cp_NullLink(cp_ToStrODBC(this.w_IMPCORR),'GSCG1BVT','IMPCORR');
            +","+cp_NullLink(cp_ToStrODBC(this.w_IVAVEN),'GSCG1BVT','IVAVEN');
            +","+cp_NullLink(cp_ToStrODBC(this.w_IMPVEN),'GSCG1BVT','IMPVEN');
            +","+cp_NullLink(cp_ToStrODBC(this.w_TOTIMP),'GSCG1BVT','TOTIMP');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'IVACORR',this.w_IVACORR,'TOTIVA',this.w_TOTIVA,'IMPCORR',this.w_IMPCORR,'IVAVEN',this.w_IVAVEN,'IMPVEN',this.w_IMPVEN,'TOTIMP',this.w_TOTIMP)
            insert into (i_cTable) (IVACORR,TOTIVA,IMPCORR,IVAVEN,IMPVEN,TOTIMP &i_ccchkf. );
               values (;
                 this.w_IVACORR;
                 ,this.w_TOTIVA;
                 ,this.w_IMPCORR;
                 ,this.w_IVAVEN;
                 ,this.w_IMPVEN;
                 ,this.w_TOTIMP;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          EXIT
            select _Curs_GSCGTSVT
            continue
          enddo
          use
        endif
    endcase
  endproc


  proc Init(oParentObject,pEXEC)
    this.pEXEC=pEXEC
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='MAN_QUVT'
    this.cWorkTables[2]='AZIENDA'
    this.cWorkTables[3]='*GSCG1BVT'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_GSCG_SVT')
      use in _Curs_GSCG_SVT
    endif
    if used('_Curs_GSCGRSVT')
      use in _Curs_GSCGRSVT
    endif
    if used('_Curs_GSCGTSVT')
      use in _Curs_GSCGTSVT
    endif
    if used('_Curs_GSCGTSVT')
      use in _Curs_GSCGTSVT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEXEC"
endproc
