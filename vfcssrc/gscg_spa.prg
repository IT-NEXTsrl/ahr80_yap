* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_spa                                                        *
*              Stampa plafond annuale                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_156]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-07-18                                                      *
* Last revis.: 2007-11-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_spa",oParentObject))

* --- Class definition
define class tgscg_spa as StdForm
  Top    = 56
  Left   = 71

  * --- Standard Properties
  Width  = 452
  Height = 215
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-11-26"
  HelpContextID=141174121
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Constant Properties
  _IDX = 0
  AZIENDA_IDX = 0
  VALUTE_IDX = 0
  ATTIMAST_IDX = 0
  DAT_IVAN_IDX = 0
  cPrg = "gscg_spa"
  cComment = "Stampa plafond annuale"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_VALUTA = space(3)
  w_CODAZI = space(5)
  w_CODATT = space(5)
  w_DECTOT = 0
  w_CALCPICT = space(10)
  w_ANNOEL = space(4)
  w_DESATT = space(35)
  w_SIMBOLO = space(5)
  w_VALPLA = space(3)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_spaPag1","gscg_spa",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oANNOEL_1_6
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='ATTIMAST'
    this.cWorkTables[4]='DAT_IVAN'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_VALUTA=space(3)
      .w_CODAZI=space(5)
      .w_CODATT=space(5)
      .w_DECTOT=0
      .w_CALCPICT=space(10)
      .w_ANNOEL=space(4)
      .w_DESATT=space(35)
      .w_SIMBOLO=space(5)
      .w_VALPLA=space(3)
        .w_VALUTA = g_PERVAL
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_VALUTA))
          .link_1_1('Full')
        endif
        .w_CODAZI = i_CODAZI
        .w_CODATT = g_CATAZI
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODATT))
          .link_1_3('Full')
        endif
          .DoRTCalc(4,4,.f.)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_ANNOEL = alltrim(str(YEAR(i_datsys)))
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_ANNOEL))
          .link_1_6('Full')
        endif
    endwith
    this.DoRTCalc(7,9,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
            .w_CODAZI = i_CODAZI
          .link_1_3('Full')
        .DoRTCalc(4,4,.t.)
            .w_CALCPICT = DEFPIC(.w_DECTOT)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(6,9,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_13.visible=!this.oPgFrm.Page1.oPag.oBtn_1_13.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=VALUTA
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALUTA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALUTA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALUTA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALUTA)
            select VACODVAL,VASIMVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALUTA = NVL(_Link_.VACODVAL,space(3))
      this.w_SIMBOLO = NVL(_Link_.VASIMVAL,space(5))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALUTA = space(3)
      endif
      this.w_SIMBOLO = space(5)
      this.w_DECTOT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALUTA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATT
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIMAST_IDX,3]
    i_lTable = "ATTIMAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2], .t., this.ATTIMAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODATT,ATDESATT";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_CODATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODATT',this.w_CODATT)
            select ATCODATT,ATDESATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATT = NVL(_Link_.ATCODATT,space(5))
      this.w_DESATT = NVL(_Link_.ATDESATT,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODATT = space(5)
      endif
      this.w_DESATT = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2])+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIMAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANNOEL
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DAT_IVAN_IDX,3]
    i_lTable = "DAT_IVAN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DAT_IVAN_IDX,2], .t., this.DAT_IVAN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DAT_IVAN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANNOEL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DAT_IVAN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IA__ANNO like "+cp_ToStrODBC(trim(this.w_ANNOEL)+"%");
                   +" and IACODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select IACODAZI,IA__ANNO,IAVALPLA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IACODAZI,IA__ANNO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IACODAZI',this.w_CODAZI;
                     ,'IA__ANNO',trim(this.w_ANNOEL))
          select IACODAZI,IA__ANNO,IAVALPLA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IACODAZI,IA__ANNO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANNOEL)==trim(_Link_.IA__ANNO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANNOEL) and !this.bDontReportError
            deferred_cp_zoom('DAT_IVAN','*','IACODAZI,IA__ANNO',cp_AbsName(oSource.parent,'oANNOEL_1_6'),i_cWhere,'',"Selezione anno di imposta",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IACODAZI,IA__ANNO,IAVALPLA";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select IACODAZI,IA__ANNO,IAVALPLA;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Inserire l'anno nei dati IVA")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IACODAZI,IA__ANNO,IAVALPLA";
                     +" from "+i_cTable+" "+i_lTable+" where IA__ANNO="+cp_ToStrODBC(oSource.xKey(2));
                     +" and IACODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IACODAZI',oSource.xKey(1);
                       ,'IA__ANNO',oSource.xKey(2))
            select IACODAZI,IA__ANNO,IAVALPLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANNOEL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IACODAZI,IA__ANNO,IAVALPLA";
                   +" from "+i_cTable+" "+i_lTable+" where IA__ANNO="+cp_ToStrODBC(this.w_ANNOEL);
                   +" and IACODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IACODAZI',this.w_CODAZI;
                       ,'IA__ANNO',this.w_ANNOEL)
            select IACODAZI,IA__ANNO,IAVALPLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANNOEL = NVL(_Link_.IA__ANNO,space(4))
      this.w_VALPLA = NVL(_Link_.IAVALPLA,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ANNOEL = space(4)
      endif
      this.w_VALPLA = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DAT_IVAN_IDX,2])+'\'+cp_ToStr(_Link_.IACODAZI,1)+'\'+cp_ToStr(_Link_.IA__ANNO,1)
      cp_ShowWarn(i_cKey,this.DAT_IVAN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANNOEL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oANNOEL_1_6.value==this.w_ANNOEL)
      this.oPgFrm.Page1.oPag.oANNOEL_1_6.value=this.w_ANNOEL
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMBOLO_1_16.value==this.w_SIMBOLO)
      this.oPgFrm.Page1.oPag.oSIMBOLO_1_16.value=this.w_SIMBOLO
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CODATT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODATT_1_3.SetFocus()
            i_bnoObbl = !empty(.w_CODATT)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ANNOEL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANNOEL_1_6.SetFocus()
            i_bnoObbl = !empty(.w_ANNOEL)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire l'anno nei dati IVA")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgscg_spaPag1 as StdContainer
  Width  = 448
  height = 215
  stdWidth  = 448
  stdheight = 215
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oANNOEL_1_6 as StdField with uid="YULKEZCXNS",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ANNOEL", cQueryName = "ANNOEL",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire l'anno nei dati IVA",;
    ToolTipText = "Anno di selezione dei documenti da elaborare",;
    HelpContextID = 130413306,;
   bGlobalFont=.t.,;
    Height=21, Width=53, Left=59, Top=140, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="DAT_IVAN", oKey_1_1="IACODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="IA__ANNO", oKey_2_2="this.w_ANNOEL"

  func oANNOEL_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oANNOEL_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANNOEL_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DAT_IVAN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"IACODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"IACODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'DAT_IVAN','*','IACODAZI,IA__ANNO',cp_AbsName(this.parent,'oANNOEL_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Selezione anno di imposta",'',this.parent.oContained
  endproc


  add object oBtn_1_12 as StdButton with uid="PWCUCFWQQP",left=314, top=164, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per avviare la stampa";
    , HelpContextID = 141145370;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      with this.Parent.oContained
        GSCG_BPI(this.Parent.oContained,"S")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_12.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty( .w_CODATT ) And Not Empty( .w_ANNOEL ))
      endwith
    endif
  endfunc


  add object oBtn_1_13 as StdButton with uid="LNMWHHBLEZ",left=364, top=164, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 133856698;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_13.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (g_APPLICATION = "ADHOC REVOLUTION")
      endwith
    endif
  endfunc

  func oBtn_1_13.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_APPLICATION <> "ADHOC REVOLUTION")
     endwith
    endif
  endfunc

  add object oSIMBOLO_1_16 as StdField with uid="FJOJZTFZMP",rtseq=8,rtrep=.f.,;
    cFormVar = "w_SIMBOLO", cQueryName = "SIMBOLO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 120784602,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=364, Top=138, InputMask=replicate('X',5)

  add object oStr_1_7 as StdString with uid="TMIDGPSAFY",Visible=.t., Left=9, Top=8,;
    Alignment=2, Width=401, Height=18,;
    Caption="Attenzione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.t., FontStrikeThru=.f.

  add object oStr_1_8 as StdString with uid="XATYSYMGDQ",Visible=.t., Left=7, Top=27,;
    Alignment=0, Width=414, Height=18,;
    Caption="Questa funzione restituisce una stampa necessaria per la compilazione"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="OOFNTNXFHF",Visible=.t., Left=7, Top=66,;
    Alignment=0, Width=419, Height=18,;
    Caption="le informazioni esclusivamente dai movimenti contabili I.V.A."  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="SPPCRZHREO",Visible=.t., Left=7, Top=85,;
    Alignment=0, Width=430, Height=18,;
    Caption="La stampa elabora tutti i codici IVA che hanno valorizzati i flag acquisti"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="NWLJYNQMIL",Visible=.t., Left=10, Top=140,;
    Alignment=1, Width=43, Height=15,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="NWNUQIZWHZ",Visible=.t., Left=227, Top=140,;
    Alignment=1, Width=128, Height=18,;
    Caption="Importi espressi in:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="FXHUVHILPS",Visible=.t., Left=7, Top=103,;
    Alignment=0, Width=416, Height=18,;
    Caption="plafond ed esportazioni, indipendentemente dal tipo di plafond."  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="MSKRCRXUQB",Visible=.t., Left=7, Top=47,;
    Alignment=0, Width=418, Height=18,;
    Caption="del quadro VC della dichiarazione annuale IVA leggendo"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_spa','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
