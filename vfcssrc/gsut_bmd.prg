* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bmd                                                        *
*              Apertura modelli documenti                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-12-06                                                      *
* Last revis.: 2010-03-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_PATMOD
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bmd",oParentObject,m.w_PATMOD)
return(i_retval)

define class tgsut_bmd as StdBatch
  * --- Local variables
  w_PATMOD = space(254)
  w_OBJECT = .NULL.
  w_HANDLE = 0
  w_SHELLEXEC = 0
  w_cLink = space(255)
  w_cParms = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * ---                                       Apre il file all'interno generazione di un nuovo documento (mask GSUT_KMD e GSUT_AMD)
    if cp_fileexist(this.w_PATMOD)
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      if AH_yesno("Il file di riferimento non esiste, si desidera crearlo?")
        if !DIRE(SUBSTR(Alltrim(this.w_PATMOD),1,AT("\",Alltrim(this.w_PATMOD),OCCURS("\",Alltrim(this.w_PATMOD)))))
          AH_ERRORMSG("Cartella di destinazione inesistente",48,"Attenzione !")
        else
          this.w_HANDLE = fcreate(Alltrim(this.w_PATMOD))
          if this.w_HANDLE<>-1
            fclose(this.w_HANDLE)
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            AH_ERRORMSG("Errore nella creazione del file ",48,"Attenzione !")
          endif
        endif
      endif
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia eseguibile associato al file
    this.w_cLink = fullpath(this.w_PATMOD)
    this.w_cParms = space(1)
    declare integer ShellExecute in shell32.dll integer nHwnd, string cOperation, string cFileName, string cParms, string cDir, integer nShowCmd
     declare integer FindWindow in win32api string cNull, string cWinName 
    w_hWnd = FindWindow(0,_screen.caption)
    this.w_SHELLEXEC = ShellExecute(w_hWnd,"open",alltrim(this.w_cLink),alltrim(this.w_cParms),sys(5)+curdir(),1)
    if this.w_SHELLEXEC <= 32
      AH_ERRORMSG("Si � verificato un errore in fase di apertura del file ",48,"Attenzione !!!")
    endif
  endproc


  proc Init(oParentObject,w_PATMOD)
    this.w_PATMOD=w_PATMOD
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_PATMOD"
endproc
