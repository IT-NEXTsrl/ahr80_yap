* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsor_st1                                                        *
*              Zoom documenti                                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_95]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-04-21                                                      *
* Last revis.: 2006-01-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsor_st1",oParentObject))

* --- Class definition
define class tgsor_st1 as StdForm
  Top    = 36
  Left   = 49

  * --- Standard Properties
  Width  = 666
  Height = 335
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2006-01-17"
  HelpContextID=74017129
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=29

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsor_st1"
  cComment = "Zoom documenti"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CAUSAL = space(5)
  w_CAUTIPO = space(1)
  w_DATA1 = ctod('  /  /  ')
  w_DATA2 = ctod('  /  /  ')
  w_TIPO = space(1)
  w_CODCON = space(15)
  w_DATDOCU = ctod('  /  /  ')
  w_NUMERO = 0
  w_CAUDOC = space(5)
  w_NUMDOC = 0
  w_NUMRIF = 0
  w_ALFDOC = space(10)
  w_ALFRIF = space(10)
  w_DATDOC = ctod('  /  /  ')
  w_DATRIF = ctod('  /  /  ')
  w_DATREG1 = ctod('  /  /  ')
  w_DATREG2 = ctod('  /  /  ')
  w_CODCLIFOR = space(15)
  w_TIPOCF = space(1)
  o_TIPOCF = space(1)
  w_MVSERIAL = space(10)
  w_TIPCAU = space(1)
  w_MVRIFCLI = space(30)
  o_MVRIFCLI = space(30)
  w_NUMDO1 = 0
  w_ALFDO1 = space(10)
  w_DATDO1 = ctod('  /  /  ')
  w_RIFTEST = 0
  w_DATEST = ctod('  /  /  ')
  w_ALFDOC1 = space(10)
  w_ALFRIF1 = space(10)
  w_docu = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsor_st1Pag1","gsor_st1",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_docu = this.oPgFrm.Pages(1).oPag.docu
    DoDefault()
    proc Destroy()
      this.w_docu = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CAUSAL=space(5)
      .w_CAUTIPO=space(1)
      .w_DATA1=ctod("  /  /  ")
      .w_DATA2=ctod("  /  /  ")
      .w_TIPO=space(1)
      .w_CODCON=space(15)
      .w_DATDOCU=ctod("  /  /  ")
      .w_NUMERO=0
      .w_CAUDOC=space(5)
      .w_NUMDOC=0
      .w_NUMRIF=0
      .w_ALFDOC=space(10)
      .w_ALFRIF=space(10)
      .w_DATDOC=ctod("  /  /  ")
      .w_DATRIF=ctod("  /  /  ")
      .w_DATREG1=ctod("  /  /  ")
      .w_DATREG2=ctod("  /  /  ")
      .w_CODCLIFOR=space(15)
      .w_TIPOCF=space(1)
      .w_MVSERIAL=space(10)
      .w_TIPCAU=space(1)
      .w_MVRIFCLI=space(30)
      .w_NUMDO1=0
      .w_ALFDO1=space(10)
      .w_DATDO1=ctod("  /  /  ")
      .w_RIFTEST=0
      .w_DATEST=ctod("  /  /  ")
      .w_ALFDOC1=space(10)
      .w_ALFRIF1=space(10)
      .w_CAUDOC=oParentObject.w_CAUDOC
      .w_NUMDOC=oParentObject.w_NUMDOC
      .w_NUMRIF=oParentObject.w_NUMRIF
      .w_ALFDOC=oParentObject.w_ALFDOC
      .w_ALFRIF=oParentObject.w_ALFRIF
      .w_DATDOC=oParentObject.w_DATDOC
      .w_DATRIF=oParentObject.w_DATRIF
      .w_DATREG1=oParentObject.w_DATREG1
      .w_DATREG2=oParentObject.w_DATREG2
      .w_CODCLIFOR=oParentObject.w_CODCLIFOR
      .w_TIPOCF=oParentObject.w_TIPOCF
      .w_MVSERIAL=oParentObject.w_MVSERIAL
      .w_TIPCAU=oParentObject.w_TIPCAU
      .w_NUMDO1=oParentObject.w_NUMDO1
      .w_ALFDO1=oParentObject.w_ALFDO1
      .w_DATDO1=oParentObject.w_DATDO1
        .w_CAUSAL = .w_CAUDOC
        .w_CAUTIPO = .w_TIPCAU
        .w_DATA1 = .w_DATREG1
        .w_DATA2 = .w_DATREG2
        .w_TIPO = IIF(.w_TIPOCF='N','',.w_TIPOCF)
        .w_CODCON = .w_CODCLIFOR
        .w_DATDOCU = .w_DATDOC
        .w_NUMERO = .w_NUMDOC
        .w_CAUDOC = .w_docu.getVar('MVTIPDOC')
        .w_NUMDOC = .w_docu.getVar('MVNUMDOC')
        .w_NUMRIF = .w_docu.getVar('MVNUMEST')
        .w_ALFDOC = .w_docu.getVar('MVALFDOC')
        .w_ALFRIF = .w_docu.getVar('MVALFEST')
        .w_DATDOC = .w_docu.getVar('MVDATDOC')
        .w_DATRIF = .w_docu.getVar('MVDATEST')
          .DoRTCalc(16,17,.f.)
        .w_CODCLIFOR = .w_docu.getVar('MVCODCON')
        .w_TIPOCF = .w_docu.getVar('MVTIPCON')
        .w_MVSERIAL = .w_docu.getVar('MVSERIAL')
      .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
      .oPgFrm.Page1.oPag.docu.Calculate()
          .DoRTCalc(21,21,.f.)
        .w_MVRIFCLI = .w_docu.getVar('MVRIFCLI')
      .oPgFrm.Page1.oPag.oObj_1_25.Calculate()
        .w_NUMDO1 = .w_docu.getVar('MVNUMDOC')
        .w_ALFDO1 = .w_docu.getVar('MVALFDOC')
        .w_DATDO1 = .w_docu.getVar('MVDATDOC')
        .w_RIFTEST = this.oParentObject.w_NUMRIF
        .w_DATEST = this.oParentObject.w_DATRIF
        .w_ALFDOC1 = this.oParentObject.w_ALFDOC
        .w_ALFRIF1 = this.oParentObject.w_ALFRIF
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_CAUDOC=.w_CAUDOC
      .oParentObject.w_NUMDOC=.w_NUMDOC
      .oParentObject.w_NUMRIF=.w_NUMRIF
      .oParentObject.w_ALFDOC=.w_ALFDOC
      .oParentObject.w_ALFRIF=.w_ALFRIF
      .oParentObject.w_DATDOC=.w_DATDOC
      .oParentObject.w_DATRIF=.w_DATRIF
      .oParentObject.w_DATREG1=.w_DATREG1
      .oParentObject.w_DATREG2=.w_DATREG2
      .oParentObject.w_CODCLIFOR=.w_CODCLIFOR
      .oParentObject.w_TIPOCF=.w_TIPOCF
      .oParentObject.w_MVSERIAL=.w_MVSERIAL
      .oParentObject.w_TIPCAU=.w_TIPCAU
      .oParentObject.w_NUMDO1=.w_NUMDO1
      .oParentObject.w_ALFDO1=.w_ALFDO1
      .oParentObject.w_DATDO1=.w_DATDO1
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
        if .o_TIPOCF<>.w_TIPOCF
            .w_TIPO = IIF(.w_TIPOCF='N','',.w_TIPOCF)
        endif
        .DoRTCalc(6,8,.t.)
            .w_CAUDOC = .w_docu.getVar('MVTIPDOC')
            .w_NUMDOC = .w_docu.getVar('MVNUMDOC')
            .w_NUMRIF = .w_docu.getVar('MVNUMEST')
            .w_ALFDOC = .w_docu.getVar('MVALFDOC')
            .w_ALFRIF = .w_docu.getVar('MVALFEST')
            .w_DATDOC = .w_docu.getVar('MVDATDOC')
            .w_DATRIF = .w_docu.getVar('MVDATEST')
        .DoRTCalc(16,17,.t.)
            .w_CODCLIFOR = .w_docu.getVar('MVCODCON')
            .w_TIPOCF = .w_docu.getVar('MVTIPCON')
            .w_MVSERIAL = .w_docu.getVar('MVSERIAL')
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
        .oPgFrm.Page1.oPag.docu.Calculate()
        .DoRTCalc(21,21,.t.)
        if .o_MVRIFCLI<>.w_MVRIFCLI
            .w_MVRIFCLI = .w_docu.getVar('MVRIFCLI')
        endif
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate()
            .w_NUMDO1 = .w_docu.getVar('MVNUMDOC')
            .w_ALFDO1 = .w_docu.getVar('MVALFDOC')
            .w_DATDO1 = .w_docu.getVar('MVDATDOC')
            .w_RIFTEST = this.oParentObject.w_NUMRIF
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(27,29,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
        .oPgFrm.Page1.oPag.docu.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_21.Event(cEvent)
      .oPgFrm.Page1.oPag.docu.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_25.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPOCF = this.w_TIPOCF
    this.o_MVRIFCLI = this.w_MVRIFCLI
    return

enddefine

* --- Define pages as container
define class tgsor_st1Pag1 as StdContainer
  Width  = 662
  height = 335
  stdWidth  = 662
  stdheight = 335
  resizeXpos=368
  resizeYpos=243
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_1_21 as cp_runprogram with uid="PLAQTHLMHF",left=42, top=334, width=125,height=25,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSOR_BTD("Z")',;
    cEvent = "w_docu selected",;
    nPag=1;
    , HelpContextID = 103980518


  add object docu as cp_zoombox with uid="TRPLKGPBRJ",left=0, top=3, width=655,height=305,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="DOC_MAST",cZoomFile="GSOR_ST1",bOptions=.t.,bAdvOptions=.t.,bQueryOnLoad=.f.,bReadOnly=.t.,;
    cEvent = "Ricerca",;
    nPag=1;
    , HelpContextID = 103980518


  add object oObj_1_25 as cp_runprogram with uid="CYSIIOVIDI",left=177, top=336, width=258,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSOR_BT1",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 103980518
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsor_st1','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
