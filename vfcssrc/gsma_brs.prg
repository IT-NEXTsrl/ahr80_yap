* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_brs                                                        *
*              RICOSTRUZIONE SALDI DI MAGAZZINO                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-07-17                                                      *
* Last revis.: 2018-09-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_brs",oParentObject)
return(i_retval)

define class tgsma_brs as StdBatch
  * --- Local variables
  w_MESS = space(100)
  w_PUNPAD = .NULL.
  w_TIPOARC = space(1)
  w_MSGLOTTI = 0
  w_APPO = space(10)
  w_FLACOSTO = space(1)
  w_FLAVEN = space(1)
  w_TOTRECORD = 0
  w_TOTARTI = 0
  w_TOTMAGA = 0
  w_AZI = space(5)
  w_DATAINIZ = ctod("  /  /  ")
  w_DATAINIZIO = ctod("  /  /  ")
  w_DATAFINE = ctod("  /  /  ")
  w_DOCCOMP = 0
  w_NUMDOC2 = 0
  w_CONNECT = 0
  w_DB = space(10)
  w_TABELLA = space(15)
  w_TABELLA2 = space(15)
  w_CAMPIINDICE = space(254)
  w_CMD = space(254)
  w_ASSEGNATA = space(0)
  w_OLDVALUE = .f.
  w_FLAGFILTRO = space(1)
  w_AVVISOLOTTI = .f.
  w_RETVAL = .f.
  w_CODMAG = space(20)
  w_ADDGG = 0
  w_NUMITER = 0
  w_GGINTERVINT = 0
  w_NEWINIDATE = ctod("  /  /  ")
  w_CRLF = space(2)
  w_DESART = space(40)
  w_ARTICOLO = space(20)
  w_USCITA = .f.
  w_CICLO = space(30)
  w_LOG = space(0)
  w_TIPOARCH = space(1)
  w_NELEMENT = 0
  w_COUNTER = 0
  w_ARCHIVIO = space(60)
  w_tmpINDEX = 0
  w_CCINDEX = 0
  * --- WorkFile variables
  MAGAZZIN_idx=0
  SALDIART_idx=0
  TMPSALDI_idx=0
  LOTTIART_idx=0
  DOC_DETT_idx=0
  MVM_DETT_idx=0
  TMPSALAGG_idx=0
  TMPSALDI1_idx=0
  ESERCIZI_idx=0
  DOC_MAST_idx=0
  LISTART_idx=0
  ART_ICOL_idx=0
  FILDTREG_idx=0
  TMP_ARTI_idx=0
  TMP_MAGA_idx=0
  runtime_filters = 2

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ricostruzione Saldi Articoli di Magazzino(da GSMA_KRS)
    this.w_TOTARTI = 0
    this.w_TOTMAGA = 0
    this.w_ASSEGNATA = .F.
    this.w_MSGLOTTI = 0
    this.w_AVVISOLOTTI = .F.
    this.w_AZI = alltrim(i_CODAZI)
    * --- Select from ESERCIZI
    i_nConn=i_TableProp[this.ESERCIZI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select MIN(ESINIESE) AS DATAINI  from "+i_cTable+" ESERCIZI ";
          +" where ESCODAZI = "+cp_ToStrODBC(this.w_AZI)+"";
           ,"_Curs_ESERCIZI")
    else
      select MIN(ESINIESE) AS DATAINI from (i_cTable);
       where ESCODAZI = this.w_AZI;
        into cursor _Curs_ESERCIZI
    endif
    if used('_Curs_ESERCIZI')
      select _Curs_ESERCIZI
      locate for 1=1
      do while not(eof())
      this.w_DATAINIZ = cp_ToDate(_Curs_ESERCIZI.DATAINI)
        select _Curs_ESERCIZI
        continue
      enddo
      use
    endif
    this.w_DATAINIZIO = iif(! empty(this.oParentObject.w_GGINTERV),i_datsys-this.oParentObject.w_GGINTERV,this.w_DATAINIZ)
    this.w_PUNPAD = this.oParentObject
    this.w_RETVAL = .F.
    if g_APPLICATION = "ADHOC REVOLUTION" OR UPPER(ALLTRIM(this.w_PUNPAD.Class))=="TGSMA_KRS"
      this.w_PUNPAD.w_MSG = ""
    endif
    CREATE CURSOR MessErr (MSG C(220))
    this.oParentObject.w_INITIME = time()
    this.oParentObject.w_ENDTIME = ""
    this.oParentObject.setcontrolsvalue()
    * --- Try
    local bErr_04651430
    bErr_04651430=bTrsErr
    this.Try_04651430()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      if g_APPLICATION = "ADHOC REVOLUTION" OR UPPER(ALLTRIM(this.w_PUNPAD.Class))=="TGSMA_KRS"
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
      endif
      ADDMSGNL(ah_msgformat("Impossibile aggiornare i saldi, errore: %1.%0Operazione annullata",message()),this)
      this.w_RETVAL = .T.
    endif
    bTrsErr=bTrsErr or bErr_04651430
    * --- End
    * --- -- Controllo se ci sono magazzini\articoli movimentati nello storico non presenti in anagrafica
    this.w_TOTARTI = 0
    this.w_TOTMAGA = 0
    this.w_ARTICOLO = ""
    this.w_CODMAG = ""
    this.w_LOG = ""
    * --- Create temporary table TMP_ARTI
    i_nIdx=cp_AddTableDef('TMP_ARTI') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('CHECKART',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMP_ARTI_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Select from TMP_ARTI
    i_nConn=i_TableProp[this.TMP_ARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_ARTI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select SLCODICE  from "+i_cTable+" TMP_ARTI ";
          +" group by SLCODICE";
           ,"_Curs_TMP_ARTI")
    else
      select SLCODICE from (i_cTable);
       group by SLCODICE;
        into cursor _Curs_TMP_ARTI
    endif
    if used('_Curs_TMP_ARTI')
      select _Curs_TMP_ARTI
      locate for 1=1
      do while not(eof())
      this.w_TOTARTI = this.w_TOTARTI + 1
        select _Curs_TMP_ARTI
        continue
      enddo
      use
    endif
    * --- Create temporary table TMP_MAGA
    i_nIdx=cp_AddTableDef('TMP_MAGA') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('CHECKMAG',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMP_MAGA_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Select from TMP_MAGA
    i_nConn=i_TableProp[this.TMP_MAGA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_MAGA_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select SLCODMAG  from "+i_cTable+" TMP_MAGA ";
          +" group by SLCODMAG";
           ,"_Curs_TMP_MAGA")
    else
      select SLCODMAG from (i_cTable);
       group by SLCODMAG;
        into cursor _Curs_TMP_MAGA
    endif
    if used('_Curs_TMP_MAGA')
      select _Curs_TMP_MAGA
      locate for 1=1
      do while not(eof())
      this.w_TOTMAGA = this.w_TOTMAGA + 1
        select _Curs_TMP_MAGA
        continue
      enddo
      use
    endif
    do case
      case this.w_TOTARTI > 0 and this.w_TOTMAGA > 0
        if ah_YesNo("Si vuole visualizzare la lista degli articoli\magazzini non pi� presenti in anagrafica?")
          this.w_CRLF = chr(13)+chr(10)
          this.w_LOG = "Lista Articoli: " + this.w_CRLF
          AH_MSG("Attendere, creazione della lista in corso ...")
          * --- Select from TMP_ARTI
          i_nConn=i_TableProp[this.TMP_ARTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMP_ARTI_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select SLCODICE  from "+i_cTable+" TMP_ARTI ";
                 ,"_Curs_TMP_ARTI")
          else
            select SLCODICE from (i_cTable);
              into cursor _Curs_TMP_ARTI
          endif
          if used('_Curs_TMP_ARTI')
            select _Curs_TMP_ARTI
            locate for 1=1
            do while not(eof())
            this.w_ARTICOLO = left(alltrim(_Curs_TMP_ARTI.SLCODICE)+repl(" ",20),20)
            this.w_LOG = this.w_LOG + this.w_ARTICOLO + this.w_CRLF
              select _Curs_TMP_ARTI
              continue
            enddo
            use
          endif
          this.w_LOG = this.w_LOG + this.w_CRLF + "Lista Magazzini: " + this.w_CRLF
          * --- Select from TMP_MAGA
          i_nConn=i_TableProp[this.TMP_MAGA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMP_MAGA_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select SLCODMAG  from "+i_cTable+" TMP_MAGA ";
                 ,"_Curs_TMP_MAGA")
          else
            select SLCODMAG from (i_cTable);
              into cursor _Curs_TMP_MAGA
          endif
          if used('_Curs_TMP_MAGA')
            select _Curs_TMP_MAGA
            locate for 1=1
            do while not(eof())
            this.w_CODMAG = left(alltrim(_Curs_TMP_MAGA.SLCODMAG)+repl(" ",20),20)
            this.w_LOG = this.w_LOG + this.w_CODMAG + this.w_CRLF
              select _Curs_TMP_MAGA
              continue
            enddo
            use
          endif
          mask=GSMA_KCU(this)
        endif
      case this.w_TOTARTI > 0
        if ah_YesNo("Si vuole visualizzare la lista degli articoli non pi� presenti in anagrafica?")
          this.w_CRLF = chr(13)+chr(10)
          AH_MSG("Attendere, creazione della lista in corso ...")
          * --- Select from TMP_ARTI
          i_nConn=i_TableProp[this.TMP_ARTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMP_ARTI_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select SLCODICE  from "+i_cTable+" TMP_ARTI ";
                 ,"_Curs_TMP_ARTI")
          else
            select SLCODICE from (i_cTable);
              into cursor _Curs_TMP_ARTI
          endif
          if used('_Curs_TMP_ARTI')
            select _Curs_TMP_ARTI
            locate for 1=1
            do while not(eof())
            this.w_ARTICOLO = left(alltrim(_Curs_TMP_ARTI.SLCODICE)+repl(" ",20),20)
            this.w_LOG = this.w_LOG + this.w_ARTICOLO + this.w_CRLF
              select _Curs_TMP_ARTI
              continue
            enddo
            use
          endif
          mask=GSMA_KCU(this)
        endif
      case this.w_TOTMAGA > 0
        if ah_YesNo("Si vuole visualizzare la lista dei magazzini non pi� presenti in anagrafica?")
          this.w_CRLF = chr(13)+chr(10)
          AH_MSG("Attendere, creazione della lista in corso ...")
          * --- Select from TMP_MAGA
          i_nConn=i_TableProp[this.TMP_MAGA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMP_MAGA_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select SLCODMAG  from "+i_cTable+" TMP_MAGA ";
                 ,"_Curs_TMP_MAGA")
          else
            select SLCODMAG from (i_cTable);
              into cursor _Curs_TMP_MAGA
          endif
          if used('_Curs_TMP_MAGA')
            select _Curs_TMP_MAGA
            locate for 1=1
            do while not(eof())
            this.w_CODMAG = left(alltrim(_Curs_TMP_MAGA.SLCODMAG)+repl(" ",20),20)
            this.w_LOG = this.w_LOG + this.w_CODMAG + this.w_CRLF
              select _Curs_TMP_MAGA
              continue
            enddo
            use
          endif
          mask=GSMA_KCU(this)
        endif
    endcase
    * --- LOG Errori
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    i_retcode = 'stop'
    i_retval = this.w_RETVAL
    return
  endproc
  proc Try_04651430()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- --definizion array
    DIMENSION l_ARRAYINDEX[1,2]
    this.Page_4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Aggiorna i flag dei di movimenti di magazzino e documenti
    if this.oParentObject.w_FLCAU="S"
      if AH_YESNO("Attenzione, la verifica del flag di aggiornamento saldi pu� richiedere molto tempo in relazione alla dimensione degli archivi. Confermi ugualmente?")
        ADDMSGNL("Verifica Flag in Corso ; Attendere...  ",this)
        * --- Il controllo seguente serve per evitare errori in fase di esecuzione della vqr in DB2
        g_DATMAT = iif(empty(nvl(g_DATMAT,ctod("  -  -    "))),i_INIDAT,g_DATMAT)
        * --- Create temporary table TMPSALAGG
        i_nIdx=cp_AddTableDef('TMPSALAGG') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('gsma1brk',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPSALAGG_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- Select from query\GSMA4BRK
        do vq_exec with 'query\GSMA4BRK',this,'_Curs_query_GSMA4BRK','',.f.,.t.
        if used('_Curs_query_GSMA4BRK')
          select _Curs_query_GSMA4BRK
          locate for 1=1
          do while not(eof())
          this.w_MSGLOTTI = this.w_MSGLOTTI + 1
          this.w_APPO = IIF (_Curs_query_GSMA4BRK.TIPOARC="M","Movimento ","Documento. ")+ " n. " + Alltrim(Str(_Curs_query_GSMA4BRK.NUMDOC)) + IIF(Not Empty(_Curs_query_GSMA4BRK.ALFDOC), "\","") + Alltrim(_Curs_query_GSMA4BRK.ALFDOC) + " del" + DTOC(NVL (_Curs_query_GSMA4BRK.DATDOC, ctod("  /  /  "))) +" causale : " + Alltrim(_Curs_query_GSMA4BRK.TIPDOC) + "  -  " + Alltrim(_Curs_query_GSMA4BRK.CMDESCRI) 
          if g_APPLICATION = "ADHOC REVOLUTION"
            this.w_APPO = this.w_APPO + Chr(13) + "La riga:" + Alltrim(Str(_Curs_query_GSMA4BRK.CPROWORD)) + " movimenta articoli gestiti a " + IIF(_Curs_query_GSMA4BRK.GESMAT="S"," matricole" , " lotti/ubicazione")
            this.w_APPO = this.w_APPO + Chr(13) + "Impossibile Aggiornare i dati di Questo "+IIF (_Curs_query_GSMA4BRK.TIPOARC="M","movimento ","documento ")
          else
            this.w_APPO = this.w_APPO + Chr(13) + "Riga nr." + Alltrim(Str(_Curs_query_GSMA4BRK.CPROWORD)) + "    impossibile aggiornare"
          endif
          if g_APPLICATION = "ADHOC REVOLUTION"
            if _Curs_query_GSMA4BRK.GESMAT="S"
              INSERT INTO MessErr (MSG) VALUES (this.w_APPO)
            endif
            if _Curs_query_GSMA4BRK.GESLOTTI="S"
              this.w_AVVISOLOTTI = .T.
            endif
          else
            INSERT INTO MessErr (MSG) VALUES (this.w_APPO)
          endif
          this.w_APPO = ""
            select _Curs_query_GSMA4BRK
            continue
          enddo
          use
        endif
        if RECCOUNT("MessErr") > 0
          if g_APPLICATION = "ADHOC REVOLUTION"
            this.w_MESS = AH_MSGFORMAT ("Ci sono movimenti/documenti le cui righe sono gestite a matricole, tali movimenti/documenti non verranno aggiornati.%0Si vuole eseguire la stampa di controllo di tali documenti?")
          else
            this.w_MESS = AH_MSGFORMAT ("Esistono movimenti/documenti che movimentano articoli gestiti a lotti/matricole oppure magazzini gestiti a ubicazione, tali movimenti/documenti non verranno aggiornati. Si desidera eseguire la stampa di controllo di tali movimenti/documenti?")
          endif
          if CP_YESNO(this.w_MESS)
            SELECT * FROM MessErr INTO CURSOR __TMP__
            l_STATO="AVVISO"
            CP_CHPRN("QUERY\GSVE1BCV.FRX")
          endif
          if !AH_YESNO("Si vuole procedere con l'elaborazione?")
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Drop temporary table TMPSALAGG
            i_nIdx=cp_GetTableDefIdx('TMPSALAGG')
            if i_nIdx<>0
              cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
              cp_RemoveTableDef('TMPSALAGG')
            endif
            i_retcode = 'stop'
            return
          endif
        endif
        * --- Se vengono aggiornati documenti che movimentano lotti allora si avvisa l utente di provverdere alla riscostruzione saldi lotti
        if this.w_AVVISOLOTTI AND this.oParentObject.w_AGGLOTUBI<>"S" AND g_APPLICATION = "ADHOC REVOLUTION"
          AH_ERRORMSG("Ci sono movimenti/documenti le cui righe sono gestite a lotti o ubicazioni, � consigliato rilanciare anche la ricostruzione saldi lotti / ubicazioni.")
        endif
        * --- begin transaction
        cp_BeginTrs()
        do case
          case g_APPLICATION = "ADHOC REVOLUTION"
            * --- Aggiorna documenti
            this.w_TIPOARC = "D"
            * --- Write into DOC_DETT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.DOC_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
              declare i_aIndex[1]
              i_cQueryTable=cp_getTempTableName(i_nConn)
              i_aIndex(1)="MVSERIAL,CPROWNUM,MVNUMRIF"
              do vq_exec with 'gsma2brk',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)	
              i_cDB=cp_GetDatabaseType(i_nConn)
              do case
              case i_cDB="SQLServer"
                i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                      +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"MVKEYSAL = _t2.NEWKEYSAL";
                  +",MVFLCASC = _t2.CMCASC";
                  +",MVFLORDI = _t2.CMORDI";
                  +",MVFLIMPE = _t2.CMIMPE";
                  +",MVFLRISE = _t2.CMRISE";
                  +",MVF2CASC = _t2.CMFLCASC";
                  +",MVF2ORDI = _t2.CMFLORDI";
                  +",MVF2IMPE = _t2.CMFLIMPE";
                  +",MVF2RISE = _t2.CMFLRISE";
                  +",MVFLELGM = _t2.FLELGM";
                  +",MVFLLOTT = _t2.NEWMVFLLOTT";
                  +",MVF2LOTT = _t2.NEWMVF2LOTT";
                  +",MVQTASAL = _t2.QTADAE";
                  +",MV_FLAGG = _t2.VARVAL";
                  +i_ccchkf;
                  +" from "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
              case i_cDB="MySQL"
                i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                      +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 set ";
                  +"DOC_DETT.MVKEYSAL = _t2.NEWKEYSAL";
                  +",DOC_DETT.MVFLCASC = _t2.CMCASC";
                  +",DOC_DETT.MVFLORDI = _t2.CMORDI";
                  +",DOC_DETT.MVFLIMPE = _t2.CMIMPE";
                  +",DOC_DETT.MVFLRISE = _t2.CMRISE";
                  +",DOC_DETT.MVF2CASC = _t2.CMFLCASC";
                  +",DOC_DETT.MVF2ORDI = _t2.CMFLORDI";
                  +",DOC_DETT.MVF2IMPE = _t2.CMFLIMPE";
                  +",DOC_DETT.MVF2RISE = _t2.CMFLRISE";
                  +",DOC_DETT.MVFLELGM = _t2.FLELGM";
                  +",DOC_DETT.MVFLLOTT = _t2.NEWMVFLLOTT";
                  +",DOC_DETT.MVF2LOTT = _t2.NEWMVF2LOTT";
                  +",DOC_DETT.MVQTASAL = _t2.QTADAE";
                  +",DOC_DETT.MV_FLAGG = _t2.VARVAL";
                  +Iif(Empty(i_ccchkf),"",",DOC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                  +" where "+i_cWhere)
              case i_cDB="Oracle"
                i_cWhere="DOC_DETT.MVSERIAL = t2.MVSERIAL";
                      +" and "+"DOC_DETT.CPROWNUM = t2.CPROWNUM";
                      +" and "+"DOC_DETT.MVNUMRIF = t2.MVNUMRIF";
                
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set (";
                  +"MVKEYSAL,";
                  +"MVFLCASC,";
                  +"MVFLORDI,";
                  +"MVFLIMPE,";
                  +"MVFLRISE,";
                  +"MVF2CASC,";
                  +"MVF2ORDI,";
                  +"MVF2IMPE,";
                  +"MVF2RISE,";
                  +"MVFLELGM,";
                  +"MVFLLOTT,";
                  +"MVF2LOTT,";
                  +"MVQTASAL,";
                  +"MV_FLAGG";
                  +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                  +"t2.NEWKEYSAL,";
                  +"t2.CMCASC,";
                  +"t2.CMORDI,";
                  +"t2.CMIMPE,";
                  +"t2.CMRISE,";
                  +"t2.CMFLCASC,";
                  +"t2.CMFLORDI,";
                  +"t2.CMFLIMPE,";
                  +"t2.CMFLRISE,";
                  +"t2.FLELGM,";
                  +"t2.NEWMVFLLOTT,";
                  +"t2.NEWMVF2LOTT,";
                  +"t2.QTADAE,";
                  +"t2.VARVAL";
                  +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                  +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
              case i_cDB="PostgreSQL"
                i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                      +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set ";
                  +"MVKEYSAL = _t2.NEWKEYSAL";
                  +",MVFLCASC = _t2.CMCASC";
                  +",MVFLORDI = _t2.CMORDI";
                  +",MVFLIMPE = _t2.CMIMPE";
                  +",MVFLRISE = _t2.CMRISE";
                  +",MVF2CASC = _t2.CMFLCASC";
                  +",MVF2ORDI = _t2.CMFLORDI";
                  +",MVF2IMPE = _t2.CMFLIMPE";
                  +",MVF2RISE = _t2.CMFLRISE";
                  +",MVFLELGM = _t2.FLELGM";
                  +",MVFLLOTT = _t2.NEWMVFLLOTT";
                  +",MVF2LOTT = _t2.NEWMVF2LOTT";
                  +",MVQTASAL = _t2.QTADAE";
                  +",MV_FLAGG = _t2.VARVAL";
                  +i_ccchkf;
                  +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
              otherwise
                i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
                      +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                      +" and "+i_cTable+".MVNUMRIF = "+i_cQueryTable+".MVNUMRIF";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"MVKEYSAL = (select NEWKEYSAL from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVFLCASC = (select CMCASC from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVFLORDI = (select CMORDI from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVFLIMPE = (select CMIMPE from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVFLRISE = (select CMRISE from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVF2CASC = (select CMFLCASC from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVF2ORDI = (select CMFLORDI from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVF2IMPE = (select CMFLIMPE from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVF2RISE = (select CMFLRISE from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVFLELGM = (select FLELGM from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVFLLOTT = (select NEWMVFLLOTT from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVF2LOTT = (select NEWMVF2LOTT from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVQTASAL = (select QTADAE from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MV_FLAGG = (select VARVAL from "+i_cQueryTable+" where "+i_cWhere+")";
                  +i_ccchkf;
                  +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
              endcase
              cp_DropTempTable(i_nConn,i_cQueryTable)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore aggiorna documenti'
              return
            endif
            this.w_TIPOARC = "M"
            * --- Aggiorna  movimenti di magazzino
            * --- Write into MVM_DETT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.MVM_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2])
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
              declare i_aIndex[1]
              i_cQueryTable=cp_getTempTableName(i_nConn)
              i_aIndex(1)="MMSERIAL,CPROWNUM,MMNUMRIF"
              do vq_exec with 'gsma5brk',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.MVM_DETT_idx,i_nConn)	
              i_cDB=cp_GetDatabaseType(i_nConn)
              do case
              case i_cDB="SQLServer"
                i_cWhere="MVM_DETT.MMSERIAL = _t2.MMSERIAL";
                      +" and "+"MVM_DETT.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"MVM_DETT.MMNUMRIF = _t2.MMNUMRIF";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"MMKEYSAL = _t2.NEWKEYSAL";
                  +",MMFLCASC = _t2.CMCASC";
                  +",MMFLORDI = _t2.CMORDI";
                  +",MMFLIMPE = _t2.CMIMPE";
                  +",MMFLRISE = _t2.CMRISE";
                  +",MMF2CASC = _t2.CMFLCASC";
                  +",MMF2ORDI = _t2.CMFLORDI";
                  +",MMF2IMPE = _t2.CMFLIMPE";
                  +",MMF2RISE = _t2.CMFLRISE";
                  +",MMFLELGM = _t2.FLELGM";
                  +",MMFLLOTT = _t2.NEWMVFLLOTT";
                  +",MMF2LOTT = _t2.NEWMVF2LOTT";
                  +i_ccchkf;
                  +" from "+i_cTable+" MVM_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
              case i_cDB="MySQL"
                i_cWhere="MVM_DETT.MMSERIAL = _t2.MMSERIAL";
                      +" and "+"MVM_DETT.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"MVM_DETT.MMNUMRIF = _t2.MMNUMRIF";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MVM_DETT, "+i_cQueryTable+" _t2 set ";
                  +"MVM_DETT.MMKEYSAL = _t2.NEWKEYSAL";
                  +",MVM_DETT.MMFLCASC = _t2.CMCASC";
                  +",MVM_DETT.MMFLORDI = _t2.CMORDI";
                  +",MVM_DETT.MMFLIMPE = _t2.CMIMPE";
                  +",MVM_DETT.MMFLRISE = _t2.CMRISE";
                  +",MVM_DETT.MMF2CASC = _t2.CMFLCASC";
                  +",MVM_DETT.MMF2ORDI = _t2.CMFLORDI";
                  +",MVM_DETT.MMF2IMPE = _t2.CMFLIMPE";
                  +",MVM_DETT.MMF2RISE = _t2.CMFLRISE";
                  +",MVM_DETT.MMFLELGM = _t2.FLELGM";
                  +",MVM_DETT.MMFLLOTT = _t2.NEWMVFLLOTT";
                  +",MVM_DETT.MMF2LOTT = _t2.NEWMVF2LOTT";
                  +Iif(Empty(i_ccchkf),"",",MVM_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                  +" where "+i_cWhere)
              case i_cDB="Oracle"
                i_cWhere="MVM_DETT.MMSERIAL = t2.MMSERIAL";
                      +" and "+"MVM_DETT.CPROWNUM = t2.CPROWNUM";
                      +" and "+"MVM_DETT.MMNUMRIF = t2.MMNUMRIF";
                
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MVM_DETT set (";
                  +"MMKEYSAL,";
                  +"MMFLCASC,";
                  +"MMFLORDI,";
                  +"MMFLIMPE,";
                  +"MMFLRISE,";
                  +"MMF2CASC,";
                  +"MMF2ORDI,";
                  +"MMF2IMPE,";
                  +"MMF2RISE,";
                  +"MMFLELGM,";
                  +"MMFLLOTT,";
                  +"MMF2LOTT";
                  +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                  +"t2.NEWKEYSAL,";
                  +"t2.CMCASC,";
                  +"t2.CMORDI,";
                  +"t2.CMIMPE,";
                  +"t2.CMRISE,";
                  +"t2.CMFLCASC,";
                  +"t2.CMFLORDI,";
                  +"t2.CMFLIMPE,";
                  +"t2.CMFLRISE,";
                  +"t2.FLELGM,";
                  +"t2.NEWMVFLLOTT,";
                  +"t2.NEWMVF2LOTT";
                  +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                  +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
              case i_cDB="PostgreSQL"
                i_cWhere="MVM_DETT.MMSERIAL = _t2.MMSERIAL";
                      +" and "+"MVM_DETT.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"MVM_DETT.MMNUMRIF = _t2.MMNUMRIF";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MVM_DETT set ";
                  +"MMKEYSAL = _t2.NEWKEYSAL";
                  +",MMFLCASC = _t2.CMCASC";
                  +",MMFLORDI = _t2.CMORDI";
                  +",MMFLIMPE = _t2.CMIMPE";
                  +",MMFLRISE = _t2.CMRISE";
                  +",MMF2CASC = _t2.CMFLCASC";
                  +",MMF2ORDI = _t2.CMFLORDI";
                  +",MMF2IMPE = _t2.CMFLIMPE";
                  +",MMF2RISE = _t2.CMFLRISE";
                  +",MMFLELGM = _t2.FLELGM";
                  +",MMFLLOTT = _t2.NEWMVFLLOTT";
                  +",MMF2LOTT = _t2.NEWMVF2LOTT";
                  +i_ccchkf;
                  +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
              otherwise
                i_cWhere=i_cTable+".MMSERIAL = "+i_cQueryTable+".MMSERIAL";
                      +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                      +" and "+i_cTable+".MMNUMRIF = "+i_cQueryTable+".MMNUMRIF";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"MMKEYSAL = (select NEWKEYSAL from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MMFLCASC = (select CMCASC from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MMFLORDI = (select CMORDI from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MMFLIMPE = (select CMIMPE from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MMFLRISE = (select CMRISE from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MMF2CASC = (select CMFLCASC from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MMF2ORDI = (select CMFLORDI from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MMF2IMPE = (select CMFLIMPE from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MMF2RISE = (select CMFLRISE from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MMFLELGM = (select FLELGM from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MMFLLOTT = (select NEWMVFLLOTT from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MMF2LOTT = (select NEWMVF2LOTT from "+i_cQueryTable+" where "+i_cWhere+")";
                  +i_ccchkf;
                  +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
              endcase
              cp_DropTempTable(i_nConn,i_cQueryTable)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore aggiorna movimenti di tesoreria'
              return
            endif
          case g_APPLICATION = "ad hoc ENTERPRISE"
            * --- Aggiorna documenti
            this.w_TIPOARC = "D"
            * --- Write into DOC_DETT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.DOC_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
              declare i_aIndex[1]
              i_cQueryTable=cp_getTempTableName(i_nConn)
              i_aIndex(1)="MVSERIAL,CPROWNUM,MVNUMRIF"
              do vq_exec with 'gsma2brk',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)	
              i_cDB=cp_GetDatabaseType(i_nConn)
              do case
              case i_cDB="SQLServer"
                i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                      +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"MVKEYSAL = _t2.NEWKEYSAL";
                  +",MVFLCASC = _t2.CMCASC";
                  +",MVFLORDI = _t2.CMORDI";
                  +",MVFLIMPE = _t2.CMIMPE";
                  +",MVFLRISE = _t2.CMRISE";
                  +",MVF2CASC = _t2.CMFLCASC";
                  +",MVF2ORDI = _t2.CMFLORDI";
                  +",MVF2IMPE = _t2.CMFLIMPE";
                  +",MVF2RISE = _t2.CMFLRISE";
                  +",MVFLELGM = _t2.FLELGM";
                  +",MVFLLOTT = _t2.NEWMVFLLOTT";
                  +",MVF2LOTT = _t2.NEWMVF2LOTT";
                  +",MVQTASAL = _t2.QTADAE";
                  +",MVLOTRIS = _t2.LOTRIS";
                  +",MVLO2RIS = _t2.LO2RIS";
                  +i_ccchkf;
                  +" from "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
              case i_cDB="MySQL"
                i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                      +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 set ";
                  +"DOC_DETT.MVKEYSAL = _t2.NEWKEYSAL";
                  +",DOC_DETT.MVFLCASC = _t2.CMCASC";
                  +",DOC_DETT.MVFLORDI = _t2.CMORDI";
                  +",DOC_DETT.MVFLIMPE = _t2.CMIMPE";
                  +",DOC_DETT.MVFLRISE = _t2.CMRISE";
                  +",DOC_DETT.MVF2CASC = _t2.CMFLCASC";
                  +",DOC_DETT.MVF2ORDI = _t2.CMFLORDI";
                  +",DOC_DETT.MVF2IMPE = _t2.CMFLIMPE";
                  +",DOC_DETT.MVF2RISE = _t2.CMFLRISE";
                  +",DOC_DETT.MVFLELGM = _t2.FLELGM";
                  +",DOC_DETT.MVFLLOTT = _t2.NEWMVFLLOTT";
                  +",DOC_DETT.MVF2LOTT = _t2.NEWMVF2LOTT";
                  +",DOC_DETT.MVQTASAL = _t2.QTADAE";
                  +",DOC_DETT.MVLOTRIS = _t2.LOTRIS";
                  +",DOC_DETT.MVLO2RIS = _t2.LO2RIS";
                  +Iif(Empty(i_ccchkf),"",",DOC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                  +" where "+i_cWhere)
              case i_cDB="Oracle"
                i_cWhere="DOC_DETT.MVSERIAL = t2.MVSERIAL";
                      +" and "+"DOC_DETT.CPROWNUM = t2.CPROWNUM";
                      +" and "+"DOC_DETT.MVNUMRIF = t2.MVNUMRIF";
                
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set (";
                  +"MVKEYSAL,";
                  +"MVFLCASC,";
                  +"MVFLORDI,";
                  +"MVFLIMPE,";
                  +"MVFLRISE,";
                  +"MVF2CASC,";
                  +"MVF2ORDI,";
                  +"MVF2IMPE,";
                  +"MVF2RISE,";
                  +"MVFLELGM,";
                  +"MVFLLOTT,";
                  +"MVF2LOTT,";
                  +"MVQTASAL,";
                  +"MVLOTRIS,";
                  +"MVLO2RIS";
                  +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                  +"t2.NEWKEYSAL,";
                  +"t2.CMCASC,";
                  +"t2.CMORDI,";
                  +"t2.CMIMPE,";
                  +"t2.CMRISE,";
                  +"t2.CMFLCASC,";
                  +"t2.CMFLORDI,";
                  +"t2.CMFLIMPE,";
                  +"t2.CMFLRISE,";
                  +"t2.FLELGM,";
                  +"t2.NEWMVFLLOTT,";
                  +"t2.NEWMVF2LOTT,";
                  +"t2.QTADAE,";
                  +"t2.LOTRIS,";
                  +"t2.LO2RIS";
                  +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                  +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
              case i_cDB="PostgreSQL"
                i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                      +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set ";
                  +"MVKEYSAL = _t2.NEWKEYSAL";
                  +",MVFLCASC = _t2.CMCASC";
                  +",MVFLORDI = _t2.CMORDI";
                  +",MVFLIMPE = _t2.CMIMPE";
                  +",MVFLRISE = _t2.CMRISE";
                  +",MVF2CASC = _t2.CMFLCASC";
                  +",MVF2ORDI = _t2.CMFLORDI";
                  +",MVF2IMPE = _t2.CMFLIMPE";
                  +",MVF2RISE = _t2.CMFLRISE";
                  +",MVFLELGM = _t2.FLELGM";
                  +",MVFLLOTT = _t2.NEWMVFLLOTT";
                  +",MVF2LOTT = _t2.NEWMVF2LOTT";
                  +",MVQTASAL = _t2.QTADAE";
                  +",MVLOTRIS = _t2.LOTRIS";
                  +",MVLO2RIS = _t2.LO2RIS";
                  +i_ccchkf;
                  +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
              otherwise
                i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
                      +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                      +" and "+i_cTable+".MVNUMRIF = "+i_cQueryTable+".MVNUMRIF";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"MVKEYSAL = (select NEWKEYSAL from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVFLCASC = (select CMCASC from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVFLORDI = (select CMORDI from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVFLIMPE = (select CMIMPE from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVFLRISE = (select CMRISE from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVF2CASC = (select CMFLCASC from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVF2ORDI = (select CMFLORDI from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVF2IMPE = (select CMFLIMPE from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVF2RISE = (select CMFLRISE from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVFLELGM = (select FLELGM from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVFLLOTT = (select NEWMVFLLOTT from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVF2LOTT = (select NEWMVF2LOTT from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVQTASAL = (select QTADAE from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVLOTRIS = (select LOTRIS from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVLO2RIS = (select LO2RIS from "+i_cQueryTable+" where "+i_cWhere+")";
                  +i_ccchkf;
                  +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
              endcase
              cp_DropTempTable(i_nConn,i_cQueryTable)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore aggiunta giacenze fuori linea'
              return
            endif
            this.w_TIPOARC = "M"
            * --- Aggiorna  movimenti di magazzino
            * --- Write into MVM_DETT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.MVM_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2])
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
              declare i_aIndex[1]
              i_cQueryTable=cp_getTempTableName(i_nConn)
              i_aIndex(1)="MMSERIAL,CPROWNUM,MMNUMRIF"
              do vq_exec with 'gsma5brk',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.MVM_DETT_idx,i_nConn)	
              i_cDB=cp_GetDatabaseType(i_nConn)
              do case
              case i_cDB="SQLServer"
                i_cWhere="MVM_DETT.MMSERIAL = _t2.MMSERIAL";
                      +" and "+"MVM_DETT.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"MVM_DETT.MMNUMRIF = _t2.MMNUMRIF";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"MMKEYSAL = _t2.NEWKEYSAL";
                  +",MMFLCASC = _t2.CMCASC";
                  +",MMFLORDI = _t2.CMORDI";
                  +",MMFLIMPE = _t2.CMIMPE";
                  +",MMFLRISE = _t2.CMRISE";
                  +",MMF2CASC = _t2.CMFLCASC";
                  +",MMF2ORDI = _t2.CMFLORDI";
                  +",MMF2IMPE = _t2.CMFLIMPE";
                  +",MMF2RISE = _t2.CMFLRISE";
                  +",MMFLELGM = _t2.FLELGM";
                  +",MMFLLOTT = _t2.NEWMVFLLOTT";
                  +",MMF2LOTT = _t2.NEWMVF2LOTT";
                  +",MMLOTRIS = _t2.LOTRIS";
                  +",MMLO2RIS = _t2.LO2RIS";
                  +i_ccchkf;
                  +" from "+i_cTable+" MVM_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
              case i_cDB="MySQL"
                i_cWhere="MVM_DETT.MMSERIAL = _t2.MMSERIAL";
                      +" and "+"MVM_DETT.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"MVM_DETT.MMNUMRIF = _t2.MMNUMRIF";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MVM_DETT, "+i_cQueryTable+" _t2 set ";
                  +"MVM_DETT.MMKEYSAL = _t2.NEWKEYSAL";
                  +",MVM_DETT.MMFLCASC = _t2.CMCASC";
                  +",MVM_DETT.MMFLORDI = _t2.CMORDI";
                  +",MVM_DETT.MMFLIMPE = _t2.CMIMPE";
                  +",MVM_DETT.MMFLRISE = _t2.CMRISE";
                  +",MVM_DETT.MMF2CASC = _t2.CMFLCASC";
                  +",MVM_DETT.MMF2ORDI = _t2.CMFLORDI";
                  +",MVM_DETT.MMF2IMPE = _t2.CMFLIMPE";
                  +",MVM_DETT.MMF2RISE = _t2.CMFLRISE";
                  +",MVM_DETT.MMFLELGM = _t2.FLELGM";
                  +",MVM_DETT.MMFLLOTT = _t2.NEWMVFLLOTT";
                  +",MVM_DETT.MMF2LOTT = _t2.NEWMVF2LOTT";
                  +",MVM_DETT.MMLOTRIS = _t2.LOTRIS";
                  +",MVM_DETT.MMLO2RIS = _t2.LO2RIS";
                  +Iif(Empty(i_ccchkf),"",",MVM_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                  +" where "+i_cWhere)
              case i_cDB="Oracle"
                i_cWhere="MVM_DETT.MMSERIAL = t2.MMSERIAL";
                      +" and "+"MVM_DETT.CPROWNUM = t2.CPROWNUM";
                      +" and "+"MVM_DETT.MMNUMRIF = t2.MMNUMRIF";
                
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MVM_DETT set (";
                  +"MMKEYSAL,";
                  +"MMFLCASC,";
                  +"MMFLORDI,";
                  +"MMFLIMPE,";
                  +"MMFLRISE,";
                  +"MMF2CASC,";
                  +"MMF2ORDI,";
                  +"MMF2IMPE,";
                  +"MMF2RISE,";
                  +"MMFLELGM,";
                  +"MMFLLOTT,";
                  +"MMF2LOTT,";
                  +"MMLOTRIS,";
                  +"MMLO2RIS";
                  +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                  +"t2.NEWKEYSAL,";
                  +"t2.CMCASC,";
                  +"t2.CMORDI,";
                  +"t2.CMIMPE,";
                  +"t2.CMRISE,";
                  +"t2.CMFLCASC,";
                  +"t2.CMFLORDI,";
                  +"t2.CMFLIMPE,";
                  +"t2.CMFLRISE,";
                  +"t2.FLELGM,";
                  +"t2.NEWMVFLLOTT,";
                  +"t2.NEWMVF2LOTT,";
                  +"t2.LOTRIS,";
                  +"t2.LO2RIS";
                  +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                  +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
              case i_cDB="PostgreSQL"
                i_cWhere="MVM_DETT.MMSERIAL = _t2.MMSERIAL";
                      +" and "+"MVM_DETT.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"MVM_DETT.MMNUMRIF = _t2.MMNUMRIF";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MVM_DETT set ";
                  +"MMKEYSAL = _t2.NEWKEYSAL";
                  +",MMFLCASC = _t2.CMCASC";
                  +",MMFLORDI = _t2.CMORDI";
                  +",MMFLIMPE = _t2.CMIMPE";
                  +",MMFLRISE = _t2.CMRISE";
                  +",MMF2CASC = _t2.CMFLCASC";
                  +",MMF2ORDI = _t2.CMFLORDI";
                  +",MMF2IMPE = _t2.CMFLIMPE";
                  +",MMF2RISE = _t2.CMFLRISE";
                  +",MMFLELGM = _t2.FLELGM";
                  +",MMFLLOTT = _t2.NEWMVFLLOTT";
                  +",MMF2LOTT = _t2.NEWMVF2LOTT";
                  +",MMLOTRIS = _t2.LOTRIS";
                  +",MMLO2RIS = _t2.LO2RIS";
                  +i_ccchkf;
                  +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
              otherwise
                i_cWhere=i_cTable+".MMSERIAL = "+i_cQueryTable+".MMSERIAL";
                      +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                      +" and "+i_cTable+".MMNUMRIF = "+i_cQueryTable+".MMNUMRIF";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"MMKEYSAL = (select NEWKEYSAL from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MMFLCASC = (select CMCASC from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MMFLORDI = (select CMORDI from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MMFLIMPE = (select CMIMPE from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MMFLRISE = (select CMRISE from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MMF2CASC = (select CMFLCASC from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MMF2ORDI = (select CMFLORDI from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MMF2IMPE = (select CMFLIMPE from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MMF2RISE = (select CMFLRISE from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MMFLELGM = (select FLELGM from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MMFLLOTT = (select NEWMVFLLOTT from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MMF2LOTT = (select NEWMVF2LOTT from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MMLOTRIS = (select LOTRIS from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MMLO2RIS = (select LO2RIS from "+i_cQueryTable+" where "+i_cWhere+")";
                  +i_ccchkf;
                  +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
              endcase
              cp_DropTempTable(i_nConn,i_cQueryTable)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore aggiunta giacenze fuori linea'
              return
            endif
        endcase
      else
        this.Page_6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        i_retcode = 'stop'
        return
      endif
      * --- Drop temporary table TMPSALAGG
      i_nIdx=cp_GetTableDefIdx('TMPSALAGG')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPSALAGG')
      endif
    else
      if g_APPLICATION = "ADHOC REVOLUTION" OR UPPER(ALLTRIM(this.w_PUNPAD.Class))=="TGSMA_KRS"
        * --- begin transaction
        cp_BeginTrs()
      endif
    endif
    * --- Azzera dati del Periodo
    ADDMSGNL("Fase azzeramento saldi magazzino  ",this)
    if this.oParentObject.w_FLDELSLD="S"
      * --- Delete from SALDIART
      i_nConn=i_TableProp[this.SALDIART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_cWhere=i_cTable+".SLCODICE = "+i_cQueryTable+".SLCODICE";
      
        do vq_exec with 'GSMADSLD',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    else
      * --- Write into SALDIART
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDIART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="SLCODICE"
        do vq_exec with 'GSMADSLD',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="SALDIART.SLCODICE = _t2.SLCODICE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SLQTAPER ="+cp_NullLink(cp_ToStrODBC(0),'SALDIART','SLQTAPER');
        +",SLQTRPER ="+cp_NullLink(cp_ToStrODBC(0),'SALDIART','SLQTRPER');
        +",SLQTOPER ="+cp_NullLink(cp_ToStrODBC(0),'SALDIART','SLQTOPER');
        +",SLQTIPER ="+cp_NullLink(cp_ToStrODBC(0),'SALDIART','SLQTIPER');
            +i_ccchkf;
            +" from "+i_cTable+" SALDIART, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="SALDIART.SLCODICE = _t2.SLCODICE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDIART, "+i_cQueryTable+" _t2 set ";
        +"SALDIART.SLQTAPER ="+cp_NullLink(cp_ToStrODBC(0),'SALDIART','SLQTAPER');
        +",SALDIART.SLQTRPER ="+cp_NullLink(cp_ToStrODBC(0),'SALDIART','SLQTRPER');
        +",SALDIART.SLQTOPER ="+cp_NullLink(cp_ToStrODBC(0),'SALDIART','SLQTOPER');
        +",SALDIART.SLQTIPER ="+cp_NullLink(cp_ToStrODBC(0),'SALDIART','SLQTIPER');
            +Iif(Empty(i_ccchkf),"",",SALDIART.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="SALDIART.SLCODICE = t2.SLCODICE";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDIART set (";
            +"SLQTAPER,";
            +"SLQTRPER,";
            +"SLQTOPER,";
            +"SLQTIPER";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +cp_NullLink(cp_ToStrODBC(0),'SALDIART','SLQTAPER')+",";
            +cp_NullLink(cp_ToStrODBC(0),'SALDIART','SLQTRPER')+",";
            +cp_NullLink(cp_ToStrODBC(0),'SALDIART','SLQTOPER')+",";
            +cp_NullLink(cp_ToStrODBC(0),'SALDIART','SLQTIPER')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="SALDIART.SLCODICE = _t2.SLCODICE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDIART set ";
        +"SLQTAPER ="+cp_NullLink(cp_ToStrODBC(0),'SALDIART','SLQTAPER');
        +",SLQTRPER ="+cp_NullLink(cp_ToStrODBC(0),'SALDIART','SLQTRPER');
        +",SLQTOPER ="+cp_NullLink(cp_ToStrODBC(0),'SALDIART','SLQTOPER');
        +",SLQTIPER ="+cp_NullLink(cp_ToStrODBC(0),'SALDIART','SLQTIPER');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".SLCODICE = "+i_cQueryTable+".SLCODICE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SLQTAPER ="+cp_NullLink(cp_ToStrODBC(0),'SALDIART','SLQTAPER');
        +",SLQTRPER ="+cp_NullLink(cp_ToStrODBC(0),'SALDIART','SLQTRPER');
        +",SLQTOPER ="+cp_NullLink(cp_ToStrODBC(0),'SALDIART','SLQTOPER');
        +",SLQTIPER ="+cp_NullLink(cp_ToStrODBC(0),'SALDIART','SLQTIPER');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Estrazione Dati
    ADDMSGNL("Fase estrazione dati",this)
    ADDMSGNL("Creazione tabella TMPSALAGG ",this)
    * --- Crea la tabella temporane contenente il raggruppamento dei documenti e mov. magazzino (per velocizzare l esecuzione della query che effettua il calcolo dei saldi da aggiornare)
    * --- Create temporary table TMPSALAGG
    i_nIdx=cp_AddTableDef('TMPSALAGG') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('GSMA2QSM',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPSALAGG_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    do case
      case g_APPLICATION = "ADHOC REVOLUTION"
        if g_PROD="S"
          * --- Dati Produzione  ODL + Movimenti di Magazzino + Documenti
          if g_GPOS="S"
            * --- Create temporary table TMPSALDI
            i_nIdx=cp_AddTableDef('TMPSALDI') && aggiunge la definizione nella lista delle tabelle
            i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
            vq_exec('GSDB3QRS',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
            this.TMPSALDI_idx=i_nIdx
            i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          else
            * --- Create temporary table TMPSALDI
            i_nIdx=cp_AddTableDef('TMPSALDI') && aggiunge la definizione nella lista delle tabelle
            i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
            vq_exec('GSDB_QRS',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
            this.TMPSALDI_idx=i_nIdx
            i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          endif
        else
          * --- Movimenti di Magazzino + Documenti
          if g_GPOS="S"
            * --- Create temporary table TMPSALDI
            i_nIdx=cp_AddTableDef('TMPSALDI') && aggiunge la definizione nella lista delle tabelle
            i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
            vq_exec('GSMA2QRS',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
            this.TMPSALDI_idx=i_nIdx
            i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          else
            * --- Create temporary table TMPSALDI
            i_nIdx=cp_AddTableDef('TMPSALDI') && aggiunge la definizione nella lista delle tabelle
            i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
            vq_exec('GSMA_QQS',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
            this.TMPSALDI_idx=i_nIdx
            i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          endif
        endif
      case g_APPLICATION = "ad hoc ENTERPRISE"
        if g_DIBA="S" 
          * --- Dati Produzione  ODL + Movimenti di Magazzino + Documenti
          * --- Create temporary table TMPSALDI
          i_nIdx=cp_AddTableDef('TMPSALDI') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('GSDB_QRS',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.TMPSALDI_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        else
          * --- Movimenti di Magazzino + Documenti
          * --- Create temporary table TMPSALDI
          i_nIdx=cp_AddTableDef('TMPSALDI') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('GSMA_QQS',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.TMPSALDI_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        endif
    endcase
    * --- Drop temporary table TMPSALAGG
    i_nIdx=cp_GetTableDefIdx('TMPSALAGG')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPSALAGG')
    endif
    ADDMSGNL("Fase scrittura saldi magazzino",this)
    * --- Insert Dati Non Presenti in SALDIART
    * --- Insert into SALDIART
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSMAIQRS",this.SALDIART_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Aggiorna Dati Esistenti in SALDIART
    * --- Write into SALDIART
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPSALDI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="SALDIART.SLCODICE = _t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = _t2.SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLQTAPER = _t2.TOTCASC";
          +",SLQTRPER = _t2.TOTRISE";
          +",SLQTOPER = _t2.TOTORDI";
          +",SLQTIPER = _t2.TOTIMPE";
          +i_ccchkf;
          +" from "+i_cTable+" SALDIART, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="SALDIART.SLCODICE = _t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = _t2.SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDIART, "+i_cQueryTable+" _t2 set ";
          +"SALDIART.SLQTAPER = _t2.TOTCASC";
          +",SALDIART.SLQTRPER = _t2.TOTRISE";
          +",SALDIART.SLQTOPER = _t2.TOTORDI";
          +",SALDIART.SLQTIPER = _t2.TOTIMPE";
          +Iif(Empty(i_ccchkf),"",",SALDIART.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="PostgreSQL"
        i_cWhere="SALDIART.SLCODICE = _t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = _t2.SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDIART set ";
          +"SLQTAPER = _t2.TOTCASC";
          +",SLQTRPER = _t2.TOTRISE";
          +",SLQTOPER = _t2.TOTORDI";
          +",SLQTIPER = _t2.TOTIMPE";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".SLCODICE = "+i_cQueryTable+".SLCODICE";
              +" and "+i_cTable+".SLCODMAG = "+i_cQueryTable+".SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLQTAPER = (select TOTCASC from "+i_cQueryTable+" where "+i_cWhere+")";
          +",SLQTRPER = (select TOTRISE from "+i_cQueryTable+" where "+i_cWhere+")";
          +",SLQTOPER = (select TOTORDI from "+i_cQueryTable+" where "+i_cWhere+")";
          +",SLQTIPER = (select TOTIMPE from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Aggiungo le giacenze fuori linea recuperando valori da Saldisart
    * --- Insert into SALDIART
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSMA7BRS",this.SALDIART_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Write into SALDIART
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="SLCODICE,SLCODMAG"
      do vq_exec with 'GSMA_QRF',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="SALDIART.SLCODICE = _t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = _t2.SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLQTAPER = SALDIART.SLQTAPER+_t2.SLQTAPRO";
          +",SLQTRPER = SALDIART.SLQTRPER+_t2.SLQTRPRO";
          +",SLQTAPRO = _t2.SLQTAPRO";
          +",SLQTRPRO = _t2.SLQTRPRO";
          +i_ccchkf;
          +" from "+i_cTable+" SALDIART, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="SALDIART.SLCODICE = _t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = _t2.SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDIART, "+i_cQueryTable+" _t2 set ";
          +"SALDIART.SLQTAPER = SALDIART.SLQTAPER+_t2.SLQTAPRO";
          +",SALDIART.SLQTRPER = SALDIART.SLQTRPER+_t2.SLQTRPRO";
          +",SALDIART.SLQTAPRO = _t2.SLQTAPRO";
          +",SALDIART.SLQTRPRO = _t2.SLQTRPRO";
          +Iif(Empty(i_ccchkf),"",",SALDIART.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="SALDIART.SLCODICE = t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = t2.SLCODMAG";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDIART set (";
          +"SLQTAPER,";
          +"SLQTRPER,";
          +"SLQTAPRO,";
          +"SLQTRPRO";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"SLQTAPER+t2.SLQTAPRO,";
          +"SLQTRPER+t2.SLQTRPRO,";
          +"t2.SLQTAPRO,";
          +"t2.SLQTRPRO";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="SALDIART.SLCODICE = _t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = _t2.SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDIART set ";
          +"SLQTAPER = SALDIART.SLQTAPER+_t2.SLQTAPRO";
          +",SLQTRPER = SALDIART.SLQTRPER+_t2.SLQTRPRO";
          +",SLQTAPRO = _t2.SLQTAPRO";
          +",SLQTRPRO = _t2.SLQTRPRO";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".SLCODICE = "+i_cQueryTable+".SLCODICE";
              +" and "+i_cTable+".SLCODMAG = "+i_cQueryTable+".SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLQTAPER = (select "+i_cTable+".SLQTAPER+SLQTAPRO from "+i_cQueryTable+" where "+i_cWhere+")";
          +",SLQTRPER = (select "+i_cTable+".SLQTRPER+SLQTRPRO from "+i_cQueryTable+" where "+i_cWhere+")";
          +",SLQTAPRO = (select SLQTAPRO from "+i_cQueryTable+" where "+i_cWhere+")";
          +",SLQTRPRO = (select SLQTRPRO from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errore aggiunta giacenze fuori linea'
      return
    endif
    if g_APPLICATION ="ad hoc ENTERPRISE"
      * --- Pulizia valori NULL su Varianti
      * --- Write into SALDIART
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDIART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SLCODVAR ="+cp_NullLink(cp_ToStrODBC(space(20)),'SALDIART','SLCODVAR');
            +i_ccchkf ;
        +" where ";
            +"SLCODVAR is null ";
               )
      else
        update (i_cTable) set;
            SLCODVAR = space(20);
            &i_ccchkf. ;
         where;
            SLCODVAR is null;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error='Errore aggiornamento valori NULL'
        return
      endif
    endif
    if this.oParentObject.w_AGGCOMME ="S"
      ADDMSGNL("Fase scrittura saldi commessa",this)
      do GSMA_BRT with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.oParentObject.w_AGGLOTUBI ="S" 
      if g_APPLICATION = "ADHOC REVOLUTION" 
        GSMD_BRL (this, , , , , .T. , this, this.oParentObject.w_FLDELSLD, this.oParentObject.w_AGCLOTUBI, this.oParentObject.w_CODARTIN, this.oParentObject.w_CODARTFI)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        do GSMA_BS1 with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    * --- Drop temporary table TMPSALDI
    i_nIdx=cp_GetTableDefIdx('TMPSALDI')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPSALDI')
    endif
    * --- AGGIORNA ULTIMO PREZZO/COSTO
    if g_APPLICATION = "ADHOC REVOLUTION" AND this.oParentObject.w_FLCP="S"
      * --- Creo tabella temporanmea contenente lista articolo non obsoleti presenti nei saldi
      * --- Create temporary table LISTART
      i_nIdx=cp_AddTableDef('LISTART') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('NOOBSART',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.LISTART_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      this.w_GGINTERVINT = 0
      this.w_ADDGG = 0
      inizio = time()
      this.w_DATAFINE = i_DATSYS
      if ! empty(this.oParentObject.w_GGINTERV)
        if ! empty(this.oParentObject.w_NUMDOC)
          this.w_GGINTERVINT = INT(LOG10(this.oParentObject.w_NUMDOC)*14)
          this.w_DATAINIZ = IIF(this.w_DATAFINE - this.w_GGINTERVINT >= this.w_DATAINIZIO,this.w_DATAFINE - this.w_GGINTERVINT,this.w_DATAINIZIO)
        else
          * --- Analizzo tutti i documenti e movimenti presenti nell'intervallo di date
          this.w_DATAINIZ = i_DATSYS - this.oParentObject.w_GGINTERV
        endif
      else
        if ! empty(this.oParentObject.w_NUMDOC)
          this.w_GGINTERVINT = INT(LOG10(this.oParentObject.w_NUMDOC)*14)
          this.w_DATAINIZ = IIF(this.w_DATAFINE - this.w_GGINTERVINT >= this.w_DATAINIZIO,this.w_DATAFINE - this.w_GGINTERVINT,this.w_DATAINIZIO)
        else
          this.w_DATAINIZ = this.w_DATAINIZIO
        endif
      endif
      do while .T.
        if ! empty(this.oParentObject.w_NUMDOC)
          this.w_NUMITER = 0
          this.w_DOCCOMP = 0
          do while this.w_DATAINIZ < this.w_DATAFINE
            this.w_NUMITER = this.w_NUMITER + 1
            this.w_ADDGG = 3 * this.w_NUMITER
            * --- Select from GSMA3BRS
            do vq_exec with 'GSMA3BRS',this,'_Curs_GSMA3BRS','',.f.,.t.
            if used('_Curs_GSMA3BRS')
              select _Curs_GSMA3BRS
              locate for 1=1
              do while not(eof())
              this.w_TOTRECORD = _Curs_GSMA3BRS.NUMREC
                select _Curs_GSMA3BRS
                continue
              enddo
              use
            endif
            if this.w_TOTRECORD > 5000
              this.w_DATAINIZ = iif(this.w_DATAINIZ + this.w_ADDGG > this.w_DATAFINE,this.w_DATAFINE,this.w_DATAINIZ + this.w_ADDGG)
            else
              exit
            endif
          enddo
        endif
        ADDMSGNL("Intervallo date:  "+DTOC(this.w_DATAFINE)+" - "+DTOC(this.w_DATAINIZ),this)
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_DATAINIZ > this.w_DATAINIZIO
          * --- Modifico intevallo date
          if this.w_GGINTERVINT # 0
            this.w_NEWINIDATE = this.w_DATAINIZ- this.w_GGINTERVINT
            this.w_DATAFINE = IIF(this.w_DATAINIZ - 1 >= this.w_NEWINIDATE,this.w_DATAINIZ -1,this.w_NEWINIDATE)
            this.w_DATAINIZ = this.w_NEWINIDATE
          else
            * --- In questo caso reimposto la data di inizio intervallo alla data di inizio del primo esercizio
            this.w_NEWINIDATE = this.w_DATAINIZIO
            this.w_DATAFINE = IIF(this.w_DATAINIZ - 1 >= this.w_NEWINIDATE,this.w_DATAINIZ -1,this.w_NEWINIDATE)
            this.w_DATAINIZ = this.w_NEWINIDATE
          endif
        else
          * --- Esco
          this.w_TOTRECORD = 0
          * --- Select from LISTART
          i_nConn=i_TableProp[this.LISTART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.LISTART_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select KEYSAL  from "+i_cTable+" LISTART ";
                +" group by KEYSAL";
                 ,"_Curs_LISTART")
          else
            select KEYSAL from (i_cTable);
             group by KEYSAL;
              into cursor _Curs_LISTART
          endif
          if used('_Curs_LISTART')
            select _Curs_LISTART
            locate for 1=1
            do while not(eof())
            this.w_TOTRECORD = this.w_TOTRECORD + 1
              select _Curs_LISTART
              continue
            enddo
            use
          endif
          if this.w_TOTRECORD > 0 and ah_yesno("Sono presenti articoli  per i quali non � stato aggiornato l'ultimo costo di acquisto/vendita.%0Si vuole vedere l'elenco? ")
            this.w_CRLF = chr(13)+chr(10)
            * --- Nuove azioni
            AH_MSG("Attendere, creazione della lista in corso ...")
            * --- Select from LISTART
            i_nConn=i_TableProp[this.LISTART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.LISTART_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select KEYSAL,MAX(ACQVEN) as CICLO1, MIN(ACQVEN) as CICLO2  from "+i_cTable+" LISTART ";
                  +" group by KEYSAL";
                  +" order by KEYSAL";
                   ,"_Curs_LISTART")
            else
              select KEYSAL,MAX(ACQVEN) as CICLO1, MIN(ACQVEN) as CICLO2 from (i_cTable);
               group by KEYSAL;
               order by KEYSAL;
                into cursor _Curs_LISTART
            endif
            if used('_Curs_LISTART')
              select _Curs_LISTART
              locate for 1=1
              do while not(eof())
              this.w_ARTICOLO = left(alltrim(_Curs_LISTART.KEYSAL)+repl(" ",20),20)
              * --- Read from ART_ICOL
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.ART_ICOL_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ARDESART"+;
                  " from "+i_cTable+" ART_ICOL where ";
                      +"ARCODART = "+cp_ToStrODBC(this.w_ARTICOLO);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ARDESART;
                  from (i_cTable) where;
                      ARCODART = this.w_ARTICOLO;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_DESART = NVL(cp_ToDate(_read_.ARDESART),cp_NullValue(_read_.ARDESART))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if _Curs_LISTART.CICLO1 # _Curs_LISTART.CICLO2
                this.w_CICLO = "Non presenti  ultimo costo e ultimo prezzo"
              else
                do case
                  case _Curs_LISTART.CICLO1="A"
                    this.w_CICLO = "Non presente ultimo costo"
                  case _Curs_LISTART.CICLO1="V"
                    this.w_CICLO = "Non presente ultimo prezzo"
                endcase
              endif
              this.w_DESART = left(alltrim(this.w_DESART)+repl(" ",40),40)
              this.w_LOG = this.w_LOG + this.w_ARTICOLO+" - "+this.w_DESART+"  -  "+this.w_CICLO+this.w_CRLF
                select _Curs_LISTART
                continue
              enddo
              use
            endif
            mask=GSMA_KCU(this)
          endif
          exit
        endif
      enddo
      * --- Chiedo se si vogliono aggiornare i valori presenti in 'FILTRI DATA REGISTRAZIONE'
      if (this.oParentObject.w_GGINTERV#this.oParentObject.OldNumGG or this.oParentObject.w_NUMDOC#this.oParentObject.OldNumDoc) AND Ah_yesNo("Si vuole memorizzare i valori relativi al numero giorni e numero documenti,%0per averli disponibili alla prossima elaborazione?")
        if empty(this.oParentObject.w_GGINTERV) and empty(this.oParentObject.w_NUMDOC)
          this.w_FLAGFILTRO = "N"
        else
          this.w_FLAGFILTRO = "S"
        endif
        * --- Aggiorno i valori di filtro
        * --- Write into FILDTREG
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.FILDTREG_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.FILDTREG_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.FILDTREG_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"FLNOFLSM ="+cp_NullLink(cp_ToStrODBC(this.w_FLAGFILTRO),'FILDTREG','FLNOFLSM');
          +",FLGIOINT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GGINTERV),'FILDTREG','FLGIOINT');
          +",FLNUMDOC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NUMDOC),'FILDTREG','FLNUMDOC');
              +i_ccchkf ;
          +" where ";
              +"FLCODAZI = "+cp_ToStrODBC(i_CODAZI);
                 )
        else
          update (i_cTable) set;
              FLNOFLSM = this.w_FLAGFILTRO;
              ,FLGIOINT = this.oParentObject.w_GGINTERV;
              ,FLNUMDOC = this.oParentObject.w_NUMDOC;
              &i_ccchkf. ;
           where;
              FLCODAZI = i_CODAZI;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    endif
    if g_APPLICATION = "ADHOC REVOLUTION" OR UPPER(ALLTRIM(this.w_PUNPAD.Class))=="TGSMA_KRS"
      * --- commit
      cp_EndTrs(.t.)
    endif
    * --- --Fuori dalla transazione elimino gli indici creati
    this.Page_6()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    ADDMSGNL("Ricostruzione saldi terminata",this)
    this.oParentObject.w_ENDTIME = time()
    this.oParentObject.setcontrolsvalue()
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    USE IN SELECT("__tmp__")
    USE IN SELECT("MessErr")
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserimento ultimo costo/prezzo, aggiornamento lista articoli non movimentati
    if g_GPOS="S"
      * --- Se P.O.S. considera anche i movimenti di vendita negozio che non hanno generato documenti
      this.w_FLAVEN = "A"
      * --- Create temporary table TMPSALDI1
      i_nIdx=cp_AddTableDef('TMPSALDI1') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('venulco',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPSALDI1_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      this.w_FLAVEN = "V"
      * --- Insert into TMPSALDI1
      i_nConn=i_TableProp[this.TMPSALDI1_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPSALDI1_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"venulco",this.TMPSALDI1_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    else
      this.w_FLAVEN = "A"
      * --- Create temporary table TMPSALDI1
      i_nIdx=cp_AddTableDef('TMPSALDI1') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('doculco',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPSALDI1_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      this.w_FLAVEN = "V"
      * --- Insert into TMPSALDI1
      i_nConn=i_TableProp[this.TMPSALDI1_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPSALDI1_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"doculco",this.TMPSALDI1_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    * --- Aggiorna MVFLULPV E MVFLULCA dei documenti
    this.w_TIPOARCH = "V"
    this.w_FLACOSTO = "C"
    * --- Write into DOC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MVSERIAL,CPROWNUM,MVNUMRIF"
      do vq_exec with 'gsma5brs',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVFLULPV = _t2.MVFLULPV";
          +",MVFLULCA = _t2.MVFLULCA";
          +i_ccchkf;
          +" from "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 set ";
          +"DOC_DETT.MVFLULPV = _t2.MVFLULPV";
          +",DOC_DETT.MVFLULCA = _t2.MVFLULCA";
          +Iif(Empty(i_ccchkf),"",",DOC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="DOC_DETT.MVSERIAL = t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = t2.MVNUMRIF";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set (";
          +"MVFLULPV,";
          +"MVFLULCA";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.MVFLULPV,";
          +"t2.MVFLULCA";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set ";
          +"MVFLULPV = _t2.MVFLULPV";
          +",MVFLULCA = _t2.MVFLULCA";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
              +" and "+i_cTable+".MVNUMRIF = "+i_cQueryTable+".MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVFLULPV = (select MVFLULPV from "+i_cQueryTable+" where "+i_cWhere+")";
          +",MVFLULCA = (select MVFLULCA from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.w_FLACOSTO = "P"
    * --- Write into DOC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MVSERIAL,CPROWNUM,MVNUMRIF"
      do vq_exec with 'gsma5brs',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVFLULPV = _t2.MVFLULPV";
          +",MVFLULCA = _t2.MVFLULCA";
          +i_ccchkf;
          +" from "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 set ";
          +"DOC_DETT.MVFLULPV = _t2.MVFLULPV";
          +",DOC_DETT.MVFLULCA = _t2.MVFLULCA";
          +Iif(Empty(i_ccchkf),"",",DOC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="DOC_DETT.MVSERIAL = t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = t2.MVNUMRIF";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set (";
          +"MVFLULPV,";
          +"MVFLULCA";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.MVFLULPV,";
          +"t2.MVFLULCA";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set ";
          +"MVFLULPV = _t2.MVFLULPV";
          +",MVFLULCA = _t2.MVFLULCA";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
              +" and "+i_cTable+".MVNUMRIF = "+i_cQueryTable+".MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVFLULPV = (select MVFLULPV from "+i_cQueryTable+" where "+i_cWhere+")";
          +",MVFLULCA = (select MVFLULCA from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Aggiorna MVFLULPV E MVFLULCA dei movimenti di magazzino
    this.w_TIPOARCH = "M"
    this.w_FLACOSTO = "C"
    * --- Write into MVM_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MVM_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MMSERIAL,CPROWNUM,MMNUMRIF"
      do vq_exec with 'gsma5brs',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MVM_DETT_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="MVM_DETT.MMSERIAL = _t2.MMSERIAL";
              +" and "+"MVM_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"MVM_DETT.MMNUMRIF = _t2.MMNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MMFLULPV = _t2.MVFLULPV";
          +",MMFLULCA = _t2.MVFLULCA";
          +i_ccchkf;
          +" from "+i_cTable+" MVM_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="MVM_DETT.MMSERIAL = _t2.MMSERIAL";
              +" and "+"MVM_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"MVM_DETT.MMNUMRIF = _t2.MMNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MVM_DETT, "+i_cQueryTable+" _t2 set ";
          +"MVM_DETT.MMFLULPV = _t2.MVFLULPV";
          +",MVM_DETT.MMFLULCA = _t2.MVFLULCA";
          +Iif(Empty(i_ccchkf),"",",MVM_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="MVM_DETT.MMSERIAL = t2.MMSERIAL";
              +" and "+"MVM_DETT.CPROWNUM = t2.CPROWNUM";
              +" and "+"MVM_DETT.MMNUMRIF = t2.MMNUMRIF";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MVM_DETT set (";
          +"MMFLULPV,";
          +"MMFLULCA";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.MVFLULPV,";
          +"t2.MVFLULCA";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="MVM_DETT.MMSERIAL = _t2.MMSERIAL";
              +" and "+"MVM_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"MVM_DETT.MMNUMRIF = _t2.MMNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MVM_DETT set ";
          +"MMFLULPV = _t2.MVFLULPV";
          +",MMFLULCA = _t2.MVFLULCA";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MMSERIAL = "+i_cQueryTable+".MMSERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
              +" and "+i_cTable+".MMNUMRIF = "+i_cQueryTable+".MMNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MMFLULPV = (select MVFLULPV from "+i_cQueryTable+" where "+i_cWhere+")";
          +",MMFLULCA = (select MVFLULCA from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.w_FLACOSTO = "P"
    * --- Write into MVM_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MVM_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MMSERIAL,CPROWNUM,MMNUMRIF"
      do vq_exec with 'gsma5brs',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MVM_DETT_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="MVM_DETT.MMSERIAL = _t2.MMSERIAL";
              +" and "+"MVM_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"MVM_DETT.MMNUMRIF = _t2.MMNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MMFLULPV = _t2.MVFLULPV";
          +",MMFLULCA = _t2.MVFLULCA";
          +i_ccchkf;
          +" from "+i_cTable+" MVM_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="MVM_DETT.MMSERIAL = _t2.MMSERIAL";
              +" and "+"MVM_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"MVM_DETT.MMNUMRIF = _t2.MMNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MVM_DETT, "+i_cQueryTable+" _t2 set ";
          +"MVM_DETT.MMFLULPV = _t2.MVFLULPV";
          +",MVM_DETT.MMFLULCA = _t2.MVFLULCA";
          +Iif(Empty(i_ccchkf),"",",MVM_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="MVM_DETT.MMSERIAL = t2.MMSERIAL";
              +" and "+"MVM_DETT.CPROWNUM = t2.CPROWNUM";
              +" and "+"MVM_DETT.MMNUMRIF = t2.MMNUMRIF";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MVM_DETT set (";
          +"MMFLULPV,";
          +"MMFLULCA";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.MVFLULPV,";
          +"t2.MVFLULCA";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="MVM_DETT.MMSERIAL = _t2.MMSERIAL";
              +" and "+"MVM_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"MVM_DETT.MMNUMRIF = _t2.MMNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MVM_DETT set ";
          +"MMFLULPV = _t2.MVFLULPV";
          +",MMFLULCA = _t2.MVFLULCA";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MMSERIAL = "+i_cQueryTable+".MMSERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
              +" and "+i_cTable+".MMNUMRIF = "+i_cQueryTable+".MMNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MMFLULPV = (select MVFLULPV from "+i_cQueryTable+" where "+i_cWhere+")";
          +",MMFLULCA = (select MVFLULCA from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Aggiorna Ultimo Costo
    this.w_FLACOSTO = "C"
    * --- Write into SALDIART
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="SLCODICE,SLCODMAG"
      do vq_exec with 'gsma3crs',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="SALDIART.SLCODICE = _t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = _t2.SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLDATUCA = _t2.MVDATDOC";
          +",SLCODVAA = _t2.MVVALNAZ";
          +",SLVALUCA = _t2.MVVALULT";
          +i_ccchkf;
          +" from "+i_cTable+" SALDIART, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="SALDIART.SLCODICE = _t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = _t2.SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDIART, "+i_cQueryTable+" _t2 set ";
          +"SALDIART.SLDATUCA = _t2.MVDATDOC";
          +",SALDIART.SLCODVAA = _t2.MVVALNAZ";
          +",SALDIART.SLVALUCA = _t2.MVVALULT";
          +Iif(Empty(i_ccchkf),"",",SALDIART.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="SALDIART.SLCODICE = t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = t2.SLCODMAG";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDIART set (";
          +"SLDATUCA,";
          +"SLCODVAA,";
          +"SLVALUCA";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.MVDATDOC,";
          +"t2.MVVALNAZ,";
          +"t2.MVVALULT";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="SALDIART.SLCODICE = _t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = _t2.SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDIART set ";
          +"SLDATUCA = _t2.MVDATDOC";
          +",SLCODVAA = _t2.MVVALNAZ";
          +",SLVALUCA = _t2.MVVALULT";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".SLCODICE = "+i_cQueryTable+".SLCODICE";
              +" and "+i_cTable+".SLCODMAG = "+i_cQueryTable+".SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLDATUCA = (select MVDATDOC from "+i_cQueryTable+" where "+i_cWhere+")";
          +",SLCODVAA = (select MVVALNAZ from "+i_cQueryTable+" where "+i_cWhere+")";
          +",SLVALUCA = (select MVVALULT from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Aggiorna Ultimo Prezzo
    this.w_FLACOSTO = "P"
    * --- Write into SALDIART
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="SLCODICE,SLCODMAG"
      do vq_exec with 'gsma4crs',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="SALDIART.SLCODICE = _t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = _t2.SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLDATUPV = _t2.MVDATDOC";
          +",SLCODVAV = _t2.MVVALNAZ";
          +",SLVALUPV = _t2.MVVALULT";
          +i_ccchkf;
          +" from "+i_cTable+" SALDIART, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="SALDIART.SLCODICE = _t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = _t2.SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDIART, "+i_cQueryTable+" _t2 set ";
          +"SALDIART.SLDATUPV = _t2.MVDATDOC";
          +",SALDIART.SLCODVAV = _t2.MVVALNAZ";
          +",SALDIART.SLVALUPV = _t2.MVVALULT";
          +Iif(Empty(i_ccchkf),"",",SALDIART.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="SALDIART.SLCODICE = t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = t2.SLCODMAG";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDIART set (";
          +"SLDATUPV,";
          +"SLCODVAV,";
          +"SLVALUPV";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.MVDATDOC,";
          +"t2.MVVALNAZ,";
          +"t2.MVVALULT";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="SALDIART.SLCODICE = _t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = _t2.SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDIART set ";
          +"SLDATUPV = _t2.MVDATDOC";
          +",SLCODVAV = _t2.MVVALNAZ";
          +",SLVALUPV = _t2.MVVALULT";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".SLCODICE = "+i_cQueryTable+".SLCODICE";
              +" and "+i_cTable+".SLCODMAG = "+i_cQueryTable+".SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLDATUPV = (select MVDATDOC from "+i_cQueryTable+" where "+i_cWhere+")";
          +",SLCODVAV = (select MVVALNAZ from "+i_cQueryTable+" where "+i_cWhere+")";
          +",SLVALUPV = (select MVVALULT from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Elimino dalla lista degli articoli quelli gi� trattati
    * --- Delete from LISTART
    i_nConn=i_TableProp[this.LISTART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LISTART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".KEYSAL = "+i_cQueryTable+".KEYSAL";
            +" and "+i_cTable+".ACQVEN = "+i_cQueryTable+".ACQVEN";
    
      do vq_exec with 'gsma4brs',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Cancello tabella temporanea
    * --- Drop temporary table TMPSALDI1
    i_nIdx=cp_GetTableDefIdx('TMPSALDI1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPSALDI1')
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --Creazione indici array
    * --- Crea array
    * --- -------------------------------------------------------------------------------------------------------------------------------------
    * --- Primo elemento identifica il nome della tabella, il secondo i campi che formano l'indice
    * --- -------------------------------------------------------------------------------------------------------------------------------------
    * --- Aggiorna array
    if UPPER(g_APPLICATION) = "ADHOC REVOLUTION"
      * --- --Tabelle indici per AdHocRevolution
      if g_GPOS="S"
        this.w_NELEMENT = 4
      else
        this.w_NELEMENT = 3
      endif
      * --- --Creazione indici array contenente indici tabella documenti/movimenti
      Dimension l_ARRAYINDEX[this.w_NELEMENT,2]
      l_ARRAYINDEX[1,1] = "DOC_DETT" 
 l_ARRAYINDEX[1,2] = "MVKEYSAL,MVCODMAG,MVCODMAT,MVQTAUM1,MVFLCASC,MVF2CASC,MVF2ORDI,MVFLORDI,MVF2IMPE,MVFLIMPE,MVF2RISE,MVFLRISE,MVQTASAL,MVTIPRIG" 
 l_ARRAYINDEX[2,1] = "DOC_DETT" 
 l_ARRAYINDEX[2,2] = "MVKEYSAL,MVCODMAG,MVVALULT,MVFLCASC,MVFLOMAG" 
 l_ARRAYINDEX[3,1] = "MVM_DETT" 
 l_ARRAYINDEX[3,2] = "MMKEYSAL,MMCODMAG,MMCODMAT,MMCODART,MMQTAUM1,MMFLCASC,MMFLRISE,MMFLORDI,MMFLIMPE,MMF2CASC,MMF2RISE,MMF2ORDI,MMF2IMPE"
      if g_GPOS="S"
        l_ARRAYINDEX[4,1] = "CORDRISP" 
 l_ARRAYINDEX[4,2] = "MDSERIAL,CPROWNUM,MDCODART,MDMAGSAL,MDFLULPV,MDVALULT,MDFLCASC , MDFLOMAG"
      endif
    else
      * --- --Tabelle indici per AdHocEnterprise
      this.w_NELEMENT = 2
      * --- --Creazione indici array contenente indici tabella documenti\movimenti
      Dimension l_ARRAYINDEX[this.w_NELEMENT,2]
      l_ARRAYINDEX[1,1] = "DOC_DETT" 
 l_ARRAYINDEX[1,2] ="MVKEYSAL,MVCODMAG,MVCODMAT,MVQTAUM1,MVFLCASC,MVF2CASC,MVF2ORDI,MVFLORDI,MVF2IMPE,MVFLIMPE,MVF2RISE,MVFLRISE,MVQTASAL,MVTIPRIG,MVCODART,MVCODVAR" 
 l_ARRAYINDEX[2,1] = "MVM_DETT" 
 l_ARRAYINDEX[2,2] = "MMKEYSAL,MMCODMAG,MMCODMAT,MMCODART,MMCODVAR,MMQTAUM1,MMFLCASC,MMFLRISE,MMFLORDI,MMFLIMPE,MMF2CASC,MMF2RISE,MMF2ORDI,MMF2IMPE"
    endif
    * --- -------------------------------------------------------------------------------------------------------------------------------------
    * --- Creazione indice
    * --- -------------------------------------------------------------------------------------------------------------------------------------
    ADDMSGNL("Creazione indici  ",this)
    * --- Creazione indici su DOC_DETT  e MVM_DETT
    * --- Istruzioni per creazione indice su DOC_DETT
    Dimension INDICE (1)
    this.w_CONNECT = i_TableProp[this.MAGAZZIN_idx,3]
    this.w_DB = SQLXGetDatabaseType(this.w_CONNECT)
    if vartype(g_NoCreateIndexes)="L"
      this.w_ASSEGNATA = .T.
      this.w_OLDVALUE = g_NoCreateIndexes
    endif
    * --- --Creazione indice su DOC_DETT
    this.w_COUNTER = 1
    this.w_ARCHIVIO = "##zz##"
    this.w_tmpINDEX = 0
    * --- -- Occorre ordinare l'array per la rottura di codice
    Asort(L_ARRAYINDEX)
    do while this.w_COUNTER<=ALEN(L_ARRAYINDEX)/2
      if this.w_ARCHIVIO<>l_ARRAYINDEX[this.w_COUNTER,1]
        if this.w_ARCHIVIO<>"##zz##"
          * --- --Occorre creare l'indice non alla prima iterazione in quanto non sono stati ancora processati
          this.Page_5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- --Cambio tabella gestione indice
        this.w_ARCHIVIO = l_ARRAYINDEX[this.w_COUNTER,1]
        * --- --Indice utilizzato per dimensionare array contenente indici
        this.w_tmpINDEX = 0
      endif
      this.w_TABELLA = cp_SetAzi(i_CODAZI)+l_ARRAYINDEX[this.w_COUNTER,1]
      this.w_CAMPIINDICE = l_ARRAYINDEX[this.w_COUNTER,2]
      this.w_tmpINDEX = this.w_tmpINDEX+1
      Dimension INDICE (this.w_tmpINDEX)
      indice[this.w_tmpINDEX] = this.w_CAMPIINDICE
      if this.w_COUNTER=ALEN(L_ARRAYINDEX)/2
        * --- --Occorre creare gli indici dell'ultima tabella processata
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      this.w_COUNTER = this.w_COUNTER+1
    enddo
    if this.w_ASSEGNATA
      * --- Ripristino il valore iniziale
      g_NoCreateIndexes=this.w_OLDVALUE
    endif
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --Gestione indice
    g_NoCreateIndexes=.f.
    * --- Elimino l'eventuale indice temporaneo (se presente)
    if this.w_DB == "DB2"
      * --- --Cancellazione preventiva dei possibili indici
      this.w_CCINDEX = 1
      do while this.w_CCINDEX<=ALEN(INDICE)
        this.w_CMD = "DROP INDEX "+ALLTRIM(this.w_TABELLA)+"_tmp"+alltrim(str(this.w_CCINDEX))
        sqlexec(this.w_CONNECT,this.w_CMD)
        this.w_CCINDEX = this.w_CCINDEX+1
        * --- --Per Drop db2 occorre memorizzarsi il contatore delle tabelle temporanee create
      enddo
    endif
    cp_CreateTempIndexes(this.w_DB,this.w_CONNECT,this.w_TABELLA,@INDICE)
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --Eliminazione Indice
    ADDMSGNL("Eliminazione indici  ",this)
    * --- --Il contatore degli indici da creare cambia in base all'applicazione
    this.w_ARCHIVIO = "##zz##"
    this.w_tmpINDEX = 0
    this.w_COUNTER = 1
    * --- --Tabella Documenti\Movimenti
    do while this.w_COUNTER<=ALEN(L_ARRAYINDEX)/2
      if this.w_ARCHIVIO<>l_ARRAYINDEX[this.w_COUNTER,1]
        this.w_tmpINDEX = 1
        this.w_ARCHIVIO = L_ARRAYINDEX[this.w_COUNTER,1]
      else
        this.w_tmpINDEX = this.w_tmpINDEX+1
      endif
      this.w_TABELLA = cp_SetAzi(i_CODAZI)+l_ARRAYINDEX[this.w_COUNTER,1]
      if this.w_DB=="DB2"
        this.w_CMD = "DROP INDEX "+ALLTRIM(this.w_TABELLA)+"_tmp"+ALLTRIM(STR(this.w_tmpINDEX))
      else
        this.w_CMD = "DROP INDEX "+ALLTRIM(this.w_TABELLA)+"."+ALLTRIM(this.w_TABELLA)+"_tmp"+ALLTRIM(STR(this.w_tmpINDEX))
      endif
      sqlexec(this.w_CONNECT,this.w_CMD)
      this.w_COUNTER = this.w_COUNTER+1
    enddo
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,15)]
    this.cWorkTables[1]='MAGAZZIN'
    this.cWorkTables[2]='SALDIART'
    this.cWorkTables[3]='*TMPSALDI'
    this.cWorkTables[4]='LOTTIART'
    this.cWorkTables[5]='DOC_DETT'
    this.cWorkTables[6]='MVM_DETT'
    this.cWorkTables[7]='*TMPSALAGG'
    this.cWorkTables[8]='*TMPSALDI1'
    this.cWorkTables[9]='ESERCIZI'
    this.cWorkTables[10]='DOC_MAST'
    this.cWorkTables[11]='*LISTART'
    this.cWorkTables[12]='ART_ICOL'
    this.cWorkTables[13]='FILDTREG'
    this.cWorkTables[14]='*TMP_ARTI'
    this.cWorkTables[15]='*TMP_MAGA'
    return(this.OpenAllTables(15))

  proc CloseCursors()
    if used('_Curs_ESERCIZI')
      use in _Curs_ESERCIZI
    endif
    if used('_Curs_query_GSMA4BRK')
      use in _Curs_query_GSMA4BRK
    endif
    if used('_Curs_GSMA3BRS')
      use in _Curs_GSMA3BRS
    endif
    if used('_Curs_LISTART')
      use in _Curs_LISTART
    endif
    if used('_Curs_LISTART')
      use in _Curs_LISTART
    endif
    if used('_Curs_TMP_ARTI')
      use in _Curs_TMP_ARTI
    endif
    if used('_Curs_TMP_MAGA')
      use in _Curs_TMP_MAGA
    endif
    if used('_Curs_TMP_ARTI')
      use in _Curs_TMP_ARTI
    endif
    if used('_Curs_TMP_MAGA')
      use in _Curs_TMP_MAGA
    endif
    if used('_Curs_TMP_ARTI')
      use in _Curs_TMP_ARTI
    endif
    if used('_Curs_TMP_MAGA')
      use in _Curs_TMP_MAGA
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
