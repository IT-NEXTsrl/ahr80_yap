* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_krs                                                        *
*              Visualizza RSS feeds                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-02-05                                                      *
* Last revis.: 2010-05-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_krs",oParentObject))

* --- Class definition
define class tgsut_krs as StdForm
  Top    = 8
  Left   = 3

  * --- Standard Properties
  Width  = 846
  Height = 556
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-05-24"
  HelpContextID=115950441
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsut_krs"
  cComment = "Visualizza RSS feeds"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_FILTRO = space(254)
  w_ISREAD = space(1)
  w_TIPO = space(1)
  w_FEEDZOOM = space(254)
  o_FEEDZOOM = space(254)
  w_BRWCOMPLETE = .F.
  w_SUBSCRIBE = space(1)
  w_RFDESCRI = space(254)
  w_RFRSSURL = space(254)
  w_SELEZI = space(1)
  o_SELEZI = space(1)
  w_TreeView = .NULL.
  w_ZOOM = .NULL.
  w_HTML = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_krsPag1","gsut_krs",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFILTRO_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_TreeView = this.oPgFrm.Pages(1).oPag.TreeView
    this.w_ZOOM = this.oPgFrm.Pages(1).oPag.ZOOM
    this.w_HTML = this.oPgFrm.Pages(1).oPag.HTML
    DoDefault()
    proc Destroy()
      this.w_TreeView = .NULL.
      this.w_ZOOM = .NULL.
      this.w_HTML = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_FILTRO=space(254)
      .w_ISREAD=space(1)
      .w_TIPO=space(1)
      .w_FEEDZOOM=space(254)
      .w_BRWCOMPLETE=.f.
      .w_SUBSCRIBE=space(1)
      .w_RFDESCRI=space(254)
      .w_RFRSSURL=space(254)
      .w_SELEZI=space(1)
      .oPgFrm.Page1.oPag.TreeView.Calculate()
          .DoRTCalc(1,1,.f.)
        .w_ISREAD = ""
      .oPgFrm.Page1.oPag.ZOOM.Calculate()
      .oPgFrm.Page1.oPag.HTML.Calculate()
        .w_TIPO = .w_TreeView.GetVar("TIPO")
        .w_FEEDZOOM = .w_ZOOM.GETVAR("TITLE")
        .w_BRWCOMPLETE = .F.
        .w_SUBSCRIBE = 'S'
        .w_RFDESCRI = .w_TreeView.GetVar("FNAME")
        .w_RFRSSURL = .w_TreeView.GetVar("FURL")
        .w_SELEZI = "D"
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_28.enabled = this.oPgFrm.Page1.oPag.oBtn_1_28.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsut_krs
    This.w_Treeview.oTree.PathSeparator = '\'
    This.w_TIPO = ""
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.TreeView.Calculate()
        .oPgFrm.Page1.oPag.ZOOM.Calculate()
        .oPgFrm.Page1.oPag.HTML.Calculate()
        .DoRTCalc(1,2,.t.)
            .w_TIPO = .w_TreeView.GetVar("TIPO")
            .w_FEEDZOOM = .w_ZOOM.GETVAR("TITLE")
        if .o_FEEDZOOM<>.w_FEEDZOOM
          .Calculate_KHPUOHSEBG()
        endif
        .DoRTCalc(5,6,.t.)
            .w_RFDESCRI = .w_TreeView.GetVar("FNAME")
            .w_RFRSSURL = .w_TreeView.GetVar("FURL")
        if .o_SELEZI<>.w_SELEZI
          .Calculate_PDEGLSQSEZ()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(9,9,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.TreeView.Calculate()
        .oPgFrm.Page1.oPag.ZOOM.Calculate()
        .oPgFrm.Page1.oPag.HTML.Calculate()
    endwith
  return

  proc Calculate_XOJFKVXEUE()
    with this
          * --- Init
          GSUT_BRS(this;
              ,'I';
             )
    endwith
  endproc
  proc Calculate_LJGPBPINXI()
    with this
          * --- Treeview NodeSelected
          GSUT_BRS(this;
              ,'C';
             )
    endwith
  endproc
  proc Calculate_KHPUOHSEBG()
    with this
          * --- Item selected
          GSUT_BRS(this;
              ,'V';
             )
    endwith
  endproc
  proc Calculate_JPOBNNEQIN()
    with this
          * --- Chiusura
          GSUT_BRS(this;
              ,'X';
             )
    endwith
  endproc
  proc Calculate_IOPZCHOXLA()
    with this
          * --- NavigationComplete
          .w_BRWCOMPLETE = .T.
    endwith
  endproc
  proc Calculate_PDEGLSQSEZ()
    with this
          * --- Seleziona/Deseleziona
          GSUT_BRS(this;
              ,'Z';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_28.enabled = this.oPgFrm.Page1.oPag.oBtn_1_28.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("FormLoad")
          .Calculate_XOJFKVXEUE()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.TreeView.Event(cEvent)
      .oPgFrm.Page1.oPag.ZOOM.Event(cEvent)
      .oPgFrm.Page1.oPag.HTML.Event(cEvent)
        if lower(cEvent)==lower("w_TreeView NodeSelected")
          .Calculate_LJGPBPINXI()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Aggiorna")
          .Calculate_KHPUOHSEBG()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Done")
          .Calculate_JPOBNNEQIN()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_HTML NavigationComplete")
          .Calculate_IOPZCHOXLA()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFILTRO_1_3.value==this.w_FILTRO)
      this.oPgFrm.Page1.oPag.oFILTRO_1_3.value=this.w_FILTRO
    endif
    if not(this.oPgFrm.Page1.oPag.oISREAD_1_5.RadioValue()==this.w_ISREAD)
      this.oPgFrm.Page1.oPag.oISREAD_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_26.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_26.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_FEEDZOOM = this.w_FEEDZOOM
    this.o_SELEZI = this.w_SELEZI
    return

enddefine

* --- Define pages as container
define class tgsut_krsPag1 as StdContainer
  Width  = 842
  height = 556
  stdWidth  = 842
  stdheight = 556
  resizeXpos=636
  resizeYpos=473
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object TreeView as cp_Treeview with uid="GRXJQZICZI",left=5, top=2, width=287,height=499,;
    caption='Object',;
   bGlobalFont=.t.,;
    nIndent=20,cCursor="TMPINFOTREEVIEW",cShowFields="FNAME",cNodeShowField="",cLeafShowField="",cNodeBmp="aperturafile.ico",cLeafBmp="RSSFeed.ico",;
    cEvent = "Init,TreeViewAgg",;
    nPag=1;
    , HelpContextID = 62047206

  add object oFILTRO_1_3 as StdField with uid="NCJYYGXWFJ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_FILTRO", cQueryName = "FILTRO",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 40908202,;
   bGlobalFont=.t.,;
    Height=21, Width=381, Left=400, Top=5, InputMask=replicate('X',254)


  add object oBtn_1_4 as StdButton with uid="AZEGEDFTZF",left=788, top=3, width=48,height=45,;
    CpPicture="BMP\requery.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la ricerca";
    , HelpContextID = 115950346;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      this.parent.oContained.NotifyEvent("Aggiorna")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oISREAD_1_5 as StdCombo with uid="TKRIUBFOFU",value=3,rtseq=2,rtrep=.f.,left=402,top=28,width=94,height=21;
    , HelpContextID = 244239226;
    , cFormVar="w_ISREAD",RowSource=""+"Non letti,"+"Letti,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oISREAD_1_5.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'',;
    space(1)))))
  endfunc
  func oISREAD_1_5.GetRadio()
    this.Parent.oContained.w_ISREAD = this.RadioValue()
    return .t.
  endfunc

  func oISREAD_1_5.SetRadio()
    this.Parent.oContained.w_ISREAD=trim(this.Parent.oContained.w_ISREAD)
    this.value = ;
      iif(this.Parent.oContained.w_ISREAD=='N',1,;
      iif(this.Parent.oContained.w_ISREAD=='S',2,;
      iif(this.Parent.oContained.w_ISREAD=='',3,;
      0)))
  endfunc


  add object ZOOM as cp_szoombox with uid="FFMOSPLAJA",left=346, top=47, width=493,height=189,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="TMPRSSITEM",bRetriveAllRows=.f.,cZoomFile="GSUT_KRS",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",;
    cEvent = "Aggiorna",;
    nPag=1;
    , HelpContextID = 62047206


  add object HTML as cp_Browser with uid="MWYTZETMZN",left=356, top=242, width=475,height=259,;
    caption='Object',;
   bGlobalFont=.t.,;
    cEvent = "NewURL",;
    nPag=1;
    , HelpContextID = 62047206


  add object oBtn_1_12 as StdButton with uid="APIWEFNJEL",left=8, top=506, width=48,height=45,;
    CpPicture="BMP\RSSFolder.BMP", caption="", nPag=1;
    , ToolTipText = "Nuova folder";
    , HelpContextID = 30535766;
    , Caption='\<Folder';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      with this.Parent.oContained
        GSUT_BRS(this.Parent.oContained,"P")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_12.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_TIPO = 'D' Or .w_TIPO = 'R')
      endwith
    endif
  endfunc


  add object oBtn_1_13 as StdButton with uid="ZJIUAZYUNY",left=788, top=506, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 108633018;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_14 as StdButton with uid="ELZIHYJLUO",left=244, top=506, width=48,height=45,;
    CpPicture="BMP\download.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per aggiornare i feed";
    , HelpContextID = 108224458;
    , Caption='\<Download';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      with this.Parent.oContained
        GSUT_BRS(this.Parent.oContained,"D")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_14.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_TIPO = "R" Or .w_TIPO = "F" Or .w_TIPO = "D")
      endwith
    endif
  endfunc


  add object oBtn_1_15 as StdButton with uid="IWUFCYLFUV",left=161, top=506, width=48,height=45,;
    CpPicture="BMP\RSSFeed2.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per sottoscrivere il feed";
    , HelpContextID = 59342376;
    , Caption='\<Sottoscrivi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      with this.Parent.oContained
        GSUT_BRS(this.Parent.oContained,"S")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_15.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_TIPO='U')
      endwith
    endif
  endfunc


  add object oBtn_1_16 as StdButton with uid="ODERRYURBK",left=110, top=506, width=48,height=45,;
    CpPicture="BMP\RSSFolder2.BMP", caption="", nPag=1;
    , ToolTipText = "Elimina sottoscrizione feed/Elimina cartella";
    , HelpContextID = 236633414;
    , Caption='\<Elimina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      with this.Parent.oContained
        GSUT_BRS(this.Parent.oContained,"E")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_16.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_TIPO = 'F' Or .w_TIPO = 'D')
      endwith
    endif
  endfunc


  add object oBtn_1_17 as StdButton with uid="KFMFVMRFEJ",left=59, top=506, width=48,height=45,;
    CpPicture="BMP\RSSFeed.BMP", caption="", nPag=1;
    , ToolTipText = "Nuovo feed";
    , HelpContextID = 108956074;
    , Caption='\<Feed';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      with this.Parent.oContained
        GSUT_BRS(this.Parent.oContained,"F")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_17.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_TIPO = 'D' Or .w_TIPO = 'R')
      endwith
    endif
  endfunc


  add object oBtn_1_25 as StdButton with uid="UKTVIDMMRB",left=483, top=506, width=48,height=45,;
    CpPicture="BMP\btsend.bmp", caption="", nPag=1;
    , ToolTipText = "Segna come gi� letto";
    , HelpContextID = 259889482;
    , Caption='\<Letto';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_25.Click()
      with this.Parent.oContained
        GSUT_BRS(this.Parent.oContained,"R")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_25.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_TIPO = 'F' Or .w_TIPO = 'D')
      endwith
    endif
  endfunc

  add object oSELEZI_1_26 as StdRadio with uid="BUGXSLLSWC",rtseq=9,rtrep=.f.,left=354, top=508, width=128,height=32;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_26.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 134166746
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 134166746
      this.Buttons(2).Top=15
      this.SetAll("Width",126)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_1_26.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"D",;
    space(1))))
  endfunc
  func oSELEZI_1_26.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_26.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=="S",1,;
      iif(this.Parent.oContained.w_SELEZI=="D",2,;
      0))
  endfunc


  add object oBtn_1_28 as StdButton with uid="LPGACROLQE",left=584, top=506, width=48,height=45,;
    CpPicture="BMP\elimina.bmp", caption="", nPag=1;
    , ToolTipText = "Elimina notizie selezionate";
    , HelpContextID = 236633414;
    , Caption='\<Elimina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_28.Click()
      with this.Parent.oContained
        GSUT_BRS(this.Parent.oContained,"N")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_28.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_TIPO = 'F' Or .w_TIPO = 'D')
      endwith
    endif
  endfunc


  add object oBtn_1_29 as StdButton with uid="SLNMMSIQQJ",left=534, top=506, width=48,height=45,;
    CpPicture="BMP\mail.bmp", caption="", nPag=1;
    , ToolTipText = "Segna come da leggere";
    , HelpContextID = 83761210;
    , Caption='\<Non letto';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_29.Click()
      with this.Parent.oContained
        GSUT_BRS(this.Parent.oContained,"U")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_29.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_TIPO = 'F' Or .w_TIPO = 'D')
      endwith
    endif
  endfunc


  add object oBtn_1_30 as StdButton with uid="BIKXMINKGL",left=300, top=54, width=48,height=45,;
    CpPicture="BMP\refresh.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per aggiornare la treeview";
    , HelpContextID = 41326695;
    , Caption='\<Aggiorna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_30.Click()
      with this.Parent.oContained
        GSUT_BRS(this.Parent.oContained,"T")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_31 as StdButton with uid="ZVAMGQYVJH",left=300, top=150, width=48,height=45,;
    CpPicture="BMP\IMPLODI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per chiudere la struttura";
    , HelpContextID = 193319290;
    , Caption='\<Implodi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_31.Click()
      with this.Parent.oContained
        .w_TREEVIEW.ExpandAll(.F.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_32 as StdButton with uid="XWAVWHNZRN",left=300, top=102, width=48,height=45,;
    CpPicture="BMP\ESPLODI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per espandere la struttura";
    , HelpContextID = 193317818;
    , Caption='\<Esplodi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_32.Click()
      with this.Parent.oContained
        .w_TREEVIEW.ExpandAll(.T.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_24 as StdString with uid="OFZHBDOOLT",Visible=.t., Left=351, Top=7,;
    Alignment=1, Width=48, Height=18,;
    Caption="Filtro:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="KVZTPYPPZY",Visible=.t., Left=305, Top=30,;
    Alignment=1, Width=94, Height=17,;
    Caption="Messaggi:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_krs','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
