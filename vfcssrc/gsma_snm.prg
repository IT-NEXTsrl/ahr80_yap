* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_snm                                                        *
*              Stampa articoli non movimentati                                 *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-14                                                      *
* Last revis.: 2012-01-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_snm",oParentObject))

* --- Class definition
define class tgsma_snm as StdForm
  Top    = 24
  Left   = 24

  * --- Standard Properties
  Width  = 593
  Height = 370
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-01-17"
  HelpContextID=93684887
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=35

  * --- Constant Properties
  _IDX = 0
  ART_ICOL_IDX = 0
  MAGAZZIN_IDX = 0
  FAM_ARTI_IDX = 0
  GRUMERC_IDX = 0
  CATEGOMO_IDX = 0
  MARCHI_IDX = 0
  CONTI_IDX = 0
  ESERCIZI_IDX = 0
  AZIENDA_IDX = 0
  DET_VARI_IDX = 0
  INVENTAR_IDX = 0
  cPrg = "gsma_snm"
  cComment = "Stampa articoli non movimentati"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_AZIENDA = space(5)
  w_INIZIO = ctod('  /  /  ')
  o_INIZIO = ctod('  /  /  ')
  w_FINE = ctod('  /  /  ')
  o_FINE = ctod('  /  /  ')
  w_ESERC = space(4)
  w_ESERF = space(4)
  w_DATASTAM = ctod('  /  /  ')
  o_DATASTAM = ctod('  /  /  ')
  w_ANTIPCON = space(10)
  w_CODMAG = space(5)
  o_CODMAG = space(5)
  w_MAGCOL = space(10)
  o_MAGCOL = space(10)
  w_CODFAM = space(5)
  w_CODGRU = space(5)
  w_CODCAT = space(5)
  w_CODMAR = space(5)
  w_ESEINV = space(4)
  w_INV = space(6)
  w_TIPOCOST = space(1)
  w_CODART = space(20)
  o_CODART = space(20)
  w_PRKEYVAR = space(20)
  o_PRKEYVAR = space(20)
  w_CODVAR = space(20)
  w_DESMAG = space(30)
  w_DESFAM = space(35)
  w_DESGRU = space(35)
  w_DESCAT = space(35)
  w_DESMAR = space(35)
  w_PRCODART = space(20)
  w_TIPO = space(2)
  w_DESINI = space(40)
  w_TIPVAR = space(5)
  w_DESVAR = space(40)
  w_SELORD = space(10)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_ELENMAG = space(254)
  w_INVMAG = space(5)
  w_CODART = space(20)
  * --- Area Manuale = Declare Variables
  * --- gsma_snm
  PROC RepoPrint
  *Estrazione dati
    ah_msg("Elaborazione in corso...")
    vq_exec(this.w_OQRY,this,"__TMP__")
    *Variabili per il Report
    L_INIZIO=this.w_INIZIO
    L_FINE=this.w_FINE
    L_CODMAG=this.w_CODMAG
    L_DATASTAM=this.w_DATASTAM
    L_MAGCOL=this.w_MAGCOL
    L_CODGRU=this.w_CODGRU
    L_CODFAM=this.w_CODFAM
    L_CODCAT=this.w_CODCAT
    L_CODMAR=this.w_CODMAR
    L_CODART=this.w_CODART
    L_CODVAR=this.w_CODVAR
    L_CODINV=this.w_INV
    L_TIPCOST=this.w_TIPOCOST
    *Lancio la stampa
    CP_CHPRN (this.w_OREP,'',this)
  EndPROC
  
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_snmPag1","gsma_snm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oINIZIO_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[11]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='MAGAZZIN'
    this.cWorkTables[3]='FAM_ARTI'
    this.cWorkTables[4]='GRUMERC'
    this.cWorkTables[5]='CATEGOMO'
    this.cWorkTables[6]='MARCHI'
    this.cWorkTables[7]='CONTI'
    this.cWorkTables[8]='ESERCIZI'
    this.cWorkTables[9]='AZIENDA'
    this.cWorkTables[10]='DET_VARI'
    this.cWorkTables[11]='INVENTAR'
    return(this.OpenAllTables(11))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AZIENDA=space(5)
      .w_INIZIO=ctod("  /  /  ")
      .w_FINE=ctod("  /  /  ")
      .w_ESERC=space(4)
      .w_ESERF=space(4)
      .w_DATASTAM=ctod("  /  /  ")
      .w_ANTIPCON=space(10)
      .w_CODMAG=space(5)
      .w_MAGCOL=space(10)
      .w_CODFAM=space(5)
      .w_CODGRU=space(5)
      .w_CODCAT=space(5)
      .w_CODMAR=space(5)
      .w_ESEINV=space(4)
      .w_INV=space(6)
      .w_TIPOCOST=space(1)
      .w_CODART=space(20)
      .w_PRKEYVAR=space(20)
      .w_CODVAR=space(20)
      .w_DESMAG=space(30)
      .w_DESFAM=space(35)
      .w_DESGRU=space(35)
      .w_DESCAT=space(35)
      .w_DESMAR=space(35)
      .w_PRCODART=space(20)
      .w_TIPO=space(2)
      .w_DESINI=space(40)
      .w_TIPVAR=space(5)
      .w_DESVAR=space(40)
      .w_SELORD=space(10)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_ELENMAG=space(254)
      .w_INVMAG=space(5)
      .w_CODART=space(20)
        .w_AZIENDA = i_CODAZI
          .DoRTCalc(2,3,.f.)
        .w_ESERC = CALCESER(.w_INIZIO,'ZZZZ')
        .w_ESERF = CALCESER(.w_FINE,'ZZZZ')
        .w_DATASTAM = i_datsys
        .w_ANTIPCON = 'F'
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_CODMAG))
          .link_1_8('Full')
        endif
        .w_MAGCOL = iif( empty(.w_CODMAG) , 'N', .w_MAGCOL )
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_CODFAM))
          .link_1_10('Full')
        endif
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_CODGRU))
          .link_1_11('Full')
        endif
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_CODCAT))
          .link_1_12('Full')
        endif
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_CODMAR))
          .link_1_13('Full')
        endif
        .w_ESEINV = IIF(.w_ESEINV>.w_ESERF OR .w_ESEINV<.w_ESERC OR EMPTY(.w_CODMAG),SPACE(4),.w_ESEINV)
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_ESEINV))
          .link_1_14('Full')
        endif
        .w_INV = IIF(.w_MAGCOL='N' OR .w_ESEINV>.w_ESERF OR .w_ESEINV<.w_ESERC OR EMPTY(.w_CODMAG),SPACE(6),.w_INV)
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_INV))
          .link_1_15('Full')
        endif
        .w_TIPOCOST = 'S'
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_CODART))
          .link_1_17('Full')
        endif
        .w_PRKEYVAR = space(20)
        .w_CODVAR = .w_PRKEYVAR
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_CODVAR))
          .link_1_19('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
          .DoRTCalc(20,24,.f.)
        .w_PRCODART = .w_CODART
          .DoRTCalc(26,29,.f.)
        .w_SELORD = 'A'
        .w_OBTEST = .w_DATASTAM
          .DoRTCalc(32,32,.f.)
        .w_ELENMAG = IIF(.w_MAGCOL='N',.w_CODMAG,.w_ELENMAG)
      .oPgFrm.Page1.oPag.oObj_1_49.Calculate()
        .DoRTCalc(34,35,.f.)
        if not(empty(.w_CODART))
          .link_1_52('Full')
        endif
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- gsma_snm
    *Lancio la stampa
    This.RepoPrint()
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
        if .o_INIZIO<>.w_INIZIO
            .w_ESERC = CALCESER(.w_INIZIO,'ZZZZ')
        endif
        if .o_FINE<>.w_FINE
            .w_ESERF = CALCESER(.w_FINE,'ZZZZ')
        endif
        .DoRTCalc(6,8,.t.)
        if .o_CODMAG<>.w_CODMAG
            .w_MAGCOL = iif( empty(.w_CODMAG) , 'N', .w_MAGCOL )
        endif
        .DoRTCalc(10,13,.t.)
        if .o_INIZIO<>.w_INIZIO.or. .o_FINE<>.w_FINE.or. .o_CODMAG<>.w_CODMAG.or. .o_MAGCOL<>.w_MAGCOL
            .w_ESEINV = IIF(.w_ESEINV>.w_ESERF OR .w_ESEINV<.w_ESERC OR EMPTY(.w_CODMAG),SPACE(4),.w_ESEINV)
          .link_1_14('Full')
        endif
        if .o_MAGCOL<>.w_MAGCOL.or. .o_INIZIO<>.w_INIZIO.or. .o_FINE<>.w_FINE.or. .o_CODMAG<>.w_CODMAG
            .w_INV = IIF(.w_MAGCOL='N' OR .w_ESEINV>.w_ESERF OR .w_ESEINV<.w_ESERC OR EMPTY(.w_CODMAG),SPACE(6),.w_INV)
          .link_1_15('Full')
        endif
        .DoRTCalc(16,17,.t.)
        if .o_CODART<>.w_CODART
            .w_PRKEYVAR = space(20)
        endif
        if .o_PRKEYVAR<>.w_PRKEYVAR.or. .o_CODART<>.w_CODART
            .w_CODVAR = .w_PRKEYVAR
          .link_1_19('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .DoRTCalc(20,24,.t.)
        if .o_CODART<>.w_CODART
            .w_PRCODART = .w_CODART
        endif
        .DoRTCalc(26,30,.t.)
        if .o_DATASTAM<>.w_DATASTAM
            .w_OBTEST = .w_DATASTAM
        endif
        .DoRTCalc(32,32,.t.)
            .w_ELENMAG = IIF(.w_MAGCOL='N',.w_CODMAG,.w_ELENMAG)
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(34,35,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oMAGCOL_1_9.enabled = this.oPgFrm.Page1.oPag.oMAGCOL_1_9.mCond()
    this.oPgFrm.Page1.oPag.oESEINV_1_14.enabled = this.oPgFrm.Page1.oPag.oESEINV_1_14.mCond()
    this.oPgFrm.Page1.oPag.oINV_1_15.enabled = this.oPgFrm.Page1.oPag.oINV_1_15.mCond()
    this.oPgFrm.Page1.oPag.oTIPOCOST_1_16.enabled = this.oPgFrm.Page1.oPag.oTIPOCOST_1_16.mCond()
    this.oPgFrm.Page1.oPag.oCODVAR_1_19.enabled = this.oPgFrm.Page1.oPag.oCODVAR_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCODART_1_17.visible=!this.oPgFrm.Page1.oPag.oCODART_1_17.mHide()
    this.oPgFrm.Page1.oPag.oCODVAR_1_19.visible=!this.oPgFrm.Page1.oPag.oCODVAR_1_19.mHide()
    this.oPgFrm.Page1.oPag.oDESVAR_1_33.visible=!this.oPgFrm.Page1.oPag.oDESVAR_1_33.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_41.visible=!this.oPgFrm.Page1.oPag.oStr_1_41.mHide()
    this.oPgFrm.Page1.oPag.oCODART_1_52.visible=!this.oPgFrm.Page1.oPag.oCODART_1_52.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_20.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_49.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODMAG
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_CODMAG))
          select MGCODMAG,MGDESMAG,MGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStr(trim(this.w_CODMAG)+"%");

            select MGCODMAG,MGDESMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCODMAG_1_8'),i_cWhere,'',"Elenco magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAG)
            select MGCODMAG,MGDESMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.MGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAG = space(5)
      endif
      this.w_DESMAG = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Magazzino inesistente oppure obsoleto")
        endif
        this.w_CODMAG = space(5)
        this.w_DESMAG = space(30)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFAM
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFAM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_CODFAM)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_CODFAM))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFAM)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODFAM) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oCODFAM_1_10'),i_cWhere,'',"Elenco famiglie",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFAM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_CODFAM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_CODFAM)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFAM = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAM = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODFAM = space(5)
      endif
      this.w_DESFAM = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFAM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODGRU
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_CODGRU)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_CODGRU))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODGRU)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODGRU) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oCODGRU_1_11'),i_cWhere,'',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_CODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_CODGRU)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODGRU = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRU = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODGRU = space(5)
      endif
      this.w_DESGRU = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCAT
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CODCAT)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CODCAT))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCAT)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCAT) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCODCAT_1_12'),i_cWhere,'',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CODCAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CODCAT)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCAT = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCAT = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODCAT = space(5)
      endif
      this.w_DESCAT = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAR
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_lTable = "MARCHI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2], .t., this.MARCHI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MARCHI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MACODICE like "+cp_ToStrODBC(trim(this.w_CODMAR)+"%");

          i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MACODICE',trim(this.w_CODMAR))
          select MACODICE,MADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAR)==trim(_Link_.MACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODMAR) and !this.bDontReportError
            deferred_cp_zoom('MARCHI','*','MACODICE',cp_AbsName(oSource.parent,'oCODMAR_1_13'),i_cWhere,'',"Elenco marchi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',oSource.xKey(1))
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(this.w_CODMAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_CODMAR)
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAR = NVL(_Link_.MACODICE,space(5))
      this.w_DESMAR = NVL(_Link_.MADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAR = space(5)
      endif
      this.w_DESMAR = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)
      cp_ShowWarn(i_cKey,this.MARCHI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ESEINV
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESEINV) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_KES',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_ESEINV)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_AZIENDA;
                     ,'ESCODESE',trim(this.w_ESEINV))
          select ESCODAZI,ESCODESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ESEINV)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ESEINV) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oESEINV_1_14'),i_cWhere,'GSAR_KES',"Esercizi",'GSMAISNM.ESERCIZI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_AZIENDA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Esercizio non compreso nell'intervallo di date")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESEINV)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESEINV);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_AZIENDA;
                       ,'ESCODESE',this.w_ESEINV)
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESEINV = NVL(_Link_.ESCODESE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_ESEINV = space(4)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ESEINV<=.w_ESERF and .w_ESEINV>=.w_ESERC OR empty(.w_ESEINV)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Esercizio non compreso nell'intervallo di date")
        endif
        this.w_ESEINV = space(4)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESEINV Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=INV
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.INVENTAR_IDX,3]
    i_lTable = "INVENTAR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2], .t., this.INVENTAR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_INV) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsma_ain',True,'INVENTAR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" INNUMINV like "+cp_ToStrODBC(trim(this.w_INV)+"%");
                   +" and INCODESE="+cp_ToStrODBC(this.w_ESEINV);

          i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INCODMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by INCODESE,INNUMINV","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'INCODESE',this.w_ESEINV;
                     ,'INNUMINV',trim(this.w_INV))
          select INCODESE,INNUMINV,INCODMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by INCODESE,INNUMINV into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_INV)==trim(_Link_.INNUMINV) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_INV) and !this.bDontReportError
            deferred_cp_zoom('INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(oSource.parent,'oINV_1_15'),i_cWhere,'gsma_ain',"Inventario",'GSMA8SNM.INVENTAR_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ESEINV<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INCODMAG";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select INCODESE,INNUMINV,INCODMAG;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Inventario selezionato con magazzino differente da quello indicato")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INCODMAG";
                     +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(oSource.xKey(2));
                     +" and INCODESE="+cp_ToStrODBC(this.w_ESEINV);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',oSource.xKey(1);
                       ,'INNUMINV',oSource.xKey(2))
            select INCODESE,INNUMINV,INCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_INV)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INCODMAG";
                   +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(this.w_INV);
                   +" and INCODESE="+cp_ToStrODBC(this.w_ESEINV);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',this.w_ESEINV;
                       ,'INNUMINV',this.w_INV)
            select INCODESE,INNUMINV,INCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_INV = NVL(_Link_.INNUMINV,space(6))
      this.w_INVMAG = NVL(_Link_.INCODMAG,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_INV = space(6)
      endif
      this.w_INVMAG = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])+'\'+cp_ToStr(_Link_.INCODESE,1)+'\'+cp_ToStr(_Link_.INNUMINV,1)
      cp_ShowWarn(i_cKey,this.INVENTAR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_INV Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODART
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODART)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARCODVAR,ARDTOBSO,ARTIPART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODART))
          select ARCODART,ARDESART,ARCODVAR,ARDTOBSO,ARTIPART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODART)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_CODART)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARCODVAR,ARDTOBSO,ARTIPART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_CODART)+"%");

            select ARCODART,ARDESART,ARCODVAR,ARDTOBSO,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODART) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODART_1_17'),i_cWhere,'',"Articoli",'GSMA0ADI.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARCODVAR,ARDTOBSO,ARTIPART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARCODVAR,ARDTOBSO,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARCODVAR,ARDTOBSO,ARTIPART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODART)
            select ARCODART,ARDESART,ARCODVAR,ARDTOBSO,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODART = NVL(_Link_.ARCODART,space(20))
      this.w_DESINI = NVL(_Link_.ARDESART,space(40))
      this.w_TIPVAR = NVL(_Link_.ARCODVAR,space(5))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
      this.w_TIPO = NVL(_Link_.ARTIPART,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_CODART = space(20)
      endif
      this.w_DESINI = space(40)
      this.w_TIPVAR = space(5)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_TIPO = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_TIPO$'PF-SE-MP-MC-MA-IM-PH'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice di tipo non articolo oppure obsoleto o inesistente")
        endif
        this.w_CODART = space(20)
        this.w_DESINI = space(40)
        this.w_TIPVAR = space(5)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_TIPO = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODVAR
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DET_VARI_IDX,3]
    i_lTable = "DET_VARI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DET_VARI_IDX,2], .t., this.DET_VARI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DET_VARI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DET_VARI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DVCODICE like "+cp_ToStrODBC(trim(this.w_CODVAR)+"%");
                   +" and DVCODVAR="+cp_ToStrODBC(this.w_TIPVAR);

          i_ret=cp_SQL(i_nConn,"select DVCODVAR,DVCODICE,DVDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DVCODVAR,DVCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DVCODVAR',this.w_TIPVAR;
                     ,'DVCODICE',trim(this.w_CODVAR))
          select DVCODVAR,DVCODICE,DVDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DVCODVAR,DVCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODVAR)==trim(_Link_.DVCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODVAR) and !this.bDontReportError
            deferred_cp_zoom('DET_VARI','*','DVCODVAR,DVCODICE',cp_AbsName(oSource.parent,'oCODVAR_1_19'),i_cWhere,'',"Varianti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPVAR<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DVCODVAR,DVCODICE,DVDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DVCODVAR,DVCODICE,DVDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DVCODVAR,DVCODICE,DVDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where DVCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and DVCODVAR="+cp_ToStrODBC(this.w_TIPVAR);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DVCODVAR',oSource.xKey(1);
                       ,'DVCODICE',oSource.xKey(2))
            select DVCODVAR,DVCODICE,DVDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DVCODVAR,DVCODICE,DVDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where DVCODICE="+cp_ToStrODBC(this.w_CODVAR);
                   +" and DVCODVAR="+cp_ToStrODBC(this.w_TIPVAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DVCODVAR',this.w_TIPVAR;
                       ,'DVCODICE',this.w_CODVAR)
            select DVCODVAR,DVCODICE,DVDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVAR = NVL(_Link_.DVCODICE,space(20))
      this.w_DESVAR = NVL(_Link_.DVDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODVAR = space(20)
      endif
      this.w_DESVAR = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DET_VARI_IDX,2])+'\'+cp_ToStr(_Link_.DVCODVAR,1)+'\'+cp_ToStr(_Link_.DVCODICE,1)
      cp_ShowWarn(i_cKey,this.DET_VARI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODART
  func Link_1_52(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODART)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARTIPART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODART))
          select ARCODART,ARDESART,ARDTOBSO,ARTIPART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODART)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_CODART)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARTIPART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_CODART)+"%");

            select ARCODART,ARDESART,ARDTOBSO,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODART) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODART_1_52'),i_cWhere,'',"Articoli",'GSMA0ADI.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARTIPART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARTIPART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODART)
            select ARCODART,ARDESART,ARDTOBSO,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODART = NVL(_Link_.ARCODART,space(20))
      this.w_DESINI = NVL(_Link_.ARDESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
      this.w_TIPO = NVL(_Link_.ARTIPART,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_CODART = space(20)
      endif
      this.w_DESINI = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_TIPO = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_TIPO$'PF-SE-MP-MC-MA-IM-PH'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice di tipo non articolo oppure obsoleto o inesistente")
        endif
        this.w_CODART = space(20)
        this.w_DESINI = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_TIPO = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oINIZIO_1_2.value==this.w_INIZIO)
      this.oPgFrm.Page1.oPag.oINIZIO_1_2.value=this.w_INIZIO
    endif
    if not(this.oPgFrm.Page1.oPag.oFINE_1_3.value==this.w_FINE)
      this.oPgFrm.Page1.oPag.oFINE_1_3.value=this.w_FINE
    endif
    if not(this.oPgFrm.Page1.oPag.oDATASTAM_1_6.value==this.w_DATASTAM)
      this.oPgFrm.Page1.oPag.oDATASTAM_1_6.value=this.w_DATASTAM
    endif
    if not(this.oPgFrm.Page1.oPag.oCODMAG_1_8.value==this.w_CODMAG)
      this.oPgFrm.Page1.oPag.oCODMAG_1_8.value=this.w_CODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oMAGCOL_1_9.RadioValue()==this.w_MAGCOL)
      this.oPgFrm.Page1.oPag.oMAGCOL_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFAM_1_10.value==this.w_CODFAM)
      this.oPgFrm.Page1.oPag.oCODFAM_1_10.value=this.w_CODFAM
    endif
    if not(this.oPgFrm.Page1.oPag.oCODGRU_1_11.value==this.w_CODGRU)
      this.oPgFrm.Page1.oPag.oCODGRU_1_11.value=this.w_CODGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCAT_1_12.value==this.w_CODCAT)
      this.oPgFrm.Page1.oPag.oCODCAT_1_12.value=this.w_CODCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oCODMAR_1_13.value==this.w_CODMAR)
      this.oPgFrm.Page1.oPag.oCODMAR_1_13.value=this.w_CODMAR
    endif
    if not(this.oPgFrm.Page1.oPag.oESEINV_1_14.value==this.w_ESEINV)
      this.oPgFrm.Page1.oPag.oESEINV_1_14.value=this.w_ESEINV
    endif
    if not(this.oPgFrm.Page1.oPag.oINV_1_15.value==this.w_INV)
      this.oPgFrm.Page1.oPag.oINV_1_15.value=this.w_INV
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOCOST_1_16.RadioValue()==this.w_TIPOCOST)
      this.oPgFrm.Page1.oPag.oTIPOCOST_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODART_1_17.value==this.w_CODART)
      this.oPgFrm.Page1.oPag.oCODART_1_17.value=this.w_CODART
    endif
    if not(this.oPgFrm.Page1.oPag.oCODVAR_1_19.value==this.w_CODVAR)
      this.oPgFrm.Page1.oPag.oCODVAR_1_19.value=this.w_CODVAR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAG_1_23.value==this.w_DESMAG)
      this.oPgFrm.Page1.oPag.oDESMAG_1_23.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAM_1_24.value==this.w_DESFAM)
      this.oPgFrm.Page1.oPag.oDESFAM_1_24.value=this.w_DESFAM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRU_1_25.value==this.w_DESGRU)
      this.oPgFrm.Page1.oPag.oDESGRU_1_25.value=this.w_DESGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAT_1_26.value==this.w_DESCAT)
      this.oPgFrm.Page1.oPag.oDESCAT_1_26.value=this.w_DESCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAR_1_27.value==this.w_DESMAR)
      this.oPgFrm.Page1.oPag.oDESMAR_1_27.value=this.w_DESMAR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINI_1_31.value==this.w_DESINI)
      this.oPgFrm.Page1.oPag.oDESINI_1_31.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESVAR_1_33.value==this.w_DESVAR)
      this.oPgFrm.Page1.oPag.oDESVAR_1_33.value=this.w_DESVAR
    endif
    if not(this.oPgFrm.Page1.oPag.oCODART_1_52.value==this.w_CODART)
      this.oPgFrm.Page1.oPag.oCODART_1_52.value=this.w_CODART
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_INIZIO)) or not(empty(.w_FINE) OR  (.w_INIZIO<=.w_FINE)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oINIZIO_1_2.SetFocus()
            i_bnoObbl = !empty(.w_INIZIO)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   not(empty(.w_FINE) OR  (.w_INIZIO<=.w_FINE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFINE_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   (empty(.w_DATASTAM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATASTAM_1_6.SetFocus()
            i_bnoObbl = !empty(.w_DATASTAM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_CODMAG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODMAG_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Magazzino inesistente oppure obsoleto")
          case   not(.w_ESEINV<=.w_ESERF and .w_ESEINV>=.w_ESERC OR empty(.w_ESEINV))  and (not empty(.w_CODMAG))  and not(empty(.w_ESEINV))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oESEINV_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Esercizio non compreso nell'intervallo di date")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_TIPO$'PF-SE-MP-MC-MA-IM-PH')  and not(upper(g_APPLICATION) <> "AD HOC ENTERPRISE")  and not(empty(.w_CODART))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODART_1_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice di tipo non articolo oppure obsoleto o inesistente")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_TIPO$'PF-SE-MP-MC-MA-IM-PH')  and not(upper(g_APPLICATION) = "AD HOC ENTERPRISE")  and not(empty(.w_CODART))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODART_1_52.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice di tipo non articolo oppure obsoleto o inesistente")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_INIZIO = this.w_INIZIO
    this.o_FINE = this.w_FINE
    this.o_DATASTAM = this.w_DATASTAM
    this.o_CODMAG = this.w_CODMAG
    this.o_MAGCOL = this.w_MAGCOL
    this.o_CODART = this.w_CODART
    this.o_PRKEYVAR = this.w_PRKEYVAR
    return

enddefine

* --- Define pages as container
define class tgsma_snmPag1 as StdContainer
  Width  = 589
  height = 370
  stdWidth  = 589
  stdheight = 370
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oINIZIO_1_2 as StdField with uid="QLMNQDIWET",rtseq=2,rtrep=.f.,;
    cFormVar = "w_INIZIO", cQueryName = "INIZIO",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data dalla quale l'articolo non deve essere movimentato",;
    HelpContextID = 108763258,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=142, Top=6

  func oINIZIO_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_FINE) OR  (.w_INIZIO<=.w_FINE))
    endwith
    return bRes
  endfunc

  add object oFINE_1_3 as StdField with uid="HJJPGTVMHO",rtseq=3,rtrep=.f.,;
    cFormVar = "w_FINE", cQueryName = "FINE",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data fino alla quale l'articolo non deve essere movimentato",;
    HelpContextID = 98546262,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=142, Top=30

  func oFINE_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_FINE) OR  (.w_INIZIO<=.w_FINE))
    endwith
    return bRes
  endfunc

  add object oDATASTAM_1_6 as StdField with uid="WYUPQMPQFS",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DATASTAM", cQueryName = "DATASTAM",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di stampa",;
    HelpContextID = 15988093,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=415, Top=6

  add object oCODMAG_1_8 as StdField with uid="BLMOAXECRF",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CODMAG", cQueryName = "CODMAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Magazzino inesistente oppure obsoleto",;
    ToolTipText = "Codice del magazzino da considerare",;
    HelpContextID = 252241882,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=142, Top=55, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMAG"

  func oCODMAG_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMAG_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAG_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCODMAG_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco magazzini",'',this.parent.oContained
  endproc

  add object oMAGCOL_1_9 as StdCheck with uid="WUXZPPXOZB",rtseq=9,rtrep=.f.,left=142, top=79, caption="Magazzini collegati",;
    ToolTipText = "Se attivato considera i magazzini collegati",;
    HelpContextID = 154322234,;
    cFormVar="w_MAGCOL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMAGCOL_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oMAGCOL_1_9.GetRadio()
    this.Parent.oContained.w_MAGCOL = this.RadioValue()
    return .t.
  endfunc

  func oMAGCOL_1_9.SetRadio()
    this.Parent.oContained.w_MAGCOL=trim(this.Parent.oContained.w_MAGCOL)
    this.value = ;
      iif(this.Parent.oContained.w_MAGCOL=='S',1,;
      0)
  endfunc

  func oMAGCOL_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.not. empty( .w_CODMAG ))
    endwith
   endif
  endfunc

  add object oCODFAM_1_10 as StdField with uid="GFWOCSHNKC",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CODFAM", cQueryName = "CODFAM",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice della famiglia da considerare",;
    HelpContextID = 152037338,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=142, Top=107, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_CODFAM"

  func oCODFAM_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFAM_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFAM_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oCODFAM_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco famiglie",'',this.parent.oContained
  endproc

  add object oCODGRU_1_11 as StdField with uid="YZBUCKEBKF",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CODGRU", cQueryName = "CODGRU",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice del gruppo merceologico da considerare",;
    HelpContextID = 268363738,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=142, Top=132, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_CODGRU"

  func oCODGRU_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODGRU_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODGRU_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oCODGRU_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi merceologici",'',this.parent.oContained
  endproc

  add object oCODCAT_1_12 as StdField with uid="QVUKAEESTC",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CODCAT", cQueryName = "CODCAT",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice della categoria omogenea da considerare",;
    HelpContextID = 34793434,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=142, Top=157, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_CODCAT"

  func oCODCAT_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCAT_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCAT_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCODCAT_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie omogenee",'',this.parent.oContained
  endproc

  add object oCODMAR_1_13 as StdField with uid="NTDHWJEJGR",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CODMAR", cQueryName = "CODMAR",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice della marca da considerare",;
    HelpContextID = 67692506,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=142, Top=182, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MARCHI", oKey_1_1="MACODICE", oKey_1_2="this.w_CODMAR"

  func oCODMAR_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMAR_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAR_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MARCHI','*','MACODICE',cp_AbsName(this.parent,'oCODMAR_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco marchi",'',this.parent.oContained
  endproc

  add object oESEINV_1_14 as StdField with uid="FTEGXZVCTW",rtseq=14,rtrep=.f.,;
    cFormVar = "w_ESEINV", cQueryName = "ESEINV",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Esercizio non compreso nell'intervallo di date",;
    ToolTipText = "Esercizio inventario",;
    HelpContextID = 255644602,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=142, Top=209, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", cZoomOnZoom="GSAR_KES", oKey_1_1="ESCODAZI", oKey_1_2="this.w_AZIENDA", oKey_2_1="ESCODESE", oKey_2_2="this.w_ESEINV"

  func oESEINV_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_CODMAG))
    endwith
   endif
  endfunc

  func oESEINV_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
      if .not. empty(.w_INV)
        bRes2=.link_1_15('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oESEINV_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oESEINV_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_AZIENDA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_AZIENDA)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oESEINV_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_KES',"Esercizi",'GSMAISNM.ESERCIZI_VZM',this.parent.oContained
  endproc
  proc oESEINV_1_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_KES()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ESCODAZI=w_AZIENDA
     i_obj.w_ESCODESE=this.parent.oContained.w_ESEINV
     i_obj.ecpSave()
  endproc

  add object oINV_1_15 as StdField with uid="IPGSQYYEXB",rtseq=15,rtrep=.f.,;
    cFormVar = "w_INV", cQueryName = "INV",nZero=6,;
    bObbl = .f. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    sErrorMsg = "Inventario selezionato con magazzino differente da quello indicato",;
    ToolTipText = "Numero inventario",;
    HelpContextID = 94058374,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=208, Top=209, InputMask=replicate('X',6), bHasZoom = .t. , cLinkFile="INVENTAR", cZoomOnZoom="gsma_ain", oKey_1_1="INCODESE", oKey_1_2="this.w_ESEINV", oKey_2_1="INNUMINV", oKey_2_2="this.w_INV"

  func oINV_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_CODMAG) and not empty(.w_ESEINV))
    endwith
   endif
  endfunc

  func oINV_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oINV_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oINV_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.INVENTAR_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStrODBC(this.Parent.oContained.w_ESEINV)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStr(this.Parent.oContained.w_ESEINV)
    endif
    do cp_zoom with 'INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(this.parent,'oINV_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'gsma_ain',"Inventario",'GSMA8SNM.INVENTAR_VZM',this.parent.oContained
  endproc
  proc oINV_1_15.mZoomOnZoom
    local i_obj
    i_obj=gsma_ain()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.INCODESE=w_ESEINV
     i_obj.w_INNUMINV=this.parent.oContained.w_INV
     i_obj.ecpSave()
  endproc


  add object oTIPOCOST_1_16 as StdCombo with uid="YOZJXBRYGO",rtseq=16,rtrep=.f.,left=398,top=209,width=184,height=21;
    , ToolTipText = "Tipo di costo da visualizzare";
    , HelpContextID = 115747958;
    , cFormVar="w_TIPOCOST",RowSource=""+"Costo standard,"+"Costo ultimo,"+"Costo medio ponderato periodo,"+"Costo medio ponderato eserc.", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOCOST_1_16.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'U',;
    iif(this.value =3,'P',;
    iif(this.value =4,'A',;
    space(1))))))
  endfunc
  func oTIPOCOST_1_16.GetRadio()
    this.Parent.oContained.w_TIPOCOST = this.RadioValue()
    return .t.
  endfunc

  func oTIPOCOST_1_16.SetRadio()
    this.Parent.oContained.w_TIPOCOST=trim(this.Parent.oContained.w_TIPOCOST)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOCOST=='S',1,;
      iif(this.Parent.oContained.w_TIPOCOST=='U',2,;
      iif(this.Parent.oContained.w_TIPOCOST=='P',3,;
      iif(this.Parent.oContained.w_TIPOCOST=='A',4,;
      0))))
  endfunc

  func oTIPOCOST_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_INV))
    endwith
   endif
  endfunc

  add object oCODART_1_17 as StdField with uid="GVUCEMWEDV",rtseq=17,rtrep=.f.,;
    cFormVar = "w_CODART", cQueryName = "CODART",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice di tipo non articolo oppure obsoleto o inesistente",;
    ToolTipText = "Codice dell'articolo da considerare",;
    HelpContextID = 17098714,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=142, Top=237, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_CODART"

  func oCODART_1_17.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "AD HOC ENTERPRISE")
    endwith
  endfunc

  func oCODART_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODART_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODART_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODART_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Articoli",'GSMA0ADI.ART_ICOL_VZM',this.parent.oContained
  endproc

  add object oCODVAR_1_19 as StdField with uid="HFOIBAAYTQ",rtseq=19,rtrep=.f.,;
    cFormVar = "w_CODVAR", cQueryName = "CODVAR",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Variante selezionata",;
    HelpContextID = 67102682,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=142, Top=266, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="DET_VARI", oKey_1_1="DVCODVAR", oKey_1_2="this.w_TIPVAR", oKey_2_1="DVCODICE", oKey_2_2="this.w_CODVAR"

  func oCODVAR_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.not. empty(.w_TIPVAR))
    endwith
   endif
  endfunc

  func oCODVAR_1_19.mHide()
    with this.Parent.oContained
      return (empty(.w_TIPVAR))
    endwith
  endfunc

  func oCODVAR_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODVAR_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODVAR_1_19.mZoom
      with this.Parent.oContained
        do GSMA_BMP with this.Parent.oContained
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc


  add object oObj_1_20 as cp_outputCombo with uid="ZLERXIVPWT",left=142, top=295, width=380,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 265188378


  add object oBtn_1_21 as StdButton with uid="XHZFTCBBPY",left=481, top=323, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la stampa";
    , HelpContextID = 83804650;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      with this.Parent.oContained
        .RepoPrint()
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_21.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_22 as StdButton with uid="YJFSXJSPCL",left=537, top=323, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 83804650;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESMAG_1_23 as StdField with uid="XAOXDQWMUO",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 252182986,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=208, Top=55, InputMask=replicate('X',30)

  add object oDESFAM_1_24 as StdField with uid="UCKKOEWHLZ",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESFAM", cQueryName = "DESFAM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 151978442,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=208, Top=107, InputMask=replicate('X',35)

  add object oDESGRU_1_25 as StdField with uid="WCITMVMCMI",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DESGRU", cQueryName = "DESGRU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 268304842,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=208, Top=132, InputMask=replicate('X',35)

  add object oDESCAT_1_26 as StdField with uid="YPEYKJDGED",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESCAT", cQueryName = "DESCAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 34734538,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=208, Top=157, InputMask=replicate('X',35)

  add object oDESMAR_1_27 as StdField with uid="VTTRIGGUXG",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESMAR", cQueryName = "DESMAR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 67633610,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=208, Top=182, InputMask=replicate('X',35)

  add object oDESINI_1_31 as StdField with uid="YCHWRKAFMY",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 205259210,;
   bGlobalFont=.t.,;
    Height=21, Width=269, Left=311, Top=237, InputMask=replicate('X',40)

  add object oDESVAR_1_33 as StdField with uid="OMCDUBDAGJ",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESVAR", cQueryName = "DESVAR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 67043786,;
   bGlobalFont=.t.,;
    Height=21, Width=269, Left=311, Top=266, InputMask=replicate('X',40)

  func oDESVAR_1_33.mHide()
    with this.Parent.oContained
      return (empty(.w_TIPVAR))
    endwith
  endfunc


  add object oObj_1_49 as cp_runprogram with uid="OMPTJHKGPV",left=3, top=404, width=219,height=19,;
    caption='GSMA_BNM',;
   bGlobalFont=.t.,;
    prg="GSMA_BNM",;
    cEvent = "w_MAGCOL Changed",;
    nPag=1;
    , HelpContextID = 231451827

  add object oCODART_1_52 as StdField with uid="GXNWGDIJEQ",rtseq=35,rtrep=.f.,;
    cFormVar = "w_CODART", cQueryName = "CODART",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice di tipo non articolo oppure obsoleto o inesistente",;
    ToolTipText = "Codice dell'articolo da considerare",;
    HelpContextID = 17098714,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=142, Top=237, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_CODART"

  func oCODART_1_52.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) = "AD HOC ENTERPRISE")
    endwith
  endfunc

  func oCODART_1_52.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_52('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODART_1_52.ecpDrop(oSource)
    this.Parent.oContained.link_1_52('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODART_1_52.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODART_1_52'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Articoli",'GSMA0ADI.ART_ICOL_VZM',this.parent.oContained
  endproc

  add object oStr_1_28 as StdString with uid="GJJNYJRYEP",Visible=.t., Left=94, Top=237,;
    Alignment=1, Width=44, Height=15,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="LTOLUXYVFT",Visible=.t., Left=78, Top=55,;
    Alignment=1, Width=60, Height=15,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="CHWOGZHYSW",Visible=.t., Left=87, Top=107,;
    Alignment=1, Width=51, Height=15,;
    Caption="Famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="JAOPVMCCTC",Visible=.t., Left=29, Top=132,;
    Alignment=1, Width=109, Height=15,;
    Caption="Gr. merceologico:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="XFNNDZNBOC",Visible=.t., Left=323, Top=10,;
    Alignment=1, Width=88, Height=15,;
    Caption="Data di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="PKNKCNHVSH",Visible=.t., Left=28, Top=157,;
    Alignment=1, Width=110, Height=15,;
    Caption="Cat. omogenea:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="QPMQZFFAZW",Visible=.t., Left=92, Top=182,;
    Alignment=1, Width=46, Height=15,;
    Caption="Marca:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="CVEUIJMZOB",Visible=.t., Left=85, Top=266,;
    Alignment=1, Width=53, Height=15,;
    Caption="Variante:"  ;
  , bGlobalFont=.t.

  func oStr_1_41.mHide()
    with this.Parent.oContained
      return (empty(.w_TIPVAR))
    endwith
  endfunc

  add object oStr_1_44 as StdString with uid="KFXPFHVEQY",Visible=.t., Left=3, Top=10,;
    Alignment=1, Width=135, Height=18,;
    Caption="Non movimentati da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="JQWCOZBNRS",Visible=.t., Left=41, Top=295,;
    Alignment=1, Width=97, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="VVZZZZNPOX",Visible=.t., Left=34, Top=211,;
    Alignment=1, Width=104, Height=18,;
    Caption="Inventario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="TKLOKCYPAD",Visible=.t., Left=278, Top=211,;
    Alignment=1, Width=117, Height=18,;
    Caption="Visualizza costo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="QLRJUAMZIY",Visible=.t., Left=105, Top=30,;
    Alignment=1, Width=33, Height=18,;
    Caption="a:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_snm','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
