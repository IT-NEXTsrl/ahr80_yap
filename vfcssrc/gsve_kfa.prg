* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_kfa                                                        *
*              Dati contabili IVA                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_109]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-10                                                      *
* Last revis.: 2010-06-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsve_kfa",oParentObject))

* --- Class definition
define class tgsve_kfa as StdForm
  Top    = 82
  Left   = 21

  * --- Standard Properties
  Width  = 518
  Height = 411
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-06-25"
  HelpContextID=219532695
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=72

  * --- Constant Properties
  _IDX = 0
  VOCIIVA_IDX = 0
  TIPCODIV_IDX = 0
  DIC_INTE_IDX = 0
  cPrg = "gsve_kfa"
  cComment = "Dati contabili IVA"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_OBTEST = ctod('  /  /  ')
  w_MVAFLOM1 = space(1)
  w_MVAFLOM2 = space(1)
  w_MVAFLOM3 = space(1)
  w_MVAFLOM4 = space(1)
  w_MVCLADOC = space(2)
  w_MVAFLOM5 = space(1)
  w_MV__MESE = 0
  w_MV__ANNO = 0
  w_MVAFLOM6 = space(1)
  w_MVIVAINC = space(5)
  w_MVIVATRA = space(5)
  w_MVIVAIMB = space(5)
  w_MVIVABOL = space(5)
  w_MVTIPOPE = space(10)
  w_MVACIVA1 = space(5)
  w_MVAIMPN1 = 0
  w_PERIV1 = 0
  w_MVAIMPS1 = 0
  w_MVACIVA2 = space(5)
  w_MVAIMPN2 = 0
  w_PERIV2 = 0
  w_MVAIMPS2 = 0
  w_MVACIVA3 = space(5)
  w_MVAIMPN3 = 0
  w_PERIV3 = 0
  w_MVAIMPS3 = 0
  w_MVACIVA4 = space(5)
  w_MVAIMPN4 = 0
  w_PERIV4 = 0
  w_MVAIMPS4 = 0
  w_MVACIVA5 = space(5)
  w_MVAIMPN5 = 0
  w_PERIV5 = 0
  w_MVAIMPS5 = 0
  w_MVACIVA6 = space(5)
  w_MVAIMPN6 = 0
  w_PERIV6 = 0
  w_MVAIMPS6 = 0
  w_MVAIMPS = 0
  w_MVAIMPN = 0
  w_FLOM1 = space(8)
  w_FLOM2 = space(8)
  w_FLOM3 = space(8)
  w_FLOM4 = space(8)
  w_FLOM5 = space(8)
  w_FLOM6 = space(8)
  w_PEIINC = 0
  w_BOLINC = space(1)
  w_DTOBSO = ctod('  /  /  ')
  w_PEIIMB = 0
  w_BOLIMB = space(1)
  w_PEITRA = 0
  w_BOLTRA = space(1)
  w_BOLBOL = space(1)
  w_AIMPINI = 0
  w_PERIVA = 0
  w_TIDESCRI = space(30)
  w_TIDTOBSO = ctod('  /  /  ')
  w_MVCATOPE = space(2)
  w_MVIVACAU = space(5)
  w_PEICAU = 0
  w_BOLCAU = space(1)
  w_REVCAU = space(1)
  w_MVDATDOC = ctod('  /  /  ')
  w_MVTIPCON = space(1)
  w_MVCODCON = space(10)
  w_MVCODIVE = space(5)
  w_DESAPP = space(35)
  w_BOLIVE = space(1)
  w_PERIVE = 0
  w_DTOBSO = ctod('  /  /  ')
  w_Imballo = .NULL.
  w_trasporto = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_kfaPag1","gsve_kfa",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMV__MESE_1_8
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_Imballo = this.oPgFrm.Pages(1).oPag.Imballo
    this.w_trasporto = this.oPgFrm.Pages(1).oPag.trasporto
    DoDefault()
    proc Destroy()
      this.w_Imballo = .NULL.
      this.w_trasporto = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='VOCIIVA'
    this.cWorkTables[2]='TIPCODIV'
    this.cWorkTables[3]='DIC_INTE'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_OBTEST=ctod("  /  /  ")
      .w_MVAFLOM1=space(1)
      .w_MVAFLOM2=space(1)
      .w_MVAFLOM3=space(1)
      .w_MVAFLOM4=space(1)
      .w_MVCLADOC=space(2)
      .w_MVAFLOM5=space(1)
      .w_MV__MESE=0
      .w_MV__ANNO=0
      .w_MVAFLOM6=space(1)
      .w_MVIVAINC=space(5)
      .w_MVIVATRA=space(5)
      .w_MVIVAIMB=space(5)
      .w_MVIVABOL=space(5)
      .w_MVTIPOPE=space(10)
      .w_MVACIVA1=space(5)
      .w_MVAIMPN1=0
      .w_PERIV1=0
      .w_MVAIMPS1=0
      .w_MVACIVA2=space(5)
      .w_MVAIMPN2=0
      .w_PERIV2=0
      .w_MVAIMPS2=0
      .w_MVACIVA3=space(5)
      .w_MVAIMPN3=0
      .w_PERIV3=0
      .w_MVAIMPS3=0
      .w_MVACIVA4=space(5)
      .w_MVAIMPN4=0
      .w_PERIV4=0
      .w_MVAIMPS4=0
      .w_MVACIVA5=space(5)
      .w_MVAIMPN5=0
      .w_PERIV5=0
      .w_MVAIMPS5=0
      .w_MVACIVA6=space(5)
      .w_MVAIMPN6=0
      .w_PERIV6=0
      .w_MVAIMPS6=0
      .w_MVAIMPS=0
      .w_MVAIMPN=0
      .w_FLOM1=space(8)
      .w_FLOM2=space(8)
      .w_FLOM3=space(8)
      .w_FLOM4=space(8)
      .w_FLOM5=space(8)
      .w_FLOM6=space(8)
      .w_PEIINC=0
      .w_BOLINC=space(1)
      .w_DTOBSO=ctod("  /  /  ")
      .w_PEIIMB=0
      .w_BOLIMB=space(1)
      .w_PEITRA=0
      .w_BOLTRA=space(1)
      .w_BOLBOL=space(1)
      .w_AIMPINI=0
      .w_PERIVA=0
      .w_TIDESCRI=space(30)
      .w_TIDTOBSO=ctod("  /  /  ")
      .w_MVCATOPE=space(2)
      .w_MVIVACAU=space(5)
      .w_PEICAU=0
      .w_BOLCAU=space(1)
      .w_REVCAU=space(1)
      .w_MVDATDOC=ctod("  /  /  ")
      .w_MVTIPCON=space(1)
      .w_MVCODCON=space(10)
      .w_MVCODIVE=space(5)
      .w_DESAPP=space(35)
      .w_BOLIVE=space(1)
      .w_PERIVE=0
      .w_DTOBSO=ctod("  /  /  ")
      .w_OBTEST=oParentObject.w_OBTEST
      .w_MVAFLOM1=oParentObject.w_MVAFLOM1
      .w_MVAFLOM2=oParentObject.w_MVAFLOM2
      .w_MVAFLOM3=oParentObject.w_MVAFLOM3
      .w_MVAFLOM4=oParentObject.w_MVAFLOM4
      .w_MVCLADOC=oParentObject.w_MVCLADOC
      .w_MVAFLOM5=oParentObject.w_MVAFLOM5
      .w_MV__MESE=oParentObject.w_MV__MESE
      .w_MV__ANNO=oParentObject.w_MV__ANNO
      .w_MVAFLOM6=oParentObject.w_MVAFLOM6
      .w_MVIVAINC=oParentObject.w_MVIVAINC
      .w_MVIVATRA=oParentObject.w_MVIVATRA
      .w_MVIVAIMB=oParentObject.w_MVIVAIMB
      .w_MVIVABOL=oParentObject.w_MVIVABOL
      .w_MVTIPOPE=oParentObject.w_MVTIPOPE
      .w_MVACIVA1=oParentObject.w_MVACIVA1
      .w_MVAIMPN1=oParentObject.w_MVAIMPN1
      .w_MVAIMPS1=oParentObject.w_MVAIMPS1
      .w_MVACIVA2=oParentObject.w_MVACIVA2
      .w_MVAIMPN2=oParentObject.w_MVAIMPN2
      .w_MVAIMPS2=oParentObject.w_MVAIMPS2
      .w_MVACIVA3=oParentObject.w_MVACIVA3
      .w_MVAIMPN3=oParentObject.w_MVAIMPN3
      .w_MVAIMPS3=oParentObject.w_MVAIMPS3
      .w_MVACIVA4=oParentObject.w_MVACIVA4
      .w_MVAIMPN4=oParentObject.w_MVAIMPN4
      .w_MVAIMPS4=oParentObject.w_MVAIMPS4
      .w_MVACIVA5=oParentObject.w_MVACIVA5
      .w_MVAIMPN5=oParentObject.w_MVAIMPN5
      .w_MVAIMPS5=oParentObject.w_MVAIMPS5
      .w_MVACIVA6=oParentObject.w_MVACIVA6
      .w_MVAIMPN6=oParentObject.w_MVAIMPN6
      .w_MVAIMPS6=oParentObject.w_MVAIMPS6
      .w_PEIINC=oParentObject.w_PEIINC
      .w_BOLINC=oParentObject.w_BOLINC
      .w_DTOBSO=oParentObject.w_DTOBSO
      .w_PEIIMB=oParentObject.w_PEIIMB
      .w_BOLIMB=oParentObject.w_BOLIMB
      .w_PEITRA=oParentObject.w_PEITRA
      .w_BOLTRA=oParentObject.w_BOLTRA
      .w_BOLBOL=oParentObject.w_BOLBOL
      .w_MVCATOPE=oParentObject.w_MVCATOPE
      .w_MVIVACAU=oParentObject.w_MVIVACAU
      .w_BOLCAU=oParentObject.w_BOLCAU
      .w_REVCAU=oParentObject.w_REVCAU
      .w_MVDATDOC=oParentObject.w_MVDATDOC
      .w_MVTIPCON=oParentObject.w_MVTIPCON
      .w_MVCODCON=oParentObject.w_MVCODCON
      .w_MVCODIVE=oParentObject.w_MVCODIVE
      .w_DESAPP=oParentObject.w_DESAPP
      .w_BOLIVE=oParentObject.w_BOLIVE
      .w_PERIVE=oParentObject.w_PERIVE
      .w_DTOBSO=oParentObject.w_DTOBSO
          .DoRTCalc(1,10,.f.)
        .w_MVIVAINC = .w_MVIVAINC
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_MVIVAINC))
          .link_1_11('Full')
        endif
        .w_MVIVATRA = .w_MVIVATRA
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_MVIVATRA))
          .link_1_12('Full')
        endif
        .w_MVIVAIMB = .w_MVIVAIMB
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_MVIVAIMB))
          .link_1_13('Full')
        endif
        .w_MVIVABOL = .w_MVIVABOL
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_MVIVABOL))
          .link_1_14('Full')
        endif
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_MVTIPOPE))
          .link_1_16('Full')
        endif
        .w_MVACIVA1 = .w_MVACIVA1
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_MVACIVA1))
          .link_1_17('Full')
        endif
          .DoRTCalc(17,19,.f.)
        .w_MVACIVA2 = .w_MVACIVA2
        .DoRTCalc(20,20,.f.)
        if not(empty(.w_MVACIVA2))
          .link_1_25('Full')
        endif
          .DoRTCalc(21,23,.f.)
        .w_MVACIVA3 = .w_MVACIVA3
        .DoRTCalc(24,24,.f.)
        if not(empty(.w_MVACIVA3))
          .link_1_29('Full')
        endif
          .DoRTCalc(25,27,.f.)
        .w_MVACIVA4 = .w_MVACIVA4
        .DoRTCalc(28,28,.f.)
        if not(empty(.w_MVACIVA4))
          .link_1_33('Full')
        endif
          .DoRTCalc(29,31,.f.)
        .w_MVACIVA5 = .w_MVACIVA5
        .DoRTCalc(32,32,.f.)
        if not(empty(.w_MVACIVA5))
          .link_1_37('Full')
        endif
          .DoRTCalc(33,35,.f.)
        .w_MVACIVA6 = .w_MVACIVA6
        .DoRTCalc(36,36,.f.)
        if not(empty(.w_MVACIVA6))
          .link_1_41('Full')
        endif
          .DoRTCalc(37,39,.f.)
        .w_MVAIMPS = .w_MVAIMPS1+.w_MVAIMPS2+.w_MVAIMPS3+.w_MVAIMPS4+.w_MVAIMPS5+.w_MVAIMPS6
        .w_MVAIMPN = .w_MVAIMPN1+.w_MVAIMPN2+.w_MVAIMPN3+.w_MVAIMPN4+.w_MVAIMPN5+.w_MVAIMPN6
        .w_FLOM1 = IIF(.w_MVAFLOM1='I', 'Imponib.', IIF(.w_MVAFLOM1='E', 'Imp.+IVA',SPACE(8)))
        .w_FLOM2 = IIF(.w_MVAFLOM2='I', 'Imponib.', IIF(.w_MVAFLOM2='E', 'Imp.+IVA',SPACE(8)))
        .w_FLOM3 = IIF(.w_MVAFLOM3='I', 'Imponib.', IIF(.w_MVAFLOM3='E', 'Imp.+IVA',SPACE(8)))
        .w_FLOM4 = IIF(.w_MVAFLOM4='I', 'Imponib.', IIF(.w_MVAFLOM4='E', 'Imp.+IVA',SPACE(8)))
        .w_FLOM5 = IIF(.w_MVAFLOM5='I', 'Imponib.', IIF(.w_MVAFLOM5='E', 'Imp.+IVA',SPACE(8)))
        .w_FLOM6 = IIF(.w_MVAFLOM6='I', 'Imponib.', IIF(.w_MVAFLOM6='E', 'Imp.+IVA',SPACE(8)))
          .DoRTCalc(48,55,.f.)
        .w_AIMPINI = .w_MVAIMPN1+.w_MVAIMPN2+.w_MVAIMPN3+.w_MVAIMPN4+.w_MVAIMPN5+.w_MVAIMPN6
          .DoRTCalc(57,59,.f.)
        .w_MVCATOPE = 'OP'
        .w_MVIVACAU = .w_MVIVACAU
        .DoRTCalc(61,61,.f.)
        if not(empty(.w_MVIVACAU))
          .link_1_74('Full')
        endif
      .oPgFrm.Page1.oPag.Imballo.Calculate(IIF(IsAlt(),Ah_MsgFormat("Codice IVA sp. generali:"),Ah_MsgFormat("Codice IVA sp.imballo:")))
      .oPgFrm.Page1.oPag.trasporto.Calculate(IIF(IsAlt(),Ah_MsgFormat("Codice IVA cassa previdenza:"),Ah_MsgFormat("Codice IVA sp.trasporto:")))
    endwith
    this.DoRTCalc(62,72,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_49.enabled = this.oPgFrm.Page1.oPag.oBtn_1_49.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_50.enabled = this.oPgFrm.Page1.oPag.oBtn_1_50.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  proc SetStatus()
    if type('this.oParentObject.cFunction')='C'
      this.cFunction=this.oParentObject.cFunction
      if this.cFunction="Load"
        this.cFunction="Edit"
      endif
    endif
    DoDefault()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oMV__MESE_1_8.enabled = i_bVal
      .Page1.oPag.oMV__ANNO_1_9.enabled = i_bVal
      .Page1.oPag.oMVIVAINC_1_11.enabled = i_bVal
      .Page1.oPag.oMVIVATRA_1_12.enabled = i_bVal
      .Page1.oPag.oMVIVAIMB_1_13.enabled = i_bVal
      .Page1.oPag.oMVIVABOL_1_14.enabled = i_bVal
      .Page1.oPag.oMVTIPOPE_1_16.enabled = i_bVal
      .Page1.oPag.oMVAIMPN1_1_18.enabled = i_bVal
      .Page1.oPag.oMVAIMPS1_1_20.enabled = i_bVal
      .Page1.oPag.oMVAIMPN2_1_26.enabled = i_bVal
      .Page1.oPag.oMVAIMPS2_1_28.enabled = i_bVal
      .Page1.oPag.oMVAIMPN3_1_30.enabled = i_bVal
      .Page1.oPag.oMVAIMPS3_1_32.enabled = i_bVal
      .Page1.oPag.oMVAIMPN4_1_34.enabled = i_bVal
      .Page1.oPag.oMVAIMPS4_1_36.enabled = i_bVal
      .Page1.oPag.oMVAIMPN5_1_38.enabled = i_bVal
      .Page1.oPag.oMVAIMPS5_1_40.enabled = i_bVal
      .Page1.oPag.oMVAIMPN6_1_42.enabled = i_bVal
      .Page1.oPag.oMVAIMPS6_1_44.enabled = i_bVal
      .Page1.oPag.oMVIVACAU_1_74.enabled = i_bVal
      .Page1.oPag.oBtn_1_15.enabled = .Page1.oPag.oBtn_1_15.mCond()
      .Page1.oPag.oBtn_1_49.enabled = .Page1.oPag.oBtn_1_49.mCond()
      .Page1.oPag.oBtn_1_50.enabled = .Page1.oPag.oBtn_1_50.mCond()
    endwith
    cp_SetEnabledExtFlds(this,'',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_OBTEST=.w_OBTEST
      .oParentObject.w_MVAFLOM1=.w_MVAFLOM1
      .oParentObject.w_MVAFLOM2=.w_MVAFLOM2
      .oParentObject.w_MVAFLOM3=.w_MVAFLOM3
      .oParentObject.w_MVAFLOM4=.w_MVAFLOM4
      .oParentObject.w_MVCLADOC=.w_MVCLADOC
      .oParentObject.w_MVAFLOM5=.w_MVAFLOM5
      .oParentObject.w_MV__MESE=.w_MV__MESE
      .oParentObject.w_MV__ANNO=.w_MV__ANNO
      .oParentObject.w_MVAFLOM6=.w_MVAFLOM6
      .oParentObject.w_MVIVAINC=.w_MVIVAINC
      .oParentObject.w_MVIVATRA=.w_MVIVATRA
      .oParentObject.w_MVIVAIMB=.w_MVIVAIMB
      .oParentObject.w_MVIVABOL=.w_MVIVABOL
      .oParentObject.w_MVTIPOPE=.w_MVTIPOPE
      .oParentObject.w_MVACIVA1=.w_MVACIVA1
      .oParentObject.w_MVAIMPN1=.w_MVAIMPN1
      .oParentObject.w_MVAIMPS1=.w_MVAIMPS1
      .oParentObject.w_MVACIVA2=.w_MVACIVA2
      .oParentObject.w_MVAIMPN2=.w_MVAIMPN2
      .oParentObject.w_MVAIMPS2=.w_MVAIMPS2
      .oParentObject.w_MVACIVA3=.w_MVACIVA3
      .oParentObject.w_MVAIMPN3=.w_MVAIMPN3
      .oParentObject.w_MVAIMPS3=.w_MVAIMPS3
      .oParentObject.w_MVACIVA4=.w_MVACIVA4
      .oParentObject.w_MVAIMPN4=.w_MVAIMPN4
      .oParentObject.w_MVAIMPS4=.w_MVAIMPS4
      .oParentObject.w_MVACIVA5=.w_MVACIVA5
      .oParentObject.w_MVAIMPN5=.w_MVAIMPN5
      .oParentObject.w_MVAIMPS5=.w_MVAIMPS5
      .oParentObject.w_MVACIVA6=.w_MVACIVA6
      .oParentObject.w_MVAIMPN6=.w_MVAIMPN6
      .oParentObject.w_MVAIMPS6=.w_MVAIMPS6
      .oParentObject.w_PEIINC=.w_PEIINC
      .oParentObject.w_BOLINC=.w_BOLINC
      .oParentObject.w_DTOBSO=.w_DTOBSO
      .oParentObject.w_PEIIMB=.w_PEIIMB
      .oParentObject.w_BOLIMB=.w_BOLIMB
      .oParentObject.w_PEITRA=.w_PEITRA
      .oParentObject.w_BOLTRA=.w_BOLTRA
      .oParentObject.w_BOLBOL=.w_BOLBOL
      .oParentObject.w_MVCATOPE=.w_MVCATOPE
      .oParentObject.w_MVIVACAU=.w_MVIVACAU
      .oParentObject.w_BOLCAU=.w_BOLCAU
      .oParentObject.w_REVCAU=.w_REVCAU
      .oParentObject.w_MVDATDOC=.w_MVDATDOC
      .oParentObject.w_MVTIPCON=.w_MVTIPCON
      .oParentObject.w_MVCODCON=.w_MVCODCON
      .oParentObject.w_MVCODIVE=.w_MVCODIVE
      .oParentObject.w_DESAPP=.w_DESAPP
      .oParentObject.w_BOLIVE=.w_BOLIVE
      .oParentObject.w_PERIVE=.w_PERIVE
      .oParentObject.w_DTOBSO=.w_DTOBSO
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,15,.t.)
          .link_1_17('Full')
        .DoRTCalc(17,19,.t.)
          .link_1_25('Full')
        .DoRTCalc(21,23,.t.)
          .link_1_29('Full')
        .DoRTCalc(25,27,.t.)
          .link_1_33('Full')
        .DoRTCalc(29,31,.t.)
          .link_1_37('Full')
        .DoRTCalc(33,35,.t.)
          .link_1_41('Full')
        .DoRTCalc(37,39,.t.)
            .w_MVAIMPS = .w_MVAIMPS1+.w_MVAIMPS2+.w_MVAIMPS3+.w_MVAIMPS4+.w_MVAIMPS5+.w_MVAIMPS6
            .w_MVAIMPN = .w_MVAIMPN1+.w_MVAIMPN2+.w_MVAIMPN3+.w_MVAIMPN4+.w_MVAIMPN5+.w_MVAIMPN6
            .w_FLOM1 = IIF(.w_MVAFLOM1='I', 'Imponib.', IIF(.w_MVAFLOM1='E', 'Imp.+IVA',SPACE(8)))
            .w_FLOM2 = IIF(.w_MVAFLOM2='I', 'Imponib.', IIF(.w_MVAFLOM2='E', 'Imp.+IVA',SPACE(8)))
            .w_FLOM3 = IIF(.w_MVAFLOM3='I', 'Imponib.', IIF(.w_MVAFLOM3='E', 'Imp.+IVA',SPACE(8)))
            .w_FLOM4 = IIF(.w_MVAFLOM4='I', 'Imponib.', IIF(.w_MVAFLOM4='E', 'Imp.+IVA',SPACE(8)))
            .w_FLOM5 = IIF(.w_MVAFLOM5='I', 'Imponib.', IIF(.w_MVAFLOM5='E', 'Imp.+IVA',SPACE(8)))
            .w_FLOM6 = IIF(.w_MVAFLOM6='I', 'Imponib.', IIF(.w_MVAFLOM6='E', 'Imp.+IVA',SPACE(8)))
        .oPgFrm.Page1.oPag.Imballo.Calculate(IIF(IsAlt(),Ah_MsgFormat("Codice IVA sp. generali:"),Ah_MsgFormat("Codice IVA sp.imballo:")))
        .oPgFrm.Page1.oPag.trasporto.Calculate(IIF(IsAlt(),Ah_MsgFormat("Codice IVA cassa previdenza:"),Ah_MsgFormat("Codice IVA sp.trasporto:")))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(48,72,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.Imballo.Calculate(IIF(IsAlt(),Ah_MsgFormat("Codice IVA sp. generali:"),Ah_MsgFormat("Codice IVA sp.imballo:")))
        .oPgFrm.Page1.oPag.trasporto.Calculate(IIF(IsAlt(),Ah_MsgFormat("Codice IVA cassa previdenza:"),Ah_MsgFormat("Codice IVA sp.trasporto:")))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oMVIVAINC_1_11.enabled = this.oPgFrm.Page1.oPag.oMVIVAINC_1_11.mCond()
    this.oPgFrm.Page1.oPag.oMVIVABOL_1_14.enabled = this.oPgFrm.Page1.oPag.oMVIVABOL_1_14.mCond()
    this.oPgFrm.Page1.oPag.oMVAIMPN1_1_18.enabled = this.oPgFrm.Page1.oPag.oMVAIMPN1_1_18.mCond()
    this.oPgFrm.Page1.oPag.oMVAIMPS1_1_20.enabled = this.oPgFrm.Page1.oPag.oMVAIMPS1_1_20.mCond()
    this.oPgFrm.Page1.oPag.oMVAIMPN2_1_26.enabled = this.oPgFrm.Page1.oPag.oMVAIMPN2_1_26.mCond()
    this.oPgFrm.Page1.oPag.oMVAIMPS2_1_28.enabled = this.oPgFrm.Page1.oPag.oMVAIMPS2_1_28.mCond()
    this.oPgFrm.Page1.oPag.oMVAIMPN3_1_30.enabled = this.oPgFrm.Page1.oPag.oMVAIMPN3_1_30.mCond()
    this.oPgFrm.Page1.oPag.oMVAIMPS3_1_32.enabled = this.oPgFrm.Page1.oPag.oMVAIMPS3_1_32.mCond()
    this.oPgFrm.Page1.oPag.oMVAIMPN4_1_34.enabled = this.oPgFrm.Page1.oPag.oMVAIMPN4_1_34.mCond()
    this.oPgFrm.Page1.oPag.oMVAIMPS4_1_36.enabled = this.oPgFrm.Page1.oPag.oMVAIMPS4_1_36.mCond()
    this.oPgFrm.Page1.oPag.oMVAIMPN5_1_38.enabled = this.oPgFrm.Page1.oPag.oMVAIMPN5_1_38.mCond()
    this.oPgFrm.Page1.oPag.oMVAIMPS5_1_40.enabled = this.oPgFrm.Page1.oPag.oMVAIMPS5_1_40.mCond()
    this.oPgFrm.Page1.oPag.oMVAIMPN6_1_42.enabled = this.oPgFrm.Page1.oPag.oMVAIMPN6_1_42.mCond()
    this.oPgFrm.Page1.oPag.oMVAIMPS6_1_44.enabled = this.oPgFrm.Page1.oPag.oMVAIMPS6_1_44.mCond()
    this.oPgFrm.Page1.oPag.oMVIVACAU_1_74.enabled = this.oPgFrm.Page1.oPag.oMVIVACAU_1_74.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oMV__MESE_1_8.visible=!this.oPgFrm.Page1.oPag.oMV__MESE_1_8.mHide()
    this.oPgFrm.Page1.oPag.oMV__ANNO_1_9.visible=!this.oPgFrm.Page1.oPag.oMV__ANNO_1_9.mHide()
    this.oPgFrm.Page1.oPag.oMVIVAINC_1_11.visible=!this.oPgFrm.Page1.oPag.oMVIVAINC_1_11.mHide()
    this.oPgFrm.Page1.oPag.oMVIVABOL_1_14.visible=!this.oPgFrm.Page1.oPag.oMVIVABOL_1_14.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_58.visible=!this.oPgFrm.Page1.oPag.oStr_1_58.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_59.visible=!this.oPgFrm.Page1.oPag.oStr_1_59.mHide()
    this.oPgFrm.Page1.oPag.oMVIVACAU_1_74.visible=!this.oPgFrm.Page1.oPag.oMVIVACAU_1_74.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_75.visible=!this.oPgFrm.Page1.oPag.oStr_1_75.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_89.visible=!this.oPgFrm.Page1.oPag.oStr_1_89.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_90.visible=!this.oPgFrm.Page1.oPag.oStr_1_90.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.Imballo.Event(cEvent)
      .oPgFrm.Page1.oPag.trasporto.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MVIVAINC
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVIVAINC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_MVIVAINC)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVPERIVA,IVBOLIVA,IVDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_MVIVAINC))
          select IVCODIVA,IVPERIVA,IVBOLIVA,IVDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVIVAINC)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVIVAINC) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oMVIVAINC_1_11'),i_cWhere,'GSAR_AIV',"Codici IVA",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVPERIVA,IVBOLIVA,IVDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVPERIVA,IVBOLIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVIVAINC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVPERIVA,IVBOLIVA,IVDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_MVIVAINC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_MVIVAINC)
            select IVCODIVA,IVPERIVA,IVBOLIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVIVAINC = NVL(_Link_.IVCODIVA,space(5))
      this.w_PEIINC = NVL(_Link_.IVPERIVA,0)
      this.w_BOLINC = NVL(_Link_.IVBOLIVA,space(1))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.IVDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MVIVAINC = space(5)
      endif
      this.w_PEIINC = 0
      this.w_BOLINC = space(1)
      this.w_DTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        endif
        this.w_MVIVAINC = space(5)
        this.w_PEIINC = 0
        this.w_BOLINC = space(1)
        this.w_DTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVIVAINC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVIVATRA
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVIVATRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_MVIVATRA)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVPERIVA,IVBOLIVA,IVDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_MVIVATRA))
          select IVCODIVA,IVPERIVA,IVBOLIVA,IVDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVIVATRA)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVIVATRA) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oMVIVATRA_1_12'),i_cWhere,'GSAR_AIV',"Codici IVA",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVPERIVA,IVBOLIVA,IVDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVPERIVA,IVBOLIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVIVATRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVPERIVA,IVBOLIVA,IVDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_MVIVATRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_MVIVATRA)
            select IVCODIVA,IVPERIVA,IVBOLIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVIVATRA = NVL(_Link_.IVCODIVA,space(5))
      this.w_PEITRA = NVL(_Link_.IVPERIVA,0)
      this.w_BOLTRA = NVL(_Link_.IVBOLIVA,space(1))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.IVDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MVIVATRA = space(5)
      endif
      this.w_PEITRA = 0
      this.w_BOLTRA = space(1)
      this.w_DTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        endif
        this.w_MVIVATRA = space(5)
        this.w_PEITRA = 0
        this.w_BOLTRA = space(1)
        this.w_DTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVIVATRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVIVAIMB
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVIVAIMB) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_MVIVAIMB)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVPERIVA,IVBOLIVA,IVDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_MVIVAIMB))
          select IVCODIVA,IVPERIVA,IVBOLIVA,IVDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVIVAIMB)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVIVAIMB) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oMVIVAIMB_1_13'),i_cWhere,'GSAR_AIV',"Codici IVA",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVPERIVA,IVBOLIVA,IVDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVPERIVA,IVBOLIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVIVAIMB)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVPERIVA,IVBOLIVA,IVDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_MVIVAIMB);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_MVIVAIMB)
            select IVCODIVA,IVPERIVA,IVBOLIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVIVAIMB = NVL(_Link_.IVCODIVA,space(5))
      this.w_PEIIMB = NVL(_Link_.IVPERIVA,0)
      this.w_BOLIMB = NVL(_Link_.IVBOLIVA,space(1))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.IVDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MVIVAIMB = space(5)
      endif
      this.w_PEIIMB = 0
      this.w_BOLIMB = space(1)
      this.w_DTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        endif
        this.w_MVIVAIMB = space(5)
        this.w_PEIIMB = 0
        this.w_BOLIMB = space(1)
        this.w_DTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVIVAIMB Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVIVABOL
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVIVABOL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_MVIVABOL)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVBOLIVA,IVDTOBSO,IVPERIVA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_MVIVABOL))
          select IVCODIVA,IVBOLIVA,IVDTOBSO,IVPERIVA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVIVABOL)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVIVABOL) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oMVIVABOL_1_14'),i_cWhere,'GSAR_AIV',"Codici IVA",'GSAR_ZVE.VOCIIVA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVBOLIVA,IVDTOBSO,IVPERIVA";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVBOLIVA,IVDTOBSO,IVPERIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVIVABOL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVBOLIVA,IVDTOBSO,IVPERIVA";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_MVIVABOL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_MVIVABOL)
            select IVCODIVA,IVBOLIVA,IVDTOBSO,IVPERIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVIVABOL = NVL(_Link_.IVCODIVA,space(5))
      this.w_BOLBOL = NVL(_Link_.IVBOLIVA,space(1))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.IVDTOBSO),ctod("  /  /  "))
      this.w_PERIVA = NVL(_Link_.IVPERIVA,0)
    else
      if i_cCtrl<>'Load'
        this.w_MVIVABOL = space(5)
      endif
      this.w_BOLBOL = space(1)
      this.w_DTOBSO = ctod("  /  /  ")
      this.w_PERIVA = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST) And .w_PERIVA=0
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice non esente, inesistente oppure obsoleto")
        endif
        this.w_MVIVABOL = space(5)
        this.w_BOLBOL = space(1)
        this.w_DTOBSO = ctod("  /  /  ")
        this.w_PERIVA = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVIVABOL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVTIPOPE
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPCODIV_IDX,3]
    i_lTable = "TIPCODIV"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPCODIV_IDX,2], .t., this.TIPCODIV_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPCODIV_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVTIPOPE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATO',True,'TIPCODIV')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TICODICE like "+cp_ToStrODBC(trim(this.w_MVTIPOPE)+"%");
                   +" and TI__TIPO="+cp_ToStrODBC(this.w_MVCATOPE);

          i_ret=cp_SQL(i_nConn,"select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TI__TIPO,TICODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TI__TIPO',this.w_MVCATOPE;
                     ,'TICODICE',trim(this.w_MVTIPOPE))
          select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TI__TIPO,TICODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVTIPOPE)==trim(_Link_.TICODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVTIPOPE) and !this.bDontReportError
            deferred_cp_zoom('TIPCODIV','*','TI__TIPO,TICODICE',cp_AbsName(oSource.parent,'oMVTIPOPE_1_16'),i_cWhere,'GSAR_ATO',"Operazioni IVA",'GSVE_ZTI.TIPCODIV_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MVCATOPE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where TICODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TI__TIPO="+cp_ToStrODBC(this.w_MVCATOPE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TI__TIPO',oSource.xKey(1);
                       ,'TICODICE',oSource.xKey(2))
            select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVTIPOPE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where TICODICE="+cp_ToStrODBC(this.w_MVTIPOPE);
                   +" and TI__TIPO="+cp_ToStrODBC(this.w_MVCATOPE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TI__TIPO',this.w_MVCATOPE;
                       ,'TICODICE',this.w_MVTIPOPE)
            select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVTIPOPE = NVL(_Link_.TICODICE,space(10))
      this.w_TIDESCRI = NVL(_Link_.TIDESCRI,space(30))
      this.w_TIDTOBSO = NVL(cp_ToDate(_Link_.TIDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MVTIPOPE = space(10)
      endif
      this.w_TIDESCRI = space(30)
      this.w_TIDTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_TIDTOBSO) OR .w_TIDTOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        endif
        this.w_MVTIPOPE = space(10)
        this.w_TIDESCRI = space(30)
        this.w_TIDTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPCODIV_IDX,2])+'\'+cp_ToStr(_Link_.TI__TIPO,1)+'\'+cp_ToStr(_Link_.TICODICE,1)
      cp_ShowWarn(i_cKey,this.TIPCODIV_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVTIPOPE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVACIVA1
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVACIVA1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVACIVA1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVPERIVA";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_MVACIVA1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_MVACIVA1)
            select IVCODIVA,IVPERIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVACIVA1 = NVL(_Link_.IVCODIVA,space(5))
      this.w_PERIV1 = NVL(_Link_.IVPERIVA,0)
    else
      if i_cCtrl<>'Load'
        this.w_MVACIVA1 = space(5)
      endif
      this.w_PERIV1 = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVACIVA1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVACIVA2
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVACIVA2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVACIVA2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVPERIVA";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_MVACIVA2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_MVACIVA2)
            select IVCODIVA,IVPERIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVACIVA2 = NVL(_Link_.IVCODIVA,space(5))
      this.w_PERIV2 = NVL(_Link_.IVPERIVA,0)
    else
      if i_cCtrl<>'Load'
        this.w_MVACIVA2 = space(5)
      endif
      this.w_PERIV2 = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVACIVA2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVACIVA3
  func Link_1_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVACIVA3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVACIVA3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVPERIVA";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_MVACIVA3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_MVACIVA3)
            select IVCODIVA,IVPERIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVACIVA3 = NVL(_Link_.IVCODIVA,space(5))
      this.w_PERIV3 = NVL(_Link_.IVPERIVA,0)
    else
      if i_cCtrl<>'Load'
        this.w_MVACIVA3 = space(5)
      endif
      this.w_PERIV3 = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVACIVA3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVACIVA4
  func Link_1_33(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVACIVA4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVACIVA4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVPERIVA";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_MVACIVA4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_MVACIVA4)
            select IVCODIVA,IVPERIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVACIVA4 = NVL(_Link_.IVCODIVA,space(5))
      this.w_PERIV4 = NVL(_Link_.IVPERIVA,0)
    else
      if i_cCtrl<>'Load'
        this.w_MVACIVA4 = space(5)
      endif
      this.w_PERIV4 = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVACIVA4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVACIVA5
  func Link_1_37(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVACIVA5) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVACIVA5)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVPERIVA";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_MVACIVA5);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_MVACIVA5)
            select IVCODIVA,IVPERIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVACIVA5 = NVL(_Link_.IVCODIVA,space(5))
      this.w_PERIV5 = NVL(_Link_.IVPERIVA,0)
    else
      if i_cCtrl<>'Load'
        this.w_MVACIVA5 = space(5)
      endif
      this.w_PERIV5 = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVACIVA5 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVACIVA6
  func Link_1_41(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVACIVA6) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVACIVA6)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVPERIVA";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_MVACIVA6);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_MVACIVA6)
            select IVCODIVA,IVPERIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVACIVA6 = NVL(_Link_.IVCODIVA,space(5))
      this.w_PERIV6 = NVL(_Link_.IVPERIVA,0)
    else
      if i_cCtrl<>'Load'
        this.w_MVACIVA6 = space(5)
      endif
      this.w_PERIV6 = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVACIVA6 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVIVACAU
  func Link_1_74(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVIVACAU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_MVIVACAU)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDTOBSO,IVPERIVA,IVBOLIVA,IVREVCHA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_MVIVACAU))
          select IVCODIVA,IVDTOBSO,IVPERIVA,IVBOLIVA,IVREVCHA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVIVACAU)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVIVACAU) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oMVIVACAU_1_74'),i_cWhere,'GSAR_AIV',"Codici IVA",'GSAR_ZVE.VOCIIVA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDTOBSO,IVPERIVA,IVBOLIVA,IVREVCHA";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDTOBSO,IVPERIVA,IVBOLIVA,IVREVCHA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVIVACAU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDTOBSO,IVPERIVA,IVBOLIVA,IVREVCHA";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_MVIVACAU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_MVIVACAU)
            select IVCODIVA,IVDTOBSO,IVPERIVA,IVBOLIVA,IVREVCHA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVIVACAU = NVL(_Link_.IVCODIVA,space(5))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.IVDTOBSO),ctod("  /  /  "))
      this.w_PEICAU = NVL(_Link_.IVPERIVA,0)
      this.w_BOLCAU = NVL(_Link_.IVBOLIVA,space(1))
      this.w_REVCAU = NVL(_Link_.IVREVCHA,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVIVACAU = space(5)
      endif
      this.w_DTOBSO = ctod("  /  /  ")
      this.w_PEICAU = 0
      this.w_BOLCAU = space(1)
      this.w_REVCAU = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST) And .w_PEICAU=0
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice non esente, inesistente oppure obsoleto")
        endif
        this.w_MVIVACAU = space(5)
        this.w_DTOBSO = ctod("  /  /  ")
        this.w_PEICAU = 0
        this.w_BOLCAU = space(1)
        this.w_REVCAU = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVIVACAU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMV__MESE_1_8.value==this.w_MV__MESE)
      this.oPgFrm.Page1.oPag.oMV__MESE_1_8.value=this.w_MV__MESE
    endif
    if not(this.oPgFrm.Page1.oPag.oMV__ANNO_1_9.value==this.w_MV__ANNO)
      this.oPgFrm.Page1.oPag.oMV__ANNO_1_9.value=this.w_MV__ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oMVIVAINC_1_11.value==this.w_MVIVAINC)
      this.oPgFrm.Page1.oPag.oMVIVAINC_1_11.value=this.w_MVIVAINC
    endif
    if not(this.oPgFrm.Page1.oPag.oMVIVATRA_1_12.value==this.w_MVIVATRA)
      this.oPgFrm.Page1.oPag.oMVIVATRA_1_12.value=this.w_MVIVATRA
    endif
    if not(this.oPgFrm.Page1.oPag.oMVIVAIMB_1_13.value==this.w_MVIVAIMB)
      this.oPgFrm.Page1.oPag.oMVIVAIMB_1_13.value=this.w_MVIVAIMB
    endif
    if not(this.oPgFrm.Page1.oPag.oMVIVABOL_1_14.value==this.w_MVIVABOL)
      this.oPgFrm.Page1.oPag.oMVIVABOL_1_14.value=this.w_MVIVABOL
    endif
    if not(this.oPgFrm.Page1.oPag.oMVTIPOPE_1_16.value==this.w_MVTIPOPE)
      this.oPgFrm.Page1.oPag.oMVTIPOPE_1_16.value=this.w_MVTIPOPE
    endif
    if not(this.oPgFrm.Page1.oPag.oMVACIVA1_1_17.value==this.w_MVACIVA1)
      this.oPgFrm.Page1.oPag.oMVACIVA1_1_17.value=this.w_MVACIVA1
    endif
    if not(this.oPgFrm.Page1.oPag.oMVAIMPN1_1_18.value==this.w_MVAIMPN1)
      this.oPgFrm.Page1.oPag.oMVAIMPN1_1_18.value=this.w_MVAIMPN1
    endif
    if not(this.oPgFrm.Page1.oPag.oPERIV1_1_19.value==this.w_PERIV1)
      this.oPgFrm.Page1.oPag.oPERIV1_1_19.value=this.w_PERIV1
    endif
    if not(this.oPgFrm.Page1.oPag.oMVAIMPS1_1_20.value==this.w_MVAIMPS1)
      this.oPgFrm.Page1.oPag.oMVAIMPS1_1_20.value=this.w_MVAIMPS1
    endif
    if not(this.oPgFrm.Page1.oPag.oMVACIVA2_1_25.value==this.w_MVACIVA2)
      this.oPgFrm.Page1.oPag.oMVACIVA2_1_25.value=this.w_MVACIVA2
    endif
    if not(this.oPgFrm.Page1.oPag.oMVAIMPN2_1_26.value==this.w_MVAIMPN2)
      this.oPgFrm.Page1.oPag.oMVAIMPN2_1_26.value=this.w_MVAIMPN2
    endif
    if not(this.oPgFrm.Page1.oPag.oPERIV2_1_27.value==this.w_PERIV2)
      this.oPgFrm.Page1.oPag.oPERIV2_1_27.value=this.w_PERIV2
    endif
    if not(this.oPgFrm.Page1.oPag.oMVAIMPS2_1_28.value==this.w_MVAIMPS2)
      this.oPgFrm.Page1.oPag.oMVAIMPS2_1_28.value=this.w_MVAIMPS2
    endif
    if not(this.oPgFrm.Page1.oPag.oMVACIVA3_1_29.value==this.w_MVACIVA3)
      this.oPgFrm.Page1.oPag.oMVACIVA3_1_29.value=this.w_MVACIVA3
    endif
    if not(this.oPgFrm.Page1.oPag.oMVAIMPN3_1_30.value==this.w_MVAIMPN3)
      this.oPgFrm.Page1.oPag.oMVAIMPN3_1_30.value=this.w_MVAIMPN3
    endif
    if not(this.oPgFrm.Page1.oPag.oPERIV3_1_31.value==this.w_PERIV3)
      this.oPgFrm.Page1.oPag.oPERIV3_1_31.value=this.w_PERIV3
    endif
    if not(this.oPgFrm.Page1.oPag.oMVAIMPS3_1_32.value==this.w_MVAIMPS3)
      this.oPgFrm.Page1.oPag.oMVAIMPS3_1_32.value=this.w_MVAIMPS3
    endif
    if not(this.oPgFrm.Page1.oPag.oMVACIVA4_1_33.value==this.w_MVACIVA4)
      this.oPgFrm.Page1.oPag.oMVACIVA4_1_33.value=this.w_MVACIVA4
    endif
    if not(this.oPgFrm.Page1.oPag.oMVAIMPN4_1_34.value==this.w_MVAIMPN4)
      this.oPgFrm.Page1.oPag.oMVAIMPN4_1_34.value=this.w_MVAIMPN4
    endif
    if not(this.oPgFrm.Page1.oPag.oPERIV4_1_35.value==this.w_PERIV4)
      this.oPgFrm.Page1.oPag.oPERIV4_1_35.value=this.w_PERIV4
    endif
    if not(this.oPgFrm.Page1.oPag.oMVAIMPS4_1_36.value==this.w_MVAIMPS4)
      this.oPgFrm.Page1.oPag.oMVAIMPS4_1_36.value=this.w_MVAIMPS4
    endif
    if not(this.oPgFrm.Page1.oPag.oMVACIVA5_1_37.value==this.w_MVACIVA5)
      this.oPgFrm.Page1.oPag.oMVACIVA5_1_37.value=this.w_MVACIVA5
    endif
    if not(this.oPgFrm.Page1.oPag.oMVAIMPN5_1_38.value==this.w_MVAIMPN5)
      this.oPgFrm.Page1.oPag.oMVAIMPN5_1_38.value=this.w_MVAIMPN5
    endif
    if not(this.oPgFrm.Page1.oPag.oPERIV5_1_39.value==this.w_PERIV5)
      this.oPgFrm.Page1.oPag.oPERIV5_1_39.value=this.w_PERIV5
    endif
    if not(this.oPgFrm.Page1.oPag.oMVAIMPS5_1_40.value==this.w_MVAIMPS5)
      this.oPgFrm.Page1.oPag.oMVAIMPS5_1_40.value=this.w_MVAIMPS5
    endif
    if not(this.oPgFrm.Page1.oPag.oMVACIVA6_1_41.value==this.w_MVACIVA6)
      this.oPgFrm.Page1.oPag.oMVACIVA6_1_41.value=this.w_MVACIVA6
    endif
    if not(this.oPgFrm.Page1.oPag.oMVAIMPN6_1_42.value==this.w_MVAIMPN6)
      this.oPgFrm.Page1.oPag.oMVAIMPN6_1_42.value=this.w_MVAIMPN6
    endif
    if not(this.oPgFrm.Page1.oPag.oPERIV6_1_43.value==this.w_PERIV6)
      this.oPgFrm.Page1.oPag.oPERIV6_1_43.value=this.w_PERIV6
    endif
    if not(this.oPgFrm.Page1.oPag.oMVAIMPS6_1_44.value==this.w_MVAIMPS6)
      this.oPgFrm.Page1.oPag.oMVAIMPS6_1_44.value=this.w_MVAIMPS6
    endif
    if not(this.oPgFrm.Page1.oPag.oMVAIMPS_1_47.value==this.w_MVAIMPS)
      this.oPgFrm.Page1.oPag.oMVAIMPS_1_47.value=this.w_MVAIMPS
    endif
    if not(this.oPgFrm.Page1.oPag.oMVAIMPN_1_48.value==this.w_MVAIMPN)
      this.oPgFrm.Page1.oPag.oMVAIMPN_1_48.value=this.w_MVAIMPN
    endif
    if not(this.oPgFrm.Page1.oPag.oFLOM1_1_51.value==this.w_FLOM1)
      this.oPgFrm.Page1.oPag.oFLOM1_1_51.value=this.w_FLOM1
    endif
    if not(this.oPgFrm.Page1.oPag.oFLOM2_1_52.value==this.w_FLOM2)
      this.oPgFrm.Page1.oPag.oFLOM2_1_52.value=this.w_FLOM2
    endif
    if not(this.oPgFrm.Page1.oPag.oFLOM3_1_53.value==this.w_FLOM3)
      this.oPgFrm.Page1.oPag.oFLOM3_1_53.value=this.w_FLOM3
    endif
    if not(this.oPgFrm.Page1.oPag.oFLOM4_1_54.value==this.w_FLOM4)
      this.oPgFrm.Page1.oPag.oFLOM4_1_54.value=this.w_FLOM4
    endif
    if not(this.oPgFrm.Page1.oPag.oFLOM5_1_55.value==this.w_FLOM5)
      this.oPgFrm.Page1.oPag.oFLOM5_1_55.value=this.w_FLOM5
    endif
    if not(this.oPgFrm.Page1.oPag.oFLOM6_1_56.value==this.w_FLOM6)
      this.oPgFrm.Page1.oPag.oFLOM6_1_56.value=this.w_FLOM6
    endif
    if not(this.oPgFrm.Page1.oPag.oTIDESCRI_1_71.value==this.w_TIDESCRI)
      this.oPgFrm.Page1.oPag.oTIDESCRI_1_71.value=this.w_TIDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oMVIVACAU_1_74.value==this.w_MVIVACAU)
      this.oPgFrm.Page1.oPag.oMVIVACAU_1_74.value=this.w_MVIVACAU
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((.w_MV__MESE>=1 and .w_MV__MESE<=12) OR .w_MV__MESE=0)  and not(Not .w_MVCLADOC $ 'FA-NC-FC')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMV__MESE_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Mese di riferimento: selezionare un mese compreso tra 1 e 12")
          case   not((.w_MV__ANNO>=1901 and .w_MV__ANNO<=2099) OR .w_MV__ANNO=0)  and not(Not .w_MVCLADOC $ 'FA-NC-FC')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMV__ANNO_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Anno solare: selezionare un anno compreso tra 1901 e 2099")
          case   not(EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST)  and not(IsAlt())  and (!IsAlt())  and not(empty(.w_MVIVAINC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVIVAINC_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure obsoleto")
          case   not(EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST)  and not(empty(.w_MVIVATRA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVIVATRA_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure obsoleto")
          case   not(EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST)  and not(empty(.w_MVIVAIMB))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVIVAIMB_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure obsoleto")
          case   not((EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST) And .w_PERIVA=0)  and not(IsAlt())  and (!IsAlt())  and not(empty(.w_MVIVABOL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVIVABOL_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice non esente, inesistente oppure obsoleto")
          case   not((EMPTY(.w_TIDTOBSO) OR .w_TIDTOBSO>.w_OBTEST))  and not(empty(.w_MVTIPOPE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVTIPOPE_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure obsoleto")
          case   not((EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST) And .w_PEICAU=0)  and not(IsAlt())  and (!IsAlt())  and not(empty(.w_MVIVACAU))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVIVACAU_1_74.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice non esente, inesistente oppure obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsve_kfa
      IF i_bRes=.T. AND this.w_MVAIMPN <> this.w_AIMPINI
         i_bRes = .f.
         i_bnoChk = .f.		
         i_cErrorMsg = Ah_MsgFormat("Impossibile modificare il totale imponibile")
      ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsve_kfaPag1 as StdContainer
  Width  = 514
  height = 411
  stdWidth  = 514
  stdheight = 411
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMV__MESE_1_8 as StdField with uid="EKOESKVWRI",rtseq=8,rtrep=.f.,;
    cFormVar = "w_MV__MESE", cQueryName = "MV__MESE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Mese di riferimento: selezionare un mese compreso tra 1 e 12",;
    ToolTipText = "Mese di riferimento dell'operazione",;
    HelpContextID = 146073333,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=156, Top=13, cSayPict='"@L"', cGetPict='"99"'

  func oMV__MESE_1_8.mHide()
    with this.Parent.oContained
      return (Not .w_MVCLADOC $ 'FA-NC-FC')
    endwith
  endfunc

  func oMV__MESE_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_MV__MESE>=1 and .w_MV__MESE<=12) OR .w_MV__MESE=0)
    endwith
    return bRes
  endfunc

  add object oMV__ANNO_1_9 as StdField with uid="YOQQQJNUOU",rtseq=9,rtrep=.f.,;
    cFormVar = "w_MV__ANNO", cQueryName = "MV__ANNO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Anno solare: selezionare un anno compreso tra 1901 e 2099",;
    ToolTipText = "Anno solare di riferimento dell'operazione",;
    HelpContextID = 7661291,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=383, Top=13, cSayPict='"9999"', cGetPict='"9999"'

  func oMV__ANNO_1_9.mHide()
    with this.Parent.oContained
      return (Not .w_MVCLADOC $ 'FA-NC-FC')
    endwith
  endfunc

  func oMV__ANNO_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_MV__ANNO>=1901 and .w_MV__ANNO<=2099) OR .w_MV__ANNO=0)
    endwith
    return bRes
  endfunc

  add object oMVIVAINC_1_11 as StdField with uid="QBPCJBVVHA",rtseq=11,rtrep=.f.,;
    cFormVar = "w_MVIVAINC", cQueryName = "MVIVAINC",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
    ToolTipText = "Codice IVA spese di incasso (vuoto = ripartisce sulle varie aliquote)",;
    HelpContextID = 92227319,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=156, Top=40, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_MVIVAINC"

  func oMVIVAINC_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!IsAlt())
    endwith
   endif
  endfunc

  func oMVIVAINC_1_11.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oMVIVAINC_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVIVAINC_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVIVAINC_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oMVIVAINC_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Codici IVA",'',this.parent.oContained
  endproc
  proc oMVIVAINC_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_MVIVAINC
     i_obj.ecpSave()
  endproc

  add object oMVIVATRA_1_12 as StdField with uid="YBMIAQFXAZ",rtseq=12,rtrep=.f.,;
    cFormVar = "w_MVIVATRA", cQueryName = "MVIVATRA",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
    ToolTipText = "Codice IVA spese di trasporto (vuoto = ripartisce sulle varie aliquote)",;
    HelpContextID = 176113401,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=383, Top=40, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_MVIVATRA"

  func oMVIVATRA_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVIVATRA_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVIVATRA_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oMVIVATRA_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Codici IVA",'',this.parent.oContained
  endproc
  proc oMVIVATRA_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_MVIVATRA
     i_obj.ecpSave()
  endproc

  add object oMVIVAIMB_1_13 as StdField with uid="BDKYYNRENX",rtseq=13,rtrep=.f.,;
    cFormVar = "w_MVIVAIMB", cQueryName = "MVIVAIMB",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
    ToolTipText = "Codice IVA spese di imballo (vuoto = ripartisce sulle varie aliquote)",;
    HelpContextID = 92227320,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=156, Top=67, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_MVIVAIMB"

  func oMVIVAIMB_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVIVAIMB_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVIVAIMB_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oMVIVAIMB_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Codici IVA",'',this.parent.oContained
  endproc
  proc oMVIVAIMB_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_MVIVAIMB
     i_obj.ecpSave()
  endproc

  add object oMVIVABOL_1_14 as StdField with uid="GFYISFKTXM",rtseq=14,rtrep=.f.,;
    cFormVar = "w_MVIVABOL", cQueryName = "MVIVABOL",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice non esente, inesistente oppure obsoleto",;
    ToolTipText = "Codice IVA spese bolli",;
    HelpContextID = 209667822,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=383, Top=66, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_MVIVABOL"

  func oMVIVABOL_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!IsAlt())
    endwith
   endif
  endfunc

  func oMVIVABOL_1_14.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oMVIVABOL_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVIVABOL_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVIVABOL_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oMVIVABOL_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Codici IVA",'GSAR_ZVE.VOCIIVA_VZM',this.parent.oContained
  endproc
  proc oMVIVABOL_1_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_MVIVABOL
     i_obj.ecpSave()
  endproc


  add object oBtn_1_15 as StdButton with uid="QGCZQYZOEL",left=451, top=40, width=48,height=45,;
    CpPicture="bmp\calcola.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per ricalcolare i codici IVA sulle righe del documento";
    , HelpContextID = 146967918;
    , TabStop=.f., Caption='\<Ricalcola';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      with this.Parent.oContained
        do GSVE_BRI with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oMVTIPOPE_1_16 as StdField with uid="WDSABEVQEJ",rtseq=15,rtrep=.f.,;
    cFormVar = "w_MVTIPOPE", cQueryName = "MVTIPOPE",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
    ToolTipText = "Tipo operazione IVA letto dalla sede di consegna",;
    HelpContextID = 245077749,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=156, Top=121, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="TIPCODIV", cZoomOnZoom="GSAR_ATO", oKey_1_1="TI__TIPO", oKey_1_2="this.w_MVCATOPE", oKey_2_1="TICODICE", oKey_2_2="this.w_MVTIPOPE"

  func oMVTIPOPE_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVTIPOPE_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVTIPOPE_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.TIPCODIV_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TI__TIPO="+cp_ToStrODBC(this.Parent.oContained.w_MVCATOPE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TI__TIPO="+cp_ToStr(this.Parent.oContained.w_MVCATOPE)
    endif
    do cp_zoom with 'TIPCODIV','*','TI__TIPO,TICODICE',cp_AbsName(this.parent,'oMVTIPOPE_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATO',"Operazioni IVA",'GSVE_ZTI.TIPCODIV_VZM',this.parent.oContained
  endproc
  proc oMVTIPOPE_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TI__TIPO=w_MVCATOPE
     i_obj.w_TICODICE=this.parent.oContained.w_MVTIPOPE
     i_obj.ecpSave()
  endproc

  add object oMVACIVA1_1_17 as StdField with uid="VAAWMJMVZF",rtseq=16,rtrep=.f.,;
    cFormVar = "w_MVACIVA1", cQueryName = "MVACIVA1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 135448329,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=91, Top=178, InputMask=replicate('X',5), cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_MVACIVA1"

  func oMVACIVA1_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oMVAIMPN1_1_18 as StdField with uid="QFDCPSAFUC",rtseq=17,rtrep=.f.,;
    cFormVar = "w_MVAIMPN1", cQueryName = "MVAIMPN1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 231524105,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=155, Top=178, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oMVAIMPN1_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_MVACIVA1))
    endwith
   endif
  endfunc

  add object oPERIV1_1_19 as StdField with uid="GLUPWSAAPU",rtseq=18,rtrep=.f.,;
    cFormVar = "w_PERIV1", cQueryName = "PERIV1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 205244426,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=303, Top=178, cSayPict='"99.99"', cGetPict='"99.99"'

  add object oMVAIMPS1_1_20 as StdField with uid="YABRUIZYJL",rtseq=19,rtrep=.f.,;
    cFormVar = "w_MVAIMPS1", cQueryName = "MVAIMPS1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 36911351,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=360, Top=178, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oMVAIMPS1_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_MVACIVA1))
    endwith
   endif
  endfunc

  add object oMVACIVA2_1_25 as StdField with uid="ICGYFLRDAQ",rtseq=20,rtrep=.f.,;
    cFormVar = "w_MVACIVA2", cQueryName = "MVACIVA2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 135448328,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=91, Top=202, InputMask=replicate('X',5), cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_MVACIVA2"

  func oMVACIVA2_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oMVAIMPN2_1_26 as StdField with uid="VAKICZGQEG",rtseq=21,rtrep=.f.,;
    cFormVar = "w_MVAIMPN2", cQueryName = "MVAIMPN2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 231524104,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=155, Top=202, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oMVAIMPN2_1_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_MVACIVA2))
    endwith
   endif
  endfunc

  add object oPERIV2_1_27 as StdField with uid="KONDIPJFAI",rtseq=22,rtrep=.f.,;
    cFormVar = "w_PERIV2", cQueryName = "PERIV2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 188467210,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=303, Top=202, cSayPict='"99.99"', cGetPict='"99.99"'

  add object oMVAIMPS2_1_28 as StdField with uid="HDYTHGEYQA",rtseq=23,rtrep=.f.,;
    cFormVar = "w_MVAIMPS2", cQueryName = "MVAIMPS2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 36911352,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=360, Top=202, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oMVAIMPS2_1_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_MVACIVA2))
    endwith
   endif
  endfunc

  add object oMVACIVA3_1_29 as StdField with uid="EHQNMUHKCO",rtseq=24,rtrep=.f.,;
    cFormVar = "w_MVACIVA3", cQueryName = "MVACIVA3",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 135448327,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=91, Top=226, InputMask=replicate('X',5), cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_MVACIVA3"

  func oMVACIVA3_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oMVAIMPN3_1_30 as StdField with uid="BKMUEYYAZY",rtseq=25,rtrep=.f.,;
    cFormVar = "w_MVAIMPN3", cQueryName = "MVAIMPN3",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 231524103,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=155, Top=226, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oMVAIMPN3_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_MVACIVA3))
    endwith
   endif
  endfunc

  add object oPERIV3_1_31 as StdField with uid="MHCHFMDLAI",rtseq=26,rtrep=.f.,;
    cFormVar = "w_PERIV3", cQueryName = "PERIV3",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 171689994,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=303, Top=226, cSayPict='"99.99"', cGetPict='"99.99"'

  add object oMVAIMPS3_1_32 as StdField with uid="RHSAMKSKFT",rtseq=27,rtrep=.f.,;
    cFormVar = "w_MVAIMPS3", cQueryName = "MVAIMPS3",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 36911353,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=360, Top=226, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oMVAIMPS3_1_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_MVACIVA3))
    endwith
   endif
  endfunc

  add object oMVACIVA4_1_33 as StdField with uid="ZCVGDWYKKV",rtseq=28,rtrep=.f.,;
    cFormVar = "w_MVACIVA4", cQueryName = "MVACIVA4",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 135448326,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=91, Top=250, InputMask=replicate('X',5), cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_MVACIVA4"

  func oMVACIVA4_1_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oMVAIMPN4_1_34 as StdField with uid="UYDHXMLCNZ",rtseq=29,rtrep=.f.,;
    cFormVar = "w_MVAIMPN4", cQueryName = "MVAIMPN4",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 231524102,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=155, Top=250, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oMVAIMPN4_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_MVACIVA4))
    endwith
   endif
  endfunc

  add object oPERIV4_1_35 as StdField with uid="CNSITHEONO",rtseq=30,rtrep=.f.,;
    cFormVar = "w_PERIV4", cQueryName = "PERIV4",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 154912778,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=303, Top=250, cSayPict='"99.99"', cGetPict='"99.99"'

  add object oMVAIMPS4_1_36 as StdField with uid="YDFNWFZSBH",rtseq=31,rtrep=.f.,;
    cFormVar = "w_MVAIMPS4", cQueryName = "MVAIMPS4",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 36911354,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=360, Top=250, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oMVAIMPS4_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_MVACIVA4))
    endwith
   endif
  endfunc

  add object oMVACIVA5_1_37 as StdField with uid="WIIEVVCBDL",rtseq=32,rtrep=.f.,;
    cFormVar = "w_MVACIVA5", cQueryName = "MVACIVA5",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 135448325,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=91, Top=274, InputMask=replicate('X',5), cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_MVACIVA5"

  func oMVACIVA5_1_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oMVAIMPN5_1_38 as StdField with uid="ADCBYCOYLS",rtseq=33,rtrep=.f.,;
    cFormVar = "w_MVAIMPN5", cQueryName = "MVAIMPN5",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 231524101,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=155, Top=274, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oMVAIMPN5_1_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_MVACIVA5))
    endwith
   endif
  endfunc

  add object oPERIV5_1_39 as StdField with uid="ISRTBJPGQM",rtseq=34,rtrep=.f.,;
    cFormVar = "w_PERIV5", cQueryName = "PERIV5",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 138135562,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=303, Top=274, cSayPict='"99.99"', cGetPict='"99.99"'

  add object oMVAIMPS5_1_40 as StdField with uid="EOLZPNHNIC",rtseq=35,rtrep=.f.,;
    cFormVar = "w_MVAIMPS5", cQueryName = "MVAIMPS5",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 36911355,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=360, Top=274, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oMVAIMPS5_1_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_MVACIVA5))
    endwith
   endif
  endfunc

  add object oMVACIVA6_1_41 as StdField with uid="XIBDMUXBXJ",rtseq=36,rtrep=.f.,;
    cFormVar = "w_MVACIVA6", cQueryName = "MVACIVA6",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 135448324,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=91, Top=298, InputMask=replicate('X',5), cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_MVACIVA6"

  func oMVACIVA6_1_41.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oMVAIMPN6_1_42 as StdField with uid="AGPESMONAE",rtseq=37,rtrep=.f.,;
    cFormVar = "w_MVAIMPN6", cQueryName = "MVAIMPN6",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 231524100,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=155, Top=298, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oMVAIMPN6_1_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_MVACIVA6))
    endwith
   endif
  endfunc

  add object oPERIV6_1_43 as StdField with uid="OSDCGTQLZC",rtseq=38,rtrep=.f.,;
    cFormVar = "w_PERIV6", cQueryName = "PERIV6",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 121358346,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=303, Top=298, cSayPict='"99.99"', cGetPict='"99.99"'

  add object oMVAIMPS6_1_44 as StdField with uid="FYDTVVJXXK",rtseq=39,rtrep=.f.,;
    cFormVar = "w_MVAIMPS6", cQueryName = "MVAIMPS6",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 36911356,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=360, Top=298, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oMVAIMPS6_1_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_MVACIVA6))
    endwith
   endif
  endfunc

  add object oMVAIMPS_1_47 as StdField with uid="CGWLPERUMU",rtseq=40,rtrep=.f.,;
    cFormVar = "w_MVAIMPS", cQueryName = "MVAIMPS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 36911302,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=360, Top=333, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  add object oMVAIMPN_1_48 as StdField with uid="SHDXOJNPFN",rtseq=41,rtrep=.f.,;
    cFormVar = "w_MVAIMPN", cQueryName = "MVAIMPN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 231524154,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=155, Top=333, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"


  add object oBtn_1_49 as StdButton with uid="NDZWWZOFWJ",left=400, top=358, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare le scelte impostate";
    , HelpContextID = 219561446;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_49.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_50 as StdButton with uid="FRBQVUSNWZ",left=451, top=358, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire senza confermare le scelte";
    , HelpContextID = 226850118;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_50.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oFLOM1_1_51 as StdField with uid="PETHDTESBT",rtseq=42,rtrep=.f.,;
    cFormVar = "w_FLOM1", cQueryName = "FLOM1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    HelpContextID = 260567466,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=6, Top=178, InputMask=replicate('X',8)

  add object oFLOM2_1_52 as StdField with uid="KXADLSMDBZ",rtseq=43,rtrep=.f.,;
    cFormVar = "w_FLOM2", cQueryName = "FLOM2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    HelpContextID = 259518890,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=6, Top=202, InputMask=replicate('X',8)

  add object oFLOM3_1_53 as StdField with uid="TUUQLUQHNE",rtseq=44,rtrep=.f.,;
    cFormVar = "w_FLOM3", cQueryName = "FLOM3",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    HelpContextID = 258470314,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=6, Top=226, InputMask=replicate('X',8)

  add object oFLOM4_1_54 as StdField with uid="PUFWJBISFN",rtseq=45,rtrep=.f.,;
    cFormVar = "w_FLOM4", cQueryName = "FLOM4",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    HelpContextID = 257421738,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=6, Top=250, InputMask=replicate('X',8)

  add object oFLOM5_1_55 as StdField with uid="RUQEMVTYWM",rtseq=46,rtrep=.f.,;
    cFormVar = "w_FLOM5", cQueryName = "FLOM5",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    HelpContextID = 256373162,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=6, Top=274, InputMask=replicate('X',8)

  add object oFLOM6_1_56 as StdField with uid="QRCAIKYZKD",rtseq=47,rtrep=.f.,;
    cFormVar = "w_FLOM6", cQueryName = "FLOM6",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    HelpContextID = 255324586,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=6, Top=298, InputMask=replicate('X',8)

  add object oTIDESCRI_1_71 as StdField with uid="VCQTSWLLPF",rtseq=58,rtrep=.f.,;
    cFormVar = "w_TIDESCRI", cQueryName = "TIDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 175154049,;
   bGlobalFont=.t.,;
    Height=21, Width=257, Left=242, Top=121, InputMask=replicate('X',30)

  add object oMVIVACAU_1_74 as StdField with uid="AQGJQBWGTM",rtseq=61,rtrep=.f.,;
    cFormVar = "w_MVIVACAU", cQueryName = "MVIVACAU",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice non esente, inesistente oppure obsoleto",;
    ToolTipText = "Codice IVA cauzioni",;
    HelpContextID = 192890597,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=156, Top=94, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_MVIVACAU"

  func oMVIVACAU_1_74.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!IsAlt())
    endwith
   endif
  endfunc

  func oMVIVACAU_1_74.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oMVIVACAU_1_74.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_74('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVIVACAU_1_74.ecpDrop(oSource)
    this.Parent.oContained.link_1_74('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVIVACAU_1_74.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oMVIVACAU_1_74'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Codici IVA",'GSAR_ZVE.VOCIIVA_VZM',this.parent.oContained
  endproc
  proc oMVIVACAU_1_74.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_MVIVACAU
     i_obj.ecpSave()
  endproc


  add object Imballo as cp_calclbl with uid="KKSANTQQQE",left=3, top=66, width=150,height=25,;
    caption='Imballo',;
   bGlobalFont=.t.,;
    caption="label text",fontname="Arial",fontsize=9,fontBold=.f.,fontItalic=.f.,fontUnderline=.f.,Alignment=1,;
    nPag=1;
    , HelpContextID = 264413306


  add object trasporto as cp_calclbl with uid="YXCGECFUXS",left=215, top=40, width=166,height=25,;
    caption='trasporto',;
   bGlobalFont=.t.,;
    caption="label text",fontname="Arial",fontsize=9,fontBold=.f.,fontItalic=.f.,fontUnderline=.f.,Alignment=1,;
    nPag=1;
    , HelpContextID = 59727514

  add object oStr_1_21 as StdString with uid="YAFZLKUBZS",Visible=.t., Left=91, Top=153,;
    Alignment=2, Width=58, Height=15,;
    Caption="Cod.IVA"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="JOAPCBYVAO",Visible=.t., Left=155, Top=153,;
    Alignment=2, Width=142, Height=15,;
    Caption="Imponibile"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="AMPUGVYRTY",Visible=.t., Left=303, Top=153,;
    Alignment=2, Width=51, Height=15,;
    Caption="% IVA"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="MBDTTZKRKD",Visible=.t., Left=360, Top=153,;
    Alignment=2, Width=142, Height=15,;
    Caption="Imposta"  ;
  , bGlobalFont=.t.

  add object oStr_1_57 as StdString with uid="MJKVTMEZCX",Visible=.t., Left=8, Top=153,;
    Alignment=2, Width=79, Height=15,;
    Caption="Omaggio"  ;
  , bGlobalFont=.t.

  add object oStr_1_58 as StdString with uid="PXGYYNWVVN",Visible=.t., Left=222, Top=66,;
    Alignment=1, Width=159, Height=15,;
    Caption="Codice IVA spese bolli:"  ;
  , bGlobalFont=.t.

  func oStr_1_58.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_59 as StdString with uid="XLJDRJJSAK",Visible=.t., Left=6, Top=40,;
    Alignment=1, Width=147, Height=15,;
    Caption="Codice IVA sp.incasso:"  ;
  , bGlobalFont=.t.

  func oStr_1_59.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_70 as StdString with uid="KBKGCIJVZD",Visible=.t., Left=84, Top=121,;
    Alignment=1, Width=69, Height=18,;
    Caption="Tipo op. IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_75 as StdString with uid="JHGEVFDODH",Visible=.t., Left=7, Top=94,;
    Alignment=1, Width=146, Height=18,;
    Caption="Codice IVA cauzioni:"  ;
  , bGlobalFont=.t.

  func oStr_1_75.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_89 as StdString with uid="JQWUBEKMKR",Visible=.t., Left=274, Top=15,;
    Alignment=1, Width=107, Height=18,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  func oStr_1_89.mHide()
    with this.Parent.oContained
      return (Not .w_MVCLADOC $ 'FA-NC-FC')
    endwith
  endfunc

  add object oStr_1_90 as StdString with uid="AHPGGXAXCK",Visible=.t., Left=109, Top=15,;
    Alignment=1, Width=44, Height=15,;
    Caption="Mese:"  ;
  , bGlobalFont=.t.

  func oStr_1_90.mHide()
    with this.Parent.oContained
      return (Not .w_MVCLADOC $ 'FA-NC-FC')
    endwith
  endfunc

  add object oBox_1_45 as StdBox with uid="MSFZNSBZOJ",left=5, top=151, width=498,height=22

  add object oBox_1_46 as StdBox with uid="ZTLMSRDORE",left=5, top=325, width=499,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_kfa','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
