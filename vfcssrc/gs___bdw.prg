* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gs___bdw                                                        *
*              Elimina l'utente dalla tabella INF_AUQU                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-03-13                                                      *
* Last revis.: 2009-04-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgs___bdw",oParentObject)
return(i_retval)

define class tgs___bdw as StdBatch
  * --- Local variables
  * --- WorkFile variables
  INF_AUQU_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimina l'utente dalla tabella INF_AUQU 
    *     Questa routine � invocata da gs___BDU (non  si poteva lasciare l ' istruzione all interno della gs___BDU
    * --- Delete from INF_AUQU
    i_nConn=i_TableProp[this.INF_AUQU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_AUQU_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"QAFLUTGR = "+cp_ToStrODBC(this.oParentObject.w_FLUTEGRP);
            +" and QAUTEGRP = "+cp_ToStrODBC(pCODE);
             )
    else
      delete from (i_cTable) where;
            QAFLUTGR = this.oParentObject.w_FLUTEGRP;
            and QAUTEGRP = pCODE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='INF_AUQU'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
