* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_ksf                                                        *
*              Documenti da fatturare                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_53]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-04-03                                                      *
* Last revis.: 2007-07-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsve_ksf",oParentObject))

* --- Class definition
define class tgsve_ksf as StdForm
  Top    = 81
  Left   = 4

  * --- Standard Properties
  Width  = 755
  Height = 398
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-17"
  HelpContextID=99234409
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=30

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsve_ksf"
  cComment = "Documenti da fatturare"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_DATRIF = ctod('  /  /  ')
  w_CAUDOC = space(5)
  w_CODCLI = space(15)
  w_CODDES = space(5)
  w_CODZON = space(3)
  w_CODAGE = space(5)
  w_CODVAL = space(3)
  w_FLVALO = space(1)
  w_XVALO = 0
  w_SERIAL = space(10)
  w_SELEZI = space(1)
  w_TESTERR = .F.
  w_SELEZ1 = space(1)
  w_CODPAG = space(5)
  w_CATCON = space(5)
  w_CONSUP = space(15)
  w_CODCLF = space(15)
  w_NUMDO1 = 0
  w_NUMDO2 = 0
  w_ALFDO1 = space(10)
  w_ALFDO2 = space(10)
  w_DATINI = ctod('  /  /  ')
  w_CATCOM = space(3)
  w_TIPOFAT = space(1)
  w_ESCL1 = space(3)
  w_ESCL3 = space(3)
  w_ESCL2 = space(3)
  w_ESCL4 = space(3)
  w_ESCL5 = space(3)
  w_PSTIPDOC = space(5)
  w_ZoomMast = .NULL.
  w_ZoomDett = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_ksfPag1","gsve_ksf",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSELEZI_1_17
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomMast = this.oPgFrm.Pages(1).oPag.ZoomMast
    this.w_ZoomDett = this.oPgFrm.Pages(1).oPag.ZoomDett
    DoDefault()
    proc Destroy()
      this.w_ZoomMast = .NULL.
      this.w_ZoomDett = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DATRIF=ctod("  /  /  ")
      .w_CAUDOC=space(5)
      .w_CODCLI=space(15)
      .w_CODDES=space(5)
      .w_CODZON=space(3)
      .w_CODAGE=space(5)
      .w_CODVAL=space(3)
      .w_FLVALO=space(1)
      .w_XVALO=0
      .w_SERIAL=space(10)
      .w_SELEZI=space(1)
      .w_TESTERR=.f.
      .w_SELEZ1=space(1)
      .w_CODPAG=space(5)
      .w_CATCON=space(5)
      .w_CONSUP=space(15)
      .w_CODCLF=space(15)
      .w_NUMDO1=0
      .w_NUMDO2=0
      .w_ALFDO1=space(10)
      .w_ALFDO2=space(10)
      .w_DATINI=ctod("  /  /  ")
      .w_CATCOM=space(3)
      .w_TIPOFAT=space(1)
      .w_ESCL1=space(3)
      .w_ESCL3=space(3)
      .w_ESCL2=space(3)
      .w_ESCL4=space(3)
      .w_ESCL5=space(3)
      .w_PSTIPDOC=space(5)
      .w_DATRIF=oParentObject.w_DATRIF
      .w_CAUDOC=oParentObject.w_CAUDOC
      .w_CODCLI=oParentObject.w_CODCLI
      .w_CODDES=oParentObject.w_CODDES
      .w_CODZON=oParentObject.w_CODZON
      .w_CODAGE=oParentObject.w_CODAGE
      .w_CODVAL=oParentObject.w_CODVAL
      .w_FLVALO=oParentObject.w_FLVALO
      .w_XVALO=oParentObject.w_XVALO
      .w_CODPAG=oParentObject.w_CODPAG
      .w_CATCON=oParentObject.w_CATCON
      .w_CONSUP=oParentObject.w_CONSUP
      .w_CODCLF=oParentObject.w_CODCLF
      .w_NUMDO1=oParentObject.w_NUMDO1
      .w_NUMDO2=oParentObject.w_NUMDO2
      .w_ALFDO1=oParentObject.w_ALFDO1
      .w_ALFDO2=oParentObject.w_ALFDO2
      .w_DATINI=oParentObject.w_DATINI
      .w_CATCOM=oParentObject.w_CATCOM
      .w_TIPOFAT=oParentObject.w_TIPOFAT
      .w_ESCL1=oParentObject.w_ESCL1
      .w_ESCL3=oParentObject.w_ESCL3
      .w_ESCL2=oParentObject.w_ESCL2
      .w_ESCL4=oParentObject.w_ESCL4
      .w_ESCL5=oParentObject.w_ESCL5
      .w_PSTIPDOC=oParentObject.w_PSTIPDOC
      .oPgFrm.Page1.oPag.ZoomMast.Calculate()
          .DoRTCalc(1,9,.f.)
        .w_SERIAL = .w_ZoomMast.getVar('MVSERIAL')
      .oPgFrm.Page1.oPag.ZoomDett.Calculate(.w_SERIAL)
        .w_SELEZI = 'S'
      .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_25.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_26.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .w_TESTERR = .T.
        .w_SELEZ1 = 'S'
      .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_51.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_52.Calculate()
    endwith
    this.DoRTCalc(14,30,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_DATRIF=.w_DATRIF
      .oParentObject.w_CAUDOC=.w_CAUDOC
      .oParentObject.w_CODCLI=.w_CODCLI
      .oParentObject.w_CODDES=.w_CODDES
      .oParentObject.w_CODZON=.w_CODZON
      .oParentObject.w_CODAGE=.w_CODAGE
      .oParentObject.w_CODVAL=.w_CODVAL
      .oParentObject.w_FLVALO=.w_FLVALO
      .oParentObject.w_XVALO=.w_XVALO
      .oParentObject.w_CODPAG=.w_CODPAG
      .oParentObject.w_CATCON=.w_CATCON
      .oParentObject.w_CONSUP=.w_CONSUP
      .oParentObject.w_CODCLF=.w_CODCLF
      .oParentObject.w_NUMDO1=.w_NUMDO1
      .oParentObject.w_NUMDO2=.w_NUMDO2
      .oParentObject.w_ALFDO1=.w_ALFDO1
      .oParentObject.w_ALFDO2=.w_ALFDO2
      .oParentObject.w_DATINI=.w_DATINI
      .oParentObject.w_CATCOM=.w_CATCOM
      .oParentObject.w_TIPOFAT=.w_TIPOFAT
      .oParentObject.w_ESCL1=.w_ESCL1
      .oParentObject.w_ESCL3=.w_ESCL3
      .oParentObject.w_ESCL2=.w_ESCL2
      .oParentObject.w_ESCL4=.w_ESCL4
      .oParentObject.w_ESCL5=.w_ESCL5
      .oParentObject.w_PSTIPDOC=.w_PSTIPDOC
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZoomMast.Calculate()
        .DoRTCalc(1,9,.t.)
            .w_SERIAL = .w_ZoomMast.getVar('MVSERIAL')
        .oPgFrm.Page1.oPag.ZoomDett.Calculate(.w_SERIAL)
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_51.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(11,30,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomMast.Calculate()
        .oPgFrm.Page1.oPag.ZoomDett.Calculate(.w_SERIAL)
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_51.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomMast.Event(cEvent)
      .oPgFrm.Page1.oPag.ZoomDett.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_23.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_24.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_25.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_26.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_27.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_28.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_29.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_30.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_31.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_43.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_51.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_52.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_17.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZ1_1_33.RadioValue()==this.w_SELEZ1)
      this.oPgFrm.Page1.oPag.oSELEZ1_1_33.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- gsve_ksf
      * --- Controlli Finali
      this.w_TESTERR=.T.
      this.NotifyEvent('ControlliFinali')
      return (this.w_TESTERR)
      
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsve_ksfPag1 as StdContainer
  Width  = 751
  height = 398
  stdWidth  = 751
  stdheight = 398
  resizeXpos=481
  resizeYpos=272
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZoomMast as cp_szoombox with uid="LKUNOBCBAW",left=-3, top=32, width=300,height=314,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="DOC_MAST",cZoomFile="GSVE_KFD",bOptions=.f.,bQueryOnLoad=.f.,bAdvOptions=.t.,cMenuFile="GSVE_KSF",cZoomOnZoom="GSZM_BZM",bReadOnly=.t.,;
    cEvent = "Calcola",;
    nPag=1;
    , HelpContextID = 78763238


  add object oBtn_1_14 as StdButton with uid="VTFQVDXIAK",left=642, top=350, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare le scelte effettuate";
    , HelpContextID = 99205658;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_15 as StdButton with uid="TPELMCRUTQ",left=695, top=350, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 91916986;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZoomDett as cp_szoombox with uid="YLDWNQYZQB",left=287, top=32, width=468,height=314,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="DOC_DETT",cZoomFile="GSVEDKFD",bOptions=.f.,bQueryOnLoad=.f.,bReadOnly=.f.,bAdvOptions=.f.,cMenuFile="",cZoomOnZoom="GSZM_BZM",;
    cEvent = "CalcRig",;
    nPag=1;
    , HelpContextID = 78763238

  add object oSELEZI_1_17 as StdRadio with uid="RKZNQKSJJB",rtseq=11,rtrep=.f.,left=287, top=354, width=134,height=32;
    , tabstop=.f.;
    , ToolTipText = "Seleziona/deseleziona le righe del documento";
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_17.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 150984742
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 150984742
      this.Buttons(2).Top=15
      this.SetAll("Width",132)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona/deseleziona le righe del documento")
      StdRadio::init()
    endproc

  func oSELEZI_1_17.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_17.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_17.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc


  add object oObj_1_23 as cp_runprogram with uid="LYEJSWZGES",left=566, top=400, width=125,height=18,;
    caption='GSVE_BSF',;
   bGlobalFont=.t.,;
    prg="GSVE_BSF",;
    cEvent = "Blank",;
    nPag=1;
    , HelpContextID = 38831532


  add object oObj_1_24 as cp_runprogram with uid="LXVKXWVOCA",left=3, top=402, width=272,height=18,;
    caption='GSVE_BS1(MRC)',;
   bGlobalFont=.t.,;
    prg='GSVE_BS1("MRC")',;
    cEvent = "ZOOMMAST row checked",;
    nPag=1;
    , HelpContextID = 86570263


  add object oObj_1_25 as cp_runprogram with uid="HOKIHYQVNB",left=278, top=401, width=286,height=18,;
    caption='GSVE_BS1(MRU)',;
   bGlobalFont=.t.,;
    prg='GSVE_BS1("MRU")',;
    cEvent = "ZOOMMAST row unchecked",;
    nPag=1;
    , HelpContextID = 87749911


  add object oObj_1_26 as cp_runprogram with uid="RPWXUUYNDS",left=279, top=420, width=288,height=18,;
    caption='GSVE_BS1(DBQ)',;
   bGlobalFont=.t.,;
    prg='GSVE_BS1("DBQ")',;
    cEvent = "ZOOMDETT before query",;
    nPag=1;
    , HelpContextID = 87419927


  add object oObj_1_27 as cp_runprogram with uid="WHVXORWNXO",left=3, top=439, width=273,height=18,;
    caption='GSVE_BS1(DAQ)',;
   bGlobalFont=.t.,;
    prg='GSVE_BS1("DAQ")',;
    cEvent = "ZOOMDETT after query",;
    nPag=1;
    , HelpContextID = 87415831


  add object oObj_1_28 as cp_runprogram with uid="ZPMIBVJNUJ",left=3, top=421, width=272,height=18,;
    caption='GSVE_BS1(DRC)',;
   bGlobalFont=.t.,;
    prg='GSVE_BS1("DRC")',;
    cEvent = "ZOOMDETT row checked",;
    nPag=1;
    , HelpContextID = 86567959


  add object oObj_1_29 as cp_runprogram with uid="CDTKJIBFBQ",left=279, top=439, width=287,height=18,;
    caption='GSVE_BS1(SCH)',;
   bGlobalFont=.t.,;
    prg='GSVE_BS1("SCH")',;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 86838039


  add object oObj_1_30 as cp_runprogram with uid="WGNUIODTHP",left=570, top=440, width=175,height=18,;
    caption='GSVE_BS2(U)',;
   bGlobalFont=.t.,;
    prg='GSVE_BS2("U")',;
    cEvent = "ControlliFinali",;
    nPag=1;
    , HelpContextID = 39021848


  add object oObj_1_31 as cp_runprogram with uid="YYYKBVBQIY",left=569, top=420, width=172,height=20,;
    caption='GSVE_BS2(A)',;
   bGlobalFont=.t.,;
    prg='GSVE_BS2("A")',;
    cEvent = "Edit Aborted",;
    nPag=1;
    , HelpContextID = 39016728

  add object oSELEZ1_1_33 as StdRadio with uid="ANDNPPMZXM",rtseq=13,rtrep=.f.,left=8, top=352, width=136,height=32;
    , tabstop=.f.;
    , ToolTipText = "Seleziona/deseleziona le righe del documento";
    , cFormVar="w_SELEZ1", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZ1_1_33.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 16767014
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 16767014
      this.Buttons(2).Top=15
      this.SetAll("Width",134)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona/deseleziona le righe del documento")
      StdRadio::init()
    endproc

  func oSELEZ1_1_33.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZ1_1_33.GetRadio()
    this.Parent.oContained.w_SELEZ1 = this.RadioValue()
    return .t.
  endfunc

  func oSELEZ1_1_33.SetRadio()
    this.Parent.oContained.w_SELEZ1=trim(this.Parent.oContained.w_SELEZ1)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZ1=='S',1,;
      iif(this.Parent.oContained.w_SELEZ1=='D',2,;
      0))
  endfunc


  add object oObj_1_43 as cp_runprogram with uid="OOTGDDFVJF",left=280, top=459, width=233,height=21,;
    caption='GSVE_BS1(SC1)',;
   bGlobalFont=.t.,;
    prg="GSVE_BS1('SC1')",;
    cEvent = "w_SELEZ1 Changed",;
    nPag=1;
    , HelpContextID = 85330711


  add object oObj_1_51 as cp_runprogram with uid="ZFRIHHQFWR",left=4, top=462, width=265,height=18,;
    caption='GSVE_BS1(SFA)',;
   bGlobalFont=.t.,;
    prg="GSVE_BS1('SFA')",;
    cEvent = "SeleFin",;
    nPag=1;
    , HelpContextID = 86391575


  add object oObj_1_52 as cp_runprogram with uid="ZTWOXEPDRP",left=5, top=482, width=265,height=21,;
    caption='GSVE_BS1(SF1)',;
   bGlobalFont=.t.,;
    prg="GSVE_BS1('SF1')",;
    cEvent = "DeseleFin",;
    nPag=1;
    , HelpContextID = 85342999

  add object oStr_1_10 as StdString with uid="SIBSRJHMHW",Visible=.t., Left=4, Top=15,;
    Alignment=0, Width=281, Height=15,;
    Caption="Elenco documenti"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="SMHJYQHTYM",Visible=.t., Left=290, Top=15,;
    Alignment=0, Width=105, Height=15,;
    Caption="Dettaglio righe"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="ZZDPRVHNID",Visible=.t., Left=616, Top=2,;
    Alignment=0, Width=48, Height=15,;
    Caption="(Rosso)"    , ForeColor=RGB(255,0,0);
  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_19 as StdString with uid="MVHMCPUMIP",Visible=.t., Left=467, Top=2,;
    Alignment=0, Width=45, Height=15,;
    Caption="(Nero)"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_20 as StdString with uid="WFSQUAIGDC",Visible=.t., Left=511, Top=2,;
    Alignment=0, Width=101, Height=15,;
    Caption="Non selezionata;"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="QANVDAYTYX",Visible=.t., Left=666, Top=2,;
    Alignment=0, Width=77, Height=15,;
    Caption="Selezionata"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="YSSLNOTZDI",Visible=.t., Left=365, Top=2,;
    Alignment=1, Width=103, Height=15,;
    Caption="Legenda righe:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_ksf','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
