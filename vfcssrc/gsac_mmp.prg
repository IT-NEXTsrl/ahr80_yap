* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsac_mmp                                                        *
*              Materiali da produzione                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_206]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-05-15                                                      *
* Last revis.: 2018-01-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsac_mmp")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsac_mmp")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsac_mmp")
  return

* --- Class definition
define class tgsac_mmp as StdPCForm
  Width  = 816
  Height = 371
  Top    = -2
  Left   = 3
  cComment = "Materiali da produzione"
  cPrg = "gsac_mmp"
  HelpContextID=70621335
  add object cnt as tcgsac_mmp
  * --- Area Manuale = Declare Variables StdPCForm
  * --- gsac_mmp
    proc ecpQuit()
      This.ecpSave()
    endproc
  * --- Fine Area Manuale
enddefine

define class tsgsac_mmp as PCContext
  w_MPSERIAL = space(10)
  w_ReadPar = space(2)
  w_MAGPRO = space(5)
  w_CPROWORD = 0
  w_MPCODICE = space(20)
  w_MPCODART = space(20)
  w_UNMIS3 = space(3)
  w_OPERA3 = space(1)
  w_MOLTI3 = 0
  w_DESART = space(40)
  w_UNMIS1 = space(3)
  w_OPERAT = space(1)
  w_MOLTIP = 0
  w_UNMIS2 = space(3)
  w_FLCOMM = space(10)
  w_MPUNIMIS = space(3)
  w_MPCOEIMP = 0
  w_MPQTAMOV = 0
  w_MPQTAEVA = 0
  w_MPFLEVAS = space(1)
  w_MPCODMAG = space(5)
  w_QTARES = 0
  w_MPQTAUM1 = 0
  w_MPSERODL = space(15)
  w_MPROWODL = 0
  w_MATCOM = space(1)
  w_OBTEST = space(8)
  w_CHANGE = space(1)
  w_MPQTAEV1 = 0
  w_MODUM2 = space(1)
  w_FLUSEP = space(1)
  w_NOFRAZ = space(1)
  w_MPNUMRIF = 0
  w_FLLOTT = space(1)
  w_FLUBI = space(1)
  w_MATCOM = space(1)
  w_MPROWDOC = 0
  w_DATREG = space(8)
  w_QTAUM1 = 0
  w_MTCARI = space(1)
  w_RIGMOVLOT = space(1)
  w_MPKEYSAL = space(20)
  w_IMPLOT = space(1)
  w_MPCODCOM = space(15)
  proc Save(i_oFrom)
    this.w_MPSERIAL = i_oFrom.w_MPSERIAL
    this.w_ReadPar = i_oFrom.w_ReadPar
    this.w_MAGPRO = i_oFrom.w_MAGPRO
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_MPCODICE = i_oFrom.w_MPCODICE
    this.w_MPCODART = i_oFrom.w_MPCODART
    this.w_UNMIS3 = i_oFrom.w_UNMIS3
    this.w_OPERA3 = i_oFrom.w_OPERA3
    this.w_MOLTI3 = i_oFrom.w_MOLTI3
    this.w_DESART = i_oFrom.w_DESART
    this.w_UNMIS1 = i_oFrom.w_UNMIS1
    this.w_OPERAT = i_oFrom.w_OPERAT
    this.w_MOLTIP = i_oFrom.w_MOLTIP
    this.w_UNMIS2 = i_oFrom.w_UNMIS2
    this.w_FLCOMM = i_oFrom.w_FLCOMM
    this.w_MPUNIMIS = i_oFrom.w_MPUNIMIS
    this.w_MPCOEIMP = i_oFrom.w_MPCOEIMP
    this.w_MPQTAMOV = i_oFrom.w_MPQTAMOV
    this.w_MPQTAEVA = i_oFrom.w_MPQTAEVA
    this.w_MPFLEVAS = i_oFrom.w_MPFLEVAS
    this.w_MPCODMAG = i_oFrom.w_MPCODMAG
    this.w_QTARES = i_oFrom.w_QTARES
    this.w_MPQTAUM1 = i_oFrom.w_MPQTAUM1
    this.w_MPSERODL = i_oFrom.w_MPSERODL
    this.w_MPROWODL = i_oFrom.w_MPROWODL
    this.w_MATCOM = i_oFrom.w_MATCOM
    this.w_OBTEST = i_oFrom.w_OBTEST
    this.w_CHANGE = i_oFrom.w_CHANGE
    this.w_MPQTAEV1 = i_oFrom.w_MPQTAEV1
    this.w_MODUM2 = i_oFrom.w_MODUM2
    this.w_FLUSEP = i_oFrom.w_FLUSEP
    this.w_NOFRAZ = i_oFrom.w_NOFRAZ
    this.w_MPNUMRIF = i_oFrom.w_MPNUMRIF
    this.w_FLLOTT = i_oFrom.w_FLLOTT
    this.w_FLUBI = i_oFrom.w_FLUBI
    this.w_MATCOM = i_oFrom.w_MATCOM
    this.w_MPROWDOC = i_oFrom.w_MPROWDOC
    this.w_DATREG = i_oFrom.w_DATREG
    this.w_QTAUM1 = i_oFrom.w_QTAUM1
    this.w_MTCARI = i_oFrom.w_MTCARI
    this.w_RIGMOVLOT = i_oFrom.w_RIGMOVLOT
    this.w_MPKEYSAL = i_oFrom.w_MPKEYSAL
    this.w_IMPLOT = i_oFrom.w_IMPLOT
    this.w_MPCODCOM = i_oFrom.w_MPCODCOM
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_MPSERIAL = this.w_MPSERIAL
    i_oTo.w_ReadPar = this.w_ReadPar
    i_oTo.w_MAGPRO = this.w_MAGPRO
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_MPCODICE = this.w_MPCODICE
    i_oTo.w_MPCODART = this.w_MPCODART
    i_oTo.w_UNMIS3 = this.w_UNMIS3
    i_oTo.w_OPERA3 = this.w_OPERA3
    i_oTo.w_MOLTI3 = this.w_MOLTI3
    i_oTo.w_DESART = this.w_DESART
    i_oTo.w_UNMIS1 = this.w_UNMIS1
    i_oTo.w_OPERAT = this.w_OPERAT
    i_oTo.w_MOLTIP = this.w_MOLTIP
    i_oTo.w_UNMIS2 = this.w_UNMIS2
    i_oTo.w_FLCOMM = this.w_FLCOMM
    i_oTo.w_MPUNIMIS = this.w_MPUNIMIS
    i_oTo.w_MPCOEIMP = this.w_MPCOEIMP
    i_oTo.w_MPQTAMOV = this.w_MPQTAMOV
    i_oTo.w_MPQTAEVA = this.w_MPQTAEVA
    i_oTo.w_MPFLEVAS = this.w_MPFLEVAS
    i_oTo.w_MPCODMAG = this.w_MPCODMAG
    i_oTo.w_QTARES = this.w_QTARES
    i_oTo.w_MPQTAUM1 = this.w_MPQTAUM1
    i_oTo.w_MPSERODL = this.w_MPSERODL
    i_oTo.w_MPROWODL = this.w_MPROWODL
    i_oTo.w_MATCOM = this.w_MATCOM
    i_oTo.w_OBTEST = this.w_OBTEST
    i_oTo.w_CHANGE = this.w_CHANGE
    i_oTo.w_MPQTAEV1 = this.w_MPQTAEV1
    i_oTo.w_MODUM2 = this.w_MODUM2
    i_oTo.w_FLUSEP = this.w_FLUSEP
    i_oTo.w_NOFRAZ = this.w_NOFRAZ
    i_oTo.w_MPNUMRIF = this.w_MPNUMRIF
    i_oTo.w_FLLOTT = this.w_FLLOTT
    i_oTo.w_FLUBI = this.w_FLUBI
    i_oTo.w_MATCOM = this.w_MATCOM
    i_oTo.w_MPROWDOC = this.w_MPROWDOC
    i_oTo.w_DATREG = this.w_DATREG
    i_oTo.w_QTAUM1 = this.w_QTAUM1
    i_oTo.w_MTCARI = this.w_MTCARI
    i_oTo.w_RIGMOVLOT = this.w_RIGMOVLOT
    i_oTo.w_MPKEYSAL = this.w_MPKEYSAL
    i_oTo.w_IMPLOT = this.w_IMPLOT
    i_oTo.w_MPCODCOM = this.w_MPCODCOM
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsac_mmp as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 816
  Height = 371
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-01-22"
  HelpContextID=70621335
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=44

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  MAT_PROD_IDX = 0
  MAGAZZIN_IDX = 0
  UNIMIS_IDX = 0
  KEY_ARTI_IDX = 0
  ART_ICOL_IDX = 0
  PAR_PROD_IDX = 0
  CAN_TIER_IDX = 0
  cFile = "MAT_PROD"
  cKeySelect = "MPSERIAL,MPNUMRIF,MPROWDOC"
  cKeyWhere  = "MPSERIAL=this.w_MPSERIAL and MPNUMRIF=this.w_MPNUMRIF and MPROWDOC=this.w_MPROWDOC"
  cKeyDetail  = "MPSERIAL=this.w_MPSERIAL and MPNUMRIF=this.w_MPNUMRIF and MPROWDOC=this.w_MPROWDOC"
  cKeyWhereODBC = '"MPSERIAL="+cp_ToStrODBC(this.w_MPSERIAL)';
      +'+" and MPNUMRIF="+cp_ToStrODBC(this.w_MPNUMRIF)';
      +'+" and MPROWDOC="+cp_ToStrODBC(this.w_MPROWDOC)';

  cKeyDetailWhereODBC = '"MPSERIAL="+cp_ToStrODBC(this.w_MPSERIAL)';
      +'+" and MPNUMRIF="+cp_ToStrODBC(this.w_MPNUMRIF)';
      +'+" and MPROWDOC="+cp_ToStrODBC(this.w_MPROWDOC)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"MAT_PROD.MPSERIAL="+cp_ToStrODBC(this.w_MPSERIAL)';
      +'+" and MAT_PROD.MPNUMRIF="+cp_ToStrODBC(this.w_MPNUMRIF)';
      +'+" and MAT_PROD.MPROWDOC="+cp_ToStrODBC(this.w_MPROWDOC)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'MAT_PROD.CPROWORD '
  cPrg = "gsac_mmp"
  cComment = "Materiali da produzione"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 16
  icon = "movi.ico"
  i_lastcheckrow = 0
  windowtype = 1
  minbutton = .f.
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_MPSERIAL = space(10)
  w_ReadPar = space(2)
  w_MAGPRO = space(5)
  w_CPROWORD = 0
  w_MPCODICE = space(20)
  o_MPCODICE = space(20)
  w_MPCODART = space(20)
  w_UNMIS3 = space(3)
  w_OPERA3 = space(1)
  w_MOLTI3 = 0
  w_DESART = space(40)
  w_UNMIS1 = space(3)
  w_OPERAT = space(1)
  w_MOLTIP = 0
  w_UNMIS2 = space(3)
  w_FLCOMM = space(10)
  w_MPUNIMIS = space(3)
  o_MPUNIMIS = space(3)
  w_MPCOEIMP = 0
  o_MPCOEIMP = 0
  w_MPQTAMOV = 0
  o_MPQTAMOV = 0
  w_MPQTAEVA = 0
  o_MPQTAEVA = 0
  w_MPFLEVAS = space(1)
  w_MPCODMAG = space(5)
  w_QTARES = 0
  w_MPQTAUM1 = 0
  w_MPSERODL = space(15)
  w_MPROWODL = 0
  w_MATCOM = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_CHANGE = .F.
  w_MPQTAEV1 = 0
  w_MODUM2 = space(1)
  w_FLUSEP = space(1)
  w_NOFRAZ = space(1)
  w_MPNUMRIF = 0
  w_FLLOTT = space(1)
  w_FLUBI = space(1)
  w_MATCOM = space(1)
  w_MPROWDOC = 0
  w_DATREG = ctod('  /  /  ')
  w_QTAUM1 = 0
  w_MTCARI = space(1)
  w_RIGMOVLOT = .F.
  w_MPKEYSAL = space(20)
  w_IMPLOT = space(1)
  w_MPCODCOM = space(15)

  * --- Children pointers
  GSAC_MLO = .NULL.
  GSAC_MCM = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsac_mmp
  func CanDeleteRow()
      This.w_CHANGE = True
      This.w_CHANGE = Empty(NVL(This.w_MPSERODL,''))
      If !This.w_CHANGE
        ah_ERRORMSG("Impossibile eliminare una riga caricata da OCL",48)
      Endif
      Return( This.w_CHANGE )
  EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    *set procedure to GSAC_MLO additive
    *set procedure to GSAC_MCM additive
    with this
      .Pages(1).addobject("oPag","tgsac_mmpPag1","gsac_mmp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    *release procedure GSAC_MLO
    *release procedure GSAC_MCM
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='MAGAZZIN'
    this.cWorkTables[2]='UNIMIS'
    this.cWorkTables[3]='KEY_ARTI'
    this.cWorkTables[4]='ART_ICOL'
    this.cWorkTables[5]='PAR_PROD'
    this.cWorkTables[6]='CAN_TIER'
    this.cWorkTables[7]='MAT_PROD'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(7))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MAT_PROD_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MAT_PROD_IDX,3]
  return

  function CreateChildren()
    this.GSAC_MLO = CREATEOBJECT('stdLazyChild',this,'GSAC_MLO')
    this.GSAC_MCM = CREATEOBJECT('stdLazyChild',this,'GSAC_MCM')
    return

  procedure NewContext()
    return(createobject('tsgsac_mmp'))

  function DestroyChildrenChain()
    this.oParentObject=.NULL.
    if !ISNULL(this.GSAC_MLO)
      this.GSAC_MLO.DestroyChildrenChain()
    endif
    if !ISNULL(this.GSAC_MCM)
      this.GSAC_MCM.DestroyChildrenChain()
    endif
    return

  function HideChildrenChain()
    *this.Hide()
    this.bOnScreen = .f.
    this.GSAC_MLO.HideChildrenChain()
    this.GSAC_MCM.HideChildrenChain()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSAC_MLO)
      this.GSAC_MLO.DestroyChildrenChain()
      this.GSAC_MLO=.NULL.
    endif
    if !ISNULL(this.GSAC_MCM)
      this.GSAC_MCM.DestroyChildrenChain()
      this.GSAC_MCM=.NULL.
    endif
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSAC_MLO.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSAC_MCM.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSAC_MLO.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSAC_MCM.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSAC_MLO.NewDocument()
    this.GSAC_MCM.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .GSAC_MLO.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_MPSERIAL,"CLDICHIA";
             ,.w_CPROWNUM,"CLROWCNM";
             ,.w_MPNUMRIF,"CLNUMRIF";
             ,.w_MPROWDOC,"CLROWDOC";
             )
      .GSAC_MCM.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_MPSERIAL,"MTSERIAL";
             ,.w_CPROWNUM,"MTROWNUM";
             ,.w_MPNUMRIF,"MTNUMRIF";
             ,.w_MPROWDOC,"MTROWDOC";
             )
    endwith
    select (i_cOldSel)
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_2_joined
    link_2_2_joined=.f.
    local link_2_3_joined
    link_2_3_joined=.f.
    local link_2_19_joined
    link_2_19_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from MAT_PROD where MPSERIAL=KeySet.MPSERIAL
    *                            and MPNUMRIF=KeySet.MPNUMRIF
    *                            and MPROWDOC=KeySet.MPROWDOC
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.MAT_PROD_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MAT_PROD_IDX,2],this.bLoadRecFilter,this.MAT_PROD_IDX,"gsac_mmp")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MAT_PROD')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MAT_PROD.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' MAT_PROD '
      link_2_2_joined=this.AddJoinedLink_2_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_19_joined=this.AddJoinedLink_2_19(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MPSERIAL',this.w_MPSERIAL  ,'MPNUMRIF',this.w_MPNUMRIF  ,'MPROWDOC',this.w_MPROWDOC  )
      select * from (i_cTable) MAT_PROD where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_OBTEST = ctod("  /  /  ")
        .w_FLUSEP = space(1)
        .w_DATREG = this.oParentObject .w_MVDATREG
        .w_RIGMOVLOT = .f.
        .w_IMPLOT = space(1)
        .w_MPSERIAL = NVL(MPSERIAL,space(10))
        .w_MPNUMRIF = NVL(MPNUMRIF,0)
        .w_MPROWDOC = NVL(MPROWDOC,0)
        .w_MTCARI = ' '
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'MAT_PROD')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
        .w_ReadPar = 'PP'
          .w_MAGPRO = space(5)
          .w_UNMIS3 = space(3)
          .w_OPERA3 = space(1)
          .w_MOLTI3 = 0
          .w_DESART = space(40)
          .w_UNMIS1 = space(3)
          .w_OPERAT = space(1)
          .w_MOLTIP = 0
          .w_UNMIS2 = space(3)
          .w_FLCOMM = space(10)
          .w_QTARES = 0
          .w_MATCOM = space(1)
        .w_CHANGE = False
          .w_MODUM2 = space(1)
          .w_NOFRAZ = space(1)
          .w_FLLOTT = space(1)
          .w_FLUBI = space(1)
          .w_MATCOM = space(1)
        .w_QTAUM1 = this.oParentObject .w_MVQTAUM1
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .link_1_2('Load')
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_MPCODICE = NVL(MPCODICE,space(20))
          if link_2_2_joined
            this.w_MPCODICE = NVL(CACODICE202,NVL(this.w_MPCODICE,space(20)))
            this.w_DESART = NVL(CADESART202,space(40))
            this.w_MPCODART = NVL(CACODART202,space(20))
            this.w_UNMIS3 = NVL(CAUNIMIS202,space(3))
            this.w_OPERA3 = NVL(CAOPERAT202,space(1))
            this.w_MOLTI3 = NVL(CAMOLTIP202,0)
            this.w_MPKEYSAL = NVL(CACODART202,space(20))
          else
          .link_2_2('Load')
          endif
          .w_MPCODART = NVL(MPCODART,space(20))
          if link_2_3_joined
            this.w_MPCODART = NVL(ARCODART203,NVL(this.w_MPCODART,space(20)))
            this.w_UNMIS1 = NVL(ARUNMIS1203,space(3))
            this.w_OPERAT = NVL(AROPERAT203,space(1))
            this.w_MOLTIP = NVL(ARMOLTIP203,0)
            this.w_UNMIS2 = NVL(ARUNMIS2203,space(3))
            this.w_MATCOM = NVL(ARGESMAT203,space(1))
            this.w_FLUSEP = NVL(ARFLUSEP203,space(1))
            this.w_FLLOTT = NVL(ARFLLOTT203,space(1))
            this.w_MATCOM = NVL(ARGESMAT203,space(1))
            this.w_FLCOMM = NVL(ARSALCOM203,space(10))
          else
          .link_2_3('Load')
          endif
          .link_2_8('Load')
          .w_MPUNIMIS = NVL(MPUNIMIS,space(3))
          * evitabile
          *.link_2_13('Load')
          .w_MPCOEIMP = NVL(MPCOEIMP,0)
          .w_MPQTAMOV = NVL(MPQTAMOV,0)
          .w_MPQTAEVA = NVL(MPQTAEVA,0)
          .w_MPFLEVAS = NVL(MPFLEVAS,space(1))
          .w_MPCODMAG = NVL(MPCODMAG,space(5))
          if link_2_19_joined
            this.w_MPCODMAG = NVL(MGCODMAG219,NVL(this.w_MPCODMAG,space(5)))
            this.w_FLUBI = NVL(MGFLUBIC219,space(1))
          else
          .link_2_19('Load')
          endif
          .w_MPQTAUM1 = NVL(MPQTAUM1,0)
          .w_MPSERODL = NVL(MPSERODL,space(15))
          .w_MPROWODL = NVL(MPROWODL,0)
          .w_MPQTAEV1 = NVL(MPQTAEV1,0)
        .oPgFrm.Page1.oPag.oObj_2_33.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_34.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_35.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_36.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_37.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_38.Calculate()
          .w_MPKEYSAL = NVL(MPKEYSAL,space(20))
        .oPgFrm.Page1.oPag.oObj_2_43.Calculate()
          .w_MPCODCOM = NVL(MPCODCOM,space(15))
          * evitabile
          *.link_2_44('Load')
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_MTCARI = ' '
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_MPSERIAL=space(10)
      .w_ReadPar=space(2)
      .w_MAGPRO=space(5)
      .w_CPROWORD=10
      .w_MPCODICE=space(20)
      .w_MPCODART=space(20)
      .w_UNMIS3=space(3)
      .w_OPERA3=space(1)
      .w_MOLTI3=0
      .w_DESART=space(40)
      .w_UNMIS1=space(3)
      .w_OPERAT=space(1)
      .w_MOLTIP=0
      .w_UNMIS2=space(3)
      .w_FLCOMM=space(10)
      .w_MPUNIMIS=space(3)
      .w_MPCOEIMP=0
      .w_MPQTAMOV=0
      .w_MPQTAEVA=0
      .w_MPFLEVAS=space(1)
      .w_MPCODMAG=space(5)
      .w_QTARES=0
      .w_MPQTAUM1=0
      .w_MPSERODL=space(15)
      .w_MPROWODL=0
      .w_MATCOM=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_CHANGE=.f.
      .w_MPQTAEV1=0
      .w_MODUM2=space(1)
      .w_FLUSEP=space(1)
      .w_NOFRAZ=space(1)
      .w_MPNUMRIF=0
      .w_FLLOTT=space(1)
      .w_FLUBI=space(1)
      .w_MATCOM=space(1)
      .w_MPROWDOC=0
      .w_DATREG=ctod("  /  /  ")
      .w_QTAUM1=0
      .w_MTCARI=space(1)
      .w_RIGMOVLOT=.f.
      .w_MPKEYSAL=space(20)
      .w_IMPLOT=space(1)
      .w_MPCODCOM=space(15)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        .w_ReadPar = 'PP'
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_ReadPar))
         .link_1_2('Full')
        endif
        .DoRTCalc(3,5,.f.)
        if not(empty(.w_MPCODICE))
         .link_2_2('Full')
        endif
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_MPCODART))
         .link_2_3('Full')
        endif
        .DoRTCalc(7,11,.f.)
        if not(empty(.w_UNMIS1))
         .link_2_8('Full')
        endif
        .DoRTCalc(12,15,.f.)
        .w_MPUNIMIS = IIF(NOT EMPTY(.w_UNMIS3) AND .w_MOLTI3<>0, .w_UNMIS3, .w_UNMIS1)
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_MPUNIMIS))
         .link_2_13('Full')
        endif
        .DoRTCalc(17,17,.f.)
        .w_MPQTAMOV = this.oParentObject .w_MVQTAUM1 * .w_MPCOEIMP
        .DoRTCalc(19,20,.f.)
        .w_MPCODMAG = IIF(EMPTY(.w_MAGPRO), g_MAGAZI, .w_MAGPRO)
        .DoRTCalc(21,21,.f.)
        if not(empty(.w_MPCODMAG))
         .link_2_19('Full')
        endif
        .DoRTCalc(22,22,.f.)
        .w_MPQTAUM1 = CALQTAADV(.w_MPQTAMOV,.w_MPUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, 'N', .w_MODUM2, '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3,,,This, "MPQTAMOV")
        .DoRTCalc(24,27,.f.)
        .w_CHANGE = False
        .w_MPQTAEV1 = CALQTAADV(.w_MPQTAEVA,.w_MPUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, 'N', .w_MODUM2, '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3,,,This, "MPQTAEVA")
        .DoRTCalc(30,32,.f.)
        .w_MPNUMRIF = -30
        .oPgFrm.Page1.oPag.oObj_2_33.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_34.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_35.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_36.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_37.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_38.Calculate()
        .DoRTCalc(34,37,.f.)
        .w_DATREG = this.oParentObject .w_MVDATREG
        .w_QTAUM1 = this.oParentObject .w_MVQTAUM1
        .w_MTCARI = ' '
        .oPgFrm.Page1.oPag.oObj_2_43.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(41,43,.f.)
        .w_MPCODCOM = iif(NVL(.w_FLCOMM,'N')='S',this.oparentobject .w_MVCODCOM,space(15))
        .DoRTCalc(44,44,.f.)
        if not(empty(.w_MPCODCOM))
         .link_2_44('Full')
        endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'MAT_PROD')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oObj_2_33.enabled = i_bVal
      .Page1.oPag.oObj_2_34.enabled = i_bVal
      .Page1.oPag.oObj_2_35.enabled = i_bVal
      .Page1.oPag.oObj_2_36.enabled = i_bVal
      .Page1.oPag.oObj_2_37.enabled = i_bVal
      .Page1.oPag.oObj_2_38.enabled = i_bVal
      .Page1.oPag.oObj_2_43.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    this.GSAC_MLO.SetStatus(i_cOp)
    this.GSAC_MCM.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'MAT_PROD',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSAC_MLO.SetChildrenStatus(i_cOp)
  *  this.GSAC_MCM.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MAT_PROD_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MPSERIAL,"MPSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MPNUMRIF,"MPNUMRIF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MPROWDOC,"MPROWDOC",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(6);
      ,t_MPCODICE C(20);
      ,t_DESART C(40);
      ,t_MPUNIMIS C(3);
      ,t_MPCOEIMP N(12,5);
      ,t_MPQTAMOV N(12,3);
      ,t_MPQTAEVA N(12,3);
      ,t_MPFLEVAS N(3);
      ,t_MPCODMAG C(5);
      ,t_MPCODCOM C(15);
      ,CPROWNUM N(10);
      ,t_ReadPar C(2);
      ,t_MAGPRO C(5);
      ,t_MPCODART C(20);
      ,t_UNMIS3 C(3);
      ,t_OPERA3 C(1);
      ,t_MOLTI3 N(10,4);
      ,t_UNMIS1 C(3);
      ,t_OPERAT C(1);
      ,t_MOLTIP N(10,4);
      ,t_UNMIS2 C(3);
      ,t_FLCOMM C(10);
      ,t_QTARES N(12,3);
      ,t_MPQTAUM1 N(12,3);
      ,t_MPSERODL C(15);
      ,t_MPROWODL N(4);
      ,t_MATCOM C(1);
      ,t_CHANGE L(1);
      ,t_MPQTAEV1 N(12,3);
      ,t_MODUM2 C(1);
      ,t_NOFRAZ C(1);
      ,t_FLLOTT C(1);
      ,t_FLUBI C(1);
      ,t_QTAUM1 N(12,3);
      ,t_MPKEYSAL C(20);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsac_mmpbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMPCODICE_2_2.controlsource=this.cTrsName+'.t_MPCODICE'
    this.oPgFRm.Page1.oPag.oDESART_2_7.controlsource=this.cTrsName+'.t_DESART'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMPUNIMIS_2_13.controlsource=this.cTrsName+'.t_MPUNIMIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMPCOEIMP_2_14.controlsource=this.cTrsName+'.t_MPCOEIMP'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMPQTAMOV_2_15.controlsource=this.cTrsName+'.t_MPQTAMOV'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMPQTAEVA_2_16.controlsource=this.cTrsName+'.t_MPQTAEVA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMPFLEVAS_2_17.controlsource=this.cTrsName+'.t_MPFLEVAS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMPCODMAG_2_19.controlsource=this.cTrsName+'.t_MPCODMAG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMPCODCOM_2_44.controlsource=this.cTrsName+'.t_MPCODCOM'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(52)
    this.AddVLine(215)
    this.AddVLine(256)
    this.AddVLine(354)
    this.AddVLine(451)
    this.AddVLine(550)
    this.AddVLine(577)
    this.AddVLine(639)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MAT_PROD_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MAT_PROD_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MAT_PROD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MAT_PROD_IDX,2])
      *
      * insert into MAT_PROD
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MAT_PROD')
        i_extval=cp_InsertValODBCExtFlds(this,'MAT_PROD')
        i_cFldBody=" "+;
                  "(MPSERIAL,CPROWORD,MPCODICE,MPCODART,MPUNIMIS"+;
                  ",MPCOEIMP,MPQTAMOV,MPQTAEVA,MPFLEVAS,MPCODMAG"+;
                  ",MPQTAUM1,MPSERODL,MPROWODL,MPQTAEV1,MPNUMRIF"+;
                  ",MPROWDOC,MPKEYSAL,MPCODCOM,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_MPSERIAL)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBCNull(this.w_MPCODICE)+","+cp_ToStrODBCNull(this.w_MPCODART)+","+cp_ToStrODBCNull(this.w_MPUNIMIS)+;
             ","+cp_ToStrODBC(this.w_MPCOEIMP)+","+cp_ToStrODBC(this.w_MPQTAMOV)+","+cp_ToStrODBC(this.w_MPQTAEVA)+","+cp_ToStrODBC(this.w_MPFLEVAS)+","+cp_ToStrODBCNull(this.w_MPCODMAG)+;
             ","+cp_ToStrODBC(this.w_MPQTAUM1)+","+cp_ToStrODBC(this.w_MPSERODL)+","+cp_ToStrODBC(this.w_MPROWODL)+","+cp_ToStrODBC(this.w_MPQTAEV1)+","+cp_ToStrODBC(this.w_MPNUMRIF)+;
             ","+cp_ToStrODBC(this.w_MPROWDOC)+","+cp_ToStrODBC(this.w_MPKEYSAL)+","+cp_ToStrODBCNull(this.w_MPCODCOM)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MAT_PROD')
        i_extval=cp_InsertValVFPExtFlds(this,'MAT_PROD')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'MPSERIAL',this.w_MPSERIAL,'MPNUMRIF',this.w_MPNUMRIF,'MPROWDOC',this.w_MPROWDOC)
        INSERT INTO (i_cTable) (;
                   MPSERIAL;
                  ,CPROWORD;
                  ,MPCODICE;
                  ,MPCODART;
                  ,MPUNIMIS;
                  ,MPCOEIMP;
                  ,MPQTAMOV;
                  ,MPQTAEVA;
                  ,MPFLEVAS;
                  ,MPCODMAG;
                  ,MPQTAUM1;
                  ,MPSERODL;
                  ,MPROWODL;
                  ,MPQTAEV1;
                  ,MPNUMRIF;
                  ,MPROWDOC;
                  ,MPKEYSAL;
                  ,MPCODCOM;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_MPSERIAL;
                  ,this.w_CPROWORD;
                  ,this.w_MPCODICE;
                  ,this.w_MPCODART;
                  ,this.w_MPUNIMIS;
                  ,this.w_MPCOEIMP;
                  ,this.w_MPQTAMOV;
                  ,this.w_MPQTAEVA;
                  ,this.w_MPFLEVAS;
                  ,this.w_MPCODMAG;
                  ,this.w_MPQTAUM1;
                  ,this.w_MPSERODL;
                  ,this.w_MPROWODL;
                  ,this.w_MPQTAEV1;
                  ,this.w_MPNUMRIF;
                  ,this.w_MPROWDOC;
                  ,this.w_MPKEYSAL;
                  ,this.w_MPCODCOM;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.MAT_PROD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MAT_PROD_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (t_CPROWORD<>0 and not empty(t_MPCODICE)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'MAT_PROD')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'MAT_PROD')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_CPROWORD<>0 and not empty(t_MPCODICE)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete row children
              this.GSAC_MLO.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_MPSERIAL,"CLDICHIA";
                     ,this.w_CPROWNUM,"CLROWCNM";
                     ,this.w_MPNUMRIF,"CLNUMRIF";
                     ,this.w_MPROWDOC,"CLROWDOC";
                     )
              this.GSAC_MCM.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_MPSERIAL,"MTSERIAL";
                     ,this.w_CPROWNUM,"MTROWNUM";
                     ,this.w_MPNUMRIF,"MTNUMRIF";
                     ,this.w_MPROWDOC,"MTROWDOC";
                     )
              this.GSAC_MLO.mDelete()
              this.GSAC_MCM.mDelete()
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update MAT_PROD
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'MAT_PROD')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",MPCODICE="+cp_ToStrODBCNull(this.w_MPCODICE)+;
                     ",MPCODART="+cp_ToStrODBCNull(this.w_MPCODART)+;
                     ",MPUNIMIS="+cp_ToStrODBCNull(this.w_MPUNIMIS)+;
                     ",MPCOEIMP="+cp_ToStrODBC(this.w_MPCOEIMP)+;
                     ",MPQTAMOV="+cp_ToStrODBC(this.w_MPQTAMOV)+;
                     ",MPQTAEVA="+cp_ToStrODBC(this.w_MPQTAEVA)+;
                     ",MPFLEVAS="+cp_ToStrODBC(this.w_MPFLEVAS)+;
                     ",MPCODMAG="+cp_ToStrODBCNull(this.w_MPCODMAG)+;
                     ",MPQTAUM1="+cp_ToStrODBC(this.w_MPQTAUM1)+;
                     ",MPSERODL="+cp_ToStrODBC(this.w_MPSERODL)+;
                     ",MPROWODL="+cp_ToStrODBC(this.w_MPROWODL)+;
                     ",MPQTAEV1="+cp_ToStrODBC(this.w_MPQTAEV1)+;
                     ",MPKEYSAL="+cp_ToStrODBC(this.w_MPKEYSAL)+;
                     ",MPCODCOM="+cp_ToStrODBCNull(this.w_MPCODCOM)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'MAT_PROD')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,MPCODICE=this.w_MPCODICE;
                     ,MPCODART=this.w_MPCODART;
                     ,MPUNIMIS=this.w_MPUNIMIS;
                     ,MPCOEIMP=this.w_MPCOEIMP;
                     ,MPQTAMOV=this.w_MPQTAMOV;
                     ,MPQTAEVA=this.w_MPQTAEVA;
                     ,MPFLEVAS=this.w_MPFLEVAS;
                     ,MPCODMAG=this.w_MPCODMAG;
                     ,MPQTAUM1=this.w_MPQTAUM1;
                     ,MPSERODL=this.w_MPSERODL;
                     ,MPROWODL=this.w_MPROWODL;
                     ,MPQTAEV1=this.w_MPQTAEV1;
                     ,MPKEYSAL=this.w_MPKEYSAL;
                     ,MPCODCOM=this.w_MPCODCOM;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask to row children to save themselves
      select (this.cTrsName)
      i_TN = this.cTrsName
      scan for (t_CPROWORD<>0 and not empty(t_MPCODICE))
        * --- > Optimize children saving
        i_nRec = recno()
        this.WorkFromTrs()
        if not(deleted())
          this.GSAC_MLO.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_MPSERIAL,"CLDICHIA";
               ,this.w_CPROWNUM,"CLROWCNM";
               ,this.w_MPNUMRIF,"CLNUMRIF";
               ,this.w_MPROWDOC,"CLROWDOC";
               )
          this.GSAC_MLO.mReplace()
          this.GSAC_MCM.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_MPSERIAL,"MTSERIAL";
               ,this.w_CPROWNUM,"MTROWNUM";
               ,this.w_MPNUMRIF,"MTNUMRIF";
               ,this.w_MPROWDOC,"MTROWDOC";
               )
          this.GSAC_MCM.mReplace()
          this.GSAC_MLO.bSaveContext=.f.
          this.GSAC_MCM.bSaveContext=.f.
        endif
      endscan
     this.GSAC_MLO.bSaveContext=.t.
     this.GSAC_MCM.bSaveContext=.t.
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.MAT_PROD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MAT_PROD_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_CPROWORD<>0 and not empty(t_MPCODICE)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_nRec = recno()
        * --- GSAC_MLO : Deleting
        this.GSAC_MLO.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_MPSERIAL,"CLDICHIA";
               ,this.w_CPROWNUM,"CLROWCNM";
               ,this.w_MPNUMRIF,"CLNUMRIF";
               ,this.w_MPROWDOC,"CLROWDOC";
               )
        this.GSAC_MLO.mDelete()
        * --- GSAC_MCM : Deleting
        this.GSAC_MCM.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_MPSERIAL,"MTSERIAL";
               ,this.w_CPROWNUM,"MTROWNUM";
               ,this.w_MPNUMRIF,"MTNUMRIF";
               ,this.w_MPROWDOC,"MTROWDOC";
               )
        this.GSAC_MCM.mDelete()
        if bTrsErr
          i_nModRow = -1
          exit
        endif
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete MAT_PROD
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_CPROWORD<>0 and not empty(t_MPCODICE)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MAT_PROD_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MAT_PROD_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
          .link_1_2('Full')
        .DoRTCalc(3,5,.t.)
          .link_2_3('Full')
        .DoRTCalc(7,10,.t.)
          .link_2_8('Full')
        .DoRTCalc(12,15,.t.)
        if .o_MPCODICE<>.w_MPCODICE
          .w_MPUNIMIS = IIF(NOT EMPTY(.w_UNMIS3) AND .w_MOLTI3<>0, .w_UNMIS3, .w_UNMIS1)
          .link_2_13('Full')
        endif
        .DoRTCalc(17,17,.t.)
        if .o_MPCOEIMP<>.w_MPCOEIMP.or. .o_MPUNIMIS<>.w_MPUNIMIS
          .w_MPQTAMOV = this.oParentObject .w_MVQTAUM1 * .w_MPCOEIMP
        endif
        .DoRTCalc(19,20,.t.)
        if .o_MPCODICE<>.w_MPCODICE
          .w_MPCODMAG = IIF(EMPTY(.w_MAGPRO), g_MAGAZI, .w_MAGPRO)
          .link_2_19('Full')
        endif
        .DoRTCalc(22,22,.t.)
        if .o_MPUNIMIS<>.w_MPUNIMIS.or. .o_MPQTAMOV<>.w_MPQTAMOV.or. .o_MPCOEIMP<>.w_MPCOEIMP
          .w_MPQTAUM1 = CALQTAADV(.w_MPQTAMOV,.w_MPUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, 'N', .w_MODUM2, '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3,,,This, "MPQTAMOV")
        endif
        .DoRTCalc(24,28,.t.)
        if .o_MPQTAEVA<>.w_MPQTAEVA.or. .o_MPUNIMIS<>.w_MPUNIMIS.or. .o_MPCOEIMP<>.w_MPCOEIMP
          .w_MPQTAEV1 = CALQTAADV(.w_MPQTAEVA,.w_MPUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, 'N', .w_MODUM2, '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3,,,This, "MPQTAEVA")
        endif
        .DoRTCalc(30,32,.t.)
          .w_MPNUMRIF = -30
        .oPgFrm.Page1.oPag.oObj_2_33.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_34.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_35.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_36.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_37.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_38.Calculate()
        .DoRTCalc(34,39,.t.)
          .w_MTCARI = ' '
        .oPgFrm.Page1.oPag.oObj_2_43.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(41,43,.t.)
        if .o_MPCODICE<>.w_MPCODICE
          .w_MPCODCOM = iif(NVL(.w_FLCOMM,'N')='S',this.oparentobject .w_MVCODCOM,space(15))
          .link_2_44('Full')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_ReadPar with this.w_ReadPar
      replace t_MAGPRO with this.w_MAGPRO
      replace t_MPCODART with this.w_MPCODART
      replace t_UNMIS3 with this.w_UNMIS3
      replace t_OPERA3 with this.w_OPERA3
      replace t_MOLTI3 with this.w_MOLTI3
      replace t_UNMIS1 with this.w_UNMIS1
      replace t_OPERAT with this.w_OPERAT
      replace t_MOLTIP with this.w_MOLTIP
      replace t_UNMIS2 with this.w_UNMIS2
      replace t_FLCOMM with this.w_FLCOMM
      replace t_QTARES with this.w_QTARES
      replace t_MPQTAUM1 with this.w_MPQTAUM1
      replace t_MPSERODL with this.w_MPSERODL
      replace t_MPROWODL with this.w_MPROWODL
      replace t_MATCOM with this.w_MATCOM
      replace t_CHANGE with this.w_CHANGE
      replace t_MPQTAEV1 with this.w_MPQTAEV1
      replace t_MODUM2 with this.w_MODUM2
      replace t_NOFRAZ with this.w_NOFRAZ
      replace t_FLLOTT with this.w_FLLOTT
      replace t_FLUBI with this.w_FLUBI
      replace t_MATCOM with this.w_MATCOM
      replace t_QTAUM1 with this.w_QTAUM1
      replace t_MPKEYSAL with this.w_MPKEYSAL
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_2_33.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_34.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_35.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_36.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_37.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_43.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_2_33.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_34.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_35.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_36.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_37.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_43.Calculate()
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMPCODICE_2_2.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMPCODICE_2_2.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMPCOEIMP_2_14.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMPCOEIMP_2_14.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMPQTAMOV_2_15.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMPQTAMOV_2_15.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMPQTAEVA_2_16.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMPQTAEVA_2_16.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMPFLEVAS_2_17.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMPFLEVAS_2_17.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMPCODMAG_2_19.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMPCODMAG_2_19.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMPCODCOM_2_44.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMPCODCOM_2_44.mCond()
    this.oPgFrm.Page1.oPag.oLinkPC_2_39.enabled =this.oPgFrm.Page1.oPag.oLinkPC_2_39.mCond()
    *--- Nascondo il figlio se non editabile
    if Type("this.GSAC_MLO.visible")=='L' And this.GSAC_MLO.visible And !this.oPgFrm.Page1.oPag.oLinkPC_2_39.enabled
      this.GSAC_MLO.HideChildrenChain()
    endif 
    this.oPgFrm.Page1.oPag.oLinkPC_2_40.enabled =this.oPgFrm.Page1.oPag.oLinkPC_2_40.mCond()
    *--- Nascondo il figlio se non editabile
    if Type("this.GSAC_MCM.visible")=='L' And this.GSAC_MCM.visible And !this.oPgFrm.Page1.oPag.oLinkPC_2_40.enabled
      this.GSAC_MCM.HideChildrenChain()
    endif 
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oLinkPC_2_39.visible=!this.oPgFrm.Page1.oPag.oLinkPC_2_39.mHide()
    *--- Nascondo il figlio se non visibile
    if Type("this.GSAC_MLO.visible")=='L' And this.GSAC_MLO.visible And !this.oPgFrm.Page1.oPag.oLinkPC_2_39.visible
      this.GSAC_MLO.HideChildrenChain()
    endif 
    this.oPgFrm.Page1.oPag.oLinkPC_2_40.visible=!this.oPgFrm.Page1.oPag.oLinkPC_2_40.mHide()
    *--- Nascondo il figlio se non visibile
    if Type("this.GSAC_MCM.visible")=='L' And this.GSAC_MCM.visible And !this.oPgFrm.Page1.oPag.oLinkPC_2_40.visible
      this.GSAC_MCM.HideChildrenChain()
    endif 
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_2_33.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_34.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_35.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_36.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_37.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_38.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_43.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ReadPar
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_PROD_IDX,3]
    i_lTable = "PAR_PROD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2], .t., this.PAR_PROD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ReadPar) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ReadPar)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPCODICE,PPMAGPRO,PPIMPLOT";
                   +" from "+i_cTable+" "+i_lTable+" where PPCODICE="+cp_ToStrODBC(this.w_ReadPar);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPCODICE',this.w_ReadPar)
            select PPCODICE,PPMAGPRO,PPIMPLOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ReadPar = NVL(_Link_.PPCODICE,space(2))
      this.w_MAGPRO = NVL(_Link_.PPMAGPRO,space(5))
      this.w_IMPLOT = NVL(_Link_.PPIMPLOT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ReadPar = space(2)
      endif
      this.w_MAGPRO = space(5)
      this.w_IMPLOT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])+'\'+cp_ToStr(_Link_.PPCODICE,1)
      cp_ShowWarn(i_cKey,this.PAR_PROD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ReadPar Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MPCODICE
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MPCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_MPCODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_MPCODICE))
          select CACODICE,CADESART,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MPCODICE)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_MPCODICE)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_MPCODICE)+"%");

            select CACODICE,CADESART,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MPCODICE) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oMPCODICE_2_2'),i_cWhere,'',"Codici di ricerca",'GSCO_MOL.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MPCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_MPCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_MPCODICE)
            select CACODICE,CADESART,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MPCODICE = NVL(_Link_.CACODICE,space(20))
      this.w_DESART = NVL(_Link_.CADESART,space(40))
      this.w_MPCODART = NVL(_Link_.CACODART,space(20))
      this.w_UNMIS3 = NVL(_Link_.CAUNIMIS,space(3))
      this.w_OPERA3 = NVL(_Link_.CAOPERAT,space(1))
      this.w_MOLTI3 = NVL(_Link_.CAMOLTIP,0)
      this.w_MPKEYSAL = NVL(_Link_.CACODART,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_MPCODICE = space(20)
      endif
      this.w_DESART = space(40)
      this.w_MPCODART = space(20)
      this.w_UNMIS3 = space(3)
      this.w_OPERA3 = space(1)
      this.w_MOLTI3 = 0
      this.w_MPKEYSAL = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MPCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 7 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+7<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_2.CACODICE as CACODICE202"+ ",link_2_2.CADESART as CADESART202"+ ",link_2_2.CACODART as CACODART202"+ ",link_2_2.CAUNIMIS as CAUNIMIS202"+ ",link_2_2.CAOPERAT as CAOPERAT202"+ ",link_2_2.CAMOLTIP as CAMOLTIP202"+ ",link_2_2.CACODART as CACODART202"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_2 on MAT_PROD.MPCODICE=link_2_2.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_2"
          i_cKey=i_cKey+'+" and MAT_PROD.MPCODICE=link_2_2.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MPCODART
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MPCODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MPCODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARUNMIS1,AROPERAT,ARMOLTIP,ARUNMIS2,ARGESMAT,ARFLUSEP,ARFLLOTT,ARSALCOM";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_MPCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_MPCODART)
            select ARCODART,ARUNMIS1,AROPERAT,ARMOLTIP,ARUNMIS2,ARGESMAT,ARFLUSEP,ARFLLOTT,ARSALCOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MPCODART = NVL(_Link_.ARCODART,space(20))
      this.w_UNMIS1 = NVL(_Link_.ARUNMIS1,space(3))
      this.w_OPERAT = NVL(_Link_.AROPERAT,space(1))
      this.w_MOLTIP = NVL(_Link_.ARMOLTIP,0)
      this.w_UNMIS2 = NVL(_Link_.ARUNMIS2,space(3))
      this.w_MATCOM = NVL(_Link_.ARGESMAT,space(1))
      this.w_FLUSEP = NVL(_Link_.ARFLUSEP,space(1))
      this.w_FLLOTT = NVL(_Link_.ARFLLOTT,space(1))
      this.w_MATCOM = NVL(_Link_.ARGESMAT,space(1))
      this.w_FLCOMM = NVL(_Link_.ARSALCOM,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_MPCODART = space(20)
      endif
      this.w_UNMIS1 = space(3)
      this.w_OPERAT = space(1)
      this.w_MOLTIP = 0
      this.w_UNMIS2 = space(3)
      this.w_MATCOM = space(1)
      this.w_FLUSEP = space(1)
      this.w_FLLOTT = space(1)
      this.w_MATCOM = space(1)
      this.w_FLCOMM = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MPCODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 10 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+10<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.ARCODART as ARCODART203"+ ",link_2_3.ARUNMIS1 as ARUNMIS1203"+ ",link_2_3.AROPERAT as AROPERAT203"+ ",link_2_3.ARMOLTIP as ARMOLTIP203"+ ",link_2_3.ARUNMIS2 as ARUNMIS2203"+ ",link_2_3.ARGESMAT as ARGESMAT203"+ ",link_2_3.ARFLUSEP as ARFLUSEP203"+ ",link_2_3.ARFLLOTT as ARFLLOTT203"+ ",link_2_3.ARGESMAT as ARGESMAT203"+ ",link_2_3.ARSALCOM as ARSALCOM203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on MAT_PROD.MPCODART=link_2_3.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+10
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and MAT_PROD.MPCODART=link_2_3.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+10
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=UNMIS1
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UNMIS1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UNMIS1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMMODUM2,UMFLFRAZ";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_UNMIS1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_UNMIS1)
            select UMCODICE,UMMODUM2,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UNMIS1 = NVL(_Link_.UMCODICE,space(3))
      this.w_MODUM2 = NVL(_Link_.UMMODUM2,space(1))
      this.w_NOFRAZ = NVL(_Link_.UMFLFRAZ,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_UNMIS1 = space(3)
      endif
      this.w_MODUM2 = space(1)
      this.w_NOFRAZ = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UNMIS1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MPUNIMIS
  func Link_2_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MPUNIMIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MPUNIMIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_MPUNIMIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_MPUNIMIS)
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MPUNIMIS = NVL(_Link_.UMCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_MPUNIMIS = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKUNIMI(.w_MPUNIMIS, .w_UNMIS1, .w_UNMIS2, .w_UNMIS3)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MPUNIMIS = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MPUNIMIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MPCODMAG
  func Link_2_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MPCODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_MPCODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGFLUBIC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_MPCODMAG))
          select MGCODMAG,MGFLUBIC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MPCODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MPCODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oMPCODMAG_2_19'),i_cWhere,'GSAR_AMA',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGFLUBIC";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MPCODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGFLUBIC";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_MPCODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_MPCODMAG)
            select MGCODMAG,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MPCODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_FLUBI = NVL(_Link_.MGFLUBIC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MPCODMAG = space(5)
      endif
      this.w_FLUBI = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MPCODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_19(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_19.MGCODMAG as MGCODMAG219"+ ",link_2_19.MGFLUBIC as MGFLUBIC219"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_19 on MAT_PROD.MPCODMAG=link_2_19.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_19"
          i_cKey=i_cKey+'+" and MAT_PROD.MPCODMAG=link_2_19.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MPCODCOM
  func Link_2_44(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MPCODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_MPCODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_MPCODCOM))
          select CNCODCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MPCODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MPCODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oMPCODCOM_2_44'),i_cWhere,'GSAR_ACN',"COMMESSE",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MPCODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_MPCODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_MPCODCOM)
            select CNCODCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MPCODCOM = NVL(_Link_.CNCODCAN,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_MPCODCOM = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MPCODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oDESART_2_7.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_2_7.value=this.w_DESART
      replace t_DESART with this.oPgFrm.Page1.oPag.oDESART_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPCODICE_2_2.value==this.w_MPCODICE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPCODICE_2_2.value=this.w_MPCODICE
      replace t_MPCODICE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPCODICE_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPUNIMIS_2_13.value==this.w_MPUNIMIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPUNIMIS_2_13.value=this.w_MPUNIMIS
      replace t_MPUNIMIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPUNIMIS_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPCOEIMP_2_14.value==this.w_MPCOEIMP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPCOEIMP_2_14.value=this.w_MPCOEIMP
      replace t_MPCOEIMP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPCOEIMP_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPQTAMOV_2_15.value==this.w_MPQTAMOV)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPQTAMOV_2_15.value=this.w_MPQTAMOV
      replace t_MPQTAMOV with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPQTAMOV_2_15.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPQTAEVA_2_16.value==this.w_MPQTAEVA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPQTAEVA_2_16.value=this.w_MPQTAEVA
      replace t_MPQTAEVA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPQTAEVA_2_16.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPFLEVAS_2_17.RadioValue()==this.w_MPFLEVAS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPFLEVAS_2_17.SetRadio()
      replace t_MPFLEVAS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPFLEVAS_2_17.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPCODMAG_2_19.value==this.w_MPCODMAG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPCODMAG_2_19.value=this.w_MPCODMAG
      replace t_MPCODMAG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPCODMAG_2_19.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPCODCOM_2_44.value==this.w_MPCODCOM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPCODCOM_2_44.value=this.w_MPCODCOM
      replace t_MPCODCOM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPCODCOM_2_44.value
    endif
    cp_SetControlsValueExtFlds(this,'MAT_PROD')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsac_mmp
      i_nOldArea=select()
      local ncount, errlot, messerr, l_nRec
      ncount = 0
      errlot = .f.
      Select (this.cTrsName)
      l_nRec = recno()
      go top
      scan for (t_FLLOTT$'CS' or t_FLUBI='S') and not deleted()
        if vartype(this.GSAC_MLO.cnt) = 'O'
          Select (this.GSAC_MLO.cnt.cTrsName)
          Count for (!empty(t_CLLOTART) or !empty(t_CLCODUBI)) and not deleted() to ncount
          if ncount = 0 and not empty(nvl(this.GSAC_MLO.cnt.w_cldichia,''))
            errlot = .t.
          endif
        else
          if vartype(this.GSAC_MCM.cnt) = 'O'
            Select (this.GSAC_MCM.cnt.cTrsName)
            Count for !empty(t_MTCODLOT) and not deleted() to ncount
            if ncount = 0 and not empty(nvl(this.GSAC_MLO.cnt.w_cldichia,''))
              errlot = .t.
            endif
          else
            errlot = .t.
          endif
        endif
      endscan
      Select (this.cTrsName)
      go l_nRec
      if errlot AND (g_PERLOT='S' OR g_PERUBI='S')
        if .w_IMPLOT = 'S'
           i_bRes = .f.
           i_bnoChk = .f.	
           i_cErrorMsg = ah_MsgFormat("Attenzione, non � stato inserito il dettaglio lotti/ubicazioni.%0Impossibile confermare.")
        else
           ah_ErrorMsg("Attenzione, non � stato inserito il dettaglio lotti/ubicazioni.")
        endif
      endif
      select (i_nOldArea)
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(CHKUNIMI(.w_MPUNIMIS, .w_UNMIS1, .w_UNMIS2, .w_UNMIS3)) and not(empty(.w_MPUNIMIS)) and (.w_CPROWORD<>0 and not empty(.w_MPCODICE))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPUNIMIS_2_13
          i_bRes = .f.
          i_bnoChk = .f.
        case   not(!(.w_MPQTAEVA>.oParentObject .w_MVQTAUM1 * .w_MPCOEIMP)) and (!empty(.w_MPSERODL) and !empty(.w_MPROWODL)) and (.w_CPROWORD<>0 and not empty(.w_MPCODICE))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPQTAEVA_2_16
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Quantit� evasa superiore a quella da evadere")
        case   empty(.w_MPCODMAG) and (NOT EMPTY(.w_MPCODICE)) and (.w_CPROWORD<>0 and not empty(.w_MPCODICE))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPCODMAG_2_19
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
      endcase
      i_bRes = i_bRes .and. .GSAC_MLO.CheckForm()
      i_bRes = i_bRes .and. .GSAC_MCM.CheckForm()
      if .w_CPROWORD<>0 and not empty(.w_MPCODICE)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MPCODICE = this.w_MPCODICE
    this.o_MPUNIMIS = this.w_MPUNIMIS
    this.o_MPCOEIMP = this.w_MPCOEIMP
    this.o_MPQTAMOV = this.w_MPQTAMOV
    this.o_MPQTAEVA = this.w_MPQTAEVA
    * --- GSAC_MLO : Depends On
    this.GSAC_MLO.SaveDependsOn()
    * --- GSAC_MCM : Depends On
    this.GSAC_MCM.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_CPROWORD<>0 and not empty(t_MPCODICE))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_ReadPar=space(2)
      .w_MAGPRO=space(5)
      .w_CPROWORD=MIN(999999,cp_maxroword()+10)
      .w_MPCODICE=space(20)
      .w_MPCODART=space(20)
      .w_UNMIS3=space(3)
      .w_OPERA3=space(1)
      .w_MOLTI3=0
      .w_DESART=space(40)
      .w_UNMIS1=space(3)
      .w_OPERAT=space(1)
      .w_MOLTIP=0
      .w_UNMIS2=space(3)
      .w_FLCOMM=space(10)
      .w_MPUNIMIS=space(3)
      .w_MPCOEIMP=0
      .w_MPQTAMOV=0
      .w_MPQTAEVA=0
      .w_MPFLEVAS=space(1)
      .w_MPCODMAG=space(5)
      .w_QTARES=0
      .w_MPQTAUM1=0
      .w_MPSERODL=space(15)
      .w_MPROWODL=0
      .w_MATCOM=space(1)
      .w_CHANGE=.f.
      .w_MPQTAEV1=0
      .w_MODUM2=space(1)
      .w_NOFRAZ=space(1)
      .w_FLLOTT=space(1)
      .w_FLUBI=space(1)
      .w_MATCOM=space(1)
      .w_QTAUM1=0
      .w_MPKEYSAL=space(20)
      .w_MPCODCOM=space(15)
      .DoRTCalc(1,1,.f.)
        .w_ReadPar = 'PP'
      .DoRTCalc(2,2,.f.)
      if not(empty(.w_ReadPar))
        .link_1_2('Full')
      endif
      .DoRTCalc(3,5,.f.)
      if not(empty(.w_MPCODICE))
        .link_2_2('Full')
      endif
      .DoRTCalc(6,6,.f.)
      if not(empty(.w_MPCODART))
        .link_2_3('Full')
      endif
      .DoRTCalc(7,11,.f.)
      if not(empty(.w_UNMIS1))
        .link_2_8('Full')
      endif
      .DoRTCalc(12,15,.f.)
        .w_MPUNIMIS = IIF(NOT EMPTY(.w_UNMIS3) AND .w_MOLTI3<>0, .w_UNMIS3, .w_UNMIS1)
      .DoRTCalc(16,16,.f.)
      if not(empty(.w_MPUNIMIS))
        .link_2_13('Full')
      endif
      .DoRTCalc(17,17,.f.)
        .w_MPQTAMOV = this.oParentObject .w_MVQTAUM1 * .w_MPCOEIMP
      .DoRTCalc(19,20,.f.)
        .w_MPCODMAG = IIF(EMPTY(.w_MAGPRO), g_MAGAZI, .w_MAGPRO)
      .DoRTCalc(21,21,.f.)
      if not(empty(.w_MPCODMAG))
        .link_2_19('Full')
      endif
      .DoRTCalc(22,22,.f.)
        .w_MPQTAUM1 = CALQTAADV(.w_MPQTAMOV,.w_MPUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, 'N', .w_MODUM2, '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3,,,This, "MPQTAMOV")
      .DoRTCalc(24,27,.f.)
        .w_CHANGE = False
        .w_MPQTAEV1 = CALQTAADV(.w_MPQTAEVA,.w_MPUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, 'N', .w_MODUM2, '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3,,,This, "MPQTAEVA")
        .oPgFrm.Page1.oPag.oObj_2_33.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_34.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_35.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_36.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_37.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_38.Calculate()
      .DoRTCalc(30,38,.f.)
        .w_QTAUM1 = this.oParentObject .w_MVQTAUM1
        .oPgFrm.Page1.oPag.oObj_2_43.Calculate()
      .DoRTCalc(40,43,.f.)
        .w_MPCODCOM = iif(NVL(.w_FLCOMM,'N')='S',this.oparentobject .w_MVCODCOM,space(15))
      .DoRTCalc(44,44,.f.)
      if not(empty(.w_MPCODCOM))
        .link_2_44('Full')
      endif
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_ReadPar = t_ReadPar
    this.w_MAGPRO = t_MAGPRO
    this.w_CPROWORD = t_CPROWORD
    this.w_MPCODICE = t_MPCODICE
    this.w_MPCODART = t_MPCODART
    this.w_UNMIS3 = t_UNMIS3
    this.w_OPERA3 = t_OPERA3
    this.w_MOLTI3 = t_MOLTI3
    this.w_DESART = t_DESART
    this.w_UNMIS1 = t_UNMIS1
    this.w_OPERAT = t_OPERAT
    this.w_MOLTIP = t_MOLTIP
    this.w_UNMIS2 = t_UNMIS2
    this.w_FLCOMM = t_FLCOMM
    this.w_MPUNIMIS = t_MPUNIMIS
    this.w_MPCOEIMP = t_MPCOEIMP
    this.w_MPQTAMOV = t_MPQTAMOV
    this.w_MPQTAEVA = t_MPQTAEVA
    this.w_MPFLEVAS = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPFLEVAS_2_17.RadioValue(.t.)
    this.w_MPCODMAG = t_MPCODMAG
    this.w_QTARES = t_QTARES
    this.w_MPQTAUM1 = t_MPQTAUM1
    this.w_MPSERODL = t_MPSERODL
    this.w_MPROWODL = t_MPROWODL
    this.w_MATCOM = t_MATCOM
    this.w_CHANGE = t_CHANGE
    this.w_MPQTAEV1 = t_MPQTAEV1
    this.w_MODUM2 = t_MODUM2
    this.w_NOFRAZ = t_NOFRAZ
    this.w_FLLOTT = t_FLLOTT
    this.w_FLUBI = t_FLUBI
    this.w_MATCOM = t_MATCOM
    this.w_QTAUM1 = t_QTAUM1
    this.w_MPKEYSAL = t_MPKEYSAL
    this.w_MPCODCOM = t_MPCODCOM
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_ReadPar with this.w_ReadPar
    replace t_MAGPRO with this.w_MAGPRO
    replace t_CPROWORD with this.w_CPROWORD
    replace t_MPCODICE with this.w_MPCODICE
    replace t_MPCODART with this.w_MPCODART
    replace t_UNMIS3 with this.w_UNMIS3
    replace t_OPERA3 with this.w_OPERA3
    replace t_MOLTI3 with this.w_MOLTI3
    replace t_DESART with this.w_DESART
    replace t_UNMIS1 with this.w_UNMIS1
    replace t_OPERAT with this.w_OPERAT
    replace t_MOLTIP with this.w_MOLTIP
    replace t_UNMIS2 with this.w_UNMIS2
    replace t_FLCOMM with this.w_FLCOMM
    replace t_MPUNIMIS with this.w_MPUNIMIS
    replace t_MPCOEIMP with this.w_MPCOEIMP
    replace t_MPQTAMOV with this.w_MPQTAMOV
    replace t_MPQTAEVA with this.w_MPQTAEVA
    replace t_MPFLEVAS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPFLEVAS_2_17.ToRadio()
    replace t_MPCODMAG with this.w_MPCODMAG
    replace t_QTARES with this.w_QTARES
    replace t_MPQTAUM1 with this.w_MPQTAUM1
    replace t_MPSERODL with this.w_MPSERODL
    replace t_MPROWODL with this.w_MPROWODL
    replace t_MATCOM with this.w_MATCOM
    replace t_CHANGE with this.w_CHANGE
    replace t_MPQTAEV1 with this.w_MPQTAEV1
    replace t_MODUM2 with this.w_MODUM2
    replace t_NOFRAZ with this.w_NOFRAZ
    replace t_FLLOTT with this.w_FLLOTT
    replace t_FLUBI with this.w_FLUBI
    replace t_MATCOM with this.w_MATCOM
    replace t_QTAUM1 with this.w_QTAUM1
    replace t_MPKEYSAL with this.w_MPKEYSAL
    replace t_MPCODCOM with this.w_MPCODCOM
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsac_mmpPag1 as StdContainer
  Width  = 812
  height = 371
  stdWidth  = 812
  stdheight = 371
  resizeXpos=189
  resizeYpos=241
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=6, top=12, width=771,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=9,Field1="CPROWORD",Label1="Posiz.",Field2="MPCODICE",Label2="Codice",Field3="MPUNIMIS",Label3="UM",Field4="MPCOEIMP",Label4="Qta da dist.",Field5="MPQTAMOV",Label5="Qta consumata",Field6="MPQTAEVA",Label6="Qta da evadere",Field7="MPFLEVAS",Label7="Ev.",Field8="MPCODMAG",Label8="Magazz.",Field9="MPCODCOM",Label9="Commessa",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 118276730

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-4,top=31,;
    width=768+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*16*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-3,top=32,width=767+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*16*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='KEY_ARTI|MAGAZZIN|CAN_TIER|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDESART_2_7.Refresh()
      this.Parent.oContained.ChildrenChangeRow()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='KEY_ARTI'
        oDropInto=this.oBodyCol.oRow.oMPCODICE_2_2
      case cFile='MAGAZZIN'
        oDropInto=this.oBodyCol.oRow.oMPCODMAG_2_19
      case cFile='CAN_TIER'
        oDropInto=this.oBodyCol.oRow.oMPCODCOM_2_44
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oDESART_2_7 as StdTrsField with uid="EHIGOOKZYE",rtseq=10,rtrep=.t.,;
    cFormVar="w_DESART",value=space(40),enabled=.f.,;
    HelpContextID = 40103370,;
    cTotal="", bFixedPos=.t., cQueryName = "DESART",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=111, Top=343, InputMask=replicate('X',40)

  add object oObj_2_33 as cp_runprogram with uid="TSLKIAXPEF",width=207,height=19,;
   left=0, top=378,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="gsac_bm1('QTAMOV')",;
    cEvent = "w_CMQTAMOV Changed",;
    nPag=2;
    , ToolTipText = "Aggiorna quantit�";
    , HelpContextID = 19816474

  add object oObj_2_34 as cp_runprogram with uid="RCGQUWAFDI",width=207,height=19,;
   left=0, top=398,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="gsac_bm1('AGGLOT')",;
    cEvent = "w_CMCODMAG Changed",;
    nPag=2;
    , ToolTipText = "Aggiorna detail lotti";
    , HelpContextID = 19816474

  add object oObj_2_35 as cp_runprogram with uid="NAYSDCHAMJ",width=207,height=19,;
   left=0, top=418,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="gsac_bm1('AGGMAT')",;
    cEvent = "w_CMCODMAG Changed",;
    nPag=2;
    , ToolTipText = "Aggiorna detail matricole";
    , HelpContextID = 19816474

  add object oObj_2_36 as cp_runprogram with uid="DBAIFGDFKV",width=207,height=19,;
   left=0, top=438,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="gsac_bm1('OPEN')",;
    cEvent = "Blank",;
    nPag=2;
    , ToolTipText = "Aggiorna detail matricole";
    , HelpContextID = 19816474

  add object oObj_2_37 as cp_runprogram with uid="LZNXNWSLYX",width=207,height=19,;
   left=0, top=458,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="gsac_bm1('INIT')",;
    cEvent = "Init",;
    nPag=2;
    , ToolTipText = "Aggiorna detail matricole";
    , HelpContextID = 19816474

  add object oObj_2_38 as cp_runprogram with uid="CGKDTDYJXH",width=207,height=19,;
   left=0, top=478,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="gsac_bm1('AGGIO')",;
    cEvent = "w_MPCODICE Changed, w_MPUNIMIS Changed, w_MPCOEIMP Changed, w_MPQTAMOV Changed, w_MPQTAEVA Changed, w_MPFLEVAS Changed, w_MPCODMAG Changed,Row deleted",;
    nPag=2;
    , ToolTipText = "Aggiorna quantit�";
    , HelpContextID = 19816474

  add object oLinkPC_2_39 as StdButton with uid="JXTVFJRTRS",width=20,height=19,;
   left=783, top=33,;
    caption="...", nPag=2;
    , ToolTipText = "Premere per aprire la gestione lotti/ubicazioni";
    , HelpContextID = 70822358;
  , bGlobalFont=.t.

    proc oLinkPC_2_39.Click()
      this.Parent.oContained.GSAC_MLO.LinkPCClick()
    endproc

  func oLinkPC_2_39.mCond()
    with this.Parent.oContained
      return ((g_PERLOT='S' AND NVL(.w_FLLOTT,'N')<>'N' or g_PERUBI='S' AND .w_FLUBI='S') and nvl(.w_MATCOM,'N')<>'S')
    endwith
  endfunc

  func oLinkPC_2_39.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_MPCODICE) and nvl(.w_FLLOTT,'N')='N')
    endwith
   endif
  endfunc

  add object oLinkPC_2_40 as StdButton with uid="OYVFVALHNL",width=20,height=19,;
   left=783, top=33,;
    caption="...", nPag=2;
    , ToolTipText = "Premere per aprire la gestione matricole";
    , HelpContextID = 70822358;
  , bGlobalFont=.t.

    proc oLinkPC_2_40.Click()
      this.Parent.oContained.GSAC_MCM.LinkPCClick()
    endproc

  func oLinkPC_2_40.mCond()
    with this.Parent.oContained
      return (g_MATR='S' AND nvl(.w_MATCOM,'N')='S' and g_DATMAT<=.w_DATREG)
    endwith
  endfunc

  func oLinkPC_2_40.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_MPCODICE) or nvl(.w_MATCOM,'N')<>'S')
    endwith
   endif
  endfunc

  add object oObj_2_43 as cp_runprogram with uid="YCYNBIPXDT",width=207,height=19,;
   left=0, top=498,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="gsac_bm1('CHKFINAL')",;
    cEvent = "Update row end,Insert end",;
    nPag=2;
    , ToolTipText = "Aggiorna detail matricole";
    , HelpContextID = 19816474

  add object oStr_2_18 as StdString with uid="MRESKLGPUN",Visible=.t., Left=7, Top=343,;
    Alignment=1, Width=101, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsac_mmpBodyRow as CPBodyRowCnt
  Width=758
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="GWXDYKJTVI",rtseq=4,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    ToolTipText = "Posizione componente",;
    HelpContextID = 117830294,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=44, Left=-2, Top=0, cSayPict=["@Z 999999"], cGetPict=["999999"]

  add object oMPCODICE_2_2 as StdTrsField with uid="UOEEPLNYCK",rtseq=5,rtrep=.t.,;
    cFormVar="w_MPCODICE",value=space(20),;
    ToolTipText = "Codice materiale",;
    HelpContextID = 238477813,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=159, Left=46, Top=0, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_MPCODICE"

  func oMPCODICE_2_2.mCond()
    with this.Parent.oContained
      return (.w_MPROWODL=0)
    endwith
  endfunc

  func oMPCODICE_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oMPCODICE_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMPCODICE_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oMPCODICE_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codici di ricerca",'GSCO_MOL.KEY_ARTI_VZM',this.parent.oContained
  endproc

  add object oMPUNIMIS_2_13 as StdTrsField with uid="LQSEQRRKVV",rtseq=16,rtrep=.t.,;
    cFormVar="w_MPUNIMIS",value=space(3),enabled=.f.,;
    HelpContextID = 102317593,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=36, Left=210, Top=0, InputMask=replicate('X',3), cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AMA", oKey_1_1="UMCODICE", oKey_1_2="this.w_MPUNIMIS"

  func oMPUNIMIS_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oMPCOEIMP_2_14 as StdTrsField with uid="FBLNIBXEWI",rtseq=17,rtrep=.t.,;
    cFormVar="w_MPCOEIMP",value=0,;
    HelpContextID = 237429226,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=93, Left=251, Top=0, cSayPict=["999999.99999"], cGetPict=["999999.99999"]

  func oMPCOEIMP_2_14.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_MPCODICE))
    endwith
  endfunc

  add object oMPQTAMOV_2_15 as StdTrsField with uid="LPZNYBJEVA",rtseq=18,rtrep=.t.,;
    cFormVar="w_MPQTAMOV",value=0,;
    HelpContextID = 174129636,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=93, Left=348, Top=0, cSayPict=[v_PQ(12)], cGetPict=[v_PQ(12)]

  func oMPQTAMOV_2_15.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_MPCODICE))
    endwith
  endfunc

  add object oMPQTAEVA_2_16 as StdTrsField with uid="UAPBKGGYPG",rtseq=19,rtrep=.t.,;
    cFormVar="w_MPQTAEVA",value=0,;
    HelpContextID = 228523527,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Quantit� evasa superiore a quella da evadere",;
   bGlobalFont=.t.,;
    Height=17, Width=93, Left=445, Top=0

  func oMPQTAEVA_2_16.mCond()
    with this.Parent.oContained
      return (!empty(.w_MPSERODL) and !empty(.w_MPROWODL))
    endwith
  endfunc

  func oMPQTAEVA_2_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!(.w_MPQTAEVA>.oParentObject .w_MVQTAUM1 * .w_MPCOEIMP))
    endwith
    return bRes
  endfunc

  add object oMPFLEVAS_2_17 as StdTrsCheck with uid="OBDPIQUZKU",rtrep=.t.,;
    cFormVar="w_MPFLEVAS",  caption="",;
    HelpContextID = 19509735,;
    Left=546, Top=0, Width=19,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oMPFLEVAS_2_17.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..MPFLEVAS,&i_cF..t_MPFLEVAS),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oMPFLEVAS_2_17.GetRadio()
    this.Parent.oContained.w_MPFLEVAS = this.RadioValue()
    return .t.
  endfunc

  func oMPFLEVAS_2_17.ToRadio()
    this.Parent.oContained.w_MPFLEVAS=trim(this.Parent.oContained.w_MPFLEVAS)
    return(;
      iif(this.Parent.oContained.w_MPFLEVAS=='S',1,;
      0))
  endfunc

  func oMPFLEVAS_2_17.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oMPFLEVAS_2_17.mCond()
    with this.Parent.oContained
      return (!empty(.w_MPSERODL) and !empty(.w_MPROWODL))
    endwith
  endfunc

  add object oMPCODMAG_2_19 as StdTrsField with uid="ZHXVFSVNLC",rtseq=21,rtrep=.t.,;
    cFormVar="w_MPCODMAG",value=space(5),;
    ToolTipText = "Codice deposito",;
    HelpContextID = 171368947,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=54, Left=574, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_MPCODMAG"

  func oMPCODMAG_2_19.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_MPCODICE))
    endwith
  endfunc

  func oMPCODMAG_2_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oMPCODMAG_2_19.ecpDrop(oSource)
    this.Parent.oContained.link_2_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMPCODMAG_2_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oMPCODMAG_2_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'',this.parent.oContained
  endproc
  proc oMPCODMAG_2_19.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_MPCODMAG
    i_obj.ecpSave()
  endproc

  add object oMPCODCOM_2_44 as StdTrsField with uid="HJRQHHJXKW",rtseq=44,rtrep=.t.,;
    cFormVar="w_MPCODCOM",value=space(15),;
    ToolTipText = "Codice commessa",;
    HelpContextID = 70705645,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=118, Left=635, Top=0, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_MPCODCOM"

  func oMPCODCOM_2_44.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_MPCODICE) and NVL(.w_FLCOMM,'N')='S')
    endwith
  endfunc

  func oMPCODCOM_2_44.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_44('Part',this)
    endwith
    return bRes
  endfunc

  proc oMPCODCOM_2_44.ecpDrop(oSource)
    this.Parent.oContained.link_2_44('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMPCODCOM_2_44.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oMPCODCOM_2_44'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"COMMESSE",'',this.parent.oContained
  endproc
  proc oMPCODCOM_2_44.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_MPCODCOM
    i_obj.ecpSave()
  endproc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=15
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsac_mmp','MAT_PROD','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MPSERIAL=MAT_PROD.MPSERIAL";
  +" and "+i_cAliasName2+".MPNUMRIF=MAT_PROD.MPNUMRIF";
  +" and "+i_cAliasName2+".MPROWDOC=MAT_PROD.MPROWDOC";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
