* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bi2                                                        *
*              Import da documenti collegati                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_243]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-14                                                      *
* Last revis.: 2016-03-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bi2",oParentObject,m.pOper)
return(i_retval)

define class tgsve_bi2 as StdBatch
  * --- Local variables
  pOper = space(1)
  w_OPER = space(1)
  w_oMESS = .NULL.
  w_oPART = .NULL.
  w_oMESS1 = .NULL.
  w_oPART1 = .NULL.
  w_PADRE = .NULL.
  w_GEST = .NULL.
  w_ZOOM = space(10)
  w_OREC = 0
  w_RECSEL = 0
  w_CHKOK = 0
  w_RESOCON1 = space(0)
  w_TEST = space(0)
  w_NOBLOK = .f.
  w_DOCWARN = .f.
  w_MESBLOK = space(0)
  w_TESBLOK = space(0)
  w_BLOK = .f.
  w_DOCBLOK = .f.
  w_DATIDOC = space(50)
  w_CODAPP = space(10)
  w_QTATOT = 0
  w_PRETOT = 0
  w_TOTROW = 0
  w_QTATOT1 = 0
  w_PRETOT1 = 0
  w_TOTROW1 = 0
  w_CNTFLVABD = space(3)
  w_RECTRS = 0
  w_MVCODVAL = space(3)
  w_MVCAOVAL = 0
  w_MVRIFDIC = space(10)
  w_MVCODIVE = space(5)
  w_MVTIPCON = space(1)
  w_MVCODCON = space(15)
  w_ESCL1 = space(3)
  w_ESCL2 = space(3)
  w_ESCL3 = space(3)
  w_ESCL4 = space(3)
  w_ESCL5 = space(3)
  w_FLIMPA = space(1)
  w_MVCODPAG = space(5)
  w_MVDATDIV = ctod("  /  /  ")
  w_MVSCOCL1 = 0
  w_MVSCOCL2 = 0
  w_MVPERFIN = 0
  w_MVSCOPAG = 0
  w_MVCODBAN = space(10)
  w_MVNUMCOR = space(25)
  w_MVCODBA2 = space(15)
  w_MVCODAGE = space(5)
  w_MVCODAG2 = space(5)
  w_MVIVAINC = space(5)
  w_MVIVAIMB = space(5)
  w_MVIVABOL = space(5)
  w_MVIVATRA = space(5)
  w_VARVAL = space(1)
  w_FLIMAC = space(1)
  w_MVCODVET = space(5)
  w_MVCODVE2 = space(5)
  w_MVCODVE3 = space(5)
  w_MVCODPOR = space(1)
  w_MVCODSPE = space(3)
  w_MVCONCON = space(1)
  w_MVCODDES = space(5)
  w_MVCODORN = space(5)
  w_TOTSCF = 0
  w_TOTSCO = 0
  w_TOTFIN = 0
  w_TOTACC = 0
  w_TOTACO = 0
  w_APPO = 0
  w_MVCODSED = space(5)
  w_OKACC = .f.
  w_TOTCAU = 0
  w_MVIVACAU = space(5)
  w_MVCLADOC = space(0)
  w_ZOOMD = space(10)
  w_CHECKCURST = space(10)
  w_RECSEL_C = 0
  w_SERIAL = space(10)
  w_ROWNUM = 0
  w_QTAEVA = 0
  w_ANNULLA = .f.
  * --- WorkFile variables
  DOC_DETT_idx=0
  runtime_filters = 2

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Import da Documenti Collegati da GSVE_KIM e GSPS_KIM
    * --- U = Conferma la Maschera (Update End) ; 
    *     A = Abbandona la Maschera (Edit Aborted)
    this.w_OPER = this.pOper
    * --- w_PADRE: GSVE_KIM o GSPS_KIM
    this.w_PADRE = this.oParentObject
    * --- w_GEST: GSVE_MDV o GSAC_MDV o GSOR_MDV o GSPS_MVD
    this.w_GEST = this.oParentObject.oParentObject
    * --- non riesegua la mcalc all'uscita...
    this.bUpdateParentObject=.f.
    * --- istanzio oggetti per mess. incrementali
    * --- per MESBLOK
    this.w_oMESS=createobject("ah_message")
    * --- per RESOCON1
    this.w_oMESS1=createobject("ah_message")
    * --- Variabili per errori non bloccanti
    * --- Per Errori Bloccanti
    * --- -
    * --- Somma valori flag bene deperibili
    * --- Variabili dal Documento di Destinazione
    * --- Generali
    * --- Pagamento
    * --- Accompagnatori
    do case
      case this.w_OPER="U"
        * --- Conferma Import
        WITH this.oParentObject.oParentObject
        this.w_FLIMPA = .w_FLIMPA
        * --- Aggiorno le variabili caller della maschera (locali nei documenti) perch� in automatico non vengono aggiornate
        .w_FLNSRI = this.w_PADRE.w_FLNSRI
        .w_FLVSRI = this.w_PADRE.w_FLVSRI
         
 .w_FLFOM = this.w_PADRE.w_FLFOM 
 .w_FLRICIVA = this.w_PADRE.w_FLRICIVA 
 .w_RIIVDTFI= this.w_PADRE.w_RIIVDTFI 
 .w_RIIVDTIN=this.w_PADRE.w_RIIVDTIN 
 .w_RIIVLSIV=this.w_PADRE.w_RIIVLSIV
        if this.w_PADRE.Class="Tgsve_kim"
          .w_FLCAUMAG= this.w_PADRE.w_FLCAUMAG 
 .w_PARCAMAG= this.w_PADRE.w_PARCAMAG
          this.w_MVCODPAG = .w_MVCODPAG
          this.w_MVSCOCL1 = .w_MVSCOCL1
          this.w_MVSCOCL2 = .w_MVSCOCL2
          this.w_MVPERFIN = .w_MVPERFIN
          this.w_MVSCOPAG = .w_MVSCOPAG
          this.w_MVTIPCON = .w_MVTIPCON
          this.w_MVCODCON = .w_MVCODCON
          this.w_MVCODVAL = .w_MVCODVAL
          this.w_MVCAOVAL = .w_MVCAOVAL
          this.w_MVRIFDIC = .w_MVRIFDIC
          this.w_MVCODIVE = .w_MVCODIVE
          this.w_MVDATDIV = .w_MVDATDIV
          this.w_MVCODBAN = .w_MVCODBAN
          this.w_MVNUMCOR = .w_MVNUMCOR
          this.w_MVCODBA2 = .w_MVCODBA2
          this.w_MVCODAGE = .w_MVCODAGE
          this.w_MVCODAG2 = .w_MVCODAG2
          this.w_FLIMAC = .w_FLIMAC
          this.w_ESCL1 = .w_ESCL1
          this.w_ESCL2 = .w_ESCL2
          this.w_ESCL3 = .w_ESCL3
          this.w_ESCL4 = .w_ESCL4
          this.w_ESCL5 = .w_ESCL5
          this.w_MVCODVET = .w_MVCODVET
          this.w_MVCODVE2 = .w_MVCODVE2
          this.w_MVCODVE3 = .w_MVCODVE3
          this.w_MVCODPOR = .w_MVCODPOR
          this.w_MVCODSPE = .w_MVCODSPE
          this.w_MVCONCON = .w_MVCONCON
          this.w_MVCODDES = .w_MVCODDES
          this.w_MVCODSED = .w_MVCODSED
          this.w_MVIVAINC = .w_MVIVAINC
          this.w_MVIVAIMB = .w_MVIVAIMB
          this.w_MVIVABOL = .w_MVIVABOL
          this.w_MVIVATRA = .w_MVIVATRA
          this.w_VARVAL = .w_VARVAL
          this.w_MVIVACAU = .w_MVIVACAU
          this.w_MVCLADOC = .w_MVCLADOC
          * --- Aggiorno le variabili caller della maschera (locali nei documenti) perch� in automatico non vengono aggiornate
          .w_FLRIDE = this.w_PADRE.w_FLRIDE
          .w_FLMGPR = this.w_PADRE.w_FLMGPR
          .w_FLMTPR = this.w_PADRE.w_FLMTPR
          * --- Se Forzati magazzini reinizializza
          if this.w_PADRE.w_FLMGPR="F"
            .w_COMAG = this.w_PADRE.w_COMAG
          endif
          if this.w_PADRE.w_FLMTPR="F"
            .w_COMAT = this.w_PADRE.w_COMAT
          endif
        else
          this.w_MVCODPAG = .w_MDCODPAG
          this.w_MVSCOCL1 = .w_MDSCOCL1
          this.w_MVSCOCL2 = .w_MDSCOCL2
          this.w_MVSCOPAG = .w_MDSCOPAG
        endif
        ENDWITH
        * --- Test Errori Bloccanti e Non Bloccanti di tutti i documenti esaminati
        this.w_BLOK = .F.
        this.w_NOBLOK = .F.
        if this.w_PADRE.Class="Tgsve_kim"
          * --- Check magazzino
          if this.w_PADRE.w_FLMGPR="F" And Empty(this.w_PADRE.w_COMAG)
            this.w_MESBLOK = "Inserire il magazzino forzato"
            this.w_BLOK = .T.
          else
            if this.w_PADRE.w_FLMTPR="F" AND NOT EMPTY(this.w_PADRE.w_CAUCOL) And Empty(this.w_PADRE.w_COMAT)
              this.w_MESBLOK = "Inserire il magazzino collegato forzato"
              this.w_BLOK = .T.
            endif
          endif
          if this.w_BLOK
            ah_ErrorMsg(this.w_MESBLOK)
            this.w_PADRE.w_TESTERR = .F.
            i_retcode = 'stop'
            return
          endif
        endif
        * --- Verifica se sono all inizio del Caricamento del Documento
        this.w_RECTRS = this.w_GEST.NumRow()
        this.w_RESOCON1 = ""
        this.w_MESBLOK = ""
        this.w_TOTSCO = 0
        this.w_TOTFIN = 0
        this.w_TOTCAU = 0
        if this.w_PADRE.Class="Tgsve_kim"
          this.w_TOTSCF = 0
          this.w_TOTACC = 0
          * --- Se documento destinazione con righe nel criterio per avvisare con warning su
          *     evetuali non omogeneit� flag beni deperibili considero anche
          *     il documento destinazione 
          if this.w_RECTRS>0
            this.w_CNTFLVABD = IIF ( Empty( this.w_GEST.w_MVFLVABD ) ,Space(1) , this.w_GEST.w_MVFLVABD )
          else
            this.w_CNTFLVABD = ""
          endif
        endif
        * --- Salva le righe del dettaglio su RigheDoc
        this.w_PADRE.NotifyEvent("ZOOMDETT before query")
        if USED("RigheDoc")
          this.w_ZOOM = this.w_PADRE.w_ZoomMast
          this.w_ZOOMD = this.w_PADRE.w_ZoomDett
          if Not Empty(this.w_PADRE.cVqrCheck)
            this.w_CHECKCURST = Sys(2015)
            this.oParentObject.w_SERIAL = Space(10)
            Vq_Exec( this.w_PADRE.cVqrCheck , this.w_PADRE , this.w_CHECKCURST ) 
          else
            this.w_CHECKCURST = this.w_Zoom.cCursor
          endif
          this.w_OREC = RECNO( this.w_CHECKCURST )
          SELECT * FROM ( this.w_CHECKCURST ) WHERE XCHK=1 INTO CURSOR APPO
          if this.w_OREC>0 AND this.w_OREC<=RECCOUNT("APPO")
            GOTO this.w_OREC
          else
            GO TOP
          endif
          this.w_RECSEL = RECCOUNT("APPO")
          this.w_RECSEL_C = this.w_RECSEL
          if this.w_PADRE.Class="Tgsve_kim"
            * --- Se dai documenti le Righe che devo considerare per i controlli sono solo quelle
            *     dello stesso ciclo
            SELECT COUNT(*) AS RIGHEOK FROM APPO WHERE Nvl(MVTIPCON,"X")=this.w_MVTIPCON INTO CURSOR APPO1
            this.w_RECSEL_C = APPO1.RIGHEOK
             
 Use In Select("appo1")
          endif
          if this.w_RECSEL>0
            * --- Crea cursore contenente tutti i riferimenti ai Documenti che devono essere importati per intero
            CREATE CURSOR INTERI (CHIAVE C(10), CODERR N(2,0), MESS C(50))
            * --- campi Testata Documento Destinazione non Significativi, Reinizializza
            if this.w_RECTRS=0
              * --- Prima importazione su documento vuoto, devo decidere su quale documento
              *     da importare eseguire i controlli di congruit� tra documenti importati
              SELECT APPO
              GO TOP
              if this.w_PADRE.Class="Tgsve_kim"
                * --- Ciclo Documentale
                LOCATE FOR NOT EMPTY(MVSERIAL) AND ; 
 ( NVL(MVTIPCON, " ")=this.w_MVTIPCON AND NVL(MVCODCON, SPACE(15))=this.w_MVCODCON ) Or ; 
 ( IIF(Not Empty(Nvl(MVCODORN,Space(15))), ; 
 NVL(MVCODORN, SPACE(15))=this.w_MVCODCON And NVL(MVTIPCON, " ")=this.w_MVTIPCON, ; 
 .F.) )
              else
                * --- POS
                LOCATE FOR NOT EMPTY(MVSERIAL) AND NOT EMPTY(NVL(MVCODCON,""))
              endif
              if FOUND()
                this.w_MVCODPAG = NVL(MVCODPAG, "     ")
                this.w_MVSCOCL1 = NVL(MVSCOCL1, 0)
                this.w_MVSCOCL2 = NVL(MVSCOCL2, 0)
                this.w_MVSCOPAG = NVL(MVSCOPAG, 0)
                if this.w_PADRE.Class="Tgsve_kim"
                  this.w_MVPERFIN = NVL(MVPERFIN, 0)
                  this.w_MVCODIVE = NVL(MVCODIVE, "     ")
                  this.w_MVRIFDIC = NVL(MVRIFDIC, SPACE(10))
                  this.w_MVDATDIV = CP_TODATE(MVDATDIV)
                  this.w_MVCODBAN = NVL(MVCODBAN, SPACE(10))
                  this.w_MVNUMCOR = NVL(MVNUMCOR, SPACE(25))
                  this.w_MVCODBA2 = NVL(MVCODBA2, SPACE(15))
                  this.w_MVCODAGE = NVL(MVCODAGE, "     ")
                  this.w_MVCODAG2 = NVL(MVCODAG2, "     ")
                  this.w_MVCODVET = NVL(MVCODVET, "     ")
                  this.w_MVCODVE2 = NVL(MVCODVE2, "     ")
                  this.w_MVCODVE3 = NVL(MVCODVE3, "     ")
                  this.w_MVCODPOR = NVL(MVCODPOR, " ")
                  this.w_MVCODSPE = NVL(MVCODSPE, "   ")
                  this.w_MVCONCON = NVL(MVCONCON, " ")
                  this.w_MVCODDES = NVL(MVCODDES,"     ")
                  this.w_MVCODSED = NVL(MVCODSED,"     ")
                  this.w_MVCODORN = NVL(MVCODORN,"     ")
                  this.w_MVIVAINC = Nvl(MVIVAINC,Space(5))
                  this.w_MVIVAIMB = Nvl(MVIVAIMB,Space(5))
                  this.w_MVIVABOL = Nvl(MVIVABOL,Space(5))
                  this.w_MVIVATRA = Nvl(MVIVATRA,Space(5))
                  this.w_MVIVACAU = Nvl(MVIVACAU, Space(5))
                endif
              endif
              * --- Devo aggiornare il valore di FLCONG sul cursore di controllo APPO
              if Upper(this.w_GEST.CLASS) <> "TGSPS_MVD"
                Local L_Macro 
 L_Macro = cp_SetSqlFunctions(AH_CONGDOC(1, this.w_MVCODVAL+"F", left(this.w_FLIMPA+space(1), 1)+left(this.w_FLIMAC+space(1), 1), this.w_MVCODIVE, this.w_MVTIPCON, this.w_MVRIFDIC, this.w_MVCODPAG, this.w_MVDATDIV, this.w_MVCODBA2, this.w_MVCODAG2, this.w_MVIVAINC, this.w_MVIVAIMB, this.w_MVIVATRA, this.w_MVIVABOL, this.w_MVIVACAU, this.w_MVCODVET, this.w_MVCODVE2, this.w_MVCODVE3, this.w_MVCODPOR, this.w_MVCODSPE, Left(this.w_MVCONCON+space(1), 1) + Left(this.w_MVCODDES+space(5), 5), Left(this.w_MVCODCON+space(15), 15) + Left(this.w_MVCLADOC+space(2), 2), Left(this.w_MVCODORN+space(15), 15) + Left(this.w_MVCODSED+space(5), 5), Left(this.w_MVCODAGE+space(5), 5) + Left(this.w_MVCODBAN+space(10), 10), this.w_MVSCOCL1, this.w_MVSCOCL2, this.w_MVSCOPAG),"VFP") 
 L_MACRO=STRTRAN(L_MACRO,"DOC_MAST.","APPO.") 
 L_MACRO=STRTRAN(L_MACRO,"DOC_COLL.DCFLEVAS","APPO.FLINTE") 
 L_MACRO=STRTRAN(L_MACRO,"DOC_COLL.","APPO.") 
 L_MACRO=STRTRAN(L_MACRO,"VALUTE.","APPO.") 
 =WrCursor("APPO") 
 UPDATE APPO SET FLCONG=&L_MACRO
              endif
            endif
            * --- Cicla sui Documenti Selezionati
            this.w_CODAPP = SPACE(10)
            this.w_CHKOK = 0
            SELECT APPO
            GO TOP
            SCAN FOR NOT EMPTY(MVSERIAL)
            * --- Variabili Controllo Errori per ogni documento esaminato
            this.w_DOCBLOK = .F.
            this.w_DOCWARN = .F.
            * --- Variabili contenenti gli errori bloccanti e solo warning di tutti i documenti esaminati
            this.w_TEST = this.w_TEST + this.w_oMESS1.ComposeMessage()
            this.w_TESBLOK = this.w_TESBLOK + this.w_oMESS.ComposeMessage()
            this.w_DATIDOC = ah_Msgformat("Documento di origine: n. %1 del %2", ALLTR(STR(NVL(MVNUMDOC,0),15)) + IIF(EMPTY(NVL(MVALFDOC, "")), "", "/"+Alltrim(MVALFDOC)) , DTOC(CP_TODATE(MVDATDOC)) )
            * --- RESOCON1
            this.w_oPART1 = this.w_oMESS1.addmsgpartNL("%1")
            this.w_oPART1.addParam(this.w_DATIDOC)     
            * --- MESBLOK
            this.w_oPART = this.w_oMESS.addmsgpartNL("%1")
            this.w_oPART.addParam(this.w_DATIDOC)     
            if this.w_PADRE.Class="Tgsve_kim"
              if FLCONG="1"
                this.w_oMESS.AddMsgPartNL("Valute incongruenti")     
                this.w_BLOK = .T.
              endif
              * --- Se eseguo l'import nei documenti (non nel POS) e il documento di origine non 
              *     � dello stesso ciclo, non devo fare nessun controllo tranne quello della valuta.
              *     Quindi skippo al record sucessivo
              if NVL(MVTIPCON, " ")<>this.w_MVTIPCON
                if NOT this.w_DOCWARN
                  * --- Se non trovo nessun Warning sbianco l'oggetto 
                  *     altrimenti rimarrebbe sporco del riferimento al documento corrente
                  * --- Sbianco l'oggetto
                  this.w_oMESS1.ComposeMessage()
                endif
                if NOT this.w_DOCBLOK
                  * --- Se non trovo nessun Errore Bloccante sbianco l'oggetto 
                  *     altrimenti rimarrebbe sporco del riferimento al documento corrente
                  * --- Sbianco l'oggetto
                  this.w_oMESS.ComposeMessage()
                endif
                Loop
              endif
              if FLCONG="2" 
                this.w_oMESS.AddMsgPartNL("Codici IVA esenzione incongruenti")     
                this.w_DOCBLOK = .T.
              endif
            endif
            if this.w_PADRE.Class="Tgsve_kim"
              do case
                case FLCONG="3"
                  this.w_oMESS.AddMsgPartNL("Pagamenti incongruenti")     
                  this.w_DOCBLOK = .T.
                case FLCONG="4"
                  this.w_oMESS.AddMsgPartNL("Sconti di piede incongruenti")     
                  this.w_DOCBLOK = .T.
              endcase
            else
              * --- POS: i controlli sono soltanto i due seguenti
              if this.w_FLIMPA="S" AND NOT EMPTY(NVL(MVCODCON,"")) 
                if NVL(MVCODPAG, SPACE(5))<>this.w_MVCODPAG
                  this.w_oMESS.AddMsgPartNL("Pagamenti incongruenti")     
                  this.w_DOCBLOK = .T.
                endif
                if NVL(MVSCOCL1, 0)<>this.w_MVSCOCL1 OR NVL(MVSCOCL2, 0)<>this.w_MVSCOCL2 OR NVL(MVSCOPAG, 0)<>this.w_MVSCOPAG
                  this.w_oMESS.AddMsgPartNL("Sconti di piede incongruenti")     
                  this.w_DOCBLOK = .T.
                endif
              endif
            endif
            if this.w_PADRE.Class="Tgsve_kim"
              if this.w_FLIMPA="S" AND NOT EMPTY(NVL(MVCODCON,"")) AND NVL(DCFLINTE," ")<>"S"
                if NVL(MVPERFIN, 0)<>this.w_MVPERFIN
                  this.w_oMESS.AddMsgPartNL("Sconto finanziario incongruente")     
                  this.w_DOCBLOK = .T.
                endif
                if CP_TODATE(MVDATDIV)<>this.w_MVDATDIV 
                  this.w_oMESS.AddMsgPartNL("Data diversa disomogenea")     
                  this.w_DOCBLOK = .T.
                endif
                if NVL(MVNUMCOR, SPACE(25))<>this.w_MVNUMCOR
                  this.w_oMESS.AddMsgPartNL("Conto corrente incongruente")     
                  this.w_DOCBLOK = .T.
                endif
                if this.w_MVCODVAL= Nvl(MVCODVAL," ") AND this.w_MVCAOVAL<>Nvl(MVCAOVAL,0)
                  * --- Stessa valuta ma con cambio differente
                  this.w_oMESS1.AddMsgPartNL("Selezionati documenti con cambio incongruente")     
                  this.w_DOCWARN = .T.
                endif
              endif
              do case
                case FLCONG="5"
                  this.w_oMESS.AddMsgPartNL("Banche di riferimento incongruenti")     
                  this.w_DOCBLOK = .T.
                case FLCONG="6"
                  this.w_oMESS.AddMsgPartNL("Codici agenti incongruenti")     
                  this.w_DOCBLOK = .T.
                case FLCONG="9"
                  this.w_oMESS.AddMsgPartNL("Codici IVA per le spese/imballi incongruenti")     
                  this.w_DOCBLOK = .T.
                case FLCONG="7"
                  * --- Incongruenza nei dati Accompagnatori
                  this.w_oMESS.AddMsgPartNL("Dati accompagnatori incongruenti%0Controllare: vettori, porto, spedizione, condizioni di consegna%0sede di destinazione e provenienza merce, per conto di..")     
                  this.w_DOCBLOK = .T.
              endcase
              * --- Conta Numero Sconti Finanziari Selezionati
              this.w_TOTFIN = this.w_TOTFIN + IIF(NVL(MVFLSFIN," ")="S", 1, 0)
              * --- Conta numero forza cauzione
              this.w_TOTCAU = this.w_TOTCAU + IIF(NVL(MVFLFOCA," ")="S", 1, 0)
            endif
            * --- Ulteriori controlli per l'import
            if this.w_PADRE.Class="Tgsve_kim" AND NOT EMPTY(FLCONG)
              this.Page_3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            * --- Conta Numero Dati di Piede Selezionati
            this.w_TOTSCO = this.w_TOTSCO + IIF(NVL(MVFLFOSC," ")="S", 1, 0)
            if this.w_TOTFIN>0 AND this.w_RECSEL_C>1
              if Not IsAlt()
                * --- Se Seleziono un Documento con Sconti di Piede Forzati posso importare solo quello e per Intero
                this.w_oMESS.AddMsgPartNL("Selezionare al massimo un documento con sconti finanziari forzati")     
                this.w_DOCBLOK = .T.
              else
                * --- Se Seleziono un Documento con Sconti di Piede Forzati posso importare solo quello e per Intero
                * --- Variabile per messaggio d'errore
                if this.w_GEST.class="Tgsve_mdv" 
                  if this.w_GEST.w_MSG_SCFIN="N"
                    * --- Non arrivo da Nuovo Documento (GSAL_KGD=>GSAL_BGD)
                    *     gestione dell'errore normale
                    this.w_oMESS1.AddMsgPartNL("con sconto finanziario.")     
                    this.w_oMESS1.AddMsgPartNL("L'importo della rata del documento di destinazione potrebbe divergere dalla somma delle rate dei documenti di origine.")     
                    this.w_oMESS1.AddMsgPartNL("Effettuare un controllo ed eventualmente modificare manualmente lo sconto finanziario")     
                    this.w_DOCWARN = .T.
                  else
                    * --- Arrivo da Nuovo Documento (GSAL_KGD=>GSAL_BGD)
                    *     la gestione dell'errore non deve interrompere la creazione del documento.
                    *     Il messaggio viene dato alla fine dell'importazione (in GSAL_BGD)
                    this.w_GEST.w_MSG_SCFIN = "S"
                  endif
                endif
              endif
            endif
            this.w_TOTACO = this.w_TOTACO + IIF(NOT EMPTY(NVL(MVFLCAPA," ")) AND NVL(MVACCONT, 0)<>0, 1, 0)
            if this.w_TOTACO>1 AND this.w_RECSEL_C>1
              * --- Se Acconti, anche piu di un documento purche' uno solo con Acconti e per Intero
              this.w_oMESS1.AddMsgPartNL("Selezionati pi� documenti con acconti contestuali di tipo caparra")     
              this.w_DOCWARN = .T.
            endif
            if this.w_PADRE.Class="Tgsve_kim"
              this.w_TOTSCF = this.w_TOTSCF + IIF(NVL(MVFLSCAF," ")="S", 1, 0)
              if this.w_TOTSCF>0 AND this.w_RECSEL_C>1
                * --- Se Seleziono un Documento con Scadenze Fissate posso importare solo quello e per Intero
                this.w_oMESS.AddMsgPartNL("Selezionare al massimo un documento con scadenze fissate")     
                this.w_DOCBLOK = .T.
              endif
              this.w_TOTACC = this.w_TOTACC + IIF(NVL(MVSPEIMB, 0)<>0 OR NVL(MVSPETRA, 0)<>0, 1, 0)
            endif
            * --- Controllo spese accessorie solo su ciclo documentale
            if NVL(MVFLFOSC," ")="S" OR (NOT EMPTY(NVL(MVFLCAPA," ")) AND NVL(MVACCONT, 0)<>0) OR IIF(this.w_PADRE.Class="Tgsve_kim",(NVL(MVFLSCAF," ")="S" And Not this.w_VARVAL $"+-") Or NVL(MVFLFOCA," ")="S" OR NVL(MVSPETRA, 0)<>0 OR NVL(MVSPEIMB, 0)<>0,.T.)
              * --- Eventuale Segnalazione di errore Contestuale al Documento da verificare
              this.w_CODAPP = MVSERIAL
              * --- Da POS MVFLSCAF non esiste. La prima condizione � falsa quindi passa al secondo parametro dell'IIF
              this.w_CHKOK = IIF(this.w_PADRE.Class="Tgsve_kim" and NVL(MVFLSCAF," ")="S" And Not this.w_VARVAL $"+-", 14, IIF(NVL(MVFLFOSC," ")="S", 15, IIF(NOT EMPTY(NVL(MVFLCAPA," ")) AND NVL(MVACCONT, 0)<>0, 18, IIF(this.w_PADRE.Class="Tgsve_kim" and NVL(MVFLFOCA," ")="S",17,16))))
              * --- Memorizza la Chiave Documento trovato con Scadenze Fissate, Sconti Forzati o Spese Accessorie per eventuali controlli successivi (Import Totale)
              INSERT INTO INTERI (CHIAVE, CODERR, MESS) VALUES (this.w_CODAPP, this.w_CHKOK, this.w_DATIDOC)
            endif
            * --- Solo per ciclo documentale
            if this.w_PADRE.Class="Tgsve_kim" And Not (NVL(MVFLVABD, " ") $ this.w_CNTFLVABD)
              * --- w_CNTFLVABD contiene la somma dei valori assunti dal flag
              this.w_CNTFLVABD = this.w_CNTFLVABD + IIF ( Empty( NVL(MVFLVABD, " ") ) ,Space(1) , NVL(MVFLVABD, " ") )
            endif
            if NOT this.w_DOCWARN
              * --- Se non trovo nessun Warning sbianco l'oggetto 
              *     altrimenti rimarrebbe sporco del riferimento al documento corrente
              * --- Sbianco l'oggetto
              this.w_oMESS1.ComposeMessage()
            else
              * --- Altrimenti Inserisco Warning attivo
              this.w_NOBLOK = .T.
            endif
            if NOT this.w_DOCBLOK
              * --- Se non trovo nessun Errore Bloccante sbianco l'oggetto 
              *     altrimenti rimarrebbe sporco del riferimento al documento corrente
              * --- Sbianco l'oggetto
              this.w_oMESS.ComposeMessage()
            else
              * --- Altrimenti Inserisco Blocco attivo
              this.w_BLOK = .T.
            endif
            ENDSCAN
            if NOT EMPTY(this.w_CODAPP) And USED("INTERI")
              SELECT INTERI
              SCAN FOR NOT EMPTY(CHIAVE)
              this.w_CODAPP = INTERI.CHIAVE
              this.w_CHKOK = INTERI.CODERR
              this.w_DATIDOC = INTERI.MESS
              * --- Verifica se il Documento e' stato importato per intero
              * --- Select from QUERY\GSVE2KIM
              do vq_exec with 'QUERY\GSVE2KIM',this,'_Curs_QUERY_GSVE2KIM','',.f.,.t.
              if used('_Curs_QUERY_GSVE2KIM')
                select _Curs_QUERY_GSVE2KIM
                locate for 1=1
                do while not(eof())
                this.w_QTATOT = NVL(_Curs_QUERY_GSVE2KIM.QTATOT,0)
                * --- Leggo i decimali globali della valuta...
                this.w_APPO = getValut( _Curs_QUERY_GSVE2KIM.MVCODVAL , "VADECTOT" )
                this.w_PRETOT = cp_ROUND(NVL(_Curs_QUERY_GSVE2KIM.PRETOT,0), this.w_APPO)
                this.w_TOTROW = NVL(_Curs_QUERY_GSVE2KIM.TOTROW,0)
                 
 Select DISTINCT CPROWNUM, MVQTAEVA, MVIMPEVA, XCHK FROM RigheDoc ; 
 WHERE MVSERIAL=this.w_CODAPP AND ( XCHK=1 OR NVL(MVQTAEVA,0)<>0 OR NVL(MVIMPEVA,0)<>0) ; 
 INTO CURSOR APPOFIS
                 
 Select APPOFIS 
 SUM MVQTAEVA, MVIMPEVA, XCHK TO this.w_QTATOT1, this.w_PRETOT1, this.w_TOTROW1
                this.w_PRETOT1 = cp_ROUND(this.w_PRETOT1, this.w_APPO)
                USE IN SELECT("AppoFis")
                if Not ( (this.w_QTATOT=this.w_QTATOT1 AND this.w_PRETOT=this.w_PRETOT1) OR this.w_TOTROW=this.w_TOTROW1 )
                  if this.w_CHKOK=14 and this.w_PADRE.Class="Tgsve_kim"
                    * --- Controllo scadenze fissate
                    *     Solo ciclo documentale
                    this.w_oPART = this.w_oMESS.addmsgpartNL("%1%0Un documento con scadenze confermate non pu� essere importato parzialmente")
                    this.w_oPART.addParam(this.w_DATIDOC)     
                    this.w_BLOK = .T.
                  endif
                  if this.w_CHKOK=15
                    this.w_oPART1 = this.w_oMESS1.addmsgpartNL("%1%0Documento con sconti di piede forzati importato parzialmente")
                    this.w_oPART1.addParam(this.w_DATIDOC)     
                    this.w_NOBLOK = .T.
                  endif
                  if this.w_CHKOK=16 and this.w_PADRE.Class="Tgsve_kim"
                    * --- Controllo su spese accessorie
                    *     Solo per ciclo documentale
                    if Not IsAlt()
                      if this.w_TOTACC>1
                        this.w_oPART1 = this.w_oMESS1.addmsgpartNL("%1%0Selezionati pi� documenti con spese accessorie%0e documento con spese accessorie importato parzialmente")
                        this.w_oPART1.addParam(this.w_DATIDOC)     
                        this.w_NOBLOK = .T.
                      else
                        this.w_oPART1 = this.w_oMESS1.addmsgpartNL("%1%0Documento con spese accessorie importato parzialmente")
                        this.w_oPART1.addParam(this.w_DATIDOC)     
                        this.w_NOBLOK = .T.
                      endif
                    endif
                  endif
                  if this.w_CHKOK=17 and this.w_PADRE.Class="Tgsve_kim"
                    this.w_oPART1 = this.w_oMESS1.addmsgpartNL("%1%0Documento con cauzione forzata importato parzialmente")
                    this.w_oPART1.addParam(this.w_DATIDOC)     
                    this.w_NOBLOK = .T.
                  endif
                  if this.w_CHKOK=18
                    this.w_oPART1 = this.w_oMESS1.addmsgpartNL("%1%0Selezionato parzialmente documento con acconto contestuale di tipo caparra")
                    this.w_oPART1.addParam(this.w_DATIDOC)     
                    this.w_NOBLOK = .T.
                  endif
                endif
                  select _Curs_QUERY_GSVE2KIM
                  continue
                enddo
                use
              endif
              SELECT INTERI
              ENDSCAN 
              SELECT RigheDoc
            endif
            if this.w_TOTSCO>1 AND this.w_RECSEL>1
              * --- se seleziono un Documento con Sconti di Piede Forzati posso importare anche altri documenti con sconti forzati
              this.w_oMESS1.AddMsgPartNL("Selezionati pi� documenti con sconti forzati")     
              this.w_DOCWARN = .T.
            endif
            if this.w_PADRE.Class="Tgsve_kim"
              * --- Nessun Errore ma presenti spese accessorie
              if this.w_TOTACC>1 AND Not( this.w_BLOK and this.w_NOBLOK) AND Not IsAlt()
                * --- Se Spese Accesorie, anche piu di un documento purche' uno solo con Spese Accessorie e per Intero
                this.w_oMESS1.AddMsgPartNL("Selezionati pi� documenti con spese accessorie%0Nel caso di importazione ripetuta le spese verranno sommate a quelle gi� presenti")     
                this.w_NOBLOK = .T.
              endif
              * --- Nessun Errore ma presenti cauzioni forzate
              if this.w_TOTCAU>1 AND this.w_RECSEL>1
                * --- Se Spese Accesorie, anche piu di un documento purche' uno solo con Spese Accessorie e per Intero
                this.w_oMESS1.AddMsgPartNL("Selezionati pi� documenti con forza cauzione")     
                this.w_DOCWARN = .T.
              endif
              * --- Se ho pi� di un valore per il flag beni deperibili tra 
              *     l'insieme dei documenti che compongono la destinazione
              *     segnalo con un Warning 
              if Len( this.w_CNTFLVABD)>1
                if this.w_RECTRS=0
                  this.w_oMESS1.AddMsgPartNL("Selezionati uno / pi� documenti con flag beni deperibili non uniformi%0Il documento sar� considerato di tipo beni deperibili")     
                else
                  this.w_oMESS1.AddMsgPartNL("Documento di destinazione e documenti di origine con flag beni deperibili non uniformi%0In questo caso verranno mantenute le impostazioni del documento di destinazione")     
                endif
                this.w_NOBLOK = .T.
              endif
            endif
            if this.w_PADRE.Class="Tgsve_kim"
              * --- Controllo righe evase senza Import (Attivo check evaso e imputata Qt� 0)
              *     Non deve essere permesso se la riga � gi� stata precedentemente evasa parzialmante
               
 Select DISTINCT CPROWNUM, CPROWORD, MVSERIAL FROM (this.w_ZoomD.cCursor) ; 
 WHERE XCHK=1 And ( (NVL(MVQTAEVA,0)=0 And MVTIPRIG<>"F") OR (NVL(MVIMPEVA,0)=0 And MVTIPRIG="F") ) And ; 
 MVTIPRIG<>"D" And FLDOCU<>"S" ; 
 INTO CURSOR EvaRighe
              if USED("EvaRighe") And Reccount("EvaRighe")>0
                Select EvaRighe 
 Go Top 
 Scan
                this.w_SERIAL = MVSERIAL
                this.w_ROWNUM = CPROWNUM
                * --- Lettura quantit� evasa dal documento di origine
                * --- Read from DOC_DETT
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.DOC_DETT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "MVQTAEVA"+;
                    " from "+i_cTable+" DOC_DETT where ";
                        +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                        +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    MVQTAEVA;
                    from (i_cTable) where;
                        MVSERIAL = this.w_SERIAL;
                        and CPROWNUM = this.w_ROWNUM;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_QTAEVA = NVL(cp_ToDate(_read_.MVQTAEVA),cp_NullValue(_read_.MVQTAEVA))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                if this.w_QTAEVA<>0
                  this.w_oPART = this.w_oMESS.addmsgpartNL("%1%0Riga %2 gi� evasa parzialmente%0Impossibile evadere senza importazione")
                  this.w_oPART.addParam(this.w_DATIDOC)     
                  this.w_oPART.addParam(Alltrim(Str(CPROWORD)))     
                  this.w_BLOK = .T.
                endif
                EndScan
              endif
              Use in Select("EvaRighe")
            endif
          else
            * --- Nessun Record da Esportare
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            i_retcode = 'stop'
            return
          endif
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.w_BLOK OR this.w_NOBLOK
            * --- Errori
            * --- Lancio la Maschera dei Log Errori
            this.w_ANNULLA = .F.
            * --- Queste ComposeMessage aggiungono i messaggi relativi all'ultimo documento del ciclo + quelli fuori dalla scan principale
            this.w_RESOCON1 = this.w_TEST + this.w_oMESS1.ComposeMessage()
            this.w_MESBLOK = this.w_TESBLOK + this.w_oMESS.ComposeMessage()
            do GSVE_KLG with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Se Presenti Messaggi Bloccanti non posso salvare la maschera di importazione
            *     e quindi non esegue l'importazione (Evento 'ImportaDoc' notificato qui di seguito)
            if Not(this.w_BLOK)
              * --- Se non esistono messaggi bloccanti ma premo Annulla sulla maschera di Log
              *     annullo comunque l'importazione
              this.w_BLOK = Not this.w_ANNULLA
            endif
            if this.w_BLOK
              this.w_PADRE.w_TESTERR=.F.
              i_retcode = 'stop'
              return
            endif
          endif
          if Not( this.w_BLOK )
            * --- Nessun Errore: Importo i documenti selezionati
            if VarType( this.oParentObject.w_GESTPARENT )="O"
              * --- controlli per ricalcolo iva
              if vartype(this.oParentObject.w_GESTPARENT.w_FLRICIVA)="L" and vartype(this.oParentObject.w_FLRICIVA)="L"
                this.oParentObject.w_GESTPARENT.w_FLRICIVA = this.oParentObject.w_FLRICIVA
              endif
              if vartype(this.oParentObject.w_GESTPARENT.w_RIIVDTIN)<>"U" and vartype(this.oParentObject.w_RIIVDTIN)<>"U"
                this.oParentObject.w_GESTPARENT.w_RIIVDTIN = this.oParentObject.w_RIIVDTIN
              endif
              if vartype(this.oParentObject.w_GESTPARENT.w_RIIVDTFI)<>"U" and vartype(this.oParentObject.w_RIIVDTFI)<>"U"
                this.oParentObject.w_GESTPARENT.w_RIIVDTFI = this.oParentObject.w_RIIVDTFI
              endif
              if vartype(this.oParentObject.w_GESTPARENT.w_RIIVLSIV)<>"U" and vartype(this.oParentObject.w_RIIVLSIV)<>"U"
                this.oParentObject.w_GESTPARENT.w_RIIVLSIV = this.oParentObject.w_RIIVLSIV
              endif
            endif
             
 I_curform=this.w_PADRE
            if VarType( this.oParentObject.w_GESTPARENT )="O" And this.oParentObject.w_GESTPARENT<>this.w_GEST
              this.oParentObject.w_GESTPARENT.NotifyEvent("ImportaDoc")     
            else
              this.w_GEST.NotifyEvent("ImportaDoc")     
            endif
          endif
        endif
      case this.w_OPER="A"
        * --- Abbandona Import (chiude i Cursori)
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiusura cursori
    if this.w_OPER="A"
       
 USE IN SELECT("Righedoc") 
 USE IN SELECT("AppCur")
    else
       
 USE IN SELECT("Interi") 
 USE IN SELECT("Appo")
    endif
    if Not Empty(this.w_PADRE.cVqrCheck) and not Empty( this.w_CHECKCURST )
       
 USE IN SELECT( this.w_CHECKCURST )
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Dato che attraverso il valore di FLCONG si pu� eseguire un solo controllo, si devono quindi effettuare tutti gli altri.
    if FLCONG<>"1" AND NVL(MVCODVAL, "xxxx")<>this.w_MVCODVAL AND NOT (this.w_MVCODVAL=g_CODEUR AND NVL(VACAOVAL,0)<>0)
      this.w_oMESS.AddMsgPartNL("Valute incongruenti")     
      this.w_BLOK = .T.
    endif
    if FLCONG<>"2" AND NVL(MVTIPCON, " ")=this.w_MVTIPCON AND (NVL(MVCODCON, SPACE(15))=this.w_MVCODCON OR NVL(MVCODORN, SPACE(15))=this.w_MVCODCON) AND NVL(MVCODIVE, SPACE(5))<>this.w_MVCODIVE 
      * --- Verifica eventuale Esenzione con attivo flag Import dati Pagamento
      * --- Se uno dei due documenti non ha lettera di intento ma Ha Codice Iva Esenzione 
      *     diverso dal documento di destinazione, devo bloccare l'importazione
      if ( ( Empty(this.w_MVRIFDIC) And Not Empty(this.w_MVCODIVE) ) Or (Empty(Nvl(MVRIFDIC," ")) And Not Empty(Nvl(MVCODIVE," ")))) AND this.w_FLIMPA="S"
        this.w_oMESS.AddMsgPartNL("Codici IVA esenzione incongruenti")     
        this.w_DOCBLOK = .T.
      endif
    endif
    if FLCONG<>"5" AND ( NVL(MVCODBAN, SPACE(10))<>this.w_MVCODBAN OR NVL(MVCODBA2, SPACE(15))<>this.w_MVCODBA2 )
      this.w_oMESS.AddMsgPartNL("Banche di riferimento incongruenti")     
      this.w_DOCBLOK = .T.
    endif
    if FLCONG<>"6" AND ( NVL(MVCODAGE, SPACE(5))<>this.w_MVCODAGE OR NVL(MVCODAG2, SPACE(5))<>this.w_MVCODAG2 )
      this.w_oMESS.AddMsgPartNL("Codici agenti incongruenti")     
      this.w_DOCBLOK = .T.
    endif
    if FLCONG<>"9" AND ( (NVL(MVIVAINC, Space(5))<>this.w_MVIVAINC And Not Empty(Nvl(MVSPEINC,0))) OR (NVL(MVIVATRA, Space(5))<>this.w_MVIVATRA And Not Empty(Nvl(MVSPETRA,0))) OR (NVL(MVIVABOL, Space(5))<>this.w_MVIVABOL And Not Empty(Nvl(MVSPEBOL,0))) OR (NVL(MVIVAIMB, Space(5))<>this.w_MVIVAIMB And Not Empty(Nvl(MVSPEIMB,0))) )
      this.w_oMESS.AddMsgPartNL("Codici IVA per le spese/imballi incongruenti")     
      this.w_DOCBLOK = .T.
    endif
    * --- Import dati Accompagnatori
    if FLCONG<>"7" AND this.w_FLIMAC="S" AND NOT EMPTY(NVL(MVCODCON,""))
      this.w_OKACC = .F.
      * --- Vettori, Porto, Mezzo di Spedizione, Condizione di Consegna, Destinatario (Per conto Di), Prov. Merce Incongruenti
      this.w_OKACC = NVL(MVCODVET, SPACE(5))<>this.w_MVCODVET OR NVL(MVCODVE2, SPACE(5))<>this.w_MVCODVE2 OR NVL(MVCODVE3, SPACE(5))<>this.w_MVCODVE3
      this.w_OKACC = this.w_OKACC Or (NVL(MVCODPOR, " ")<>this.w_MVCODPOR OR NVL(MVCODSPE, "   ")<>this.w_MVCODSPE OR NVL(MVCONCON, " ")<>this.w_MVCONCON)
      * --- Controllo la destinazione solo se non sto importando tramite Per conto Di.
      this.w_OKACC = this.w_OKACC Or ( NVL(MVCODDES, SPACE(5))<>this.w_MVCODDES And Not Empty(NVL(MVCODORN, SPACE(5))) And this.w_MVCODCON <> NVL(MVCODORN, SPACE(5)))
      this.w_OKACC = this.w_OKACC Or NVL(MVCODDES, SPACE(5))<>this.w_MVCODDES And Empty(NVL(MVCODORN, SPACE(5))) And Empty(this.w_MVCODORN)
      this.w_OKACC = this.w_OKACC Or ( NVL(MVCODSED, SPACE(5))<>this.w_MVCODSED )
      * --- Controllo Il Per conto di Solo se non sto Importando tramite il Per Conto Di
      this.w_OKACC = this.w_OKACC Or ( NVL(MVCODORN, SPACE(5))<>this.w_MVCODORN And NVL(MVCODORN, SPACE(5)) <> this.w_MVCODCON)
      if this.w_OKACC
        * --- Incongruenza nei dati Accompagnatori
        this.w_oMESS.AddMsgPartNL("Dati accompagnatori incongruenti%0Controllare: vettori, porto, spedizione, condizioni di consegna%0sede di destinazione e provenienza merce, per conto di..")     
        this.w_DOCBLOK = .T.
      endif
    endif
    if this.w_FLIMPA="S" AND NOT EMPTY(NVL(MVCODCON,"")) AND NVL(DCFLINTE," ")<>"S"
      if FLCONG<>"3" AND NVL(MVCODPAG, SPACE(5))<>this.w_MVCODPAG
        this.w_oMESS.AddMsgPartNL("Pagamenti incongruenti")     
        this.w_DOCBLOK = .T.
      endif
      if FLCONG<>"4" AND ( NVL(MVSCOCL1, 0)<>this.w_MVSCOCL1 OR NVL(MVSCOCL2, 0)<>this.w_MVSCOCL2 OR NVL(MVSCOPAG, 0)<>this.w_MVSCOPAG )
        this.w_oMESS.AddMsgPartNL("Sconti di piede incongruenti")     
        this.w_DOCBLOK = .T.
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='DOC_DETT'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_QUERY_GSVE2KIM')
      use in _Curs_QUERY_GSVE2KIM
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
