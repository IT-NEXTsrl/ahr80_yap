* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bsm                                                        *
*              Verifica flag destinatario stampa                               *
*                                                                              *
*      Author: Zucchetti S.p.a.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-09-01                                                      *
* Last revis.: 2012-05-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bsm",oParentObject,m.pOPE)
return(i_retval)

define class tgsar_bsm as StdBatch
  * --- Local variables
  pOPE = space(10)
  w_AZIENDA = space(5)
  * --- WorkFile variables
  SEDIAZIE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica che il flag 'Sede preferita per le mappe' 
    *     sia impostato e associa la sede predefinita per le mappe
    *     Stesso controllo su sede pref. consegna merce (solo AHR)
    this.w_AZIENDA = i_CODAZI
    if EMPTY(this.pOPE)
      this.pOPE = "UPD"
    endif
    do case
      case this.pOPE="UPD"
        if this.oParentObject.w_SEPREMAP="S"
          * --- Select from SEDIAZIE
          i_nConn=i_TableProp[this.SEDIAZIE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SEDIAZIE_idx,2],.t.,this.SEDIAZIE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select SECODDES  from "+i_cTable+" SEDIAZIE ";
                +" where SECODAZI="+cp_ToStrODBC(this.w_AZIENDA)+" and   SECODDES<>"+cp_ToStrODBC(this.oParentObject.w_SECODDES)+" and   SEPREMAP='S'";
                 ,"_Curs_SEDIAZIE")
          else
            select SECODDES from (i_cTable);
             where SECODAZI=this.w_AZIENDA and   SECODDES<>this.oParentObject.w_SECODDES and   SEPREMAP="S";
              into cursor _Curs_SEDIAZIE
          endif
          if used('_Curs_SEDIAZIE')
            select _Curs_SEDIAZIE
            locate for 1=1
            do while not(eof())
            this.oParentObject.w_OLDMAPPE = NVL(_Curs_SEDIAZIE.SECODDES, SPACE(5))
              select _Curs_SEDIAZIE
              continue
            enddo
            use
          endif
          if NOT EMPTY(this.oParentObject.w_OLDMAPPE)
            * --- Write into SEDIAZIE
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SEDIAZIE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SEDIAZIE_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SEDIAZIE_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SEPREMAP ="+cp_NullLink(cp_ToStrODBC("N"),'SEDIAZIE','SEPREMAP');
                  +i_ccchkf ;
              +" where ";
                  +"SECODAZI = "+cp_ToStrODBC(this.w_AZIENDA);
                  +" and SECODDES <> "+cp_ToStrODBC(this.oParentObject.w_SECODDES);
                  +" and SEPREMAP = "+cp_ToStrODBC("S");
                     )
            else
              update (i_cTable) set;
                  SEPREMAP = "N";
                  &i_ccchkf. ;
               where;
                  SECODAZI = this.w_AZIENDA;
                  and SECODDES <> this.oParentObject.w_SECODDES;
                  and SEPREMAP = "S";

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        endif
        if isahr()
          if nvl(this.oParentObject.w_SEDESPRE," ")="S"
            * --- Select from SEDIAZIE
            i_nConn=i_TableProp[this.SEDIAZIE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SEDIAZIE_idx,2],.t.,this.SEDIAZIE_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select SECODDES  from "+i_cTable+" SEDIAZIE ";
                  +" where SECODAZI="+cp_ToStrODBC(this.w_AZIENDA)+" and   SECODDES<>"+cp_ToStrODBC(this.oParentObject.w_SECODDES)+" and   SEDESPRE='S'";
                   ,"_Curs_SEDIAZIE")
            else
              select SECODDES from (i_cTable);
               where SECODAZI=this.w_AZIENDA and   SECODDES<>this.oParentObject.w_SECODDES and   SEDESPRE="S";
                into cursor _Curs_SEDIAZIE
            endif
            if used('_Curs_SEDIAZIE')
              select _Curs_SEDIAZIE
              locate for 1=1
              do while not(eof())
              this.oParentObject.w_OLDDESPRE = NVL(_Curs_SEDIAZIE.SECODDES, SPACE(5))
                select _Curs_SEDIAZIE
                continue
              enddo
              use
            endif
            if NOT EMPTY(this.oParentObject.w_OLDDESPRE)
              * --- Write into SEDIAZIE
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SEDIAZIE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SEDIAZIE_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SEDIAZIE_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SEDESPRE ="+cp_NullLink(cp_ToStrODBC(" "),'SEDIAZIE','SEDESPRE');
                    +i_ccchkf ;
                +" where ";
                    +"SECODAZI = "+cp_ToStrODBC(this.w_AZIENDA);
                    +" and SECODDES <> "+cp_ToStrODBC(this.oParentObject.w_SECODDES);
                    +" and SEDESPRE = "+cp_ToStrODBC("S");
                       )
              else
                update (i_cTable) set;
                    SEDESPRE = " ";
                    &i_ccchkf. ;
                 where;
                    SECODAZI = this.w_AZIENDA;
                    and SECODDES <> this.oParentObject.w_SECODDES;
                    and SEDESPRE = "S";

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
          endif
        endif
      case this.pOPE="MSG"
        if not empty(this.oParentObject.w_OLDMAPPE)
          ah_ErrorMsg("Attenzione: %1 non sar� pi� la sede preferita per le mappe",,, this.oParentObject.w_OLDMAPPE)
        endif
        if isahr() AND not EMPTY(this.oParentObject.w_OLDDESPRE)
          ah_ErrorMsg("Attenzione: %1 non sar� pi� la sede di consegna preferita",,, this.oParentObject.w_OLDDESPRE)
        endif
    endcase
  endproc


  proc Init(oParentObject,pOPE)
    this.pOPE=pOPE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='SEDIAZIE'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_SEDIAZIE')
      use in _Curs_SEDIAZIE
    endif
    if used('_Curs_SEDIAZIE')
      use in _Curs_SEDIAZIE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPE"
endproc
