* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bvs                                                        *
*              Aggiorna stampa versamenti                                      *
*                                                                              *
*      Author: ZUCCHETTI TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_6]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-10-30                                                      *
* Last revis.: 2001-03-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bvs",oParentObject)
return(i_retval)

define class tgsve_bvs as StdBatch
  * --- Local variables
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch creato per poter sbiancare i valori nella stampa versamenti al variare
    * --- della scelta di stampa.
    this.oParentObject.w_PREGNO = 0
    this.oParentObject.w_ANNO = space(4)
    this.oParentObject.w_DATINI = cp_CharToDate("  -  -  ")
    this.oParentObject.w_DATREG = cp_CharToDate("  -  -  ")
    this.oParentObject.w_DATFIN = cp_CharToDate("  -  -  ")
    this.oParentObject.w_CODVAL = space(3)
    if g_APPLICATION = "ad hoc ENTERPRISE"
      this.oParentObject.w_CODBUN = space(3)
      this.oParentObject.w_DESBUN = space(40)
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
