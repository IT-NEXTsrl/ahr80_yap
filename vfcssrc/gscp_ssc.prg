* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscp_ssc                                                        *
*              Stampe controllo dati per CPZ                                   *
*                                                                              *
*      Author: Pollina Fabrizio & Bellani Gianluca                             *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-12-15                                                      *
* Last revis.: 2009-06-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscp_ssc",oParentObject))

* --- Class definition
define class tgscp_ssc as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 523
  Height = 422
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-06-23"
  HelpContextID=90805609
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=8

  * --- Constant Properties
  _IDX = 0
  cPrg = "gscp_ssc"
  cComment = "Stampe controllo dati per CPZ"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_FLCHKAGE = space(1)
  w_FLCHKART = space(1)
  w_FLCHKLIS = space(1)
  w_FLCHKORD = space(1)
  w_FLCHKDOC = space(1)
  w_FLCHKPRO = space(1)
  w_FLSELDES = space(1)
  w_VERIFICHE = space(0)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscp_sscPag1","gscp_ssc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFLCHKAGE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_FLCHKAGE=space(1)
      .w_FLCHKART=space(1)
      .w_FLCHKLIS=space(1)
      .w_FLCHKORD=space(1)
      .w_FLCHKDOC=space(1)
      .w_FLCHKPRO=space(1)
      .w_FLSELDES=space(1)
      .w_VERIFICHE=space(0)
        .w_FLCHKAGE = 'N'
        .w_FLCHKART = 'N'
        .w_FLCHKLIS = 'N'
        .w_FLCHKORD = 'N'
        .w_FLCHKDOC = 'N'
        .w_FLCHKPRO = 'N'
        .w_FLSELDES = 'D'
      .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
    endwith
    this.DoRTCalc(8,8,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,8,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_7.visible=!this.oPgFrm.Page1.oPag.oBtn_1_7.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_8.visible=!this.oPgFrm.Page1.oPag.oBtn_1_8.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_9.visible=!this.oPgFrm.Page1.oPag.oBtn_1_9.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_10.visible=!this.oPgFrm.Page1.oPag.oBtn_1_10.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_11.visible=!this.oPgFrm.Page1.oPag.oBtn_1_11.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_12.visible=!this.oPgFrm.Page1.oPag.oBtn_1_12.mHide()
    this.oPgFrm.Page1.oPag.oVERIFICHE_1_15.visible=!this.oPgFrm.Page1.oPag.oVERIFICHE_1_15.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_14.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_17.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFLCHKAGE_1_1.RadioValue()==this.w_FLCHKAGE)
      this.oPgFrm.Page1.oPag.oFLCHKAGE_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLCHKART_1_2.RadioValue()==this.w_FLCHKART)
      this.oPgFrm.Page1.oPag.oFLCHKART_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLCHKLIS_1_3.RadioValue()==this.w_FLCHKLIS)
      this.oPgFrm.Page1.oPag.oFLCHKLIS_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLCHKORD_1_4.RadioValue()==this.w_FLCHKORD)
      this.oPgFrm.Page1.oPag.oFLCHKORD_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLCHKDOC_1_5.RadioValue()==this.w_FLCHKDOC)
      this.oPgFrm.Page1.oPag.oFLCHKDOC_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLCHKPRO_1_6.RadioValue()==this.w_FLCHKPRO)
      this.oPgFrm.Page1.oPag.oFLCHKPRO_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLSELDES_1_13.RadioValue()==this.w_FLSELDES)
      this.oPgFrm.Page1.oPag.oFLSELDES_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVERIFICHE_1_15.value==this.w_VERIFICHE)
      this.oPgFrm.Page1.oPag.oVERIFICHE_1_15.value=this.w_VERIFICHE
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgscp_sscPag1 as StdContainer
  Width  = 519
  height = 422
  stdWidth  = 519
  stdheight = 422
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oFLCHKAGE_1_1 as StdCheck with uid="KNTUNJSEIO",rtseq=1,rtrep=.f.,left=18, top=318, caption="Agenti",;
    HelpContextID = 9628571,;
    cFormVar="w_FLCHKAGE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLCHKAGE_1_1.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oFLCHKAGE_1_1.GetRadio()
    this.Parent.oContained.w_FLCHKAGE = this.RadioValue()
    return .t.
  endfunc

  func oFLCHKAGE_1_1.SetRadio()
    this.Parent.oContained.w_FLCHKAGE=trim(this.Parent.oContained.w_FLCHKAGE)
    this.value = ;
      iif(this.Parent.oContained.w_FLCHKAGE=='S',1,;
      0)
  endfunc

  add object oFLCHKART_1_2 as StdCheck with uid="XARKLUKQNW",rtseq=2,rtrep=.f.,left=129, top=318, caption="Articoli",;
    HelpContextID = 9628586,;
    cFormVar="w_FLCHKART", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLCHKART_1_2.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oFLCHKART_1_2.GetRadio()
    this.Parent.oContained.w_FLCHKART = this.RadioValue()
    return .t.
  endfunc

  func oFLCHKART_1_2.SetRadio()
    this.Parent.oContained.w_FLCHKART=trim(this.Parent.oContained.w_FLCHKART)
    this.value = ;
      iif(this.Parent.oContained.w_FLCHKART=='S',1,;
      0)
  endfunc

  add object oFLCHKLIS_1_3 as StdCheck with uid="VPHYKUDRXW",rtseq=3,rtrep=.f.,left=243, top=318, caption="Listini e contratti",;
    HelpContextID = 74257495,;
    cFormVar="w_FLCHKLIS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLCHKLIS_1_3.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oFLCHKLIS_1_3.GetRadio()
    this.Parent.oContained.w_FLCHKLIS = this.RadioValue()
    return .t.
  endfunc

  func oFLCHKLIS_1_3.SetRadio()
    this.Parent.oContained.w_FLCHKLIS=trim(this.Parent.oContained.w_FLCHKLIS)
    this.value = ;
      iif(this.Parent.oContained.w_FLCHKLIS=='S',1,;
      0)
  endfunc

  add object oFLCHKORD_1_4 as StdCheck with uid="LTRFQETITR",rtseq=4,rtrep=.f.,left=18, top=345, caption="Ordini",;
    HelpContextID = 244509594,;
    cFormVar="w_FLCHKORD", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLCHKORD_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oFLCHKORD_1_4.GetRadio()
    this.Parent.oContained.w_FLCHKORD = this.RadioValue()
    return .t.
  endfunc

  func oFLCHKORD_1_4.SetRadio()
    this.Parent.oContained.w_FLCHKORD=trim(this.Parent.oContained.w_FLCHKORD)
    this.value = ;
      iif(this.Parent.oContained.w_FLCHKORD=='S',1,;
      0)
  endfunc

  add object oFLCHKDOC_1_5 as StdCheck with uid="TNRMYMNQUI",rtseq=5,rtrep=.f.,left=129, top=345, caption="Documenti",;
    HelpContextID = 208475239,;
    cFormVar="w_FLCHKDOC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLCHKDOC_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oFLCHKDOC_1_5.GetRadio()
    this.Parent.oContained.w_FLCHKDOC = this.RadioValue()
    return .t.
  endfunc

  func oFLCHKDOC_1_5.SetRadio()
    this.Parent.oContained.w_FLCHKDOC=trim(this.Parent.oContained.w_FLCHKDOC)
    this.value = ;
      iif(this.Parent.oContained.w_FLCHKDOC=='S',1,;
      0)
  endfunc

  add object oFLCHKPRO_1_6 as StdCheck with uid="AXRUBFIDFP",rtseq=6,rtrep=.f.,left=243, top=345, caption="Provvigioni",;
    HelpContextID = 261286821,;
    cFormVar="w_FLCHKPRO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLCHKPRO_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oFLCHKPRO_1_6.GetRadio()
    this.Parent.oContained.w_FLCHKPRO = this.RadioValue()
    return .t.
  endfunc

  func oFLCHKPRO_1_6.SetRadio()
    this.Parent.oContained.w_FLCHKPRO=trim(this.Parent.oContained.w_FLCHKPRO)
    this.value = ;
      iif(this.Parent.oContained.w_FLCHKPRO=='S',1,;
      0)
  endfunc


  add object oBtn_1_7 as StdButton with uid="GOUXQYHQBS",left=421, top=389, width=79,height=25,;
    caption="Esci", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 83488186;
    , caption='\<Esci';
  , bGlobalFont=.t.

    proc oBtn_1_7.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_7.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_typebutton<>1)
     endwith
    endif
  endfunc


  add object oBtn_1_8 as StdButton with uid="JWIBHYOYEX",left=450, top=384, width=50,height=30,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 83488186;
  , bGlobalFont=.t.

    proc oBtn_1_8.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_8.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_typebutton<>10)
     endwith
    endif
  endfunc


  add object oBtn_1_9 as StdButton with uid="APRCFTBFHJ",left=452, top=369, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 83488186;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_typebutton<>11)
     endwith
    endif
  endfunc


  add object oBtn_1_10 as StdButton with uid="OKBBFJJKZS",left=394, top=384, width=50,height=30,;
    CpPicture="BMP\PRINT.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 90776858;
  , bGlobalFont=.t.

    proc oBtn_1_10.Click()
      with this.Parent.oContained
        GSCP_BSC(this.Parent.oContained,"E")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_10.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ('S' $ .w_FLCHKAGE+.w_FLCHKORD+.w_FLCHKDOC+.w_FLCHKPRO+.w_FLCHKLIS+.w_FLCHKART)
      endwith
    endif
  endfunc

  func oBtn_1_10.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_typebutton<>10)
     endwith
    endif
  endfunc


  add object oBtn_1_11 as StdButton with uid="SKBBJFTYSB",left=318, top=391, width=74,height=23,;
    caption="OK", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 90785050;
    , caption='\<OK';
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      with this.Parent.oContained
        GSCP_BSC(this.Parent.oContained,"E")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_11.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ('S' $ .w_FLCHKAGE+.w_FLCHKORD+.w_FLCHKDOC+.w_FLCHKPRO+.w_FLCHKLIS+.w_FLCHKART)
      endwith
    endif
  endfunc

  func oBtn_1_11.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_typebutton<>1)
     endwith
    endif
  endfunc


  add object oBtn_1_12 as StdButton with uid="WRBWGKENQC",left=396, top=369, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 90776858;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      with this.Parent.oContained
        GSCP_BSC(this.Parent.oContained,"E")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_12.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ('S' $ .w_FLCHKAGE+.w_FLCHKORD+.w_FLCHKDOC+.w_FLCHKPRO+.w_FLCHKLIS+.w_FLCHKART)
      endwith
    endif
  endfunc

  func oBtn_1_12.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_typebutton<>11)
     endwith
    endif
  endfunc

  add object oFLSELDES_1_13 as StdRadio with uid="JQCTYHQWQD",rtseq=7,rtrep=.f.,left=14, top=382, width=136,height=38;
    , cFormVar="w_FLSELDES", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oFLSELDES_1_13.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutti"
      this.Buttons(1).HelpContextID = 60877737
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutti"
      this.Buttons(2).HelpContextID = 60877737
      this.Buttons(2).Top=18
      this.SetAll("Width",134)
      this.SetAll("Height",20)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oFLSELDES_1_13.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oFLSELDES_1_13.GetRadio()
    this.Parent.oContained.w_FLSELDES = this.RadioValue()
    return .t.
  endfunc

  func oFLSELDES_1_13.SetRadio()
    this.Parent.oContained.w_FLSELDES=trim(this.Parent.oContained.w_FLSELDES)
    this.value = ;
      iif(this.Parent.oContained.w_FLSELDES=='S',1,;
      iif(this.Parent.oContained.w_FLSELDES=='D',2,;
      0))
  endfunc


  add object oObj_1_14 as cp_runprogram with uid="ZSODRMFIZD",left=1, top=430, width=268,height=19,;
    caption='GSCP_BSC(S)',;
   bGlobalFont=.t.,;
    prg="GSCP_BSC('S')",;
    cEvent = "w_FLSELDES Changed",;
    nPag=1;
    , HelpContextID = 48093225

  add object oVERIFICHE_1_15 as StdMemo with uid="RBYWXIBDFV",rtseq=8,rtrep=.f.,;
    cFormVar = "w_VERIFICHE", cQueryName = "VERIFICHE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 138729966,;
   bGlobalFont=.t.,;
    Height=284, Width=486, Left=15, Top=21, enabled=.T., readonly=.T., backstyle=0

  func oVERIFICHE_1_15.mHide()
    with this.Parent.oContained
      return (g_APPLICATION = 'ADHOC ENTERPRISE')
    endwith
  endfunc


  add object oObj_1_17 as cp_runprogram with uid="OPQKBCYWFL",left=2, top=455, width=266,height=19,;
    caption='GSCP_BSC(I)',;
   bGlobalFont=.t.,;
    prg="GSCP_BSC('I')",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 48090665

  add object oStr_1_16 as StdString with uid="CUCTNXGTND",Visible=.t., Left=23, Top=5,;
    Alignment=0, Width=175, Height=18,;
    Caption="Verifiche effettuate:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscp_ssc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
