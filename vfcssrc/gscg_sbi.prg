* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_sbi                                                        *
*              Stampa bilancio (libro inventari)                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-03-05                                                      *
* Last revis.: 2013-03-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_sbi",oParentObject))

* --- Class definition
define class tgscg_sbi as StdForm
  Top    = 18
  Left   = 5

  * --- Standard Properties
  Width  = 627
  Height = 226
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-03-26"
  HelpContextID=107619689
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=58

  * --- Constant Properties
  _IDX = 0
  TIR_MAST_IDX = 0
  ESERCIZI_IDX = 0
  VALUTE_IDX = 0
  BUSIUNIT_IDX = 0
  AZIENDA_IDX = 0
  SB_MAST_IDX = 0
  cPrg = "gscg_sbi"
  cComment = "Stampa bilancio (libro inventari)"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_codaz1 = space(5)
  w_INTLIN = space(1)
  w_MASABI = space(1)
  w_AZESSTCO = space(4)
  w_FINSTO = ctod('  /  /  ')
  w_TIPO = space(1)
  w_gestbu = space(1)
  w_ese = space(4)
  o_ese = space(4)
  w_ESSALDI = space(1)
  w_ESSALDI = space(1)
  w_BILANCIO = space(15)
  w_BILDES = space(0)
  w_ESESTA = space(4)
  w_PRPALI = 0
  w_ULTPLI = 0
  w_totdat = space(1)
  o_totdat = space(1)
  w_data1 = ctod('  /  /  ')
  o_data1 = ctod('  /  /  ')
  w_data2 = ctod('  /  /  ')
  w_monete = space(1)
  w_divisa = space(3)
  o_divisa = space(3)
  w_simval = space(5)
  w_CODBUN = space(3)
  w_SUPERBU = space(15)
  w_SBDESCRI = space(35)
  w_TUTTI = space(1)
  w_ARROT = space(1)
  w_PROVVI = space(1)
  w_DETCON = space(1)
  o_DETCON = space(1)
  w_NOTE = space(1)
  o_NOTE = space(1)
  w_CONFRONTO = space(1)
  o_CONFRONTO = space(1)
  w_ORIPREC = space(1)
  w_INFRAAN = space(1)
  w_USO = space(1)
  w_descri = space(30)
  w_decimi = 0
  w_UTENTE = 0
  w_BUDESCRI = space(35)
  w_CAMVAL = 0
  w_EXTRAEUR = 0
  w_ESPREC = space(1)
  o_ESPREC = space(1)
  w_ARROT = space(1)
  w_ARRVOCI = space(1)
  w_DATEMU = ctod('  /  /  ')
  w_DATINIESE = ctod('  /  /  ')
  w_DATFINESE = ctod('  /  /  ')
  w_TIPVOC = space(10)
  w_VALAPP = space(3)
  w_DECNAZ = 0
  w_CAONAZ = 0
  w_esepre = space(4)
  w_PRVALNAZ = space(3)
  w_PRCAOVAL = 0
  w_TIPSTA = space(1)
  w_PRDECTOT = 0
  w_PRINIESE = ctod('  /  /  ')
  w_PREFLI = space(20)
  w_PCONLI = space(1)
  w_DNUMLI = space(50)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_sbiPag1","gscg_sbi",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oese_1_8
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='TIR_MAST'
    this.cWorkTables[2]='ESERCIZI'
    this.cWorkTables[3]='VALUTE'
    this.cWorkTables[4]='BUSIUNIT'
    this.cWorkTables[5]='AZIENDA'
    this.cWorkTables[6]='SB_MAST'
    return(this.OpenAllTables(6))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSCG_BBG(this,0)
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gscg_sbi
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_codaz1=space(5)
      .w_INTLIN=space(1)
      .w_MASABI=space(1)
      .w_AZESSTCO=space(4)
      .w_FINSTO=ctod("  /  /  ")
      .w_TIPO=space(1)
      .w_gestbu=space(1)
      .w_ese=space(4)
      .w_ESSALDI=space(1)
      .w_ESSALDI=space(1)
      .w_BILANCIO=space(15)
      .w_BILDES=space(0)
      .w_ESESTA=space(4)
      .w_PRPALI=0
      .w_ULTPLI=0
      .w_totdat=space(1)
      .w_data1=ctod("  /  /  ")
      .w_data2=ctod("  /  /  ")
      .w_monete=space(1)
      .w_divisa=space(3)
      .w_simval=space(5)
      .w_CODBUN=space(3)
      .w_SUPERBU=space(15)
      .w_SBDESCRI=space(35)
      .w_TUTTI=space(1)
      .w_ARROT=space(1)
      .w_PROVVI=space(1)
      .w_DETCON=space(1)
      .w_NOTE=space(1)
      .w_CONFRONTO=space(1)
      .w_ORIPREC=space(1)
      .w_INFRAAN=space(1)
      .w_USO=space(1)
      .w_descri=space(30)
      .w_decimi=0
      .w_UTENTE=0
      .w_BUDESCRI=space(35)
      .w_CAMVAL=0
      .w_EXTRAEUR=0
      .w_ESPREC=space(1)
      .w_ARROT=space(1)
      .w_ARRVOCI=space(1)
      .w_DATEMU=ctod("  /  /  ")
      .w_DATINIESE=ctod("  /  /  ")
      .w_DATFINESE=ctod("  /  /  ")
      .w_TIPVOC=space(10)
      .w_VALAPP=space(3)
      .w_DECNAZ=0
      .w_CAONAZ=0
      .w_esepre=space(4)
      .w_PRVALNAZ=space(3)
      .w_PRCAOVAL=0
      .w_TIPSTA=space(1)
      .w_PRDECTOT=0
      .w_PRINIESE=ctod("  /  /  ")
      .w_PREFLI=space(20)
      .w_PCONLI=space(1)
      .w_DNUMLI=space(50)
        .w_codaz1 = i_codazi
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_codaz1))
          .link_1_1('Full')
        endif
        .w_INTLIN = IIF(g_APPLICATION = "ADHOC REVOLUTION",LOOKTAB("AZIENDA","AZINTLIN","AZCODAZI",.w_CODAZ1),' ')
        .w_MASABI = ' '
        .w_AZESSTCO = IIF(g_APPLICATION="ADHOC REVOLUTION",'    ',nvl(LOOKTAB("AZIENDA","AZESSTCO","AZCODAZI",.w_CODAZ1),'   '))
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_AZESSTCO))
          .link_1_4('Full')
        endif
          .DoRTCalc(5,5,.f.)
        .w_TIPO = 'M'
          .DoRTCalc(7,7,.f.)
        .w_ese = g_codese
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_ese))
          .link_1_8('Full')
        endif
        .w_ESSALDI = IIF(.w_MASABI='S','M','S')
        .w_ESSALDI = IIF(.w_MASABI='S',' ','S')
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_BILANCIO))
          .link_1_11('Full')
        endif
          .DoRTCalc(12,12,.f.)
        .w_ESESTA = IIF(g_APPLICATION = "ADHOC REVOLUTION" ,Nvl(LOOKTAB("AZIENDA","AZESLBIN","AZCODAZI",.w_CODAZ1),space(4)),SPACE(4))
        .w_PRPALI = IIF(g_APPLICATION = "ADHOC REVOLUTION" AND (.w_ESE=.w_ESESTA OR Empty(.w_ESESTA)),LOOKTAB("AZIENDA","AZPRPALI","AZCODAZI",.w_CODAZ1),0)
        .w_ULTPLI = IIF(g_APPLICATION = "ADHOC REVOLUTION",0,LOOKTAB("NUMEREGI","NRULTPLI","NRCODAZI",.w_CODAZ1))
        .w_totdat = 'T'
          .DoRTCalc(17,18,.f.)
        .w_monete = 'c'
        .DoRTCalc(20,20,.f.)
        if not(empty(.w_divisa))
          .link_1_20('Full')
        endif
        .DoRTCalc(21,22,.f.)
        if not(empty(.w_CODBUN))
          .link_1_22('Full')
        endif
        .w_SUPERBU = ''
        .DoRTCalc(23,23,.f.)
        if not(empty(.w_SUPERBU))
          .link_1_23('Full')
        endif
          .DoRTCalc(24,26,.f.)
        .w_PROVVI = 'N'
        .w_DETCON = 'S'
          .DoRTCalc(29,29,.f.)
        .w_CONFRONTO = ' '
        .w_ORIPREC = IIF(.w_MASABI='S','M','S')
          .DoRTCalc(32,32,.f.)
        .w_USO = 'N'
          .DoRTCalc(34,35,.f.)
        .w_UTENTE = g_CODUTE
          .DoRTCalc(37,37,.f.)
        .w_CAMVAL = GETCAM(.w_DIVISA,i_DATSYS,0)
          .DoRTCalc(39,39,.f.)
        .w_ESPREC = 'S'
        .w_ARROT = 'N'
        .w_ARRVOCI = IIF(.w_DETCON='S','M','V')
          .DoRTCalc(43,45,.f.)
        .w_TIPVOC = 'M'
        .w_VALAPP = g_PERVAL
        .DoRTCalc(47,47,.f.)
        if not(empty(.w_VALAPP))
          .link_1_51('Full')
        endif
          .DoRTCalc(48,49,.f.)
        .w_esepre = CALCESPR(i_CODAZI,.w_DATINIESE,.T.)
        .DoRTCalc(50,50,.f.)
        if not(empty(.w_esepre))
          .link_1_54('Full')
        endif
        .DoRTCalc(51,51,.f.)
        if not(empty(.w_PRVALNAZ))
          .link_1_55('Full')
        endif
          .DoRTCalc(52,52,.f.)
        .w_TIPSTA = 'T'
          .DoRTCalc(54,55,.f.)
        .w_PREFLI = IIF(g_APPLICATION = "ADHOC REVOLUTION",LOOKTAB("AZIENDA","AZPREFLI","AZCODAZI",.w_CODAZ1),' ')
        .w_PCONLI = IIF(g_APPLICATION = "ADHOC REVOLUTION",' ',LOOKTAB("NUMEREGI","NRPCONLI","NRCODAZI",.w_CODAZ1))
        .w_DNUMLI = IIF(g_APPLICATION = "ADHOC REVOLUTION",' ',LOOKTAB("NUMEREGI","NRDNUMLI","NRCODAZI",.w_CODAZ1))
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_34.enabled = this.oPgFrm.Page1.oPag.oBtn_1_34.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_35.enabled = this.oPgFrm.Page1.oPag.oBtn_1_35.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,3,.t.)
          .link_1_4('Full')
        .DoRTCalc(5,12,.t.)
            .w_ESESTA = IIF(g_APPLICATION = "ADHOC REVOLUTION" ,Nvl(LOOKTAB("AZIENDA","AZESLBIN","AZCODAZI",.w_CODAZ1),space(4)),SPACE(4))
        if .o_ESE<>.w_ESE
            .w_PRPALI = IIF(g_APPLICATION = "ADHOC REVOLUTION" AND (.w_ESE=.w_ESESTA OR Empty(.w_ESESTA)),LOOKTAB("AZIENDA","AZPRPALI","AZCODAZI",.w_CODAZ1),0)
        endif
        .DoRTCalc(15,19,.t.)
          .link_1_20('Full')
        .DoRTCalc(21,21,.t.)
          .link_1_22('Full')
        if .o_data1<>.w_data1.or. .o_ESPREC<>.w_ESPREC.or. .o_totdat<>.w_totdat
            .w_SUPERBU = ''
          .link_1_23('Full')
        endif
        .DoRTCalc(24,29,.t.)
        if .o_NOTE<>.w_NOTE
            .w_CONFRONTO = ' '
        endif
        .DoRTCalc(31,37,.t.)
        if .o_DIVISA<>.w_DIVISA.or. .o_ESE<>.w_ESE
            .w_CAMVAL = GETCAM(.w_DIVISA,i_DATSYS,0)
        endif
        .DoRTCalc(39,41,.t.)
        if .o_DETCON<>.w_DETCON
            .w_ARRVOCI = IIF(.w_DETCON='S','M','V')
        endif
        .DoRTCalc(43,46,.t.)
        if .o_ESE<>.w_ESE
          .link_1_51('Full')
        endif
        .DoRTCalc(48,49,.t.)
        if .o_ese<>.w_ese
            .w_esepre = CALCESPR(i_CODAZI,.w_DATINIESE,.T.)
          .link_1_54('Full')
        endif
          .link_1_55('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(52,58,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBILANCIO_1_11.enabled = this.oPgFrm.Page1.oPag.oBILANCIO_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_34.enabled = this.oPgFrm.Page1.oPag.oBtn_1_34.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oESSALDI_1_9.visible=!this.oPgFrm.Page1.oPag.oESSALDI_1_9.mHide()
    this.oPgFrm.Page1.oPag.oESSALDI_1_10.visible=!this.oPgFrm.Page1.oPag.oESSALDI_1_10.mHide()
    this.oPgFrm.Page1.oPag.oBILANCIO_1_11.visible=!this.oPgFrm.Page1.oPag.oBILANCIO_1_11.mHide()
    this.oPgFrm.Page1.oPag.oPRPALI_1_14.visible=!this.oPgFrm.Page1.oPag.oPRPALI_1_14.mHide()
    this.oPgFrm.Page1.oPag.oULTPLI_1_15.visible=!this.oPgFrm.Page1.oPag.oULTPLI_1_15.mHide()
    this.oPgFrm.Page1.oPag.oCONFRONTO_1_30.visible=!this.oPgFrm.Page1.oPag.oCONFRONTO_1_30.mHide()
    this.oPgFrm.Page1.oPag.oORIPREC_1_31.visible=!this.oPgFrm.Page1.oPag.oORIPREC_1_31.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_38.visible=!this.oPgFrm.Page1.oPag.oStr_1_38.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_60.visible=!this.oPgFrm.Page1.oPag.oStr_1_60.mHide()
    this.oPgFrm.Page1.oPag.oPREFLI_1_61.visible=!this.oPgFrm.Page1.oPag.oPREFLI_1_61.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_62.visible=!this.oPgFrm.Page1.oPag.oStr_1_62.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_64.visible=!this.oPgFrm.Page1.oPag.oStr_1_64.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_65.visible=!this.oPgFrm.Page1.oPag.oStr_1_65.mHide()
    this.oPgFrm.Page1.oPag.oDNUMLI_1_66.visible=!this.oPgFrm.Page1.oPag.oDNUMLI_1_66.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_67.visible=!this.oPgFrm.Page1.oPag.oStr_1_67.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_68.visible=!this.oPgFrm.Page1.oPag.oStr_1_68.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=codaz1
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_codaz1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_codaz1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZFLBUNI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_codaz1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_codaz1)
            select AZCODAZI,AZFLBUNI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_codaz1 = NVL(_Link_.AZCODAZI,space(5))
      this.w_gestbu = NVL(_Link_.AZFLBUNI,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_codaz1 = space(5)
      endif
      this.w_gestbu = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_codaz1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AZESSTCO
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZESSTCO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZESSTCO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_AZESSTCO);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZ1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZ1;
                       ,'ESCODESE',this.w_AZESSTCO)
            select ESCODAZI,ESCODESE,ESINIESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZESSTCO = NVL(_Link_.ESCODESE,space(4))
      this.w_FINSTO = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_AZESSTCO = space(4)
      endif
      this.w_FINSTO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZESSTCO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ese
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ese) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_ese)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_codaz1);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_codaz1;
                     ,'ESCODESE',trim(this.w_ese))
          select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ese)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ese) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oese_1_8'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_codaz1<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Esercizio storicizzato")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_codaz1);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ese)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ese);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_codaz1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_codaz1;
                       ,'ESCODESE',this.w_ese)
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ese = NVL(_Link_.ESCODESE,space(4))
      this.w_DATINIESE = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_DATFINESE = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
      this.w_divisa = NVL(_Link_.ESVALNAZ,space(3))
      this.w_VALAPP = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ese = space(4)
      endif
      this.w_DATINIESE = ctod("  /  /  ")
      this.w_DATFINESE = ctod("  /  /  ")
      this.w_divisa = space(3)
      this.w_VALAPP = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATINIESE>.w_FINSTO
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Esercizio storicizzato")
        endif
        this.w_ese = space(4)
        this.w_DATINIESE = ctod("  /  /  ")
        this.w_DATFINESE = ctod("  /  /  ")
        this.w_divisa = space(3)
        this.w_VALAPP = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ese Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=BILANCIO
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIR_MAST_IDX,3]
    i_lTable = "TIR_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIR_MAST_IDX,2], .t., this.TIR_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIR_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_BILANCIO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TIR_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_BILANCIO)+"%");
                   +" and TR__TIPO="+cp_ToStrODBC(this.w_TIPO);

          i_ret=cp_SQL(i_nConn,"select TR__TIPO,TRCODICE,TRDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TR__TIPO,TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TR__TIPO',this.w_TIPO;
                     ,'TRCODICE',trim(this.w_BILANCIO))
          select TR__TIPO,TRCODICE,TRDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TR__TIPO,TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_BILANCIO)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_BILANCIO) and !this.bDontReportError
            deferred_cp_zoom('TIR_MAST','*','TR__TIPO,TRCODICE',cp_AbsName(oSource.parent,'oBILANCIO_1_11'),i_cWhere,'',"Strutture di bilancio",'GSAR0MTR.TIR_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TR__TIPO,TRCODICE,TRDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TR__TIPO,TRCODICE,TRDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TR__TIPO,TRCODICE,TRDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TR__TIPO="+cp_ToStrODBC(this.w_TIPO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TR__TIPO',oSource.xKey(1);
                       ,'TRCODICE',oSource.xKey(2))
            select TR__TIPO,TRCODICE,TRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_BILANCIO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TR__TIPO,TRCODICE,TRDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_BILANCIO);
                   +" and TR__TIPO="+cp_ToStrODBC(this.w_TIPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TR__TIPO',this.w_TIPO;
                       ,'TRCODICE',this.w_BILANCIO)
            select TR__TIPO,TRCODICE,TRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_BILANCIO = NVL(_Link_.TRCODICE,space(15))
      this.w_BILDES = NVL(_Link_.TRDESCRI,space(0))
    else
      if i_cCtrl<>'Load'
        this.w_BILANCIO = space(15)
      endif
      this.w_BILDES = space(0)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIR_MAST_IDX,2])+'\'+cp_ToStr(_Link_.TR__TIPO,1)+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.TIR_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_BILANCIO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=divisa
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_divisa) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_divisa)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VACAOVAL,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_divisa);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_divisa)
            select VACODVAL,VADESVAL,VADECTOT,VACAOVAL,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_divisa = NVL(_Link_.VACODVAL,space(3))
      this.w_descri = NVL(_Link_.VADESVAL,space(30))
      this.w_decimi = NVL(_Link_.VADECTOT,0)
      this.w_EXTRAEUR = NVL(_Link_.VACAOVAL,0)
      this.w_simval = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_divisa = space(3)
      endif
      this.w_descri = space(30)
      this.w_decimi = 0
      this.w_EXTRAEUR = 0
      this.w_simval = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_divisa Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODBUN
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BUSIUNIT_IDX,3]
    i_lTable = "BUSIUNIT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2], .t., this.BUSIUNIT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODBUN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODBUN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(this.w_CODBUN);
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_codaz1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',this.w_codaz1;
                       ,'BUCODICE',this.w_CODBUN)
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODBUN = NVL(_Link_.BUCODICE,space(3))
      this.w_BUDESCRI = NVL(_Link_.BUDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODBUN = space(3)
      endif
      this.w_BUDESCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])+'\'+cp_ToStr(_Link_.BUCODAZI,1)+'\'+cp_ToStr(_Link_.BUCODICE,1)
      cp_ShowWarn(i_cKey,this.BUSIUNIT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODBUN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SUPERBU
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SB_MAST_IDX,3]
    i_lTable = "SB_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SB_MAST_IDX,2], .t., this.SB_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SB_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SUPERBU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SUPERBU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SBCODICE,SBDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where SBCODICE="+cp_ToStrODBC(this.w_SUPERBU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SBCODICE',this.w_SUPERBU)
            select SBCODICE,SBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SUPERBU = NVL(_Link_.SBCODICE,space(15))
      this.w_SBDESCRI = NVL(_Link_.SBDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SUPERBU = space(15)
      endif
      this.w_SBDESCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SB_MAST_IDX,2])+'\'+cp_ToStr(_Link_.SBCODICE,1)
      cp_ShowWarn(i_cKey,this.SB_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SUPERBU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALAPP
  func Link_1_51(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALAPP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALAPP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT,VACAOVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALAPP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALAPP)
            select VACODVAL,VADECTOT,VACAOVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALAPP = NVL(_Link_.VACODVAL,space(3))
      this.w_DECNAZ = NVL(_Link_.VADECTOT,0)
      this.w_CAONAZ = NVL(_Link_.VACAOVAL,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALAPP = space(3)
      endif
      this.w_DECNAZ = 0
      this.w_CAONAZ = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALAPP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=esepre
  func Link_1_54(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_esepre) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_esepre)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ,ESINIESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_esepre);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_codaz1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_codaz1;
                       ,'ESCODESE',this.w_esepre)
            select ESCODAZI,ESCODESE,ESVALNAZ,ESINIESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_esepre = NVL(_Link_.ESCODESE,space(4))
      this.w_PRVALNAZ = NVL(_Link_.ESVALNAZ,space(3))
      this.w_PRINIESE = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_esepre = space(4)
      endif
      this.w_PRVALNAZ = space(3)
      this.w_PRINIESE = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_esepre Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PRVALNAZ
  func Link_1_55(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRVALNAZ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRVALNAZ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VACAOVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_PRVALNAZ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_PRVALNAZ)
            select VACODVAL,VACAOVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRVALNAZ = NVL(_Link_.VACODVAL,space(3))
      this.w_PRCAOVAL = NVL(_Link_.VACAOVAL,0)
      this.w_PRDECTOT = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_PRVALNAZ = space(3)
      endif
      this.w_PRCAOVAL = 0
      this.w_PRDECTOT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRVALNAZ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oese_1_8.value==this.w_ese)
      this.oPgFrm.Page1.oPag.oese_1_8.value=this.w_ese
    endif
    if not(this.oPgFrm.Page1.oPag.oESSALDI_1_9.RadioValue()==this.w_ESSALDI)
      this.oPgFrm.Page1.oPag.oESSALDI_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oESSALDI_1_10.RadioValue()==this.w_ESSALDI)
      this.oPgFrm.Page1.oPag.oESSALDI_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oBILANCIO_1_11.value==this.w_BILANCIO)
      this.oPgFrm.Page1.oPag.oBILANCIO_1_11.value=this.w_BILANCIO
    endif
    if not(this.oPgFrm.Page1.oPag.oBILDES_1_12.value==this.w_BILDES)
      this.oPgFrm.Page1.oPag.oBILDES_1_12.value=this.w_BILDES
    endif
    if not(this.oPgFrm.Page1.oPag.oPRPALI_1_14.value==this.w_PRPALI)
      this.oPgFrm.Page1.oPag.oPRPALI_1_14.value=this.w_PRPALI
    endif
    if not(this.oPgFrm.Page1.oPag.oULTPLI_1_15.value==this.w_ULTPLI)
      this.oPgFrm.Page1.oPag.oULTPLI_1_15.value=this.w_ULTPLI
    endif
    if not(this.oPgFrm.Page1.oPag.oTUTTI_1_25.RadioValue()==this.w_TUTTI)
      this.oPgFrm.Page1.oPag.oTUTTI_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARROT_1_26.RadioValue()==this.w_ARROT)
      this.oPgFrm.Page1.oPag.oARROT_1_26.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDETCON_1_28.RadioValue()==this.w_DETCON)
      this.oPgFrm.Page1.oPag.oDETCON_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNOTE_1_29.RadioValue()==this.w_NOTE)
      this.oPgFrm.Page1.oPag.oNOTE_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCONFRONTO_1_30.RadioValue()==this.w_CONFRONTO)
      this.oPgFrm.Page1.oPag.oCONFRONTO_1_30.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oORIPREC_1_31.RadioValue()==this.w_ORIPREC)
      this.oPgFrm.Page1.oPag.oORIPREC_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPREFLI_1_61.value==this.w_PREFLI)
      this.oPgFrm.Page1.oPag.oPREFLI_1_61.value=this.w_PREFLI
    endif
    if not(this.oPgFrm.Page1.oPag.oDNUMLI_1_66.value==this.w_DNUMLI)
      this.oPgFrm.Page1.oPag.oDNUMLI_1_66.value=this.w_DNUMLI
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_ese)) or not(.w_DATINIESE>.w_FINSTO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oese_1_8.SetFocus()
            i_bnoObbl = !empty(.w_ese)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Esercizio storicizzato")
          case   (empty(.w_BILANCIO))  and not(g_APPLICATION="ADHOC REVOLUTION")  and (g_APPLICATION<>"ADHOC REVOLUTION")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oBILANCIO_1_11.SetFocus()
            i_bnoObbl = !empty(.w_BILANCIO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_esepre))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oesepre_1_54.SetFocus()
            i_bnoObbl = !empty(.w_esepre)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ese = this.w_ese
    this.o_totdat = this.w_totdat
    this.o_data1 = this.w_data1
    this.o_divisa = this.w_divisa
    this.o_DETCON = this.w_DETCON
    this.o_NOTE = this.w_NOTE
    this.o_CONFRONTO = this.w_CONFRONTO
    this.o_ESPREC = this.w_ESPREC
    return

enddefine

* --- Define pages as container
define class tgscg_sbiPag1 as StdContainer
  Width  = 623
  height = 226
  stdWidth  = 623
  stdheight = 226
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oese_1_8 as StdField with uid="GTTIRTWBYE",rtseq=8,rtrep=.f.,;
    cFormVar = "w_ese", cQueryName = "ese",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Esercizio storicizzato",;
    ToolTipText = "Esercizio di competenza dei movimenti da stampare",;
    HelpContextID = 107174842,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=99, Top=8, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_codaz1", oKey_2_1="ESCODESE", oKey_2_2="this.w_ese"

  func oese_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oese_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oese_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_codaz1)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_codaz1)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oese_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc


  add object oESSALDI_1_9 as StdCombo with uid="REQHJLJGOZ",rtseq=9,rtrep=.f.,left=298,top=8,width=163,height=21;
    , ToolTipText = "I dati estratti per esercizio attuale verranno presi dall'origine indicata";
    , HelpContextID = 224632250;
    , cFormVar="w_ESSALDI",RowSource=""+"Saldi esercizio,"+"Movimenti di primanota,"+"Saldi bilancio UE", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oESSALDI_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'M',;
    iif(this.value =3,'B',;
    space(1)))))
  endfunc
  func oESSALDI_1_9.GetRadio()
    this.Parent.oContained.w_ESSALDI = this.RadioValue()
    return .t.
  endfunc

  func oESSALDI_1_9.SetRadio()
    this.Parent.oContained.w_ESSALDI=trim(this.Parent.oContained.w_ESSALDI)
    this.value = ;
      iif(this.Parent.oContained.w_ESSALDI=='S',1,;
      iif(this.Parent.oContained.w_ESSALDI=='M',2,;
      iif(this.Parent.oContained.w_ESSALDI=='B',3,;
      0)))
  endfunc

  func oESSALDI_1_9.mHide()
    with this.Parent.oContained
      return (g_APPLICATION<>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oESSALDI_1_10 as StdCheck with uid="HYXMAEJYHM",rtseq=10,rtrep=.f.,left=178, top=8, caption="Lettura saldi eser. attuale",;
    ToolTipText = "I dati estratti verranno presi dall'archivio saldi",;
    HelpContextID = 224632250,;
    cFormVar="w_ESSALDI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oESSALDI_1_10.RadioValue()
    return(iif(this.value =1,'S',;
    ''))
  endfunc
  func oESSALDI_1_10.GetRadio()
    this.Parent.oContained.w_ESSALDI = this.RadioValue()
    return .t.
  endfunc

  func oESSALDI_1_10.SetRadio()
    this.Parent.oContained.w_ESSALDI=trim(this.Parent.oContained.w_ESSALDI)
    this.value = ;
      iif(this.Parent.oContained.w_ESSALDI=='S',1,;
      0)
  endfunc

  func oESSALDI_1_10.mHide()
    with this.Parent.oContained
      return (g_APPLICATION="ADHOC REVOLUTION" or .w_MASABI='S')
    endwith
  endfunc

  add object oBILANCIO_1_11 as StdField with uid="QGMOPBOPQN",rtseq=11,rtrep=.f.,;
    cFormVar = "w_BILANCIO", cQueryName = "BILANCIO",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Struttura di bilancio selezionata",;
    HelpContextID = 239343515,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=482, Top=8, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="TIR_MAST", oKey_1_1="TR__TIPO", oKey_1_2="this.w_TIPO", oKey_2_1="TRCODICE", oKey_2_2="this.w_BILANCIO"

  func oBILANCIO_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_APPLICATION<>"ADHOC REVOLUTION")
    endwith
   endif
  endfunc

  func oBILANCIO_1_11.mHide()
    with this.Parent.oContained
      return (g_APPLICATION="ADHOC REVOLUTION")
    endwith
  endfunc

  func oBILANCIO_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oBILANCIO_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oBILANCIO_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.TIR_MAST_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TR__TIPO="+cp_ToStrODBC(this.Parent.oContained.w_TIPO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TR__TIPO="+cp_ToStr(this.Parent.oContained.w_TIPO)
    endif
    do cp_zoom with 'TIR_MAST','*','TR__TIPO,TRCODICE',cp_AbsName(this.parent,'oBILANCIO_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Strutture di bilancio",'GSAR0MTR.TIR_MAST_VZM',this.parent.oContained
  endproc

  add object oBILDES_1_12 as StdMemo with uid="AINDDTDAYH",rtseq=12,rtrep=.f.,;
    cFormVar = "w_BILDES", cQueryName = "BILDES",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione del bilancio",;
    HelpContextID = 19851286,;
   bGlobalFont=.t.,;
    Height=62, Width=485, Left=99, Top=38

  add object oPRPALI_1_14 as StdField with uid="GZVSBCMEQD",rtseq=14,rtrep=.f.,;
    cFormVar = "w_PRPALI", cQueryName = "PRPALI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ultima pagina stampata nel precedente libro inventari",;
    HelpContextID = 127676918,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=99, Top=113, cSayPict='"9999999"', cGetPict='"9999999"'

  func oPRPALI_1_14.mHide()
    with this.Parent.oContained
      return (g_APPLICATION = "ad hoc ENTERPRISE" or .w_INTLIN<>'S')
    endwith
  endfunc

  add object oULTPLI_1_15 as StdField with uid="JAERQLJPDM",rtseq=15,rtrep=.f.,;
    cFormVar = "w_ULTPLI", cQueryName = "ULTPLI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ultima pagina stampata nel libro inventari",;
    HelpContextID = 128674886,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=99, Top=113, cSayPict='"999999"', cGetPict='"999999"'

  func oULTPLI_1_15.mHide()
    with this.Parent.oContained
      return (g_APPLICATION = "ADHOC REVOLUTION" or .w_PCONLI<>'S')
    endwith
  endfunc

  add object oTUTTI_1_25 as StdCheck with uid="IQLQCFYATX",rtseq=25,rtrep=.f.,left=99, top=152, caption="Saldi significativi",;
    ToolTipText = "Stampa solo i conti con saldo diverso da 0",;
    HelpContextID = 25201354,;
    cFormVar="w_TUTTI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTUTTI_1_25.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oTUTTI_1_25.GetRadio()
    this.Parent.oContained.w_TUTTI = this.RadioValue()
    return .t.
  endfunc

  func oTUTTI_1_25.SetRadio()
    this.Parent.oContained.w_TUTTI=trim(this.Parent.oContained.w_TUTTI)
    this.value = ;
      iif(this.Parent.oContained.w_TUTTI=='S',1,;
      0)
  endfunc

  add object oARROT_1_26 as StdCheck with uid="LXFYKUCFLT",rtseq=26,rtrep=.f.,left=99, top=173, caption="Arrotonda importi",;
    ToolTipText = "Arrotonda importi",;
    HelpContextID = 14003962,;
    cFormVar="w_ARROT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oARROT_1_26.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oARROT_1_26.GetRadio()
    this.Parent.oContained.w_ARROT = this.RadioValue()
    return .t.
  endfunc

  func oARROT_1_26.SetRadio()
    this.Parent.oContained.w_ARROT=trim(this.Parent.oContained.w_ARROT)
    this.value = ;
      iif(this.Parent.oContained.w_ARROT=='S',1,;
      0)
  endfunc

  add object oDETCON_1_28 as StdCheck with uid="DSZZQYLGFY",rtseq=28,rtrep=.f.,left=242, top=152, caption="Dettaglio voci",;
    ToolTipText = "Stampa l'elenco dei componenti della voce",;
    HelpContextID = 214852662,;
    cFormVar="w_DETCON", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDETCON_1_28.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oDETCON_1_28.GetRadio()
    this.Parent.oContained.w_DETCON = this.RadioValue()
    return .t.
  endfunc

  func oDETCON_1_28.SetRadio()
    this.Parent.oContained.w_DETCON=trim(this.Parent.oContained.w_DETCON)
    this.value = ;
      iif(this.Parent.oContained.w_DETCON=='S',1,;
      0)
  endfunc

  add object oNOTE_1_29 as StdCheck with uid="NSLUWPVOCV",rtseq=29,rtrep=.f.,left=242, top=173, caption="Note aggiuntive",;
    ToolTipText = "Stampa le note associate alle voci",;
    HelpContextID = 102732074,;
    cFormVar="w_NOTE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oNOTE_1_29.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oNOTE_1_29.GetRadio()
    this.Parent.oContained.w_NOTE = this.RadioValue()
    return .t.
  endfunc

  func oNOTE_1_29.SetRadio()
    this.Parent.oContained.w_NOTE=trim(this.Parent.oContained.w_NOTE)
    this.value = ;
      iif(this.Parent.oContained.w_NOTE=='S',1,;
      0)
  endfunc

  add object oCONFRONTO_1_30 as StdCheck with uid="JEGLMRCLLH",rtseq=30,rtrep=.f.,left=393, top=152, caption="Confronto es. precedente",;
    ToolTipText = "Confronto con es. precedente",;
    HelpContextID = 234951530,;
    cFormVar="w_CONFRONTO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCONFRONTO_1_30.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCONFRONTO_1_30.GetRadio()
    this.Parent.oContained.w_CONFRONTO = this.RadioValue()
    return .t.
  endfunc

  func oCONFRONTO_1_30.SetRadio()
    this.Parent.oContained.w_CONFRONTO=trim(this.Parent.oContained.w_CONFRONTO)
    this.value = ;
      iif(this.Parent.oContained.w_CONFRONTO=='S',1,;
      0)
  endfunc

  func oCONFRONTO_1_30.mHide()
    with this.Parent.oContained
      return (!(empty(.w_NOTE)))
    endwith
  endfunc


  add object oORIPREC_1_31 as StdCombo with uid="AEGWSZGINK",rtseq=31,rtrep=.f.,left=282,top=198,width=161,height=21;
    , ToolTipText = "I dati estratti per esercizio precedente verranno presi dall'origine indicata";
    , HelpContextID = 67813862;
    , cFormVar="w_ORIPREC",RowSource=""+"Saldi esercizio,"+"Movimenti di primanota,"+"Saldi bilancio UE", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oORIPREC_1_31.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'M',;
    iif(this.value =3,'B',;
    space(1)))))
  endfunc
  func oORIPREC_1_31.GetRadio()
    this.Parent.oContained.w_ORIPREC = this.RadioValue()
    return .t.
  endfunc

  func oORIPREC_1_31.SetRadio()
    this.Parent.oContained.w_ORIPREC=trim(this.Parent.oContained.w_ORIPREC)
    this.value = ;
      iif(this.Parent.oContained.w_ORIPREC=='S',1,;
      iif(this.Parent.oContained.w_ORIPREC=='M',2,;
      iif(this.Parent.oContained.w_ORIPREC=='B',3,;
      0)))
  endfunc

  func oORIPREC_1_31.mHide()
    with this.Parent.oContained
      return (.w_MASABI='S'  OR .w_CONFRONTO<>'S' OR g_APPLICATION<>"ADHOC REVOLUTION")
    endwith
  endfunc


  add object oBtn_1_34 as StdButton with uid="PXGLLSNYDY",left=514, top=175, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la stampa";
    , HelpContextID = 251761686;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_34.Click()
      with this.Parent.oContained
        GSCG_BBG(this.Parent.oContained,0)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_34.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_BILANCIO) or g_APPLICATION="ADHOC REVOLUTION")
      endwith
    endif
  endfunc


  add object oBtn_1_35 as StdButton with uid="SHTWUOTWIU",left=566, top=175, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 251761686;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_35.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oPREFLI_1_61 as StdField with uid="SJBUMXHJGU",rtseq=56,rtrep=.f.,;
    cFormVar = "w_PREFLI", cQueryName = "PREFLI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Prefisso che comparirÓ nella stampa prima del numero di pagina",;
    HelpContextID = 127959542,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=394, Top=113, InputMask=replicate('X',20)

  func oPREFLI_1_61.mHide()
    with this.Parent.oContained
      return (g_APPLICATION = "ad hoc ENTERPRISE" or .w_INTLIN<>'S')
    endwith
  endfunc

  add object oDNUMLI_1_66 as StdField with uid="DRBJDGPDCO",rtseq=58,rtrep=.f.,;
    cFormVar = "w_DNUMLI", cQueryName = "DNUMLI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Dicitura numerazione libro inventari",;
    HelpContextID = 128482614,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=328, Top=113, InputMask=replicate('X',50)

  func oDNUMLI_1_66.mHide()
    with this.Parent.oContained
      return (g_APPLICATION = "ADHOC REVOLUTION" or .w_PCONLI<>'S')
    endwith
  endfunc

  add object oStr_1_36 as StdString with uid="OSAQGQIPMD",Visible=.t., Left=30, Top=8,;
    Alignment=1, Width=68, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="CAJQESLBEZ",Visible=.t., Left=407, Top=8,;
    Alignment=1, Width=71, Height=15,;
    Caption="Bilancio:"  ;
  , bGlobalFont=.t.

  func oStr_1_38.mHide()
    with this.Parent.oContained
      return (g_APPLICATION="ADHOC REVOLUTION")
    endwith
  endfunc

  add object oStr_1_60 as StdString with uid="FWNKOATCCI",Visible=.t., Left=13, Top=115,;
    Alignment=1, Width=84, Height=17,;
    Caption="Ultima pagina:"  ;
  , bGlobalFont=.t.

  func oStr_1_60.mHide()
    with this.Parent.oContained
      return (g_APPLICATION = "ad hoc ENTERPRISE" or .w_INTLIN<>'S')
    endwith
  endfunc

  add object oStr_1_62 as StdString with uid="QBIQLOVEJE",Visible=.t., Left=284, Top=115,;
    Alignment=1, Width=110, Height=17,;
    Caption="Prefisso num. pag.:"  ;
  , bGlobalFont=.t.

  func oStr_1_62.mHide()
    with this.Parent.oContained
      return (g_APPLICATION = "ad hoc ENTERPRISE" or .w_INTLIN<>'S')
    endwith
  endfunc

  add object oStr_1_64 as StdString with uid="LGZTUJTGGL",Visible=.t., Left=10, Top=115,;
    Alignment=1, Width=84, Height=18,;
    Caption="Ultima pagina:"  ;
  , bGlobalFont=.t.

  func oStr_1_64.mHide()
    with this.Parent.oContained
      return (g_APPLICATION = "ADHOC REVOLUTION" or .w_PCONLI<>'S')
    endwith
  endfunc

  add object oStr_1_65 as StdString with uid="MFUTAYZFJN",Visible=.t., Left=190, Top=115,;
    Alignment=1, Width=135, Height=18,;
    Caption="Dicitura numerazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_65.mHide()
    with this.Parent.oContained
      return (g_APPLICATION = "ADHOC REVOLUTION" or .w_PCONLI<>'S')
    endwith
  endfunc

  add object oStr_1_67 as StdString with uid="NNDEPXVVFG",Visible=.t., Left=159, Top=8,;
    Alignment=1, Width=135, Height=18,;
    Caption="Origine eser. attuale:"  ;
  , bGlobalFont=.t.

  func oStr_1_67.mHide()
    with this.Parent.oContained
      return (g_APPLICATION<>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oStr_1_68 as StdString with uid="PZPMZOOYSB",Visible=.t., Left=95, Top=197,;
    Alignment=1, Width=183, Height=18,;
    Caption="Origine eser. precedente:"  ;
  , bGlobalFont=.t.

  func oStr_1_68.mHide()
    with this.Parent.oContained
      return (.w_CONFRONTO<>'S' OR g_APPLICATION<>"ADHOC REVOLUTION")
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_sbi','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
