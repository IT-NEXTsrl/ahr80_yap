* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_aif                                                        *
*              Modello F24 ICI altri tributi locali                            *
*                                                                              *
*      Author: ZUCCHETTI S.P.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-28                                                      *
* Last revis.: 2011-05-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgscg_aif")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgscg_aif")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgscg_aif")
  return

* --- Class definition
define class tgscg_aif as StdPCForm
  Width  = 784
  Height = 200
  Top    = 10
  Left   = 10
  cComment = "Modello F24 ICI altri tributi locali"
  cPrg = "gscg_aif"
  HelpContextID=9053545
  add object cnt as tcgscg_aif
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgscg_aif as PCContext
  w_IFCODEL1 = space(4)
  w_IFTRIEL1 = space(5)
  w_IFRATEL1 = space(4)
  w_IFANNEL1 = space(4)
  w_IFIMDEL1 = 0
  w_IFIMCEL1 = 0
  w_IFRAVVE1 = 0
  w_IFIMMOV1 = 0
  w_IFACCON1 = 0
  w_IFTSALD1 = 0
  w_IFNUMFA1 = 0
  w_IFCODEL2 = space(4)
  w_IFTRIEL2 = space(5)
  w_IFRATEL2 = space(4)
  w_IFANNEL2 = space(4)
  w_IFIMDEL2 = 0
  w_IFIMCEL2 = 0
  w_IFRAVVE2 = 0
  w_IFIMMOV2 = 0
  w_IFACCON2 = 0
  w_IFTSALD2 = 0
  w_IFNUMFA2 = 0
  w_IFCODEL3 = space(4)
  w_IFTRIEL3 = space(5)
  w_IFRATEL3 = space(4)
  w_IFANNEL3 = space(4)
  w_IFIMDEL3 = 0
  w_IFIMCEL3 = 0
  w_IFRAVVE3 = 0
  w_IFIMMOV3 = 0
  w_IFACCON3 = 0
  w_IFTSALD3 = 0
  w_IFNUMFA3 = 0
  w_IFCODEL4 = space(4)
  w_IFTRIEL4 = space(5)
  w_IFRATEL4 = space(4)
  w_IFANNEL4 = space(4)
  w_IFIMDEL4 = 0
  w_IFIMCEL4 = 0
  w_IFRAVVE4 = 0
  w_IFIMMOV4 = 0
  w_IFACCON4 = 0
  w_IFTSALD4 = 0
  w_IFNUMFA4 = 0
  w_IFDETICI = 0
  w_IFTOTDEL = 0
  w_IFTOTCEL = 0
  w_IFSALDEL = 0
  w_IFSERIAL = space(10)
  w_TIPTRI = space(10)
  proc Save(oFrom)
    this.w_IFCODEL1 = oFrom.w_IFCODEL1
    this.w_IFTRIEL1 = oFrom.w_IFTRIEL1
    this.w_IFRATEL1 = oFrom.w_IFRATEL1
    this.w_IFANNEL1 = oFrom.w_IFANNEL1
    this.w_IFIMDEL1 = oFrom.w_IFIMDEL1
    this.w_IFIMCEL1 = oFrom.w_IFIMCEL1
    this.w_IFRAVVE1 = oFrom.w_IFRAVVE1
    this.w_IFIMMOV1 = oFrom.w_IFIMMOV1
    this.w_IFACCON1 = oFrom.w_IFACCON1
    this.w_IFTSALD1 = oFrom.w_IFTSALD1
    this.w_IFNUMFA1 = oFrom.w_IFNUMFA1
    this.w_IFCODEL2 = oFrom.w_IFCODEL2
    this.w_IFTRIEL2 = oFrom.w_IFTRIEL2
    this.w_IFRATEL2 = oFrom.w_IFRATEL2
    this.w_IFANNEL2 = oFrom.w_IFANNEL2
    this.w_IFIMDEL2 = oFrom.w_IFIMDEL2
    this.w_IFIMCEL2 = oFrom.w_IFIMCEL2
    this.w_IFRAVVE2 = oFrom.w_IFRAVVE2
    this.w_IFIMMOV2 = oFrom.w_IFIMMOV2
    this.w_IFACCON2 = oFrom.w_IFACCON2
    this.w_IFTSALD2 = oFrom.w_IFTSALD2
    this.w_IFNUMFA2 = oFrom.w_IFNUMFA2
    this.w_IFCODEL3 = oFrom.w_IFCODEL3
    this.w_IFTRIEL3 = oFrom.w_IFTRIEL3
    this.w_IFRATEL3 = oFrom.w_IFRATEL3
    this.w_IFANNEL3 = oFrom.w_IFANNEL3
    this.w_IFIMDEL3 = oFrom.w_IFIMDEL3
    this.w_IFIMCEL3 = oFrom.w_IFIMCEL3
    this.w_IFRAVVE3 = oFrom.w_IFRAVVE3
    this.w_IFIMMOV3 = oFrom.w_IFIMMOV3
    this.w_IFACCON3 = oFrom.w_IFACCON3
    this.w_IFTSALD3 = oFrom.w_IFTSALD3
    this.w_IFNUMFA3 = oFrom.w_IFNUMFA3
    this.w_IFCODEL4 = oFrom.w_IFCODEL4
    this.w_IFTRIEL4 = oFrom.w_IFTRIEL4
    this.w_IFRATEL4 = oFrom.w_IFRATEL4
    this.w_IFANNEL4 = oFrom.w_IFANNEL4
    this.w_IFIMDEL4 = oFrom.w_IFIMDEL4
    this.w_IFIMCEL4 = oFrom.w_IFIMCEL4
    this.w_IFRAVVE4 = oFrom.w_IFRAVVE4
    this.w_IFIMMOV4 = oFrom.w_IFIMMOV4
    this.w_IFACCON4 = oFrom.w_IFACCON4
    this.w_IFTSALD4 = oFrom.w_IFTSALD4
    this.w_IFNUMFA4 = oFrom.w_IFNUMFA4
    this.w_IFDETICI = oFrom.w_IFDETICI
    this.w_IFTOTDEL = oFrom.w_IFTOTDEL
    this.w_IFTOTCEL = oFrom.w_IFTOTCEL
    this.w_IFSALDEL = oFrom.w_IFSALDEL
    this.w_IFSERIAL = oFrom.w_IFSERIAL
    this.w_TIPTRI = oFrom.w_TIPTRI
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_IFCODEL1 = this.w_IFCODEL1
    oTo.w_IFTRIEL1 = this.w_IFTRIEL1
    oTo.w_IFRATEL1 = this.w_IFRATEL1
    oTo.w_IFANNEL1 = this.w_IFANNEL1
    oTo.w_IFIMDEL1 = this.w_IFIMDEL1
    oTo.w_IFIMCEL1 = this.w_IFIMCEL1
    oTo.w_IFRAVVE1 = this.w_IFRAVVE1
    oTo.w_IFIMMOV1 = this.w_IFIMMOV1
    oTo.w_IFACCON1 = this.w_IFACCON1
    oTo.w_IFTSALD1 = this.w_IFTSALD1
    oTo.w_IFNUMFA1 = this.w_IFNUMFA1
    oTo.w_IFCODEL2 = this.w_IFCODEL2
    oTo.w_IFTRIEL2 = this.w_IFTRIEL2
    oTo.w_IFRATEL2 = this.w_IFRATEL2
    oTo.w_IFANNEL2 = this.w_IFANNEL2
    oTo.w_IFIMDEL2 = this.w_IFIMDEL2
    oTo.w_IFIMCEL2 = this.w_IFIMCEL2
    oTo.w_IFRAVVE2 = this.w_IFRAVVE2
    oTo.w_IFIMMOV2 = this.w_IFIMMOV2
    oTo.w_IFACCON2 = this.w_IFACCON2
    oTo.w_IFTSALD2 = this.w_IFTSALD2
    oTo.w_IFNUMFA2 = this.w_IFNUMFA2
    oTo.w_IFCODEL3 = this.w_IFCODEL3
    oTo.w_IFTRIEL3 = this.w_IFTRIEL3
    oTo.w_IFRATEL3 = this.w_IFRATEL3
    oTo.w_IFANNEL3 = this.w_IFANNEL3
    oTo.w_IFIMDEL3 = this.w_IFIMDEL3
    oTo.w_IFIMCEL3 = this.w_IFIMCEL3
    oTo.w_IFRAVVE3 = this.w_IFRAVVE3
    oTo.w_IFIMMOV3 = this.w_IFIMMOV3
    oTo.w_IFACCON3 = this.w_IFACCON3
    oTo.w_IFTSALD3 = this.w_IFTSALD3
    oTo.w_IFNUMFA3 = this.w_IFNUMFA3
    oTo.w_IFCODEL4 = this.w_IFCODEL4
    oTo.w_IFTRIEL4 = this.w_IFTRIEL4
    oTo.w_IFRATEL4 = this.w_IFRATEL4
    oTo.w_IFANNEL4 = this.w_IFANNEL4
    oTo.w_IFIMDEL4 = this.w_IFIMDEL4
    oTo.w_IFIMCEL4 = this.w_IFIMCEL4
    oTo.w_IFRAVVE4 = this.w_IFRAVVE4
    oTo.w_IFIMMOV4 = this.w_IFIMMOV4
    oTo.w_IFACCON4 = this.w_IFACCON4
    oTo.w_IFTSALD4 = this.w_IFTSALD4
    oTo.w_IFNUMFA4 = this.w_IFNUMFA4
    oTo.w_IFDETICI = this.w_IFDETICI
    oTo.w_IFTOTDEL = this.w_IFTOTDEL
    oTo.w_IFTOTCEL = this.w_IFTOTCEL
    oTo.w_IFSALDEL = this.w_IFSALDEL
    oTo.w_IFSERIAL = this.w_IFSERIAL
    oTo.w_TIPTRI = this.w_TIPTRI
    PCContext::Load(oTo)
enddefine

define class tcgscg_aif as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 784
  Height = 200
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-05-09"
  HelpContextID=9053545
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=50

  * --- Constant Properties
  MODIPAG_IDX = 0
  ENTI_LOC_IDX = 0
  COD_TRIB_IDX = 0
  ENTI_COM_IDX = 0
  cFile = "MODIPAG"
  cKeySelect = "IFSERIAL"
  cKeyWhere  = "IFSERIAL=this.w_IFSERIAL"
  cKeyWhereODBC = '"IFSERIAL="+cp_ToStrODBC(this.w_IFSERIAL)';

  cKeyWhereODBCqualified = '"MODIPAG.IFSERIAL="+cp_ToStrODBC(this.w_IFSERIAL)';

  cPrg = "gscg_aif"
  cComment = "Modello F24 ICI altri tributi locali"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_IFCODEL1 = space(4)
  o_IFCODEL1 = space(4)
  w_IFTRIEL1 = space(5)
  o_IFTRIEL1 = space(5)
  w_IFRATEL1 = space(4)
  w_IFANNEL1 = space(4)
  w_IFIMDEL1 = 0
  w_IFIMCEL1 = 0
  w_IFRAVVE1 = 0
  w_IFIMMOV1 = 0
  w_IFACCON1 = 0
  w_IFTSALD1 = 0
  w_IFNUMFA1 = 0
  w_IFCODEL2 = space(4)
  o_IFCODEL2 = space(4)
  w_IFTRIEL2 = space(5)
  o_IFTRIEL2 = space(5)
  w_IFRATEL2 = space(4)
  w_IFANNEL2 = space(4)
  w_IFIMDEL2 = 0
  w_IFIMCEL2 = 0
  w_IFRAVVE2 = 0
  w_IFIMMOV2 = 0
  w_IFACCON2 = 0
  w_IFTSALD2 = 0
  w_IFNUMFA2 = 0
  w_IFCODEL3 = space(4)
  o_IFCODEL3 = space(4)
  w_IFTRIEL3 = space(5)
  o_IFTRIEL3 = space(5)
  w_IFRATEL3 = space(4)
  w_IFANNEL3 = space(4)
  w_IFIMDEL3 = 0
  w_IFIMCEL3 = 0
  w_IFRAVVE3 = 0
  w_IFIMMOV3 = 0
  w_IFACCON3 = 0
  w_IFTSALD3 = 0
  w_IFNUMFA3 = 0
  w_IFCODEL4 = space(4)
  o_IFCODEL4 = space(4)
  w_IFTRIEL4 = space(5)
  o_IFTRIEL4 = space(5)
  w_IFRATEL4 = space(4)
  w_IFANNEL4 = space(4)
  w_IFIMDEL4 = 0
  w_IFIMCEL4 = 0
  w_IFRAVVE4 = 0
  w_IFIMMOV4 = 0
  w_IFACCON4 = 0
  w_IFTSALD4 = 0
  w_IFNUMFA4 = 0
  w_IFDETICI = 0
  w_IFTOTDEL = 0
  w_IFTOTCEL = 0
  w_IFSALDEL = 0
  w_IFSERIAL = space(10)
  w_TIPTRI = space(10)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_aifPag1","gscg_aif",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).HelpContextID = 219041546
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oIFCODEL1_1_6
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='ENTI_LOC'
    this.cWorkTables[2]='COD_TRIB'
    this.cWorkTables[3]='ENTI_COM'
    this.cWorkTables[4]='MODIPAG'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MODIPAG_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MODIPAG_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgscg_aif'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_7_joined
    link_1_7_joined=.f.
    local link_1_18_joined
    link_1_18_joined=.f.
    local link_1_29_joined
    link_1_29_joined=.f.
    local link_1_40_joined
    link_1_40_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from MODIPAG where IFSERIAL=KeySet.IFSERIAL
    *
    i_nConn = i_TableProp[this.MODIPAG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MODIPAG_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MODIPAG')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MODIPAG.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' MODIPAG '
      link_1_7_joined=this.AddJoinedLink_1_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_18_joined=this.AddJoinedLink_1_18(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_29_joined=this.AddJoinedLink_1_29(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_40_joined=this.AddJoinedLink_1_40(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'IFSERIAL',this.w_IFSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TIPTRI = space(10)
        .w_IFCODEL1 = NVL(IFCODEL1,space(4))
          * evitabile
          *.link_1_6('Load')
        .w_IFTRIEL1 = NVL(IFTRIEL1,space(5))
          if link_1_7_joined
            this.w_IFTRIEL1 = NVL(TRCODICE107,NVL(this.w_IFTRIEL1,space(5)))
            this.w_TIPTRI = NVL(TRTIPTRI107,space(10))
          else
          .link_1_7('Load')
          endif
        .w_IFRATEL1 = NVL(IFRATEL1,space(4))
        .w_IFANNEL1 = NVL(IFANNEL1,space(4))
        .w_IFIMDEL1 = NVL(IFIMDEL1,0)
        .w_IFIMCEL1 = NVL(IFIMCEL1,0)
        .w_IFRAVVE1 = NVL(IFRAVVE1,0)
        .w_IFIMMOV1 = NVL(IFIMMOV1,0)
        .w_IFACCON1 = NVL(IFACCON1,0)
        .w_IFTSALD1 = NVL(IFTSALD1,0)
        .w_IFNUMFA1 = NVL(IFNUMFA1,0)
        .w_IFCODEL2 = NVL(IFCODEL2,space(4))
          * evitabile
          *.link_1_17('Load')
        .w_IFTRIEL2 = NVL(IFTRIEL2,space(5))
          if link_1_18_joined
            this.w_IFTRIEL2 = NVL(TRCODICE118,NVL(this.w_IFTRIEL2,space(5)))
            this.w_TIPTRI = NVL(TRTIPTRI118,space(10))
          else
          .link_1_18('Load')
          endif
        .w_IFRATEL2 = NVL(IFRATEL2,space(4))
        .w_IFANNEL2 = NVL(IFANNEL2,space(4))
        .w_IFIMDEL2 = NVL(IFIMDEL2,0)
        .w_IFIMCEL2 = NVL(IFIMCEL2,0)
        .w_IFRAVVE2 = NVL(IFRAVVE2,0)
        .w_IFIMMOV2 = NVL(IFIMMOV2,0)
        .w_IFACCON2 = NVL(IFACCON2,0)
        .w_IFTSALD2 = NVL(IFTSALD2,0)
        .w_IFNUMFA2 = NVL(IFNUMFA2,0)
        .w_IFCODEL3 = NVL(IFCODEL3,space(4))
          * evitabile
          *.link_1_28('Load')
        .w_IFTRIEL3 = NVL(IFTRIEL3,space(5))
          if link_1_29_joined
            this.w_IFTRIEL3 = NVL(TRCODICE129,NVL(this.w_IFTRIEL3,space(5)))
            this.w_TIPTRI = NVL(TRTIPTRI129,space(10))
          else
          .link_1_29('Load')
          endif
        .w_IFRATEL3 = NVL(IFRATEL3,space(4))
        .w_IFANNEL3 = NVL(IFANNEL3,space(4))
        .w_IFIMDEL3 = NVL(IFIMDEL3,0)
        .w_IFIMCEL3 = NVL(IFIMCEL3,0)
        .w_IFRAVVE3 = NVL(IFRAVVE3,0)
        .w_IFIMMOV3 = NVL(IFIMMOV3,0)
        .w_IFACCON3 = NVL(IFACCON3,0)
        .w_IFTSALD3 = NVL(IFTSALD3,0)
        .w_IFNUMFA3 = NVL(IFNUMFA3,0)
        .w_IFCODEL4 = NVL(IFCODEL4,space(4))
          * evitabile
          *.link_1_39('Load')
        .w_IFTRIEL4 = NVL(IFTRIEL4,space(5))
          if link_1_40_joined
            this.w_IFTRIEL4 = NVL(TRCODICE140,NVL(this.w_IFTRIEL4,space(5)))
            this.w_TIPTRI = NVL(TRTIPTRI140,space(10))
          else
          .link_1_40('Load')
          endif
        .w_IFRATEL4 = NVL(IFRATEL4,space(4))
        .w_IFANNEL4 = NVL(IFANNEL4,space(4))
        .w_IFIMDEL4 = NVL(IFIMDEL4,0)
        .w_IFIMCEL4 = NVL(IFIMCEL4,0)
        .w_IFRAVVE4 = NVL(IFRAVVE4,0)
        .w_IFIMMOV4 = NVL(IFIMMOV4,0)
        .w_IFACCON4 = NVL(IFACCON4,0)
        .w_IFTSALD4 = NVL(IFTSALD4,0)
        .w_IFNUMFA4 = NVL(IFNUMFA4,0)
        .w_IFDETICI = NVL(IFDETICI,0)
        .w_IFTOTDEL = NVL(IFTOTDEL,0)
        .w_IFTOTCEL = NVL(IFTOTCEL,0)
        .w_IFSALDEL = NVL(IFSALDEL,0)
        .w_IFSERIAL = NVL(IFSERIAL,space(10))
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_67.Calculate(ah_MSGFORMAT('Anno di %0riferim.'))
        .oPgFrm.Page1.oPag.oObj_1_68.Calculate(ah_MSGFORMAT('Immob. %0variati'))
        .oPgFrm.Page1.oPag.oObj_1_69.Calculate(ah_MSGFORMAT('Cod. %0tributo'))
        .oPgFrm.Page1.oPag.oObj_1_70.Calculate(ah_MSGFORMAT('Num. %0immob.'))
        cp_LoadRecExtFlds(this,'MODIPAG')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gscg_aif
    local lTot
    lTot=0
    with this.oParentObject
       lTot = .w_MFSALDPS + .w_MFSALDRE + .w_MFSALINA + .w_MFSALAEN + .w_APPOIMP
    endwith
    * aggiorna variabile sul padre
    this.oParentObject.w_APPOIMP2 = this.w_IFSALDEL
    this.oParentObject.w_MFSALFIN = this.w_IFSALDEL + lTot
    this.oParentObject.SetControlsValue()
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_IFCODEL1 = space(4)
      .w_IFTRIEL1 = space(5)
      .w_IFRATEL1 = space(4)
      .w_IFANNEL1 = space(4)
      .w_IFIMDEL1 = 0
      .w_IFIMCEL1 = 0
      .w_IFRAVVE1 = 0
      .w_IFIMMOV1 = 0
      .w_IFACCON1 = 0
      .w_IFTSALD1 = 0
      .w_IFNUMFA1 = 0
      .w_IFCODEL2 = space(4)
      .w_IFTRIEL2 = space(5)
      .w_IFRATEL2 = space(4)
      .w_IFANNEL2 = space(4)
      .w_IFIMDEL2 = 0
      .w_IFIMCEL2 = 0
      .w_IFRAVVE2 = 0
      .w_IFIMMOV2 = 0
      .w_IFACCON2 = 0
      .w_IFTSALD2 = 0
      .w_IFNUMFA2 = 0
      .w_IFCODEL3 = space(4)
      .w_IFTRIEL3 = space(5)
      .w_IFRATEL3 = space(4)
      .w_IFANNEL3 = space(4)
      .w_IFIMDEL3 = 0
      .w_IFIMCEL3 = 0
      .w_IFRAVVE3 = 0
      .w_IFIMMOV3 = 0
      .w_IFACCON3 = 0
      .w_IFTSALD3 = 0
      .w_IFNUMFA3 = 0
      .w_IFCODEL4 = space(4)
      .w_IFTRIEL4 = space(5)
      .w_IFRATEL4 = space(4)
      .w_IFANNEL4 = space(4)
      .w_IFIMDEL4 = 0
      .w_IFIMCEL4 = 0
      .w_IFRAVVE4 = 0
      .w_IFIMMOV4 = 0
      .w_IFACCON4 = 0
      .w_IFTSALD4 = 0
      .w_IFNUMFA4 = 0
      .w_IFDETICI = 0
      .w_IFTOTDEL = 0
      .w_IFTOTCEL = 0
      .w_IFSALDEL = 0
      .w_IFSERIAL = space(10)
      .w_TIPTRI = space(10)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_IFCODEL1))
          .link_1_6('Full')
          endif
        .w_IFTRIEL1 = iif(empty(.w_IFCODEL1),' ',.w_IFTRIEL1)
        .DoRTCalc(2,2,.f.)
          if not(empty(.w_IFTRIEL1))
          .link_1_7('Full')
          endif
        .w_IFRATEL1 = iif(empty(.w_IFCODEL1),' ',.w_IFRATEL1)
        .w_IFANNEL1 = iif(empty(.w_IFCODEL1),' ',.w_IFANNEL1)
        .w_IFIMDEL1 = iif(empty(.w_IFCODEL1),0,.w_IFIMDEL1)
        .w_IFIMCEL1 = iif(empty(.w_IFCODEL1),0,.w_IFIMCEL1)
        .w_IFRAVVE1 = iif(empty(.w_IFCODEL1) OR NOT INLIST (.w_IFTRIEL1,'3901','3902','3903','3904','3905','3906','3907'), 0, .w_IFRAVVE1)
        .w_IFIMMOV1 = iif(empty(.w_IFCODEL1) OR NOT INLIST (.w_IFTRIEL1,'3901','3902','3903','3904','3905','3906','3907'), 0, .w_IFIMMOV1)
        .w_IFACCON1 = iif(empty(.w_IFCODEL1) OR NOT INLIST (.w_IFTRIEL1,'3901','3902','3903','3904','3905','3906','3907'), 0, .w_IFACCON1)
        .w_IFTSALD1 = iif(empty(.w_IFCODEL1) OR NOT INLIST (.w_IFTRIEL1,'3901','3902','3903','3904','3905','3906','3907'), 0, .w_IFTSALD1)
        .w_IFNUMFA1 = iif(empty(.w_IFCODEL1) OR NOT INLIST (.w_IFTRIEL1,'3901','3902','3903','3904','3905','3906','3907'), 0, .w_IFNUMFA1)
        .DoRTCalc(12,12,.f.)
          if not(empty(.w_IFCODEL2))
          .link_1_17('Full')
          endif
        .w_IFTRIEL2 = iif(empty(.w_IFCODEL2),' ',.w_IFTRIEL2)
        .DoRTCalc(13,13,.f.)
          if not(empty(.w_IFTRIEL2))
          .link_1_18('Full')
          endif
        .w_IFRATEL2 = iif(empty(.w_IFCODEL2),' ',.w_IFRATEL2)
        .w_IFANNEL2 = iif(empty(.w_IFCODEL2),' ',.w_IFANNEL2)
        .w_IFIMDEL2 = iif(empty(.w_IFCODEL2),0,.w_IFIMDEL2)
        .w_IFIMCEL2 = iif(empty(.w_IFCODEL2),0,.w_IFIMCEL2)
        .w_IFRAVVE2 = iif(empty(.w_IFCODEL2) OR NOT INLIST (.w_IFTRIEL2,'3901','3902','3903','3904','3905','3906','3907'), 0, .w_IFRAVVE2)
        .w_IFIMMOV2 = iif(empty(.w_IFCODEL2) OR NOT INLIST (.w_IFTRIEL2,'3901','3902','3903','3904','3905','3906','3907'), 0, .w_IFIMMOV2)
        .w_IFACCON2 = iif(empty(.w_IFCODEL2) OR NOT INLIST (.w_IFTRIEL2,'3901','3902','3903','3904','3905','3906','3907'), 0, .w_IFACCON2)
        .w_IFTSALD2 = iif(empty(.w_IFCODEL2) OR NOT INLIST (.w_IFTRIEL2,'3901','3902','3903','3904','3905','3906','3907'), 0, .w_IFTSALD2)
        .w_IFNUMFA2 = iif(empty(.w_IFCODEL2) OR NOT INLIST (.w_IFTRIEL2,'3901','3902','3903','3904','3905','3906','3907'), 0, .w_IFNUMFA2)
        .DoRTCalc(23,23,.f.)
          if not(empty(.w_IFCODEL3))
          .link_1_28('Full')
          endif
        .w_IFTRIEL3 = iif(empty(.w_IFCODEL3),' ',.w_IFTRIEL3)
        .DoRTCalc(24,24,.f.)
          if not(empty(.w_IFTRIEL3))
          .link_1_29('Full')
          endif
        .w_IFRATEL3 = iif(empty(.w_IFCODEL3),' ',.w_IFRATEL3)
        .w_IFANNEL3 = iif(empty(.w_IFCODEL3),' ',.w_IFANNEL3)
        .w_IFIMDEL3 = iif(empty(.w_IFCODEL3),0,.w_IFIMDEL3)
        .w_IFIMCEL3 = iif(empty(.w_IFCODEL3),0,.w_IFIMCEL3)
        .w_IFRAVVE3 = iif(empty(.w_IFCODEL3) OR NOT INLIST (.w_IFTRIEL3,'3901','3902','3903','3904','3905','3906','3907'), 0, .w_IFRAVVE3)
        .w_IFIMMOV3 = iif(empty(.w_IFCODEL3) OR NOT INLIST (.w_IFTRIEL3,'3901','3902','3903','3904','3905','3906','3907'), 0, .w_IFIMMOV3)
        .w_IFACCON3 = iif(empty(.w_IFCODEL3) OR NOT INLIST (.w_IFTRIEL3,'3901','3902','3903','3904','3905','3906','3907'), 0, .w_IFACCON3)
        .w_IFTSALD3 = iif(empty(.w_IFCODEL3) OR NOT INLIST (.w_IFTRIEL3,'3901','3902','3903','3904','3905','3906','3907'), 0, .w_IFTSALD3)
        .w_IFNUMFA3 = iif(empty(.w_IFCODEL3) OR NOT INLIST (.w_IFTRIEL3,'3901','3902','3903','3904','3905','3906','3907'), 0, .w_IFNUMFA3)
        .DoRTCalc(34,34,.f.)
          if not(empty(.w_IFCODEL4))
          .link_1_39('Full')
          endif
        .w_IFTRIEL4 = iif(empty(.w_IFCODEL4),' ',.w_IFTRIEL4)
        .DoRTCalc(35,35,.f.)
          if not(empty(.w_IFTRIEL4))
          .link_1_40('Full')
          endif
        .w_IFRATEL4 = iif(empty(.w_IFCODEL4),' ',.w_IFRATEL4)
        .w_IFANNEL4 = iif(empty(.w_IFCODEL4),' ',.w_IFANNEL4)
        .w_IFIMDEL4 = iif(empty(.w_IFCODEL4),0,.w_IFIMDEL4)
        .w_IFIMCEL4 = iif(empty(.w_IFCODEL4),0,.w_IFIMCEL4)
        .w_IFRAVVE4 = iif(empty(.w_IFCODEL4) OR NOT INLIST (.w_IFTRIEL4,'3901','3902','3903','3904','3905','3906','3907'), 0, .w_IFRAVVE4)
        .w_IFIMMOV4 = iif(empty(.w_IFCODEL4) OR NOT INLIST (.w_IFTRIEL4,'3901','3902','3903','3904','3905','3906','3907'), 0, .w_IFIMMOV4)
        .w_IFACCON4 = iif(empty(.w_IFCODEL4) OR NOT INLIST (.w_IFTRIEL4,'3901','3902','3903','3904','3905','3906','3907'), 0, .w_IFACCON4)
        .w_IFTSALD4 = iif(empty(.w_IFCODEL4) OR NOT INLIST (.w_IFTRIEL4,'3901','3902','3903','3904','3905','3906','3907'), 0, .w_IFTSALD4)
        .w_IFNUMFA4 = iif(empty(.w_IFCODEL4) OR NOT INLIST (.w_IFTRIEL4,'3901','3902','3903','3904','3905','3906','3907'), 0, .w_IFNUMFA4)
          .DoRTCalc(45,45,.f.)
        .w_IFTOTDEL = .w_IFIMDEL1+.w_IFIMDEL2+.w_IFIMDEL3+.w_IFIMDEL4
        .w_IFTOTCEL = .w_IFIMCEL1+.w_IFIMCEL2+.w_IFIMCEL3+.w_IFIMCEL4
        .w_IFSALDEL = .w_IFTOTDEL-.w_IFTOTCEL
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_67.Calculate(ah_MSGFORMAT('Anno di %0riferim.'))
        .oPgFrm.Page1.oPag.oObj_1_68.Calculate(ah_MSGFORMAT('Immob. %0variati'))
        .oPgFrm.Page1.oPag.oObj_1_69.Calculate(ah_MSGFORMAT('Cod. %0tributo'))
        .oPgFrm.Page1.oPag.oObj_1_70.Calculate(ah_MSGFORMAT('Num. %0immob.'))
      endif
    endwith
    cp_BlankRecExtFlds(this,'MODIPAG')
    this.DoRTCalc(49,50,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gscg_aif
    * Forza aggiornamento del database
    this.bupdated=.t.
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oIFCODEL1_1_6.enabled = i_bVal
      .Page1.oPag.oIFTRIEL1_1_7.enabled = i_bVal
      .Page1.oPag.oIFRATEL1_1_8.enabled = i_bVal
      .Page1.oPag.oIFANNEL1_1_9.enabled = i_bVal
      .Page1.oPag.oIFIMDEL1_1_10.enabled = i_bVal
      .Page1.oPag.oIFIMCEL1_1_11.enabled = i_bVal
      .Page1.oPag.oIFRAVVE1_1_12.enabled = i_bVal
      .Page1.oPag.oIFIMMOV1_1_13.enabled = i_bVal
      .Page1.oPag.oIFACCON1_1_14.enabled = i_bVal
      .Page1.oPag.oIFTSALD1_1_15.enabled = i_bVal
      .Page1.oPag.oIFNUMFA1_1_16.enabled = i_bVal
      .Page1.oPag.oIFCODEL2_1_17.enabled = i_bVal
      .Page1.oPag.oIFTRIEL2_1_18.enabled = i_bVal
      .Page1.oPag.oIFRATEL2_1_19.enabled = i_bVal
      .Page1.oPag.oIFANNEL2_1_20.enabled = i_bVal
      .Page1.oPag.oIFIMDEL2_1_21.enabled = i_bVal
      .Page1.oPag.oIFIMCEL2_1_22.enabled = i_bVal
      .Page1.oPag.oIFRAVVE2_1_23.enabled = i_bVal
      .Page1.oPag.oIFIMMOV2_1_24.enabled = i_bVal
      .Page1.oPag.oIFACCON2_1_25.enabled = i_bVal
      .Page1.oPag.oIFTSALD2_1_26.enabled = i_bVal
      .Page1.oPag.oIFNUMFA2_1_27.enabled = i_bVal
      .Page1.oPag.oIFCODEL3_1_28.enabled = i_bVal
      .Page1.oPag.oIFTRIEL3_1_29.enabled = i_bVal
      .Page1.oPag.oIFRATEL3_1_30.enabled = i_bVal
      .Page1.oPag.oIFANNEL3_1_31.enabled = i_bVal
      .Page1.oPag.oIFIMDEL3_1_32.enabled = i_bVal
      .Page1.oPag.oIFIMCEL3_1_33.enabled = i_bVal
      .Page1.oPag.oIFRAVVE3_1_34.enabled = i_bVal
      .Page1.oPag.oIFIMMOV3_1_35.enabled = i_bVal
      .Page1.oPag.oIFACCON3_1_36.enabled = i_bVal
      .Page1.oPag.oIFTSALD3_1_37.enabled = i_bVal
      .Page1.oPag.oIFNUMFA3_1_38.enabled = i_bVal
      .Page1.oPag.oIFCODEL4_1_39.enabled = i_bVal
      .Page1.oPag.oIFTRIEL4_1_40.enabled = i_bVal
      .Page1.oPag.oIFRATEL4_1_41.enabled = i_bVal
      .Page1.oPag.oIFANNEL4_1_42.enabled = i_bVal
      .Page1.oPag.oIFIMDEL4_1_43.enabled = i_bVal
      .Page1.oPag.oIFIMCEL4_1_44.enabled = i_bVal
      .Page1.oPag.oIFRAVVE4_1_45.enabled = i_bVal
      .Page1.oPag.oIFIMMOV4_1_46.enabled = i_bVal
      .Page1.oPag.oIFACCON4_1_47.enabled = i_bVal
      .Page1.oPag.oIFTSALD4_1_48.enabled = i_bVal
      .Page1.oPag.oIFNUMFA4_1_49.enabled = i_bVal
      .Page1.oPag.oIFDETICI_1_50.enabled = i_bVal
      .Page1.oPag.oObj_1_65.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'MODIPAG',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MODIPAG_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFCODEL1,"IFCODEL1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFTRIEL1,"IFTRIEL1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFRATEL1,"IFRATEL1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFANNEL1,"IFANNEL1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFIMDEL1,"IFIMDEL1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFIMCEL1,"IFIMCEL1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFRAVVE1,"IFRAVVE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFIMMOV1,"IFIMMOV1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFACCON1,"IFACCON1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFTSALD1,"IFTSALD1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFNUMFA1,"IFNUMFA1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFCODEL2,"IFCODEL2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFTRIEL2,"IFTRIEL2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFRATEL2,"IFRATEL2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFANNEL2,"IFANNEL2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFIMDEL2,"IFIMDEL2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFIMCEL2,"IFIMCEL2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFRAVVE2,"IFRAVVE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFIMMOV2,"IFIMMOV2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFACCON2,"IFACCON2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFTSALD2,"IFTSALD2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFNUMFA2,"IFNUMFA2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFCODEL3,"IFCODEL3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFTRIEL3,"IFTRIEL3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFRATEL3,"IFRATEL3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFANNEL3,"IFANNEL3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFIMDEL3,"IFIMDEL3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFIMCEL3,"IFIMCEL3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFRAVVE3,"IFRAVVE3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFIMMOV3,"IFIMMOV3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFACCON3,"IFACCON3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFTSALD3,"IFTSALD3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFNUMFA3,"IFNUMFA3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFCODEL4,"IFCODEL4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFTRIEL4,"IFTRIEL4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFRATEL4,"IFRATEL4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFANNEL4,"IFANNEL4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFIMDEL4,"IFIMDEL4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFIMCEL4,"IFIMCEL4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFRAVVE4,"IFRAVVE4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFIMMOV4,"IFIMMOV4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFACCON4,"IFACCON4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFTSALD4,"IFTSALD4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFNUMFA4,"IFNUMFA4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFDETICI,"IFDETICI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFTOTDEL,"IFTOTDEL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFTOTCEL,"IFTOTCEL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFSALDEL,"IFSALDEL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IFSERIAL,"IFSERIAL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MODIPAG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MODIPAG_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.MODIPAG_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into MODIPAG
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MODIPAG')
        i_extval=cp_InsertValODBCExtFlds(this,'MODIPAG')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(IFCODEL1,IFTRIEL1,IFRATEL1,IFANNEL1,IFIMDEL1"+;
                  ",IFIMCEL1,IFRAVVE1,IFIMMOV1,IFACCON1,IFTSALD1"+;
                  ",IFNUMFA1,IFCODEL2,IFTRIEL2,IFRATEL2,IFANNEL2"+;
                  ",IFIMDEL2,IFIMCEL2,IFRAVVE2,IFIMMOV2,IFACCON2"+;
                  ",IFTSALD2,IFNUMFA2,IFCODEL3,IFTRIEL3,IFRATEL3"+;
                  ",IFANNEL3,IFIMDEL3,IFIMCEL3,IFRAVVE3,IFIMMOV3"+;
                  ",IFACCON3,IFTSALD3,IFNUMFA3,IFCODEL4,IFTRIEL4"+;
                  ",IFRATEL4,IFANNEL4,IFIMDEL4,IFIMCEL4,IFRAVVE4"+;
                  ",IFIMMOV4,IFACCON4,IFTSALD4,IFNUMFA4,IFDETICI"+;
                  ",IFTOTDEL,IFTOTCEL,IFSALDEL,IFSERIAL "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_IFCODEL1)+;
                  ","+cp_ToStrODBCNull(this.w_IFTRIEL1)+;
                  ","+cp_ToStrODBC(this.w_IFRATEL1)+;
                  ","+cp_ToStrODBC(this.w_IFANNEL1)+;
                  ","+cp_ToStrODBC(this.w_IFIMDEL1)+;
                  ","+cp_ToStrODBC(this.w_IFIMCEL1)+;
                  ","+cp_ToStrODBC(this.w_IFRAVVE1)+;
                  ","+cp_ToStrODBC(this.w_IFIMMOV1)+;
                  ","+cp_ToStrODBC(this.w_IFACCON1)+;
                  ","+cp_ToStrODBC(this.w_IFTSALD1)+;
                  ","+cp_ToStrODBC(this.w_IFNUMFA1)+;
                  ","+cp_ToStrODBCNull(this.w_IFCODEL2)+;
                  ","+cp_ToStrODBCNull(this.w_IFTRIEL2)+;
                  ","+cp_ToStrODBC(this.w_IFRATEL2)+;
                  ","+cp_ToStrODBC(this.w_IFANNEL2)+;
                  ","+cp_ToStrODBC(this.w_IFIMDEL2)+;
                  ","+cp_ToStrODBC(this.w_IFIMCEL2)+;
                  ","+cp_ToStrODBC(this.w_IFRAVVE2)+;
                  ","+cp_ToStrODBC(this.w_IFIMMOV2)+;
                  ","+cp_ToStrODBC(this.w_IFACCON2)+;
                  ","+cp_ToStrODBC(this.w_IFTSALD2)+;
                  ","+cp_ToStrODBC(this.w_IFNUMFA2)+;
                  ","+cp_ToStrODBCNull(this.w_IFCODEL3)+;
                  ","+cp_ToStrODBCNull(this.w_IFTRIEL3)+;
                  ","+cp_ToStrODBC(this.w_IFRATEL3)+;
                  ","+cp_ToStrODBC(this.w_IFANNEL3)+;
                  ","+cp_ToStrODBC(this.w_IFIMDEL3)+;
                  ","+cp_ToStrODBC(this.w_IFIMCEL3)+;
                  ","+cp_ToStrODBC(this.w_IFRAVVE3)+;
                  ","+cp_ToStrODBC(this.w_IFIMMOV3)+;
                  ","+cp_ToStrODBC(this.w_IFACCON3)+;
                  ","+cp_ToStrODBC(this.w_IFTSALD3)+;
                  ","+cp_ToStrODBC(this.w_IFNUMFA3)+;
                  ","+cp_ToStrODBCNull(this.w_IFCODEL4)+;
                  ","+cp_ToStrODBCNull(this.w_IFTRIEL4)+;
                  ","+cp_ToStrODBC(this.w_IFRATEL4)+;
                  ","+cp_ToStrODBC(this.w_IFANNEL4)+;
                  ","+cp_ToStrODBC(this.w_IFIMDEL4)+;
                  ","+cp_ToStrODBC(this.w_IFIMCEL4)+;
                  ","+cp_ToStrODBC(this.w_IFRAVVE4)+;
                  ","+cp_ToStrODBC(this.w_IFIMMOV4)+;
                  ","+cp_ToStrODBC(this.w_IFACCON4)+;
                  ","+cp_ToStrODBC(this.w_IFTSALD4)+;
                  ","+cp_ToStrODBC(this.w_IFNUMFA4)+;
                  ","+cp_ToStrODBC(this.w_IFDETICI)+;
                  ","+cp_ToStrODBC(this.w_IFTOTDEL)+;
                  ","+cp_ToStrODBC(this.w_IFTOTCEL)+;
                  ","+cp_ToStrODBC(this.w_IFSALDEL)+;
                  ","+cp_ToStrODBC(this.w_IFSERIAL)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MODIPAG')
        i_extval=cp_InsertValVFPExtFlds(this,'MODIPAG')
        cp_CheckDeletedKey(i_cTable,0,'IFSERIAL',this.w_IFSERIAL)
        INSERT INTO (i_cTable);
              (IFCODEL1,IFTRIEL1,IFRATEL1,IFANNEL1,IFIMDEL1,IFIMCEL1,IFRAVVE1,IFIMMOV1,IFACCON1,IFTSALD1,IFNUMFA1,IFCODEL2,IFTRIEL2,IFRATEL2,IFANNEL2,IFIMDEL2,IFIMCEL2,IFRAVVE2,IFIMMOV2,IFACCON2,IFTSALD2,IFNUMFA2,IFCODEL3,IFTRIEL3,IFRATEL3,IFANNEL3,IFIMDEL3,IFIMCEL3,IFRAVVE3,IFIMMOV3,IFACCON3,IFTSALD3,IFNUMFA3,IFCODEL4,IFTRIEL4,IFRATEL4,IFANNEL4,IFIMDEL4,IFIMCEL4,IFRAVVE4,IFIMMOV4,IFACCON4,IFTSALD4,IFNUMFA4,IFDETICI,IFTOTDEL,IFTOTCEL,IFSALDEL,IFSERIAL  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_IFCODEL1;
                  ,this.w_IFTRIEL1;
                  ,this.w_IFRATEL1;
                  ,this.w_IFANNEL1;
                  ,this.w_IFIMDEL1;
                  ,this.w_IFIMCEL1;
                  ,this.w_IFRAVVE1;
                  ,this.w_IFIMMOV1;
                  ,this.w_IFACCON1;
                  ,this.w_IFTSALD1;
                  ,this.w_IFNUMFA1;
                  ,this.w_IFCODEL2;
                  ,this.w_IFTRIEL2;
                  ,this.w_IFRATEL2;
                  ,this.w_IFANNEL2;
                  ,this.w_IFIMDEL2;
                  ,this.w_IFIMCEL2;
                  ,this.w_IFRAVVE2;
                  ,this.w_IFIMMOV2;
                  ,this.w_IFACCON2;
                  ,this.w_IFTSALD2;
                  ,this.w_IFNUMFA2;
                  ,this.w_IFCODEL3;
                  ,this.w_IFTRIEL3;
                  ,this.w_IFRATEL3;
                  ,this.w_IFANNEL3;
                  ,this.w_IFIMDEL3;
                  ,this.w_IFIMCEL3;
                  ,this.w_IFRAVVE3;
                  ,this.w_IFIMMOV3;
                  ,this.w_IFACCON3;
                  ,this.w_IFTSALD3;
                  ,this.w_IFNUMFA3;
                  ,this.w_IFCODEL4;
                  ,this.w_IFTRIEL4;
                  ,this.w_IFRATEL4;
                  ,this.w_IFANNEL4;
                  ,this.w_IFIMDEL4;
                  ,this.w_IFIMCEL4;
                  ,this.w_IFRAVVE4;
                  ,this.w_IFIMMOV4;
                  ,this.w_IFACCON4;
                  ,this.w_IFTSALD4;
                  ,this.w_IFNUMFA4;
                  ,this.w_IFDETICI;
                  ,this.w_IFTOTDEL;
                  ,this.w_IFTOTCEL;
                  ,this.w_IFSALDEL;
                  ,this.w_IFSERIAL;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.MODIPAG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MODIPAG_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.MODIPAG_IDX,i_nConn)
      *
      * update MODIPAG
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'MODIPAG')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " IFCODEL1="+cp_ToStrODBCNull(this.w_IFCODEL1)+;
             ",IFTRIEL1="+cp_ToStrODBCNull(this.w_IFTRIEL1)+;
             ",IFRATEL1="+cp_ToStrODBC(this.w_IFRATEL1)+;
             ",IFANNEL1="+cp_ToStrODBC(this.w_IFANNEL1)+;
             ",IFIMDEL1="+cp_ToStrODBC(this.w_IFIMDEL1)+;
             ",IFIMCEL1="+cp_ToStrODBC(this.w_IFIMCEL1)+;
             ",IFRAVVE1="+cp_ToStrODBC(this.w_IFRAVVE1)+;
             ",IFIMMOV1="+cp_ToStrODBC(this.w_IFIMMOV1)+;
             ",IFACCON1="+cp_ToStrODBC(this.w_IFACCON1)+;
             ",IFTSALD1="+cp_ToStrODBC(this.w_IFTSALD1)+;
             ",IFNUMFA1="+cp_ToStrODBC(this.w_IFNUMFA1)+;
             ",IFCODEL2="+cp_ToStrODBCNull(this.w_IFCODEL2)+;
             ",IFTRIEL2="+cp_ToStrODBCNull(this.w_IFTRIEL2)+;
             ",IFRATEL2="+cp_ToStrODBC(this.w_IFRATEL2)+;
             ",IFANNEL2="+cp_ToStrODBC(this.w_IFANNEL2)+;
             ",IFIMDEL2="+cp_ToStrODBC(this.w_IFIMDEL2)+;
             ",IFIMCEL2="+cp_ToStrODBC(this.w_IFIMCEL2)+;
             ",IFRAVVE2="+cp_ToStrODBC(this.w_IFRAVVE2)+;
             ",IFIMMOV2="+cp_ToStrODBC(this.w_IFIMMOV2)+;
             ",IFACCON2="+cp_ToStrODBC(this.w_IFACCON2)+;
             ",IFTSALD2="+cp_ToStrODBC(this.w_IFTSALD2)+;
             ",IFNUMFA2="+cp_ToStrODBC(this.w_IFNUMFA2)+;
             ",IFCODEL3="+cp_ToStrODBCNull(this.w_IFCODEL3)+;
             ",IFTRIEL3="+cp_ToStrODBCNull(this.w_IFTRIEL3)+;
             ",IFRATEL3="+cp_ToStrODBC(this.w_IFRATEL3)+;
             ",IFANNEL3="+cp_ToStrODBC(this.w_IFANNEL3)+;
             ",IFIMDEL3="+cp_ToStrODBC(this.w_IFIMDEL3)+;
             ",IFIMCEL3="+cp_ToStrODBC(this.w_IFIMCEL3)+;
             ",IFRAVVE3="+cp_ToStrODBC(this.w_IFRAVVE3)+;
             ",IFIMMOV3="+cp_ToStrODBC(this.w_IFIMMOV3)+;
             ",IFACCON3="+cp_ToStrODBC(this.w_IFACCON3)+;
             ",IFTSALD3="+cp_ToStrODBC(this.w_IFTSALD3)+;
             ",IFNUMFA3="+cp_ToStrODBC(this.w_IFNUMFA3)+;
             ",IFCODEL4="+cp_ToStrODBCNull(this.w_IFCODEL4)+;
             ",IFTRIEL4="+cp_ToStrODBCNull(this.w_IFTRIEL4)+;
             ",IFRATEL4="+cp_ToStrODBC(this.w_IFRATEL4)+;
             ",IFANNEL4="+cp_ToStrODBC(this.w_IFANNEL4)+;
             ",IFIMDEL4="+cp_ToStrODBC(this.w_IFIMDEL4)+;
             ",IFIMCEL4="+cp_ToStrODBC(this.w_IFIMCEL4)+;
             ",IFRAVVE4="+cp_ToStrODBC(this.w_IFRAVVE4)+;
             ",IFIMMOV4="+cp_ToStrODBC(this.w_IFIMMOV4)+;
             ",IFACCON4="+cp_ToStrODBC(this.w_IFACCON4)+;
             ",IFTSALD4="+cp_ToStrODBC(this.w_IFTSALD4)+;
             ",IFNUMFA4="+cp_ToStrODBC(this.w_IFNUMFA4)+;
             ",IFDETICI="+cp_ToStrODBC(this.w_IFDETICI)+;
             ",IFTOTDEL="+cp_ToStrODBC(this.w_IFTOTDEL)+;
             ",IFTOTCEL="+cp_ToStrODBC(this.w_IFTOTCEL)+;
             ",IFSALDEL="+cp_ToStrODBC(this.w_IFSALDEL)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'MODIPAG')
        i_cWhere = cp_PKFox(i_cTable  ,'IFSERIAL',this.w_IFSERIAL  )
        UPDATE (i_cTable) SET;
              IFCODEL1=this.w_IFCODEL1;
             ,IFTRIEL1=this.w_IFTRIEL1;
             ,IFRATEL1=this.w_IFRATEL1;
             ,IFANNEL1=this.w_IFANNEL1;
             ,IFIMDEL1=this.w_IFIMDEL1;
             ,IFIMCEL1=this.w_IFIMCEL1;
             ,IFRAVVE1=this.w_IFRAVVE1;
             ,IFIMMOV1=this.w_IFIMMOV1;
             ,IFACCON1=this.w_IFACCON1;
             ,IFTSALD1=this.w_IFTSALD1;
             ,IFNUMFA1=this.w_IFNUMFA1;
             ,IFCODEL2=this.w_IFCODEL2;
             ,IFTRIEL2=this.w_IFTRIEL2;
             ,IFRATEL2=this.w_IFRATEL2;
             ,IFANNEL2=this.w_IFANNEL2;
             ,IFIMDEL2=this.w_IFIMDEL2;
             ,IFIMCEL2=this.w_IFIMCEL2;
             ,IFRAVVE2=this.w_IFRAVVE2;
             ,IFIMMOV2=this.w_IFIMMOV2;
             ,IFACCON2=this.w_IFACCON2;
             ,IFTSALD2=this.w_IFTSALD2;
             ,IFNUMFA2=this.w_IFNUMFA2;
             ,IFCODEL3=this.w_IFCODEL3;
             ,IFTRIEL3=this.w_IFTRIEL3;
             ,IFRATEL3=this.w_IFRATEL3;
             ,IFANNEL3=this.w_IFANNEL3;
             ,IFIMDEL3=this.w_IFIMDEL3;
             ,IFIMCEL3=this.w_IFIMCEL3;
             ,IFRAVVE3=this.w_IFRAVVE3;
             ,IFIMMOV3=this.w_IFIMMOV3;
             ,IFACCON3=this.w_IFACCON3;
             ,IFTSALD3=this.w_IFTSALD3;
             ,IFNUMFA3=this.w_IFNUMFA3;
             ,IFCODEL4=this.w_IFCODEL4;
             ,IFTRIEL4=this.w_IFTRIEL4;
             ,IFRATEL4=this.w_IFRATEL4;
             ,IFANNEL4=this.w_IFANNEL4;
             ,IFIMDEL4=this.w_IFIMDEL4;
             ,IFIMCEL4=this.w_IFIMCEL4;
             ,IFRAVVE4=this.w_IFRAVVE4;
             ,IFIMMOV4=this.w_IFIMMOV4;
             ,IFACCON4=this.w_IFACCON4;
             ,IFTSALD4=this.w_IFTSALD4;
             ,IFNUMFA4=this.w_IFNUMFA4;
             ,IFDETICI=this.w_IFDETICI;
             ,IFTOTDEL=this.w_IFTOTDEL;
             ,IFTOTCEL=this.w_IFTOTCEL;
             ,IFSALDEL=this.w_IFSALDEL;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.MODIPAG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MODIPAG_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.MODIPAG_IDX,i_nConn)
      *
      * delete MODIPAG
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'IFSERIAL',this.w_IFSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MODIPAG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MODIPAG_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_IFCODEL1<>.w_IFCODEL1
            .w_IFTRIEL1 = iif(empty(.w_IFCODEL1),' ',.w_IFTRIEL1)
          .link_1_7('Full')
        endif
        if .o_IFCODEL1<>.w_IFCODEL1
            .w_IFRATEL1 = iif(empty(.w_IFCODEL1),' ',.w_IFRATEL1)
        endif
        if .o_IFCODEL1<>.w_IFCODEL1
            .w_IFANNEL1 = iif(empty(.w_IFCODEL1),' ',.w_IFANNEL1)
        endif
        if .o_IFCODEL1<>.w_IFCODEL1
            .w_IFIMDEL1 = iif(empty(.w_IFCODEL1),0,.w_IFIMDEL1)
        endif
        if .o_IFCODEL1<>.w_IFCODEL1
            .w_IFIMCEL1 = iif(empty(.w_IFCODEL1),0,.w_IFIMCEL1)
        endif
        if .o_IFCODEL1<>.w_IFCODEL1.or. .o_IFTRIEL1<>.w_IFTRIEL1
            .w_IFRAVVE1 = iif(empty(.w_IFCODEL1) OR NOT INLIST (.w_IFTRIEL1,'3901','3902','3903','3904','3905','3906','3907'), 0, .w_IFRAVVE1)
        endif
        if .o_IFCODEL1<>.w_IFCODEL1.or. .o_IFTRIEL1<>.w_IFTRIEL1
            .w_IFIMMOV1 = iif(empty(.w_IFCODEL1) OR NOT INLIST (.w_IFTRIEL1,'3901','3902','3903','3904','3905','3906','3907'), 0, .w_IFIMMOV1)
        endif
        if .o_IFCODEL1<>.w_IFCODEL1.or. .o_IFTRIEL1<>.w_IFTRIEL1
            .w_IFACCON1 = iif(empty(.w_IFCODEL1) OR NOT INLIST (.w_IFTRIEL1,'3901','3902','3903','3904','3905','3906','3907'), 0, .w_IFACCON1)
        endif
        if .o_IFCODEL1<>.w_IFCODEL1.or. .o_IFTRIEL1<>.w_IFTRIEL1
            .w_IFTSALD1 = iif(empty(.w_IFCODEL1) OR NOT INLIST (.w_IFTRIEL1,'3901','3902','3903','3904','3905','3906','3907'), 0, .w_IFTSALD1)
        endif
        if .o_IFCODEL1<>.w_IFCODEL1.or. .o_IFTRIEL1<>.w_IFTRIEL1
            .w_IFNUMFA1 = iif(empty(.w_IFCODEL1) OR NOT INLIST (.w_IFTRIEL1,'3901','3902','3903','3904','3905','3906','3907'), 0, .w_IFNUMFA1)
        endif
        .DoRTCalc(12,12,.t.)
        if .o_IFCODEL2<>.w_IFCODEL2
            .w_IFTRIEL2 = iif(empty(.w_IFCODEL2),' ',.w_IFTRIEL2)
          .link_1_18('Full')
        endif
        if .o_IFCODEL2<>.w_IFCODEL2
            .w_IFRATEL2 = iif(empty(.w_IFCODEL2),' ',.w_IFRATEL2)
        endif
        if .o_IFCODEL2<>.w_IFCODEL2
            .w_IFANNEL2 = iif(empty(.w_IFCODEL2),' ',.w_IFANNEL2)
        endif
        if .o_IFCODEL2<>.w_IFCODEL2
            .w_IFIMDEL2 = iif(empty(.w_IFCODEL2),0,.w_IFIMDEL2)
        endif
        if .o_IFCODEL2<>.w_IFCODEL2
            .w_IFIMCEL2 = iif(empty(.w_IFCODEL2),0,.w_IFIMCEL2)
        endif
        if .o_IFCODEL2<>.w_IFCODEL2.or. .o_IFTRIEL2<>.w_IFTRIEL2
            .w_IFRAVVE2 = iif(empty(.w_IFCODEL2) OR NOT INLIST (.w_IFTRIEL2,'3901','3902','3903','3904','3905','3906','3907'), 0, .w_IFRAVVE2)
        endif
        if .o_IFCODEL2<>.w_IFCODEL2.or. .o_IFTRIEL2<>.w_IFTRIEL2
            .w_IFIMMOV2 = iif(empty(.w_IFCODEL2) OR NOT INLIST (.w_IFTRIEL2,'3901','3902','3903','3904','3905','3906','3907'), 0, .w_IFIMMOV2)
        endif
        if .o_IFCODEL2<>.w_IFCODEL2.or. .o_IFTRIEL2<>.w_IFTRIEL2
            .w_IFACCON2 = iif(empty(.w_IFCODEL2) OR NOT INLIST (.w_IFTRIEL2,'3901','3902','3903','3904','3905','3906','3907'), 0, .w_IFACCON2)
        endif
        if .o_IFCODEL2<>.w_IFCODEL2.or. .o_IFTRIEL2<>.w_IFTRIEL2
            .w_IFTSALD2 = iif(empty(.w_IFCODEL2) OR NOT INLIST (.w_IFTRIEL2,'3901','3902','3903','3904','3905','3906','3907'), 0, .w_IFTSALD2)
        endif
        if .o_IFCODEL2<>.w_IFCODEL2.or. .o_IFTRIEL2<>.w_IFTRIEL2
            .w_IFNUMFA2 = iif(empty(.w_IFCODEL2) OR NOT INLIST (.w_IFTRIEL2,'3901','3902','3903','3904','3905','3906','3907'), 0, .w_IFNUMFA2)
        endif
        .DoRTCalc(23,23,.t.)
        if .o_IFCODEL3<>.w_IFCODEL3
            .w_IFTRIEL3 = iif(empty(.w_IFCODEL3),' ',.w_IFTRIEL3)
          .link_1_29('Full')
        endif
        if .o_IFCODEL3<>.w_IFCODEL3
            .w_IFRATEL3 = iif(empty(.w_IFCODEL3),' ',.w_IFRATEL3)
        endif
        if .o_IFCODEL3<>.w_IFCODEL3
            .w_IFANNEL3 = iif(empty(.w_IFCODEL3),' ',.w_IFANNEL3)
        endif
        if .o_IFCODEL3<>.w_IFCODEL3
            .w_IFIMDEL3 = iif(empty(.w_IFCODEL3),0,.w_IFIMDEL3)
        endif
        if .o_IFCODEL3<>.w_IFCODEL3
            .w_IFIMCEL3 = iif(empty(.w_IFCODEL3),0,.w_IFIMCEL3)
        endif
        if .o_IFCODEL3<>.w_IFCODEL3.or. .o_IFTRIEL3<>.w_IFTRIEL3
            .w_IFRAVVE3 = iif(empty(.w_IFCODEL3) OR NOT INLIST (.w_IFTRIEL3,'3901','3902','3903','3904','3905','3906','3907'), 0, .w_IFRAVVE3)
        endif
        if .o_IFCODEL3<>.w_IFCODEL3.or. .o_IFTRIEL3<>.w_IFTRIEL3
            .w_IFIMMOV3 = iif(empty(.w_IFCODEL3) OR NOT INLIST (.w_IFTRIEL3,'3901','3902','3903','3904','3905','3906','3907'), 0, .w_IFIMMOV3)
        endif
        if .o_IFCODEL3<>.w_IFCODEL3.or. .o_IFTRIEL3<>.w_IFTRIEL3
            .w_IFACCON3 = iif(empty(.w_IFCODEL3) OR NOT INLIST (.w_IFTRIEL3,'3901','3902','3903','3904','3905','3906','3907'), 0, .w_IFACCON3)
        endif
        if .o_IFCODEL3<>.w_IFCODEL3.or. .o_IFTRIEL3<>.w_IFTRIEL3
            .w_IFTSALD3 = iif(empty(.w_IFCODEL3) OR NOT INLIST (.w_IFTRIEL3,'3901','3902','3903','3904','3905','3906','3907'), 0, .w_IFTSALD3)
        endif
        if .o_IFCODEL3<>.w_IFCODEL3.or. .o_IFTRIEL3<>.w_IFTRIEL3
            .w_IFNUMFA3 = iif(empty(.w_IFCODEL3) OR NOT INLIST (.w_IFTRIEL3,'3901','3902','3903','3904','3905','3906','3907'), 0, .w_IFNUMFA3)
        endif
        .DoRTCalc(34,34,.t.)
        if .o_IFCODEL4<>.w_IFCODEL4
            .w_IFTRIEL4 = iif(empty(.w_IFCODEL4),' ',.w_IFTRIEL4)
          .link_1_40('Full')
        endif
        if .o_IFCODEL4<>.w_IFCODEL4
            .w_IFRATEL4 = iif(empty(.w_IFCODEL4),' ',.w_IFRATEL4)
        endif
        if .o_IFCODEL4<>.w_IFCODEL4
            .w_IFANNEL4 = iif(empty(.w_IFCODEL4),' ',.w_IFANNEL4)
        endif
        if .o_IFCODEL4<>.w_IFCODEL4
            .w_IFIMDEL4 = iif(empty(.w_IFCODEL4),0,.w_IFIMDEL4)
        endif
        if .o_IFCODEL4<>.w_IFCODEL4
            .w_IFIMCEL4 = iif(empty(.w_IFCODEL4),0,.w_IFIMCEL4)
        endif
        if .o_IFCODEL4<>.w_IFCODEL4.or. .o_IFTRIEL4<>.w_IFTRIEL4
            .w_IFRAVVE4 = iif(empty(.w_IFCODEL4) OR NOT INLIST (.w_IFTRIEL4,'3901','3902','3903','3904','3905','3906','3907'), 0, .w_IFRAVVE4)
        endif
        if .o_IFCODEL4<>.w_IFCODEL4.or. .o_IFTRIEL4<>.w_IFTRIEL4
            .w_IFIMMOV4 = iif(empty(.w_IFCODEL4) OR NOT INLIST (.w_IFTRIEL4,'3901','3902','3903','3904','3905','3906','3907'), 0, .w_IFIMMOV4)
        endif
        if .o_IFCODEL4<>.w_IFCODEL4.or. .o_IFTRIEL4<>.w_IFTRIEL4
            .w_IFACCON4 = iif(empty(.w_IFCODEL4) OR NOT INLIST (.w_IFTRIEL4,'3901','3902','3903','3904','3905','3906','3907'), 0, .w_IFACCON4)
        endif
        if .o_IFCODEL4<>.w_IFCODEL4.or. .o_IFTRIEL4<>.w_IFTRIEL4
            .w_IFTSALD4 = iif(empty(.w_IFCODEL4) OR NOT INLIST (.w_IFTRIEL4,'3901','3902','3903','3904','3905','3906','3907'), 0, .w_IFTSALD4)
        endif
        if .o_IFCODEL4<>.w_IFCODEL4.or. .o_IFTRIEL4<>.w_IFTRIEL4
            .w_IFNUMFA4 = iif(empty(.w_IFCODEL4) OR NOT INLIST (.w_IFTRIEL4,'3901','3902','3903','3904','3905','3906','3907'), 0, .w_IFNUMFA4)
        endif
        .DoRTCalc(45,45,.t.)
            .w_IFTOTDEL = .w_IFIMDEL1+.w_IFIMDEL2+.w_IFIMDEL3+.w_IFIMDEL4
            .w_IFTOTCEL = .w_IFIMCEL1+.w_IFIMCEL2+.w_IFIMCEL3+.w_IFIMCEL4
            .w_IFSALDEL = .w_IFTOTDEL-.w_IFTOTCEL
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_67.Calculate(ah_MSGFORMAT('Anno di %0riferim.'))
        .oPgFrm.Page1.oPag.oObj_1_68.Calculate(ah_MSGFORMAT('Immob. %0variati'))
        .oPgFrm.Page1.oPag.oObj_1_69.Calculate(ah_MSGFORMAT('Cod. %0tributo'))
        .oPgFrm.Page1.oPag.oObj_1_70.Calculate(ah_MSGFORMAT('Num. %0immob.'))
        * --- Area Manuale = Calculate
        * --- gscg_aif
        * aggiorna variabile sul padre
        .oParentObject.w_APPOIMP2 = .w_IFSALDEL
        .oParentObject.w_MFSALFIN = .w_IFSALDEL + .oParentObject.w_APPOIMP +  .oParentObject.w_MFSALDPS + .oParentObject.w_MFSALDRE + .oParentObject.w_MFSALINA + .oParentObject.w_MFSALAEN
        * aggiorna variabile sul padre
        .oParentObject.SetControlsValue()
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(49,50,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_67.Calculate(ah_MSGFORMAT('Anno di %0riferim.'))
        .oPgFrm.Page1.oPag.oObj_1_68.Calculate(ah_MSGFORMAT('Immob. %0variati'))
        .oPgFrm.Page1.oPag.oObj_1_69.Calculate(ah_MSGFORMAT('Cod. %0tributo'))
        .oPgFrm.Page1.oPag.oObj_1_70.Calculate(ah_MSGFORMAT('Num. %0immob.'))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oIFTRIEL1_1_7.enabled = this.oPgFrm.Page1.oPag.oIFTRIEL1_1_7.mCond()
    this.oPgFrm.Page1.oPag.oIFRATEL1_1_8.enabled = this.oPgFrm.Page1.oPag.oIFRATEL1_1_8.mCond()
    this.oPgFrm.Page1.oPag.oIFANNEL1_1_9.enabled = this.oPgFrm.Page1.oPag.oIFANNEL1_1_9.mCond()
    this.oPgFrm.Page1.oPag.oIFIMDEL1_1_10.enabled = this.oPgFrm.Page1.oPag.oIFIMDEL1_1_10.mCond()
    this.oPgFrm.Page1.oPag.oIFIMCEL1_1_11.enabled = this.oPgFrm.Page1.oPag.oIFIMCEL1_1_11.mCond()
    this.oPgFrm.Page1.oPag.oIFRAVVE1_1_12.enabled = this.oPgFrm.Page1.oPag.oIFRAVVE1_1_12.mCond()
    this.oPgFrm.Page1.oPag.oIFIMMOV1_1_13.enabled = this.oPgFrm.Page1.oPag.oIFIMMOV1_1_13.mCond()
    this.oPgFrm.Page1.oPag.oIFACCON1_1_14.enabled = this.oPgFrm.Page1.oPag.oIFACCON1_1_14.mCond()
    this.oPgFrm.Page1.oPag.oIFTSALD1_1_15.enabled = this.oPgFrm.Page1.oPag.oIFTSALD1_1_15.mCond()
    this.oPgFrm.Page1.oPag.oIFNUMFA1_1_16.enabled = this.oPgFrm.Page1.oPag.oIFNUMFA1_1_16.mCond()
    this.oPgFrm.Page1.oPag.oIFTRIEL2_1_18.enabled = this.oPgFrm.Page1.oPag.oIFTRIEL2_1_18.mCond()
    this.oPgFrm.Page1.oPag.oIFRATEL2_1_19.enabled = this.oPgFrm.Page1.oPag.oIFRATEL2_1_19.mCond()
    this.oPgFrm.Page1.oPag.oIFANNEL2_1_20.enabled = this.oPgFrm.Page1.oPag.oIFANNEL2_1_20.mCond()
    this.oPgFrm.Page1.oPag.oIFIMDEL2_1_21.enabled = this.oPgFrm.Page1.oPag.oIFIMDEL2_1_21.mCond()
    this.oPgFrm.Page1.oPag.oIFIMCEL2_1_22.enabled = this.oPgFrm.Page1.oPag.oIFIMCEL2_1_22.mCond()
    this.oPgFrm.Page1.oPag.oIFRAVVE2_1_23.enabled = this.oPgFrm.Page1.oPag.oIFRAVVE2_1_23.mCond()
    this.oPgFrm.Page1.oPag.oIFIMMOV2_1_24.enabled = this.oPgFrm.Page1.oPag.oIFIMMOV2_1_24.mCond()
    this.oPgFrm.Page1.oPag.oIFACCON2_1_25.enabled = this.oPgFrm.Page1.oPag.oIFACCON2_1_25.mCond()
    this.oPgFrm.Page1.oPag.oIFTSALD2_1_26.enabled = this.oPgFrm.Page1.oPag.oIFTSALD2_1_26.mCond()
    this.oPgFrm.Page1.oPag.oIFNUMFA2_1_27.enabled = this.oPgFrm.Page1.oPag.oIFNUMFA2_1_27.mCond()
    this.oPgFrm.Page1.oPag.oIFTRIEL3_1_29.enabled = this.oPgFrm.Page1.oPag.oIFTRIEL3_1_29.mCond()
    this.oPgFrm.Page1.oPag.oIFRATEL3_1_30.enabled = this.oPgFrm.Page1.oPag.oIFRATEL3_1_30.mCond()
    this.oPgFrm.Page1.oPag.oIFANNEL3_1_31.enabled = this.oPgFrm.Page1.oPag.oIFANNEL3_1_31.mCond()
    this.oPgFrm.Page1.oPag.oIFIMDEL3_1_32.enabled = this.oPgFrm.Page1.oPag.oIFIMDEL3_1_32.mCond()
    this.oPgFrm.Page1.oPag.oIFIMCEL3_1_33.enabled = this.oPgFrm.Page1.oPag.oIFIMCEL3_1_33.mCond()
    this.oPgFrm.Page1.oPag.oIFRAVVE3_1_34.enabled = this.oPgFrm.Page1.oPag.oIFRAVVE3_1_34.mCond()
    this.oPgFrm.Page1.oPag.oIFIMMOV3_1_35.enabled = this.oPgFrm.Page1.oPag.oIFIMMOV3_1_35.mCond()
    this.oPgFrm.Page1.oPag.oIFACCON3_1_36.enabled = this.oPgFrm.Page1.oPag.oIFACCON3_1_36.mCond()
    this.oPgFrm.Page1.oPag.oIFTSALD3_1_37.enabled = this.oPgFrm.Page1.oPag.oIFTSALD3_1_37.mCond()
    this.oPgFrm.Page1.oPag.oIFNUMFA3_1_38.enabled = this.oPgFrm.Page1.oPag.oIFNUMFA3_1_38.mCond()
    this.oPgFrm.Page1.oPag.oIFTRIEL4_1_40.enabled = this.oPgFrm.Page1.oPag.oIFTRIEL4_1_40.mCond()
    this.oPgFrm.Page1.oPag.oIFRATEL4_1_41.enabled = this.oPgFrm.Page1.oPag.oIFRATEL4_1_41.mCond()
    this.oPgFrm.Page1.oPag.oIFANNEL4_1_42.enabled = this.oPgFrm.Page1.oPag.oIFANNEL4_1_42.mCond()
    this.oPgFrm.Page1.oPag.oIFIMDEL4_1_43.enabled = this.oPgFrm.Page1.oPag.oIFIMDEL4_1_43.mCond()
    this.oPgFrm.Page1.oPag.oIFIMCEL4_1_44.enabled = this.oPgFrm.Page1.oPag.oIFIMCEL4_1_44.mCond()
    this.oPgFrm.Page1.oPag.oIFRAVVE4_1_45.enabled = this.oPgFrm.Page1.oPag.oIFRAVVE4_1_45.mCond()
    this.oPgFrm.Page1.oPag.oIFIMMOV4_1_46.enabled = this.oPgFrm.Page1.oPag.oIFIMMOV4_1_46.mCond()
    this.oPgFrm.Page1.oPag.oIFACCON4_1_47.enabled = this.oPgFrm.Page1.oPag.oIFACCON4_1_47.mCond()
    this.oPgFrm.Page1.oPag.oIFTSALD4_1_48.enabled = this.oPgFrm.Page1.oPag.oIFTSALD4_1_48.mCond()
    this.oPgFrm.Page1.oPag.oIFNUMFA4_1_49.enabled = this.oPgFrm.Page1.oPag.oIFNUMFA4_1_49.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_65.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_67.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_68.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_69.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_70.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=IFCODEL1
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ENTI_COM_IDX,3]
    i_lTable = "ENTI_COM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ENTI_COM_IDX,2], .t., this.ENTI_COM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ENTI_COM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IFCODEL1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_AEC',True,'ENTI_COM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ECCODICE like "+cp_ToStrODBC(trim(this.w_IFCODEL1)+"%");

          i_ret=cp_SQL(i_nConn,"select ECCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ECCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ECCODICE',trim(this.w_IFCODEL1))
          select ECCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ECCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IFCODEL1)==trim(_Link_.ECCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IFCODEL1) and !this.bDontReportError
            deferred_cp_zoom('ENTI_COM','*','ECCODICE',cp_AbsName(oSource.parent,'oIFCODEL1_1_6'),i_cWhere,'GSCG_AEC',"Codici enti / comuni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ECCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where ECCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ECCODICE',oSource.xKey(1))
            select ECCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IFCODEL1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ECCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where ECCODICE="+cp_ToStrODBC(this.w_IFCODEL1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ECCODICE',this.w_IFCODEL1)
            select ECCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IFCODEL1 = NVL(_Link_.ECCODICE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_IFCODEL1 = space(4)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ENTI_COM_IDX,2])+'\'+cp_ToStr(_Link_.ECCODICE,1)
      cp_ShowWarn(i_cKey,this.ENTI_COM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IFCODEL1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=IFTRIEL1
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_TRIB_IDX,3]
    i_lTable = "COD_TRIB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2], .t., this.COD_TRIB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IFTRIEL1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ATR',True,'COD_TRIB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_IFTRIEL1)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_IFTRIEL1))
          select TRCODICE,TRTIPTRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IFTRIEL1)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IFTRIEL1) and !this.bDontReportError
            deferred_cp_zoom('COD_TRIB','*','TRCODICE',cp_AbsName(oSource.parent,'oIFTRIEL1_1_7'),i_cWhere,'GSCG_ATR',"Codici tributo",'GSCG2ZTR.COD_TRIB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE,TRTIPTRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IFTRIEL1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_IFTRIEL1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_IFTRIEL1)
            select TRCODICE,TRTIPTRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IFTRIEL1 = NVL(_Link_.TRCODICE,space(5))
      this.w_TIPTRI = NVL(_Link_.TRTIPTRI,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_IFTRIEL1 = space(5)
      endif
      this.w_TIPTRI = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPTRI = 'Enti local' or .w_TIPTRI = 'ICI' or .w_TIPTRI = 'Altri trib' or empty(.w_TIPTRI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Selezionare un codice tributo di tipo 'ICI' o 'enti local'O 'altri trib'")
        endif
        this.w_IFTRIEL1 = space(5)
        this.w_TIPTRI = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_TRIB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IFTRIEL1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COD_TRIB_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_7.TRCODICE as TRCODICE107"+ ",link_1_7.TRTIPTRI as TRTIPTRI107"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_7 on MODIPAG.IFTRIEL1=link_1_7.TRCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_7"
          i_cKey=i_cKey+'+" and MODIPAG.IFTRIEL1=link_1_7.TRCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=IFCODEL2
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ENTI_COM_IDX,3]
    i_lTable = "ENTI_COM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ENTI_COM_IDX,2], .t., this.ENTI_COM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ENTI_COM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IFCODEL2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_AEC',True,'ENTI_COM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ECCODICE like "+cp_ToStrODBC(trim(this.w_IFCODEL2)+"%");

          i_ret=cp_SQL(i_nConn,"select ECCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ECCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ECCODICE',trim(this.w_IFCODEL2))
          select ECCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ECCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IFCODEL2)==trim(_Link_.ECCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IFCODEL2) and !this.bDontReportError
            deferred_cp_zoom('ENTI_COM','*','ECCODICE',cp_AbsName(oSource.parent,'oIFCODEL2_1_17'),i_cWhere,'GSCG_AEC',"Codici enti / comuni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ECCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where ECCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ECCODICE',oSource.xKey(1))
            select ECCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IFCODEL2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ECCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where ECCODICE="+cp_ToStrODBC(this.w_IFCODEL2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ECCODICE',this.w_IFCODEL2)
            select ECCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IFCODEL2 = NVL(_Link_.ECCODICE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_IFCODEL2 = space(4)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ENTI_COM_IDX,2])+'\'+cp_ToStr(_Link_.ECCODICE,1)
      cp_ShowWarn(i_cKey,this.ENTI_COM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IFCODEL2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=IFTRIEL2
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_TRIB_IDX,3]
    i_lTable = "COD_TRIB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2], .t., this.COD_TRIB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IFTRIEL2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ATR',True,'COD_TRIB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_IFTRIEL2)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_IFTRIEL2))
          select TRCODICE,TRTIPTRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IFTRIEL2)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IFTRIEL2) and !this.bDontReportError
            deferred_cp_zoom('COD_TRIB','*','TRCODICE',cp_AbsName(oSource.parent,'oIFTRIEL2_1_18'),i_cWhere,'GSCG_ATR',"Codici tributo",'GSCG2ZTR.COD_TRIB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE,TRTIPTRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IFTRIEL2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_IFTRIEL2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_IFTRIEL2)
            select TRCODICE,TRTIPTRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IFTRIEL2 = NVL(_Link_.TRCODICE,space(5))
      this.w_TIPTRI = NVL(_Link_.TRTIPTRI,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_IFTRIEL2 = space(5)
      endif
      this.w_TIPTRI = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPTRI = 'Enti local' or .w_TIPTRI = 'ICI' or .w_TIPTRI = 'Altri trib' or empty(.w_TIPTRI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Selezionare un codice tributo di tipo 'ICI' o 'enti local'O 'altri trib'")
        endif
        this.w_IFTRIEL2 = space(5)
        this.w_TIPTRI = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_TRIB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IFTRIEL2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_18(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COD_TRIB_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_18.TRCODICE as TRCODICE118"+ ",link_1_18.TRTIPTRI as TRTIPTRI118"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_18 on MODIPAG.IFTRIEL2=link_1_18.TRCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_18"
          i_cKey=i_cKey+'+" and MODIPAG.IFTRIEL2=link_1_18.TRCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=IFCODEL3
  func Link_1_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ENTI_COM_IDX,3]
    i_lTable = "ENTI_COM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ENTI_COM_IDX,2], .t., this.ENTI_COM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ENTI_COM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IFCODEL3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_AEC',True,'ENTI_COM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ECCODICE like "+cp_ToStrODBC(trim(this.w_IFCODEL3)+"%");

          i_ret=cp_SQL(i_nConn,"select ECCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ECCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ECCODICE',trim(this.w_IFCODEL3))
          select ECCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ECCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IFCODEL3)==trim(_Link_.ECCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IFCODEL3) and !this.bDontReportError
            deferred_cp_zoom('ENTI_COM','*','ECCODICE',cp_AbsName(oSource.parent,'oIFCODEL3_1_28'),i_cWhere,'GSCG_AEC',"Codici enti / comuni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ECCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where ECCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ECCODICE',oSource.xKey(1))
            select ECCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IFCODEL3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ECCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where ECCODICE="+cp_ToStrODBC(this.w_IFCODEL3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ECCODICE',this.w_IFCODEL3)
            select ECCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IFCODEL3 = NVL(_Link_.ECCODICE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_IFCODEL3 = space(4)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ENTI_COM_IDX,2])+'\'+cp_ToStr(_Link_.ECCODICE,1)
      cp_ShowWarn(i_cKey,this.ENTI_COM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IFCODEL3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=IFTRIEL3
  func Link_1_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_TRIB_IDX,3]
    i_lTable = "COD_TRIB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2], .t., this.COD_TRIB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IFTRIEL3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ATR',True,'COD_TRIB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_IFTRIEL3)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_IFTRIEL3))
          select TRCODICE,TRTIPTRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IFTRIEL3)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IFTRIEL3) and !this.bDontReportError
            deferred_cp_zoom('COD_TRIB','*','TRCODICE',cp_AbsName(oSource.parent,'oIFTRIEL3_1_29'),i_cWhere,'GSCG_ATR',"Codici tributo",'GSCG2ZTR.COD_TRIB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE,TRTIPTRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IFTRIEL3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_IFTRIEL3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_IFTRIEL3)
            select TRCODICE,TRTIPTRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IFTRIEL3 = NVL(_Link_.TRCODICE,space(5))
      this.w_TIPTRI = NVL(_Link_.TRTIPTRI,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_IFTRIEL3 = space(5)
      endif
      this.w_TIPTRI = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPTRI = 'Enti local' or .w_TIPTRI = 'ICI' or .w_TIPTRI = 'Altri trib' or empty(.w_TIPTRI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Selezionare un codice tributo di tipo 'ICI' o 'enti local'O 'altri trib'")
        endif
        this.w_IFTRIEL3 = space(5)
        this.w_TIPTRI = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_TRIB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IFTRIEL3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_29(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COD_TRIB_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_29.TRCODICE as TRCODICE129"+ ",link_1_29.TRTIPTRI as TRTIPTRI129"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_29 on MODIPAG.IFTRIEL3=link_1_29.TRCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_29"
          i_cKey=i_cKey+'+" and MODIPAG.IFTRIEL3=link_1_29.TRCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=IFCODEL4
  func Link_1_39(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ENTI_COM_IDX,3]
    i_lTable = "ENTI_COM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ENTI_COM_IDX,2], .t., this.ENTI_COM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ENTI_COM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IFCODEL4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_AEC',True,'ENTI_COM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ECCODICE like "+cp_ToStrODBC(trim(this.w_IFCODEL4)+"%");

          i_ret=cp_SQL(i_nConn,"select ECCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ECCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ECCODICE',trim(this.w_IFCODEL4))
          select ECCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ECCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IFCODEL4)==trim(_Link_.ECCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IFCODEL4) and !this.bDontReportError
            deferred_cp_zoom('ENTI_COM','*','ECCODICE',cp_AbsName(oSource.parent,'oIFCODEL4_1_39'),i_cWhere,'GSCG_AEC',"Codici enti / comuni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ECCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where ECCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ECCODICE',oSource.xKey(1))
            select ECCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IFCODEL4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ECCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where ECCODICE="+cp_ToStrODBC(this.w_IFCODEL4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ECCODICE',this.w_IFCODEL4)
            select ECCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IFCODEL4 = NVL(_Link_.ECCODICE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_IFCODEL4 = space(4)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ENTI_COM_IDX,2])+'\'+cp_ToStr(_Link_.ECCODICE,1)
      cp_ShowWarn(i_cKey,this.ENTI_COM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IFCODEL4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=IFTRIEL4
  func Link_1_40(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_TRIB_IDX,3]
    i_lTable = "COD_TRIB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2], .t., this.COD_TRIB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IFTRIEL4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ATR',True,'COD_TRIB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_IFTRIEL4)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_IFTRIEL4))
          select TRCODICE,TRTIPTRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IFTRIEL4)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IFTRIEL4) and !this.bDontReportError
            deferred_cp_zoom('COD_TRIB','*','TRCODICE',cp_AbsName(oSource.parent,'oIFTRIEL4_1_40'),i_cWhere,'GSCG_ATR',"Codici tributo",'GSCG2ZTR.COD_TRIB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE,TRTIPTRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IFTRIEL4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_IFTRIEL4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_IFTRIEL4)
            select TRCODICE,TRTIPTRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IFTRIEL4 = NVL(_Link_.TRCODICE,space(5))
      this.w_TIPTRI = NVL(_Link_.TRTIPTRI,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_IFTRIEL4 = space(5)
      endif
      this.w_TIPTRI = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPTRI = 'Enti local' or .w_TIPTRI = 'ICI' or .w_TIPTRI = 'Altri trib' or empty(.w_TIPTRI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Selezionare un codice tributo di tipo 'ICI' o 'enti local'O 'altri trib'")
        endif
        this.w_IFTRIEL4 = space(5)
        this.w_TIPTRI = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_TRIB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IFTRIEL4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_40(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COD_TRIB_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_40.TRCODICE as TRCODICE140"+ ",link_1_40.TRTIPTRI as TRTIPTRI140"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_40 on MODIPAG.IFTRIEL4=link_1_40.TRCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_40"
          i_cKey=i_cKey+'+" and MODIPAG.IFTRIEL4=link_1_40.TRCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oIFCODEL1_1_6.value==this.w_IFCODEL1)
      this.oPgFrm.Page1.oPag.oIFCODEL1_1_6.value=this.w_IFCODEL1
    endif
    if not(this.oPgFrm.Page1.oPag.oIFTRIEL1_1_7.value==this.w_IFTRIEL1)
      this.oPgFrm.Page1.oPag.oIFTRIEL1_1_7.value=this.w_IFTRIEL1
    endif
    if not(this.oPgFrm.Page1.oPag.oIFRATEL1_1_8.value==this.w_IFRATEL1)
      this.oPgFrm.Page1.oPag.oIFRATEL1_1_8.value=this.w_IFRATEL1
    endif
    if not(this.oPgFrm.Page1.oPag.oIFANNEL1_1_9.value==this.w_IFANNEL1)
      this.oPgFrm.Page1.oPag.oIFANNEL1_1_9.value=this.w_IFANNEL1
    endif
    if not(this.oPgFrm.Page1.oPag.oIFIMDEL1_1_10.value==this.w_IFIMDEL1)
      this.oPgFrm.Page1.oPag.oIFIMDEL1_1_10.value=this.w_IFIMDEL1
    endif
    if not(this.oPgFrm.Page1.oPag.oIFIMCEL1_1_11.value==this.w_IFIMCEL1)
      this.oPgFrm.Page1.oPag.oIFIMCEL1_1_11.value=this.w_IFIMCEL1
    endif
    if not(this.oPgFrm.Page1.oPag.oIFRAVVE1_1_12.value==this.w_IFRAVVE1)
      this.oPgFrm.Page1.oPag.oIFRAVVE1_1_12.value=this.w_IFRAVVE1
    endif
    if not(this.oPgFrm.Page1.oPag.oIFIMMOV1_1_13.value==this.w_IFIMMOV1)
      this.oPgFrm.Page1.oPag.oIFIMMOV1_1_13.value=this.w_IFIMMOV1
    endif
    if not(this.oPgFrm.Page1.oPag.oIFACCON1_1_14.value==this.w_IFACCON1)
      this.oPgFrm.Page1.oPag.oIFACCON1_1_14.value=this.w_IFACCON1
    endif
    if not(this.oPgFrm.Page1.oPag.oIFTSALD1_1_15.value==this.w_IFTSALD1)
      this.oPgFrm.Page1.oPag.oIFTSALD1_1_15.value=this.w_IFTSALD1
    endif
    if not(this.oPgFrm.Page1.oPag.oIFNUMFA1_1_16.value==this.w_IFNUMFA1)
      this.oPgFrm.Page1.oPag.oIFNUMFA1_1_16.value=this.w_IFNUMFA1
    endif
    if not(this.oPgFrm.Page1.oPag.oIFCODEL2_1_17.value==this.w_IFCODEL2)
      this.oPgFrm.Page1.oPag.oIFCODEL2_1_17.value=this.w_IFCODEL2
    endif
    if not(this.oPgFrm.Page1.oPag.oIFTRIEL2_1_18.value==this.w_IFTRIEL2)
      this.oPgFrm.Page1.oPag.oIFTRIEL2_1_18.value=this.w_IFTRIEL2
    endif
    if not(this.oPgFrm.Page1.oPag.oIFRATEL2_1_19.value==this.w_IFRATEL2)
      this.oPgFrm.Page1.oPag.oIFRATEL2_1_19.value=this.w_IFRATEL2
    endif
    if not(this.oPgFrm.Page1.oPag.oIFANNEL2_1_20.value==this.w_IFANNEL2)
      this.oPgFrm.Page1.oPag.oIFANNEL2_1_20.value=this.w_IFANNEL2
    endif
    if not(this.oPgFrm.Page1.oPag.oIFIMDEL2_1_21.value==this.w_IFIMDEL2)
      this.oPgFrm.Page1.oPag.oIFIMDEL2_1_21.value=this.w_IFIMDEL2
    endif
    if not(this.oPgFrm.Page1.oPag.oIFIMCEL2_1_22.value==this.w_IFIMCEL2)
      this.oPgFrm.Page1.oPag.oIFIMCEL2_1_22.value=this.w_IFIMCEL2
    endif
    if not(this.oPgFrm.Page1.oPag.oIFRAVVE2_1_23.value==this.w_IFRAVVE2)
      this.oPgFrm.Page1.oPag.oIFRAVVE2_1_23.value=this.w_IFRAVVE2
    endif
    if not(this.oPgFrm.Page1.oPag.oIFIMMOV2_1_24.value==this.w_IFIMMOV2)
      this.oPgFrm.Page1.oPag.oIFIMMOV2_1_24.value=this.w_IFIMMOV2
    endif
    if not(this.oPgFrm.Page1.oPag.oIFACCON2_1_25.value==this.w_IFACCON2)
      this.oPgFrm.Page1.oPag.oIFACCON2_1_25.value=this.w_IFACCON2
    endif
    if not(this.oPgFrm.Page1.oPag.oIFTSALD2_1_26.value==this.w_IFTSALD2)
      this.oPgFrm.Page1.oPag.oIFTSALD2_1_26.value=this.w_IFTSALD2
    endif
    if not(this.oPgFrm.Page1.oPag.oIFNUMFA2_1_27.value==this.w_IFNUMFA2)
      this.oPgFrm.Page1.oPag.oIFNUMFA2_1_27.value=this.w_IFNUMFA2
    endif
    if not(this.oPgFrm.Page1.oPag.oIFCODEL3_1_28.value==this.w_IFCODEL3)
      this.oPgFrm.Page1.oPag.oIFCODEL3_1_28.value=this.w_IFCODEL3
    endif
    if not(this.oPgFrm.Page1.oPag.oIFTRIEL3_1_29.value==this.w_IFTRIEL3)
      this.oPgFrm.Page1.oPag.oIFTRIEL3_1_29.value=this.w_IFTRIEL3
    endif
    if not(this.oPgFrm.Page1.oPag.oIFRATEL3_1_30.value==this.w_IFRATEL3)
      this.oPgFrm.Page1.oPag.oIFRATEL3_1_30.value=this.w_IFRATEL3
    endif
    if not(this.oPgFrm.Page1.oPag.oIFANNEL3_1_31.value==this.w_IFANNEL3)
      this.oPgFrm.Page1.oPag.oIFANNEL3_1_31.value=this.w_IFANNEL3
    endif
    if not(this.oPgFrm.Page1.oPag.oIFIMDEL3_1_32.value==this.w_IFIMDEL3)
      this.oPgFrm.Page1.oPag.oIFIMDEL3_1_32.value=this.w_IFIMDEL3
    endif
    if not(this.oPgFrm.Page1.oPag.oIFIMCEL3_1_33.value==this.w_IFIMCEL3)
      this.oPgFrm.Page1.oPag.oIFIMCEL3_1_33.value=this.w_IFIMCEL3
    endif
    if not(this.oPgFrm.Page1.oPag.oIFRAVVE3_1_34.value==this.w_IFRAVVE3)
      this.oPgFrm.Page1.oPag.oIFRAVVE3_1_34.value=this.w_IFRAVVE3
    endif
    if not(this.oPgFrm.Page1.oPag.oIFIMMOV3_1_35.value==this.w_IFIMMOV3)
      this.oPgFrm.Page1.oPag.oIFIMMOV3_1_35.value=this.w_IFIMMOV3
    endif
    if not(this.oPgFrm.Page1.oPag.oIFACCON3_1_36.value==this.w_IFACCON3)
      this.oPgFrm.Page1.oPag.oIFACCON3_1_36.value=this.w_IFACCON3
    endif
    if not(this.oPgFrm.Page1.oPag.oIFTSALD3_1_37.value==this.w_IFTSALD3)
      this.oPgFrm.Page1.oPag.oIFTSALD3_1_37.value=this.w_IFTSALD3
    endif
    if not(this.oPgFrm.Page1.oPag.oIFNUMFA3_1_38.value==this.w_IFNUMFA3)
      this.oPgFrm.Page1.oPag.oIFNUMFA3_1_38.value=this.w_IFNUMFA3
    endif
    if not(this.oPgFrm.Page1.oPag.oIFCODEL4_1_39.value==this.w_IFCODEL4)
      this.oPgFrm.Page1.oPag.oIFCODEL4_1_39.value=this.w_IFCODEL4
    endif
    if not(this.oPgFrm.Page1.oPag.oIFTRIEL4_1_40.value==this.w_IFTRIEL4)
      this.oPgFrm.Page1.oPag.oIFTRIEL4_1_40.value=this.w_IFTRIEL4
    endif
    if not(this.oPgFrm.Page1.oPag.oIFRATEL4_1_41.value==this.w_IFRATEL4)
      this.oPgFrm.Page1.oPag.oIFRATEL4_1_41.value=this.w_IFRATEL4
    endif
    if not(this.oPgFrm.Page1.oPag.oIFANNEL4_1_42.value==this.w_IFANNEL4)
      this.oPgFrm.Page1.oPag.oIFANNEL4_1_42.value=this.w_IFANNEL4
    endif
    if not(this.oPgFrm.Page1.oPag.oIFIMDEL4_1_43.value==this.w_IFIMDEL4)
      this.oPgFrm.Page1.oPag.oIFIMDEL4_1_43.value=this.w_IFIMDEL4
    endif
    if not(this.oPgFrm.Page1.oPag.oIFIMCEL4_1_44.value==this.w_IFIMCEL4)
      this.oPgFrm.Page1.oPag.oIFIMCEL4_1_44.value=this.w_IFIMCEL4
    endif
    if not(this.oPgFrm.Page1.oPag.oIFRAVVE4_1_45.value==this.w_IFRAVVE4)
      this.oPgFrm.Page1.oPag.oIFRAVVE4_1_45.value=this.w_IFRAVVE4
    endif
    if not(this.oPgFrm.Page1.oPag.oIFIMMOV4_1_46.value==this.w_IFIMMOV4)
      this.oPgFrm.Page1.oPag.oIFIMMOV4_1_46.value=this.w_IFIMMOV4
    endif
    if not(this.oPgFrm.Page1.oPag.oIFACCON4_1_47.value==this.w_IFACCON4)
      this.oPgFrm.Page1.oPag.oIFACCON4_1_47.value=this.w_IFACCON4
    endif
    if not(this.oPgFrm.Page1.oPag.oIFTSALD4_1_48.value==this.w_IFTSALD4)
      this.oPgFrm.Page1.oPag.oIFTSALD4_1_48.value=this.w_IFTSALD4
    endif
    if not(this.oPgFrm.Page1.oPag.oIFNUMFA4_1_49.value==this.w_IFNUMFA4)
      this.oPgFrm.Page1.oPag.oIFNUMFA4_1_49.value=this.w_IFNUMFA4
    endif
    if not(this.oPgFrm.Page1.oPag.oIFDETICI_1_50.value==this.w_IFDETICI)
      this.oPgFrm.Page1.oPag.oIFDETICI_1_50.value=this.w_IFDETICI
    endif
    if not(this.oPgFrm.Page1.oPag.oIFTOTDEL_1_51.value==this.w_IFTOTDEL)
      this.oPgFrm.Page1.oPag.oIFTOTDEL_1_51.value=this.w_IFTOTDEL
    endif
    if not(this.oPgFrm.Page1.oPag.oIFTOTCEL_1_52.value==this.w_IFTOTCEL)
      this.oPgFrm.Page1.oPag.oIFTOTCEL_1_52.value=this.w_IFTOTCEL
    endif
    if not(this.oPgFrm.Page1.oPag.oIFSALDEL_1_53.value==this.w_IFSALDEL)
      this.oPgFrm.Page1.oPag.oIFSALDEL_1_53.value=this.w_IFSALDEL
    endif
    cp_SetControlsValueExtFlds(this,'MODIPAG')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_IFTRIEL1)) or not(.w_TIPTRI = 'Enti local' or .w_TIPTRI = 'ICI' or .w_TIPTRI = 'Altri trib' or empty(.w_TIPTRI)))  and (not empty(.w_IFCODEL1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIFTRIEL1_1_7.SetFocus()
            i_bnoObbl = !empty(.w_IFTRIEL1)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezionare un codice tributo di tipo 'ICI' o 'enti local'O 'altri trib'")
          case   not(empty(.w_IFRATEL1) or left(.w_IFRATEL1,2)<=right(.w_IFRATEL1,2) and len(alltrim(.w_IFRATEL1))=4)  and (not empty(.w_IFCODEL1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIFRATEL1_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione enti locali: numero rata o totale rate errato")
          case   not(!empty(.w_IFANNEL1) and (val(.w_IFANNEL1)>=1996  and val(.w_IFANNEL1)<=2050 or val(.w_IFANNEL1)=0))  and (not empty(.w_IFCODEL1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIFANNEL1_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione enti locali: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>")
          case   not(.w_IFRAVVE1=0 OR .w_IFRAVVE1=1)  and (not empty(.w_IFCODEL1) AND INLIST (.w_IFTRIEL1,'3901','3902','3903','3904','3905','3906','3907'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIFRAVVE1_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire solo i valori '0' oppure '1'")
          case   not(.w_IFIMMOV1=0 or .w_IFIMMOV1=1)  and (not empty(.w_IFCODEL1) AND INLIST (.w_IFTRIEL1,'3901','3902','3903','3904','3905','3906','3907'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIFIMMOV1_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire solo i valori '0' oppure '1'")
          case   not(.w_IFACCON1=0  or .w_IFACCON1=1)  and (not empty(.w_IFCODEL1) AND INLIST (.w_IFTRIEL1,'3901','3902','3903','3904','3905','3906','3907'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIFACCON1_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire solo i valori '0' oppure '1'")
          case   not(.w_IFTSALD1=0 or .w_IFTSALD1=1)  and (not empty(.w_IFCODEL1) AND INLIST (.w_IFTRIEL1,'3901','3902','3903','3904','3905','3906','3907'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIFTSALD1_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire solo i valori '0' oppure '1'")
          case   ((empty(.w_IFTRIEL2)) or not(.w_TIPTRI = 'Enti local' or .w_TIPTRI = 'ICI' or .w_TIPTRI = 'Altri trib' or empty(.w_TIPTRI)))  and (not empty(.w_IFCODEL2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIFTRIEL2_1_18.SetFocus()
            i_bnoObbl = !empty(.w_IFTRIEL2)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezionare un codice tributo di tipo 'ICI' o 'enti local'O 'altri trib'")
          case   not(empty(.w_IFRATEL2) or left(.w_IFRATEL2,2)<=right(.w_IFRATEL2,2) and len(alltrim(.w_IFRATEL2))=4)  and (not empty(.w_IFCODEL2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIFRATEL2_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione enti locali: numero rata o totale rate errato")
          case   not(!empty(.w_IFANNEL2) and (val(.w_IFANNEL2)>=1996  and val(.w_IFANNEL2)<=2050  or val(.w_IFANNEL2)=0))  and (not empty(.w_IFCODEL2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIFANNEL2_1_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione enti locali: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>")
          case   not(.w_IFRAVVE2=0 OR .w_IFRAVVE2=1)  and (not empty(.w_IFCODEL2) AND INLIST (.w_IFTRIEL2,'3901','3902','3903','3904','3905','3906','3907'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIFRAVVE2_1_23.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire solo i valori '0' oppure '1'")
          case   not(.w_IFIMMOV2=0 or .w_IFIMMOV2=1)  and (not empty(.w_IFCODEL2) AND INLIST (.w_IFTRIEL2,'3901','3902','3903','3904','3905','3906','3907'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIFIMMOV2_1_24.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire solo i valori '0' oppure '1'")
          case   not(.w_IFACCON2=0  or .w_IFACCON2=1)  and (not empty(.w_IFCODEL2) AND INLIST (.w_IFTRIEL2,'3901','3902','3903','3904','3905','3906','3907'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIFACCON2_1_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire solo i valori '0' oppure '1'")
          case   not(.w_IFTSALD2=0 or .w_IFTSALD2=1)  and (not empty(.w_IFCODEL2) AND INLIST (.w_IFTRIEL2,'3901','3902','3903','3904','3905','3906','3907'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIFTSALD2_1_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire solo i valori '0' oppure '1'")
          case   ((empty(.w_IFTRIEL3)) or not(.w_TIPTRI = 'Enti local' or .w_TIPTRI = 'ICI' or .w_TIPTRI = 'Altri trib' or empty(.w_TIPTRI)))  and (not empty(.w_IFCODEL3))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIFTRIEL3_1_29.SetFocus()
            i_bnoObbl = !empty(.w_IFTRIEL3)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezionare un codice tributo di tipo 'ICI' o 'enti local'O 'altri trib'")
          case   not(empty(.w_IFRATEL3) or left(.w_IFRATEL3,2)<=right(.w_IFRATEL3,2) and len(alltrim(.w_IFRATEL3))=4)  and (not empty(.w_IFCODEL3))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIFRATEL3_1_30.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione enti locali: numero rata o totale rate errato")
          case   not(!empty(.w_IFANNEL3) and (val(.w_IFANNEL3)>=1996  and val(.w_IFANNEL3)<=2050  or val(.w_IFANNEL3)=0))  and (not empty(.w_IFCODEL3))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIFANNEL3_1_31.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione enti locali: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>")
          case   not(.w_IFRAVVE3=0 OR .w_IFRAVVE3=1)  and (not empty(.w_IFCODEL3) AND INLIST (.w_IFTRIEL3,'3901','3902','3903','3904','3905','3906','3907'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIFRAVVE3_1_34.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire solo i valori '0' oppure '1'")
          case   not(.w_IFIMMOV3=0 or .w_IFIMMOV3=1)  and (not empty(.w_IFCODEL3) AND INLIST (.w_IFTRIEL3,'3901','3902','3903','3904','3905','3906','3907'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIFIMMOV3_1_35.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire solo i valori '0' oppure '1'")
          case   not(.w_IFACCON3=0  or .w_IFACCON3=1)  and (not empty(.w_IFCODEL3) AND INLIST (.w_IFTRIEL3,'3901','3902','3903','3904','3905','3906','3907'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIFACCON3_1_36.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire solo i valori '0' oppure '1'")
          case   not(.w_IFTSALD3=0 or .w_IFTSALD3=1)  and (not empty(.w_IFCODEL3) AND INLIST (.w_IFTRIEL3,'3901','3902','3903','3904','3905','3906','3907'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIFTSALD3_1_37.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire solo i valori '0' oppure '1'")
          case   ((empty(.w_IFTRIEL4)) or not(.w_TIPTRI = 'Enti local' or .w_TIPTRI = 'ICI' or .w_TIPTRI = 'Altri trib' or empty(.w_TIPTRI)))  and (not empty(.w_IFCODEL4))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIFTRIEL4_1_40.SetFocus()
            i_bnoObbl = !empty(.w_IFTRIEL4)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezionare un codice tributo di tipo 'ICI' o 'enti local'O 'altri trib'")
          case   not(empty(.w_IFRATEL4) or left(.w_IFRATEL4,2)<=right(.w_IFRATEL4,2) and len(alltrim(.w_IFRATEL4))=4)  and (not empty(.w_IFCODEL4))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIFRATEL4_1_41.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione enti locali: numero rata o totale rate errato")
          case   not(!empty(.w_IFANNEL4) and (val(.w_IFANNEL4)>=1996  and val(.w_IFANNEL4)<=2050  or val(.w_IFANNEL4)=0))  and (not empty(.w_IFCODEL4))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIFANNEL4_1_42.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione enti locali: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>")
          case   not(.w_IFRAVVE4=0 OR .w_IFRAVVE4=1)  and (not empty(.w_IFCODEL4) AND INLIST (.w_IFTRIEL4,'3901','3902','3903','3904','3905','3906','3907'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIFRAVVE4_1_45.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire solo i valori '0' oppure '1'")
          case   not(.w_IFIMMOV4=0 or .w_IFIMMOV4=1)  and (not empty(.w_IFCODEL4) AND INLIST (.w_IFTRIEL4,'3901','3902','3903','3904','3905','3906','3907'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIFIMMOV4_1_46.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire solo i valori '0' oppure '1'")
          case   not(.w_IFACCON4=0  or .w_IFACCON4=1)  and (not empty(.w_IFCODEL4) AND INLIST (.w_IFTRIEL4,'3901','3902','3903','3904','3905','3906','3907'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIFACCON4_1_47.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire solo i valori '0' oppure '1'")
          case   not(.w_IFTSALD4=0 or .w_IFTSALD4=1)  and (not empty(.w_IFCODEL4) AND INLIST (.w_IFTRIEL4,'3901','3902','3903','3904','3905','3906','3907'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIFTSALD4_1_48.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire solo i valori '0' oppure '1'")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gscg_aif
      .NotifyEvent('ControllaDati')
      
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_IFCODEL1 = this.w_IFCODEL1
    this.o_IFTRIEL1 = this.w_IFTRIEL1
    this.o_IFCODEL2 = this.w_IFCODEL2
    this.o_IFTRIEL2 = this.w_IFTRIEL2
    this.o_IFCODEL3 = this.w_IFCODEL3
    this.o_IFTRIEL3 = this.w_IFTRIEL3
    this.o_IFCODEL4 = this.w_IFCODEL4
    this.o_IFTRIEL4 = this.w_IFTRIEL4
    return

enddefine

* --- Define pages as container
define class tgscg_aifPag1 as StdContainer
  Width  = 780
  height = 200
  stdWidth  = 780
  stdheight = 200
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oIFCODEL1_1_6 as StdField with uid="OWHRAGUTTI",rtseq=1,rtrep=.f.,;
    cFormVar = "w_IFCODEL1", cQueryName = "IFCODEL1",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Codice ente / comune",;
    HelpContextID = 116828745,;
   bGlobalFont=.t.,;
    Height=21, Width=56, Left=21, Top=69, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ENTI_COM", cZoomOnZoom="GSCG_AEC", oKey_1_1="ECCODICE", oKey_1_2="this.w_IFCODEL1"

  func oIFCODEL1_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oIFCODEL1_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIFCODEL1_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ENTI_COM','*','ECCODICE',cp_AbsName(this.parent,'oIFCODEL1_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_AEC',"Codici enti / comuni",'',this.parent.oContained
  endproc
  proc oIFCODEL1_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSCG_AEC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ECCODICE=this.parent.oContained.w_IFCODEL1
     i_obj.ecpSave()
  endproc

  add object oIFTRIEL1_1_7 as StdField with uid="YWULUYCAVU",rtseq=2,rtrep=.f.,;
    cFormVar = "w_IFTRIEL1", cQueryName = "IFTRIEL1",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Selezionare un codice tributo di tipo 'ICI' o 'enti local'O 'altri trib'",;
    ToolTipText = "Codice tributo",;
    HelpContextID = 111319625,;
   bGlobalFont=.t.,;
    Height=21, Width=47, Left=111, Top=70, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_TRIB", cZoomOnZoom="GSCG_ATR", oKey_1_1="TRCODICE", oKey_1_2="this.w_IFTRIEL1"

  func oIFTRIEL1_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_IFCODEL1))
    endwith
   endif
  endfunc

  func oIFTRIEL1_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oIFTRIEL1_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIFTRIEL1_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_TRIB','*','TRCODICE',cp_AbsName(this.parent,'oIFTRIEL1_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ATR',"Codici tributo",'GSCG2ZTR.COD_TRIB_VZM',this.parent.oContained
  endproc
  proc oIFTRIEL1_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_IFTRIEL1
     i_obj.ecpSave()
  endproc

  add object oIFRATEL1_1_8 as StdField with uid="CINHCWXKTX",rtseq=3,rtrep=.f.,;
    cFormVar = "w_IFRATEL1", cQueryName = "IFRATEL1",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione enti locali: numero rata o totale rate errato",;
    ToolTipText = "Numero rata (nn) + totale rate (rr)",;
    HelpContextID = 100907593,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=165, Top=70, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oIFRATEL1_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_IFCODEL1))
    endwith
   endif
  endfunc

  func oIFRATEL1_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_IFRATEL1) or left(.w_IFRATEL1,2)<=right(.w_IFRATEL1,2) and len(alltrim(.w_IFRATEL1))=4)
    endwith
    return bRes
  endfunc

  add object oIFANNEL1_1_9 as StdField with uid="EUVLCLSTWA",rtseq=4,rtrep=.f.,;
    cFormVar = "w_IFANNEL1", cQueryName = "IFANNEL1",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione enti locali: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>",;
    ToolTipText = "Anno di riferimento (aaaa)",;
    HelpContextID = 106416713,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=218, Top=70, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oIFANNEL1_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_IFCODEL1))
    endwith
   endif
  endfunc

  func oIFANNEL1_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_IFANNEL1) and (val(.w_IFANNEL1)>=1996  and val(.w_IFANNEL1)<=2050 or val(.w_IFANNEL1)=0))
    endwith
    return bRes
  endfunc

  add object oIFIMDEL1_1_10 as StdField with uid="XKUTIFTYDZ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_IFIMDEL1", cQueryName = "IFIMDEL1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 116935241,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=277, Top=70, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oIFIMDEL1_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_IFCODEL1))
    endwith
   endif
  endfunc

  add object oIFIMCEL1_1_11 as StdField with uid="XKVQMDBRXD",rtseq=6,rtrep=.f.,;
    cFormVar = "w_IFIMCEL1", cQueryName = "IFIMCEL1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 117983817,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=432, Top=70, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oIFIMCEL1_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_IFCODEL1))
    endwith
   endif
  endfunc

  add object oIFRAVVE1_1_12 as StdField with uid="TSGXIBECVS",rtseq=7,rtrep=.f.,;
    cFormVar = "w_IFRAVVE1", cQueryName = "IFRAVVE1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire solo i valori '0' oppure '1'",;
    ToolTipText = "Ravvedimento",;
    HelpContextID = 186402231,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=571, Top=70, cSayPict='"9"', cGetPict='"9"'

  func oIFRAVVE1_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_IFCODEL1) AND INLIST (.w_IFTRIEL1,'3901','3902','3903','3904','3905','3906','3907'))
    endwith
   endif
  endfunc

  func oIFRAVVE1_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_IFRAVVE1=0 OR .w_IFRAVVE1=1)
    endwith
    return bRes
  endfunc

  add object oIFIMMOV1_1_13 as StdField with uid="OTGOZGZYPJ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_IFIMMOV1", cQueryName = "IFIMMOV1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire solo i valori '0' oppure '1'",;
    ToolTipText = "Immobili variati",;
    HelpContextID = 208161353,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=618, Top=70, cSayPict='"9"', cGetPict='"9"'

  func oIFIMMOV1_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_IFCODEL1) AND INLIST (.w_IFTRIEL1,'3901','3902','3903','3904','3905','3906','3907'))
    endwith
   endif
  endfunc

  func oIFIMMOV1_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_IFIMMOV1=0 or .w_IFIMMOV1=1)
    endwith
    return bRes
  endfunc

  add object oIFACCON1_1_14 as StdField with uid="YQCANCKIUT",rtseq=9,rtrep=.f.,;
    cFormVar = "w_IFACCON1", cQueryName = "IFACCON1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire solo i valori '0' oppure '1'",;
    ToolTipText = "Acconto",;
    HelpContextID = 49100215,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=668, Top=70, cSayPict='"9"', cGetPict='"9"'

  func oIFACCON1_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_IFCODEL1) AND INLIST (.w_IFTRIEL1,'3901','3902','3903','3904','3905','3906','3907'))
    endwith
   endif
  endfunc

  func oIFACCON1_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_IFACCON1=0  or .w_IFACCON1=1)
    endwith
    return bRes
  endfunc

  add object oIFTSALD1_1_15 as StdField with uid="GFITCADUTW",rtseq=10,rtrep=.f.,;
    cFormVar = "w_IFTSALD1", cQueryName = "IFTSALD1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire solo i valori '0' oppure '1'",;
    ToolTipText = "Saldo",;
    HelpContextID = 266233271,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=701, Top=70, cSayPict='"9"', cGetPict='"9"'

  func oIFTSALD1_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_IFCODEL1) AND INLIST (.w_IFTRIEL1,'3901','3902','3903','3904','3905','3906','3907'))
    endwith
   endif
  endfunc

  func oIFTSALD1_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_IFTSALD1=0 or .w_IFTSALD1=1)
    endwith
    return bRes
  endfunc

  add object oIFNUMFA1_1_16 as StdField with uid="ITSKBHLVZR",rtseq=11,rtrep=.f.,;
    cFormVar = "w_IFNUMFA1", cQueryName = "IFNUMFA1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero fabbricati",;
    HelpContextID = 178259383,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=733, Top=70, cSayPict='"999"', cGetPict='"999"'

  func oIFNUMFA1_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_IFCODEL1) AND INLIST (.w_IFTRIEL1,'3901','3902','3903','3904','3905','3906','3907'))
    endwith
   endif
  endfunc

  add object oIFCODEL2_1_17 as StdField with uid="PTOXXPRYPG",rtseq=12,rtrep=.f.,;
    cFormVar = "w_IFCODEL2", cQueryName = "IFCODEL2",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Codice ente / comune",;
    HelpContextID = 116828744,;
   bGlobalFont=.t.,;
    Height=21, Width=56, Left=21, Top=89, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ENTI_COM", cZoomOnZoom="GSCG_AEC", oKey_1_1="ECCODICE", oKey_1_2="this.w_IFCODEL2"

  func oIFCODEL2_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oIFCODEL2_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIFCODEL2_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ENTI_COM','*','ECCODICE',cp_AbsName(this.parent,'oIFCODEL2_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_AEC',"Codici enti / comuni",'',this.parent.oContained
  endproc
  proc oIFCODEL2_1_17.mZoomOnZoom
    local i_obj
    i_obj=GSCG_AEC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ECCODICE=this.parent.oContained.w_IFCODEL2
     i_obj.ecpSave()
  endproc

  add object oIFTRIEL2_1_18 as StdField with uid="CWWCMTJXHJ",rtseq=13,rtrep=.f.,;
    cFormVar = "w_IFTRIEL2", cQueryName = "IFTRIEL2",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Selezionare un codice tributo di tipo 'ICI' o 'enti local'O 'altri trib'",;
    ToolTipText = "Codice tributo",;
    HelpContextID = 111319624,;
   bGlobalFont=.t.,;
    Height=21, Width=47, Left=111, Top=90, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_TRIB", cZoomOnZoom="GSCG_ATR", oKey_1_1="TRCODICE", oKey_1_2="this.w_IFTRIEL2"

  func oIFTRIEL2_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_IFCODEL2))
    endwith
   endif
  endfunc

  func oIFTRIEL2_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oIFTRIEL2_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIFTRIEL2_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_TRIB','*','TRCODICE',cp_AbsName(this.parent,'oIFTRIEL2_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ATR',"Codici tributo",'GSCG2ZTR.COD_TRIB_VZM',this.parent.oContained
  endproc
  proc oIFTRIEL2_1_18.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_IFTRIEL2
     i_obj.ecpSave()
  endproc

  add object oIFRATEL2_1_19 as StdField with uid="YQTOAEZSKO",rtseq=14,rtrep=.f.,;
    cFormVar = "w_IFRATEL2", cQueryName = "IFRATEL2",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione enti locali: numero rata o totale rate errato",;
    ToolTipText = "Numero rata (nn) + totale rate (rr)",;
    HelpContextID = 100907592,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=165, Top=90, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oIFRATEL2_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_IFCODEL2))
    endwith
   endif
  endfunc

  func oIFRATEL2_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_IFRATEL2) or left(.w_IFRATEL2,2)<=right(.w_IFRATEL2,2) and len(alltrim(.w_IFRATEL2))=4)
    endwith
    return bRes
  endfunc

  add object oIFANNEL2_1_20 as StdField with uid="XQNJZHRXSR",rtseq=15,rtrep=.f.,;
    cFormVar = "w_IFANNEL2", cQueryName = "IFANNEL2",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione enti locali: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>",;
    ToolTipText = "Anno di riferimento (aaaa)",;
    HelpContextID = 106416712,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=218, Top=90, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oIFANNEL2_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_IFCODEL2))
    endwith
   endif
  endfunc

  func oIFANNEL2_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_IFANNEL2) and (val(.w_IFANNEL2)>=1996  and val(.w_IFANNEL2)<=2050  or val(.w_IFANNEL2)=0))
    endwith
    return bRes
  endfunc

  add object oIFIMDEL2_1_21 as StdField with uid="JXREHKAWZX",rtseq=16,rtrep=.f.,;
    cFormVar = "w_IFIMDEL2", cQueryName = "IFIMDEL2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 116935240,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=277, Top=90, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oIFIMDEL2_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_IFCODEL2))
    endwith
   endif
  endfunc

  add object oIFIMCEL2_1_22 as StdField with uid="KUOXMSJXNF",rtseq=17,rtrep=.f.,;
    cFormVar = "w_IFIMCEL2", cQueryName = "IFIMCEL2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 117983816,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=432, Top=90, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oIFIMCEL2_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_IFCODEL2))
    endwith
   endif
  endfunc

  add object oIFRAVVE2_1_23 as StdField with uid="KSQEOWTSTW",rtseq=18,rtrep=.f.,;
    cFormVar = "w_IFRAVVE2", cQueryName = "IFRAVVE2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire solo i valori '0' oppure '1'",;
    ToolTipText = "Ravvedimento",;
    HelpContextID = 186402232,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=571, Top=90, cSayPict='"9"', cGetPict='"9"'

  func oIFRAVVE2_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_IFCODEL2) AND INLIST (.w_IFTRIEL2,'3901','3902','3903','3904','3905','3906','3907'))
    endwith
   endif
  endfunc

  func oIFRAVVE2_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_IFRAVVE2=0 OR .w_IFRAVVE2=1)
    endwith
    return bRes
  endfunc

  add object oIFIMMOV2_1_24 as StdField with uid="UUWGBNFUIW",rtseq=19,rtrep=.f.,;
    cFormVar = "w_IFIMMOV2", cQueryName = "IFIMMOV2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire solo i valori '0' oppure '1'",;
    ToolTipText = "Immobili variati",;
    HelpContextID = 208161352,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=618, Top=90, cSayPict='"9"', cGetPict='"9"'

  func oIFIMMOV2_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_IFCODEL2) AND INLIST (.w_IFTRIEL2,'3901','3902','3903','3904','3905','3906','3907'))
    endwith
   endif
  endfunc

  func oIFIMMOV2_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_IFIMMOV2=0 or .w_IFIMMOV2=1)
    endwith
    return bRes
  endfunc

  add object oIFACCON2_1_25 as StdField with uid="ERLGEMDMWG",rtseq=20,rtrep=.f.,;
    cFormVar = "w_IFACCON2", cQueryName = "IFACCON2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire solo i valori '0' oppure '1'",;
    ToolTipText = "Acconto",;
    HelpContextID = 49100216,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=668, Top=90, cSayPict='"9"', cGetPict='"9"'

  func oIFACCON2_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_IFCODEL2) AND INLIST (.w_IFTRIEL2,'3901','3902','3903','3904','3905','3906','3907'))
    endwith
   endif
  endfunc

  func oIFACCON2_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_IFACCON2=0  or .w_IFACCON2=1)
    endwith
    return bRes
  endfunc

  add object oIFTSALD2_1_26 as StdField with uid="RHAZXXAQUR",rtseq=21,rtrep=.f.,;
    cFormVar = "w_IFTSALD2", cQueryName = "IFTSALD2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire solo i valori '0' oppure '1'",;
    ToolTipText = "Saldo",;
    HelpContextID = 266233272,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=701, Top=90, cSayPict='"9"', cGetPict='"9"'

  func oIFTSALD2_1_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_IFCODEL2) AND INLIST (.w_IFTRIEL2,'3901','3902','3903','3904','3905','3906','3907'))
    endwith
   endif
  endfunc

  func oIFTSALD2_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_IFTSALD2=0 or .w_IFTSALD2=1)
    endwith
    return bRes
  endfunc

  add object oIFNUMFA2_1_27 as StdField with uid="LFIGBBZPKI",rtseq=22,rtrep=.f.,;
    cFormVar = "w_IFNUMFA2", cQueryName = "IFNUMFA2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero fabbricati",;
    HelpContextID = 178259384,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=733, Top=90, cSayPict='"999"', cGetPict='"999"'

  func oIFNUMFA2_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_IFCODEL2) AND INLIST (.w_IFTRIEL2,'3901','3902','3903','3904','3905','3906','3907'))
    endwith
   endif
  endfunc

  add object oIFCODEL3_1_28 as StdField with uid="ONWMPBUXNL",rtseq=23,rtrep=.f.,;
    cFormVar = "w_IFCODEL3", cQueryName = "IFCODEL3",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Codice ente / comune",;
    HelpContextID = 116828743,;
   bGlobalFont=.t.,;
    Height=21, Width=56, Left=21, Top=109, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ENTI_COM", cZoomOnZoom="GSCG_AEC", oKey_1_1="ECCODICE", oKey_1_2="this.w_IFCODEL3"

  func oIFCODEL3_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_28('Part',this)
    endwith
    return bRes
  endfunc

  proc oIFCODEL3_1_28.ecpDrop(oSource)
    this.Parent.oContained.link_1_28('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIFCODEL3_1_28.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ENTI_COM','*','ECCODICE',cp_AbsName(this.parent,'oIFCODEL3_1_28'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_AEC',"Codici enti / comuni",'',this.parent.oContained
  endproc
  proc oIFCODEL3_1_28.mZoomOnZoom
    local i_obj
    i_obj=GSCG_AEC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ECCODICE=this.parent.oContained.w_IFCODEL3
     i_obj.ecpSave()
  endproc

  add object oIFTRIEL3_1_29 as StdField with uid="EZLDQOIGDH",rtseq=24,rtrep=.f.,;
    cFormVar = "w_IFTRIEL3", cQueryName = "IFTRIEL3",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Selezionare un codice tributo di tipo 'ICI' o 'enti local'O 'altri trib'",;
    ToolTipText = "Codice tributo",;
    HelpContextID = 111319623,;
   bGlobalFont=.t.,;
    Height=21, Width=47, Left=111, Top=110, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_TRIB", cZoomOnZoom="GSCG_ATR", oKey_1_1="TRCODICE", oKey_1_2="this.w_IFTRIEL3"

  func oIFTRIEL3_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_IFCODEL3))
    endwith
   endif
  endfunc

  func oIFTRIEL3_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_29('Part',this)
    endwith
    return bRes
  endfunc

  proc oIFTRIEL3_1_29.ecpDrop(oSource)
    this.Parent.oContained.link_1_29('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIFTRIEL3_1_29.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_TRIB','*','TRCODICE',cp_AbsName(this.parent,'oIFTRIEL3_1_29'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ATR',"Codici tributo",'GSCG2ZTR.COD_TRIB_VZM',this.parent.oContained
  endproc
  proc oIFTRIEL3_1_29.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_IFTRIEL3
     i_obj.ecpSave()
  endproc

  add object oIFRATEL3_1_30 as StdField with uid="TMOXVNBKKN",rtseq=25,rtrep=.f.,;
    cFormVar = "w_IFRATEL3", cQueryName = "IFRATEL3",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione enti locali: numero rata o totale rate errato",;
    ToolTipText = "Numero rata (nn) + totale rate (rr)",;
    HelpContextID = 100907591,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=165, Top=110, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oIFRATEL3_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_IFCODEL3))
    endwith
   endif
  endfunc

  func oIFRATEL3_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_IFRATEL3) or left(.w_IFRATEL3,2)<=right(.w_IFRATEL3,2) and len(alltrim(.w_IFRATEL3))=4)
    endwith
    return bRes
  endfunc

  add object oIFANNEL3_1_31 as StdField with uid="SZXNAMQCGT",rtseq=26,rtrep=.f.,;
    cFormVar = "w_IFANNEL3", cQueryName = "IFANNEL3",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione enti locali: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>",;
    ToolTipText = "Anno di riferimento (aaaa)",;
    HelpContextID = 106416711,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=218, Top=110, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oIFANNEL3_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_IFCODEL3))
    endwith
   endif
  endfunc

  func oIFANNEL3_1_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_IFANNEL3) and (val(.w_IFANNEL3)>=1996  and val(.w_IFANNEL3)<=2050  or val(.w_IFANNEL3)=0))
    endwith
    return bRes
  endfunc

  add object oIFIMDEL3_1_32 as StdField with uid="GPEJSBGZGU",rtseq=27,rtrep=.f.,;
    cFormVar = "w_IFIMDEL3", cQueryName = "IFIMDEL3",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 116935239,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=277, Top=110, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oIFIMDEL3_1_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_IFCODEL3))
    endwith
   endif
  endfunc

  add object oIFIMCEL3_1_33 as StdField with uid="ARMYKADZBN",rtseq=28,rtrep=.f.,;
    cFormVar = "w_IFIMCEL3", cQueryName = "IFIMCEL3",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Imp. a credito",;
    HelpContextID = 117983815,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=432, Top=110, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oIFIMCEL3_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_IFCODEL3))
    endwith
   endif
  endfunc

  add object oIFRAVVE3_1_34 as StdField with uid="TUPPIQRWBQ",rtseq=29,rtrep=.f.,;
    cFormVar = "w_IFRAVVE3", cQueryName = "IFRAVVE3",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire solo i valori '0' oppure '1'",;
    ToolTipText = "Ravvedimento",;
    HelpContextID = 186402233,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=571, Top=110, cSayPict='"9"', cGetPict='"9"'

  func oIFRAVVE3_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_IFCODEL3) AND INLIST (.w_IFTRIEL3,'3901','3902','3903','3904','3905','3906','3907'))
    endwith
   endif
  endfunc

  func oIFRAVVE3_1_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_IFRAVVE3=0 OR .w_IFRAVVE3=1)
    endwith
    return bRes
  endfunc

  add object oIFIMMOV3_1_35 as StdField with uid="DOSENMDHES",rtseq=30,rtrep=.f.,;
    cFormVar = "w_IFIMMOV3", cQueryName = "IFIMMOV3",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire solo i valori '0' oppure '1'",;
    ToolTipText = "Immobili variati",;
    HelpContextID = 208161351,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=618, Top=110, cSayPict='"9"', cGetPict='"9"'

  func oIFIMMOV3_1_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_IFCODEL3) AND INLIST (.w_IFTRIEL3,'3901','3902','3903','3904','3905','3906','3907'))
    endwith
   endif
  endfunc

  func oIFIMMOV3_1_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_IFIMMOV3=0 or .w_IFIMMOV3=1)
    endwith
    return bRes
  endfunc

  add object oIFACCON3_1_36 as StdField with uid="OUQEVREZLF",rtseq=31,rtrep=.f.,;
    cFormVar = "w_IFACCON3", cQueryName = "IFACCON3",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire solo i valori '0' oppure '1'",;
    ToolTipText = "Acconto",;
    HelpContextID = 49100217,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=668, Top=110, cSayPict='"9"', cGetPict='"9"'

  func oIFACCON3_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_IFCODEL3) AND INLIST (.w_IFTRIEL3,'3901','3902','3903','3904','3905','3906','3907'))
    endwith
   endif
  endfunc

  func oIFACCON3_1_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_IFACCON3=0  or .w_IFACCON3=1)
    endwith
    return bRes
  endfunc

  add object oIFTSALD3_1_37 as StdField with uid="TPDBBXONGP",rtseq=32,rtrep=.f.,;
    cFormVar = "w_IFTSALD3", cQueryName = "IFTSALD3",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire solo i valori '0' oppure '1'",;
    ToolTipText = "Saldo",;
    HelpContextID = 266233273,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=701, Top=110, cSayPict='"9"', cGetPict='"9"'

  func oIFTSALD3_1_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_IFCODEL3) AND INLIST (.w_IFTRIEL3,'3901','3902','3903','3904','3905','3906','3907'))
    endwith
   endif
  endfunc

  func oIFTSALD3_1_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_IFTSALD3=0 or .w_IFTSALD3=1)
    endwith
    return bRes
  endfunc

  add object oIFNUMFA3_1_38 as StdField with uid="VAUOPKSZMS",rtseq=33,rtrep=.f.,;
    cFormVar = "w_IFNUMFA3", cQueryName = "IFNUMFA3",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero fabbricati",;
    HelpContextID = 178259385,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=733, Top=110, cSayPict='"999"', cGetPict='"999"'

  func oIFNUMFA3_1_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_IFCODEL3) AND INLIST (.w_IFTRIEL3,'3901','3902','3903','3904','3905','3906','3907'))
    endwith
   endif
  endfunc

  add object oIFCODEL4_1_39 as StdField with uid="IVFEAWQIFD",rtseq=34,rtrep=.f.,;
    cFormVar = "w_IFCODEL4", cQueryName = "IFCODEL4",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Codice ente / comune",;
    HelpContextID = 116828742,;
   bGlobalFont=.t.,;
    Height=21, Width=56, Left=21, Top=129, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ENTI_COM", cZoomOnZoom="GSCG_AEC", oKey_1_1="ECCODICE", oKey_1_2="this.w_IFCODEL4"

  func oIFCODEL4_1_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_39('Part',this)
    endwith
    return bRes
  endfunc

  proc oIFCODEL4_1_39.ecpDrop(oSource)
    this.Parent.oContained.link_1_39('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIFCODEL4_1_39.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ENTI_COM','*','ECCODICE',cp_AbsName(this.parent,'oIFCODEL4_1_39'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_AEC',"Codici enti / comuni",'',this.parent.oContained
  endproc
  proc oIFCODEL4_1_39.mZoomOnZoom
    local i_obj
    i_obj=GSCG_AEC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ECCODICE=this.parent.oContained.w_IFCODEL4
     i_obj.ecpSave()
  endproc

  add object oIFTRIEL4_1_40 as StdField with uid="KMFOHYOGQU",rtseq=35,rtrep=.f.,;
    cFormVar = "w_IFTRIEL4", cQueryName = "IFTRIEL4",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Selezionare un codice tributo di tipo 'ICI' o 'enti local'O 'altri trib'",;
    ToolTipText = "Codice tributo",;
    HelpContextID = 111319622,;
   bGlobalFont=.t.,;
    Height=21, Width=47, Left=111, Top=130, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_TRIB", cZoomOnZoom="GSCG_ATR", oKey_1_1="TRCODICE", oKey_1_2="this.w_IFTRIEL4"

  func oIFTRIEL4_1_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_IFCODEL4))
    endwith
   endif
  endfunc

  func oIFTRIEL4_1_40.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_40('Part',this)
    endwith
    return bRes
  endfunc

  proc oIFTRIEL4_1_40.ecpDrop(oSource)
    this.Parent.oContained.link_1_40('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIFTRIEL4_1_40.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_TRIB','*','TRCODICE',cp_AbsName(this.parent,'oIFTRIEL4_1_40'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ATR',"Codici tributo",'GSCG2ZTR.COD_TRIB_VZM',this.parent.oContained
  endproc
  proc oIFTRIEL4_1_40.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_IFTRIEL4
     i_obj.ecpSave()
  endproc

  add object oIFRATEL4_1_41 as StdField with uid="KCJMYNQUUR",rtseq=36,rtrep=.f.,;
    cFormVar = "w_IFRATEL4", cQueryName = "IFRATEL4",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione enti locali: numero rata o totale rate errato",;
    ToolTipText = "Numero rata (nn) + totale rate (rr)",;
    HelpContextID = 100907590,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=165, Top=130, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oIFRATEL4_1_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_IFCODEL4))
    endwith
   endif
  endfunc

  func oIFRATEL4_1_41.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_IFRATEL4) or left(.w_IFRATEL4,2)<=right(.w_IFRATEL4,2) and len(alltrim(.w_IFRATEL4))=4)
    endwith
    return bRes
  endfunc

  add object oIFANNEL4_1_42 as StdField with uid="BWMXTCBQGQ",rtseq=37,rtrep=.f.,;
    cFormVar = "w_IFANNEL4", cQueryName = "IFANNEL4",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione enti locali: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>",;
    ToolTipText = "Anno di riferimento (aaaa)",;
    HelpContextID = 106416710,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=218, Top=130, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oIFANNEL4_1_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_IFCODEL4))
    endwith
   endif
  endfunc

  func oIFANNEL4_1_42.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_IFANNEL4) and (val(.w_IFANNEL4)>=1996  and val(.w_IFANNEL4)<=2050  or val(.w_IFANNEL4)=0))
    endwith
    return bRes
  endfunc

  add object oIFIMDEL4_1_43 as StdField with uid="RUVDINGVHD",rtseq=38,rtrep=.f.,;
    cFormVar = "w_IFIMDEL4", cQueryName = "IFIMDEL4",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 116935238,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=277, Top=131, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oIFIMDEL4_1_43.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_IFCODEL4))
    endwith
   endif
  endfunc

  add object oIFIMCEL4_1_44 as StdField with uid="GIMNVSHQRD",rtseq=39,rtrep=.f.,;
    cFormVar = "w_IFIMCEL4", cQueryName = "IFIMCEL4",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 117983814,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=432, Top=131, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oIFIMCEL4_1_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_IFCODEL4))
    endwith
   endif
  endfunc

  add object oIFRAVVE4_1_45 as StdField with uid="EGVCNZJWBL",rtseq=40,rtrep=.f.,;
    cFormVar = "w_IFRAVVE4", cQueryName = "IFRAVVE4",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire solo i valori '0' oppure '1'",;
    ToolTipText = "Ravvedimento",;
    HelpContextID = 186402234,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=571, Top=131, cSayPict='"9"', cGetPict='"9"'

  func oIFRAVVE4_1_45.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_IFCODEL4) AND INLIST (.w_IFTRIEL4,'3901','3902','3903','3904','3905','3906','3907'))
    endwith
   endif
  endfunc

  func oIFRAVVE4_1_45.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_IFRAVVE4=0 OR .w_IFRAVVE4=1)
    endwith
    return bRes
  endfunc

  add object oIFIMMOV4_1_46 as StdField with uid="YGHAJFHTFC",rtseq=41,rtrep=.f.,;
    cFormVar = "w_IFIMMOV4", cQueryName = "IFIMMOV4",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire solo i valori '0' oppure '1'",;
    ToolTipText = "Immobili variati",;
    HelpContextID = 208161350,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=618, Top=131, cSayPict='"9"', cGetPict='"9"'

  func oIFIMMOV4_1_46.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_IFCODEL4) AND INLIST (.w_IFTRIEL4,'3901','3902','3903','3904','3905','3906','3907'))
    endwith
   endif
  endfunc

  func oIFIMMOV4_1_46.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_IFIMMOV4=0 or .w_IFIMMOV4=1)
    endwith
    return bRes
  endfunc

  add object oIFACCON4_1_47 as StdField with uid="RPGPEXOVCF",rtseq=42,rtrep=.f.,;
    cFormVar = "w_IFACCON4", cQueryName = "IFACCON4",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire solo i valori '0' oppure '1'",;
    ToolTipText = "Acconto",;
    HelpContextID = 49100218,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=668, Top=131, cSayPict='"9"', cGetPict='"9"'

  func oIFACCON4_1_47.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_IFCODEL4) AND INLIST (.w_IFTRIEL4,'3901','3902','3903','3904','3905','3906','3907'))
    endwith
   endif
  endfunc

  func oIFACCON4_1_47.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_IFACCON4=0  or .w_IFACCON4=1)
    endwith
    return bRes
  endfunc

  add object oIFTSALD4_1_48 as StdField with uid="SSURZEXZFV",rtseq=43,rtrep=.f.,;
    cFormVar = "w_IFTSALD4", cQueryName = "IFTSALD4",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire solo i valori '0' oppure '1'",;
    ToolTipText = "Saldo",;
    HelpContextID = 266233274,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=701, Top=131, cSayPict='"9"', cGetPict='"9"'

  func oIFTSALD4_1_48.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_IFCODEL4) AND INLIST (.w_IFTRIEL4,'3901','3902','3903','3904','3905','3906','3907'))
    endwith
   endif
  endfunc

  func oIFTSALD4_1_48.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_IFTSALD4=0 or .w_IFTSALD4=1)
    endwith
    return bRes
  endfunc

  add object oIFNUMFA4_1_49 as StdField with uid="FMGCMGSWEO",rtseq=44,rtrep=.f.,;
    cFormVar = "w_IFNUMFA4", cQueryName = "IFNUMFA4",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero fabbricati",;
    HelpContextID = 178259386,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=733, Top=131, cSayPict='"999"', cGetPict='"999"'

  func oIFNUMFA4_1_49.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_IFCODEL4) AND INLIST (.w_IFTRIEL4,'3901','3902','3903','3904','3905','3906','3907'))
    endwith
   endif
  endfunc

  add object oIFDETICI_1_50 as StdField with uid="QKTTBVEMVU",rtseq=45,rtrep=.f.,;
    cFormVar = "w_IFDETICI", cQueryName = "IFDETICI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Detrazione ICI per l'abitazione principale",;
    HelpContextID = 234841551,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=94, Top=170, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  add object oIFTOTDEL_1_51 as StdField with uid="TUTWDVKUWK",rtseq=46,rtrep=.f.,;
    cFormVar = "w_IFTOTDEL", cQueryName = "IFTOTDEL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 151676370,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=277, Top=170, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  add object oIFTOTCEL_1_52 as StdField with uid="VEHYDMMZRC",rtseq=47,rtrep=.f.,;
    cFormVar = "w_IFTOTCEL", cQueryName = "IFTOTCEL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 134899154,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=432, Top=170, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  add object oIFSALDEL_1_53 as StdField with uid="QMUWYTXWWP",rtseq=48,rtrep=.f.,;
    cFormVar = "w_IFSALDEL", cQueryName = "IFSALDEL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 142366162,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=618, Top=170, cSayPict="'@Z '+v_PV(78)", cGetPict="'@Z '+v_GV(78)"


  add object oObj_1_65 as cp_runprogram with uid="IMVHWAQFRK",left=0, top=226, width=110,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSTE_BFC('IC')",;
    cEvent = "ControllaDati",;
    nPag=1;
    , HelpContextID = 168944102


  add object oObj_1_67 as cp_calclbl with uid="GKGLWDZFVF",left=218, top=35, width=54,height=35,;
    caption='Anno di riferim.',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=1;
    , HelpContextID = 108526964


  add object oObj_1_68 as cp_calclbl with uid="FUGWQKLDTE",left=608, top=35, width=58,height=35,;
    caption='Immob. variati',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=1;
    , HelpContextID = 78914828


  add object oObj_1_69 as cp_calclbl with uid="ZUOHGMEAFA",left=111, top=35, width=52,height=35,;
    caption='Cod. tributo',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=1;
    , HelpContextID = 102844847


  add object oObj_1_70 as cp_calclbl with uid="PNAKQWIJHW",left=726, top=35, width=48,height=35,;
    caption='Num. immob.',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=1;
    , HelpContextID = 179203635

  add object oStr_1_1 as StdString with uid="TGTDJGUXMP",Visible=.t., Left=20, Top=31,;
    Alignment=0, Width=73, Height=18,;
    Caption="Codice ente\"  ;
  , bGlobalFont=.t.

  add object oStr_1_2 as StdString with uid="CIJLWALMQA",Visible=.t., Left=163, Top=49,;
    Alignment=0, Width=56, Height=15,;
    Caption="Rateaz."  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="ZISLZJKDPJ",Visible=.t., Left=273, Top=48,;
    Alignment=0, Width=149, Height=15,;
    Caption="Importi a debito versati"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="SLLANVSIZO",Visible=.t., Left=426, Top=48,;
    Alignment=0, Width=142, Height=15,;
    Caption="Importi a credito comp."  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="WCRKUKLJYO",Visible=.t., Left=9, Top=47,;
    Alignment=0, Width=104, Height=18,;
    Caption="Codice comune"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="DDEDDRYFQY",Visible=.t., Left=256, Top=169,;
    Alignment=0, Width=22, Height=18,;
    Caption="G"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_55 as StdString with uid="CBEVQCIWID",Visible=.t., Left=618, Top=154,;
    Alignment=2, Width=132, Height=15,;
    Caption="Saldo (G-H)"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_56 as StdString with uid="YVEZCPMINK",Visible=.t., Left=415, Top=172,;
    Alignment=0, Width=12, Height=15,;
    Caption="H"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_58 as StdString with uid="WCXQOPNHBO",Visible=.t., Left=9, Top=6,;
    Alignment=0, Width=220, Height=18,;
    Caption="Sezione ICI ed altri tributi locali"  ;
  , bGlobalFont=.t.

  add object oStr_1_59 as StdString with uid="ILJNCTJGRY",Visible=.t., Left=565, Top=48,;
    Alignment=0, Width=44, Height=15,;
    Caption="Ravv."  ;
  , bGlobalFont=.t.

  add object oStr_1_60 as StdString with uid="NRVRKZSEOQ",Visible=.t., Left=666, Top=48,;
    Alignment=0, Width=28, Height=15,;
    Caption="Acc."  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="DFQZNPDJAM",Visible=.t., Left=694, Top=48,;
    Alignment=0, Width=32, Height=15,;
    Caption="Saldo"  ;
  , bGlobalFont=.t.

  add object oStr_1_62 as StdString with uid="NVSGLSVWQB",Visible=.t., Left=9, Top=165,;
    Alignment=1, Width=83, Height=15,;
    Caption="Detrazione ICI"  ;
  , bGlobalFont=.t.

  add object oStr_1_63 as StdString with uid="KWLXZYNCRG",Visible=.t., Left=9, Top=178,;
    Alignment=1, Width=83, Height=18,;
    Caption="Abitaz. princ."  ;
  , bGlobalFont=.t.

  add object oBox_1_57 as StdBox with uid="QWPUGCRDON",left=9, top=25, width=766,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_aif','MODIPAG','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".IFSERIAL=MODIPAG.IFSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
