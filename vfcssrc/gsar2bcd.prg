* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar2bcd                                                        *
*              Modello com.ann.IVA 2008                                        *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_151]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-16                                                      *
* Last revis.: 2016-01-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar2bcd",oParentObject,m.pParam)
return(i_retval)

define class tgsar2bcd as StdBatch
  * --- Local variables
  pParam = space(4)
  w_ORIGPATH = space(200)
  w_ANNIMP = 0
  w_TESTATT = space(5)
  w_KEYATT = space(5)
  w_FLPROR = space(1)
  w_FLAGRI = space(1)
  w_PERPRO = 0
  w_TESTPROR = space(1)
  w_TIPREG = space(1)
  w_PERCOM = 0
  w_TIPAGR = space(1)
  w_VPIMPOR7 = 0
  w_MAXPER = 0
  w_NUMPER = 0
  w_TOTATT = 0
  w_TA_NIM = 0
  w_TA_ESE = 0
  w_TA_CIN = 0
  w_TA_CBS = 0
  w_TOTPAS = 0
  w_TP_NIM = 0
  w_TP_ESE = 0
  w_TP_CIN = 0
  w_TP_CBS = 0
  w_IMPORO = 0
  w_IVAORO = 0
  w_IVAESI = 0
  w_IVADET = 0
  w_IVADOV = 0
  w_IVACRE = 0
  w_IMPROT = 0
  w_IVAROT = 0
  w_INIPER = ctod("  /  /  ")
  w_FINPER = ctod("  /  /  ")
  w_IMPONI = 0
  w_IMPONICBS = 0
  w_VALNAZ = space(3)
  w_IMPAUT = 0
  w_IMPAUT_CBS = 0
  w_AIANNIMP = space(4)
  w_AI___CAPAZI = space(8)
  w_LOCAZI = space(30)
  w_LANNO = space(4)
  w_PERSFIS = space(1)
  w_TFORN = space(2)
  w_DENFORN = space(40)
  w_INDAZI = space(35)
  w_PROAZI = space(2)
  w_CDFAZI = space(16)
  w_AZPARIVA = space(11)
  w_TIPDEN = space(1)
  w_CODFISAZI = space(16)
  w_PERFIS = space(1)
  w_DENOMAZI = space(80)
  w_NOMPERF = space(25)
  w_COGPERF = space(25)
  w_TIPDIC = space(1)
  w_TTCOGTIT = space(24)
  w_TTNOMTIT = space(20)
  w_TT_SESSO = space(1)
  w_TTDATNAS = ctod("  /  /  ")
  w_TTLUONAS = space(30)
  w_TTPRONAS = space(2)
  w_TTLOCTIT = space(30)
  w_TTPROTIT = space(2)
  w_TTINDIRI = space(30)
  w_TTCAPTIT = space(5)
  w_SELOCALI = space(40)
  w_SEPROVIN = space(2)
  w_SEINDIRI = space(35)
  w_SE___CAP = space(8)
  w_DFLOCALI = space(40)
  w_DFPROVIN = space(2)
  w_DFINDIRI = space(35)
  w_DF___CAP = space(8)
  w_VPVARIMP = space(1)
  w_VPDICGRU = space(1)
  w_VPDICSOC = space(1)
  w_VPCODVAL = space(1)
  w_VPCODAZI = space(5)
  w_VPCODCAB = space(5)
  w_VPVERNEF = space(1)
  w_VPAR74C5 = space(1)
  w_VPAR74C4 = space(1)
  w_VPOPMEDI = space(1)
  w_VPOPNOIM = space(1)
  w_VPCRERIM = 0
  w_VPEURO19 = space(1)
  w_VPCREUTI = 0
  w_CODFISDI = space(16)
  w_CODCARIC = space(2)
  w_CODATT = space(6)
  w_VERCAS17 = space(1)
  w_VPDATVER = ctod("  /  /  ")
  w_IDPRODSW = space(16)
  w_ACBEN = space(1)
  w_AIPERPRO = 0
  w_ATPERPRO = space(1)
  w_SPAZI = space(100)
  w_cDate = ctod("  /  /  ")
  w_cMese = space(2)
  w_LTmp = .f.
  w_DATINO = ctod("  /  /  ")
  w_cTmp = space(100)
  w_nMAXCAMPO = 0
  w_cAnno = space(4)
  w_CONTRO = 0
  w_nTmp1 = 0
  w_nErrFile = 0
  w_cGiorno = space(2)
  w_MESS = space(100)
  w_nZERI = space(20)
  w_nTmp2 = 0
  w_MEMOREC = space(0)
  w_OK = .f.
  w_cNUMPROT = space(11)
  w_OKFISC = .f.
  w_OBBLIG = .f.
  * --- WorkFile variables
  ATTIDETT_idx=0
  ATTIMAST_idx=0
  AZIENDA_idx=0
  MOD_COAN_idx=0
  SEDIAZIE_idx=0
  TITOLARI_idx=0
  IVA_PERI_idx=0
  PRO_RATA_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questo batch legge i dati relativi all'iva per poi poter scrivere il file ascii.
    this.Page_9()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    do case
      case this.pParam="CALC"
        this.Page_7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        i_retcode = 'stop'
        return
      case this.pParam="PERF"
        this.oParentObject.w_AICOGNOM = iif(this.oParentObject.w_AIPERAZI="S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AICOGNOM," ")
        this.oParentObject.w_AINOME = iif(this.oParentObject.w_AIPERAZI="S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AINOME," ")
        this.oParentObject.w_AISESSO = "M"
        this.oParentObject.w_AIDATNAS = iif(this.oParentObject.w_AIPERAZI="S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AIDATNAS,cp_CharToDate("  -  -  "))
        this.oParentObject.w_AICOMUNE = iif(this.oParentObject.w_AIPERAZI="S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AICOMUNE," ")
        this.oParentObject.w_AISIGLA = iif(this.oParentObject.w_AIPERAZI="S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AISIGLA," ")
        this.oParentObject.w_AIRESCOM = iif(this.oParentObject.w_AIPERAZI="S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AIRESCOM," ")
        this.oParentObject.w_AIRESSIG = iif(this.oParentObject.w_AIPERAZI="S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AIRESSIG," ")
        this.oParentObject.w_AIINDIRI = iif(this.oParentObject.w_AIPERAZI="S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AIINDIRI," ")
        this.oParentObject.w_AI___CAP = iif(this.oParentObject.w_AIPERAZI="S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AI___CAP," ")
        this.oParentObject.w_AIDENOMI = iif(this.oParentObject.w_AIPERAZI<>"S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AIDENOMI," ")
        this.oParentObject.w_AISECOMU = iif(this.oParentObject.w_AIPERAZI<>"S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AISECOMU," ")
        this.oParentObject.w_AISESIGL = iif(this.oParentObject.w_AIPERAZI<>"S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AISESIGL," ")
        this.oParentObject.w_AISEIND2 = iif(this.oParentObject.w_AIPERAZI<>"S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AISEIND2," ")
        this.oParentObject.w_AISE_CAP = iif(this.oParentObject.w_AIPERAZI<>"S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AISE_CAP," ")
        this.oParentObject.w_AISERCOM = iif(this.oParentObject.w_AIPERAZI<>"S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AISERCOM," ")
        this.oParentObject.w_AISERSIG = iif(this.oParentObject.w_AIPERAZI<>"S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AISERSIG," ")
        this.oParentObject.w_AISERIND = iif(this.oParentObject.w_AIPERAZI<>"S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AISERIND," ")
        this.oParentObject.w_AISERCAP = iif(this.oParentObject.w_AIPERAZI<>"S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AISERCAP," ")
        if this.oParentObject.w_AIPERAZI="S"
          * --- Persona Fisica
          * --- Read from AZIENDA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.AZIENDA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "AZCOGFOR,AZNOMFOR,AZSEXFOR,AZCONFOR,AZDATFOR,AZPRNFOR,AZCOMFOR,AZPROFOR,AZINDFOR,AZCAPFOR"+;
              " from "+i_cTable+" AZIENDA where ";
                  +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              AZCOGFOR,AZNOMFOR,AZSEXFOR,AZCONFOR,AZDATFOR,AZPRNFOR,AZCOMFOR,AZPROFOR,AZINDFOR,AZCAPFOR;
              from (i_cTable) where;
                  AZCODAZI = i_CODAZI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_AICOGNOM = NVL(cp_ToDate(_read_.AZCOGFOR),cp_NullValue(_read_.AZCOGFOR))
            this.oParentObject.w_AINOME = NVL(cp_ToDate(_read_.AZNOMFOR),cp_NullValue(_read_.AZNOMFOR))
            this.oParentObject.w_AISESSO = NVL(cp_ToDate(_read_.AZSEXFOR),cp_NullValue(_read_.AZSEXFOR))
            this.oParentObject.w_AICOMUNE = NVL(cp_ToDate(_read_.AZCONFOR),cp_NullValue(_read_.AZCONFOR))
            this.oParentObject.w_AIDATNAS = NVL(cp_ToDate(_read_.AZDATFOR),cp_NullValue(_read_.AZDATFOR))
            this.oParentObject.w_AISIGLA = NVL(cp_ToDate(_read_.AZPRNFOR),cp_NullValue(_read_.AZPRNFOR))
            this.oParentObject.w_AIRESCOM = NVL(cp_ToDate(_read_.AZCOMFOR),cp_NullValue(_read_.AZCOMFOR))
            this.oParentObject.w_AIRESSIG = NVL(cp_ToDate(_read_.AZPROFOR),cp_NullValue(_read_.AZPROFOR))
            this.oParentObject.w_AIINDIRI = NVL(cp_ToDate(_read_.AZINDFOR),cp_NullValue(_read_.AZINDFOR))
            this.oParentObject.w_AI___CAP = NVL(cp_ToDate(_read_.AZCAPFOR),cp_NullValue(_read_.AZCAPFOR))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        else
          * --- Altri Soggetti
          * --- Read from AZIENDA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.AZIENDA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "AZDENFOR,AZCOLFOR,AZINLFOR,AZPRLFOR,AZCALFOR,AZCOMFOR,AZPROFOR,AZINDFOR,AZCAPFOR"+;
              " from "+i_cTable+" AZIENDA where ";
                  +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              AZDENFOR,AZCOLFOR,AZINLFOR,AZPRLFOR,AZCALFOR,AZCOMFOR,AZPROFOR,AZINDFOR,AZCAPFOR;
              from (i_cTable) where;
                  AZCODAZI = i_CODAZI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_AIDENOMI = NVL(cp_ToDate(_read_.AZDENFOR),cp_NullValue(_read_.AZDENFOR))
            this.oParentObject.w_AISECOMU = NVL(cp_ToDate(_read_.AZCOLFOR),cp_NullValue(_read_.AZCOLFOR))
            this.oParentObject.w_AISEIND2 = NVL(cp_ToDate(_read_.AZINLFOR),cp_NullValue(_read_.AZINLFOR))
            this.oParentObject.w_AISESIGL = NVL(cp_ToDate(_read_.AZPRLFOR),cp_NullValue(_read_.AZPRLFOR))
            this.oParentObject.w_AISE_CAP = NVL(cp_ToDate(_read_.AZCALFOR),cp_NullValue(_read_.AZCALFOR))
            this.oParentObject.w_AISERCOM = NVL(cp_ToDate(_read_.AZCOMFOR),cp_NullValue(_read_.AZCOMFOR))
            this.oParentObject.w_AISERSIG = NVL(cp_ToDate(_read_.AZPROFOR),cp_NullValue(_read_.AZPROFOR))
            this.oParentObject.w_AISERIND = NVL(cp_ToDate(_read_.AZINDFOR),cp_NullValue(_read_.AZINDFOR))
            this.oParentObject.w_AISERCAP = NVL(cp_ToDate(_read_.AZCAPFOR),cp_NullValue(_read_.AZCAPFOR))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        i_retcode = 'stop'
        return
      case this.pParam="TIPF"
        if this.oParentObject.w_AITIPFOR<>"01" AND this.oParentObject.w_AITIPFOR<>"02"
          * --- Read from AZIENDA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.AZIENDA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "AZCOFFOR"+;
              " from "+i_cTable+" AZIENDA where ";
                  +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              AZCOFFOR;
              from (i_cTable) where;
                  AZCODAZI = i_CODAZI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_AICOFINT = NVL(cp_ToDate(_read_.AZCOFFOR),cp_NullValue(_read_.AZCOFFOR))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.oParentObject.w_AIDATIMP = i_DATSYS
          this.oParentObject.w_AIFIRMA = "1"
          this.oParentObject.w_AIINVCON = "1"
          this.oParentObject.w_AIPERAZI = "S"
          this.oParentObject.w_AICOGNOM = iif(this.oParentObject.w_AIPERAZI="S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AICOGNOM," ")
          this.oParentObject.w_AINOME = iif(this.oParentObject.w_AIPERAZI="S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AINOME," ")
          this.oParentObject.w_AISESSO = "M"
          this.oParentObject.w_AIDATNAS = iif(this.oParentObject.w_AIPERAZI="S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AIDATNAS,cp_CharToDate("  -  -  "))
          this.oParentObject.w_AICOMUNE = iif(this.oParentObject.w_AIPERAZI="S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AICOMUNE," ")
          this.oParentObject.w_AISIGLA = iif(this.oParentObject.w_AIPERAZI="S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AISIGLA," ")
          this.oParentObject.w_AIRESCOM = iif(this.oParentObject.w_AIPERAZI="S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AIRESCOM," ")
          this.oParentObject.w_AIRESSIG = iif(this.oParentObject.w_AIPERAZI="S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AIRESSIG," ")
          this.oParentObject.w_AIINDIRI = iif(this.oParentObject.w_AIPERAZI="S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AIINDIRI," ")
          this.oParentObject.w_AI___CAP = iif(this.oParentObject.w_AIPERAZI="S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AI___CAP," ")
          this.oParentObject.w_AIDENOMI = iif(this.oParentObject.w_AIPERAZI<>"S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AIDENOMI," ")
          this.oParentObject.w_AISECOMU = iif(this.oParentObject.w_AIPERAZI<>"S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AISECOMU," ")
          this.oParentObject.w_AISESIGL = iif(this.oParentObject.w_AIPERAZI<>"S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AISESIGL," ")
          this.oParentObject.w_AISEIND2 = iif(this.oParentObject.w_AIPERAZI<>"S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AISEIND2," ")
          this.oParentObject.w_AISE_CAP = iif(this.oParentObject.w_AIPERAZI<>"S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AISE_CAP," ")
          this.oParentObject.w_AISERCOM = iif(this.oParentObject.w_AIPERAZI<>"S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AISERCOM," ")
          this.oParentObject.w_AISERSIG = iif(this.oParentObject.w_AIPERAZI<>"S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AISERSIG," ")
          this.oParentObject.w_AISERIND = iif(this.oParentObject.w_AIPERAZI<>"S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AISERIND," ")
          this.oParentObject.w_AISERCAP = iif(this.oParentObject.w_AIPERAZI<>"S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AISERCAP," ")
          * --- Lettura campo Persona Fisica e Num. Iscr. C.A.F.
          * --- Read from AZIENDA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.AZIENDA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "AZFISFOR,AZNUMCAF"+;
              " from "+i_cTable+" AZIENDA where ";
                  +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              AZFISFOR,AZNUMCAF;
              from (i_cTable) where;
                  AZCODAZI = i_CODAZI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_AIPERAZI = NVL(cp_ToDate(_read_.AZFISFOR),cp_NullValue(_read_.AZFISFOR))
            this.oParentObject.w_AIALBCAF = NVL(cp_ToDate(_read_.AZNUMCAF),cp_NullValue(_read_.AZNUMCAF))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.oParentObject.w_AIPERAZI="S"
            * --- Persona Fisica
            * --- Read from AZIENDA
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.AZIENDA_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "AZCOGFOR,AZNOMFOR,AZSEXFOR,AZCONFOR,AZDATFOR,AZPRNFOR,AZCOMFOR,AZPROFOR,AZINDFOR,AZCAPFOR"+;
                " from "+i_cTable+" AZIENDA where ";
                    +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                AZCOGFOR,AZNOMFOR,AZSEXFOR,AZCONFOR,AZDATFOR,AZPRNFOR,AZCOMFOR,AZPROFOR,AZINDFOR,AZCAPFOR;
                from (i_cTable) where;
                    AZCODAZI = i_CODAZI;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_AICOGNOM = NVL(cp_ToDate(_read_.AZCOGFOR),cp_NullValue(_read_.AZCOGFOR))
              this.oParentObject.w_AINOME = NVL(cp_ToDate(_read_.AZNOMFOR),cp_NullValue(_read_.AZNOMFOR))
              this.oParentObject.w_AISESSO = NVL(cp_ToDate(_read_.AZSEXFOR),cp_NullValue(_read_.AZSEXFOR))
              this.oParentObject.w_AICOMUNE = NVL(cp_ToDate(_read_.AZCONFOR),cp_NullValue(_read_.AZCONFOR))
              this.oParentObject.w_AIDATNAS = NVL(cp_ToDate(_read_.AZDATFOR),cp_NullValue(_read_.AZDATFOR))
              this.oParentObject.w_AISIGLA = NVL(cp_ToDate(_read_.AZPRNFOR),cp_NullValue(_read_.AZPRNFOR))
              this.oParentObject.w_AIRESCOM = NVL(cp_ToDate(_read_.AZCOMFOR),cp_NullValue(_read_.AZCOMFOR))
              this.oParentObject.w_AIRESSIG = NVL(cp_ToDate(_read_.AZPROFOR),cp_NullValue(_read_.AZPROFOR))
              this.oParentObject.w_AIINDIRI = NVL(cp_ToDate(_read_.AZINDFOR),cp_NullValue(_read_.AZINDFOR))
              this.oParentObject.w_AI___CAP = NVL(cp_ToDate(_read_.AZCAPFOR),cp_NullValue(_read_.AZCAPFOR))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          else
            this.oParentObject.w_AIPERAZI = "N"
            * --- Altri Soggetti
            * --- Read from AZIENDA
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.AZIENDA_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "AZDENFOR,AZCOLFOR,AZINLFOR,AZPRLFOR,AZCALFOR,AZCOMFOR,AZPROFOR,AZINDFOR,AZCAPFOR"+;
                " from "+i_cTable+" AZIENDA where ";
                    +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                AZDENFOR,AZCOLFOR,AZINLFOR,AZPRLFOR,AZCALFOR,AZCOMFOR,AZPROFOR,AZINDFOR,AZCAPFOR;
                from (i_cTable) where;
                    AZCODAZI = i_CODAZI;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_AIDENOMI = NVL(cp_ToDate(_read_.AZDENFOR),cp_NullValue(_read_.AZDENFOR))
              this.oParentObject.w_AISECOMU = NVL(cp_ToDate(_read_.AZCOLFOR),cp_NullValue(_read_.AZCOLFOR))
              this.oParentObject.w_AISEIND2 = NVL(cp_ToDate(_read_.AZINLFOR),cp_NullValue(_read_.AZINLFOR))
              this.oParentObject.w_AISESIGL = NVL(cp_ToDate(_read_.AZPRLFOR),cp_NullValue(_read_.AZPRLFOR))
              this.oParentObject.w_AISE_CAP = NVL(cp_ToDate(_read_.AZCALFOR),cp_NullValue(_read_.AZCALFOR))
              this.oParentObject.w_AISERCOM = NVL(cp_ToDate(_read_.AZCOMFOR),cp_NullValue(_read_.AZCOMFOR))
              this.oParentObject.w_AISERSIG = NVL(cp_ToDate(_read_.AZPROFOR),cp_NullValue(_read_.AZPROFOR))
              this.oParentObject.w_AISERIND = NVL(cp_ToDate(_read_.AZINDFOR),cp_NullValue(_read_.AZINDFOR))
              this.oParentObject.w_AISERCAP = NVL(cp_ToDate(_read_.AZCAPFOR),cp_NullValue(_read_.AZCAPFOR))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
        else
          this.oParentObject.w_AICOFINT = SPACE(16)
          this.oParentObject.w_AIDATIMP = cp_CharToDate("  -  -    ")
          this.oParentObject.w_AIFIRMA = "0"
          this.oParentObject.w_AIINVCON = "0"
        endif
        i_retcode = 'stop'
        return
      case this.pParam="INIT"
        this.w_AIANNIMP = IIF(this.oParentObject.w_AIANNIMP=0,ALLTRIM(STR(YEAR(this.oParentObject.w_INIESE)-1)),this.w_AIANNIMP)
        if NOT ((this.oParentObject.w_AIANNIMP=this.oParentObject.w_ANNO - 1) OR this.oParentObject.w_AIANNIMP=0) AND (this.oParentObject.w_ANNO=2008 or this.oParentObject.w_ANNO=2009 or this.oParentObject.w_ANNO=2010)
          this.oParentObject.w_AIANNIMP=0
          ah_errormsg ("Anno di imposta errato: selezionare anno %1",,,alltrim(STR(this.oParentObject.w_ANNO - 1)))
          i_retcode = 'stop'
          return
        endif
        if NOT ((this.oParentObject.w_AIANNIMP>=(this.oParentObject.w_ANNO-1)) OR this.oParentObject.w_AIANNIMP=0) AND this.oParentObject.w_ANNO=2011
          this.oParentObject.w_AIANNIMP=0
          ah_errormsg ("Anno di imposta errato")
          i_retcode = 'stop'
          return
        endif
        * --- Lettura Contabilit� Separate, Societ� aderente al gruppo, Carica, Tipo fornitore e Codice Fiscale
        * --- Read from AZIENDA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.AZIENDA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "AZTIPDIC,AZIVACAR,AZTIPFOR,AZIVACOF"+;
            " from "+i_cTable+" AZIENDA where ";
                +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            AZTIPDIC,AZIVACAR,AZTIPFOR,AZIVACOF;
            from (i_cTable) where;
                AZCODAZI = i_CODAZI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_TIPDIC = NVL(cp_ToDate(_read_.AZTIPDIC),cp_NullValue(_read_.AZTIPDIC))
          this.oParentObject.w_AICODCAR = NVL(cp_ToDate(_read_.AZIVACAR),cp_NullValue(_read_.AZIVACAR))
          this.oParentObject.w_AITIPFOR = NVL(cp_ToDate(_read_.AZTIPFOR),cp_NullValue(_read_.AZTIPFOR))
          this.oParentObject.w_AICODFIS = NVL(cp_ToDate(_read_.AZIVACOF),cp_NullValue(_read_.AZIVACOF))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.oParentObject.w_AICODIVA = SPACE(11)
        if this.w_TIPDIC="S"
          this.oParentObject.w_AISOCGRU = "1"
        else
          this.oParentObject.w_AISOCGRU = "0"
        endif
        if this.oParentObject.w_AITIPFOR<>"01" AND this.oParentObject.w_AITIPFOR<>"10"
          ah_errormsg ("Tipo fornitore specificato nei parametri IVA non congruente")
          this.oParentObject.w_AITIPFOR = "01"
        endif
        if this.oParentObject.w_AITIPFOR<>"01" AND this.oParentObject.w_AITIPFOR<>"02"
          * --- Read from AZIENDA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.AZIENDA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "AZCOFFOR"+;
              " from "+i_cTable+" AZIENDA where ";
                  +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              AZCOFFOR;
              from (i_cTable) where;
                  AZCODAZI = i_CODAZI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_AICOFINT = NVL(cp_ToDate(_read_.AZCOFFOR),cp_NullValue(_read_.AZCOFFOR))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.oParentObject.w_AIDATIMP = i_DATSYS
          this.oParentObject.w_AIFIRMA = "1"
          this.oParentObject.w_AIINVCON = "1"
          this.oParentObject.w_AIPERAZI = "S"
          this.oParentObject.w_AICOGNOM = iif(this.oParentObject.w_AIPERAZI="S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AICOGNOM," ")
          this.oParentObject.w_AINOME = iif(this.oParentObject.w_AIPERAZI="S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AINOME," ")
          this.oParentObject.w_AISESSO = "M"
          this.oParentObject.w_AIDATNAS = iif(this.oParentObject.w_AIPERAZI="S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AIDATNAS,cp_CharToDate("  -  -  "))
          this.oParentObject.w_AICOMUNE = iif(this.oParentObject.w_AIPERAZI="S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AICOMUNE," ")
          this.oParentObject.w_AISIGLA = iif(this.oParentObject.w_AIPERAZI="S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AISIGLA," ")
          this.oParentObject.w_AIRESCOM = iif(this.oParentObject.w_AIPERAZI="S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AIRESCOM," ")
          this.oParentObject.w_AIRESSIG = iif(this.oParentObject.w_AIPERAZI="S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AIRESSIG," ")
          this.oParentObject.w_AIINDIRI = iif(this.oParentObject.w_AIPERAZI="S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AIINDIRI," ")
          this.oParentObject.w_AI___CAP = iif(this.oParentObject.w_AIPERAZI="S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AI___CAP," ")
          this.oParentObject.w_AIDENOMI = iif(this.oParentObject.w_AIPERAZI<>"S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AIDENOMI," ")
          this.oParentObject.w_AISECOMU = iif(this.oParentObject.w_AIPERAZI<>"S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AISECOMU," ")
          this.oParentObject.w_AISESIGL = iif(this.oParentObject.w_AIPERAZI<>"S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AISESIGL," ")
          this.oParentObject.w_AISEIND2 = iif(this.oParentObject.w_AIPERAZI<>"S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AISEIND2," ")
          this.oParentObject.w_AISE_CAP = iif(this.oParentObject.w_AIPERAZI<>"S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AISE_CAP," ")
          this.oParentObject.w_AISERCOM = iif(this.oParentObject.w_AIPERAZI<>"S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AISERCOM," ")
          this.oParentObject.w_AISERSIG = iif(this.oParentObject.w_AIPERAZI<>"S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AISERSIG," ")
          this.oParentObject.w_AISERIND = iif(this.oParentObject.w_AIPERAZI<>"S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AISERIND," ")
          this.oParentObject.w_AISERCAP = iif(this.oParentObject.w_AIPERAZI<>"S" and !empty(this.oParentObject.w_AICOFINT),this.oParentObject.w_AISERCAP," ")
          * --- Lettura campo Persona Fisica e Num. Iscr. C.A.F.
          * --- Read from AZIENDA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.AZIENDA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "AZFISFOR,AZNUMCAF"+;
              " from "+i_cTable+" AZIENDA where ";
                  +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              AZFISFOR,AZNUMCAF;
              from (i_cTable) where;
                  AZCODAZI = i_CODAZI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_AIPERAZI = NVL(cp_ToDate(_read_.AZFISFOR),cp_NullValue(_read_.AZFISFOR))
            this.oParentObject.w_AIALBCAF = NVL(cp_ToDate(_read_.AZNUMCAF),cp_NullValue(_read_.AZNUMCAF))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.oParentObject.w_AIPERAZI="S"
            * --- Persona Fisica
            * --- Read from AZIENDA
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.AZIENDA_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "AZCOGFOR,AZNOMFOR,AZSEXFOR,AZCONFOR,AZDATFOR,AZPRNFOR,AZCOMFOR,AZPROFOR,AZINDFOR,AZCAPFOR"+;
                " from "+i_cTable+" AZIENDA where ";
                    +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                AZCOGFOR,AZNOMFOR,AZSEXFOR,AZCONFOR,AZDATFOR,AZPRNFOR,AZCOMFOR,AZPROFOR,AZINDFOR,AZCAPFOR;
                from (i_cTable) where;
                    AZCODAZI = i_CODAZI;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_AICOGNOM = NVL(cp_ToDate(_read_.AZCOGFOR),cp_NullValue(_read_.AZCOGFOR))
              this.oParentObject.w_AINOME = NVL(cp_ToDate(_read_.AZNOMFOR),cp_NullValue(_read_.AZNOMFOR))
              this.oParentObject.w_AISESSO = NVL(cp_ToDate(_read_.AZSEXFOR),cp_NullValue(_read_.AZSEXFOR))
              this.oParentObject.w_AICOMUNE = NVL(cp_ToDate(_read_.AZCONFOR),cp_NullValue(_read_.AZCONFOR))
              this.oParentObject.w_AIDATNAS = NVL(cp_ToDate(_read_.AZDATFOR),cp_NullValue(_read_.AZDATFOR))
              this.oParentObject.w_AISIGLA = NVL(cp_ToDate(_read_.AZPRNFOR),cp_NullValue(_read_.AZPRNFOR))
              this.oParentObject.w_AIRESCOM = NVL(cp_ToDate(_read_.AZCOMFOR),cp_NullValue(_read_.AZCOMFOR))
              this.oParentObject.w_AIRESSIG = NVL(cp_ToDate(_read_.AZPROFOR),cp_NullValue(_read_.AZPROFOR))
              this.oParentObject.w_AIINDIRI = NVL(cp_ToDate(_read_.AZINDFOR),cp_NullValue(_read_.AZINDFOR))
              this.oParentObject.w_AI___CAP = NVL(cp_ToDate(_read_.AZCAPFOR),cp_NullValue(_read_.AZCAPFOR))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          else
            this.oParentObject.w_AIPERAZI = "N"
            * --- Altri Soggetti
            * --- Read from AZIENDA
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.AZIENDA_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "AZDENFOR,AZCOLFOR,AZINLFOR,AZPRLFOR,AZCALFOR,AZCOMFOR,AZPROFOR,AZINDFOR,AZCAPFOR"+;
                " from "+i_cTable+" AZIENDA where ";
                    +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                AZDENFOR,AZCOLFOR,AZINLFOR,AZPRLFOR,AZCALFOR,AZCOMFOR,AZPROFOR,AZINDFOR,AZCAPFOR;
                from (i_cTable) where;
                    AZCODAZI = i_CODAZI;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_AIDENOMI = NVL(cp_ToDate(_read_.AZDENFOR),cp_NullValue(_read_.AZDENFOR))
              this.oParentObject.w_AISECOMU = NVL(cp_ToDate(_read_.AZCOLFOR),cp_NullValue(_read_.AZCOLFOR))
              this.oParentObject.w_AISEIND2 = NVL(cp_ToDate(_read_.AZINLFOR),cp_NullValue(_read_.AZINLFOR))
              this.oParentObject.w_AISESIGL = NVL(cp_ToDate(_read_.AZPRLFOR),cp_NullValue(_read_.AZPRLFOR))
              this.oParentObject.w_AISE_CAP = NVL(cp_ToDate(_read_.AZCALFOR),cp_NullValue(_read_.AZCALFOR))
              this.oParentObject.w_AISERCOM = NVL(cp_ToDate(_read_.AZCOMFOR),cp_NullValue(_read_.AZCOMFOR))
              this.oParentObject.w_AISERSIG = NVL(cp_ToDate(_read_.AZPROFOR),cp_NullValue(_read_.AZPROFOR))
              this.oParentObject.w_AISERIND = NVL(cp_ToDate(_read_.AZINDFOR),cp_NullValue(_read_.AZINDFOR))
              this.oParentObject.w_AISERCAP = NVL(cp_ToDate(_read_.AZCAPFOR),cp_NullValue(_read_.AZCAPFOR))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
        else
          this.oParentObject.w_AICOFINT = SPACE(16)
          this.oParentObject.w_AIDATIMP = cp_CharToDate("  -  -    ")
          this.oParentObject.w_AIFIRMA = "0"
          this.oParentObject.w_AIINVCON = "0"
        endif
        i_retcode = 'stop'
        return
      case this.pParam="FILE"
        this.w_ORIGPATH = sys(5)+sys(2003)
        INVPATH="VAL"
        ON ERROR INVPATH="ERR"
        CD (iif(EMPTY(this.oParentObject.w_NOMEFILE),this.w_ORIGPATH,SUBSTR(this.oParentObject.w_NOMEFILE,1,RATC("\",this.oParentObject.w_NOMEFILE))))
        ON ERROR
        this.oParentObject.w_NOMEFILE = GetFile()
        CD (this.w_ORIGPATH)
        if EMPTY(this.oParentObject.w_NOMEFILE)
          this.oParentObject.w_NOMEFILE = left(sys(5)+sys(2003)+"\"+this.oParentObject.w_NOMEFILEFIS+space(200),200)
        endif
        if RIGHT(ALLTRIM(this.oParentObject.w_NOMEFILE),1)="\"
          this.oParentObject.w_NOMEFILE = left(ALLTRIM(this.oParentObject.w_NOMEFILE)+this.oParentObject.w_NOMEFILEFIS+space(200),200)
        endif
        i_retcode = 'stop'
        return
    endcase
    this.w_ANNIMP = this.oParentObject.w_AIANNIMP
    * --- Read from MOD_COAN
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.MOD_COAN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOD_COAN_idx,2],.t.,this.MOD_COAN_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AICODAT1"+;
        " from "+i_cTable+" MOD_COAN where ";
            +"AIANNIMP = "+cp_ToStrODBC(this.w_ANNIMP);
            +" and AIDATINV = "+cp_ToStrODBC(this.oParentObject.w_AIDATINV);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AICODAT1;
        from (i_cTable) where;
            AIANNIMP = this.w_ANNIMP;
            and AIDATINV = this.oParentObject.w_AIDATINV;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TESTATT = NVL(cp_ToDate(_read_.AICODAT1),cp_NullValue(_read_.AICODAT1))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if Not Empty(this.w_TESTATT) and this.pParam<>"STAM" and This.oparentobject.cFunction="Load"
      ah_ErrorMsg("Attenzione, esiste gi� una comunicazione per lo stesso anno e data invio","!","")
      i_retcode = 'stop'
      return
    endif
    this.w_Okfisc = .F.
    * --- Controllo se nella maschera di inoltro telematico ho specificato il codice fiscale
    * --- dell'intermediario.Se ho specificato il codice vuol dire che i dati devono essere riportati
    * --- dalla seconda pagina della maschera altrimenti vengono letti nel batch.
    if Not empty (this.oParentObject.w_AICOFINT)
      this.w_Okfisc = .T.
      if empty(this.oParentObject.w_AIDATIMP)
        ah_ErrorMsg("Data di presentazione obbligatoria","!","")
        i_retcode = 'stop'
        return
      endif
    else
      if this.oParentObject.w_AITIPFOR="03" OR this.oParentObject.w_AITIPFOR="05" OR this.oParentObject.w_AITIPFOR="09" OR this.oParentObject.w_AITIPFOR="10"
        ah_ErrorMsg("Dati intermediario obbligatori","!","")
        i_retcode = 'stop'
        return
      endif
      this.w_Okfisc = .F.
    endif
    if ((Not Empty(this.oParentObject.w_AICODFIS) And this.oParentObject.w_AICODCAR="00") OR (this.oParentObject.w_AICODCAR<>"00" And Empty(this.oParentObject.w_AICODFIS))) And this.oParentObject.w_EDITADICH="S"
      ah_ErrorMsg("Codice fiscale dichiarante o codice carica non valorizzato",48)
      i_retcode = 'stop'
      return
    endif
    if Empty (this.oParentObject.w_AICODAT1)
      ah_ErrorMsg("Codice attivit� obbligatorio.","!","")
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_AZPERAZI="S" And (Empty (this.oParentObject.w_COGTIT) OR Empty (this.oParentObject.w_NOMTIT))
      ah_ErrorMsg("Mancano il nome o cognome del titolare.","!","")
      i_retcode = 'stop'
      return
    endif
    if Empty (this.oParentObject.w_CODFIS)
      ah_ErrorMsg("Codice fiscale azienda obbligatorio.","!","")
      i_retcode = 'stop'
      return
    endif
    if Empty (this.oParentObject.w_AIPARIVA)
      ah_ErrorMsg("Partita IVA contribuente obbligatoria.","!","")
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_EDITADICH="S" And this.oParentObject.w_CODFIS=this.oParentObject.w_AICODFIS
      ah_ErrorMsg("Codice fiscale dichiarante uguale al codice fiscale contribuente",48)
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_AIINOLTRO="S"
      this.w_MESS = "Dichiarazione gi� inoltrata:%0si intende proseguire?%0"
      this.w_OK = ah_YesNo(this.w_MESS)
      if NOT this.w_OK
        i_retcode = 'stop'
        return
      endif
    endif
    this.w_CODATT = this.oParentObject.w_AICODAT1
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZPERAZI,AZRAGAZI,AZLOCAZI,AZINDAZI,AZCAPAZI,AZPROAZI,AZTIPDEN"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZPERAZI,AZRAGAZI,AZLOCAZI,AZINDAZI,AZCAPAZI,AZPROAZI,AZTIPDEN;
        from (i_cTable) where;
            AZCODAZI = this.oParentObject.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PERSFIS = NVL(cp_ToDate(_read_.AZPERAZI),cp_NullValue(_read_.AZPERAZI))
      this.w_DENFORN = NVL(cp_ToDate(_read_.AZRAGAZI),cp_NullValue(_read_.AZRAGAZI))
      this.w_LOCAZI = NVL(cp_ToDate(_read_.AZLOCAZI),cp_NullValue(_read_.AZLOCAZI))
      this.w_INDAZI = NVL(cp_ToDate(_read_.AZINDAZI),cp_NullValue(_read_.AZINDAZI))
      this.w_AI___CAPAZI = NVL(cp_ToDate(_read_.AZCAPAZI),cp_NullValue(_read_.AZCAPAZI))
      this.w_PROAZI = NVL(cp_ToDate(_read_.AZPROAZI),cp_NullValue(_read_.AZPROAZI))
      this.w_TIPDEN = NVL(cp_ToDate(_read_.AZTIPDEN),cp_NullValue(_read_.AZTIPDEN))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_CDFAZI = this.oParentObject.w_CODFIS
    this.w_AZPARIVA = this.oParentObject.w_AIPARIVA
    * --- Read from TITOLARI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.TITOLARI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TITOLARI_idx,2],.t.,this.TITOLARI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "TTCOGTIT,TTNOMTIT,TT_SESSO,TTDATNAS,TTLUONAS,TTPRONAS,TTLOCTIT,TTPROTIT,TTINDIRI,TTCAPTIT"+;
        " from "+i_cTable+" TITOLARI where ";
            +"TTCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        TTCOGTIT,TTNOMTIT,TT_SESSO,TTDATNAS,TTLUONAS,TTPRONAS,TTLOCTIT,TTPROTIT,TTINDIRI,TTCAPTIT;
        from (i_cTable) where;
            TTCODAZI = this.oParentObject.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TTCOGTIT = NVL(cp_ToDate(_read_.TTCOGTIT),cp_NullValue(_read_.TTCOGTIT))
      this.w_TTNOMTIT = NVL(cp_ToDate(_read_.TTNOMTIT),cp_NullValue(_read_.TTNOMTIT))
      this.w_TT_SESSO = NVL(cp_ToDate(_read_.TT_SESSO),cp_NullValue(_read_.TT_SESSO))
      this.w_TTDATNAS = NVL(cp_ToDate(_read_.TTDATNAS),cp_NullValue(_read_.TTDATNAS))
      this.w_TTLUONAS = NVL(cp_ToDate(_read_.TTLUONAS),cp_NullValue(_read_.TTLUONAS))
      this.w_TTPRONAS = NVL(cp_ToDate(_read_.TTPRONAS),cp_NullValue(_read_.TTPRONAS))
      this.w_TTLOCTIT = NVL(cp_ToDate(_read_.TTLOCTIT),cp_NullValue(_read_.TTLOCTIT))
      this.w_TTPROTIT = NVL(cp_ToDate(_read_.TTPROTIT),cp_NullValue(_read_.TTPROTIT))
      this.w_TTINDIRI = NVL(cp_ToDate(_read_.TTINDIRI),cp_NullValue(_read_.TTINDIRI))
      this.w_TTCAPTIT = NVL(cp_ToDate(_read_.TTCAPTIT),cp_NullValue(_read_.TTCAPTIT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_TTCOGTIT = LEFT(ALLTRIM(this.w_TTCOGTIT),24)
    this.w_TTNOMTIT = LEFT(ALLTRIM(this.w_TTNOMTIT),20)
    * --- Due letture per leggere la sede legale, il domicilio fiscale
    * --- Read from SEDIAZIE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.SEDIAZIE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SEDIAZIE_idx,2],.t.,this.SEDIAZIE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "SELOCALI,SEPROVIN,SEINDIRI,SE___CAP"+;
        " from "+i_cTable+" SEDIAZIE where ";
            +"SECODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
            +" and SETIPRIF = "+cp_ToStrODBC("DF");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        SELOCALI,SEPROVIN,SEINDIRI,SE___CAP;
        from (i_cTable) where;
            SECODAZI = this.oParentObject.w_CODAZI;
            and SETIPRIF = "DF";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DFLOCALI = NVL(cp_ToDate(_read_.SELOCALI),cp_NullValue(_read_.SELOCALI))
      this.w_DFPROVIN = NVL(cp_ToDate(_read_.SEPROVIN),cp_NullValue(_read_.SEPROVIN))
      this.w_DFINDIRI = NVL(cp_ToDate(_read_.SEINDIRI),cp_NullValue(_read_.SEINDIRI))
      this.w_DF___CAP = NVL(cp_ToDate(_read_.SE___CAP),cp_NullValue(_read_.SE___CAP))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from SEDIAZIE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.SEDIAZIE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SEDIAZIE_idx,2],.t.,this.SEDIAZIE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "SELOCALI,SEPROVIN,SEINDIRI,SE___CAP"+;
        " from "+i_cTable+" SEDIAZIE where ";
            +"SECODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
            +" and SETIPRIF = "+cp_ToStrODBC("SL");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        SELOCALI,SEPROVIN,SEINDIRI,SE___CAP;
        from (i_cTable) where;
            SECODAZI = this.oParentObject.w_CODAZI;
            and SETIPRIF = "SL";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_SELOCALI = NVL(cp_ToDate(_read_.SELOCALI),cp_NullValue(_read_.SELOCALI))
      this.w_SEPROVIN = NVL(cp_ToDate(_read_.SEPROVIN),cp_NullValue(_read_.SEPROVIN))
      this.w_SEINDIRI = NVL(cp_ToDate(_read_.SEINDIRI),cp_NullValue(_read_.SEINDIRI))
      this.w_SE___CAP = NVL(cp_ToDate(_read_.SE___CAP),cp_NullValue(_read_.SE___CAP))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Creazione del record di  Tipo A record di testa
    this.w_TFORN = this.oParentObject.w_AITIPFOR
    * --- Leggo i dati dall'angrafica dell'azienda e creazione del cursore TIP_REC_A per contenere i dati.
    Create Cursor tip_rec_a ;
    ( TIPREC C(1), PRIMOFILL C(14), CODFORN C(5), TIPFORN C(2), CODFISF C(16), ;
    TTCOGTIT C(24), TTNOMTIT C(20), TT_SESSO C(1), TTDATNAS C(8), TTLUONAS C(40), TTPRONAS C(2), TTLOCTIT C(40), ;
    TTPROTIT C(2), TTINDIRI C(35), TTCAPTIT C(5), AZRAGAZI C(60), AZLOCAZI C(40), AZPROAZI C(2), AZINDAZI C(35), AZCAPAZI C(5), ;
    SELOCALI C(40), SEPROVIN C(2), SEINDIRI C(35), SE__CAP C(5), CAFLOCAL C(40), CAFPROV C(2), CAFINDIR C(35), CAF__CAP C(5), ;
    CAFFILL C(6), CAMPOUT C(100), FILLER301 C(254), FILLER302 C(254), FILLER303 C(254), FILLER304 C(254), FILLER305 C(54),;
    SPAZRIS C(200), CONTR32 C(1), CONTR33 C(2) )
    Insert Into tip_rec_a ;
    ( TIPREC, PRIMOFILL, CODFORN, TIPFORN, CODFISF, ;
    TTCOGTIT, TTNOMTIT, TT_SESSO, TTDATNAS, TTLUONAS, TTPRONAS, TTLOCTIT, ;
    TTPROTIT, TTINDIRI, TTCAPTIT, AZRAGAZI, AZLOCAZI, AZPROAZI, AZINDAZI, AZCAPAZI, ;
    SELOCALI, SEPROVIN, SEINDIRI, SE__CAP, CAFLOCAL, CAFPROV, CAFINDIR, CAF__CAP, ;
    CAFFILL, CAMPOUT, FILLER301, FILLER302, FILLER303, FILLER304, FILLER305,;
    SPAZRIS, CONTR32, CONTR33 );
    values ("", "", "","","",;
    "","","","","","","",;
    "","","","","","","","",;
    "","","","","","","","",;
    "","","","","","","",;
    "","","")
    * --- rendo scrivibile il cursore tip_rec_a
    =wrcursor("tip_rec_a")
    select tip_rec_a
    Replace TIPREC with "A"
    Replace Primofill with Space(14)
    if this.oParentObject.w_ANNO>=2010
      Replace CODFORN with "IVC10"
    else
      Replace CODFORN with "IVC08"
    endif
    Replace Tipforn with ALLTRIM(this.w_TFORN)
    do case
      case this.oParentObject.w_AITIPFOR="01" OR this.oParentObject.w_AITIPFOR="02"
        Replace CODFISF with LEFT(alltrim(Upper(this.w_CDFAZI))+space(16),16)
      case this.oParentObject.w_AITIPFOR="03" OR this.oParentObject.w_AITIPFOR="05" OR this.oParentObject.w_AITIPFOR="09" OR this.oParentObject.w_AITIPFOR="10"
        Replace CODFISF with LEFT(IIF(EMPTY(NVL(this.oParentObject.w_AICOFINT,"")),alltrim(Upper(this.w_CDFAZI)),alltrim(Upper(this.oParentObject.w_AICOFINT)))+space(16),16)
      otherwise
        Replace CODFISF with LEFT(IIF(EMPTY(NVL(this.oParentObject.w_AICODFIS,"")),alltrim(Upper(this.w_CDFAZI)),alltrim(Upper(this.oParentObject.w_AICODFIS)))+space(16),16)
    endcase
    if this.w_Okfisc=.T.
      if this.oParentObject.w_AIPERAZI<>"S"
        * --- L'intermediario non � una persona fisica
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        * --- L'intermediario � una persona fisica
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    else
      * --- controllo se l'azienda e' formata da una persona fisica
      if this.w_PERSFIS <> "S"
        * --- Flag persona fisica non valorizzato scrittura del record di tipo 'A' dal campo 6 ... 24
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        * --- Flag persona fisica valorizzato scrittura del record di tipo 'A' dal campo 6 ... 24
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    * --- scrittura del recod di tipo 'A' dal campo 25 ... 34
    * --- CAMPI DA IMPOSTARE SE LA FORNITURA VIENE EFFETTUATA DA UN UFFICIO PERIFERICO DEL C.A.F.
    Replace CAFLOCAL with Space(40)
    Replace CAFPROV with Space(2)
    Replace CAFINDIR with Space(35)
    Replace CAF__CAP with "00000"
    Replace CAFFILL with "000000"
    * --- SPAZIO A DISPOSIZIONE DELL'UTENTE
    Replace CAMPOUT with Space(100)
    Replace FILLER301 with Space(483)
    Replace FILLER302 with Space(254)
    Replace FILLER303 with Space(254)
    Replace FILLER304 with Space(254)
    Replace FILLER305 with Space(54)
    Replace SPAZRIS with Space(200)
    Replace CONTR32 with "A"
    Replace CONTR33 with CHR(13)+CHR(10)
    * --- Creazione del record di tipo B
    * --- Leggo i dati dell'IVA, per i progressivi IVA da PRI_DETT, PRI_MAST
    * --- Leggo i dati dell'IVA da IVA_PERI per manutenione IVA periodica.
    * --- Leggo i dati dall'angrafica dell'azienda e creazione del cursore TIP_REC_B per contenere i dati.
    CREATE CURSOR tip_rec_b ;
    ( TIPREC C(1), AZCOFAZI C(16), FILLER3 C(8), SPAZIO4 C(3), FILLER5 C(25), SPAZIO6 C(20), ;
    IDPRODSW C(16), FLAGCONF C(1), AZRAGAZI C(60), TTCOGTIT C(24), TTNOMTIT C(20), DI_ANNO C(4), AZPIVAZI C(11), ;
    CODATT C(6),CONTSEPA C(1), COMSOCGRU C(1), EVENECCE C(1), CODFISDIC C(16), CODCAR C(2), PARIVADIC C(11), ;
    CD1_1 C(11), CD1_2 C(11), CD1_3 C(11), CD1_4 C(11), CD1_5 C(11), CD2_1 C(11), CD2_2 C(11), CD2_3 C(11), CD2_4 C(11),CD2_5 C (11), ;
    CD3_1 C(11), CD3_2 C(11), CD3_3 C(11), CD3_4 C(11),CD4 C(11), CD5 C(11), CD6_1 C(11), CD6_2 C(11), ;
    FIRMA C(1), VPCODFIS C(16), CODCAF C(5), IMPTRACON C(1), IMPTRASOG C(1), DATIMPTRA C(8), FIRMAINT C(1),;
    FILLER741 C(250),FILLER742 C(250),FILLER743 C(250),FILLER744 C(250),FILLER745 C(250),FILLER746 C(160),;
    SPASETEL C(20),FILLER76 C(8), FILLER77 C(8), FILLER78 C(1), FILLER79 C(17), FILLER80 C(1), FILLER81 C(2),FLCODF C(1))
    Insert Into tip_rec_b ;
    (TIPREC, AZCOFAZI, FILLER3, SPAZIO4, FILLER5, SPAZIO6, ;
    IDPRODSW, FLAGCONF, AZRAGAZI, TTCOGTIT, TTNOMTIT, DI_ANNO, AZPIVAZI, ;
    CODATT,CONTSEPA, COMSOCGRU, EVENECCE, CODFISDIC, CODCAR, PARIVADIC, ;
    CD1_1, CD1_2, CD1_3, CD1_4,CD1_5, CD2_1, CD2_2, CD2_3, CD2_4,CD2_5,;
    CD3_1, CD3_2, CD3_3, CD3_4, CD4, CD5, CD6_1, CD6_2, ;
    FIRMA, VPCODFIS, CODCAF, IMPTRACON, IMPTRASOG, DATIMPTRA, FIRMAINT,;
    FILLER741,FILLER742,FILLER743,FILLER744,FILLER745,FILLER746,;
    SPASETEL,FILLER76, FILLER77, FILLER78, FILLER79, FILLER80, FILLER81,FLCODF) ;
    values ("", "", "","","","",;
    "","","","","","","",;
    "","","","","","","","","",;
    "","","","","","","","",;
    "","","","","","","","",;
    "","","","","","","",;
    "","","","","","",;
    "","","","","","","","")
    * --- rendo scrivibile il cursore tip_rec_b
    =wrcursor("tip_rec_b")
    select tip_rec_b
    * --- scrittura record di tipo B campi 1.. 20
    this.Page_4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- SCRITTURA DATI CONTABILI CAMPI 21 ... 34
    this.Page_5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- SCRITTURA DATI CONTABILI CAMPI  35 ... 49 DEL RECORD B
    this.Page_6()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Creazione del RECORD DI TIPO Z
    CREATE CURSOR tip_rec_z ;
    (TIPREC C(1), FILLER2 C(14), NUMRECB C(9), FILLER41 C(254), FILLER42 C(254), FILLER43 C(254), FILLER44 C(254), FILLER45 C(254),;
    FILLER46 C(254), FILLER47 C(254), FILLER48 C(95), FILLER5 C(1), FILLER6 C(2))
    Insert Into tip_rec_z (TIPREC, FILLER2, NUMRECB, FILLER41, FILLER42, FILLER43, FILLER44, FILLER45, ;
    FILLER46, FILLER47, FILLER48, FILLER5, FILLER6 ) ;
    Values ("","","","","","","","","","","","","")
    =wrcursor("tip_rec_z")
    select tip_rec_z
    * --- scrittura del campo z E SCRITTURA DEL FILE.
    this.Page_8()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- SCRITTURA DEL RECORD DI TIPO 'A' DAL CAMPO 6 ... 24
    Replace TTCOGTIT with Space(24)
    Replace TTNOMTIT with Space(20)
    Replace TT_SESSO with Space(1)
    Replace TTDATNAS with "00000000"
    Replace TTLUONAS with Space(40)
    Replace TTPRONAS with Space(2)
    Replace TTLOCTIT with Space(40)
    Replace TTPROTIT with Space(2)
    Replace TTINDIRI with Space(35)
    Replace TTCAPTIT with "00000"
    * --- FINE AGGIORNAMENTO CURSORE DATI RISERVATI AL FORNITORE PERSONA FISICA
    * --- Crea parte di record per la persona non fisica
    * --- Verifico sempre se ho impostato il codice fiscale dell'intermediario
    * --- Se Intermediario legge i dati dalla Maschera
    if this.w_Okfisc=.T.
      this.w_nTmp1 = len(alltrim(this.oParentObject.w_AIDENOMI))
      this.w_nMAXCAMPO = 60
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace AZRAGAZI with alltrim(Upper(this.oParentObject.w_AIDENOMI))+this.w_SPAZI
      this.w_nTmp1 = len(alltrim(this.oParentObject.w_AISECOMU))
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_nMAXCAMPO = 40
      this.w_SPAZI = space(this.w_nTmp2)
      Replace AZLOCAZI with alltrim(Upper(this.oParentObject.w_AISECOMU))+this.w_SPAZI
      this.w_nTmp1 = len(alltrim(this.oParentObject.w_AISESIGL))
      this.w_nMAXCAMPO = 2
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace AZPROAZI with alltrim(upper(this.oParentObject.w_AISESIGL))+this.w_SPAZI
      this.w_nTmp1 = len(alltrim(this.oParentObject.w_AISEIND2))
      this.w_nMAXCAMPO = 35
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace AZINDAZI with alltrim(Upper(this.oParentObject.w_AISEIND2))+this.w_SPAZI
      this.w_SPAZI = "00000"
      this.w_nTmp1 = len(alltrim(this.oParentObject.w_AISE_CAP))
      this.w_nMAXCAMPO = 5
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = SUBSTR(this.w_SPAZI,1,this.w_nTmp2)+ALLTRIM(this.oParentObject.w_AISE_CAP)
      Replace AZCAPAZI with this.w_SPAZI
      this.w_nTmp1 = len(alltrim(this.oParentObject.w_AISERCOM))
      this.w_nMAXCAMPO = 40
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace SELOCALI with alltrim(Upper(this.oParentObject.w_AISERCOM))+this.w_SPAZI
      this.w_nTmp1 = len(alltrim(this.oParentObject.w_AISERSIG))
      this.w_nMAXCAMPO = 2
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace SEPROVIN with alltrim(upper(this.oParentObject.w_AISERSIG))+this.w_SPAZI
      this.w_nTmp1 = len(alltrim(this.oParentObject.w_AISERIND))
      this.w_nMAXCAMPO = 35
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace SEINDIRI with alltrim(Upper(this.oParentObject.w_AISERIND))+this.w_SPAZI
      this.w_SPAZI = "00000"
      this.w_nTmp1 = len(alltrim(this.oParentObject.w_AISERCAP))
      this.w_nMAXCAMPO = 5
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = SUBSTR(this.w_SPAZI,1,this.w_nTmp2)+ALLTRIM(this.oParentObject.w_AISERCAP)
      Replace SE__CAP with this.w_SPAZI
    else
      * --- Se non Intermediario legge i dati dall'Azienda o dalle Sedi (Se presenti)
      * --- Se i dati si riferiscono all'azienda imposto le variabili ai dati letti dalle sedi
      * --- Leggo se esiste la sede legale
      if !empty(this.w_SELOCALI)
        this.w_LOCAZI = this.w_SELOCALI
        this.w_PROAZI = this.w_SEPROVIN
        this.w_INDAZI = this.w_SEINDIRI
        this.w_AI___CAPAZI = this.w_SE___CAP
      endif
      this.w_nTmp1 = len(alltrim(this.w_DENFORN))
      this.w_nMAXCAMPO = 80
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace AZRAGAZI with alltrim(Upper(this.w_DENFORN))+this.w_SPAZI
      this.w_nTmp1 = len(alltrim(this.w_LOCAZI))
      this.w_nMAXCAMPO = 40
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace AZLOCAZI with alltrim(Upper(this.w_LOCAZI))+this.w_SPAZI
      this.w_nTmp1 = len(alltrim(this.w_PROAZI))
      this.w_nMAXCAMPO = 2
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace AZPROAZI with alltrim(upper(this.w_PROAZI))+this.w_SPAZI
      this.w_nTmp1 = len(alltrim(this.w_INDAZI))
      this.w_nMAXCAMPO = 35
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace AZINDAZI with alltrim(Upper(this.w_INDAZI))+this.w_SPAZI
      this.w_SPAZI = "00000"
      this.w_nTmp1 = len(alltrim(this.w_AI___CAPAZI))
      this.w_nMAXCAMPO = 5
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = SUBSTR(this.w_SPAZI,1,this.w_nTmp2)+ALLTRIM(this.w_AI___CAPAZI)
      Replace AZCAPAZI with +this.w_SPAZI
      * --- Se i dati si riferiscono all'azienda imposto le variabili ai dati letti dalle sedi
      * --- Leggo se esiste il domicilio fiscale
      if !empty(this.w_DFLOCALI)
        this.w_LOCAZI = this.w_DFLOCALI
        this.w_PROAZI = this.w_DFPROVIN
        this.w_INDAZI = this.w_DFINDIRI
        this.w_AI___CAPAZI = this.w_DF___CAP
      endif
      this.w_nTmp1 = len(alltrim(this.w_LOCAZI))
      this.w_nMAXCAMPO = 40
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace SELOCALI with alltrim(Upper(this.w_LOCAZI))+this.w_SPAZI
      this.w_nTmp1 = len(alltrim(this.w_PROAZI))
      this.w_nMAXCAMPO = 2
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace SEPROVIN with alltrim(upper(this.w_PROAZI))+this.w_SPAZI
      this.w_nTmp1 = len(alltrim(this.w_INDAZI))
      this.w_nMAXCAMPO = 35
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace SEINDIRI with alltrim(Upper(this.w_INDAZI))+this.w_SPAZI
      this.w_SPAZI = "00000"
      this.w_nTmp1 = len(alltrim(this.w_AI___CAPAZI))
      this.w_nMAXCAMPO = 5
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = SUBSTR(this.w_SPAZI,1,this.w_nTmp2)+ALLTRIM(this.w_AI___CAPAZI)
      Replace SE__CAP with this.w_SPAZI
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- SCRIVE I CAMPI DEL RECORD DATI PERSONA DEL FORNITORE
    * --- Effettuo il controllo se ho specificato il codice fiscale dell'intermediario
    if this.w_Okfisc=.T.
      this.w_nTmp1 = len(alltrim(this.oParentObject.w_AICOGNOM))
      this.w_nMAXCAMPO = 24
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace TTCOGTIT with alltrim(Upper(this.oParentObject.w_AICOGNOM))+this.w_SPAZI
      this.w_nTmp1 = len(alltrim(this.oParentObject.w_AINOME))
      this.w_nMAXCAMPO = 20
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace TTNOMTIT with alltrim(Upper(this.oParentObject.w_AINOME))+this.w_SPAZI
      this.w_nTmp1 = len(alltrim(this.oParentObject.w_AISESSO))
      this.w_nMAXCAMPO = 1
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace TT_SESSO with alltrim(upper(this.oParentObject.w_AISESSO))+this.w_SPAZI
      this.w_cDate = this.oParentObject.w_AIDATNAS
      this.w_cAnno = ALLTRIM(STR(YEAR(this.w_cDate)))
      if LEN(ALLTRIM(STR(DAY(this.w_cDate ))))<2
        this.w_cGiorno = "0"+ALLTRIM(STR(DAY(this.w_cDate)))
      else
        this.w_cGiorno = ALLTRIM(STR(DAY(this.w_cDate)))
      endif
      if LEN(ALLTRIM(STR(MONTH(this.w_cDate ))))<2
        this.w_cMese = "0"+ALLTRIM(STR(MONTH(this.w_cDate)))
      else
        this.w_cMese = ALLTRIM(STR(MONTH(this.w_cDate)))
      endif
      Replace TTDATNAS with this.w_cGiorno+this.w_cMese+this.w_cAnno
      this.w_nTmp1 = len(alltrim(this.oParentObject.w_AICOMUNE))
      this.w_nMAXCAMPO = 40
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace TTLUONAS with alltrim(Upper(this.oParentObject.w_AICOMUNE))+this.w_SPAZI
      this.w_nTmp1 = len(alltrim(this.oParentObject.w_AISIGLA))
      this.w_nMAXCAMPO = 2
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace TTPRONAS with alltrim(upper(this.oParentObject.w_AISIGLA))+this.w_SPAZI
      this.w_nTmp1 = len(alltrim(this.oParentObject.w_AIRESCOM))
      this.w_nMAXCAMPO = 40
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace TTLOCTIT with alltrim(Upper(this.oParentObject.w_AIRESCOM))+this.w_SPAZI
      this.w_nTmp1 = len(alltrim(this.oParentObject.w_AIRESSIG))
      this.w_nMAXCAMPO = 2
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace TTPROTIT with alltrim(upper(this.oParentObject.w_AIRESSIG))+this.w_SPAZI
      this.w_nTmp1 = len(alltrim(this.oParentObject.w_AIINDIRI))
      this.w_nMAXCAMPO = 40
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace TTINDIRI with alltrim(Upper(this.oParentObject.w_AIINDIRI))+this.w_SPAZI
      this.w_SPAZI = "00000"
      this.w_nTmp1 = len(alltrim(this.oParentObject.w_AI___CAP))
      this.w_nMAXCAMPO = 5
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = SUBSTR(this.w_SPAZI,1,this.w_nTmp2)+ALLTRIM(this.oParentObject.w_AI___CAP)
      Replace TTCAPTIT with this.w_SPAZI
    else
      this.w_nTmp1 = len(alltrim(this.w_TTCOGTIT))
      this.w_nMAXCAMPO = 24
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace TTCOGTIT with alltrim(Upper(this.w_TTCOGTIT))+this.w_SPAZI
      this.w_nTmp1 = len(alltrim(this.w_TTNOMTIT))
      this.w_nMAXCAMPO = 20
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace TTNOMTIT with alltrim(Upper(this.w_TTNOMTIT))+this.w_SPAZI
      this.w_nTmp1 = len(alltrim(this.w_TT_SESSO))
      this.w_nMAXCAMPO = 1
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace TT_SESSO with alltrim(upper(this.w_TT_SESSO))+this.w_SPAZI
      this.w_cDate = this.w_TTDATNAS
      this.w_cAnno = ALLTRIM(STR(YEAR(this.w_cDate)))
      if LEN(ALLTRIM(STR(DAY(this.w_cDate ))))<2
        this.w_cGiorno = "0"+ALLTRIM(STR(DAY(this.w_cDate)))
      else
        this.w_cGiorno = ALLTRIM(STR(DAY(this.w_cDate)))
      endif
      if LEN(ALLTRIM(STR(MONTH(this.w_cDate ))))<2
        this.w_cMese = "0"+ALLTRIM(STR(MONTH(this.w_cDate)))
      else
        this.w_cMese = ALLTRIM(STR(MONTH(this.w_cDate)))
      endif
      Replace TTDATNAS with this.w_cGiorno+this.w_cMese+this.w_cAnno
      this.w_nTmp1 = len(alltrim(this.w_TTLUONAS))
      this.w_nMAXCAMPO = 40
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace TTLUONAS with alltrim(Upper(this.w_TTLUONAS))+this.w_SPAZI
      this.w_nTmp1 = len(alltrim(this.w_TTPRONAS))
      this.w_nMAXCAMPO = 2
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace TTPRONAS with alltrim(upper(this.w_TTPRONAS))+this.w_SPAZI
      this.w_nTmp1 = len(alltrim(this.w_TTLOCTIT))
      this.w_nMAXCAMPO = 40
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace TTLOCTIT with alltrim(Upper(this.w_TTLOCTIT))+this.w_SPAZI
      this.w_nTmp1 = len(alltrim(this.w_TTPROTIT))
      this.w_nMAXCAMPO = 2
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace TTPROTIT with alltrim(upper(this.w_TTPROTIT))+this.w_SPAZI
      this.w_nTmp1 = len(alltrim(this.w_TTINDIRI))
      this.w_nMAXCAMPO = 40
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace TTINDIRI with alltrim(Upper(this.w_TTINDIRI))+this.w_SPAZI
      this.w_SPAZI = "00000"
      this.w_nTmp1 = len(alltrim(this.w_TTCAPTIT))
      this.w_nMAXCAMPO = 5
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = SUBSTR(this.w_SPAZI,1,this.w_nTmp2)+ALLTRIM(this.w_TTCAPTIT)
      Replace TTCAPTIT with alltrim(this.w_TTCAPTIT)+this.w_SPAZI
    endif
    * --- DATI RISERVATI AL FORNITORE PERSONA NON FISICA
    Replace AZRAGAZI with SPACE(60)
    Replace AZLOCAZI with SPACE(40)
    Replace AZPROAZI with SPACE(2)
    Replace AZINDAZI with SPACE(35)
    Replace AZCAPAZI with "00000"
    Replace SELOCALI with SPACE(40)
    Replace SEPROVIN with SPACE(2)
    Replace SEINDIRI with SPACE(35)
    Replace SE__CAP with "00000"
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- SCRITTURA DEL RECORD DI TIPO 'B', DAL CAMPO 1 ... 20
    Replace TIPREC with "B"
    this.w_nTmp1 = len(alltrim(this.w_CDFAZI))
    this.w_nMAXCAMPO = 16
    this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
    this.w_SPAZI = space(this.w_nTmp2)
    Replace AZCOFAZI with alltrim(Upper(this.w_CDFAZI))+this.w_SPAZI
    Replace FILLER3 with SPACE(8)
    Replace SPAZIO4 with SPACE(3)
    Replace FILLER5 with SPACE(25)
    Replace SPAZIO6 with SPACE(20)
    * --- IDENTIFICATIVO PRODUTTORE DEL SOFTWARE, CI VUOLE LA PARTITA IVA DI ZUCCHETTI SPA
    this.w_IDPRODSW = "05006900962"
    this.w_nTmp1 = len(alltrim(this.w_IDPRODSW))
    this.w_nMAXCAMPO = 16
    this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
    this.w_SPAZI = space(this.w_nTmp2)
    Replace IDPRODSW with alltrim(this.w_IDPRODSW)+this.w_SPAZI
    * --- COMUNICAZIONE DI MANCATA CORRISPONDENZA DEI DATI DA TRASMETTERE
    Replace FLAGCONF with ALLTRIM(this.oParentObject.w_AIMANCOR)
    * --- DATI DEL CONTRIBUENTE
    * --- controllo se l'azienda e' formata da una persona fisica
    if this.w_PERSFIS <> "S"
      * --- Flag persona fisica non valorizzato
      this.w_nTmp1 = len(alltrim(this.w_DENFORN))
      this.w_nMAXCAMPO = 80
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace AZRAGAZI with alltrim(Upper(this.w_DENFORN))+this.w_SPAZI
      Replace TTCOGTIT with SPACE(24)
      Replace TTNOMTIT with SPACE(20)
    else
      * --- Flag persona fisica valorizzato
      Replace AZRAGAZI with SPACE(60)
      this.w_nTmp1 = len(alltrim(this.w_TTCOGTIT))
      this.w_nMAXCAMPO = 24
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace TTCOGTIT with alltrim(Upper(this.w_TTCOGTIT))+this.w_SPAZI
      this.w_nTmp1 = len(alltrim(this.w_TTNOMTIT))
      this.w_nMAXCAMPO = 20
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace TTNOMTIT with alltrim(Upper(this.w_TTNOMTIT))+this.w_SPAZI
    endif
    Replace AZPIVAZI with RIGHT("00000000000"+ALLTRIM(this.w_AZPARIVA),11)
    * --- PERIODO
    Replace DI_ANNO with ALLTRIM(this.w_AIANNIMP)
    Replace CODATT with ALLTRIM(this.oParentObject.w_AICODAT1)
    Replace CONTSEPA with IIF(this.oParentObject.w_AICONSEP="1",this.oParentObject.w_AICONSEP,"0")
    Replace COMSOCGRU with IIF(this.oParentObject.w_AISOCGRU="1",this.oParentObject.w_AISOCGRU,"0")
    Replace EVENECCE with IIF(this.oParentObject.w_AIEVEECC="1",this.oParentObject.w_AIEVEECC,"0")
    this.w_nTmp1 = len(alltrim(this.oParentObject.w_AICODFIS))
    this.w_nMAXCAMPO = 16
    this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
    this.w_SPAZI = space(this.w_nTmp2)
    Replace CODFISDIC with IIF(this.oParentObject.w_EDITADICH="S",this.oParentObject.w_AICODFIS+this.w_SPAZI,"                ")
    Replace PARIVADIC with RIGHT("00000000000"+IIF(this.oParentObject.w_EDITADICH="S",ALLTRIM(this.oParentObject.w_AICODIVA),"00000000000"),11)
    Replace CODCAR with right("00"+IIF(this.oParentObject.w_EDITADICH="S",this.oParentObject.w_AICODCAR,"00"),2)
    Replace FLCODF with ALLTRIM(this.oParentObject.w_AIFLCODF)
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valorizzo tutti gli importi
    Replace CD1_1 with "          0"
    if this.oParentObject.w_AITOTATT <> 0
      this.w_nTmp1 = Len(Alltrim(Str(this.oParentObject.w_AITOTATT)))
      this.w_nMAXCAMPO = 11
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace CD1_1 with this.w_SPAZI+ALLTRIM(STR(this.oParentObject.w_AITOTATT))
    endif
    Replace CD1_2 with "          0"
    if this.oParentObject.w_AITA_NIM <> 0
      this.w_nTmp1 = Len(Alltrim(Str(this.oParentObject.w_AITA_NIM)))
      this.w_nMAXCAMPO = 11
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace CD1_2 with this.w_SPAZI+ALLTRIM(STR(this.oParentObject.w_AITA_NIM))
    endif
    Replace CD1_3 with "          0"
    if this.oParentObject.w_AITA_ESE <> 0
      this.w_nTmp1 = Len(Alltrim(Str(this.oParentObject.w_AITA_ESE)))
      this.w_nMAXCAMPO = 11
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace CD1_3 with this.w_SPAZI+ALLTRIM(STR(this.oParentObject.w_AITA_ESE))
    endif
    Replace CD1_4 with "          0"
    if this.oParentObject.w_AITA_CIN <> 0
      this.w_nTmp1 = Len(Alltrim(Str(this.oParentObject.w_AITA_CIN)))
      this.w_nMAXCAMPO = 11
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace CD1_4 with this.w_SPAZI+ALLTRIM(STR(this.oParentObject.w_AITA_CIN))
    endif
    Replace CD1_5 with "          0"
    if this.oParentObject.w_AITA_CBS <> 0
      this.w_nTmp1 = Len(Alltrim(Str(this.oParentObject.w_AITA_CBS)))
      this.w_nMAXCAMPO = 11
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace CD1_5 with this.w_SPAZI+ALLTRIM(STR(this.oParentObject.w_AITA_CBS))
    endif
    Replace CD2_1 with "          0"
    if this.oParentObject.w_AITOTPAS <> 0
      this.w_nTmp1 = Len(Alltrim(Str(this.oParentObject.w_AITOTPAS)))
      this.w_nMAXCAMPO = 11
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace CD2_1 with this.w_SPAZI+ALLTRIM(STR(this.oParentObject.w_AITOTPAS))
    endif
    Replace CD2_2 with "          0"
    if this.oParentObject.w_AITP_NIM <> 0
      this.w_nTmp1 = Len(Alltrim(Str(this.oParentObject.w_AITP_NIM)))
      this.w_nMAXCAMPO = 11
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace CD2_2 with this.w_SPAZI+ALLTRIM(STR(this.oParentObject.w_AITP_NIM))
    endif
    Replace CD2_3 with "          0"
    if this.oParentObject.w_AITP_ESE <> 0
      this.w_nTmp1 = Len(Alltrim(Str(this.oParentObject.w_AITP_ESE)))
      this.w_nMAXCAMPO = 11
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace CD2_3 with this.w_SPAZI+ALLTRIM(STR(this.oParentObject.w_AITP_ESE))
    endif
    Replace CD2_4 with "          0"
    if this.oParentObject.w_AITP_CIN <> 0
      this.w_nTmp1 = Len(Alltrim(Str(this.oParentObject.w_AITP_CIN)))
      this.w_nMAXCAMPO = 11
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace CD2_4 with this.w_SPAZI+ALLTRIM(STR(this.oParentObject.w_AITP_CIN))
    endif
    Replace CD2_5 with "          0"
    if this.oParentObject.w_AITP_CBS <> 0
      this.w_nTmp1 = Len(Alltrim(Str(this.oParentObject.w_AITP_CBS)))
      this.w_nMAXCAMPO = 11
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace CD2_5 with this.w_SPAZI+ALLTRIM(STR(this.oParentObject.w_AITP_CBS))
    endif
    Replace CD3_1 with "          0"
    if this.oParentObject.w_AIIMPORO <> 0
      this.w_nTmp1 = Len(Alltrim(Str(this.oParentObject.w_AIIMPORO)))
      this.w_nMAXCAMPO = 11
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace CD3_1 with this.w_SPAZI+ALLTRIM(STR(this.oParentObject.w_AIIMPORO))
    endif
    Replace CD3_2 with "          0"
    if this.oParentObject.w_AIIVAORO <> 0
      this.w_nTmp1 = Len(Alltrim(Str(this.oParentObject.w_AIIVAORO)))
      this.w_nMAXCAMPO = 11
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace CD3_2 with this.w_SPAZI+ALLTRIM(STR(this.oParentObject.w_AIIVAORO))
    endif
    Replace CD3_3 with "          0"
    if this.oParentObject.w_AIIMPROT <> 0
      this.w_nTmp1 = Len(Alltrim(Str(this.oParentObject.w_AIIMPROT)))
      this.w_nMAXCAMPO = 11
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace CD3_3 with this.w_SPAZI+ALLTRIM(STR(this.oParentObject.w_AIIMPROT))
    endif
    Replace CD3_4 with "          0"
    if this.oParentObject.w_AIIVAROT <> 0
      this.w_nTmp1 = Len(Alltrim(Str(this.oParentObject.w_AIIVAROT)))
      this.w_nMAXCAMPO = 11
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace CD3_4 with this.w_SPAZI+ALLTRIM(STR(this.oParentObject.w_AIIVAROT))
    endif
    Replace CD4 with "          0"
    if this.oParentObject.w_AIIVAESI <> 0
      this.w_nTmp1 = Len(Alltrim(Str(this.oParentObject.w_AIIVAESI)))
      this.w_nMAXCAMPO = 11
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace CD4 with this.w_SPAZI+ALLTRIM(STR(this.oParentObject.w_AIIVAESI))
    endif
    Replace CD5 with "          0"
    if this.oParentObject.w_AIIVADET <> 0
      this.w_nTmp1 = Len(Alltrim(Str(this.oParentObject.w_AIIVADET)))
      this.w_nMAXCAMPO = 11
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace CD5 with this.w_SPAZI+ALLTRIM(STR(this.oParentObject.w_AIIVADET))
    endif
    Replace CD6_1 with "          0"
    if this.oParentObject.w_AIIVADOV <> 0
      this.w_nTmp1 = Len(Alltrim(Str(this.oParentObject.w_AIIVADOV)))
      this.w_nMAXCAMPO = 11
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace CD6_1 with this.w_SPAZI+ALLTRIM(STR(this.oParentObject.w_AIIVADOV))
    endif
    Replace CD6_2 with "          0"
    if this.oParentObject.w_AIIVACRE <> 0
      this.w_nTmp1 = Len(Alltrim(Str(this.oParentObject.w_AIIVACRE)))
      this.w_nMAXCAMPO = 11
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace CD6_2 with this.w_SPAZI+ALLTRIM(STR(this.oParentObject.w_AIIVACRE))
    endif
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- DATI DEL DICHIARANTE
    REPLACE FIRMA with this.oParentObject.w_AIFIRMAC
    if empty(this.oParentObject.w_AICOFINT)
      REPLACE VPCODFIS with Space(16)
    else
      this.w_nTmp1 = len(alltrim(this.oParentObject.w_AICOFINT))
      this.w_nMAXCAMPO = 16
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = space(this.w_nTmp2)
      Replace VPCODFIS with alltrim(Upper(this.oParentObject.w_AICOFINT)) + this.w_SPAZI
    endif
    Replace CODCAF with "00000"
    if NOT EMPTY(this.oParentObject.w_AIALBCAF)
      this.w_nTmp1 = Len(Alltrim(this.oParentObject.w_AIALBCAF))
      this.w_nMAXCAMPO = 5
      this.w_nTmp2 = this.w_nMAXCAMPO-this.w_nTmp1
      this.w_SPAZI = REPLICATE("0",this.w_nTmp2)
      Replace CODCAF with this.w_SPAZI+ALLTRIM(this.oParentObject.w_AIALBCAF)
    endif
    Replace IMPTRACON with this.oParentObject.w_AIINVCON
    Replace IMPTRASOG with this.oParentObject.w_AIINVSOG
    * --- Data presentazione
    Replace DATIMPTRA with "00000000"
    this.w_cDate = this.oParentObject.w_AIDATIMP
    if NOT EMPTY(this.w_cDate)
      this.w_cAnno = ALLTRIM(STR(YEAR(this.w_cDate)))
      if LEN(ALLTRIM(STR(DAY(this.w_cDate ))))<2
        this.w_cGiorno = "0"+ALLTRIM(STR(DAY(this.w_cDate)))
      else
        this.w_cGiorno = ALLTRIM(STR(DAY(this.w_cDate)))
      endif
      if LEN(ALLTRIM(STR(MONTH(this.w_cDate ))))<2
        this.w_cMese = "0"+ALLTRIM(STR(MONTH(this.w_cDate)))
      else
        this.w_cMese = ALLTRIM(STR(MONTH(this.w_cDate)))
      endif
      Replace DATIMPTRA with this.w_cGiorno+this.w_cMese+this.w_cAnno
    endif
    REPLACE FIRMAINT with this.oParentObject.w_AIFIRMA
    * --- SPAZIO NON UTILIZZATO
    REPLACE FILLER741 with SPACE(250)
    REPLACE FILLER742 with SPACE(250)
    REPLACE FILLER743 with SPACE(250)
    REPLACE FILLER744 with SPACE(250)
    REPLACE FILLER745 with SPACE(250)
    REPLACE FILLER746 with SPACE(160)
    * --- SPAZIO NON DISPONIBILE
    REPLACE SPASETEL with SPACE(20)
    * --- RISERVATO AI CENTRI DI SERVIZIO
    REPLACE FILLER76 with SPACE(8)
    REPLACE FILLER77 with SPACE(8)
    REPLACE FILLER78 with " "
    REPLACE FILLER79 with " "
    * --- CARATTERI DI CONTROLLO DEL RECORD
    REPLACE FILLER80 with "A"
    REPLACE FILLER81 with CHR(13)+CHR(10)
  endproc


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcolo Importi Maschera
    * --- Lettura Periodicit� IVA
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZTIPDEN"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZTIPDEN;
        from (i_cTable) where;
            AZCODAZI = this.oParentObject.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TIPDEN = NVL(cp_ToDate(_read_.AZTIPDEN),cp_NullValue(_read_.AZTIPDEN))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_TIPDEN="T"
      this.w_MAXPER = 4
    else
      this.w_MAXPER = 12
    endif
    * --- Inizia Elaborazione ,Cicla sulle Tutte le Attivita' 
    this.w_KEYATT = "     "
    VQ_EXEC("QUERY\GSAR1BCD.VQR",this,"ATTIVITA")
    if USED("ATTIVITA")
      this.oParentObject.w_AITOTATT = 0
      this.oParentObject.w_AITA_NIM = 0
      this.oParentObject.w_AITA_ESE = 0
      this.oParentObject.w_AITA_CIN = 0
      this.oParentObject.w_AITA_CBS = 0
      this.oParentObject.w_AITOTPAS = 0
      this.oParentObject.w_AITP_NIM = 0
      this.oParentObject.w_AITP_ESE = 0
      this.oParentObject.w_AITP_CIN = 0
      this.oParentObject.w_AITP_CBS = 0
      this.oParentObject.w_AIIMPORO = 0
      this.oParentObject.w_AIIVAORO = 0
      this.oParentObject.w_AIIVAESI = 0
      this.oParentObject.w_AIIVADET = 0
      this.oParentObject.w_AIIVADOV = 0
      this.oParentObject.w_AIIVACRE = 0
      this.w_VPIMPOR7 = 0
      this.oParentObject.w_AIIMPROT = 0
      this.oParentObject.w_AIIVAROT = 0
      * --- Cicla Sui Cursori delle Queries e Calcola i valori per il Temporaneo di Elaborazione
      SELECT ATTIVITA
      GO TOP
      SCAN FOR NOT EMPTY(NVL(CODATT," "))
      * --- %Prorata (Attenzione: ricalcola per le Attivita')
      this.w_KEYATT = CODATT
      this.w_FLPROR = NVL(PERPRO," ")
      this.w_FLAGRI = NVL(FLAGRI, " ")
      this.w_PERPRO = 0
      this.w_TESTPROR = "N"
      if this.w_FLPROR="S"
        * --- Read from PRO_RATA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PRO_RATA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRO_RATA_idx,2],.t.,this.PRO_RATA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "AIPERPRO"+;
            " from "+i_cTable+" PRO_RATA where ";
                +"AICODATT = "+cp_ToStrODBC(this.w_KEYATT);
                +" and AI__ANNO = "+cp_ToStrODBC(this.w_AIANNIMP);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            AIPERPRO;
            from (i_cTable) where;
                AICODATT = this.w_KEYATT;
                and AI__ANNO = this.w_AIANNIMP;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PERPRO = NVL(cp_ToDate(_read_.AIPERPRO),cp_NullValue(_read_.AIPERPRO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_Rows=0
          * --- Se non trova prorata relativo all'anno lo metto a 100 cos� rimane invariato.
          this.w_PERPRO = 100
        else
          this.w_TESTPROR = "S"
        endif
      else
        this.w_PERPRO = 100
      endif
      this.w_NUMPER = 1
      do while this.w_NUMPER<=this.w_MAXPER
        * --- Azzera dettagli delle Singole Attivita'
        VQ_EXEC("QUERY\GSAR_BCD.VQR",this,"SALDIIVA")
        if USED("SALDIIVA")
          this.w_TOTATT = 0
          this.w_TA_NIM = 0
          this.w_TA_ESE = 0
          this.w_TA_CIN = 0
          this.w_TA_CBS = 0
          this.w_TOTPAS = 0
          this.w_TP_NIM = 0
          this.w_TP_ESE = 0
          this.w_TP_CIN = 0
          this.w_TP_CBS = 0
          this.w_IMPORO = 0
          this.w_IVAORO = 0
          this.w_IVAESI = 0
          this.w_IVADET = 0
          this.w_IVADOV = 0
          this.w_IVACRE = 0
          this.w_IMPROT = 0
          this.w_IVAROT = 0
          SELECT SALDIIVA
          GO TOP
          SCAN FOR NVL(TIPREG," ") $ "VACE" AND NVL(NUMREG, 0)<>0 AND NOT EMPTY(NVL(CODIVA, " "))
          * --- Per Dettagli Liquidazione
          this.w_TIPREG = TIPREG
          this.w_PERCOM = NVL(PERCOM, 0)
          this.w_TIPAGR = NVL(TIPAGR, "N")
          * --- Operazioni Attive (Codici IVA Normali ed esclusi V.A. e No Acquisti/Importazioni)
          if TIPREG<>"A" AND NVL(FLVAFF,"N")<> "E"
            * --- Totale operazioni attive
            this.w_TOTATT = this.w_TOTATT + IIF(NVL(DATANP, 0) <> 3 AND NVL(DATANP, 0) <> 6 AND NVL(DATANP, 0) <> 5 AND NVL(DATANP, 0) <> 8 AND AUTCOM<>"S", (NVL(IMPSTA,0)+NVL(IMPSEG,0))-NVL(IMPPRE,0), 0)
            * --- Operazioni non imponibili
            this.w_TA_NIM = this.w_TA_NIM + IIF(NVL(DATANA,0) = 1 AND AUTCOM<>"S", (NVL(IMPSTA,0)+NVL(IMPSEG,0))-NVL(IMPPRE,0), 0)
            * --- Operazioni esenti
            this.w_TA_ESE = this.w_TA_ESE + IIF(NVL(DATANA,0) = 2 AND AUTCOM<>"S", (NVL(IMPSTA,0)+NVL(IMPSEG,0))-NVL(IMPPRE,0), 0)
            * --- Cessioni intracomunitari di beni
            this.w_TA_CIN = this.w_TA_CIN + IIF(NVL(DATANA,0) = 3 AND AUTCOM<>"S", (NVL(IMPSTA,0)+NVL(IMPSEG,0))-NVL(IMPPRE,0), 0)
            * --- Controllo se la comunicazione annuale dati IVA � del 2010 o superiori
            *     in questo caso calcolo il campo "Cessioni beni strumentali"
            if this.oParentObject.w_ANNO>=2010
              this.w_TA_CBS = this.w_TA_CBS + IIF(NVL(DATANP, 0) <> 3 AND NVL(DATANP, 0) <> 6 AND NVL(DATANP, 0) <> 5 AND NVL(DATANP, 0) <> 8 AND NVL(TIPBEN,"N") $"AS" AND AUTCOM<>"S", (NVL(IMPSTA,0)+NVL(IMPSEG,0))-NVL(IMPPRE,0), 0)
            endif
            * --- IVA Esigibile a Debito del Periodo
            this.w_IVAESI = this.w_IVAESI + IIF(IVSPLPAY="S" AND TIPREG="V",0, ((NVL(IVASTA,0)+NVL(IVASEG,0)+NVL(IVAIND,0))-(NVL(IVAPRE,0)+NVL(IVAFAD,0))))
            * --- IVA Detratta
            if this.w_TIPREG $ "VC" AND (NVL(IMPSTA,0)+NVL(IMPSEG,0)+NVL(IMPIND,0))-(NVL(IMPPRE,0)+NVL(IMPFAD,0))<>0 AND this.w_FLAGRI="P" AND this.w_PERCOM>0
              * --- Se Tipo Reg.IVA Vendite o Corrisp.Scorporo e Attivita Produttore Agricolo e Aliq.IVA Compensazione, calcola IVA Detraibile 
              this.w_IVADET = this.w_IVADET + cp_ROUND(((((NVL(IMPSTA,0)+NVL(IMPSEG,0)+NVL(IMPIND,0))-(NVL(IMPPRE,0)+NVL(IMPFAD,0))) * this.w_PERCOM) / 100), 2)
            endif
          endif
          * --- Operazioni Passive (Codici IVA Normali)
          if TIPREG="A" AND NVL(FLVAFF,"N")="N"
            * --- Totale operazioni passive
            this.w_TOTPAS = this.w_TOTPAS + (NVL(IMPSTA,0)+NVL(IMPSEG,0))-NVL(IMPPRE,0)
            * --- Acquisti non imponibili
            this.w_TP_NIM = this.w_TP_NIM + IIF(NVL(DATANP, 0) = 1, (NVL(IMPSTA,0)+NVL(IMPSEG,0))-NVL(IMPPRE,0), 0)
            * --- Acquisti esenti
            this.w_TP_ESE = this.w_TP_ESE + IIF(NVL(DATANP, 0) = 2, (NVL(IMPSTA,0)+NVL(IMPSEG,0))-NVL(IMPPRE,0), 0)
            * --- Acquisti intracomunitari di beni
            this.w_TP_CIN = this.w_TP_CIN + IIF(NVL(DATANP, 0) = 3 OR NVL(DATANP, 0) = 5 or NVL(DATANP," ") = 8, (NVL(IMPSTA,0)+NVL(IMPSEG,0))-NVL(IMPPRE,0), 0)
            * --- Controllo se la comunicazione annuale dati IVA � del 2010 o superiori
            *     in questo caso calcolo il campo "Acquisti beni strumentali"
            if this.oParentObject.w_ANNO>=2010
              this.w_TP_CBS = this.w_TP_CBS + IIF(NVL(TIPBEN,"N") $"AS",(NVL(IMPSTA,0)+NVL(IMPSEG,0))-NVL(IMPPRE,0),0)
            endif
            * --- Imponibili Importazione ORO e ARGENTO
            this.w_IMPORO = this.w_IMPORO + IIF(NVL(DATANP, 0) = 4 OR NVL(DATANP, 0) = 5, (NVL(IMPSTA,0)+NVL(IMPSEG,0))-NVL(IMPPRE,0), 0)
            * --- Imposta Importazioni ORO e ARGENTO
            this.w_IVAORO = this.w_IVAORO + IIF(NVL(DATANP, 0) = 4 OR NVL(DATANP, 0) = 5, (NVL(IVASTA,0)+NVL(IVISTA,0)+NVL(IVASEG,0)+NVL(IVISEG,0))-(NVL(IVAPRE,0)+NVL(IVIPRE,0)), 0)
            * --- Imponibili Importazione Rottami
            this.w_IMPROT = this.w_IMPROT + IIF(NVL(DATANP, 0) = 7 or NVL(DATANP, 0) = 8 , (NVL(IMPSTA,0)+NVL(IMPSEG,0))-NVL(IMPPRE,0), 0)
            * --- Imposta Importazioni Rottami
            this.w_IVAROT = this.w_IVAROT + IIF(NVL(DATANP, 0) = 7 or NVL(DATANP, 0) = 8, (NVL(IVASTA,0)+NVL(IVASEG,0))-(NVL(IVAPRE,0)), 0)
            * --- IVA Detraibile a Credito del Periodo
            if this.w_FLAGRI<>"P" OR this.w_TIPAGR<>"A"
              * --- Se Attivita no Produttore Agricolo o Prod.Agricolo e Aliq.IVA non Agevolata)
              this.w_IVADET = this.w_IVADET + ((NVL(IVASTA,0)+NVL(IVASEG,0)+NVL(IVAIND,0))-(NVL(IVAPRE,0)+NVL(IVAFAD,0)))
            endif
          endif
          SELECT SALDIIVA
          ENDSCAN
          * --- Se Gestito Prorata Storna IVA Detraibile a Credito per la % Prorata (Per ciascuna Attivita')
          if this.w_FLPROR="S" AND this.w_IVADET<>0 AND this.w_PERPRO<>100
            this.w_IVADET = cp_ROUND((this.w_IVADET * this.w_PERPRO) / 100, g_PERPVL)
          endif
          * --- Importi definiti a 2 decimali
          this.oParentObject.w_AITOTATT = this.oParentObject.w_AITOTATT + cp_ROUND(this.w_TOTATT, 2)
          this.oParentObject.w_AITA_NIM = this.oParentObject.w_AITA_NIM + cp_ROUND(this.w_TA_NIM, 2)
          this.oParentObject.w_AITA_ESE = this.oParentObject.w_AITA_ESE + cp_ROUND(this.w_TA_ESE, 2)
          this.oParentObject.w_AITA_CIN = this.oParentObject.w_AITA_CIN + cp_ROUND(this.w_TA_CIN, 2)
          this.oParentObject.w_AITA_CBS = this.oParentObject.w_AITA_CBS + cp_ROUND(this.w_TA_CBS, 2)
          this.oParentObject.w_AITOTPAS = this.oParentObject.w_AITOTPAS + cp_ROUND(this.w_TOTPAS, 2)
          this.oParentObject.w_AITP_NIM = this.oParentObject.w_AITP_NIM + cp_ROUND(this.w_TP_NIM, 2)
          this.oParentObject.w_AITP_ESE = this.oParentObject.w_AITP_ESE + cp_ROUND(this.w_TP_ESE, 2)
          this.oParentObject.w_AITP_CIN = this.oParentObject.w_AITP_CIN + cp_ROUND(this.w_TP_CIN, 2)
          this.oParentObject.w_AITP_CBS = this.oParentObject.w_AITP_CBS + cp_ROUND(this.w_TP_CBS, 2)
          this.oParentObject.w_AIIMPORO = this.oParentObject.w_AIIMPORO + cp_ROUND(this.w_IMPORO, 2)
          this.oParentObject.w_AIIVAORO = this.oParentObject.w_AIIVAORO + cp_ROUND(this.w_IVAORO, 2)
          this.oParentObject.w_AIIVAESI = this.oParentObject.w_AIIVAESI + cp_ROUND(this.w_IVAESI, 2)
          this.oParentObject.w_AIIVADET = this.oParentObject.w_AIIVADET + cp_ROUND(this.w_IVADET, 2)
          this.oParentObject.w_AIIVADOV = this.oParentObject.w_AIIVADOV + cp_ROUND(this.w_IVADOV, 2)
          this.oParentObject.w_AIIVACRE = this.oParentObject.w_AIIVACRE + cp_ROUND(this.w_IVACRE, 2)
          * --- IVA Del Periodo (VP5 - VP6 ; se Positiva a Debito altrimenti a Credito)
          this.w_VPIMPOR7 = this.oParentObject.w_AIIVAESI - this.oParentObject.w_AIIVADET
          this.oParentObject.w_AIIVADOV = IIF( this.w_VPIMPOR7>0 , this.w_VPIMPOR7, 0)
          this.oParentObject.w_AIIVACRE = IIF( this.w_VPIMPOR7<0 , ABS(this.w_VPIMPOR7) ,0 )
          this.oParentObject.w_AIIMPROT = this.oParentObject.w_AIIMPROT + cp_ROUND(this.w_IMPROT, 2)
          this.oParentObject.w_AIIVAROT = this.oParentObject.w_AIIVAROT + cp_ROUND(this.w_IVAROT, 2)
        endif
        this.w_NUMPER = this.w_NUMPER+1
      enddo
      SELECT ATTIVITA
      ENDSCAN
      * --- Dalle operazioni attive devono essere tolte le registrazioni di tipo autofattura (che movimentano il registro vendite)
      this.w_INIPER = cp_CharToDate("01/01/"+ this.w_AIANNIMP )
      this.w_FINPER = cp_CharToDate("31/12/"+ this.w_AIANNIMP )
      this.w_KEYATT = "     "
      VQ_EXEC("QUERY\GSCG_BAU.VQR",this,"AUTOFATT")
      this.w_IMPAUT = 0
      this.w_IMPAUT_CBS = 0
      if USED("AUTOFATT")
        SELECT AUTOFATT
        GO TOP
        SCAN
        this.w_IMPONI = NVL(IMPONI, 0)
        this.w_IMPONICBS = NVL(IMPONICBS, 0)
        this.w_VALNAZ = NVL(VALNAZ, g_PERVAL)
        * --- Se La valuta dell'esercizio della Riga e' diversa da quella dell'Esercizio Attuale, converte
        * --- Prevediamo solo LIRE o EURO
        if this.w_VALNAZ<>g_PERVAL AND g_CAOEUR<>0
          do case
            case this.w_VALNAZ=g_CODLIR
              this.w_IMPONI = cp_ROUND(this.w_IMPONI/g_CAOEUR, g_PERPVL)
              this.w_IMPONICBS = cp_ROUND(this.w_IMPONICBS/g_CAOEUR, g_PERPVL)
            case this.w_VALNAZ=g_CODEUR
              this.w_IMPONI = cp_ROUND(this.w_IMPONI*g_CAOEUR, g_PERPVL)
              this.w_IMPONICBS = cp_ROUND(this.w_IMPONICBS*g_CAOEUR, g_PERPVL)
          endcase
        endif
        this.w_IMPAUT = this.w_IMPAUT+this.w_IMPONI
        this.w_IMPAUT_CBS = this.w_IMPAUT_CBS+this.w_IMPONICBS
        SELECT AUTOFATT
        ENDSCAN
        * --- Chiusura Cursore
        SELECT AUTOFATT
        USE
      endif
      * --- Importi definiti all'Unit� di Euro
      this.oParentObject.w_AITOTATT = cp_ROUND(this.oParentObject.w_AITOTATT - this.w_IMPAUT , 0)
      this.oParentObject.w_AITA_NIM = cp_ROUND(this.oParentObject.w_AITA_NIM, 0)
      this.oParentObject.w_AITA_ESE = cp_ROUND(this.oParentObject.w_AITA_ESE, 0)
      this.oParentObject.w_AITA_CIN = cp_ROUND(this.oParentObject.w_AITA_CIN, 0)
      this.oParentObject.w_AITA_CBS = cp_ROUND(this.oParentObject.w_AITA_CBS- IIF(this.oParentObject.w_ANNO>=2010,this.w_IMPAUT_CBS,0), 0)
      this.oParentObject.w_AITOTPAS = cp_ROUND(this.oParentObject.w_AITOTPAS, 0)
      this.oParentObject.w_AITP_NIM = cp_ROUND(this.oParentObject.w_AITP_NIM, 0)
      this.oParentObject.w_AITP_ESE = cp_ROUND(this.oParentObject.w_AITP_ESE, 0)
      this.oParentObject.w_AITP_CIN = cp_ROUND(this.oParentObject.w_AITP_CIN, 0)
      this.oParentObject.w_AITP_CBS = cp_ROUND(this.oParentObject.w_AITP_CBS, 0)
      this.oParentObject.w_AIIMPORO = cp_ROUND(this.oParentObject.w_AIIMPORO, 0)
      this.oParentObject.w_AIIVAORO = cp_ROUND(this.oParentObject.w_AIIVAORO, 0)
      this.oParentObject.w_AIIVAESI = cp_ROUND(this.oParentObject.w_AIIVAESI, 0)
      this.oParentObject.w_AIIVADET = cp_ROUND(this.oParentObject.w_AIIVADET, 0)
      this.oParentObject.w_AIIVADOV = cp_ROUND(this.oParentObject.w_AIIVADOV, 0)
      this.oParentObject.w_AIIVACRE = cp_ROUND(this.oParentObject.w_AIIVACRE, 0)
      this.w_VPIMPOR7 = cp_ROUND(this.w_VPIMPOR7, 0)
      if USED("SALDIIVA")
        SELECT SALDIIVA
        USE
      endif
    endif
    if USED("ATTIVITA")
      SELECT ATTIVITA
      USE
    endif
    ah_ErrorMsg("Terminata procedura calcolo importi","!","")
  endproc


  procedure Page_8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- CREAZIONE DEL RECORD DI TIPO Z
    Replace TIPREC with "Z"
    Replace FILLER2 with SPACE(14)
    Replace NUMRECB with "000000001"
    Replace FILLER41 with SPACE(254)
    Replace FILLER42 with SPACE(254)
    Replace FILLER43 with SPACE(254)
    Replace FILLER44 with SPACE(254)
    Replace FILLER45 with SPACE(254)
    Replace FILLER46 with SPACE(254)
    Replace FILLER47 with SPACE(254)
    Replace FILLER48 with SPACE(95)
    Replace FILLER5 with "A"
    Replace FILLER6 with CHR(13)+CHR(10)
    if this.pParam="GENE"
      this.w_LTmp = .T.
      if Isinoltel()
        if FILE(this.oParentObject.w_NOMEFILE)
          this.w_LTmp = ah_YesNo("Il file %1 � gi� presente: sovrascrivo?",,alltrim(this.oParentObject.w_NOMEFILE))
        endif
        if this.w_LTmp
          this.w_MESS = Gencomiva(this.oParentObject.w_NOMEFILE,"tip_rec_a","tip_rec_b","tip_rec_z",.F.,this.oParentObject.w_ANNO)
          if Empty(this.w_MESS)
            this.w_MESS = "Scrittura del file %1 terminata con successo%0Si vuole rendere definitivo l'inoltro telematico?%0"
            this.w_OK = ah_YesNo(this.w_MESS,, ALLTRIM(this.oParentObject.w_NOMEFILE))
            if this.w_OK
              if this.oParentObject.cFunction="Edit" or this.oParentObject.cFunction="Load"
                this.oParentObject.w_AIINOLTRO = "S"
                * --- In variazione forzo il salvataggio del record
                if this.oParentObject.cFunction="Edit" 
                  this.oParentObject.bUpdated=.T.
                endif
                this.oParentObject.EcpSave()
              endif
            endif
          else
            AH_ErrorMsg(this.w_MESS,48,"")
          endif
        endif
      else
        AH_ErrorMsg("Attenzione, funzionalit� di inoltro telematico non attivata",48,"")
      endif
      * --- Chiudo cursori
      if USED("tip_rec_a")
        SELECT "tip_rec_a"
        USE
      endif
      if USED("tip_rec_b")
        SELECT "tip_rec_b"
        USE
      endif
      if USED("tip_rec_z")
        SELECT "tip_rec_z"
        USE
      endif
    else
      * --- Stampa PDF
      this.Page_10()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  procedure Page_9
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Variabili globali
    * --- Variabile che contiene il nome del file e il percorso
    * --- Indica l'anno dell'IVA considerata
    this.w_AIANNIMP = ALLTRIM(STR(this.oParentObject.w_AIANNIMP))
    * --- Firma intermediario
    * --- Variabili presenti nella seconda pagina della maschera
    * --- Importi da Calcolare
    * --- Variabili locali per la lettura dei dati azienda
    * --- variabili locali per la lettura dei dati dei titolari
    * --- variabili locali per la lettura dei dati delle sedi: Sede Legale, Domicilio Fiscale
    * --- variabili locali per la lettura dei dati dell'IVA periodica IVA_PERI per il rigo VP17
    * --- Variabili per calcolo pro-rata
    * --- var locali
    * --- Variabile che controllo se ho specificato il codice fiscale dell'intermediario sulla maschera
  endproc


  procedure Page_10
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Leggo il codice fiscale dell'azienda
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZCOFAZI,AZPERAZI"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZCOFAZI,AZPERAZI;
        from (i_cTable) where;
            AZCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODFISAZI = NVL(cp_ToDate(_read_.AZCOFAZI),cp_NullValue(_read_.AZCOFAZI))
      this.w_PERFIS = NVL(cp_ToDate(_read_.AZPERAZI),cp_NullValue(_read_.AZPERAZI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_PERFIS="S"
      * --- Read from TITOLARI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.TITOLARI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TITOLARI_idx,2],.t.,this.TITOLARI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TTNOMTIT,TTCOGTIT"+;
          " from "+i_cTable+" TITOLARI where ";
              +"TTCODAZI = "+cp_ToStrODBC(i_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TTNOMTIT,TTCOGTIT;
          from (i_cTable) where;
              TTCODAZI = i_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_NOMPERF = NVL(cp_ToDate(_read_.TTNOMTIT),cp_NullValue(_read_.TTNOMTIT))
        this.w_COGPERF = NVL(cp_ToDate(_read_.TTCOGTIT),cp_NullValue(_read_.TTCOGTIT))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_DENOMAZI = this.w_COGPERF+" "+this.w_NOMPERF
    endif
    * --- Preparo il cursore __TMP__ per stapare su PDF.I dati saranno elaborati nella CP_FFDF
    CREATE CURSOR __TMP__ ;
    (DENOMAZI C(80),CODFISAZI C(16),AIANNIMP C(4), AIPARIVA C(16), AICODATT C(6), ;
    AICONSEP C(1), AISOCGRU C(1), AIEVEECC C(1), AICODFIS C(16), AICODCAR C(1), AICODIVA C(11), AITOTATT N(18,0), ;
    AITA_NIM N(18,0), AITA_ESE N(18,0), AITA_CIN N(18,0),AITA_CBS N(18,0),AITOTPAS N(18,0), AITP_NIM N(18,0), AITP_ESE N(18,0), AITP_CIN N(18,0), AITP_CBS N(18,0), ;
    AIIMPORO N(18,0),AIIVAORO N(18,0),AIIMPROT N(18,0),AIIVAROT N(18,0),AIIVAESI N(18,0),AIIVADET N(18,0),AIIVADOV N(18,0),AIIVACRE N(18,0), AICOFINT C(16), ;
    AIALBCAF C(5), AIINVCON C(1),AIINVSOG C(1),AIDATIMG C(2), AIDATIMM C(2),AIDATIMA C(4))
    * --- Definisco Array bidimensionale da passare alla funzione CP_FFDF per la creazione del file FDF
    DECLARE ARRFDF (8,2)
    * --- Riempio l'Array ARRFDF con i nomi dei campi che nel Modello PDF sono rappresentati come N, N>1, celle distinte.
    ARRFDF (1,1) = "AIANNIMP"
    ARRFDF (1,2) = 4
    ARRFDF (2,1) = "AIPARIVA"
    ARRFDF (2,2) = 16
    ARRFDF (3,1) = "AICODATT"
    ARRFDF (3,2) = 6
    ARRFDF (4,1) = "AICODFIS"
    ARRFDF (4,2) = 16
    ARRFDF (5,1) = "AICODIVA"
    ARRFDF (5,2) = 11
    ARRFDF (6,1) = "AICOFINT"
    ARRFDF (6,2) = 16
    ARRFDF (7,1) = "AIALBCAF"
    ARRFDF (7,2) = 5
    ARRFDF (8,1) = "CODFISAZI"
    ARRFDF (8,2) = 16
    * --- Memorizzo i dati nel cursore __TMP__
    INSERT INTO __TMP__ VALUES ;
    (iif(this.w_PERFIS="S",upper(this.w_DENOMAZI),upper(g_RAGAZI)),this.oParentObject.w_CODFIS,this.w_AIANNIMP , this.oParentObject.w_AIPARIVA , this.oParentObject.w_AICODAT1 , ;
    iif(this.oParentObject.w_AICONSEP="1","X"," ") , iif(this.oParentObject.w_AISOCGRU="1","X"," ") ,iif(this.oParentObject.w_AIEVEECC="1","X"," ") , IIF(this.oParentObject.w_EDITADICH="S",this.oParentObject.w_AICODFIS,"                ") , ;
    IIF(this.oParentObject.w_EDITADICH="S",this.oParentObject.w_AICODCAR,"  "), IIF(this.oParentObject.w_EDITADICH="S",this.oParentObject.w_AICODIVA,"           ") ,this.oParentObject.w_AITOTATT, ;
    this.oParentObject.w_AITA_NIM , this.oParentObject.w_AITA_ESE , this.oParentObject.w_AITA_CIN , this.oParentObject.w_AITA_CBS, this.oParentObject.w_AITOTPAS, this.oParentObject.w_AITP_NIM , this.oParentObject.w_AITP_ESE , ;
    this.oParentObject.w_AITP_CIN,this.oParentObject.w_AITP_CBS,this.oParentObject.w_AIIMPORO,this.oParentObject.w_AIIVAORO,this.oParentObject.w_AIIMPROT,this.oParentObject.w_AIIVAROT,this.oParentObject.w_AIIVAESI,this.oParentObject.w_AIIVADET,this.oParentObject.w_AIIVADOV,this.oParentObject.w_AIIVACRE,this.oParentObject.w_AICOFINT, ;
    this.oParentObject.w_AIALBCAF,iif(this.oParentObject.w_AIINVCON="1","X"," "),iif(this.oParentObject.w_AIINVSOG="1","X"," "), ;
    iif(empty(this.oParentObject.w_AIDATIMP),"",right("0"+alltrim(str(day(this.oParentObject.w_AIDATIMP))),2)), ;
    iif(empty(this.oParentObject.w_AIDATIMP),"",right("0"+alltrim(str(month(this.oParentObject.w_AIDATIMP))),2)),iif(empty(this.oParentObject.w_AIDATIMP),"",alltrim(str(year(this.oParentObject.w_AIDATIMP)))))
    * --- Verifico se la comunicazione annuale IVA � del 2010 oppure precendente
    do case
      case this.oParentObject.w_ANNO>=2011
        * --- Elabora la stampa in formato Grafico PDF
        * --- Funzione di creazione dell' FDF
        result1=cp_ffdf(" ","MODIVA2010.PDF",this," ",@ARRFDF)
        * --- Funzione di visualizzazione su PDF
        result2=gsar_bsf(tempadhoc()+"\MODIVA20101.FDF", "OPEN", " ")
      case this.oParentObject.w_ANNO=2010
        * --- Elabora la stampa in formato Grafico PDF
        * --- Funzione di creazione dell' FDF
        result1=cp_ffdf(" ","MODIVA2009.PDF",this," ",@ARRFDF)
        * --- Funzione di visualizzazione su PDF
        result2=gsar_bsf(tempadhoc()+"\MODIVA20091.FDF", "OPEN", " ")
      otherwise
        * --- Elabora la stampa in formato Grafico PDF
        * --- Funzione di creazione dell' FDF
        result1=cp_ffdf(" ","MODIVA2008.PDF",this," ",@ARRFDF)
        * --- Funzione di visualizzazione su PDF
        result2=gsar_bsf(tempadhoc()+"\MODIVA20081.FDF", "OPEN", " ")
    endcase
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,8)]
    this.cWorkTables[1]='ATTIDETT'
    this.cWorkTables[2]='ATTIMAST'
    this.cWorkTables[3]='AZIENDA'
    this.cWorkTables[4]='MOD_COAN'
    this.cWorkTables[5]='SEDIAZIE'
    this.cWorkTables[6]='TITOLARI'
    this.cWorkTables[7]='IVA_PERI'
    this.cWorkTables[8]='PRO_RATA'
    return(this.OpenAllTables(8))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
