* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_acm                                                        *
*              Causali magazzino                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_80]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-16                                                      *
* Last revis.: 2014-11-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_acm"))

* --- Class definition
define class tgsma_acm as StdForm
  Top    = 1
  Left   = 5

  * --- Standard Properties
  Width  = 656
  Height = 316+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-11-18"
  HelpContextID=158696599
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=39

  * --- Constant Properties
  CAM_AGAZ_IDX = 0
  CAM_AGAZ_IDX = 0
  TRADCAMA_IDX = 0
  TIPITRAN_IDX = 0
  CPAR_DEF_IDX = 0
  cFile = "CAM_AGAZ"
  cKeySelect = "CMCODICE"
  cKeyWhere  = "CMCODICE=this.w_CMCODICE"
  cKeyWhereODBC = '"CMCODICE="+cp_ToStrODBC(this.w_CMCODICE)';

  cKeyWhereODBCqualified = '"CAM_AGAZ.CMCODICE="+cp_ToStrODBC(this.w_CMCODICE)';

  cPrg = "gsma_acm"
  cComment = "Causali magazzino"
  icon = "anag.ico"
  cAutoZoom = 'GSMA0ACM'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CMCODICE = space(5)
  w_CMDESCRI = space(35)
  w_CHIAVE = space(10)
  w_PCAUMAG = space(5)
  w_CMCAUCOL = space(5)
  w_CMFLCLFR = space(1)
  w_CMFLNDOC = space(1)
  w_CMMTCARI = space(1)
  w_CMFLLAUE = space(1)
  w_CMFLCOMM = space(1)
  w_CMFLCASC = space(1)
  o_CMFLCASC = space(1)
  w_CMFLORDI = space(1)
  o_CMFLORDI = space(1)
  w_CMFLIMPE = space(1)
  o_CMFLIMPE = space(1)
  w_CMFLRISE = space(1)
  w_FLMAG = space(10)
  w_CMFLAVAL = space(1)
  o_CMFLAVAL = space(1)
  w_CMVARVAL = space(1)
  o_CMVARVAL = space(1)
  w_CMFLELGM = space(1)
  o_CMFLELGM = space(1)
  w_CMAGGVAL = space(1)
  o_CMAGGVAL = space(1)
  w_CMFLRIVA = space(1)
  o_CMFLRIVA = space(1)
  w_CMCRIVAL = space(2)
  w_CMSEQCAL = 0
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_CMDTINVA = ctod('  /  /  ')
  w_DESCOL = space(35)
  w_CMDTOBSO = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_CMNATTRA = space(3)
  w_TIDESC = space(30)
  w_CODI = space(5)
  w_DESC = space(35)
  w_FLELG2 = space(1)
  w_VARVAL = space(1)
  w_CMNATTR2 = space(3)
  w_TIDES2 = space(30)

  * --- Children pointers
  GSMA_MTC = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CAM_AGAZ','gsma_acm')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_acmPag1","gsma_acm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Generali")
      .Pages(1).HelpContextID = 33672911
      .Pages(2).addobject("oPag","tgsma_acmPag2","gsma_acm",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("INTRA/estero")
      .Pages(2).HelpContextID = 44850119
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCMCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='CAM_AGAZ'
    this.cWorkTables[2]='TRADCAMA'
    this.cWorkTables[3]='TIPITRAN'
    this.cWorkTables[4]='CPAR_DEF'
    this.cWorkTables[5]='CAM_AGAZ'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CAM_AGAZ_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CAM_AGAZ_IDX,3]
  return

  function CreateChildren()
    this.GSMA_MTC = CREATEOBJECT('stdDynamicChild',this,'GSMA_MTC',this.oPgFrm.Page2.oPag.oLinkPC_2_9)
    this.GSMA_MTC.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSMA_MTC)
      this.GSMA_MTC.DestroyChildrenChain()
      this.GSMA_MTC=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_2_9')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSMA_MTC.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSMA_MTC.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSMA_MTC.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSMA_MTC.SetKey(;
            .w_CMCODICE,"LGCODICE";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSMA_MTC.ChangeRow(this.cRowID+'      1',1;
             ,.w_CMCODICE,"LGCODICE";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSMA_MTC)
        i_f=.GSMA_MTC.BuildFilter()
        if !(i_f==.GSMA_MTC.cQueryFilter)
          i_fnidx=.GSMA_MTC.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSMA_MTC.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSMA_MTC.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSMA_MTC.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSMA_MTC.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_CMCODICE = NVL(CMCODICE,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_5_joined
    link_1_5_joined=.f.
    local link_2_1_joined
    link_2_1_joined=.f.
    local link_2_11_joined
    link_2_11_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from CAM_AGAZ where CMCODICE=KeySet.CMCODICE
    *
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CAM_AGAZ')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CAM_AGAZ.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CAM_AGAZ '
      link_1_5_joined=this.AddJoinedLink_1_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_11_joined=this.AddJoinedLink_2_11(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CMCODICE',this.w_CMCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CHIAVE = 'TAM'
        .w_PCAUMAG = space(5)
        .w_DESCOL = space(35)
        .w_OBTEST = i_datsys
        .w_DATOBSO = ctod("  /  /  ")
        .w_TIDESC = space(30)
        .w_FLELG2 = space(1)
        .w_VARVAL = space(1)
        .w_TIDES2 = space(30)
        .w_CMCODICE = NVL(CMCODICE,space(5))
        .w_CMDESCRI = NVL(CMDESCRI,space(35))
          .link_1_3('Load')
        .w_CMCAUCOL = NVL(CMCAUCOL,space(5))
          if link_1_5_joined
            this.w_CMCAUCOL = NVL(CMCODICE105,NVL(this.w_CMCAUCOL,space(5)))
            this.w_DESCOL = NVL(CMDESCRI105,space(35))
            this.w_DATOBSO = NVL(cp_ToDate(CMDTOBSO105),ctod("  /  /  "))
            this.w_FLELG2 = NVL(CMFLELGM105,space(1))
            this.w_VARVAL = NVL(CMVARVAL105,space(1))
          else
          .link_1_5('Load')
          endif
        .w_CMFLCLFR = NVL(CMFLCLFR,space(1))
        .w_CMFLNDOC = NVL(CMFLNDOC,space(1))
        .w_CMMTCARI = NVL(CMMTCARI,space(1))
        .w_CMFLLAUE = NVL(CMFLLAUE,space(1))
        .w_CMFLCOMM = NVL(CMFLCOMM,space(1))
        .w_CMFLCASC = NVL(CMFLCASC,space(1))
        .w_CMFLORDI = NVL(CMFLORDI,space(1))
        .w_CMFLIMPE = NVL(CMFLIMPE,space(1))
        .w_CMFLRISE = NVL(CMFLRISE,space(1))
        .w_FLMAG = .w_CMFLCASC+.w_CMFLORDI+.w_CMFLRISE+.w_CMFLIMPE
        .w_CMFLAVAL = NVL(CMFLAVAL,space(1))
        .w_CMVARVAL = NVL(CMVARVAL,space(1))
        .w_CMFLELGM = NVL(CMFLELGM,space(1))
        .w_CMAGGVAL = NVL(CMAGGVAL,space(1))
        .w_CMFLRIVA = NVL(CMFLRIVA,space(1))
        .w_CMCRIVAL = NVL(CMCRIVAL,space(2))
        .w_CMSEQCAL = NVL(CMSEQCAL,0)
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_CMDTINVA = NVL(cp_ToDate(CMDTINVA),ctod("  /  /  "))
        .w_CMDTOBSO = NVL(cp_ToDate(CMDTOBSO),ctod("  /  /  "))
        .w_CMNATTRA = NVL(CMNATTRA,space(3))
          if link_2_1_joined
            this.w_CMNATTRA = NVL(TTCODICE201,NVL(this.w_CMNATTRA,space(3)))
            this.w_TIDESC = NVL(TTDESCRI201,space(30))
          else
          .link_2_1('Load')
          endif
        .w_CODI = .w_CMCODICE
        .w_DESC = .w_CMDESCRI
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate()
        .w_CMNATTR2 = NVL(CMNATTR2,space(3))
          if link_2_11_joined
            this.w_CMNATTR2 = NVL(TTCODICE211,NVL(this.w_CMNATTR2,space(3)))
            this.w_TIDES2 = NVL(TTDESCRI211,space(30))
          else
          .link_2_11('Load')
          endif
        cp_LoadRecExtFlds(this,'CAM_AGAZ')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CMCODICE = space(5)
      .w_CMDESCRI = space(35)
      .w_CHIAVE = space(10)
      .w_PCAUMAG = space(5)
      .w_CMCAUCOL = space(5)
      .w_CMFLCLFR = space(1)
      .w_CMFLNDOC = space(1)
      .w_CMMTCARI = space(1)
      .w_CMFLLAUE = space(1)
      .w_CMFLCOMM = space(1)
      .w_CMFLCASC = space(1)
      .w_CMFLORDI = space(1)
      .w_CMFLIMPE = space(1)
      .w_CMFLRISE = space(1)
      .w_FLMAG = space(10)
      .w_CMFLAVAL = space(1)
      .w_CMVARVAL = space(1)
      .w_CMFLELGM = space(1)
      .w_CMAGGVAL = space(1)
      .w_CMFLRIVA = space(1)
      .w_CMCRIVAL = space(2)
      .w_CMSEQCAL = 0
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_CMDTINVA = ctod("  /  /  ")
      .w_DESCOL = space(35)
      .w_CMDTOBSO = ctod("  /  /  ")
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_CMNATTRA = space(3)
      .w_TIDESC = space(30)
      .w_CODI = space(5)
      .w_DESC = space(35)
      .w_FLELG2 = space(1)
      .w_VARVAL = space(1)
      .w_CMNATTR2 = space(3)
      .w_TIDES2 = space(30)
      if .cFunction<>"Filter"
          .DoRTCalc(1,2,.f.)
        .w_CHIAVE = 'TAM'
        .DoRTCalc(3,3,.f.)
          if not(empty(.w_CHIAVE))
          .link_1_3('Full')
          endif
          .DoRTCalc(4,4,.f.)
        .w_CMCAUCOL = IIF( .w_CMVARVAL='N' , .w_CMCAUCOL , Space(5) )
        .DoRTCalc(5,5,.f.)
          if not(empty(.w_CMCAUCOL))
          .link_1_5('Full')
          endif
        .w_CMFLCLFR = 'N'
          .DoRTCalc(7,8,.f.)
        .w_CMFLLAUE = 'N'
        .w_CMFLCOMM = 'N'
          .DoRTCalc(11,14,.f.)
        .w_FLMAG = .w_CMFLCASC+.w_CMFLORDI+.w_CMFLRISE+.w_CMFLIMPE
          .DoRTCalc(16,16,.f.)
        .w_CMVARVAL = IIF( .w_CMAGGVAL $ 'AV' , .w_CMVARVAL , 'N' )
        .w_CMFLELGM = IIF(.w_CMFLAVAL $ 'AVCS' And .w_CMVARVAL='N', 'S', ' ')
        .w_CMAGGVAL = IIF(((.w_CMFLAVAL='A' And .w_CMFLCASC<>'-')Or(.w_CMFLAVAL='V' And .w_CMFLCASC<>'+')Or .w_CMFLAVAL $ 'CS') AND .w_CMFLELGM='S', 'S', ' ')
        .w_CMFLRIVA = 'N'
        .w_CMCRIVAL = IIF(.w_CMFLRIVA='S', 'CM', 'NO')
        .w_CMSEQCAL = IIF(.w_CMFLRIVA='S', 10, 0)
          .DoRTCalc(23,29,.f.)
        .w_OBTEST = i_datsys
        .DoRTCalc(31,32,.f.)
          if not(empty(.w_CMNATTRA))
          .link_2_1('Full')
          endif
          .DoRTCalc(33,33,.f.)
        .w_CODI = .w_CMCODICE
        .w_DESC = .w_CMDESCRI
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate()
        .DoRTCalc(36,38,.f.)
          if not(empty(.w_CMNATTR2))
          .link_2_11('Full')
          endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'CAM_AGAZ')
    this.DoRTCalc(39,39,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCMCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oCMDESCRI_1_2.enabled = i_bVal
      .Page1.oPag.oCMCAUCOL_1_5.enabled = i_bVal
      .Page1.oPag.oCMFLCLFR_1_6.enabled = i_bVal
      .Page1.oPag.oCMFLNDOC_1_7.enabled = i_bVal
      .Page1.oPag.oCMMTCARI_1_8.enabled = i_bVal
      .Page1.oPag.oCMFLLAUE_1_9.enabled = i_bVal
      .Page1.oPag.oCMFLCOMM_1_10.enabled = i_bVal
      .Page1.oPag.oCMFLCASC_1_11.enabled = i_bVal
      .Page1.oPag.oCMFLORDI_1_12.enabled = i_bVal
      .Page1.oPag.oCMFLIMPE_1_13.enabled = i_bVal
      .Page1.oPag.oCMFLRISE_1_14.enabled = i_bVal
      .Page1.oPag.oCMFLAVAL_1_16.enabled = i_bVal
      .Page1.oPag.oCMVARVAL_1_17.enabled = i_bVal
      .Page1.oPag.oCMFLELGM_1_18.enabled = i_bVal
      .Page1.oPag.oCMAGGVAL_1_19.enabled = i_bVal
      .Page1.oPag.oCMFLRIVA_1_20.enabled = i_bVal
      .Page1.oPag.oCMCRIVAL_1_21.enabled = i_bVal
      .Page1.oPag.oCMSEQCAL_1_22.enabled = i_bVal
      .Page1.oPag.oCMDTINVA_1_36.enabled = i_bVal
      .Page1.oPag.oCMDTOBSO_1_38.enabled = i_bVal
      .Page2.oPag.oCMNATTRA_2_1.enabled = i_bVal
      .Page2.oPag.oCMNATTR2_2_11.enabled = i_bVal
      .Page1.oPag.oObj_1_49.enabled = i_bVal
      .Page1.oPag.oObj_1_50.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oCMCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oCMCODICE_1_1.enabled = .t.
        .Page1.oPag.oCMDESCRI_1_2.enabled = .t.
      endif
    endwith
    this.GSMA_MTC.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'CAM_AGAZ',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSMA_MTC.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CMCODICE,"CMCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CMDESCRI,"CMDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CMCAUCOL,"CMCAUCOL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CMFLCLFR,"CMFLCLFR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CMFLNDOC,"CMFLNDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CMMTCARI,"CMMTCARI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CMFLLAUE,"CMFLLAUE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CMFLCOMM,"CMFLCOMM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CMFLCASC,"CMFLCASC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CMFLORDI,"CMFLORDI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CMFLIMPE,"CMFLIMPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CMFLRISE,"CMFLRISE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CMFLAVAL,"CMFLAVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CMVARVAL,"CMVARVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CMFLELGM,"CMFLELGM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CMAGGVAL,"CMAGGVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CMFLRIVA,"CMFLRIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CMCRIVAL,"CMCRIVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CMSEQCAL,"CMSEQCAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CMDTINVA,"CMDTINVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CMDTOBSO,"CMDTOBSO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CMNATTRA,"CMNATTRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CMNATTR2,"CMNATTR2",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    i_lTable = "CAM_AGAZ"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CAM_AGAZ_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSMA_SCM with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.CAM_AGAZ_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CAM_AGAZ
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CAM_AGAZ')
        i_extval=cp_InsertValODBCExtFlds(this,'CAM_AGAZ')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(CMCODICE,CMDESCRI,CMCAUCOL,CMFLCLFR,CMFLNDOC"+;
                  ",CMMTCARI,CMFLLAUE,CMFLCOMM,CMFLCASC,CMFLORDI"+;
                  ",CMFLIMPE,CMFLRISE,CMFLAVAL,CMVARVAL,CMFLELGM"+;
                  ",CMAGGVAL,CMFLRIVA,CMCRIVAL,CMSEQCAL,UTCC"+;
                  ",UTCV,UTDC,UTDV,CMDTINVA,CMDTOBSO"+;
                  ",CMNATTRA,CMNATTR2 "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_CMCODICE)+;
                  ","+cp_ToStrODBC(this.w_CMDESCRI)+;
                  ","+cp_ToStrODBCNull(this.w_CMCAUCOL)+;
                  ","+cp_ToStrODBC(this.w_CMFLCLFR)+;
                  ","+cp_ToStrODBC(this.w_CMFLNDOC)+;
                  ","+cp_ToStrODBC(this.w_CMMTCARI)+;
                  ","+cp_ToStrODBC(this.w_CMFLLAUE)+;
                  ","+cp_ToStrODBC(this.w_CMFLCOMM)+;
                  ","+cp_ToStrODBC(this.w_CMFLCASC)+;
                  ","+cp_ToStrODBC(this.w_CMFLORDI)+;
                  ","+cp_ToStrODBC(this.w_CMFLIMPE)+;
                  ","+cp_ToStrODBC(this.w_CMFLRISE)+;
                  ","+cp_ToStrODBC(this.w_CMFLAVAL)+;
                  ","+cp_ToStrODBC(this.w_CMVARVAL)+;
                  ","+cp_ToStrODBC(this.w_CMFLELGM)+;
                  ","+cp_ToStrODBC(this.w_CMAGGVAL)+;
                  ","+cp_ToStrODBC(this.w_CMFLRIVA)+;
                  ","+cp_ToStrODBC(this.w_CMCRIVAL)+;
                  ","+cp_ToStrODBC(this.w_CMSEQCAL)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBC(this.w_CMDTINVA)+;
                  ","+cp_ToStrODBC(this.w_CMDTOBSO)+;
                  ","+cp_ToStrODBCNull(this.w_CMNATTRA)+;
                  ","+cp_ToStrODBCNull(this.w_CMNATTR2)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CAM_AGAZ')
        i_extval=cp_InsertValVFPExtFlds(this,'CAM_AGAZ')
        cp_CheckDeletedKey(i_cTable,0,'CMCODICE',this.w_CMCODICE)
        INSERT INTO (i_cTable);
              (CMCODICE,CMDESCRI,CMCAUCOL,CMFLCLFR,CMFLNDOC,CMMTCARI,CMFLLAUE,CMFLCOMM,CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLAVAL,CMVARVAL,CMFLELGM,CMAGGVAL,CMFLRIVA,CMCRIVAL,CMSEQCAL,UTCC,UTCV,UTDC,UTDV,CMDTINVA,CMDTOBSO,CMNATTRA,CMNATTR2  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_CMCODICE;
                  ,this.w_CMDESCRI;
                  ,this.w_CMCAUCOL;
                  ,this.w_CMFLCLFR;
                  ,this.w_CMFLNDOC;
                  ,this.w_CMMTCARI;
                  ,this.w_CMFLLAUE;
                  ,this.w_CMFLCOMM;
                  ,this.w_CMFLCASC;
                  ,this.w_CMFLORDI;
                  ,this.w_CMFLIMPE;
                  ,this.w_CMFLRISE;
                  ,this.w_CMFLAVAL;
                  ,this.w_CMVARVAL;
                  ,this.w_CMFLELGM;
                  ,this.w_CMAGGVAL;
                  ,this.w_CMFLRIVA;
                  ,this.w_CMCRIVAL;
                  ,this.w_CMSEQCAL;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_CMDTINVA;
                  ,this.w_CMDTOBSO;
                  ,this.w_CMNATTRA;
                  ,this.w_CMNATTR2;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.CAM_AGAZ_IDX,i_nConn)
      *
      * update CAM_AGAZ
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'CAM_AGAZ')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " CMDESCRI="+cp_ToStrODBC(this.w_CMDESCRI)+;
             ",CMCAUCOL="+cp_ToStrODBCNull(this.w_CMCAUCOL)+;
             ",CMFLCLFR="+cp_ToStrODBC(this.w_CMFLCLFR)+;
             ",CMFLNDOC="+cp_ToStrODBC(this.w_CMFLNDOC)+;
             ",CMMTCARI="+cp_ToStrODBC(this.w_CMMTCARI)+;
             ",CMFLLAUE="+cp_ToStrODBC(this.w_CMFLLAUE)+;
             ",CMFLCOMM="+cp_ToStrODBC(this.w_CMFLCOMM)+;
             ",CMFLCASC="+cp_ToStrODBC(this.w_CMFLCASC)+;
             ",CMFLORDI="+cp_ToStrODBC(this.w_CMFLORDI)+;
             ",CMFLIMPE="+cp_ToStrODBC(this.w_CMFLIMPE)+;
             ",CMFLRISE="+cp_ToStrODBC(this.w_CMFLRISE)+;
             ",CMFLAVAL="+cp_ToStrODBC(this.w_CMFLAVAL)+;
             ",CMVARVAL="+cp_ToStrODBC(this.w_CMVARVAL)+;
             ",CMFLELGM="+cp_ToStrODBC(this.w_CMFLELGM)+;
             ",CMAGGVAL="+cp_ToStrODBC(this.w_CMAGGVAL)+;
             ",CMFLRIVA="+cp_ToStrODBC(this.w_CMFLRIVA)+;
             ",CMCRIVAL="+cp_ToStrODBC(this.w_CMCRIVAL)+;
             ",CMSEQCAL="+cp_ToStrODBC(this.w_CMSEQCAL)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",CMDTINVA="+cp_ToStrODBC(this.w_CMDTINVA)+;
             ",CMDTOBSO="+cp_ToStrODBC(this.w_CMDTOBSO)+;
             ",CMNATTRA="+cp_ToStrODBCNull(this.w_CMNATTRA)+;
             ",CMNATTR2="+cp_ToStrODBCNull(this.w_CMNATTR2)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'CAM_AGAZ')
        i_cWhere = cp_PKFox(i_cTable  ,'CMCODICE',this.w_CMCODICE  )
        UPDATE (i_cTable) SET;
              CMDESCRI=this.w_CMDESCRI;
             ,CMCAUCOL=this.w_CMCAUCOL;
             ,CMFLCLFR=this.w_CMFLCLFR;
             ,CMFLNDOC=this.w_CMFLNDOC;
             ,CMMTCARI=this.w_CMMTCARI;
             ,CMFLLAUE=this.w_CMFLLAUE;
             ,CMFLCOMM=this.w_CMFLCOMM;
             ,CMFLCASC=this.w_CMFLCASC;
             ,CMFLORDI=this.w_CMFLORDI;
             ,CMFLIMPE=this.w_CMFLIMPE;
             ,CMFLRISE=this.w_CMFLRISE;
             ,CMFLAVAL=this.w_CMFLAVAL;
             ,CMVARVAL=this.w_CMVARVAL;
             ,CMFLELGM=this.w_CMFLELGM;
             ,CMAGGVAL=this.w_CMAGGVAL;
             ,CMFLRIVA=this.w_CMFLRIVA;
             ,CMCRIVAL=this.w_CMCRIVAL;
             ,CMSEQCAL=this.w_CMSEQCAL;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,CMDTINVA=this.w_CMDTINVA;
             ,CMDTOBSO=this.w_CMDTOBSO;
             ,CMNATTRA=this.w_CMNATTRA;
             ,CMNATTR2=this.w_CMNATTR2;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSMA_MTC : Saving
      this.GSMA_MTC.ChangeRow(this.cRowID+'      1',0;
             ,this.w_CMCODICE,"LGCODICE";
             )
      this.GSMA_MTC.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSMA_MTC : Deleting
    this.GSMA_MTC.ChangeRow(this.cRowID+'      1',0;
           ,this.w_CMCODICE,"LGCODICE";
           )
    this.GSMA_MTC.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.CAM_AGAZ_IDX,i_nConn)
      *
      * delete CAM_AGAZ
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'CMCODICE',this.w_CMCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
          .link_1_3('Full')
        .DoRTCalc(4,4,.t.)
        if .o_CMVARVAL<>.w_CMVARVAL
            .w_CMCAUCOL = IIF( .w_CMVARVAL='N' , .w_CMCAUCOL , Space(5) )
          .link_1_5('Full')
        endif
        .DoRTCalc(6,14,.t.)
            .w_FLMAG = .w_CMFLCASC+.w_CMFLORDI+.w_CMFLRISE+.w_CMFLIMPE
        .DoRTCalc(16,16,.t.)
        if .o_CMAGGVAL<>.w_CMAGGVAL
            .w_CMVARVAL = IIF( .w_CMAGGVAL $ 'AV' , .w_CMVARVAL , 'N' )
        endif
        if .o_CMFLAVAL<>.w_CMFLAVAL.or. .o_CMVARVAL<>.w_CMVARVAL
            .w_CMFLELGM = IIF(.w_CMFLAVAL $ 'AVCS' And .w_CMVARVAL='N', 'S', ' ')
        endif
        if .o_CMFLAVAL<>.w_CMFLAVAL.or. .o_CMFLELGM<>.w_CMFLELGM.or. .o_CMFLCASC<>.w_CMFLCASC
            .w_CMAGGVAL = IIF(((.w_CMFLAVAL='A' And .w_CMFLCASC<>'-')Or(.w_CMFLAVAL='V' And .w_CMFLCASC<>'+')Or .w_CMFLAVAL $ 'CS') AND .w_CMFLELGM='S', 'S', ' ')
        endif
        .DoRTCalc(20,20,.t.)
        if .o_CMFLRIVA<>.w_CMFLRIVA
            .w_CMCRIVAL = IIF(.w_CMFLRIVA='S', 'CM', 'NO')
        endif
        if .o_CMFLRIVA<>.w_CMFLRIVA
            .w_CMSEQCAL = IIF(.w_CMFLRIVA='S', 10, 0)
        endif
        .DoRTCalc(23,33,.t.)
            .w_CODI = .w_CMCODICE
            .w_DESC = .w_CMDESCRI
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(36,39,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCMCAUCOL_1_5.enabled = this.oPgFrm.Page1.oPag.oCMCAUCOL_1_5.mCond()
    this.oPgFrm.Page1.oPag.oCMMTCARI_1_8.enabled = this.oPgFrm.Page1.oPag.oCMMTCARI_1_8.mCond()
    this.oPgFrm.Page1.oPag.oCMFLCOMM_1_10.enabled = this.oPgFrm.Page1.oPag.oCMFLCOMM_1_10.mCond()
    this.oPgFrm.Page1.oPag.oCMVARVAL_1_17.enabled = this.oPgFrm.Page1.oPag.oCMVARVAL_1_17.mCond()
    this.oPgFrm.Page1.oPag.oCMFLELGM_1_18.enabled = this.oPgFrm.Page1.oPag.oCMFLELGM_1_18.mCond()
    this.oPgFrm.Page1.oPag.oCMAGGVAL_1_19.enabled = this.oPgFrm.Page1.oPag.oCMAGGVAL_1_19.mCond()
    this.oPgFrm.Page1.oPag.oCMFLRIVA_1_20.enabled = this.oPgFrm.Page1.oPag.oCMFLRIVA_1_20.mCond()
    this.oPgFrm.Page1.oPag.oCMCRIVAL_1_21.enabled = this.oPgFrm.Page1.oPag.oCMCRIVAL_1_21.mCond()
    this.oPgFrm.Page1.oPag.oCMSEQCAL_1_22.enabled = this.oPgFrm.Page1.oPag.oCMSEQCAL_1_22.mCond()
    this.oPgFrm.Page2.oPag.oCMNATTRA_2_1.enabled = this.oPgFrm.Page2.oPag.oCMNATTRA_2_1.mCond()
    this.oPgFrm.Page2.oPag.oCMNATTR2_2_11.enabled = this.oPgFrm.Page2.oPag.oCMNATTR2_2_11.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCMMTCARI_1_8.visible=!this.oPgFrm.Page1.oPag.oCMMTCARI_1_8.mHide()
    this.oPgFrm.Page1.oPag.oCMFLCOMM_1_10.visible=!this.oPgFrm.Page1.oPag.oCMFLCOMM_1_10.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_46.visible=!this.oPgFrm.Page1.oPag.oStr_1_46.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_49.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_50.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CHIAVE
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPAR_DEF_IDX,3]
    i_lTable = "CPAR_DEF"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2], .t., this.CPAR_DEF_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CHIAVE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CHIAVE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDCHIAVE,PDCAUMAG";
                   +" from "+i_cTable+" "+i_lTable+" where PDCHIAVE="+cp_ToStrODBC(this.w_CHIAVE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PDCHIAVE',this.w_CHIAVE)
            select PDCHIAVE,PDCAUMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CHIAVE = NVL(_Link_.PDCHIAVE,space(10))
      this.w_PCAUMAG = NVL(_Link_.PDCAUMAG,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CHIAVE = space(10)
      endif
      this.w_PCAUMAG = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2])+'\'+cp_ToStr(_Link_.PDCHIAVE,1)
      cp_ShowWarn(i_cKey,this.CPAR_DEF_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CHIAVE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CMCAUCOL
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CMCAUCOL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAM_AGAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_CMCAUCOL)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMDTOBSO,CMFLELGM,CMVARVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_CMCAUCOL))
          select CMCODICE,CMDESCRI,CMDTOBSO,CMFLELGM,CMVARVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CMCAUCOL)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CMDESCRI like "+cp_ToStrODBC(trim(this.w_CMCAUCOL)+"%");

            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMDTOBSO,CMFLELGM,CMVARVAL";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CMDESCRI like "+cp_ToStr(trim(this.w_CMCAUCOL)+"%");

            select CMCODICE,CMDESCRI,CMDTOBSO,CMFLELGM,CMVARVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CMCAUCOL) and !this.bDontReportError
            deferred_cp_zoom('CAM_AGAZ','*','CMCODICE',cp_AbsName(oSource.parent,'oCMCAUCOL_1_5'),i_cWhere,'',"Causale collegata",'GSMA_ACM.CAM_AGAZ_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMDTOBSO,CMFLELGM,CMVARVAL";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI,CMDTOBSO,CMFLELGM,CMVARVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CMCAUCOL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMDTOBSO,CMFLELGM,CMVARVAL";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_CMCAUCOL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_CMCAUCOL)
            select CMCODICE,CMDESCRI,CMDTOBSO,CMFLELGM,CMVARVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CMCAUCOL = NVL(_Link_.CMCODICE,space(5))
      this.w_DESCOL = NVL(_Link_.CMDESCRI,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CMDTOBSO),ctod("  /  /  "))
      this.w_FLELG2 = NVL(_Link_.CMFLELGM,space(1))
      this.w_VARVAL = NVL(_Link_.CMVARVAL,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CMCAUCOL = space(5)
      endif
      this.w_DESCOL = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_FLELG2 = space(1)
      this.w_VARVAL = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_CMCAUCOL<>.w_CMCODICE) AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) And .w_VARVAL='N'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale magazzino inesistente, che gestisce variazioni a valore oppure obsoleta")
        endif
        this.w_CMCAUCOL = space(5)
        this.w_DESCOL = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_FLELG2 = space(1)
        this.w_VARVAL = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CMCAUCOL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAM_AGAZ_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_5.CMCODICE as CMCODICE105"+ ",link_1_5.CMDESCRI as CMDESCRI105"+ ",link_1_5.CMDTOBSO as CMDTOBSO105"+ ",link_1_5.CMFLELGM as CMFLELGM105"+ ",link_1_5.CMVARVAL as CMVARVAL105"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_5 on CAM_AGAZ.CMCAUCOL=link_1_5.CMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_5"
          i_cKey=i_cKey+'+" and CAM_AGAZ.CMCAUCOL=link_1_5.CMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CMNATTRA
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPITRAN_IDX,3]
    i_lTable = "TIPITRAN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPITRAN_IDX,2], .t., this.TIPITRAN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPITRAN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CMNATTRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATT',True,'TIPITRAN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TTCODICE like "+cp_ToStrODBC(trim(this.w_CMNATTRA)+"%");

          i_ret=cp_SQL(i_nConn,"select TTCODICE,TTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TTCODICE',trim(this.w_CMNATTRA))
          select TTCODICE,TTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CMNATTRA)==trim(_Link_.TTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CMNATTRA) and !this.bDontReportError
            deferred_cp_zoom('TIPITRAN','*','TTCODICE',cp_AbsName(oSource.parent,'oCMNATTRA_2_1'),i_cWhere,'GSAR_ATT',"Natura transazioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TTCODICE,TTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TTCODICE',oSource.xKey(1))
            select TTCODICE,TTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CMNATTRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TTCODICE,TTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TTCODICE="+cp_ToStrODBC(this.w_CMNATTRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TTCODICE',this.w_CMNATTRA)
            select TTCODICE,TTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CMNATTRA = NVL(_Link_.TTCODICE,space(3))
      this.w_TIDESC = NVL(_Link_.TTDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CMNATTRA = space(3)
      endif
      this.w_TIDESC = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPITRAN_IDX,2])+'\'+cp_ToStr(_Link_.TTCODICE,1)
      cp_ShowWarn(i_cKey,this.TIPITRAN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CMNATTRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIPITRAN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIPITRAN_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.TTCODICE as TTCODICE201"+ ",link_2_1.TTDESCRI as TTDESCRI201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on CAM_AGAZ.CMNATTRA=link_2_1.TTCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and CAM_AGAZ.CMNATTRA=link_2_1.TTCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CMNATTR2
  func Link_2_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPITRAN_IDX,3]
    i_lTable = "TIPITRAN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPITRAN_IDX,2], .t., this.TIPITRAN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPITRAN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CMNATTR2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATT',True,'TIPITRAN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TTCODICE like "+cp_ToStrODBC(trim(this.w_CMNATTR2)+"%");

          i_ret=cp_SQL(i_nConn,"select TTCODICE,TTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TTCODICE',trim(this.w_CMNATTR2))
          select TTCODICE,TTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CMNATTR2)==trim(_Link_.TTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CMNATTR2) and !this.bDontReportError
            deferred_cp_zoom('TIPITRAN','*','TTCODICE',cp_AbsName(oSource.parent,'oCMNATTR2_2_11'),i_cWhere,'GSAR_ATT',"Natura transazioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TTCODICE,TTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TTCODICE',oSource.xKey(1))
            select TTCODICE,TTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CMNATTR2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TTCODICE,TTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TTCODICE="+cp_ToStrODBC(this.w_CMNATTR2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TTCODICE',this.w_CMNATTR2)
            select TTCODICE,TTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CMNATTR2 = NVL(_Link_.TTCODICE,space(3))
      this.w_TIDES2 = NVL(_Link_.TTDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CMNATTR2 = space(3)
      endif
      this.w_TIDES2 = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPITRAN_IDX,2])+'\'+cp_ToStr(_Link_.TTCODICE,1)
      cp_ShowWarn(i_cKey,this.TIPITRAN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CMNATTR2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_11(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIPITRAN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIPITRAN_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_11.TTCODICE as TTCODICE211"+ ",link_2_11.TTDESCRI as TTDESCRI211"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_11 on CAM_AGAZ.CMNATTR2=link_2_11.TTCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_11"
          i_cKey=i_cKey+'+" and CAM_AGAZ.CMNATTR2=link_2_11.TTCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCMCODICE_1_1.value==this.w_CMCODICE)
      this.oPgFrm.Page1.oPag.oCMCODICE_1_1.value=this.w_CMCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oCMDESCRI_1_2.value==this.w_CMDESCRI)
      this.oPgFrm.Page1.oPag.oCMDESCRI_1_2.value=this.w_CMDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCMCAUCOL_1_5.value==this.w_CMCAUCOL)
      this.oPgFrm.Page1.oPag.oCMCAUCOL_1_5.value=this.w_CMCAUCOL
    endif
    if not(this.oPgFrm.Page1.oPag.oCMFLCLFR_1_6.RadioValue()==this.w_CMFLCLFR)
      this.oPgFrm.Page1.oPag.oCMFLCLFR_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCMFLNDOC_1_7.RadioValue()==this.w_CMFLNDOC)
      this.oPgFrm.Page1.oPag.oCMFLNDOC_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCMMTCARI_1_8.RadioValue()==this.w_CMMTCARI)
      this.oPgFrm.Page1.oPag.oCMMTCARI_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCMFLLAUE_1_9.RadioValue()==this.w_CMFLLAUE)
      this.oPgFrm.Page1.oPag.oCMFLLAUE_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCMFLCOMM_1_10.RadioValue()==this.w_CMFLCOMM)
      this.oPgFrm.Page1.oPag.oCMFLCOMM_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCMFLCASC_1_11.RadioValue()==this.w_CMFLCASC)
      this.oPgFrm.Page1.oPag.oCMFLCASC_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCMFLORDI_1_12.RadioValue()==this.w_CMFLORDI)
      this.oPgFrm.Page1.oPag.oCMFLORDI_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCMFLIMPE_1_13.RadioValue()==this.w_CMFLIMPE)
      this.oPgFrm.Page1.oPag.oCMFLIMPE_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCMFLRISE_1_14.RadioValue()==this.w_CMFLRISE)
      this.oPgFrm.Page1.oPag.oCMFLRISE_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCMFLAVAL_1_16.RadioValue()==this.w_CMFLAVAL)
      this.oPgFrm.Page1.oPag.oCMFLAVAL_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCMVARVAL_1_17.RadioValue()==this.w_CMVARVAL)
      this.oPgFrm.Page1.oPag.oCMVARVAL_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCMFLELGM_1_18.RadioValue()==this.w_CMFLELGM)
      this.oPgFrm.Page1.oPag.oCMFLELGM_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCMAGGVAL_1_19.RadioValue()==this.w_CMAGGVAL)
      this.oPgFrm.Page1.oPag.oCMAGGVAL_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCMFLRIVA_1_20.RadioValue()==this.w_CMFLRIVA)
      this.oPgFrm.Page1.oPag.oCMFLRIVA_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCMCRIVAL_1_21.RadioValue()==this.w_CMCRIVAL)
      this.oPgFrm.Page1.oPag.oCMCRIVAL_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCMSEQCAL_1_22.value==this.w_CMSEQCAL)
      this.oPgFrm.Page1.oPag.oCMSEQCAL_1_22.value=this.w_CMSEQCAL
    endif
    if not(this.oPgFrm.Page1.oPag.oCMDTINVA_1_36.value==this.w_CMDTINVA)
      this.oPgFrm.Page1.oPag.oCMDTINVA_1_36.value=this.w_CMDTINVA
    endif
    if not(this.oPgFrm.Page1.oPag.oCMDTOBSO_1_38.value==this.w_CMDTOBSO)
      this.oPgFrm.Page1.oPag.oCMDTOBSO_1_38.value=this.w_CMDTOBSO
    endif
    if not(this.oPgFrm.Page2.oPag.oCMNATTRA_2_1.value==this.w_CMNATTRA)
      this.oPgFrm.Page2.oPag.oCMNATTRA_2_1.value=this.w_CMNATTRA
    endif
    if not(this.oPgFrm.Page2.oPag.oTIDESC_2_2.value==this.w_TIDESC)
      this.oPgFrm.Page2.oPag.oTIDESC_2_2.value=this.w_TIDESC
    endif
    if not(this.oPgFrm.Page2.oPag.oCODI_2_7.value==this.w_CODI)
      this.oPgFrm.Page2.oPag.oCODI_2_7.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESC_2_8.value==this.w_DESC)
      this.oPgFrm.Page2.oPag.oDESC_2_8.value=this.w_DESC
    endif
    if not(this.oPgFrm.Page2.oPag.oCMNATTR2_2_11.value==this.w_CMNATTR2)
      this.oPgFrm.Page2.oPag.oCMNATTR2_2_11.value=this.w_CMNATTR2
    endif
    if not(this.oPgFrm.Page2.oPag.oTIDES2_2_12.value==this.w_TIDES2)
      this.oPgFrm.Page2.oPag.oTIDES2_2_12.value=this.w_TIDES2
    endif
    cp_SetControlsValueExtFlds(this,'CAM_AGAZ')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CMCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCMCODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_CMCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.w_CMCAUCOL<>.w_CMCODICE) AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) And .w_VARVAL='N')  and (.w_CMVARVAL='N')  and not(empty(.w_CMCAUCOL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCMCAUCOL_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale magazzino inesistente, che gestisce variazioni a valore oppure obsoleta")
          case   (empty(.w_CMFLCLFR))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCMFLCLFR_1_6.SetFocus()
            i_bnoObbl = !empty(.w_CMFLCLFR)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.t.)  and not(g_COMM<>'S')  and (g_COMM='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCMFLCOMM_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CMFLAVAL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCMFLAVAL_1_16.SetFocus()
            i_bnoObbl = !empty(.w_CMFLAVAL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CMVARVAL='N' Or Empty(.w_CMFLCASC+.w_CMFLORDI+.w_CMFLIMPE+.w_CMFLRISE+.w_CMCAUCOL))  and (.w_CMFLAVAL $ 'AV')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCMVARVAL_1_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La variazione a valore � possibile solo se non � movimentato il magazzino, no mov.fiscale, e non si ha causali collegate")
          case   not((.w_CMFLRIVA='S' AND .w_CMCRIVAL $ 'CM-UC-CS') OR .w_CMFLRIVA<>'S')  and (.w_CMFLAVAL$'C,S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCMCRIVAL_1_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un criterio di rivalorizzazione")
          case   (empty(.w_CMSEQCAL))  and (NOT(.w_CMFLAVAL$'X-N') And .w_CMVARVAL='N')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCMSEQCAL_1_22.SetFocus()
            i_bnoObbl = !empty(.w_CMSEQCAL)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .GSMA_MTC.CheckForm()
      if i_bres
        i_bres=  .GSMA_MTC.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsma_acm
      if i_bRes and .w_CMFLELGM<>'S' and .w_CMFLAVAL $ 'CS'
       if not iif(.w_CMFLAVAL='S',ah_yesno("Causale altri scarichi non fiscale%0Confermi ugualmente?"),ah_yesno("Causale altri carichi non fiscale%0Confermi ugualmente?"))
            i_cErrorMsg = Ah_MsgFormat("Valore non ammesso")
            i_bnoChk = .f.
            i_bRes = .f.
         endif
      endif
      
      if i_bRes and  (((.w_CMFLAVAL='V' and .w_CMFLCASC = '+') Or(.w_CMFLAVAL='A' and .w_CMFLCASC = '-')) And .w_CMAGGVAL='S')
            i_cErrorMsg = Ah_MsgFormat("Un movimento di reso non pu� aggiornare il valore di magazzino")
            i_bnoChk = .f.
            i_bRes = .f.
      endif
      
      If i_bRes and g_ORDI='S' And !GSOR_BOA(This)
            Ah_ErrorMsg("La causale di magazzino non pu� movimentare esistenza, ordinato, impegnato o riservato%0perch� utilizzata in una causale ordine con flag ordine aperto")
            i_bRes = .f.
      Endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CMFLCASC = this.w_CMFLCASC
    this.o_CMFLORDI = this.w_CMFLORDI
    this.o_CMFLIMPE = this.w_CMFLIMPE
    this.o_CMFLAVAL = this.w_CMFLAVAL
    this.o_CMVARVAL = this.w_CMVARVAL
    this.o_CMFLELGM = this.w_CMFLELGM
    this.o_CMAGGVAL = this.w_CMAGGVAL
    this.o_CMFLRIVA = this.w_CMFLRIVA
    * --- GSMA_MTC : Depends On
    this.GSMA_MTC.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsma_acmPag1 as StdContainer
  Width  = 652
  height = 316
  stdWidth  = 652
  stdheight = 316
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCMCODICE_1_1 as StdField with uid="LQMFCOINWH",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CMCODICE", cQueryName = "CMCODICE",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Digitare il codice primario della causale.",;
    HelpContextID = 150403477,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=58, Left=62, Top=10, cSayPict='"!!!!!"', cGetPict='"!!!!!"', InputMask=replicate('X',5)

  add object oCMDESCRI_1_2 as StdField with uid="PJISYRAOCK",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CMDESCRI", cQueryName = "CMDESCRI",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Digitare la descrizione della causale.",;
    HelpContextID = 235989393,;
    FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=254, Left=129, Top=10, InputMask=replicate('X',35)

  add object oCMCAUCOL_1_5 as StdField with uid="OBQHUQJIQU",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CMCAUCOL", cQueryName = "CMCAUCOL",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale magazzino inesistente, che gestisce variazioni a valore oppure obsoleta",;
    ToolTipText = "Codice della causale collegata",;
    HelpContextID = 34276978,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=585, Top=10, cSayPict='"!!!!!"', cGetPict='"!!!!!"', InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAM_AGAZ", oKey_1_1="CMCODICE", oKey_1_2="this.w_CMCAUCOL"

  func oCMCAUCOL_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CMVARVAL='N')
    endwith
   endif
  endfunc

  func oCMCAUCOL_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCMCAUCOL_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCMCAUCOL_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAM_AGAZ','*','CMCODICE',cp_AbsName(this.parent,'oCMCAUCOL_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Causale collegata",'GSMA_ACM.CAM_AGAZ_VZM',this.parent.oContained
  endproc


  add object oCMFLCLFR_1_6 as StdCombo with uid="TGGJEVPJOK",rtseq=6,rtrep=.f.,left=62,top=40,width=103,height=21;
    , ToolTipText = "La causale pu� essere riferita a cliente o fornitore o nessuno";
    , HelpContextID = 101304712;
    , cFormVar="w_CMFLCLFR",RowSource=""+"Cliente,"+"Fornitore,"+"Nessuno", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oCMFLCLFR_1_6.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oCMFLCLFR_1_6.GetRadio()
    this.Parent.oContained.w_CMFLCLFR = this.RadioValue()
    return .t.
  endfunc

  func oCMFLCLFR_1_6.SetRadio()
    this.Parent.oContained.w_CMFLCLFR=trim(this.Parent.oContained.w_CMFLCLFR)
    this.value = ;
      iif(this.Parent.oContained.w_CMFLCLFR=='C',1,;
      iif(this.Parent.oContained.w_CMFLCLFR=='F',2,;
      iif(this.Parent.oContained.w_CMFLCLFR=='N',3,;
      0)))
  endfunc

  add object oCMFLNDOC_1_7 as StdCheck with uid="CAJKLTMURL",rtseq=7,rtrep=.f.,left=174, top=40, caption="Docum.obbligatorio",;
    ToolTipText = "Se attivo: abilita l'obbligo del documento nei movimenti magazzino",;
    HelpContextID = 44447337,;
    cFormVar="w_CMFLNDOC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCMFLNDOC_1_7.RadioValue()
    return(iif(this.value =1,'O',;
    ' '))
  endfunc
  func oCMFLNDOC_1_7.GetRadio()
    this.Parent.oContained.w_CMFLNDOC = this.RadioValue()
    return .t.
  endfunc

  func oCMFLNDOC_1_7.SetRadio()
    this.Parent.oContained.w_CMFLNDOC=trim(this.Parent.oContained.w_CMFLNDOC)
    this.value = ;
      iif(this.Parent.oContained.w_CMFLNDOC=='O',1,;
      0)
  endfunc

  add object oCMMTCARI_1_8 as StdCheck with uid="TBXREKVGFA",rtseq=8,rtrep=.f.,left=174, top=61, caption="Ins rapido matricole",;
    ToolTipText = "Attiva l'inserimento rapido matricole",;
    HelpContextID = 16865681,;
    cFormVar="w_CMMTCARI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCMMTCARI_1_8.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCMMTCARI_1_8.GetRadio()
    this.Parent.oContained.w_CMMTCARI = this.RadioValue()
    return .t.
  endfunc

  func oCMMTCARI_1_8.SetRadio()
    this.Parent.oContained.w_CMMTCARI=trim(this.Parent.oContained.w_CMMTCARI)
    this.value = ;
      iif(this.Parent.oContained.w_CMMTCARI=='S',1,;
      0)
  endfunc

  func oCMMTCARI_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CMFLIMPE) AND EMPTY(.w_CMFLORDI))
    endwith
   endif
  endfunc

  func oCMMTCARI_1_8.mHide()
    with this.Parent.oContained
      return (g_MATR<>'S')
    endwith
  endfunc

  add object oCMFLLAUE_1_9 as StdCheck with uid="NNXOGBOUGU",rtseq=9,rtrep=.f.,left=174, top=82, caption="Beni in lavorazione",;
    ToolTipText = "Se attivo: il movimento sar� stampato nel registro beni in lavorazione",;
    HelpContextID = 7981461,;
    cFormVar="w_CMFLLAUE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCMFLLAUE_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCMFLLAUE_1_9.GetRadio()
    this.Parent.oContained.w_CMFLLAUE = this.RadioValue()
    return .t.
  endfunc

  func oCMFLLAUE_1_9.SetRadio()
    this.Parent.oContained.w_CMFLLAUE=trim(this.Parent.oContained.w_CMFLLAUE)
    this.value = ;
      iif(this.Parent.oContained.w_CMFLLAUE=='S',1,;
      0)
  endfunc


  add object oCMFLCOMM_1_10 as StdCombo with uid="CFMUTSTQNJ",rtseq=10,rtrep=.f.,left=495,top=40,width=148,height=21;
    , ToolTipText = "Aggiornamento saldi attivit� legati alla commessa";
    , HelpContextID = 217462387;
    , cFormVar="w_CMFLCOMM",RowSource=""+"Non gestita,"+"Aumenta impegni fin.,"+"Dim. impegni fin.,"+"Aumenta consuntivo,"+"Dim. consuntivo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCMFLCOMM_1_10.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'I',;
    iif(this.value =3,'D',;
    iif(this.value =4,'C',;
    iif(this.value =5,'S',;
    space(1)))))))
  endfunc
  func oCMFLCOMM_1_10.GetRadio()
    this.Parent.oContained.w_CMFLCOMM = this.RadioValue()
    return .t.
  endfunc

  func oCMFLCOMM_1_10.SetRadio()
    this.Parent.oContained.w_CMFLCOMM=trim(this.Parent.oContained.w_CMFLCOMM)
    this.value = ;
      iif(this.Parent.oContained.w_CMFLCOMM=='N',1,;
      iif(this.Parent.oContained.w_CMFLCOMM=='I',2,;
      iif(this.Parent.oContained.w_CMFLCOMM=='D',3,;
      iif(this.Parent.oContained.w_CMFLCOMM=='C',4,;
      iif(this.Parent.oContained.w_CMFLCOMM=='S',5,;
      0)))))
  endfunc

  func oCMFLCOMM_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COMM='S')
    endwith
   endif
  endfunc

  func oCMFLCOMM_1_10.mHide()
    with this.Parent.oContained
      return (g_COMM<>'S')
    endwith
  endfunc

  func oCMFLCOMM_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.t.)
      if bRes and !(g_COMM='S' and (.w_PCAUMAG<>.w_CMCODICE OR .w_CMFLCOMM='N'))
         bRes=(cp_WarningMsg(thisform.msgFmt("Causale di magazzino utilizzata nei parametri di default per l'import di righe documento che non  gestiscono la commessa")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc


  add object oCMFLCASC_1_11 as StdCombo with uid="QAAZQXAFQL",value=3,rtseq=11,rtrep=.f.,left=66,top=112,width=92,height=21;
    , ToolTipText = "Flag di aggiornamento esistenza di magazzino";
    , HelpContextID = 17418647;
    , cFormVar="w_CMFLCASC",RowSource=""+"Aumenta,"+"Diminuisce,"+"Invariato", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCMFLCASC_1_11.RadioValue()
    return(iif(this.value =1,'+',;
    iif(this.value =2,'-',;
    iif(this.value =3,' ',;
    space(1)))))
  endfunc
  func oCMFLCASC_1_11.GetRadio()
    this.Parent.oContained.w_CMFLCASC = this.RadioValue()
    return .t.
  endfunc

  func oCMFLCASC_1_11.SetRadio()
    this.Parent.oContained.w_CMFLCASC=trim(this.Parent.oContained.w_CMFLCASC)
    this.value = ;
      iif(this.Parent.oContained.w_CMFLCASC=='+',1,;
      iif(this.Parent.oContained.w_CMFLCASC=='-',2,;
      iif(this.Parent.oContained.w_CMFLCASC=='',3,;
      0)))
  endfunc


  add object oCMFLORDI_1_12 as StdCombo with uid="BPNXSDRDAQ",value=3,rtseq=12,rtrep=.f.,left=222,top=112,width=92,height=21;
    , ToolTipText = "Flag di aggiornamento quantit� e valore ordinato";
    , HelpContextID = 256493969;
    , cFormVar="w_CMFLORDI",RowSource=""+"Aumenta,"+"Diminuisce,"+"Invariato", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCMFLORDI_1_12.RadioValue()
    return(iif(this.value =1,'+',;
    iif(this.value =2,'-',;
    iif(this.value =3,' ',;
    space(1)))))
  endfunc
  func oCMFLORDI_1_12.GetRadio()
    this.Parent.oContained.w_CMFLORDI = this.RadioValue()
    return .t.
  endfunc

  func oCMFLORDI_1_12.SetRadio()
    this.Parent.oContained.w_CMFLORDI=trim(this.Parent.oContained.w_CMFLORDI)
    this.value = ;
      iif(this.Parent.oContained.w_CMFLORDI=='+',1,;
      iif(this.Parent.oContained.w_CMFLORDI=='-',2,;
      iif(this.Parent.oContained.w_CMFLORDI=='',3,;
      0)))
  endfunc


  add object oCMFLIMPE_1_13 as StdCombo with uid="DJYVVUTOPE",value=3,rtseq=13,rtrep=.f.,left=390,top=112,width=92,height=21;
    , ToolTipText = "Flag di aggiornamento quantit� e valore impegnato";
    , HelpContextID = 78236053;
    , cFormVar="w_CMFLIMPE",RowSource=""+"Aumenta,"+"Diminuisce,"+"Invariato", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCMFLIMPE_1_13.RadioValue()
    return(iif(this.value =1,'+',;
    iif(this.value =2,'-',;
    iif(this.value =3,' ',;
    space(1)))))
  endfunc
  func oCMFLIMPE_1_13.GetRadio()
    this.Parent.oContained.w_CMFLIMPE = this.RadioValue()
    return .t.
  endfunc

  func oCMFLIMPE_1_13.SetRadio()
    this.Parent.oContained.w_CMFLIMPE=trim(this.Parent.oContained.w_CMFLIMPE)
    this.value = ;
      iif(this.Parent.oContained.w_CMFLIMPE=='+',1,;
      iif(this.Parent.oContained.w_CMFLIMPE=='-',2,;
      iif(this.Parent.oContained.w_CMFLIMPE=='',3,;
      0)))
  endfunc


  add object oCMFLRISE_1_14 as StdCombo with uid="ZZMPDIGCEP",value=3,rtseq=14,rtrep=.f.,left=551,top=112,width=92,height=21;
    , ToolTipText = "Flag di aggiornamento quantit� e valore riservato";
    , HelpContextID = 135907733;
    , cFormVar="w_CMFLRISE",RowSource=""+"Aumenta,"+"Diminuisce,"+"Invariato", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCMFLRISE_1_14.RadioValue()
    return(iif(this.value =1,'+',;
    iif(this.value =2,'-',;
    iif(this.value =3,' ',;
    space(1)))))
  endfunc
  func oCMFLRISE_1_14.GetRadio()
    this.Parent.oContained.w_CMFLRISE = this.RadioValue()
    return .t.
  endfunc

  func oCMFLRISE_1_14.SetRadio()
    this.Parent.oContained.w_CMFLRISE=trim(this.Parent.oContained.w_CMFLRISE)
    this.value = ;
      iif(this.Parent.oContained.w_CMFLRISE=='+',1,;
      iif(this.Parent.oContained.w_CMFLRISE=='-',2,;
      iif(this.Parent.oContained.w_CMFLRISE=='',3,;
      0)))
  endfunc


  add object oCMFLAVAL_1_16 as StdCombo with uid="LUROHKHHPM",rtseq=16,rtrep=.f.,left=145,top=178,width=123,height=21;
    , ToolTipText = "Tipo di valorizzazione effettuata dal movimento";
    , HelpContextID = 204065166;
    , cFormVar="w_CMFLAVAL",RowSource=""+"Acquistato,"+"Venduto,"+"Altri carichi,"+"Altri scarichi,"+"Solo ord./imp./ris.,"+"Nessuno", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oCMFLAVAL_1_16.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'V',;
    iif(this.value =3,'C',;
    iif(this.value =4,'S',;
    iif(this.value =5,'X',;
    iif(this.value =6,'N',;
    space(1))))))))
  endfunc
  func oCMFLAVAL_1_16.GetRadio()
    this.Parent.oContained.w_CMFLAVAL = this.RadioValue()
    return .t.
  endfunc

  func oCMFLAVAL_1_16.SetRadio()
    this.Parent.oContained.w_CMFLAVAL=trim(this.Parent.oContained.w_CMFLAVAL)
    this.value = ;
      iif(this.Parent.oContained.w_CMFLAVAL=='A',1,;
      iif(this.Parent.oContained.w_CMFLAVAL=='V',2,;
      iif(this.Parent.oContained.w_CMFLAVAL=='C',3,;
      iif(this.Parent.oContained.w_CMFLAVAL=='S',4,;
      iif(this.Parent.oContained.w_CMFLAVAL=='X',5,;
      iif(this.Parent.oContained.w_CMFLAVAL=='N',6,;
      0))))))
  endfunc


  add object oCMVARVAL_1_17 as StdCombo with uid="PAAXYUGNAK",rtseq=17,rtrep=.f.,left=145,top=206,width=123,height=21;
    , ToolTipText = "Gestione storni: variazione a valore";
    , HelpContextID = 186894734;
    , cFormVar="w_CMVARVAL",RowSource=""+"Diminuisce,"+"Aumenta,"+"Non gestito", bObbl = .f. , nPag = 1;
    , sErrorMsg = "La variazione a valore � possibile solo se non � movimentato il magazzino, no mov.fiscale, e non si ha causali collegate";
  , bGlobalFont=.t.


  func oCMVARVAL_1_17.RadioValue()
    return(iif(this.value =1,'-',;
    iif(this.value =2,'+',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oCMVARVAL_1_17.GetRadio()
    this.Parent.oContained.w_CMVARVAL = this.RadioValue()
    return .t.
  endfunc

  func oCMVARVAL_1_17.SetRadio()
    this.Parent.oContained.w_CMVARVAL=trim(this.Parent.oContained.w_CMVARVAL)
    this.value = ;
      iif(this.Parent.oContained.w_CMVARVAL=='-',1,;
      iif(this.Parent.oContained.w_CMVARVAL=='+',2,;
      iif(this.Parent.oContained.w_CMVARVAL=='N',3,;
      0)))
  endfunc

  func oCMVARVAL_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CMFLAVAL $ 'AV')
    endwith
   endif
  endfunc

  func oCMVARVAL_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CMVARVAL='N' Or Empty(.w_CMFLCASC+.w_CMFLORDI+.w_CMFLIMPE+.w_CMFLRISE+.w_CMCAUCOL))
    endwith
    return bRes
  endfunc

  add object oCMFLELGM_1_18 as StdCheck with uid="HGKQHDSQYX",rtseq=18,rtrep=.f.,left=145, top=232, caption="Movimento fiscale",;
    ToolTipText = "Se attivo: causale valida per la contabilit� fiscale di magazzino",;
    HelpContextID = 99207565,;
    cFormVar="w_CMFLELGM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCMFLELGM_1_18.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCMFLELGM_1_18.GetRadio()
    this.Parent.oContained.w_CMFLELGM = this.RadioValue()
    return .t.
  endfunc

  func oCMFLELGM_1_18.SetRadio()
    this.Parent.oContained.w_CMFLELGM=trim(this.Parent.oContained.w_CMFLELGM)
    this.value = ;
      iif(this.Parent.oContained.w_CMFLELGM=='S',1,;
      0)
  endfunc

  func oCMFLELGM_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CMFLAVAL $ 'CS')
    endwith
   endif
  endfunc

  add object oCMAGGVAL_1_19 as StdCheck with uid="NGHIYUVCGG",rtseq=19,rtrep=.f.,left=145, top=255, caption="Aggiornamento valori",;
    ToolTipText = "Se attivo: il movimento aggiorna i valori di costo/prezzo d'inventario",;
    HelpContextID = 198121870,;
    cFormVar="w_CMAGGVAL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCMAGGVAL_1_19.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCMAGGVAL_1_19.GetRadio()
    this.Parent.oContained.w_CMAGGVAL = this.RadioValue()
    return .t.
  endfunc

  func oCMAGGVAL_1_19.SetRadio()
    this.Parent.oContained.w_CMAGGVAL=trim(this.Parent.oContained.w_CMAGGVAL)
    this.value = ;
      iif(this.Parent.oContained.w_CMAGGVAL=='S',1,;
      0)
  endfunc

  func oCMAGGVAL_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CMFLAVAL $ 'CS' AND .w_CMFLELGM='S')
    endwith
   endif
  endfunc

  add object oCMFLRIVA_1_20 as StdCheck with uid="CGJRQOEPEX",rtseq=20,rtrep=.f.,left=391, top=178, caption="Da rivalorizzare",;
    ToolTipText = "Abilita la rivalorizzazione del movimento",;
    HelpContextID = 135907737,;
    cFormVar="w_CMFLRIVA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCMFLRIVA_1_20.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCMFLRIVA_1_20.GetRadio()
    this.Parent.oContained.w_CMFLRIVA = this.RadioValue()
    return .t.
  endfunc

  func oCMFLRIVA_1_20.SetRadio()
    this.Parent.oContained.w_CMFLRIVA=trim(this.Parent.oContained.w_CMFLRIVA)
    this.value = ;
      iif(this.Parent.oContained.w_CMFLRIVA=='S',1,;
      0)
  endfunc

  func oCMFLRIVA_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CMFLAVAL $ 'CS')
    endwith
   endif
  endfunc


  add object oCMCRIVAL_1_21 as StdCombo with uid="RNDNZXPGOZ",rtseq=21,rtrep=.f.,left=391,top=205,width=139,height=21;
    , ToolTipText = "Criterio di valorizzazione dei trasferimenti e dei consumi";
    , HelpContextID = 195295630;
    , cFormVar="w_CMCRIVAL",RowSource=""+"Costo medio ponderato,"+"Costo standard,"+"Ultimo costo,"+"Nessuno", bObbl = .f. , nPag = 1;
    , sErrorMsg = "Impostare un criterio di rivalorizzazione";
  , bGlobalFont=.t.


  func oCMCRIVAL_1_21.RadioValue()
    return(iif(this.value =1,'CM',;
    iif(this.value =2,'CS',;
    iif(this.value =3,'UC',;
    iif(this.value =4,'NO',;
    space(2))))))
  endfunc
  func oCMCRIVAL_1_21.GetRadio()
    this.Parent.oContained.w_CMCRIVAL = this.RadioValue()
    return .t.
  endfunc

  func oCMCRIVAL_1_21.SetRadio()
    this.Parent.oContained.w_CMCRIVAL=trim(this.Parent.oContained.w_CMCRIVAL)
    this.value = ;
      iif(this.Parent.oContained.w_CMCRIVAL=='CM',1,;
      iif(this.Parent.oContained.w_CMCRIVAL=='CS',2,;
      iif(this.Parent.oContained.w_CMCRIVAL=='UC',3,;
      iif(this.Parent.oContained.w_CMCRIVAL=='NO',4,;
      0))))
  endfunc

  func oCMCRIVAL_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CMFLAVAL$'C,S')
    endwith
   endif
  endfunc

  func oCMCRIVAL_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_CMFLRIVA='S' AND .w_CMCRIVAL $ 'CM-UC-CS') OR .w_CMFLRIVA<>'S')
    endwith
    return bRes
  endfunc

  add object oCMSEQCAL_1_22 as StdField with uid="YVQEPRXQNO",rtseq=22,rtrep=.f.,;
    cFormVar = "w_CMSEQCAL", cQueryName = "CMSEQCAL",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Sequenza di stampa giornale magazzino e ricalcolo costo ultimo e medio ponderato",;
    HelpContextID = 238025102,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=611, Top=205, cSayPict='"99"', cGetPict='"99"'

  func oCMSEQCAL_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT(.w_CMFLAVAL$'X-N') And .w_CMVARVAL='N')
    endwith
   endif
  endfunc

  add object oCMDTINVA_1_36 as StdField with uid="OCLAYWWELC",rtseq=27,rtrep=.f.,;
    cFormVar = "w_CMDTINVA", cQueryName = "CMDTINVA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio validit�",;
    HelpContextID = 60942745,;
   bGlobalFont=.t.,;
    Height=19, Width=79, Left=347, Top=285, tabstop=.f.

  add object oCMDTOBSO_1_38 as StdField with uid="HFBXVYZXEZ",rtseq=29,rtrep=.f.,;
    cFormVar = "w_CMDTOBSO", cQueryName = "CMDTOBSO",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Ultima data di validit�",;
    HelpContextID = 255977867,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=568, Top=285, tabstop=.f.


  add object oObj_1_49 as cp_runprogram with uid="JTEUZJKRRA",left=365, top=335, width=208,height=18,;
    caption='GSMA_BCM',;
   bGlobalFont=.t.,;
    prg="GSMA_BCM",;
    cEvent = "Insert start,Update start",;
    nPag=1;
    , HelpContextID = 240407373


  add object oObj_1_50 as cp_runprogram with uid="MCLVACVPTQ",left=4, top=335, width=354,height=18,;
    caption='GSMA_BWC',;
   bGlobalFont=.t.,;
    prg='GSMA_BWC',;
    cEvent = "w_CMFLAVAL Changed,w_CMFLCASC Changed",;
    nPag=1;
    , ToolTipText = "Mostra a video messaggio di warning";
    , HelpContextID = 240407383

  add object oStr_1_23 as StdString with uid="HMATDXAFIS",Visible=.t., Left=3, Top=10,;
    Alignment=1, Width=57, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_24 as StdString with uid="HAQFFRZWFG",Visible=.t., Left=321, Top=114,;
    Alignment=1, Width=67, Height=18,;
    Caption="Impegnato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="QAEXIVRVKV",Visible=.t., Left=152, Top=114,;
    Alignment=1, Width=67, Height=18,;
    Caption="Ordinato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="OVNFKILEKU",Visible=.t., Left=386, Top=10,;
    Alignment=1, Width=198, Height=15,;
    Caption="Causale collegata:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="VRNGPZNDRX",Visible=.t., Left=542, Top=205,;
    Alignment=1, Width=67, Height=15,;
    Caption="Sequenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="BHZZZAPWMO",Visible=.t., Left=482, Top=114,;
    Alignment=1, Width=67, Height=18,;
    Caption="Riservato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="FQLEBIWKXS",Visible=.t., Left=445, Top=285,;
    Alignment=1, Width=123, Height=15,;
    Caption="Data obsolescenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="PRLBTTIQGK",Visible=.t., Left=-2, Top=114,;
    Alignment=1, Width=67, Height=18,;
    Caption="Esistenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="ZUZBDXTTAO",Visible=.t., Left=234, Top=285,;
    Alignment=1, Width=110, Height=15,;
    Caption="Data validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="FVYKXEEGBX",Visible=.t., Left=3, Top=40,;
    Alignment=1, Width=57, Height=15,;
    Caption="Riferim.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="LLMAAOMRJI",Visible=.t., Left=5, Top=87,;
    Alignment=0, Width=159, Height=17,;
    Caption="Aggiornamento saldi"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_43 as StdString with uid="MXZCZZDOVJ",Visible=.t., Left=5, Top=148,;
    Alignment=0, Width=187, Height=15,;
    Caption="Valorizzazione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_46 as StdString with uid="SSDTZJAQSL",Visible=.t., Left=365, Top=40,;
    Alignment=1, Width=126, Height=15,;
    Caption="Gestione progetti:"  ;
  , bGlobalFont=.t.

  func oStr_1_46.mHide()
    with this.Parent.oContained
      return (g_COMM<>'S')
    endwith
  endfunc

  add object oStr_1_47 as StdString with uid="MKZMJNDKBK",Visible=.t., Left=3, Top=178,;
    Alignment=1, Width=139, Height=15,;
    Caption="Valore da aggiornare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="RXBHHRLQAM",Visible=.t., Left=276, Top=205,;
    Alignment=1, Width=112, Height=15,;
    Caption="Tipo valorizzazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="IACWNGSNEO",Visible=.t., Left=24, Top=206,;
    Alignment=1, Width=116, Height=18,;
    Caption="Variazioni di valore:"  ;
  , bGlobalFont=.t.

  add object oBox_1_44 as StdBox with uid="RGKGWYHQJH",left=5, top=105, width=642,height=1

  add object oBox_1_45 as StdBox with uid="IFVNGDMTXR",left=5, top=166, width=642,height=1
enddefine
define class tgsma_acmPag2 as StdContainer
  Width  = 652
  height = 316
  stdWidth  = 652
  stdheight = 316
  resizeXpos=323
  resizeYpos=263
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCMNATTRA_2_1 as StdField with uid="DWUVWSFMIP",rtseq=32,rtrep=.f.,;
    cFormVar = "w_CMNATTRA", cQueryName = "CMNATTRA",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Natura transazione (F9 =tabella)",;
    HelpContextID = 218384793,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=230, Top=70, cSayPict='"XXX"', cGetPict='"XXX"', InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="TIPITRAN", cZoomOnZoom="GSAR_ATT", oKey_1_1="TTCODICE", oKey_1_2="this.w_CMNATTRA"

  func oCMNATTRA_2_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_INTR='S')
    endwith
   endif
  endfunc

  func oCMNATTRA_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCMNATTRA_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCMNATTRA_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIPITRAN','*','TTCODICE',cp_AbsName(this.parent,'oCMNATTRA_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATT',"Natura transazioni",'',this.parent.oContained
  endproc
  proc oCMNATTRA_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TTCODICE=this.parent.oContained.w_CMNATTRA
     i_obj.ecpSave()
  endproc

  add object oTIDESC_2_2 as StdField with uid="UYQBQZYUHA",rtseq=33,rtrep=.f.,;
    cFormVar = "w_TIDESC", cQueryName = "TIDESC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 235990218,;
   bGlobalFont=.t.,;
    Height=21, Width=240, Left=279, Top=70, cSayPict='repl("X",30)', cGetPict='repl("X",30)', InputMask=replicate('X',30)

  add object oCODI_2_7 as StdField with uid="ODZYGGINQZ",rtseq=34,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 163780646,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=58, Left=63, Top=10, InputMask=replicate('X',5)

  add object oDESC_2_8 as StdField with uid="JEJXOZVRXT",rtseq=35,rtrep=.f.,;
    cFormVar = "w_DESC", cQueryName = "DESC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 163446326,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=129, Top=10, InputMask=replicate('X',35)


  add object oLinkPC_2_9 as stdDynamicChildContainer with uid="WGWYIGYBIF",left=6, top=150, width=564, height=149, bOnScreen=.t.;


  add object oCMNATTR2_2_11 as StdField with uid="MQSDMEMPQQ",rtseq=38,rtrep=.f.,;
    cFormVar = "w_CMNATTR2", cQueryName = "CMNATTR2",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Utilizzata in NC per accorpamento con fattura in generazione file INTRA",;
    HelpContextID = 218384808,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=230, Top=97, cSayPict='"XXX"', cGetPict='"XXX"', InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="TIPITRAN", cZoomOnZoom="GSAR_ATT", oKey_1_1="TTCODICE", oKey_1_2="this.w_CMNATTR2"

  func oCMNATTR2_2_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_INTR='S')
    endwith
   endif
  endfunc

  func oCMNATTR2_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oCMNATTR2_2_11.ecpDrop(oSource)
    this.Parent.oContained.link_2_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCMNATTR2_2_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIPITRAN','*','TTCODICE',cp_AbsName(this.parent,'oCMNATTR2_2_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATT',"Natura transazioni",'',this.parent.oContained
  endproc
  proc oCMNATTR2_2_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TTCODICE=this.parent.oContained.w_CMNATTR2
     i_obj.ecpSave()
  endproc

  add object oTIDES2_2_12 as StdField with uid="HINEPVAHYT",rtseq=39,rtrep=.f.,;
    cFormVar = "w_TIDES2", cQueryName = "TIDES2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 15668022,;
   bGlobalFont=.t.,;
    Height=21, Width=240, Left=279, Top=97, cSayPict='repl("X",30)', cGetPict='repl("X",30)', InputMask=replicate('X',30)

  add object oStr_2_3 as StdString with uid="HUPMIPMJNK",Visible=.t., Left=93, Top=70,;
    Alignment=1, Width=134, Height=15,;
    Caption="Natura transazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_4 as StdString with uid="XCOVITBFQP",Visible=.t., Left=2, Top=10,;
    Alignment=1, Width=58, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_6 as StdString with uid="TKWAMATUGP",Visible=.t., Left=10, Top=39,;
    Alignment=0, Width=634, Height=15,;
    Caption="Dati per transazioni intracomunitarie"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_10 as StdString with uid="XZGONLVFXM",Visible=.t., Left=6, Top=130,;
    Alignment=0, Width=470, Height=15,;
    Caption="Traduzioni in lingua estera"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_13 as StdString with uid="PTRRHONTAZ",Visible=.t., Left=17, Top=97,;
    Alignment=1, Width=210, Height=18,;
    Caption="Natura transazione per storno NC:"  ;
  , bGlobalFont=.t.

  add object oBox_2_5 as StdBox with uid="XNJMDUHSBA",left=10, top=56, width=637,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_acm','CAM_AGAZ','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CMCODICE=CAM_AGAZ.CMCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
