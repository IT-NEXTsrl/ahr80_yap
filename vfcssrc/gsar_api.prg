* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_api                                                        *
*              Conti                                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_140]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-12                                                      *
* Last revis.: 2015-11-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_api"))

* --- Class definition
define class tgsar_api as StdForm
  Top    = 10
  Left   = 16

  * --- Standard Properties
  Width  = 595
  Height = 336+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-11-20"
  HelpContextID=108431511
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=37

  * --- Constant Properties
  CONTI_IDX = 0
  MASTRI_IDX = 0
  COLLCENT_IDX = 0
  VALUTE_IDX = 0
  PAG_AMEN_IDX = 0
  STU_PIAC_IDX = 0
  NUMAUT_M_IDX = 0
  cFile = "CONTI"
  cKeySelect = "ANTIPCON,ANCODICE"
  cQueryFilter="ANTIPCON='G'"
  cKeyWhere  = "ANTIPCON=this.w_ANTIPCON and ANCODICE=this.w_ANCODICE"
  cKeyWhereODBC = '"ANTIPCON="+cp_ToStrODBC(this.w_ANTIPCON)';
      +'+" and ANCODICE="+cp_ToStrODBC(this.w_ANCODICE)';

  cKeyWhereODBCqualified = '"CONTI.ANTIPCON="+cp_ToStrODBC(this.w_ANTIPCON)';
      +'+" and CONTI.ANCODICE="+cp_ToStrODBC(this.w_ANCODICE)';

  cPrg = "gsar_api"
  cComment = "Conti"
  icon = "anag.ico"
  cAutoZoom = 'GSAR0API'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_AUTOAZI = space(5)
  w_AUTO = space(1)
  o_AUTO = space(1)
  w_ANTIPCON = space(1)
  w_ANCODICE = space(15)
  w_ANDESCRI = space(40)
  w_ANNOTAIN = 0
  w_ANCONSUP = space(15)
  o_ANCONSUP = space(15)
  w_CODSTU = space(3)
  o_CODSTU = space(3)
  w_PROSTU = space(8)
  w_DESSUP = space(40)
  w_ANTIPSOT = space(1)
  o_ANTIPSOT = space(1)
  w_ANRISESE = space(1)
  w_ANFLIVAD = space(1)
  w_ANPARTSN = space(1)
  w_CODPIA = space(4)
  w_SEZBIL = space(10)
  w_OKCC = space(1)
  o_OKCC = space(1)
  w_UTCC = 0
  w_UTCV = 0
  w_OBTEST = ctod('  /  /  ')
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_ANDTOBSO = ctod('  /  /  ')
  w_NULIV = 0
  w_TIPMAS = space(1)
  w_ANDTINVA = ctod('  /  /  ')
  w_ANCODSTU = space(10)
  o_ANCODSTU = space(10)
  w_ANCODPAG = space(5)
  w_DESPAG = space(30)
  w_CODI = space(15)
  w_DESC = space(40)
  w_ANCCNOTE = space(50)
  w_ANCCTAGG = space(1)
  w_DTOBSO = ctod('  /  /  ')
  w_DESSOT = space(30)
  w_ANFLACBD = space(1)
  w_ANESCDOF = space(1)

  * --- Children pointers
  GSAR_MRC = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CONTI','gsar_api')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_apiPag1","gsar_api",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Conto")
      .Pages(1).HelpContextID = 232905766
      .Pages(2).addobject("oPag","tgsar_apiPag2","gsar_api",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Analitica")
      .Pages(2).HelpContextID = 24711545
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oANCODICE_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='MASTRI'
    this.cWorkTables[2]='COLLCENT'
    this.cWorkTables[3]='VALUTE'
    this.cWorkTables[4]='PAG_AMEN'
    this.cWorkTables[5]='STU_PIAC'
    this.cWorkTables[6]='NUMAUT_M'
    this.cWorkTables[7]='CONTI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(7))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CONTI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CONTI_IDX,3]
  return

  function CreateChildren()
    this.GSAR_MRC = CREATEOBJECT('stdDynamicChild',this,'GSAR_MRC',this.oPgFrm.Page2.oPag.oLinkPC_2_6)
    this.GSAR_MRC.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSAR_MRC)
      this.GSAR_MRC.DestroyChildrenChain()
      this.GSAR_MRC=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_2_6')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSAR_MRC.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSAR_MRC.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSAR_MRC.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSAR_MRC.SetKey(;
            .w_ANTIPCON,"MRTIPCON";
            ,.w_ANCODICE,"MRCODICE";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSAR_MRC.ChangeRow(this.cRowID+'      1',1;
             ,.w_ANTIPCON,"MRTIPCON";
             ,.w_ANCODICE,"MRCODICE";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSAR_MRC)
        i_f=.GSAR_MRC.BuildFilter()
        if !(i_f==.GSAR_MRC.cQueryFilter)
          i_fnidx=.GSAR_MRC.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAR_MRC.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAR_MRC.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAR_MRC.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAR_MRC.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_ANTIPCON = NVL(ANTIPCON,space(1))
      .w_ANCODICE = NVL(ANCODICE,space(15))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_7_joined
    link_1_7_joined=.f.
    local link_1_38_joined
    link_1_38_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from CONTI where ANTIPCON=KeySet.ANTIPCON
    *                            and ANCODICE=KeySet.ANCODICE
    *
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CONTI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CONTI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CONTI '
      link_1_7_joined=this.AddJoinedLink_1_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_38_joined=this.AddJoinedLink_1_38(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'ANTIPCON',this.w_ANTIPCON  ,'ANCODICE',this.w_ANCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_AUTOAZI = i_CODAZI
        .w_AUTO = space(1)
        .w_CODSTU = space(3)
        .w_PROSTU = space(8)
        .w_DESSUP = space(40)
        .w_CODPIA = space(4)
        .w_OBTEST = i_datsys
        .w_NULIV = 0
        .w_TIPMAS = space(1)
        .w_DESPAG = space(30)
        .w_DTOBSO = ctod("  /  /  ")
        .w_DESSOT = space(30)
          .link_1_1('Load')
        .w_ANTIPCON = NVL(ANTIPCON,space(1))
        .w_ANCODICE = NVL(ANCODICE,space(15))
        .w_ANDESCRI = NVL(ANDESCRI,space(40))
        .w_ANNOTAIN = NVL(ANNOTAIN,0)
        .w_ANCONSUP = NVL(ANCONSUP,space(15))
          if link_1_7_joined
            this.w_ANCONSUP = NVL(MCCODICE107,NVL(this.w_ANCONSUP,space(15)))
            this.w_DESSUP = NVL(MCDESCRI107,space(40))
            this.w_NULIV = NVL(MCNUMLIV107,0)
            this.w_TIPMAS = NVL(MCTIPMAS107,space(1))
            this.w_CODSTU = NVL(MCCODSTU107,space(3))
            this.w_PROSTU = NVL(MCPROSTU107,space(8))
          else
          .link_1_7('Load')
          endif
        .w_ANTIPSOT = NVL(ANTIPSOT,space(1))
        .w_ANRISESE = NVL(ANRISESE,space(1))
        .w_ANFLIVAD = NVL(ANFLIVAD,space(1))
        .w_ANPARTSN = NVL(ANPARTSN,space(1))
        .w_SEZBIL = CALCSEZ(.w_ANCONSUP)
        .w_OKCC = IIF(.w_SEZBIL $ "CR" AND g_PERCCR="S", "S", "N")
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_ANDTOBSO = NVL(cp_ToDate(ANDTOBSO),ctod("  /  /  "))
        .w_ANDTINVA = NVL(cp_ToDate(ANDTINVA),ctod("  /  /  "))
        .w_ANCODSTU = NVL(ANCODSTU,space(10))
          .link_1_36('Load')
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate(FUNSEZ(.w_SEZBIL),'','')
        .w_ANCODPAG = NVL(ANCODPAG,space(5))
          if link_1_38_joined
            this.w_ANCODPAG = NVL(PACODICE138,NVL(this.w_ANCODPAG,space(5)))
            this.w_DESPAG = NVL(PADESCRI138,space(30))
            this.w_DTOBSO = NVL(cp_ToDate(PADTOBSO138),ctod("  /  /  "))
          else
          .link_1_38('Load')
          endif
        .w_CODI = .w_ANCODICE
        .w_DESC = .w_ANDESCRI
        .w_ANCCNOTE = NVL(ANCCNOTE,space(50))
        .w_ANCCTAGG = NVL(ANCCTAGG,space(1))
        .oPgFrm.Page2.oPag.oObj_2_9.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_10.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        .w_ANFLACBD = NVL(ANFLACBD,space(1))
        .w_ANESCDOF = NVL(ANESCDOF,space(1))
        cp_LoadRecExtFlds(this,'CONTI')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_33.enabled = this.oPgFrm.Page1.oPag.oBtn_1_33.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AUTOAZI = space(5)
      .w_AUTO = space(1)
      .w_ANTIPCON = space(1)
      .w_ANCODICE = space(15)
      .w_ANDESCRI = space(40)
      .w_ANNOTAIN = 0
      .w_ANCONSUP = space(15)
      .w_CODSTU = space(3)
      .w_PROSTU = space(8)
      .w_DESSUP = space(40)
      .w_ANTIPSOT = space(1)
      .w_ANRISESE = space(1)
      .w_ANFLIVAD = space(1)
      .w_ANPARTSN = space(1)
      .w_CODPIA = space(4)
      .w_SEZBIL = space(10)
      .w_OKCC = space(1)
      .w_UTCC = 0
      .w_UTCV = 0
      .w_OBTEST = ctod("  /  /  ")
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_ANDTOBSO = ctod("  /  /  ")
      .w_NULIV = 0
      .w_TIPMAS = space(1)
      .w_ANDTINVA = ctod("  /  /  ")
      .w_ANCODSTU = space(10)
      .w_ANCODPAG = space(5)
      .w_DESPAG = space(30)
      .w_CODI = space(15)
      .w_DESC = space(40)
      .w_ANCCNOTE = space(50)
      .w_ANCCTAGG = space(1)
      .w_DTOBSO = ctod("  /  /  ")
      .w_DESSOT = space(30)
      .w_ANFLACBD = space(1)
      .w_ANESCDOF = space(1)
      if .cFunction<>"Filter"
        .w_AUTOAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_AUTOAZI))
          .link_1_1('Full')
          endif
          .DoRTCalc(2,2,.f.)
        .w_ANTIPCON = 'G'
        .w_ANCODICE = iif(.cFunction='Load' AND .w_AUTO='S', 'AUTO',.w_ANCODICE)
        .DoRTCalc(5,7,.f.)
          if not(empty(.w_ANCONSUP))
          .link_1_7('Full')
          endif
          .DoRTCalc(8,10,.f.)
        .w_ANTIPSOT = 'X'
        .w_ANRISESE = IIF(.w_ANTIPSOT<>'X', ' ', .w_ANRISESE)
        .w_ANFLIVAD = 'N'
          .DoRTCalc(14,15,.f.)
        .w_SEZBIL = CALCSEZ(.w_ANCONSUP)
        .w_OKCC = IIF(.w_SEZBIL $ "CR" AND g_PERCCR="S", "S", "N")
          .DoRTCalc(18,19,.f.)
        .w_OBTEST = i_datsys
        .DoRTCalc(21,27,.f.)
          if not(empty(.w_ANCODSTU))
          .link_1_36('Full')
          endif
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate(FUNSEZ(.w_SEZBIL),'','')
        .DoRTCalc(28,28,.f.)
          if not(empty(.w_ANCODPAG))
          .link_1_38('Full')
          endif
          .DoRTCalc(29,29,.f.)
        .w_CODI = .w_ANCODICE
        .w_DESC = .w_ANDESCRI
          .DoRTCalc(32,32,.f.)
        .w_ANCCTAGG = IIF(.w_OKCC<>'S', 'E', IIF(.w_ANCCTAGG $ 'AM', .w_ANCCTAGG, 'M'))
        .oPgFrm.Page2.oPag.oObj_2_9.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_10.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
          .DoRTCalc(34,35,.f.)
        .w_ANFLACBD = ' '
        .w_ANESCDOF = 'N'
      endif
    endwith
    cp_BlankRecExtFlds(this,'CONTI')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_33.enabled = this.oPgFrm.Page1.oPag.oBtn_1_33.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oANCODICE_1_4.enabled = i_bVal
      .Page1.oPag.oANDESCRI_1_5.enabled = i_bVal
      .Page1.oPag.oANNOTAIN_1_6.enabled = i_bVal
      .Page1.oPag.oANCONSUP_1_7.enabled = i_bVal
      .Page1.oPag.oANTIPSOT_1_11.enabled = i_bVal
      .Page1.oPag.oANRISESE_1_12.enabled = i_bVal
      .Page1.oPag.oANFLIVAD_1_13.enabled = i_bVal
      .Page1.oPag.oANPARTSN_1_14.enabled = i_bVal
      .Page1.oPag.oANDTOBSO_1_28.enabled = i_bVal
      .Page1.oPag.oANDTINVA_1_35.enabled = i_bVal
      .Page1.oPag.oANCODSTU_1_36.enabled = i_bVal
      .Page1.oPag.oANCODPAG_1_38.enabled = i_bVal
      .Page2.oPag.oANCCNOTE_2_3.enabled = i_bVal
      .Page2.oPag.oANCCTAGG_2_5.enabled = i_bVal
      .Page1.oPag.oANESCDOF_1_48.enabled = i_bVal
      .Page1.oPag.oBtn_1_33.enabled = .Page1.oPag.oBtn_1_33.mCond()
      .Page2.oPag.oObj_2_9.enabled = i_bVal
      .Page2.oPag.oObj_2_10.enabled = i_bVal
      .Page1.oPag.oObj_1_44.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oANCODICE_1_4.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oANCODICE_1_4.enabled = .t.
        .Page1.oPag.oANDESCRI_1_5.enabled = .t.
      endif
    endwith
    this.GSAR_MRC.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'CONTI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSAR_MRC.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANTIPCON,"ANTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODICE,"ANCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANDESCRI,"ANDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANNOTAIN,"ANNOTAIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCONSUP,"ANCONSUP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANTIPSOT,"ANTIPSOT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANRISESE,"ANRISESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANFLIVAD,"ANFLIVAD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANPARTSN,"ANPARTSN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANDTOBSO,"ANDTOBSO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANDTINVA,"ANDTINVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODSTU,"ANCODSTU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODPAG,"ANCODPAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCCNOTE,"ANCCNOTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCCTAGG,"ANCCTAGG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANFLACBD,"ANFLACBD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANESCDOF,"ANESCDOF",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- gsar_api
    * --- aggiunge alla Chiave ulteriore filtro su Tipo Conto
    IF NOT EMPTY(i_cWhere)
       IF AT('ANTIPSOT', UPPER(i_cWhere))=0
          i_cWhere=i_cWhere+" and ANTIPCON='G'"
       ENDIF
    ENDIF
    
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    i_lTable = "CONTI"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CONTI_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSAR_SMC with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CONTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.CONTI_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CONTI
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CONTI')
        i_extval=cp_InsertValODBCExtFlds(this,'CONTI')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(ANTIPCON,ANCODICE,ANDESCRI,ANNOTAIN,ANCONSUP"+;
                  ",ANTIPSOT,ANRISESE,ANFLIVAD,ANPARTSN,UTCC"+;
                  ",UTCV,UTDC,UTDV,ANDTOBSO,ANDTINVA"+;
                  ",ANCODSTU,ANCODPAG,ANCCNOTE,ANCCTAGG,ANFLACBD"+;
                  ",ANESCDOF "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_ANTIPCON)+;
                  ","+cp_ToStrODBC(this.w_ANCODICE)+;
                  ","+cp_ToStrODBC(this.w_ANDESCRI)+;
                  ","+cp_ToStrODBC(this.w_ANNOTAIN)+;
                  ","+cp_ToStrODBCNull(this.w_ANCONSUP)+;
                  ","+cp_ToStrODBC(this.w_ANTIPSOT)+;
                  ","+cp_ToStrODBC(this.w_ANRISESE)+;
                  ","+cp_ToStrODBC(this.w_ANFLIVAD)+;
                  ","+cp_ToStrODBC(this.w_ANPARTSN)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBC(this.w_ANDTOBSO)+;
                  ","+cp_ToStrODBC(this.w_ANDTINVA)+;
                  ","+cp_ToStrODBCNull(this.w_ANCODSTU)+;
                  ","+cp_ToStrODBCNull(this.w_ANCODPAG)+;
                  ","+cp_ToStrODBC(this.w_ANCCNOTE)+;
                  ","+cp_ToStrODBC(this.w_ANCCTAGG)+;
                  ","+cp_ToStrODBC(this.w_ANFLACBD)+;
                  ","+cp_ToStrODBC(this.w_ANESCDOF)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CONTI')
        i_extval=cp_InsertValVFPExtFlds(this,'CONTI')
        cp_CheckDeletedKey(i_cTable,0,'ANTIPCON',this.w_ANTIPCON,'ANCODICE',this.w_ANCODICE)
        INSERT INTO (i_cTable);
              (ANTIPCON,ANCODICE,ANDESCRI,ANNOTAIN,ANCONSUP,ANTIPSOT,ANRISESE,ANFLIVAD,ANPARTSN,UTCC,UTCV,UTDC,UTDV,ANDTOBSO,ANDTINVA,ANCODSTU,ANCODPAG,ANCCNOTE,ANCCTAGG,ANFLACBD,ANESCDOF  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_ANTIPCON;
                  ,this.w_ANCODICE;
                  ,this.w_ANDESCRI;
                  ,this.w_ANNOTAIN;
                  ,this.w_ANCONSUP;
                  ,this.w_ANTIPSOT;
                  ,this.w_ANRISESE;
                  ,this.w_ANFLIVAD;
                  ,this.w_ANPARTSN;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_ANDTOBSO;
                  ,this.w_ANDTINVA;
                  ,this.w_ANCODSTU;
                  ,this.w_ANCODPAG;
                  ,this.w_ANCCNOTE;
                  ,this.w_ANCCTAGG;
                  ,this.w_ANFLACBD;
                  ,this.w_ANESCDOF;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.CONTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.CONTI_IDX,i_nConn)
      *
      * update CONTI
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'CONTI')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " ANDESCRI="+cp_ToStrODBC(this.w_ANDESCRI)+;
             ",ANNOTAIN="+cp_ToStrODBC(this.w_ANNOTAIN)+;
             ",ANCONSUP="+cp_ToStrODBCNull(this.w_ANCONSUP)+;
             ",ANTIPSOT="+cp_ToStrODBC(this.w_ANTIPSOT)+;
             ",ANRISESE="+cp_ToStrODBC(this.w_ANRISESE)+;
             ",ANFLIVAD="+cp_ToStrODBC(this.w_ANFLIVAD)+;
             ",ANPARTSN="+cp_ToStrODBC(this.w_ANPARTSN)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",ANDTOBSO="+cp_ToStrODBC(this.w_ANDTOBSO)+;
             ",ANDTINVA="+cp_ToStrODBC(this.w_ANDTINVA)+;
             ",ANCODSTU="+cp_ToStrODBCNull(this.w_ANCODSTU)+;
             ",ANCODPAG="+cp_ToStrODBCNull(this.w_ANCODPAG)+;
             ",ANCCNOTE="+cp_ToStrODBC(this.w_ANCCNOTE)+;
             ",ANCCTAGG="+cp_ToStrODBC(this.w_ANCCTAGG)+;
             ",ANFLACBD="+cp_ToStrODBC(this.w_ANFLACBD)+;
             ",ANESCDOF="+cp_ToStrODBC(this.w_ANESCDOF)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'CONTI')
        i_cWhere = cp_PKFox(i_cTable  ,'ANTIPCON',this.w_ANTIPCON  ,'ANCODICE',this.w_ANCODICE  )
        UPDATE (i_cTable) SET;
              ANDESCRI=this.w_ANDESCRI;
             ,ANNOTAIN=this.w_ANNOTAIN;
             ,ANCONSUP=this.w_ANCONSUP;
             ,ANTIPSOT=this.w_ANTIPSOT;
             ,ANRISESE=this.w_ANRISESE;
             ,ANFLIVAD=this.w_ANFLIVAD;
             ,ANPARTSN=this.w_ANPARTSN;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,ANDTOBSO=this.w_ANDTOBSO;
             ,ANDTINVA=this.w_ANDTINVA;
             ,ANCODSTU=this.w_ANCODSTU;
             ,ANCODPAG=this.w_ANCODPAG;
             ,ANCCNOTE=this.w_ANCCNOTE;
             ,ANCCTAGG=this.w_ANCCTAGG;
             ,ANFLACBD=this.w_ANFLACBD;
             ,ANESCDOF=this.w_ANESCDOF;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSAR_MRC : Saving
      this.GSAR_MRC.ChangeRow(this.cRowID+'      1',0;
             ,this.w_ANTIPCON,"MRTIPCON";
             ,this.w_ANCODICE,"MRCODICE";
             )
      this.GSAR_MRC.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSAR_MRC : Deleting
    this.GSAR_MRC.ChangeRow(this.cRowID+'      1',0;
           ,this.w_ANTIPCON,"MRTIPCON";
           ,this.w_ANCODICE,"MRCODICE";
           )
    this.GSAR_MRC.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CONTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.CONTI_IDX,i_nConn)
      *
      * delete CONTI
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'ANTIPCON',this.w_ANTIPCON  ,'ANCODICE',this.w_ANCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,3,.t.)
        if .o_AUTO<>.w_AUTO
            .w_ANCODICE = iif(.cFunction='Load' AND .w_AUTO='S', 'AUTO',.w_ANCODICE)
        endif
        .DoRTCalc(5,11,.t.)
        if .o_ANTIPSOT<>.w_ANTIPSOT
            .w_ANRISESE = IIF(.w_ANTIPSOT<>'X', ' ', .w_ANRISESE)
        endif
        if .o_ANTIPSOT<>.w_ANTIPSOT
            .w_ANFLIVAD = 'N'
        endif
        .DoRTCalc(14,15,.t.)
        if .o_ANCONSUP<>.w_ANCONSUP
            .w_SEZBIL = CALCSEZ(.w_ANCONSUP)
        endif
            .w_OKCC = IIF(.w_SEZBIL $ "CR" AND g_PERCCR="S", "S", "N")
        .DoRTCalc(18,26,.t.)
        if .o_ANCODSTU<>.w_ANCODSTU
          .link_1_36('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate(FUNSEZ(.w_SEZBIL),'','')
        .DoRTCalc(28,29,.t.)
            .w_CODI = .w_ANCODICE
            .w_DESC = .w_ANDESCRI
        .DoRTCalc(32,32,.t.)
        if .o_OKCC<>.w_OKCC
            .w_ANCCTAGG = IIF(.w_OKCC<>'S', 'E', IIF(.w_ANCCTAGG $ 'AM', .w_ANCCTAGG, 'M'))
        endif
        .oPgFrm.Page2.oPag.oObj_2_9.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_10.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        if .o_CODSTU<>.w_CODSTU
          .Calculate_DBRMIMJNUE()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(34,37,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate(FUNSEZ(.w_SEZBIL),'','')
        .oPgFrm.Page2.oPag.oObj_2_9.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_10.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
    endwith
  return

  proc Calculate_DBRMIMJNUE()
    with this
          * --- Calcolo valore variabile w_codpia
          .w_CODPIA = iif(g_LEMC='S',Looktab('STU_PARA','LMCODPDC','LMCODAZI',i_CODAZI),space(4))
    endwith
  endproc
  proc Calculate_OLYGVFQXTR()
    with this
          * --- Calcola Auto
          GSAR_BSS(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oANCODICE_1_4.enabled = this.oPgFrm.Page1.oPag.oANCODICE_1_4.mCond()
    this.oPgFrm.Page1.oPag.oANRISESE_1_12.enabled = this.oPgFrm.Page1.oPag.oANRISESE_1_12.mCond()
    this.oPgFrm.Page1.oPag.oANPARTSN_1_14.enabled = this.oPgFrm.Page1.oPag.oANPARTSN_1_14.mCond()
    this.oPgFrm.Page1.oPag.oANCODSTU_1_36.enabled = this.oPgFrm.Page1.oPag.oANCODSTU_1_36.mCond()
    this.oPgFrm.Page2.oPag.oANCCNOTE_2_3.enabled = this.oPgFrm.Page2.oPag.oANCCNOTE_2_3.mCond()
    this.oPgFrm.Page2.oPag.oANCCTAGG_2_5.enabled = this.oPgFrm.Page2.oPag.oANCCTAGG_2_5.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_33.enabled = this.oPgFrm.Page1.oPag.oBtn_1_33.mCond()
    this.GSAR_MRC.enabled = this.oPgFrm.Page2.oPag.oLinkPC_2_6.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oANFLIVAD_1_13.visible=!this.oPgFrm.Page1.oPag.oANFLIVAD_1_13.mHide()
    this.oPgFrm.Page1.oPag.oANCODSTU_1_36.visible=!this.oPgFrm.Page1.oPag.oANCODSTU_1_36.mHide()
    this.oPgFrm.Page1.oPag.oANCODPAG_1_38.visible=!this.oPgFrm.Page1.oPag.oANCODPAG_1_38.mHide()
    this.oPgFrm.Page1.oPag.oDESPAG_1_39.visible=!this.oPgFrm.Page1.oPag.oDESPAG_1_39.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_40.visible=!this.oPgFrm.Page1.oPag.oStr_1_40.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_42.visible=!this.oPgFrm.Page1.oPag.oStr_1_42.mHide()
    this.oPgFrm.Page1.oPag.oDESSOT_1_43.visible=!this.oPgFrm.Page1.oPag.oDESSOT_1_43.mHide()
    this.oPgFrm.Page1.oPag.oANESCDOF_1_48.visible=!this.oPgFrm.Page1.oPag.oANESCDOF_1_48.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsar_api
    
    
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_37.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_9.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_10.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_44.Event(cEvent)
        if lower(cEvent)==lower("Blank") or lower(cEvent)==lower("Edit Started") or lower(cEvent)==lower("Init")
          .Calculate_DBRMIMJNUE()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Insert start")
          .Calculate_OLYGVFQXTR()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=AUTOAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NUMAUT_M_IDX,3]
    i_lTable = "NUMAUT_M"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NUMAUT_M_IDX,2], .t., this.NUMAUT_M_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NUMAUT_M_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AUTOAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AUTOAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NATIPGES,NACODAZI,NAACTIVE";
                   +" from "+i_cTable+" "+i_lTable+" where NACODAZI="+cp_ToStrODBC(this.w_AUTOAZI);
                   +" and NATIPGES="+cp_ToStrODBC('CG');
                   +" and NACODAZI="+cp_ToStrODBC(this.w_AUTOAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NATIPGES','CG';
                       ,'NACODAZI',this.w_AUTOAZI;
                       ,'NACODAZI',this.w_AUTOAZI)
            select NATIPGES,NACODAZI,NAACTIVE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AUTOAZI = NVL(_Link_.NACODAZI,space(5))
      this.w_AUTO = NVL(_Link_.NAACTIVE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_AUTOAZI = space(5)
      endif
      this.w_AUTO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NUMAUT_M_IDX,2])+'\'+cp_ToStr(_Link_.NATIPGES,1)+'\'+cp_ToStr(_Link_.NACODAZI,1)+'\'+cp_ToStr(_Link_.NACODAZI,1)
      cp_ShowWarn(i_cKey,this.NUMAUT_M_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AUTOAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANCONSUP
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_lTable = "MASTRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2], .t., this.MASTRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCONSUP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMC',True,'MASTRI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MCCODICE like "+cp_ToStrODBC(trim(this.w_ANCONSUP)+"%");

          i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS,MCCODSTU,MCPROSTU";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MCCODICE',trim(this.w_ANCONSUP))
          select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS,MCCODSTU,MCPROSTU;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCONSUP)==trim(_Link_.MCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStrODBC(trim(this.w_ANCONSUP)+"%");

            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS,MCCODSTU,MCPROSTU";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStr(trim(this.w_ANCONSUP)+"%");

            select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS,MCCODSTU,MCPROSTU;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ANCONSUP) and !this.bDontReportError
            deferred_cp_zoom('MASTRI','*','MCCODICE',cp_AbsName(oSource.parent,'oANCONSUP_1_7'),i_cWhere,'GSAR_AMC',"Mastri contabili (generici)",'GSAR_API.MASTRI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS,MCCODSTU,MCPROSTU";
                     +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',oSource.xKey(1))
            select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS,MCCODSTU,MCPROSTU;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCONSUP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS,MCCODSTU,MCPROSTU";
                   +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(this.w_ANCONSUP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',this.w_ANCONSUP)
            select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS,MCCODSTU,MCPROSTU;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCONSUP = NVL(_Link_.MCCODICE,space(15))
      this.w_DESSUP = NVL(_Link_.MCDESCRI,space(40))
      this.w_NULIV = NVL(_Link_.MCNUMLIV,0)
      this.w_TIPMAS = NVL(_Link_.MCTIPMAS,space(1))
      this.w_CODSTU = NVL(_Link_.MCCODSTU,space(3))
      this.w_PROSTU = NVL(_Link_.MCPROSTU,space(8))
    else
      if i_cCtrl<>'Load'
        this.w_ANCONSUP = space(15)
      endif
      this.w_DESSUP = space(40)
      this.w_NULIV = 0
      this.w_TIPMAS = space(1)
      this.w_CODSTU = space(3)
      this.w_PROSTU = space(8)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_NULIV=1 AND .w_TIPMAS='G'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ANCONSUP = space(15)
        this.w_DESSUP = space(40)
        this.w_NULIV = 0
        this.w_TIPMAS = space(1)
        this.w_CODSTU = space(3)
        this.w_PROSTU = space(8)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])+'\'+cp_ToStr(_Link_.MCCODICE,1)
      cp_ShowWarn(i_cKey,this.MASTRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCONSUP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MASTRI_IDX,3] and i_nFlds+6<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_7.MCCODICE as MCCODICE107"+ ",link_1_7.MCDESCRI as MCDESCRI107"+ ",link_1_7.MCNUMLIV as MCNUMLIV107"+ ",link_1_7.MCTIPMAS as MCTIPMAS107"+ ",link_1_7.MCCODSTU as MCCODSTU107"+ ",link_1_7.MCPROSTU as MCPROSTU107"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_7 on CONTI.ANCONSUP=link_1_7.MCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_7"
          i_cKey=i_cKey+'+" and CONTI.ANCONSUP=link_1_7.MCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANCODSTU
  func Link_1_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STU_PIAC_IDX,3]
    i_lTable = "STU_PIAC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STU_PIAC_IDX,2], .t., this.STU_PIAC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STU_PIAC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCODSTU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'STU_PIAC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LMCODSOT like "+cp_ToStrODBC(trim(this.w_ANCODSTU)+"%");
                   +" and LMCODCON="+cp_ToStrODBC(this.w_CODSTU);

          i_ret=cp_SQL(i_nConn,"select LMCODCON,LMCODSOT,LMDESSOT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LMCODCON,LMCODSOT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LMCODCON',this.w_CODSTU;
                     ,'LMCODSOT',trim(this.w_ANCODSTU))
          select LMCODCON,LMCODSOT,LMDESSOT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LMCODCON,LMCODSOT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCODSTU)==trim(_Link_.LMCODSOT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANCODSTU) and !this.bDontReportError
            deferred_cp_zoom('STU_PIAC','*','LMCODCON,LMCODSOT',cp_AbsName(oSource.parent,'oANCODSTU_1_36'),i_cWhere,'',"Sottoconti studio",'GSLM_ASS.STU_PIAC_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODSTU<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LMCODCON,LMCODSOT,LMDESSOT";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select LMCODCON,LMCODSOT,LMDESSOT;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice non valido")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LMCODCON,LMCODSOT,LMDESSOT";
                     +" from "+i_cTable+" "+i_lTable+" where LMCODSOT="+cp_ToStrODBC(oSource.xKey(2));
                     +" and LMCODCON="+cp_ToStrODBC(this.w_CODSTU);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LMCODCON',oSource.xKey(1);
                       ,'LMCODSOT',oSource.xKey(2))
            select LMCODCON,LMCODSOT,LMDESSOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCODSTU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LMCODCON,LMCODSOT,LMDESSOT";
                   +" from "+i_cTable+" "+i_lTable+" where LMCODSOT="+cp_ToStrODBC(this.w_ANCODSTU);
                   +" and LMCODCON="+cp_ToStrODBC(this.w_CODSTU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LMCODCON',this.w_CODSTU;
                       ,'LMCODSOT',this.w_ANCODSTU)
            select LMCODCON,LMCODSOT,LMDESSOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCODSTU = NVL(_Link_.LMCODSOT,space(10))
      this.w_DESSOT = NVL(_Link_.LMDESSOT,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_ANCODSTU = space(10)
      endif
      this.w_DESSOT = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=LEN(alltrim(.w_ANCODSTU))=5  or LEN(alltrim(.w_ANCODSTU))=6 or LEN(alltrim(.w_ANCODSTU))=9
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice non valido")
        endif
        this.w_ANCODSTU = space(10)
        this.w_DESSOT = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STU_PIAC_IDX,2])+'\'+cp_ToStr(_Link_.LMCODCON,1)+'\'+cp_ToStr(_Link_.LMCODSOT,1)
      cp_ShowWarn(i_cKey,this.STU_PIAC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCODSTU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANCODPAG
  func Link_1_38(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_lTable = "PAG_AMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2], .t., this.PAG_AMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCODPAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APA',True,'PAG_AMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PACODICE like "+cp_ToStrODBC(trim(this.w_ANCODPAG)+"%");

          i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PACODICE',trim(this.w_ANCODPAG))
          select PACODICE,PADESCRI,PADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCODPAG)==trim(_Link_.PACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"( PADESCRI like "+cp_ToStrODBC(trim(this.w_ANCODPAG)+"%")+cp_TransWhereFldName('PADESCRI',trim(this.w_ANCODPAG))+")";

            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" PADESCRI like "+cp_ToStr(trim(this.w_ANCODPAG)+"%");

            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ANCODPAG) and !this.bDontReportError
            deferred_cp_zoom('PAG_AMEN','*','PACODICE',cp_AbsName(oSource.parent,'oANCODPAG_1_38'),i_cWhere,'GSAR_APA',"Pagamenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',oSource.xKey(1))
            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCODPAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_ANCODPAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_ANCODPAG)
            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCODPAG = NVL(_Link_.PACODICE,space(5))
      this.w_DESPAG = NVL(cp_TransLoadField('_Link_.PADESCRI'),space(30))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.PADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ANCODPAG = space(5)
      endif
      this.w_DESPAG = space(30)
      this.w_DTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice pagamento inesistente oppure obsoleto")
        endif
        this.w_ANCODPAG = space(5)
        this.w_DESPAG = space(30)
        this.w_DTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_AMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCODPAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_38(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PAG_AMEN_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_38.PACODICE as PACODICE138"+ ","+cp_TransLinkFldName('link_1_38.PADESCRI')+" as PADESCRI138"+ ",link_1_38.PADTOBSO as PADTOBSO138"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_38 on CONTI.ANCODPAG=link_1_38.PACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_38"
          i_cKey=i_cKey+'+" and CONTI.ANCODPAG=link_1_38.PACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oANCODICE_1_4.value==this.w_ANCODICE)
      this.oPgFrm.Page1.oPag.oANCODICE_1_4.value=this.w_ANCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oANDESCRI_1_5.value==this.w_ANDESCRI)
      this.oPgFrm.Page1.oPag.oANDESCRI_1_5.value=this.w_ANDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oANNOTAIN_1_6.value==this.w_ANNOTAIN)
      this.oPgFrm.Page1.oPag.oANNOTAIN_1_6.value=this.w_ANNOTAIN
    endif
    if not(this.oPgFrm.Page1.oPag.oANCONSUP_1_7.value==this.w_ANCONSUP)
      this.oPgFrm.Page1.oPag.oANCONSUP_1_7.value=this.w_ANCONSUP
    endif
    if not(this.oPgFrm.Page1.oPag.oDESSUP_1_10.value==this.w_DESSUP)
      this.oPgFrm.Page1.oPag.oDESSUP_1_10.value=this.w_DESSUP
    endif
    if not(this.oPgFrm.Page1.oPag.oANTIPSOT_1_11.RadioValue()==this.w_ANTIPSOT)
      this.oPgFrm.Page1.oPag.oANTIPSOT_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANRISESE_1_12.RadioValue()==this.w_ANRISESE)
      this.oPgFrm.Page1.oPag.oANRISESE_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANFLIVAD_1_13.RadioValue()==this.w_ANFLIVAD)
      this.oPgFrm.Page1.oPag.oANFLIVAD_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANPARTSN_1_14.RadioValue()==this.w_ANPARTSN)
      this.oPgFrm.Page1.oPag.oANPARTSN_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANDTOBSO_1_28.value==this.w_ANDTOBSO)
      this.oPgFrm.Page1.oPag.oANDTOBSO_1_28.value=this.w_ANDTOBSO
    endif
    if not(this.oPgFrm.Page1.oPag.oANDTINVA_1_35.value==this.w_ANDTINVA)
      this.oPgFrm.Page1.oPag.oANDTINVA_1_35.value=this.w_ANDTINVA
    endif
    if not(this.oPgFrm.Page1.oPag.oANCODSTU_1_36.value==this.w_ANCODSTU)
      this.oPgFrm.Page1.oPag.oANCODSTU_1_36.value=this.w_ANCODSTU
    endif
    if not(this.oPgFrm.Page1.oPag.oANCODPAG_1_38.value==this.w_ANCODPAG)
      this.oPgFrm.Page1.oPag.oANCODPAG_1_38.value=this.w_ANCODPAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPAG_1_39.value==this.w_DESPAG)
      this.oPgFrm.Page1.oPag.oDESPAG_1_39.value=this.w_DESPAG
    endif
    if not(this.oPgFrm.Page2.oPag.oCODI_2_1.value==this.w_CODI)
      this.oPgFrm.Page2.oPag.oCODI_2_1.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESC_2_2.value==this.w_DESC)
      this.oPgFrm.Page2.oPag.oDESC_2_2.value=this.w_DESC
    endif
    if not(this.oPgFrm.Page2.oPag.oANCCNOTE_2_3.value==this.w_ANCCNOTE)
      this.oPgFrm.Page2.oPag.oANCCNOTE_2_3.value=this.w_ANCCNOTE
    endif
    if not(this.oPgFrm.Page2.oPag.oANCCTAGG_2_5.RadioValue()==this.w_ANCCTAGG)
      this.oPgFrm.Page2.oPag.oANCCTAGG_2_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESSOT_1_43.value==this.w_DESSOT)
      this.oPgFrm.Page1.oPag.oDESSOT_1_43.value=this.w_DESSOT
    endif
    if not(this.oPgFrm.Page1.oPag.oANESCDOF_1_48.RadioValue()==this.w_ANESCDOF)
      this.oPgFrm.Page1.oPag.oANESCDOF_1_48.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'CONTI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_ANCODICE))  and (.w_AUTO<>'S' AND .cFunction='Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANCODICE_1_4.SetFocus()
            i_bnoObbl = !empty(.w_ANCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ANDESCRI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANDESCRI_1_5.SetFocus()
            i_bnoObbl = !empty(.w_ANDESCRI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_ANCONSUP)) or not(.w_NULIV=1 AND .w_TIPMAS='G'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANCONSUP_1_7.SetFocus()
            i_bnoObbl = !empty(.w_ANCONSUP)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_ANTIPSOT<>'T' OR .w_ANTIPSOT=.w_SEZBIL)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANTIPSOT_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.cFunction='Load' or iif(not EMPTY(.w_ANDTOBSO) And .w_ANDTOBSO<=.w_OBTEST, CHKOBCON(.w_ANCODICE, THIS,.w_ANTIPCON) , .T.))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANDTOBSO_1_28.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(LEN(alltrim(.w_ANCODSTU))=5  or LEN(alltrim(.w_ANCODSTU))=6 or LEN(alltrim(.w_ANCODSTU))=9)  and not(g_LEMC<>'S' OR g_TRAEXP='N')  and (g_LEMC='S' And Not Empty( .w_CODSTU ))  and not(empty(.w_ANCODSTU))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANCODSTU_1_36.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice non valido")
          case   not(EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST)  and not(.w_ANPARTSN<>'S')  and not(empty(.w_ANCODPAG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANCODPAG_1_38.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice pagamento inesistente oppure obsoleto")
        endcase
      endif
      *i_bRes = i_bRes .and. .GSAR_MRC.CheckForm()
      if i_bres
        i_bres=  .GSAR_MRC.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_AUTO = this.w_AUTO
    this.o_ANCONSUP = this.w_ANCONSUP
    this.o_CODSTU = this.w_CODSTU
    this.o_ANTIPSOT = this.w_ANTIPSOT
    this.o_OKCC = this.w_OKCC
    this.o_ANCODSTU = this.w_ANCODSTU
    * --- GSAR_MRC : Depends On
    this.GSAR_MRC.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsar_apiPag1 as StdContainer
  Width  = 591
  height = 336
  stdWidth  = 591
  stdheight = 336
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oANCODICE_1_4 as StdField with uid="SJOFCVWWNZ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_ANCODICE", cQueryName = "ANTIPCON,ANCODICE",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice conto",;
    HelpContextID = 200668341,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=135, Left=64, Top=9, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15)

  func oANCODICE_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AUTO<>'S' AND .cFunction='Load')
    endwith
   endif
  endfunc

  add object oANDESCRI_1_5 as StdField with uid="JWLAKKCPKN",rtseq=5,rtrep=.f.,;
    cFormVar = "w_ANDESCRI", cQueryName = "ANDESCRI",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 250616655,;
   bGlobalFont=.t.,;
    Height=21, Width=328, Left=208, Top=9, InputMask=replicate('X',40)

  add object oANNOTAIN_1_6 as StdField with uid="UCGTFVAZVH",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ANNOTAIN", cQueryName = "ANNOTAIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Eventuale voce di riferimento sulla nota integrativa",;
    HelpContextID = 49628332,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=506, Top=39, cSayPict='"99"', cGetPict='"99"'

  add object oANCONSUP_1_7 as StdField with uid="BBDNRXNDXL",rtseq=7,rtrep=.f.,;
    cFormVar = "w_ANCONSUP", cQueryName = "ANCONSUP",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Mastro di raggruppamento del conto",;
    HelpContextID = 22410410,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=24, Top=94, cSayPict="p_MAS", cGetPict="p_MAS", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="MASTRI", cZoomOnZoom="GSAR_AMC", oKey_1_1="MCCODICE", oKey_1_2="this.w_ANCONSUP"

  func oANCONSUP_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCONSUP_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCONSUP_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MASTRI','*','MCCODICE',cp_AbsName(this.parent,'oANCONSUP_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMC',"Mastri contabili (generici)",'GSAR_API.MASTRI_VZM',this.parent.oContained
  endproc
  proc oANCONSUP_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MCCODICE=this.parent.oContained.w_ANCONSUP
     i_obj.ecpSave()
  endproc

  add object oDESSUP_1_10 as StdField with uid="WQCLZQBZFM",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DESSUP", cQueryName = "DESSUP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 203358774,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=166, Top=94, InputMask=replicate('X',40)


  add object oANTIPSOT_1_11 as StdCombo with uid="WROYRQNXCC",rtseq=11,rtrep=.f.,left=12,top=146,width=157,height=21;
    , ToolTipText = "Tipo di appartenenza del conto";
    , HelpContextID = 247798618;
    , cFormVar="w_ANTIPSOT",RowSource=""+"Generico,"+"Cespite,"+"IVA,"+"Banca,"+"Controp.acquisti,"+"Controp.vendite,"+"Apertura/chiusura,"+"Inventariale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oANTIPSOT_1_11.RadioValue()
    return(iif(this.value =1,'X',;
    iif(this.value =2,'M',;
    iif(this.value =3,'I',;
    iif(this.value =4,'B',;
    iif(this.value =5,'A',;
    iif(this.value =6,'V',;
    iif(this.value =7,'T',;
    iif(this.value =8,'R',;
    space(1))))))))))
  endfunc
  func oANTIPSOT_1_11.GetRadio()
    this.Parent.oContained.w_ANTIPSOT = this.RadioValue()
    return .t.
  endfunc

  func oANTIPSOT_1_11.SetRadio()
    this.Parent.oContained.w_ANTIPSOT=trim(this.Parent.oContained.w_ANTIPSOT)
    this.value = ;
      iif(this.Parent.oContained.w_ANTIPSOT=='X',1,;
      iif(this.Parent.oContained.w_ANTIPSOT=='M',2,;
      iif(this.Parent.oContained.w_ANTIPSOT=='I',3,;
      iif(this.Parent.oContained.w_ANTIPSOT=='B',4,;
      iif(this.Parent.oContained.w_ANTIPSOT=='A',5,;
      iif(this.Parent.oContained.w_ANTIPSOT=='V',6,;
      iif(this.Parent.oContained.w_ANTIPSOT=='T',7,;
      iif(this.Parent.oContained.w_ANTIPSOT=='R',8,;
      0))))))))
  endfunc

  func oANTIPSOT_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ANTIPSOT<>'T' OR .w_ANTIPSOT=.w_SEZBIL)
    endwith
    return bRes
  endfunc

  add object oANRISESE_1_12 as StdCheck with uid="WNJYOGAJLI",rtseq=12,rtrep=.f.,left=12, top=175, caption="Risultato esercizio",;
    ToolTipText = "Conto per risultato esercizio",;
    HelpContextID = 252380341,;
    cFormVar="w_ANRISESE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oANRISESE_1_12.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oANRISESE_1_12.GetRadio()
    this.Parent.oContained.w_ANRISESE = this.RadioValue()
    return .t.
  endfunc

  func oANRISESE_1_12.SetRadio()
    this.Parent.oContained.w_ANRISESE=trim(this.Parent.oContained.w_ANRISESE)
    this.value = ;
      iif(this.Parent.oContained.w_ANRISESE=='S',1,;
      0)
  endfunc

  func oANRISESE_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANTIPSOT='X')
    endwith
   endif
  endfunc

  add object oANFLIVAD_1_13 as StdCheck with uid="SNWSOKXIQL",rtseq=13,rtrep=.f.,left=12, top=206, caption="IVA ad esig. dfferita",;
    ToolTipText = "Se attivo identifica IVA ad esigibilitÓ differita",;
    HelpContextID = 22494026,;
    cFormVar="w_ANFLIVAD", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oANFLIVAD_1_13.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANFLIVAD_1_13.GetRadio()
    this.Parent.oContained.w_ANFLIVAD = this.RadioValue()
    return .t.
  endfunc

  func oANFLIVAD_1_13.SetRadio()
    this.Parent.oContained.w_ANFLIVAD=trim(this.Parent.oContained.w_ANFLIVAD)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLIVAD=='S',1,;
      0)
  endfunc

  func oANFLIVAD_1_13.mHide()
    with this.Parent.oContained
      return (.w_ANTIPSOT<>'I')
    endwith
  endfunc

  add object oANPARTSN_1_14 as StdCheck with uid="GRXOCQUHCK",rtseq=14,rtrep=.f.,left=12, top=236, caption="Partite",;
    ToolTipText = "Se attivo: sul conto si gestiscono le partite aperte",;
    HelpContextID = 2303148,;
    cFormVar="w_ANPARTSN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oANPARTSN_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oANPARTSN_1_14.GetRadio()
    this.Parent.oContained.w_ANPARTSN = this.RadioValue()
    return .t.
  endfunc

  func oANPARTSN_1_14.SetRadio()
    this.Parent.oContained.w_ANPARTSN=trim(this.Parent.oContained.w_ANPARTSN)
    this.value = ;
      iif(this.Parent.oContained.w_ANPARTSN=='S',1,;
      0)
  endfunc

  func oANPARTSN_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERPAR='S')
    endwith
   endif
  endfunc

  add object oANDTOBSO_1_28 as StdField with uid="YCZQDGHPWJ",rtseq=23,rtrep=.f.,;
    cFormVar = "w_ANDTOBSO", cQueryName = "ANDTOBSO",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di disattivazione del conto",;
    HelpContextID = 230628181,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=509, Top=312, tabstop=.f.

  func oANDTOBSO_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.cFunction='Load' or iif(not EMPTY(.w_ANDTOBSO) And .w_ANDTOBSO<=.w_OBTEST, CHKOBCON(.w_ANCODICE, THIS,.w_ANTIPCON) , .T.))
    endwith
    return bRes
  endfunc


  add object oBtn_1_33 as StdButton with uid="OULBCSQTHJ",left=11, top=290, width=48,height=45,;
    CpPicture="BMP\SALDI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere ai saldi del conto";
    , HelpContextID = 225554214;
    , CAPTION='\<Saldi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_33.Click()
      with this.Parent.oContained
        GSAR_BGS(this.Parent.oContained,"G", .w_ANCODICE)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_33.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction<>'Load' AND NOT EMPTY(.w_ANCODICE))
      endwith
    endif
  endfunc

  add object oANDTINVA_1_35 as StdField with uid="FSTHJKPJUW",rtseq=26,rtrep=.f.,;
    cFormVar = "w_ANDTINVA", cQueryName = "ANDTINVA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio validitÓ",;
    HelpContextID = 111207609,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=299, Top=311, tabstop=.f.

  add object oANCODSTU_1_36 as StdField with uid="CKWPNYCMOW",rtseq=27,rtrep=.f.,;
    cFormVar = "w_ANCODSTU", cQueryName = "ANCODSTU",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Codice non valido",;
    HelpContextID = 32896165,;
   bGlobalFont=.t.,;
    Height=21, Width=81, Left=282, Top=208, cSayPict='"999999999"', cGetPict='"999999999"', InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="STU_PIAC", oKey_1_1="LMCODCON", oKey_1_2="this.w_CODSTU", oKey_2_1="LMCODSOT", oKey_2_2="this.w_ANCODSTU"

  func oANCODSTU_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_LEMC='S' And Not Empty( .w_CODSTU ))
    endwith
   endif
  endfunc

  func oANCODSTU_1_36.mHide()
    with this.Parent.oContained
      return (g_LEMC<>'S' OR g_TRAEXP='N')
    endwith
  endfunc

  func oANCODSTU_1_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_36('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCODSTU_1_36.ecpDrop(oSource)
    this.Parent.oContained.link_1_36('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCODSTU_1_36.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.STU_PIAC_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LMCODCON="+cp_ToStrODBC(this.Parent.oContained.w_CODSTU)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LMCODCON="+cp_ToStr(this.Parent.oContained.w_CODSTU)
    endif
    do cp_zoom with 'STU_PIAC','*','LMCODCON,LMCODSOT',cp_AbsName(this.parent,'oANCODSTU_1_36'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Sottoconti studio",'GSLM_ASS.STU_PIAC_VZM',this.parent.oContained
  endproc


  add object oObj_1_37 as cp_calclbl with uid="DOLTQCNXCG",left=362, top=151, width=100,height=25,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",FontBold =.t.,;
    nPag=1;
    , HelpContextID = 250441754

  add object oANCODPAG_1_38 as StdField with uid="RQACGOMSXU",rtseq=28,rtrep=.f.,;
    cFormVar = "w_ANCODPAG", cQueryName = "ANCODPAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice pagamento inesistente oppure obsoleto",;
    ToolTipText = "Pagamento utilizzato per creare le partite/scadenze relative al conto",;
    HelpContextID = 185207629,;
   bGlobalFont=.t.,;
    Height=21, Width=75, Left=282, Top=236, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="PAG_AMEN", cZoomOnZoom="GSAR_APA", oKey_1_1="PACODICE", oKey_1_2="this.w_ANCODPAG"

  func oANCODPAG_1_38.mHide()
    with this.Parent.oContained
      return (.w_ANPARTSN<>'S')
    endwith
  endfunc

  func oANCODPAG_1_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_38('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCODPAG_1_38.ecpDrop(oSource)
    this.Parent.oContained.link_1_38('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCODPAG_1_38.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PAG_AMEN','*','PACODICE',cp_AbsName(this.parent,'oANCODPAG_1_38'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APA',"Pagamenti",'',this.parent.oContained
  endproc
  proc oANCODPAG_1_38.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PACODICE=this.parent.oContained.w_ANCODPAG
     i_obj.ecpSave()
  endproc

  add object oDESPAG_1_39 as StdField with uid="SKRMNQQUHD",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESPAG", cQueryName = "DESPAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 31195702,;
   bGlobalFont=.t.,;
    Height=22, Width=223, Left=365, Top=236, InputMask=replicate('X',30)

  func oDESPAG_1_39.mHide()
    with this.Parent.oContained
      return (.w_ANPARTSN<>'S')
    endwith
  endfunc

  add object oDESSOT_1_43 as StdField with uid="CPIKFEMOYH",rtseq=35,rtrep=.f.,;
    cFormVar = "w_DESSOT", cQueryName = "DESSOT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 264176182,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=365, Top=208, InputMask=replicate('X',30)

  func oDESSOT_1_43.mHide()
    with this.Parent.oContained
      return (g_LEMC<>'S' OR g_TRAEXP='N')
    endwith
  endfunc


  add object oObj_1_44 as cp_runprogram with uid="YTLPUDVKVC",left=-2, top=393, width=156,height=22,;
    caption='GSAR_BPI',;
   bGlobalFont=.t.,;
    prg="GSAR_BPI",;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 247263407

  add object oANESCDOF_1_48 as StdCheck with uid="JFHGGHJQQS",rtseq=37,rtrep=.f.,left=12, top=260, caption="Escludi nell'esportazione per DocFinance",;
    ToolTipText = "Escludi nell'esportazione per DocFinance",;
    HelpContextID = 251538252,;
    cFormVar="w_ANESCDOF", bObbl = .f. , nPag = 1;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oANESCDOF_1_48.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANESCDOF_1_48.GetRadio()
    this.Parent.oContained.w_ANESCDOF = this.RadioValue()
    return .t.
  endfunc

  func oANESCDOF_1_48.SetRadio()
    this.Parent.oContained.w_ANESCDOF=trim(this.Parent.oContained.w_ANESCDOF)
    this.value = ;
      iif(this.Parent.oContained.w_ANESCDOF=='S',1,;
      0)
  endfunc

  func oANESCDOF_1_48.mHide()
    with this.Parent.oContained
      return (g_ISDF<>'S')
    endwith
  endfunc

  add object oStr_1_15 as StdString with uid="KJZQMUPLNK",Visible=.t., Left=2, Top=9,;
    Alignment=1, Width=60, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_16 as StdString with uid="ANDNCEUBHD",Visible=.t., Left=13, Top=130,;
    Alignment=0, Width=157, Height=15,;
    Caption="Tipo conto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="DHOHSJFWPA",Visible=.t., Left=13, Top=61,;
    Alignment=0, Width=239, Height=18,;
    Caption="Mastro di raggruppamento contabile"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="MSGWZJWQEC",Visible=.t., Left=217, Top=151,;
    Alignment=1, Width=142, Height=15,;
    Caption="Sezione di bilancio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="RPRYQVKLHL",Visible=.t., Left=386, Top=312,;
    Alignment=1, Width=121, Height=15,;
    Caption="Data obsolescenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="DSAPJGBVKG",Visible=.t., Left=344, Top=39,;
    Alignment=1, Width=157, Height=15,;
    Caption="Voce nota integrativa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="DPJBBIYRET",Visible=.t., Left=196, Top=312,;
    Alignment=1, Width=99, Height=15,;
    Caption="Data validitÓ:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="BCVWQXWDBQ",Visible=.t., Left=156, Top=236,;
    Alignment=1, Width=122, Height=15,;
    Caption="Cod. pagamento:"  ;
  , bGlobalFont=.t.

  func oStr_1_40.mHide()
    with this.Parent.oContained
      return (.w_ANPARTSN<>'S')
    endwith
  endfunc

  add object oStr_1_42 as StdString with uid="CAWKTCOOVL",Visible=.t., Left=156, Top=208,;
    Alignment=1, Width=122, Height=18,;
    Caption="Sottoconto studio:"  ;
  , bGlobalFont=.t.

  func oStr_1_42.mHide()
    with this.Parent.oContained
      return (g_LEMC<>'S' OR g_TRAEXP='N')
    endwith
  endfunc

  add object oBox_1_17 as StdBox with uid="BUKKOTPGCD",left=13, top=83, width=522,height=42
enddefine
define class tgsar_apiPag2 as StdContainer
  Width  = 591
  height = 336
  stdWidth  = 591
  stdheight = 336
  resizeYpos=213
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODI_2_1 as StdField with uid="OBIVPOXQPX",rtseq=30,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 113515558,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=135, Left=65, Top=13, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15)

  add object oDESC_2_2 as StdField with uid="BFIXYOGLNQ",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DESC", cQueryName = "DESC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 113181238,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=207, Top=13, InputMask=replicate('X',40)

  add object oANCCNOTE_2_3 as StdField with uid="AEUUMXODZX",rtseq=32,rtrep=.f.,;
    cFormVar = "w_ANCCNOTE", cQueryName = "ANCCNOTE",;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Note relative alle eventuali ripartizioni nei centri di costo/ricavo",;
    HelpContextID = 90305717,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=65, Top=52, InputMask=replicate('X',50)

  func oANCCNOTE_2_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OKCC="S")
    endwith
   endif
  endfunc


  add object oANCCTAGG_2_5 as StdCombo with uid="APCAWVURFX",rtseq=33,rtrep=.f.,left=108,top=91,width=116,height=21;
    , ToolTipText = "Metodo di aggiornamento importi da ripartire sulla primanota";
    , HelpContextID = 50459827;
    , cFormVar="w_ANCCTAGG",RowSource=""+"Automatico,"+"Manuale,"+"Escluso", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oANCCTAGG_2_5.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'M',;
    iif(this.value =3,'E',;
    space(1)))))
  endfunc
  func oANCCTAGG_2_5.GetRadio()
    this.Parent.oContained.w_ANCCTAGG = this.RadioValue()
    return .t.
  endfunc

  func oANCCTAGG_2_5.SetRadio()
    this.Parent.oContained.w_ANCCTAGG=trim(this.Parent.oContained.w_ANCCTAGG)
    this.value = ;
      iif(this.Parent.oContained.w_ANCCTAGG=='A',1,;
      iif(this.Parent.oContained.w_ANCCTAGG=='M',2,;
      iif(this.Parent.oContained.w_ANCCTAGG=='E',3,;
      0)))
  endfunc

  func oANCCTAGG_2_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OKCC="S")
    endwith
   endif
  endfunc


  add object oLinkPC_2_6 as stdDynamicChildContainer with uid="QNOEBEKFDX",left=3, top=125, width=564, height=198, bOnScreen=.t.;


  func oLinkPC_2_6.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (g_PERCCR='S' AND .w_ANCCTAGG $ 'AM')
      endwith
    endif
  endfunc


  add object oObj_2_9 as cp_runprogram with uid="NCNLNEYNMR",left=-6, top=354, width=159,height=19,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSAR_BRC("C")',;
    cEvent = "Controllo",;
    nPag=2;
    , HelpContextID = 250441754


  add object oObj_2_10 as cp_runprogram with uid="IUAFQGPOSE",left=160, top=355, width=159,height=19,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSAR_BRC("S")',;
    cEvent = "Save",;
    nPag=2;
    , HelpContextID = 250441754

  add object oStr_2_4 as StdString with uid="UQVPOVKHEL",Visible=.t., Left=5, Top=13,;
    Alignment=1, Width=58, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_7 as StdString with uid="MLXWZYGBSU",Visible=.t., Left=5, Top=91,;
    Alignment=1, Width=98, Height=15,;
    Caption="Aggiornamento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_8 as StdString with uid="DQBDSYOXBQ",Visible=.t., Left=5, Top=52,;
    Alignment=1, Width=58, Height=15,;
    Caption="Note:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result="ANTIPCON='G'"
  if lower(right(result,4))='.vqr'
  i_cAliasName = "cp"+Right(SYS(2015),8)
    result=" exists (select 1 from ("+cp_GetSQLFromQuery(result)+") "+i_cAliasName+" where ";
  +" "+i_cAliasName+".ANTIPCON=CONTI.ANTIPCON";
  +" and "+i_cAliasName+".ANCODICE=CONTI.ANCODICE";
  +")"
  endif
  i_res=cp_AppQueryFilter('gsar_api','CONTI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".ANTIPCON=CONTI.ANTIPCON";
  +" and "+i_cAliasName2+".ANCODICE=CONTI.ANCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
