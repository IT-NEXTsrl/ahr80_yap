* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bp1                                                        *
*              Generazione packing list                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_486]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-10                                                      *
* Last revis.: 2011-07-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bp1",oParentObject)
return(i_retval)

define class tgsma_bp1 as StdBatch
  * --- Local variables
  w_ZOOM = space(10)
  w_OREC = 0
  w_UM1 = space(3)
  w_QTA = 0
  w_UM2 = space(3)
  w_NUMCOL = 0
  w_UM3 = space(3)
  w_PESNET = 0
  w_PN = 0
  w_PESLOR = 0
  w_PL = 0
  w_CODART = space(20)
  w_PC = 0
  w_CODICE = space(20)
  w_CC = 0
  w_UNIMIS = space(3)
  w_CODCOL = space(5)
  w_TC = space(5)
  w_CDCONF = space(3)
  w_MESS = space(200)
  w_MESS1 = space(200)
  w_NR = 0
  w_ROWNUM = 0
  w_DESVO = space(15)
  w_UMDIM = space(3)
  w_TARA = 0
  w_FLMIX = space(1)
  w_PERTOL = 0
  w_MAXCON = 0
  w_PROCOL = 0
  w_RIGCOL = 0
  w_QTACON = 0
  w_DESART = space(40)
  w_COLLO = 0
  w_CONAGG = 0
  w_ARTAGG = 0
  w_APPO = 0
  w_QTACON2 = 0
  w_CONAGG2 = 0
  w_OLDCOL = 0
  w_TARCOL = 0
  w_SERIAL = space(10)
  w_NUMDOC = 0
  w_DATDOC = ctod("  /  /  ")
  w_ALFDOC = space(10)
  w_TIPRIG = space(1)
  w_TIPDOC = space(5)
  w_CLADOC = space(2)
  w_TIPCON = space(1)
  w_CODCON = space(15)
  w_DESCONF = space(35)
  w_VOLCOL = 0
  w_DESCOL = space(35)
  w_UMVOL = space(3)
  w_TARACO = 0
  w_LORDCO = 0
  w_PLSERIAL = space(10)
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_OKDOC = 0
  w_NUDOC = 0
  w_FLERR = .f.
  w_STATUS = space(1)
  w_LUNGH = 0
  w_LARGH = 0
  w_ALTEZ = 0
  w_VOLUME = 0
  w_CODDES = space(5)
  w_TIPORN = space(1)
  w_CODORN = space(15)
  w_CODVET = space(5)
  w_CODPOR = space(1)
  w_CODSPE = space(3)
  w_PROAPPO = 0
  w_LORDO = 0
  w_TEST = 0
  w_TEST1 = 0
  w_TESTCOL = space(5)
  w_oERRORLOG = .NULL.
  w_FLCOVA = space(1)
  w_FLFRAZ = space(1)
  w_PROG = .NULL.
  * --- WorkFile variables
  ART_ICOL_idx=0
  CON_COLL_idx=0
  KEY_ARTI_idx=0
  PAC_CONF_idx=0
  PAC_DETT_idx=0
  PAC_MAST_idx=0
  TIPICONF_idx=0
  TIP_COLL_idx=0
  UNIMIS_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch di generazione Packing List lanciato dalla maschera GSMA_KPK  premendo il bottone 'Genera'
    * --- Per i documenti con attivato il Flag <Packing List> e che non la hanno ancora generata
    this.w_TARCOL = 0
    * --- Stanzia l'oggetto per la gestione delle messaggistiche di errore
    this.w_oERRORLOG=createobject("AH_ErrorLog")
    * --- Selezione cursore Zoom
    this.w_ZOOM = this.oParentObject.w_ZoomDoc
    NC = this.w_Zoom.cCursor
    SELECT (NC)
    SELECT * FROM (NC) WHERE XCHK=1 INTO CURSOR APPO1
    if RECCOUNT("APPO1") >0
      this.w_OKDOC = 0
      this.w_NUDOC = 0
      SELECT APPO1
      GO TOP
      SCAN 
      this.w_NUDOC = this.w_NUDOC + 1
      this.w_PLSERIAL = SPACE(10)
      this.w_SERIAL = SERIAL
      this.w_NUMDOC = NVL(NUMDOC,0)
      this.w_DATDOC = DATDOC
      this.w_ALFDOC = NVL(ALFDOC,SPACE(10))
      this.w_TIPDOC = NVL(TIPDOC,SPACE(5))
      this.w_CLADOC = NVL(CLADOC,SPACE(2))
      this.w_TIPCON = NVL(TIPCON," ")
      this.w_CODCON = NVL(CODCON,SPACE(15))
      this.w_CODDES = NVL(CODDES,SPACE(5))
      this.w_TIPORN = NVL(TIPORN,SPACE(1))
      this.w_CODORN = NVL(CODORN,SPACE(15))
      this.w_CODVET = NVL(CODVET,SPACE(5))
      this.w_CODPOR = NVL(CODPOR,SPACE(1))
      this.w_CODSPE = NVL(CODSPE,SPACE(3))
      this.w_FLERR = .F.
      * --- Try
      local bErr_03812AC8
      bErr_03812AC8=bTrsErr
      this.Try_03812AC8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        if i_ErrMsg<>"ERRORE"
          * --- Se esco per un errore non gestito segnalo il messaggio di errore
          *     che mi arriva dal sistema, altrimenti il report successivo mostra all'utente
          *     i messaggi di errore controllati.
          ah_ErrorMsg("Errore generazione %1",,"", Message())
        endif
      endif
      bTrsErr=bTrsErr or bErr_03812AC8
      * --- End
      select APPO1
      ENDSCAN
      if used("TMPCUR1")
        select TMPCUR1
        use
      endif
      if used("TMPCUR2")
        select TMPCUR2
        use
      endif
      if used("APPO")
        select APPO
        use
      endif
      if used("PROGRES")
        select PROGRES
        use
      endif
      this.w_APPO = "Operazione completata%0N.%1 documenti elaborati%0Su %2 documenti selezionati"
      ah_ErrorMsg(this.w_APPO,,"", ALLTRIM(STR(this.w_OKDOC)), ALLTRIM(STR(this.w_NUDOC)))
      * --- LOG Errori
      if this.w_NUDOC>0 AND this.w_OKDOC<>this.w_NUDOC
        this.w_oERRORLOG.PrintLog(this.oParentObject,"Segnalazioni in fase di generazione")     
      endif
      if USED("__TMP__")
        SELECT __TMP__
        USE
      endif
      * --- Refresh dello Zoom
      this.oParentObject.NotifyEvent("Ricerca")
      if USED("APPO1")
        SELECT APPO1
        USE
      endif
    else
      if used("APPO1")
        select APPO1
        use
      endif
      this.w_PROG = GSMA_MPK()
      if !(this.w_PROG.bSec1)
        i_retcode = 'stop'
        return
      endif
      * --- mi metto in interrogazione
      this.w_PROG.LoadRecWarn()     
    endif
  endproc
  proc Try_03812AC8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Creazione cursore di appoggio per calcolo Packing List.
     
 CREATE CURSOR TMPCUR1; 
 (t_CODCONF C(3), t_CODCOL C(5), t_QTACON N(5,0), t_MAXCON N(6,0), ; 
 t_TARA N(9,0), t_FL_MIX C(1), t_PERTOL N(6,2), t_PESNET N(9,3), t_PESLOR N(9,3), t_ROWNUM N(4,0), t_ROWORD N(5,0), ; 
 t_CODICE C(20), t_DESART C(40), t_QTAMOV N(12,3), t_PZCON N(9,3), t_UNIMIS C(3), t_DESVO C(15), t_UMDIM C(3), t_VOLCONF N(10,2))
    vq_exec("query\GSMA3QPK.VQR",this,"RIGHEDOC")
    * --- Cicla sulle Righe Documento
    SELECT RIGHEDOC
    this.w_OREC = RECNO()
    GO TOP
    SCAN FOR CPROWORD<>0 AND MVTIPRIG="R" AND NOT EMPTY(MVCODICE) AND NOT DELETED()
    * --- Legge i Dati del Temporaneo
    this.w_QTA = NVL(MVQTAMOV, 0)
    this.w_NUMCOL = NVL(MVNUMCOL, 0)
    this.w_PESNET = 0
    this.w_PESLOR = 0
    this.w_UNIMIS = NVL(MVUNIMIS, "   ")
    this.w_UM1 = NVL(UNMIS1, "   ")
    this.w_UM2 = NVL(UNMIS2, "   ")
    this.w_UM3 = NVL(UNMIS3, "   ")
    this.w_CODART = NVL(MVCODART, " ")
    this.w_DESART = NVL(MVDESART,SPACE(40))
    this.w_CODICE = NVL(MVCODICE, "   ")
    this.w_CODCOL = NVL(MVCODCOL,SPACE(5))
    this.w_NR = CPROWORD
    this.w_TIPRIG = NVL(MVTIPRIG," ")
    this.w_ROWNUM = NVL(CPROWNUM,0)
    if this.w_QTA>0 AND NOT EMPTY(this.w_UNIMIS)
      this.w_PN = 0
      this.w_PL = 0
      this.w_PC = 0
      this.w_CC = 0
      this.w_TC = SPACE(5)
      do case
        case this.w_UNIMIS=this.w_UM3 AND NOT EMPTY(this.w_UM3)
          * --- UM. del Codice di Ricerca
          * --- Read from KEY_ARTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CACOCOL3,CATPCON3,CADESVO3,CAPZCON3,CAPESLO3,CAPESNE3,CATIPCO3,CAUMDIM3,CADIMLU3,CADIMLA3,CADIMAL3"+;
              " from "+i_cTable+" KEY_ARTI where ";
                  +"CACODICE = "+cp_ToStrODBC(this.w_CODICE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CACOCOL3,CATPCON3,CADESVO3,CAPZCON3,CAPESLO3,CAPESNE3,CATIPCO3,CAUMDIM3,CADIMLU3,CADIMLA3,CADIMAL3;
              from (i_cTable) where;
                  CACODICE = this.w_CODICE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CC = NVL(cp_ToDate(_read_.CACOCOL3),cp_NullValue(_read_.CACOCOL3))
            this.w_CDCONF = NVL(cp_ToDate(_read_.CATPCON3),cp_NullValue(_read_.CATPCON3))
            this.w_DESVO = NVL(cp_ToDate(_read_.CADESVO3),cp_NullValue(_read_.CADESVO3))
            this.w_PC = NVL(cp_ToDate(_read_.CAPZCON3),cp_NullValue(_read_.CAPZCON3))
            this.w_PL = NVL(cp_ToDate(_read_.CAPESLO3),cp_NullValue(_read_.CAPESLO3))
            this.w_PN = NVL(cp_ToDate(_read_.CAPESNE3),cp_NullValue(_read_.CAPESNE3))
            this.w_TC = NVL(cp_ToDate(_read_.CATIPCO3),cp_NullValue(_read_.CATIPCO3))
            this.w_UMDIM = NVL(cp_ToDate(_read_.CAUMDIM3),cp_NullValue(_read_.CAUMDIM3))
            this.w_LUNGH = NVL(cp_ToDate(_read_.CADIMLU3),cp_NullValue(_read_.CADIMLU3))
            this.w_LARGH = NVL(cp_ToDate(_read_.CADIMLA3),cp_NullValue(_read_.CADIMLA3))
            this.w_ALTEZ = NVL(cp_ToDate(_read_.CADIMAL3),cp_NullValue(_read_.CADIMAL3))
            this.w_TESTCOL = NVL(cp_ToDate(_read_.CATIPCO3),cp_NullValue(_read_.CATIPCO3))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        case this.w_UNIMIS=this.w_UM2 AND NOT EMPTY(this.w_UM2) AND NOT EMPTY(this.w_CODART)
          * --- UM della 2^UM Articolo
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARPESNE2,ARPESLO2,ARPZCON2,ARCOCOL2,ARDESVO2,ARUMDIM2,ARTPCON2,ARTIPCO2,ARDIMLU2,ARDIMLA2,ARDIMAL2"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARPESNE2,ARPESLO2,ARPZCON2,ARCOCOL2,ARDESVO2,ARUMDIM2,ARTPCON2,ARTIPCO2,ARDIMLU2,ARDIMLA2,ARDIMAL2;
              from (i_cTable) where;
                  ARCODART = this.w_CODART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_PN = NVL(cp_ToDate(_read_.ARPESNE2),cp_NullValue(_read_.ARPESNE2))
            this.w_PL = NVL(cp_ToDate(_read_.ARPESLO2),cp_NullValue(_read_.ARPESLO2))
            this.w_PC = NVL(cp_ToDate(_read_.ARPZCON2),cp_NullValue(_read_.ARPZCON2))
            this.w_CC = NVL(cp_ToDate(_read_.ARCOCOL2),cp_NullValue(_read_.ARCOCOL2))
            this.w_DESVO = NVL(cp_ToDate(_read_.ARDESVO2),cp_NullValue(_read_.ARDESVO2))
            this.w_UMDIM = NVL(cp_ToDate(_read_.ARUMDIM2),cp_NullValue(_read_.ARUMDIM2))
            this.w_CDCONF = NVL(cp_ToDate(_read_.ARTPCON2),cp_NullValue(_read_.ARTPCON2))
            this.w_TC = NVL(cp_ToDate(_read_.ARTIPCO2),cp_NullValue(_read_.ARTIPCO2))
            this.w_LUNGH = NVL(cp_ToDate(_read_.ARDIMLU2),cp_NullValue(_read_.ARDIMLU2))
            this.w_LARGH = NVL(cp_ToDate(_read_.ARDIMLA2),cp_NullValue(_read_.ARDIMLA2))
            this.w_ALTEZ = NVL(cp_ToDate(_read_.ARDIMAL2),cp_NullValue(_read_.ARDIMAL2))
            this.w_TESTCOL = NVL(cp_ToDate(_read_.ARTIPCO2),cp_NullValue(_read_.ARTIPCO2))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        case this.w_UNIMIS=this.w_UM1 AND NOT EMPTY(this.w_UM1) AND NOT EMPTY(this.w_CODART)
          * --- UM della 1^UM Articolo
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARPESNET,ARPESLOR,ARPZCONF,ARCOCOL1,ARDESVOL,ARUMDIME,ARTPCONF,ARTIPCO1,ARDIMLUN,ARDIMLAR,ARDIMALT"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARPESNET,ARPESLOR,ARPZCONF,ARCOCOL1,ARDESVOL,ARUMDIME,ARTPCONF,ARTIPCO1,ARDIMLUN,ARDIMLAR,ARDIMALT;
              from (i_cTable) where;
                  ARCODART = this.w_CODART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_PN = NVL(cp_ToDate(_read_.ARPESNET),cp_NullValue(_read_.ARPESNET))
            this.w_PL = NVL(cp_ToDate(_read_.ARPESLOR),cp_NullValue(_read_.ARPESLOR))
            this.w_PC = NVL(cp_ToDate(_read_.ARPZCONF),cp_NullValue(_read_.ARPZCONF))
            this.w_CC = NVL(cp_ToDate(_read_.ARCOCOL1),cp_NullValue(_read_.ARCOCOL1))
            this.w_DESVO = NVL(cp_ToDate(_read_.ARDESVOL),cp_NullValue(_read_.ARDESVOL))
            this.w_UMDIM = NVL(cp_ToDate(_read_.ARUMDIME),cp_NullValue(_read_.ARUMDIME))
            this.w_CDCONF = NVL(cp_ToDate(_read_.ARTPCONF),cp_NullValue(_read_.ARTPCONF))
            this.w_TC = NVL(cp_ToDate(_read_.ARTIPCO1),cp_NullValue(_read_.ARTIPCO1))
            this.w_LUNGH = NVL(cp_ToDate(_read_.ARDIMLUN),cp_NullValue(_read_.ARDIMLUN))
            this.w_LARGH = NVL(cp_ToDate(_read_.ARDIMLAR),cp_NullValue(_read_.ARDIMLAR))
            this.w_ALTEZ = NVL(cp_ToDate(_read_.ARDIMALT),cp_NullValue(_read_.ARDIMALT))
            this.w_TESTCOL = NVL(cp_ToDate(_read_.ARTIPCO1),cp_NullValue(_read_.ARTIPCO1))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
      endcase
      * --- Verfico se la confezione � a contenuto variabile
      * --- Read from TIPICONF
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.TIPICONF_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIPICONF_idx,2],.t.,this.TIPICONF_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TCFLCOVA"+;
          " from "+i_cTable+" TIPICONF where ";
              +"TCCODICE = "+cp_ToStrODBC(this.w_CDCONF);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TCFLCOVA;
          from (i_cTable) where;
              TCCODICE = this.w_CDCONF;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_FLCOVA = NVL(cp_ToDate(_read_.TCFLCOVA),cp_NullValue(_read_.TCFLCOVA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Verifico se l'unit� di misura � frazionabile
      * --- Read from UNIMIS
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.UNIMIS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "UMFLFRAZ"+;
          " from "+i_cTable+" UNIMIS where ";
              +"UMCODICE = "+cp_ToStrODBC(this.w_UNIMIS);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          UMFLFRAZ;
          from (i_cTable) where;
              UMCODICE = this.w_UNIMIS;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_FLFRAZ = NVL(cp_ToDate(_read_.UMFLFRAZ),cp_NullValue(_read_.UMFLFRAZ))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_FLCOVA="S" AND this.w_NUMCOL<>0
        * --- Se ho inserito un numero di confezioni diverso da zero eseguo una ripartizione 
        *     proporzionale
        if this.w_FLFRAZ="S" and MOD(this.w_QTA,this.w_NUMCOL) <> 0
          this.w_PC = INT(this.w_QTA/(this.w_NUMCOL-1))
        else
          this.w_PC = this.w_QTA/this.w_NUMCOL
        endif
      endif
      if this.w_TESTCOL<>this.w_CODCOL And Not Empty( this.w_CODCOL)
        * --- Nel documento � stata cambiata la tipologia collo che era associata all'articolo
        *     ed � piena, leggo le confezioni per collo
        * --- Read from CON_COLL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CON_COLL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CON_COLL_idx,2],.t.,this.CON_COLL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TCQTACON"+;
            " from "+i_cTable+" CON_COLL where ";
                +"TCCODICE = "+cp_ToStrODBC(this.w_CODCOL);
                +" and TCCODCON = "+cp_ToStrODBC(this.w_CDCONF);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TCQTACON;
            from (i_cTable) where;
                TCCODICE = this.w_CODCOL;
                and TCCODCON = this.w_CDCONF;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CC = NVL(cp_ToDate(_read_.TCQTACON),cp_NullValue(_read_.TCQTACON))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      * --- Se il numero di confezioni per collo � a zero la procedura va in loop
      if this.w_CC=0
        * --- Segnalo l'errore
        this.w_oERRORLOG.AddMsgLog("Verificare documento %1 del %2 riga %3", ALLTRIM(str(this.w_NUMDOC,15))+"/ "+ALLTRIM(this.w_ALFDOC), DTOC(this.w_DATDOC), ALLTRIM(STR(this.w_NR)) )     
        this.w_oERRORLOG.AddMsgLogPartNoTrans(space(20), "Numero confezioni per collo a zero")     
        this.w_FLERR = .T.
      else
        * --- Se Tipo Collo non definito sui Documenti inserisce quello presente nel'anagrafica Articoli
        this.w_CODCOL = IIF(EMPTY(this.w_CODCOL), this.w_TC, this.w_CODCOL)
        * --- Read from TIP_COLL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TIP_COLL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIP_COLL_idx,2],.t.,this.TIP_COLL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TC__TARA,TCFL_MIX"+;
            " from "+i_cTable+" TIP_COLL where ";
                +"TCCODICE = "+cp_ToStrODBC(this.w_CODCOL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TC__TARA,TCFL_MIX;
            from (i_cTable) where;
                TCCODICE = this.w_CODCOL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_TARA = NVL(cp_ToDate(_read_.TC__TARA),cp_NullValue(_read_.TC__TARA))
          this.w_FLMIX = NVL(cp_ToDate(_read_.TCFL_MIX),cp_NullValue(_read_.TCFL_MIX))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_FLMIX="S"
          * --- Read from CON_COLL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CON_COLL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CON_COLL_idx,2],.t.,this.CON_COLL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TCPERTOL"+;
              " from "+i_cTable+" CON_COLL where ";
                  +"TCCODICE = "+cp_ToStrODBC(this.w_CODCOL);
                  +" and TCCODCON = "+cp_ToStrODBC(this.w_CDCONF);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TCPERTOL;
              from (i_cTable) where;
                  TCCODICE = this.w_CODCOL;
                  and TCCODCON = this.w_CDCONF;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_PERTOL = NVL(cp_ToDate(_read_.TCPERTOL),cp_NullValue(_read_.TCPERTOL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        * --- Se Forzati sulla riga N.Colli e Peso Netto prende questi
        this.w_PESNET = cp_ROUND(this.w_QTA * this.w_PN, 3)
        this.w_PESLOR = cp_ROUND(this.w_QTA * this.w_PL, 3)
        this.w_VOLUME = cp_ROUND((NVL(this.w_LUNGH,0) * NVL(this.w_LARGH,0) * NVL(this.w_ALTEZ,0)),2)
        if MVTIPRIG="R" AND (EMPTY(this.w_CDCONF) OR EMPTY(this.w_CODCOL))
          * --- Segnalo l'errore
          this.w_oERRORLOG.AddMsgLog("Verificare documento %1 del %2 riga %3", ALLTRIM(str(this.w_NUMDOC,15))+"/ "+ALLTRIM(this.w_ALFDOC), DTOC(this.w_DATDOC), ALLTRIM(STR(this.w_NR)) )     
          this.w_oERRORLOG.AddMsgLogPartNoTrans(space(20), "Codice collo o codice confezione mancante")     
          this.w_FLERR = .T.
        else
          * --- Se gi� valorizzato non ricalcolo MVNUMCOL nel caso di tipo confezione
          *     a contenuto variabile
          if this.w_NUMCOL=0 OR this.w_FLCOVA<>"S"
            if this.w_PC<>0 
              this.w_NUMCOL = cp_ROUND((this.w_QTA / this.w_PC)+ .499, 0)
            else
              this.w_NUMCOL = 0
            endif
          endif
        endif
        if Not this.w_FLERR
          if MVTIPRIG="R"
             
 INSERT INTO TMPCUR1; 
 (t_CODCONF, t_CODCOL, t_QTACON, t_MAXCON, t_TARA, t_FL_MIX,; 
 t_PERTOL, t_PESNET, t_PESLOR, t_ROWNUM, t_ROWORD,; 
 t_CODICE, t_DESART, t_QTAMOV, t_PZCON, t_UNIMIS, t_DESVO, t_UMDIM, t_VOLCONF); 
 VALUES; 
 (this.w_CDCONF, this.w_CODCOL, this.w_NUMCOL, this.w_CC, this.w_TARA, this.w_FLMIX,; 
 this.w_PERTOL, this.w_PN, this.w_PL, this.w_ROWNUM, this.w_NR,; 
 this.w_CODICE, this.w_DESART, this.w_QTA, this.w_PC, this.w_UNIMIS, this.w_DESVO, this.w_UMDIM, this.w_VOLUME)
          endif
        endif
      endif
    endif
    ENDSCAN
    if this.w_FLERR=.F.
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.w_FLERR=.T.
      * --- Abbandona la Registrazione (da non tradurre, � testato nella Catch)
      * --- Raise
      i_Error="ERRORE"
      return
    else
      * --- commit
      cp_EndTrs(.t.)
      this.w_OKDOC = this.w_OKDOC + 1
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Costruisco il secondo cursore di appoggio per calcolo Packing List
     
 CREATE CURSOR TMPCUR2 (t_PROCOL N(5,0), t_CODCONF C(3), t_CODCOL C(5), t_QTACON N(6,0), t_MAXCON N(6,0),; 
 t_PESTAR N(9,0), t_FLCMIX C(1), t_PERTOL N(6,2), t_PESNET N(9,3), t_PESLOR N(9,3), t_NUMRIG N(4,0), t_NUMORD N(5,0),; 
 t_CODICE C(20), t_DESART C(40), t_QTAART N(12,3), t_PZCONF N(9,3),t_UNIMIS C(3), t_VOLUME C(15),; 
 t_UMVOLU C(3), t_QTACON2 N(10,6), t_VOLCONF N(10,2))
    SELECT * FROM TMPCUR1 ORDER BY t_CODCONF, t_CODCOL, t_CODICE INTO CURSOR TMPCUR1
    SELECT TMPCUR1
    if RECCOUNT()>0
      this.w_PLSERIAL = SPACE(10)
      this.w_STATUS = "N"
      this.w_TEST1 = 0
      * --- Lettura Progressivo Chiave primaria PLSERIAL Packing List
      i_Conn=i_TableProp[this.PAC_MAST_IDX, 3]
      cp_NextTableProg(this, i_Conn, "SEPACK", "i_codazi,w_PLSERIAL")
      * --- Inserimento Testata Packing List
      * --- Insert into PAC_MAST
      i_nConn=i_TableProp[this.PAC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAC_MAST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAC_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"PLSERIAL"+",PLRIFDOC"+",PLNUMDOC"+",PLALFDOC"+",PLDATDOC"+",PLTIPDOC"+",PLTIPCON"+",PLCODCON"+",PLSTATUS"+",PLTIPORN"+",PLCODORN"+",PLCODVET"+",PLCODPOR"+",PLCODDES"+",PLCODSPE"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_PLSERIAL),'PAC_MAST','PLSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'PAC_MAST','PLRIFDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NUMDOC),'PAC_MAST','PLNUMDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ALFDOC),'PAC_MAST','PLALFDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DATDOC),'PAC_MAST','PLDATDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_TIPDOC),'PAC_MAST','PLTIPDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_TIPCON),'PAC_MAST','PLTIPCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODCON),'PAC_MAST','PLCODCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_STATUS),'PAC_MAST','PLSTATUS');
        +","+cp_NullLink(cp_ToStrODBC(this.w_TIPORN),'PAC_MAST','PLTIPORN');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODORN),'PAC_MAST','PLCODORN');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODVET),'PAC_MAST','PLCODVET');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODPOR),'PAC_MAST','PLCODPOR');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODDES),'PAC_MAST','PLCODDES');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODSPE),'PAC_MAST','PLCODSPE');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'PLSERIAL',this.w_PLSERIAL,'PLRIFDOC',this.w_SERIAL,'PLNUMDOC',this.w_NUMDOC,'PLALFDOC',this.w_ALFDOC,'PLDATDOC',this.w_DATDOC,'PLTIPDOC',this.w_TIPDOC,'PLTIPCON',this.w_TIPCON,'PLCODCON',this.w_CODCON,'PLSTATUS',this.w_STATUS,'PLTIPORN',this.w_TIPORN,'PLCODORN',this.w_CODORN,'PLCODVET',this.w_CODVET)
        insert into (i_cTable) (PLSERIAL,PLRIFDOC,PLNUMDOC,PLALFDOC,PLDATDOC,PLTIPDOC,PLTIPCON,PLCODCON,PLSTATUS,PLTIPORN,PLCODORN,PLCODVET,PLCODPOR,PLCODDES,PLCODSPE &i_ccchkf. );
           values (;
             this.w_PLSERIAL;
             ,this.w_SERIAL;
             ,this.w_NUMDOC;
             ,this.w_ALFDOC;
             ,this.w_DATDOC;
             ,this.w_TIPDOC;
             ,this.w_TIPCON;
             ,this.w_CODCON;
             ,this.w_STATUS;
             ,this.w_TIPORN;
             ,this.w_CODORN;
             ,this.w_CODVET;
             ,this.w_CODPOR;
             ,this.w_CODDES;
             ,this.w_CODSPE;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      SELECT TMPCUR1
      GO TOP 
      this.w_PROCOL = 0
      this.w_RIGCOL = 0
      this.w_CPROWNUM = 0
      this.w_CPROWORD = 0
      this.w_LORDCO = 0
      * --- Ciclo sul Cursore
      SCAN FOR NOT EMPTY(t_CODCONF) 
      this.w_CDCONF = t_CODCONF
      this.w_CODCOL = t_CODCOL
      this.w_QTACON = NVL(t_QTACON,0)
      this.w_MAXCON = NVL(t_MAXCON,0)
      this.w_QTA = NVL(t_QTAMOV,0)
      this.w_PC = NVL(t_PZCON,0)
      this.w_TARA = NVL(t_TARA,0)
      this.w_FLMIX = NVL(t_FL_MIX,SPACE(1))
      this.w_PERTOL = NVL(t_PERTOL,0)
      this.w_PESNET = NVL(t_PESNET,0)
      this.w_PESLOR = NVL(t_PESLOR,0)
      this.w_NR = t_ROWORD
      this.w_ROWNUM = t_ROWNUM
      this.w_CODART = t_CODICE
      this.w_DESART = NVL(t_DESART,SPACE(40))
      this.w_UNIMIS = NVL(t_UNIMIS,SPACE(3))
      this.w_DESVO = NVL(t_DESVO,SPACE(15))
      this.w_UMDIM = NVL(t_UMDIM,SPACE(3))
      this.w_VOLUME = NVL(t_VOLCONF,0)
      SELECT t_PROCOL, t_CODCOL, t_CODCONF, SUM(t_QTACON) AS QTACON;
      FROM TMPCUR2 INTO CURSOR APPO GROUP BY t_PROCOL, t_CODCOL,t_CODCONF
      SELECT APPO 
      LOCATE FOR t_CODCONF=this.w_CDCONF AND t_CODCOL=this.w_CODCOL AND QTACON<this.w_MAXCON
      if FOUND()
        this.w_COLLO = t_PROCOL
        * --- Calcola quante confezioni possono rientrare nel collo
        this.w_CONAGG = MIN((this.w_MAXCON-QTACON),this.w_QTACON)
        this.w_ARTAGG = this.w_QTA
        if this.w_CONAGG<>this.w_QTACON
          this.w_ARTAGG = this.w_CONAGG*this.w_PC
        endif
         
 INSERT INTO TMPCUR2; 
 (t_PROCOL, t_CODCONF, t_CODCOL, t_QTACON, t_MAXCON, t_PESTAR, t_FLCMIX,; 
 t_PERTOL, t_PESNET, t_PESLOR, t_NUMRIG, t_NUMORD,; 
 t_CODICE, t_DESART, t_QTAART, t_PZCONF, t_UNIMIS, t_VOLUME, t_UMVOLU, t_QTACON2, t_VOLCONF) ; 
 VALUES; 
 (this.w_COLLO,this.w_CDCONF, this.w_CODCOL, this.w_CONAGG, this.w_MAXCON, this.w_TARA, this.w_FLMIX,; 
 this.w_PERTOL, this.w_PESNET, this.w_PESLOR, this.w_ROWNUM, this.w_NR,; 
 this.w_CODART, this.w_DESART, this.w_ARTAGG, this.w_PC, this.w_UNIMIS, this.w_DESVO, this.w_UMDIM, ; 
 cp_ROUND((this.w_CONAGG * 100) / this.w_MAXCON, 6), this.w_VOLUME)
        this.w_QTACON = this.w_QTACON-this.w_CONAGG
        this.w_QTA = this.w_QTA-this.w_ARTAGG
      endif
      do while this.w_QTACON>0
        this.w_CONAGG = MIN(this.w_MAXCON,this.w_QTACON)
        this.w_ARTAGG = this.w_QTA
        if this.w_CONAGG<>this.w_QTACON
          this.w_APPO = this.w_CONAGG*this.w_PC
          this.w_ARTAGG = MIN(this.w_APPO,this.w_QTA)
        endif
        this.w_PROCOL = this.w_PROCOL+1
        this.w_TARCOL = this.w_TARCOL + this.w_TARA
         
 INSERT INTO TMPCUR2; 
 (t_PROCOL, t_CODCONF, t_CODCOL, t_QTACON, t_MAXCON, t_PESTAR, t_FLCMIX,; 
 t_PERTOL, t_PESNET, t_PESLOR, t_NUMRIG, t_NUMORD,; 
 t_CODICE, t_DESART, t_QTAART, t_PZCONF, t_UNIMIS, t_VOLUME, t_UMVOLU, t_QTACON2, t_VOLCONF) ; 
 VALUES; 
 (this.w_PROCOL ,this.w_CDCONF, this.w_CODCOL, this.w_CONAGG, this.w_MAXCON, this.w_TARA, this.w_FLMIX,; 
 this.w_PERTOL, this.w_PESNET, this.w_PESLOR, this.w_ROWNUM, this.w_NR,; 
 this.w_CODART, this.w_DESART, this.w_ARTAGG, this.w_PC, this.w_UNIMIS, this.w_DESVO, this.w_UMDIM, ; 
 cp_ROUND((this.w_CONAGG * 100) / this.w_MAXCON, 6), this.w_VOLUME)
        this.w_QTACON = this.w_QTACON-this.w_CONAGG
        this.w_QTA = this.w_QTA-this.w_ARTAGG
      enddo
      SELECT TMPCUR1
      ENDSCAN
      * --- Ricerca Eventuli Colli a Composizione Mista non ancora Pieni
       
 SELECT t_PROCOL AS t_PROCOL, SUM(t_QTACON) AS QTACON, MAX(t_MAXCON) AS MAXCON FROM TMPCUR2 ; 
 WHERE t_FLCMIX="S" GROUP BY t_PROCOL HAVING SUM(t_QTACON)<MAXCON ; 
 INTO CURSOR APPO
      if used("APPO")
        if RECCOUNT("APPO")>1
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
      SELECT * FROM TMPCUR2 INTO CURSOR APPO ORDER BY t_PROCOL
      SELECT APPO
      SELECT t_PROCOL FROM APPO GROUP BY t_PROCOL INTO CURSOR PROGRES ORDER BY t_PROCOL
      SELECT PROGRES
      GO BOTTOM
      this.w_TEST = t_PROCOL
      if this.w_TEST > 99999
        this.w_oERRORLOG.AddMsgLogPartNoTrans(space(20), "Superato massimo numero colli per documento%0Impossibile generare la Packing List")     
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg="ERRORE"
        i_Error=i_TrsMsg
        return
      else
        if this.w_TEST > 500
          this.w_MESS1 = "L'elaborazione potrebbe richiedere alcuni minuti%0Si desidera continuare?"
          if NOT ah_YesNo(this.w_MESS1)
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg="ERRORE"
            i_Error=i_TrsMsg
            return
          endif
        endif
      endif
      SELECT APPO
      GO TOP
      this.w_CPROWNUM = 0
      this.w_CPROWORD = 0
      this.w_PROCOL = -987
      SCAN FOR NOT EMPTY(t_PROCOL)
      this.w_CDCONF = NVL(t_CODCONF,SPACE(3))
      this.w_CODCOL = NVL(t_CODCOL,SPACE(5))
      this.w_DESCONF = SPACE(35)
      * --- Read from TIPICONF
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.TIPICONF_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIPICONF_idx,2],.t.,this.TIPICONF_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TCDESCRI"+;
          " from "+i_cTable+" TIPICONF where ";
              +"TCCODICE = "+cp_ToStrODBC(this.w_CDCONF);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TCDESCRI;
          from (i_cTable) where;
              TCCODICE = this.w_CDCONF;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DESCONF = NVL(cp_ToDate(_read_.TCDESCRI),cp_NullValue(_read_.TCDESCRI))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_PROCOL<>t_PROCOL
        * --- Cambia Numero di Collo
        this.w_VOLCOL = 0
        this.w_DESCOL = " "
        this.w_UMVOL = " "
        this.w_TARACO = 0
        * --- Read from TIP_COLL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TIP_COLL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIP_COLL_idx,2],.t.,this.TIP_COLL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TCDESCRI,TCVOLUME,TCUMVOLU,TC__TARA"+;
            " from "+i_cTable+" TIP_COLL where ";
                +"TCCODICE = "+cp_ToStrODBC(this.w_CODCOL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TCDESCRI,TCVOLUME,TCUMVOLU,TC__TARA;
            from (i_cTable) where;
                TCCODICE = this.w_CODCOL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DESCOL = NVL(cp_ToDate(_read_.TCDESCRI),cp_NullValue(_read_.TCDESCRI))
          this.w_VOLCOL = NVL(cp_ToDate(_read_.TCVOLUME),cp_NullValue(_read_.TCVOLUME))
          this.w_UMVOL = NVL(cp_ToDate(_read_.TCUMVOLU),cp_NullValue(_read_.TCUMVOLU))
          this.w_TARACO = NVL(cp_ToDate(_read_.TC__TARA),cp_NullValue(_read_.TC__TARA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_PROCOL = t_PROCOL
        this.w_RIGCOL = INT(this.w_PROCOL * 10)
        * --- Insert into PAC_DETT
        i_nConn=i_TableProp[this.PAC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAC_DETT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAC_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"PLTIPCOL"+",PLDESCOL"+",PLSERIAL"+",CPROWNUM"+",PLPESLOR"+",PL__TARA"+",PLUMVOLU"+",PLVOLCOL"+",CPROWORD"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_CODCOL),'PAC_DETT','PLTIPCOL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DESCOL),'PAC_DETT','PLDESCOL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PLSERIAL),'PAC_DETT','PLSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PROCOL),'PAC_DETT','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_TARACO),'PAC_DETT','PLPESLOR');
          +","+cp_NullLink(cp_ToStrODBC(this.w_TARACO),'PAC_DETT','PL__TARA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_UMVOL),'PAC_DETT','PLUMVOLU');
          +","+cp_NullLink(cp_ToStrODBC(this.w_VOLCOL),'PAC_DETT','PLVOLCOL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RIGCOL),'PAC_DETT','CPROWORD');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'PLTIPCOL',this.w_CODCOL,'PLDESCOL',this.w_DESCOL,'PLSERIAL',this.w_PLSERIAL,'CPROWNUM',this.w_PROCOL,'PLPESLOR',this.w_TARACO,'PL__TARA',this.w_TARACO,'PLUMVOLU',this.w_UMVOL,'PLVOLCOL',this.w_VOLCOL,'CPROWORD',this.w_RIGCOL)
          insert into (i_cTable) (PLTIPCOL,PLDESCOL,PLSERIAL,CPROWNUM,PLPESLOR,PL__TARA,PLUMVOLU,PLVOLCOL,CPROWORD &i_ccchkf. );
             values (;
               this.w_CODCOL;
               ,this.w_DESCOL;
               ,this.w_PLSERIAL;
               ,this.w_PROCOL;
               ,this.w_TARACO;
               ,this.w_TARACO;
               ,this.w_UMVOL;
               ,this.w_VOLCOL;
               ,this.w_RIGCOL;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        this.w_TEST1 = this.w_TEST1+1
        ah_Msg("Elaborati %1 colli su %2",.T.,.F.,.F.,alltrim(str(this.w_TEST1)), alltrim(STR(this.w_TEST)) )
        this.w_CPROWNUM = 0
        this.w_CPROWORD = 0
      endif
      this.w_CPROWNUM = this.w_CPROWNUM+1
      this.w_CPROWORD = this.w_CPROWORD + 10
      this.w_CONAGG = nvl(t_QTACON,0)
      this.w_CODART = nvl(t_CODICE,space(20))
      this.w_DESART = nvl(t_DESART,space(40))
      this.w_UNIMIS = nvl(t_UNIMIS,space(3))
      this.w_ARTAGG = nvl(t_QTAART,0)
      this.w_PESNET = nvl(t_PESNET,0)*this.w_ARTAGG
      this.w_PESLOR = nvl(t_PESLOR,0)*this.w_ARTAGG
      this.w_UMDIM = nvl(t_UMVOLU,space(3))
      this.w_VOLUME = (NVL(t_VOLCONF,0)*this.w_CONAGG)
      * --- Insert into PAC_CONF
      i_nConn=i_TableProp[this.PAC_CONF_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAC_CONF_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAC_CONF_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"PCSERIAL"+",PCNUMCOL"+",CPROWNUM"+",CPROWORD"+",PCCODCON"+",PCDESCON"+",PCQTACON"+",PCCODICE"+",PCDESART"+",PCUNIMIS"+",PCQTAART"+",PCPESNET"+",PCPESLOR"+",PCUMVOLU"+",PCVOLUME"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_PLSERIAL),'PAC_CONF','PCSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PROCOL),'PAC_CONF','PCNUMCOL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PAC_CONF','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'PAC_CONF','CPROWORD');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CDCONF),'PAC_CONF','PCCODCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DESCONF),'PAC_CONF','PCDESCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CONAGG),'PAC_CONF','PCQTACON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODART),'PAC_CONF','PCCODICE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DESART),'PAC_CONF','PCDESART');
        +","+cp_NullLink(cp_ToStrODBC(this.w_UNIMIS),'PAC_CONF','PCUNIMIS');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ARTAGG),'PAC_CONF','PCQTAART');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PESNET),'PAC_CONF','PCPESNET');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PESLOR),'PAC_CONF','PCPESLOR');
        +","+cp_NullLink(cp_ToStrODBC(this.w_UMDIM),'PAC_CONF','PCUMVOLU');
        +","+cp_NullLink(cp_ToStrODBC(this.w_VOLUME),'PAC_CONF','PCVOLUME');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'PCSERIAL',this.w_PLSERIAL,'PCNUMCOL',this.w_PROCOL,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'PCCODCON',this.w_CDCONF,'PCDESCON',this.w_DESCONF,'PCQTACON',this.w_CONAGG,'PCCODICE',this.w_CODART,'PCDESART',this.w_DESART,'PCUNIMIS',this.w_UNIMIS,'PCQTAART',this.w_ARTAGG,'PCPESNET',this.w_PESNET)
        insert into (i_cTable) (PCSERIAL,PCNUMCOL,CPROWNUM,CPROWORD,PCCODCON,PCDESCON,PCQTACON,PCCODICE,PCDESART,PCUNIMIS,PCQTAART,PCPESNET,PCPESLOR,PCUMVOLU,PCVOLUME &i_ccchkf. );
           values (;
             this.w_PLSERIAL;
             ,this.w_PROCOL;
             ,this.w_CPROWNUM;
             ,this.w_CPROWORD;
             ,this.w_CDCONF;
             ,this.w_DESCONF;
             ,this.w_CONAGG;
             ,this.w_CODART;
             ,this.w_DESART;
             ,this.w_UNIMIS;
             ,this.w_ARTAGG;
             ,this.w_PESNET;
             ,this.w_PESLOR;
             ,this.w_UMDIM;
             ,this.w_VOLUME;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      SELECT APPO
      ENDSCAN
      SELECT t_PROCOL,SUM(t_PESLOR*t_QTAART) AS LORDO FROM TMPCUR2 GROUP BY t_PROCOL ORDER BY t_PROCOL INTO CURSOR APPO 
      SELECT APPO
      GO TOP
      SCAN FOR NOT EMPTY(t_PROCOL)
      this.w_PROAPPO = t_PROCOL
      this.w_LORDO = LORDO
      * --- Write into PAC_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PAC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAC_DETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PAC_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PLPESLOR =PLPESLOR+ "+cp_ToStrODBC(this.w_LORDO);
            +i_ccchkf ;
        +" where ";
            +"PLSERIAL = "+cp_ToStrODBC(this.w_PLSERIAL);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_PROAPPO);
               )
      else
        update (i_cTable) set;
            PLPESLOR = PLPESLOR + this.w_LORDO;
            &i_ccchkf. ;
         where;
            PLSERIAL = this.w_PLSERIAL;
            and CPROWNUM = this.w_PROAPPO;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      SELECT APPO
      ENDSCAN
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ricalcolo Colli a Cmposizione Mista
    * --- Estraggo i Colli da Rielaborare
    SELECT * FROM TMPCUR2 WHERE t_PROCOL IN (SELECT t_PROCOL FROM APPO) INTO CURSOR TMPCUR1 ;
    ORDER BY t_CODCOL, t_CODCONF, t_CODICE, t_MAXCON
    * --- Li elimino dal cursore dei Rielaborati
    DELETE FROM TMPCUR2 WHERE t_PROCOL IN (SELECT t_PROCOL FROM APPO)
    SELECT TMPCUR2
    GO TOP
    * --- Ciclo sul Cursore da riassegnare
    SELECT TMPCUR1
    GO TOP 
    SCAN FOR NOT EMPTY(t_CODCONF) 
    this.w_CDCONF = t_CODCONF
    this.w_CODCOL = t_CODCOL
    this.w_QTACON = NVL(t_QTACON,0)
    this.w_QTACON2 = NVL(t_QTACON2,0)
    this.w_QTA = NVL(t_QTAART,0)
    this.w_PC = NVL(t_PZCONF,0)
    this.w_TARA = NVL(t_PESTAR,0)
    this.w_FLMIX = NVL(t_FLCMIX,SPACE(1))
    this.w_PERTOL = NVL(t_PERTOL,0)
    this.w_PESNET = NVL(t_PESNET,0)
    this.w_PESLOR = NVL(t_PESLOR,0)
    this.w_NR = t_NUMORD
    this.w_ROWNUM = t_NUMRIG
    this.w_CODART = t_CODICE
    this.w_DESART = NVL(t_DESART,SPACE(40))
    this.w_UNIMIS = NVL(t_UNIMIS,SPACE(3))
    this.w_DESVO = NVL(t_VOLUME,SPACE(15))
    this.w_UMDIM = NVL(t_UMVOLU,SPACE(3))
    this.w_VOLUME = NVL(t_VOLCONF,0)
    this.w_MAXCON = NVL(t_MAXCON,0)
    do while this.w_QTACON>0
      SELECT t_PROCOL, t_CODCOL, cp_round(SUM((t_QTACON2*100)/(100-t_PERTOL)),5) AS QTACON2;
      FROM TMPCUR2 WHERE t_FLCMIX="S" INTO CURSOR APPO GROUP BY t_PROCOL, t_CODCOL ORDER BY t_CODCOL, t_CODICE, QTACON2,t_PROCOL
      SELECT APPO 
      LOCATE FOR t_CODCOL=this.w_CODCOL AND INT(QTACON2) < 100 and MIN(INT((this.w_MAXCON * INT(100 - INT(QTACON2))) / 100),this.w_QTACON) <>0
      if FOUND()
        this.w_COLLO = t_PROCOL
        * --- Calcola la percentuale ancora libera del collo
        this.w_CONAGG2 = INT(100 - INT(QTACON2))
        * --- Calcola quante confezioni possono rientrare nel collo
        this.w_CONAGG = MIN(INT((this.w_MAXCON * this.w_CONAGG2) / 100),this.w_QTACON)
        if this.w_CONAGG=0
          EXIT
        endif
        this.w_ARTAGG = this.w_QTA
        if this.w_CONAGG<>this.w_QTACON
          this.w_ARTAGG = MIN(INT(this.w_CONAGG*this.w_PC),this.w_QTA)
        endif
        INSERT INTO TMPCUR2;
        (t_PROCOL, t_CODCONF, t_CODCOL, t_QTACON, t_MAXCON, t_PESTAR, t_FLCMIX,;
         t_PERTOL, t_PESNET, t_PESLOR, t_NUMRIG, t_NUMORD,;
        t_CODICE, t_DESART, t_QTAART, t_PZCONF, t_UNIMIS, t_VOLUME, t_UMVOLU, t_QTACON2, t_VOLCONF) ;
        VALUES;
        (this.w_COLLO,this.w_CDCONF, this.w_CODCOL, this.w_CONAGG, this.w_MAXCON, this.w_TARA, this.w_FLMIX,;
        this.w_PERTOL, this.w_PESNET, this.w_PESLOR, this.w_ROWNUM, this.w_NR,;
        this.w_CODART, this.w_DESART, this.w_ARTAGG, this.w_PC, this.w_UNIMIS, this.w_DESVO, this.w_UMDIM, ;
        cp_ROUND((this.w_CONAGG * 100) / this.w_MAXCON, 6), this.w_VOLUME)
        this.w_QTACON = this.w_QTACON-this.w_CONAGG
        this.w_QTA = this.w_QTA-this.w_ARTAGG
      else
        EXIT
      endif
    enddo
    do while this.w_QTACON>0
      this.w_CONAGG = MIN(this.w_MAXCON,this.w_QTACON)
      if this.w_CONAGG=0
        EXIT
      endif
      this.w_ARTAGG = this.w_QTA
      if this.w_CONAGG<>this.w_QTACON
        this.w_APPO = INT(this.w_CONAGG*this.w_PC)
        this.w_ARTAGG = MIN(this.w_APPO,this.w_QTA)
      endif
      this.w_PROCOL = this.w_PROCOL+1
      INSERT INTO TMPCUR2;
      (t_PROCOL, t_CODCONF, t_CODCOL, t_QTACON, t_MAXCON, t_PESTAR, t_FLCMIX,;
       t_PERTOL, t_PESNET, t_PESLOR, t_NUMRIG, t_NUMORD,;
      t_CODICE, t_DESART, t_QTAART, t_PZCONF, t_UNIMIS, t_VOLUME, t_UMVOLU, t_QTACON2, t_VOLCONF) ;
      VALUES;
      (this.w_PROCOL ,this.w_CDCONF, this.w_CODCOL, this.w_CONAGG, this.w_MAXCON, this.w_TARA, this.w_FLMIX,;
      this.w_PERTOL, this.w_PESNET, this.w_PESLOR, this.w_ROWNUM, this.w_NR,;
      this.w_CODART, this.w_DESART, this.w_ARTAGG, this.w_PC, this.w_UNIMIS, this.w_DESVO, this.w_UMDIM, ;
      cp_ROUND((this.w_CONAGG * 100) / this.w_MAXCON, 6), this.w_VOLUME)
      this.w_QTACON = this.w_QTACON-this.w_CONAGG
      this.w_QTA = this.w_QTA-this.w_ARTAGG
    enddo
    SELECT TMPCUR1
    ENDSCAN
    * --- Rinumero i Colli 
    SELECT t_PROCOL, MAX(t_PESTAR) AS t_PESTAR FROM TMPCUR2 INTO CURSOR APPO GROUP BY t_PROCOL ORDER BY t_PROCOL
    SELECT APPO
    GO TOP
    this.w_PROCOL = 0
    this.w_TARCOL = 0
    SCAN
    this.w_OLDCOL = t_PROCOL
    this.w_PROCOL = this.w_PROCOL + 1
    this.w_TARCOL = this.w_TARCOL + t_PESTAR
    UPDATE TMPCUR2 SET t_PROCOL=this.w_PROCOL, t_QTACON2=-87 WHERE t_PROCOL=this.w_OLDCOL AND t_QTACON2<>-87
    ENDSCAN
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,9)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='CON_COLL'
    this.cWorkTables[3]='KEY_ARTI'
    this.cWorkTables[4]='PAC_CONF'
    this.cWorkTables[5]='PAC_DETT'
    this.cWorkTables[6]='PAC_MAST'
    this.cWorkTables[7]='TIPICONF'
    this.cWorkTables[8]='TIP_COLL'
    this.cWorkTables[9]='UNIMIS'
    return(this.OpenAllTables(9))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
