* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bno                                                        *
*              Gestione note                                                   *
*                                                                              *
*      Author: Mimmo Fasano                                                    *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-09-20                                                      *
* Last revis.: 2007-12-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bno",oParentObject,m.pTipo)
return(i_retval)

define class tgsar_bno as StdBatch
  * --- Local variables
  pTipo = space(1)
  DATACHR = space(12)
  HEX0D = space(1)
  HEX0A = space(1)
  w_OBJ = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Imposta Data e Descrizione x Ogni Riga del Campo Note
    this.DATACHR = dtoc(i_DATSYS)
    this.HEX0D = chr(13)
    this.HEX0A = chr(10)
    do case
      case this.pTipo = "N"
        this.oParentObject.W_NO__NOTE = this.HEX0D + this.HEX0A + "[" + this.DATACHR + "]" + " (" + alltrim(g_DESUTE) +")" + ": " + this.HEX0D + this.HEX0A + this.oParentObject.w_NO__NOTE
        MOUSE CLICK AT 8,45 window (this.oParentObject.name)
      case this.pTipo = "O"
        this.oParentObject.W_OF__NOTE = this.HEX0D + this.HEX0A + "[" + this.DATACHR + "]" + " (" + alltrim(g_DESUTE) +")" + ": " + this.HEX0D + this.HEX0A + this.oParentObject.w_OF__NOTE
        MOUSE CLICK AT 16,45 window (this.oParentObject.name)
      case this.pTipo = "P"
        * --- Note pratica
        this.oParentObject.w_CN__NOTE = "[" + this.DATACHR + "]" + " (" + alltrim(g_DESUTE) +")" + ":" + this.HEX0D + this.HEX0A + IIF(NOT EMPTY(this.oParentObject.w_CN__NOTE), this.HEX0D + this.HEX0A, "") + this.oParentObject.w_CN__NOTE
        this.w_OBJ = this.oParentObject.Getctrl("w_CN__NOTE")
        this.w_OBJ.Setfocus()     
        this.w_OBJ.SelStart = at(":" , this.oParentObject.w_CN__NOTE) + 2
    endcase
  endproc


  proc Init(oParentObject,pTipo)
    this.pTipo=pTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo"
endproc
