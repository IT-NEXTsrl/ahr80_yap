* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_ms2                                                        *
*              Inserimento scaglioni listini                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_97]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-17                                                      *
* Last revis.: 2011-03-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsma_ms2")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsma_ms2")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsma_ms2")
  return

* --- Class definition
define class tgsma_ms2 as StdPCForm
  Width  = 269
  Height = 245
  Top    = 134
  Left   = 258
  cComment = "Inserimento scaglioni listini"
  cPrg = "gsma_ms2"
  HelpContextID=97155945
  add object cnt as tcgsma_ms2
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsma_ms2 as PCContext
  w_LICODLIS = space(5)
  w_LIROWNUM = 0
  w_LIPREZZO = 0
  w_LIQUANTI = 0
  w_OLTRE = space(5)
  w_LISCONT1 = 0
  w_LISCONT2 = 0
  w_LISCONT3 = 0
  w_LISCONT4 = 0
  w_FLSCO = space(1)
  proc Save(i_oFrom)
    this.w_LICODLIS = i_oFrom.w_LICODLIS
    this.w_LIROWNUM = i_oFrom.w_LIROWNUM
    this.w_LIPREZZO = i_oFrom.w_LIPREZZO
    this.w_LIQUANTI = i_oFrom.w_LIQUANTI
    this.w_OLTRE = i_oFrom.w_OLTRE
    this.w_LISCONT1 = i_oFrom.w_LISCONT1
    this.w_LISCONT2 = i_oFrom.w_LISCONT2
    this.w_LISCONT3 = i_oFrom.w_LISCONT3
    this.w_LISCONT4 = i_oFrom.w_LISCONT4
    this.w_FLSCO = i_oFrom.w_FLSCO
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_LICODLIS = this.w_LICODLIS
    i_oTo.w_LIROWNUM = this.w_LIROWNUM
    i_oTo.w_LIPREZZO = this.w_LIPREZZO
    i_oTo.w_LIQUANTI = this.w_LIQUANTI
    i_oTo.w_OLTRE = this.w_OLTRE
    i_oTo.w_LISCONT1 = this.w_LISCONT1
    i_oTo.w_LISCONT2 = this.w_LISCONT2
    i_oTo.w_LISCONT3 = this.w_LISCONT3
    i_oTo.w_LISCONT4 = this.w_LISCONT4
    i_oTo.w_FLSCO = this.w_FLSCO
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsma_ms2 as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 269
  Height = 245
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-03-01"
  HelpContextID=97155945
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  INS_SCAG_IDX = 0
  LISTINI_IDX = 0
  cFile = "INS_SCAG"
  cKeySelect = "LICODLIS,LIROWNUM"
  cKeyWhere  = "LICODLIS=this.w_LICODLIS and LIROWNUM=this.w_LIROWNUM"
  cKeyDetail  = "LICODLIS=this.w_LICODLIS and LIROWNUM=this.w_LIROWNUM and LIQUANTI=this.w_LIQUANTI"
  cKeyWhereODBC = '"LICODLIS="+cp_ToStrODBC(this.w_LICODLIS)';
      +'+" and LIROWNUM="+cp_ToStrODBC(this.w_LIROWNUM)';

  cKeyDetailWhereODBC = '"LICODLIS="+cp_ToStrODBC(this.w_LICODLIS)';
      +'+" and LIROWNUM="+cp_ToStrODBC(this.w_LIROWNUM)';
      +'+" and LIQUANTI="+cp_ToStrODBC(this.w_LIQUANTI)';

  cKeyWhereODBCqualified = '"INS_SCAG.LICODLIS="+cp_ToStrODBC(this.w_LICODLIS)';
      +'+" and INS_SCAG.LIROWNUM="+cp_ToStrODBC(this.w_LIROWNUM)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gsma_ms2"
  cComment = "Inserimento scaglioni listini"
  i_nRowNum = 0
  i_nRowPerPage = 9
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_LICODLIS = space(5)
  w_LIROWNUM = 0
  w_LIPREZZO = 0
  w_LIQUANTI = 0
  w_OLTRE = space(5)
  w_LISCONT1 = 0
  o_LISCONT1 = 0
  w_LISCONT2 = 0
  o_LISCONT2 = 0
  w_LISCONT3 = 0
  o_LISCONT3 = 0
  w_LISCONT4 = 0
  w_FLSCO = space(1)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_ms2Pag1","gsma_ms2",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='LISTINI'
    this.cWorkTables[2]='INS_SCAG'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.INS_SCAG_IDX,5],7]
    this.nPostItConn=i_TableProp[this.INS_SCAG_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsma_ms2'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from INS_SCAG where LICODLIS=KeySet.LICODLIS
    *                            and LIROWNUM=KeySet.LIROWNUM
    *                            and LIQUANTI=KeySet.LIQUANTI
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- gsma_ms2
      * --- Setta Ordine per Quantita'
      i_cOrder = 'order by LIPREZZO Desc '
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.INS_SCAG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INS_SCAG_IDX,2],this.bLoadRecFilter,this.INS_SCAG_IDX,"gsma_ms2")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('INS_SCAG')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "INS_SCAG.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' INS_SCAG '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'LICODLIS',this.w_LICODLIS  ,'LIROWNUM',this.w_LIROWNUM  )
      select * from (i_cTable) INS_SCAG where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_LICODLIS = NVL(LICODLIS,space(5))
        .w_LIROWNUM = NVL(LIROWNUM,0)
        .w_FLSCO = this.oparentobject.w_FLSCO
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'INS_SCAG')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_LIPREZZO = NVL(LIPREZZO,0)
          .w_LIQUANTI = NVL(LIQUANTI,0)
        .w_OLTRE = IIF(.w_LIQUANTI=0 AND .w_LIPREZZO<>0,'Oltre','')
          .w_LISCONT1 = NVL(LISCONT1,0)
          .w_LISCONT2 = NVL(LISCONT2,0)
          .w_LISCONT3 = NVL(LISCONT3,0)
          .w_LISCONT4 = NVL(LISCONT4,0)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace LIQUANTI with .w_LIQUANTI
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_FLSCO = this.oparentobject.w_FLSCO
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_LICODLIS=space(5)
      .w_LIROWNUM=0
      .w_LIPREZZO=0
      .w_LIQUANTI=0
      .w_OLTRE=space(5)
      .w_LISCONT1=0
      .w_LISCONT2=0
      .w_LISCONT3=0
      .w_LISCONT4=0
      .w_FLSCO=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,4,.f.)
        .w_OLTRE = IIF(.w_LIQUANTI=0 AND .w_LIPREZZO<>0,'Oltre','')
        .DoRTCalc(6,6,.f.)
        .w_LISCONT2 = 0
        .w_LISCONT3 = 0
        .w_LISCONT4 = 0
        .w_FLSCO = this.oparentobject.w_FLSCO
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'INS_SCAG')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oLISCONT1_2_4.enabled = i_bVal
      .Page1.oPag.oLISCONT2_2_5.enabled = i_bVal
      .Page1.oPag.oLISCONT3_2_6.enabled = i_bVal
      .Page1.oPag.oLISCONT4_2_7.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'INS_SCAG',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.INS_SCAG_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LICODLIS,"LICODLIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LIROWNUM,"LIROWNUM",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_LIPREZZO N(18,5);
      ,t_LIQUANTI N(12,3);
      ,t_OLTRE C(5);
      ,t_LISCONT1 N(6,2);
      ,t_LISCONT2 N(6,2);
      ,t_LISCONT3 N(6,2);
      ,t_LISCONT4 N(6,2);
      ,LIQUANTI N(12,3);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsma_ms2bodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLIPREZZO_2_1.controlsource=this.cTrsName+'.t_LIPREZZO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLIQUANTI_2_2.controlsource=this.cTrsName+'.t_LIQUANTI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oOLTRE_2_3.controlsource=this.cTrsName+'.t_OLTRE'
    this.oPgFRm.Page1.oPag.oLISCONT1_2_4.controlsource=this.cTrsName+'.t_LISCONT1'
    this.oPgFRm.Page1.oPag.oLISCONT2_2_5.controlsource=this.cTrsName+'.t_LISCONT2'
    this.oPgFRm.Page1.oPag.oLISCONT3_2_6.controlsource=this.cTrsName+'.t_LISCONT3'
    this.oPgFRm.Page1.oPag.oLISCONT4_2_7.controlsource=this.cTrsName+'.t_LISCONT4'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(128)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLIPREZZO_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.INS_SCAG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INS_SCAG_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.INS_SCAG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INS_SCAG_IDX,2])
      *
      * insert into INS_SCAG
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'INS_SCAG')
        i_extval=cp_InsertValODBCExtFlds(this,'INS_SCAG')
        i_cFldBody=" "+;
                  "(LICODLIS,LIROWNUM,LIPREZZO,LIQUANTI,LISCONT1"+;
                  ",LISCONT2,LISCONT3,LISCONT4,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_LICODLIS)+","+cp_ToStrODBC(this.w_LIROWNUM)+","+cp_ToStrODBC(this.w_LIPREZZO)+","+cp_ToStrODBC(this.w_LIQUANTI)+","+cp_ToStrODBC(this.w_LISCONT1)+;
             ","+cp_ToStrODBC(this.w_LISCONT2)+","+cp_ToStrODBC(this.w_LISCONT3)+","+cp_ToStrODBC(this.w_LISCONT4)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'INS_SCAG')
        i_extval=cp_InsertValVFPExtFlds(this,'INS_SCAG')
        cp_CheckDeletedKey(i_cTable,0,'LICODLIS',this.w_LICODLIS,'LIROWNUM',this.w_LIROWNUM,'LIQUANTI',this.w_LIQUANTI)
        INSERT INTO (i_cTable) (;
                   LICODLIS;
                  ,LIROWNUM;
                  ,LIPREZZO;
                  ,LIQUANTI;
                  ,LISCONT1;
                  ,LISCONT2;
                  ,LISCONT3;
                  ,LISCONT4;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_LICODLIS;
                  ,this.w_LIROWNUM;
                  ,this.w_LIPREZZO;
                  ,this.w_LIQUANTI;
                  ,this.w_LISCONT1;
                  ,this.w_LISCONT2;
                  ,this.w_LISCONT3;
                  ,this.w_LISCONT4;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.INS_SCAG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INS_SCAG_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (t_LIPREZZO<>0) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'INS_SCAG')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and LIQUANTI="+cp_ToStrODBC(&i_TN.->LIQUANTI)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'INS_SCAG')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and LIQUANTI=&i_TN.->LIQUANTI;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_LIPREZZO<>0) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and LIQUANTI="+cp_ToStrODBC(&i_TN.->LIQUANTI)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and LIQUANTI=&i_TN.->LIQUANTI;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace LIQUANTI with this.w_LIQUANTI
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update INS_SCAG
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'INS_SCAG')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " LIPREZZO="+cp_ToStrODBC(this.w_LIPREZZO)+;
                     ",LISCONT1="+cp_ToStrODBC(this.w_LISCONT1)+;
                     ",LISCONT2="+cp_ToStrODBC(this.w_LISCONT2)+;
                     ",LISCONT3="+cp_ToStrODBC(this.w_LISCONT3)+;
                     ",LISCONT4="+cp_ToStrODBC(this.w_LISCONT4)+;
                     ",LIQUANTI="+cp_ToStrODBC(this.w_LIQUANTI)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and LIQUANTI="+cp_ToStrODBC(LIQUANTI)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'INS_SCAG')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      LIPREZZO=this.w_LIPREZZO;
                     ,LISCONT1=this.w_LISCONT1;
                     ,LISCONT2=this.w_LISCONT2;
                     ,LISCONT3=this.w_LISCONT3;
                     ,LISCONT4=this.w_LISCONT4;
                     ,LIQUANTI=this.w_LIQUANTI;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and LIQUANTI=&i_TN.->LIQUANTI;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.INS_SCAG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INS_SCAG_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_LIPREZZO<>0) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete INS_SCAG
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and LIQUANTI="+cp_ToStrODBC(&i_TN.->LIQUANTI)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and LIQUANTI=&i_TN.->LIQUANTI;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_LIPREZZO<>0) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.INS_SCAG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INS_SCAG_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
          .w_OLTRE = IIF(.w_LIQUANTI=0 AND .w_LIPREZZO<>0,'Oltre','')
        .DoRTCalc(6,6,.t.)
        if .o_LISCONT1<>.w_LISCONT1
          .w_LISCONT2 = 0
        endif
        if .o_LISCONT2<>.w_LISCONT2
          .w_LISCONT3 = 0
        endif
        if .o_LISCONT3<>.w_LISCONT3
          .w_LISCONT4 = 0
        endif
          .w_FLSCO = this.oparentobject.w_FLSCO
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oLIQUANTI_2_2.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oLIQUANTI_2_2.mCond()
    this.oPgFrm.Page1.oPag.oLISCONT1_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oLISCONT1_2_4.mCond()
    this.oPgFrm.Page1.oPag.oLISCONT2_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oLISCONT2_2_5.mCond()
    this.oPgFrm.Page1.oPag.oLISCONT3_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oLISCONT3_2_6.mCond()
    this.oPgFrm.Page1.oPag.oLISCONT4_2_7.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oLISCONT4_2_7.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_4.visible=!this.oPgFrm.Page1.oPag.oStr_1_4.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oLISCONT1_2_4.visible=!this.oPgFrm.Page1.oPag.oLISCONT1_2_4.mHide()
    this.oPgFrm.Page1.oPag.oLISCONT2_2_5.visible=!this.oPgFrm.Page1.oPag.oLISCONT2_2_5.mHide()
    this.oPgFrm.Page1.oPag.oLISCONT3_2_6.visible=!this.oPgFrm.Page1.oPag.oLISCONT3_2_6.mHide()
    this.oPgFrm.Page1.oPag.oLISCONT4_2_7.visible=!this.oPgFrm.Page1.oPag.oLISCONT4_2_7.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oLISCONT1_2_4.value==this.w_LISCONT1)
      this.oPgFrm.Page1.oPag.oLISCONT1_2_4.value=this.w_LISCONT1
      replace t_LISCONT1 with this.oPgFrm.Page1.oPag.oLISCONT1_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oLISCONT2_2_5.value==this.w_LISCONT2)
      this.oPgFrm.Page1.oPag.oLISCONT2_2_5.value=this.w_LISCONT2
      replace t_LISCONT2 with this.oPgFrm.Page1.oPag.oLISCONT2_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oLISCONT3_2_6.value==this.w_LISCONT3)
      this.oPgFrm.Page1.oPag.oLISCONT3_2_6.value=this.w_LISCONT3
      replace t_LISCONT3 with this.oPgFrm.Page1.oPag.oLISCONT3_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oLISCONT4_2_7.value==this.w_LISCONT4)
      this.oPgFrm.Page1.oPag.oLISCONT4_2_7.value=this.w_LISCONT4
      replace t_LISCONT4 with this.oPgFrm.Page1.oPag.oLISCONT4_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLIPREZZO_2_1.value==this.w_LIPREZZO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLIPREZZO_2_1.value=this.w_LIPREZZO
      replace t_LIPREZZO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLIPREZZO_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLIQUANTI_2_2.value==this.w_LIQUANTI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLIQUANTI_2_2.value=this.w_LIQUANTI
      replace t_LIQUANTI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLIQUANTI_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOLTRE_2_3.value==this.w_OLTRE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOLTRE_2_3.value=this.w_OLTRE
      replace t_OLTRE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOLTRE_2_3.value
    endif
    cp_SetControlsValueExtFlds(this,'INS_SCAG')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_LIPREZZO=0 OR CHKSCAGL( .ctrsname , .w_LIQUANTI )) and (.w_LIPREZZO<>0) and (.w_LIPREZZO<>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLIQUANTI_2_2
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Scaglione gi� esistente")
      endcase
      if .w_LIPREZZO<>0
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_LISCONT1 = this.w_LISCONT1
    this.o_LISCONT2 = this.w_LISCONT2
    this.o_LISCONT3 = this.w_LISCONT3
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_LIPREZZO<>0)
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_LIPREZZO=0
      .w_LIQUANTI=0
      .w_OLTRE=space(5)
      .w_LISCONT1=0
      .w_LISCONT2=0
      .w_LISCONT3=0
      .w_LISCONT4=0
      .DoRTCalc(1,4,.f.)
        .w_OLTRE = IIF(.w_LIQUANTI=0 AND .w_LIPREZZO<>0,'Oltre','')
      .DoRTCalc(6,6,.f.)
        .w_LISCONT2 = 0
        .w_LISCONT3 = 0
        .w_LISCONT4 = 0
    endwith
    this.DoRTCalc(10,10,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_LIPREZZO = t_LIPREZZO
    this.w_LIQUANTI = t_LIQUANTI
    this.w_OLTRE = t_OLTRE
    this.w_LISCONT1 = t_LISCONT1
    this.w_LISCONT2 = t_LISCONT2
    this.w_LISCONT3 = t_LISCONT3
    this.w_LISCONT4 = t_LISCONT4
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_LIPREZZO with this.w_LIPREZZO
    replace t_LIQUANTI with this.w_LIQUANTI
    replace t_OLTRE with this.w_OLTRE
    replace t_LISCONT1 with this.w_LISCONT1
    replace t_LISCONT2 with this.w_LISCONT2
    replace t_LISCONT3 with this.w_LISCONT3
    replace t_LISCONT4 with this.w_LISCONT4
    if i_srv='A'
      replace LIQUANTI with this.w_LIQUANTI
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsma_ms2Pag1 as StdContainer
  Width  = 265
  height = 245
  stdWidth  = 265
  stdheight = 245
  resizeYpos=121
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=11, top=0, width=246,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=2,Field1="LIPREZZO",Label1="Prezzo",Field2="LIQUANTI",Label2="Fino a qta",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 218934906

  add object oStr_1_4 as StdString with uid="HGXAUUDAHR",Visible=.t., Left=11, Top=195,;
    Alignment=0, Width=208, Height=18,;
    Caption="Sconti/maggiorazioni"  ;
  , bGlobalFont=.t.

  func oStr_1_4.mHide()
    with this.Parent.oContained
      return (.w_FLSCO<>'S')
    endwith
  endfunc

  add object oBox_1_3 as StdBox with uid="DIJZNMIOVG",left=11, top=212, width=234,height=2

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=1,top=19,;
    width=242+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*9*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=2,top=20,width=241+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*9*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oLISCONT1_2_4.Refresh()
      this.Parent.oLISCONT2_2_5.Refresh()
      this.Parent.oLISCONT3_2_6.Refresh()
      this.Parent.oLISCONT4_2_7.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oLISCONT1_2_4 as StdTrsField with uid="YUFHOEGKFR",rtseq=6,rtrep=.t.,;
    cFormVar="w_LISCONT1",value=0,;
    ToolTipText = "1^ Maggiorazione (se positiva) o sconto (se negativa) appicata allo scaglione",;
    HelpContextID = 225313511,;
    cTotal="", bFixedPos=.t., cQueryName = "LISCONT1",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=11, Top=217

  func oLISCONT1_2_4.mCond()
    with this.Parent.oContained
      return (g_NUMSCO>0 AND .w_FLSCO='S')
    endwith
  endfunc

  func oLISCONT1_2_4.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLSCO<>'S')
    endwith
    endif
  endfunc

  add object oLISCONT2_2_5 as StdTrsField with uid="OFMQAQJDYX",rtseq=7,rtrep=.t.,;
    cFormVar="w_LISCONT2",value=0,;
    ToolTipText = "2^ Maggiorazione (se positiva) o sconto (se negativa) appicata allo scaglione",;
    HelpContextID = 225313512,;
    cTotal="", bFixedPos=.t., cQueryName = "LISCONT2",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=70, Top=217

  func oLISCONT2_2_5.mCond()
    with this.Parent.oContained
      return (.w_LISCONT1<>0 AND g_NUMSCO>1 AND .w_FLSCO='S')
    endwith
  endfunc

  func oLISCONT2_2_5.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLSCO<>'S')
    endwith
    endif
  endfunc

  add object oLISCONT3_2_6 as StdTrsField with uid="DUZVAIZVAH",rtseq=8,rtrep=.t.,;
    cFormVar="w_LISCONT3",value=0,;
    ToolTipText = "3^ Maggiorazione (se positiva) o sconto (se negativa) appicata allo scaglione",;
    HelpContextID = 225313513,;
    cTotal="", bFixedPos=.t., cQueryName = "LISCONT3",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=129, Top=217

  func oLISCONT3_2_6.mCond()
    with this.Parent.oContained
      return (.w_LISCONT2<>0 AND g_NUMSCO>2 AND .w_FLSCO='S')
    endwith
  endfunc

  func oLISCONT3_2_6.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLSCO<>'S')
    endwith
    endif
  endfunc

  add object oLISCONT4_2_7 as StdTrsField with uid="JMNOLIGDEL",rtseq=9,rtrep=.t.,;
    cFormVar="w_LISCONT4",value=0,;
    ToolTipText = "4^ Maggiorazione (se positiva) o sconto (se negativa) appicata allo scaglione",;
    HelpContextID = 225313514,;
    cTotal="", bFixedPos=.t., cQueryName = "LISCONT4",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=188, Top=217

  func oLISCONT4_2_7.mCond()
    with this.Parent.oContained
      return (.w_LISCONT3<>0 AND g_NUMSCO>3 AND .w_FLSCO='S')
    endwith
  endfunc

  func oLISCONT4_2_7.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLSCO<>'S')
    endwith
    endif
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsma_ms2BodyRow as CPBodyRowCnt
  Width=232
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oLIPREZZO_2_1 as StdTrsField with uid="CAXSETNOED",rtseq=3,rtrep=.t.,;
    cFormVar="w_LIPREZZO",value=0,;
    ToolTipText = "Prezzo di listino",;
    HelpContextID = 119745787,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=113, Left=-2, Top=0, cSayPict=[v_PU(40+VVL)], cGetPict=[v_GU(40+VVL)]

  add object oLIQUANTI_2_2 as StdTrsField with uid="HCAUXKRQHD",rtseq=4,rtrep=.t.,;
    cFormVar="w_LIQUANTI",value=0,isprimarykey=.t.,;
    ToolTipText = "Quantit� scaglione",;
    HelpContextID = 211804927,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Scaglione gi� esistente",;
   bGlobalFont=.t.,;
    Height=17, Width=107, Left=120, Top=0, cSayPict=['@Z '+v_PQ(11)], cGetPict=['@Z '+v_GQ(11)]

  func oLIQUANTI_2_2.mCond()
    with this.Parent.oContained
      return (.w_LIPREZZO<>0)
    endwith
  endfunc

  func oLIQUANTI_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_LIPREZZO=0 OR CHKSCAGL( .ctrsname , .w_LIQUANTI ))
    endwith
    return bRes
  endfunc

  add object oOLTRE_2_3 as StdTrsField with uid="DPQKPVHHDM",rtseq=5,rtrep=.t.,;
    cFormVar="w_OLTRE",value=space(5),enabled=.f.,;
    HelpContextID = 19065370,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=30, Left=120, Top=0, InputMask=replicate('X',5)
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oLIPREZZO_2_1.When()
    return(.t.)
  proc oLIPREZZO_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oLIPREZZO_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=8
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_ms2','INS_SCAG','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".LICODLIS=INS_SCAG.LICODLIS";
  +" and "+i_cAliasName2+".LIROWNUM=INS_SCAG.LIROWNUM";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
