* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: ah_inputbox                                                     *
*              INPUTBOX                                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-12-04                                                      *
* Last revis.: 2009-01-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pETICHETTA,pTITOLO
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tah_inputbox",oParentObject,m.pETICHETTA,m.pTITOLO)
return(i_retval)

define class tah_inputbox as StdBatch
  * --- Local variables
  pETICHETTA = space(250)
  pTITOLO = space(250)
  w_ETICHETTA = space(250)
  w_TITOLO = space(250)
  w_VALOREDIRITORNO = space(0)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Da utilizzare al posto di INPUTBOX di Viisual Fox Pro che non consente l'utilizzo di CTRL+V
    * --- Parametri:
    *     w_ETICHETTA : label da visualizzare
    *     w_TITOLO: Caption della maschera
    this.w_VALOREDIRITORNO = ""
    if VARTYPE( this.pETICHETTA ) = "C"
      this.w_ETICHETTA = this.pETICHETTA
    else
      this.w_ETICHETTA = ""
    endif
    if VARTYPE( this.pTITOLO ) = "C"
      this.w_TITOLO = this.pTITOLO
    else
      this.w_TITOLO = ""
    endif
    do GSUT_KIN with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    i_retcode = 'stop'
    i_retval = this.w_VALOREDIRITORNO
    return
  endproc


  proc Init(oParentObject,pETICHETTA,pTITOLO)
    this.pETICHETTA=pETICHETTA
    this.pTITOLO=pTITOLO
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pETICHETTA,pTITOLO"
endproc
