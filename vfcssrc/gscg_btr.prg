* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_btr                                                        *
*              Carica saldi bilancio eu                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][39][VRS_71]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-14                                                      *
* Last revis.: 2011-05-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEXEC
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_btr",oParentObject,m.pEXEC)
return(i_retval)

define class tgscg_btr as StdBatch
  * --- Local variables
  pEXEC = space(1)
  w_LDESCRI = space(40)
  w_LNOTE = space(0)
  w_SLSERIAL = space(10)
  w_TRCODVOC = space(15)
  w_TRROWORD = 0
  w_PADRE = .NULL.
  w_STRUT = .NULL.
  w_APERTURA = .f.
  w_ESE = space(4)
  w_ESEPRE = space(4)
  w_CODAZI = space(5)
  w_INIESE = ctod("  /  /  ")
  * --- WorkFile variables
  AGG_SALD_idx=0
  BIL_MAST_idx=0
  TMPMOVIMAST_idx=0
  TMP_PART_idx=0
  ESERCIZI_idx=0
  BIL_DETT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eseguito:
    *     - dalla gestione aggiorna saldi bilancio UE GSCG_MAS
    *     - maschera caricamento Manutenzione GSCG_KTR
    * --- C: carica dettaglio conti attingendo elenco conti dalla struttura e i valori 
    *         dai movimenti di primanota
    *     T: inserisce testata bilancio ue qualora perl'esercizio selezionato non esista
    *     D: azzera tabella di appoggio AGG_SALD e aggiorna descrizioni
    *     F: alla delete end cancella riga
    do case
      case this.pEXEC="C"
        this.w_PADRE = this.oparentobject
        this.w_STRUT = this.oparentobject.oparentobject
        this.w_TRCODVOC = this.w_STRUT.w_TRCODICE
        this.w_TRROWORD = this.w_STRUT.w_CPROWNUM
        * --- Create temporary table TMP_PART
        i_nIdx=cp_AddTableDef('TMP_PART') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('gscg_btr',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMP_PART_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        this.w_ESE = this.oParentObject.w_TRCODESE
        if this.pEXEC<>"C"
          * --- esegue valorizzazione da movimenti primanota (non abilitata)
          * --- Varibili di struttura
          this.w_CODAZI = i_CODAZI
          * --- Read from ESERCIZI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ESERCIZI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ESINIESE"+;
              " from "+i_cTable+" ESERCIZI where ";
                  +"ESCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ESINIESE;
              from (i_cTable) where;
                  ESCODAZI = this.w_CODAZI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_INIESE = NVL(cp_ToDate(_read_.ESINIESE),cp_NullValue(_read_.ESINIESE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_ESEPRE = CALCESPR(this.w_CODAZI,this.w_INIESE,.T.)
          * --- Creo Temporanea con struttura aggiornata
          * --- Leggo Importi da Movimenti relativi alla struttura creata
          * --- Create temporary table TMPMOVIMAST
          i_nIdx=cp_AddTableDef('TMPMOVIMAST') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          declare indexes_AUDPTELMAU[2]
          indexes_AUDPTELMAU[1]='PNTIPCON,PNCODCON'
          indexes_AUDPTELMAU[2]='MCCODICE'
          vq_exec('query\gscg0btr',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_AUDPTELMAU,.f.)
          this.TMPMOVIMAST_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          * --- Verifica effettuazione Apertura Conti
          * --- Insert into TMPMOVIMAST
          i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\GSCGABTR",this.TMPMOVIMAST_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          if i_Rows > 0
            this.w_APERTURA = .T.
          endif
          if NOT this.w_APERTURA 
            * --- 2- SE NON E' STATA FATTA L'APERTURA DEI CONTI, LEGGIAMO I SALDI DAI MOVIMENTI DELL'ESERCIZIO PRECEDENDE
            *     DALLA LETTURA ESCLUDIAMO I MOVIMENTI DI CHIUSURA
            * --- Insert into TMPMOVIMAST
            i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\GSCGSBTR",this.TMPMOVIMAST_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
            if i_Rows=0 
              * --- non ci sono movimenti nell'esercizio precedente per cui provo a leggere il movimento di apertura
              * --- Verifica effettuazione Apertura Conti
              this.w_ESE = this.w_ESEPRE
              * --- Insert into TMPMOVIMAST
              i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              if i_nConn<>0
                i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\GSCGABTR",this.TMPMOVIMAST_idx)
              else
                error "not yet implemented!"
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
            endif
            * --- LEGGIAMO ANCHE I MOVIMENTI FUORI LINEA CHE DEVONO POI ESSERE SOMMATI.
            * --- Lettura saldi fuori linea
            * --- Insert into TMPMOVIMAST
            i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\GSCGMBTR",this.TMPMOVIMAST_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
          * --- Drop temporary table TMPMOVIMAST
          i_nIdx=cp_GetTableDefIdx('TMPMOVIMAST')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMPMOVIMAST')
          endif
        else
          * --- Read from BIL_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.BIL_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.BIL_MAST_idx,2],.t.,this.BIL_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "SLSERIAL"+;
              " from "+i_cTable+" BIL_MAST where ";
                  +"SLCODESE = "+cp_ToStrODBC(this.w_ESE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              SLSERIAL;
              from (i_cTable) where;
                  SLCODESE = this.w_ESE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_SLSERIAL = NVL(cp_ToDate(_read_.SLSERIAL),cp_NullValue(_read_.SLSERIAL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        * --- Aggiorno importi  sulla struttura
        * --- Write into TMP_PART
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMP_PART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMP_PART_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="SLFLMACO,SLCODCON,SL__DAVE"
          do vq_exec with 'GSCG3BTR',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_PART_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="TMP_PART.SLFLMACO = _t2.SLFLMACO";
                  +" and "+"TMP_PART.SLCODCON = _t2.SLCODCON";
                  +" and "+"TMP_PART.SL__DAVE = _t2.SL__DAVE";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SLIMPDAR = _t2.SLIMPDAR";
              +",SLIMPAVE = _t2.SLIMPAVE";
              +i_ccchkf;
              +" from "+i_cTable+" TMP_PART, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="TMP_PART.SLFLMACO = _t2.SLFLMACO";
                  +" and "+"TMP_PART.SLCODCON = _t2.SLCODCON";
                  +" and "+"TMP_PART.SL__DAVE = _t2.SL__DAVE";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_PART, "+i_cQueryTable+" _t2 set ";
              +"TMP_PART.SLIMPDAR = _t2.SLIMPDAR";
              +",TMP_PART.SLIMPAVE = _t2.SLIMPAVE";
              +Iif(Empty(i_ccchkf),"",",TMP_PART.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="TMP_PART.SLFLMACO = t2.SLFLMACO";
                  +" and "+"TMP_PART.SLCODCON = t2.SLCODCON";
                  +" and "+"TMP_PART.SL__DAVE = t2.SL__DAVE";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_PART set (";
              +"SLIMPDAR,";
              +"SLIMPAVE";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +"t2.SLIMPDAR,";
              +"t2.SLIMPAVE";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="TMP_PART.SLFLMACO = _t2.SLFLMACO";
                  +" and "+"TMP_PART.SLCODCON = _t2.SLCODCON";
                  +" and "+"TMP_PART.SL__DAVE = _t2.SL__DAVE";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_PART set ";
              +"SLIMPDAR = _t2.SLIMPDAR";
              +",SLIMPAVE = _t2.SLIMPAVE";
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".SLFLMACO = "+i_cQueryTable+".SLFLMACO";
                  +" and "+i_cTable+".SLCODCON = "+i_cQueryTable+".SLCODCON";
                  +" and "+i_cTable+".SL__DAVE = "+i_cQueryTable+".SL__DAVE";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SLIMPDAR = (select SLIMPDAR from "+i_cQueryTable+" where "+i_cWhere+")";
              +",SLIMPAVE = (select SLIMPAVE from "+i_cQueryTable+" where "+i_cWhere+")";
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.w_PADRE.Markpos()     
        * --- Select from TMP_PART
        i_nConn=i_TableProp[this.TMP_PART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMP_PART_idx,2],.t.,this.TMP_PART_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMP_PART ";
               ,"_Curs_TMP_PART")
        else
          select * from (i_cTable);
            into cursor _Curs_TMP_PART
        endif
        if used('_Curs_TMP_PART')
          select _Curs_TMP_PART
          locate for 1=1
          do while not(eof())
          this.w_PADRE.Addrow()     
          this.oParentObject.w_TRFLMACO = _Curs_TMP_PART.SLFLMACO
          this.oParentObject.w_TRCODMAS = _Curs_TMP_PART.SLCODMAS
          this.oParentObject.w_TRCODCON = _Curs_TMP_PART.SLCODCON
          this.oParentObject.w_TR__DAVE = Nvl(_Curs_TMP_PART.SL__DAVE," ")
          this.oParentObject.w_TRIMPDAR = Nvl(_Curs_TMP_PART.SLIMPDAR,0)
          this.oParentObject.w_TRIMPAVE = Nvl(_Curs_TMP_PART.SLIMPAVE,0)
          this.oParentObject.w_TR_SEGNO = Nvl(_Curs_TMP_PART.SL_SEGNO," ")
          this.w_PADRE.SaveRow()     
            select _Curs_TMP_PART
            continue
          enddo
          use
        endif
        this.w_PADRE.Repos()     
        * --- Drop temporary table TMP_PART
        i_nIdx=cp_GetTableDefIdx('TMP_PART')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMP_PART')
        endif
      case this.pEXEC="D" AND this.oParentObject.w_TIPGES="C"
        * --- Azzero  Tabella di Appoggio
        * --- Read from BIL_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.BIL_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.BIL_MAST_idx,2],.t.,this.BIL_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "SLSERIAL"+;
            " from "+i_cTable+" BIL_MAST where ";
                +"SLCODESE = "+cp_ToStrODBC(this.oParentObject.w_CODESE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            SLSERIAL;
            from (i_cTable) where;
                SLCODESE = this.oParentObject.w_CODESE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SLSERIAL = NVL(cp_ToDate(_read_.SLSERIAL),cp_NullValue(_read_.SLSERIAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Aggiornamento importi dare incongruenti con la manutenzione
        * --- Write into BIL_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.BIL_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.BIL_DETT_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="SLSERIAL,SLFLMACO,SLCODCON"
          do vq_exec with 'GSCGDBTR_1',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.BIL_DETT_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="BIL_DETT.SLSERIAL = _t2.SLSERIAL";
                  +" and "+"BIL_DETT.SLFLMACO = _t2.SLFLMACO";
                  +" and "+"BIL_DETT.SLCODCON = _t2.SLCODCON";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SLIMPDAR = _t2.SALDO";
              +i_ccchkf;
              +" from "+i_cTable+" BIL_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="BIL_DETT.SLSERIAL = _t2.SLSERIAL";
                  +" and "+"BIL_DETT.SLFLMACO = _t2.SLFLMACO";
                  +" and "+"BIL_DETT.SLCODCON = _t2.SLCODCON";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" BIL_DETT, "+i_cQueryTable+" _t2 set ";
              +"BIL_DETT.SLIMPDAR = _t2.SALDO";
              +Iif(Empty(i_ccchkf),"",",BIL_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="BIL_DETT.SLSERIAL = t2.SLSERIAL";
                  +" and "+"BIL_DETT.SLFLMACO = t2.SLFLMACO";
                  +" and "+"BIL_DETT.SLCODCON = t2.SLCODCON";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" BIL_DETT set (";
              +"SLIMPDAR";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +"t2.SALDO";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="BIL_DETT.SLSERIAL = _t2.SLSERIAL";
                  +" and "+"BIL_DETT.SLFLMACO = _t2.SLFLMACO";
                  +" and "+"BIL_DETT.SLCODCON = _t2.SLCODCON";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" BIL_DETT set ";
              +"SLIMPDAR = _t2.SALDO";
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".SLSERIAL = "+i_cQueryTable+".SLSERIAL";
                  +" and "+i_cTable+".SLFLMACO = "+i_cQueryTable+".SLFLMACO";
                  +" and "+i_cTable+".SLCODCON = "+i_cQueryTable+".SLCODCON";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SLIMPDAR = (select SALDO from "+i_cQueryTable+" where "+i_cWhere+")";
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Aggiornamento importi avere incongruenti con la manutenzione
        * --- Write into BIL_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.BIL_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.BIL_DETT_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="SLSERIAL,SLFLMACO,SLCODCON"
          do vq_exec with 'GSCGDBTR_2',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.BIL_DETT_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="BIL_DETT.SLSERIAL = _t2.SLSERIAL";
                  +" and "+"BIL_DETT.SLFLMACO = _t2.SLFLMACO";
                  +" and "+"BIL_DETT.SLCODCON = _t2.SLCODCON";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SLIMPAVE = _t2.SALDO";
              +i_ccchkf;
              +" from "+i_cTable+" BIL_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="BIL_DETT.SLSERIAL = _t2.SLSERIAL";
                  +" and "+"BIL_DETT.SLFLMACO = _t2.SLFLMACO";
                  +" and "+"BIL_DETT.SLCODCON = _t2.SLCODCON";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" BIL_DETT, "+i_cQueryTable+" _t2 set ";
              +"BIL_DETT.SLIMPAVE = _t2.SALDO";
              +Iif(Empty(i_ccchkf),"",",BIL_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="BIL_DETT.SLSERIAL = t2.SLSERIAL";
                  +" and "+"BIL_DETT.SLFLMACO = t2.SLFLMACO";
                  +" and "+"BIL_DETT.SLCODCON = t2.SLCODCON";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" BIL_DETT set (";
              +"SLIMPAVE";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +"t2.SALDO";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="BIL_DETT.SLSERIAL = _t2.SLSERIAL";
                  +" and "+"BIL_DETT.SLFLMACO = _t2.SLFLMACO";
                  +" and "+"BIL_DETT.SLCODCON = _t2.SLCODCON";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" BIL_DETT set ";
              +"SLIMPAVE = _t2.SALDO";
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".SLSERIAL = "+i_cQueryTable+".SLSERIAL";
                  +" and "+i_cTable+".SLFLMACO = "+i_cQueryTable+".SLFLMACO";
                  +" and "+i_cTable+".SLCODCON = "+i_cQueryTable+".SLCODCON";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SLIMPAVE = (select SALDO from "+i_cQueryTable+" where "+i_cWhere+")";
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Elimino record a zero dalla manutenzione
        * --- Delete from BIL_DETT
        i_nConn=i_TableProp[this.BIL_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.BIL_DETT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          local i_cQueryTable,i_cWhere
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_cWhere=i_cTable+".SLSERIAL = "+i_cQueryTable+".SLSERIAL";
                +" and "+i_cTable+".SLFLMACO = "+i_cQueryTable+".SLFLMACO";
                +" and "+i_cTable+".SLCODCON = "+i_cQueryTable+".SLCODCON";
        
          do vq_exec with 'GSCGDBTR',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Delete from AGG_SALD
        i_nConn=i_TableProp[this.AGG_SALD_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AGG_SALD_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"1 = "+cp_ToStrODBC(1);
                 )
        else
          delete from (i_cTable) where;
                1 = 1;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Aggiorno descrizioni
        if i_Rows<>0
          * --- Write into BIL_MAST
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.BIL_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.BIL_MAST_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.BIL_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SLDESCRI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DESCRI),'BIL_MAST','SLDESCRI');
            +",SL__NOTE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NOTE),'BIL_MAST','SL__NOTE');
                +i_ccchkf ;
            +" where ";
                +"SLSERIAL = "+cp_ToStrODBC(this.w_SLSERIAL);
                   )
          else
            update (i_cTable) set;
                SLDESCRI = this.oParentObject.w_DESCRI;
                ,SL__NOTE = this.oParentObject.w_NOTE;
                &i_ccchkf. ;
             where;
                SLSERIAL = this.w_SLSERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      case this.pEXEC="T" AND this.oParentObject.w_TIPGES="C"
        this.w_LDESCRI = this.oParentObject.w_DESCRI
        this.w_LNOTE = this.oParentObject.w_NOTE
        * --- Read from BIL_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.BIL_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.BIL_MAST_idx,2],.t.,this.BIL_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "SLSERIAL,SLDESCRI,SL__NOTE"+;
            " from "+i_cTable+" BIL_MAST where ";
                +"SLCODESE = "+cp_ToStrODBC(this.oParentObject.w_CODESE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            SLSERIAL,SLDESCRI,SL__NOTE;
            from (i_cTable) where;
                SLCODESE = this.oParentObject.w_CODESE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SLSERIAL = NVL(cp_ToDate(_read_.SLSERIAL),cp_NullValue(_read_.SLSERIAL))
          this.w_LDESCRI = NVL(cp_ToDate(_read_.SLDESCRI),cp_NullValue(_read_.SLDESCRI))
          this.w_LNOTE = NVL(cp_ToDate(_read_.SL__NOTE),cp_NullValue(_read_.SL__NOTE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_Rows=0
          this.w_SLSERIAL = SPACE(10)
          * --- Try
          local bErr_047C1240
          bErr_047C1240=bTrsErr
          this.Try_047C1240()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
          endif
          bTrsErr=bTrsErr or bErr_047C1240
          * --- End
        else
          this.oParentObject.w_DESCRI = this.w_LDESCRI
          this.oParentObject.w_NOTE = this.w_LNOTE
        endif
      case this.pEXEC="D" AND this.oParentObject.w_TIPGES="A"
        * --- Alla cancellazione del conto/mastro dal dettaglio struttura si cancellano le movimentazione del cont/mastro  associati alla struttura 
        * --- Delete from BIL_DETT
        i_nConn=i_TableProp[this.BIL_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.BIL_DETT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          local i_cQueryTable,i_cWhere
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_cWhere=i_cTable+".SLFLMACO = "+i_cQueryTable+".SLFLMACO";
                +" and "+i_cTable+".SLCODCON = "+i_cQueryTable+".SLCODCON";
        
          do vq_exec with 'GSCGCBTR',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
    endcase
  endproc
  proc Try_047C1240()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_Conn=i_TableProp[this.BIL_MAST_IDX, 3]
    cp_NextTableProg(this, i_Conn, "SEBIL", "i_codazi,w_SLSERIAL")
    * --- Insert into BIL_MAST
    i_nConn=i_TableProp[this.BIL_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.BIL_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.BIL_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLSERIAL"+",SLCODICE"+",SLCODESE"+",UTCC"+",UTDC"+",SLDESCRI"+",SL__NOTE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_SLSERIAL),'BIL_MAST','SLSERIAL');
      +","+cp_NullLink(cp_ToStrODBC("BILANCOGE"),'BIL_MAST','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODESE),'BIL_MAST','SLCODESE');
      +","+cp_NullLink(cp_ToStrODBC(i_codute),'BIL_MAST','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'BIL_MAST','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DESCRI),'BIL_MAST','SLDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NOTE),'BIL_MAST','SL__NOTE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLSERIAL',this.w_SLSERIAL,'SLCODICE',"BILANCOGE",'SLCODESE',this.oParentObject.w_CODESE,'UTCC',i_codute,'UTDC',SetInfoDate( g_CALUTD ),'SLDESCRI',this.oParentObject.w_DESCRI,'SL__NOTE',this.oParentObject.w_NOTE)
      insert into (i_cTable) (SLSERIAL,SLCODICE,SLCODESE,UTCC,UTDC,SLDESCRI,SL__NOTE &i_ccchkf. );
         values (;
           this.w_SLSERIAL;
           ,"BILANCOGE";
           ,this.oParentObject.w_CODESE;
           ,i_codute;
           ,SetInfoDate( g_CALUTD );
           ,this.oParentObject.w_DESCRI;
           ,this.oParentObject.w_NOTE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore nella inserzione testata manutenzione bilancio EU'
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,pEXEC)
    this.pEXEC=pEXEC
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='AGG_SALD'
    this.cWorkTables[2]='BIL_MAST'
    this.cWorkTables[3]='*TMPMOVIMAST'
    this.cWorkTables[4]='*TMP_PART'
    this.cWorkTables[5]='ESERCIZI'
    this.cWorkTables[6]='BIL_DETT'
    return(this.OpenAllTables(6))

  proc CloseCursors()
    if used('_Curs_TMP_PART')
      use in _Curs_TMP_PART
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEXEC"
endproc
