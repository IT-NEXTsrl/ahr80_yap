* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_mtr                                                        *
*              Gestione trascodifiche                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [39][VRS_22]                                                    *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-09-08                                                      *
* Last revis.: 2015-08-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_mtr"))

* --- Class definition
define class tgsar_mtr as StdTrsForm
  Top    = 10
  Left   = 11

  * --- Standard Properties
  Width  = 815
  Height = 346+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-08-24"
  HelpContextID=80312169
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=15

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  TRS_MAST_IDX = 0
  TRS_DETT_IDX = 0
  XDC_TABLE_IDX = 0
  XDC_FIELDS_IDX = 0
  cFile = "TRS_MAST"
  cFileDetail = "TRS_DETT"
  cKeySelect = "TRCODICE"
  cKeyWhere  = "TRCODICE=this.w_TRCODICE"
  cKeyDetail  = "TRCODICE=this.w_TRCODICE"
  cKeyWhereODBC = '"TRCODICE="+cp_ToStrODBC(this.w_TRCODICE)';

  cKeyDetailWhereODBC = '"TRCODICE="+cp_ToStrODBC(this.w_TRCODICE)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"TRS_DETT.TRCODICE="+cp_ToStrODBC(this.w_TRCODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'TRS_DETT.CPROWORD '
  cPrg = "gsar_mtr"
  cComment = "Gestione trascodifiche"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TRCODICE = space(20)
  w_TRDESCRI = space(50)
  w_TRFLALLT = space(1)
  w_TR_TABEL = space(30)
  w_TBCOMMENT = space(254)
  w_TR_CAMPO = space(30)
  o_TR_CAMPO = space(30)
  w_COMMENT = space(80)
  w_CPROWORD = 0
  w_TRCODEXT = space(100)
  w_TRCODINT = space(100)
  w_FLCOMMENT = space(254)
  w_UTCC = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_UTCV = 0
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'TRS_MAST','gsar_mtr')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_mtrPag1","gsar_mtr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Trascodifiche")
      .Pages(1).HelpContextID = 127840255
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTRCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='XDC_TABLE'
    this.cWorkTables[2]='XDC_FIELDS'
    this.cWorkTables[3]='TRS_MAST'
    this.cWorkTables[4]='TRS_DETT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.TRS_MAST_IDX,5],7]
    this.nPostItConn=i_TableProp[this.TRS_MAST_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_TRCODICE = NVL(TRCODICE,space(20))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_5_joined
    link_1_5_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from TRS_MAST where TRCODICE=KeySet.TRCODICE
    *
    i_nConn = i_TableProp[this.TRS_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TRS_MAST_IDX,2],this.bLoadRecFilter,this.TRS_MAST_IDX,"gsar_mtr")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('TRS_MAST')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "TRS_MAST.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"TRS_DETT.","TRS_MAST.")
      i_cTable = i_cTable+' TRS_MAST '
      link_1_5_joined=this.AddJoinedLink_1_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'TRCODICE',this.w_TRCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_TBCOMMENT = space(254)
        .w_COMMENT = space(80)
        .w_FLCOMMENT = space(254)
        .w_TRCODICE = NVL(TRCODICE,space(20))
        .w_TRDESCRI = NVL(TRDESCRI,space(50))
        .w_TRFLALLT = NVL(TRFLALLT,space(1))
        .w_TR_TABEL = NVL(TR_TABEL,space(30))
          if link_1_5_joined
            this.w_TR_TABEL = NVL(TBNAME105,NVL(this.w_TR_TABEL,space(30)))
            this.w_TBCOMMENT = NVL(TBCOMMENT105,space(254))
          else
          .link_1_5('Load')
          endif
        .w_TR_CAMPO = NVL(TR_CAMPO,space(30))
          .link_1_8('Load')
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_UTCC = NVL(UTCC,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_UTCV = NVL(UTCV,0)
        cp_LoadRecExtFlds(this,'TRS_MAST')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from TRS_DETT where TRCODICE=KeySet.TRCODICE
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.TRS_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TRS_DETT_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('TRS_DETT')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "TRS_DETT.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" TRS_DETT"
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'TRCODICE',this.w_TRCODICE  )
        select * from (i_cTable) TRS_DETT where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_CPROWNUM = CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_TRCODEXT = NVL(TRCODEXT,space(100))
          .w_TRCODINT = NVL(TRCODINT,space(100))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_TRCODICE=space(20)
      .w_TRDESCRI=space(50)
      .w_TRFLALLT=space(1)
      .w_TR_TABEL=space(30)
      .w_TBCOMMENT=space(254)
      .w_TR_CAMPO=space(30)
      .w_COMMENT=space(80)
      .w_CPROWORD=10
      .w_TRCODEXT=space(100)
      .w_TRCODINT=space(100)
      .w_FLCOMMENT=space(254)
      .w_UTCC=0
      .w_UTDC=ctot("")
      .w_UTDV=ctot("")
      .w_UTCV=0
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        .w_TRFLALLT = 'T'
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_TR_TABEL))
         .link_1_5('Full')
        endif
        .DoRTCalc(5,6,.f.)
        if not(empty(.w_TR_CAMPO))
         .link_1_8('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'TRS_MAST')
    this.DoRTCalc(7,15,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oTRCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oTRDESCRI_1_2.enabled = i_bVal
      .Page1.oPag.oTRFLALLT_1_3.enabled = i_bVal
      .Page1.oPag.oTR_TABEL_1_5.enabled = i_bVal
      .Page1.oPag.oTR_CAMPO_1_8.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oTRCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oTRCODICE_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'TRS_MAST',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.TRS_MAST_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRCODICE,"TRCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRDESCRI,"TRDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRFLALLT,"TRFLALLT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TR_TABEL,"TR_TABEL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TR_CAMPO,"TR_CAMPO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.TRS_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TRS_MAST_IDX,2])
    i_lTable = "TRS_MAST"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.TRS_MAST_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do gsar_str with this
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_TRCODEXT C(100);
      ,t_TRCODINT C(100);
      ,CPROWNUM N(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsar_mtrbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTRCODEXT_2_2.controlsource=this.cTrsName+'.t_TRCODEXT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTRCODINT_2_3.controlsource=this.cTrsName+'.t_TRCODINT'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(58)
    this.AddVLine(424)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.TRS_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TRS_MAST_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into TRS_MAST
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'TRS_MAST')
        i_extval=cp_InsertValODBCExtFlds(this,'TRS_MAST')
        local i_cFld
        i_cFld=" "+;
                  "(TRCODICE,TRDESCRI,TRFLALLT,TR_TABEL,TR_CAMPO"+;
                  ",UTCC,UTDC,UTDV,UTCV"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_TRCODICE)+;
                    ","+cp_ToStrODBC(this.w_TRDESCRI)+;
                    ","+cp_ToStrODBC(this.w_TRFLALLT)+;
                    ","+cp_ToStrODBCNull(this.w_TR_TABEL)+;
                    ","+cp_ToStrODBCNull(this.w_TR_CAMPO)+;
                    ","+cp_ToStrODBC(this.w_UTCC)+;
                    ","+cp_ToStrODBC(this.w_UTDC)+;
                    ","+cp_ToStrODBC(this.w_UTDV)+;
                    ","+cp_ToStrODBC(this.w_UTCV)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'TRS_MAST')
        i_extval=cp_InsertValVFPExtFlds(this,'TRS_MAST')
        cp_CheckDeletedKey(i_cTable,0,'TRCODICE',this.w_TRCODICE)
        INSERT INTO (i_cTable);
              (TRCODICE,TRDESCRI,TRFLALLT,TR_TABEL,TR_CAMPO,UTCC,UTDC,UTDV,UTCV &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_TRCODICE;
                  ,this.w_TRDESCRI;
                  ,this.w_TRFLALLT;
                  ,this.w_TR_TABEL;
                  ,this.w_TR_CAMPO;
                  ,this.w_UTCC;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_UTCV;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.TRS_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TRS_DETT_IDX,2])
      *
      * insert into TRS_DETT
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(TRCODICE,CPROWORD,TRCODEXT,TRCODINT,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_TRCODICE)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_TRCODEXT)+","+cp_ToStrODBC(this.w_TRCODINT)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'TRCODICE',this.w_TRCODICE)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_TRCODICE,this.w_CPROWORD,this.w_TRCODEXT,this.w_TRCODINT,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.TRS_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TRS_MAST_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update TRS_MAST
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'TRS_MAST')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " TRDESCRI="+cp_ToStrODBC(this.w_TRDESCRI)+;
             ",TRFLALLT="+cp_ToStrODBC(this.w_TRFLALLT)+;
             ",TR_TABEL="+cp_ToStrODBCNull(this.w_TR_TABEL)+;
             ",TR_CAMPO="+cp_ToStrODBCNull(this.w_TR_CAMPO)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'TRS_MAST')
          i_cWhere = cp_PKFox(i_cTable  ,'TRCODICE',this.w_TRCODICE  )
          UPDATE (i_cTable) SET;
              TRDESCRI=this.w_TRDESCRI;
             ,TRFLALLT=this.w_TRFLALLT;
             ,TR_TABEL=this.w_TR_TABEL;
             ,TR_CAMPO=this.w_TR_CAMPO;
             ,UTCC=this.w_UTCC;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,UTCV=this.w_UTCV;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_TRCODINT))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.TRS_DETT_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.TRS_DETT_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from TRS_DETT
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update TRS_DETT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",TRCODEXT="+cp_ToStrODBC(this.w_TRCODEXT)+;
                     ",TRCODINT="+cp_ToStrODBC(this.w_TRCODINT)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,TRCODEXT=this.w_TRCODEXT;
                     ,TRCODINT=this.w_TRCODINT;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_TRCODINT))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.TRS_DETT_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.TRS_DETT_IDX,2])
        *
        * delete TRS_DETT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.TRS_MAST_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.TRS_MAST_IDX,2])
        *
        * delete TRS_MAST
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_TRCODINT))) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.TRS_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TRS_MAST_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,15,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCOMMENT_1_9.visible=!this.oPgFrm.Page1.oPag.oCOMMENT_1_9.mHide()
    this.oPgFrm.Page1.oPag.oFLCOMMENT_1_11.visible=!this.oPgFrm.Page1.oPag.oFLCOMMENT_1_11.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsar_mtr
    IF (CEVENT='w_TR_CAMPO Changed' OR CEVENT='Load') AND  Empty(This.w_COMMENT) and g_VEFA='S'
        Local cursore,Lobj
        cursore=readtable('FLATDETT','FTFLDDES',' ',null,.f.,"  FTTABNAM = " + cp_ToStr( This.w_TR_TABEL) +  " AND FTFLDNAM = " + cp_ToStr( This.w_TR_CAMPO))
       if  Reccount(cursore)>0
        Select ((cursore))
         This.w_FLCOMMENT=FTFLDDES
         Lobj=This.getctrl('w_FLCOMMENT')
         Lobj.value=FTFLDDES
         Lobj=null
        else
         This.w_FLCOMMENT=''
       endif
       if used((cursore))
         Select ((cursore))
         Use
       Endif
    Endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=TR_TABEL
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.XDC_TABLE_IDX,3]
    i_lTable = "XDC_TABLE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2], .t., this.XDC_TABLE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TR_TABEL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'XDC_TABLE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TBNAME like "+cp_ToStrODBC(trim(this.w_TR_TABEL)+"%");

          i_ret=cp_SQL(i_nConn,"select TBNAME,TBCOMMENT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TBNAME","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TBNAME',trim(this.w_TR_TABEL))
          select TBNAME,TBCOMMENT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TBNAME into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TR_TABEL)==trim(_Link_.TBNAME) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TR_TABEL) and !this.bDontReportError
            deferred_cp_zoom('XDC_TABLE','*','TBNAME',cp_AbsName(oSource.parent,'oTR_TABEL_1_5'),i_cWhere,'',"Elenco tabelle",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,TBCOMMENT";
                     +" from "+i_cTable+" "+i_lTable+" where TBNAME="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',oSource.xKey(1))
            select TBNAME,TBCOMMENT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TR_TABEL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,TBCOMMENT";
                   +" from "+i_cTable+" "+i_lTable+" where TBNAME="+cp_ToStrODBC(this.w_TR_TABEL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',this.w_TR_TABEL)
            select TBNAME,TBCOMMENT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TR_TABEL = NVL(_Link_.TBNAME,space(30))
      this.w_TBCOMMENT = NVL(_Link_.TBCOMMENT,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_TR_TABEL = space(30)
      endif
      this.w_TBCOMMENT = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])+'\'+cp_ToStr(_Link_.TBNAME,1)
      cp_ShowWarn(i_cKey,this.XDC_TABLE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TR_TABEL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.XDC_TABLE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_5.TBNAME as TBNAME105"+ ",link_1_5.TBCOMMENT as TBCOMMENT105"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_5 on TRS_MAST.TR_TABEL=link_1_5.TBNAME"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_5"
          i_cKey=i_cKey+'+" and TRS_MAST.TR_TABEL=link_1_5.TBNAME(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=TR_CAMPO
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.XDC_FIELDS_IDX,3]
    i_lTable = "XDC_FIELDS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2], .t., this.XDC_FIELDS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TR_CAMPO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'XDC_FIELDS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FLNAME like "+cp_ToStrODBC(trim(this.w_TR_CAMPO)+"%");
                   +" and TBNAME="+cp_ToStrODBC(this.w_TR_TABEL);

          i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLCOMMEN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TBNAME,FLNAME","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TBNAME',this.w_TR_TABEL;
                     ,'FLNAME',trim(this.w_TR_CAMPO))
          select TBNAME,FLNAME,FLCOMMEN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TBNAME,FLNAME into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TR_CAMPO)==trim(_Link_.FLNAME) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TR_CAMPO) and !this.bDontReportError
            deferred_cp_zoom('XDC_FIELDS','*','TBNAME,FLNAME',cp_AbsName(oSource.parent,'oTR_CAMPO_1_8'),i_cWhere,'',"Elenco campi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TR_TABEL<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLCOMMEN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TBNAME,FLNAME,FLCOMMEN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLCOMMEN";
                     +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TBNAME="+cp_ToStrODBC(this.w_TR_TABEL);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',oSource.xKey(1);
                       ,'FLNAME',oSource.xKey(2))
            select TBNAME,FLNAME,FLCOMMEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TR_CAMPO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLCOMMEN";
                   +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(this.w_TR_CAMPO);
                   +" and TBNAME="+cp_ToStrODBC(this.w_TR_TABEL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',this.w_TR_TABEL;
                       ,'FLNAME',this.w_TR_CAMPO)
            select TBNAME,FLNAME,FLCOMMEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    local i_bTmp
    i_bTmp=.t.
    if i_reccount>0 and used("_Link_")
      this.w_TR_CAMPO = NVL(_Link_.FLNAME,space(30))
      this.w_COMMENT = NVL(_Link_.FLCOMMEN,space(80))
    else
      this.w_COMMENT = space(80)
    endif
    i_bRes=i_reccount=1  or i_bTmp
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])+'\'+cp_ToStr(_Link_.TBNAME,1)+'\'+cp_ToStr(_Link_.FLNAME,1)
      cp_ShowWarn(i_cKey,this.XDC_FIELDS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TR_CAMPO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oTRCODICE_1_1.value==this.w_TRCODICE)
      this.oPgFrm.Page1.oPag.oTRCODICE_1_1.value=this.w_TRCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oTRDESCRI_1_2.value==this.w_TRDESCRI)
      this.oPgFrm.Page1.oPag.oTRDESCRI_1_2.value=this.w_TRDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oTRFLALLT_1_3.RadioValue()==this.w_TRFLALLT)
      this.oPgFrm.Page1.oPag.oTRFLALLT_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTR_TABEL_1_5.value==this.w_TR_TABEL)
      this.oPgFrm.Page1.oPag.oTR_TABEL_1_5.value=this.w_TR_TABEL
    endif
    if not(this.oPgFrm.Page1.oPag.oTBCOMMENT_1_6.value==this.w_TBCOMMENT)
      this.oPgFrm.Page1.oPag.oTBCOMMENT_1_6.value=this.w_TBCOMMENT
    endif
    if not(this.oPgFrm.Page1.oPag.oTR_CAMPO_1_8.value==this.w_TR_CAMPO)
      this.oPgFrm.Page1.oPag.oTR_CAMPO_1_8.value=this.w_TR_CAMPO
    endif
    if not(this.oPgFrm.Page1.oPag.oCOMMENT_1_9.value==this.w_COMMENT)
      this.oPgFrm.Page1.oPag.oCOMMENT_1_9.value=this.w_COMMENT
    endif
    if not(this.oPgFrm.Page1.oPag.oFLCOMMENT_1_11.value==this.w_FLCOMMENT)
      this.oPgFrm.Page1.oPag.oFLCOMMENT_1_11.value=this.w_FLCOMMENT
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCODEXT_2_2.value==this.w_TRCODEXT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCODEXT_2_2.value=this.w_TRCODEXT
      replace t_TRCODEXT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCODEXT_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCODINT_2_3.value==this.w_TRCODINT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCODINT_2_3.value=this.w_TRCODINT
      replace t_TRCODINT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCODINT_2_3.value
    endif
    cp_SetControlsValueExtFlds(this,'TRS_MAST')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_TRCODICE))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oTRCODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_TRCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_TRCODINT))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TR_CAMPO = this.w_TR_CAMPO
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_TRCODINT)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_TRCODEXT=space(100)
      .w_TRCODINT=space(100)
    endwith
    this.DoRTCalc(1,15,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_TRCODEXT = t_TRCODEXT
    this.w_TRCODINT = t_TRCODINT
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_TRCODEXT with this.w_TRCODEXT
    replace t_TRCODINT with this.w_TRCODINT
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsar_mtrPag1 as StdContainer
  Width  = 811
  height = 346
  stdWidth  = 811
  stdheight = 346
  resizeXpos=442
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTRCODICE_1_1 as StdField with uid="GCVEGVZYDE",rtseq=1,rtrep=.f.,;
    cFormVar = "w_TRCODICE", cQueryName = "TRCODICE",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice trascodifica",;
    HelpContextID = 147460219,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=202, Left=129, Top=14, InputMask=replicate('X',20)

  add object oTRDESCRI_1_2 as StdField with uid="PXSYUHSWRP",rtseq=2,rtrep=.f.,;
    cFormVar = "w_TRDESCRI", cQueryName = "TRDESCRI",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 61874303,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=129, Top=40, InputMask=replicate('X',50)


  add object oTRFLALLT_1_3 as StdCombo with uid="ZZKYXINRIN",rtseq=3,rtrep=.f.,left=644,top=40,width=158,height=21;
    , ToolTipText = "Tipo applicazione alltrim";
    , HelpContextID = 73973622;
    , cFormVar="w_TRFLALLT",RowSource=""+"Codice origine,"+"Codice destinazione,"+"Entrambi,"+"Nessuno", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTRFLALLT_1_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..TRFLALLT,&i_cF..t_TRFLALLT),this.value)
    return(iif(xVal =1,'I',;
    iif(xVal =2,'E',;
    iif(xVal =3,'T',;
    iif(xVal =4,'N',;
    space(1))))))
  endfunc
  func oTRFLALLT_1_3.GetRadio()
    this.Parent.oContained.w_TRFLALLT = this.RadioValue()
    return .t.
  endfunc

  func oTRFLALLT_1_3.ToRadio()
    this.Parent.oContained.w_TRFLALLT=trim(this.Parent.oContained.w_TRFLALLT)
    return(;
      iif(this.Parent.oContained.w_TRFLALLT=='I',1,;
      iif(this.Parent.oContained.w_TRFLALLT=='E',2,;
      iif(this.Parent.oContained.w_TRFLALLT=='T',3,;
      iif(this.Parent.oContained.w_TRFLALLT=='N',4,;
      0)))))
  endfunc

  func oTRFLALLT_1_3.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oTR_TABEL_1_5 as StdField with uid="PFRISDUWER",rtseq=4,rtrep=.f.,;
    cFormVar = "w_TR_TABEL", cQueryName = "TR_TABEL",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Tabella per trascodifica",;
    HelpContextID = 27316354,;
   bGlobalFont=.t.,;
    Height=21, Width=237, Left=129, Top=66, InputMask=replicate('X',30), bHasZoom = .t. , cLinkFile="XDC_TABLE", oKey_1_1="TBNAME", oKey_1_2="this.w_TR_TABEL"

  func oTR_TABEL_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
      if .not. empty(.w_TR_CAMPO)
        bRes2=.link_1_8('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oTR_TABEL_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTR_TABEL_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'XDC_TABLE','*','TBNAME',cp_AbsName(this.parent,'oTR_TABEL_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco tabelle",'',this.parent.oContained
  endproc

  add object oTBCOMMENT_1_6 as StdField with uid="YFEGBVEEXM",rtseq=5,rtrep=.f.,;
    cFormVar = "w_TBCOMMENT", cQueryName = "TBCOMMENT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 224003524,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=369, Top=66, InputMask=replicate('X',254)

  add object oTR_CAMPO_1_8 as StdField with uid="SCEDXZCUOD",rtseq=6,rtrep=.f.,;
    cFormVar = "w_TR_CAMPO", cQueryName = "TR_CAMPO",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Campo per trascodfica",;
    HelpContextID = 57683835,;
   bGlobalFont=.t.,;
    Height=21, Width=237, Left=129, Top=92, InputMask=replicate('X',30), bHasZoom = .t. , cLinkFile="XDC_FIELDS", oKey_1_1="TBNAME", oKey_1_2="this.w_TR_TABEL", oKey_2_1="FLNAME", oKey_2_2="this.w_TR_CAMPO"

  func oTR_CAMPO_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oTR_CAMPO_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTR_CAMPO_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.XDC_FIELDS_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TBNAME="+cp_ToStrODBC(this.Parent.oContained.w_TR_TABEL)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TBNAME="+cp_ToStr(this.Parent.oContained.w_TR_TABEL)
    endif
    do cp_zoom with 'XDC_FIELDS','*','TBNAME,FLNAME',cp_AbsName(this.parent,'oTR_CAMPO_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco campi",'',this.parent.oContained
  endproc

  add object oCOMMENT_1_9 as StdField with uid="FPNMQJEDGD",rtseq=7,rtrep=.f.,;
    cFormVar = "w_COMMENT", cQueryName = "COMMENT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(80), bMultilanguage =  .f.,;
    HelpContextID = 232303654,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=368, Top=92, InputMask=replicate('X',80)

  func oCOMMENT_1_9.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(NVL(.w_COMMENT,'')))
    endwith
    endif
  endfunc

  add object oFLCOMMENT_1_11 as StdField with uid="QVKFFKMTIM",rtseq=11,rtrep=.f.,;
    cFormVar = "w_FLCOMMENT", cQueryName = "FLCOMMENT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 224005860,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=369, Top=92, InputMask=replicate('X',254)

  func oFLCOMMENT_1_11.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not EMPTY(NVL(.w_COMMENT,'')))
    endwith
    endif
  endfunc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=7, top=126, width=799,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=3,Field1="CPROWORD",Label1="Riga",Field2="TRCODEXT",Label2="Codice origine",Field3="TRCODINT",Label3="Codice destinazione",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 235778682

  add object oStr_1_4 as StdString with uid="GBMHKXANNT",Visible=.t., Left=4, Top=15,;
    Alignment=1, Width=123, Height=19,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_7 as StdString with uid="ENIXLETWQA",Visible=.t., Left=4, Top=68,;
    Alignment=1, Width=123, Height=19,;
    Caption="Tabella:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="DOEHATDEUD",Visible=.t., Left=4, Top=94,;
    Alignment=1, Width=123, Height=19,;
    Caption="Campo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="YQZYJPSTZS",Visible=.t., Left=498, Top=42,;
    Alignment=1, Width=142, Height=18,;
    Caption="Elimina spazi su:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="ZTDFIODBYB",Visible=.t., Left=4, Top=42,;
    Alignment=1, Width=123, Height=19,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-1,top=145,;
    width=793+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=0,top=146,width=792+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsar_mtrBodyRow as CPBodyRowCnt
  Width=783
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="JPKGCMJSZP",rtseq=8,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    ToolTipText = "Numero riga",;
    HelpContextID = 268107114,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  add object oTRCODEXT_2_2 as StdTrsField with uid="UQIXRNBPEG",rtseq=9,rtrep=.t.,;
    cFormVar="w_TRCODEXT",value=space(100),;
    ToolTipText = "Valore di origine",;
    HelpContextID = 80351370,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=363, Left=49, Top=0, InputMask=replicate('X',100)

  add object oTRCODINT_2_3 as StdTrsField with uid="ENLVHFTDAX",rtseq=10,rtrep=.t.,;
    cFormVar="w_TRCODINT",value=space(100),;
    ToolTipText = "Valore/espressione di destinazione",;
    HelpContextID = 120975222,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=363, Left=415, Top=0, InputMask=replicate('X',100)
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_mtr','TRS_MAST','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".TRCODICE=TRS_MAST.TRCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
