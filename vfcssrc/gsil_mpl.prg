* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsil_mpl                                                        *
*              Parametri listini                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_116]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-07                                                      *
* Last revis.: 2011-11-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsil_mpl"))

* --- Class definition
define class tgsil_mpl as StdTrsForm
  Top    = 5
  Left   = 8

  * --- Standard Properties
  Width  = 829
  Height = 294+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-11-22"
  HelpContextID=120991895
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  PARMLIST_IDX = 0
  PARALIST_IDX = 0
  STRUTABE_IDX = 0
  CATMLIST_IDX = 0
  XDC_TABLE_IDX = 0
  XDC_FIELDS_IDX = 0
  cFile = "PARMLIST"
  cFileDetail = "PARALIST"
  cKeySelect = "PLCODCAT"
  cKeyWhere  = "PLCODCAT=this.w_PLCODCAT"
  cKeyDetail  = "PLCODCAT=this.w_PLCODCAT"
  cKeyWhereODBC = '"PLCODCAT="+cp_ToStrODBC(this.w_PLCODCAT)';

  cKeyDetailWhereODBC = '"PLCODCAT="+cp_ToStrODBC(this.w_PLCODCAT)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"PARALIST.PLCODCAT="+cp_ToStrODBC(this.w_PLCODCAT)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'PARALIST.PLNOMTAB,PARALIST.PLNOMCAM'
  cPrg = "gsil_mpl"
  cComment = "Parametri listini"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PLCODCAT = space(15)
  w_PL__PATH = space(100)
  w_PLNOMTAB = space(8)
  w_PLNOMCAM = space(8)
  w_PLFLTRAS = space(1)
  w_PLVALFIS = space(20)

  * --- Children pointers
  GSIL_MPT = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'PARMLIST','gsil_mpl')
    stdPageFrame::Init()
    *set procedure to GSIL_MPT additive
    with this
      .Pages(1).addobject("oPag","tgsil_mplPag1","gsil_mpl",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Parametri listini")
      .Pages(1).HelpContextID = 15817482
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPLCODCAT_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSIL_MPT
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='STRUTABE'
    this.cWorkTables[2]='CATMLIST'
    this.cWorkTables[3]='XDC_TABLE'
    this.cWorkTables[4]='XDC_FIELDS'
    this.cWorkTables[5]='PARMLIST'
    this.cWorkTables[6]='PARALIST'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(6))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PARMLIST_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PARMLIST_IDX,3]
  return

  function CreateChildren()
    this.GSIL_MPT = CREATEOBJECT('stdDynamicChild',this,'GSIL_MPT',this.oPgFrm.Page1.oPag.oLinkPC_2_5)
    this.GSIL_MPT.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSIL_MPT)
      this.GSIL_MPT.DestroyChildrenChain()
      this.GSIL_MPT=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_2_5')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSIL_MPT.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSIL_MPT.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSIL_MPT.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .GSIL_MPT.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_PLCODCAT,"PTCODCAT";
             ,.w_CPROWNUM,"PTROWNUM";
             )
    endwith
    select (i_cOldSel)
    return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_PLCODCAT = NVL(PLCODCAT,space(15))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from PARMLIST where PLCODCAT=KeySet.PLCODCAT
    *
    i_nConn = i_TableProp[this.PARMLIST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PARMLIST_IDX,2],this.bLoadRecFilter,this.PARMLIST_IDX,"gsil_mpl")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PARMLIST')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PARMLIST.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"PARALIST.","PARMLIST.")
      i_cTable = i_cTable+' PARMLIST '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PLCODCAT',this.w_PLCODCAT  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_PLCODCAT = NVL(PLCODCAT,space(15))
          * evitabile
          *.link_1_2('Load')
        .w_PL__PATH = NVL(PL__PATH,space(100))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'PARMLIST')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from PARALIST where PLCODCAT=KeySet.PLCODCAT
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.PARALIST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PARALIST_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('PARALIST')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "PARALIST.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" PARALIST"
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'PLCODCAT',this.w_PLCODCAT  )
        select * from (i_cTable) PARALIST where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_CPROWNUM = CPROWNUM
          .w_PLNOMTAB = NVL(PLNOMTAB,space(8))
          * evitabile
          *.link_2_1('Load')
          .w_PLNOMCAM = NVL(PLNOMCAM,space(8))
          * evitabile
          *.link_2_2('Load')
          .w_PLFLTRAS = NVL(PLFLTRAS,space(1))
          .w_PLVALFIS = NVL(PLVALFIS,space(20))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_PLCODCAT=space(15)
      .w_PL__PATH=space(100)
      .w_PLNOMTAB=space(8)
      .w_PLNOMCAM=space(8)
      .w_PLFLTRAS=space(1)
      .w_PLVALFIS=space(20)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_PLCODCAT))
         .link_1_2('Full')
        endif
        .DoRTCalc(2,3,.f.)
        if not(empty(.w_PLNOMTAB))
         .link_2_1('Full')
        endif
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_PLNOMCAM))
         .link_2_2('Full')
        endif
        .w_PLFLTRAS = 'N'
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'PARMLIST')
    this.DoRTCalc(6,6,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oPLCODCAT_1_2.enabled = i_bVal
      .Page1.oPag.oPL__PATH_1_4.enabled = i_bVal
      .Page1.oPag.oBtn_1_5.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oPLCODCAT_1_2.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oPLCODCAT_1_2.enabled = .t.
      endif
    endwith
    this.GSIL_MPT.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'PARMLIST',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSIL_MPT.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PARMLIST_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLCODCAT,"PLCODCAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PL__PATH,"PL__PATH",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PARMLIST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PARMLIST_IDX,2])
    i_lTable = "PARMLIST"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.PARMLIST_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_PLNOMTAB C(8);
      ,t_PLNOMCAM C(8);
      ,t_PLFLTRAS N(3);
      ,t_PLVALFIS C(20);
      ,CPROWNUM N(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsil_mplbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPLNOMTAB_2_1.controlsource=this.cTrsName+'.t_PLNOMTAB'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPLNOMCAM_2_2.controlsource=this.cTrsName+'.t_PLNOMCAM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPLFLTRAS_2_3.controlsource=this.cTrsName+'.t_PLFLTRAS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPLVALFIS_2_4.controlsource=this.cTrsName+'.t_PLVALFIS'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(101)
    this.AddVLine(196)
    this.AddVLine(297)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPLNOMTAB_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PARMLIST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PARMLIST_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into PARMLIST
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PARMLIST')
        i_extval=cp_InsertValODBCExtFlds(this,'PARMLIST')
        local i_cFld
        i_cFld=" "+;
                  "(PLCODCAT,PL__PATH"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBCNull(this.w_PLCODCAT)+;
                    ","+cp_ToStrODBC(this.w_PL__PATH)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PARMLIST')
        i_extval=cp_InsertValVFPExtFlds(this,'PARMLIST')
        cp_CheckDeletedKey(i_cTable,0,'PLCODCAT',this.w_PLCODCAT)
        INSERT INTO (i_cTable);
              (PLCODCAT,PL__PATH &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_PLCODCAT;
                  ,this.w_PL__PATH;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PARALIST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PARALIST_IDX,2])
      *
      * insert into PARALIST
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(PLCODCAT,PLNOMTAB,PLNOMCAM,PLFLTRAS,PLVALFIS,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBCNull(this.w_PLCODCAT)+","+cp_ToStrODBCNull(this.w_PLNOMTAB)+","+cp_ToStrODBCNull(this.w_PLNOMCAM)+","+cp_ToStrODBC(this.w_PLFLTRAS)+","+cp_ToStrODBC(this.w_PLVALFIS)+;
             ","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'PLCODCAT',this.w_PLCODCAT)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_PLCODCAT,this.w_PLNOMTAB,this.w_PLNOMCAM,this.w_PLFLTRAS,this.w_PLVALFIS,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.PARMLIST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PARMLIST_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update PARMLIST
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'PARMLIST')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " PL__PATH="+cp_ToStrODBC(this.w_PL__PATH)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'PARMLIST')
          i_cWhere = cp_PKFox(i_cTable  ,'PLCODCAT',this.w_PLCODCAT  )
          UPDATE (i_cTable) SET;
              PL__PATH=this.w_PL__PATH;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (NOT EMPTY(t_PLNOMTAB) AND NOT EMPTY(t_PLNOMCAM) AND NOT EMPTY(t_PLFLTRAS)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.PARALIST_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.PARALIST_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Deleting row children
              this.GSIL_MPT.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_PLCODCAT,"PTCODCAT";
                     ,this.w_CPROWNUM,"PTROWNUM";
                     )
              this.GSIL_MPT.mDelete()
              *
              * delete from PARALIST
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update PARALIST
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " PLNOMTAB="+cp_ToStrODBCNull(this.w_PLNOMTAB)+;
                     ",PLNOMCAM="+cp_ToStrODBCNull(this.w_PLNOMCAM)+;
                     ",PLFLTRAS="+cp_ToStrODBC(this.w_PLFLTRAS)+;
                     ",PLVALFIS="+cp_ToStrODBC(this.w_PLVALFIS)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      PLNOMTAB=this.w_PLNOMTAB;
                     ,PLNOMCAM=this.w_PLNOMCAM;
                     ,PLFLTRAS=this.w_PLFLTRAS;
                     ,PLVALFIS=this.w_PLVALFIS;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask children belonging to rows to save themselves
      select (this.cTrsName)
      i_TN = this.cTrsName
      scan for (NOT EMPTY(t_PLNOMTAB) AND NOT EMPTY(t_PLNOMCAM) AND NOT EMPTY(t_PLFLTRAS))
        * --- > Optimize children saving
        i_nRec = recno()
        this.WorkFromTrs()
        this.GSIL_MPT.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
             ,this.w_PLCODCAT,"PTCODCAT";
             ,this.w_CPROWNUM,"PTROWNUM";
             )
        this.GSIL_MPT.mReplace()
        this.GSIL_MPT.bSaveContext=.f.
      endscan
     this.GSIL_MPT.bSaveContext=.t.
      * --- Make the last record the actual position (mcalc problem with double transitory)2
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (NOT EMPTY(t_PLNOMTAB) AND NOT EMPTY(t_PLNOMCAM) AND NOT EMPTY(t_PLFLTRAS)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_nRec = recno()
        * --- GSIL_MPT : Deleting
        this.GSIL_MPT.bSaveContext=.f.
        this.GSIL_MPT.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_PLCODCAT,"PTCODCAT";
               ,this.w_CPROWNUM,"PTROWNUM";
               )
        this.GSIL_MPT.bSaveContext=.t.
        this.GSIL_MPT.mDelete()
        if bTrsErr
          i_nModRow = -1
          exit
        endif
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.PARALIST_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.PARALIST_IDX,2])
        *
        * delete PARALIST
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.PARMLIST_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.PARMLIST_IDX,2])
        *
        * delete PARMLIST
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (NOT EMPTY(t_PLNOMTAB) AND NOT EMPTY(t_PLNOMCAM) AND NOT EMPTY(t_PLFLTRAS)) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PARMLIST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PARMLIST_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,6,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPLVALFIS_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPLVALFIS_2_4.mCond()
    this.GSIL_MPT.enabled = this.oPgFrm.Page1.oPag.oLinkPC_2_5.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PLCODCAT
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATMLIST_IDX,3]
    i_lTable = "CATMLIST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATMLIST_IDX,2], .t., this.CATMLIST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATMLIST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PLCODCAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsil_mcl',True,'CATMLIST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODCAT like "+cp_ToStrODBC(trim(this.w_PLCODCAT)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODCAT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODCAT',trim(this.w_PLCODCAT))
          select CTCODCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODCAT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PLCODCAT)==trim(_Link_.CTCODCAT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PLCODCAT) and !this.bDontReportError
            deferred_cp_zoom('CATMLIST','*','CTCODCAT',cp_AbsName(oSource.parent,'oPLCODCAT_1_2'),i_cWhere,'gsil_mcl',"Categorie listini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODCAT="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODCAT',oSource.xKey(1))
            select CTCODCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PLCODCAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODCAT="+cp_ToStrODBC(this.w_PLCODCAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODCAT',this.w_PLCODCAT)
            select CTCODCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PLCODCAT = NVL(_Link_.CTCODCAT,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_PLCODCAT = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATMLIST_IDX,2])+'\'+cp_ToStr(_Link_.CTCODCAT,1)
      cp_ShowWarn(i_cKey,this.CATMLIST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PLCODCAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PLNOMTAB
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STRUTABE_IDX,3]
    i_lTable = "STRUTABE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STRUTABE_IDX,2], .t., this.STRUTABE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STRUTABE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PLNOMTAB) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSIL_MSD',True,'STRUTABE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" STNOMTAB like "+cp_ToStrODBC(trim(this.w_PLNOMTAB)+"%");

          i_ret=cp_SQL(i_nConn,"select STNOMTAB";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by STNOMTAB","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'STNOMTAB',trim(this.w_PLNOMTAB))
          select STNOMTAB;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by STNOMTAB into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PLNOMTAB)==trim(_Link_.STNOMTAB) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PLNOMTAB) and !this.bDontReportError
            deferred_cp_zoom('STRUTABE','*','STNOMTAB',cp_AbsName(oSource.parent,'oPLNOMTAB_2_1'),i_cWhere,'GSIL_MSD',"Nome tabella",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STNOMTAB";
                     +" from "+i_cTable+" "+i_lTable+" where STNOMTAB="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'STNOMTAB',oSource.xKey(1))
            select STNOMTAB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PLNOMTAB)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STNOMTAB";
                   +" from "+i_cTable+" "+i_lTable+" where STNOMTAB="+cp_ToStrODBC(this.w_PLNOMTAB);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'STNOMTAB',this.w_PLNOMTAB)
            select STNOMTAB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PLNOMTAB = NVL(_Link_.STNOMTAB,space(8))
    else
      if i_cCtrl<>'Load'
        this.w_PLNOMTAB = space(8)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PLNOMTAB<>'LIS_TINI'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Impossibile associare la tabella lis_tini o nome della tabella non valido")
        endif
        this.w_PLNOMTAB = space(8)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STRUTABE_IDX,2])+'\'+cp_ToStr(_Link_.STNOMTAB,1)
      cp_ShowWarn(i_cKey,this.STRUTABE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PLNOMTAB Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PLNOMCAM
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STRUTABE_IDX,3]
    i_lTable = "STRUTABE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STRUTABE_IDX,2], .t., this.STRUTABE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STRUTABE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PLNOMCAM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSIL_MSD',True,'STRUTABE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" STNOMCAM like "+cp_ToStrODBC(trim(this.w_PLNOMCAM)+"%");
                   +" and STNOMTAB="+cp_ToStrODBC(this.w_PLNOMTAB);

          i_ret=cp_SQL(i_nConn,"select STNOMTAB,STNOMCAM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by STNOMTAB,STNOMCAM","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'STNOMTAB',this.w_PLNOMTAB;
                     ,'STNOMCAM',trim(this.w_PLNOMCAM))
          select STNOMTAB,STNOMCAM;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by STNOMTAB,STNOMCAM into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PLNOMCAM)==trim(_Link_.STNOMCAM) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PLNOMCAM) and !this.bDontReportError
            deferred_cp_zoom('STRUTABE','*','STNOMTAB,STNOMCAM',cp_AbsName(oSource.parent,'oPLNOMCAM_2_2'),i_cWhere,'GSIL_MSD',"Nome campo",'GSIL1ZNC.STRUTABE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_PLNOMTAB<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STNOMTAB,STNOMCAM";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select STNOMTAB,STNOMCAM;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Manca il nome campo")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STNOMTAB,STNOMCAM";
                     +" from "+i_cTable+" "+i_lTable+" where STNOMCAM="+cp_ToStrODBC(oSource.xKey(2));
                     +" and STNOMTAB="+cp_ToStrODBC(this.w_PLNOMTAB);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'STNOMTAB',oSource.xKey(1);
                       ,'STNOMCAM',oSource.xKey(2))
            select STNOMTAB,STNOMCAM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PLNOMCAM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STNOMTAB,STNOMCAM";
                   +" from "+i_cTable+" "+i_lTable+" where STNOMCAM="+cp_ToStrODBC(this.w_PLNOMCAM);
                   +" and STNOMTAB="+cp_ToStrODBC(this.w_PLNOMTAB);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'STNOMTAB',this.w_PLNOMTAB;
                       ,'STNOMCAM',this.w_PLNOMCAM)
            select STNOMTAB,STNOMCAM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PLNOMCAM = NVL(_Link_.STNOMCAM,space(8))
    else
      if i_cCtrl<>'Load'
        this.w_PLNOMCAM = space(8)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PLNOMCAM<>'UTCC' AND .w_PLNOMCAM<>'UTCV' AND .w_PLNOMCAM<>'UTDC' AND .w_PLNOMCAM<>'UTDV'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Manca il nome campo")
        endif
        this.w_PLNOMCAM = space(8)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STRUTABE_IDX,2])+'\'+cp_ToStr(_Link_.STNOMTAB,1)+'\'+cp_ToStr(_Link_.STNOMCAM,1)
      cp_ShowWarn(i_cKey,this.STRUTABE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PLNOMCAM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oPLCODCAT_1_2.value==this.w_PLCODCAT)
      this.oPgFrm.Page1.oPag.oPLCODCAT_1_2.value=this.w_PLCODCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oPL__PATH_1_4.value==this.w_PL__PATH)
      this.oPgFrm.Page1.oPag.oPL__PATH_1_4.value=this.w_PL__PATH
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPLNOMTAB_2_1.value==this.w_PLNOMTAB)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPLNOMTAB_2_1.value=this.w_PLNOMTAB
      replace t_PLNOMTAB with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPLNOMTAB_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPLNOMCAM_2_2.value==this.w_PLNOMCAM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPLNOMCAM_2_2.value=this.w_PLNOMCAM
      replace t_PLNOMCAM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPLNOMCAM_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPLFLTRAS_2_3.RadioValue()==this.w_PLFLTRAS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPLFLTRAS_2_3.SetRadio()
      replace t_PLFLTRAS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPLFLTRAS_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPLVALFIS_2_4.value==this.w_PLVALFIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPLVALFIS_2_4.value=this.w_PLVALFIS
      replace t_PLVALFIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPLVALFIS_2_4.value
    endif
    cp_SetControlsValueExtFlds(this,'PARMLIST')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_PLNOMTAB<>'LIS_TINI') and not(empty(.w_PLNOMTAB)) and (NOT EMPTY(.w_PLNOMTAB) AND NOT EMPTY(.w_PLNOMCAM) AND NOT EMPTY(.w_PLFLTRAS))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPLNOMTAB_2_1
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Impossibile associare la tabella lis_tini o nome della tabella non valido")
        case   not(.w_PLNOMCAM<>'UTCC' AND .w_PLNOMCAM<>'UTCV' AND .w_PLNOMCAM<>'UTDC' AND .w_PLNOMCAM<>'UTDV') and not(empty(.w_PLNOMCAM)) and (NOT EMPTY(.w_PLNOMTAB) AND NOT EMPTY(.w_PLNOMCAM) AND NOT EMPTY(.w_PLFLTRAS))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPLNOMCAM_2_2
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Manca il nome campo")
        case   not(EMPTY(.w_PLVALFIS) OR CHKTIPIM(.w_PLNOMTAB, .w_PLNOMCAM,.w_PLVALFIS)) and (.w_PLFLTRAS='P' OR .w_PLFLTRAS='F') and (NOT EMPTY(.w_PLNOMTAB) AND NOT EMPTY(.w_PLNOMCAM) AND NOT EMPTY(.w_PLFLTRAS))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPLVALFIS_2_4
          i_bRes = .f.
          i_bnoChk = .f.
      endcase
      i_bRes = i_bRes .and. .GSIL_MPT.CheckForm()
      if NOT EMPTY(.w_PLNOMTAB) AND NOT EMPTY(.w_PLNOMCAM) AND NOT EMPTY(.w_PLFLTRAS)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    * --- GSIL_MPT : Depends On
    this.GSIL_MPT.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(NOT EMPTY(t_PLNOMTAB) AND NOT EMPTY(t_PLNOMCAM) AND NOT EMPTY(t_PLFLTRAS))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_PLNOMTAB=space(8)
      .w_PLNOMCAM=space(8)
      .w_PLFLTRAS=space(1)
      .w_PLVALFIS=space(20)
      .DoRTCalc(1,3,.f.)
      if not(empty(.w_PLNOMTAB))
        .link_2_1('Full')
      endif
      .DoRTCalc(4,4,.f.)
      if not(empty(.w_PLNOMCAM))
        .link_2_2('Full')
      endif
        .w_PLFLTRAS = 'N'
    endwith
    this.DoRTCalc(6,6,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_PLNOMTAB = t_PLNOMTAB
    this.w_PLNOMCAM = t_PLNOMCAM
    this.w_PLFLTRAS = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPLFLTRAS_2_3.RadioValue(.t.)
    this.w_PLVALFIS = t_PLVALFIS
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_PLNOMTAB with this.w_PLNOMTAB
    replace t_PLNOMCAM with this.w_PLNOMCAM
    replace t_PLFLTRAS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPLFLTRAS_2_3.ToRadio()
    replace t_PLVALFIS with this.w_PLVALFIS
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsil_mplPag1 as StdContainer
  Width  = 825
  height = 294
  stdWidth  = 825
  stdheight = 294
  resizeXpos=276
  resizeYpos=205
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPLCODCAT_1_2 as StdField with uid="RGWIOPYTVY",rtseq=1,rtrep=.f.,;
    cFormVar = "w_PLCODCAT", cQueryName = "PLCODCAT",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria (da tabella categorie)",;
    HelpContextID = 20336054,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=74, Top=11, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CATMLIST", cZoomOnZoom="gsil_mcl", oKey_1_1="CTCODCAT", oKey_1_2="this.w_PLCODCAT"

  func oPLCODCAT_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oPLCODCAT_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPLCODCAT_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oPLCODCAT_1_2.readonly and this.parent.oPLCODCAT_1_2.isprimarykey)
    do cp_zoom with 'CATMLIST','*','CTCODCAT',cp_AbsName(this.parent,'oPLCODCAT_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'gsil_mcl',"Categorie listini",'',this.parent.oContained
   endif
  endproc
  proc oPLCODCAT_1_2.mZoomOnZoom
    local i_obj
    i_obj=gsil_mcl()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODCAT=this.parent.oContained.w_PLCODCAT
    i_obj.ecpSave()
  endproc

  add object oPL__PATH_1_4 as StdField with uid="LUQEDARKYY",rtseq=2,rtrep=.f.,;
    cFormVar = "w_PL__PATH", cQueryName = "PL__PATH",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Percorso file txt",;
    HelpContextID = 40144322,;
   bGlobalFont=.t.,;
    Height=21, Width=678, Left=74, Top=38, InputMask=replicate('X',100)


  add object oBtn_1_5 as StdButton with uid="QZPSENJZIY",left=756, top=38, width=22,height=22,;
    caption="...", nPag=1;
    , HelpContextID = 121192918;
  , bGlobalFont=.t.

    proc oBtn_1_5.Click()
      with this.Parent.oContained
        .w_PL__PATH = LEFT(GETFILE()+space(100),100)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=8, top=82, width=451,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=4,Field1="PLNOMTAB",Label1="Tabella",Field2="PLNOMCAM",Label2="Campo",Field3="PLFLTRAS",Label3="Tipo valorizzazione",Field4="PLVALFIS",Label4="Valore fisso",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 99788166

  add object oStr_1_1 as StdString with uid="HISKEUHQPR",Visible=.t., Left=5, Top=11,;
    Alignment=1, Width=65, Height=18,;
    Caption="Cat. listino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="FSKALPJAWP",Visible=.t., Left=39, Top=38,;
    Alignment=1, Width=31, Height=18,;
    Caption="Path:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-2,top=101,;
    width=444+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-1,top=102,width=443+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='STRUTABE|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oContained.ChildrenChangeRow()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='STRUTABE'
        oDropInto=this.oBodyCol.oRow.oPLNOMTAB_2_1
      case cFile='STRUTABE'
        oDropInto=this.oBodyCol.oRow.oPLNOMCAM_2_2
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oLinkPC_2_5 as stdDynamicChildContainer with uid="ZWKGLEXVXV",bOnScreen=.t.,width=368,height=211,;
   left=458, top=82;


  func oLinkPC_2_5.mCond()
    with this.Parent.oContained
      return (.w_PLFLTRAS='T')
    endwith
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsil_mplBodyRow as CPBodyRowCnt
  Width=434
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oPLNOMTAB_2_1 as StdTrsField with uid="WPOIITRHEW",rtseq=3,rtrep=.t.,;
    cFormVar="w_PLNOMTAB",value=space(8),;
    ToolTipText = "Nome tabella",;
    HelpContextID = 262512072,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Impossibile associare la tabella lis_tini o nome della tabella non valido",;
   bGlobalFont=.t.,;
    Height=17, Width=91, Left=-2, Top=0, InputMask=replicate('X',8), bHasZoom = .t. , cLinkFile="STRUTABE", cZoomOnZoom="GSIL_MSD", oKey_1_1="STNOMTAB", oKey_1_2="this.w_PLNOMTAB"

  func oPLNOMTAB_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
      if .not. empty(.w_PLNOMCAM)
        bRes2=.link_2_2('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oPLNOMTAB_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPLNOMTAB_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'STRUTABE','*','STNOMTAB',cp_AbsName(this.parent,'oPLNOMTAB_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSIL_MSD',"Nome tabella",'',this.parent.oContained
  endproc
  proc oPLNOMTAB_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSIL_MSD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_STNOMTAB=this.parent.oContained.w_PLNOMTAB
    i_obj.ecpSave()
  endproc

  add object oPLNOMCAM_2_2 as StdTrsField with uid="QUNPYITJWQ",rtseq=4,rtrep=.t.,;
    cFormVar="w_PLNOMCAM",value=space(8),;
    ToolTipText = "Nome campo",;
    HelpContextID = 10853821,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Manca il nome campo",;
   bGlobalFont=.t.,;
    Height=17, Width=91, Left=93, Top=0, InputMask=replicate('X',8), bHasZoom = .t. , cLinkFile="STRUTABE", cZoomOnZoom="GSIL_MSD", oKey_1_1="STNOMTAB", oKey_1_2="this.w_PLNOMTAB", oKey_2_1="STNOMCAM", oKey_2_2="this.w_PLNOMCAM"

  func oPLNOMCAM_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oPLNOMCAM_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPLNOMCAM_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.STRUTABE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"STNOMTAB="+cp_ToStrODBC(this.Parent.oContained.w_PLNOMTAB)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"STNOMTAB="+cp_ToStr(this.Parent.oContained.w_PLNOMTAB)
    endif
    do cp_zoom with 'STRUTABE','*','STNOMTAB,STNOMCAM',cp_AbsName(this.parent,'oPLNOMCAM_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSIL_MSD',"Nome campo",'GSIL1ZNC.STRUTABE_VZM',this.parent.oContained
  endproc
  proc oPLNOMCAM_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSIL_MSD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.STNOMTAB=w_PLNOMTAB
     i_obj.w_STNOMCAM=this.parent.oContained.w_PLNOMCAM
    i_obj.ecpSave()
  endproc

  add object oPLFLTRAS_2_3 as StdTrsCombo with uid="EHOARCYWLX",rtrep=.t.,;
    cFormVar="w_PLFLTRAS", RowSource=""+"Nessuno,"+"Predefinito,"+"Predef. forzato,"+"Trascodifiche" , ;
    ToolTipText = "Flag trascodifica (P, F, T)",;
    HelpContextID = 20520375,;
    Height=21, Width=97, Left=188, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2  , sErrorMsg = "Manca il flag trascodifica";
, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oPLFLTRAS_2_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PLFLTRAS,&i_cF..t_PLFLTRAS),this.value)
    return(iif(xVal =1,'N',;
    iif(xVal =2,'P',;
    iif(xVal =3,'F',;
    iif(xVal =4,'T',;
    'N')))))
  endfunc
  func oPLFLTRAS_2_3.GetRadio()
    this.Parent.oContained.w_PLFLTRAS = this.RadioValue()
    return .t.
  endfunc

  func oPLFLTRAS_2_3.ToRadio()
    this.Parent.oContained.w_PLFLTRAS=trim(this.Parent.oContained.w_PLFLTRAS)
    return(;
      iif(this.Parent.oContained.w_PLFLTRAS=='N',1,;
      iif(this.Parent.oContained.w_PLFLTRAS=='P',2,;
      iif(this.Parent.oContained.w_PLFLTRAS=='F',3,;
      iif(this.Parent.oContained.w_PLFLTRAS=='T',4,;
      0)))))
  endfunc

  func oPLFLTRAS_2_3.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oPLVALFIS_2_4 as StdTrsField with uid="DBLZQFXWMQ",rtseq=6,rtrep=.t.,;
    cFormVar="w_PLVALFIS",value=space(20),;
    ToolTipText = "Valore predefinito",;
    HelpContextID = 37544521,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=139, Left=290, Top=0, InputMask=replicate('X',20)

  func oPLVALFIS_2_4.mCond()
    with this.Parent.oContained
      return (.w_PLFLTRAS='P' OR .w_PLFLTRAS='F')
    endwith
  endfunc

  func oPLVALFIS_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_PLVALFIS) OR CHKTIPIM(.w_PLNOMTAB, .w_PLNOMCAM,.w_PLVALFIS))
    endwith
    return bRes
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oPLNOMTAB_2_1.When()
    return(.t.)
  proc oPLNOMTAB_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oPLNOMTAB_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsil_mpl','PARMLIST','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PLCODCAT=PARMLIST.PLCODCAT";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
