* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bs1                                                        *
*              Control righe chiusura partite                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_110]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-25                                                      *
* Last revis.: 2011-11-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bs1",oParentObject,m.pOPER)
return(i_retval)

define class tgscg_bs1 as StdBatch
  * --- Local variables
  pOPER = space(4)
  w_CAMB = 0
  w_VALDIF = 0
  w_VAL1 = 0
  w_VAL2 = 0
  w_APPO = 0
  w_APPO1 = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue Calcoli/Controlli dopo l'input Importo Saldato e Flag Saldato (GSCG_MSP)
    * --- Parametro pOPER:
    * --- 'IMPO' input eseguito su Importo Saldato
    * --- 'FLAG' input eseguito su Flag Saldato
    this.oParentObject.w_FASE = 1
    do case
      case this.pOPER="IMPO"
        if ABS(this.oParentObject.w_SAIMPSAL)>=ABS(this.oParentObject.w_SATOTIMP)
          if this.oParentObject.pTIPGES<>"B" or (this.oParentObject.pTIPGES= "B" and ABS(this.oParentObject.w_SAIMPSAL)=ABS(this.oParentObject.w_SATOTIMP))
            this.oParentObject.w_SAFLSALD = "S"
          endif
          this.oParentObject.w_SAIMPABB = this.oParentObject.w_SATOTIMP - this.oParentObject.w_SAIMPSAL
        else
          this.oParentObject.w_SAIMPABB = IIF(this.oParentObject.w_SAFLSALD="S", this.oParentObject.w_SATOTIMP - this.oParentObject.w_SAIMPSAL, 0)
        endif
        if UPPER(this.oParentObject.w_CLASS) ="TGSCG_MSC" AND this.oParentObject.w_SAIMPABB<>0
          this.oParentObject.w_FLACC = IIF(this.oParentObject.w_SAIMPABB>0 ," ","S")
        endif
      case this.pOPER="DAO"
        this.oParentObject.w_TOTDAV = VAL2MON(this.oParentObject.w_TOTDAO, this.oParentObject.w_CAOVAL, this.oParentObject.w_CAONAZ, this.oParentObject.w_DATRIF, this.oParentObject.w_CODNAZ, this.oParentObject.w_DECTOP)
      case this.pOPER="FLAG"
        if this.oParentObject.w_SAFLSALD="S"
          * --- Non riscontro Abbuoni nel caso di partite di segno concorde alla sezione di PN (Note di Cerdito)
          if this.oParentObject.w_SA_SEGNO= this.oParentObject.w_SEGNO
            if this.oParentObject.w_SAIMPSAL=0 
              if this.oParentObject.w_TOTDAV=0 
                this.oParentObject.w_SAIMPSAL = this.oParentObject.w_SATOTIMP
              else
                if this.oParentObject.w_TOTDAO<>0 AND this.oParentObject.w_NUMVAL=1
                  this.oParentObject.w_SAIMPSAL = this.oParentObject.w_VALRES
                else
                  this.oParentObject.w_SAIMPSAL = this.oParentObject.w_TOTRES
                  if this.oParentObject.w_SACODVAL<>this.oParentObject.w_CODNAZ
                    * --- Riporta alla Valuta Partita (con il cambio attuale)
                    this.oParentObject.w_SAIMPSAL = MON2VAL(this.oParentObject.w_SAIMPSAL, this.oParentObject.w_SACAOVAL, this.oParentObject.w_CAOVAL, this.oParentObject.w_DATRIF, this.oParentObject.w_CODNAZ, this.oParentObject.w_DECRIG)
                  endif
                endif
                if this.oParentObject.w_SAIMPSAL<this.oParentObject.w_SATOTIMP 
                  * --- Verifica se l'importo Abbuono e' inferiore al minimo gestibile per la valuta di conto (Es Partite LIRE Val.Conto EURO)
                  this.w_APPO1 = VAL2MON(this.oParentObject.w_SATOTIMP-this.oParentObject.w_SAIMPSAL, this.oParentObject.w_SACAOVAL, this.oParentObject.w_CAONAZ, this.oParentObject.w_DATRIF, this.oParentObject.w_CODNAZ, this.oParentObject.w_DECTOP)
                  if this.oParentObject.w_SAIMPSAL<0
                    this.oParentObject.w_SAIMPSAL = 0
                  endif
                else
                  this.oParentObject.w_SAIMPSAL = this.oParentObject.w_SATOTIMP
                endif
              endif
            endif
          else
            this.oParentObject.w_SAIMPSAL = this.oParentObject.w_SATOTIMP
          endif
          this.oParentObject.w_SAIMPABB = this.oParentObject.w_SATOTIMP - this.oParentObject.w_SAIMPSAL
        else
          this.oParentObject.w_SAIMPABB = 0
          if ABS(this.oParentObject.w_SAIMPSAL)>=ABS(this.oParentObject.w_SATOTIMP)
            this.oParentObject.w_SAIMPSAL = 0
          endif
          this.oParentObject.w_FLACC = " "
        endif
    endcase
    if this.pOper<>"DAO"
      this.oParentObject.w_SEGNO = IIF(this.oParentObject.w_FLAGAC="S",IIF(this.oParentObject.w_SEGSAL="D","A","D"),this.oParentObject.w_SEGSAL)
      this.oParentObject.w_TOTSAL = iif(this.oParentObject.w_TIPSBF="S",this.oParentObject.w_TOTSAL + this.oParentObject.w_DARSAL1,this.oParentObject.w_TOTSAL - this.oParentObject.w_DARSAL1)
      this.oParentObject.w_VALSAL = this.oParentObject.w_VALSAL - this.oParentObject.w_DARSAL
      this.oParentObject.w_DARSAL = (this.oParentObject.w_SAIMPSAL+this.oParentObject.w_SAIMPABB) * IIF(this.oParentObject.w_SEGNO=this.oParentObject.w_SA_SEGNO, 1, -1)
      this.oParentObject.w_DARSAL1 = this.oParentObject.w_DARSAL
      if this.oParentObject.w_SACODVAL<>this.oParentObject.w_CODNAZ
        this.oParentObject.w_DARSAL1 = VAL2MON(this.oParentObject.w_DARSAL, this.oParentObject.w_SACAOVAL, this.oParentObject.w_CAONAZ, this.oParentObject.w_DATRIF, this.oParentObject.w_CODNAZ, this.oParentObject.w_DECTOP)
      endif
      this.oParentObject.w_DIFCAM = 0
      if this.oParentObject.w_EURVAL=0 OR this.oParentObject.w_DATRIF<GETVALUT(g_PERVAL, "VADATEUR")
        * --- Fase Pre EURO o Valuta non EURO; Calcola Differenze Cambi
        this.w_VALDIF = MIN(ABS(this.oParentObject.w_SATOTIMP), ABS(this.oParentObject.w_SAIMPSAL))
        this.w_VAL1 = VAL2MON(this.w_VALDIF, this.oParentObject.w_SACAOVAL, this.oParentObject.w_CAONAZ, this.oParentObject.w_DATRIF, this.oParentObject.w_CODNAZ, this.oParentObject.w_DECTOP)
        this.w_VAL2 = VAL2MON(this.w_VALDIF, this.oParentObject.w_SACAOAPE, this.oParentObject.w_CAONAZ, this.oParentObject.w_SADATAPE, this.oParentObject.w_CODNAZ, this.oParentObject.w_DECTOP)
        this.oParentObject.w_DIFCAM = this.w_VAL1 - this.w_VAL2
      endif
      this.oParentObject.w_VALSAL = this.oParentObject.w_VALSAL + this.oParentObject.w_DARSAL
      this.oParentObject.w_TOTSAL = iif(this.oParentObject.w_TIPSBF="S",this.oParentObject.w_TOTSAL - this.oParentObject.w_DARSAL1,this.oParentObject.w_TOTSAL + this.oParentObject.w_DARSAL1)
    endif
    this.oParentObject.w_TOTRES = IIF(this.oParentObject.w_TOTDAV=0, 0, Abs(this.oParentObject.w_TOTSAL)- Abs(this.oParentObject.w_TOTDAV))
    this.oParentObject.w_VALRES = IIF(this.oParentObject.w_TOTDAO=0, 0, Abs(this.oParentObject.w_VALSAL)-Abs(this.oParentObject.w_TOTDAO))
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
