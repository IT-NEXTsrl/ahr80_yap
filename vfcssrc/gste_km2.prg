* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_km2                                                        *
*              Altri dati scadenze                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_77]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-22                                                      *
* Last revis.: 2013-03-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgste_km2",oParentObject))

* --- Class definition
define class tgste_km2 as StdForm
  Top    = 102
  Left   = 40

  * --- Standard Properties
  Width  = 728
  Height = 268
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-03-21"
  HelpContextID=199898217
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=37

  * --- Constant Properties
  _IDX = 0
  VALUTE_IDX = 0
  COC_MAST_IDX = 0
  CONTI_IDX = 0
  AGENTI_IDX = 0
  cPrg = "gste_km2"
  cComment = "Altri dati scadenze"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_DVSERIAL = space(10)
  w_RIFNUM = 0
  w_RIFALF = space(10)
  w_PTDATAPE = ctod('  /  /  ')
  w_PTIMPDOC = 0
  w_NUMDIS = 0
  w_ANNDIS = space(4)
  w_DATDIS = ctod('  /  /  ')
  w_NUMIND = 0
  w_ESEIND = space(4)
  w_DATIND = ctod('  /  /  ')
  w_PTCODVAL = space(3)
  w_DESVAL = space(35)
  w_PTCAOVAL = 0
  w_PTNUMDOC = 0
  w_PTALFDOC = space(10)
  w_PTDATDOC = ctod('  /  /  ')
  w_PTCAOAPE = 0
  w_PTBANNOS = space(15)
  w_PTFLCRSA = space(1)
  w_NUMDIS1 = 0
  w_ANNDIS1 = space(4)
  w_TIPDIS = space(2)
  w_DATDIS1 = ctod('  /  /  ')
  w_NUMEFF = 0
  w_PTNUMEFF = 0
  w_PTCODAGE = space(5)
  w_DESAGE = space(35)
  w_PTDATINT = ctod('  /  /  ')
  w_PTFLVABD = space(1)
  w_PTFLRAGG = space(1)
  w_SIMBVA = space(5)
  w_DECTOT = 0
  w_OBTEST = ctod('  /  /  ')
  w_PTTIPCON = space(10)
  w_DTOBSO = ctod('  /  /  ')
  w_DATDOC = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgste_km2Pag1","gste_km2",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPTIMPDOC_1_8
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='VALUTE'
    this.cWorkTables[2]='COC_MAST'
    this.cWorkTables[3]='CONTI'
    this.cWorkTables[4]='AGENTI'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DVSERIAL=space(10)
      .w_RIFNUM=0
      .w_RIFALF=space(10)
      .w_PTDATAPE=ctod("  /  /  ")
      .w_PTIMPDOC=0
      .w_NUMDIS=0
      .w_ANNDIS=space(4)
      .w_DATDIS=ctod("  /  /  ")
      .w_NUMIND=0
      .w_ESEIND=space(4)
      .w_DATIND=ctod("  /  /  ")
      .w_PTCODVAL=space(3)
      .w_DESVAL=space(35)
      .w_PTCAOVAL=0
      .w_PTNUMDOC=0
      .w_PTALFDOC=space(10)
      .w_PTDATDOC=ctod("  /  /  ")
      .w_PTCAOAPE=0
      .w_PTBANNOS=space(15)
      .w_PTFLCRSA=space(1)
      .w_NUMDIS1=0
      .w_ANNDIS1=space(4)
      .w_TIPDIS=space(2)
      .w_DATDIS1=ctod("  /  /  ")
      .w_NUMEFF=0
      .w_PTNUMEFF=0
      .w_PTCODAGE=space(5)
      .w_DESAGE=space(35)
      .w_PTDATINT=ctod("  /  /  ")
      .w_PTFLVABD=space(1)
      .w_PTFLRAGG=space(1)
      .w_SIMBVA=space(5)
      .w_DECTOT=0
      .w_OBTEST=ctod("  /  /  ")
      .w_PTTIPCON=space(10)
      .w_DTOBSO=ctod("  /  /  ")
      .w_DATDOC=ctod("  /  /  ")
      .w_DVSERIAL=oParentObject.w_DVSERIAL
      .w_RIFNUM=oParentObject.w_RIFNUM
      .w_RIFALF=oParentObject.w_RIFALF
      .w_PTDATAPE=oParentObject.w_PTDATAPE
      .w_PTIMPDOC=oParentObject.w_PTIMPDOC
      .w_NUMDIS=oParentObject.w_NUMDIS
      .w_ANNDIS=oParentObject.w_ANNDIS
      .w_DATDIS=oParentObject.w_DATDIS
      .w_NUMIND=oParentObject.w_NUMIND
      .w_ESEIND=oParentObject.w_ESEIND
      .w_DATIND=oParentObject.w_DATIND
      .w_PTCODVAL=oParentObject.w_PTCODVAL
      .w_PTCAOVAL=oParentObject.w_PTCAOVAL
      .w_PTNUMDOC=oParentObject.w_PTNUMDOC
      .w_PTALFDOC=oParentObject.w_PTALFDOC
      .w_PTDATDOC=oParentObject.w_PTDATDOC
      .w_PTCAOAPE=oParentObject.w_PTCAOAPE
      .w_PTBANNOS=oParentObject.w_PTBANNOS
      .w_PTFLCRSA=oParentObject.w_PTFLCRSA
      .w_TIPDIS=oParentObject.w_TIPDIS
      .w_NUMEFF=oParentObject.w_NUMEFF
      .w_PTNUMEFF=oParentObject.w_PTNUMEFF
      .w_PTCODAGE=oParentObject.w_PTCODAGE
      .w_PTDATINT=oParentObject.w_PTDATINT
      .w_PTFLVABD=oParentObject.w_PTFLVABD
      .w_PTFLRAGG=oParentObject.w_PTFLRAGG
      .w_PTTIPCON=oParentObject.w_PTTIPCON
      .w_DATDOC=oParentObject.w_DATDOC
          .DoRTCalc(1,11,.f.)
        .w_PTCODVAL = .w_PTCODVAL
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_PTCODVAL))
          .link_1_27('Full')
        endif
          .DoRTCalc(13,18,.f.)
        .w_PTBANNOS = .w_PTBANNOS
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_PTBANNOS))
          .link_1_34('Full')
        endif
          .DoRTCalc(20,20,.f.)
        .w_NUMDIS1 = IIF(.w_NUMIND<>0 AND .w_NUMDIS=0, .w_NUMIND, .w_NUMDIS)
        .w_ANNDIS1 = IIF(.w_NUMIND<>0  AND .w_NUMDIS=0, .w_ESEIND, .w_ANNDIS)
          .DoRTCalc(23,23,.f.)
        .w_DATDIS1 = IIF(.w_NUMIND<>0  AND .w_NUMDIS=0, .w_DATIND, .w_DATDIS)
          .DoRTCalc(25,26,.f.)
        .w_PTCODAGE = .w_PTCODAGE
        .DoRTCalc(27,27,.f.)
        if not(empty(.w_PTCODAGE))
          .link_1_42('Full')
        endif
          .DoRTCalc(28,33,.f.)
        .w_OBTEST = I_DATSYS
    endwith
    this.DoRTCalc(35,37,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_49.enabled = this.oPgFrm.Page1.oPag.oBtn_1_49.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_50.enabled = this.oPgFrm.Page1.oPag.oBtn_1_50.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  proc SetStatus()
    if type('this.oParentObject.cFunction')='C'
      this.cFunction=this.oParentObject.cFunction
      if this.cFunction="Load"
        this.cFunction="Edit"
      endif
    endif
    DoDefault()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oPTIMPDOC_1_8.enabled = i_bVal
      .Page1.oPag.oPTCODVAL_1_27.enabled = i_bVal
      .Page1.oPag.oPTCAOVAL_1_29.enabled = i_bVal
      .Page1.oPag.oPTNUMDOC_1_30.enabled = i_bVal
      .Page1.oPag.oPTALFDOC_1_31.enabled = i_bVal
      .Page1.oPag.oPTDATDOC_1_32.enabled = i_bVal
      .Page1.oPag.oPTCAOAPE_1_33.enabled = i_bVal
      .Page1.oPag.oPTBANNOS_1_34.enabled = i_bVal
      .Page1.oPag.oPTFLCRSA_1_35.enabled = i_bVal
      .Page1.oPag.oNUMEFF_1_40.enabled = i_bVal
      .Page1.oPag.oPTCODAGE_1_42.enabled = i_bVal
      .Page1.oPag.oPTDATINT_1_44.enabled = i_bVal
      .Page1.oPag.oPTFLVABD_1_45.enabled = i_bVal
      .Page1.oPag.oPTFLRAGG_1_46.enabled = i_bVal
      .Page1.oPag.oBtn_1_49.enabled = .Page1.oPag.oBtn_1_49.mCond()
      .Page1.oPag.oBtn_1_50.enabled = .Page1.oPag.oBtn_1_50.mCond()
    endwith
    cp_SetEnabledExtFlds(this,'',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_DVSERIAL=.w_DVSERIAL
      .oParentObject.w_RIFNUM=.w_RIFNUM
      .oParentObject.w_RIFALF=.w_RIFALF
      .oParentObject.w_PTDATAPE=.w_PTDATAPE
      .oParentObject.w_PTIMPDOC=.w_PTIMPDOC
      .oParentObject.w_NUMDIS=.w_NUMDIS
      .oParentObject.w_ANNDIS=.w_ANNDIS
      .oParentObject.w_DATDIS=.w_DATDIS
      .oParentObject.w_NUMIND=.w_NUMIND
      .oParentObject.w_ESEIND=.w_ESEIND
      .oParentObject.w_DATIND=.w_DATIND
      .oParentObject.w_PTCODVAL=.w_PTCODVAL
      .oParentObject.w_PTCAOVAL=.w_PTCAOVAL
      .oParentObject.w_PTNUMDOC=.w_PTNUMDOC
      .oParentObject.w_PTALFDOC=.w_PTALFDOC
      .oParentObject.w_PTDATDOC=.w_PTDATDOC
      .oParentObject.w_PTCAOAPE=.w_PTCAOAPE
      .oParentObject.w_PTBANNOS=.w_PTBANNOS
      .oParentObject.w_PTFLCRSA=.w_PTFLCRSA
      .oParentObject.w_TIPDIS=.w_TIPDIS
      .oParentObject.w_NUMEFF=.w_NUMEFF
      .oParentObject.w_PTNUMEFF=.w_PTNUMEFF
      .oParentObject.w_PTCODAGE=.w_PTCODAGE
      .oParentObject.w_PTDATINT=.w_PTDATINT
      .oParentObject.w_PTFLVABD=.w_PTFLVABD
      .oParentObject.w_PTFLRAGG=.w_PTFLRAGG
      .oParentObject.w_PTTIPCON=.w_PTTIPCON
      .oParentObject.w_DATDOC=.w_DATDOC
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,20,.t.)
            .w_NUMDIS1 = IIF(.w_NUMIND<>0 AND .w_NUMDIS=0, .w_NUMIND, .w_NUMDIS)
            .w_ANNDIS1 = IIF(.w_NUMIND<>0  AND .w_NUMDIS=0, .w_ESEIND, .w_ANNDIS)
        .DoRTCalc(23,23,.t.)
            .w_DATDIS1 = IIF(.w_NUMIND<>0  AND .w_NUMDIS=0, .w_DATIND, .w_DATDIS)
        .DoRTCalc(25,33,.t.)
            .w_OBTEST = I_DATSYS
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(35,37,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPTCAOVAL_1_29.enabled = this.oPgFrm.Page1.oPag.oPTCAOVAL_1_29.mCond()
    this.oPgFrm.Page1.oPag.oPTCAOAPE_1_33.enabled = this.oPgFrm.Page1.oPag.oPTCAOAPE_1_33.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oTIPDIS_1_38.visible=!this.oPgFrm.Page1.oPag.oTIPDIS_1_38.mHide()
    this.oPgFrm.Page1.oPag.oPTCODAGE_1_42.visible=!this.oPgFrm.Page1.oPag.oPTCODAGE_1_42.mHide()
    this.oPgFrm.Page1.oPag.oDESAGE_1_43.visible=!this.oPgFrm.Page1.oPag.oDESAGE_1_43.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_53.visible=!this.oPgFrm.Page1.oPag.oStr_1_53.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_54.visible=!this.oPgFrm.Page1.oPag.oStr_1_54.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PTCODVAL
  func Link_1_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PTCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_PTCODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VASIMVAL,VADECTOT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_PTCODVAL))
          select VACODVAL,VADESVAL,VASIMVAL,VADECTOT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PTCODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PTCODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oPTCODVAL_1_27'),i_cWhere,'GSAR_AVL',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VASIMVAL,VADECTOT";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL,VASIMVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PTCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VASIMVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_PTCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_PTCODVAL)
            select VACODVAL,VADESVAL,VASIMVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PTCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DESVAL = NVL(_Link_.VADESVAL,space(35))
      this.w_SIMBVA = NVL(_Link_.VASIMVAL,space(5))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_PTCODVAL = space(3)
      endif
      this.w_DESVAL = space(35)
      this.w_SIMBVA = space(5)
      this.w_DECTOT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PTCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PTBANNOS
  func Link_1_34(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PTBANNOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_PTBANNOS)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_PTBANNOS))
          select BACODBAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PTBANNOS)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PTBANNOS) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oPTBANNOS_1_34'),i_cWhere,'GSTE_ACB',"Elenco conti banche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PTBANNOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_PTBANNOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_PTBANNOS)
            select BACODBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PTBANNOS = NVL(_Link_.BACODBAN,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_PTBANNOS = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PTBANNOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PTCODAGE
  func Link_1_42(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PTCODAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_PTCODAGE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_PTCODAGE))
          select AGCODAGE,AGDESAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PTCODAGE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PTCODAGE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oPTCODAGE_1_42'),i_cWhere,'GSAR_AGE',"Elenco agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PTCODAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_PTCODAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_PTCODAGE)
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PTCODAGE = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGE = NVL(_Link_.AGDESAGE,space(35))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PTCODAGE = space(5)
      endif
      this.w_DESAGE = space(35)
      this.w_DTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DTOBSO) OR .w_OBTEST<.w_DTOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice agente inesistente oppure obsoleto")
        endif
        this.w_PTCODAGE = space(5)
        this.w_DESAGE = space(35)
        this.w_DTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PTCODAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oRIFNUM_1_5.value==this.w_RIFNUM)
      this.oPgFrm.Page1.oPag.oRIFNUM_1_5.value=this.w_RIFNUM
    endif
    if not(this.oPgFrm.Page1.oPag.oRIFALF_1_6.value==this.w_RIFALF)
      this.oPgFrm.Page1.oPag.oRIFALF_1_6.value=this.w_RIFALF
    endif
    if not(this.oPgFrm.Page1.oPag.oPTIMPDOC_1_8.value==this.w_PTIMPDOC)
      this.oPgFrm.Page1.oPag.oPTIMPDOC_1_8.value=this.w_PTIMPDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oPTCODVAL_1_27.value==this.w_PTCODVAL)
      this.oPgFrm.Page1.oPag.oPTCODVAL_1_27.value=this.w_PTCODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESVAL_1_28.value==this.w_DESVAL)
      this.oPgFrm.Page1.oPag.oDESVAL_1_28.value=this.w_DESVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oPTCAOVAL_1_29.value==this.w_PTCAOVAL)
      this.oPgFrm.Page1.oPag.oPTCAOVAL_1_29.value=this.w_PTCAOVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oPTNUMDOC_1_30.value==this.w_PTNUMDOC)
      this.oPgFrm.Page1.oPag.oPTNUMDOC_1_30.value=this.w_PTNUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oPTALFDOC_1_31.value==this.w_PTALFDOC)
      this.oPgFrm.Page1.oPag.oPTALFDOC_1_31.value=this.w_PTALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oPTDATDOC_1_32.value==this.w_PTDATDOC)
      this.oPgFrm.Page1.oPag.oPTDATDOC_1_32.value=this.w_PTDATDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oPTCAOAPE_1_33.value==this.w_PTCAOAPE)
      this.oPgFrm.Page1.oPag.oPTCAOAPE_1_33.value=this.w_PTCAOAPE
    endif
    if not(this.oPgFrm.Page1.oPag.oPTBANNOS_1_34.value==this.w_PTBANNOS)
      this.oPgFrm.Page1.oPag.oPTBANNOS_1_34.value=this.w_PTBANNOS
    endif
    if not(this.oPgFrm.Page1.oPag.oPTFLCRSA_1_35.RadioValue()==this.w_PTFLCRSA)
      this.oPgFrm.Page1.oPag.oPTFLCRSA_1_35.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMDIS1_1_36.value==this.w_NUMDIS1)
      this.oPgFrm.Page1.oPag.oNUMDIS1_1_36.value=this.w_NUMDIS1
    endif
    if not(this.oPgFrm.Page1.oPag.oANNDIS1_1_37.value==this.w_ANNDIS1)
      this.oPgFrm.Page1.oPag.oANNDIS1_1_37.value=this.w_ANNDIS1
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPDIS_1_38.value==this.w_TIPDIS)
      this.oPgFrm.Page1.oPag.oTIPDIS_1_38.value=this.w_TIPDIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDATDIS1_1_39.value==this.w_DATDIS1)
      this.oPgFrm.Page1.oPag.oDATDIS1_1_39.value=this.w_DATDIS1
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMEFF_1_40.value==this.w_NUMEFF)
      this.oPgFrm.Page1.oPag.oNUMEFF_1_40.value=this.w_NUMEFF
    endif
    if not(this.oPgFrm.Page1.oPag.oPTCODAGE_1_42.value==this.w_PTCODAGE)
      this.oPgFrm.Page1.oPag.oPTCODAGE_1_42.value=this.w_PTCODAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESAGE_1_43.value==this.w_DESAGE)
      this.oPgFrm.Page1.oPag.oDESAGE_1_43.value=this.w_DESAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oPTDATINT_1_44.value==this.w_PTDATINT)
      this.oPgFrm.Page1.oPag.oPTDATINT_1_44.value=this.w_PTDATINT
    endif
    if not(this.oPgFrm.Page1.oPag.oPTFLVABD_1_45.RadioValue()==this.w_PTFLVABD)
      this.oPgFrm.Page1.oPag.oPTFLVABD_1_45.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPTFLRAGG_1_46.RadioValue()==this.w_PTFLRAGG)
      this.oPgFrm.Page1.oPag.oPTFLRAGG_1_46.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMBVA_1_47.value==this.w_SIMBVA)
      this.oPgFrm.Page1.oPag.oSIMBVA_1_47.value=this.w_SIMBVA
    endif
    if not(this.oPgFrm.Page1.oPag.oDATDOC_1_58.value==this.w_DATDOC)
      this.oPgFrm.Page1.oPag.oDATDOC_1_58.value=this.w_DATDOC
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_PTCODVAL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPTCODVAL_1_27.SetFocus()
            i_bnoObbl = !empty(.w_PTCODVAL)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valuta inesistente o diversa dalla valuta documento")
          case   (empty(.w_PTCAOVAL))  and (Empty(.w_DVSERIAL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPTCAOVAL_1_29.SetFocus()
            i_bnoObbl = !empty(.w_PTCAOVAL)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Cambio scadenza non definito")
          case   (empty(.w_PTCAOAPE))  and (Empty(.w_DVSERIAL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPTCAOAPE_1_33.SetFocus()
            i_bnoObbl = !empty(.w_PTCAOAPE)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Cambio del documento/reg. di apertura non definito")
          case   not((EMPTY(.w_DTOBSO) OR .w_OBTEST<.w_DTOBSO))  and not(.w_PTTIPCON='F')  and not(empty(.w_PTCODAGE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPTCODAGE_1_42.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice agente inesistente oppure obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgste_km2Pag1 as StdContainer
  Width  = 724
  height = 268
  stdWidth  = 724
  stdheight = 268
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oRIFNUM_1_5 as StdField with uid="AHUDGOCQKX",rtseq=2,rtrep=.f.,;
    cFormVar = "w_RIFNUM", cQueryName = "RIFNUM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "N� doc. registrazione contabile",;
    HelpContextID = 155682282,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=135, Top=15

  add object oRIFALF_1_6 as StdField with uid="XVAEEEKKHK",rtseq=3,rtrep=.f.,;
    cFormVar = "w_RIFALF", cQueryName = "RIFALF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 14976490,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=270, Top=15, InputMask=replicate('X',10)

  add object oPTIMPDOC_1_8 as StdField with uid="MIGHHRXIZX",rtseq=5,rtrep=.f.,;
    cFormVar = "w_PTIMPDOC", cQueryName = "PTIMPDOC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo del documento",;
    HelpContextID = 43535047,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=588, Top=15, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  add object oPTCODVAL_1_27 as StdField with uid="EOIPPDVWHS",rtseq=12,rtrep=.t.,;
    cFormVar = "w_PTCODVAL", cQueryName = "PTCODVAL",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Valuta inesistente o diversa dalla valuta documento",;
    ToolTipText = "Codice valuta della partita",;
    HelpContextID = 22457022,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=135, Top=45, InputMask=replicate('X',3), bHasZoom = .t. , tabstop=.f., cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_PTCODVAL"

  func oPTCODVAL_1_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_27('Part',this)
    endwith
    return bRes
  endfunc

  proc oPTCODVAL_1_27.ecpDrop(oSource)
    this.Parent.oContained.link_1_27('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPTCODVAL_1_27.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oPTCODVAL_1_27'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"",'',this.parent.oContained
  endproc
  proc oPTCODVAL_1_27.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_PTCODVAL
     i_obj.ecpSave()
  endproc

  add object oDESVAL_1_28 as StdField with uid="BTQYVGIQLE",rtseq=13,rtrep=.t.,;
    cFormVar = "w_DESVAL", cQueryName = "DESVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Valuta della partita associata",;
    HelpContextID = 192854730,;
   bGlobalFont=.t.,;
    Height=21, Width=224, Left=239, Top=45, InputMask=replicate('X',35)

  add object oPTCAOVAL_1_29 as StdField with uid="JWRRPFTIXF",rtseq=14,rtrep=.t.,;
    cFormVar = "w_PTCAOVAL", cQueryName = "PTCAOVAL",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Cambio scadenza non definito",;
    ToolTipText = "Cambio della partita",;
    HelpContextID = 11840190,;
   bGlobalFont=.t.,;
    Height=21, Width=107, Left=588, Top=45, cSayPict='"99999.999999"', cGetPict='"99999.999999"', tabstop=.f.

  func oPTCAOVAL_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_DVSERIAL))
    endwith
   endif
  endfunc

  add object oPTNUMDOC_1_30 as StdField with uid="XNHJOERHLK",rtseq=15,rtrep=.f.,;
    cFormVar = "w_PTNUMDOC", cQueryName = "PTNUMDOC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero del documento registrazione contabile di origine della scadenza",;
    HelpContextID = 46136007,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=135, Top=75, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  add object oPTALFDOC_1_31 as StdField with uid="TNFKUHEKWF",rtseq=16,rtrep=.f.,;
    cFormVar = "w_PTALFDOC", cQueryName = "PTALFDOC",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie del documento di origine della scadenza",;
    HelpContextID = 54119111,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=270, Top=75, InputMask=replicate('X',10)

  add object oPTDATDOC_1_32 as StdField with uid="UUOQGINWOQ",rtseq=17,rtrep=.f.,;
    cFormVar = "w_PTDATDOC", cQueryName = "PTDATDOC",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di riferimento del documento/registrazione P.N. di apertura",;
    HelpContextID = 40147655,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=417, Top=75

  add object oPTCAOAPE_1_33 as StdField with uid="PPHZJZSPYD",rtseq=18,rtrep=.t.,;
    cFormVar = "w_PTCAOAPE", cQueryName = "PTCAOAPE",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Cambio del documento/reg. di apertura non definito",;
    ToolTipText = "Cambio riferito al documento di apertura",;
    HelpContextID = 95726277,;
   bGlobalFont=.t.,;
    Height=21, Width=107, Left=588, Top=75, cSayPict='"99999.999999"', cGetPict='"99999.999999"'

  func oPTCAOAPE_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_DVSERIAL))
    endwith
   endif
  endfunc

  add object oPTBANNOS_1_34 as StdField with uid="BAZEIRCOHB",rtseq=19,rtrep=.f.,;
    cFormVar = "w_PTBANNOS", cQueryName = "PTBANNOS",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Nostro C/C. di appoggio per bonifici e RB",;
    HelpContextID = 147110583,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=135, Top=105, InputMask=replicate('X',15), bHasZoom = .t. , tabstop=.f., cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_PTBANNOS"

  func oPTBANNOS_1_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_34('Part',this)
    endwith
    return bRes
  endfunc

  proc oPTBANNOS_1_34.ecpDrop(oSource)
    this.Parent.oContained.link_1_34('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPTBANNOS_1_34.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oPTBANNOS_1_34'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"Elenco conti banche",'',this.parent.oContained
  endproc
  proc oPTBANNOS_1_34.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_PTBANNOS
     i_obj.ecpSave()
  endproc


  add object oPTFLCRSA_1_35 as StdCombo with uid="VJKGQVAWPZ",rtseq=20,rtrep=.f.,left=588,top=105,width=106,height=21;
    , tabstop=.f.;
    , ToolTipText = "Tipo partita";
    , HelpContextID = 177636663;
    , cFormVar="w_PTFLCRSA",RowSource=""+"Creazione,"+"Saldo,"+"Acconto", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPTFLCRSA_1_35.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'S',;
    iif(this.value =3,'A',;
    space(1)))))
  endfunc
  func oPTFLCRSA_1_35.GetRadio()
    this.Parent.oContained.w_PTFLCRSA = this.RadioValue()
    return .t.
  endfunc

  func oPTFLCRSA_1_35.SetRadio()
    this.Parent.oContained.w_PTFLCRSA=trim(this.Parent.oContained.w_PTFLCRSA)
    this.value = ;
      iif(this.Parent.oContained.w_PTFLCRSA=='C',1,;
      iif(this.Parent.oContained.w_PTFLCRSA=='S',2,;
      iif(this.Parent.oContained.w_PTFLCRSA=='A',3,;
      0)))
  endfunc

  add object oNUMDIS1_1_36 as StdField with uid="XXOSEXEUEE",rtseq=21,rtrep=.t.,;
    cFormVar = "w_NUMDIS1", cQueryName = "NUMDIS1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero distinta",;
    HelpContextID = 68225578,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=135, Top=135

  add object oANNDIS1_1_37 as StdField with uid="ZIJLQHULTB",rtseq=22,rtrep=.t.,;
    cFormVar = "w_ANNDIS1", cQueryName = "ANNDIS1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno distinta",;
    HelpContextID = 68223482,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=216, Top=135, InputMask=replicate('X',4)

  add object oTIPDIS_1_38 as StdField with uid="ENPLLNGWTQ",rtseq=23,rtrep=.t.,;
    cFormVar = "w_TIPDIS", cQueryName = "TIPDIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Tipo distinta",;
    HelpContextID = 68216266,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=268, Top=135, InputMask=replicate('X',2)

  func oTIPDIS_1_38.mHide()
    with this.Parent.oContained
      return (.w_NUMIND<>0 AND .w_NUMDIS=0)
    endwith
  endfunc

  add object oDATDIS1_1_39 as StdField with uid="NKNEUUWQTW",rtseq=24,rtrep=.t.,;
    cFormVar = "w_DATDIS1", cQueryName = "DATDIS1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data distinta",;
    HelpContextID = 68202186,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=420, Top=135

  add object oNUMEFF_1_40 as StdField with uid="QMDFZAZLEN",rtseq=25,rtrep=.t.,;
    cFormVar = "w_NUMEFF", cQueryName = "NUMEFF",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero effetto",;
    HelpContextID = 20974122,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=588, Top=135, tabstop=.f.

  add object oPTCODAGE_1_42 as StdField with uid="PGNLXZAQMO",rtseq=27,rtrep=.f.,;
    cFormVar = "w_PTCODAGE", cQueryName = "PTCODAGE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice agente inesistente oppure obsoleto",;
    ToolTipText = "Codice agente della partita/scadenza",;
    HelpContextID = 162092347,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=136, Top=163, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_PTCODAGE"

  func oPTCODAGE_1_42.mHide()
    with this.Parent.oContained
      return (.w_PTTIPCON='F')
    endwith
  endfunc

  func oPTCODAGE_1_42.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_42('Part',this)
    endwith
    return bRes
  endfunc

  proc oPTCODAGE_1_42.ecpDrop(oSource)
    this.Parent.oContained.link_1_42('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPTCODAGE_1_42.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oPTCODAGE_1_42'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"Elenco agenti",'',this.parent.oContained
  endproc
  proc oPTCODAGE_1_42.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_PTCODAGE
     i_obj.ecpSave()
  endproc

  add object oDESAGE_1_43 as StdField with uid="NRARGMDHYD",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DESAGE", cQueryName = "DESAGE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 36944586,;
   bGlobalFont=.t.,;
    Height=21, Width=224, Left=200, Top=163, InputMask=replicate('X',35)

  func oDESAGE_1_43.mHide()
    with this.Parent.oContained
      return (.w_PTTIPCON='F')
    endwith
  endfunc

  add object oPTDATINT_1_44 as StdField with uid="PBLGQHZHQD",rtseq=29,rtrep=.f.,;
    cFormVar = "w_PTDATINT", cQueryName = "PTDATINT",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Dal giorno successivo alla data impostata verranno calcolati gli interessi di mora",;
    HelpContextID = 224697014,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=588, Top=163

  add object oPTFLVABD_1_45 as StdCheck with uid="DAXINOZFHA",rtseq=30,rtrep=.f.,left=588, top=189, caption="Beni deperibili",;
    ToolTipText = "Flag beni deperibili",;
    HelpContextID = 87653062,;
    cFormVar="w_PTFLVABD", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPTFLVABD_1_45.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPTFLVABD_1_45.GetRadio()
    this.Parent.oContained.w_PTFLVABD = this.RadioValue()
    return .t.
  endfunc

  func oPTFLVABD_1_45.SetRadio()
    this.Parent.oContained.w_PTFLVABD=trim(this.Parent.oContained.w_PTFLVABD)
    this.value = ;
      iif(this.Parent.oContained.w_PTFLVABD=='S',1,;
      0)
  endfunc

  add object oPTFLRAGG_1_46 as StdCheck with uid="MITAVIHKGK",rtseq=31,rtrep=.t.,left=37, top=189, caption="Raggruppata",;
    ToolTipText = "Se attivo: scadenza raggruppata",;
    HelpContextID = 176588093,;
    cFormVar="w_PTFLRAGG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPTFLRAGG_1_46.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPTFLRAGG_1_46.GetRadio()
    this.Parent.oContained.w_PTFLRAGG = this.RadioValue()
    return .t.
  endfunc

  func oPTFLRAGG_1_46.SetRadio()
    this.Parent.oContained.w_PTFLRAGG=trim(this.Parent.oContained.w_PTFLRAGG)
    this.value = ;
      iif(this.Parent.oContained.w_PTFLRAGG=='S',1,;
      0)
  endfunc

  add object oSIMBVA_1_47 as StdField with uid="UMYMJFDXNT",rtseq=32,rtrep=.f.,;
    cFormVar = "w_SIMBVA", cQueryName = "SIMBVA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 88282586,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=184, Top=45, InputMask=replicate('X',5)


  add object oBtn_1_49 as StdButton with uid="MOPMNDHOAO",left=618, top=216, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare le modifiche";
    , HelpContextID = 199869466;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_49.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_50 as StdButton with uid="XQOFPYLORC",left=668, top=216, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire senza confermare";
    , HelpContextID = 192580794;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_50.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDATDOC_1_58 as StdField with uid="AHIKTGQWCH",rtseq=37,rtrep=.t.,;
    cFormVar = "w_DATDOC", cQueryName = "DATDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di riferimento del documento/registrazione P.N. di apertura",;
    HelpContextID = 61910730,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=420, Top=15

  add object oStr_1_1 as StdString with uid="YWFGHSESGV",Visible=.t., Left=15, Top=15,;
    Alignment=1, Width=117, Height=15,;
    Caption="Documento n.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_2 as StdString with uid="CEOHCJVSYG",Visible=.t., Left=260, Top=15,;
    Alignment=2, Width=10, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="QGTDIRCMAL",Visible=.t., Left=360, Top=15,;
    Alignment=1, Width=54, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="OWTAXXMBBC",Visible=.t., Left=508, Top=15,;
    Alignment=1, Width=77, Height=15,;
    Caption="Importo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="OFOQFVZFTP",Visible=.t., Left=508, Top=45,;
    Alignment=1, Width=77, Height=15,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="VVUBQNCFDQ",Visible=.t., Left=15, Top=45,;
    Alignment=1, Width=117, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="JEWDDYYNLL",Visible=.t., Left=15, Top=105,;
    Alignment=1, Width=117, Height=15,;
    Caption="Ns. banca:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="IPIKQHVKDA",Visible=.t., Left=4, Top=75,;
    Alignment=1, Width=128, Height=15,;
    Caption="Rif.doc. di origine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="FKGDDFEELN",Visible=.t., Left=260, Top=75,;
    Alignment=2, Width=10, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="DSRKJHEHMD",Visible=.t., Left=360, Top=75,;
    Alignment=1, Width=54, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="RRGRLPLEQD",Visible=.t., Left=508, Top=75,;
    Alignment=1, Width=77, Height=15,;
    Caption="C.origine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="OKQFPUNIXZ",Visible=.t., Left=15, Top=135,;
    Alignment=1, Width=117, Height=15,;
    Caption="Distinta n.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="SANGNTQFKX",Visible=.t., Left=207, Top=135,;
    Alignment=0, Width=10, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="JQIIUIUBDT",Visible=.t., Left=360, Top=135,;
    Alignment=1, Width=54, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="VDAJTFPMYZ",Visible=.t., Left=508, Top=135,;
    Alignment=1, Width=77, Height=15,;
    Caption="Effetto n.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="QZHHQKRULT",Visible=.t., Left=508, Top=105,;
    Alignment=1, Width=77, Height=15,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="VPYHFRHQVA",Visible=.t., Left=170, Top=191,;
    Alignment=0, Width=175, Height=18,;
    Caption="(Contab. indiretta effetti)"  ;
  , bGlobalFont=.t.

  func oStr_1_53.mHide()
    with this.Parent.oContained
      return (.w_NUMIND=0  OR .w_NUMDIS<>0)
    endwith
  endfunc

  add object oStr_1_54 as StdString with uid="XITLPZDGOW",Visible=.t., Left=91, Top=163,;
    Alignment=1, Width=41, Height=18,;
    Caption="Agente:"  ;
  , bGlobalFont=.t.

  func oStr_1_54.mHide()
    with this.Parent.oContained
      return (.w_PTTIPCON='F')
    endwith
  endfunc

  add object oStr_1_55 as StdString with uid="VLYQDQRHYX",Visible=.t., Left=467, Top=163,;
    Alignment=1, Width=118, Height=18,;
    Caption="Data mat. int.:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gste_km2','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
