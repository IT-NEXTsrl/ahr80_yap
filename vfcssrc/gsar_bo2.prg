* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bo2                                                        *
*              Lancia anagrafica clienti                                       *
*                                                                              *
*      Author: Mimmo Fasano                                                    *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_39]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-10-04                                                      *
* Last revis.: 2014-02-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTIPO,pCodice,pCliFor
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bo2",oParentObject,m.pTIPO,m.pCodice,m.pCliFor)
return(i_retval)

define class tgsar_bo2 as StdBatch
  * --- Local variables
  pTIPO = space(1)
  pCodice = space(15)
  pCliFor = space(1)
  w_PROG = .NULL.
  w_OBJ = .NULL.
  w_OBJ1 = .NULL.
  w_ANTIPCON = space(1)
  w_LOOP = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia  maschera anagrafica clienti  GSAR_ACL
    *     pTIPO :  N = lancia l'anagrafica Clienti dai Nominativi
    *     pTIPO :  A = lancia elenco Attivit� dai Nominativi
    * --- pCodice � l'eventuale codice del cliente nel caso in cui si attivi GSAR_ACL tramite tasto dx sul nominativo
    do case
      case this.pTIPO="N" OR this.pTIPO="C" OR this.pTIPO="M" OR this.pTIPO="F"
        * --- C e M sono attivati da RightClik oppure da GSAG_BRU
        *     In quel caso, il codice del cliente � passato direttamente come parametro
        if TYPE("pCliFor")="C"
          * --- Cliente o fornitore come specificato nei parametri
          this.w_ANTIPCON = this.pCliFor
        else
          this.w_ANTIPCON = "C"
        endif
        if this.w_ANTIPCON="F"
          this.w_PROG = GSAR_AFR()
        else
          this.w_PROG = GSAR_ACL()
        endif
        * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
        if !(this.w_PROG.bSec1)
          Ah_ErrorMsg("Impossibile aprire la gestione richiesta!",48,"")
          i_retcode = 'stop'
          return
        endif
        this.w_PROG.ecpFilter()     
        this.w_PROG.w_ANTIPCON = this.w_ANTIPCON
        if this.pTIPO="N"
          this.w_PROG.w_ANCODICE = this.oParentObject.w_NOCODCLI
        else
          this.w_PROG.w_ANCODICE = this.pCodice
        endif
        this.w_PROG.ecpSave()     
        if this.pTIPO="M" 
          this.w_PROG.ecpEdit()     
        else
          this.w_PROG.ecpQuery()     
        endif
        if Isalt() and this.pTIPO="N"
          * --- gestisco apertura coretta del codice passato simulando F8
          do while !(this.w_PROG.w_ANCODICE==this.oParentObject.w_NOCODCLI and Not empty(this.w_PROG.w_ANCODICE)) and this.w_LOOP<100
            this.w_PROG.ecpNext()     
            this.w_LOOP = this.w_LOOP + 1
          enddo
        endif
      case this.pTIPO="A"
        if g_AGEN = "S"
          * --- Apre la maschera di Ricerca Attivit�
          this.w_PROG = GSAG_KRA()
          * --- Controllo se ha passato il test di accesso
          if !(this.w_PROG.bSec1)
            Ah_ErrorMsg("Impossibile accedere alle attivit�!",48,"")
            i_retcode = 'stop'
            return
          endif
          this.w_PROG.w_EDITCODNOM = .F.
          this.w_PROG.w_CODNOM = this.oParentObject.w_NOCODICE
          this.w_OBJ = this.w_PROG.GetCtrl("w_CODNOM")
          this.w_OBJ.Check()     
          * --- Viene notificato l'evento per eseguire la query dello zoom delle Attivit�
          this.w_PROG.NotifyEvent("Ricerca")     
        else
          * --- Apre maschera agenda operatore 
          this.w_PROG = GSOF_KAT()
          * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
          if !(this.w_PROG.bSec1)
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            i_retcode = 'stop'
            return
          endif
          this.w_PROG.w_OPERAT = i_codute
          this.w_PROG.w_NOMINI = this.oParentObject.w_NOCODICE
          this.w_OBJ = this.w_PROG.GetcTRL("w_NOMINI")
          this.w_OBJ.Check()     
          this.w_PROG.w_NOMFIN = this.oParentObject.w_NOCODICE
          this.w_OBJ1 = this.w_PROG.GetcTRL("w_NOMFIN")
          this.w_OBJ1.Check()     
          this.w_PROG.mCalc(.T.)     
          this.w_PROG.NotifyEvent("Ricerca")     
        endif
      case this.pTIPO="P"
        this.w_PROG = GSPR_KRO()
        this.w_PROG.w_ROCODNOM = this.oParentObject.w_NOCODICE
        this.w_OBJ = this.w_PROG.GetCtrl("w_ROCODNOM")
        this.w_OBJ.Check()     
        * --- Viene notificato l'evento per eseguire la query dello zoom delle Attivit�
        this.w_PROG.NotifyEvent("Ricerca")     
        this.w_PROG.mcalc(.t.)     
    endcase
    this.w_OBJ = Null
    this.w_PROG = Null
  endproc


  proc Init(oParentObject,pTIPO,pCodice,pCliFor)
    this.pTIPO=pTIPO
    this.pCodice=pCodice
    this.pCliFor=pCliFor
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTIPO,pCodice,pCliFor"
endproc
