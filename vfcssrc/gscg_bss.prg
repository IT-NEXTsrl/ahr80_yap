* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bss                                                        *
*              Chiusura primanota da saldaconto                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_271]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-12-13                                                      *
* Last revis.: 2016-04-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bss",oParentObject,m.pOPER)
return(i_retval)

define class tgscg_bss as StdBatch
  * --- Local variables
  w_PTSA = .NULL.
  w_TIPO = space(2)
  w_STAMPA = space(1)
  pOPER = space(1)
  w_PADRE = .NULL.
  w_AGGTES = .f.
  w_PNSERIAL = space(10)
  w_PNNUMRER = 0
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_PNNUMPRO = 0
  w_PNPRP = space(2)
  w_DTOBSO = ctod("  /  /  ")
  w_DATAPE = ctod("  /  /  ")
  w_DATRIF = ctod("  /  /  ")
  w_PNFLPART = space(1)
  w_PNTIPCON = space(1)
  w_PNCODCON = space(15)
  w_PNCODPAG = space(5)
  w_PNCODAGE = space(5)
  w_PNRIFDIS = space(10)
  w_PNIMPDAR = 0
  w_PNIMPAVE = 0
  w_SALROW = 0
  w_VAL1 = 0
  w_VAL2 = 0
  w_OLDTIPCON = space(1)
  w_OLDCODCON = space(15)
  w_SEMAFORO = space(1)
  w_CCDESSUP = space(254)
  w_CCDESRIG = space(254)
  w_PNDESSUP = space(50)
  w_NUMPAR = space(14)
  w_DATSCA = ctod("  /  /  ")
  w_CODAGE = space(5)
  w_CONASS = space(15)
  w_PNCODCONASS = space(15)
  w_OLDCONASS = space(15)
  w_APPO1 = 0
  w_APPO = 0
  w_VALNAZ = space(3)
  w_VALACC = 0
  w_IMPABB = 0
  w_ABBCLI = 0
  w_CAOVAL = 0
  w_UTCC = 0
  w_CPROWORD = 0
  w_ABBROW = 0
  w_ROWCLI = 0
  w_DATBLO = ctod("  /  /  ")
  w_STALIG = ctod("  /  /  ")
  w_MESS = space(10)
  w_PNFLSALD = space(1)
  w_IMPCON = 0
  w_UTDC = ctod("  /  /  ")
  w_PNCAOVAL = 0
  w_IMPCLF = 0
  w_UTCV = 0
  w_PNDATREG = ctod("  /  /  ")
  w_PNFLVABD = space(1)
  w_APPO = 0
  w_UTDV = ctod("  /  /  ")
  w_CCFLPART = space(1)
  w_CODAZI = space(5)
  w_PNTIPDOC = space(2)
  w_FLPPRO = space(1)
  w_DATBLO = ctod("  /  /  ")
  w_PNTIPREG = space(1)
  w_PNANNPRO = space(4)
  w_STALIG = ctod("  /  /  ")
  w_PNNUMREG = 0
  w_PNCODESE = space(4)
  w_PNALFPRO = space(10)
  w_IMPDCA = 0
  w_CAOVAC = 0
  w_IMPDCP = 0
  w_PNCODVAL = space(3)
  w_TIPCON = space(1)
  w_PNCAOVAL = 0
  w_CODCON = space(15)
  w_PNCODCAU = space(5)
  w_PNCODUTE = 0
  w_PNPRG = space(8)
  w_PTFLRAGG = space(1)
  w_PNINICOM = ctod("  /  /  ")
  w_PNFINCOM = ctod("  /  /  ")
  w_PNVALNAZ = space(3)
  w_CAONAZ = 0
  w_PERPVL = 0
  w_FLPRO = space(1)
  w_CODVAP = space(3)
  w_CAOVAP = 0
  w_PARROW = 0
  w_PTROWORD = 0
  w_PTROWNUM = 0
  w_PT_SEGNO = space(1)
  w_PTTIPCON = space(1)
  w_PTCODCON = space(15)
  w_PTTOTIMP = 0
  w_PTCODVAL = space(3)
  w_PTCAOVAL = 0
  w_PTCAOAPE = 0
  w_EURVAL = 0
  w_PTDATAPE = ctod("  /  /  ")
  w_PTDESRIG = space(50)
  w_PTSERRIF = space(10)
  w_PTORDRIF = 0
  w_PTNUMRIF = 0
  w_ABB = 0
  w_PTTOTABB = 0
  w_PTIMPDOC = 0
  w_SALDROW = 0
  w_PTBANNOS = space(15)
  w_PTBANAPP = space(10)
  w_SERIAL = space(10)
  w_SIMVAL = space(3)
  w_PNDESRIG = space(50)
  w_SEZB = space(1)
  w_CONSUP = space(15)
  w_SQUAD = 0
  w_FLACC = 0
  w_PTDESCRI = space(50)
  w_IMPORTO = 0
  w_TOTABB = 0
  w_ROWORD = 0
  w_ROWNUM = 0
  w_NUMDIS = space(10)
  w_FLMOVC = space(1)
  w_CAUMOV = space(5)
  w_CAUSBF = space(5)
  w_CAUCC = space(5)
  w_TIPCAU = space(1)
  w_FLCRDE = space(1)
  w_CONBAN = space(15)
  w_APPVAL = 0
  w_IMPDEB = 0
  w_IMPCRE = 0
  w_NUMCOR = space(15)
  w_TIPBAN = space(1)
  w_CALFES = space(3)
  w_CODVAL = space(3)
  w_CCFLCRED = space(1)
  w_CCFLDEBI = space(1)
  w_CCFLCOMM = space(1)
  w_DATVAL = ctod("  /  /  ")
  w_TIPSOT = space(1)
  w_ROWCC = 0
  w_ORDCC = 0
  w_NUMDIS = space(10)
  * --- WorkFile variables
  AZIENDA_idx=0
  CAU_CONT_idx=0
  ESERCIZI_idx=0
  VALUTE_idx=0
  PNT_DETT_idx=0
  SALDIART_idx=0
  CONTI_idx=0
  PAR_TITE_idx=0
  SALDDACO_idx=0
  PNT_MAST_idx=0
  SALMDACO_idx=0
  CONTROPA_idx=0
  COC_MAST_idx=0
  CCM_DETT_idx=0
  CCC_DETT_idx=0
  DIS_TINT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue Chiusura Partite da Saldaconto Lanciato da GSCG_MSC
    * --- C: Posiziona saldaconto in Modifica
    *     S:Creazione Saldaconto
    *     D,E: gestione cancellazione Saldaconto
    this.w_STAMPA = this.oParentObject.w_EXECSTAM
    do case
      case this.pOPER="C"
        this.w_PADRE = This.oparentobject
        this.oParentObject.w_FASE = 1
        * --- Memorizzazione Saldaconto
        this.w_PADRE.bUpdated = .T.
        this.w_PADRE.NotifyEvent("Save")     
        this.w_PADRE.GotFocus()     
        this.w_PADRE.ecpSave()     
        if this.w_PADRE.w_FASE<>-1
          this.w_PADRE.ecpQuery()     
          * --- oserial propiet� valorizzata alla replace end
          * --- Creazione cursore delle chiavi
          this.w_PADRE.QueryKeySet("SCSERIAL=" + cp_ToStrODBC(this.oParentobject.OSERIAL) , "" )     
          * --- Caricamento record ed accesso in modifica
          this.w_PADRE.LoadRecWarn()     
          this.w_PADRE.NotifyEvent("Edit")     
          this.w_PADRE.ecpEdit()     
          this.oParentObject.w_FASE = 1
          this.w_PADRE.w_EXECSTAM = this.w_STAMPA
        endif
      case this.pOPER="S"
        this.w_PTSA = this.oParentObject.w_OGG
        DIMENSION ARPARAM[12,2]
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Controlli Preliminari
        if this.oParentObject.w_SCTIPGES="S"
          * --- Controlli solo per Saldaconto
          do case
            case EMPTY(this.oParentObject.w_SCCONTRO)
              ah_ErrorMsg("Conto contropartita saldaconto inesistente",,"")
              i_retcode = 'stop'
              return
            case EMPTY(this.oParentObject.w_SCCAUCON)
              ah_ErrorMsg("Causale contabile cliente per saldaconto inesistente",,"")
              i_retcode = 'stop'
              return
            case EMPTY(this.oParentObject.w_SCCAUCOF)
              ah_ErrorMsg("Causale contabile fornitore per saldaconto inesistente",,"")
              i_retcode = 'stop'
              return
          endcase
        endif
        do case
          case EMPTY(this.oParentObject.w_SCCONDCA) OR EMPTY(this.oParentObject.w_SCCONDCP)
            ah_ErrorMsg("Contropartite differenze cambi non definite",,"")
            i_retcode = 'stop'
            return
          case EMPTY(this.oParentObject.w_CONABBP)
            ah_ErrorMsg("Codice conto abbuoni passivi non definito",,"")
            i_retcode = 'stop'
            return
          case EMPTY(this.oParentObject.w_CONABBA)
            ah_ErrorMsg("Codice conto abbuoni attivi non definito",,"")
            i_retcode = 'stop'
            return
          case EMPTY(this.oParentObject.w_SCCAUCOC)
            if this.oParentObject.w_SCTIPGES="S"
              ah_ErrorMsg("Causale contabile conto per saldaconto inesistente",,"")
            else
              ah_ErrorMsg("Causale storno conti SBF inesistente",,"")
            endif
            i_retcode = 'stop'
            return
          case EMPTY(this.w_CAUCC) AND g_BANC="S" AND this.w_FLMOVC="S" AND this.oParentObject.w_SCTIPGES="B"
            ah_ErrorMsg("Causale movimenti C\C non definita nella causale contabile",,"")
            i_retcode = 'stop'
            return
          case EMPTY(this.w_CAUSBF) AND g_BANC="S" AND this.w_FLMOVC="S" AND this.oParentObject.w_SCTIPGES="B"
            ah_ErrorMsg("Causale movimenti C\C SBF non definita nella causale contabile",,"")
            i_retcode = 'stop'
            return
        endcase
        SELECT (this.w_PTSA.cTrsName) 
 Nc= (this.w_PTSA.cTrsName)
         
 SELECT t_SATIPCON,t_SACODCON FROM &NC Where (t_SAIMPSAL<>0 OR t_FLSALD="S") AND t_SACAOAPE<>0 ; 
 Group By t_SATIPCON,t_SACODCON ORDER BY t_SATIPCON,t_SACODCON INTO CURSOR Tmpn
        * --- Eseguo controlli premininari numero di righe della registrazione
        do case
          case Reccount("Tmpn")=0
            ah_ErrorMsg("Non ci sono importi da saldare",,"")
            i_retcode = 'stop'
            return
          case Reccount("Tmpn") >=1000
            ah_ErrorMsg("Superato limite massimo righe clienti\fornitori da saldare",,"")
            i_retcode = 'stop'
            return
        endcase
        Select Tmpn 
 Use
        * --- Try
        local bErr_04584BD0
        bErr_04584BD0=bTrsErr
        this.Try_04584BD0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          ah_ErrorMsg(i_errmsg,,"")
          i_retcode = 'stop'
          return
        endif
        bTrsErr=bTrsErr or bErr_04584BD0
        * --- End
         
 SELECT (this.w_PTSA.cTrsName) 
 NC= this.w_PTSA.cTrsName
        * --- Ordino le righe del cursore prima di creare la registrazione contabile
        * --- Try
        local bErr_04584ED0
        bErr_04584ED0=bTrsErr
        this.Try_04584ED0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          if NOT EMPTY(I_ERRMSG)
            ah_ErrorMsg(i_errmsg,,"")
          else
            ah_ErrorMsg("Errore durante l'aggiornamento; operazione abbandonata",,"")
          endif
        endif
        bTrsErr=bTrsErr or bErr_04584ED0
        * --- End
        if used ("CONTABIL")
          select CONTABIL
          use
        endif
        * --- Toglie <Blocco> per Primanota
        * --- Try
        local bErr_04582050
        bErr_04582050=bTrsErr
        this.Try_04582050()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          ah_ErrorMsg("Impossibile rimuovere il blocco della prima nota. Rimuoverlo dai dati azienda",,"")
        endif
        bTrsErr=bTrsErr or bErr_04582050
        * --- End
        * --- Salvo Registrazione Saldaconto
         
 this.w_PTSA.cFunction="Query" 
 this.w_PTSA.ecpQuery() 
 this.w_PTSA.ecpQuit()
        * --- Riapre la Gestione in Interroga sulla registrazione salvata
         
 This.bUpdateParentObject=.f. 
 This.oparentobject.bUpdated=.t. 
 This.oparentobject.Loadrec() 
 This.oparentobject.EcpSave()
        * --- Avvio la stampa delle partite generate dal saldaconto attuale...
        if this.w_STAMPA = "S"
           
 L_DATREG = this.oParentObject.w_SCDATREG 
 L_NUMREG = this.oParentObject.w_SCNUMREG 
 L_CODESE = this.oParentObject.w_SCCODESE
          vx_exec("query\gscg_bss.vqr, query\gscg_bss.frx",this)
        endif
      case this.pOPER="E" OR this.pOPER="D" 
        if this.pOPER="D" 
          * --- Passo la verifica dal campo di testata SCRIFCON al campo di dettaglio SCRIFCOR per la cancellazione dell registrazione di Prima Nota collegate al saldaconto....
          if this.oParentObject.w_SCMODGEN = "I"
            * --- Non posso usare la select perch� il cursore da essa prodotto viene eliminato alla  cancellazione della prima registrazione di primanota. (dando quindi errore al ritorno dell'operazione stessa)
            *     Quindi la lista delle registrazione di PN da eliminare vengono prima inserite in un cursore e succesivamente  in un array.
            *     Non posso utilizzare la currtoarr() poich� lavora solo su una riga del cursore e non incrementa in modo corretto  la dimensione dell'array
            vq_exec("query\gscg1bss.vqr",this,"ELEREGPN")
             
 SELECT("ELEREGPN") 
 GO TOP 
 SCAN
            DIMENSION ALISTPN (RECNO())
            STORE ELEREGPN.SCRIFCOR TO ALISTPN(RECNO())
            ENDSCAN
            USE IN SELECT("ELEREGPN")
            INDICE=0
            FOR INDICE=1 to ALEN(ALISTPN,1)
            this.oParentObject.w_RIFCONPN = ALISTPN(INDICE)
            if EMPTY(this.oParentObject.w_RIFCONPN)
              * --- Elimino testata senza righe di dettaglio
              This.oparentobject.ecpDelete()
            else
              * --- Richiamo Primanota associata
              This.oparentobject.Notifyevent("Elimina")
            endif
            NEXT
          else
            if EMPTY(this.oParentObject.w_SCRIFCON)
              * --- Elimino testata senza righe di dettaglio
              This.oparentobject.ecpDelete()
            else
              * --- Richiamo Primanota associata
              this.oParentObject.w_RIFCONPN = this.oParentObject.w_SCRIFCON
              This.oparentobject.Notifyevent("Elimina")
            endif
          endif
           
 i_curform = this.oparentobject 
 this.oparentobject.Active() 
 this.oparentobject.Ecpquery()
          * --- Esegue refresh gestione saldaconto
          This.oparentobject.cFunction="Query"
          This.oparentobject.ecpQuery()
          This.oparentobject.bUpdated=.T.
        else
          * --- Lanciato da evento legge in area manuale della primanota dopo la cancellazione 
          *     esegue refresh gestione saldaconto
          This.oparentobject.cFunction="Query"
          This.oparentobject.ecpQuery()
          This.oparentobject.bUpdated=.T.
        endif
      case this.pOPER="P"
        * --- Istanzio le variabili da passare al report
         
 L_DATREG = this.oParentObject.w_SCDATREG 
 L_NUMREG = this.oParentObject.w_SCNUMREG 
 L_CODESE = this.oParentObject.w_SCCODESE
        vx_exec("query\gscg_bss.vqr, query\gscg_bss.frx",this)
    endcase
  endproc
  proc Try_04584BD0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Leggo la data dei blocco e la data stampa del libro giornale
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZDATBLO,AZSTALIG"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZDATBLO,AZSTALIG;
        from (i_cTable) where;
            AZCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DATBLO = NVL(cp_ToDate(_read_.AZDATBLO),cp_NullValue(_read_.AZDATBLO))
      this.w_STALIG = NVL(cp_ToDate(_read_.AZSTALIG),cp_NullValue(_read_.AZSTALIG))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.oParentObject.w_SCDATREG<=this.w_STALIG AND NOT EMPTY(this.w_STALIG)
      this.w_MESS = "Data di contabilizzazione inferiore a data ultima stampa L.G."
      * --- Raise
      i_Error=this.w_MESS
      return
    else
      if EMPTY(this.w_DATBLO)
        * --- Inserisce <Blocco> per Primanota
        this.w_MESS = BLOC_AZI( .T. , this.oParentObject.w_SCDATREG, .T. ) 
        if not Empty( this.w_MESS )
          ah_ErrorMsg("%1","!","", this.w_MESS )
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          i_retcode = 'stop'
          return
        endif
      else
        * --- UN altro utente ha impostato il blocco - controllo concorrenza
        this.w_MESS = "Prima nota bloccata - verificare semaforo bollati in dati azienda"
        * --- Raise
        i_Error=this.w_MESS
        return
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_04584ED0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_OLDTIPCON = "X"
    this.w_OLDCODCON = REPL("X",15)
    this.w_SEMAFORO = "V"
    * --- begin transaction
    cp_BeginTrs()
    this.w_ROWCLI = 1
    this.w_CPROWNUM = 1
    this.w_SALROW = 0
     
 SELECT * FROM &NC WHERE (Nvl(t_SAIMPSAL,0) <>0 OR Nvl(t_FLSALD," ")="S") AND Nvl(t_SACAOAPE,0)<>0 ; 
 ORDER BY t_SATIPCON,t_SACODCON INTO CURSOR CONTABIL 
 SELECT CONTABIL 
 GO TOP 
 SCAN 
    do case
      case this.oParentObject.w_SCMODGEN = "I" AND this.w_OLDCODCON # t_SACODCON
        this.w_ROWCLI = 1
        this.w_CPROWNUM = 1
        this.w_AGGTES = .F.
        this.w_SEMAFORO = "R"
        this.w_OLDTIPCON = t_SATIPCON
        this.w_OLDCODCON = t_SACODCON
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.oParentObject.w_FLPDOC#"N" 
          i_Conn=i_TableProp[this.AZIENDA_idx,3]
          this.oParentObject.w_SCNUMDOC = 0
          cp_NextTableProg(this.oParentObject,i_Conn,"NDSALDAC","i_codazi,w_CAUSALE,w_SCCODESE,w_PNPRD,w_SERDOC,w_SCNUMDOC")
          * --- Necessario impostare il cursore in scansione
          SELECT CONTABIL
        endif
      case this.oParentObject.w_SCMODGEN = "G" AND this.oParentObject.w_FLPDOC#"N" AND RECNO() = 1
        i_Conn=i_TableProp[this.AZIENDA_idx,3]
        this.oParentObject.w_SCNUMDOC = 0
        cp_NextTableProg(this.oParentObject,i_Conn,"NDSALDAC","i_codazi,w_CAUSALE,w_SCCODESE,w_PNPRD,w_SERDOC,w_SCNUMDOC")
        * --- Necessario impostare il cursore in scansione
        SELECT CONTABIL
    endcase
    if this.w_AGGTES=.F.
      if Empty(this.w_PNDESSUP) and (Not Empty(this.w_CCDESSUP) OR Not Empty(this.w_CCDESRIG))
        * --- Array elenco parametri per descrizioni di riga e testata
         
 ARPARAM[1,1]="NUMDOC" 
 ARPARAM[1,2]=ALLTRIM(STR(this.oParentObject.w_SCNUMDOC)) 
 ARPARAM[2,1]="ALFDOC" 
 ARPARAM[2,2]=this.oParentObject.w_SCALFDOC 
 ARPARAM[3,1]="DATDOC" 
 ARPARAM[3,2]=DTOC(this.oParentObject.w_SCDATDOC) 
 ARPARAM[4,1]="ALFPRO" 
 ARPARAM[4,2]=this.w_PNALFPRO 
 ARPARAM[5,1]="DATREG" 
 ARPARAM[5,2]=DTOC(this.w_PNDATREG) 
 ARPARAM[6,1]="CODCON" 
 ARPARAM[6,2]="" 
 ARPARAM[7,1]="ELEDOC" 
 ARPARAM[7,2]="" 
 ARPARAM[8,1]="CODAGE" 
 ARPARAM[8,2]="" 
 ARPARAM[9,1]="NUMPRO" 
 ARPARAM[9,2]=ALLTRIM(STR(this.w_PNNUMPRO)) 
 ARPARAM[10,1]="NUMPAR" 
 ARPARAM[10,2]="" 
 ARPARAM[11,1]="DATSCA" 
 ARPARAM[11,2]="" 
 ARPARAM[12,1]="TIPCON" 
 ARPARAM[12,2]=""
      endif
      this.w_PNDESSUP = this.oParentObject.w_SCDESSUP
      if Empty(this.w_PNDESSUP) and Not Empty(this.w_CCDESSUP)
         
 ARPARAM[7,1]="ELEDOC" 
 ARPARAM[7,2]=this.w_PNDESSUP
        this.w_PNDESSUP = CALDESPA(this.w_CCDESSUP,@ARPARAM)
      endif
      * --- Esegue un unica registrazione contabile 'per volta'
      this.w_AGGTES = .T.
      * --- Calcola PNSERIAL e PNNUMRER
      this.w_TOTABB = 0
      this.w_PNSERIAL = SPACE(10)
      this.w_PNNUMRER = 0
      this.w_CPROWNUM = 0
      this.w_CPROWORD = 0
      this.w_PNNUMPRO = 0
      this.w_PNPRP = "NN"
      i_Conn=i_TableProp[this.PNT_MAST_IDX, 3]
      cp_NextTableProg(this, i_Conn, "SEPNT", "i_codazi,w_PNSERIAL")
      cp_NextTableProg(this, i_Conn, "PRPNT", "i_codazi,w_PNCODESE,w_PNCODUTE,w_PNPRG,w_PNNUMRER")
      * --- Protocollo (se Gestito)
      if NOT EMPTY(this.w_PNANNPRO)
        cp_NextTableProg(this, i_Conn, "PRPRO", "i_codazi,w_PNANNPRO,w_PNPRP,w_PNALFPRO,w_PNNUMPRO")
      endif
      * --- Segno - per distinguerla dalle Distinte Effetti
      this.w_CODVAP = this.w_PNCODVAL
      this.w_CAOVAP = this.w_PNCAOVAL
      if NOT EMPTY(this.w_DTOBSO) AND this.w_DTOBSO<=this.oParentObject.w_SCDATREG
        * --- Vauta EMU Obsoleta ; Converte in EURO
        this.w_CODVAP = g_CODEUR
        this.w_CAOVAP = 1
      endif
      * --- Insert into PNT_MAST
      i_nConn=i_TableProp[this.PNT_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"PNSERIAL"+",PNFLIVDF"+",PNNUMREG"+",PNTIPDOC"+",PNDATREG"+",PNPRD"+",PNCODCAU"+",PNTIPREG"+",PNCODVAL"+",PNPRG"+",PNCAOVAL"+",PNDESSUP"+",PNCOMPET"+",PNVALNAZ"+",UTCC"+",UTCV"+",UTDC"+",UTDV"+",PNCODESE"+",PNCODUTE"+",PNNUMRER"+",PNDATDOC"+",PNNUMDOC"+",PNALFDOC"+",PNFLPROV"+",PNPRP"+",PNANNDOC"+",PNANNPRO"+",PNALFPRO"+",PNNUMPRO"+",PNRIFDIS"+",PNRIFSAL"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_MAST','PNSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_MAST','PNFLIVDF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMREG),'PNT_MAST','PNNUMREG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPDOC),'PNT_MAST','PNTIPDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_SCDATREG),'PNT_MAST','PNDATREG');
        +","+cp_NullLink(cp_ToStrODBC("NN"),'PNT_MAST','PNPRD');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCAU),'PNT_MAST','PNCODCAU');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPREG),'PNT_MAST','PNTIPREG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODVAP),'PNT_MAST','PNCODVAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNPRG),'PNT_MAST','PNPRG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CAOVAP),'PNT_MAST','PNCAOVAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESSUP),'PNT_MAST','PNDESSUP');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODESE),'PNT_MAST','PNCOMPET');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNVALNAZ),'PNT_MAST','PNVALNAZ');
        +","+cp_NullLink(cp_ToStrODBC(this.w_UTCC),'PNT_MAST','UTCC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_UTCV),'PNT_MAST','UTCV');
        +","+cp_NullLink(cp_ToStrODBC(this.w_UTDC),'PNT_MAST','UTDC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_UTDV),'PNT_MAST','UTDV');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODESE),'PNT_MAST','PNCODESE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODUTE),'PNT_MAST','PNCODUTE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMRER),'PNT_MAST','PNNUMRER');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_SCDATDOC),'PNT_MAST','PNDATDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_SCNUMDOC),'PNT_MAST','PNNUMDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_SCALFDOC),'PNT_MAST','PNALFDOC');
        +","+cp_NullLink(cp_ToStrODBC("N"),'PNT_MAST','PNFLPROV');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNPRP),'PNT_MAST','PNPRP');
        +","+cp_NullLink(cp_ToStrODBC("    "),'PNT_MAST','PNANNDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNANNPRO),'PNT_MAST','PNANNPRO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNALFPRO),'PNT_MAST','PNALFPRO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMPRO),'PNT_MAST','PNNUMPRO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNRIFDIS),'PNT_MAST','PNRIFDIS');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_SCSERIAL),'PNT_MAST','PNRIFSAL');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL,'PNFLIVDF'," ",'PNNUMREG',this.w_PNNUMREG,'PNTIPDOC',this.w_PNTIPDOC,'PNDATREG',this.oParentObject.w_SCDATREG,'PNPRD',"NN",'PNCODCAU',this.w_PNCODCAU,'PNTIPREG',this.w_PNTIPREG,'PNCODVAL',this.w_CODVAP,'PNPRG',this.w_PNPRG,'PNCAOVAL',this.w_CAOVAP,'PNDESSUP',this.w_PNDESSUP)
        insert into (i_cTable) (PNSERIAL,PNFLIVDF,PNNUMREG,PNTIPDOC,PNDATREG,PNPRD,PNCODCAU,PNTIPREG,PNCODVAL,PNPRG,PNCAOVAL,PNDESSUP,PNCOMPET,PNVALNAZ,UTCC,UTCV,UTDC,UTDV,PNCODESE,PNCODUTE,PNNUMRER,PNDATDOC,PNNUMDOC,PNALFDOC,PNFLPROV,PNPRP,PNANNDOC,PNANNPRO,PNALFPRO,PNNUMPRO,PNRIFDIS,PNRIFSAL &i_ccchkf. );
           values (;
             this.w_PNSERIAL;
             ," ";
             ,this.w_PNNUMREG;
             ,this.w_PNTIPDOC;
             ,this.oParentObject.w_SCDATREG;
             ,"NN";
             ,this.w_PNCODCAU;
             ,this.w_PNTIPREG;
             ,this.w_CODVAP;
             ,this.w_PNPRG;
             ,this.w_CAOVAP;
             ,this.w_PNDESSUP;
             ,this.w_PNCODESE;
             ,this.w_PNVALNAZ;
             ,this.w_UTCC;
             ,this.w_UTCV;
             ,this.w_UTDC;
             ,this.w_UTDV;
             ,this.w_PNCODESE;
             ,this.w_PNCODUTE;
             ,this.w_PNNUMRER;
             ,this.oParentObject.w_SCDATDOC;
             ,this.oParentObject.w_SCNUMDOC;
             ,this.oParentObject.w_SCALFDOC;
             ,"N";
             ,this.w_PNPRP;
             ,"    ";
             ,this.w_PNANNPRO;
             ,this.w_PNALFPRO;
             ,this.w_PNNUMPRO;
             ,this.w_PNRIFDIS;
             ,this.oParentObject.w_SCSERIAL;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      this.w_IMPABB = 0
      this.w_CONASS = SPACE(15)
      this.w_OLDCONASS = "@@@@@@@@@"
    endif
    * --- Anticiapata scansione su righe
    * --- Dettaglia Righe Cli/For
    * --- Calcola Differenza Cambio (Passiva in Dare; Attiva in Avere) (PTCAOVAL=C.Apertura; w_CAOVAC=C.Chiusura)
    this.w_CAOVAL = t_SACAOVAL
    this.w_CAOVAC = t_SACAOAPE
    this.w_DATAPE = IIF(EMPTY(t_SADATAPE), this.w_DATRIF, t_SADATAPE)
    if (Nvl(t_SACODVAL," ")<>this.w_PNVALNAZ) OR this.oParentObject.w_SCDATREG<GETVALUT(g_PERVAL, "VADATEUR")
      this.w_APPO1 = MIN(ABS(t_SATOTIMP), ABS(t_SAIMPSAL))
      * --- Fase Pre EURO o Valuta non EURO; Calcola Differenze Cambi
      this.w_VAL1 = VAL2MON(this.w_APPO1, this.w_CAOVAC, this.w_CAONAZ, this.oParentObject.w_SCDATREG, this.w_PNVALNAZ)
      this.w_VAL2 = VAL2MON(this.w_APPO1, this.w_CAOVAL, this.w_CAONAZ, this.w_DATAPE, this.w_PNVALNAZ)
      this.w_APPO1 = cp_ROUND(this.w_VAL1 - this.w_VAL2, 6)
      if NVL(t_SA_SEGNO, "D")="D"
        this.w_IMPDCA = this.w_IMPDCA - IIF(this.w_APPO1>0, 0, ABS(this.w_APPO1))
        this.w_IMPDCP = this.w_IMPDCP + IIF(this.w_APPO1>0, ABS(this.w_APPO1), 0)
      else
        this.w_IMPDCA = this.w_IMPDCA - IIF(this.w_APPO1>0, ABS(this.w_APPO1), 0)
        this.w_IMPDCP = this.w_IMPDCP + IIF(this.w_APPO1>0, 0, ABS(this.w_APPO1))
      endif
      SELECT CONTABIL
    endif
    this.w_PTSERRIF = t_SASERRIF
    this.w_PTORDRIF = t_SAORDRIF
    this.w_PTNUMRIF = t_SANUMRIF
    this.Pag8()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.w_TIPCON<>t_SATIPCON OR this.w_CODCON<>t_SACODCON OR (this.w_OLDCONASS<>this.w_CONASS AND this.oParentObject.w_SCTIPGES="B")
      * --- Scrive la Partita di Chiusura Associata al Cliente/Fornitore
      this.w_CODAGE = NVL(t_SACODAGE, SPACE(5))
      this.w_IMPCLF = cp_ROUND(this.w_IMPCLF, this.w_PERPVL)
      this.w_SEZB = CALCSEZ(this.w_CODCON)
      * --- Scrive nuova Riga Clienti/Fornitori
      this.w_PNTIPCON = this.w_TIPCON
      this.w_PNCODCON = this.w_CODCON
      this.w_PNCODAGE = NVL(t_SACODAGE, SPACE(5))
      this.w_PNFLVABD = NVL(t_SAFLVABD, " ")
      this.w_PNFLPART = "S"
      this.w_APPO = 0
      SELECT CONTABIL
      * --- Creo Riga Cliente\Fornitore
      this.w_APPO = this.w_IMPCLF 
      this.w_CPROWNUM = this.w_ROWCLI
      this.w_CPROWORD = this.w_CPROWNUM*10
      this.w_TIPO = "PI"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_ABBCLI<>0
        * --- Creo riga del cliente Collegata all'abbuono
        this.w_APPO = this.w_ABBCLI
        this.w_CPROWNUM = this.w_ROWCLI
        this.w_CPROWORD = this.w_CPROWNUM*10
        this.w_TIPO = "PA"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_ABBCLI = 0
      endif
      this.w_NUMPAR = Nvl(t_SANUMPAR," ")
      this.w_DATSCA = cp_todate(t_SADATSCA)
      if this.oParentObject.w_SCTIPGES="B"
        this.w_PNCODCON = this.w_PNCODCONASS
        this.w_APPO = (this.w_IMPCON+ (this.w_IMPDCA + this.w_IMPDCP)) * -1
        if Not Empty(this.w_PNCODCON) AND this.w_APPO<>0
          this.w_TIPO = "PI"
          this.w_PNFLPART = "N"
          this.w_PNTIPCON = "G"
          this.w_CPROWNUM = this.w_ROWCLI
          this.w_CPROWORD = this.w_CPROWNUM*10
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_CONBAN = this.w_CONASS
          this.w_CAUMOV = this.w_CAUCC
          this.w_APPVAL = this.w_APPO
          this.w_ROWCC = 0
          this.w_DATVAL = Cp_Chartodate("    -  -  ")
          this.w_ORDCC = this.w_CPROWNUM
          this.Pag6()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_IMPCON = 0
          this.w_IMPDCA = 0
          this.w_IMPDCP = 0
        endif
        * --- Read from COC_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.COC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2],.t.,this.COC_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "BACONCOL"+;
            " from "+i_cTable+" COC_MAST where ";
                +"BACODBAN = "+cp_ToStrODBC(this.w_CONASS);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            BACONCOL;
            from (i_cTable) where;
                BACODBAN = this.w_CONASS;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PNCODCONASS = NVL(cp_ToDate(_read_.BACONCOL),cp_NullValue(_read_.BACONCOL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      SELECT CONTABIL
      this.w_TIPCON = t_SATIPCON
      this.w_CODCON = t_SACODCON
      this.w_ROWCC = 0
    endif
    this.w_PARROW = this.w_PARROW + 1
    this.w_PTDATAPE = IIF(EMPTY(t_SADATAPE), this.w_DATRIF, t_SADATAPE)
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_OLDCONASS = this.w_CONASS
    * --- Totalizza Importo Riga Cliente/Fornitore
    this.w_APPO1 = this.w_PTTOTIMP * IIF(this.w_PT_SEGNO="D", 1, -1)
    this.w_IMPCLF = this.w_IMPCLF + this.w_APPO1
    this.w_IMPCON = this.w_IMPCON + this.w_APPO1
    this.w_PNCODAGE = SPACE(5)
    this.w_PNFLVABD = " "
    Avanzamento = .f.
    if recno() # reccount()
      Skip 1
      Avanzamento = .t.
    endif
    if this.oParentObject.w_SCMODGEN = "I" AND this.w_OLDCODCON # t_SACODCON
      * --- Chiamata  pagina per 'normalizzazione' registrazione
      this.Pag7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_SEMAFORO = "V"
    endif
    if Avanzamento
      Skip -1
      Avanzamento = .f.
    endif
     
 SELECT CONTABIL 
 ENDSCAN
    * --- Test Ultima Riga
    if this.oParentObject.w_SCMODGEN = "G" or this.w_SEMAFORO = "R"
      * --- Chiamata  pagina per 'normalizzazione' registrazione
      this.Pag7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_04582050()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_MESS = BLOC_AZI( .F. ) 
    if not Empty( this.w_MESS )
      ah_ErrorMsg("%1","!","", this.w_MESS)
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inizializza variabili di Lavoro
    * --- Inizializa Valori
    this.w_SERIAL = this.oParentObject.w_SCSERIAL
    this.w_PNFLSALD = "+"
    this.w_IMPCON = 0
    this.w_PNCAOVAL = GETCAM(g_PERVAL, i_DATSYS)
    this.w_IMPCLF = 0
    this.w_ABBCLI = 0
    this.w_UTCC = i_CODUTE
    this.w_UTDC = SetInfoDate( g_CALUTD )
    this.w_UTCV = 0
    this.w_UTDV = cp_CharToDate("  -  -    ")
    this.w_PNDATREG = this.oParentObject.w_SCDATREG
    this.w_PNCODPAG = SPACE(5)
    this.w_PNCODAGE = SPACE(5)
    this.w_PNFLVABD = " "
    this.w_APPO = 0
    this.w_CCFLPART = " "
    this.w_CODAZI = i_CODAZI
    this.w_PNTIPDOC = "  "
    this.w_FLPPRO = "N"
    this.w_DATBLO = cp_CharToDate("  -  -  ")
    this.w_PNTIPREG = "N"
    this.w_PNANNPRO = SPACE(4)
    this.w_STALIG = cp_CharToDate("  -  -  ")
    this.w_PNNUMREG = 0
    this.w_PNCODESE = g_CODESE
    this.w_PNALFPRO = Space(10)
    this.w_IMPDCA = 0
    this.w_CAOVAC = this.oParentObject.w_SCCAOVAL
    this.w_IMPDCP = 0
    this.w_PNCODVAL = iif(Not Empty(this.w_PTSA.w_CODVAL),this.w_PTSA.w_CODVAL,g_PERVAL)
    this.w_TIPCON = "X"
    this.w_PNCAOVAL = ICASE(Not Empty(this.w_PTSA.w_SACAOVAL),this.w_PTSA.w_SACAOVAL,Not Empty(this.oParentObject.w_SCCAOVAL),this.oParentObject.w_SCCAOVAL,g_CAOVAL)
    this.w_CODCON = SPACE(15)
    this.w_PNCODUTE = IIF(g_UNIUTE $ "UE", i_CODUTE, 0)
    this.w_PNPRG = IIF(g_UNIUTE $ "GE", DTOS(this.w_PNDATREG), SPACE(8))
    this.w_PTFLRAGG = " "
    this.w_SALDROW = 0
    this.w_ABBROW = 0
    this.w_VALNAZ = g_PERVAL
    this.w_PNRIFDIS = SPACE(10)
    do case
      case this.oParentObject.w_SCTIPCON="G"
        this.w_PNCODCAU = this.oParentObject.w_SCCAUCOC
      case this.oParentObject.w_SCTIPCON="F"
        this.w_PNCODCAU = this.oParentObject.w_SCCAUCOF
      case this.oParentObject.w_SCTIPCON="C"
        this.w_PNCODCAU = this.oParentObject.w_SCCAUCON
    endcase
    * --- Legge dati collegati alla Causale Contabile
    * --- Read from CAU_CONT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAU_CONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2],.t.,this.CAU_CONT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CCSERPRO,CCTIPDOC,CCTIPREG,CCNUMREG,CCFLPPRO,CCFLPART,CCDESSUP,CCDESRIG,CCFLMOVC,CCCAUMOV,CCCAUSBF"+;
        " from "+i_cTable+" CAU_CONT where ";
            +"CCCODICE = "+cp_ToStrODBC(this.w_PNCODCAU);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CCSERPRO,CCTIPDOC,CCTIPREG,CCNUMREG,CCFLPPRO,CCFLPART,CCDESSUP,CCDESRIG,CCFLMOVC,CCCAUMOV,CCCAUSBF;
        from (i_cTable) where;
            CCCODICE = this.w_PNCODCAU;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PNALFPRO = NVL(cp_ToDate(_read_.CCSERPRO),cp_NullValue(_read_.CCSERPRO))
      this.w_PNTIPDOC = NVL(cp_ToDate(_read_.CCTIPDOC),cp_NullValue(_read_.CCTIPDOC))
      this.w_PNTIPREG = NVL(cp_ToDate(_read_.CCTIPREG),cp_NullValue(_read_.CCTIPREG))
      this.w_PNNUMREG = NVL(cp_ToDate(_read_.CCNUMREG),cp_NullValue(_read_.CCNUMREG))
      this.w_FLPPRO = NVL(cp_ToDate(_read_.CCFLPPRO),cp_NullValue(_read_.CCFLPPRO))
      this.w_CCFLPART = NVL(cp_ToDate(_read_.CCFLPART),cp_NullValue(_read_.CCFLPART))
      this.w_CCDESSUP = NVL(cp_ToDate(_read_.CCDESSUP),cp_NullValue(_read_.CCDESSUP))
      this.w_CCDESRIG = NVL(cp_ToDate(_read_.CCDESRIG),cp_NullValue(_read_.CCDESRIG))
      this.w_FLMOVC = NVL(cp_ToDate(_read_.CCFLMOVC),cp_NullValue(_read_.CCFLMOVC))
      this.w_CAUCC = NVL(cp_ToDate(_read_.CCCAUMOV),cp_NullValue(_read_.CCCAUMOV))
      this.w_CAUSBF = NVL(cp_ToDate(_read_.CCCAUSBF),cp_NullValue(_read_.CCCAUSBF))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    do case
      case this.w_FLPPRO="D"
        this.w_PNANNPRO = CALPRO(this.w_PNDATREG, this.w_PNCODESE, this.w_FLPPRO)
      case this.w_FLPPRO="E"
        this.w_PNANNPRO = this.w_PNCODESE
    endcase
    this.w_PNINICOM = g_INIESE
    this.w_PNFINCOM = g_FINESE
    this.w_PNVALNAZ = g_PERVAL
    this.w_CAONAZ = GETCAM(this.w_PNVALNAZ, this.w_PNDATREG, 0)
    this.w_CAONAZ = IIF(this.w_CAONAZ=0, 1, this.w_CAONAZ)
    this.w_PERPVL = g_PERPVL
    this.w_DATRIF = IIF(EMPTY(this.oParentObject.w_SCDATDOC), this.oParentObject.w_SCDATREG, this.oParentObject.w_SCDATDOC)
    if this.w_PNDATREG<g_INIESE OR this.w_PNDATREG>g_FINESE
      * --- Se di un altro esercizio legge Codice e Divisa
      * --- Select from ESERCIZI
      i_nConn=i_TableProp[this.ESERCIZI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select ESCODESE, ESVALNAZ, ESINIESE, ESFINESE  from "+i_cTable+" ESERCIZI ";
            +" where ESINIESE<="+cp_ToStrODBC(this.w_PNDATREG)+" AND ESFINESE>="+cp_ToStrODBC(this.w_PNDATREG)+" AND ESCODAZI="+cp_ToStrODBC(this.w_CODAZI)+"";
             ,"_Curs_ESERCIZI")
      else
        select ESCODESE, ESVALNAZ, ESINIESE, ESFINESE from (i_cTable);
         where ESINIESE<=this.w_PNDATREG AND ESFINESE>=this.w_PNDATREG AND ESCODAZI=this.w_CODAZI;
          into cursor _Curs_ESERCIZI
      endif
      if used('_Curs_ESERCIZI')
        select _Curs_ESERCIZI
        locate for 1=1
        do while not(eof())
        this.w_PNCODESE = ESCODESE
        this.w_PNVALNAZ = NVL(ESVALNAZ, g_PERVAL)
        this.w_PNINICOM = CP_TODATE(ESINIESE)
        this.w_PNFINCOM = CP_TODATE(ESFINESE)
          select _Curs_ESERCIZI
          continue
        enddo
        use
      endif
    endif
    if this.w_PNVALNAZ<>g_PERVAL
      this.w_CAONAZ = GETCAM(this.w_PNVALNAZ, this.oParentObject.w_SCDATREG, 0)
      this.w_CAONAZ = IIF(this.w_CAONAZ=0, 1, this.w_CAONAZ)
    endif
    this.w_EURVAL = 999
    * --- Calcola Differenza Cambio (Passiva in Dare; Attiva in Avere) (PTCAOVAL=C.Apertura; w_CAOVAC=C.Chiusura)
    if this.w_PNCODVAL<>this.w_PNVALNAZ
      this.w_EURVAL = 0
      this.w_EURVAL = GETCAM(this.w_PNCODVAL,this.w_PNDATREG)
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna Dettaglio Primanota e Saldi
    this.w_PNDESRIG = this.oParentObject.w_SCDESSUP
    if empty(this.w_PNDESRIG) and Not Empty(this.w_CCDESRIG) and this.w_PNTIPCON<>"G"
       
 ARPARAM[8,1]="CODAGE" 
 ARPARAM[8,2]=ALLTRIM(this.w_PNCODAGE) 
 ARPARAM[10,1]="NUMPAR" 
 ARPARAM[10,2]=this.w_NUMPAR 
 ARPARAM[11,1]="DATSCA" 
 ARPARAM[11,2]=DTOC(this.w_DATSCA)
      this.w_PNDESRIG = CALDESPA(this.w_CCDESRIG,@ARPARAM)
    endif
    if this.w_TIPCON <> "X" AND RECCOUNT("contabil") > 0
      this.w_PNIMPDAR = IIF(this.w_APPO>0, this.w_APPO, 0)
      this.w_PNIMPAVE = IIF(this.w_APPO<0, ABS(this.w_APPO), 0)
      * --- Try
      local bErr_044E5AE0
      bErr_044E5AE0=bTrsErr
      this.Try_044E5AE0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- Raise
        i_Error="Impossibile inserire movimento in prima nota"
        return
      endif
      bTrsErr=bTrsErr or bErr_044E5AE0
      * --- End
    endif
  endproc
  proc Try_044E5AE0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Leggo dai Conti il mastro di raggruppamento associato
    * --- Quindi lo utilizzo per fare la calcsez
    * --- Read from CONTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ANCONSUP,ANCODPAG,ANTIPSOT"+;
        " from "+i_cTable+" CONTI where ";
            +"ANCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
            +" and ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ANCONSUP,ANCODPAG,ANTIPSOT;
        from (i_cTable) where;
            ANCODICE = this.w_PNCODCON;
            and ANTIPCON = this.w_PNTIPCON;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CONSUP = NVL(cp_ToDate(_read_.ANCONSUP),cp_NullValue(_read_.ANCONSUP))
      this.w_PNCODPAG = NVL(cp_ToDate(_read_.ANCODPAG),cp_NullValue(_read_.ANCODPAG))
      this.w_TIPSOT = NVL(cp_ToDate(_read_.ANTIPSOT),cp_NullValue(_read_.ANTIPSOT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_PNCODPAG = IIF(NOT EMPTY(this.w_PNCODPAG),g_SAPAGA,this.w_PNCODPAG)
    this.w_SEZB = CALCSEZ(this.w_CONSUP)
    if this.w_SEZB$"PA"
      * --- Se la sezione di Bilancio � un' attivit� o una passivit�, sbianco le date di inizio e fine competenza
      this.w_PNFINCOM = cp_CharToDate("  -  -  ")
      this.w_PNINICOM = cp_CharToDate("  -  -  ")
    endif
    this.w_SQUAD = this.w_SQUAD + (this.w_PNIMPDAR-this.w_PNIMPAVE)
    * --- Creo riga di Primanota Contabile
    GSCG_BGP(this,this.w_TIPO,this.w_PNSERIAL,this.w_CPROWNUM,this.w_CPROWORD,this.w_PNTIPCON,this.w_PNCODCON,this.w_PNCODESE,; 
 this.w_PNIMPDAR,this.w_PNIMPAVE,this.w_PNFLPART,this.w_PNDESRIG,this.w_PNCODCAU,this.w_PNFLSALD," "," ",this.w_PNCODPAG,; 
 this.w_PNCODAGE,this.w_PNFLVABD,this.w_PNINICOM,this.w_PNFINCOM)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Aggiorna Saldo
    GSCG_BGP(this,"SI",SPACE(10),0,0,this.w_PNTIPCON,this.w_PNCODCON,this.w_PNCODESE,this.w_PNIMPDAR,this.w_PNIMPAVE,"",SPACE(35); 
 ,SPACE(5)," "," "," ",SPACE(5),SPACE(5),"",cp_CharToDate("    -  -  "),cp_CharToDate("    -  -  "))
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_ROWCLI = this.w_ROWCLI+1
    if this.oParentObject.w_SCMODGEN = "I" and this.w_PNTIPCON $ "C-F" 
      * --- Write into PNT_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PNT_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PNTIPCLF ="+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCON),'PNT_MAST','PNTIPCLF');
        +",PNCODCLF ="+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'PNT_MAST','PNCODCLF');
            +i_ccchkf ;
        +" where ";
            +"PNSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
               )
      else
        update (i_cTable) set;
            PNTIPCLF = this.w_PNTIPCON;
            ,PNCODCLF = this.w_PNCODCON;
            &i_ccchkf. ;
         where;
            PNSERIAL = this.w_PNSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Azzero contatori  per iniziare elaborazione nuovo cliente\Fornitore
    this.w_IMPCLF = 0
    this.w_PARROW = 0
    this.w_ABBROW = 0
    return


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione Partite
    this.w_VALACC = 0
    this.w_FLACC = Nvl(t_FLACC,0)
    if this.w_FLACC=1 AND t_FLSALD="S"
      this.w_VALACC = Nvl(t_SAIMPSAL,0) - Nvl(t_SATOTIMP,0)
    endif
    this.w_PTROWORD = this.w_ROWCLI
    this.w_PTROWNUM = this.w_PARROW
    this.w_PTTOTIMP = t_SAIMPSAL
    this.w_EURVAL = t_EURVAL
    this.w_PTDATAPE = IIF(EMPTY(t_SADATAPE), this.w_DATRIF, t_SADATAPE)
    this.w_PTDESCRI = NVL(t_SADESRIG,Space(50))
    * --- Se abbuono attivo t_SAIMPABB<0
    this.w_ABB = t_SAIMPABB
    this.w_PTTOTABB = t_SAIMPABB
    this.w_PTIMPDOC = t_IMPDOC
    this.w_PTCODVAL = t_SACODVAL
    this.w_PT_SEGNO = IIF(t_SA_SEGNO="D", "A", "D")
    this.w_TOTABB = this.w_TOTABB + t_SAIMPABB * IIF(t_SA_SEGNO="A", -1, 1)
    * --- Importo Abbuono 
    if this.w_PTCODVAL<>this.w_VALNAZ AND this.w_ABB<0 AND this.w_FLACC<>1
      * --- Se Se Abbuono Attivo da Cliente o Abbuono Passivo a Fornitore scrive una nuova riga con cambio di Chiusura
      * --- Storna la parte riferita al cambio di apertura (w_ABB Negativo)
      this.w_PTTOTIMP = this.w_PTTOTIMP + this.w_ABB
    endif
    * --- Calcola Importo Riga Primanota
    if this.w_ABB<0
      * --- Se abbuono Attivo
      this.w_IMPORTO = ABS(this.w_PTTOTABB) * IIF(t_SA_SEGNO = "A", -1, 1)
    else
      * --- Se abbuono Passivo
      this.w_IMPORTO = ABS(this.w_PTTOTABB) * IIF(t_SA_SEGNO = "A", 1, -1)
    endif
    * --- Se acconto non devo creare riga di abbuono
    this.w_IMPORTO = IIF(this.w_FLACC=1,0,this.w_IMPORTO)
    * --- Creo Partita di Abbuono
    if this.w_IMPORTO<>0
      this.w_ROWORD = this.w_PTROWORD+1
      this.w_ABBROW = this.w_ABBROW +1
      this.w_ROWNUM = this.w_ABBROW
      if this.w_ABB<0
        * --- Se Abbuono Attivo da Cliente o Abbuono Passivo a Fornitore Il cambio va riferito alla Data Attuale
        this.w_PTCAOVAL = t_SACAOVAL
        this.w_PTCAOAPE = t_SACAOVAL
        this.w_PTDATAPE = this.w_DATRIF
      else
        this.w_PTCAOVAL = t_SACAOVAL
        this.w_PTCAOAPE = t_SACAOAPE
      endif
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Aggiorno Totale importo Abuoni
      this.w_IMPABB = this.w_IMPABB + this.w_IMPORTO
      this.w_ABBCLI = this.w_ABBCLI + this.w_IMPORTO
      this.w_IMPORTO = 0
    endif
    * --- Creo partite di chiusura
    if this.w_PTTOTIMP<>0
      this.w_PTCAOVAL = t_SACAOVAL
      this.w_PTCAOAPE = t_SACAOAPE
      this.w_DATVAL = cp_todate(t_SADATSCA)
      GSCG_BBP(this,"S",this.w_PTSERRIF,this.w_PTORDRIF,this.w_PTNUMRIF,this.w_PNSERIAL,this.w_PTROWORD,this.w_PTROWNUM,; 
 ABS(this.w_PTTOTIMP),IIF(this.w_FLACC=1,0,this.w_PTTOTABB),this.w_VALACC," "," ",space(10),this.w_PTCAOVAL,this.w_PTCAOAPE,this.w_PTDATAPE,this.w_PTDESCRI)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.oParentObject.w_SCTIPGES="B"
        * --- Read from PAR_TITE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_TITE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PTNUMDIS"+;
            " from "+i_cTable+" PAR_TITE where ";
                +"PTSERIAL = "+cp_ToStrODBC(this.w_PTSERRIF);
                +" and PTROWORD = "+cp_ToStrODBC(this.w_PTORDRIF);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_PTNUMRIF);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PTNUMDIS;
            from (i_cTable) where;
                PTSERIAL = this.w_PTSERRIF;
                and PTROWORD = this.w_PTORDRIF;
                and CPROWNUM = this.w_PTNUMRIF;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_NUMDIS = NVL(cp_ToDate(_read_.PTNUMDIS),cp_NullValue(_read_.PTNUMDIS))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Recupera il conto banca dalla distinta che ha chiuso la partita 
        if not empty (this.w_NUMDIS)
          * --- Read from DIS_TINT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DIS_TINT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DIS_TINT_idx,2],.t.,this.DIS_TINT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "DIBANRIF"+;
              " from "+i_cTable+" DIS_TINT where ";
                  +"DINUMDIS = "+cp_ToStrODBC(this.w_NUMDIS);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              DIBANRIF;
              from (i_cTable) where;
                  DINUMDIS = this.w_NUMDIS;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CONBAN = NVL(cp_ToDate(_read_.DIBANRIF),cp_NullValue(_read_.DIBANRIF))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from COC_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.COC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2],.t.,this.COC_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "BACONASS"+;
              " from "+i_cTable+" COC_MAST where ";
                  +"BACODBAN = "+cp_ToStrODBC(this.w_CONBAN);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              BACONASS;
              from (i_cTable) where;
                  BACODBAN = this.w_CONBAN;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CONASS = NVL(cp_ToDate(_read_.BACONASS),cp_NullValue(_read_.BACONASS))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        else
          * --- se non c � una distinta pernde un conto sbf che abbia il conto contabile che stiamo stornando
          * --- Read from COC_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.COC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2],.t.,this.COC_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "BACONASS,BACODBAN"+;
              " from "+i_cTable+" COC_MAST where ";
                  +"BACONCOL = "+cp_ToStrODBC(this.w_CODCON);
                  +" and BACONSBF = "+cp_ToStrODBC("S");
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              BACONASS,BACODBAN;
              from (i_cTable) where;
                  BACONCOL = this.w_CODCON;
                  and BACONSBF = "S";
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CONASS = NVL(cp_ToDate(_read_.BACONASS),cp_NullValue(_read_.BACONASS))
            this.w_CONBAN = NVL(cp_ToDate(_read_.BACODBAN),cp_NullValue(_read_.BACODBAN))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        this.w_CAUMOV = this.w_CAUSBF
        this.w_APPVAL = this.w_PTTOTIMP
        this.w_TIPSOT = "B"
        this.w_ORDCC = this.w_PTROWORD
        this.Pag6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      this.w_SALROW = this.w_SALROW+1
      * --- Insert into SALDDACO
      i_nConn=i_TableProp[this.SALDDACO_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDDACO_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDDACO_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"SCSERIAL"+",SCSERRIF"+",SCORDRIF"+",SCNUMRIF"+",CPROWORD"+",SCRIFCOR"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.oParentObject.w_SCSERIAL),'SALDDACO','SCSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'SALDDACO','SCSERRIF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTROWORD),'SALDDACO','SCORDRIF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PARROW),'SALDDACO','SCNUMRIF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_SALROW*10),'SALDDACO','CPROWORD');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'SALDDACO','SCRIFCOR');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'SCSERIAL',this.oParentObject.w_SCSERIAL,'SCSERRIF',this.w_PNSERIAL,'SCORDRIF',this.w_PTROWORD,'SCNUMRIF',this.w_PARROW,'CPROWORD',this.w_SALROW*10,'SCRIFCOR',this.w_PNSERIAL)
        insert into (i_cTable) (SCSERIAL,SCSERRIF,SCORDRIF,SCNUMRIF,CPROWORD,SCRIFCOR &i_ccchkf. );
           values (;
             this.oParentObject.w_SCSERIAL;
             ,this.w_PNSERIAL;
             ,this.w_PTROWORD;
             ,this.w_PARROW;
             ,this.w_SALROW*10;
             ,this.w_PNSERIAL;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error='Errore in inserimento in SALDDACO'
        return
      endif
      * --- Converto eventuale importo in valuta
      if this.w_PTTOTIMP<>0 AND this.w_PTCODVAL<>this.w_VALNAZ 
        this.w_PTTOTIMP = cp_ROUND(VAL2MON(this.w_PTTOTIMP-this.w_VALACC, this.w_PTCAOAPE, this.w_CAONAZ, this.w_PTDATAPE, this.w_VALNAZ),6)
        * --- L'eventuale acconto generico deve avere il cambio attuale
        this.w_PTTOTIMP = this.w_PTTOTIMP + cp_ROUND(VAL2MON(this.w_VALACC, this.w_PTCAOVAL, this.w_CAONAZ, this.w_PTDATAPE, this.w_VALNAZ),6)
        this.w_PTTOTIMP = cp_ROUND(this.w_PTTOTIMP, this.w_PERPVL)
      endif
      if this.w_PTCODVAL<>this.w_VALNAZ AND this.w_ABB<0 AND this.w_FLACC<>1
        * --- Se Se Abbuono Attivo da Cliente o Abbuono Passivo a Fornitore Il cambio va riferito alla Data Attuale
        *      e quindi creare una partita separata nella riga del cliente.
        this.w_IMPORTO = this.w_ABB * IIF(t_SATIPCON="F", -1, 1)
        this.w_PTCAOVAL = t_SACAOVAL
        this.w_PTCAOAPE = t_SACAOVAL
        this.w_PTDATAPE = this.w_DATRIF
        this.w_ROWORD = this.w_PTROWORD 
        this.w_ROWNUM = this.w_PTROWNUM+1
        this.w_PARROW = this.w_PARROW+1
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_IMPORTO = this.w_IMPORTO* IIF(this.w_PT_SEGNO="D", 1, -1)
        this.w_PTTOTIMP = this.w_PTTOTIMP+this.w_IMPORTO
        this.w_IMPORTO = 0
      endif
    endif
    if this.w_VALACC>0
      * --- Inserisce riga Relativa al Saldaconto
      this.w_PARROW = this.w_PARROW + 1
      this.w_SALROW = this.w_SALROW+1
      * --- Insert into SALDDACO
      i_nConn=i_TableProp[this.SALDDACO_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDDACO_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDDACO_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"SCSERIAL"+",SCSERRIF"+",SCORDRIF"+",SCNUMRIF"+",CPROWORD"+",SCRIFCOR"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.oParentObject.w_SCSERIAL),'SALDDACO','SCSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'SALDDACO','SCSERRIF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTROWORD),'SALDDACO','SCORDRIF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PARROW),'SALDDACO','SCNUMRIF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_SALROW*10),'SALDDACO','CPROWORD');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'SALDDACO','SCRIFCOR');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'SCSERIAL',this.oParentObject.w_SCSERIAL,'SCSERRIF',this.w_PNSERIAL,'SCORDRIF',this.w_PTROWORD,'SCNUMRIF',this.w_PARROW,'CPROWORD',this.w_SALROW*10,'SCRIFCOR',this.w_PNSERIAL)
        insert into (i_cTable) (SCSERIAL,SCSERRIF,SCORDRIF,SCNUMRIF,CPROWORD,SCRIFCOR &i_ccchkf. );
           values (;
             this.oParentObject.w_SCSERIAL;
             ,this.w_PNSERIAL;
             ,this.w_PTROWORD;
             ,this.w_PARROW;
             ,this.w_SALROW*10;
             ,this.w_PNSERIAL;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error='Errore in inserimento in SALDDACO'
        return
      endif
    endif
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_SALROW = this.w_SALROW+1
    GSCG_BBP(this,"A",this.w_PTSERRIF,this.w_PTORDRIF,this.w_PTNUMRIF,this.w_PNSERIAL,this.w_ROWORD,; 
 this.w_ROWNUM,this.w_IMPORTO,0,0," "," ",space(10),this.w_PTCAOVAL,this.w_PTCAOAPE,this.w_PTDATAPE,this.w_PTDESCRI)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into SALDDACO
    i_nConn=i_TableProp[this.SALDDACO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDDACO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDDACO_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SCSERIAL"+",SCSERRIF"+",SCORDRIF"+",SCNUMRIF"+",CPROWORD"+",SCRIFCOR"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_SCSERIAL),'SALDDACO','SCSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'SALDDACO','SCSERRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ROWORD),'SALDDACO','SCORDRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'SALDDACO','SCNUMRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SALROW*10),'SALDDACO','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'SALDDACO','SCRIFCOR');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SCSERIAL',this.oParentObject.w_SCSERIAL,'SCSERRIF',this.w_PNSERIAL,'SCORDRIF',this.w_ROWORD,'SCNUMRIF',this.w_ROWNUM,'CPROWORD',this.w_SALROW*10,'SCRIFCOR',this.w_PNSERIAL)
      insert into (i_cTable) (SCSERIAL,SCSERRIF,SCORDRIF,SCNUMRIF,CPROWORD,SCRIFCOR &i_ccchkf. );
         values (;
           this.oParentObject.w_SCSERIAL;
           ,this.w_PNSERIAL;
           ,this.w_ROWORD;
           ,this.w_ROWNUM;
           ,this.w_SALROW*10;
           ,this.w_PNSERIAL;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    if this.w_PTCODVAL<>this.w_VALNAZ AND this.w_IMPORTO<>0 
      * --- Se Valuta...   Il cambio va riferito alla Data Attuale
      this.w_IMPORTO = cp_ROUND(VAL2MON(this.w_IMPORTO, this.w_PTCAOAPE, this.w_CAONAZ, this.w_PTDATAPE, this.w_VALNAZ),6)
      this.w_IMPORTO = cp_ROUND(this.w_IMPORTO, g_PERPVL)
    endif
  endproc


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Genero Relativi Movimenti C\C
    * --- Read from COC_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.COC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2],.t.,this.COC_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "BACALFES,BATIPCON,BACODVAL"+;
        " from "+i_cTable+" COC_MAST where ";
            +"BACODBAN = "+cp_ToStrODBC(this.w_CONBAN);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        BACALFES,BATIPCON,BACODVAL;
        from (i_cTable) where;
            BACODBAN = this.w_CONBAN;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CALFES = NVL(cp_ToDate(_read_.BACALFES),cp_NullValue(_read_.BACALFES))
      this.w_TIPBAN = NVL(cp_ToDate(_read_.BATIPCON),cp_NullValue(_read_.BATIPCON))
      this.w_CODVAL = NVL(cp_ToDate(_read_.BACODVAL),cp_NullValue(_read_.BACODVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if g_BANC="S" AND this.w_FLMOVC="S" AND this.w_TIPSOT="B"
      * --- Se presente modulo C\C e la causale contabile movimenta i C\C
      * --- Select from gscg_bmc
      do vq_exec with 'gscg_bmc',this,'_Curs_gscg_bmc','',.f.,.t.
      if used('_Curs_gscg_bmc')
        select _Curs_gscg_bmc
        locate for 1=1
        do while not(eof())
        this.w_TIPCAU = _Curs_gscg_bmc.CATIPCON
        this.w_FLCRDE = _Curs_gscg_bmc.CAFLCRDE
          select _Curs_gscg_bmc
          continue
        enddo
        use
      endif
      if this.w_CODVAL<>this.w_PNCODVAL
        * --- Raise
        i_Error="Valuta del conto corrente incongruente con la valuta della partita"
        return
      else
        * --- Read from CCC_DETT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CCC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CCC_DETT_idx,2],.t.,this.CCC_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CANUMCOR"+;
            " from "+i_cTable+" CCC_DETT where ";
                +"CACODICE = "+cp_ToStrODBC(this.w_CAUMOV);
                +" and CANUMCOR = "+cp_ToStrODBC(this.w_CONBAN);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CANUMCOR;
            from (i_cTable) where;
                CACODICE = this.w_CAUMOV;
                and CANUMCOR = this.w_CONBAN;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CONBAN = NVL(cp_ToDate(_read_.CANUMCOR),cp_NullValue(_read_.CANUMCOR))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_ROWS=0
          * --- Raise
          i_Error="Conto corrente incongruente con la causale movimenti C\C"
          return
        else
          this.w_IMPDEB = IIF(this.w_FLCRDE="C",0,ABS(this.w_APPVAL))
          this.w_IMPCRE = IIF(this.w_FLCRDE="C",ABS(this.w_APPVAL),0)
          this.w_CCFLCRED = IIF(this.w_FLCRDE="C", "+", " ")
          this.w_CCFLDEBI = IIF(this.w_FLCRDE="D", "+", " ")
          this.w_CCFLCOMM = "+"
          this.w_DATVAL = IIF(Not Empty(this.w_DATVAL),this.w_DATVAL,CALCFEST(this.w_PNDATREG, this.w_CALFES, this.w_CONBAN, this.w_CAUMOV, this.w_TIPCAU, 1))
          this.w_ROWCC = this.w_ROWCC + 1
          * --- Insert into CCM_DETT
          i_nConn=i_TableProp[this.CCM_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CCM_DETT_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CCM_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"CCSERIAL"+",CCROWRIF"+",CCCODCAU"+",CCNUMCOR"+",CCDATVAL"+",CCIMPCRE"+",CCIMPDEB"+",CPROWNUM"+",CPROWORD"+",CCFLDEBI"+",CCFLCRED"+",CCFLCOMM"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'CCM_DETT','CCSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ORDCC),'CCM_DETT','CCROWRIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CAUMOV),'CCM_DETT','CCCODCAU');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CONBAN),'CCM_DETT','CCNUMCOR');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DATVAL),'CCM_DETT','CCDATVAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_IMPCRE),'CCM_DETT','CCIMPCRE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_IMPDEB),'CCM_DETT','CCIMPDEB');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ROWCC),'CCM_DETT','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ROWCC*10),'CCM_DETT','CPROWORD');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLDEBI),'CCM_DETT','CCFLDEBI');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLCRED),'CCM_DETT','CCFLCRED');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLCOMM),'CCM_DETT','CCFLCOMM');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'CCSERIAL',this.w_PNSERIAL,'CCROWRIF',this.w_ORDCC,'CCCODCAU',this.w_CAUMOV,'CCNUMCOR',this.w_CONBAN,'CCDATVAL',this.w_DATVAL,'CCIMPCRE',this.w_IMPCRE,'CCIMPDEB',this.w_IMPDEB,'CPROWNUM',this.w_ROWCC,'CPROWORD',this.w_ROWCC*10,'CCFLDEBI',this.w_CCFLDEBI,'CCFLCRED',this.w_CCFLCRED,'CCFLCOMM',this.w_CCFLCOMM)
            insert into (i_cTable) (CCSERIAL,CCROWRIF,CCCODCAU,CCNUMCOR,CCDATVAL,CCIMPCRE,CCIMPDEB,CPROWNUM,CPROWORD,CCFLDEBI,CCFLCRED,CCFLCOMM &i_ccchkf. );
               values (;
                 this.w_PNSERIAL;
                 ,this.w_ORDCC;
                 ,this.w_CAUMOV;
                 ,this.w_CONBAN;
                 ,this.w_DATVAL;
                 ,this.w_IMPCRE;
                 ,this.w_IMPDEB;
                 ,this.w_ROWCC;
                 ,this.w_ROWCC*10;
                 ,this.w_CCFLDEBI;
                 ,this.w_CCFLCRED;
                 ,this.w_CCFLCOMM;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          * --- Aggiorno Saldi C\C
          * --- Write into COC_MAST
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.COC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.COC_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"BASALCRE =BASALCRE+ "+cp_ToStrODBC(this.w_IMPCRE);
            +",BASALDEB =BASALDEB+ "+cp_ToStrODBC(this.w_IMPDEB);
                +i_ccchkf ;
            +" where ";
                +"BACODBAN = "+cp_ToStrODBC(this.w_CONBAN);
                   )
          else
            update (i_cTable) set;
                BASALCRE = BASALCRE + this.w_IMPCRE;
                ,BASALDEB = BASALDEB + this.w_IMPDEB;
                &i_ccchkf. ;
             where;
                BACODBAN = this.w_CONBAN;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
    endif
  endproc


  procedure Pag7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_PNTIPCON = this.w_TIPCON
    this.w_PNCODCON = this.w_CODCON
    this.w_PNFLPART = "S"
    this.w_APPO = this.w_IMPCLF 
    this.w_CPROWNUM = this.w_ROWCLI
    this.w_CPROWORD = this.w_CPROWNUM*10
    this.w_TIPO = "PI"
    SELECT CONTABIL
    this.w_PNCODAGE = this.w_CODAGE
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_IMPCLF = 0
    if this.w_ABBCLI<>0
      this.w_APPO = this.w_ABBCLI
      this.w_CPROWNUM = this.w_ROWCLI
      this.w_CPROWORD = this.w_CPROWNUM*10
      this.w_TIPO = "PA"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_ABBCLI = 0
    endif
    if this.oParentObject.w_SCTIPGES="B"
      * --- Read from COC_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.COC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2],.t.,this.COC_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "BACONCOL"+;
          " from "+i_cTable+" COC_MAST where ";
              +"BACODBAN = "+cp_ToStrODBC(this.w_CONASS);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          BACONCOL;
          from (i_cTable) where;
              BACODBAN = this.w_CONASS;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PNCODCONASS = NVL(cp_ToDate(_read_.BACONCOL),cp_NullValue(_read_.BACONCOL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_PNCODCON = this.w_PNCODCONASS
      if Not Empty(this.w_PNCODCON)
        this.w_TIPO = "PI"
        this.w_PNFLPART = "N"
        this.w_PNTIPCON = "G"
        this.w_ROWCLI = this.w_ROWCLI + 1
        this.w_CPROWNUM = this.w_ROWCLI
        this.w_CPROWORD = this.w_CPROWNUM*10
        this.w_APPO = (this.w_IMPCON+ (this.w_IMPDCA + this.w_IMPDCP)) * -1
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_CONBAN = this.w_CONASS
        this.w_CAUMOV = this.w_CAUCC
        this.w_APPVAL = this.w_APPO
        this.w_ROWCC = 0
        this.w_DATVAL = Cp_Chartodate("    -  -  ")
        this.w_ORDCC = this.w_CPROWNUM
        this.Pag6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_IMPCON = 0
        this.w_IMPDCA = 0
        this.w_IMPDCP = 0
      endif
    endif
    if this.w_AGGTES=.T.
      * --- Scrive Reg.Contabili (Movimentazione)
      this.w_PNFLPART = "N"
      this.w_PNTIPCON = "G"
      this.w_PNCODPAG = SPACE(5)
      this.w_PNCODAGE = SPACE(5)
      this.w_PNFLVABD = " "
      * --- Importo Riga Effetti Attivi: Totale Clienti - (Diff.Cambio Attiva + Passiva)
      this.w_IMPCON = cp_ROUND(this.w_IMPCON, this.w_PERPVL)
      this.w_IMPDCA = cp_ROUND(this.w_IMPDCA, this.w_PERPVL)
      this.w_IMPDCP = cp_ROUND(this.w_IMPDCP, this.w_PERPVL)
      this.w_IMPABB = cp_ROUND(this.w_IMPABB, this.w_PERPVL)
      if this.oParentObject.w_SCTIPGES="S"
        * --- Solo se gestione saldaconto
        this.w_IMPCON = (this.w_IMPCON + (this.w_IMPDCA + this.w_IMPDCP)) * -1
      endif
      * --- Riga di Abbuono  Attivo\Passivo
      if this.w_IMPABB<>0
        this.w_APPO = -this.w_IMPABB
        this.w_PNCODCON = IIF( this.w_TOTABB>0,this.oParentObject.w_CONABBP,this.oParentObject.w_CONABBA)
        this.w_CPROWNUM = this.w_CPROWNUM+1
        this.w_CPROWORD = this.w_CPROWORD+10
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Differenza Cambi Attiva
      if this.w_IMPDCA<>0
        this.w_PNCODCON = this.oParentObject.w_SCCONDCA
        this.w_APPO = this.w_IMPDCA
        this.w_CPROWNUM = this.w_CPROWNUM + 1
        this.w_CPROWORD = this.w_CPROWORD + 10
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Differenza Cambi Passiva
      if this.w_IMPDCP<>0
        this.w_PNCODCON = this.oParentObject.w_SCCONDCP
        this.w_APPO = this.w_IMPDCP
        this.w_CPROWNUM = this.w_CPROWNUM + 1
        this.w_CPROWORD = this.w_CPROWORD + 10
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- A Credito: Dare / a Debito: Avere
      if this.w_IMPCON<>0
        this.w_PNCODCON = this.oParentObject.w_SCCONTRO
        this.w_APPO = this.w_IMPCON
        this.w_CPROWNUM = this.w_CPROWNUM + 1
        this.w_CPROWORD = this.w_CPROWORD + 10
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    * --- Memorizzo riferimento alla primanota
    if NOT EMPTY(this.w_PNSERIAL)
      * --- Write into SALMDACO
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALMDACO_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALMDACO_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALMDACO_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SCRIFCON ="+cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'SALMDACO','SCRIFCON');
        +",SCDESSUP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_SCDESSUP),'SALMDACO','SCDESSUP');
            +i_ccchkf ;
        +" where ";
            +"SCSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
               )
      else
        update (i_cTable) set;
            SCRIFCON = this.w_PNSERIAL;
            ,SCDESSUP = this.oParentObject.w_SCDESSUP;
            &i_ccchkf. ;
         where;
            SCSERIAL = this.w_SERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    if this.w_SQUAD<>0 OR this.w_AGGTES=.F.
      if this.w_AGGTES=.F.
        this.w_MESS = "ATTENZIONE%0Registrazione di contabilizzazione da saldaconto non generata%0Confermi ugualmente?"
      else
        this.w_MESS = "ATTENZIONE%0Errore di quadratura (%1)%0nella registrazione di contabilizzazione da saldaconto%0Confermi ugualmente?"
      endif
      if NOT ah_YesNo(this.w_MESS,"", ALLTRIM(STR(this.w_SQUAD)) )
        * --- Raise
        i_Error="Errore durante la generazione del movimento contabile; operazione annullata"
        return
      endif
    endif
    if Isalt()
      * --- Aggiorno stato incasso
      gsal_baf(this,this.w_PNSERIAL,"P",Cp_chartodate("  -  -    "),Cp_chartodate("  -  -    "),"A")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  procedure Pag8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.oParentObject.w_SCTIPGES="B"
      * --- Read from PAR_TITE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PTNUMDIS"+;
          " from "+i_cTable+" PAR_TITE where ";
              +"PTSERIAL = "+cp_ToStrODBC(this.w_PTSERRIF);
              +" and PTROWORD = "+cp_ToStrODBC(this.w_PTORDRIF);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_PTNUMRIF);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PTNUMDIS;
          from (i_cTable) where;
              PTSERIAL = this.w_PTSERRIF;
              and PTROWORD = this.w_PTORDRIF;
              and CPROWNUM = this.w_PTNUMRIF;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_NUMDIS = NVL(cp_ToDate(_read_.PTNUMDIS),cp_NullValue(_read_.PTNUMDIS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Recupera il conto banca dalla distinta che ha chiuso la partita 
      if not empty (this.w_NUMDIS)
        * --- Read from DIS_TINT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DIS_TINT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DIS_TINT_idx,2],.t.,this.DIS_TINT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DIBANRIF"+;
            " from "+i_cTable+" DIS_TINT where ";
                +"DINUMDIS = "+cp_ToStrODBC(this.w_NUMDIS);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DIBANRIF;
            from (i_cTable) where;
                DINUMDIS = this.w_NUMDIS;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CONBAN = NVL(cp_ToDate(_read_.DIBANRIF),cp_NullValue(_read_.DIBANRIF))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from COC_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.COC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2],.t.,this.COC_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "BACONASS"+;
            " from "+i_cTable+" COC_MAST where ";
                +"BACODBAN = "+cp_ToStrODBC(this.w_CONBAN);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            BACONASS;
            from (i_cTable) where;
                BACODBAN = this.w_CONBAN;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CONASS = NVL(cp_ToDate(_read_.BACONASS),cp_NullValue(_read_.BACONASS))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      else
        * --- se non c � una distinta pernde un conto sbf che abbia il conto contabile che stiamo stornando
        * --- Read from COC_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.COC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2],.t.,this.COC_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "BACONASS,BACODBAN"+;
            " from "+i_cTable+" COC_MAST where ";
                +"BACONCOL = "+cp_ToStrODBC(this.w_CODCON);
                +" and BACONSBF = "+cp_ToStrODBC("S");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            BACONASS,BACODBAN;
            from (i_cTable) where;
                BACONCOL = this.w_CODCON;
                and BACONSBF = "S";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CONASS = NVL(cp_ToDate(_read_.BACONASS),cp_NullValue(_read_.BACONASS))
          this.w_CONBAN = NVL(cp_ToDate(_read_.BACODBAN),cp_NullValue(_read_.BACODBAN))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
    endif
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,16)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='CAU_CONT'
    this.cWorkTables[3]='ESERCIZI'
    this.cWorkTables[4]='VALUTE'
    this.cWorkTables[5]='PNT_DETT'
    this.cWorkTables[6]='SALDIART'
    this.cWorkTables[7]='CONTI'
    this.cWorkTables[8]='PAR_TITE'
    this.cWorkTables[9]='SALDDACO'
    this.cWorkTables[10]='PNT_MAST'
    this.cWorkTables[11]='SALMDACO'
    this.cWorkTables[12]='CONTROPA'
    this.cWorkTables[13]='COC_MAST'
    this.cWorkTables[14]='CCM_DETT'
    this.cWorkTables[15]='CCC_DETT'
    this.cWorkTables[16]='DIS_TINT'
    return(this.OpenAllTables(16))

  proc CloseCursors()
    if used('_Curs_ESERCIZI')
      use in _Curs_ESERCIZI
    endif
    if used('_Curs_gscg_bmc')
      use in _Curs_gscg_bmc
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
