* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_aes                                                        *
*              Estensioni allegati                                             *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-05-13                                                      *
* Last revis.: 2008-09-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_aes"))

* --- Class definition
define class tgsut_aes as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 505
  Height = 127+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-01"
  HelpContextID=76104553
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Constant Properties
  EXT_ENS_IDX = 0
  TIP_ALLE_IDX = 0
  CLA_ALLE_IDX = 0
  cFile = "EXT_ENS"
  cKeySelect = "EXCODICE"
  cKeyWhere  = "EXCODICE=this.w_EXCODICE"
  cKeyWhereODBC = '"EXCODICE="+cp_ToStrODBC(this.w_EXCODICE)';

  cKeyWhereODBCqualified = '"EXT_ENS.EXCODICE="+cp_ToStrODBC(this.w_EXCODICE)';

  cPrg = "gsut_aes"
  cComment = "Estensioni allegati"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_EXCODICE = space(10)
  w_EXDESCRI = space(40)
  w_EXTIPALL = space(5)
  w_EXCLAALL = space(5)
  w_DESALL = space(40)
  w_DESCLA = space(40)
  w_EXESTCBI = space(4)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'EXT_ENS','gsut_aes')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_aesPag1","gsut_aes",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Estensioni allegati")
      .Pages(1).HelpContextID = 149810956
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oEXCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='TIP_ALLE'
    this.cWorkTables[2]='CLA_ALLE'
    this.cWorkTables[3]='EXT_ENS'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.EXT_ENS_IDX,5],7]
    this.nPostItConn=i_TableProp[this.EXT_ENS_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_EXCODICE = NVL(EXCODICE,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_4_joined
    link_1_4_joined=.f.
    local link_1_6_joined
    link_1_6_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from EXT_ENS where EXCODICE=KeySet.EXCODICE
    *
    i_nConn = i_TableProp[this.EXT_ENS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.EXT_ENS_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('EXT_ENS')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "EXT_ENS.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' EXT_ENS '
      link_1_4_joined=this.AddJoinedLink_1_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_6_joined=this.AddJoinedLink_1_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'EXCODICE',this.w_EXCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESALL = space(40)
        .w_DESCLA = space(40)
        .w_EXCODICE = NVL(EXCODICE,space(10))
        .w_EXDESCRI = NVL(EXDESCRI,space(40))
        .w_EXTIPALL = NVL(EXTIPALL,space(5))
          if link_1_4_joined
            this.w_EXTIPALL = NVL(TACODICE104,NVL(this.w_EXTIPALL,space(5)))
            this.w_DESALL = NVL(TADESCRI104,space(40))
          else
          .link_1_4('Load')
          endif
        .w_EXCLAALL = NVL(EXCLAALL,space(5))
          if link_1_6_joined
            this.w_EXCLAALL = NVL(TACODCLA106,NVL(this.w_EXCLAALL,space(5)))
            this.w_DESCLA = NVL(TACLADES106,space(40))
          else
          .link_1_6('Load')
          endif
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
        .w_EXESTCBI = NVL(EXESTCBI,space(4))
        cp_LoadRecExtFlds(this,'EXT_ENS')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_EXCODICE = space(10)
      .w_EXDESCRI = space(40)
      .w_EXTIPALL = space(5)
      .w_EXCLAALL = space(5)
      .w_DESALL = space(40)
      .w_DESCLA = space(40)
      .w_EXESTCBI = space(4)
      if .cFunction<>"Filter"
        .DoRTCalc(1,3,.f.)
          if not(empty(.w_EXTIPALL))
          .link_1_4('Full')
          endif
        .DoRTCalc(4,4,.f.)
          if not(empty(.w_EXCLAALL))
          .link_1_6('Full')
          endif
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'EXT_ENS')
    this.DoRTCalc(5,7,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oEXCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oEXDESCRI_1_3.enabled = i_bVal
      .Page1.oPag.oEXTIPALL_1_4.enabled = i_bVal
      .Page1.oPag.oEXCLAALL_1_6.enabled = i_bVal
      .Page1.oPag.oEXESTCBI_1_11.enabled = i_bVal
      .Page1.oPag.oObj_1_10.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oEXCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oEXCODICE_1_1.enabled = .t.
        .Page1.oPag.oEXDESCRI_1_3.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'EXT_ENS',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.EXT_ENS_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EXCODICE,"EXCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EXDESCRI,"EXDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EXTIPALL,"EXTIPALL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EXCLAALL,"EXCLAALL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EXESTCBI,"EXESTCBI",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.EXT_ENS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.EXT_ENS_IDX,2])
    i_lTable = "EXT_ENS"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.EXT_ENS_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.EXT_ENS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.EXT_ENS_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.EXT_ENS_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into EXT_ENS
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'EXT_ENS')
        i_extval=cp_InsertValODBCExtFlds(this,'EXT_ENS')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(EXCODICE,EXDESCRI,EXTIPALL,EXCLAALL,EXESTCBI "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_EXCODICE)+;
                  ","+cp_ToStrODBC(this.w_EXDESCRI)+;
                  ","+cp_ToStrODBCNull(this.w_EXTIPALL)+;
                  ","+cp_ToStrODBCNull(this.w_EXCLAALL)+;
                  ","+cp_ToStrODBC(this.w_EXESTCBI)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'EXT_ENS')
        i_extval=cp_InsertValVFPExtFlds(this,'EXT_ENS')
        cp_CheckDeletedKey(i_cTable,0,'EXCODICE',this.w_EXCODICE)
        INSERT INTO (i_cTable);
              (EXCODICE,EXDESCRI,EXTIPALL,EXCLAALL,EXESTCBI  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_EXCODICE;
                  ,this.w_EXDESCRI;
                  ,this.w_EXTIPALL;
                  ,this.w_EXCLAALL;
                  ,this.w_EXESTCBI;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.EXT_ENS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.EXT_ENS_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.EXT_ENS_IDX,i_nConn)
      *
      * update EXT_ENS
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'EXT_ENS')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " EXDESCRI="+cp_ToStrODBC(this.w_EXDESCRI)+;
             ",EXTIPALL="+cp_ToStrODBCNull(this.w_EXTIPALL)+;
             ",EXCLAALL="+cp_ToStrODBCNull(this.w_EXCLAALL)+;
             ",EXESTCBI="+cp_ToStrODBC(this.w_EXESTCBI)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'EXT_ENS')
        i_cWhere = cp_PKFox(i_cTable  ,'EXCODICE',this.w_EXCODICE  )
        UPDATE (i_cTable) SET;
              EXDESCRI=this.w_EXDESCRI;
             ,EXTIPALL=this.w_EXTIPALL;
             ,EXCLAALL=this.w_EXCLAALL;
             ,EXESTCBI=this.w_EXESTCBI;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.EXT_ENS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.EXT_ENS_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.EXT_ENS_IDX,i_nConn)
      *
      * delete EXT_ENS
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'EXCODICE',this.w_EXCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.EXT_ENS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.EXT_ENS_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,7,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oEXCLAALL_1_6.enabled = this.oPgFrm.Page1.oPag.oEXCLAALL_1_6.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_10.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=EXTIPALL
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_ALLE_IDX,3]
    i_lTable = "TIP_ALLE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_ALLE_IDX,2], .t., this.TIP_ALLE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_ALLE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EXTIPALL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSUT_MTA',True,'TIP_ALLE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TACODICE like "+cp_ToStrODBC(trim(this.w_EXTIPALL)+"%");

          i_ret=cp_SQL(i_nConn,"select TACODICE,TADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TACODICE',trim(this.w_EXTIPALL))
          select TACODICE,TADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_EXTIPALL)==trim(_Link_.TACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_EXTIPALL) and !this.bDontReportError
            deferred_cp_zoom('TIP_ALLE','*','TACODICE',cp_AbsName(oSource.parent,'oEXTIPALL_1_4'),i_cWhere,'GSUT_MTA',"Tipologie allegati",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODICE',oSource.xKey(1))
            select TACODICE,TADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EXTIPALL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TACODICE="+cp_ToStrODBC(this.w_EXTIPALL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODICE',this.w_EXTIPALL)
            select TACODICE,TADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EXTIPALL = NVL(_Link_.TACODICE,space(5))
      this.w_DESALL = NVL(_Link_.TADESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_EXTIPALL = space(5)
      endif
      this.w_DESALL = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_ALLE_IDX,2])+'\'+cp_ToStr(_Link_.TACODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_ALLE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EXTIPALL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIP_ALLE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIP_ALLE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_4.TACODICE as TACODICE104"+ ",link_1_4.TADESCRI as TADESCRI104"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_4 on EXT_ENS.EXTIPALL=link_1_4.TACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_4"
          i_cKey=i_cKey+'+" and EXT_ENS.EXTIPALL=link_1_4.TACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=EXCLAALL
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_ALLE_IDX,3]
    i_lTable = "CLA_ALLE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_ALLE_IDX,2], .t., this.CLA_ALLE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_ALLE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EXCLAALL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CLA_ALLE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TACODCLA like "+cp_ToStrODBC(trim(this.w_EXCLAALL)+"%");
                   +" and TACODICE="+cp_ToStrODBC(this.w_EXTIPALL);

          i_ret=cp_SQL(i_nConn,"select TACODICE,TACODCLA,TACLADES";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TACODICE,TACODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TACODICE',this.w_EXTIPALL;
                     ,'TACODCLA',trim(this.w_EXCLAALL))
          select TACODICE,TACODCLA,TACLADES;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TACODICE,TACODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_EXCLAALL)==trim(_Link_.TACODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_EXCLAALL) and !this.bDontReportError
            deferred_cp_zoom('CLA_ALLE','*','TACODICE,TACODCLA',cp_AbsName(oSource.parent,'oEXCLAALL_1_6'),i_cWhere,'',"Classi allegati",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_EXTIPALL<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TACODCLA,TACLADES";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TACODICE,TACODCLA,TACLADES;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TACODCLA,TACLADES";
                     +" from "+i_cTable+" "+i_lTable+" where TACODCLA="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TACODICE="+cp_ToStrODBC(this.w_EXTIPALL);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODICE',oSource.xKey(1);
                       ,'TACODCLA',oSource.xKey(2))
            select TACODICE,TACODCLA,TACLADES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EXCLAALL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TACODCLA,TACLADES";
                   +" from "+i_cTable+" "+i_lTable+" where TACODCLA="+cp_ToStrODBC(this.w_EXCLAALL);
                   +" and TACODICE="+cp_ToStrODBC(this.w_EXTIPALL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODICE',this.w_EXTIPALL;
                       ,'TACODCLA',this.w_EXCLAALL)
            select TACODICE,TACODCLA,TACLADES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EXCLAALL = NVL(_Link_.TACODCLA,space(5))
      this.w_DESCLA = NVL(_Link_.TACLADES,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_EXCLAALL = space(5)
      endif
      this.w_DESCLA = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_ALLE_IDX,2])+'\'+cp_ToStr(_Link_.TACODICE,1)+'\'+cp_ToStr(_Link_.TACODCLA,1)
      cp_ShowWarn(i_cKey,this.CLA_ALLE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EXCLAALL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CLA_ALLE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CLA_ALLE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_6.TACODCLA as TACODCLA106"+ ",link_1_6.TACLADES as TACLADES106"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_6 on EXT_ENS.EXCLAALL=link_1_6.TACODCLA"+" and EXT_ENS.EXTIPALL=link_1_6.TACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_6"
          i_cKey=i_cKey+'+" and EXT_ENS.EXCLAALL=link_1_6.TACODCLA(+)"'+'+" and EXT_ENS.EXTIPALL=link_1_6.TACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oEXCODICE_1_1.value==this.w_EXCODICE)
      this.oPgFrm.Page1.oPag.oEXCODICE_1_1.value=this.w_EXCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oEXDESCRI_1_3.value==this.w_EXDESCRI)
      this.oPgFrm.Page1.oPag.oEXDESCRI_1_3.value=this.w_EXDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oEXTIPALL_1_4.value==this.w_EXTIPALL)
      this.oPgFrm.Page1.oPag.oEXTIPALL_1_4.value=this.w_EXTIPALL
    endif
    if not(this.oPgFrm.Page1.oPag.oEXCLAALL_1_6.value==this.w_EXCLAALL)
      this.oPgFrm.Page1.oPag.oEXCLAALL_1_6.value=this.w_EXCLAALL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESALL_1_8.value==this.w_DESALL)
      this.oPgFrm.Page1.oPag.oDESALL_1_8.value=this.w_DESALL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLA_1_9.value==this.w_DESCLA)
      this.oPgFrm.Page1.oPag.oDESCLA_1_9.value=this.w_DESCLA
    endif
    if not(this.oPgFrm.Page1.oPag.oEXESTCBI_1_11.value==this.w_EXESTCBI)
      this.oPgFrm.Page1.oPag.oEXESTCBI_1_11.value=this.w_EXESTCBI
    endif
    cp_SetControlsValueExtFlds(this,'EXT_ENS')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_EXTIPALL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEXTIPALL_1_4.SetFocus()
            i_bnoObbl = !empty(.w_EXTIPALL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_EXCLAALL))  and (Not Empty(.w_EXTIPALL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEXCLAALL_1_6.SetFocus()
            i_bnoObbl = !empty(.w_EXCLAALL)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsut_aesPag1 as StdContainer
  Width  = 501
  height = 127
  stdWidth  = 501
  stdheight = 127
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oEXCODICE_1_1 as StdField with uid="QXYTQQRYTQ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_EXCODICE", cQueryName = "EXCODICE",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Estensione file allegati: alla estensione possono essere legate tipologia e classe allegato",;
    HelpContextID = 151669131,;
   bGlobalFont=.t.,;
    Height=21, Width=88, Left=102, Top=13, InputMask=replicate('X',10)

  add object oEXDESCRI_1_3 as StdField with uid="HKHQGDMCFJ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_EXDESCRI", cQueryName = "EXDESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 66083215,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=194, Top=13, InputMask=replicate('X',40)

  add object oEXTIPALL_1_4 as StdField with uid="YQTPNUVHEY",rtseq=3,rtrep=.f.,;
    cFormVar = "w_EXTIPALL", cQueryName = "EXTIPALL",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipologia allegato legata all'estensione: verr� utilizzata nella associazione file con questa estensione",;
    HelpContextID = 238724718,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=102, Top=43, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_ALLE", cZoomOnZoom="GSUT_MTA", oKey_1_1="TACODICE", oKey_1_2="this.w_EXTIPALL"

  func oEXTIPALL_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
      if .not. empty(.w_EXCLAALL)
        bRes2=.link_1_6('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oEXTIPALL_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oEXTIPALL_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_ALLE','*','TACODICE',cp_AbsName(this.parent,'oEXTIPALL_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSUT_MTA',"Tipologie allegati",'',this.parent.oContained
  endproc
  proc oEXTIPALL_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSUT_MTA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TACODICE=this.parent.oContained.w_EXTIPALL
     i_obj.ecpSave()
  endproc

  add object oEXCLAALL_1_6 as StdField with uid="VHDILDFVMI",rtseq=4,rtrep=.f.,;
    cFormVar = "w_EXCLAALL", cQueryName = "EXCLAALL",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Classe allegato legata all'estensione: verr� utilizzata nella associazione file con questa estensione",;
    HelpContextID = 254326382,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=102, Top=72, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CLA_ALLE", oKey_1_1="TACODICE", oKey_1_2="this.w_EXTIPALL", oKey_2_1="TACODCLA", oKey_2_2="this.w_EXCLAALL"

  func oEXCLAALL_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty(.w_EXTIPALL))
    endwith
   endif
  endfunc

  func oEXCLAALL_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oEXCLAALL_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oEXCLAALL_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CLA_ALLE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TACODICE="+cp_ToStrODBC(this.Parent.oContained.w_EXTIPALL)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TACODICE="+cp_ToStr(this.Parent.oContained.w_EXTIPALL)
    endif
    do cp_zoom with 'CLA_ALLE','*','TACODICE,TACODCLA',cp_AbsName(this.parent,'oEXCLAALL_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Classi allegati",'',this.parent.oContained
  endproc

  add object oDESALL_1_8 as StdField with uid="SHIRPYJLWM",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESALL", cQueryName = "DESALL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 58902986,;
   bGlobalFont=.t.,;
    Height=21, Width=319, Left=175, Top=43, InputMask=replicate('X',40)

  add object oDESCLA_1_9 as StdField with uid="CZQWGPVKZZ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DESCLA", cQueryName = "DESCLA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 243321290,;
   bGlobalFont=.t.,;
    Height=21, Width=319, Left=175, Top=72, InputMask=replicate('X',40)


  add object oObj_1_10 as cp_runprogram with uid="OAFXBYMJSJ",left=51, top=173, width=219,height=25,;
    caption='GSUT_BCT(B)',;
   bGlobalFont=.t.,;
    prg="GSUT_BCT(w_EXTIPALL,'B')",;
    cEvent = "w_EXTIPALL Changed",;
    nPag=1;
    , HelpContextID = 63125818

  add object oEXESTCBI_1_11 as StdField with uid="XNMDLCWONJ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_EXESTCBI", cQueryName = "EXESTCBI",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 68053391,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=102, Top=101, InputMask=replicate('X',4)

  add object oStr_1_2 as StdString with uid="JOFMCRFGGT",Visible=.t., Left=19, Top=14,;
    Alignment=1, Width=82, Height=18,;
    Caption="Estensione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="WKCWGBCKZU",Visible=.t., Left=28, Top=44,;
    Alignment=1, Width=73, Height=18,;
    Caption="Tipologia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="RMOSWWAGCS",Visible=.t., Left=42, Top=72,;
    Alignment=1, Width=59, Height=18,;
    Caption="Classe:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="LQOWSJNBIW",Visible=.t., Left=5, Top=104,;
    Alignment=1, Width=97, Height=18,;
    Caption="Estensione CBI :"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_aes','EXT_ENS','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".EXCODICE=EXT_ENS.EXCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
