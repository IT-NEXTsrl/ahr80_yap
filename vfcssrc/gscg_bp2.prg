* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bp2                                                        *
*              Carica automatismi IVA                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_53]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-21                                                      *
* Last revis.: 2014-03-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bp2",oParentObject)
return(i_retval)

define class tgscg_bp2 as StdBatch
  * --- Local variables
  w_INIZIO = .f.
  w_CHKAUT = 0
  w_CODCAU = space(5)
  w_NUMREG = 0
  w_TIPCLF = space(1)
  w_TR = space(1)
  w_TIPREG = space(1)
  w_NR = 0
  w_CODCLF = space(15)
  w_PADRE = .NULL.
  w_CONTA = 0
  w_MAXREG = space(1)
  w_MINREG = space(1)
  w_LCODIVA = space(5)
  w_FIRSTROW = .f.
  w_TOTDOC = 0
  w_CAOVAL = 0
  w_DECTOT = 0
  w_DECTOP = 0
  w_TIPDOC = space(2)
  w_VALNAZ = space(3)
  w_CODVAL = space(3)
  w_CAONAZ = 0
  w_DATREG = ctod("  /  /  ")
  * --- WorkFile variables
  CAUIVA_idx=0
  VOCIIVA_idx=0
  CAUIVA1_idx=0
  CONTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carica temporaneo Registrazioni IVA Primanota dalle Causali Contabili (da GSCG_MIV)
    * --- Abilitare solo in Caricamento ...
    * --- Se Impostata la Causale
    this.w_CHKAUT = this.oParentObject.oParentObject.w_CHKAUT
    * --- No Automatismi
    if this.w_CHKAUT=0
      i_retcode = 'stop'
      return
    endif
    this.w_PADRE = this.oParentObject
    this.w_PADRE.MarkPos()     
    this.w_CODCAU = this.oParentObject.oParentObject.w_PNCODCAU
    this.w_TIPCLF = this.oParentObject.oParentObject.w_PNTIPCLF
    this.w_CODCLF = this.oParentObject.oParentObject.w_PNCODCLF
    this.w_TIPREG = this.oParentObject.oParentObject.w_PNTIPREG
    this.w_NUMREG = this.oParentObject.oParentObject.w_PNNUMREG
    this.w_TIPDOC = this.oparentobject.oparentobject.w_PNTIPDOC
    * --- Azzera il Transitorio
    this.oParentObject.BlankRec()
     this.oParentObject.oPgFrm.Page1.oPag.oBody.Refresh()
    if ISAHR()
      this.oParentObject.w_TOTIMP = 0
      this.oParentObject.w_TOTIVA = 0
    endif
    * --- Cicla Sugli Automatismi IVA Generico
    this.w_INIZIO = .T.
    this.w_FIRSTROW = .T.
    do case
      case this.w_CHKAUT=1
        * --- Automatismo su Causale
        if this.w_TIPDOC $ "FE-NE-AU-NU"
          * --- Se l'automatismo consta di due righe 
          * --- Select from CAUIVA1
          i_nConn=i_TableProp[this.CAUIVA1_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAUIVA1_idx,2],.t.,this.CAUIVA1_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select AICODIVA,count(AICODIVA) as Conta , Max(AITIPREG) as MAXREG,MIN(AITIPREG) as MINREG, Min(CPROWNUM) as CPROWNUM  from "+i_cTable+" CAUIVA1 ";
                +" where AICODCAU = "+cp_ToStrODBC(this.w_CODCAU)+"";
                +" group by AICODIVA";
                +" order by CPROWNUM ASC, AICODIVA DESC";
                 ,"_Curs_CAUIVA1")
          else
            select AICODIVA,count(AICODIVA) as Conta , Max(AITIPREG) as MAXREG,MIN(AITIPREG) as MINREG, Min(CPROWNUM) as CPROWNUM from (i_cTable);
             where AICODCAU = this.w_CODCAU;
             group by AICODIVA;
             order by CPROWNUM ASC, AICODIVA DESC;
              into cursor _Curs_CAUIVA1
          endif
          if used('_Curs_CAUIVA1')
            select _Curs_CAUIVA1
            locate for 1=1
            do while not(eof())
            this.w_CONTA = Nvl(_Curs_CAUIVA1.CONTA,0)
            this.w_MAXREG = Nvl(_Curs_CAUIVA1.MAXREG," ")
            this.w_MINREG = Nvl(_Curs_CAUIVA1.MINREG," ")
            if this.w_CONTA=2 AND this.w_MINREG<>this.w_MAXREG
              this.w_LCODIVA = _Curs_CAUIVA1.AICODIVA
              exit
            endif
              select _Curs_CAUIVA1
              continue
            enddo
            use
          endif
        endif
        * --- Select from CAUIVA1
        i_nConn=i_TableProp[this.CAUIVA1_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAUIVA1_idx,2],.t.,this.CAUIVA1_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" CAUIVA1 ";
              +" where AICODCAU = "+cp_ToStrODBC(this.w_CODCAU)+"";
               ,"_Curs_CAUIVA1")
        else
          select * from (i_cTable);
           where AICODCAU = this.w_CODCAU;
            into cursor _Curs_CAUIVA1
        endif
        if used('_Curs_CAUIVA1')
          select _Curs_CAUIVA1
          locate for 1=1
          do while not(eof())
          if this.w_LCODIVA=Nvl(_Curs_CAUIVA1.AICODIVA,Space(5)) or (Empty(this.w_LCODIVA) and this.w_FIRSTROW)
            this.w_FIRSTROW = .T.
          else
            this.w_FIRSTROW = .F.
          endif
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
            select _Curs_CAUIVA1
            continue
          enddo
          use
        endif
      case this.w_CHKAUT=2
        * --- Automatismo su Modello
        if this.w_TIPDOC $ "FE-NE-AU-NU"
          * --- Select from CAUIVA
          i_nConn=i_TableProp[this.CAUIVA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAUIVA_idx,2],.t.,this.CAUIVA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select AICODIVA,count(AICODIVA) as Conta,Max(AITIPREG) as MAXREG,MIN(AITIPREG) as MINREG, Min(CPROWNUM) as CPROWNUM  from "+i_cTable+" CAUIVA ";
                +" where AICODCAU = "+cp_ToStrODBC(this.w_CODCAU)+" AND AITIPCLF="+cp_ToStrODBC(this.w_TIPCLF)+" AND AICODCLF="+cp_ToStrODBC(this.w_CODCLF)+"";
                +" group by AICODIVA";
                +" order by CPROWNUM ASC, AICODIVA DESC";
                 ,"_Curs_CAUIVA")
          else
            select AICODIVA,count(AICODIVA) as Conta,Max(AITIPREG) as MAXREG,MIN(AITIPREG) as MINREG, Min(CPROWNUM) as CPROWNUM from (i_cTable);
             where AICODCAU = this.w_CODCAU AND AITIPCLF=this.w_TIPCLF AND AICODCLF=this.w_CODCLF;
             group by AICODIVA;
             order by CPROWNUM ASC, AICODIVA DESC;
              into cursor _Curs_CAUIVA
          endif
          if used('_Curs_CAUIVA')
            select _Curs_CAUIVA
            locate for 1=1
            do while not(eof())
            this.w_CONTA = Nvl(_Curs_CAUIVA.CONTA,0)
            this.w_MAXREG = Nvl(_Curs_CAUIVA.MAXREG," ")
            this.w_MINREG = Nvl(_Curs_CAUIVA.MINREG," ")
            if this.w_CONTA=2 AND this.w_MINREG<>this.w_MAXREG
              this.w_LCODIVA = _Curs_CAUIVA.AICODIVA
              exit
            endif
              select _Curs_CAUIVA
              continue
            enddo
            use
          endif
        endif
        * --- Select from CAUIVA
        i_nConn=i_TableProp[this.CAUIVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAUIVA_idx,2],.t.,this.CAUIVA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" CAUIVA ";
              +" where AICODCAU="+cp_ToStrODBC(this.w_CODCAU)+" AND AITIPCLF="+cp_ToStrODBC(this.w_TIPCLF)+" AND AICODCLF="+cp_ToStrODBC(this.w_CODCLF)+"";
               ,"_Curs_CAUIVA")
        else
          select * from (i_cTable);
           where AICODCAU=this.w_CODCAU AND AITIPCLF=this.w_TIPCLF AND AICODCLF=this.w_CODCLF;
            into cursor _Curs_CAUIVA
        endif
        if used('_Curs_CAUIVA')
          select _Curs_CAUIVA
          locate for 1=1
          do while not(eof())
          if this.w_LCODIVA=Nvl(_Curs_CAUIVA.AICODIVA,Space(5)) or (Empty(this.w_LCODIVA) and this.w_FIRSTROW)
            this.w_FIRSTROW = .T.
          else
            this.w_FIRSTROW = .F.
          endif
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
            select _Curs_CAUIVA
            continue
          enddo
          use
        endif
    endcase
    * --- Questa Parte derivata dal Metodo LoadRec
    this.w_PADRE.RePos(.T.)     
    * --- Flag Caricati Automatismi
    this.oParentObject.w_GIACAR = "C"
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Nuova Riga del Temporaneo
    if this.w_INIZIO=.F.
      * --- Append gia' eseguito la prima volta nella BlankRec
      this.oParentObject.InitRow()
    else
      this.w_INIZIO = .F.
    endif
    this.oParentObject.w_IVCODIVA = iif(this.w_CHKAUT=2,NVL(_Curs_CAUIVA.AICODIVA, SPACE(5)),NVL(_Curs_CAUIVA1.AICODIVA, SPACE(5)))
    this.oParentObject.w_IVTIPCON = iif(this.w_CHKAUT=2,NVL(_Curs_CAUIVA.AITIPCON, " "),NVL(_Curs_CAUIVA1.AITIPCON, " "))
    this.oParentObject.w_IVCODCON = iif(this.w_CHKAUT=2,NVL(_Curs_CAUIVA.AICONDET, SPACE(15)),NVL(_Curs_CAUIVA1.AICONDET, SPACE(15)))
    this.oParentObject.w_IVCODCOI = iif(this.w_CHKAUT=2,NVL(_Curs_CAUIVA.AICONIND, SPACE(15)),NVL(_Curs_CAUIVA1.AICONIND, SPACE(15)))
    this.oParentObject.w_IVCONTRO = iif(this.w_CHKAUT=2,IIF(g_DETCON="S", NVL(_Curs_CAUIVA.AICONTRO, SPACE(15)), SPACE(15)),IIF(g_DETCON="S", NVL(_Curs_CAUIVA1.AICONTRO, SPACE(15)), SPACE(15)))
    this.oParentObject.w_IVTIPCOP = iif(this.w_CHKAUT=2,IIF(g_DETCON="S", NVL(_Curs_CAUIVA.AITIPCOP, "G"), "G"),IIF(g_DETCON="S", NVL(_Curs_CAUIVA1.AITIPCOP, SPACE(1)), "G"))
    this.w_TR = iif(this.w_CHKAUT=2,NVL(_Curs_CAUIVA.AITIPREG, "N"),NVL(_Curs_CAUIVA1.AITIPREG, "N"))
    this.w_NR = iif(this.w_CHKAUT=2,NVL(_Curs_CAUIVA.AINUMREG, 0),NVL(_Curs_CAUIVA1.AINUMREG, 0))
    this.oParentObject.w_IVTIPREG = IIF(this.w_TR="N", this.w_TIPREG, this.w_TR)
    this.oParentObject.w_IVNUMREG = IIF(this.w_NR=0, this.w_NUMREG, this.w_NR)
    this.oParentObject.w_CTIPREG = NVL(this.oParentObject.w_IVTIPREG, "N")
    this.oParentObject.w_IVFLOMAG = "X"
    this.oParentObject.w_IVCFLOMA = "X"
    this.oParentObject.w_CODIVA = this.oParentObject.w_IVCODIVA
    * --- Read from CONTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ANDTOBSO,ANDESCRI"+;
        " from "+i_cTable+" CONTI where ";
            +"ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_IVTIPCOP);
            +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_IVCONTRO);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ANDTOBSO,ANDESCRI;
        from (i_cTable) where;
            ANTIPCON = this.oParentObject.w_IVTIPCOP;
            and ANCODICE = this.oParentObject.w_IVCONTRO;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.oParentObject.w_DTOBSO = NVL(cp_ToDate(_read_.ANDTOBSO),cp_NullValue(_read_.ANDTOBSO))
      this.oParentObject.w_DESCON = NVL(cp_ToDate(_read_.ANDESCRI),cp_NullValue(_read_.ANDESCRI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from VOCIIVA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VOCIIVA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "IVDESIVA,IVPERIVA,IVPERIND,IVDTOBSO,IVREVCHA,IVREGMAR"+;
        " from "+i_cTable+" VOCIIVA where ";
            +"IVCODIVA = "+cp_ToStrODBC(this.oParentObject.w_IVCODIVA);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        IVDESIVA,IVPERIVA,IVPERIND,IVDTOBSO,IVREVCHA,IVREGMAR;
        from (i_cTable) where;
            IVCODIVA = this.oParentObject.w_IVCODIVA;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.oParentObject.w_DESIVA = NVL(cp_ToDate(_read_.IVDESIVA),cp_NullValue(_read_.IVDESIVA))
      this.oParentObject.w_PERIVA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
      this.oParentObject.w_IVPERIND = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
      this.oParentObject.w_IVOBSO = NVL(cp_ToDate(_read_.IVDTOBSO),cp_NullValue(_read_.IVDTOBSO))
      this.oParentObject.w_REVCHA = NVL(cp_ToDate(_read_.IVREVCHA),cp_NullValue(_read_.IVREVCHA))
      this.oParentObject.w_REGMAR = NVL(cp_ToDate(_read_.IVREGMAR),cp_NullValue(_read_.IVREGMAR))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_FIRSTROW
      * --- Variabili lette da GSCG_MPN...
      this.w_TOTDOC = this.oParentObject.oParentObject.w_PNTOTDOC
      this.w_CAOVAL = this.oParentObject.oParentObject.w_PNCAOVAL
      this.w_DECTOT = this.oparentobject.oparentobject.w_DECTOT
      this.w_VALNAZ = this.oparentobject.oparentobject.w_PNVALNAZ
      this.w_CODVAL = this.oparentobject.oparentobject.w_PNCODVAL
      this.w_CAONAZ = this.oparentobject.oparentobject.w_CAONAZ
      this.w_DATREG = this.oparentobject.oparentobject.w_PNDATREG
      this.w_DECTOP = this.oparentobject.oparentobject.w_DECTOP
      * --- Converto il totale documento in valuta di conto...
      if this.w_VALNAZ<>this.w_CODVAL
        if this.w_CAOVAL=0
          ah_ErrorMsg("Impossibile valorizzare imponibile ed imposta: cambio inesistente",,"")
          i_retcode = 'stop'
          return
        endif
      endif
      * --- Se non ho righe sul castelleto IVA riporto lo scorporo del totale documento
      *     sulla prima riga IVA. Se documento UE allora valorizzo solo imponibile con
      *     totale documento
      if Not (this.w_TIPDOC $ "FE-NE" OR (this.w_TIPDOC $ "AU-NU" AND this.oParentObject.w_REVCHA="S"))
        * --- Calcolo imponibile per scorporo...
        this.oParentObject.w_IVIMPONI = CALNET(this.w_TOTDOC,this.oParentObject.w_PERIVA, this.w_DECTOT ,"",0)
        this.oParentObject.w_IVIMPIVA = this.w_TOTDOC - this.oParentObject.w_IVIMPONI
      else
        this.oParentObject.w_IVIMPONI = this.w_TOTDOC
        * --- Calcolo imposta...
        this.oParentObject.w_IVIMPIVA = (this.oParentObject.w_IVIMPONI*this.oParentObject.w_PERIVA)/100
        if this.w_CODVAL=g_CODLIR OR this.w_TIPDOC $ "FE-NE" 
          * --- Se Lire Arrotonda l' IVA all'importo Superiore
          this.oParentObject.w_IVIMPIVA = IVAROUND(this.oParentObject.w_IVIMPIVA, this.w_DECTOT, IIF(this.oParentObject.w_IVIMPIVA<0, 0, 1),this.oparentobject.oparentobject.w_PNVALNAZ)
        else
          this.oParentObject.w_IVIMPIVA = cp_ROUND(this.oParentObject.w_IVIMPIVA, this.w_DECTOT)
        endif
      endif
      * --- Calcolo i totali (ho solo la prima riga valorizzata)
      if this.w_VALNAZ<>this.w_CODVAL
        this.w_TOTDOC = cp_ROUND(VAL2MON( this.w_TOTDOC , this.w_CAOVAL, this.w_CAONAZ ,this.w_DATREG , this.w_VALNAZ ),this.w_DECTOP)
        this.oParentObject.w_IVIMPONI = cp_ROUND(VAL2MON( this.oParentObject.w_IVIMPONI, this.w_CAOVAL, this.w_CAONAZ ,this.w_DATREG , this.w_VALNAZ ),this.w_DECTOP)
        this.oParentObject.w_IVIMPIVA = cp_ROUND(VAL2MON( this.oParentObject.w_IVIMPIVA, this.w_CAOVAL, this.w_CAONAZ ,this.w_DATREG , this.w_VALNAZ ),this.w_DECTOP)
      endif
      this.oParentObject.w_IMPONI = this.oParentObject.w_IVIMPONI
      this.oParentObject.w_TOTIMP = this.oParentObject.w_TOTIMP+this.oParentObject.w_IVIMPONI
      this.oParentObject.w_TOTIVA = this.oParentObject.w_TOTIVA+this.oParentObject.w_IVIMPIVA
      if ISAHR()
        this.oparentobject.O_IVIMPONI=this.oParentObject.w_IVIMPONI
      endif
    endif
    if Empty(this.w_LCODIVA)
      this.w_FIRSTROW = .F.
    endif
    * --- Carica il Temporaneo dei Dati e skippa al record successivo
    this.oParentObject.TrsFromWork()
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='CAUIVA'
    this.cWorkTables[2]='VOCIIVA'
    this.cWorkTables[3]='CAUIVA1'
    this.cWorkTables[4]='CONTI'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_CAUIVA1')
      use in _Curs_CAUIVA1
    endif
    if used('_Curs_CAUIVA1')
      use in _Curs_CAUIVA1
    endif
    if used('_Curs_CAUIVA')
      use in _Curs_CAUIVA
    endif
    if used('_Curs_CAUIVA')
      use in _Curs_CAUIVA
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
