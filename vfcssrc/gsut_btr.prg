* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_btr                                                        *
*              Carica le traduzioni descrizioni output nelle lingue abilitate  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-06-24                                                      *
* Last revis.: 2010-10-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_btr",oParentObject)
return(i_retval)

define class tgsut_btr as StdBatch
  * --- Local variables
  w_OUNOMPRG = space(30)
  w_OUROWNUM = 0
  w_OUDESCRI = space(100)
  w_TOUDESCRI = space(100)
  w_LOOP = 0
  w_STRAD = .f.
  w_oERRORLOG = .NULL.
  * --- WorkFile variables
  OUT_PUTS_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --
    * --- --
    this.w_oERRORLOG=createobject("AH_ERRORLOG")
    * --- --
    if !empty(i_aCpLangs)
      * --- scrivo nella tabella i valori dei campi multilingua
      this.w_TOUDESCRI = ""
      * --- Select from OUT_PUTS
      i_nConn=i_TableProp[this.OUT_PUTS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OUT_PUTS_idx,2],.t.,this.OUT_PUTS_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" OUT_PUTS ";
             ,"_Curs_OUT_PUTS")
      else
        select * from (i_cTable);
          into cursor _Curs_OUT_PUTS
      endif
      if used('_Curs_OUT_PUTS')
        select _Curs_OUT_PUTS
        locate for 1=1
        do while not(eof())
        this.w_LOOP = 0
        do while this.w_LOOP<alen(i_aCpLangs)
          this.w_LOOP = this.w_LOOP+1
          appo="OUDESCRI_"+alltrim(i_aCpLangs[this.w_LOOP])
          this.w_TOUDESCRI = iif(empty(nvl(&appo,"")),this.w_TOUDESCRI,&appo)
        enddo
          select _Curs_OUT_PUTS
          continue
        enddo
        use
      endif
      this.w_STRAD = iif(Empty(nvl(this.w_TOUDESCRI,"")),.T.,AH_Yesno("Si vuole sovrascrivere le traduzioni gi� caricate?"))
      this.w_TOUDESCRI = ""
      * --- Select from OUT_PUTS
      i_nConn=i_TableProp[this.OUT_PUTS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OUT_PUTS_idx,2],.t.,this.OUT_PUTS_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select OUDESCRI,OUNOMPRG,OUROWNUM  from "+i_cTable+" OUT_PUTS ";
             ,"_Curs_OUT_PUTS")
      else
        select OUDESCRI,OUNOMPRG,OUROWNUM from (i_cTable);
          into cursor _Curs_OUT_PUTS
      endif
      if used('_Curs_OUT_PUTS')
        select _Curs_OUT_PUTS
        locate for 1=1
        do while not(eof())
        this.w_OUNOMPRG = _Curs_OUT_PUTS.OUNOMPRG
        this.w_OUROWNUM = _Curs_OUT_PUTS.OUROWNUM
        this.w_OUDESCRI = _Curs_OUT_PUTS.OUDESCRI
        AH_msg("Caricamento gestione: %1",.t.,,,alltrim(this.w_OUNOMPRG))
        this.w_LOOP = 0
        do while this.w_LOOP<alen(i_aCpLangs)
          this.w_LOOP = this.w_LOOP+1
          this.w_TOUDESCRI = cp_Translate(_Curs_OUT_PUTS.OUDESCRI,g_PROJECTLANGUAGE,i_aCpLangs[this.w_LOOP])
          * --- Lunghezza di OUDESCRI portata a 100 caratteri
          if len(alltrim(this.w_TOUDESCRI))>100
            this.w_oERRORLOG.AddMsgLog("Errore aggiornamento output: %1, record: %2 - %3, lingua: %4", alltrim(this.w_OUNOMPRG) , alltrim(str(this.w_OUROWNUM,9)), alltrim(this.w_OUDESCRI) , alltrim(i_aCpLangs[this.w_LOOP]))     
            this.w_oERRORLOG.AddMsgLog("Errore: la descrizione in lingua � stata troncata")     
          endif
          this.w_TOUDESCRI = left(iif(this.w_TOUDESCRI==this.w_OUDESCRI,space(100),alltrim(this.w_TOUDESCRI)),100)
          if !empty(this.w_TOUDESCRI)
            * --- Try
            local bErr_03BB6330
            bErr_03BB6330=bTrsErr
            this.Try_03BB6330()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- Salvo il messaggio nella Clipboard e avviso l'utente
              _ClipText = i_ErrMsg
              this.w_oERRORLOG.AddMsgLog("Errore aggiornamento output: %1, record: %2 - %3, lingua: %4", alltrim(this.w_OUNOMPRG) , alltrim(str(this.w_OUROWNUM,9)), alltrim(this.w_OUDESCRI) , alltrim(i_aCpLangs[this.w_LOOP]))     
              this.w_oERRORLOG.AddMsgLog("Errore: %1", i_ErrMsg)     
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_03BB6330
            * --- End
          endif
        enddo
          select _Curs_OUT_PUTS
          continue
        enddo
        use
      endif
      AH_Errormsg("Caricamento completato")
    else
      AH_Errormsg("Non � abilitata alcuna lingua per la traduzione dei dati a runtime")
    endif
    * --- Lancia report
    this.w_oERRORLOG.PrintLog(this,"Errori riscontrati")     
  endproc
  proc Try_03BB6330()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
     
 L_errsav=on("ERROR") 
 L_ERR=.F. 
 On Error L_ERR = .T.
    if this.w_STRAD
      * --- Write into OUT_PUTS
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.OUT_PUTS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OUT_PUTS_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.OUT_PUTS_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"OUDESCRI_"+alltrim(i_aCpLangs[this.w_LOOP])+" ="+cp_NullLink(cp_ToStrODBC(this.w_TOUDESCRI),'OUT_PUTS','OUDESCRI_"+alltrim(i_aCpLangs[this.w_LOOP])+"');
            +i_ccchkf ;
        +" where ";
            +"OUNOMPRG = "+cp_ToStrODBC(this.w_OUNOMPRG);
            +" and OUROWNUM = "+cp_ToStrODBC(this.w_OUROWNUM);
               )
      else
        update (i_cTable) set;
            OUDESCRI_"+alltrim(i_aCpLangs[this.w_LOOP])+" = this.w_TOUDESCRI;
            &i_ccchkf. ;
         where;
            OUNOMPRG = this.w_OUNOMPRG;
            and OUROWNUM = this.w_OUROWNUM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Write into OUT_PUTS
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.OUT_PUTS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OUT_PUTS_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.OUT_PUTS_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"OUDESCRI_"+alltrim(i_aCpLangs[this.w_LOOP])+" ="+cp_NullLink(cp_ToStrODBC(this.w_TOUDESCRI),'OUT_PUTS','OUDESCRI_"+alltrim(i_aCpLangs[this.w_LOOP])+"');
            +i_ccchkf ;
        +" where ";
            +"OUNOMPRG = "+cp_ToStrODBC(this.w_OUNOMPRG);
            +" and OUROWNUM = "+cp_ToStrODBC(this.w_OUROWNUM);
            +" and "+cp_SetSQLFunctions("[NOTEMPTYSTR(OUDESCRI_"+alltrim(i_aCpLangs[this.w_LOOP])+")]", cp_GetDatabaseType(IIF(i_nConn=0,i_serverconn[1,2],i_nConn)))+" = "+cp_ToStrODBC(0);
               )
      else
        update (i_cTable) set;
            OUDESCRI_"+alltrim(i_aCpLangs[this.w_LOOP])+" = this.w_TOUDESCRI;
            &i_ccchkf. ;
         where;
            OUNOMPRG = this.w_OUNOMPRG;
            and OUROWNUM = this.w_OUROWNUM;
            and "+cp_SetSQLFunctions("[NOTEMPTYSTR(OUDESCRI_"+alltrim(i_aCpLangs[this.w_LOOP])+")]", cp_GetDatabaseType(IIF(i_nConn=0,i_serverconn[1,2],i_nConn)))+" = 0;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Ripristino la vecchia gestione errori
    on error &L_errsav
    if L_ERR
      * --- Raise
      i_Error=Message() + " - " + Message(1)
      return
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='OUT_PUTS'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_OUT_PUTS')
      use in _Curs_OUT_PUTS
    endif
    if used('_Curs_OUT_PUTS')
      use in _Curs_OUT_PUTS
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
