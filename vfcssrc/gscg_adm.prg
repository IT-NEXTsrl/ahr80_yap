* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_adm                                                        *
*              Dimensioni                                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-06-15                                                      *
* Last revis.: 2009-12-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_adm"))

* --- Class definition
define class tgscg_adm as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 692
  Height = 134+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-12-11"
  HelpContextID=175495831
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=11

  * --- Constant Properties
  TAB_DIME_IDX = 0
  TAB_FONT_IDX = 0
  CONTI_IDX = 0
  ATTIMAST_IDX = 0
  VOCIIVA_IDX = 0
  CAU_CONT_IDX = 0
  DETCOMBO_IDX = 0
  cFile = "TAB_DIME"
  cKeySelect = "DICODICE,DI_FONTE"
  cKeyWhere  = "DICODICE=this.w_DICODICE and DI_FONTE=this.w_DI_FONTE"
  cKeyWhereODBC = '"DICODICE="+cp_ToStrODBC(this.w_DICODICE)';
      +'+" and DI_FONTE="+cp_ToStrODBC(this.w_DI_FONTE)';

  cKeyWhereODBCqualified = '"TAB_DIME.DICODICE="+cp_ToStrODBC(this.w_DICODICE)';
      +'+" and TAB_DIME.DI_FONTE="+cp_ToStrODBC(this.w_DI_FONTE)';

  cPrg = "gscg_adm"
  cComment = "Dimensioni"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DICODICE = space(15)
  o_DICODICE = space(15)
  w_DITIPDIM = space(2)
  o_DITIPDIM = space(2)
  w_DIDESCRI = space(80)
  w_DI_FONTE = space(10)
  w_DITIPDAT = space(1)
  o_DITIPDAT = space(1)
  w_DIARLINK = space(15)
  w_DIVALCHR = space(50)
  o_DIVALCHR = space(50)
  w_DIVALDAT = ctod('  /  /  ')
  o_DIVALDAT = ctod('  /  /  ')
  w_DIVALNUM = 0
  o_DIVALNUM = 0
  w_DIFLZERO = space(1)
  w_FDESCRI = space(40)

  * --- Children pointers
  gscg_mdc = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'TAB_DIME','gscg_adm')
    stdPageFrame::Init()
    *set procedure to gscg_mdc additive
    with this
      .Pages(1).addobject("oPag","tgscg_admPag1","gscg_adm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dimensioni")
      .Pages(1).HelpContextID = 79857797
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDICODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure gscg_mdc
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='TAB_FONT'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='ATTIMAST'
    this.cWorkTables[4]='VOCIIVA'
    this.cWorkTables[5]='CAU_CONT'
    this.cWorkTables[6]='DETCOMBO'
    this.cWorkTables[7]='TAB_DIME'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(7))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.TAB_DIME_IDX,5],7]
    this.nPostItConn=i_TableProp[this.TAB_DIME_IDX,3]
  return

  function CreateChildren()
    this.gscg_mdc = CREATEOBJECT('stdLazyChild',this,'gscg_mdc')
    return

  procedure DestroyChildren()
    if !ISNULL(this.gscg_mdc)
      this.gscg_mdc.DestroyChildrenChain()
      this.gscg_mdc=.NULL.
    endif
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.gscg_mdc.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.gscg_mdc.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.gscg_mdc.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.gscg_mdc.SetKey(;
            .w_DI_FONTE,"DC_FONTE";
            ,.w_DICODICE,"DCCODDIS";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .gscg_mdc.ChangeRow(this.cRowID+'      1',1;
             ,.w_DI_FONTE,"DC_FONTE";
             ,.w_DICODICE,"DCCODDIS";
             )
    endwith
    return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_DICODICE = NVL(DICODICE,space(15))
      .w_DI_FONTE = NVL(DI_FONTE,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_4_joined
    link_1_4_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from TAB_DIME where DICODICE=KeySet.DICODICE
    *                            and DI_FONTE=KeySet.DI_FONTE
    *
    i_nConn = i_TableProp[this.TAB_DIME_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_DIME_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('TAB_DIME')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "TAB_DIME.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' TAB_DIME '
      link_1_4_joined=this.AddJoinedLink_1_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DICODICE',this.w_DICODICE  ,'DI_FONTE',this.w_DI_FONTE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_FDESCRI = space(40)
        .w_DICODICE = NVL(DICODICE,space(15))
        .w_DITIPDIM = NVL(DITIPDIM,space(2))
        .w_DIDESCRI = NVL(DIDESCRI,space(80))
        .w_DI_FONTE = NVL(DI_FONTE,space(10))
          if link_1_4_joined
            this.w_DI_FONTE = NVL(FOCODICE104,NVL(this.w_DI_FONTE,space(10)))
            this.w_FDESCRI = NVL(FODESCRI104,space(40))
          else
          .link_1_4('Load')
          endif
        .w_DITIPDAT = NVL(DITIPDAT,space(1))
        .w_DIARLINK = NVL(DIARLINK,space(15))
        .w_DIVALCHR = NVL(DIVALCHR,space(50))
        .w_DIVALDAT = NVL(cp_ToDate(DIVALDAT),ctod("  /  /  "))
        .w_DIVALNUM = NVL(DIVALNUM,0)
        .w_DIFLZERO = NVL(DIFLZERO,space(1))
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        cp_LoadRecExtFlds(this,'TAB_DIME')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DICODICE = space(15)
      .w_DITIPDIM = space(2)
      .w_DIDESCRI = space(80)
      .w_DI_FONTE = space(10)
      .w_DITIPDAT = space(1)
      .w_DIARLINK = space(15)
      .w_DIVALCHR = space(50)
      .w_DIVALDAT = ctod("  /  /  ")
      .w_DIVALNUM = 0
      .w_DIFLZERO = space(1)
      .w_FDESCRI = space(40)
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_DITIPDIM = 'LB'
        .DoRTCalc(3,4,.f.)
          if not(empty(.w_DI_FONTE))
          .link_1_4('Full')
          endif
        .w_DITIPDAT = 'C'
        .w_DIARLINK = SPACE(15)
        .w_DIVALCHR = SPACE(50)
        .w_DIVALDAT = cp_CharToDate('  -  -  ')
        .w_DIVALNUM = 0
        .w_DIFLZERO = iif(Not Empty(.w_DIVALCHR) OR .w_DIVALNUM>0 OR Not Empty(.w_DIVALDAT)  ,'N',.w_DIFLZERO)
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'TAB_DIME')
    this.DoRTCalc(11,11,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oDICODICE_1_1.enabled = i_bVal
      .Page1.oPag.oDITIPDIM_1_2.enabled = i_bVal
      .Page1.oPag.oDIDESCRI_1_3.enabled = i_bVal
      .Page1.oPag.oDI_FONTE_1_4.enabled = i_bVal
      .Page1.oPag.oDITIPDAT_1_5.enabled = i_bVal
      .Page1.oPag.oDIARLINK_1_6.enabled = i_bVal
      .Page1.oPag.oDIVALCHR_1_7.enabled = i_bVal
      .Page1.oPag.oDIVALDAT_1_8.enabled = i_bVal
      .Page1.oPag.oDIVALNUM_1_9.enabled = i_bVal
      .Page1.oPag.oDIFLZERO_1_10.enabled = i_bVal
      .Page1.oPag.oObj_1_19.enabled = i_bVal
      .Page1.oPag.oObj_1_20.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oDICODICE_1_1.enabled = .f.
        .Page1.oPag.oDI_FONTE_1_4.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oDICODICE_1_1.enabled = .t.
        .Page1.oPag.oDI_FONTE_1_4.enabled = .t.
      endif
    endwith
    this.gscg_mdc.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'TAB_DIME',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.gscg_mdc.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.TAB_DIME_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DICODICE,"DICODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DITIPDIM,"DITIPDIM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIDESCRI,"DIDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DI_FONTE,"DI_FONTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DITIPDAT,"DITIPDAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIARLINK,"DIARLINK",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIVALCHR,"DIVALCHR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIVALDAT,"DIVALDAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIVALNUM,"DIVALNUM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIFLZERO,"DIFLZERO",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.TAB_DIME_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_DIME_IDX,2])
    i_lTable = "TAB_DIME"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.TAB_DIME_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.TAB_DIME_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_DIME_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.TAB_DIME_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into TAB_DIME
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'TAB_DIME')
        i_extval=cp_InsertValODBCExtFlds(this,'TAB_DIME')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(DICODICE,DITIPDIM,DIDESCRI,DI_FONTE,DITIPDAT"+;
                  ",DIARLINK,DIVALCHR,DIVALDAT,DIVALNUM,DIFLZERO "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_DICODICE)+;
                  ","+cp_ToStrODBC(this.w_DITIPDIM)+;
                  ","+cp_ToStrODBC(this.w_DIDESCRI)+;
                  ","+cp_ToStrODBCNull(this.w_DI_FONTE)+;
                  ","+cp_ToStrODBC(this.w_DITIPDAT)+;
                  ","+cp_ToStrODBC(this.w_DIARLINK)+;
                  ","+cp_ToStrODBC(this.w_DIVALCHR)+;
                  ","+cp_ToStrODBC(this.w_DIVALDAT)+;
                  ","+cp_ToStrODBC(this.w_DIVALNUM)+;
                  ","+cp_ToStrODBC(this.w_DIFLZERO)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'TAB_DIME')
        i_extval=cp_InsertValVFPExtFlds(this,'TAB_DIME')
        cp_CheckDeletedKey(i_cTable,0,'DICODICE',this.w_DICODICE,'DI_FONTE',this.w_DI_FONTE)
        INSERT INTO (i_cTable);
              (DICODICE,DITIPDIM,DIDESCRI,DI_FONTE,DITIPDAT,DIARLINK,DIVALCHR,DIVALDAT,DIVALNUM,DIFLZERO  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_DICODICE;
                  ,this.w_DITIPDIM;
                  ,this.w_DIDESCRI;
                  ,this.w_DI_FONTE;
                  ,this.w_DITIPDAT;
                  ,this.w_DIARLINK;
                  ,this.w_DIVALCHR;
                  ,this.w_DIVALDAT;
                  ,this.w_DIVALNUM;
                  ,this.w_DIFLZERO;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.TAB_DIME_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_DIME_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.TAB_DIME_IDX,i_nConn)
      *
      * update TAB_DIME
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'TAB_DIME')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " DITIPDIM="+cp_ToStrODBC(this.w_DITIPDIM)+;
             ",DIDESCRI="+cp_ToStrODBC(this.w_DIDESCRI)+;
             ",DITIPDAT="+cp_ToStrODBC(this.w_DITIPDAT)+;
             ",DIARLINK="+cp_ToStrODBC(this.w_DIARLINK)+;
             ",DIVALCHR="+cp_ToStrODBC(this.w_DIVALCHR)+;
             ",DIVALDAT="+cp_ToStrODBC(this.w_DIVALDAT)+;
             ",DIVALNUM="+cp_ToStrODBC(this.w_DIVALNUM)+;
             ",DIFLZERO="+cp_ToStrODBC(this.w_DIFLZERO)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'TAB_DIME')
        i_cWhere = cp_PKFox(i_cTable  ,'DICODICE',this.w_DICODICE  ,'DI_FONTE',this.w_DI_FONTE  )
        UPDATE (i_cTable) SET;
              DITIPDIM=this.w_DITIPDIM;
             ,DIDESCRI=this.w_DIDESCRI;
             ,DITIPDAT=this.w_DITIPDAT;
             ,DIARLINK=this.w_DIARLINK;
             ,DIVALCHR=this.w_DIVALCHR;
             ,DIVALDAT=this.w_DIVALDAT;
             ,DIVALNUM=this.w_DIVALNUM;
             ,DIFLZERO=this.w_DIFLZERO;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- gscg_mdc : Saving
      this.gscg_mdc.ChangeRow(this.cRowID+'      1',0;
             ,this.w_DI_FONTE,"DC_FONTE";
             ,this.w_DICODICE,"DCCODDIS";
             )
      this.gscg_mdc.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- gscg_adm
    if not(bTrsErr)
        * --- Controlli Finali
        this.NotifyEvent('ControlliFinali')
    endif
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- gscg_mdc : Deleting
    this.gscg_mdc.ChangeRow(this.cRowID+'      1',0;
           ,this.w_DI_FONTE,"DC_FONTE";
           ,this.w_DICODICE,"DCCODDIS";
           )
    this.gscg_mdc.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.TAB_DIME_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_DIME_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.TAB_DIME_IDX,i_nConn)
      *
      * delete TAB_DIME
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'DICODICE',this.w_DICODICE  ,'DI_FONTE',this.w_DI_FONTE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.TAB_DIME_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_DIME_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
        if .o_DITIPDIM<>.w_DITIPDIM
            .w_DIARLINK = SPACE(15)
        endif
        if .o_DITIPDIM<>.w_DITIPDIM.or. .o_DITIPDAT<>.w_DITIPDAT
            .w_DIVALCHR = SPACE(50)
        endif
        if .o_DITIPDIM<>.w_DITIPDIM.or. .o_DITIPDAT<>.w_DITIPDAT
            .w_DIVALDAT = cp_CharToDate('  -  -  ')
        endif
        if .o_DITIPDIM<>.w_DITIPDIM.or. .o_DITIPDAT<>.w_DITIPDAT
            .w_DIVALNUM = 0
        endif
        if .o_DICODICE<>.w_DICODICE.or. .o_DIVALCHR<>.w_DIVALCHR.or. .o_DIVALDAT<>.w_DIVALDAT.or. .o_DIVALNUM<>.w_DIVALNUM
            .w_DIFLZERO = iif(Not Empty(.w_DIVALCHR) OR .w_DIVALNUM>0 OR Not Empty(.w_DIVALDAT)  ,'N',.w_DIFLZERO)
        endif
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(11,11,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDIFLZERO_1_10.enabled = this.oPgFrm.Page1.oPag.oDIFLZERO_1_10.mCond()
    this.oPgFrm.Page1.oPag.oLinkPC_1_11.enabled = this.oPgFrm.Page1.oPag.oLinkPC_1_11.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oDIARLINK_1_6.visible=!this.oPgFrm.Page1.oPag.oDIARLINK_1_6.mHide()
    this.oPgFrm.Page1.oPag.oDIVALCHR_1_7.visible=!this.oPgFrm.Page1.oPag.oDIVALCHR_1_7.mHide()
    this.oPgFrm.Page1.oPag.oDIVALDAT_1_8.visible=!this.oPgFrm.Page1.oPag.oDIVALDAT_1_8.mHide()
    this.oPgFrm.Page1.oPag.oDIVALNUM_1_9.visible=!this.oPgFrm.Page1.oPag.oDIVALNUM_1_9.mHide()
    this.oPgFrm.Page1.oPag.oDIFLZERO_1_10.visible=!this.oPgFrm.Page1.oPag.oDIFLZERO_1_10.mHide()
    this.oPgFrm.Page1.oPag.oLinkPC_1_11.visible=!this.oPgFrm.Page1.oPag.oLinkPC_1_11.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_18.visible=!this.oPgFrm.Page1.oPag.oStr_1_18.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_19.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_20.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DI_FONTE
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TAB_FONT_IDX,3]
    i_lTable = "TAB_FONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_FONT_IDX,2], .t., this.TAB_FONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_FONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DI_FONTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_AFO',True,'TAB_FONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FOCODICE like "+cp_ToStrODBC(trim(this.w_DI_FONTE)+"%");

          i_ret=cp_SQL(i_nConn,"select FOCODICE,FODESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FOCODICE',trim(this.w_DI_FONTE))
          select FOCODICE,FODESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DI_FONTE)==trim(_Link_.FOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DI_FONTE) and !this.bDontReportError
            deferred_cp_zoom('TAB_FONT','*','FOCODICE',cp_AbsName(oSource.parent,'oDI_FONTE_1_4'),i_cWhere,'GSCG_AFO',"FONTI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FOCODICE,FODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where FOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FOCODICE',oSource.xKey(1))
            select FOCODICE,FODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DI_FONTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FOCODICE,FODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where FOCODICE="+cp_ToStrODBC(this.w_DI_FONTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FOCODICE',this.w_DI_FONTE)
            select FOCODICE,FODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DI_FONTE = NVL(_Link_.FOCODICE,space(10))
      this.w_FDESCRI = NVL(_Link_.FODESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DI_FONTE = space(10)
      endif
      this.w_FDESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TAB_FONT_IDX,2])+'\'+cp_ToStr(_Link_.FOCODICE,1)
      cp_ShowWarn(i_cKey,this.TAB_FONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DI_FONTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TAB_FONT_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TAB_FONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_4.FOCODICE as FOCODICE104"+ ",link_1_4.FODESCRI as FODESCRI104"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_4 on TAB_DIME.DI_FONTE=link_1_4.FOCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_4"
          i_cKey=i_cKey+'+" and TAB_DIME.DI_FONTE=link_1_4.FOCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDICODICE_1_1.value==this.w_DICODICE)
      this.oPgFrm.Page1.oPag.oDICODICE_1_1.value=this.w_DICODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oDITIPDIM_1_2.RadioValue()==this.w_DITIPDIM)
      this.oPgFrm.Page1.oPag.oDITIPDIM_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDIDESCRI_1_3.value==this.w_DIDESCRI)
      this.oPgFrm.Page1.oPag.oDIDESCRI_1_3.value=this.w_DIDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDI_FONTE_1_4.value==this.w_DI_FONTE)
      this.oPgFrm.Page1.oPag.oDI_FONTE_1_4.value=this.w_DI_FONTE
    endif
    if not(this.oPgFrm.Page1.oPag.oDITIPDAT_1_5.RadioValue()==this.w_DITIPDAT)
      this.oPgFrm.Page1.oPag.oDITIPDAT_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDIARLINK_1_6.RadioValue()==this.w_DIARLINK)
      this.oPgFrm.Page1.oPag.oDIARLINK_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDIVALCHR_1_7.value==this.w_DIVALCHR)
      this.oPgFrm.Page1.oPag.oDIVALCHR_1_7.value=this.w_DIVALCHR
    endif
    if not(this.oPgFrm.Page1.oPag.oDIVALDAT_1_8.value==this.w_DIVALDAT)
      this.oPgFrm.Page1.oPag.oDIVALDAT_1_8.value=this.w_DIVALDAT
    endif
    if not(this.oPgFrm.Page1.oPag.oDIVALNUM_1_9.value==this.w_DIVALNUM)
      this.oPgFrm.Page1.oPag.oDIVALNUM_1_9.value=this.w_DIVALNUM
    endif
    if not(this.oPgFrm.Page1.oPag.oDIFLZERO_1_10.RadioValue()==this.w_DIFLZERO)
      this.oPgFrm.Page1.oPag.oDIFLZERO_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFDESCRI_1_17.value==this.w_FDESCRI)
      this.oPgFrm.Page1.oPag.oFDESCRI_1_17.value=this.w_FDESCRI
    endif
    cp_SetControlsValueExtFlds(this,'TAB_DIME')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DICODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDICODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_DICODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DI_FONTE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDI_FONTE_1_4.SetFocus()
            i_bnoObbl = !empty(.w_DI_FONTE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DIARLINK))  and not(.w_DITIPDIM <>'LK' or Empty(.w_DICODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDIARLINK_1_6.SetFocus()
            i_bnoObbl = !empty(.w_DIARLINK)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Archivio non gestito")
        endcase
      endif
      *i_bRes = i_bRes .and. .gscg_mdc.CheckForm()
      if i_bres
        i_bres=  .gscg_mdc.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DICODICE = this.w_DICODICE
    this.o_DITIPDIM = this.w_DITIPDIM
    this.o_DITIPDAT = this.w_DITIPDAT
    this.o_DIVALCHR = this.w_DIVALCHR
    this.o_DIVALDAT = this.w_DIVALDAT
    this.o_DIVALNUM = this.w_DIVALNUM
    * --- gscg_mdc : Depends On
    this.gscg_mdc.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgscg_admPag1 as StdContainer
  Width  = 688
  height = 134
  stdWidth  = 688
  stdheight = 134
  resizeXpos=352
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDICODICE_1_1 as StdField with uid="MEMEEERCHU",rtseq=1,rtrep=.f.,;
    cFormVar = "w_DICODICE", cQueryName = "DICODICE",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice dimensione",;
    HelpContextID = 133605253,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=108, Left=109, Top=20, InputMask=replicate('X',15)


  add object oDITIPDIM_1_2 as StdCombo with uid="HETIRIAGZE",rtseq=2,rtrep=.t.,left=523,top=22,width=159,height=21;
    , ToolTipText = "Tipo dimensione";
    , HelpContextID = 63203459;
    , cFormVar="w_DITIPDIM",RowSource=""+"Libera,"+"Link,"+"Combo\Check,"+"Data Comp. IVA iniz.,"+"Data Comp. IVA fin.,"+"Anno,"+"Periodo,"+"Attivit�", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDITIPDIM_1_2.RadioValue()
    return(iif(this.value =1,'LB',;
    iif(this.value =2,'LK',;
    iif(this.value =3,'CC',;
    iif(this.value =4,'CI',;
    iif(this.value =5,'CF',;
    iif(this.value =6,'AN',;
    iif(this.value =7,'PE',;
    iif(this.value =8,'AT',;
    space(2))))))))))
  endfunc
  func oDITIPDIM_1_2.GetRadio()
    this.Parent.oContained.w_DITIPDIM = this.RadioValue()
    return .t.
  endfunc

  func oDITIPDIM_1_2.SetRadio()
    this.Parent.oContained.w_DITIPDIM=trim(this.Parent.oContained.w_DITIPDIM)
    this.value = ;
      iif(this.Parent.oContained.w_DITIPDIM=='LB',1,;
      iif(this.Parent.oContained.w_DITIPDIM=='LK',2,;
      iif(this.Parent.oContained.w_DITIPDIM=='CC',3,;
      iif(this.Parent.oContained.w_DITIPDIM=='CI',4,;
      iif(this.Parent.oContained.w_DITIPDIM=='CF',5,;
      iif(this.Parent.oContained.w_DITIPDIM=='AN',6,;
      iif(this.Parent.oContained.w_DITIPDIM=='PE',7,;
      iif(this.Parent.oContained.w_DITIPDIM=='AT',8,;
      0))))))))
  endfunc

  add object oDIDESCRI_1_3 as StdField with uid="YLSSSXPILE",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DIDESCRI", cQueryName = "DIDESCRI",;
    bObbl = .f. , nPag = 1, value=space(80), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione dimensione",;
    HelpContextID = 219191169,;
   bGlobalFont=.t.,;
    Height=21, Width=573, Left=109, Top=49, InputMask=replicate('X',80)

  add object oDI_FONTE_1_4 as StdField with uid="RUTZKXSXGE",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DI_FONTE", cQueryName = "DICODICE,DI_FONTE",;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice fonte associata",;
    HelpContextID = 38659973,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=109, Top=75, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="TAB_FONT", cZoomOnZoom="GSCG_AFO", oKey_1_1="FOCODICE", oKey_1_2="this.w_DI_FONTE"

  func oDI_FONTE_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oDI_FONTE_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDI_FONTE_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TAB_FONT','*','FOCODICE',cp_AbsName(this.parent,'oDI_FONTE_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_AFO',"FONTI",'',this.parent.oContained
  endproc
  proc oDI_FONTE_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSCG_AFO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FOCODICE=this.parent.oContained.w_DI_FONTE
     i_obj.ecpSave()
  endproc


  add object oDITIPDAT_1_5 as StdCombo with uid="BPWOBGCWIP",rtseq=5,rtrep=.t.,left=109,top=105,width=125,height=21;
    , ToolTipText = "Tipologia del valore della dimensione";
    , HelpContextID = 205231990;
    , cFormVar="w_DITIPDAT",RowSource=""+"Char,"+"Numerico,"+"Data", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDITIPDAT_1_5.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'N',;
    iif(this.value =3,'D',;
    space(1)))))
  endfunc
  func oDITIPDAT_1_5.GetRadio()
    this.Parent.oContained.w_DITIPDAT = this.RadioValue()
    return .t.
  endfunc

  func oDITIPDAT_1_5.SetRadio()
    this.Parent.oContained.w_DITIPDAT=trim(this.Parent.oContained.w_DITIPDAT)
    this.value = ;
      iif(this.Parent.oContained.w_DITIPDAT=='C',1,;
      iif(this.Parent.oContained.w_DITIPDAT=='N',2,;
      iif(this.Parent.oContained.w_DITIPDAT=='D',3,;
      0)))
  endfunc


  add object oDIARLINK_1_6 as StdCombo with uid="FKUBCZSGPL",rtseq=6,rtrep=.f.,left=290,top=105,width=118,height=21;
    , ToolTipText = "Nome dell'archivio per le dimensioni di tipo link";
    , HelpContextID = 143407233;
    , cFormVar="w_DIARLINK",RowSource=""+"Causali contabili,"+"Codici IVA,"+"Conti", bObbl = .t. , nPag = 1;
    , sErrorMsg = "Archivio non gestito";
  , bGlobalFont=.t.


  func oDIARLINK_1_6.RadioValue()
    return(iif(this.value =1,'CAU_CONT',;
    iif(this.value =2,'VOCIIVA',;
    iif(this.value =3,'CONTI',;
    space(15)))))
  endfunc
  func oDIARLINK_1_6.GetRadio()
    this.Parent.oContained.w_DIARLINK = this.RadioValue()
    return .t.
  endfunc

  func oDIARLINK_1_6.SetRadio()
    this.Parent.oContained.w_DIARLINK=trim(this.Parent.oContained.w_DIARLINK)
    this.value = ;
      iif(this.Parent.oContained.w_DIARLINK=='CAU_CONT',1,;
      iif(this.Parent.oContained.w_DIARLINK=='VOCIIVA',2,;
      iif(this.Parent.oContained.w_DIARLINK=='CONTI',3,;
      0)))
  endfunc

  func oDIARLINK_1_6.mHide()
    with this.Parent.oContained
      return (.w_DITIPDIM <>'LK' or Empty(.w_DICODICE))
    endwith
  endfunc

  add object oDIVALCHR_1_7 as StdField with uid="XYTKPKFMCK",rtseq=7,rtrep=.t.,;
    cFormVar = "w_DIVALCHR", cQueryName = "DIVALCHR",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Valore delle dimensioni di tipo carattere",;
    HelpContextID = 41715848,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=290, Top=105, InputMask=replicate('X',50)

  func oDIVALCHR_1_7.mHide()
    with this.Parent.oContained
      return (.w_DITIPDAT<>'C' OR Not(.w_DITIPDIM $ 'LB-AN') or Empty(.w_DICODICE))
    endwith
  endfunc

  add object oDIVALDAT_1_8 as StdField with uid="HDUXYMXCIO",rtseq=8,rtrep=.t.,;
    cFormVar = "w_DIVALDAT", cQueryName = "DIVALDAT",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Valore delle dimensioni di tipo data",;
    HelpContextID = 209942390,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=290, Top=105

  func oDIVALDAT_1_8.mHide()
    with this.Parent.oContained
      return (.w_DITIPDAT<>'D' OR Not(.w_DITIPDIM $ 'LB-CI-CF') or Empty(.w_DICODICE))
    endwith
  endfunc

  add object oDIVALNUM_1_9 as StdField with uid="PHITCZZERZ",rtseq=9,rtrep=.t.,;
    cFormVar = "w_DIVALNUM", cQueryName = "DIVALNUM",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Valore delle dimensioni di tipo numerico",;
    HelpContextID = 42170237,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=290, Top=105, cSayPict='"99999999999999.99999"', cGetPict='"99999999999999.99999"'

  func oDIVALNUM_1_9.mHide()
    with this.Parent.oContained
      return (.w_DITIPDAT<>'N' OR Not(.w_DITIPDIM $ 'LB-AN-PE') or Empty(.w_DICODICE))
    endwith
  endfunc

  add object oDIFLZERO_1_10 as StdCheck with uid="FANQAQQPJW",rtseq=10,rtrep=.t.,left=526, top=105, caption="Filtra default",;
    ToolTipText = "Se attivo, filtra per il valore di default",;
    HelpContextID = 177829755,;
    cFormVar="w_DIFLZERO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDIFLZERO_1_10.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDIFLZERO_1_10.GetRadio()
    this.Parent.oContained.w_DIFLZERO = this.RadioValue()
    return .t.
  endfunc

  func oDIFLZERO_1_10.SetRadio()
    this.Parent.oContained.w_DIFLZERO=trim(this.Parent.oContained.w_DIFLZERO)
    this.value = ;
      iif(this.Parent.oContained.w_DIFLZERO=='S',1,;
      0)
  endfunc

  func oDIFLZERO_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_DIVALCHR) AND .w_DIVALNUM=0 AND Empty(.w_DIVALDAT))
    endwith
   endif
  endfunc

  func oDIFLZERO_1_10.mHide()
    with this.Parent.oContained
      return (Empty(.w_DICODICE) or .w_DITIPDIM='CC' OR ((.w_DITIPDAT<>'N' OR Not(.w_DITIPDIM $ 'LB-AN-PE')) AND (.w_DITIPDAT<>'D' OR Not(.w_DITIPDIM $ 'LB-CI-CF')) AND (.w_DITIPDAT<>'C' OR Not(.w_DITIPDIM $ 'LB-AN')) AND (.w_DITIPDIM <>'LK')))
    endwith
  endfunc


  add object oLinkPC_1_11 as StdButton with uid="NLKYCREYQR",left=634, top=87, width=48,height=45,;
    CpPicture="bmp\ZOOM.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere al dettaglio valori combo";
    , HelpContextID = 30350886;
  , bGlobalFont=.t.

    proc oLinkPC_1_11.Click()
      this.Parent.oContained.gscg_mdc.LinkPCClick()
    endproc

  func oLinkPC_1_11.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_DITIPDIM='CC')
      endwith
    endif
  endfunc

  func oLinkPC_1_11.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_DITIPDIM<>'CC')
     endwith
    endif
  endfunc

  add object oFDESCRI_1_17 as StdField with uid="QQNWBNINYF",rtseq=11,rtrep=.f.,;
    cFormVar = "w_FDESCRI", cQueryName = "FDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 16610134,;
   bGlobalFont=.t.,;
    Height=21, Width=306, Left=205, Top=75, InputMask=replicate('X',40)


  add object oObj_1_19 as cp_runprogram with uid="AJGDZEWMQR",left=39, top=156, width=206,height=18,;
    caption='GSCG_BKC(A)',;
   bGlobalFont=.t.,;
    prg="GSCG_BKC('A')",;
    cEvent = "ControlliFinali",;
    nPag=1;
    , HelpContextID = 45364777


  add object oObj_1_20 as cp_runprogram with uid="VZVWNSSOSM",left=263, top=157, width=241,height=18,;
    caption='GSCG_BKC(B)',;
   bGlobalFont=.t.,;
    prg="GSCG_BKC('B')",;
    cEvent = "w_DITIPDIM Changed",;
    nPag=1;
    , HelpContextID = 45365033

  add object oStr_1_12 as StdString with uid="VXAKQPGASY",Visible=.t., Left=42, Top=78,;
    Alignment=1, Width=63, Height=18,;
    Caption="Fonte:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="MONUJMCSBI",Visible=.t., Left=23, Top=22,;
    Alignment=1, Width=82, Height=18,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_14 as StdString with uid="ZTWJEFXUMF",Visible=.t., Left=12, Top=50,;
    Alignment=1, Width=93, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="GLXKCRMBSI",Visible=.t., Left=6, Top=104,;
    Alignment=1, Width=99, Height=18,;
    Caption="Tipologia valore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="BGXIUXLEBN",Visible=.t., Left=393, Top=21,;
    Alignment=1, Width=126, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="NXGFRGZQJW",Visible=.t., Left=240, Top=105,;
    Alignment=1, Width=46, Height=18,;
    Caption="Valore:"  ;
  , bGlobalFont=.t.

  func oStr_1_18.mHide()
    with this.Parent.oContained
      return (Empty(.w_DICODICE) or .w_DITIPDIM='CC' OR ((.w_DITIPDAT<>'N' OR Not(.w_DITIPDIM $ 'LB-AN-PE')) AND (.w_DITIPDAT<>'D' OR Not(.w_DITIPDIM $ 'LB-CI-CF')) AND (.w_DITIPDAT<>'C' OR Not(.w_DITIPDIM $ 'LB-AN')) AND (.w_DITIPDIM <>'LK')))
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_adm','TAB_DIME','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DICODICE=TAB_DIME.DICODICE";
  +" and "+i_cAliasName2+".DI_FONTE=TAB_DIME.DI_FONTE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
