* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bir                                                        *
*              Cambio righe mov.INTRA                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_48]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-03-13                                                      *
* Last revis.: 2010-10-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_PARAM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bir",oParentObject,m.w_PARAM)
return(i_retval)

define class tgsar_bir as StdBatch
  * --- Local variables
  w_PARAM = space(2)
  w_MIN = 0
  w_MESS = space(50)
  w_AbsRow = 0
  w_nRelRow = 0
  w_RECPOS = 0
  w_TOTRIGA = 0
  w_PADRE = .NULL.
  w_CONDCO = space(1)
  w_NATTRA = space(3)
  w_MODTRA = space(3)
  w_NAZORI = space(3)
  w_NAZPRO = space(3)
  w_PRODES = space(2)
  w_NAZDES = space(3)
  w_PROORI = space(2)
  w_SEZDOG = space(6)
  w_PRORET = space(6)
  w_PERSER = space(1)
  w_MODINC = space(1)
  w_PAEPAG = space(3)
  w_OK = .f.
  * --- WorkFile variables
  VALUTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Riporto sulle righe dopo la prima: natura della transazione, modalit� di trasporto ...
    *     Il parametro Param assume i seguenti valori:
    *     'PI' ->w_PITIPMOV Changed
    *     'NO'->w_ARUTISER Changed
    *     'NT'->w_PINATTRA Changed
    *     'MT'->w_PIMODTRA Changed
    *     'VO'->w_PIPROORI Changed
    *     'ND'->w_PINAZDES Changed
    *     'VD'->w_PIPRODES Changed
    *     'PP'->w_PINAZPRO Changed
    *     'PO'->w_PINAZORI Changed
    *     'MC'->w_PICONDCO Changed
    *     'SZ'->w_PISEZDOG Changed
    *     'PR'->w_PIPRORET Changed
    *     'ME'->w_PIPERSER Changed
    *     'MI'->w_PIMODINC Changed
    *     'PA'->w_PIPAEPAG Changed
    this.w_PADRE = this.oParentObject
    this.w_CONDCO = this.oParentObject.w_PICONDCO
    this.w_NATTRA = this.oParentObject.w_PINATTRA
    this.w_MODTRA = this.oParentObject.w_PIMODTRA
    this.w_NAZORI = this.oParentObject.w_PINAZORI
    this.w_NAZPRO = this.oParentObject.w_PINAZPRO
    this.w_PRODES = this.oParentObject.w_PIPRODES
    this.w_NAZDES = this.oParentObject.w_PINAZDES
    this.w_PROORI = this.oParentObject.w_PIPROORI
    this.w_SEZDOG = this.oParentObject.w_PISEZDOG
    this.w_PRORET = this.oParentObject.w_PIPRORET
    this.w_PERSER = this.oParentObject.w_PIPERSER
    this.w_MODINC = this.oParentObject.w_PIMODINC
    this.w_PAEPAG = this.oParentObject.w_PIPAEPAG
    this.w_OK = .F.
    if this.w_PARAM="PI" OR this.w_PARAM="NO"
      if this.w_PARAM="PI"
        this.oParentObject.w_PIPERRET = IIF(this.oParentObject.w_PITIPMOV $ "RC-RA",this.oParentObject.w_PIPERRET,0)
        this.oParentObject.w_DESPER = IIF(this.oParentObject.w_PITIPMOV $ "RC-RA",this.oParentObject.w_DESPER,"")
        this.oParentObject.w_PIANNRET = IIF(this.oParentObject.w_PITIPMOV $ "RC-RA",this.oParentObject.w_PIANNRET,"")
        * --- aggiungere pulizia combo mensile
      endif
      this.w_PADRE.MarkPos()     
      this.w_PADRE.FirstRow()     
      do while Not this.w_PADRE.Eof_Trs()
        this.w_PADRE.SetRow()     
        if this.w_PARAM="PI"
          this.oParentObject.w_PIPERSER = IIF(this.oParentObject.w_PITIPMOV $ "CE-AC-RC-RA","I",this.oParentObject.w_PIPERSER)
          this.oParentObject.w_PIIMPVAL = IIF(this.oParentObject.w_PITIPMOV $"CE-CS-RC-RS",0,this.oParentObject.w_PIIMPVAL)
          this.oParentObject.w_PINAZORI = IIF(this.oParentObject.w_PITIPMOV $ "CE-CS-AS-RC-RA-RX-RS","",this.oParentObject.w_PINAZORI)
          this.oParentObject.w_PINAZPRO = IIF(this.oParentObject.w_PITIPMOV $ "CE-CS-AS-RC-RA-RX-RS","",this.oParentObject.w_PINAZPRO)
          this.oParentObject.w_PIPRODES = IIF(this.oParentObject.w_PITIPMOV $ "CE-CS-AS-RC-RA-RX-RS","",this.oParentObject.w_PIPRODES)
          this.oParentObject.w_PITIPRET = IIF(this.oParentObject.w_PITIPMOV $ "CE-CS-AS-RX-RS","",this.oParentObject.w_PITIPRET)
          this.oParentObject.w_PIMODINC = IIF(this.oParentObject.w_PITIPMOV $ "CE-AC-RC-RA","X",this.oParentObject.w_PIMODINC)
          this.oParentObject.w_PIPAEPAG = IIF(this.oParentObject.w_PITIPMOV $ "CE-AC-RC-RA","",this.oParentObject.w_PIPAEPAG)
          this.oParentObject.w_PISEZDOG = IIF(this.oParentObject.w_PITIPMOV $ "CE-AC-CS-AS-RC-RA","",this.oParentObject.w_PISEZDOG)
          this.oParentObject.w_PIANNREG = IIF(this.oParentObject.w_PITIPMOV $ "CE-AC-CS-AS-RC-RA","",this.oParentObject.w_PIANNREG)
          this.oParentObject.w_PIPRORET = IIF(this.oParentObject.w_PITIPMOV $ "CE-AC-CS-AS-RC-RA","",this.oParentObject.w_PIPRORET)
          this.oParentObject.w_PIPROGSE = IIF(this.oParentObject.w_PITIPMOV $ "CE-AC-CS-AS-RC-RA","",this.oParentObject.w_PIPROGSE)
          this.oParentObject.w_PINAZDES = IIF(this.oParentObject.w_PITIPMOV $ "AC-CS-AS-RC-RA-RX-RS","",this.oParentObject.w_PINAZDES)
          this.oParentObject.w_PIPROORI = IIF(this.oParentObject.w_PITIPMOV $ "AC-CS-AS-RC-RA-RX-RS","",this.oParentObject.w_PIPROORI)
          this.oParentObject.w_PINATTRA = IIF(this.oParentObject.w_PITIPMOV $ "CS-AS-RX-RS","",this.oParentObject.w_PINATTRA)
          this.oParentObject.w_PIMASNET = IIF(this.oParentObject.w_PITIPMOV $ "CS-AS-RX-RS-RC-RA",0,this.oParentObject.w_PIMASNET)
          this.oParentObject.w_PIQTASUP = IIF(this.oParentObject.w_PITIPMOV $ "CS-AS-RX-RS-RC-RA",0,this.oParentObject.w_PIQTASUP)
          this.oParentObject.w_PIVALSTA = IIF(this.oParentObject.w_PITIPMOV $ "CS-AS-RX-RS",0,this.oParentObject.w_PIVALSTA)
          this.oParentObject.w_PICONDCO = IIF(this.oParentObject.w_PITIPMOV $ "CS-AS-RX-RS","E",this.oParentObject.w_PICONDCO)
          this.oParentObject.w_PIMODTRA = IIF(this.oParentObject.w_PITIPMOV $ "CS-AS-RX-RS-RC-RA","",this.oParentObject.w_PIMODTRA)
          this.oParentObject.w_TIPOMOV = this.oParentObject.w_PITIPMOV
        else
          this.oParentObject.w_PINOMENC = SPACE(8)
        endif
        this.w_PADRE.SaveRow()     
        this.w_PADRE.NextRow()     
      enddo
      this.w_PADRE.Repos()     
    else
      * --- Salvo la riga di partenza per il riposizionamento
      this.w_PADRE.MarkPos()     
      this.w_TOTRIGA = this.w_PADRE.NumRow()
      * --- Calolo il totale righe
      GO TOP
      if this.w_PARAM="CC"
        * --- Al momento della modifica del codice conto rileggo i campi associati 
        *     alla valuta originaria
        * --- Read from VALUTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VALUTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VADECTOT,VACAOVAL,VADTOBSO,VADESVAL"+;
            " from "+i_cTable+" VALUTE where ";
                +"VACODVAL = "+cp_ToStrODBC(this.oParentObject.w_PIVALORI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VADECTOT,VACAOVAL,VADTOBSO,VADESVAL;
            from (i_cTable) where;
                VACODVAL = this.oParentObject.w_PIVALORI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          w_DECVAL = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
          w_CAMVAL = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
          w_DATOBSO = NVL(cp_ToDate(_read_.VADTOBSO),cp_NullValue(_read_.VADTOBSO))
          w_DESVAL = NVL(cp_ToDate(_read_.VADESVAL),cp_NullValue(_read_.VADESVAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.oParentObject.w_PITIPMOV $ "CE-AC" AND this.oParentObject.w_TIPPER="M" And g_ISONAZ<>"ESP" And Not Empty(this.oParentObject.w_PICODCON)
          this.w_MESS = "Si vuole aggiornare le condizioni di consegna sulle righe del movimento?"
          if this.w_TOTRIGA>=1
            if ah_YesNo(this.w_MESS) 
              this.w_PADRE.FirstRow()     
              do while !this.w_PADRE.Eof_Trs()
                this.w_PADRE.SetRow()     
                if this.w_PADRE.FullRow()
                  this.oParentObject.w_PICONDCO = IIF(Not Empty(this.oParentObject.w_Anconcon),this.oParentObject.w_ANCONCON,"E")
                  this.w_PADRE.SaveRow()     
                else
                  this.oParentObject.w_PICONDCO = IIF(Not Empty(this.oParentObject.w_Anconcon),this.oParentObject.w_ANCONCON,"E")
                  this.w_PADRE.SetControlsValue()     
                endif
                this.w_PADRE.NextRow()     
              enddo
            endif
          else
            this.oParentObject.w_PICONDCO = IIF(Not Empty(this.oParentObject.w_Anconcon),this.oParentObject.w_ANCONCON,"E")
            this.w_PADRE.SetControlsValue()     
          endif
        endif
      else
        CALCULATE MIN(t_CPROWORD) TO this.w_MIN
        if this.w_MIN = this.oParentObject.w_CPROWORD
          * --- Se sono sulla prima riga
          if this.w_TOTRIGA>1
            do case
              case this.w_PARAM="NT"
                * --- se le righe sono pi� di una
                this.w_MESS = "Si vuole aggiornare la natura della transazione sulle altre righe del movimento?"
                this.w_OK = .T.
              case this.w_PARAM="MT"
                this.w_MESS = "Si vuole aggiornare la modalit� di trasporto sulle altre righe del movimento?"
                this.w_OK = .T.
              case this.w_PARAM="MC"
                this.w_MESS = "Si vuole aggiornare la modalit� di consegna sulle altre righe del movimento?"
                this.w_OK = .T.
              case this.w_PARAM="PO"
                this.w_MESS = "Si vuole aggiornare il paese di origine sulle altre righe del movimento?"
                this.w_OK = .T.
              case this.w_PARAM="PP"
                this.w_MESS = "Si vuole aggiornare il paese di provenienza sulle altre righe del movimento?"
                this.w_OK = .T.
              case this.w_PARAM="VD"
                this.w_MESS = "Si vuole aggiornare la provincia di destinazione sulle altre righe del movimento?"
                this.w_OK = .T.
              case this.w_PARAM="ND"
                this.w_MESS = "Si vuole aggiornare la nazione di destinazione sulle altre righe del movimento?"
                this.w_OK = .T.
              case this.w_PARAM="VO"
                this.w_MESS = "Si vuole aggiornare la provincia di origine sulle altre righe del movimento?"
                this.w_OK = .T.
              case this.w_PARAM="SZ"
                this.w_MESS = "Si vuole aggiornare la sezione doganale sulle altre righe del movimento?"
                this.w_OK = .T.
              case this.w_PARAM="PR"
                this.w_MESS = "Si vuole aggiornare il protocollo della dichiarazione sulle altre righe del movimento?"
                this.w_OK = .T.
              case this.w_PARAM="ME" And this.oParentObject.w_PITIPMOV$"CS-AS"
                this.w_MESS = "Si vuole aggiornare la modalit� di erogazione sulle altre righe del movimento?"
                this.w_OK = .T.
              case this.w_PARAM="MI" And this.oParentObject.w_PITIPMOV$"CS-AS"
                this.w_MESS = "Si vuole aggiornare la modalit� di incasso sulle altre righe del movimento?"
                this.w_OK = .T.
              case this.w_PARAM="PA" And this.oParentObject.w_PITIPMOV$"CS-AS"
                this.w_MESS = "Si vuole aggiornare il paese di pagamento sulle altre righe del movimento?"
                this.w_OK = .T.
              case this.w_PARAM="SE"
                if this.w_TOTRIGA>1
                  this.w_MESS = "Si vuole aggiornare il segno della rettifica%0sulle altre righe del movimento?"
                  this.w_OK = .T.
                endif
            endcase
            if this.w_OK
              if ah_YesNo(this.w_MESS)
                this.w_PADRE.FirstRow()     
                do while !this.w_PADRE.Eof_Trs()
                  this.w_PADRE.SetRow()     
                  if this.w_PADRE.FullRow()
                    do case
                      case this.w_PARAM="NT"
                        this.oParentObject.w_PINATTRA = this.w_NATTRA
                      case this.w_PARAM="MT"
                        this.oParentObject.w_PIMODTRA = this.w_MODTRA
                      case this.w_PARAM="MC"
                        this.oParentObject.w_PICONDCO = this.w_CONDCO
                      case this.w_PARAM="PO"
                        this.oParentObject.w_PINAZORI = this.w_NAZORI
                      case this.w_PARAM="PP"
                        this.oParentObject.w_PINAZPRO = this.w_NAZPRO
                      case this.w_PARAM="VD"
                        this.oParentObject.w_PIPRODES = this.w_PRODES
                      case this.w_PARAM="ND"
                        this.oParentObject.w_PINAZDES = this.w_NAZDES
                      case this.w_PARAM="VO"
                        this.oParentObject.w_PIPROORI = this.w_PROORI
                      case this.w_PARAM="SZ"
                        this.oParentObject.w_PISEZDOG = this.w_SEZDOG
                      case this.w_PARAM="PR"
                        this.oParentObject.w_PIPRORET = this.w_PRORET
                      case this.w_PARAM="ME" 
                        this.oParentObject.w_PIPERSER = this.w_PERSER
                      case this.w_PARAM="MI"
                        this.oParentObject.w_PIMODINC = this.w_MODINC
                      case this.w_PARAM="PA"
                        this.oParentObject.w_PIPAEPAG = this.w_PAEPAG
                      case this.w_PARAM="SE"
                    endcase
                    this.w_PADRE.SaveRow()     
                  endif
                  this.w_PADRE.NextRow()     
                enddo
              endif
            endif
          endif
        endif
      endif
      this.w_PADRE.RePos()     
    endif
  endproc


  proc Init(oParentObject,w_PARAM)
    this.w_PARAM=w_PARAM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='VALUTE'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_PARAM"
endproc
