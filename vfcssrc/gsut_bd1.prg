* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bd1                                                        *
*              Eliminazione tabella parametri gestione studio                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-02-21                                                      *
* Last revis.: 2008-04-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,CodiceAzi,oMessage
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bd1",oParentObject,m.CodiceAzi,m.oMessage)
return(i_retval)

define class tgsut_bd1 as StdBatch
  * --- Local variables
  CodiceAzi = space(5)
  oMessage = .NULL.
  * --- WorkFile variables
  PAR_ALTE_idx=0
  PAR_ANTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eliminazione tabella PAR_ALTE
    * --- Try
    local bErr_03A85270
    bErr_03A85270=bTrsErr
    this.Try_03A85270()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      this.oMessage.AddMsgPartNL("Par_alte - parametri gestione studio")     
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_03A85270
    * --- End
    * --- Se attivato il modulo Antiriciclaggio
    if g_ANTI="S"
      * --- Try
      local bErr_03831C18
      bErr_03831C18=bTrsErr
      this.Try_03831C18()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.oMessage.AddMsgPartNL("Par_anti - parametri antiriciclaggio")     
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_03831C18
      * --- End
    endif
  endproc
  proc Try_03A85270()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("ELIMINO: %1 DA TABELLA: PAR_ALTE",.T.,.F.,.F., this.CodiceAzi)
    * --- Delete from PAR_ALTE
    i_nConn=i_TableProp[this.PAR_ALTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_ALTE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"PACODAZI = "+cp_ToStrODBC(this.CodiceAzi);
             )
    else
      delete from (i_cTable) where;
            PACODAZI = this.CodiceAzi;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_03831C18()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("ELIMINO: %1 DA TABELLA: PAR_ANTI",.T.,.F.,.F., this.CodiceAzi)
    * --- Delete from PAR_ANTI
    i_nConn=i_TableProp[this.PAR_ANTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_ANTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"PACODAZI = "+cp_ToStrODBC(this.CodiceAzi);
             )
    else
      delete from (i_cTable) where;
            PACODAZI = this.CodiceAzi;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return


  proc Init(oParentObject,CodiceAzi,oMessage)
    this.CodiceAzi=CodiceAzi
    this.oMessage=oMessage
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PAR_ALTE'
    this.cWorkTables[2]='PAR_ANTI'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="CodiceAzi,oMessage"
endproc
