* ---------------------------------------------------------------------------- *
* #%&%#Build:0000
*                                                                              *
*   Procedure: REGCOS_V                                                        *
*              Corrispettivi scorporo Vert.                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 10/4/01                                                         *
* Last revis.: 11/9/14                                                         *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
private i_formh13,i_formh14
private i_brk,i_quit,i_row,i_pag,i_oldarea,i_oldrows
private i_rdvars,i_rdkvars,i_rdkey
private w_s,i_rec,i_wait,i_modal
private i_usr_brk          && .T. se l'utente ha interrotto la stampa
private i_frm_rpr          && .T. se � stata stampata la testata del report
private Sm                 && Variabile di appoggio per formattazione memo
private i_form_ph,i_form_phh,i_form_phg,i_form_pf,i_saveh13,i_exec

* --- Variabili per configurazione stampante
private w_t_stdevi,w_t_stmsin,w_t_stnrig
private w_t_strese,w_t_stlung,w_t_st10,w_t_st12,w_t_st15
private w_t_stcomp,w_t_stnorm,w_t_stnlq,w_t_stdraf
private w_t_stpica,w_t_stelit
private w_t_stbold,w_t_stwide,w_t_stital,w_t_stunde
private w_t_stbol_,w_t_stwid_,w_t_stita_,w_t_stund_
private w_stdesc
store " " to w_t_stdevi
store " " to w_t_strese,w_t_stlung,w_t_st10,w_t_st12,w_t_st15
store " " to w_t_stcomp,w_t_stnorm,w_t_stnlq,w_t_stdraf
store " " to w_t_stpica,w_t_stelit, i_rdkey
store " " to w_t_stbold,w_t_stwide,w_t_stital,w_t_stunde
store " " to w_t_stbol_,w_t_stwid_,w_t_stita_,w_t_stund_
store ""  to Sm
store 0 to i_formh13,i_formh14,i_form_phh,i_form_phg,i_saveh13
store space(20) to w_stdesc
i_form_ph = 12
i_form_pf = 13
i_wait = 1
i_modal = .t.
i_oldrows = 0
store .F. to i_usr_brk, i_frm_rpr, w_s
i_exec = ""

dimension i_rdvars[41,2],i_rdkvars[6,2],i_zoompars[3]
store 0 to i_rdvars[1,1],i_rdkvars[1,1]
private w_TIPREC,w_CORGIO,w_TIPO,w_TOTCOR,w_COMPET
private P_ELENCO,w_RIEIMP,w_RIEIVA,w_RIEIMP1,w_RIEIMP2
private w_RIEIVA1,w_RIEIVA2,w_ARIMP1,w_ARIMP2,w_ARIVA1
private w_ARIVA2,w_APIMP,w_APIVA,w_ARIMP,w_ARIVA
private w_EPIMP,w_EPIVA,w_ERIMP,w_ERIVA,w_APIMPT
private w_APIVAT,w_ARIMPT,w_ARIVAT,w_SETTA,w_SETTA
private PREFIS,NUMPAG,p_RAGAZI,p_PARTO,w_DATIAZ
private p_CODO,w_Set,w_INTESTA,P_DATINI,P_DATFIN
private P_VALSIM,P_DESVAL,L_STAMPATO,w_SETTA,w_SETTA
private PREFIS,NUMPAG,p_RAGAZI,p_PARTO,w_DATIAZ
private p_CODO,w_INTESTA,P_DATINI,P_DATFIN,P_VALSIM
private P_DESVAL
w_TIPREC = space(10)
w_CORGIO = space(30)
w_TIPO = space(2)
w_TOTCOR = space(18)
w_COMPET = space(9)
P_ELENCO = space(0)
w_RIEIMP = 0
w_RIEIVA = 0
w_RIEIMP1 = 0
w_RIEIMP2 = 0
w_RIEIVA1 = 0
w_RIEIVA2 = 0
w_ARIMP1 = 0
w_ARIMP2 = 0
w_ARIVA1 = 0
w_ARIVA2 = 0
w_APIMP = 0
w_APIVA = 0
w_ARIMP = 0
w_ARIVA = 0
w_EPIMP = 0
w_EPIVA = 0
w_ERIMP = 0
w_ERIVA = 0
w_APIMPT = 0
w_APIVAT = 0
w_ARIMPT = 0
w_ARIVAT = 0
w_SETTA = space(1)
w_SETTA = space(1)
PREFIS = space(20)
NUMPAG = 0
p_RAGAZI = space(99)
p_PARTO = space(16)
w_DATIAZ = space(99)
p_CODO = space(16)
w_Set = space(10)
w_INTESTA = space(35)
P_DATINI = ctod("  /  /  ")
P_DATFIN = ctod("  /  /  ")
P_VALSIM = space(3)
P_DESVAL = space(35)
L_STAMPATO = .f.
w_SETTA = space(1)
w_SETTA = space(1)
PREFIS = space(20)
NUMPAG = 0
p_RAGAZI = space(99)
p_PARTO = space(16)
w_DATIAZ = space(99)
p_CODO = space(16)
w_INTESTA = space(35)
P_DATINI = ctod("  /  /  ")
P_DATFIN = ctod("  /  /  ")
P_VALSIM = space(3)
P_DESVAL = space(35)

i_oldarea = select()
select __tmp__
go top

w_t_stnrig = 65
w_t_stmsin = 0
  
  
* --- Inizializza Variabili per configurazione stampante da CP_CHPRN
w_t_stdevi = cFileStampa+'.prn'
w_t_stlung = ts_ForPag
w_t_stnrig = ts_RowOk 
w_t_strese = ts_Reset+ts_Inizia
w_t_st10 = ts_10Cpi
w_t_st12 = ts_12Cpi
w_t_st15 = ts_15Cpi
w_t_stcomp = ts_Comp
w_t_stnorm = ts_RtComp
w_t_stbold = ts_StBold
w_t_stwide = ts_StDoub
w_t_stital = ts_StItal
w_t_stunde = ts_StUnde
w_t_stbol_ = ts_FiBold
w_t_stwid_ = ts_FiDoub
w_t_stita_ = ts_FiItal
w_t_stund_ = ts_FiUnde
* --- non definiti
*w_t_stmsin
*w_t_stnlq
*w_t_stdraf
*w_t_stpica
*w_t_stelit

i_row = 0
i_pag = 1
*---------------------------------------
wait wind "Generazione file appoggio..." nowait
*----------------------------------------
activate screen
* --- Settaggio stampante
set printer to &w_t_stdevi
set device to printer
set margin to w_t_stmsin
if len(trim(w_t_strese))>0
  @ 0,0 say &w_t_strese
endif
if len(trim(w_t_stlung))>0
  @ 0,0 say &w_t_stlung
endif
* --- Inizio stampa
do REGC4S_V with 1, 0
if i_frm_rpr .and. .not. i_usr_brk
  * stampa il piede del report
  do REGC4S_V with 14, 0
endif
if i_row<>0 
  @ prow(),pcol() say chr(12)
endif
set device to screen
set printer off
set printer to
if .not. i_frm_rpr
  do cplu_erm with "Non ci sono dati da stampare"
endif
* --- Fine
if .not.(empty(wontop()))
  activate  window (wontop())
endif
i_warea = alltrim(str(i_oldarea))
select (i_oldarea)
return


procedure REGC4S_V
* === Procedure REGC4S_V
parameters i_form_id, i_height

private i_currec, i_prevrec, i_formh
private i_frm_brk    && flag che indica il verificarsi di un break interform
                     && anche se la specifica condizione non � soddisfatta
private i_break, i_cond1

do case
  case i_form_id=1
    select __tmp__
    i_warea = '__tmp__'
    * --- inizializza le condizioni dei break interform
    i_cond1 = (TIPREC)
    i_frm_brk = .T.
    do while .not. eof() .and. (TIPREC<>'R')
     wait wind "Elabora riga:"+str(recno()) +"/"+str(reccount()) nowait
      if .not. i_frm_rpr
        * --- stampa l'intestazione del report
        do REGC4S_V with 11, 0
        i_frm_rpr = .T.
      endif
      if i_cond1<>(TIPREC) .or. i_frm_brk
        i_cond1 = (TIPREC)
        i_frm_brk = .T.
        do REGC5S_V with 1.00, 0
      endif
      i_frm_brk = .F.
      * stampa del dettaglio
        do REGC5S_V with 1.01, 0
      if TIPREC$ 'FI' and tipdoc<>'FC'
        do REGC5S_V with 1.02, 1
      endif
      if NVL(FLPROV,'N')='S' AND TIPREC$ 'FI'
        do REGC5S_V with 1.03, 1
      endif
      if i_usr_brk
        exit
      endif
      * --- passa al record successivo
      i_prevrec = recno()
      if .not. eof()
        skip
      endif
      i_currec = iif(eof(), -1, recno())
      if eof() .or..not.(TIPREC<>'R') .or. i_cond1<>(TIPREC)      
        go i_prevrec
          do REGC5S_V with 1.04, 0
        if TIPREC='F' AND NOT EMPTY(L_ELENCO)
          do REGC5S_V with 1.05, 1
        endif
        if EndOfGroup() .OR. EOF()
          do REGC5S_V with 1.06, 1
        endif
        do cplu_go with i_currec
      endif
    enddo
    * --- imposta i_row in modo da provocare un salto pagina
    i_row = w_t_stnrig
    * --- lancio del form successivo
    if .not. i_usr_brk
      do REGC4S_V with 2, 0
    endif
  case i_form_id=2
    select __tmp__
    i_warea = '__tmp__'
    * --- inizializza le condizioni dei break interform
    i_cond1 = (TIPREC)
    i_frm_brk = .T.
    do while .not. eof() 
     wait wind "Elabora riga:"+str(recno()) +"/"+str(reccount()) nowait
      if .not. i_frm_rpr
        * --- stampa l'intestazione del report
        do REGC4S_V with 11, 0
        i_frm_rpr = .T.
      endif
      if i_cond1<>(TIPREC) .or. i_frm_brk
        i_cond1 = (TIPREC)
        i_frm_brk = .T.
        do REGC5S_V with 2.00, 1
      endif
      i_frm_brk = .F.
      * stampa del dettaglio
        do REGC5S_V with 2.01, 0
      if TIPREC='R'
        do REGC5S_V with 2.02, 1
      endif
      if TIPREC='R' and (IMPSTA+IVASTA)<>0
        do REGC5S_V with 2.03, 2
      endif
      if TIPREC='R' and (IMPPRE+IVAPRE)<>0
        do REGC5S_V with 2.04, 1
      endif
      if TIPREC='R' and (IMPSEG+IVASEG)<>0
        do REGC5S_V with 2.05, 1
      endif
      if TIPREC='R'
        do REGC5S_V with 2.06, 1
      endif
      if TIPREC='R'
        do REGC5S_V with 2.07, 0
      endif
      if i_usr_brk
        exit
      endif
      * --- passa al record successivo
      i_prevrec = recno()
      if .not. eof()
        skip
      endif
      i_currec = iif(eof(), -1, recno())
      if eof()  .or. i_cond1<>(TIPREC)      
        go i_prevrec
        do REGC5S_V with 2.08, 5
        if TIPREC='R'
          do REGC5S_V with 2.09, 2
        endif
        do cplu_go with i_currec
      endif
    enddo
  case i_form_id=11
    do REGC5S_V with 11.00, 12
  case i_form_id=12
    do REGC5S_V with 12.00, 12
  case i_form_id=99
    * --- controllo per il salto pagina
    if inkey()=27
      i_usr_brk = .T.
    else
      if i_row+i_height+i_formh13>w_t_stnrig
        * --- stampa il piede di pagina
        i_row = w_t_stnrig-i_formh13
        if i_form_pf=13
          do REGC4S_V with 13, 0
        else
          do REGC5S_V with -i_form_pf,i_formh13
        endif
        i_row = 0
        i_pag = i_pag+1
        @ prow(),pcol() say chr(12)+chr(13)
        w_s = 0
        * --- stampa l'intestazione di pagina
        if i_form_ph=12
          do REGC4S_V with 12, 0
        else
          do REGC5S_V with -i_form_ph,i_form_phh
        endif
      endif
    endif
  case i_form_id=98
    i_form_pf = 0.00
    i_saveh13 = i_formh13
    i_formh13 = 0
endcase
return

procedure REGC5S_V
* === Procedure REGC5S_V
parameters i_form_id, i_form_h

* --- controllo per il salto pagina
if i_form_id<11 .and. i_form_id>0
  do REGC4S_V with 99, i_form_h
  if i_usr_brk
    return
  endif
endif
if i_form_id<0
  i_form_id = -i_form_id
endif
do case
  * --- 1� form
  case i_form_id=1.0
    do frm1_0
  case i_form_id=1.01
    do frm1_01
  case i_form_id=1.02
    do frm1_02
  case i_form_id=1.03
    do frm1_03
  case i_form_id=1.04
    do frm1_04
  case i_form_id=1.05
    do frm1_05
  case i_form_id=1.06
    do frm1_06
  * --- 2� form
  case i_form_id=2.0
    do frm2_0
  case i_form_id=2.01
    do frm2_01
  case i_form_id=2.02
    do frm2_02
  case i_form_id=2.03
    do frm2_03
  case i_form_id=2.04
    do frm2_04
  case i_form_id=2.05
    do frm2_05
  case i_form_id=2.06
    do frm2_06
  case i_form_id=2.07
    do frm2_07
  case i_form_id=2.08
    do frm2_08
  case i_form_id=2.09
    do frm2_09
  * --- 11� form
  case i_form_id=11.0
    do frm11_0
  * --- 12� form
  case i_form_id=12.0
    do frm12_0
endcase
i_row = i_row+i_form_h
return


* --- 1� form
procedure frm1_0
return

* --- frm1_01
procedure frm1_01
return

* --- frm1_02
procedure frm1_02
  w_TIPREC = CP_TODATE(datreg)
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(42-41),1,at_x(10),transform(w_TIPREC,""),i_fn
  w_CORGIO = left(descri,35)
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(42-41),13,at_x(104),transform(w_CORGIO,""),i_fn
  w_TIPO = tipdoc
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(42-41),44,at_x(356),transform(w_TIPO,""),i_fn
  w_TOTCOR = IMPONI+IMPIVA
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(42-41),51,at_x(410),transform(w_TOTCOR,V_PV[14]),i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(42-41),68,at_x(550),transform(IMPONI,V_PV[14]),i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(42-41),83,at_x(668),transform(IMPIVA,V_PV[14]),i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(42-41),98,at_x(784),transform(PERIVA,'999.9'),i_fn
  w_COMPET = IIF(VAL(COMPET)>9.or.VAL(COMPET)=0, "", " ")+COMPET
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(42-41),105,at_x(845),transform(w_COMPET,""),i_fn
return

* --- frm1_03
procedure frm1_03
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(83-79),1,at_x(9),"*** Registrazione non confermata ***",i_fn
return

* --- frm1_04
procedure frm1_04
return

* --- frm1_05
procedure frm1_05
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(140-139),2,at_x(16),"Nell' importo dei Corrispettivi sono comprese le fatture N. :",i_fn
  P_ELENCO = L_ELENCO
  i_fn = ""
  w_s = 51
  set memowidth to w_s
  Sm = mline(P_ELENCO,1)+space(w_s-len(mline(P_ELENCO,1)))
  do F_Say with i_row+0,i_row+at_y(140-139),65,at_x(520),alltrim(Sm),i_fn
  i_ml = 0
  w_s = 51
  set memowidth to w_s
  i_ml = max(i_ml,memlines(P_ELENCO))
  i_row = i_row+i_form_h
  for i_i=2 to i_ml
    do REGC4S_V with 99,1
    if i_usr_brk
      exit
    endif
    w_s = 51
    set memowidth to w_s
      i_fn = ""
    Sm = mline(P_ELENCO,i_i)+space(w_s-len(mline(P_ELENCO,i_i)))
    do F_Say with i_row,i_row,65,at_x(520),alltrim(Sm),i_fn
    i_row = i_row+1
  next
  i_row = i_row-i_form_h
return

* --- frm1_06
procedure frm1_06
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(181-177),18,at_x(145),"Lo spazio sottostante di questa pagina non e' stato utilizzato ed e' da",i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(181-177),90,at_x(723),"considerarsi annullato",i_fn
return

* --- 2� form
procedure frm2_0
return

* --- frm2_01
procedure frm2_01
return

* --- frm2_02
procedure frm2_02
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(58-57),3,at_x(25),transform(CODIVA,""),i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(58-57),10,at_x(81),transform(PERIVA,'999.9'),i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(58-57),18,at_x(149),transform(DESIVA,""),i_fn
return

* --- frm2_03
procedure frm2_03
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(115-95),8,at_x(69),"Documenti Registrati nel Periodo:",i_fn
  if IMPSTA<>0
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(115-95),41,at_x(335),transform(IMPSTA,V_PV[14]),i_fn
  endif
  if IVASTA<>0
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(115-95),57,at_x(462),transform(IVASTA,V_PV[14]),i_fn
  endif
return

* --- frm2_04
procedure frm2_04
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(153-152),0,at_x(5),"- Documenti Competenza del Periodo Prec.:",i_fn
  if IMPPRE<>0
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(153-152),41,at_x(335),transform(IMPPRE,V_PV[14]),i_fn
  endif
  if IVAPRE<>0
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(153-152),57,at_x(462),transform(IVAPRE,V_PV[14]),i_fn
  endif
return

* --- frm2_05
procedure frm2_05
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(191-190),1,at_x(13),"+ Documenti Registrati nel Periodo Seg.:",i_fn
  if IMPSEG<>0
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(191-190),41,at_x(335),transform(IMPSEG,V_PV[14]),i_fn
  endif
  if IVASEG<>0
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(191-190),57,at_x(462),transform(IVASEG,V_PV[14]),i_fn
  endif
return

* --- frm2_06
procedure frm2_06
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(229-228),11,at_x(93),"= Doc. Competenza del Periodo:",i_fn
  w_RIEIMP = (IMPSTA+IMPSEG)-IMPPRE
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(229-228),41,at_x(335),transform(w_RIEIMP,V_PV[14]),i_fn
  w_RIEIVA = (IVASTA+IVASEG)-IVAPRE
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(229-228),57,at_x(462),transform(w_RIEIVA,V_PV[14]),i_fn
  w_RIEIMP1 = IIF(NVL(PERIVA,0)=0,0,w_RIEIMP)
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(229-228),72,at_x(579),transform(w_RIEIMP1,V_PV[20]),i_fn
   endif
   w_APIMP = w_APIMP+w_RIEIMP1
  w_RIEIMP2 = IIF(NVL(PERIVA,0)=0,w_RIEIMP,0)
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(229-228),75,at_x(607),transform(w_RIEIMP2,V_PV[20]),i_fn
   endif
   w_EPIMP = w_EPIMP+w_RIEIMP2
  w_RIEIVA1 = IIF(NVL(PERIVA,0)=0,0,w_RIEIVA)
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(229-228),79,at_x(635),transform(w_RIEIVA1,V_PV[20]),i_fn
   endif
   w_APIVA = w_APIVA+w_RIEIVA1
  w_RIEIVA2 = IIF(NVL(PERIVA,0)=0,w_RIEIVA,0)
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(229-228),82,at_x(663),transform(w_RIEIVA2,V_PV[20]),i_fn
   endif
   w_EPIVA = w_EPIVA+w_RIEIVA2
  w_ARIMP1 = IIF(NVL(PERIVA,0)=0,0,PROIMP)
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(229-228),86,at_x(691),transform(w_ARIMP1,V_PV[20]),i_fn
   endif
  w_ARIMP2 = IIF(NVL(PERIVA,0)=0,PROIMP,0)
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(229-228),89,at_x(719),transform(w_ARIMP2,V_PV[20]),i_fn
   endif
  w_ARIVA1 = IIF(NVL(PERIVA,0)=0,0,PROIVA)
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(229-228),93,at_x(747),transform(w_ARIVA1,V_PV[20]),i_fn
   endif
  w_ARIVA2 = IIF(NVL(PERIVA,0)=0,PROIVA,0)
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(229-228),97,at_x(776),transform(w_ARIVA2,V_PV[20]),i_fn
   endif
return

* --- frm2_07
procedure frm2_07
return

* --- frm2_08
procedure frm2_08
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(304-300),72,at_x(576),"Imponibile Progr.IVA Esigibile Progr.",i_fn
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(324-300),25,at_x(205),"Totale a Debito:",i_fn
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(324-300),41,at_x(335),transform(w_APIMP,V_PV[14]),i_fn
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(324-300),57,at_x(462),transform(w_APIVA,V_PV[14]),i_fn
  w_ARIMP = L_ARIMP
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(324-300),73,at_x(589),transform(w_ARIMP,V_PV[14]),i_fn
  w_ARIVA = L_ARIVA
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(324-300),89,at_x(715),transform(w_ARIVA,V_PV[14]),i_fn
  i_fn = ""
  do F_Say with i_row+3,i_row+at_y(343-300),19,at_x(157),"Totale altre Aliquote:",i_fn
  i_fn = ""
  do F_Say with i_row+3,i_row+at_y(343-300),41,at_x(335),transform(w_EPIMP,V_PV[14]),i_fn
  i_fn = ""
  do F_Say with i_row+3,i_row+at_y(343-300),57,at_x(462),transform(w_EPIVA,V_PV[14]),i_fn
  w_ERIMP = L_ERIMP
  i_fn = ""
  do F_Say with i_row+3,i_row+at_y(343-300),73,at_x(589),transform(w_ERIMP,V_PV[14]),i_fn
  w_ERIVA = L_ERIVA
  i_fn = ""
  do F_Say with i_row+3,i_row+at_y(343-300),89,at_x(715),transform(w_ERIVA,V_PV[14]),i_fn
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(362-300),22,at_x(181),"Totale Complessivo:",i_fn
  w_APIMPT = w_APIMP+w_EPIMP
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(362-300),41,at_x(335),transform(w_APIMPT,V_PV[14]),i_fn
  w_APIVAT = w_APIVA+w_EPIVA
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(362-300),57,at_x(462),transform(w_APIVAT,V_PV[14]),i_fn
  w_ARIMPT = L_ARIMP+L_ERIMP
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(362-300),73,at_x(589),transform(w_ARIMPT,V_PV[14]),i_fn
  w_ARIVAT = L_ARIVA+L_ERIVA
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(362-300),89,at_x(715),transform(w_ARIVAT,V_PV[14]),i_fn
return

* --- frm2_09
procedure frm2_09
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(412-399),17,at_x(137),"��������������������������������������������������������������������������������",i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(412-399),97,at_x(777),"������",i_fn
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(432-399),16,at_x(129),"Lo spazio sottostante di questa pagina non e' stato utilizzato ed e' da",i_fn
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(432-399),88,at_x(708),"considerarsi annullato",i_fn
return

* --- 11� form
procedure frm11_0
  w_SETTA = " "
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(0-0),0,at_x(3),transform(w_SETTA,"X"),i_fn
  if len(trim('&w_t_stnorm'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stnorm,""
  endif
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(0-0),11,at_x(91),"// Setta carattere di stampa e compresso",i_fn
   endif
  w_SETTA = " "
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(0-0),60,at_x(483),transform(w_SETTA,"X"),i_fn
  if len(trim('&w_t_stcomp'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stcomp,""
  endif
  if L_INTLIG='S'
  if len(trim('&w_t_stnorm'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stnorm,""
  endif
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(19-0),0,at_x(4),"REGISTRO IVA CORRISPETTIVI",i_fn
  if len(trim('&w_t_stcomp'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stcomp,""
  endif
  endif
  PREFIS = RIGHT(SPACE(20)+ALLTRIM(L_PREFIS),20)
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(38-0),89,at_x(715),transform(PREFIS,""),i_fn
  endif
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(38-0),111,at_x(891),"Pag.",i_fn
  endif
  NUMPAG = L_PRPARI+I_PAG
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(38-0),125,at_x(1000),transform(NUMPAG,"9999999"),i_fn
   STPAG=NUMPAG
  endif
  p_RAGAZI = g_RAGAZI
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+3,i_row+at_y(57-0),0,at_x(3),transform(p_RAGAZI,""),i_fn
  endif
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+3,i_row+at_y(57-0),103,at_x(827),"Partita IVA:",i_fn
  endif
  p_PARTO = RIGHT(SPACE(16) + ALLTRIM(L_PIVAZI),16)
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+3,i_row+at_y(57-0),116,at_x(928),transform(p_PARTO,""),i_fn
  endif
  w_DATIAZ = trim(L_INDAZI)+' - '+L_CAPAZI+' - '+TRIM(L_LOCAZI)+IIF(EMPTY(L_PROAZI),'',' ( '+L_PROAZI+' )')
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(76-0),0,at_x(3),transform(w_DATIAZ,""),i_fn
  endif
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(76-0),100,at_x(803),"Codice fiscale:",i_fn
  endif
  p_CODO = RIGHT(SPACE(16) + ALLTRIM(L_COFAZI),16)
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(76-0),116,at_x(928),transform(p_CODO,""),i_fn
  endif
  if .f.
  if len(trim('&w_t_st10'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_st10,""
  endif
  i_fn = ""
  do F_Say with i_row+7,i_row+at_y(138-0),0,at_x(2),transform(w_Set,""),i_fn
   endif
  w_INTESTA = CompString()
  if len(trim('&w_t_stcomp'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stcomp,""
  endif
  i_fn = ""
  do F_Say with i_row+7,i_row+at_y(138-0),2,at_x(18),transform(w_INTESTA,""),i_fn
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(165-0),1,at_x(8),"Dal:",i_fn
  P_DATINI = L_DATINI
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(165-0),5,at_x(42),transform(P_DATINI,""),i_fn
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(165-0),17,at_x(142),"Al:",i_fn
  P_DATFIN = L_DATFIN
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(165-0),20,at_x(167),transform(P_DATFIN,""),i_fn
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(165-0),40,at_x(320),"Importi espressi in:",i_fn
  P_VALSIM = G_VALSIM
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(165-0),60,at_x(487),transform(P_VALSIM,""),i_fn
  P_DESVAL = L_DESVAL
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(165-0),65,at_x(520),transform(P_DESVAL,""),i_fn
  L_STAMPATO = .t.
  if .f.
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(165-0),101,at_x(813),transform(L_STAMPATO,""),i_fn
   endif
  if TIPREC$ 'FI'
  i_fn = ""
  do F_Say with i_row+10,i_row+at_y(200-0),1,at_x(8),"Data Reg.   Denominazione Corrispettivi   Tipo",i_fn
  endif
  if TIPREC$ 'FI'
  i_fn = ""
  do F_Say with i_row+10,i_row+at_y(200-0),48,at_x(386),"Tot.Corrispettivi       Imponibile        Imposta  % IVA",i_fn
  endif
  if TIPREC$ 'FI'
  i_fn = ""
  do F_Say with i_row+10,i_row+at_y(200-0),106,at_x(848),"Comp.",i_fn
  endif
  if TIPREC='R'
  i_fn = ""
  do F_Say with i_row+11,i_row+at_y(224-0),0,at_x(2)," C.I.     %      Descrizione",i_fn
  endif
  if TIPREC='R'
  i_fn = ""
  do F_Say with i_row+11,i_row+at_y(224-0),36,at_x(291),"Imponibile Periodo IVA Esigibile Periodo",i_fn
  endif
return

* --- 12� form
procedure frm12_0
  w_SETTA = " "
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(0-0),0,at_x(3),transform(w_SETTA,"X"),i_fn
  if len(trim('&w_t_stnorm'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stnorm,""
  endif
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(0-0),11,at_x(91),"// Setta carattere di stampa e compresso",i_fn
   endif
  w_SETTA = " "
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(0-0),60,at_x(483),transform(w_SETTA,"X"),i_fn
  if len(trim('&w_t_stcomp'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stcomp,""
  endif
  if L_INTLIG='S'
  if len(trim('&w_t_stnorm'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stnorm,""
  endif
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(19-0),0,at_x(3),"REGISTRO IVA CORRISPETTIVI",i_fn
  if len(trim('&w_t_stcomp'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stcomp,""
  endif
  endif
  PREFIS = RIGHT(SPACE(20)+ALLTRIM(L_PREFIS),20)
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(38-0),89,at_x(715),transform(PREFIS,""),i_fn
  endif
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(38-0),111,at_x(892),"Pag.",i_fn
  endif
  NUMPAG = L_PRPARI+I_PAG
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(38-0),125,at_x(1000),transform(NUMPAG,"9999999"),i_fn
   STPAG=NUMPAG
  endif
  p_RAGAZI = g_RAGAZI
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+3,i_row+at_y(57-0),0,at_x(3),transform(p_RAGAZI,""),i_fn
  endif
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+3,i_row+at_y(57-0),103,at_x(828),"Partita IVA:",i_fn
  endif
  p_PARTO = RIGHT(SPACE(16) + ALLTRIM(L_PIVAZI),16)
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+3,i_row+at_y(57-0),116,at_x(928),transform(p_PARTO,""),i_fn
  endif
  w_DATIAZ = trim(L_INDAZI)+' - '+L_CAPAZI+' - '+TRIM(L_LOCAZI)+IIF(EMPTY(L_PROAZI),'',' ( '+L_PROAZI+' )')
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(76-0),0,at_x(3),transform(w_DATIAZ,""),i_fn
  endif
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(76-0),100,at_x(804),"Codice fiscale:",i_fn
  endif
  p_CODO = RIGHT(SPACE(16) + ALLTRIM(L_COFAZI),16)
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(76-0),116,at_x(928),transform(p_CODO,""),i_fn
  endif
  w_INTESTA = CompString()
  if len(trim('&w_t_stcomp'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stcomp,""
  endif
  i_fn = ""
  do F_Say with i_row+7,i_row+at_y(138-0),2,at_x(18),transform(w_INTESTA,""),i_fn
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(165-0),1,at_x(8),"Dal:",i_fn
  P_DATINI = L_DATINI
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(165-0),5,at_x(42),transform(P_DATINI,""),i_fn
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(165-0),17,at_x(142),"Al:",i_fn
  P_DATFIN = L_DATFIN
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(165-0),20,at_x(167),transform(P_DATFIN,""),i_fn
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(165-0),40,at_x(320),"Importi espressi in:",i_fn
  P_VALSIM = G_VALSIM
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(165-0),60,at_x(487),transform(P_VALSIM,""),i_fn
  P_DESVAL = L_DESVAL
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(165-0),65,at_x(520),transform(P_DESVAL,""),i_fn
  if TIPREC$ 'FI'
  i_fn = ""
  do F_Say with i_row+10,i_row+at_y(200-0),1,at_x(8),"Data Reg.   Denominazione Corrispettivi   Tipo",i_fn
  endif
  if TIPREC$ 'FI'
  i_fn = ""
  do F_Say with i_row+10,i_row+at_y(200-0),48,at_x(386),"Tot.Corrispettivi       Imponibile        Imposta  % IVA",i_fn
  endif
  if TIPREC$ 'FI'
  i_fn = ""
  do F_Say with i_row+10,i_row+at_y(200-0),106,at_x(848),"Comp.",i_fn
  endif
  if TIPREC='R'
  i_fn = ""
  do F_Say with i_row+11,i_row+at_y(224-0),0,at_x(2)," C.I.     %      Descrizione",i_fn
  endif
  if TIPREC='R'
  i_fn = ""
  do F_Say with i_row+11,i_row+at_y(224-0),36,at_x(291),"Imponibile Periodo IVA Esigibile Periodo",i_fn
  endif
return

function at_x
parameters i_pos, i_fnt, i_h, i_s
  i_pos = i_pos/6
  return i_pos

function at_y
parameters i_pos, i_fnt, i_h, i_s
  i_pos = i_pos/13
  return i_pos

procedure F_Say
  parameter y,py,x,px,s,f
  * --- Questa funzione corregge un errore del driver "Generica solo testo"
  *     di Windows che aggiunge spazi oltre la 89 colonna
  *     Inoltre in Windows sostituisce il carattere 196 con un '-'
  if y=-1
    y = prow()
    x = pcol()
  endif
  @ y,x say s
  return

PROCEDURE CPLU_GO
parameter i_recpos

if i_recpos<=0 .or. i_recpos>reccount()
  if reccount()<>0
    goto bottom
    if .not. eof()
      skip
    endif
  endif  
else
  goto i_recpos
endif
return

* --- Area Manuale = Functions & Procedures 
* --- REGCOS_V

FUNCTION CompString
* Composizione stringa per intestazione pagina
private w_r_Stringa
w_r_Stringa=Space(35)

if TIPREC$'F'
   w_r_Stringa='Registro IVA corrispettivi/scorporo num. '+Alltrim(str(L_NUMREG,6,0)) +'  anno: ' + alltrim(str(year(l_datini)))
else
     if TIPREC$'R' 
        w_r_Stringa='Riepilogo registro IVA corrispettivi/scorporo num. '+Alltrim(str(L_NUMREG,6,0)) +'  anno: ' + alltrim(str(year(l_datini)))
     endif
endif

return (w_r_Stringa)

FUNCTION EndOfGroup()
*Verifica fine gruppo e fine pagina per stampa
*dicitura di annullamento spazio sottostante

Private w_Ret, w_CtrlREC
w_Ret=.f.
w_CtrlREC=TIPREC
skip 
If w_CtrlREC<>TIPREC .and. i_Row<>ts_RowOk .and. !empty(TIPREC)
      w_Ret=.t.
endif
skip -1

Return (w_Ret)

* --- Fine Area Manuale 
