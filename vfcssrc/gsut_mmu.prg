* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_mmu                                                        *
*              Dettaglio Email                                                 *
*                                                                              *
*      Author: Zucchetti s.p.a.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-04-26                                                      *
* Last revis.: 2016-04-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsut_mmu")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsut_mmu")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsut_mmu")
  return

* --- Class definition
define class tgsut_mmu as StdPCForm
  Width  = 624
  Height = 198
  Top    = 4
  Left   = 9
  cComment = "Dettaglio Email"
  cPrg = "gsut_mmu"
  HelpContextID=197739369
  add object cnt as tcgsut_mmu
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsut_mmu as PCContext
  w_UMCODUTE = 0
  w_ISALT = space(1)
  w_UMDIALOG = space(1)
  w_UTEMAIL = space(254)
  w_RETURN = space(1)
  w_SETCALLER = space(1)
  w_UMCODAZI = space(5)
  w_CPROWORD = 0
  w_UMFLGPEC = space(1)
  w_UMSERIAL = space(5)
  w_AM_INVIO = space(1)
  w_AM_EMAIL = space(254)
  w_UMFLGDEF = space(1)
  w_UMFLSYNC = space(1)
  w_UMSYNCTO = 0
  w_EDITMAIL = space(1)
  w_AMSHARED = space(1)
  w_AMAUTSRV = space(1)
  w_AMCCNEML = space(254)
  w_AMCC_EML = space(254)
  w_AM_FIRMA = space(10)
  proc Save(i_oFrom)
    this.w_UMCODUTE = i_oFrom.w_UMCODUTE
    this.w_ISALT = i_oFrom.w_ISALT
    this.w_UMDIALOG = i_oFrom.w_UMDIALOG
    this.w_UTEMAIL = i_oFrom.w_UTEMAIL
    this.w_RETURN = i_oFrom.w_RETURN
    this.w_SETCALLER = i_oFrom.w_SETCALLER
    this.w_UMCODAZI = i_oFrom.w_UMCODAZI
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_UMFLGPEC = i_oFrom.w_UMFLGPEC
    this.w_UMSERIAL = i_oFrom.w_UMSERIAL
    this.w_AM_INVIO = i_oFrom.w_AM_INVIO
    this.w_AM_EMAIL = i_oFrom.w_AM_EMAIL
    this.w_UMFLGDEF = i_oFrom.w_UMFLGDEF
    this.w_UMFLSYNC = i_oFrom.w_UMFLSYNC
    this.w_UMSYNCTO = i_oFrom.w_UMSYNCTO
    this.w_EDITMAIL = i_oFrom.w_EDITMAIL
    this.w_AMSHARED = i_oFrom.w_AMSHARED
    this.w_AMAUTSRV = i_oFrom.w_AMAUTSRV
    this.w_AMCCNEML = i_oFrom.w_AMCCNEML
    this.w_AMCC_EML = i_oFrom.w_AMCC_EML
    this.w_AM_FIRMA = i_oFrom.w_AM_FIRMA
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_UMCODUTE = this.w_UMCODUTE
    i_oTo.w_ISALT = this.w_ISALT
    i_oTo.w_UMDIALOG = this.w_UMDIALOG
    i_oTo.w_UTEMAIL = this.w_UTEMAIL
    i_oTo.w_RETURN = this.w_RETURN
    i_oTo.w_SETCALLER = this.w_SETCALLER
    i_oTo.w_UMCODAZI = this.w_UMCODAZI
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_UMFLGPEC = this.w_UMFLGPEC
    i_oTo.w_UMSERIAL = this.w_UMSERIAL
    i_oTo.w_AM_INVIO = this.w_AM_INVIO
    i_oTo.w_AM_EMAIL = this.w_AM_EMAIL
    i_oTo.w_UMFLGDEF = this.w_UMFLGDEF
    i_oTo.w_UMFLSYNC = this.w_UMFLSYNC
    i_oTo.w_UMSYNCTO = this.w_UMSYNCTO
    i_oTo.w_EDITMAIL = this.w_EDITMAIL
    i_oTo.w_AMSHARED = this.w_AMSHARED
    i_oTo.w_AMAUTSRV = this.w_AMAUTSRV
    i_oTo.w_AMCCNEML = this.w_AMCCNEML
    i_oTo.w_AMCC_EML = this.w_AMCC_EML
    i_oTo.w_AM_FIRMA = this.w_AM_FIRMA
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsut_mmu as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 624
  Height = 198
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-04-29"
  HelpContextID=197739369
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=21

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  UTEMAILS_IDX = 0
  AZIENDA_IDX = 0
  ACC_MAIL_IDX = 0
  cFile = "UTEMAILS"
  cKeySelect = "UMCODUTE"
  cKeyWhere  = "UMCODUTE=this.w_UMCODUTE"
  cKeyDetail  = "UMCODUTE=this.w_UMCODUTE and UMCODAZI=this.w_UMCODAZI and UMSERIAL=this.w_UMSERIAL"
  cKeyWhereODBC = '"UMCODUTE="+cp_ToStrODBC(this.w_UMCODUTE)';

  cKeyDetailWhereODBC = '"UMCODUTE="+cp_ToStrODBC(this.w_UMCODUTE)';
      +'+" and UMCODAZI="+cp_ToStrODBC(this.w_UMCODAZI)';
      +'+" and UMSERIAL="+cp_ToStrODBC(this.w_UMSERIAL)';

  cKeyWhereODBCqualified = '"UTEMAILS.UMCODUTE="+cp_ToStrODBC(this.w_UMCODUTE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'UTEMAILS.UMCODAZI'
  cPrg = "gsut_mmu"
  cComment = "Dettaglio Email"
  i_nRowNum = 0
  i_nRowPerPage = 7
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_UMCODUTE = 0
  w_ISALT = .F.
  w_UMDIALOG = space(1)
  o_UMDIALOG = space(1)
  w_UTEMAIL = space(254)
  w_RETURN = .F.
  w_SETCALLER = .F.
  w_UMCODAZI = space(5)
  w_CPROWORD = 0
  w_UMFLGPEC = space(1)
  o_UMFLGPEC = space(1)
  w_UMSERIAL = space(5)
  o_UMSERIAL = space(5)
  w_AM_INVIO = space(1)
  o_AM_INVIO = space(1)
  w_AM_EMAIL = space(254)
  o_AM_EMAIL = space(254)
  w_UMFLGDEF = space(1)
  w_UMFLSYNC = space(1)
  o_UMFLSYNC = space(1)
  w_UMSYNCTO = 0
  w_EDITMAIL = .F.
  w_AMSHARED = space(1)
  w_AMAUTSRV = space(1)
  w_AMCCNEML = space(254)
  w_AMCC_EML = space(254)
  w_AM_FIRMA = space(0)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_mmuPag1","gsut_mmu",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gsut_mmu
    *** CHIUDO proc oPgFrm.Init
    Endproc
    
    proc SetCallerVars
       * --- Aggiorna var padre
       WITH this
         .oParentObject.w_AMCC_EML = .w_AMCC_EML
         .oParentObject.w_AMCCNEML = .w_AMCCNEML
         .oParentObject.w_AM_FIRMA = .w_AM_FIRMA
         .oParentObject.w_AMSHARED = .w_AMSHARED
         .oParentObject.SetControlsValue()
         .oParentObject.mEnableControls()
       ENDWITH
       RETURN
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='ACC_MAIL'
    this.cWorkTables[3]='UTEMAILS'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.UTEMAILS_IDX,5],7]
    this.nPostItConn=i_TableProp[this.UTEMAILS_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsut_mmu'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_4_joined
    link_2_4_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from UTEMAILS where UMCODUTE=KeySet.UMCODUTE
    *                            and UMCODAZI=KeySet.UMCODAZI
    *                            and UMSERIAL=KeySet.UMSERIAL
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.UTEMAILS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.UTEMAILS_IDX,2],this.bLoadRecFilter,this.UTEMAILS_IDX,"gsut_mmu")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('UTEMAILS')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "UTEMAILS.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' UTEMAILS '
      link_2_4_joined=this.AddJoinedLink_2_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'UMCODUTE',this.w_UMCODUTE  )
      select * from (i_cTable) UTEMAILS where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_ISALT = isalt()
        .w_UMDIALOG = space(1)
        .w_UTEMAIL = space(254)
        .w_RETURN = .T.
        .w_SETCALLER = .f.
        .w_UMCODUTE = NVL(UMCODUTE,0)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'UTEMAILS')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_AM_INVIO = space(1)
          .w_AMSHARED = space(1)
          .w_AMAUTSRV = space(1)
          .w_AMCCNEML = space(254)
          .w_AMCC_EML = space(254)
          .w_AM_FIRMA = space(0)
          .w_UMCODAZI = NVL(UMCODAZI,space(5))
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_UMFLGPEC = NVL(UMFLGPEC,space(1))
          .w_UMSERIAL = NVL(UMSERIAL,space(5))
          if link_2_4_joined
            this.w_UMSERIAL = NVL(AMSERIAL204,NVL(this.w_UMSERIAL,space(5)))
            this.w_AM_INVIO = NVL(AM_INVIO204,space(1))
            this.w_AM_EMAIL = NVL(AM_EMAIL204,space(254))
            this.w_AMAUTSRV = NVL(AMAUTSRV204,space(1))
            this.w_AMCC_EML = NVL(AMCC_EML204,space(254))
            this.w_AMCCNEML = NVL(AMCCNEML204,space(254))
            this.w_AM_FIRMA = NVL(AM_FIRMA204,space(0))
            this.w_AMSHARED = NVL(AMSHARED204,space(1))
          else
          .link_2_4('Load')
          endif
        .w_AM_EMAIL = STRTRAN( .w_AM_EMAIL, 'infinity:', '')
          .w_UMFLGDEF = NVL(UMFLGDEF,space(1))
          .w_UMFLSYNC = NVL(UMFLSYNC,space(1))
          .w_UMSYNCTO = NVL(UMSYNCTO,0)
        .w_EDITMAIL = Empty(.w_UMSERIAL)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace UMCODAZI with .w_UMCODAZI
          replace UMSERIAL with .w_UMSERIAL
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsut_mmu
    this.SetCallerVars()
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_UMCODUTE=0
      .w_ISALT=.f.
      .w_UMDIALOG=space(1)
      .w_UTEMAIL=space(254)
      .w_RETURN=.f.
      .w_SETCALLER=.f.
      .w_UMCODAZI=space(5)
      .w_CPROWORD=10
      .w_UMFLGPEC=space(1)
      .w_UMSERIAL=space(5)
      .w_AM_INVIO=space(1)
      .w_AM_EMAIL=space(254)
      .w_UMFLGDEF=space(1)
      .w_UMFLSYNC=space(1)
      .w_UMSYNCTO=0
      .w_EDITMAIL=.f.
      .w_AMSHARED=space(1)
      .w_AMAUTSRV=space(1)
      .w_AMCCNEML=space(254)
      .w_AMCC_EML=space(254)
      .w_AM_FIRMA=space(0)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        .w_ISALT = isalt()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(3,4,.f.)
        .w_RETURN = .T.
        .DoRTCalc(6,8,.f.)
        .w_UMFLGPEC = 'N'
        .w_UMSERIAL = ' '
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_UMSERIAL))
         .link_2_4('Full')
        endif
        .DoRTCalc(11,11,.f.)
        .w_AM_EMAIL = STRTRAN( .w_AM_EMAIL, 'infinity:', '')
        .w_UMFLGDEF = iif(empty(.w_UMCODAZI) and .w_UMFLGPEC<>'N', 'S', 'N')
        .w_UMFLSYNC = IIF(.w_UMDIALOG='S' OR .w_AM_INVIO<>'O', 'N', IIF(EMPTY(.w_UMFLSYNC),'N',.w_UMFLSYNC ) )
        .w_UMSYNCTO = ICASE(.w_AM_INVIO<>'O' OR .w_UMFLSYNC<>'S', 0, EMPTY(.w_UMSYNCTO), 30, .w_UMSYNCTO)
        .w_EDITMAIL = Empty(.w_UMSERIAL)
      endif
    endwith
    cp_BlankRecExtFlds(this,'UTEMAILS')
    this.DoRTCalc(17,21,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oUMFLSYNC_2_8.enabled = i_bVal
      .Page1.oPag.oUMSYNCTO_2_9.enabled = i_bVal
      .Page1.oPag.oBtn_2_17.enabled = i_bVal
      .Page1.oPag.oBtn_2_18.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'UTEMAILS',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.UTEMAILS_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UMCODUTE,"UMCODUTE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_UMCODAZI C(5);
      ,t_UMFLGPEC N(3);
      ,t_AM_INVIO N(3);
      ,t_AM_EMAIL C(254);
      ,t_UMFLSYNC N(3);
      ,t_UMSYNCTO N(3);
      ,t_AMSHARED N(3);
      ,t_AMAUTSRV N(3);
      ,UMCODAZI C(5);
      ,UMSERIAL C(5);
      ,t_CPROWORD N(5);
      ,t_UMSERIAL C(5);
      ,t_UMFLGDEF C(1);
      ,t_EDITMAIL L(1);
      ,t_AMCCNEML C(254);
      ,t_AMCC_EML C(254);
      ,t_AM_FIRMA M(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsut_mmubodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oUMCODAZI_2_1.controlsource=this.cTrsName+'.t_UMCODAZI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oUMFLGPEC_2_3.controlsource=this.cTrsName+'.t_UMFLGPEC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oAM_INVIO_2_5.controlsource=this.cTrsName+'.t_AM_INVIO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oAM_EMAIL_2_6.controlsource=this.cTrsName+'.t_AM_EMAIL'
    this.oPgFRm.Page1.oPag.oUMFLSYNC_2_8.controlsource=this.cTrsName+'.t_UMFLSYNC'
    this.oPgFRm.Page1.oPag.oUMSYNCTO_2_9.controlsource=this.cTrsName+'.t_UMSYNCTO'
    this.oPgFRm.Page1.oPag.oAMSHARED_2_11.controlsource=this.cTrsName+'.t_AMSHARED'
    this.oPgFRm.Page1.oPag.oAMAUTSRV_2_12.controlsource=this.cTrsName+'.t_AMAUTSRV'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(80)
    this.AddVLine(174)
    this.AddVLine(309)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUMCODAZI_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.UTEMAILS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.UTEMAILS_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.UTEMAILS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.UTEMAILS_IDX,2])
      *
      * insert into UTEMAILS
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'UTEMAILS')
        i_extval=cp_InsertValODBCExtFlds(this,'UTEMAILS')
        i_cFldBody=" "+;
                  "(UMCODUTE,UMCODAZI,CPROWORD,UMFLGPEC,UMSERIAL"+;
                  ",UMFLGDEF,UMFLSYNC,UMSYNCTO,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_UMCODUTE)+","+cp_ToStrODBC(this.w_UMCODAZI)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_UMFLGPEC)+","+cp_ToStrODBCNull(this.w_UMSERIAL)+;
             ","+cp_ToStrODBC(this.w_UMFLGDEF)+","+cp_ToStrODBC(this.w_UMFLSYNC)+","+cp_ToStrODBC(this.w_UMSYNCTO)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'UTEMAILS')
        i_extval=cp_InsertValVFPExtFlds(this,'UTEMAILS')
        cp_CheckDeletedKey(i_cTable,0,'UMCODUTE',this.w_UMCODUTE,'UMCODAZI',this.w_UMCODAZI,'UMSERIAL',this.w_UMSERIAL)
        INSERT INTO (i_cTable) (;
                   UMCODUTE;
                  ,UMCODAZI;
                  ,CPROWORD;
                  ,UMFLGPEC;
                  ,UMSERIAL;
                  ,UMFLGDEF;
                  ,UMFLSYNC;
                  ,UMSYNCTO;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_UMCODUTE;
                  ,this.w_UMCODAZI;
                  ,this.w_CPROWORD;
                  ,this.w_UMFLGPEC;
                  ,this.w_UMSERIAL;
                  ,this.w_UMFLGDEF;
                  ,this.w_UMFLSYNC;
                  ,this.w_UMSYNCTO;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.UTEMAILS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.UTEMAILS_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not Empty(t_UMSERIAL)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'UTEMAILS')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and UMCODAZI="+cp_ToStrODBC(&i_TN.->UMCODAZI)+;
                 " and UMSERIAL="+cp_ToStrODBC(&i_TN.->UMSERIAL)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'UTEMAILS')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and UMCODAZI=&i_TN.->UMCODAZI;
                      and UMSERIAL=&i_TN.->UMSERIAL;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not Empty(t_UMSERIAL)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and UMCODAZI="+cp_ToStrODBC(&i_TN.->UMCODAZI)+;
                            " and UMSERIAL="+cp_ToStrODBC(&i_TN.->UMSERIAL)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and UMCODAZI=&i_TN.->UMCODAZI;
                            and UMSERIAL=&i_TN.->UMSERIAL;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace UMCODAZI with this.w_UMCODAZI
              replace UMSERIAL with this.w_UMSERIAL
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update UTEMAILS
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'UTEMAILS')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",UMFLGPEC="+cp_ToStrODBC(this.w_UMFLGPEC)+;
                     ",UMFLGDEF="+cp_ToStrODBC(this.w_UMFLGDEF)+;
                     ",UMFLSYNC="+cp_ToStrODBC(this.w_UMFLSYNC)+;
                     ",UMSYNCTO="+cp_ToStrODBC(this.w_UMSYNCTO)+;
                     ",UMCODAZI="+cp_ToStrODBC(this.w_UMCODAZI)+;
                     ",UMSERIAL="+cp_ToStrODBC(this.w_UMSERIAL)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and UMCODAZI="+cp_ToStrODBC(UMCODAZI)+;
                             " and UMSERIAL="+cp_ToStrODBC(UMSERIAL)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'UTEMAILS')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,UMFLGPEC=this.w_UMFLGPEC;
                     ,UMFLGDEF=this.w_UMFLGDEF;
                     ,UMFLSYNC=this.w_UMFLSYNC;
                     ,UMSYNCTO=this.w_UMSYNCTO;
                     ,UMCODAZI=this.w_UMCODAZI;
                     ,UMSERIAL=this.w_UMSERIAL;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and UMCODAZI=&i_TN.->UMCODAZI;
                                      and UMSERIAL=&i_TN.->UMSERIAL;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.UTEMAILS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.UTEMAILS_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not Empty(t_UMSERIAL)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete UTEMAILS
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and UMCODAZI="+cp_ToStrODBC(&i_TN.->UMCODAZI)+;
                            " and UMSERIAL="+cp_ToStrODBC(&i_TN.->UMSERIAL)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and UMCODAZI=&i_TN.->UMCODAZI;
                              and UMSERIAL=&i_TN.->UMSERIAL;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not Empty(t_UMSERIAL)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.UTEMAILS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.UTEMAILS_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(1,9,.t.)
        if .o_UMFLGPEC<>.w_UMFLGPEC
          .w_UMSERIAL = ' '
          .link_2_4('Full')
        endif
        .DoRTCalc(11,11,.t.)
        if .o_AM_EMAIL<>.w_AM_EMAIL
          .w_AM_EMAIL = STRTRAN( .w_AM_EMAIL, 'infinity:', '')
        endif
        .DoRTCalc(13,13,.t.)
        if .o_UMDIALOG<>.w_UMDIALOG.or. .o_AM_INVIO<>.w_AM_INVIO
          .w_UMFLSYNC = IIF(.w_UMDIALOG='S' OR .w_AM_INVIO<>'O', 'N', IIF(EMPTY(.w_UMFLSYNC),'N',.w_UMFLSYNC ) )
        endif
        if .o_AM_INVIO<>.w_AM_INVIO.or. .o_UMFLSYNC<>.w_UMFLSYNC
          .w_UMSYNCTO = ICASE(.w_AM_INVIO<>'O' OR .w_UMFLSYNC<>'S', 0, EMPTY(.w_UMSYNCTO), 30, .w_UMSYNCTO)
        endif
          .w_EDITMAIL = Empty(.w_UMSERIAL)
        if .o_UMSERIAL<>.w_UMSERIAL
          .Calculate_POZCCMUPZW()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(17,21,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_CPROWORD with this.w_CPROWORD
      replace t_UMSERIAL with this.w_UMSERIAL
      replace t_UMFLGDEF with this.w_UMFLGDEF
      replace t_EDITMAIL with this.w_EDITMAIL
      replace t_AMCCNEML with this.w_AMCCNEML
      replace t_AMCC_EML with this.w_AMCC_EML
      replace t_AM_FIRMA with this.w_AM_FIRMA
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_POZCCMUPZW()
    with this
          * --- SetCallerVars
          .w_SETCALLER = .SetCallerVars()
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oUMFLSYNC_2_8.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oUMFLSYNC_2_8.mCond()
    this.oPgFrm.Page1.oPag.oUMSYNCTO_2_9.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oUMSYNCTO_2_9.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_3_1.visible=!this.oPgFrm.Page1.oPag.oStr_3_1.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oUMFLSYNC_2_8.visible=!this.oPgFrm.Page1.oPag.oUMFLSYNC_2_8.mHide()
    this.oPgFrm.Page1.oPag.oUMSYNCTO_2_9.visible=!this.oPgFrm.Page1.oPag.oUMSYNCTO_2_9.mHide()
    this.oPgFrm.Page1.oPag.oAMSHARED_2_11.visible=!this.oPgFrm.Page1.oPag.oAMSHARED_2_11.mHide()
    this.oPgFrm.Page1.oPag.oAMAUTSRV_2_12.visible=!this.oPgFrm.Page1.oPag.oAMAUTSRV_2_12.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_17.visible=!this.oPgFrm.Page1.oPag.oBtn_2_17.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
        if lower(cEvent)==lower("Init Row")
          .Calculate_POZCCMUPZW()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=UMSERIAL
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ACC_MAIL_IDX,3]
    i_lTable = "ACC_MAIL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ACC_MAIL_IDX,2], .t., this.ACC_MAIL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ACC_MAIL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UMSERIAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UMSERIAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AMSERIAL,AM_INVIO,AM_EMAIL,AMAUTSRV,AMCC_EML,AMCCNEML,AM_FIRMA,AMSHARED";
                   +" from "+i_cTable+" "+i_lTable+" where AMSERIAL="+cp_ToStrODBC(this.w_UMSERIAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AMSERIAL',this.w_UMSERIAL)
            select AMSERIAL,AM_INVIO,AM_EMAIL,AMAUTSRV,AMCC_EML,AMCCNEML,AM_FIRMA,AMSHARED;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UMSERIAL = NVL(_Link_.AMSERIAL,space(5))
      this.w_AM_INVIO = NVL(_Link_.AM_INVIO,space(1))
      this.w_AM_EMAIL = NVL(_Link_.AM_EMAIL,space(254))
      this.w_AMAUTSRV = NVL(_Link_.AMAUTSRV,space(1))
      this.w_AMCC_EML = NVL(_Link_.AMCC_EML,space(254))
      this.w_AMCCNEML = NVL(_Link_.AMCCNEML,space(254))
      this.w_AM_FIRMA = NVL(_Link_.AM_FIRMA,space(0))
      this.w_AMSHARED = NVL(_Link_.AMSHARED,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_UMSERIAL = space(5)
      endif
      this.w_AM_INVIO = space(1)
      this.w_AM_EMAIL = space(254)
      this.w_AMAUTSRV = space(1)
      this.w_AMCC_EML = space(254)
      this.w_AMCCNEML = space(254)
      this.w_AM_FIRMA = space(0)
      this.w_AMSHARED = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ACC_MAIL_IDX,2])+'\'+cp_ToStr(_Link_.AMSERIAL,1)
      cp_ShowWarn(i_cKey,this.ACC_MAIL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UMSERIAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 8 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ACC_MAIL_IDX,3] and i_nFlds+8<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ACC_MAIL_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_4.AMSERIAL as AMSERIAL204"+ ",link_2_4.AM_INVIO as AM_INVIO204"+ ",link_2_4.AM_EMAIL as AM_EMAIL204"+ ",link_2_4.AMAUTSRV as AMAUTSRV204"+ ",link_2_4.AMCC_EML as AMCC_EML204"+ ",link_2_4.AMCCNEML as AMCCNEML204"+ ",link_2_4.AM_FIRMA as AM_FIRMA204"+ ",link_2_4.AMSHARED as AMSHARED204"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_4 on UTEMAILS.UMSERIAL=link_2_4.AMSERIAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_4"
          i_cKey=i_cKey+'+" and UTEMAILS.UMSERIAL=link_2_4.AMSERIAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oUMFLSYNC_2_8.RadioValue()==this.w_UMFLSYNC)
      this.oPgFrm.Page1.oPag.oUMFLSYNC_2_8.SetRadio()
      replace t_UMFLSYNC with this.oPgFrm.Page1.oPag.oUMFLSYNC_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oUMSYNCTO_2_9.value==this.w_UMSYNCTO)
      this.oPgFrm.Page1.oPag.oUMSYNCTO_2_9.value=this.w_UMSYNCTO
      replace t_UMSYNCTO with this.oPgFrm.Page1.oPag.oUMSYNCTO_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oAMSHARED_2_11.RadioValue()==this.w_AMSHARED)
      this.oPgFrm.Page1.oPag.oAMSHARED_2_11.SetRadio()
      replace t_AMSHARED with this.oPgFrm.Page1.oPag.oAMSHARED_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oAMAUTSRV_2_12.RadioValue()==this.w_AMAUTSRV)
      this.oPgFrm.Page1.oPag.oAMAUTSRV_2_12.SetRadio()
      replace t_AMAUTSRV with this.oPgFrm.Page1.oPag.oAMAUTSRV_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUMCODAZI_2_1.value==this.w_UMCODAZI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUMCODAZI_2_1.value=this.w_UMCODAZI
      replace t_UMCODAZI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUMCODAZI_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUMFLGPEC_2_3.RadioValue()==this.w_UMFLGPEC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUMFLGPEC_2_3.SetRadio()
      replace t_UMFLGPEC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUMFLGPEC_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAM_INVIO_2_5.RadioValue()==this.w_AM_INVIO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAM_INVIO_2_5.SetRadio()
      replace t_AM_INVIO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAM_INVIO_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAM_EMAIL_2_6.value==this.w_AM_EMAIL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAM_EMAIL_2_6.value=this.w_AM_EMAIL
      replace t_AM_EMAIL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAM_EMAIL_2_6.value
    endif
    cp_SetControlsValueExtFlds(this,'UTEMAILS')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsut_mmu
      If i_bRes
        GSUT_BMU(This,'S') && controlli finali
        i_bRes = .w_RETURN
      Endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case not(.w_UMFLGPEC<>"S" or Inlist(.w_AM_INVIO,'P','M','O') or empty(.w_AM_INVIO) or .w_AM_INVIO="I" )
          i_bnoChk = .f.
          i_bRes = .f.
          i_cErrorMsg = cp_Translate("Supporto non valido per tipologia PEC")
        case   not(Looktab("AZIENDA","AZCODAZI","AZCODAZI",.w_UMCODAZI)=.w_UMCODAZI) and (not Empty(.w_UMSERIAL))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUMCODAZI_2_1
          i_bRes = .f.
          i_bnoChk = .f.
        case   not(.w_AM_INVIO<>'O' OR .w_UMFLSYNC<>'S' OR .w_UMSYNCTO>0) and (.w_EDITMAIL and .w_UMFLSYNC='S') and (not Empty(.w_UMSERIAL))
          .oNewFocus=.oPgFrm.Page1.oPag.oUMSYNCTO_2_9
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Il tempo d'attesa deve essere maggiore di zero")
      endcase
      if not Empty(.w_UMSERIAL)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_UMDIALOG = this.w_UMDIALOG
    this.o_UMFLGPEC = this.w_UMFLGPEC
    this.o_UMSERIAL = this.w_UMSERIAL
    this.o_AM_INVIO = this.w_AM_INVIO
    this.o_AM_EMAIL = this.w_AM_EMAIL
    this.o_UMFLSYNC = this.w_UMFLSYNC
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not Empty(t_UMSERIAL))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_UMCODAZI=space(5)
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_UMFLGPEC=space(1)
      .w_UMSERIAL=space(5)
      .w_AM_INVIO=space(1)
      .w_AM_EMAIL=space(254)
      .w_UMFLGDEF=space(1)
      .w_UMFLSYNC=space(1)
      .w_UMSYNCTO=0
      .w_EDITMAIL=.f.
      .w_AMSHARED=space(1)
      .w_AMAUTSRV=space(1)
      .w_AMCCNEML=space(254)
      .w_AMCC_EML=space(254)
      .w_AM_FIRMA=space(0)
      .DoRTCalc(1,8,.f.)
        .w_UMFLGPEC = 'N'
        .w_UMSERIAL = ' '
      .DoRTCalc(10,10,.f.)
      if not(empty(.w_UMSERIAL))
        .link_2_4('Full')
      endif
      .DoRTCalc(11,11,.f.)
        .w_AM_EMAIL = STRTRAN( .w_AM_EMAIL, 'infinity:', '')
        .w_UMFLGDEF = iif(empty(.w_UMCODAZI) and .w_UMFLGPEC<>'N', 'S', 'N')
        .w_UMFLSYNC = IIF(.w_UMDIALOG='S' OR .w_AM_INVIO<>'O', 'N', IIF(EMPTY(.w_UMFLSYNC),'N',.w_UMFLSYNC ) )
        .w_UMSYNCTO = ICASE(.w_AM_INVIO<>'O' OR .w_UMFLSYNC<>'S', 0, EMPTY(.w_UMSYNCTO), 30, .w_UMSYNCTO)
        .w_EDITMAIL = Empty(.w_UMSERIAL)
    endwith
    this.DoRTCalc(17,21,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_UMCODAZI = t_UMCODAZI
    this.w_CPROWORD = t_CPROWORD
    this.w_UMFLGPEC = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUMFLGPEC_2_3.RadioValue(.t.)
    this.w_UMSERIAL = t_UMSERIAL
    this.w_AM_INVIO = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAM_INVIO_2_5.RadioValue(.t.)
    this.w_AM_EMAIL = t_AM_EMAIL
    this.w_UMFLGDEF = t_UMFLGDEF
    this.w_UMFLSYNC = this.oPgFrm.Page1.oPag.oUMFLSYNC_2_8.RadioValue(.t.)
    this.w_UMSYNCTO = t_UMSYNCTO
    this.w_EDITMAIL = t_EDITMAIL
    this.w_AMSHARED = this.oPgFrm.Page1.oPag.oAMSHARED_2_11.RadioValue(.t.)
    this.w_AMAUTSRV = this.oPgFrm.Page1.oPag.oAMAUTSRV_2_12.RadioValue(.t.)
    this.w_AMCCNEML = t_AMCCNEML
    this.w_AMCC_EML = t_AMCC_EML
    this.w_AM_FIRMA = t_AM_FIRMA
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_UMCODAZI with this.w_UMCODAZI
    replace t_CPROWORD with this.w_CPROWORD
    replace t_UMFLGPEC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUMFLGPEC_2_3.ToRadio()
    replace t_UMSERIAL with this.w_UMSERIAL
    replace t_AM_INVIO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAM_INVIO_2_5.ToRadio()
    replace t_AM_EMAIL with this.w_AM_EMAIL
    replace t_UMFLGDEF with this.w_UMFLGDEF
    replace t_UMFLSYNC with this.oPgFrm.Page1.oPag.oUMFLSYNC_2_8.ToRadio()
    replace t_UMSYNCTO with this.w_UMSYNCTO
    replace t_EDITMAIL with this.w_EDITMAIL
    replace t_AMSHARED with this.oPgFrm.Page1.oPag.oAMSHARED_2_11.ToRadio()
    replace t_AMAUTSRV with this.oPgFrm.Page1.oPag.oAMAUTSRV_2_12.ToRadio()
    replace t_AMCCNEML with this.w_AMCCNEML
    replace t_AMCC_EML with this.w_AMCC_EML
    replace t_AM_FIRMA with this.w_AM_FIRMA
    if i_srv='A'
      replace UMCODAZI with this.w_UMCODAZI
      replace UMSERIAL with this.w_UMSERIAL
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsut_mmuPag1 as StdContainer
  Width  = 620
  height = 198
  stdWidth  = 620
  stdheight = 198
  resizeXpos=425
  resizeYpos=124
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=14, top=1, width=595,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=4,Field1="UMCODAZI",Label1="Azienda",Field2="UMFLGPEC",Label2="Tipologia",Field3="AM_INVIO",Label3="Supporto",Field4="AM_EMAIL",Label4="Indirizzo mail mittente",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 118351482

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=6,top=20,;
    width=543+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*7*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=7,top=21,width=542+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*7*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oUMFLSYNC_2_8.Refresh()
      this.Parent.oUMSYNCTO_2_9.Refresh()
      this.Parent.oAMSHARED_2_11.Refresh()
      this.Parent.oAMAUTSRV_2_12.Refresh()
      * --- Area Manuale = Set Current Row
      * --- gsut_mmu
      this.Parent.oContained.SetCallerVars()
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oUMFLSYNC_2_8 as StdTrsCheck with uid="STWMKXHFPW",rtrep=.t.,;
    cFormVar="w_UMFLSYNC",  caption="Invio posta immediato",;
    ToolTipText = "Se attivo l'invio della posta di Microsoft Outlook viene effettuato immediatamente",;
    HelpContextID = 45576073,;
    Left=297, Top=165,;
    cTotal="", cQueryName = "UMFLSYNC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oUMFLSYNC_2_8.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..UMFLSYNC,&i_cF..t_UMFLSYNC),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oUMFLSYNC_2_8.GetRadio()
    this.Parent.oContained.w_UMFLSYNC = this.RadioValue()
    return .t.
  endfunc

  func oUMFLSYNC_2_8.ToRadio()
    this.Parent.oContained.w_UMFLSYNC=trim(this.Parent.oContained.w_UMFLSYNC)
    return(;
      iif(this.Parent.oContained.w_UMFLSYNC=='S',1,;
      0))
  endfunc

  func oUMFLSYNC_2_8.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oUMFLSYNC_2_8.mCond()
    with this.Parent.oContained
      return (.w_EDITMAIL and .w_UMDIALOG='A')
    endwith
  endfunc

  func oUMFLSYNC_2_8.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AM_INVIO<>'O' or not .w_ISALT)
    endwith
    endif
  endfunc

  add object oUMSYNCTO_2_9 as StdTrsField with uid="JSCZWFVTKY",rtseq=15,rtrep=.t.,;
    cFormVar="w_UMSYNCTO",value=0,;
    ToolTipText = "Numero di secondi di attesa per verifica invio E-mail",;
    HelpContextID = 209010581,;
    cTotal="", bFixedPos=.t., cQueryName = "UMSYNCTO",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Il tempo d'attesa deve essere maggiore di zero",;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=559, Top=167

  func oUMSYNCTO_2_9.mCond()
    with this.Parent.oContained
      return (.w_EDITMAIL and .w_UMFLSYNC='S')
    endwith
  endfunc

  func oUMSYNCTO_2_9.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AM_INVIO<>'O' or not .w_ISALT)
    endwith
    endif
  endfunc

  func oUMSYNCTO_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_AM_INVIO<>'O' OR .w_UMFLSYNC<>'S' OR .w_UMSYNCTO>0)
    endwith
    return bRes
  endfunc

  add object oAMSHARED_2_11 as StdTrsCheck with uid="OGGICOLGDH",rtrep=.t.,;
    cFormVar="w_AMSHARED", enabled=.f., caption="Account condiviso",;
    HelpContextID = 177487434,;
    Left=16, Top=165,;
    cTotal="", cQueryName = "AMSHARED",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oAMSHARED_2_11.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..AMSHARED,&i_cF..t_AMSHARED),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oAMSHARED_2_11.GetRadio()
    this.Parent.oContained.w_AMSHARED = this.RadioValue()
    return .t.
  endfunc

  func oAMSHARED_2_11.ToRadio()
    this.Parent.oContained.w_AMSHARED=trim(this.Parent.oContained.w_AMSHARED)
    return(;
      iif(this.Parent.oContained.w_AMSHARED=='S',1,;
      0))
  endfunc

  func oAMSHARED_2_11.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oAMSHARED_2_11.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_AM_INVIO))
    endwith
    endif
  endfunc

  add object oAMAUTSRV_2_12 as StdTrsCheck with uid="STRCKYKJQX",rtrep=.t.,;
    cFormVar="w_AMAUTSRV", enabled=.f., caption="Autenticazione utente",;
    ToolTipText = "Se attivo abilita autenticazione utente/password durante invio mail",;
    HelpContextID = 214965852,;
    Left=146, Top=165,;
    cTotal="", cQueryName = "AMAUTSRV",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oAMAUTSRV_2_12.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..AMAUTSRV,&i_cF..t_AMAUTSRV),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oAMAUTSRV_2_12.GetRadio()
    this.Parent.oContained.w_AMAUTSRV = this.RadioValue()
    return .t.
  endfunc

  func oAMAUTSRV_2_12.ToRadio()
    this.Parent.oContained.w_AMAUTSRV=trim(this.Parent.oContained.w_AMAUTSRV)
    return(;
      iif(this.Parent.oContained.w_AMAUTSRV=='S',1,;
      0))
  endfunc

  func oAMAUTSRV_2_12.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oAMAUTSRV_2_12.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AM_INVIO<>'S' AND .w_AM_INVIO<>'P')
    endwith
    endif
  endfunc

  add object oBtn_2_17 as StdButton with uid="WEULJWSHPP",width=48,height=45, tabstop=.f.,;
   left=566, top=69,;
    CpPicture="BMP\LOG_PWD.BMP", caption="", nPag=2;
    , ToolTipText = "Modifica password";
    , HelpContextID = 84595930;
    , caption='\<Mod.pw';
  , bGlobalFont=.t.

    proc oBtn_2_17.Click()
      with this.Parent.oContained
        GSUT_BMU(this.Parent.oContained,"P")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_17.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AMSHARED<>"N" or .w_AMAUTSRV<>"S" or .cFunction<>"Edit" or empty(.w_UMSERIAL))
    endwith
   endif
  endfunc

  add object oBtn_2_18 as StdButton with uid="ALOGMYLFHF",width=48,height=45,;
   left=566, top=22,;
    CpPicture="bmp\fxmail.ico", caption="", nPag=2;
    , ToolTipText = "Associa Account email";
    , HelpContextID = 167531526;
    , caption='\<Account';
  , bGlobalFont=.t.

    proc oBtn_2_18.Click()
      with this.Parent.oContained
        GSUT_BAM(this.Parent.oContained,"")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oStr_3_1 as StdString with uid="WYJNKERZEG",Visible=.t., Left=445, Top=168,;
    Alignment=1, Width=114, Height=18,;
    Caption="Attesa invio E-mail:"  ;
  , bGlobalFont=.t.

  func oStr_3_1.mHide()
    with this.Parent.oContained
      return (.w_AM_INVIO<>'O' or not .w_ISALT)
    endwith
  endfunc
enddefine

* --- Defining Body row
define class tgsut_mmuBodyRow as CPBodyRowCnt
  Width=533
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oUMCODAZI_2_1 as StdTrsField with uid="YMMMKRQQLE",rtseq=7,rtrep=.t.,;
    cFormVar="w_UMCODAZI",value=space(5),isprimarykey=.t.,;
    HelpContextID = 104185969,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=62, Left=-2, Top=0, InputMask=replicate('X',5), bHasZoom = .t. 

  func oUMCODAZI_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (Looktab("AZIENDA","AZCODAZI","AZCODAZI",.w_UMCODAZI)=.w_UMCODAZI)
    endwith
    return bRes
  endfunc

  proc oUMCODAZI_2_1.mZoom
    vx_exec("query\gsut_mmu.vzm",this.Parent.oContained)
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oUMFLGPEC_2_3 as StdTrsCombo with uid="VKUQKKCIGG",rtrep=.t.,;
    cFormVar="w_UMFLGPEC", RowSource=""+"Mail Standard,"+"PEC" , ;
    ToolTipText = "Account di invio di posta elettronica certificata",;
    HelpContextID = 150433673,;
    Height=22, Width=90, Left=65, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2  , tabstop=.f.;
, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oUMFLGPEC_2_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..UMFLGPEC,&i_cF..t_UMFLGPEC),this.value)
    return(iif(xVal =1,'N',;
    iif(xVal =2,'S',;
    space(1))))
  endfunc
  func oUMFLGPEC_2_3.GetRadio()
    this.Parent.oContained.w_UMFLGPEC = this.RadioValue()
    return .t.
  endfunc

  func oUMFLGPEC_2_3.ToRadio()
    this.Parent.oContained.w_UMFLGPEC=trim(this.Parent.oContained.w_UMFLGPEC)
    return(;
      iif(this.Parent.oContained.w_UMFLGPEC=='N',1,;
      iif(this.Parent.oContained.w_UMFLGPEC=='S',2,;
      0)))
  endfunc

  func oUMFLGPEC_2_3.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oAM_INVIO_2_5 as StdTrsCombo with uid="WHFSTPPBBP",rtrep=.t.,;
    cFormVar="w_AM_INVIO", RowSource=""+"Interfaccia MAPI,"+"Microsoft Outlook,"+"Servizio SMTP,"+"Servizio SMTP con SSL,"+"Infinity" , enabled=.f.,;
    ToolTipText = "Supporto per l'invio mail (interfaccia MAPI o servizio SMTP o servizio SMTP con SSL o Microsoft Outlook)",;
    HelpContextID = 10092971,;
    Height=22, Width=132, Left=158, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oAM_INVIO_2_5.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..AM_INVIO,&i_cF..t_AM_INVIO),this.value)
    return(iif(xVal =1,'M',;
    iif(xVal =2,'O',;
    iif(xVal =3,'S',;
    iif(xVal =4,'P',;
    iif(xVal =5,'I',;
    space(1)))))))
  endfunc
  func oAM_INVIO_2_5.GetRadio()
    this.Parent.oContained.w_AM_INVIO = this.RadioValue()
    return .t.
  endfunc

  func oAM_INVIO_2_5.ToRadio()
    this.Parent.oContained.w_AM_INVIO=trim(this.Parent.oContained.w_AM_INVIO)
    return(;
      iif(this.Parent.oContained.w_AM_INVIO=='M',1,;
      iif(this.Parent.oContained.w_AM_INVIO=='O',2,;
      iif(this.Parent.oContained.w_AM_INVIO=='S',3,;
      iif(this.Parent.oContained.w_AM_INVIO=='P',4,;
      iif(this.Parent.oContained.w_AM_INVIO=='I',5,;
      0))))))
  endfunc

  func oAM_INVIO_2_5.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oAM_EMAIL_2_6 as StdTrsField with uid="PIJFCLNWIA",rtseq=12,rtrep=.t.,;
    cFormVar="w_AM_EMAIL",value=space(254),enabled=.f.,;
    HelpContextID = 95289774,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=236, Left=292, Top=0, InputMask=replicate('X',254)
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oUMCODAZI_2_1.When()
    return(.t.)
  proc oUMCODAZI_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oUMCODAZI_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=6
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_mmu','UTEMAILS','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".UMCODUTE=UTEMAILS.UMCODUTE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
