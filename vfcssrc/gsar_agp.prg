* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_agp                                                        *
*              Categorie provvigioni                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_6]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-11                                                      *
* Last revis.: 2012-09-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_agp"))

* --- Class definition
define class tgsar_agp as StdForm
  Top    = 45
  Left   = 42

  * --- Standard Properties
  Width  = 382
  Height = 215+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-09-28"
  HelpContextID=225872023
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Constant Properties
  GRUPRO_IDX = 0
  cFile = "GRUPRO"
  cKeySelect = "GPCODICE"
  cKeyWhere  = "GPCODICE=this.w_GPCODICE"
  cKeyWhereODBC = '"GPCODICE="+cp_ToStrODBC(this.w_GPCODICE)';

  cKeyWhereODBCqualified = '"GRUPRO.GPCODICE="+cp_ToStrODBC(this.w_GPCODICE)';

  cPrg = "gsar_agp"
  cComment = "Categorie provvigioni"
  icon = "anag.ico"
  cAutoZoom = 'GSAR0AGP'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_GPCODICE = space(5)
  w_GPDESCRI = space(35)
  w_GPTIPPRO = space(1)
  w_GPPERDFA = 0
  w_GPPERDSC = 0
  w_GPPERDIN = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'GRUPRO','gsar_agp')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_agpPag1","gsar_agp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Categorie")
      .Pages(1).HelpContextID = 55784671
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oGPCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='GRUPRO'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.GRUPRO_IDX,5],7]
    this.nPostItConn=i_TableProp[this.GRUPRO_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_GPCODICE = NVL(GPCODICE,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from GRUPRO where GPCODICE=KeySet.GPCODICE
    *
    i_nConn = i_TableProp[this.GRUPRO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('GRUPRO')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "GRUPRO.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' GRUPRO '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'GPCODICE',this.w_GPCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_GPCODICE = NVL(GPCODICE,space(5))
        .w_GPDESCRI = NVL(GPDESCRI,space(35))
        .w_GPTIPPRO = NVL(GPTIPPRO,space(1))
        .w_GPPERDFA = NVL(GPPERDFA,0)
        .w_GPPERDSC = NVL(GPPERDSC,0)
        .w_GPPERDIN = NVL(GPPERDIN,0)
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
        cp_LoadRecExtFlds(this,'GRUPRO')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_GPCODICE = space(5)
      .w_GPDESCRI = space(35)
      .w_GPTIPPRO = space(1)
      .w_GPPERDFA = 0
      .w_GPPERDSC = 0
      .w_GPPERDIN = 0
      if .cFunction<>"Filter"
          .DoRTCalc(1,2,.f.)
        .w_GPTIPPRO = 'C'
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'GRUPRO')
    this.DoRTCalc(4,6,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oGPCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oGPDESCRI_1_2.enabled = i_bVal
      .Page1.oPag.oGPTIPPRO_1_5.enabled = i_bVal
      .Page1.oPag.oGPPERDFA_1_7.enabled = i_bVal
      .Page1.oPag.oGPPERDSC_1_8.enabled = i_bVal
      .Page1.oPag.oGPPERDIN_1_9.enabled = i_bVal
      .Page1.oPag.oObj_1_15.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oGPCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oGPCODICE_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'GRUPRO',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.GRUPRO_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPCODICE,"GPCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPDESCRI,"GPDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPTIPPRO,"GPTIPPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPPERDFA,"GPPERDFA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPPERDSC,"GPPERDSC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPPERDIN,"GPPERDIN",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.GRUPRO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2])
    i_lTable = "GRUPRO"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.GRUPRO_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSVE_SGP with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.GRUPRO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.GRUPRO_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into GRUPRO
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'GRUPRO')
        i_extval=cp_InsertValODBCExtFlds(this,'GRUPRO')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(GPCODICE,GPDESCRI,GPTIPPRO,GPPERDFA,GPPERDSC"+;
                  ",GPPERDIN "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_GPCODICE)+;
                  ","+cp_ToStrODBC(this.w_GPDESCRI)+;
                  ","+cp_ToStrODBC(this.w_GPTIPPRO)+;
                  ","+cp_ToStrODBC(this.w_GPPERDFA)+;
                  ","+cp_ToStrODBC(this.w_GPPERDSC)+;
                  ","+cp_ToStrODBC(this.w_GPPERDIN)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'GRUPRO')
        i_extval=cp_InsertValVFPExtFlds(this,'GRUPRO')
        cp_CheckDeletedKey(i_cTable,0,'GPCODICE',this.w_GPCODICE)
        INSERT INTO (i_cTable);
              (GPCODICE,GPDESCRI,GPTIPPRO,GPPERDFA,GPPERDSC,GPPERDIN  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_GPCODICE;
                  ,this.w_GPDESCRI;
                  ,this.w_GPTIPPRO;
                  ,this.w_GPPERDFA;
                  ,this.w_GPPERDSC;
                  ,this.w_GPPERDIN;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.GRUPRO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.GRUPRO_IDX,i_nConn)
      *
      * update GRUPRO
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'GRUPRO')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " GPDESCRI="+cp_ToStrODBC(this.w_GPDESCRI)+;
             ",GPTIPPRO="+cp_ToStrODBC(this.w_GPTIPPRO)+;
             ",GPPERDFA="+cp_ToStrODBC(this.w_GPPERDFA)+;
             ",GPPERDSC="+cp_ToStrODBC(this.w_GPPERDSC)+;
             ",GPPERDIN="+cp_ToStrODBC(this.w_GPPERDIN)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'GRUPRO')
        i_cWhere = cp_PKFox(i_cTable  ,'GPCODICE',this.w_GPCODICE  )
        UPDATE (i_cTable) SET;
              GPDESCRI=this.w_GPDESCRI;
             ,GPTIPPRO=this.w_GPTIPPRO;
             ,GPPERDFA=this.w_GPPERDFA;
             ,GPPERDSC=this.w_GPPERDSC;
             ,GPPERDIN=this.w_GPPERDIN;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.GRUPRO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.GRUPRO_IDX,i_nConn)
      *
      * delete GRUPRO
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'GPCODICE',this.w_GPCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.GRUPRO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,6,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oGPPERDFA_1_7.enabled = this.oPgFrm.Page1.oPag.oGPPERDFA_1_7.mCond()
    this.oPgFrm.Page1.oPag.oGPPERDSC_1_8.enabled = this.oPgFrm.Page1.oPag.oGPPERDSC_1_8.mCond()
    this.oPgFrm.Page1.oPag.oGPPERDIN_1_9.enabled = this.oPgFrm.Page1.oPag.oGPPERDIN_1_9.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oGPPERDFA_1_7.visible=!this.oPgFrm.Page1.oPag.oGPPERDFA_1_7.mHide()
    this.oPgFrm.Page1.oPag.oGPPERDSC_1_8.visible=!this.oPgFrm.Page1.oPag.oGPPERDSC_1_8.mHide()
    this.oPgFrm.Page1.oPag.oGPPERDIN_1_9.visible=!this.oPgFrm.Page1.oPag.oGPPERDIN_1_9.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_10.visible=!this.oPgFrm.Page1.oPag.oStr_1_10.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_11.visible=!this.oPgFrm.Page1.oPag.oStr_1_11.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_12.visible=!this.oPgFrm.Page1.oPag.oStr_1_12.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_13.visible=!this.oPgFrm.Page1.oPag.oStr_1_13.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_15.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oGPCODICE_1_1.value==this.w_GPCODICE)
      this.oPgFrm.Page1.oPag.oGPCODICE_1_1.value=this.w_GPCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oGPDESCRI_1_2.value==this.w_GPDESCRI)
      this.oPgFrm.Page1.oPag.oGPDESCRI_1_2.value=this.w_GPDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oGPTIPPRO_1_5.RadioValue()==this.w_GPTIPPRO)
      this.oPgFrm.Page1.oPag.oGPTIPPRO_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGPPERDFA_1_7.value==this.w_GPPERDFA)
      this.oPgFrm.Page1.oPag.oGPPERDFA_1_7.value=this.w_GPPERDFA
    endif
    if not(this.oPgFrm.Page1.oPag.oGPPERDSC_1_8.value==this.w_GPPERDSC)
      this.oPgFrm.Page1.oPag.oGPPERDSC_1_8.value=this.w_GPPERDSC
    endif
    if not(this.oPgFrm.Page1.oPag.oGPPERDIN_1_9.value==this.w_GPPERDIN)
      this.oPgFrm.Page1.oPag.oGPPERDIN_1_9.value=this.w_GPPERDIN
    endif
    cp_SetControlsValueExtFlds(this,'GRUPRO')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_GPCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGPCODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_GPCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsar_agp
      * --- Controlli Finali
      
            if i_bRes=.t.
              if .w_GPPERDFA+.w_GPPERDSC+.w_GPPERDIN<>100 AND .w_GPTIPPRO='G'
                  i_cErrorMsg = Ah_MsgFormat("Importi % maturazione non corretti")
                  i_bnoChk = .f.
                  i_bRes = .f.
              endif
            endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_agpPag1 as StdContainer
  Width  = 378
  height = 215
  stdWidth  = 378
  stdheight = 215
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oGPCODICE_1_1 as StdField with uid="TJCHJUCBHY",rtseq=1,rtrep=.f.,;
    cFormVar = "w_GPCODICE", cQueryName = "GPCODICE",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria provvigioni",;
    HelpContextID = 83227221,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=80, Top=12, InputMask=replicate('X',5)

  add object oGPDESCRI_1_2 as StdField with uid="YZSBJVHYSX",rtseq=2,rtrep=.f.,;
    cFormVar = "w_GPDESCRI", cQueryName = "GPDESCRI",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione categoria provvigioni",;
    HelpContextID = 168813137,;
   bGlobalFont=.t.,;
    Height=21, Width=288, Left=80, Top=37, InputMask=replicate('X',35)


  add object oGPTIPPRO_1_5 as StdCombo with uid="AOTWJQIZDN",rtseq=3,rtrep=.f.,left=80,top=68,width=138,height=21;
    , ToolTipText = "Indica l'archivio in cui � utilizzato";
    , HelpContextID = 221962827;
    , cFormVar="w_GPTIPPRO",RowSource=""+"Categoria clienti,"+"Categoria articoli,"+"Categoria agenti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oGPTIPPRO_1_5.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'A',;
    iif(this.value =3,'G',;
    space(1)))))
  endfunc
  func oGPTIPPRO_1_5.GetRadio()
    this.Parent.oContained.w_GPTIPPRO = this.RadioValue()
    return .t.
  endfunc

  func oGPTIPPRO_1_5.SetRadio()
    this.Parent.oContained.w_GPTIPPRO=trim(this.Parent.oContained.w_GPTIPPRO)
    this.value = ;
      iif(this.Parent.oContained.w_GPTIPPRO=='C',1,;
      iif(this.Parent.oContained.w_GPTIPPRO=='A',2,;
      iif(this.Parent.oContained.w_GPTIPPRO=='G',3,;
      0)))
  endfunc

  add object oGPPERDFA_1_7 as StdField with uid="CKFGMGWACJ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_GPPERDFA", cQueryName = "GPPERDFA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale di ripartizione delle provvigioni per data fattura",;
    HelpContextID = 115400103,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=113, Top=130, cSayPict='"999.99"', cGetPict='"999.99"'

  proc oGPPERDFA_1_7.mDefault
    with this.Parent.oContained
      if empty(.w_GPPERDFA)
        .w_GPPERDFA = 100-(.w_GPPERDSC+.w_GPPERDIN)
      endif
    endwith
  endproc

  func oGPPERDFA_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GPTIPPRO='G')
    endwith
   endif
  endfunc

  func oGPPERDFA_1_7.mHide()
    with this.Parent.oContained
      return (.w_GPTIPPRO<>'G')
    endwith
  endfunc

  add object oGPPERDSC_1_8 as StdField with uid="XWLEZBQKHH",rtseq=5,rtrep=.f.,;
    cFormVar = "w_GPPERDSC", cQueryName = "GPPERDSC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale di ripartizione delle provvigioni per data scadenza",;
    HelpContextID = 153035351,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=113, Top=158, cSayPict='"999.99"', cGetPict='"999.99"'

  proc oGPPERDSC_1_8.mDefault
    with this.Parent.oContained
      if empty(.w_GPPERDSC)
        .w_GPPERDSC = 100-(.w_GPPERDFA+.w_GPPERDIN)
      endif
    endwith
  endproc

  func oGPPERDSC_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GPTIPPRO='G')
    endwith
   endif
  endfunc

  func oGPPERDSC_1_8.mHide()
    with this.Parent.oContained
      return (.w_GPTIPPRO<>'G')
    endwith
  endfunc

  add object oGPPERDIN_1_9 as StdField with uid="AFKTBVSIMH",rtseq=6,rtrep=.f.,;
    cFormVar = "w_GPPERDIN", cQueryName = "GPPERDIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale di ripartizione delle provvigioni per data incasso",;
    HelpContextID = 115400116,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=113, Top=186, cSayPict='"999.99"', cGetPict='"999.99"'

  proc oGPPERDIN_1_9.mDefault
    with this.Parent.oContained
      if empty(.w_GPPERDIN)
        .w_GPPERDIN = 100-(.w_GPPERDSC+.w_GPPERDFA)
      endif
    endwith
  endproc

  func oGPPERDIN_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GPTIPPRO='G')
    endwith
   endif
  endfunc

  func oGPPERDIN_1_9.mHide()
    with this.Parent.oContained
      return (.w_GPTIPPRO<>'G')
    endwith
  endfunc


  add object oObj_1_15 as cp_runprogram with uid="MDJIKRPDOG",left=-4, top=228, width=134,height=36,;
    caption='GSAR_BDS',;
   bGlobalFont=.t.,;
    prg="GSAR_BDS",;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 172166983

  add object oStr_1_3 as StdString with uid="TMUHCSYIXP",Visible=.t., Left=2, Top=12,;
    Alignment=1, Width=76, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_4 as StdString with uid="MITZMXRUVE",Visible=.t., Left=2, Top=37,;
    Alignment=1, Width=76, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="WLCIUGPOIV",Visible=.t., Left=2, Top=68,;
    Alignment=1, Width=76, Height=15,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="HMICQJUXSO",Visible=.t., Left=14, Top=101,;
    Alignment=0, Width=156, Height=15,;
    Caption="% Maturazione"  ;
  , bGlobalFont=.t.

  func oStr_1_10.mHide()
    with this.Parent.oContained
      return (.w_GPTIPPRO<>'G')
    endwith
  endfunc

  add object oStr_1_11 as StdString with uid="YGHBTNRODT",Visible=.t., Left=2, Top=130,;
    Alignment=1, Width=109, Height=15,;
    Caption="a data fattura:"  ;
  , bGlobalFont=.t.

  func oStr_1_11.mHide()
    with this.Parent.oContained
      return (.w_GPTIPPRO<>'G')
    endwith
  endfunc

  add object oStr_1_12 as StdString with uid="XSRAIZURIJ",Visible=.t., Left=2, Top=158,;
    Alignment=1, Width=109, Height=15,;
    Caption="a data scadenza:"  ;
  , bGlobalFont=.t.

  func oStr_1_12.mHide()
    with this.Parent.oContained
      return (.w_GPTIPPRO<>'G')
    endwith
  endfunc

  add object oStr_1_13 as StdString with uid="WAWLFPVIWH",Visible=.t., Left=2, Top=186,;
    Alignment=1, Width=109, Height=15,;
    Caption="a data incasso:"  ;
  , bGlobalFont=.t.

  func oStr_1_13.mHide()
    with this.Parent.oContained
      return (.w_GPTIPPRO<>'G')
    endwith
  endfunc

  add object oBox_1_14 as StdBox with uid="WBZWDIVCNG",left=14, top=121, width=158,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_agp','GRUPRO','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".GPCODICE=GRUPRO.GPCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
