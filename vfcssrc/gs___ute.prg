* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gs___ute                                                        *
*              Selezione azienda                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_106]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-10                                                      *
* Last revis.: 2015-07-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgs___ute",oParentObject))

* --- Class definition
define class tgs___ute as StdForm
  Top    = 82
  Left   = 222

  * --- Standard Properties
  Width  = 658
  Height = 223
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-07-06"
  HelpContextID=72001897
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=29

  * --- Constant Properties
  _IDX = 0
  AZIENDA_IDX = 0
  ESERCIZI_IDX = 0
  cpazi_IDX = 0
  cpusers_IDX = 0
  CPUSERS_IDX = 0
  POL_SIC_IDX = 0
  CONF_INT_IDX = 0
  cPrg = "gs___ute"
  cComment = "Selezione azienda"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_NOMEUTE = space(254)
  w_AHE = space(10)
  o_AHE = space(10)
  w_UTE = 0
  o_UTE = 0
  w_DESUTE = space(20)
  w_pwd = space(20)
  w_GROUPDEF = 0
  o_GROUPDEF = 0
  w_LINDEF = space(3)
  w_Azi = space(5)
  o_Azi = space(5)
  w_CODAZI = space(5)
  w_RAGAZI = space(40)
  w_DATSYS = ctod('  /  /  ')
  w_CODESE = space(4)
  w_INIESE = ctod('  /  /  ')
  w_FINESE = ctod('  /  /  ')
  w_OUTE = 0
  w_CLIENTE = space(40)
  w_WEBTYPE = space(1)
  w_CODUTEMOBY = 0
  w_OKPWD = space(3)
  w_MOBIMODE = .F.
  w_TODO = .F.
  w_NOCODUTE = space(1)
  w_CONTACC = 0
  w_UTESILENT = 0
  w_PWDSILENT = space(20)
  w_NUMBGROUP = 0
  w_VALLIN = 0
  w_OLDAZI = space(5)
  w_CHANGED = .F.
  w_LblAzie = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gs___ute
  * --- Disabilito Scrollbars
  ScrollBars=0
  * --- Centro la finestra
  Autocenter=.t.
  
  *Closable=.f.
  * --- disabilito il men� sulla barra
  ControlBox = .f.
  * ---- disabilito l'ingrandimento della maschera
  MaxButton=.f.
  
  BorderStyle=2
  
  Proc CalcGROUPDEF(w_obj)
  local ComboGROUPDEF
     ComboGROUPDEF=w_obj.GetCtrl("w_GROUPDEF")
     ComboGROUPDEF.popola()
  endproc
  proc cambiogruppo(w_obj)
    local ComboGROUPDEF,i,ComboVALTROV
    ComboVALTROV=.f.
    i=0
    ComboGROUPDEF=w_obj.GetCtrl("w_GROUPDEF")
    for i=alen(ComboGROUPDEF.combovalues) to 1 step -1
     if TYPE('ComboGROUPDEF.combovalues[i]')='N' AND this.w_GROUPDEF==ComboGROUPDEF.combovalues[i]
        ComboVALTROV=.t.
     endif
    endfor
   i_GROUPROLE=iif(ComboVALTROV,this.w_GROUPDEF,0)
   this.w_GROUPDEF=iif(ComboVALTROV,this.w_GROUPDEF,0)
   RELEASE i_aUsrGrps
   cp_FillGroupsArray()
  endproc
  
  * --- Ridimensiona la pagina
  * --- a seguito della pressione dei tasti
  * --- Opzioni...
  * --- ParamTipo=.t. Da espansa a ridotta
  * --- .f. Da ridotta a espansa
  Proc Sizer(ParamTipo)
  * --- Recupero il bottone Opzioni..
  Local BOpt,Bok,BAnnulla
  If ParamTipo
    Bopt=This.GetCtrl(ah_Msgformat('\<Riduci'))
    IF (type ('Bopt')<>'O')
     Bopt=This.GetCtrl(ah_Msgformat('\<Espandi'))
    ENDIF
  Else
   Bopt=This.GetCtrl(ah_Msgformat('\<Espandi'))
    IF (type ('Bopt')<>'O')
     Bopt=This.GetCtrl(ah_Msgformat('\<Riduci'))
    ENDIF
  Endif
  
  Bok=This.GetCtrl(ah_Msgformat('\<OK'))
  BAnnulla=This.GetCtrl(ah_Msgformat('\<Esci'))
  
  If ParamTipo
    This.Height=88
    This.oPgFrm.Height=85
    This.w_TODO=.F.
  Else
    This.Height=223
    This.oPgFrm.Height=223
    This.w_TODO=.T.
  Endif
  If This.bApplyFormDecorator AND i_ThemesManager.GetProp(123)=0
     This.Height=This.Height + This.oCaptionBar.Height
     This.oPgFrm.Height=This.oPgFrm.Height + This.oCaptionBar.Height
  EndIf
  * --- Nascondo / Mostro gli oggetti
  This.mHideControls()
  Endproc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgs___utePag1","gs___ute",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oNOMEUTE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_LblAzie = this.oPgFrm.Pages(1).oPag.LblAzie
    DoDefault()
    proc Destroy()
      this.w_LblAzie = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='ESERCIZI'
    this.cWorkTables[3]='cpazi'
    this.cWorkTables[4]='cpusers'
    this.cWorkTables[5]='CPUSERS'
    this.cWorkTables[6]='POL_SIC'
    this.cWorkTables[7]='CONF_INT'
    return(this.OpenAllTables(7))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gs___ute
    * --- cambio intestazione maschera
    * --- a seconda se ingresso iniziale o
    * --- da men� all'interno della procedura
    This.cComment = iif(g_INIZ , AH_MSGFORMAT("Autenticazione") , AH_MSGFORMAT("Scelta azienda") )
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_NOMEUTE=space(254)
      .w_AHE=space(10)
      .w_UTE=0
      .w_DESUTE=space(20)
      .w_pwd=space(20)
      .w_GROUPDEF=0
      .w_LINDEF=space(3)
      .w_Azi=space(5)
      .w_CODAZI=space(5)
      .w_RAGAZI=space(40)
      .w_DATSYS=ctod("  /  /  ")
      .w_CODESE=space(4)
      .w_INIESE=ctod("  /  /  ")
      .w_FINESE=ctod("  /  /  ")
      .w_OUTE=0
      .w_CLIENTE=space(40)
      .w_WEBTYPE=space(1)
      .w_CODUTEMOBY=0
      .w_OKPWD=space(3)
      .w_MOBIMODE=.f.
      .w_TODO=.f.
      .w_NOCODUTE=space(1)
      .w_CONTACC=0
      .w_UTESILENT=0
      .w_PWDSILENT=space(20)
      .w_NUMBGROUP=0
      .w_VALLIN=0
      .w_OLDAZI=space(5)
      .w_CHANGED=.f.
      .w_UTESILENT=oParentObject.w_UTESILENT
      .w_PWDSILENT=oParentObject.w_PWDSILENT
          .DoRTCalc(1,1,.f.)
        .w_AHE = 'AHE'
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_AHE))
          .link_1_2('Full')
        endif
        .w_UTE = iif(.w_UTESILENT<>0, .w_UTESILENT, g_CODUTE)
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_UTE))
          .link_1_3('Full')
        endif
          .DoRTCalc(4,4,.f.)
        .w_pwd = .w_PWDSILENT
        .w_GROUPDEF = i_GROUPROLE
          .DoRTCalc(7,7,.f.)
        .w_Azi = i_CODAZI
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_Azi))
          .link_1_8('Full')
        endif
        .w_CODAZI = LEFT(.w_AZI,5)
          .DoRTCalc(10,10,.f.)
        .w_DATSYS = i_DATSYS
        .w_CODESE = CALCESER(i_DATSYS, g_CODESE)
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_CODESE))
          .link_1_12('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_19.Calculate('*')
      .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
          .DoRTCalc(13,14,.f.)
        .w_OUTE = i_CODUTE
        .w_CLIENTE = GetCliRagSoc()
      .oPgFrm.Page1.oPag.oObj_1_26.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_27.Calculate(.w_CLIENTE,'','')
      .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
          .DoRTCalc(17,17,.f.)
        .w_CODUTEMOBY = -10*.w_UTE
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_CODUTEMOBY))
          .link_1_32('Full')
        endif
          .DoRTCalc(19,19,.f.)
        .w_MOBIMODE = i_bMobileMode AND NVL(g_MOBY,'N')='S' AND NVL(.w_CODUTEMOBY, 0)<>0
      .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
      .oPgFrm.Page1.oPag.LblAzie.Calculate(IIF(IsAlt(),AH_MSGFORMAT("Studio:"),AH_MSGFORMAT("Azienda:")))
      .oPgFrm.Page1.oPag.oObj_1_52.Calculate(IIF(IsAlt(), "Codice studio in cui operare", "Codice azienda in cui operare"))
      .oPgFrm.Page1.oPag.oObj_1_53.Calculate(IIF(IsAlt(), "Studio non esistente", "Azienda non esistente"))
          .DoRTCalc(21,26,.f.)
        .w_VALLIN = Calcnlin(this)
        .w_OLDAZI = LEFT(.w_AZI,5)
    endwith
    this.DoRTCalc(29,29,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_35.enabled = this.oPgFrm.Page1.oPag.oBtn_1_35.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_36.enabled = this.oPgFrm.Page1.oPag.oBtn_1_36.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_37.enabled = this.oPgFrm.Page1.oPag.oBtn_1_37.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_38.enabled = this.oPgFrm.Page1.oPag.oBtn_1_38.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_40.enabled = this.oPgFrm.Page1.oPag.oBtn_1_40.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_42.enabled = this.oPgFrm.Page1.oPag.oBtn_1_42.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gs___ute
    * --- Ingresso connessione silente
      if this.oParentObject.w_UTESILENT<>0
        this.bUpdated=.t.
        if this.CheckForm() and TerminateEditNoSet()
          this.lockscreen=.t.
          this.mReplace(.t.)
          this.lockscreen=.f.
          this.Hide()
          this.NotifyEvent("ChkPwd")
          if this.w_OKPWD='OK'
           this.NotifyEvent("Done")
           this.windowtype=0
           this.Release()
          endif
        endif
      endif
    
    * --- Apertura o meno completa maschera
    * --- se non presente azienda o esercizio apro tutto..
    * --- Se mancano campi necessari (Azienda/Esercizio), non siamo all'avvio
    * --- oppure g_OPENAUT definita apro la maschera completamente..
    If Empty( This.w_CODAZI ) Or Empty( this.w_CODESE ) Or Not g_INIZ Or Type('g_OPENAUT')<>'U'
     This.Sizer(.f.)
    Else
     This.Sizer(.t.)
    Endif
    
    *--- Controllo Single Sign-on, se disabilitato entro con autenticazione
    *--- di tipo Login
    If Type("g_NoSingleSignOn") = 'L' And g_NoSingleSignOn And This.w_NOCODUTE = 'W'
       This.w_NOCODUTE = 'N'
    Endif
    
    *--- Potrebbe essere vuoto
    This.w_NOCODUTE = IIF(Empty(This.w_NOCODUTE),'N', This.w_NOCODUTE)
    this.mcalc(.t.)
    *--- Single Sign-On
    This.NotifyEvent("SingleSignOn")
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- gs___ute
    *** se il gruppo preferenziale � cambiato
    *** ricalcolo il menu e la lista tabelle filtrate
    if this.w_GROUPDEF<>i_GROUPROLE
      i_GROUPROLE=this.w_GROUPDEF
      cp_LoadTableSecArray()
    endif
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_UTESILENT=.w_UTESILENT
      .oParentObject.w_PWDSILENT=.w_PWDSILENT
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_AHE<>.w_AHE
          .link_1_2('Full')
        endif
        .DoRTCalc(3,8,.t.)
        if .o_Azi<>.w_Azi
            .w_CODAZI = LEFT(.w_AZI,5)
        endif
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate('*')
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate(.w_CLIENTE,'','')
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .DoRTCalc(10,17,.t.)
            .w_CODUTEMOBY = -10*.w_UTE
          .link_1_32('Full')
        .DoRTCalc(19,19,.t.)
        if .o_UTE<>.w_UTE
            .w_MOBIMODE = i_bMobileMode AND NVL(g_MOBY,'N')='S' AND NVL(.w_CODUTEMOBY, 0)<>0
        endif
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        .oPgFrm.Page1.oPag.LblAzie.Calculate(IIF(IsAlt(),AH_MSGFORMAT("Studio:"),AH_MSGFORMAT("Azienda:")))
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate(IIF(IsAlt(), "Codice studio in cui operare", "Codice azienda in cui operare"))
        .oPgFrm.Page1.oPag.oObj_1_53.Calculate(IIF(IsAlt(), "Studio non esistente", "Azienda non esistente"))
        if .o_UTE<>.w_UTE
          .Calculate_JOXQQTHIPZ()
        endif
        if .o_UTE<>.w_UTE
          .Calculate_YFJQRTCDIB()
        endif
        if .o_GROUPDEF<>.w_GROUPDEF
          .Calculate_WTFPITFRQQ()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(21,29,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate('*')
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate(.w_CLIENTE,'','')
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        .oPgFrm.Page1.oPag.LblAzie.Calculate(IIF(IsAlt(),AH_MSGFORMAT("Studio:"),AH_MSGFORMAT("Azienda:")))
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate(IIF(IsAlt(), "Codice studio in cui operare", "Codice azienda in cui operare"))
        .oPgFrm.Page1.oPag.oObj_1_53.Calculate(IIF(IsAlt(), "Studio non esistente", "Azienda non esistente"))
    endwith
  return

  proc Calculate_NHGZPPOKMU()
    with this
          * --- Cambia caption form
          .Caption = IIF(!g_INIZ.and.IsAlt(),AH_MSGFORMAT("Selezione studio"),.Caption)
    endwith
  endproc
  proc Calculate_JOXQQTHIPZ()
    with this
          * --- Calcolo GROUPDEF
          .CalcGROUPDEF(this;
             )
          .cambiogruppo(this;
             )
    endwith
  endproc
  proc Calculate_EHAHHSYDAD()
    with this
          * --- Single Sign-on
          GSUT_BPA(this;
              ,'W';
             )
    endwith
  endproc
  proc Calculate_QABZYYDKOM()
    with this
          * --- Update end
          assignlanguage(this;
             )
          GS___BAZ(this;
             )
    endwith
  endproc
  proc Calculate_ACCFZOCJYD()
    with this
          * --- sbianco i_codute
          BlankCodute(this;
             )
    endwith
  endproc
  proc Calculate_NTBOEJPIVU()
    with this
          * --- Cambia la modalit� standard / mobile
          .w_MOBIMODE = !.w_MOBIMODE
    endwith
  endproc
  proc Calculate_YFJQRTCDIB()
    with this
          * --- se non attiva la libreria mobile allargo le descrizioni
          CheckSizeMoby(this;
             )
    endwith
  endproc
  proc Calculate_VIMBRPNMCN()
    with this
          * --- Assegno groupdef all'ingresso
          .w_GROUPDEF = i_GROUPROLE
    endwith
  endproc
  proc Calculate_WTFPITFRQQ()
    with this
          * --- GROUPROLE
          .cambiogruppo(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oNOMEUTE_1_1.enabled = this.oPgFrm.Page1.oPag.oNOMEUTE_1_1.mCond()
    this.oPgFrm.Page1.oPag.oUTE_1_3.enabled = this.oPgFrm.Page1.oPag.oUTE_1_3.mCond()
    this.oPgFrm.Page1.oPag.opwd_1_5.enabled = this.oPgFrm.Page1.oPag.opwd_1_5.mCond()
    this.oPgFrm.Page1.oPag.oGROUPDEF_1_6.enabled = this.oPgFrm.Page1.oPag.oGROUPDEF_1_6.mCond()
    this.oPgFrm.Page1.oPag.oAzi_1_8.enabled = this.oPgFrm.Page1.oPag.oAzi_1_8.mCond()
    this.oPgFrm.Page1.oPag.oDATSYS_1_11.enabled = this.oPgFrm.Page1.oPag.oDATSYS_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_35.enabled = this.oPgFrm.Page1.oPag.oBtn_1_35.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_36.enabled = this.oPgFrm.Page1.oPag.oBtn_1_36.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_37.enabled = this.oPgFrm.Page1.oPag.oBtn_1_37.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oNOMEUTE_1_1.visible=!this.oPgFrm.Page1.oPag.oNOMEUTE_1_1.mHide()
    this.oPgFrm.Page1.oPag.oUTE_1_3.visible=!this.oPgFrm.Page1.oPag.oUTE_1_3.mHide()
    this.oPgFrm.Page1.oPag.oDESUTE_1_4.visible=!this.oPgFrm.Page1.oPag.oDESUTE_1_4.mHide()
    this.oPgFrm.Page1.oPag.oLINDEF_1_7.visible=!this.oPgFrm.Page1.oPag.oLINDEF_1_7.mHide()
    this.oPgFrm.Page1.oPag.oAzi_1_8.visible=!this.oPgFrm.Page1.oPag.oAzi_1_8.mHide()
    this.oPgFrm.Page1.oPag.oRAGAZI_1_10.visible=!this.oPgFrm.Page1.oPag.oRAGAZI_1_10.mHide()
    this.oPgFrm.Page1.oPag.oDATSYS_1_11.visible=!this.oPgFrm.Page1.oPag.oDATSYS_1_11.mHide()
    this.oPgFrm.Page1.oPag.oCODESE_1_12.visible=!this.oPgFrm.Page1.oPag.oCODESE_1_12.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_13.visible=!this.oPgFrm.Page1.oPag.oStr_1_13.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_17.visible=!this.oPgFrm.Page1.oPag.oStr_1_17.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_35.visible=!this.oPgFrm.Page1.oPag.oBtn_1_35.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_36.visible=!this.oPgFrm.Page1.oPag.oBtn_1_36.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_40.visible=!this.oPgFrm.Page1.oPag.oBtn_1_40.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_41.visible=!this.oPgFrm.Page1.oPag.oStr_1_41.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_42.visible=!this.oPgFrm.Page1.oPag.oBtn_1_42.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_43.visible=!this.oPgFrm.Page1.oPag.oStr_1_43.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_47.visible=!this.oPgFrm.Page1.oPag.oStr_1_47.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_58.visible=!this.oPgFrm.Page1.oPag.oStr_1_58.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_19.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_20.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_26.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_27.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_31.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_44.Event(cEvent)
        if lower(cEvent)==lower("Init")
          .Calculate_NHGZPPOKMU()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.LblAzie.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_52.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_53.Event(cEvent)
        if lower(cEvent)==lower("Blank")
          .Calculate_JOXQQTHIPZ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("SingleSignOn")
          .Calculate_EHAHHSYDAD()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Update end")
          .Calculate_QABZYYDKOM()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Edit Aborted")
          .Calculate_ACCFZOCJYD()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("ChangeMode")
          .Calculate_NTBOEJPIVU()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank")
          .Calculate_YFJQRTCDIB()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank")
          .Calculate_VIMBRPNMCN()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gs___ute
    * ---- Se l'utente preme Esc o annulla
    * ---- esco dalla procedura..
    If cEvent='Edit Aborted'
      i_codute=0
      CLEAR EVENTS
    Endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=AHE
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.POL_SIC_IDX,3]
    i_lTable = "POL_SIC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.POL_SIC_IDX,2], .t., this.POL_SIC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.POL_SIC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AHE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AHE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PSSERIAL,PS_NONUM";
                   +" from "+i_cTable+" "+i_lTable+" where PSSERIAL="+cp_ToStrODBC(this.w_AHE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PSSERIAL',this.w_AHE)
            select PSSERIAL,PS_NONUM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AHE = NVL(_Link_.PSSERIAL,space(10))
      this.w_NOCODUTE = NVL(_Link_.PS_NONUM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_AHE = space(10)
      endif
      this.w_NOCODUTE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.POL_SIC_IDX,2])+'\'+cp_ToStr(_Link_.PSSERIAL,1)
      cp_ShowWarn(i_cKey,this.POL_SIC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AHE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UTE
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_UTE);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME,WEBTYPE,LANGUAGE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_UTE)
          select CODE,NAME,WEBTYPE,LANGUAGE;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_UTE) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oUTE_1_3'),i_cWhere,'',"Elenco utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME,WEBTYPE,LANGUAGE";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME,WEBTYPE,LANGUAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME,WEBTYPE,LANGUAGE";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_UTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_UTE)
            select CODE,NAME,WEBTYPE,LANGUAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UTE = NVL(_Link_.CODE,0)
      this.w_DESUTE = NVL(_Link_.NAME,space(20))
      this.w_WEBTYPE = NVL(_Link_.WEBTYPE,space(1))
      this.w_LINDEF = NVL(_Link_.LANGUAGE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_UTE = 0
      endif
      this.w_DESUTE = space(20)
      this.w_WEBTYPE = space(1)
      this.w_LINDEF = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_WEBTYPE)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Utente non definito")
        endif
        this.w_UTE = 0
        this.w_DESUTE = space(20)
        this.w_WEBTYPE = space(1)
        this.w_LINDEF = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=Azi
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_Azi) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AZIENDA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AZCODAZI like "+cp_ToStrODBC(trim(this.w_Azi)+"%");

          i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AZCODAZI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AZCODAZI',trim(this.w_Azi))
          select AZCODAZI,AZRAGAZI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AZCODAZI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_Azi)==trim(_Link_.AZCODAZI) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" AZRAGAZI like "+cp_ToStrODBC(trim(this.w_Azi)+"%");

            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" AZRAGAZI like "+cp_ToStr(trim(this.w_Azi)+"%");

            select AZCODAZI,AZRAGAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_Azi) and !this.bDontReportError
            deferred_cp_zoom('AZIENDA','*','AZCODAZI',cp_AbsName(oSource.parent,'oAzi_1_8'),i_cWhere,'',"",'loginazi.AZIENDA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                     +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',oSource.xKey(1))
            select AZCODAZI,AZRAGAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_Azi)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_Azi);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_Azi)
            select AZCODAZI,AZRAGAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_Azi = NVL(_Link_.AZCODAZI,space(5))
      this.w_RAGAZI = NVL(_Link_.AZRAGAZI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_Azi = space(5)
      endif
      this.w_RAGAZI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_Azi Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODESE
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_CODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_CODESE))
          select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oCODESE_1_12'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_CODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_CODESE)
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_INIESE = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_FINESE = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODESE = space(4)
      endif
      this.w_INIESE = ctod("  /  /  ")
      this.w_FINESE = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODUTEMOBY
  func Link_1_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONF_INT_IDX,3]
    i_lTable = "CONF_INT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONF_INT_IDX,2], .t., this.CONF_INT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONF_INT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODUTEMOBY) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODUTEMOBY)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CICODUTE";
                   +" from "+i_cTable+" "+i_lTable+" where CICODUTE="+cp_ToStrODBC(this.w_CODUTEMOBY);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CICODUTE',this.w_CODUTEMOBY)
            select CICODUTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODUTEMOBY = NVL(_Link_.CICODUTE,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODUTEMOBY = 0
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONF_INT_IDX,2])+'\'+cp_ToStr(_Link_.CICODUTE,1)
      cp_ShowWarn(i_cKey,this.CONF_INT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODUTEMOBY Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oNOMEUTE_1_1.value==this.w_NOMEUTE)
      this.oPgFrm.Page1.oPag.oNOMEUTE_1_1.value=this.w_NOMEUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oUTE_1_3.value==this.w_UTE)
      this.oPgFrm.Page1.oPag.oUTE_1_3.value=this.w_UTE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUTE_1_4.value==this.w_DESUTE)
      this.oPgFrm.Page1.oPag.oDESUTE_1_4.value=this.w_DESUTE
    endif
    if not(this.oPgFrm.Page1.oPag.opwd_1_5.value==this.w_pwd)
      this.oPgFrm.Page1.oPag.opwd_1_5.value=this.w_pwd
    endif
    if not(this.oPgFrm.Page1.oPag.oGROUPDEF_1_6.RadioValue()==this.w_GROUPDEF)
      this.oPgFrm.Page1.oPag.oGROUPDEF_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLINDEF_1_7.RadioValue()==this.w_LINDEF)
      this.oPgFrm.Page1.oPag.oLINDEF_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAzi_1_8.value==this.w_Azi)
      this.oPgFrm.Page1.oPag.oAzi_1_8.value=this.w_Azi
    endif
    if not(this.oPgFrm.Page1.oPag.oRAGAZI_1_10.value==this.w_RAGAZI)
      this.oPgFrm.Page1.oPag.oRAGAZI_1_10.value=this.w_RAGAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATSYS_1_11.value==this.w_DATSYS)
      this.oPgFrm.Page1.oPag.oDATSYS_1_11.value=this.w_DATSYS
    endif
    if not(this.oPgFrm.Page1.oPag.oCODESE_1_12.value==this.w_CODESE)
      this.oPgFrm.Page1.oPag.oCODESE_1_12.value=this.w_CODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oINIESE_1_15.value==this.w_INIESE)
      this.oPgFrm.Page1.oPag.oINIESE_1_15.value=this.w_INIESE
    endif
    if not(this.oPgFrm.Page1.oPag.oFINESE_1_16.value==this.w_FINESE)
      this.oPgFrm.Page1.oPag.oFINESE_1_16.value=this.w_FINESE
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_NOMEUTE))  and not(.w_NOCODUTE = 'N')  and (.w_NOCODUTE = 'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNOMEUTE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_NOMEUTE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_UTE)) or not(EMPTY(.w_WEBTYPE)))  and not(.w_NOCODUTE <> 'N')  and (.w_NOCODUTE = 'N')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oUTE_1_3.SetFocus()
            i_bnoObbl = !empty(.w_UTE)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Utente non definito")
          case   (empty(.w_Azi))  and not(Not .w_TODO)  and (!(Vartype(g_REVI)='C' And g_REVI='S' And Vartype(p_EntryKey)='C'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAzi_1_8.SetFocus()
            i_bnoObbl = !empty(.w_Azi)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DATSYS))  and not(Not .w_TODO)  and (!(Vartype(g_REVI)='C' And g_REVI='S' And Vartype(p_EntryKey)='C'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATSYS_1_11.SetFocus()
            i_bnoObbl = !empty(.w_DATSYS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CODESE))  and not(Not .w_TODO)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODESE_1_12.SetFocus()
            i_bnoObbl = !empty(.w_CODESE)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gs___ute
      * --- Controlli per gestione Privacy (se tutto ok)
      if i_bRes
        this.w_OKPWD='OK'
        this.NotifyEvent('ChkPwd')
        do case
          case this.w_OKPWD='OK'
            i_bRes = .t.
          case this.w_OKPWD='RIT'
            i_bRes = .f.
          case this.w_OKPWD='ESC'
           i_bRes = .f.
           i_codute=0
           Clear Events
           this.EcpQuit()
        endcase
      endif
      
      if i_bRes and empty(.w_azi)
         i_cErrorMsg=Ah_MsgFormat("Codice azienda obbligatorio")
         i_bRes = .f.
         i_bnoChk = .f.
      endif
      
      if i_bRes and empty(.w_CODESE)
         i_cErrorMsg=Ah_MsgFormat("Codice esercizio obbligatorio")
         i_bRes = .f.
         i_bnoChk = .f.
      endif
      
      if i_bRes and not chkuteaz(.w_CODAZI, .w_UTE)
               i_bnoChk = .f.
               i_bRes = .f.
               i_cErrorMsg=Ah_MsgFormat("Codice utente inesistente o non associato all'azienda")
               .w_UTE=0
               i_CODUTE=0
               .w_DESUTE=space(20)
               .w_NOMEUTE=space(40)
               .w_pwd=space(20)
      endif
      
      if i_bRes and nvl(.w_CODUTEMOBY,0)=0 and .w_MOBIMODE
         .w_MOBIMODE=.f.
         i_bMobileMode=.f.
         i_cErrorMsg=Ah_MsgFormat("Impossibile accedere in modalit� mobile")
         i_bRes = .f.
         i_bnoChk = .f.
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_AHE = this.w_AHE
    this.o_UTE = this.w_UTE
    this.o_GROUPDEF = this.w_GROUPDEF
    this.o_Azi = this.w_Azi
    return

enddefine

* --- Define pages as container
define class tgs___utePag1 as StdContainer
  Width  = 654
  height = 223
  stdWidth  = 654
  stdheight = 223
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oNOMEUTE_1_1 as StdField with uid="HBIGYBDBVX",rtseq=1,rtrep=.f.,;
    cFormVar = "w_NOMEUTE", cQueryName = "NOMEUTE",;
    bObbl = .t. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Codice login",;
    HelpContextID = 89094870,;
   bGlobalFont=.t.,;
    Height=21, Width=297, Left=145, Top=6, InputMask=replicate('X',254)

  func oNOMEUTE_1_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NOCODUTE = 'S')
    endwith
   endif
  endfunc

  func oNOMEUTE_1_1.mHide()
    with this.Parent.oContained
      return (.w_NOCODUTE = 'N')
    endwith
  endfunc

  add object oUTE_1_3 as StdField with uid="DOBBSYFCFJ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_UTE", cQueryName = "UTE",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Utente non definito",;
    ToolTipText = "Codice utente",;
    HelpContextID = 71696314,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=145, Top=6, cSayPict='"9999"', cGetPict='"9999"', bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_UTE"

  func oUTE_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NOCODUTE = 'N')
    endwith
   endif
  endfunc

  func oUTE_1_3.mHide()
    with this.Parent.oContained
      return (.w_NOCODUTE <> 'N')
    endwith
  endfunc

  func oUTE_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oUTE_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oUTE_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oUTE_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco utenti",'',this.parent.oContained
  endproc

  add object oDESUTE_1_4 as StdField with uid="WDOPCYWMCC",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESUTE", cQueryName = "DESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 105893942,;
   bGlobalFont=.t.,;
    Height=21, Width=237, Left=210, Top=6, InputMask=replicate('X',20)

  func oDESUTE_1_4.mHide()
    with this.Parent.oContained
      return (.w_NOCODUTE <> 'N')
    endwith
  endfunc

  add object opwd_1_5 as StdField with uid="ZZUBQTSIIP",rtseq=5,rtrep=.f.,;
    cFormVar = "w_pwd", cQueryName = "pwd",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Password per accedere ad adhoc Revolution",;
    HelpContextID = 71559946,;
   bGlobalFont=.t.,;
    Height=21, Width=302, Left=145, Top=34, InputMask=replicate('X',20)

  func opwd_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NOCODUTE <> 'W')
    endwith
   endif
  endfunc


  add object oGROUPDEF_1_6 as StdZTamTableCombo with uid="RVXHRTQDCL",rtseq=6,rtrep=.f.,left=145,top=61,width=136,height=21;
    , ToolTipText = "Gruppo preferenziale di utilizzo della procedura";
    , HelpContextID = 84909484;
    , cFormVar="w_GROUPDEF",tablefilter="", bObbl = .f. , nPag = 1;
    , cTable='GROUPSROLE1.VQR',cKey='code',cValue='name',cOrderBy='code',xDefault=0;
  , bGlobalFont=.t.


  func oGROUPDEF_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_UTE<>0 and .w_NUMBGROUP>1)
    endwith
   endif
  endfunc


  add object oLINDEF_1_7 as StdTableCombo with uid="GMNGSDGKLZ",rtseq=7,rtrep=.f.,left=385,top=59,width=57,height=21;
    , ToolTipText = "Lingua della procedura";
    , HelpContextID = 105809078;
    , cFormVar="w_LINDEF",tablefilter="", bObbl = .f. , nPag = 1;
    , cTable='luser.vqr',cKey='language',cValue='language',cOrderBy='language',xDefault=space(3);
  , bGlobalFont=.t.


  func oLINDEF_1_7.mHide()
    with this.Parent.oContained
      return (.w_VALLIN <2)
    endwith
  endfunc

  add object oAzi_1_8 as StdField with uid="ZWZJXDBBWI",rtseq=8,rtrep=.f.,;
    cFormVar = "w_Azi", cQueryName = "Azi",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 71539450,;
   bGlobalFont=.t.,;
    Height=21, Width=64, Left=145, Top=102, cSayPict='"AXXXX"', cGetPict='"AXXXX"', InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AZIENDA", oKey_1_1="AZCODAZI", oKey_1_2="this.w_Azi"

  func oAzi_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!(Vartype(g_REVI)='C' And g_REVI='S' And Vartype(p_EntryKey)='C'))
    endwith
   endif
  endfunc

  func oAzi_1_8.mHide()
    with this.Parent.oContained
      return (Not .w_TODO)
    endwith
  endfunc

  func oAzi_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oAzi_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAzi_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AZIENDA','*','AZCODAZI',cp_AbsName(this.parent,'oAzi_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'loginazi.AZIENDA_VZM',this.parent.oContained
  endproc

  add object oRAGAZI_1_10 as StdField with uid="AFCKKDKMGG",rtseq=10,rtrep=.f.,;
    cFormVar = "w_RAGAZI", cQueryName = "RAGAZI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 177933590,;
   bGlobalFont=.t.,;
    Height=21, Width=435, Left=211, Top=102, InputMask=replicate('X',40)

  func oRAGAZI_1_10.mHide()
    with this.Parent.oContained
      return (Not .w_TODO)
    endwith
  endfunc

  add object oDATSYS_1_11 as StdField with uid="FHFGLGJIZC",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DATSYS", cQueryName = "DATSYS",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di sistema",;
    HelpContextID = 77454390,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=145, Top=130

  func oDATSYS_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!(Vartype(g_REVI)='C' And g_REVI='S' And Vartype(p_EntryKey)='C'))
    endwith
   endif
  endfunc

  func oDATSYS_1_11.mHide()
    with this.Parent.oContained
      return (Not .w_TODO)
    endwith
  endfunc

  add object oCODESE_1_12 as StdField with uid="QQTTIZSRND",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CODESE", cQueryName = "CODESE",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Codice esercizio da elaborare",;
    HelpContextID = 103737894,;
   bGlobalFont=.t.,;
    Height=21, Width=60, Left=145, Top=158, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_CODESE"

  func oCODESE_1_12.mHide()
    with this.Parent.oContained
      return (Not .w_TODO)
    endwith
  endfunc

  func oCODESE_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODESE_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODESE_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oCODESE_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oINIESE_1_15 as StdField with uid="QGJEKFHLXH",rtseq=13,rtrep=.f.,;
    cFormVar = "w_INIESE", cQueryName = "INIESE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio dell'esercizio selezionato",;
    HelpContextID = 103758214,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=253, Top=158

  add object oFINESE_1_16 as StdField with uid="JWSBTPSPQF",rtseq=14,rtrep=.f.,;
    cFormVar = "w_FINESE", cQueryName = "FINESE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di fine dell'esercizio selezionato",;
    HelpContextID = 103777366,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=367, Top=158


  add object oObj_1_19 as cp_setobjprop with uid="TSUREHZRHG",left=393, top=306, width=178,height=19,;
    caption='Pwd',;
   bGlobalFont=.t.,;
    cObj='w_pwd',cProp='passwordchar',;
    nPag=1;
    , HelpContextID = 71560458


  add object oObj_1_20 as cp_runprogram with uid="LHFNGKFOVU",left=216, top=284, width=294,height=19,;
    caption='GS___BCK',;
   bGlobalFont=.t.,;
    prg="GS___BCK",;
    cEvent = "w_DATSYS Changed,w_Azi Changed",;
    nPag=1;
    , HelpContextID = 67804849


  add object oObj_1_26 as cp_runprogram with uid="TLZMAHCYNU",left=2, top=285, width=209,height=19,;
    caption='GS__QUIT',;
   bGlobalFont=.t.,;
    prg="GS__QUIT",;
    cEvent = "Edit Aborted",;
    nPag=1;
    , HelpContextID = 164979014


  add object oObj_1_27 as cp_calclbl with uid="GIKLJLSMOK",left=9, top=201, width=634,height=15,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",fontsize=8,fontitalic=.f.,fontbold=.t.,;
    nPag=1;
    , HelpContextID = 105995750


  add object oObj_1_31 as cp_runprogram with uid="IOYDEPLMWN",left=217, top=307, width=171,height=18,;
    caption='GSUT_BPS(U)',;
   bGlobalFont=.t.,;
    prg="GSUT_BPS('U')",;
    cEvent = "ChkPwd",;
    nPag=1;
    , ToolTipText = "Verifica password..";
    , HelpContextID = 67233337


  add object oBtn_1_35 as StdButton with uid="DHVZGQIAHL",left=451, top=6, width=48,height=45,;
    CpPicture="BMP\SCARICA.BMP", caption="", nPag=1;
    , ToolTipText = "L'ingresso nella procedura avverr� con modalit� mobile. Premere per attivare l'ingresso con modalit� standard";
    , HelpContextID = 132442822;
    , Caption='\<Mobile';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_35.Click()
      this.parent.oContained.NotifyEvent("ChangeMode")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_35.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NVL(g_MOBY,'N')='S')
      endwith
    endif
  endfunc

  func oBtn_1_35.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (!.w_MOBIMODE or NVL(g_MOBY,'N')<>'S' or NVL(.w_CODUTEMOBY, 0)=0)
     endwith
    endif
  endfunc


  add object oBtn_1_36 as StdButton with uid="RKYABXSZJD",left=451, top=6, width=48,height=45,;
    CpPicture="BMP\MOSTRA.BMP", caption="", nPag=1;
    , ToolTipText = "L'ingresso nella procedura avverr� con modalit� standard. Premere per attivare l'ingresso con modalit� mobile";
    , HelpContextID = 57270410;
    , Caption='\<Standard';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_36.Click()
      this.parent.oContained.NotifyEvent("ChangeMode")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_36.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NVL(g_MOBY,'N')='S')
      endwith
    endif
  endfunc

  func oBtn_1_36.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_MOBIMODE or NVL(g_MOBY,'N')<>'S' or NVL(.w_CODUTEMOBY, 0)=0)
     endwith
    endif
  endfunc


  add object oBtn_1_37 as StdButton with uid="IFYYEBLTNS",left=500, top=6, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 71973146;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_37.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_37.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty( .w_UTE ) Or Not Empty( .w_NOMEUTE ))
      endwith
    endif
  endfunc


  add object oBtn_1_38 as StdButton with uid="BZJPSEFWYZ",left=549, top=6, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci dalla procedura";
    , HelpContextID = 64684474;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_38.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_40 as StdButton with uid="JTFDWIUASN",left=598, top=6, width=48,height=45,;
    CpPicture="BMP\DOWN.BMP", caption="", nPag=1;
    , ToolTipText = "Apri opzioni";
    , HelpContextID = 71984938;
    , Caption='E\<spandi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_40.Click()
      with this.Parent.oContained
        .Sizer( .w_TODO )
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_40.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_TODO)
     endwith
    endif
  endfunc


  add object oBtn_1_42 as StdButton with uid="SCHGVJCQGP",left=598, top=6, width=48,height=45,;
    CpPicture="BMP\UP.BMP", caption="", nPag=1;
    , ToolTipText = "Chiudi opzioni";
    , HelpContextID = 71985482;
    , Caption='\<Riduci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_42.Click()
      with this.Parent.oContained
        .Sizer( .w_TODO )
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_42.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT .w_TODO)
     endwith
    endif
  endfunc


  add object oObj_1_44 as cp_runprogram with uid="YJCWAIPSWO",left=3, top=379, width=256,height=21,;
    caption='GSUT_BPS(N)',;
   bGlobalFont=.t.,;
    prg='GSUT_BPS("N")',;
    cEvent = "w_NOMEUTE Changed",;
    nPag=1;
    , HelpContextID = 67231545


  add object LblAzie as cp_calclbl with uid="KZLEWMBSIL",left=49, top=102, width=94,height=25,;
    caption='LblAzie',;
   bGlobalFont=.t.,;
    caption="label text",fontname="Arial",fontsize=9,fontBold=.t.,fontItalic=.f.,fontUnderline=.f.,Alignment=1,;
    nPag=1;
    , HelpContextID = 56787530


  add object oObj_1_52 as cp_setobjprop with uid="HXGWXJKQFL",left=3, top=436, width=175,height=21,;
    caption='ToolTip di w_Azi',;
   bGlobalFont=.t.,;
    cProp="ToolTipText",cObj="w_Azi",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 30550783


  add object oObj_1_53 as cp_setobjprop with uid="PKLQURBILB",left=3, top=462, width=175,height=21,;
    caption='sErrorMsg di w_Azi',;
   bGlobalFont=.t.,;
    cProp="sErrorMsg",cObj="w_Azi",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 244226794

  add object oStr_1_13 as StdString with uid="SHKAMVUBZK",Visible=.t., Left=39, Top=130,;
    Alignment=1, Width=104, Height=15,;
    Caption="Data di sistema:"  ;
  , bGlobalFont=.t.

  func oStr_1_13.mHide()
    with this.Parent.oContained
      return (Not .w_TODO)
    endwith
  endfunc

  add object oStr_1_14 as StdString with uid="WKCSCGJSUF",Visible=.t., Left=49, Top=158,;
    Alignment=1, Width=94, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="JOPQFORQYO",Visible=.t., Left=49, Top=6,;
    Alignment=1, Width=94, Height=15,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  func oStr_1_17.mHide()
    with this.Parent.oContained
      return (.w_NOCODUTE = 'S')
    endwith
  endfunc

  add object oStr_1_18 as StdString with uid="QHEMUWCBJH",Visible=.t., Left=49, Top=34,;
    Alignment=1, Width=94, Height=15,;
    Caption="Password:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="OKJCLUHKRX",Visible=.t., Left=216, Top=158,;
    Alignment=1, Width=34, Height=15,;
    Caption="Dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="TUERKFZQLT",Visible=.t., Left=341, Top=158,;
    Alignment=1, Width=23, Height=15,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="REJVTYRNRA",Visible=.t., Left=9, Top=185,;
    Alignment=1, Width=153, Height=17,;
    Caption="Licenza d'uso intestata a:"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_41 as StdString with uid="HMEZMCEPYL",Visible=.t., Left=2, Top=337,;
    Alignment=0, Width=365, Height=18,;
    Caption="Attenzione - caption bottoni utilizzate per catturare l'oggetto"  ;
  , bGlobalFont=.t.

  func oStr_1_41.mHide()
    with this.Parent.oContained
      return (.t.)
    endwith
  endfunc

  add object oStr_1_43 as StdString with uid="GMPWFTFGKD",Visible=.t., Left=1, Top=358,;
    Alignment=0, Width=506, Height=18,;
    Caption="Campi editabili con condizione di hide per escluderli dalla tabulazione se maschera chiusa.."  ;
  , bGlobalFont=.t.

  func oStr_1_43.mHide()
    with this.Parent.oContained
      return (.t.)
    endwith
  endfunc

  add object oStr_1_47 as StdString with uid="FMVPEFVYNL",Visible=.t., Left=49, Top=6,;
    Alignment=1, Width=94, Height=15,;
    Caption="Login:"  ;
  , bGlobalFont=.t.

  func oStr_1_47.mHide()
    with this.Parent.oContained
      return (.w_NOCODUTE <> 'S')
    endwith
  endfunc

  add object oStr_1_54 as StdString with uid="OGCHUJXVXU",Visible=.t., Left=5, Top=63,;
    Alignment=1, Width=138, Height=18,;
    Caption="Gruppo preferenziale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_58 as StdString with uid="GTERHJEEVQ",Visible=.t., Left=320, Top=63,;
    Alignment=1, Width=61, Height=18,;
    Caption="Lingua:"  ;
  , bGlobalFont=.t.

  func oStr_1_58.mHide()
    with this.Parent.oContained
      return (.w_VALLIN <2)
    endwith
  endfunc

  add object oBox_1_24 as StdBox with uid="HDLBKXXJVM",left=4, top=92, width=641,height=1

  add object oBox_1_29 as StdBox with uid="IRUEXORHGP",left=4, top=185, width=644,height=34
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gs___ute','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gs___ute
func Calcnlin(w_obj)
   local ComboLINDEF,nvalLINDEF
   ComboLINDEF=w_obj.GetCtrl("w_LINDEF",1,1,upper("StdTableCombo"))
   nvalLINDEF=ComboLINDEF.nvalues
   return nvalLINDEF
endfunc

Proc assignlanguage(pParent)
   i_clanguage=iif(empty(nvl(pParent.w_LINDEF,' ')),g_PROJECTLANGUAGE,pParent.w_LINDEF)
   if islanguagetranslate(i_cLanguage)
	   i_cLanguageData=i_cLanguage
	 else
	   i_cLanguageData=space(3)
	 endif
endproc

  func TerminateEditNoSet()
    local bRes,ac
    *ac=this.activeControl
    this.bOkToTerminate=.f.
    this.__dummy__.enabled=.t.
    return(this.bOkToTerminate)

Proc CheckSizeMoby(pParent)
local vardes
if g_MOBY<>'S' or pParent.w_CODUTEMOBY=0
   vardes=pParent.GetCtrl("w_DESUTE")
   vardes.width=237+48
   vardes=pParent.GetCtrl("w_NOMEUTE")
   vardes.width=297+48
   vardes=pParent.GetCtrl("w_PWD")
   vardes.width=302+48
   vardes=.null.
else
   vardes=pParent.GetCtrl("w_DESUTE")
   vardes.width=237
   vardes=pParent.GetCtrl("w_NOMEUTE")
   vardes.width=297
   vardes=pParent.GetCtrl("w_PWD")
   vardes.width=302
   vardes=.null.
endif
endproc

proc BlankCodute(obj)
   i_CODUTE=0
endproc

define class StdZTamTableCombo as StdTableCombo

proc Init()
  IF VARTYPE(this.bNoBackColor)='U'
		This.backcolor=i_EBackColor
	ENDIF
endproc

  proc Popola()
  * --- Zucchetti Aulla - Inizio Interfaccia
    IF VARTYPE(this.bNoBackColor)='U'
         This.backcolor=i_EBackColor
    ENDIF
  * --- Zucchetti Aulla - Fine Interfaccia	
    this.ToolTipText=cp_Translate(this.ToolTipText)	
    local i_nIdx,i_nConn,i_cTable,i_n1,i_n2,i_n3,i_curs,i_bCharKey,i_flt
    LOCAL i_fk,i_fd
    i_curs=sys(2015)
    if not empty(this.cTable)
       vq_exec(this.cTable,this.parent.oContained,i_curs)
    endif
    i_fk=this.cKey
    i_fd=this.cValue
    if used(i_curs)
        select (i_curs)
        this.nValues=iif(reccount()>1,reccount()+1,1)
        for i_j=alen(this.combovalues) to 1 step -1
          adel(this.combovalues,i_j)
          IF this.ListCount>0 AND i_j <= this.ListCount
         	 this.RemoveItem(i_j)
          ENDIF
        endfor
        dimension this.combovalues[MAX(1,this.nValues)]
        i_bCharKey=type(i_fk)='C'
        if this.nValues>1
            this.AddItem(' ')
            this.combovalues[1]=iif(i_bCharKey,Space(1),0)          
        else 
            i_GROUPROLE=code
            this.parent.oContained.w_GROUPDEF=i_GROUPROLE
        endif
        this.parent.oContained.w_NUMBGROUP=this.nValues
        do while !eof()
          this.AddItem(iif(type(i_fd)='C',ALLTRIM(&i_fd),ALLTRIM(str(&i_fd))))
          if i_bCharKey
            this.combovalues[iif(this.nValues=1,0,1)+recno()]=trim(&i_fk)
          else
            this.combovalues[iif(this.nValues=1,0,1)+recno()]=&i_fk
          endif
          skip
        enddo
        this.SetRadio()
        select (i_curs)
        use
    else
        this.enabled=.f.
    endif
    *--- Zucchetti Aulla Inizio - Interfaccia  
    IF This.bSetFont
   		This.SetFont()
    ENDIF
    *--- Zucchetti Aulla Fine - Interfaccia   

enddefine
* --- Fine Area Manuale
