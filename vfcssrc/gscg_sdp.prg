* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_sdp                                                        *
*              Stampa liquidazione periodica IVA                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_335]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-07-05                                                      *
* Last revis.: 2011-05-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_sdp",oParentObject))

* --- Class definition
define class tgscg_sdp as StdForm
  Top    = 3
  Left   = 95

  * --- Standard Properties
  Width  = 665
  Height = 411+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-05-11"
  HelpContextID=194370199
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=72

  * --- Constant Properties
  _IDX = 0
  AZIENDA_IDX = 0
  VALUTE_IDX = 0
  COC_MAST_IDX = 0
  IVA_PERI_IDX = 0
  ATTIMAST_IDX = 0
  COD_ABI_IDX = 0
  COD_CAB_IDX = 0
  cPrg = "gscg_sdp"
  cComment = "Stampa liquidazione periodica IVA   "
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(10)
  w_VP__ANNO = space(4)
  w_VPPERIOD = 0
  w_VPKEYATT = space(5)
  w_VPCODATT = space(5)
  w_VPCODVAL = space(1)
  w_IMPVAL = space(3)
  w_VPVARIMP = space(1)
  w_VPCORTER = space(1)
  w_RAGAZI = space(30)
  w_TIPDIC = space(1)
  w_VPDICGRU = space(1)
  w_VPDICSOC = space(1)
  w_FLTEST = space(1)
  w_VISPDF = space(1)
  w_VPIMPOR1 = 0
  w_VPCESINT = 0
  w_VPIMPOR2 = 0
  w_VPACQINT = 0
  w_VPIMPON3 = 0
  w_VPIMPOS3 = 0
  w_VPIMPOR5 = 0
  w_VPIMPOR6 = 0
  w_VPIMPOR7 = 0
  w_VPIMPOR8 = 0
  w_VPIMPO10 = 0
  w_VPIMPO12 = 0
  w_IMPDEB7 = 0
  w_IMPCRE7 = 0
  w_IMPDEB8 = 0
  w_IMPCRE8 = 0
  w_VPIMPOR9 = 0
  w_IMPDEB10 = 0
  w_IMPCRE10 = 0
  w_VPIMPO11 = 0
  w_VPIMPO13 = 0
  w_IMPDEB12 = 0
  w_IMPCRE12 = 0
  w_VPIMPO14 = 0
  w_VPIMPO16 = 0
  w_VPIMPO15 = 0
  w_VPVERNEF = space(1)
  w_VPAR74C5 = space(1)
  w_VPAR74C4 = space(1)
  w_VPOPMEDI = space(1)
  w_VPOPNOIM = space(1)
  w_VPCRERIM = 0
  w_VPEURO19 = space(1)
  w_VPCREUTI = 0
  w_VPCODCAR = space(1)
  w_VPCODFIS = space(16)
  w_VPDATVER = ctod('  /  /  ')
  w_VPCODAZI = space(5)
  w_VPCODCAB = space(5)
  w_COFAZI = space(16)
  w_PIVAZI = space(12)
  w_TIPLIQ = space(1)
  w_CONCES = space(3)
  w_DESBAN = space(35)
  w_DESATT = space(35)
  w_VPDATPRE = ctod('  /  /  ')
  w_VPACQBEA = space(1)
  w_IMPDEB9 = 0
  w_IMPCRE9 = 0
  w_VPIMPVER = 0
  w_VPVEREUR = space(1)
  w_FLTEST = space(1)
  w_VPIMPOR6C = 0
  w_DESABI = space(80)
  w_DESFIL = space(40)
  w_INDIRI = space(50)
  w_CAP = space(5)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_sdpPag1","gscg_sdp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dati IVA")
      .Pages(2).addobject("oPag","tgscg_sdpPag2","gscg_sdp",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Dati presentazione")
      .Pages(3).addobject("oPag","tgscg_sdpPag3","gscg_sdp",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Dati versamento")
      .Pages(3).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oVP__ANNO_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='COC_MAST'
    this.cWorkTables[4]='IVA_PERI'
    this.cWorkTables[5]='ATTIMAST'
    this.cWorkTables[6]='COD_ABI'
    this.cWorkTables[7]='COD_CAB'
    return(this.OpenAllTables(7))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(10)
      .w_VP__ANNO=space(4)
      .w_VPPERIOD=0
      .w_VPKEYATT=space(5)
      .w_VPCODATT=space(5)
      .w_VPCODVAL=space(1)
      .w_IMPVAL=space(3)
      .w_VPVARIMP=space(1)
      .w_VPCORTER=space(1)
      .w_RAGAZI=space(30)
      .w_TIPDIC=space(1)
      .w_VPDICGRU=space(1)
      .w_VPDICSOC=space(1)
      .w_FLTEST=space(1)
      .w_VISPDF=space(1)
      .w_VPIMPOR1=0
      .w_VPCESINT=0
      .w_VPIMPOR2=0
      .w_VPACQINT=0
      .w_VPIMPON3=0
      .w_VPIMPOS3=0
      .w_VPIMPOR5=0
      .w_VPIMPOR6=0
      .w_VPIMPOR7=0
      .w_VPIMPOR8=0
      .w_VPIMPO10=0
      .w_VPIMPO12=0
      .w_IMPDEB7=0
      .w_IMPCRE7=0
      .w_IMPDEB8=0
      .w_IMPCRE8=0
      .w_VPIMPOR9=0
      .w_IMPDEB10=0
      .w_IMPCRE10=0
      .w_VPIMPO11=0
      .w_VPIMPO13=0
      .w_IMPDEB12=0
      .w_IMPCRE12=0
      .w_VPIMPO14=0
      .w_VPIMPO16=0
      .w_VPIMPO15=0
      .w_VPVERNEF=space(1)
      .w_VPAR74C5=space(1)
      .w_VPAR74C4=space(1)
      .w_VPOPMEDI=space(1)
      .w_VPOPNOIM=space(1)
      .w_VPCRERIM=0
      .w_VPEURO19=space(1)
      .w_VPCREUTI=0
      .w_VPCODCAR=space(1)
      .w_VPCODFIS=space(16)
      .w_VPDATVER=ctod("  /  /  ")
      .w_VPCODAZI=space(5)
      .w_VPCODCAB=space(5)
      .w_COFAZI=space(16)
      .w_PIVAZI=space(12)
      .w_TIPLIQ=space(1)
      .w_CONCES=space(3)
      .w_DESBAN=space(35)
      .w_DESATT=space(35)
      .w_VPDATPRE=ctod("  /  /  ")
      .w_VPACQBEA=space(1)
      .w_IMPDEB9=0
      .w_IMPCRE9=0
      .w_VPIMPVER=0
      .w_VPVEREUR=space(1)
      .w_FLTEST=space(1)
      .w_VPIMPOR6C=0
      .w_DESABI=space(80)
      .w_DESFIL=space(40)
      .w_INDIRI=space(50)
      .w_CAP=space(5)
        .w_CODAZI = i_codazi
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODAZI))
          .link_1_1('Full')
        endif
        .w_VP__ANNO = g_CODESE
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_VPPERIOD))
          .link_1_3('Full')
        endif
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_VPKEYATT))
          .link_1_4('Full')
        endif
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_VPCODATT))
          .link_1_5('Full')
        endif
          .DoRTCalc(6,6,.f.)
        .w_IMPVAL = IIF(.w_VPCODVAL='S',g_CODEUR,g_CODLIR)
          .DoRTCalc(8,9,.f.)
        .w_RAGAZI = g_RAGAZI
        .w_TIPDIC = IIF(.w_VPDICGRU='S', 'G', IIF(.w_VPDICSOC='S', 'S', 'N'))
          .DoRTCalc(12,13,.f.)
        .w_FLTEST = ' '
          .DoRTCalc(15,27,.f.)
        .w_IMPDEB7 = IIF(.w_VPIMPOR7>0,  .w_VPIMPOR7, 0)
        .w_IMPCRE7 = IIF(.w_VPIMPOR7<0, ABS(.w_VPIMPOR7), 0)
        .w_IMPDEB8 = IIF(.w_VPIMPOR8>0, .w_VPIMPOR8, 0 )
        .w_IMPCRE8 = IIF(.w_VPIMPOR8<0, ABS(.w_VPIMPOR8), 0)
          .DoRTCalc(32,32,.f.)
        .w_IMPDEB10 = IIF(.w_VPIMPO10>0,  .w_VPIMPO10, 0)
        .w_IMPCRE10 = IIF( .w_VPIMPO10<0,  ABS(.w_VPIMPO10), 0)
          .DoRTCalc(35,36,.f.)
        .w_IMPDEB12 = IIF(.w_VPIMPO12>0,  .w_VPIMPO12  , 0 )
        .w_IMPCRE12 = IIF(.w_VPIMPO12<0, ABS(.w_VPIMPO12), 0)
        .DoRTCalc(39,53,.f.)
        if not(empty(.w_VPCODAZI))
          .link_3_21('Full')
        endif
        .DoRTCalc(54,54,.f.)
        if not(empty(.w_VPCODCAB))
          .link_3_22('Full')
        endif
          .DoRTCalc(55,56,.f.)
        .w_TIPLIQ = IIF(EMPTY(.w_VPCODATT), 'R', 'S')
          .DoRTCalc(58,62,.f.)
        .w_IMPDEB9 = IIF(.w_VPIMPOR9>0, .w_VPIMPOR9, 0)
        .w_IMPCRE9 = IIF(.w_VPIMPOR9<0, ABS(.w_VPIMPOR9), 0)
          .DoRTCalc(65,66,.f.)
        .w_FLTEST = ' '
    endwith
    this.DoRTCalc(68,72,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,4,.t.)
          .link_1_5('Full')
        .DoRTCalc(6,6,.t.)
            .w_IMPVAL = IIF(.w_VPCODVAL='S',g_CODEUR,g_CODLIR)
        .DoRTCalc(8,10,.t.)
            .w_TIPDIC = IIF(.w_VPDICGRU='S', 'G', IIF(.w_VPDICSOC='S', 'S', 'N'))
        .DoRTCalc(12,27,.t.)
            .w_IMPDEB7 = IIF(.w_VPIMPOR7>0,  .w_VPIMPOR7, 0)
            .w_IMPCRE7 = IIF(.w_VPIMPOR7<0, ABS(.w_VPIMPOR7), 0)
            .w_IMPDEB8 = IIF(.w_VPIMPOR8>0, .w_VPIMPOR8, 0 )
            .w_IMPCRE8 = IIF(.w_VPIMPOR8<0, ABS(.w_VPIMPOR8), 0)
        .DoRTCalc(32,32,.t.)
            .w_IMPDEB10 = IIF(.w_VPIMPO10>0,  .w_VPIMPO10, 0)
            .w_IMPCRE10 = IIF( .w_VPIMPO10<0,  ABS(.w_VPIMPO10), 0)
        .DoRTCalc(35,36,.t.)
            .w_IMPDEB12 = IIF(.w_VPIMPO12>0,  .w_VPIMPO12  , 0 )
            .w_IMPCRE12 = IIF(.w_VPIMPO12<0, ABS(.w_VPIMPO12), 0)
        .DoRTCalc(39,52,.t.)
          .link_3_21('Full')
          .link_3_22('Full')
        .DoRTCalc(55,56,.t.)
            .w_TIPLIQ = IIF(EMPTY(.w_VPCODATT), 'R', 'S')
        .DoRTCalc(58,62,.t.)
            .w_IMPDEB9 = IIF(.w_VPIMPOR9>0, .w_VPIMPOR9, 0)
            .w_IMPCRE9 = IIF(.w_VPIMPOR9<0, ABS(.w_VPIMPOR9), 0)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(65,72,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oVPPERIOD_1_3.enabled = this.oPgFrm.Page1.oPag.oVPPERIOD_1_3.mCond()
    this.oPgFrm.Page1.oPag.oVPKEYATT_1_4.enabled = this.oPgFrm.Page1.oPag.oVPKEYATT_1_4.mCond()
    this.oPgFrm.Page1.oPag.oVISPDF_1_20.enabled = this.oPgFrm.Page1.oPag.oVISPDF_1_20.mCond()
    this.oPgFrm.Page1.oPag.oFLTEST_1_34.enabled = this.oPgFrm.Page1.oPag.oFLTEST_1_34.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oVISPDF_1_20.visible=!this.oPgFrm.Page1.oPag.oVISPDF_1_20.mHide()
    this.oPgFrm.Page2.oPag.oIMPCRE7_2_18.visible=!this.oPgFrm.Page2.oPag.oIMPCRE7_2_18.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_37.visible=!this.oPgFrm.Page2.oPag.oStr_2_37.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_38.visible=!this.oPgFrm.Page2.oPag.oStr_2_38.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_39.visible=!this.oPgFrm.Page2.oPag.oStr_2_39.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_40.visible=!this.oPgFrm.Page2.oPag.oStr_2_40.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_41.visible=!this.oPgFrm.Page2.oPag.oStr_2_41.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_42.visible=!this.oPgFrm.Page2.oPag.oStr_2_42.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_43.visible=!this.oPgFrm.Page2.oPag.oStr_2_43.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_44.visible=!this.oPgFrm.Page2.oPag.oStr_2_44.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_45.visible=!this.oPgFrm.Page2.oPag.oStr_2_45.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_46.visible=!this.oPgFrm.Page2.oPag.oStr_2_46.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_47.visible=!this.oPgFrm.Page2.oPag.oStr_2_47.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_48.visible=!this.oPgFrm.Page2.oPag.oStr_2_48.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_49.visible=!this.oPgFrm.Page2.oPag.oStr_2_49.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_50.visible=!this.oPgFrm.Page2.oPag.oStr_2_50.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_51.visible=!this.oPgFrm.Page2.oPag.oStr_2_51.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_52.visible=!this.oPgFrm.Page2.oPag.oStr_2_52.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_53.visible=!this.oPgFrm.Page2.oPag.oStr_2_53.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_54.visible=!this.oPgFrm.Page2.oPag.oStr_2_54.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_55.visible=!this.oPgFrm.Page2.oPag.oStr_2_55.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_56.visible=!this.oPgFrm.Page2.oPag.oStr_2_56.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_57.visible=!this.oPgFrm.Page2.oPag.oStr_2_57.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_10.visible=!this.oPgFrm.Page3.oPag.oStr_3_10.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_19.visible=!this.oPgFrm.Page3.oPag.oStr_3_19.mHide()
    this.oPgFrm.Page1.oPag.oDESATT_1_29.visible=!this.oPgFrm.Page1.oPag.oDESATT_1_29.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_30.visible=!this.oPgFrm.Page1.oPag.oStr_1_30.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_72.visible=!this.oPgFrm.Page2.oPag.oStr_2_72.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_73.visible=!this.oPgFrm.Page2.oPag.oStr_2_73.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_34.visible=!this.oPgFrm.Page3.oPag.oStr_3_34.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZCOFAZI,AZPIVAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI,AZCOFAZI,AZPIVAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(10))
      this.w_COFAZI = NVL(_Link_.AZCOFAZI,space(16))
      this.w_PIVAZI = NVL(_Link_.AZPIVAZI,space(12))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(10)
      endif
      this.w_COFAZI = space(16)
      this.w_PIVAZI = space(12)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VPPERIOD
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IVA_PERI_IDX,3]
    i_lTable = "IVA_PERI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IVA_PERI_IDX,2], .t., this.IVA_PERI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IVA_PERI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VPPERIOD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVP',True,'IVA_PERI')
        if i_nConn<>0
          i_cWhere = " VPPERIOD="+cp_ToStrODBC(this.w_VPPERIOD);
                   +" and VP__ANNO="+cp_ToStrODBC(this.w_VP__ANNO);

          i_ret=cp_SQL(i_nConn,"select VP__ANNO,VPPERIOD";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VP__ANNO',this.w_VP__ANNO;
                     ,'VPPERIOD',this.w_VPPERIOD)
          select VP__ANNO,VPPERIOD;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_VPPERIOD) and !this.bDontReportError
            deferred_cp_zoom('IVA_PERI','*','VP__ANNO,VPPERIOD',cp_AbsName(oSource.parent,'oVPPERIOD_1_3'),i_cWhere,'GSAR_AVP',"Dichiarazioni IVA",'GSCGPSDP.IVA_PERI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_VP__ANNO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VP__ANNO,VPPERIOD";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select VP__ANNO,VPPERIOD;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Periodo dichiarazione non valido")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VP__ANNO,VPPERIOD";
                     +" from "+i_cTable+" "+i_lTable+" where VPPERIOD="+cp_ToStrODBC(oSource.xKey(2));
                     +" and VP__ANNO="+cp_ToStrODBC(this.w_VP__ANNO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VP__ANNO',oSource.xKey(1);
                       ,'VPPERIOD',oSource.xKey(2))
            select VP__ANNO,VPPERIOD;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VPPERIOD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VP__ANNO,VPPERIOD";
                   +" from "+i_cTable+" "+i_lTable+" where VPPERIOD="+cp_ToStrODBC(this.w_VPPERIOD);
                   +" and VP__ANNO="+cp_ToStrODBC(this.w_VP__ANNO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VP__ANNO',this.w_VP__ANNO;
                       ,'VPPERIOD',this.w_VPPERIOD)
            select VP__ANNO,VPPERIOD;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VPPERIOD = NVL(_Link_.VPPERIOD,0)
    else
      if i_cCtrl<>'Load'
        this.w_VPPERIOD = 0
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IVA_PERI_IDX,2])+'\'+cp_ToStr(_Link_.VP__ANNO,1)+'\'+cp_ToStr(_Link_.VPPERIOD,1)
      cp_ShowWarn(i_cKey,this.IVA_PERI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VPPERIOD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VPKEYATT
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IVA_PERI_IDX,3]
    i_lTable = "IVA_PERI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IVA_PERI_IDX,2], .t., this.IVA_PERI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IVA_PERI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VPKEYATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVP',True,'IVA_PERI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VPKEYATT like "+cp_ToStrODBC(trim(this.w_VPKEYATT)+"%");
                   +" and VP__ANNO="+cp_ToStrODBC(this.w_VP__ANNO);
                   +" and VPPERIOD="+cp_ToStrODBC(this.w_VPPERIOD);

          i_ret=cp_SQL(i_nConn,"select *";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VP__ANNO,VPPERIOD,VPKEYATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VP__ANNO',this.w_VP__ANNO;
                     ,'VPPERIOD',this.w_VPPERIOD;
                     ,'VPKEYATT',trim(this.w_VPKEYATT))
          select *;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VP__ANNO,VPPERIOD,VPKEYATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VPKEYATT)==trim(_Link_.VPKEYATT) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VPCODATT like "+cp_ToStrODBC(trim(this.w_VPKEYATT)+"%");
                   +" and VP__ANNO="+cp_ToStrODBC(this.w_VP__ANNO);
                   +" and VPPERIOD="+cp_ToStrODBC(this.w_VPPERIOD);

            i_ret=cp_SQL(i_nConn,"select *";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VPCODATT like "+cp_ToStr(trim(this.w_VPKEYATT)+"%");
                   +" and VP__ANNO="+cp_ToStr(this.w_VP__ANNO);
                   +" and VPPERIOD="+cp_ToStr(this.w_VPPERIOD);

            select *;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_VPKEYATT) and !this.bDontReportError
            deferred_cp_zoom('IVA_PERI','*','VP__ANNO,VPPERIOD,VPKEYATT',cp_AbsName(oSource.parent,'oVPKEYATT_1_4'),i_cWhere,'GSAR_AVP',"Dichiarazioni IVA",'GSCGTSDP.IVA_PERI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_VP__ANNO<>oSource.xKey(1);
           .or. this.w_VPPERIOD<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select *";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select *;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select *";
                     +" from "+i_cTable+" "+i_lTable+" where VPKEYATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and VP__ANNO="+cp_ToStrODBC(this.w_VP__ANNO);
                     +" and VPPERIOD="+cp_ToStrODBC(this.w_VPPERIOD);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VP__ANNO',oSource.xKey(1);
                       ,'VPPERIOD',oSource.xKey(2);
                       ,'VPKEYATT',oSource.xKey(3))
            select *;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VPKEYATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select *";
                   +" from "+i_cTable+" "+i_lTable+" where VPKEYATT="+cp_ToStrODBC(this.w_VPKEYATT);
                   +" and VP__ANNO="+cp_ToStrODBC(this.w_VP__ANNO);
                   +" and VPPERIOD="+cp_ToStrODBC(this.w_VPPERIOD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VP__ANNO',this.w_VP__ANNO;
                       ,'VPPERIOD',this.w_VPPERIOD;
                       ,'VPKEYATT',this.w_VPKEYATT)
            select *;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VPKEYATT = NVL(_Link_.VPKEYATT,space(5))
      this.w_VPCODATT = NVL(_Link_.VPCODATT,space(5))
      this.w_VPCODVAL = NVL(_Link_.VPCODVAL,space(1))
      this.w_VPVARIMP = NVL(_Link_.VPVARIMP,space(1))
      this.w_VPDICGRU = NVL(_Link_.VPDICGRU,space(1))
      this.w_VPDICSOC = NVL(_Link_.VPDICSOC,space(1))
      this.w_VPIMPOR1 = NVL(_Link_.VPIMPOR1,0)
      this.w_VPIMPOR2 = NVL(_Link_.VPIMPOR2,0)
      this.w_VPCESINT = NVL(_Link_.VPCESINT,0)
      this.w_VPACQINT = NVL(_Link_.VPACQINT,0)
      this.w_VPIMPON3 = NVL(_Link_.VPIMPON3,0)
      this.w_VPIMPOS3 = NVL(_Link_.VPIMPOS3,0)
      this.w_VPIMPOR5 = NVL(_Link_.VPIMPOR5,0)
      this.w_VPIMPOR6 = NVL(_Link_.VPIMPOR6,0)
      this.w_VPIMPOR7 = NVL(_Link_.VPIMPOR7,0)
      this.w_VPIMPOR8 = NVL(_Link_.VPIMPOR8,0)
      this.w_VPIMPOR9 = NVL(_Link_.VPIMPOR9,0)
      this.w_VPIMPO10 = NVL(_Link_.VPIMPO10,0)
      this.w_VPIMPO11 = NVL(_Link_.VPIMPO11,0)
      this.w_VPIMPO12 = NVL(_Link_.VPIMPO12,0)
      this.w_VPIMPO13 = NVL(_Link_.VPIMPO13,0)
      this.w_VPIMPO14 = NVL(_Link_.VPIMPO14,0)
      this.w_VPIMPO15 = NVL(_Link_.VPIMPO15,0)
      this.w_VPIMPO16 = NVL(_Link_.VPIMPO16,0)
      this.w_VPDATVER = NVL(cp_ToDate(_Link_.VPDATVER),ctod("  /  /  "))
      this.w_VPCODAZI = NVL(_Link_.VPCODAZI,space(5))
      this.w_VPCODCAB = NVL(_Link_.VPCODCAB,space(5))
      this.w_VPVERNEF = NVL(_Link_.VPVERNEF,space(1))
      this.w_VPAR74C4 = NVL(_Link_.VPAR74C4,space(1))
      this.w_VPOPMEDI = NVL(_Link_.VPOPMEDI,space(1))
      this.w_VPOPNOIM = NVL(_Link_.VPOPNOIM,space(1))
      this.w_VPCRERIM = NVL(_Link_.VPCRERIM,0)
      this.w_VPEURO19 = NVL(_Link_.VPEURO19,space(1))
      this.w_VPCREUTI = NVL(_Link_.VPCREUTI,0)
      this.w_VPCODCAR = NVL(_Link_.VPCODCAR,space(1))
      this.w_VPCODFIS = NVL(_Link_.VPCODFIS,space(16))
      this.w_VPIMPVER = NVL(_Link_.VPIMPVER,0)
      this.w_VPVEREUR = NVL(_Link_.VPVEREUR,space(1))
      this.w_VPDATPRE = NVL(cp_ToDate(_Link_.VPDATPRE),ctod("  /  /  "))
      this.w_VPCORTER = NVL(_Link_.VPCORTER,space(1))
      this.w_VPACQBEA = NVL(_Link_.VPACQBEA,space(1))
      this.w_VPAR74C5 = NVL(_Link_.VPAR74C5,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_VPKEYATT = space(5)
      endif
      this.w_VPCODATT = space(5)
      this.w_VPCODVAL = space(1)
      this.w_VPVARIMP = space(1)
      this.w_VPDICGRU = space(1)
      this.w_VPDICSOC = space(1)
      this.w_VPIMPOR1 = 0
      this.w_VPIMPOR2 = 0
      this.w_VPCESINT = 0
      this.w_VPACQINT = 0
      this.w_VPIMPON3 = 0
      this.w_VPIMPOS3 = 0
      this.w_VPIMPOR5 = 0
      this.w_VPIMPOR6 = 0
      this.w_VPIMPOR7 = 0
      this.w_VPIMPOR8 = 0
      this.w_VPIMPOR9 = 0
      this.w_VPIMPO10 = 0
      this.w_VPIMPO11 = 0
      this.w_VPIMPO12 = 0
      this.w_VPIMPO13 = 0
      this.w_VPIMPO14 = 0
      this.w_VPIMPO15 = 0
      this.w_VPIMPO16 = 0
      this.w_VPDATVER = ctod("  /  /  ")
      this.w_VPCODAZI = space(5)
      this.w_VPCODCAB = space(5)
      this.w_VPVERNEF = space(1)
      this.w_VPAR74C4 = space(1)
      this.w_VPOPMEDI = space(1)
      this.w_VPOPNOIM = space(1)
      this.w_VPCRERIM = 0
      this.w_VPEURO19 = space(1)
      this.w_VPCREUTI = 0
      this.w_VPCODCAR = space(1)
      this.w_VPCODFIS = space(16)
      this.w_VPIMPVER = 0
      this.w_VPVEREUR = space(1)
      this.w_VPDATPRE = ctod("  /  /  ")
      this.w_VPCORTER = space(1)
      this.w_VPACQBEA = space(1)
      this.w_VPAR74C5 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IVA_PERI_IDX,2])+'\'+cp_ToStr(_Link_.VP__ANNO,1)+'\'+cp_ToStr(_Link_.VPPERIOD,1)+'\'+cp_ToStr(_Link_.VPKEYATT,1)
      cp_ShowWarn(i_cKey,this.IVA_PERI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VPKEYATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VPCODATT
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIMAST_IDX,3]
    i_lTable = "ATTIMAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2], .t., this.ATTIMAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VPCODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VPCODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODATT,ATDESATT";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_VPCODATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODATT',this.w_VPCODATT)
            select ATCODATT,ATDESATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VPCODATT = NVL(_Link_.ATCODATT,space(5))
      this.w_DESATT = NVL(_Link_.ATDESATT,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_VPCODATT = space(5)
      endif
      this.w_DESATT = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2])+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIMAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VPCODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VPCODAZI
  func Link_3_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_ABI_IDX,3]
    i_lTable = "COD_ABI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_ABI_IDX,2], .t., this.COD_ABI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_ABI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VPCODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VPCODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ABCODABI,ABDESABI";
                   +" from "+i_cTable+" "+i_lTable+" where ABCODABI="+cp_ToStrODBC(this.w_VPCODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ABCODABI',this.w_VPCODAZI)
            select ABCODABI,ABDESABI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VPCODAZI = NVL(_Link_.ABCODABI,space(5))
      this.w_DESABI = NVL(_Link_.ABDESABI,space(80))
    else
      if i_cCtrl<>'Load'
        this.w_VPCODAZI = space(5)
      endif
      this.w_DESABI = space(80)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_ABI_IDX,2])+'\'+cp_ToStr(_Link_.ABCODABI,1)
      cp_ShowWarn(i_cKey,this.COD_ABI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VPCODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VPCODCAB
  func Link_3_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_CAB_IDX,3]
    i_lTable = "COD_CAB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_CAB_IDX,2], .t., this.COD_CAB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_CAB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VPCODCAB) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VPCODCAB)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FICODABI,FICODCAB,FIDESFIL,FIINDIRI,FI___CAP";
                   +" from "+i_cTable+" "+i_lTable+" where FICODCAB="+cp_ToStrODBC(this.w_VPCODCAB);
                   +" and FICODABI="+cp_ToStrODBC(this.w_VPCODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FICODABI',this.w_VPCODAZI;
                       ,'FICODCAB',this.w_VPCODCAB)
            select FICODABI,FICODCAB,FIDESFIL,FIINDIRI,FI___CAP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VPCODCAB = NVL(_Link_.FICODCAB,space(5))
      this.w_DESFIL = NVL(_Link_.FIDESFIL,space(40))
      this.w_INDIRI = NVL(_Link_.FIINDIRI,space(50))
      this.w_CAP = NVL(_Link_.FI___CAP,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_VPCODCAB = space(5)
      endif
      this.w_DESFIL = space(40)
      this.w_INDIRI = space(50)
      this.w_CAP = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_CAB_IDX,2])+'\'+cp_ToStr(_Link_.FICODABI,1)+'\'+cp_ToStr(_Link_.FICODCAB,1)
      cp_ShowWarn(i_cKey,this.COD_CAB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VPCODCAB Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oVP__ANNO_1_2.value==this.w_VP__ANNO)
      this.oPgFrm.Page1.oPag.oVP__ANNO_1_2.value=this.w_VP__ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oVPPERIOD_1_3.value==this.w_VPPERIOD)
      this.oPgFrm.Page1.oPag.oVPPERIOD_1_3.value=this.w_VPPERIOD
    endif
    if not(this.oPgFrm.Page1.oPag.oVPKEYATT_1_4.value==this.w_VPKEYATT)
      this.oPgFrm.Page1.oPag.oVPKEYATT_1_4.value=this.w_VPKEYATT
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPVAL_1_9.value==this.w_IMPVAL)
      this.oPgFrm.Page1.oPag.oIMPVAL_1_9.value=this.w_IMPVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oVPVARIMP_1_10.RadioValue()==this.w_VPVARIMP)
      this.oPgFrm.Page1.oPag.oVPVARIMP_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVPCORTER_1_11.RadioValue()==this.w_VPCORTER)
      this.oPgFrm.Page1.oPag.oVPCORTER_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRAGAZI_1_13.value==this.w_RAGAZI)
      this.oPgFrm.Page1.oPag.oRAGAZI_1_13.value=this.w_RAGAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPDIC_1_14.RadioValue()==this.w_TIPDIC)
      this.oPgFrm.Page1.oPag.oTIPDIC_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVISPDF_1_20.RadioValue()==this.w_VISPDF)
      this.oPgFrm.Page1.oPag.oVISPDF_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oVPIMPOR1_2_1.value==this.w_VPIMPOR1)
      this.oPgFrm.Page2.oPag.oVPIMPOR1_2_1.value=this.w_VPIMPOR1
    endif
    if not(this.oPgFrm.Page2.oPag.oVPCESINT_2_2.value==this.w_VPCESINT)
      this.oPgFrm.Page2.oPag.oVPCESINT_2_2.value=this.w_VPCESINT
    endif
    if not(this.oPgFrm.Page2.oPag.oVPIMPOR2_2_3.value==this.w_VPIMPOR2)
      this.oPgFrm.Page2.oPag.oVPIMPOR2_2_3.value=this.w_VPIMPOR2
    endif
    if not(this.oPgFrm.Page2.oPag.oVPACQINT_2_4.value==this.w_VPACQINT)
      this.oPgFrm.Page2.oPag.oVPACQINT_2_4.value=this.w_VPACQINT
    endif
    if not(this.oPgFrm.Page2.oPag.oVPIMPON3_2_5.value==this.w_VPIMPON3)
      this.oPgFrm.Page2.oPag.oVPIMPON3_2_5.value=this.w_VPIMPON3
    endif
    if not(this.oPgFrm.Page2.oPag.oVPIMPOS3_2_7.value==this.w_VPIMPOS3)
      this.oPgFrm.Page2.oPag.oVPIMPOS3_2_7.value=this.w_VPIMPOS3
    endif
    if not(this.oPgFrm.Page2.oPag.oVPIMPOR5_2_8.value==this.w_VPIMPOR5)
      this.oPgFrm.Page2.oPag.oVPIMPOR5_2_8.value=this.w_VPIMPOR5
    endif
    if not(this.oPgFrm.Page2.oPag.oVPIMPOR6_2_9.value==this.w_VPIMPOR6)
      this.oPgFrm.Page2.oPag.oVPIMPOR6_2_9.value=this.w_VPIMPOR6
    endif
    if not(this.oPgFrm.Page2.oPag.oIMPDEB7_2_16.value==this.w_IMPDEB7)
      this.oPgFrm.Page2.oPag.oIMPDEB7_2_16.value=this.w_IMPDEB7
    endif
    if not(this.oPgFrm.Page2.oPag.oIMPCRE7_2_18.value==this.w_IMPCRE7)
      this.oPgFrm.Page2.oPag.oIMPCRE7_2_18.value=this.w_IMPCRE7
    endif
    if not(this.oPgFrm.Page2.oPag.oIMPDEB8_2_19.value==this.w_IMPDEB8)
      this.oPgFrm.Page2.oPag.oIMPDEB8_2_19.value=this.w_IMPDEB8
    endif
    if not(this.oPgFrm.Page2.oPag.oIMPCRE8_2_20.value==this.w_IMPCRE8)
      this.oPgFrm.Page2.oPag.oIMPCRE8_2_20.value=this.w_IMPCRE8
    endif
    if not(this.oPgFrm.Page2.oPag.oIMPDEB10_2_22.value==this.w_IMPDEB10)
      this.oPgFrm.Page2.oPag.oIMPDEB10_2_22.value=this.w_IMPDEB10
    endif
    if not(this.oPgFrm.Page2.oPag.oIMPCRE10_2_24.value==this.w_IMPCRE10)
      this.oPgFrm.Page2.oPag.oIMPCRE10_2_24.value=this.w_IMPCRE10
    endif
    if not(this.oPgFrm.Page2.oPag.oVPIMPO11_2_26.value==this.w_VPIMPO11)
      this.oPgFrm.Page2.oPag.oVPIMPO11_2_26.value=this.w_VPIMPO11
    endif
    if not(this.oPgFrm.Page2.oPag.oVPIMPO13_2_28.value==this.w_VPIMPO13)
      this.oPgFrm.Page2.oPag.oVPIMPO13_2_28.value=this.w_VPIMPO13
    endif
    if not(this.oPgFrm.Page2.oPag.oIMPDEB12_2_29.value==this.w_IMPDEB12)
      this.oPgFrm.Page2.oPag.oIMPDEB12_2_29.value=this.w_IMPDEB12
    endif
    if not(this.oPgFrm.Page2.oPag.oIMPCRE12_2_30.value==this.w_IMPCRE12)
      this.oPgFrm.Page2.oPag.oIMPCRE12_2_30.value=this.w_IMPCRE12
    endif
    if not(this.oPgFrm.Page2.oPag.oVPIMPO14_2_33.value==this.w_VPIMPO14)
      this.oPgFrm.Page2.oPag.oVPIMPO14_2_33.value=this.w_VPIMPO14
    endif
    if not(this.oPgFrm.Page2.oPag.oVPIMPO16_2_34.value==this.w_VPIMPO16)
      this.oPgFrm.Page2.oPag.oVPIMPO16_2_34.value=this.w_VPIMPO16
    endif
    if not(this.oPgFrm.Page2.oPag.oVPIMPO15_2_35.value==this.w_VPIMPO15)
      this.oPgFrm.Page2.oPag.oVPIMPO15_2_35.value=this.w_VPIMPO15
    endif
    if not(this.oPgFrm.Page3.oPag.oVPVERNEF_3_1.RadioValue()==this.w_VPVERNEF)
      this.oPgFrm.Page3.oPag.oVPVERNEF_3_1.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oVPAR74C5_3_2.RadioValue()==this.w_VPAR74C5)
      this.oPgFrm.Page3.oPag.oVPAR74C5_3_2.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oVPAR74C4_3_4.RadioValue()==this.w_VPAR74C4)
      this.oPgFrm.Page3.oPag.oVPAR74C4_3_4.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oVPOPMEDI_3_7.RadioValue()==this.w_VPOPMEDI)
      this.oPgFrm.Page3.oPag.oVPOPMEDI_3_7.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oVPOPNOIM_3_8.RadioValue()==this.w_VPOPNOIM)
      this.oPgFrm.Page3.oPag.oVPOPNOIM_3_8.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oVPCRERIM_3_9.value==this.w_VPCRERIM)
      this.oPgFrm.Page3.oPag.oVPCRERIM_3_9.value=this.w_VPCRERIM
    endif
    if not(this.oPgFrm.Page3.oPag.oVPEURO19_3_11.RadioValue()==this.w_VPEURO19)
      this.oPgFrm.Page3.oPag.oVPEURO19_3_11.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oVPCREUTI_3_12.value==this.w_VPCREUTI)
      this.oPgFrm.Page3.oPag.oVPCREUTI_3_12.value=this.w_VPCREUTI
    endif
    if not(this.oPgFrm.Page3.oPag.oVPCODCAR_3_13.RadioValue()==this.w_VPCODCAR)
      this.oPgFrm.Page3.oPag.oVPCODCAR_3_13.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oVPCODFIS_3_15.value==this.w_VPCODFIS)
      this.oPgFrm.Page3.oPag.oVPCODFIS_3_15.value=this.w_VPCODFIS
    endif
    if not(this.oPgFrm.Page3.oPag.oVPDATVER_3_20.value==this.w_VPDATVER)
      this.oPgFrm.Page3.oPag.oVPDATVER_3_20.value=this.w_VPDATVER
    endif
    if not(this.oPgFrm.Page3.oPag.oVPCODAZI_3_21.value==this.w_VPCODAZI)
      this.oPgFrm.Page3.oPag.oVPCODAZI_3_21.value=this.w_VPCODAZI
    endif
    if not(this.oPgFrm.Page3.oPag.oVPCODCAB_3_22.value==this.w_VPCODCAB)
      this.oPgFrm.Page3.oPag.oVPCODCAB_3_22.value=this.w_VPCODCAB
    endif
    if not(this.oPgFrm.Page1.oPag.oDESATT_1_29.value==this.w_DESATT)
      this.oPgFrm.Page1.oPag.oDESATT_1_29.value=this.w_DESATT
    endif
    if not(this.oPgFrm.Page3.oPag.oVPDATPRE_3_29.value==this.w_VPDATPRE)
      this.oPgFrm.Page3.oPag.oVPDATPRE_3_29.value=this.w_VPDATPRE
    endif
    if not(this.oPgFrm.Page3.oPag.oVPACQBEA_3_31.RadioValue()==this.w_VPACQBEA)
      this.oPgFrm.Page3.oPag.oVPACQBEA_3_31.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oIMPDEB9_2_70.value==this.w_IMPDEB9)
      this.oPgFrm.Page2.oPag.oIMPDEB9_2_70.value=this.w_IMPDEB9
    endif
    if not(this.oPgFrm.Page2.oPag.oIMPCRE9_2_71.value==this.w_IMPCRE9)
      this.oPgFrm.Page2.oPag.oIMPCRE9_2_71.value=this.w_IMPCRE9
    endif
    if not(this.oPgFrm.Page3.oPag.oVPIMPVER_3_32.value==this.w_VPIMPVER)
      this.oPgFrm.Page3.oPag.oVPIMPVER_3_32.value=this.w_VPIMPVER
    endif
    if not(this.oPgFrm.Page3.oPag.oVPVEREUR_3_33.RadioValue()==this.w_VPVEREUR)
      this.oPgFrm.Page3.oPag.oVPVEREUR_3_33.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLTEST_1_34.RadioValue()==this.w_FLTEST)
      this.oPgFrm.Page1.oPag.oFLTEST_1_34.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_VPKEYATT))  and (NOT EMPTY(.w_VP__ANNO) AND NOT EMPTY(.w_VPPERIOD))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oVPKEYATT_1_4.SetFocus()
            i_bnoObbl = !empty(.w_VPKEYATT)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_VPCODCAR))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oVPCODCAR_3_13.SetFocus()
            i_bnoObbl = !empty(.w_VPCODCAR)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgscg_sdpPag1 as StdContainer
  Width  = 661
  height = 411
  stdWidth  = 661
  stdheight = 411
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

    add object oBmp_1_7 as image with uid="ZBDKYRRVNV",left=20, top=21, width=15,height=15,;
      Picture="BMP\IVAMODP1.BMP"

    add object oBmp_1_22 as image with uid="MASLBFXFFJ",left=20, top=20, width=151,height=132,;
      Picture="BMP\IVAMODP1.BMP"

  add object oVP__ANNO_1_2 as StdField with uid="HOWSQVPBBL",rtseq=2,rtrep=.f.,;
    cFormVar = "w_VP__ANNO", cQueryName = "VP__ANNO",nZero=4,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Anno dichiarazione non valido",;
    HelpContextID = 32825179,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=248, Top=70, cSayPict="'9999'", cGetPict="'9999'", InputMask=replicate('X',4)

  func oVP__ANNO_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_VPPERIOD)
        bRes2=.link_1_3('Full')
      endif
      if .not. empty(.w_VPKEYATT)
        bRes2=.link_1_4('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oVPPERIOD_1_3 as StdField with uid="LRAJJJZUNE",rtseq=3,rtrep=.f.,;
    cFormVar = "w_VPPERIOD", cQueryName = "VPPERIOD",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Periodo dichiarazione non valido",;
    HelpContextID = 100650854,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=371, Top=70, cSayPict='"99"', cGetPict='"99"', bHasZoom = .t. , cLinkFile="IVA_PERI", cZoomOnZoom="GSAR_AVP", oKey_1_1="VP__ANNO", oKey_1_2="this.w_VP__ANNO", oKey_2_1="VPPERIOD", oKey_2_2="this.w_VPPERIOD"

  func oVPPERIOD_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_VP__ANNO))
    endwith
   endif
  endfunc

  func oVPPERIOD_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
      if .not. empty(.w_VPKEYATT)
        bRes2=.link_1_4('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oVPPERIOD_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVPPERIOD_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.IVA_PERI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"VP__ANNO="+cp_ToStrODBC(this.Parent.oContained.w_VP__ANNO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"VP__ANNO="+cp_ToStr(this.Parent.oContained.w_VP__ANNO)
    endif
    do cp_zoom with 'IVA_PERI','*','VP__ANNO,VPPERIOD',cp_AbsName(this.parent,'oVPPERIOD_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVP',"Dichiarazioni IVA",'GSCGPSDP.IVA_PERI_VZM',this.parent.oContained
  endproc
  proc oVPPERIOD_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.VP__ANNO=w_VP__ANNO
     i_obj.w_VPPERIOD=this.parent.oContained.w_VPPERIOD
     i_obj.ecpSave()
  endproc

  add object oVPKEYATT_1_4 as StdField with uid="OSXCEGUCZE",rtseq=4,rtrep=.f.,;
    cFormVar = "w_VPKEYATT", cQueryName = "VPKEYATT",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 227549014,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=470, Top=70, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="IVA_PERI", cZoomOnZoom="GSAR_AVP", oKey_1_1="VP__ANNO", oKey_1_2="this.w_VP__ANNO", oKey_2_1="VPPERIOD", oKey_2_2="this.w_VPPERIOD", oKey_3_1="VPKEYATT", oKey_3_2="this.w_VPKEYATT"

  func oVPKEYATT_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_VP__ANNO) AND NOT EMPTY(.w_VPPERIOD))
    endwith
   endif
  endfunc

  func oVPKEYATT_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oVPKEYATT_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVPKEYATT_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.IVA_PERI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"VP__ANNO="+cp_ToStrODBC(this.Parent.oContained.w_VP__ANNO)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"VPPERIOD="+cp_ToStrODBC(this.Parent.oContained.w_VPPERIOD)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"VP__ANNO="+cp_ToStr(this.Parent.oContained.w_VP__ANNO)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"VPPERIOD="+cp_ToStr(this.Parent.oContained.w_VPPERIOD)
    endif
    do cp_zoom with 'IVA_PERI','*','VP__ANNO,VPPERIOD,VPKEYATT',cp_AbsName(this.parent,'oVPKEYATT_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVP',"Dichiarazioni IVA",'GSCGTSDP.IVA_PERI_VZM',this.parent.oContained
  endproc
  proc oVPKEYATT_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.VP__ANNO=w_VP__ANNO
    i_obj.VPPERIOD=w_VPPERIOD
     i_obj.w_VPKEYATT=this.parent.oContained.w_VPKEYATT
     i_obj.ecpSave()
  endproc

  add object oIMPVAL_1_9 as StdField with uid="MLLCKXSKHE",rtseq=7,rtrep=.f.,;
    cFormVar = "w_IMPVAL", cQueryName = "IMPVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Valuta importi",;
    HelpContextID = 67031930,;
    FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=37, Left=576, Top=169, InputMask=replicate('X',3)

  add object oVPVARIMP_1_10 as StdCheck with uid="FJPHMUBUFQ",rtseq=8,rtrep=.f.,left=18, top=209, caption="Nel periodo sono comprese variazioni di imponibile relative a periodi precedenti", enabled=.f.,;
    ToolTipText = "Variazione di imponibile periodi precedenti comprese nel periodo",;
    HelpContextID = 100888410,;
    cFormVar="w_VPVARIMP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oVPVARIMP_1_10.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oVPVARIMP_1_10.GetRadio()
    this.Parent.oContained.w_VPVARIMP = this.RadioValue()
    return .t.
  endfunc

  func oVPVARIMP_1_10.SetRadio()
    this.Parent.oContained.w_VPVARIMP=trim(this.Parent.oContained.w_VPVARIMP)
    this.value = ;
      iif(this.Parent.oContained.w_VPVARIMP=='S',1,;
      0)
  endfunc

  add object oVPCORTER_1_11 as StdCheck with uid="WGLLEOVLFL",rtseq=9,rtrep=.f.,left=18, top=229, caption="Correttiva nei termini", enabled=.f.,;
    ToolTipText = "Se attivo: dichiarazione correttiva nei termini",;
    HelpContextID = 84500648,;
    cFormVar="w_VPCORTER", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oVPCORTER_1_11.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oVPCORTER_1_11.GetRadio()
    this.Parent.oContained.w_VPCORTER = this.RadioValue()
    return .t.
  endfunc

  func oVPCORTER_1_11.SetRadio()
    this.Parent.oContained.w_VPCORTER=trim(this.Parent.oContained.w_VPCORTER)
    this.value = ;
      iif(this.Parent.oContained.w_VPCORTER=='S',1,;
      0)
  endfunc

  add object oRAGAZI_1_13 as StdField with uid="IWEYEBGERD",rtseq=10,rtrep=.f.,;
    cFormVar = "w_RAGAZI", cQueryName = "RAGAZI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Ragione sociale",;
    HelpContextID = 92565226,;
   bGlobalFont=.t.,;
    Height=21, Width=404, Left=209, Top=33, InputMask=replicate('X',30)


  add object oTIPDIC_1_14 as StdCombo with uid="RMBIZGHRWF",rtseq=11,rtrep=.f.,left=209,top=169,width=245,height=21, enabled=.f.;
    , HelpContextID = 210818762;
    , cFormVar="w_TIPDIC",RowSource=""+"Dichiarazione del gruppo,"+"Dichiarazione societ� aderente al gruppo,"+"Normale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPDIC_1_14.RadioValue()
    return(iif(this.value =1,'G',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oTIPDIC_1_14.GetRadio()
    this.Parent.oContained.w_TIPDIC = this.RadioValue()
    return .t.
  endfunc

  func oTIPDIC_1_14.SetRadio()
    this.Parent.oContained.w_TIPDIC=trim(this.Parent.oContained.w_TIPDIC)
    this.value = ;
      iif(this.Parent.oContained.w_TIPDIC=='G',1,;
      iif(this.Parent.oContained.w_TIPDIC=='S',2,;
      iif(this.Parent.oContained.w_TIPDIC=='N',3,;
      0)))
  endfunc

  add object oVISPDF_1_20 as StdCheck with uid="XTCQFDSZVP",rtseq=15,rtrep=.f.,left=441, top=338, caption="Visualizza modulo PDF",;
    ToolTipText = "Se attivo: carica il modulo della dichiarazione in Acrobat Reader",;
    HelpContextID = 164931242,;
    cFormVar="w_VISPDF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oVISPDF_1_20.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oVISPDF_1_20.GetRadio()
    this.Parent.oContained.w_VISPDF = this.RadioValue()
    return .t.
  endfunc

  func oVISPDF_1_20.SetRadio()
    this.Parent.oContained.w_VISPDF=trim(this.Parent.oContained.w_VISPDF)
    this.value = ;
      iif(this.Parent.oContained.w_VISPDF=='S',1,;
      0)
  endfunc

  func oVISPDF_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (VAL(.w_VP__ANNO)<2002)
    endwith
   endif
  endfunc

  func oVISPDF_1_20.mHide()
    with this.Parent.oContained
      return (.w_FLTEST='S' OR NOT (.w_TIPLIQ='R' OR g_ATTIVI<>'S'))
    endwith
  endfunc


  add object oBtn_1_21 as StdButton with uid="IZQXXFWYNR",left=558, top=363, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la stampa";
    , HelpContextID = 194398950;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      with this.Parent.oContained
        GSCG_BDV(this.Parent.oContained,"S")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_21.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (VAL(.w_VP__ANNO)<2002 AND NOT EMPTY(.w_VP__ANNO) AND NOT EMPTY(.w_VPKEYATT) AND .w_VPPERIOD<>0)
      endwith
    endif
  endfunc


  add object oBtn_1_26 as StdButton with uid="RAHSNYQPUF",left=609, top=363, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 201687622;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_26.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESATT_1_29 as StdField with uid="RCKAFREUAB",rtseq=60,rtrep=.f.,;
    cFormVar = "w_DESATT", cQueryName = "DESATT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 182692810,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=246, Top=106, InputMask=replicate('X',35)

  func oDESATT_1_29.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_VPCODATT))
    endwith
  endfunc

  add object oFLTEST_1_34 as StdCheck with uid="DXYVVIOBTQ",rtseq=67,rtrep=.f.,left=441, top=308, caption="Stampa solo testo",;
    ToolTipText = "Se attivo: stampa su modulo solo testo",;
    HelpContextID = 183473322,;
    cFormVar="w_FLTEST", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLTEST_1_34.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFLTEST_1_34.GetRadio()
    this.Parent.oContained.w_FLTEST = this.RadioValue()
    return .t.
  endfunc

  func oFLTEST_1_34.SetRadio()
    this.Parent.oContained.w_FLTEST=trim(this.Parent.oContained.w_FLTEST)
    this.value = ;
      iif(this.Parent.oContained.w_FLTEST=='S',1,;
      0)
  endfunc

  func oFLTEST_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (VAL(.w_VP__ANNO)<2002)
    endwith
   endif
  endfunc

  add object oStr_1_8 as StdString with uid="POSOMVUTNF",Visible=.t., Left=209, Top=146,;
    Alignment=0, Width=292, Height=15,;
    Caption="Dati per liquidazione IVA di gruppo (art. 73)"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_15 as StdString with uid="DMSHUJVYVE",Visible=.t., Left=209, Top=18,;
    Alignment=0, Width=403, Height=15,;
    Caption="Ragione sociale"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="LDJNYCVEGY",Visible=.t., Left=494, Top=169,;
    Alignment=1, Width=80, Height=15,;
    Caption="Importi in:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="CTWOVRUHAI",Visible=.t., Left=540, Top=70,;
    Alignment=0, Width=84, Height=18,;
    Caption="Riepilogo"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_30.mHide()
    with this.Parent.oContained
      return (NOT EMPTY(.w_VPCODATT))
    endwith
  endfunc

  add object oStr_1_31 as StdString with uid="SPMPNAUDYV",Visible=.t., Left=194, Top=70,;
    Alignment=1, Width=50, Height=18,;
    Caption="Anno:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_32 as StdString with uid="QDUWBYZYSI",Visible=.t., Left=311, Top=70,;
    Alignment=1, Width=58, Height=18,;
    Caption="Periodo:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_33 as StdString with uid="RYUIUGJEOD",Visible=.t., Left=412, Top=70,;
    Alignment=1, Width=56, Height=18,;
    Caption="Attivit�:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_12 as StdBox with uid="VRUZJCLLYJ",left=17, top=17, width=157,height=139
enddefine
define class tgscg_sdpPag2 as StdContainer
  Width  = 661
  height = 411
  stdWidth  = 661
  stdheight = 411
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oVPIMPOR1_2_1 as StdField with uid="PWGJRUTEBA",rtseq=16,rtrep=.f.,;
    cFormVar = "w_VPIMPOR1", cQueryName = "VPIMPOR1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Operazioni attive al netto d'IVA ("+g_perval+")",;
    HelpContextID = 1589113,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=141, Top=16, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  add object oVPCESINT_2_2 as StdField with uid="RUJEQTOWVS",rtseq=17,rtrep=.f.,;
    cFormVar = "w_VPCESINT", cQueryName = "VPCESINT",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "di cui cessioni intracomunitarie",;
    HelpContextID = 99655510,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=480, Top=16, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  add object oVPIMPOR2_2_3 as StdField with uid="DLPHQJDUHJ",rtseq=18,rtrep=.f.,;
    cFormVar = "w_VPIMPOR2", cQueryName = "VPIMPOR2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Operazioni passive al netto d'IVA ("+g_perval+")",;
    HelpContextID = 1589112,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=141, Top=36, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  add object oVPACQINT_2_4 as StdField with uid="FIDKOZWXUA",rtseq=19,rtrep=.f.,;
    cFormVar = "w_VPACQINT", cQueryName = "VPACQINT",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "di cui acquisti intracomunitari",;
    HelpContextID = 101891926,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=480, Top=36, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  add object oVPIMPON3_2_5 as StdField with uid="YFMPTYGEES",rtseq=20,rtrep=.f.,;
    cFormVar = "w_VPIMPON3", cQueryName = "VPIMPON3",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Imponibile",;
    HelpContextID = 1589111,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=141, Top=72, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  add object oVPIMPOS3_2_7 as StdField with uid="TVNXZIVOQY",rtseq=21,rtrep=.f.,;
    cFormVar = "w_VPIMPOS3", cQueryName = "VPIMPOS3",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Imposta",;
    HelpContextID = 1589111,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=480, Top=72, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  add object oVPIMPOR5_2_8 as StdField with uid="BPFPTNQWLD",rtseq=22,rtrep=.f.,;
    cFormVar = "w_VPIMPOR5", cQueryName = "VPIMPOR5",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "IVA esigibile per il periodo",;
    HelpContextID = 1589109,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=296, Top=125, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  add object oVPIMPOR6_2_9 as StdField with uid="IKIABJLIWZ",rtseq=23,rtrep=.f.,;
    cFormVar = "w_VPIMPOR6", cQueryName = "VPIMPOR6",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "IVA che si detrae per il periodo",;
    HelpContextID = 1589108,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=480, Top=145, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  add object oIMPDEB7_2_16 as StdField with uid="VNNVAAYEFZ",rtseq=28,rtrep=.f.,;
    cFormVar = "w_IMPDEB7", cQueryName = "IMPDEB7",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "IVA a debito per il periodo",;
    HelpContextID = 36646022,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=296, Top=165, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  add object oIMPCRE7_2_18 as StdField with uid="VHTJHIPKLS",rtseq=29,rtrep=.f.,;
    cFormVar = "w_IMPCRE7", cQueryName = "IMPCRE7",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "IVA a credito per il periodo",;
    HelpContextID = 100543622,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=480, Top=165, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  func oIMPCRE7_2_18.mHide()
    with this.Parent.oContained
      return (.w_VPIMPOR7<0)
    endwith
  endfunc

  add object oIMPDEB8_2_19 as StdField with uid="TQPHOQULBE",rtseq=30,rtrep=.f.,;
    cFormVar = "w_IMPDEB8", cQueryName = "IMPDEB8",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Variazioni d'imposta",;
    HelpContextID = 36646022,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=296, Top=185, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  add object oIMPCRE8_2_20 as StdField with uid="XSQHIYKMVO",rtseq=31,rtrep=.f.,;
    cFormVar = "w_IMPCRE8", cQueryName = "IMPCRE8",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Variazioni d'imposta",;
    HelpContextID = 100543622,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=480, Top=185, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  add object oIMPDEB10_2_22 as StdField with uid="UGFEJRVVWQ",rtseq=33,rtrep=.f.,;
    cFormVar = "w_IMPDEB10", cQueryName = "IMPDEB10",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Debito non superiore al versamento minimo",;
    HelpContextID = 231789386,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=296, Top=226, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  add object oIMPCRE10_2_24 as StdField with uid="CLOTIYEQFI",rtseq=34,rtrep=.f.,;
    cFormVar = "w_IMPCRE10", cQueryName = "IMPCRE10",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo a credito",;
    HelpContextID = 167891786,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=480, Top=226, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  add object oVPIMPO11_2_26 as StdField with uid="FJRNSZCIOV",rtseq=35,rtrep=.f.,;
    cFormVar = "w_VPIMPO11", cQueryName = "VPIMPO11",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Credito IVA compensabile",;
    HelpContextID = 1589113,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=480, Top=266, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  add object oVPIMPO13_2_28 as StdField with uid="GXKNDIBOCO",rtseq=36,rtrep=.f.,;
    cFormVar = "w_VPIMPO13", cQueryName = "VPIMPO13",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Valore riferito ai crediti d'imposta utilizzati ("+g_perval+")",;
    HelpContextID = 1589111,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=480, Top=307, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  add object oIMPDEB12_2_29 as StdField with uid="WTKFEMECVL",rtseq=37,rtrep=.f.,;
    cFormVar = "w_IMPDEB12", cQueryName = "IMPDEB12",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 231789384,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=296, Top=286, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  add object oIMPCRE12_2_30 as StdField with uid="NBTUUUEIPW",rtseq=38,rtrep=.f.,;
    cFormVar = "w_IMPCRE12", cQueryName = "IMPCRE12",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 167891784,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=480, Top=286, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  add object oVPIMPO14_2_33 as StdField with uid="SUIFFRRTBK",rtseq=39,rtrep=.f.,;
    cFormVar = "w_VPIMPO14", cQueryName = "VPIMPO14",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Interessi dovuti per liquidazioni trimestrali",;
    HelpContextID = 1589110,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=296, Top=326, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  add object oVPIMPO16_2_34 as StdField with uid="RUGMYFUATR",rtseq=40,rtrep=.f.,;
    cFormVar = "w_VPIMPO16", cQueryName = "VPIMPO16",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo da versare",;
    HelpContextID = 1589108,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=296, Top=382, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  add object oVPIMPO15_2_35 as StdField with uid="OQWHXQQIXD",rtseq=41,rtrep=.f.,;
    cFormVar = "w_VPIMPO15", cQueryName = "VPIMPO15",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Acconto versato",;
    HelpContextID = 1589109,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=480, Top=346, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  add object oIMPDEB9_2_70 as StdField with uid="SATVCEOLAA",rtseq=63,rtrep=.f.,;
    cFormVar = "w_IMPDEB9", cQueryName = "IMPDEB9",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Variazioni d'imposta",;
    HelpContextID = 36646022,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=296, Top=205, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  add object oIMPCRE9_2_71 as StdField with uid="IRTSPZBQVT",rtseq=64,rtrep=.f.,;
    cFormVar = "w_IMPCRE9", cQueryName = "IMPCRE9",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Variazioni d'imposta",;
    HelpContextID = 100543622,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=480, Top=205, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  add object oStr_2_6 as StdString with uid="AIFVGQYSYL",Visible=.t., Left=5, Top=74,;
    Alignment=0, Width=110, Height=15,;
    Caption="VP3 - imponibile"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_14 as StdString with uid="VBXJAOIANY",Visible=.t., Left=298, Top=100,;
    Alignment=0, Width=161, Height=15,;
    Caption="DEBITI"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_15 as StdString with uid="QCQJDMYKZI",Visible=.t., Left=480, Top=99,;
    Alignment=0, Width=164, Height=15,;
    Caption="CREDITI"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_17 as StdString with uid="QJKXRRDRWU",Visible=.t., Left=5, Top=164,;
    Alignment=0, Width=228, Height=18,;
    Caption="VP12 - debito o credito per il periodo"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_23 as StdString with uid="GSMHRMIGQE",Visible=.t., Left=5, Top=225,;
    Alignment=0, Width=212, Height=18,;
    Caption="VP15 - debito/credito riport."  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_25 as StdString with uid="BEBRHVUYQZ",Visible=.t., Left=5, Top=285,;
    Alignment=0, Width=252, Height=18,;
    Caption="VP17 - IVA dovuta/credito per il per."  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_27 as StdString with uid="BOKFQYQGAP",Visible=.t., Left=5, Top=305,;
    Alignment=0, Width=212, Height=18,;
    Caption="VP18 - crediti speciali"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_31 as StdString with uid="FAHMJDTXHA",Visible=.t., Left=5, Top=325,;
    Alignment=0, Width=212, Height=18,;
    Caption="VP19 - interessi dovuti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_32 as StdString with uid="SIBNRUIVXN",Visible=.t., Left=5, Top=345,;
    Alignment=0, Width=212, Height=18,;
    Caption="VP20 - acconto versato"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_36 as StdString with uid="VGEBENLDMD",Visible=.t., Left=5, Top=382,;
    Alignment=0, Width=212, Height=18,;
    Caption="VP21 - importo da versare"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_37 as StdString with uid="QUFBEELTCM",Visible=.t., Left=293, Top=18,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_37.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_2_38 as StdString with uid="SLYSGBCRZN",Visible=.t., Left=293, Top=38,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_38.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_2_39 as StdString with uid="IRPSGRJDNR",Visible=.t., Left=293, Top=74,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_39.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_2_40 as StdString with uid="YDFBALPHJA",Visible=.t., Left=447, Top=127,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_40.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_2_41 as StdString with uid="TWWKVTPFFL",Visible=.t., Left=631, Top=18,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_41.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_2_42 as StdString with uid="RJREOCCRXO",Visible=.t., Left=631, Top=38,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_42.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_2_43 as StdString with uid="LFXLJUZXPM",Visible=.t., Left=631, Top=74,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_43.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_2_44 as StdString with uid="CFEBIRNLNP",Visible=.t., Left=631, Top=187,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_44.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_2_45 as StdString with uid="HBISYGKVQG",Visible=.t., Left=447, Top=167,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_45.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_2_46 as StdString with uid="KLEZAEXGRV",Visible=.t., Left=447, Top=187,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_46.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_2_47 as StdString with uid="SNEOSXNZZZ",Visible=.t., Left=447, Top=228,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_47.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_2_48 as StdString with uid="PEIHVFZMQL",Visible=.t., Left=447, Top=288,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_48.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_2_49 as StdString with uid="ZQGYYRGPDB",Visible=.t., Left=447, Top=328,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_49.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_2_50 as StdString with uid="TJIQNSBAST",Visible=.t., Left=631, Top=147,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_50.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_2_51 as StdString with uid="NERWZVAOZI",Visible=.t., Left=631, Top=167,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_51.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_2_52 as StdString with uid="IIRDVIPUXU",Visible=.t., Left=631, Top=228,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_52.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_2_53 as StdString with uid="FPBSETLVAU",Visible=.t., Left=631, Top=268,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_53.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_2_54 as StdString with uid="GPTLJXYSAC",Visible=.t., Left=631, Top=288,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_54.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_2_55 as StdString with uid="YQGGDYLPVV",Visible=.t., Left=631, Top=308,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_55.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_2_56 as StdString with uid="NCOLSKIHQH",Visible=.t., Left=631, Top=348,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_56.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_2_57 as StdString with uid="BSMDDTABLC",Visible=.t., Left=447, Top=384,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_57.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_2_59 as StdString with uid="HVLCAKZUMD",Visible=.t., Left=323, Top=74,;
    Alignment=0, Width=111, Height=15,;
    Caption="Imposta"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_61 as StdString with uid="AALXTHKZDQ",Visible=.t., Left=5, Top=16,;
    Alignment=0, Width=122, Height=18,;
    Caption="VP1 - op.attive"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_62 as StdString with uid="MIJLJFSSNE",Visible=.t., Left=5, Top=36,;
    Alignment=0, Width=133, Height=18,;
    Caption="VP2 - op.passive"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_63 as StdString with uid="MGQTJJVPEW",Visible=.t., Left=323, Top=36,;
    Alignment=0, Width=141, Height=18,;
    Caption="di cui acquisti INTRA"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_64 as StdString with uid="HOBSIWBZVS",Visible=.t., Left=323, Top=16,;
    Alignment=0, Width=169, Height=18,;
    Caption="di cui cessioni INTRA"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_65 as StdString with uid="ZEATCXYZXS",Visible=.t., Left=5, Top=125,;
    Alignment=0, Width=212, Height=18,;
    Caption="VP10 - IVA esigibile per il periodo"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_66 as StdString with uid="TAAZPSOVVB",Visible=.t., Left=5, Top=145,;
    Alignment=0, Width=252, Height=18,;
    Caption="VP11 - IVA che si detrae per il periodo"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_67 as StdString with uid="QVZHDRADFN",Visible=.t., Left=5, Top=185,;
    Alignment=0, Width=212, Height=18,;
    Caption="VP13 - variazioni di imposta"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_68 as StdString with uid="DMMDSLBJOM",Visible=.t., Left=5, Top=205,;
    Alignment=0, Width=251, Height=18,;
    Caption="VP14 - IVA non versata/in eccesso"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_69 as StdString with uid="PLCBSFSCED",Visible=.t., Left=5, Top=266,;
    Alignment=0, Width=212, Height=18,;
    Caption="VP16 - credito IVA compensabile"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_72 as StdString with uid="EWLMLRTXQK",Visible=.t., Left=631, Top=205,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_72.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_2_73 as StdString with uid="VMMBAUTATD",Visible=.t., Left=447, Top=205,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_73.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oBox_2_58 as StdBox with uid="INGBLIRNHF",left=2, top=374, width=657,height=2

  add object oBox_2_60 as StdBox with uid="HAPQCQFGKX",left=2, top=117, width=657,height=2
enddefine
define class tgscg_sdpPag3 as StdContainer
  Width  = 661
  height = 411
  stdWidth  = 661
  stdheight = 411
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oVPVERNEF_3_1 as StdCheck with uid="GAODJIVVMR",rtseq=42,rtrep=.f.,left=52, top=95, caption="Versamento non effettuato a seguito di agevolazioni per eventi eccezionali", enabled=.f.,;
    HelpContextID = 251695260,;
    cFormVar="w_VPVERNEF", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oVPVERNEF_3_1.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oVPVERNEF_3_1.GetRadio()
    this.Parent.oContained.w_VPVERNEF = this.RadioValue()
    return .t.
  endfunc

  func oVPVERNEF_3_1.SetRadio()
    this.Parent.oContained.w_VPVERNEF=trim(this.Parent.oContained.w_VPVERNEF)
    this.value = ;
      iif(this.Parent.oContained.w_VPVERNEF=='S',1,;
      0)
  endfunc

  add object oVPAR74C5_3_2 as StdCheck with uid="UATPFQYAGN",rtseq=43,rtrep=.f.,left=52, top=116, caption="Subfornitori che si sono avvalsi delle agevolazioni di cui all'art.74, comma 5", enabled=.f.,;
    HelpContextID = 212057973,;
    cFormVar="w_VPAR74C5", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oVPAR74C5_3_2.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oVPAR74C5_3_2.GetRadio()
    this.Parent.oContained.w_VPAR74C5 = this.RadioValue()
    return .t.
  endfunc

  func oVPAR74C5_3_2.SetRadio()
    this.Parent.oContained.w_VPAR74C5=trim(this.Parent.oContained.w_VPAR74C5)
    this.value = ;
      iif(this.Parent.oContained.w_VPAR74C5=='S',1,;
      0)
  endfunc

  add object oVPAR74C4_3_4 as StdCheck with uid="DXTRSGFLIH",rtseq=44,rtrep=.f.,left=52, top=137, caption="Subfornitori che si sono avvalsi delle agevolazioni di cui all'art.74, comma 4 bis", enabled=.f.,;
    HelpContextID = 212057974,;
    cFormVar="w_VPAR74C4", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oVPAR74C4_3_4.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oVPAR74C4_3_4.GetRadio()
    this.Parent.oContained.w_VPAR74C4 = this.RadioValue()
    return .t.
  endfunc

  func oVPAR74C4_3_4.SetRadio()
    this.Parent.oContained.w_VPAR74C4=trim(this.Parent.oContained.w_VPAR74C4)
    this.value = ;
      iif(this.Parent.oContained.w_VPAR74C4=='S',1,;
      0)
  endfunc

  add object oVPOPMEDI_3_7 as StdCheck with uid="QAFEEBYKEB",rtseq=45,rtrep=.f.,left=77, top=198, caption="Aliquota media", enabled=.f.,;
    HelpContextID = 172285793,;
    cFormVar="w_VPOPMEDI", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oVPOPMEDI_3_7.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oVPOPMEDI_3_7.GetRadio()
    this.Parent.oContained.w_VPOPMEDI = this.RadioValue()
    return .t.
  endfunc

  func oVPOPMEDI_3_7.SetRadio()
    this.Parent.oContained.w_VPOPMEDI=trim(this.Parent.oContained.w_VPOPMEDI)
    this.value = ;
      iif(this.Parent.oContained.w_VPOPMEDI=='S',1,;
      0)
  endfunc

  add object oVPOPNOIM_3_8 as StdCheck with uid="VMWJPUFKEM",rtseq=46,rtrep=.f.,left=205, top=198, caption="Operazioni non imponibili", enabled=.f.,;
    HelpContextID = 264970403,;
    cFormVar="w_VPOPNOIM", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oVPOPNOIM_3_8.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oVPOPNOIM_3_8.GetRadio()
    this.Parent.oContained.w_VPOPNOIM = this.RadioValue()
    return .t.
  endfunc

  func oVPOPNOIM_3_8.SetRadio()
    this.Parent.oContained.w_VPOPNOIM=trim(this.Parent.oContained.w_VPOPNOIM)
    this.value = ;
      iif(this.Parent.oContained.w_VPOPNOIM=='S',1,;
      0)
  endfunc

  add object oVPCRERIM_3_9 as StdField with uid="PJFRILTJFK",rtseq=47,rtrep=.f.,;
    cFormVar = "w_VPCRERIM", cQueryName = "VPCRERIM",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo rimborsato",;
    HelpContextID = 37511331,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=276, Top=241, cSayPict="v_PV(40)", cGetPict="v_GV(40)"


  add object oVPEURO19_3_11 as StdCombo with uid="BZIBMLRDWF",rtseq=48,rtrep=.f.,left=532,top=241,width=71,height=21, enabled=.f.;
    , ToolTipText = "Valuta con cui viene espresso il versamento";
    , HelpContextID = 267419505;
    , cFormVar="w_VPEURO19",RowSource=""+"Lire,"+"Euro", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oVPEURO19_3_11.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oVPEURO19_3_11.GetRadio()
    this.Parent.oContained.w_VPEURO19 = this.RadioValue()
    return .t.
  endfunc

  func oVPEURO19_3_11.SetRadio()
    this.Parent.oContained.w_VPEURO19=trim(this.Parent.oContained.w_VPEURO19)
    this.value = ;
      iif(this.Parent.oContained.w_VPEURO19=='N',1,;
      iif(this.Parent.oContained.w_VPEURO19=='S',2,;
      0))
  endfunc

  add object oVPCREUTI_3_12 as StdField with uid="HBKZWSPNNE",rtseq=49,rtrep=.f.,;
    cFormVar = "w_VPCREUTI", cQueryName = "VPCREUTI",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Credito da utilizzare in compensazione con mod. F24",;
    HelpContextID = 87842975,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=276, Top=287, cSayPict="v_PV(40)", cGetPict="v_GV(40)"


  add object oVPCODCAR_3_13 as StdCombo with uid="GIGHGMXZAD",rtseq=50,rtrep=.f.,left=142,top=349,width=227,height=21, enabled=.f.;
    , ToolTipText = "Codice carica dichiarante";
    , HelpContextID = 215392088;
    , cFormVar="w_VPCODCAR",RowSource=""+"1) Rappresentante legale o negoziale,"+"2) Socio amministratore,"+"3) Curatore fallimentare,"+"4) Commissario liquidatore,"+"5) Commissario giudiziale,"+"6) Rappresentante fiscale,"+"7) Eredi del contribuente,"+"8) Liquidatore,"+"9) Societ� beneficiaria o incorporante", bObbl = .t. , nPag = 3;
  , bGlobalFont=.t.


  func oVPCODCAR_3_13.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'3',;
    iif(this.value =4,'4',;
    iif(this.value =5,'5',;
    iif(this.value =6,'6',;
    iif(this.value =7,'7',;
    iif(this.value =8,'8',;
    iif(this.value =9,'9',;
    space(1)))))))))))
  endfunc
  func oVPCODCAR_3_13.GetRadio()
    this.Parent.oContained.w_VPCODCAR = this.RadioValue()
    return .t.
  endfunc

  func oVPCODCAR_3_13.SetRadio()
    this.Parent.oContained.w_VPCODCAR=trim(this.Parent.oContained.w_VPCODCAR)
    this.value = ;
      iif(this.Parent.oContained.w_VPCODCAR=='1',1,;
      iif(this.Parent.oContained.w_VPCODCAR=='2',2,;
      iif(this.Parent.oContained.w_VPCODCAR=='3',3,;
      iif(this.Parent.oContained.w_VPCODCAR=='4',4,;
      iif(this.Parent.oContained.w_VPCODCAR=='5',5,;
      iif(this.Parent.oContained.w_VPCODCAR=='6',6,;
      iif(this.Parent.oContained.w_VPCODCAR=='7',7,;
      iif(this.Parent.oContained.w_VPCODCAR=='8',8,;
      iif(this.Parent.oContained.w_VPCODCAR=='9',9,;
      0)))))))))
  endfunc

  add object oVPCODFIS_3_15 as StdField with uid="YNHWXFZDHX",rtseq=51,rtrep=.f.,;
    cFormVar = "w_VPCODFIS", cQueryName = "VPCODFIS",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale del dichiarante",;
    HelpContextID = 103375017,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=478, Top=349, InputMask=replicate('X',16)

  add object oVPDATVER_3_20 as StdField with uid="DMPMJPBNOL",rtseq=52,rtrep=.f.,;
    cFormVar = "w_VPDATVER", cQueryName = "VPDATVER",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data del versamento",;
    HelpContextID = 119238824,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=216, Top=47

  add object oVPCODAZI_3_21 as StdField with uid="ZVPIDXLTDH",rtseq=53,rtrep=.f.,;
    cFormVar = "w_VPCODAZI", cQueryName = "VPCODAZI",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice ABI del conto",;
    HelpContextID = 19488927,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=421, Top=47, InputMask=replicate('X',5), cLinkFile="COD_ABI", cZoomOnZoom="GSAR_ABI", oKey_1_1="ABCODABI", oKey_1_2="this.w_VPCODAZI"

  func oVPCODAZI_3_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_VPCODCAB)
        bRes2=.link_3_22('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oVPCODCAB_3_22 as StdField with uid="LYQTQSMXLU",rtseq=54,rtrep=.f.,;
    cFormVar = "w_VPCODCAB", cQueryName = "VPCODCAB",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice CAB del conto",;
    HelpContextID = 215392104,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=555, Top=47, InputMask=replicate('X',5), cLinkFile="COD_CAB", cZoomOnZoom="GSAR_AFI", oKey_1_1="FICODABI", oKey_1_2="this.w_VPCODAZI", oKey_2_1="FICODCAB", oKey_2_2="this.w_VPCODCAB"

  func oVPCODCAB_3_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oVPDATPRE_3_29 as StdField with uid="RFTBJCSYTU",rtseq=61,rtrep=.f.,;
    cFormVar = "w_VPDATPRE", cQueryName = "VPDATPRE",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 249859941,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=142, Top=381

  add object oVPACQBEA_3_31 as StdCheck with uid="PGQTKYERVP",rtseq=62,rtrep=.f.,left=400, top=201, caption="Acquisto di beni ammortizzabili", enabled=.f.,;
    HelpContextID = 49102999,;
    cFormVar="w_VPACQBEA", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oVPACQBEA_3_31.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oVPACQBEA_3_31.GetRadio()
    this.Parent.oContained.w_VPACQBEA = this.RadioValue()
    return .t.
  endfunc

  func oVPACQBEA_3_31.SetRadio()
    this.Parent.oContained.w_VPACQBEA=trim(this.Parent.oContained.w_VPACQBEA)
    this.value = ;
      iif(this.Parent.oContained.w_VPACQBEA=='S',1,;
      0)
  endfunc

  add object oVPIMPVER_3_32 as StdField with uid="GZGTCYTRIH",rtseq=65,rtrep=.f.,;
    cFormVar = "w_VPIMPVER", cQueryName = "VPIMPVER",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo versato",;
    HelpContextID = 115851432,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=276, Top=15, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  add object oVPVEREUR_3_33 as StdCheck with uid="ZWVMINLWIR",rtseq=66,rtrep=.f.,left=466, top=15, caption="Versamento in Euro", enabled=.f.,;
    ToolTipText = "Valuta con cui viene espresso il versamento",;
    HelpContextID = 100700328,;
    cFormVar="w_VPVEREUR", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oVPVEREUR_3_33.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oVPVEREUR_3_33.GetRadio()
    this.Parent.oContained.w_VPVEREUR = this.RadioValue()
    return .t.
  endfunc

  func oVPVEREUR_3_33.SetRadio()
    this.Parent.oContained.w_VPVEREUR=trim(this.Parent.oContained.w_VPVEREUR)
    this.value = ;
      iif(this.Parent.oContained.w_VPVEREUR=='S',1,;
      0)
  endfunc

  proc oVPVEREUR_3_33.mAfter
    with this.Parent.oContained
      .w_VPIMPVER= 0
    endwith
  endproc

  add object oStr_3_3 as StdString with uid="OQDPAOBDKC",Visible=.t., Left=16, Top=92,;
    Alignment=0, Width=30, Height=18,;
    Caption="VP23"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_5 as StdString with uid="IBLSQOCEPF",Visible=.t., Left=16, Top=171,;
    Alignment=0, Width=30, Height=18,;
    Caption="VP30"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_6 as StdString with uid="CAVGVGGCAG",Visible=.t., Left=77, Top=173,;
    Alignment=0, Width=533, Height=18,;
    Caption="Dati per rimborso o compensazione infrannuale (art.30, 3)"  ;
  , bGlobalFont=.t.

  add object oStr_3_10 as StdString with uid="URGSTHQWGO",Visible=.t., Left=428, Top=241,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_3_10.mHide()
    with this.Parent.oContained
      return (.w_VPEURO19='S')
    endwith
  endfunc

  add object oStr_3_14 as StdString with uid="SBZTRYKJVE",Visible=.t., Left=47, Top=349,;
    Alignment=1, Width=93, Height=15,;
    Caption="Codice carica:"  ;
  , bGlobalFont=.t.

  add object oStr_3_16 as StdString with uid="ZIEWGUXDDT",Visible=.t., Left=19, Top=320,;
    Alignment=0, Width=210, Height=15,;
    Caption="Dichiarante"  ;
  , bGlobalFont=.t.

  add object oStr_3_17 as StdString with uid="TCTURUYMZK",Visible=.t., Left=381, Top=349,;
    Alignment=1, Width=94, Height=15,;
    Caption="Codice fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_3_19 as StdString with uid="ZTTOQVCBVD",Visible=.t., Left=428, Top=289,;
    Alignment=0, Width=28, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_3_19.mHide()
    with this.Parent.oContained
      return (.w_VPEURO19='S')
    endwith
  endfunc

  add object oStr_3_23 as StdString with uid="IKQMECUGPT",Visible=.t., Left=304, Top=47,;
    Alignment=1, Width=114, Height=18,;
    Caption="Codice azienda:"  ;
  , bGlobalFont=.t.

  add object oStr_3_24 as StdString with uid="WDCTMHXWHJ",Visible=.t., Left=501, Top=47,;
    Alignment=1, Width=50, Height=18,;
    Caption="CAB:"  ;
  , bGlobalFont=.t.

  add object oStr_3_25 as StdString with uid="ZDQQDVDVPU",Visible=.t., Left=97, Top=47,;
    Alignment=1, Width=115, Height=18,;
    Caption="Data versamento:"  ;
  , bGlobalFont=.t.

  add object oStr_3_27 as StdString with uid="QYOLXSPUXB",Visible=.t., Left=89, Top=241,;
    Alignment=1, Width=185, Height=18,;
    Caption="Credito chiesto a rimborso:"  ;
  , bGlobalFont=.t.

  add object oStr_3_28 as StdString with uid="IXRPIVSXJP",Visible=.t., Left=81, Top=287,;
    Alignment=1, Width=193, Height=18,;
    Caption="Credito da utilizzare in mod. F24:"  ;
  , bGlobalFont=.t.

  add object oStr_3_30 as StdString with uid="QWRGZJNYQG",Visible=.t., Left=22, Top=381,;
    Alignment=1, Width=118, Height=18,;
    Caption="Data presentazione:"  ;
  , bGlobalFont=.t.

  add object oStr_3_34 as StdString with uid="ILTUYFVZBO",Visible=.t., Left=428, Top=15,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_3_34.mHide()
    with this.Parent.oContained
      return (.w_VPVEREUR='S')
    endwith
  endfunc

  add object oStr_3_35 as StdString with uid="LSOJGAVCXX",Visible=.t., Left=16, Top=15,;
    Alignment=0, Width=122, Height=18,;
    Caption="VP22 - versamento"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_36 as StdString with uid="WDJGJUGWOZ",Visible=.t., Left=162, Top=15,;
    Alignment=1, Width=112, Height=18,;
    Caption="Importo versato:"  ;
  , bGlobalFont=.t.

  add object oStr_3_39 as StdString with uid="STMEKPGSPC",Visible=.t., Left=16, Top=241,;
    Alignment=0, Width=30, Height=18,;
    Caption="VP31"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_40 as StdString with uid="DFUVFNSYVB",Visible=.t., Left=16, Top=287,;
    Alignment=0, Width=30, Height=18,;
    Caption="VP32"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_41 as StdString with uid="DWXGWKPPSS",Visible=.t., Left=510, Top=241,;
    Alignment=1, Width=18, Height=18,;
    Caption="in:"  ;
  , bGlobalFont=.t.

  add object oBox_3_18 as StdBox with uid="LPHMUPQZTR",left=16, top=339, width=633,height=2

  add object oBox_3_26 as StdBox with uid="RCWLAHNAFG",left=16, top=230, width=633,height=1

  add object oBox_3_37 as StdBox with uid="EHFMIKSOJY",left=16, top=82, width=633,height=2

  add object oBox_3_38 as StdBox with uid="HWMNNFWENC",left=16, top=161, width=633,height=2

  add object oBox_3_42 as StdBox with uid="HBJZBKVLCK",left=16, top=276, width=633,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_sdp','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
