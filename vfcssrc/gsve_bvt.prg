* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bvt                                                        *
*              Batch versamenti trimestrali                                    *
*                                                                              *
*      Author: TAM Software & CODELAB                                          *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_296]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-06-05                                                      *
* Last revis.: 2013-05-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bvt",oParentObject)
return(i_retval)

define class tgsve_bvt as StdBatch
  * --- Local variables
  w_MAX = 0
  w_MIN = 0
  w_MAX_MONO = 0
  w_MAX_PLURI = 0
  w_MIN_MONO = 0
  w_MIN_PLURI = 0
  w_IMPENA = 0
  w_IMPASSI = 0
  w_IMPPREVPREC = 0
  w_CONTRPREVPREC = 0
  w_DATATRIM = ctod("  /  /  ")
  w_MINIMALE = 0
  w_MIN_AGE = 0
  w_CONTRPREV = 0
  w_CONTRASSI = 0
  w_TOTPER = 0
  w_CODAZI = space(5)
  w_AGENTE = space(5)
  w_IMPENA = 0
  w_TOTIMPEN = 0
  w_TRIMINES = 0
  w_MINESC = 0
  w_DATACOL = ctod("  /  /  ")
  w_CAMBIO = 0
  w_SCELTA = space(2)
  w_PLCAO = space(1)
  w_ANNO1 = space(4)
  * --- WorkFile variables
  PAR_PROV_idx=0
  VALUTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch lanciato dal bottone di RICERCA (Versamenti Trimestrali)
    *     ======================================================
    *     ATTENZIONE:La procedura ragiona in Euro, poich� modificata nel maggio 2002
    *     Tutti gli importi vengono tradotti in Euro e poi, eventualmente, convertiti in Lire nella
    *     remota ipotesi che nel 2002 si vogliano ancora convertire i dati del versamento
    if  NOT this.oParentObject.cFunction = "Load"
      ah_ErrorMsg("FUNZIONE DISPONIBILE SOLO IN CARICAMENTO VERSAMENTO TRIMESTRALE")
      i_retcode = 'stop'
      return
    endif
    if EMPTY(this.oParentObject.w_VTDATINI) OR EMPTY(this.oParentObject.w_VTDATFIN)
      ah_ErrorMsg("Inserire le date pagamento di selezione")
      i_retcode = 'stop'
      return
    endif
    * --- La qury mette in JOIN due risultati raggruppati per agente:
    *     GSVE_QVT, GSVE1QVT: provvigioni maturate nel periodo: restituisce PREVIDE e ASSISTE che costituiscono gli imponibili
    *     GSVE2QVT, GSVE3QVT: versamenti dei periodi precedenti: In ENAPREC e CONTPREC gli importi di imponibile e contributi gi� versati. In CREDITO il credito maturato
    *     GSVE4QVT-GSVE5QVT
    *     CREDITO: vengono letti i  versamenti effettuati nei periodi precedenti, e calcola il contributo dovuto, verificando se
    *     sono sorti dei crediti verso l'enasarco. Questo accade quando si � versato in un periodo per arrivare al minimale;
    *     se nel periodo succesivo, si devono versare contributi in eccesso rispetto al minimale, si possono scontare i contributi versati
    *     nei periodi precedenti
    *     ************** GLI IMPORTI SONO TRADOTTI SEMPRE IN EURO *****************
    vq_exec("GSVE_QVT.VQR",this,"PROVLI")
    if reccount("PROVLI")<>0
      this.w_PLCAO = "S"
      SELECT PROVLI
      GO TOP
      SCAN
      if PLCAOVAL=0
        this.w_PLCAO = "N"
      endif
      ENDSCAN
      if used("PROVLI")
        select PROVLI
        use
      endif
      if this.w_PLCAO="N"
        ah_ErrorMsg("Impossibile procedere con il calcolo in quanto sono presenti provvigioni liquidate con cambio non valorizzato")
        i_retcode = 'stop'
        return
      endif
    endif
    this.w_ANNO1 = STR(this.oParentObject.w_ANNO,4,0)
    vq_exec("query\GSVE4QVT.VQR",this,"DATI")
    this.w_CODAZI = i_CODAZI
    * --- Azzero i totali dei fondi assistenziali,previdenziali
    this.w_IMPENA = 0
    this.oParentObject.w_TOTPRE1 = 0
    this.oParentObject.w_TOTASS1 = 0
    this.oParentObject.w_TOTASG1 = 0
    this.w_SCELTA = IIF(this.oParentObject.w_VTFLCONT= "P", "I",IIF(this.oParentObject.w_VTFLCONT= "A", "S","IS"))
    * --- Read from PAR_PROV
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_PROV_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROV_idx,2],.t.,this.PAR_PROV_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PPMAXESC,PPMINESC,PPMAXNES,PPMINNES"+;
        " from "+i_cTable+" PAR_PROV where ";
            +"PPCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PPMAXESC,PPMINESC,PPMAXNES,PPMINNES;
        from (i_cTable) where;
            PPCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MAX_MONO = NVL(cp_ToDate(_read_.PPMAXESC),cp_NullValue(_read_.PPMAXESC))
      this.w_MIN_MONO = NVL(cp_ToDate(_read_.PPMINESC),cp_NullValue(_read_.PPMINESC))
      this.w_MAX_PLURI = NVL(cp_ToDate(_read_.PPMAXNES),cp_NullValue(_read_.PPMAXNES))
      this.w_MIN_PLURI = NVL(cp_ToDate(_read_.PPMINNES),cp_NullValue(_read_.PPMINNES))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Carica Detail Versamenti Trimestrali
    SELECT (this.oParentObject.cTrsName)
    * --- Azzera il Transitorio
    if reccount()<>0
      ZAP
    endif
    this.oParentObject.InitRow()
    * --- Importo Minimo per singolo trimestre.
    SELECT DATI
    GO TOP
    SCAN FOR PREVASSI $ this.w_SCELTA
    this.oParentObject.w_VTIMPLIQ = PREVIDE + ASSISTE
    this.oParentObject.w_VTIMPENA = 0
    this.oParentObject.w_VTCONPRE = 0
    this.oParentObject.w_VTCONASS = 0
    this.oParentObject.w_VTCONASG = 0
    this.oParentObject.w_VTFLORMI = 1
    this.oParentObject.w_TIPOAGE = "S"
    this.oParentObject.w_VTCODAGE = AGENTE
    this.oParentObject.w_DESAGE = DESCRI
    this.w_IMPPREVPREC = ENAPREC
    this.w_CONTRPREVPREC = CONTPREC
    this.w_IMPENA = PREVIDE
    this.w_IMPASSI = ASSISTE
    this.w_DATACOL = cp_todate(INIZIO)
    this.w_DATATRIM = this.oParentObject.w_VTDATINI
    if DATI.MONOPLURI = "M"
      this.w_MAX = this.w_MAX_MONO
      this.w_MIN = this.w_MIN_MONO
    else
      this.w_MAX = this.w_MAX_PLURI
      this.w_MIN = this.w_MIN_PLURI
    endif
    this.w_TRIMINES = cp_ROUND((this.w_MIN/4),2)
    * --- Moltiplico il minimale mensile per il n� di trimestri che vanno dall'inizio dell'anno alla data di inizio versamento
    this.w_MINIMALE = this.w_TRIMINES * INT(MONTH(this.w_DATATRIM)/3 + 0.99)
    if DATI.PREVASSI = "I"
      if this.w_IMPPREVPREC + this.w_IMPENA > this.w_MAX
        this.w_IMPENA = this.w_MAX - this.w_IMPPREVPREC
      endif
      this.oParentObject.w_VTIMPENA = this.w_IMPENA
      this.oParentObject.w_VTCONPRE = cp_ROUND(this.oParentObject.w_VTIMPENA * (this.oParentObject.w_VTPERPRE + this.oParentObject.w_VTPERPR2) / 100, this.oParentObject.w_DECEUR)
      if YEAR(this.w_DATACOL) = YEAR(this.w_DATATRIM)
        * --- Se l'agente ha iniziato il rapporto durante l'anno, ricalcolo il minimale in proporzione
        *     DATA DI INIZIO w_DATACOL
        this.w_MIN_AGE = this.w_TRIMINES * ((INT(MONTH(this.w_DATATRIM)/3 + 0.99) - INT(MONTH(this.w_DATACOL)/3 + 0.99)) + 1)
      else
        this.w_MIN_AGE = this.w_MINIMALE
      endif
      if this.w_CONTRPREVPREC + this.oParentObject.w_VTCONPRE < this.w_MIN_AGE
        this.oParentObject.w_VTCONPRE = this.w_MIN_AGE - this.w_CONTRPREVPREC
      else
        * --- Verifico l'esistenza di un credito da spendere. Se esiste, lo utilizzo tenendo presente
        *     che non posso scendere sotto il minimale
        if CREDITO < 0
          this.oParentObject.w_VTCONPRE = this.oParentObject.w_VTCONPRE - MIN(ABS(CREDITO), (this.oParentObject.w_VTCONPRE + this.w_CONTRPREVPREC - this.w_MIN_AGE))
        endif
      endif
      if this.oParentObject.w_Valuta<>g_PERVAL
        this.oParentObject.w_VTIMPENA = VAL2MON(this.oParentObject.w_VTIMPENA,1,this.oParentObject.w_CAOVAL,this.oParentObject.w_VTDATINI,this.oParentObject.w_Valuta,this.oParentObject.w_DECTOT)
        this.oParentObject.w_VTCONPRE = VAL2MON(this.oParentObject.w_VTCONPRE,1,this.oParentObject.w_CAOVAL,this.oParentObject.w_VTDATINI,this.oParentObject.w_Valuta,this.oParentObject.w_DECTOT)
        this.oParentObject.w_VTIMPLIQ = VAL2MON(this.oParentObject.w_VTIMPLIQ,1,this.oParentObject.w_CAOVAL,this.oParentObject.w_VTDATINI,this.oParentObject.w_Valuta,this.oParentObject.w_DECTOT)
      endif
      this.oParentObject.w_TOTPRE1 = this.oParentObject.w_TOTPRE1+ this.oParentObject.w_VTCONPRE
    else
      this.oParentObject.w_VTCONASS = cp_ROUND(this.w_IMPASSI * this.oParentObject.w_VTPERASS / 100, this.oParentObject.w_DECEUR)
      if this.oParentObject.w_VTPERAGE<>0
        this.oParentObject.w_VTCONASG = cp_ROUND(this.w_IMPASSI * this.oParentObject.w_VTPERAGE / 100, this.oParentObject.w_DECEUR)
      endif
      * --- Se il versamento � in Lire, traduco gli importi da Euro
      *     Come data di traduzione prendo quella del versamento perch� le modifiche alla presente funione sono state realizate nel 2002
      if this.oParentObject.w_Valuta<>g_PERVAL
        this.oParentObject.w_VTCONASS = VAL2MON(this.oParentObject.w_VTCONASS,1,this.oParentObject.w_CAOVAL,this.oParentObject.w_VTDATINI,this.oParentObject.w_Valuta,this.oParentObject.w_DECTOT)
        if this.oParentObject.w_VTPERAGE<>0
          this.oParentObject.w_VTCONASG = VAL2MON(this.oParentObject.w_VTCONASG,1,this.oParentObject.w_CAOVAL,this.oParentObject.w_VTDATINI,this.oParentObject.w_Valuta,this.oParentObject.w_DECTOT)
        endif
        this.oParentObject.w_VTIMPLIQ = VAL2MON(this.oParentObject.w_VTIMPLIQ,1,this.oParentObject.w_CAOVAL,this.oParentObject.w_VTDATINI,this.oParentObject.w_Valuta,this.oParentObject.w_DECTOT)
      endif
      this.oParentObject.w_TOTASS1 = this.oParentObject.w_TOTASS1+ this.oParentObject.w_VTCONASS
      this.oParentObject.w_TOTASG1 = this.oParentObject.w_TOTASG1+ this.oParentObject.w_VTCONASG
    endif
    if ABS(this.oParentObject.w_VTIMPLIQ) + ABS(this.oParentObject.w_VTIMPENA) + ABS(this.oParentObject.w_VTCONPRE) + ABS(this.oParentObject.w_VTCONASS) + ABS(this.oParentObject.w_VTCONASG)<> 0
      this.oParentObject.TrsFromWork()
      this.oParentObject.InitRow()
    endif
    SELECT DATI
    ENDSCAN
    * --- Se almeno un valore � diverso da zero, scrivo il record
    SELECT (this.oParentObject.cTrsName)
    GO TOP
    With this.oParentObject
    .WorkFromTrs()
    .SetControlsValue()
    .ChildrenChangeRow()
    .oPgFrm.Page1.oPag.oBody.Refresh()
    .oPgFrm.Page1.oPag.oBody.nAbsRow=1
    EndWith
    if used("DATI")
      select DATI
      use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PAR_PROV'
    this.cWorkTables[2]='VALUTE'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
