* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bof                                                        *
*              Lancia programma collegato                                      *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_186]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-12                                                      *
* Last revis.: 2011-01-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTIPO,p_ANCODICE,p_ANTIPCON
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bof",oParentObject,m.pTIPO,m.p_ANCODICE,m.p_ANTIPCON)
return(i_retval)

define class tgsar_bof as StdBatch
  * --- Local variables
  pTIPO = space(1)
  p_ANCODICE = space(15)
  p_ANTIPCON = space(1)
  w_PROG = .NULL.
  w_OBJ = .NULL.
  w_OBJ1 = .NULL.
  w_CODNOM = space(20)
  * --- WorkFile variables
  CONTI_idx=0
  OFF_NOMI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia dettaglio offerte da anagrafica clienti
    *     la routine non deve avere variabili caller in quanto deve essere lanciabile anche da
    *     tasto destro, sostituirle con i parametri
    * --- --tipo operazione da effettuare
    do case
      case this.pTIPO="A"
        * --- Apre Anagrafica nominativi
        * --- VARIABILI DA PASSARE ALLA MASCHERA
        * --- --Utilizzare la open gest
         OpenGest("A","GSAR_ANO","NOCODCLI",this.p_ANCODICE)
      case this.pTIPO="B"
        * --- ricercare le offerte di quel nominativo/cliente (scheda offerte del nominativo).
        this.w_PROG = GSOF_KRO()
        * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
        if !(this.w_PROG.bSec1)
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          i_retcode = 'stop'
          return
        endif
        this.w_PROG.w_RONOMINA = "C"
        this.w_PROG.w_CHECK = "F"
        this.w_PROG.mCalc(.T.)     
        this.w_PROG.SaveDependsOn()     
        * --- Read from OFF_NOMI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "NOCODICE"+;
            " from "+i_cTable+" OFF_NOMI where ";
                +"NOCODCLI = "+cp_ToStrODBC(this.p_ANCODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            NOCODICE;
            from (i_cTable) where;
                NOCODCLI = this.p_ANCODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CODNOM = NVL(cp_ToDate(_read_.NOCODICE),cp_NullValue(_read_.NOCODICE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_PROG.w_ROCODNOM = this.w_CODNOM
        this.w_OBJ = this.w_PROG.GetcTRL("w_ROCODNOM")
        this.w_OBJ.Check()     
        this.w_PROG.mCalc(.T.)     
        DO GSOF_BVO WITH this.w_PROG
      case this.pTIPO="C"
        * --- Read from OFF_NOMI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "NOCODICE"+;
            " from "+i_cTable+" OFF_NOMI where ";
                +"NOCODCLI = "+cp_ToStrODBC(this.p_ANCODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            NOCODICE;
            from (i_cTable) where;
                NOCODCLI = this.p_ANCODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CODNOM = NVL(cp_ToDate(_read_.NOCODICE),cp_NullValue(_read_.NOCODICE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if g_AGEN = "S"
          * --- Apre la maschera di Ricerca Attivit�
          this.w_PROG = GSAG_KRA()
          * --- Controllo se ha passato il test di accesso
          if !(this.w_PROG.bSec1)
            Ah_ErrorMsg("Impossibile accedere alle attivit�!",48,"")
            i_retcode = 'stop'
            return
          endif
          this.w_PROG.w_EDITCODNOM = .F.
          this.w_PROG.w_CODNOM = this.w_CODNOM
          this.w_OBJ = this.w_PROG.GetCtrl("w_CODNOM")
          this.w_OBJ.Check()     
          * --- Viene notificato l'evento per eseguire la query dello zoom delle Attivit�
          this.w_PROG.NotifyEvent("Ricerca")     
        else
          * --- Apre maschera agenda operatore 
          this.w_PROG = GSOF_KAT()
          * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
          if !(this.w_PROG.bSec1)
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            i_retcode = 'stop'
            return
          endif
          this.w_PROG.w_OPERAT = i_codute
          this.w_PROG.w_NOMINI = this.w_CODNOM
          this.w_OBJ = this.w_PROG.GetcTRL("w_NOMINI")
          this.w_OBJ.Check()     
          this.w_PROG.w_NOMFIN = this.w_CODNOM
          this.w_OBJ1 = this.w_PROG.GetcTRL("w_NOMFIN")
          this.w_OBJ1.Check()     
          this.w_PROG.mCalc(.T.)     
          this.w_PROG.NotifyEvent("Ricerca")     
        endif
    endcase
    this.bUpdateParentObject=.F.
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Messaggio di errore
    Ah_ErrorMsg("Impossibile aprire la gestione collegata!",48,"")
  endproc


  proc Init(oParentObject,pTIPO,p_ANCODICE,p_ANTIPCON)
    this.pTIPO=pTIPO
    this.p_ANCODICE=p_ANCODICE
    this.p_ANTIPCON=p_ANTIPCON
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='OFF_NOMI'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTIPO,p_ANCODICE,p_ANTIPCON"
endproc
