* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_brr                                                        *
*              Emissione R.I.D. - scrittura                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_73]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-02-20                                                      *
* Last revis.: 2016-07-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_brr",oParentObject)
return(i_retval)

define class tgste_brr as StdBatch
  * --- Local variables
  w_SepRec = space(2)
  w_DatPre = ctod("  /  /  ")
  w_NomeFile = space(40)
  w_NomeFile5 = space(40)
  w_NomeFile4 = space(40)
  w_NomeFile7 = space(40)
  w_FLSEPA = space(1)
  w_SERIALE = space(10)
  w_FLIBAN = space(1)
  w_INFMAND = space(1)
  w_Separa = space(2)
  w_Decimali = 0
  w_Handle = 0
  w_Record = space(120)
  w_AziAtt = space(1)
  w_AziSia = space(5)
  w_BanAbi = space(5)
  w_BanCab = space(5)
  w_CliAbi = space(5)
  w_CliCab = space(5)
  w_CliFis = space(16)
  w_CliCod = space(16)
  w_DatCre = space(6)
  w_DatSca = space(6)
  w_NomSup = space(20)
  w_CodDiv = space(1)
  w_Totale = 0
  w_NumDis = space(7)
  w_NumRec = space(7)
  w_Valore = 0
  w_Lunghezza = 0
  w_StrInt = space(1)
  w_StrDec = space(1)
  w_RifDoc = space(1)
  w_TotDoc = space(1)
  w_CodFis = space(16)
  w_ParIva = space(12)
  w_CLIConCor = space(12)
  w_ConCor = space(12)
  w_TipRid = space(1)
  w_MESS = space(200)
  w_OK = .f.
  w_OK1 = .f.
  w_CLIFIS = space(16)
  w_IDBAN = space(16)
  w_LSERIAL = space(10)
  w_TIPCON = space(1)
  w_CODCON = space(15)
  w_SEGNO = space(1)
  w_NUMPAR = space(31)
  w_LDATSCA = ctod("  /  /  ")
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_DATDOC = ctod("  /  /  ")
  w_RIF = space(40)
  w_CINABI = space(1)
  w_BBAN = space(30)
  w_BA__IBAN = space(35)
  w_ChekIban = space(2)
  w_AN__IBAN = space(35)
  w_Chek = space(2)
  w_BANCA = space(35)
  w_BANCA1 = space(35)
  w_CIN = space(1)
  w_CABCLI = space(5)
  w_PTNUMEFF = 0
  w_DESPAR = space(1)
  w_BAIDCRED = space(23)
  w_CODRIF = space(1)
  w_TIPDIS = space(1)
  w_oERRORLOG = .NULL.
  w_FILECREATO = space(200)
  w_ROWEFFE = 0
  w_CODCUC = space(8)
  w_ConCor1 = space(12)
  w_PTTOTIMP = 0
  w_PASEUR = space(2)
  w_IBANSEPA = space(34)
  w_CONTO = space(12)
  w_PAESEBEN = space(2)
  w_IBANDEB = space(34)
  w_LOCALITA = space(254)
  w_DATIAGG = space(254)
  w_DINUMDIS = space(10)
  w_PTDESCRI = space(40)
  w_PTRIFDOC = space(140)
  w_PTCODVAL = space(5)
  w_PTBANAPP = space(5)
  w_PTNUMCOR = space(5)
  w_CPROWNUM = 0
  w_STFILXSD = space(254)
  w_MSGVAL = space(0)
  w_FILEXML = space(100)
  w_FLCAU = space(1)
  w_CASTRUTT = space(10)
  w_BLOCCO = .f.
  w_NOCLOSE = .f.
  w_MAXCODICE = space(10)
  w_Ridvelox = space(1)
  w_TIPOSEQ = space(4)
  w_DATSOT = space(8)
  w_TIPOSTORNO = space(1)
  w_TIPOMAN = space(10)
  w_MANOMFIL = space(200)
  w_TIPOMANPLU = space(10)
  w_CODCLI = space(15)
  w_TIPMAN = space(1)
  w_PTDATSOT = ctod("  /  /  ")
  w_MANUMDIS = space(200)
  w_PTNUMPAR = space(31)
  w_PTCODMAD = space(16)
  w_MINTIPSDD = space(1)
  * --- WorkFile variables
  ATTIMAST_idx=0
  PAR_TITE_idx=0
  MAN_DATI_idx=0
  PAR_EFFE_idx=0
  VASTRUTT_idx=0
  DIS_TINT_idx=0
  SOT_DIST_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione file R.I.D. secondo tracciato CBI
    * --- Variabili della maschera
    this.w_SepRec = this.oParentObject.oParentObject.w_SepRec
    this.w_DatPre = this.oParentObject.oParentObject.w_DatPre
    this.w_FLSEPA = this.oParentObject.oParentObject.w_FLSEPA
    this.w_SERIALE = this.oParentObject.oParentObject.w_DISERIAL
    this.w_NomeFile = this.oParentObject.oParentObject.w_NomeFile
    this.w_NomeFile5 = this.oParentObject.oParentObject.w_NomeFile5
    this.w_NomeFile4 = this.oParentObject.oParentObject.w_NomeFile4
    this.w_NomeFile7 = this.oParentObject.oParentObject.w_NomeFile7
    this.w_FLIBAN = this.oParentObject.oParentObject.w_FLIBAN
    this.w_INFMAND = this.oParentObject.oParentObject.w_INFMAND
    * --- Variabili del batch precedente
    * --- Variabili locali
    this.w_Separa = iif(this.w_SEPREC="S", chr(13)+chr(10), "")
    this.w_Decimali = GETVALUT( this.oParentObject.w_Valuta, "VADECTOT")
    this.w_Handle = 0
    this.w_Record = space(120)
    this.w_AziAtt = space(35)
    this.w_AziSia = this.oParentObject.w_CodSia
    this.w_BanAbi = space(5)
    this.w_BanCab = space(5)
    this.w_CliAbi = space(5)
    this.w_CliCab = space(5)
    this.w_CliFis = space(16)
    this.w_CliCod = space(16)
    this.w_DatCre = space(6)
    this.w_DatSca = space(6)
    this.w_NomSup = space(20)
    this.w_CodDiv = space(1)
    this.w_Totale = 0
    this.w_NumDis = "0000000"
    this.w_NumRec = "0000000"
    this.w_Valore = 0
    this.w_Lunghezza = 0
    this.w_StrInt = space(1)
    this.w_StrDec = space(1)
    this.w_RifDoc = space(1)
    this.w_TotDoc = space(1)
    this.w_CodFis = space(16)
    this.w_ParIva = space(12)
    this.w_CLIConCor = space(12)
    this.w_ConCor = space(12)
    this.w_TipRid = iif(This.oParentObject.oParentObject.w_Tiprid="S","V"," ")
    this.w_OK = .F.
    this.w_OK1 = .F.
    this.w_IDBAN = space(16)
    this.w_CINABI = SPACE(1)
    this.w_BBAN = SPACE(30)
    this.w_BA__IBAN = SPACE(35)
    this.w_ChekIban = space(2)
    this.w_AN__IBAN = SPACE(35)
    this.w_Chek = space(2)
    this.w_CIN = SPACE(1)
    this.w_CABCLI = SPACE(5)
    this.w_BANCA = this.oParentObject.w_BADESC
    this.w_BANCA1 = STRTRAN(this.w_BANCA, "'", " ")
    this.w_BANCA1 = STRTRAN(this.w_BANCA1, '"', " ")
    this.w_BANCA1 = STRTRAN(this.w_BANCA1,"?" , " ")
    this.w_BANCA1 = STRTRAN(this.w_BANCA1,"!" , " ")
    this.w_BANCA1 = STRTRAN(this.w_BANCA1,"/" , " ")
    this.w_BANCA1 = STRTRAN(this.w_BANCA1,"\" , " ")
    this.w_BANCA1 = STRTRAN(this.w_BANCA1,"*" , " ")
    this.w_BANCA1 = STRTRAN(this.w_BANCA1,"," , " ")
    this.w_BANCA1 = STRTRAN(this.w_BANCA1,":" , " ")
    this.w_DESPAR = This.oParentObject.oParentObject.w_DESPAR
    this.w_CODRIF = this.oParentObject.oParentObject.w_CODRIF
    this.oParentObject.w_GENERA = .F.
    this.w_CASTRUTT = this.oParentObject.w_CASTRUTT
    this.w_TIPDIS = this.oParentObject.oParentObject.w_TIPDIS
    Create Cursor MANDATI (CODICE char (15))
    * --- Stanzia l'oggetto per la gestione delle messaggistiche di errore
    this.w_oERRORLOG=createobject("AH_ErrorLog")
    * --- Creazione file R.I.D.
    ah_Msg("Creazione file R.I.D....",.T.)
    do case
      case this.oParentObject.w_TIPOBAN="C" AND EMPTY(ALLTRIM(this.oParentObject.w_PATHFILE2))
        if !DIRECTORY(ALLTRIM(this.oParentObject.w_PATHFILE) + ALLTRIM(this.oParentObject.w_COBANC))
          MKDIR(ALLTRIM(this.oParentObject.w_PATHFILE) + ALLTRIM(this.oParentObject.w_COBANC))
        endif
        this.oParentObject.w_PATHFILE = ALLTRIM(this.oParentObject.w_PATHFILE) + ALLTRIM(this.oParentObject.w_COBANC)+ ALLTRIM(IIF(EMPTY(this.oParentObject.w_COBANC), " ", "\"))
      case this.oParentObject.w_TIPOBAN="D" AND EMPTY(ALLTRIM(this.oParentObject.w_PATHFILE2))
        if !DIRECTORY(ALLTRIM(this.oParentObject.w_PATHFILE) + ALLTRIM(this.w_BANCA1))
          MKDIR(ALLTRIM(this.oParentObject.w_PATHFILE) + ALLTRIM(this.w_BANCA1))
        endif
        this.oParentObject.w_PATHFILE = ALLTRIM(this.oParentObject.w_PATHFILE) + ALLTRIM(this.w_BANCA1)+ ALLTRIM(IIF(EMPTY(this.w_BANCA1), " ", "\"))
    endcase
    if this.oParentObject.w_TIPOPAG="S" AND EMPTY(ALLTRIM(this.oParentObject.w_PATHFILE2))
      if !DIRECTORY(ALLTRIM(this.oParentObject.w_PATHFILE) +iif(this.w_TIPDIS<>"SD","\RID","\SDD"))
        MKDIR(ALLTRIM(this.oParentObject.w_PATHFILE) + iif(this.w_TIPDIS<>"SD","\RID","\SDD"))
      endif
      this.oParentObject.w_PATHFILE = Addbs(ALLTRIM(this.oParentObject.w_PATHFILE) + iif(this.w_TIPDIS<>"SD", ALLTRIM("RID"), ALLTRIM("SDD")))
    endif
    if EMPTY(ALLTRIM(this.oParentObject.w_PATHFILE2))
      if this.w_TIPDIS<>"SD"
        this.w_Handle =fcreate(ALLTRIM(this.oParentObject.w_PATHFILE) + this.oParentObject.w_PATHFILE1, 0)
      endif
      this.w_FILECREATO = ALLTRIM(this.oParentObject.w_PATHFILE) + this.oParentObject.w_PATHFILE1
    else
      if this.w_TIPDIS<>"SD"
        this.w_Handle = fcreate(ALLTRIM(this.oParentObject.w_PATHFILE) + this.oParentObject.w_PATHFILE2 , 0)
      endif
      this.w_FILECREATO = ALLTRIM(this.oParentObject.w_PATHFILE) + this.oParentObject.w_PATHFILE2 
    endif
    if this.w_TIPDIS<>"SD"
      if this.w_Handle = -1
        ah_ErrorMsg("Non � possibile creare il file delle R.I.D.",,"")
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Lettura attivita' principale azienda
    * --- Read from ATTIMAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ATTIMAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATTIMAST_idx,2],.t.,this.ATTIMAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ATDESATT"+;
        " from "+i_cTable+" ATTIMAST where ";
            +"ATCODATT = "+cp_ToStrODBC(g_CATAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ATDESATT;
        from (i_cTable) where;
            ATCODATT = g_CATAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_AziAtt = NVL(cp_ToDate(_read_.ATDESATT),cp_NullValue(_read_.ATDESATT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Lettura degli effetti presenti sul temporaneo e scrittura del file ascii
    this.w_ROWEFFE = 0
    select __tmp__
    go top
    * --- Record - IR (record di testa)
    * --- Codice assegnato dalla Sia all'Azienda Mittente
    this.w_AziSia = left( this.w_AziSia + space(5) , 5)
    * --- Controllo su codice assegnato dalla SIa all'Azienda Mittente (creditrice)
    if empty(NVL(this.w_AziSia,"")) AND this.w_TIPDIS<>"SD"
      this.w_oERRORLOG.AddMsgLog("Codice Sia per azienda mittente non inserito")     
    endif
    this.w_CODCUC = __TMP__.BACODCUC
    if empty(NVL(this.w_CODCUC,"")) AND this.w_TIPDIS="SD" AND g_ISONAZ="ITA"
      this.w_oERRORLOG.AddMsgLog("Codice CUC assegnato all'azienda mittente non inserito")     
      this.w_BLOCCO = .T.
    endif
    * --- Dati banca ricevente
    this.w_BanAbi = right("00000" + alltrim(NVL(__tmp__.bacodabi,SPACE(5))), 5)
    this.w_BanCab = right(space(5) + alltrim(NVL(__tmp__.bacodcab,SPACE(5))), 5)
    this.w_CINABI = alltrim(nvl(__tmp__.bacinabi, " "))
    this.w_BAIDCRED = alltrim(nvl(__tmp__.BAIDCRED, " "))
    this.w_ConCor = Right("000000000000" + alltrim(NVL(UPPER( __tmp__.baconcor),space(12))) , 12)
    this.w_ConCor1 = this.w_ConCor
    this.w_BA__IBAN = alltrim(nvl(__tmp__.ba__iban, space(35)))
    * --- Controllo su codice banca a cui devono essere inviate le disposizioni
    if this.w_BanAbi="00000"
      this.w_oERRORLOG.AddMsgLog("Codice ABI della banca a cui devono essere inviate le disposizioni di incasso (ns. banca) non inserito")     
    endif
    * --- Data creazione file
    this.w_DatCre = dtoc(CP_TODATE(this.w_DatPre))
    this.w_DatCre = left(this.w_DatCre,2) + substr(this.w_DatCre,4,2) + right(this.w_DatCre,2)
    * --- Nome supporto
    * --- w_FILDIS - Nome FISICO del file generato, deve essere un valore univoco
    * --- w_NomSup - Nome del supporto di origine scritto nel file (Record EF) deve essere SEMPRE il nome del file generato dalla distinta EFFETTI di origine
    * --- Campo di libera composizione. Deve comunque essere univoco: data,ora,minuti,secondi,SIA
    this.w_NomSup = time()
    this.w_NomSup = substr(this.w_NomSup,1,2) + substr(this.w_NomSup,4,2) + substr(this.w_NomSup,7,2)
    this.w_NomSup = left( this.w_DatCre + this.w_NomSup + this.w_AziSia +space(20), 20)
    this.oParentObject.w_FILDIS = this.w_NomSup
    * --- Codice divisa (valuta)
    this.w_CodDiv = iif( this.oParentObject.w_Valuta=g_PERVAL , "E" , "I")
    * --- Composizione record e scrittura
    this.w_Record = space(1)+"IR"
    this.w_Record = this.w_Record + this.w_AziSia + this.w_BanAbi + this.w_DatCre + this.w_NomSup
    this.w_Record = this.w_Record + space(6) + space(67) + this.w_TipRid + this.w_CodDiv + space(1) + space(5)
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Ciclo sugli effetti da presentare
    ah_Msg("Scrittura file R.I.D....",.T.)
    scan
    * --- Messaggio
    ah_Msg("Lettura effetto n. %1 del %2",.T.,.F.,.F., str(NVL(__tmp__.PTNUMEFF,0),6,0), dtoc(CP_TODATE(__tmp__.PTDATSCA)) )
    this.w_PTCODMAD = " "
    if this.w_INFMAND="S"
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Totale Importi Effetti
    this.w_TOTALE = this.w_TOTALE + NVL(__tmp__.PTTOTIMP,0)
    * --- Record - (10)
    * --- Calcolo numero progressivo disposizione (progressivo R.I.D)
    this.w_NumDis = right( "0000000" + alltrim( str(val(this.w_NumDis)+1) ) , 7)
    * --- Calcolo data scadenza
    if empty(CP_TODATE(__tmp__.PTDATSCA))
      this.w_DatSca = space(8)
    else
      this.w_DatSca = dtoc(CP_TODATE(__tmp__.PTDATSCA))
      this.w_DatSca = left(this.w_DatSca,2) + substr(this.w_DatSca,4,2) + right(this.w_DatSca,2)
    endif
    * --- Calcolo importo effetto
    this.w_Valore = NVL(__tmp__.PTTOTIMP,0)
    this.w_PTTOTIMP = NVL(__tmp__.PTTOTIMP,0)
    this.w_Lunghezza = 13
    this.Page_3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Dati banca domiciliataria
    this.w_CliAbi = right(space(5) + alltrim(NVL(__tmp__.clcodabi,space(5))), 5)
    this.w_CliCab = right(space(5) + alltrim(NVL(__tmp__.clcodcab,space(5))), 5)
    * --- Controllo su codice ABI della banca  del cliente debitore
    if this.w_CliAbi=space(5)
      this.w_oERRORLOG.AddMsgLog("Codice ABI della banca del cliente debitore (banca di appoggio) non inserito")     
    endif
    * --- Codice cliente
    this.w_CliCod = left( alltrim(NVL( __tmp__.codclien,space(16))) + space(16) , 16)
    * --- Controllo su codice cliente/fornitore
    if empty(NVL(this.w_CliCod,""))
      this.w_oERRORLOG.AddMsgLog("Codice cliente/fornitore non inserito")     
    endif
    * --- Codice Fiscale
    this.w_CodFis = right(alltrim(NVL(__tmp__.ancodfis,space(16))), 16)
    * --- Partita Iva
    this.w_ParIva = right(alltrim(NVL(__tmp__.anpariva,space(12))), 12)
    * --- Conto Corrente del Cliente
    this.w_CLIConCor = left(alltrim(NVL(__tmp__.annumcor,space(12))) + space(12), 12)
    * --- Identificativo Bancario RID
    this.w_IDBAN = left(alltrim(NVL(__tmp__.anibarid,space(16))) + space(16), 16)
    * --- Conto Corrente della banca
    this.w_ConCor = left( alltrim(NVL( __tmp__.baconcor,space(12))) + space(12) , 12)
    * --- Composizione record e scrittura
    if empty(NVL(ANIBARID," ")) and nvl(ANIDRIDY," ") # "S"
      this.w_CliCod = "4" +LEFT(NVL(CODCLIEN,space(16))+ space(16), 16)
    else
      if empty(NVL(ANIBARID,"  "))
        this.w_oERRORLOG.AddMsgLog("Id bancario RID non inserito per %1",NVL( __tmp__.codclien,space(16)))     
      endif
      this.w_CliCod = ALLTRIM(STR(ANTIIDRI,1,0))+LEFT(NVL(ANIBARID,space(16))+ space(16), 16)
    endif
    this.w_PTCODMAD = iif(Not Empty(this.w_PTCODMAD),this.w_PTCODMAD,this.w_CLICOD)
    this.w_Record = " 10" + this.w_NumDis + space(12) + this.w_DatSca + "50000" + this.w_Valore + "-"
    this.w_Record = this.w_Record + this.w_BanAbi + this.w_BanCab + this.w_ConCor + this.w_CliAbi + this.w_CliCab
    this.w_Record = this.w_Record + this.w_CLIConCor + this.w_AziSia + this.w_CliCod + space(5) + this.w_TipRid + this.w_CodDiv
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.w_FLIBAN="S" or this.w_TIPDIS="SD"
      * --- Record - (16)
      * --- Coordinate Ordinante
      * --- Composizione codice BBAN
      if EMPTY(this.w_BA__IBAN)
        this.w_oERRORLOG.AddMsgLog("Codice IBAN dell'ordinante non definito nei conti banche")     
      endif
      if UPPER ( left(this.w_BA__IBAN,2) )<>"IT" And UPPER (left(this.w_BA__IBAN,2) ) <>"SM" AND this.w_FLSEPA="S" and g_ISONAZ="ITA"
        this.w_oERRORLOG.AddMsgLog("Codice paese banca ordinante diverso da 'IT' o diverso da 'SM'")     
      endif
      this.w_PASEUR = SUBSTR(this.w_BA__IBAN, 1,2)
      this.w_PASEUR = iif(empty(this.w_PASEUR),"IT",this.w_PASEUR) 
      this.w_BBAN = CALCBBAN(this.w_CINABI, " ", this.w_BANABI, this.w_BANCAB, this.w_CONCOR)
      this.w_ChekIban = SUBSTR(this.w_BA__IBAN, 3,2)
      if this.w_FLSEPA="S"
        * --- Tracciato SEPA
        this.w_IBANSEPA = this.w_PASEUR+ this.w_ChekIban + this.w_CINABI + this.w_BanAbi +this.w_BanCab + this.w_ConCor1+SPACE(7)
        this.w_Record = " 16" + this.w_NumDis +this.w_PASEUR+ this.w_ChekIban + this.w_CINABI + this.w_BanAbi +this.w_BanCab + this.w_ConCor+SPACE(7)+ left (this.w_BAIDCRED+SPACE ( 23) , 23 ) + space(8) + space(45)
      else
        this.w_Record = " 16" + this.w_NumDis +this.w_PASEUR+ this.w_ChekIban + this.w_CINABI + this.w_BanAbi +this.w_BanCab + this.w_ConCor + space(8) + space(75)
      endif
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_FLSEPA="S"
        * --- Record - (17)
        * --- Coordinate Beneficiario
        * --- Composizione codice BBAN
        this.w_CONTO = right("000000000000" + alltrim(NVL(UPPER(__tmp__.annumcor),space(12))) , 12)
        this.w_AN__IBAN = ALLTRIM(NVL(__tmp__.an__iban, space(35)))
        this.w_PAESEBEN = SUBSTR(this.w_AN__IBAN, 1,2)
        this.w_Chek = SUBSTR(this.w_AN__IBAN, 3,2)
        this.w_CIN = NVL(__tmp__.ANCINABI, " ")
        this.w_CABCLI = right("00000"+ alltrim(NVL(__tmp__.clcodcab,space(5))), 5)
        this.w_IBANDEB = Nvl(__tmp__.CCCOIBAN," ")
        if UPPER (left(this.w_AN__IBAN,2))<>"IT" AND UPPER (left(this.w_AN__IBAN,2))<>"SM" And this.w_FLSEPA="S" and g_ISONAZ="ITA"
          this.w_oERRORLOG.AddMsgLog("Codice paese banca beneficiario diverso da 'IT' o diverso da 'SM'")     
        endif
        if this.w_INFMAND="S"
          this.w_TIPOSEQ = IIF (EMPTY (this.w_MANOMFIL) and this.w_TIPMAN="G" , "FRST" ,this.w_TIPOSEQ)
          this.w_Record = " 17" + this.w_NumDis +this.w_PAESEBEN+ this.w_Chek + this.w_CIN + this.w_CliAbi +this.w_CABCLI + this.w_CONTO + IIF (EMPTY (this.w_MANOMFIL) and this.w_TIPMAN="G" , "FRST" ,this.w_TIPOSEQ)+ this.w_DATSOT+ space(73)
        else
          this.w_Record = " 17" + this.w_NumDis +this.w_PAESEBEN+ this.w_Chek + this.w_CIN + this.w_CliAbi +this.w_CABCLI + this.w_CONTO + space(8) + space(75)
        endif
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    * --- Record - (20)
    * --- Controllo Formale su descrizione del creditore.
    if empty(NVL(g_RAGAZI,""))
      if this.w_OK1=.F.
        this.w_oERRORLOG.AddMsgLog("Descrizione del creditore non inserita")     
        this.w_OK1 = .T.
      endif
    endif
    * --- Composizione record e scrittura
    if this.w_FLSEPA="S"
      this.w_Record = " 20" + this.w_NumDis + left(g_RagAzi+space(70),70)
      this.w_Record = this.w_Record + space(20)
    else
      this.w_Record = " 20" + this.w_NumDis + left(g_RagAzi+space(30),30) + left(g_IndAzi+space(30),30)
      this.w_Record = this.w_Record + left( trim(g_CapAzi) + " " + trim(g_LocAzi) + " " + trim(g_ProAzi) + space(30) ,30)
    endif
    this.w_Record = this.w_Record + space(20) 
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Record - (30)
    * --- Controllo Formale sul codice fiscale (se persona fisica) oppure su partita IVA.
    if empty(NVL(ANCODFIS,""))
      this.w_CLIFIS = LEFT(NVL(ANPARIVA,space(16))+ space(16), 16)
    else
      this.w_CLIFIS = LEFT(NVL(ANCODFIS,space(16))+ space(16), 16)
    endif
    * --- Composizione record e scrittura
    * --- Controllo Formale sull'indirizzo, il cap, comune, sigla della provincia.
    if nvl(__tmp__.ddtiprif, space(2))="PA"
      this.w_Record = " 30" + this.w_NumDis + left( NVL(__tmp__.ddnomdes,space(60)) + space(60) , 60) + space(30) + SPACE(16) + space(4)
    else
      this.w_Record = " 30" + this.w_NumDis + left( NVL(__tmp__.andescri,space(60)) + space(60) , 60) + space(30) + space(16) + space(4)
    endif
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Record - (40)
    * --- Controllo Formale sull'indirizzo, il cap, comune, sigla della provincia.
    if empty(NVL(anindiri,""))
      this.w_oERRORLOG.AddMsgLog("Indirizzo del debitore: %1 non inserito", alltrim(andescri))     
    endif
    if empty(NVL(an___cap,""))
      this.w_oERRORLOG.AddMsgLog("Codice avv. postale del debitore: %1 non inserito",alltrim(andescri))     
    endif
    if empty(NVL(anlocali,""))
      this.w_oERRORLOG.AddMsgLog("Comune del debitore: %1 non inserito", alltrim(andescri))     
    endif
    if empty(NVL(anprovin,""))
      this.w_oERRORLOG.AddMsgLog("Provincia del debitore: %1 non inserita", alltrim(andescri))     
    endif
    * --- Composizione record e scrittura
    * --- Controllo Formale sull'indirizzo, il cap, comune, sigla della provincia.
    this.w_LOCALITA = iif(NVL(__tmp__.DDTIPRIF," ")="PA", alltrim(Nvl(__tmp__.DDPROVIN," ")) +" "+ alltrim(Nvl(__tmp__.DD___CAP," ")) +" "+ alltrim(Nvl(__tmp__.DDLOCALI," "))+" "+alltrim(Nvl(__tmp__.DDINDIRI," ")), Alltrim(Nvl(__tmp__.ANPROVIN," ")) +" "+ alltrim(Nvl(__tmp__.AN___CAP," ")) + " " + alltrim(Nvl(__tmp__.ANLOCALI," "))+" "+alltrim(NVL(__tmp__.ANINDIRI," "))) 
    if nvl(__tmp__.ddtiprif, space(2))="PA"
      this.w_Record = " 40"+ this.w_NumDis + left( NVL(__tmp__.ddindiri,space(30)) + space(30) ,30)
      this.w_Record = this.w_Record + right( "00000" + trim(NVL(__tmp__.dd___cap,space(5))) , 5)
      this.w_Record = this.w_Record + left( NVL(__tmp__.ddlocali,space(22)) + space(22) , 22)
      this.w_Record = this.w_Record + " " + left( NVL(__tmp__.ddprovin,space(2)) + space(2), 2)
      this.w_Record = this.w_Record + space(50)
    else
      this.w_Record = " 40"+ this.w_NumDis + left( NVL(__tmp__.anindiri,space(30)) + space(30) ,30)
      this.w_Record = this.w_Record + right( "00000" + trim(NVL(__tmp__.an___cap,space(5))) , 5)
      this.w_Record = this.w_Record + left( NVL(__tmp__.anlocali,space(22)) + space(22) , 22)
      this.w_Record = this.w_Record + " " + left( NVL(__tmp__.anprovin,space(2)) + space(2), 2)
      this.w_Record = this.w_Record + space(50)
    endif
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Record - (50)
    if this.oParentObject.w_MAXAGG<>this.oParentObject.w_MINAGG
      this.w_DATIAGG = ""
    else
      this.w_DATIAGG = __TMP__.DESCRIZ
    endif
    this.w_LDATSCA = CP_TODATE(__TMP__.PTDATSCA)
    this.w_CODCON = NVL(__TMP__.CODCLIEN, SPACE(15))
    this.w_TIPCON = NVL(__TMP__.PNTIPCON, " ")
    this.w_LSERIAL = NVL(__TMP__.PTSERIAL, SPACE(10))
    this.w_PTNUMEFF = __TMP__.PTNUMEFF
    this.w_DINUMDIS = NVL(__TMP__.PTSERIAL, SPACE(10))
    if NVL(__TMP__.CAFLDSPR, " ")="S"
      if this.w_DESPAR="S"
        this.w_PTDESCRI = __TMP__.PTDESRIG
        this.w_RifDoc = LEFT(alltrim(this.w_DATIAGG) +space(1)+alltrim(this.w_PTDESCRI) +space(90),90)
        this.w_PTRIFDOC = LEFT(alltrim(this.w_DATIAGG) +space(1)+alltrim(this.w_PTDESCRI) +space(140),140)
      else
        this.w_RifDoc = LEFT(this.w_DATIAGG+space(90),90)
        this.w_PTRifDoc = LEFT(this.w_DATIAGG+space(140),140)
      endif
    else
      do case
        case Not EMpty(NVL(__TMP__.ANFLRAGG,"")) AND NUMGRUP>1
          this.w_SEGNO = IIF(NVL(__TMP__.PT_SEGNO, " ")="D","A","D")
          this.w_NUMPAR = NVL(__TMP__.PTNUMPAR, SPACE(31))
          this.w_RifDoc = ah_Msgformat("DOC:") +" "
          VQ_EXEC("QUERY\GSTE4BRF.VQR",this,"temp1")
          SELECT TEMP1
          GO TOP
          SCAN
          this.w_SEGNO = IIF(NVL(PT_SEGNO," ")="A","D","A")
          this.w_RifDoc = alltrim(this.w_RifDoc) + alltrim(str(NVL(temp1.pnnumdoc, 0),15,0))
          this.w_RifDoc = alltrim(this.w_RifDoc)+ iif( empty(NVL(temp1.pnalfdoc,space(10))) , "", "/" + Alltrim(NVL(temp1.pnalfdoc,space(10)) ) )
          this.w_RifDoc = left(alltrim(this.w_RifDoc) + "-" + dtoc(CP_TODATE(temp1.pndatdoc))+" ", 90)
          this.w_PTRifDoc = left(alltrim(this.w_RifDoc) + "-" + dtoc(CP_TODATE(temp1.pndatdoc))+" ", 140)
          ENDSCAN
          if USED("TEMP1")
            SELECT TEMP1
            USE
          endif
        case NVL(__TMP__.PTFLRAGG, "")="S" 
          if NVL(__TMP__.PNNUMDOC, 0)>0
            this.w_RifDoc = ah_Msgformat("DOC:") +" "
            this.w_RifDoc = alltrim(this.w_RifDoc) + alltrim(str(NVL(__tmp__.PNNUMDOC, 0),15,0))
            this.w_RifDoc = alltrim(this.w_RifDoc)+ iif( empty(NVL(__tmp__.PNALFDOC,space(10))) , "", "/" +Alltrim( NVL(__tmp__.PNALFDOC,space(10)) ) )
            this.w_RifDoc = left(alltrim(this.w_RifDoc) + "-" + dtoc(CP_TODATE(__tmp__.PNDATDOC))+" ", 90)
            this.w_PTRifDoc = left(alltrim(this.w_RifDoc) + "-" + dtoc(CP_TODATE(__tmp__.PNDATDOC))+" ", 140)
          else
            this.w_SEGNO = IIF(NVL(__TMP__.PT_SEGNO, " ")="D","A","D")
            this.w_CODCON = NVL(__TMP__.CODCLIEN, SPACE(15))
            this.w_TIPCON = NVL(__TMP__.PNTIPCON, " ")
            this.w_NUMPAR = NVL(__TMP__.PTNUMPAR, SPACE(31))
            this.w_LDATSCA = CP_TODATE(__TMP__.PTDATSCA)
            * --- Select from PAR_TITE
            i_nConn=i_TableProp[this.PAR_TITE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select PTSERIAL  from "+i_cTable+" PAR_TITE ";
                  +" where PTNUMPAR="+cp_ToStrODBC(this.w_NUMPAR)+" AND PTDATSCA="+cp_ToStrODBC(this.w_LDATSCA)+" AND PTTIPCON="+cp_ToStrODBC(this.w_TIPCON)+" AND PTCODCON="+cp_ToStrODBC(this.w_CODCON)+" AND PT_SEGNO="+cp_ToStrODBC(this.w_SEGNO)+" AND PTROWORD =-1 AND PTFLRAGG='S'";
                  +" order by PTFLRAGG";
                   ,"_Curs_PAR_TITE")
            else
              select PTSERIAL from (i_cTable);
               where PTNUMPAR=this.w_NUMPAR AND PTDATSCA=this.w_LDATSCA AND PTTIPCON=this.w_TIPCON AND PTCODCON=this.w_CODCON AND PT_SEGNO=this.w_SEGNO AND PTROWORD =-1 AND PTFLRAGG="S";
               order by PTFLRAGG;
                into cursor _Curs_PAR_TITE
            endif
            if used('_Curs_PAR_TITE')
              select _Curs_PAR_TITE
              locate for 1=1
              do while not(eof())
              this.w_LSERIAL = _Curs_PAR_TITE.PTSERIAL
                select _Curs_PAR_TITE
                continue
              enddo
              use
            endif
            this.w_RifDoc = ah_Msgformat("DOC:") +" "
            VQ_EXEC("QUERY\GSTE2BRF.VQR",this,"temp")
            SELECT TEMP
            GO TOP
            SCAN
            this.w_SEGNO = IIF(NVL(PT_SEGNO," ")="A","D","A")
            this.w_RifDoc = alltrim(this.w_RifDoc) + alltrim(str(NVL(temp.ptnumdoc, 0),15,0))
            this.w_RifDoc = alltrim(this.w_RifDoc)+ iif( empty(NVL(temp.ptalfdoc,space(10))) , "", "/" + Alltrim( NVL(temp.ptalfdoc,space(10)) ) )
            this.w_RifDoc = left(alltrim(this.w_RifDoc) + "-" + dtoc(CP_TODATE(temp.ptdatdoc))+" ", 90)
            this.w_PTRifDoc = left(alltrim(this.w_RifDoc) + "-" + dtoc(CP_TODATE(temp.ptdatdoc))+" ", 140)
            ENDSCAN
            if USED("TEMP")
              SELECT TEMP
              USE
            endif
          endif
        otherwise
          if NOT EMPTY(NVL(__TMP__.PTDESRIG, SPACE(50))) AND this.w_DESPAR="S"
            this.w_RifDoc = LEFT(__TMP__.PTDESRIG+SPACE(90),90)
            this.w_PTRifDoc = LEFT(__TMP__.PTDESRIG+SPACE(140),140)
          else
            this.w_RifDoc = left(ah_Msgformat("PER LA FATTURA N. %1 DEL %2", alltrim(str(NVL(__tmp__.pnnumdoc,0),15,0)) + iif( empty(NVL(__tmp__.pnalfdoc,space(10))) , "", "/" +Alltrim( NVL(__tmp__.pnalfdoc,space(10))) ), dtoc(CP_TODATE(__tmp__.pndatdoc)) + space(90)), 90) 
            this.w_PTRifDoc = left(ah_Msgformat("PER LA FATTURA N. %1 DEL %2", alltrim(str(NVL(__tmp__.pnnumdoc,0),15,0)) + iif( empty(NVL(__tmp__.pnalfdoc,space(10))) , "", "/" +Alltrim( NVL(__tmp__.pnalfdoc,space(10))) ), dtoc(CP_TODATE(__tmp__.pndatdoc)) + space(140)), 140) 
          endif
          * --- Riferimenti documento
          * --- Controllo su numero ricevuta attribuita dal creditore
          if pnnumdoc=0
            this.w_oERRORLOG.AddMsgLog("Numero ricevuta attribuita dal creditore non inserito")     
          endif
      endcase
    endif
    * --- Totale documento
    if Not Empty(this.oParentObject.w_DESCRIZI)
      this.w_TotDoc = ""
    else
      this.w_TotDoc = ah_Msgformat("IMP:")
      this.w_TotDoc = alltrim(this.w_TotDoc) + alltrim(str(NVL(__tmp__.pttotimp,0),18,this.w_Decimali))
    endif
    this.w_PTRIFDOC = left(alltrim(this.w_PTRifDoc) + this.w_TotDoc + SPACE(140), 140)
    this.w_RifDoc = left(alltrim(this.w_RifDoc) + this.w_TotDoc + SPACE(90), 90)
    * --- Composizione record e scrittura
    this.w_Record = " 50" + this.w_NumDis + this.w_RifDoc + space(20)
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Record - (70)
    * --- Composizione record e scrittura
    if this.w_FLSEPA="S" AND this.w_CODRIF="S"
      this.w_Record = " 70" + this.w_NumDis +right("000000000000000"+alltrim(NVL(this.w_SERIALE,0)), 15)+ SPACE (70) + IIF(this.w_INFMAND="S",this.w_TIPOSTORNO,SPACE(1))+ space(24)
    else
      this.w_Record = " 70" + this.w_NumDis +right("000000000000000"+alltrim(str(NVL(__tmp__.PTNUMEFF,0))), 15)+ SPACE (70) + IIF(this.w_INFMAND ="S" AND this.w_FLSEPA="S" ,this.w_TIPOSTORNO,SPACE(1)) +space(24)
    endif
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- DA METTRE INSERT SE FACCIO XML
    if this.w_TIPDIS="SD"
      this.w_PTCODVAL = __TMP__.PTCODVAL
      this.w_PTNUMCOR = __TMP__.PTNUMCOR
      this.w_PTBANAPP = __TMP__.BANAPP
      this.w_CPROWNUM = __tmp__.Cprownum
      if this.w_TIPDIS="SD"
        this.w_ROWEFFE = this.w_ROWEFFE + 1
        * --- Try
        local bErr_04926F98
        bErr_04926F98=bTrsErr
        this.Try_04926F98()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_04926F98
        * --- End
      endif
    endif
    endscan
    * --- Record - EF (Record di coda)
    * --- Totale importi negativi (totale delle disposizioni : somma importi Ri.Ba)
    this.w_Valore = this.w_Totale
    this.w_Lunghezza = 15
    this.Page_3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_Totale = this.w_Valore
    * --- Numero disposizioni (numero totale delle Ri.Ba)
    if this.w_FLIBAN="S" or this.w_TIPDIS="SD"
      this.w_NumRec = right( "0000000" + alltrim( str( (val(this.w_NumDis) * IIF( this.w_FLSEPA="S" , 8 , 7) ) + 2 , 7, 0) ) , 7)
    else
      this.w_NumRec = right( "0000000" + alltrim( str( (val(this.w_NumDis) * 6) + 2 , 7, 0) ) , 7)
    endif
    * --- Composizione record e scrittura
    this.w_Record = space(1)
    this.w_Record = space(1) + "EF" + this.w_AziSia + this.w_BanAbi + this.w_DatCre + this.w_NomSup + space(6)
    this.w_Record = this.w_Record + this.w_NumDis + this.w_Totale + repl("0",15) + this.w_NumRec + space(23)
    this.w_Record = this.w_Record +this.w_TipRid + this.w_CodDiv + space(6)
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Try
    local bErr_049560C8
    bErr_049560C8=bTrsErr
    this.Try_049560C8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_049560C8
    * --- End
    if this.w_TIPDIS<>"SD"
      this.w_NOCLOSE = .not. fclose(this.w_Handle) 
    else
      this.w_NOCLOSE = .F.
    endif
    if this.w_NOCLOSE OR this.w_oErrorLog.IsFullLog()
      if this.w_BLOCCO
        if AH_YESNO("Generazione file fallita. %0Stampo situazione messaggi di errore?")
          this.w_oERRORLOG.PrintLog(this.oParentObject.oParentObject,"Errori riscontrati",.F.)     
        endif
      else
        if this.w_TIPDIS<>"SD"
          this.w_oERRORLOG.PrintLog(this,"Errori riscontrati",.T., "File R.I.D. generato con errori%0Stampo situazione messaggi di errore?")     
        else
          this.w_oERRORLOG.PrintLog(this,"Errori riscontrati",.T., "File SEPA Direct Debit generato con errori%0Stampo situazione messaggi di errore?")     
        endif
      endif
      if this.w_BLOCCO OR NOT ah_YesNo("Mantengo comunque il file generato?")
        NAMEFILE=ALLTRIM(this.oParentObject.w_PATHFILE) + IIF(EMPTY(ALLTRIM(this.w_NOMEFILE5)), ALLTRIM(this.w_NOMEFILE4), ALLTRIM(this.w_NOMEFILE7))
        DELETE FILE (NAMEFILE)
        this.oParentObject.w_GENERA = .T.
        this.oParentObject.w_FILE=" "
      endif
    endif
    * --- Try
    local bErr_04955A68
    bErr_04955A68=bTrsErr
    this.Try_04955A68()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      AH_ERRORMSG("Aggiornamento distinta non avvenuto correttamente")
    endif
    bTrsErr=bTrsErr or bErr_04955A68
    * --- End
    if USED("MANDATI")
      SELECT MANDATI
      USE
    endif
  endproc
  proc Try_04926F98()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PAR_EFFE
    i_nConn=i_TableProp[this.PAR_EFFE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_EFFE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_EFFE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTDATSCA"+",PTTIPCON"+",PTCODCON"+",PTTOTIMP"+",PTCODVAL"+",PTBANAPP"+",PTNUMEFF"+",PTDATPRE"+",PTIBANSE"+",PTNOMFIL"+",PTRIFDOC"+",PTTIPMAN"+",PTDATSOT"+",PTTIPSEQ"+",PTCODRIF"+",PTTIPSDD"+",PTTIPRID"+",PTFLIBAN"+",PTINFMAN"+",PTNUMCOR"+",PTIBADEB"+",PTMESGID"+",PTCODMAD"+",PTLOCALI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_DINUMDIS),'PAR_EFFE','PTSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(-2),'PAR_EFFE','PTROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ROWEFFE),'PAR_EFFE','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LDATSCA),'PAR_EFFE','PTDATSCA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPCON),'PAR_EFFE','PTTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODCON),'PAR_EFFE','PTCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTTOTIMP),'PAR_EFFE','PTTOTIMP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODVAL),'PAR_EFFE','PTCODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_EFFE','PTBANAPP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMEFF),'PAR_EFFE','PTNUMEFF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DatPre),'PAR_EFFE','PTDATPRE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IBANSEPA),'PAR_EFFE','PTIBANSE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MANOMFIL),'PAR_EFFE','PTNOMFIL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTRIFDOC),'PAR_EFFE','PTRIFDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPMAN),'PAR_EFFE','PTTIPMAN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATSOT),'PAR_EFFE','PTDATSOT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPOSEQ),'PAR_EFFE','PTTIPSEQ');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODRIF),'PAR_EFFE','PTCODRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MINTIPSDD),'PAR_EFFE','PTTIPSDD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPRID),'PAR_EFFE','PTTIPRID');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FLIBAN),'PAR_EFFE','PTFLIBAN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_INFMAND),'PAR_EFFE','PTINFMAN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMCOR),'PAR_EFFE','PTNUMCOR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IBANDEB),'PAR_EFFE','PTIBADEB');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FILDIS),'PAR_EFFE','PTMESGID');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ptcodmad),'PAR_EFFE','PTCODMAD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LOCALITA),'PAR_EFFE','PTLOCALI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.w_DINUMDIS,'PTROWORD',-2,'CPROWNUM',this.w_ROWEFFE,'PTDATSCA',this.w_LDATSCA,'PTTIPCON',this.w_TIPCON,'PTCODCON',this.w_CODCON,'PTTOTIMP',this.w_PTTOTIMP,'PTCODVAL',this.w_PTCODVAL,'PTBANAPP',this.w_PTBANAPP,'PTNUMEFF',this.w_PTNUMEFF,'PTDATPRE',this.w_DatPre,'PTIBANSE',this.w_IBANSEPA)
      insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTDATSCA,PTTIPCON,PTCODCON,PTTOTIMP,PTCODVAL,PTBANAPP,PTNUMEFF,PTDATPRE,PTIBANSE,PTNOMFIL,PTRIFDOC,PTTIPMAN,PTDATSOT,PTTIPSEQ,PTCODRIF,PTTIPSDD,PTTIPRID,PTFLIBAN,PTINFMAN,PTNUMCOR,PTIBADEB,PTMESGID,PTCODMAD,PTLOCALI &i_ccchkf. );
         values (;
           this.w_DINUMDIS;
           ,-2;
           ,this.w_ROWEFFE;
           ,this.w_LDATSCA;
           ,this.w_TIPCON;
           ,this.w_CODCON;
           ,this.w_PTTOTIMP;
           ,this.w_PTCODVAL;
           ,this.w_PTBANAPP;
           ,this.w_PTNUMEFF;
           ,this.w_DatPre;
           ,this.w_IBANSEPA;
           ,this.w_MANOMFIL;
           ,this.w_PTRIFDOC;
           ,this.w_TIPMAN;
           ,this.w_PTDATSOT;
           ,this.w_TIPOSEQ;
           ,this.w_CODRIF;
           ,this.w_MINTIPSDD;
           ,this.w_TIPRID;
           ,this.w_FLIBAN;
           ,this.w_INFMAND;
           ,this.w_PTNUMCOR;
           ,this.w_IBANDEB;
           ,this.oParentObject.w_FILDIS;
           ,this.w_ptcodmad;
           ,this.w_LOCALITA;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_049560C8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Generazione XML
    if this.w_TIPDIS="SD" AND ! this.w_BLOCCO
      this.w_FILEXML = FORCEEXT(ADDBS(JUSTPATH(Alltrim(this.w_FILECREATO)))+Juststem(Alltrim(this.w_FILECREATO)),"xml")
      if Not Empty(this.w_CASTRUTT)
        * --- Insert into SOT_DIST
        i_nConn=i_TableProp[this.SOT_DIST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SOT_DIST_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"sot_dist",this.SOT_DIST_idx)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        GSTE_BGF(this,this.w_DINUMDIS,this.w_CASTRUTT,this.w_FILEXML)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if ! cp_fileexist(this.w_FILEXML)
          this.w_oERRORLOG.AddMsgLog("Generazione file Xml non avvenuta")     
        else
          * --- Read from VASTRUTT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VASTRUTT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VASTRUTT_idx,2],.t.,this.VASTRUTT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "STFILXSD"+;
              " from "+i_cTable+" VASTRUTT where ";
                  +"STCODICE = "+cp_ToStrODBC(this.w_CASTRUTT);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              STFILXSD;
              from (i_cTable) where;
                  STCODICE = this.w_CASTRUTT;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_STFILXSD = NVL(cp_ToDate(_read_.STFILXSD),cp_NullValue(_read_.STFILXSD))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_MSGVAL = " "
          if g_ISONAZ="ITA"
            this.w_MSGVAL = ValidateXml(this.w_FILEXML,Fullpath(this.w_STFILXSD))
          endif
          if Not Empty(this.w_MSGVAL)
            this.w_oERRORLOG.AddMsgLog("Errore di validazione XML: %1",Alltrim(this.w_MSGVAL))     
          endif
        endif
      else
        this.w_oERRORLOG.AddMsgLog("Codice Strutta CBI/SEPA non presente nella causale distinta associata")     
      endif
    endif
    * --- begin transaction
    cp_BeginTrs()
    if this.w_TIPDIS="SD"
      * --- Delete from PAR_EFFE
      i_nConn=i_TableProp[this.PAR_EFFE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_EFFE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"PTSERIAL = "+cp_ToStrODBC(this.w_DINUMDIS);
               )
      else
        delete from (i_cTable) where;
              PTSERIAL = this.w_DINUMDIS;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Delete from SOT_DIST
      i_nConn=i_TableProp[this.SOT_DIST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SOT_DIST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"PTSERIAL = "+cp_ToStrODBC(this.w_DINUMDIS);
               )
      else
        delete from (i_cTable) where;
              PTSERIAL = this.w_DINUMDIS;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_04955A68()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Se ci sono dei mandati intestati di tipo recurrenti per i quali deve essere scritto il campo MANOMFIL
    if RECCOUNT ("MANDATI")>0
      SELECT MANDATI 
 Go TOP 
 SCAN
      * --- Write into MAN_DATI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MAN_DATI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAN_DATI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MAN_DATI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MANOMFIL ="+cp_NullLink(cp_ToStrODBC(this.w_FILECREATO),'MAN_DATI','MANOMFIL');
        +",MANUMDIS ="+cp_NullLink(cp_ToStrODBC(this.oparentobject.w_NUMDIS),'MAN_DATI','MANUMDIS');
            +i_ccchkf ;
        +" where ";
            +"MACODICE = "+cp_ToStrODBC(CODICE);
               )
      else
        update (i_cTable) set;
            MANOMFIL = this.w_FILECREATO;
            ,MANUMDIS = this.oparentobject.w_NUMDIS;
            &i_ccchkf. ;
         where;
            MACODICE = CODICE;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      ENDSCAN
    endif
    * --- commit
    cp_EndTrs(.t.)
    if this.w_TIPDIS<>"SD"
      ah_ErrorMsg("Generazione file R.I.D. terminata","","")
    else
      ah_ErrorMsg("Generazione file SEPA Direct Debit terminata","","")
    endif
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrittura record su file ascii
    * --- Scrittura record su File R.I.D.
    if this.w_TIPDIS<>"SD"
      w_t = fwrite(this.w_Handle, this.w_Record+this.w_Separa)
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Trasformazione degli importi nel formato richiesto dal tracciato R.I.D.
    * --- UTILIZZO
    * --- Prima della chiamata a questa pagina del batch devono essere impostate le variabili:
    * --- w_Valore          = importo da convertire in stringa
    * --- w_Lunghezza  = lunghezza della stringa
    * --- Il risultato della conversione � disponibile nella variabile w_Valore.
    * --- CONVENZIONI
    * --- La virgola deve essere eliminata
    * --- Se la valuta � Euro le ultime due cifre rappresentano i decimali
    * --- Il valore deve essere allineato a destra
    * --- E' richiesto il riempimento con 0 delle cifre non significative
    * --- Calcolo parte intera
    this.w_StrInt = alltrim( str( int(this.w_Valore), this.w_Lunghezza, 0 ) )
    * --- Calcolo parte decimale
    this.w_StrDec = ""
    if this.w_Decimali > 0
      this.w_StrDec = right( str( this.w_Valore, this.w_Lunghezza, this.w_Decimali ) , this.w_Decimali )
    endif
    * --- Risultato
    this.w_Valore = right( repl("0",this.w_Lunghezza) + this.w_StrInt + this.w_StrDec , this.w_Lunghezza )
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli sul mandato
    this.w_Ridvelox = This.oParentObject.oParentObject.w_Tiprid
    * --- Estrae informazioni sui mandati
    this.w_CODCLI = ""
    this.w_TIPOMAN = ""
    this.w_TIPOMANPLU = ""
    this.w_MAXCODICE = SPACE(10)
    this.w_TIPOSEQ = space(4)
    this.w_DATSOT = space(6)
    this.w_TIPOSTORNO = ""
    this.w_PTNUMEFF = NVL(__TMP__.PTNUMEFF,0)
    this.w_LSERIAL = NVL(__TMP__.PTSERIAL, SPACE(10))
    if this.w_TIPDIS="SD" and !this.w_BLOCCO
      * --- Select from gstesman
      do vq_exec with 'gstesman',this,'_Curs_gstesman','',.f.,.t.
      if used('_Curs_gstesman')
        select _Curs_gstesman
        locate for 1=1
        do while not(eof())
        this.w_MESS = AH_MSGFORMAT ("Disitinta contenente partite associate a mandati con tipologie SDD diverse")
        this.w_oERRORLOG.AddMsgLog(this.w_MESS)     
        this.w_BLOCCO = .T.
          select _Curs_gstesman
          continue
        enddo
        use
      endif
    endif
    * --- Select from gste2man
    do vq_exec with 'gste2man',this,'_Curs_gste2man','',.f.,.t.
    if used('_Curs_gste2man')
      select _Curs_gste2man
      locate for 1=1
      do while not(eof())
      this.w_TIPOSEQ = MAXTIPSEQ
      this.w_DATSOT = nvl(dtoc(MAXDATSOT),"")
      this.w_PTDATSOT = CP_TODATE(MAXDATSOT)
      this.w_DATSOT = left(this.w_DATSOT,2) + substr(this.w_DATSOT,4,2) + right(this.w_DATSOT,2)
      this.w_TIPOSTORNO = MAXTIPSTO
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      exit
        select _Curs_gste2man
        continue
      enddo
      use
    endif
    this.w_TIPOMAN = "intestato "
    this.w_TIPOMANPLU = "intestati "
    this.w_CODCLI = __tmp__.codclien
    if EMPTY (this.w_MAXCODICE )
      * --- Select from gste3man
      do vq_exec with 'gste3man',this,'_Curs_gste3man','',.f.,.t.
      if used('_Curs_gste3man')
        select _Curs_gste3man
        locate for 1=1
        do while not(eof())
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_MANOMFIL = NVL (MANOMFIL , " ")
        this.w_MANUMDIS = NVL (MANUMDIS , " ")
        this.w_TIPMAN = NVL(MATIPMAN," ")
        exit
          select _Curs_gste3man
          continue
        enddo
        use
      endif
      * --- Se il mandato � intestato di tipo recurrent la prima scadenza del mandato deve uscire con tipo sequenza First 
      *     (il nome del file deve essere scritto sul mandato. Utilizzo il cursore per scrivere i mandati alla fine della generazione del file per non mettere sotto transazione la generazione del file
      if (EMPTY (this.w_MANOMFIL) OR this.w_MANUMDIS=this.oparentobject.w_NUMDIS) AND NVL (this.w_TIPOSEQ," ")="RCUR" AND NOT EMPTY (this.w_MAXCODICE)
        * --- Verifico se � stata scritta una partita per il mandato intestato
        SELECT MANDATI 
 LOCATE FOR CODICE=this.w_MAXCODICE
        if FOUND ()
          this.w_MANOMFIL = "NomeFile"
        else
          insert into MANDATI values (this.w_MAXCODICE)
          this.w_MANOMFIL = ""
        endif
        SELECT __tmp__
      else
        this.w_MANOMFIL = "NomeFile"
      endif
      if EMPTY (this.w_MAXCODICE )
        this.w_TIPOMAN = "generico "
        this.w_TIPOMANPLU = "generici "
        this.w_CODCLI = ""
        * --- Select from gste4man
        do vq_exec with 'gste4man',this,'_Curs_gste4man','',.f.,.t.
        if used('_Curs_gste4man')
          select _Curs_gste4man
          locate for 1=1
          do while not(eof())
          this.Pag5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          exit
            select _Curs_gste4man
            continue
          enddo
          use
        endif
      endif
      if EMPTY (this.w_MAXCODICE )
        * --- Read from PAR_TITE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_TITE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PTNUMPAR"+;
            " from "+i_cTable+" PAR_TITE where ";
                +"PTSERIAL = "+cp_ToStrODBC(this.w_LSERIAL);
                +" and PTROWORD = "+cp_ToStrODBC(-2);
                +" and CPROWNUM = "+cp_ToStrODBC(__tmp__.Cprownum);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PTNUMPAR;
            from (i_cTable) where;
                PTSERIAL = this.w_LSERIAL;
                and PTROWORD = -2;
                and CPROWNUM = __tmp__.Cprownum;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PTNUMPAR = NVL(cp_ToDate(_read_.PTNUMPAR),cp_NullValue(_read_.PTNUMPAR))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_MESS = AH_MSGFORMAT ("Partita numero %5 con scadenza %2 del %3 %4 non � presente in alcun mandato valido", "" ,dtoc(__tmp__.PTDATSCA) ,IIF(__tmp__.PNTIPCON="C","cliente",IIF(__tmp__.PNTIPCON="F","fornitore","conto generico")), alltrim (__tmp__.Codclien), alltrim ( NVL (this.w_PTNUMPAR," ")) )
        this.w_oERRORLOG.AddMsgLog(this.w_MESS)     
        this.w_BLOCCO = .T.
      endif
    endif
    select __tmp__
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if RECCOUNT() > 0 AND not EMPTY (MAXCODICE)
      * --- Controlli sul mandato
      if MAXCODICE <> MINCODICE
        if EMPTY (MINCODICE)
          this.w_MESS = AH_MSGFORMAT ("Esistono scadenze relative alla partita raggruppata numero %1 del %2 relative al %3 %4 non associate ad alcun mandato", alltrim(PTNUMPAR), dtoc(DATSCA),IIF(PTTIPCON="C","cliente",IIF(PTTIPCON="F","fornitore","conto generico")), alltrim (PTCODCON), this.w_TIPOMANPLU)
          this.w_oERRORLOG.AddMsgLog(this.w_MESS)     
          this.w_BLOCCO = .T.
        else
          if NVL (TIPORAG,0)>1 
            this.w_MESS = AH_MSGFORMAT ("Partita raggruppata numero %1 scadenza %2 del %3 %4 associata a pi� mandati %5", alltrim(PTNUMPAR), dtoc(DATSCA),IIF(PTTIPCON="C","cliente",IIF(PTTIPCON="F","fornitore","conto generico")), alltrim (PTCODCON), this.w_TIPOMANPLU)
            this.w_oERRORLOG.AddMsgLog(this.w_MESS)     
            this.w_BLOCCO = .T.
          else
            this.w_MESS = AH_MSGFORMAT ("Partita numero %1 scadenza %2 del %3 %4 associata a pi� mandati %5", alltrim(PTNUMPAR), dtoc(DATSCA),IIF(PTTIPCON="C","cliente",IIF(PTTIPCON="F","fornitore","conto generico")), alltrim (PTCODCON), this.w_TIPOMANPLU)
            this.w_oERRORLOG.AddMsgLog(this.w_MESS)     
            this.w_BLOCCO = .T.
          endif
        endif
      else
        if NOT ( NOT EMPTY ( nvl (MAXDATSOT,"") ) AND MAXDATSOT<DATSCA AND ( EMPTY (nvl (MINDATFIN,"")) OR MINDATFIN>DATSCA) )
          this.w_MESS = AH_MSGFORMAT ("Partita numero %1 scadenza %2 del %3 %4 associata a un mandato %5non valido",alltrim(PTNUMPAR), dtoc(DATSCA),IIF(PTTIPCON="C","cliente",IIF(PTTIPCON="F","fornitore","conto generico")), alltrim (PTCODCON),this.w_TIPOMAN)
          this.w_oERRORLOG.AddMsgLog(this.w_MESS)     
          this.w_BLOCCO = .T.
        endif
        if ((MINTIPSDD = "C" AND this.w_RIDVELOX="S" ) OR (MINTIPSDD = "B" AND this.w_RIDVELOX<>"S" )) AND this.w_TIPOMAN<>"generico " and this.w_TIPDIS<>"SD"
          this.w_MESS = AH_MSGFORMAT ("Partita numero %1 scadenza %2 del %3 %4 associata a un mandato %5%0con tipo SDD incoerente con il tipo RID",alltrim(PTNUMPAR), dtoc(DATSCA),IIF(PTTIPCON="C","cliente",IIF(PTTIPCON="F","fornitore","conto generico")), alltrim (PTCODCON),this.w_TIPOMAN)
          this.w_oERRORLOG.AddMsgLog(this.w_MESS)     
          this.w_BLOCCO = .T.
        endif
        * --- Se la partita � ragruppata controllo che i mandati abbiano tutti lo stesso tipo sequenza
        if NVL (TIPORAG,0)>1 AND MINTIPSEQ<>MAXTIPSEQ 
          this.w_MESS = AH_MSGFORMAT ("Partita raggruppata numero %1 scadenza %2 del %3 %4 associata a mandati %5con tipo sequenza diverso",alltrim(PTNUMPAR), dtoc(DATSCA),IIF(PTTIPCON="C","cliente",IIF(PTTIPCON="F","fornitore","conto generico")), alltrim (PTCODCON), this.w_TIPOMANPLU )
          this.w_oERRORLOG.AddMsgLog(this.w_MESS)     
          this.w_BLOCCO = .T.
        endif
      endif
      this.w_MAXCODICE = MAXCODICE
      this.w_TIPOSEQ = IIF (MAXTIPSEQ=MINTIPSEQ , MINTIPSEQ , SPACE(4))
      this.w_PTDATSOT = cp_todate(MAXDATSOT)
      this.w_PTCODMAD = iif(this.w_TIPOMAN="intestato " or Empty(this.w_TIPOMAN),this.w_MAXCODICE," ")
      this.w_DATSOT = IIF ( NOT EMPTY ( NVL ( MAXDATSOT,"") ) , right ("00"+alltrim(str(DAY(MAXDATSOT))),2) + right ("00"+alltrim(str(MONTH(MAXDATSOT))),2) + right ("00"+alltrim(str(YEAR(MAXDATSOT))),2) , SPACE(6))
      this.w_TIPOSTORNO = NVL (MAXTIPSTO , Space(1) )
      * --- Se mandato generico e RID veloce mettiamo B2B altrimenti dal campo dei mandati
      this.w_MINTIPSDD = iif(this.w_TIPOMAN="generico " ,"C",MINTIPSDD)
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='ATTIMAST'
    this.cWorkTables[2]='PAR_TITE'
    this.cWorkTables[3]='MAN_DATI'
    this.cWorkTables[4]='PAR_EFFE'
    this.cWorkTables[5]='VASTRUTT'
    this.cWorkTables[6]='DIS_TINT'
    this.cWorkTables[7]='SOT_DIST'
    return(this.OpenAllTables(7))

  proc CloseCursors()
    if used('_Curs_PAR_TITE')
      use in _Curs_PAR_TITE
    endif
    if used('_Curs_gstesman')
      use in _Curs_gstesman
    endif
    if used('_Curs_gste2man')
      use in _Curs_gste2man
    endif
    if used('_Curs_gste3man')
      use in _Curs_gste3man
    endif
    if used('_Curs_gste4man')
      use in _Curs_gste4man
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
