* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bd5                                                        *
*              Selezione zoom dettaglio cash flow effettivo                    *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][207]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-09-12                                                      *
* Last revis.: 2014-09-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bd5",oParentObject,m.pTipo)
return(i_retval)

define class tgste_bd5 as StdBatch
  * --- Local variables
  pTipo = space(1)
  w_ZOOM = .NULL.
  w_MSK = .NULL.
  w_ZOOM2 = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiamato da GSTE_KDE
    *     Dettaglio cash flow effettivo
    do case
      case this.pTipo="C"
        this.w_MSK = THIS.OPARENTOBJECT
        if this.oParentObject.w_TIPOCASHFLOW="E"
          This.oParentObject.w_Zoomddc.Visible=.f.
          This.oParentObject.w_Zoomddce.Visible=.f.
          this.w_ZOOM = this.w_MSK.w_ZOOMDDE
          this.w_ZOOM2 = this.w_MSK.w_ZOOMDDER
        else
          This.oParentObject.w_Zoomdde.Visible=.f.
          This.oParentObject.w_Zoomdder.Visible=.f.
          this.w_ZOOM = this.w_MSK.w_ZOOMDDC
          this.w_ZOOM2 = this.w_MSK.w_ZOOMDDCE
        endif
        ah_msg("Elabora dettaglio scadenze per il periodo selezionato",.t.,.t.)
        * --- Assegna il nome delle query
        this.w_ZOOM.cCpQueryName = "QUERY\GSTE1KDE"
        this.w_MSK.NotifyEvent("Esegui")     
        this.w_ZOOM2.cCpQueryName = "QUERY\GSTE3KDE"
        this.w_MSK.NotifyEvent("Raggruppate")     
      case this.pTipo="A"
        this.w_MSK = THIS.OPARENTOBJECT
        this.w_ZOOM = this.w_MSK.w_ZOOMDDE
        ah_msg("Elabora dettaglio scadenze per il periodo selezionato",.t.,.t.)
        this.w_ZOOM.cCpQueryName = "QUERY\GSTE1KAT"
        this.w_MSK.NotifyEvent("Esegui")     
    endcase
  endproc


  proc Init(oParentObject,pTipo)
    this.pTipo=pTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo"
endproc
