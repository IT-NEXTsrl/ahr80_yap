* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bla                                                        *
*              Lancia archivi multiazienda                                     *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_60]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-25                                                      *
* Last revis.: 2006-03-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOpz
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bla",oParentObject,m.pOpz)
return(i_retval)

define class tgsar_bla as StdBatch
  * --- Local variables
  pOpz = space(1)
  w_PROG = .NULL.
  w_DXBTN = .f.
  * --- WorkFile variables
  PAR_PROV_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue il Lancio dei Programmi Multiazienda da Menu' (deve sempre puntare alla Azienda Corrente)
    this.w_DXBTN = .F.
    if Type("g_oMenu.oKey")<>"U"
      * --- Gestione Tasto Destro
      this.w_DXBTN = .T.
    endif
    if TYPE("this.pOpz")<>"C"
      this.pOpz = "3"
    endif
    this.pOpz = IIF(EMPTY(this.pOpz), "3", this.pOpz)
    * --- Istanzio l'oggetto
    do case
      case LEFT(this.pOpz,1)="2"
        * --- Contropartite
        APPO = UPPER(ALLTRIM(SUBSTR(this.pOpz, 2)))
        if NOT EMPTY(APPO)
          this.w_PROG = &APPO
          * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
          if !(this.w_PROG.bSec1)
            i_retcode = 'stop'
            return
          endif
          * --- inizializzo la chiave Primaria
          this.w_PROG.ecpFilter()     
          this.w_PROG.w_COCODAZI = i_CODAZI
        endif
    endcase
    this.w_PROG.ecpSave()     
    * --- Gestione Tasto destro: Modifica e Carica aprono comunque il dettaglio in modifica
    if this.w_DXBTN and g_oMenu.cBatchType$"ML"
      * --- Modifica
      this.w_PROG.ecpEdit()     
    endif
  endproc


  proc Init(oParentObject,pOpz)
    this.pOpz=pOpz
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PAR_PROV'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOpz"
endproc
