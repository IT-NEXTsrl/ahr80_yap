* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_miu                                                        *
*              Difformit� prezzi impostati e ultimo costo d'acquisto           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-02-01                                                      *
* Last revis.: 2012-04-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsve_miu"))

* --- Class definition
define class tgsve_miu as StdTrsForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 790
  Height = 270+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-04-16"
  HelpContextID=264909417
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=37

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  CHECKUCA_IDX = 0
  cFile = "CHECKUCA"
  cKeySelect = "LISERIAL"
  cKeyWhere  = "LISERIAL=this.w_LISERIAL"
  cKeyDetail  = "LISERIAL=this.w_LISERIAL and LIROWNUM=this.w_LIROWNUM"
  cKeyWhereODBC = '"LISERIAL="+cp_ToStrODBC(this.w_LISERIAL)';

  cKeyDetailWhereODBC = '"LISERIAL="+cp_ToStrODBC(this.w_LISERIAL)';
      +'+" and LIROWNUM="+cp_ToStrODBC(this.w_LIROWNUM)';

  cKeyWhereODBCqualified = '"CHECKUCA.LISERIAL="+cp_ToStrODBC(this.w_LISERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gsve_miu"
  cComment = "Difformit� prezzi impostati e ultimo costo d'acquisto"
  i_nRowNum = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  windowtype = 1
  minbutton = .f.
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_LISERIAL = space(10)
  w_LIROWORD = 0
  w_LICODICE = space(41)
  w_LIDESART = space(40)
  w_LIKEYSAL = space(40)
  w_LICODMAG = space(5)
  w_LIUNIMIS = space(3)
  w_LIQTAMOV = 0
  w_LIPREZZO = 0
  w_LISCONT1 = 0
  w_LISCONT2 = 0
  w_LISCONT3 = 0
  w_LISCONT4 = 0
  w_LIFLCPRO = space(1)
  w_LIIMPCOL = 0
  w_LIIMPNAZ = 0
  w_LIIMPPRO = 0
  w_LIPERPRO = 0
  w_LIIMPUCA = 0
  w_LISCOCL1 = 0
  w_LISCOCL2 = 0
  w_LIDATUCA = ctod('  /  /  ')
  w_LIROWNUM = 0
  w_LIKEYLIS = space(10)
  w_LICODLIS = space(5)
  w_LISCOLIS = space(5)
  w_LIPROLIS = space(5)
  w_LIPROSCO = space(5)
  w_LICODVAL = space(3)
  w_LIQTAUM1 = 0
  w_LITIPCON = space(1)
  w_LICODCON = space(15)
  w_LICAOVAL = 0
  w_LIDATDOC = ctod('  /  /  ')
  w_LILISCHK = space(5)
  w_LICHKUCA = space(1)
  w_LIIMPSCO = 0
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsve_miu
  * Movimentazione d'appoggio al salvataggio
  * non scrive mai sul database ma sulla gestione
  * sottostante (NotifyEvent) ed in seguito
  * si chiude...
  Proc EcpSave()
   this.NotifyEvent('SalvaeChiudi')
   * Per evitare msg "Abbandoni Modifiche.."
   This.cFunction='Query'
   This.EcpQuit()
  EndProc
  
  * redichiaro in toto la ECPQUIT
  * per chiudere la maschera a seguito del messaggio
  * Abbandoni le modifiche
  * --- Esc
  proc ecpQuit()
      local i_oldfunc
      * ===========================
      * unica modifica da
      * i_oldfunc=this.cFunction
      * a
      i_oldfunc='Query'
      * ===========================
      if inlist(this.cFunction,'Edit','Load')
        if this.IsAChildUpdated()
          if !cp_YesNo(MSG_DISCARD_CHANGES_QP)
            return
          endif
        endif
        this.bUpdated=.f.
        this.NotifyEvent('Edit Aborted')
      endif
      *--- Activity Logger traccio ESC
      If i_nACTIVATEPROFILER>0
      	cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "UES", "", This, i_ServerConn(1,2), 0, Iif(bTrsErr, i_TrsMsg,""),This)
      Endif
      *--- Activity Logger traccio ESC
      * --- si sposta senza attivare i controlli
      this.cFunction='Filter'
      local i_err
      i_err=on('error')
      on error =.t.
      this.activecontrol.parent.oContained.bDontReportError=.t.
      on error &i_err
      this.__dummy__.enabled=.t.
      this.__dummy__.Setfocus()
      * ---
      do case
        case i_oldfunc="Query"
          this.NotifyEvent("Done")
          this.Hide()
          this.Release()
        case i_oldfunc="Load"
          *this.QueryKeySet(this.cLastWhere,this.cLastOrderBy)
          *this.ecpQuery()
          *this.nLoadTime=0
          This.cFunction="Query"
          This.BlankRec()
          This.bLoaded=.F.
          This.SetStatus()
          This.SetFocusOnFirstControl()
        case i_oldfunc="Edit"
          this.ecpQuery()
          this.nLoadTime=0
        otherwise
          this.ecpQuery()
      endcase
      return
  EndProc
  
  Proc ecpLoad()
    Dodefault()
    this.NotifyEvent("CaricaRighe")
  Endproc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CHECKUCA','gsve_miu')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_miuPag1","gsve_miu",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Righe difformi")
      .Pages(1).HelpContextID = 146929441
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='CHECKUCA'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CHECKUCA_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CHECKUCA_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_LISERIAL = NVL(LISERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from CHECKUCA where LISERIAL=KeySet.LISERIAL
    *                            and LIROWNUM=KeySet.LIROWNUM
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.CHECKUCA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CHECKUCA_IDX,2],this.bLoadRecFilter,this.CHECKUCA_IDX,"gsve_miu")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CHECKUCA')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CHECKUCA.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CHECKUCA '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'LISERIAL',this.w_LISERIAL  )
      select * from (i_cTable) CHECKUCA where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_LISERIAL = NVL(LISERIAL,space(10))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_LIKEYLIS = NVL(LIKEYLIS,space(10))
        .w_LICODVAL = NVL(LICODVAL,space(3))
        .w_LITIPCON = NVL(LITIPCON,space(1))
        .w_LICODCON = NVL(LICODCON,space(15))
        .w_LICAOVAL = NVL(LICAOVAL,0)
        .w_LIDATDOC = NVL(cp_ToDate(LIDATDOC),ctod("  /  /  "))
        cp_LoadRecExtFlds(this,'CHECKUCA')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_LILISCHK = space(5)
          .w_LICHKUCA = space(1)
          .w_LIIMPSCO = 0
          .w_LIROWORD = NVL(LIROWORD,0)
          .w_LICODICE = NVL(LICODICE,space(41))
          .w_LIDESART = NVL(LIDESART,space(40))
          .w_LIKEYSAL = NVL(LIKEYSAL,space(40))
          .w_LICODMAG = NVL(LICODMAG,space(5))
          .w_LIUNIMIS = NVL(LIUNIMIS,space(3))
          .w_LIQTAMOV = NVL(LIQTAMOV,0)
          .w_LIPREZZO = NVL(LIPREZZO,0)
          .w_LISCONT1 = NVL(LISCONT1,0)
          .w_LISCONT2 = NVL(LISCONT2,0)
          .w_LISCONT3 = NVL(LISCONT3,0)
          .w_LISCONT4 = NVL(LISCONT4,0)
          .w_LIFLCPRO = NVL(LIFLCPRO,space(1))
          .w_LIIMPCOL = NVL(LIIMPCOL,0)
          .w_LIIMPNAZ = NVL(LIIMPNAZ,0)
          .w_LIIMPPRO = NVL(LIIMPPRO,0)
          .w_LIPERPRO = NVL(LIPERPRO,0)
          .w_LIIMPUCA = NVL(LIIMPUCA,0)
          .w_LISCOCL1 = NVL(LISCOCL1,0)
          .w_LISCOCL2 = NVL(LISCOCL2,0)
          .w_LIDATUCA = NVL(cp_ToDate(LIDATUCA),ctod("  /  /  "))
          .w_LIROWNUM = NVL(LIROWNUM,0)
          .w_LICODLIS = NVL(LICODLIS,space(5))
          .w_LISCOLIS = NVL(LISCOLIS,space(5))
          .w_LIPROLIS = NVL(LIPROLIS,space(5))
          .w_LIPROSCO = NVL(LIPROSCO,space(5))
          .w_LIQTAUM1 = NVL(LIQTAUM1,0)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace LIROWNUM with .w_LIROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_1_9.enabled = .oPgFrm.Page1.oPag.oBtn_1_9.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_LISERIAL=space(10)
      .w_LIROWORD=0
      .w_LICODICE=space(41)
      .w_LIDESART=space(40)
      .w_LIKEYSAL=space(40)
      .w_LICODMAG=space(5)
      .w_LIUNIMIS=space(3)
      .w_LIQTAMOV=0
      .w_LIPREZZO=0
      .w_LISCONT1=0
      .w_LISCONT2=0
      .w_LISCONT3=0
      .w_LISCONT4=0
      .w_LIFLCPRO=space(1)
      .w_LIIMPCOL=0
      .w_LIIMPNAZ=0
      .w_LIIMPPRO=0
      .w_LIPERPRO=0
      .w_LIIMPUCA=0
      .w_LISCOCL1=0
      .w_LISCOCL2=0
      .w_LIDATUCA=ctod("  /  /  ")
      .w_LIROWNUM=0
      .w_LIKEYLIS=space(10)
      .w_LICODLIS=space(5)
      .w_LISCOLIS=space(5)
      .w_LIPROLIS=space(5)
      .w_LIPROSCO=space(5)
      .w_LICODVAL=space(3)
      .w_LIQTAUM1=0
      .w_LITIPCON=space(1)
      .w_LICODCON=space(15)
      .w_LICAOVAL=0
      .w_LIDATDOC=ctod("  /  /  ")
      .w_LILISCHK=space(5)
      .w_LICHKUCA=space(1)
      .w_LIIMPSCO=0
      if .cFunction<>"Filter"
        .DoRTCalc(1,13,.f.)
        .w_LIFLCPRO = 'N'
        .w_LIIMPCOL = cp_Round(.w_LIPREZZO * (1+.w_LISCONT1/100)*(1+.w_LISCONT2/100)*(1+.w_LISCONT3/100)*(1+.w_LISCONT4/100),5)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'CHECKUCA')
    this.DoRTCalc(16,37,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsve_miu
    if this.cFunction<>'Load'
      this.ecpLoad()
    endif
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oBtn_1_7.enabled = i_bVal
      .Page1.oPag.oBtn_1_8.enabled = i_bVal
      .Page1.oPag.oBtn_1_9.enabled = .Page1.oPag.oBtn_1_9.mCond()
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'CHECKUCA',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- gsve_miu
    * disabilito il tab elenco
    this.oPgFrm.Pages(this.oPgFrm.PageCount).enabled=.f.
    this.oPgFrm.Pages(this.oPgFrm.PageCount).Caption=""
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CHECKUCA_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LISERIAL,"LISERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LIKEYLIS,"LIKEYLIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LICODVAL,"LICODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LITIPCON,"LITIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LICODCON,"LICODCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LICAOVAL,"LICAOVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LIDATDOC,"LIDATDOC",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CHECKUCA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CHECKUCA_IDX,2])
    i_lTable = "CHECKUCA"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CHECKUCA_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select distinct "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select distinct &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_LIROWORD N(5);
      ,t_LICODICE C(41);
      ,t_LIDESART C(40);
      ,t_LIUNIMIS C(3);
      ,t_LIQTAMOV N(12,3);
      ,t_LIPREZZO N(18,5);
      ,t_LISCONT1 N(6,2);
      ,t_LISCONT2 N(6,2);
      ,t_LISCONT3 N(6,2);
      ,t_LISCONT4 N(6,2);
      ,t_LIFLCPRO N(3);
      ,LIROWNUM N(4);
      ,t_LIKEYSAL C(40);
      ,t_LICODMAG C(5);
      ,t_LIIMPCOL N(18,4);
      ,t_LIIMPNAZ N(18,4);
      ,t_LIIMPPRO N(18,4);
      ,t_LIPERPRO N(6,2);
      ,t_LIIMPUCA N(18,5);
      ,t_LISCOCL1 N(6,2);
      ,t_LISCOCL2 N(6,2);
      ,t_LIDATUCA D(8);
      ,t_LIROWNUM N(4);
      ,t_LICODLIS C(5);
      ,t_LISCOLIS C(5);
      ,t_LIPROLIS C(5);
      ,t_LIPROSCO C(5);
      ,t_LIQTAUM1 N(12,3);
      ,t_LILISCHK C(5);
      ,t_LICHKUCA C(1);
      ,t_LIIMPSCO N(18,5);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsve_miubodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLIROWORD_2_1.controlsource=this.cTrsName+'.t_LIROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLICODICE_2_2.controlsource=this.cTrsName+'.t_LICODICE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLIDESART_2_3.controlsource=this.cTrsName+'.t_LIDESART'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLIUNIMIS_2_6.controlsource=this.cTrsName+'.t_LIUNIMIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLIQTAMOV_2_7.controlsource=this.cTrsName+'.t_LIQTAMOV'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLIPREZZO_2_8.controlsource=this.cTrsName+'.t_LIPREZZO'
    this.oPgFRm.Page1.oPag.oLISCONT1_2_9.controlsource=this.cTrsName+'.t_LISCONT1'
    this.oPgFRm.Page1.oPag.oLISCONT2_2_10.controlsource=this.cTrsName+'.t_LISCONT2'
    this.oPgFRm.Page1.oPag.oLISCONT3_2_11.controlsource=this.cTrsName+'.t_LISCONT3'
    this.oPgFRm.Page1.oPag.oLISCONT4_2_12.controlsource=this.cTrsName+'.t_LISCONT4'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLIFLCPRO_2_13.controlsource=this.cTrsName+'.t_LIFLCPRO'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(51)
    this.AddVLine(179)
    this.AddVLine(462)
    this.AddVLine(500)
    this.AddVLine(601)
    this.AddVLine(744)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLIROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CHECKUCA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CHECKUCA_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CHECKUCA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CHECKUCA_IDX,2])
      *
      * insert into CHECKUCA
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CHECKUCA')
        i_extval=cp_InsertValODBCExtFlds(this,'CHECKUCA')
        i_cFldBody=" "+;
                  "(LISERIAL,LIROWORD,LICODICE,LIDESART,LIKEYSAL"+;
                  ",LICODMAG,LIUNIMIS,LIQTAMOV,LIPREZZO,LISCONT1"+;
                  ",LISCONT2,LISCONT3,LISCONT4,LIFLCPRO,LIIMPCOL"+;
                  ",LIIMPNAZ,LIIMPPRO,LIPERPRO,LIIMPUCA,LISCOCL1"+;
                  ",LISCOCL2,LIDATUCA,LIROWNUM,LIKEYLIS,LICODLIS"+;
                  ",LISCOLIS,LIPROLIS,LIPROSCO,LICODVAL,LIQTAUM1"+;
                  ",LITIPCON,LICODCON,LICAOVAL,LIDATDOC,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_LISERIAL)+","+cp_ToStrODBC(this.w_LIROWORD)+","+cp_ToStrODBC(this.w_LICODICE)+","+cp_ToStrODBC(this.w_LIDESART)+","+cp_ToStrODBC(this.w_LIKEYSAL)+;
             ","+cp_ToStrODBC(this.w_LICODMAG)+","+cp_ToStrODBC(this.w_LIUNIMIS)+","+cp_ToStrODBC(this.w_LIQTAMOV)+","+cp_ToStrODBC(this.w_LIPREZZO)+","+cp_ToStrODBC(this.w_LISCONT1)+;
             ","+cp_ToStrODBC(this.w_LISCONT2)+","+cp_ToStrODBC(this.w_LISCONT3)+","+cp_ToStrODBC(this.w_LISCONT4)+","+cp_ToStrODBC(this.w_LIFLCPRO)+","+cp_ToStrODBC(this.w_LIIMPCOL)+;
             ","+cp_ToStrODBC(this.w_LIIMPNAZ)+","+cp_ToStrODBC(this.w_LIIMPPRO)+","+cp_ToStrODBC(this.w_LIPERPRO)+","+cp_ToStrODBC(this.w_LIIMPUCA)+","+cp_ToStrODBC(this.w_LISCOCL1)+;
             ","+cp_ToStrODBC(this.w_LISCOCL2)+","+cp_ToStrODBC(this.w_LIDATUCA)+","+cp_ToStrODBC(this.w_LIROWNUM)+","+cp_ToStrODBC(this.w_LIKEYLIS)+","+cp_ToStrODBC(this.w_LICODLIS)+;
             ","+cp_ToStrODBC(this.w_LISCOLIS)+","+cp_ToStrODBC(this.w_LIPROLIS)+","+cp_ToStrODBC(this.w_LIPROSCO)+","+cp_ToStrODBC(this.w_LICODVAL)+","+cp_ToStrODBC(this.w_LIQTAUM1)+;
             ","+cp_ToStrODBC(this.w_LITIPCON)+","+cp_ToStrODBC(this.w_LICODCON)+","+cp_ToStrODBC(this.w_LICAOVAL)+","+cp_ToStrODBC(this.w_LIDATDOC)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CHECKUCA')
        i_extval=cp_InsertValVFPExtFlds(this,'CHECKUCA')
        cp_CheckDeletedKey(i_cTable,0,'LISERIAL',this.w_LISERIAL,'LIROWNUM',this.w_LIROWNUM)
        INSERT INTO (i_cTable) (;
                   LISERIAL;
                  ,LIROWORD;
                  ,LICODICE;
                  ,LIDESART;
                  ,LIKEYSAL;
                  ,LICODMAG;
                  ,LIUNIMIS;
                  ,LIQTAMOV;
                  ,LIPREZZO;
                  ,LISCONT1;
                  ,LISCONT2;
                  ,LISCONT3;
                  ,LISCONT4;
                  ,LIFLCPRO;
                  ,LIIMPCOL;
                  ,LIIMPNAZ;
                  ,LIIMPPRO;
                  ,LIPERPRO;
                  ,LIIMPUCA;
                  ,LISCOCL1;
                  ,LISCOCL2;
                  ,LIDATUCA;
                  ,LIROWNUM;
                  ,LIKEYLIS;
                  ,LICODLIS;
                  ,LISCOLIS;
                  ,LIPROLIS;
                  ,LIPROSCO;
                  ,LICODVAL;
                  ,LIQTAUM1;
                  ,LITIPCON;
                  ,LICODCON;
                  ,LICAOVAL;
                  ,LIDATDOC;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_LISERIAL;
                  ,this.w_LIROWORD;
                  ,this.w_LICODICE;
                  ,this.w_LIDESART;
                  ,this.w_LIKEYSAL;
                  ,this.w_LICODMAG;
                  ,this.w_LIUNIMIS;
                  ,this.w_LIQTAMOV;
                  ,this.w_LIPREZZO;
                  ,this.w_LISCONT1;
                  ,this.w_LISCONT2;
                  ,this.w_LISCONT3;
                  ,this.w_LISCONT4;
                  ,this.w_LIFLCPRO;
                  ,this.w_LIIMPCOL;
                  ,this.w_LIIMPNAZ;
                  ,this.w_LIIMPPRO;
                  ,this.w_LIPERPRO;
                  ,this.w_LIIMPUCA;
                  ,this.w_LISCOCL1;
                  ,this.w_LISCOCL2;
                  ,this.w_LIDATUCA;
                  ,this.w_LIROWNUM;
                  ,this.w_LIKEYLIS;
                  ,this.w_LICODLIS;
                  ,this.w_LISCOLIS;
                  ,this.w_LIPROLIS;
                  ,this.w_LIPROSCO;
                  ,this.w_LICODVAL;
                  ,this.w_LIQTAUM1;
                  ,this.w_LITIPCON;
                  ,this.w_LICODCON;
                  ,this.w_LICAOVAL;
                  ,this.w_LIDATDOC;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.CHECKUCA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CHECKUCA_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_LIROWORD)) and not(Empty(t_LICODICE))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'CHECKUCA')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " LIKEYLIS="+cp_ToStrODBC(this.w_LIKEYLIS)+;
                 ",LICODVAL="+cp_ToStrODBC(this.w_LICODVAL)+;
                 ",LITIPCON="+cp_ToStrODBC(this.w_LITIPCON)+;
                 ",LICODCON="+cp_ToStrODBC(this.w_LICODCON)+;
                 ",LICAOVAL="+cp_ToStrODBC(this.w_LICAOVAL)+;
                 ",LIDATDOC="+cp_ToStrODBC(this.w_LIDATDOC)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and LIROWNUM="+cp_ToStrODBC(&i_TN.->LIROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'CHECKUCA')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  LIKEYLIS=this.w_LIKEYLIS;
                 ,LICODVAL=this.w_LICODVAL;
                 ,LITIPCON=this.w_LITIPCON;
                 ,LICODCON=this.w_LICODCON;
                 ,LICAOVAL=this.w_LICAOVAL;
                 ,LIDATDOC=this.w_LIDATDOC;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and LIROWNUM=&i_TN.->LIROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_LIROWORD)) and not(Empty(t_LICODICE))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and LIROWNUM="+cp_ToStrODBC(&i_TN.->LIROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and LIROWNUM=&i_TN.->LIROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace LIROWNUM with this.w_LIROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update CHECKUCA
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'CHECKUCA')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " LIROWORD="+cp_ToStrODBC(this.w_LIROWORD)+;
                     ",LICODICE="+cp_ToStrODBC(this.w_LICODICE)+;
                     ",LIDESART="+cp_ToStrODBC(this.w_LIDESART)+;
                     ",LIKEYSAL="+cp_ToStrODBC(this.w_LIKEYSAL)+;
                     ",LICODMAG="+cp_ToStrODBC(this.w_LICODMAG)+;
                     ",LIUNIMIS="+cp_ToStrODBC(this.w_LIUNIMIS)+;
                     ",LIQTAMOV="+cp_ToStrODBC(this.w_LIQTAMOV)+;
                     ",LIPREZZO="+cp_ToStrODBC(this.w_LIPREZZO)+;
                     ",LISCONT1="+cp_ToStrODBC(this.w_LISCONT1)+;
                     ",LISCONT2="+cp_ToStrODBC(this.w_LISCONT2)+;
                     ",LISCONT3="+cp_ToStrODBC(this.w_LISCONT3)+;
                     ",LISCONT4="+cp_ToStrODBC(this.w_LISCONT4)+;
                     ",LIFLCPRO="+cp_ToStrODBC(this.w_LIFLCPRO)+;
                     ",LIIMPCOL="+cp_ToStrODBC(this.w_LIIMPCOL)+;
                     ",LIIMPNAZ="+cp_ToStrODBC(this.w_LIIMPNAZ)+;
                     ",LIIMPPRO="+cp_ToStrODBC(this.w_LIIMPPRO)+;
                     ",LIPERPRO="+cp_ToStrODBC(this.w_LIPERPRO)+;
                     ",LIIMPUCA="+cp_ToStrODBC(this.w_LIIMPUCA)+;
                     ",LISCOCL1="+cp_ToStrODBC(this.w_LISCOCL1)+;
                     ",LISCOCL2="+cp_ToStrODBC(this.w_LISCOCL2)+;
                     ",LIDATUCA="+cp_ToStrODBC(this.w_LIDATUCA)+;
                     ",LIKEYLIS="+cp_ToStrODBC(this.w_LIKEYLIS)+;
                     ",LICODLIS="+cp_ToStrODBC(this.w_LICODLIS)+;
                     ",LISCOLIS="+cp_ToStrODBC(this.w_LISCOLIS)+;
                     ",LIPROLIS="+cp_ToStrODBC(this.w_LIPROLIS)+;
                     ",LIPROSCO="+cp_ToStrODBC(this.w_LIPROSCO)+;
                     ",LICODVAL="+cp_ToStrODBC(this.w_LICODVAL)+;
                     ",LIQTAUM1="+cp_ToStrODBC(this.w_LIQTAUM1)+;
                     ",LITIPCON="+cp_ToStrODBC(this.w_LITIPCON)+;
                     ",LICODCON="+cp_ToStrODBC(this.w_LICODCON)+;
                     ",LICAOVAL="+cp_ToStrODBC(this.w_LICAOVAL)+;
                     ",LIDATDOC="+cp_ToStrODBC(this.w_LIDATDOC)+;
                     ",LIROWNUM="+cp_ToStrODBC(this.w_LIROWNUM)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and LIROWNUM="+cp_ToStrODBC(LIROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'CHECKUCA')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      LIROWORD=this.w_LIROWORD;
                     ,LICODICE=this.w_LICODICE;
                     ,LIDESART=this.w_LIDESART;
                     ,LIKEYSAL=this.w_LIKEYSAL;
                     ,LICODMAG=this.w_LICODMAG;
                     ,LIUNIMIS=this.w_LIUNIMIS;
                     ,LIQTAMOV=this.w_LIQTAMOV;
                     ,LIPREZZO=this.w_LIPREZZO;
                     ,LISCONT1=this.w_LISCONT1;
                     ,LISCONT2=this.w_LISCONT2;
                     ,LISCONT3=this.w_LISCONT3;
                     ,LISCONT4=this.w_LISCONT4;
                     ,LIFLCPRO=this.w_LIFLCPRO;
                     ,LIIMPCOL=this.w_LIIMPCOL;
                     ,LIIMPNAZ=this.w_LIIMPNAZ;
                     ,LIIMPPRO=this.w_LIIMPPRO;
                     ,LIPERPRO=this.w_LIPERPRO;
                     ,LIIMPUCA=this.w_LIIMPUCA;
                     ,LISCOCL1=this.w_LISCOCL1;
                     ,LISCOCL2=this.w_LISCOCL2;
                     ,LIDATUCA=this.w_LIDATUCA;
                     ,LIKEYLIS=this.w_LIKEYLIS;
                     ,LICODLIS=this.w_LICODLIS;
                     ,LISCOLIS=this.w_LISCOLIS;
                     ,LIPROLIS=this.w_LIPROLIS;
                     ,LIPROSCO=this.w_LIPROSCO;
                     ,LICODVAL=this.w_LICODVAL;
                     ,LIQTAUM1=this.w_LIQTAUM1;
                     ,LITIPCON=this.w_LITIPCON;
                     ,LICODCON=this.w_LICODCON;
                     ,LICAOVAL=this.w_LICAOVAL;
                     ,LIDATDOC=this.w_LIDATDOC;
                     ,LIROWNUM=this.w_LIROWNUM;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and LIROWNUM=&i_TN.->LIROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CHECKUCA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CHECKUCA_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_LIROWORD)) and not(Empty(t_LICODICE))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete CHECKUCA
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and LIROWNUM="+cp_ToStrODBC(&i_TN.->LIROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and LIROWNUM=&i_TN.->LIROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_LIROWORD)) and not(Empty(t_LICODICE))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CHECKUCA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CHECKUCA_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,14,.t.)
          .w_LIIMPCOL = cp_Round(.w_LIPREZZO * (1+.w_LISCONT1/100)*(1+.w_LISCONT2/100)*(1+.w_LISCONT3/100)*(1+.w_LISCONT4/100),5)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(16,37,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_LIKEYSAL with this.w_LIKEYSAL
      replace t_LICODMAG with this.w_LICODMAG
      replace t_LIIMPCOL with this.w_LIIMPCOL
      replace t_LIIMPNAZ with this.w_LIIMPNAZ
      replace t_LIIMPPRO with this.w_LIIMPPRO
      replace t_LIPERPRO with this.w_LIPERPRO
      replace t_LIIMPUCA with this.w_LIIMPUCA
      replace t_LISCOCL1 with this.w_LISCOCL1
      replace t_LISCOCL2 with this.w_LISCOCL2
      replace t_LIDATUCA with this.w_LIDATUCA
      replace t_LIROWNUM with this.w_LIROWNUM
      replace t_LICODLIS with this.w_LICODLIS
      replace t_LISCOLIS with this.w_LISCOLIS
      replace t_LIPROLIS with this.w_LIPROLIS
      replace t_LIPROSCO with this.w_LIPROSCO
      replace t_LIQTAUM1 with this.w_LIQTAUM1
      replace t_LILISCHK with this.w_LILISCHK
      replace t_LICHKUCA with this.w_LICHKUCA
      replace t_LIIMPSCO with this.w_LIIMPSCO
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_FZDXUAMYQC()
    with this
          * --- Inizializzazione righe
          gsve_biu(this;
              ,'INIT';
             )
    endwith
  endproc
  proc Calculate_MNDABWVQVW()
    with this
          * --- Salvataggio dati righe
          gsve_biu(this;
              ,'CONFIRM';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_9.visible=!this.oPgFrm.Page1.oPag.oBtn_1_9.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
        if lower(cEvent)==lower("CaricaRighe")
          .Calculate_FZDXUAMYQC()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("SalvaeChiudi")
          .Calculate_MNDABWVQVW()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oLISCONT1_2_9.value==this.w_LISCONT1)
      this.oPgFrm.Page1.oPag.oLISCONT1_2_9.value=this.w_LISCONT1
      replace t_LISCONT1 with this.oPgFrm.Page1.oPag.oLISCONT1_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oLISCONT2_2_10.value==this.w_LISCONT2)
      this.oPgFrm.Page1.oPag.oLISCONT2_2_10.value=this.w_LISCONT2
      replace t_LISCONT2 with this.oPgFrm.Page1.oPag.oLISCONT2_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oLISCONT3_2_11.value==this.w_LISCONT3)
      this.oPgFrm.Page1.oPag.oLISCONT3_2_11.value=this.w_LISCONT3
      replace t_LISCONT3 with this.oPgFrm.Page1.oPag.oLISCONT3_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oLISCONT4_2_12.value==this.w_LISCONT4)
      this.oPgFrm.Page1.oPag.oLISCONT4_2_12.value=this.w_LISCONT4
      replace t_LISCONT4 with this.oPgFrm.Page1.oPag.oLISCONT4_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLIROWORD_2_1.value==this.w_LIROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLIROWORD_2_1.value=this.w_LIROWORD
      replace t_LIROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLIROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLICODICE_2_2.value==this.w_LICODICE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLICODICE_2_2.value=this.w_LICODICE
      replace t_LICODICE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLICODICE_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLIDESART_2_3.value==this.w_LIDESART)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLIDESART_2_3.value=this.w_LIDESART
      replace t_LIDESART with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLIDESART_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLIUNIMIS_2_6.value==this.w_LIUNIMIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLIUNIMIS_2_6.value=this.w_LIUNIMIS
      replace t_LIUNIMIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLIUNIMIS_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLIQTAMOV_2_7.value==this.w_LIQTAMOV)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLIQTAMOV_2_7.value=this.w_LIQTAMOV
      replace t_LIQTAMOV with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLIQTAMOV_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLIPREZZO_2_8.value==this.w_LIPREZZO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLIPREZZO_2_8.value=this.w_LIPREZZO
      replace t_LIPREZZO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLIPREZZO_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLIFLCPRO_2_13.RadioValue()==this.w_LIFLCPRO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLIFLCPRO_2_13.SetRadio()
      replace t_LIFLCPRO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLIFLCPRO_2_13.value
    endif
    cp_SetControlsValueExtFlds(this,'CHECKUCA')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (this.cTrsName);
       where not(deleted()) and (not(Empty(t_LIROWORD)) and not(Empty(t_LICODICE)));
        into cursor __chk__
    if not(max(0,1)<=cnt)
      do cp_ErrorMsg with MSG_YOU_MUST_INSERT_AT_LEAST_ONE_ROW
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_LIROWORD)) and not(Empty(.w_LICODICE))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_LIROWORD)) and not(Empty(t_LICODICE)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_LIROWORD=0
      .w_LICODICE=space(41)
      .w_LIDESART=space(40)
      .w_LIKEYSAL=space(40)
      .w_LICODMAG=space(5)
      .w_LIUNIMIS=space(3)
      .w_LIQTAMOV=0
      .w_LIPREZZO=0
      .w_LISCONT1=0
      .w_LISCONT2=0
      .w_LISCONT3=0
      .w_LISCONT4=0
      .w_LIFLCPRO=space(1)
      .w_LIIMPCOL=0
      .w_LIIMPNAZ=0
      .w_LIIMPPRO=0
      .w_LIPERPRO=0
      .w_LIIMPUCA=0
      .w_LISCOCL1=0
      .w_LISCOCL2=0
      .w_LIDATUCA=ctod("  /  /  ")
      .w_LIROWNUM=0
      .w_LICODLIS=space(5)
      .w_LISCOLIS=space(5)
      .w_LIPROLIS=space(5)
      .w_LIPROSCO=space(5)
      .w_LIQTAUM1=0
      .w_LILISCHK=space(5)
      .w_LICHKUCA=space(1)
      .w_LIIMPSCO=0
      .DoRTCalc(1,13,.f.)
        .w_LIFLCPRO = 'N'
        .w_LIIMPCOL = cp_Round(.w_LIPREZZO * (1+.w_LISCONT1/100)*(1+.w_LISCONT2/100)*(1+.w_LISCONT3/100)*(1+.w_LISCONT4/100),5)
    endwith
    this.DoRTCalc(16,37,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_LIROWORD = t_LIROWORD
    this.w_LICODICE = t_LICODICE
    this.w_LIDESART = t_LIDESART
    this.w_LIKEYSAL = t_LIKEYSAL
    this.w_LICODMAG = t_LICODMAG
    this.w_LIUNIMIS = t_LIUNIMIS
    this.w_LIQTAMOV = t_LIQTAMOV
    this.w_LIPREZZO = t_LIPREZZO
    this.w_LISCONT1 = t_LISCONT1
    this.w_LISCONT2 = t_LISCONT2
    this.w_LISCONT3 = t_LISCONT3
    this.w_LISCONT4 = t_LISCONT4
    this.w_LIFLCPRO = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLIFLCPRO_2_13.RadioValue(.t.)
    this.w_LIIMPCOL = t_LIIMPCOL
    this.w_LIIMPNAZ = t_LIIMPNAZ
    this.w_LIIMPPRO = t_LIIMPPRO
    this.w_LIPERPRO = t_LIPERPRO
    this.w_LIIMPUCA = t_LIIMPUCA
    this.w_LISCOCL1 = t_LISCOCL1
    this.w_LISCOCL2 = t_LISCOCL2
    this.w_LIDATUCA = t_LIDATUCA
    this.w_LIROWNUM = t_LIROWNUM
    this.w_LICODLIS = t_LICODLIS
    this.w_LISCOLIS = t_LISCOLIS
    this.w_LIPROLIS = t_LIPROLIS
    this.w_LIPROSCO = t_LIPROSCO
    this.w_LIQTAUM1 = t_LIQTAUM1
    this.w_LILISCHK = t_LILISCHK
    this.w_LICHKUCA = t_LICHKUCA
    this.w_LIIMPSCO = t_LIIMPSCO
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_LIROWORD with this.w_LIROWORD
    replace t_LICODICE with this.w_LICODICE
    replace t_LIDESART with this.w_LIDESART
    replace t_LIKEYSAL with this.w_LIKEYSAL
    replace t_LICODMAG with this.w_LICODMAG
    replace t_LIUNIMIS with this.w_LIUNIMIS
    replace t_LIQTAMOV with this.w_LIQTAMOV
    replace t_LIPREZZO with this.w_LIPREZZO
    replace t_LISCONT1 with this.w_LISCONT1
    replace t_LISCONT2 with this.w_LISCONT2
    replace t_LISCONT3 with this.w_LISCONT3
    replace t_LISCONT4 with this.w_LISCONT4
    replace t_LIFLCPRO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLIFLCPRO_2_13.ToRadio()
    replace t_LIIMPCOL with this.w_LIIMPCOL
    replace t_LIIMPNAZ with this.w_LIIMPNAZ
    replace t_LIIMPPRO with this.w_LIIMPPRO
    replace t_LIPERPRO with this.w_LIPERPRO
    replace t_LIIMPUCA with this.w_LIIMPUCA
    replace t_LISCOCL1 with this.w_LISCOCL1
    replace t_LISCOCL2 with this.w_LISCOCL2
    replace t_LIDATUCA with this.w_LIDATUCA
    replace t_LIROWNUM with this.w_LIROWNUM
    replace t_LICODLIS with this.w_LICODLIS
    replace t_LISCOLIS with this.w_LISCOLIS
    replace t_LIPROLIS with this.w_LIPROLIS
    replace t_LIPROSCO with this.w_LIPROSCO
    replace t_LIQTAUM1 with this.w_LIQTAUM1
    replace t_LILISCHK with this.w_LILISCHK
    replace t_LICHKUCA with this.w_LICHKUCA
    replace t_LIIMPSCO with this.w_LIIMPSCO
    if i_srv='A'
      replace LIROWNUM with this.w_LIROWNUM
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsve_miuPag1 as StdContainer
  Width  = 786
  height = 270
  stdWidth  = 786
  stdheight = 270
  resizeXpos=436
  resizeYpos=184
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="HBNWIGPNUX",left=2, top=7, width=780,height=19,;
    caption='Object',;
   bGlobalFont=.t.,;
    nNumberColumn=7,Field1="LIROWORD",Label1="Riga",Field2="LICODICE",Label2="Articolo",Field3="LIDESART",Label3="Descrizione",Field4="LIUNIMIS",Label4="U.M.",Field5="LIQTAMOV",Label5="Quantit�",Field6="LIPREZZO",Label6="Prezzo unitario",Field7="LIFLCPRO",Label7="Provv.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 181523686


  add object oBtn_1_7 as StdButton with uid="HZPJQRFJAB",left=681, top=222, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per aggiornare i dati del documento con quelli inseriti";
    , HelpContextID = 160803175;
    , Caption='\<Aggiorna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_8 as StdButton with uid="YKTSCIFCSO",left=731, top=222, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 57810950;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_9 as StdButton with uid="GJQKCBDMMT",left=268, top=222, width=48,height=45,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere al dettaglio";
    , HelpContextID = 257526986;
    , TabStop=.f., Caption='\<Dettaglio';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      do GSVE_KIU with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_LICODICE))
    endwith
   endif
  endfunc

  add object oStr_1_3 as StdString with uid="UPJHRDMLDK",Visible=.t., Left=66, Top=249,;
    Alignment=0, Width=9, Height=15,;
    Caption="+"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="BTXCUYMVGK",Visible=.t., Left=131, Top=249,;
    Alignment=0, Width=9, Height=15,;
    Caption="+"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="WJRQCMMGJK",Visible=.t., Left=196, Top=249,;
    Alignment=0, Width=9, Height=15,;
    Caption="+"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="IQANBLKFBS",Visible=.t., Left=11, Top=226,;
    Alignment=0, Width=245, Height=15,;
    Caption="Sconti / maggiorazioni di riga"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-8,top=25,;
    width=777+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-7,top=26,width=776+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oLISCONT1_2_9.Refresh()
      this.Parent.oLISCONT2_2_10.Refresh()
      this.Parent.oLISCONT3_2_11.Refresh()
      this.Parent.oLISCONT4_2_12.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oLISCONT1_2_9 as StdTrsField with uid="WMQLVJJWFZ",rtseq=10,rtrep=.t.,;
    cFormVar="w_LISCONT1",value=0,enabled=.f.,;
    HelpContextID = 57560039,;
    cTotal="", bFixedPos=.t., cQueryName = "LISCONT1",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=11, Top=245, cSayPict=["999.99"], cGetPict=["999.99"]

  add object oLISCONT2_2_10 as StdTrsField with uid="FCOZNCLDJO",rtseq=11,rtrep=.t.,;
    cFormVar="w_LISCONT2",value=0,enabled=.f.,;
    HelpContextID = 57560040,;
    cTotal="", bFixedPos=.t., cQueryName = "LISCONT2",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=74, Top=245, cSayPict=["999.99"], cGetPict=["999.99"]

  add object oLISCONT3_2_11 as StdTrsField with uid="PBAKBNCVHJ",rtseq=12,rtrep=.t.,;
    cFormVar="w_LISCONT3",value=0,enabled=.f.,;
    HelpContextID = 57560041,;
    cTotal="", bFixedPos=.t., cQueryName = "LISCONT3",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=140, Top=245, cSayPict=["999.99"], cGetPict=["999.99"]

  add object oLISCONT4_2_12 as StdTrsField with uid="OZROTXQIAT",rtseq=13,rtrep=.t.,;
    cFormVar="w_LISCONT4",value=0,enabled=.f.,;
    HelpContextID = 57560042,;
    cTotal="", bFixedPos=.t., cQueryName = "LISCONT4",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=205, Top=245, cSayPict=["999.99"], cGetPict=["999.99"]

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsve_miuBodyRow as CPBodyRowCnt
  Width=767
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oLIROWORD_2_1 as StdTrsField with uid="GPITJAPDVC",rtseq=2,rtrep=.t.,;
    cFormVar="w_LIROWORD",value=0,;
    HelpContextID = 83508218,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"], readonly=.T.

  add object oLICODICE_2_2 as StdTrsField with uid="WTRAPGNKZM",rtseq=3,rtrep=.t.,;
    cFormVar="w_LICODICE",value=space(41),enabled=.f.,;
    HelpContextID = 231295995,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=126, Left=48, Top=0, InputMask=replicate('X',41)

  add object oLIDESART_2_3 as StdTrsField with uid="OSIWEXVDKA",rtseq=4,rtrep=.t.,;
    cFormVar="w_LIDESART",value=space(40),enabled=.f.,;
    HelpContextID = 112155658,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=280, Left=176, Top=0, InputMask=replicate('X',40)

  add object oLIUNIMIS_2_6 as StdTrsField with uid="ABGVARJUSR",rtseq=7,rtrep=.t.,;
    cFormVar="w_LIUNIMIS",value=space(3),enabled=.f.,;
    HelpContextID = 233214967,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=34, Left=460, Top=0, InputMask=replicate('X',3)

  add object oLIQTAMOV_2_7 as StdTrsField with uid="EYDOALKTXC",rtseq=8,rtrep=.t.,;
    cFormVar="w_LIQTAMOV",value=0,enabled=.f.,;
    HelpContextID = 27208716,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=97, Left=498, Top=0, cSayPict=["99999999.999"], cGetPict=["99999999.999"]

  add object oLIPREZZO_2_8 as StdTrsField with uid="WKUPPJCNQS",rtseq=9,rtrep=.t.,;
    cFormVar="w_LIPREZZO",value=0,;
    HelpContextID = 19063803,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=139, Left=599, Top=0, cSayPict=["999999999999.99999"], cGetPict=["999999999999.99999"]

  add object oLIFLCPRO_2_13 as StdTrsCheck with uid="BNLBWCEZKN",rtrep=.t.,;
    cFormVar="w_LIFLCPRO", enabled=.f., caption="",;
    HelpContextID = 79068165,;
    Left=742, Top=0, Width=20,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oLIFLCPRO_2_13.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..LIFLCPRO,&i_cF..t_LIFLCPRO),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oLIFLCPRO_2_13.GetRadio()
    this.Parent.oContained.w_LIFLCPRO = this.RadioValue()
    return .t.
  endfunc

  func oLIFLCPRO_2_13.ToRadio()
    this.Parent.oContained.w_LIFLCPRO=trim(this.Parent.oContained.w_LIFLCPRO)
    return(;
      iif(this.Parent.oContained.w_LIFLCPRO=='S',1,;
      0))
  endfunc

  func oLIFLCPRO_2_13.SetRadio()
    this.value=this.ToRadio()
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oLIROWORD_2_1.When()
    return(.t.)
  proc oLIROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oLIROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_miu','CHECKUCA','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".LISERIAL=CHECKUCA.LISERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
