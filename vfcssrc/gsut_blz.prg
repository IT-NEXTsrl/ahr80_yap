* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_blz                                                        *
*              Eventi GSUT_KLZ (Esportazione dati azeinde)                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-09-27                                                      *
* Last revis.: 2009-03-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPARAM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_blz",oParentObject,m.pPARAM)
return(i_retval)

define class tgsut_blz as StdBatch
  * --- Local variables
  pPARAM = space(1)
  w_MSK = .NULL.
  w_ZOOMCUR = space(10)
  w_FILENAME = space(254)
  w_FOUTFILE = 0
  w_OUTSTR = space(190)
  w_CF = space(20)
  w_PI = space(20)
  w_RS = space(80)
  w_NA = space(2)
  w_CIFRATURA = space(20)
  w_CODISO = space(3)
  w_OLDCODAZI = space(5)
  w_CODNAZ = space(3)
  w_SPECCHAR = space(254)
  w_OLDCODFIS = space(16)
  w_OLDPARIVA = space(12)
  w_WARNMSG = .f.
  * --- WorkFile variables
  AZIENDA_idx=0
  NAZIONI_idx=0
  TMP_PART_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eventi da GSUT_KLZ
    if this.pPARAM # "C"
      this.w_MSK = this.oParentObject
      this.w_ZOOMCUR = this.w_MSK.w_ZOOMAZI.cCursor
    endif
    do case
      case this.pPARAM="S"
        if this.oParentObject.w_ZOOMSEL="S"
          if g_APPLICATION="ADHOC REVOLUTION"
            UPDATE (this.w_ZOOMCUR) set XCHK = 1 where azcodazi <> "START" and azcodazi <> "DEMO" and azcodazi <> "CONTB" and azcodazi <> "LEMCO" and (not empty(nvl(azcofazi," ")) or not empty(nvl(azpivazi," "))) and empty(CHRTRAN(nvl(AZRAGAZI," "),CHR(39)+'!"#$%&\\()*+,-./0123456789:;<=>?@abcdefghijklmnopqrstuvwxyz[]^_`\{|}~*ABCDEFGHIJKLMNOPQRSTUVWXYZ ',""))
          else
            UPDATE (this.w_ZOOMCUR) set XCHK = 1 where azcodazi <> "START" and azcodazi <> "DEMO" and azcodazi <> "ALFA " and azcodazi <> "BETA " and azcodazi <> "DELTA" and azcodazi <> "ESPOS" and azcodazi <> "OMEGA" and azcodazi <> "GCRED" and (not empty(nvl(azcofazi," ")) or not empty(nvl(azpivazi," "))) and empty(CHRTRAN(nvl(AZRAGAZI," "),CHR(39)+'!"#$%&\\()*+,-./0123456789:;<=>?@abcdefghijklmnopqrstuvwxyz[]^_`\{|}~*ABCDEFGHIJKLMNOPQRSTUVWXYZ ',""))
          endif
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          UPDATE (this.w_ZOOMCUR) set XCHK = 0 
        endif
        this.w_MSK.w_ZOOMAZI.grd.Refresh()     
      case this.pPARAM="C"
        * --- Creazione della tabella temporanea di appoggio per il visual zoom
        * --- Scorro l'elenco delle aziende correnti e per ognuna determino 
        *     se sono in demo o meno
        * --- Create temporary table TMP_PART
        i_nIdx=cp_AddTableDef('TMP_PART') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        i_nConn=i_TableProp[this.AZIENDA_idx,3] && recupera la connessione
        i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
        cp_CreateTempTable(i_nConn,i_cTempTable,"AZRAGAZI As AZRAGAZI,AZPIVAZI As AZPIVAZI,AZCOFAZI As AZCOFAZI,AZCODNAZ As AZCODNAZ,AZCODAZI As AZCODAZI "," from "+i_cTable;
              )
        this.TMP_PART_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- Select from TMP_PART
        i_nConn=i_TableProp[this.TMP_PART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMP_PART_idx,2],.t.,this.TMP_PART_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMP_PART ";
               ,"_Curs_TMP_PART")
        else
          select * from (i_cTable);
            into cursor _Curs_TMP_PART
        endif
        if used('_Curs_TMP_PART')
          select _Curs_TMP_PART
          locate for 1=1
          do while not(eof())
          this.w_OLDCODAZI = i_codazi
          i_Codazi = nvl(_Curs_TMP_PART.AZCODAZI,space(5))
          this.w_CODNAZ = nvl(_Curs_TMP_PART.AZCODNAZ,"   ")
          THIS.CHECKAZI(THIS) 
          * --- Read from NAZIONI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.NAZIONI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.NAZIONI_idx,2],.t.,this.NAZIONI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "NACODISO"+;
              " from "+i_cTable+" NAZIONI where ";
                  +"NACODNAZ = "+cp_ToStrODBC(this.w_CODNAZ);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              NACODISO;
              from (i_cTable) where;
                  NACODNAZ = this.w_CODNAZ;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CODISO = NVL(cp_ToDate(_read_.NACODISO),cp_NullValue(_read_.NACODISO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          i_Codazi = this.w_OLDCODAZI
          if ! empty(this.w_CIFRATURA)
            * --- Write into TMP_PART
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TMP_PART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMP_PART_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_PART_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"AZCODNAZ ="+cp_NullLink(cp_ToStrODBC(this.w_CODISO),'TMP_PART','AZCODNAZ');
              +",AZPIVAZI ="+cp_NullLink(cp_ToStrODBC(this.w_CIFRATURA),'TMP_PART','AZPIVAZI');
              +",AZCOFAZI ="+cp_NullLink(cp_ToStrODBC(this.w_CIFRATURA),'TMP_PART','AZCOFAZI');
              +",AZRAGAZI ="+cp_NullLink(cp_ToStrODBC(this.w_CIFRATURA),'TMP_PART','AZRAGAZI');
                  +i_ccchkf ;
              +" where ";
                  +"AZCODAZI = "+cp_ToStrODBC(_Curs_TMP_PART.AZCODAZI);
                     )
            else
              update (i_cTable) set;
                  AZCODNAZ = this.w_CODISO;
                  ,AZPIVAZI = this.w_CIFRATURA;
                  ,AZCOFAZI = this.w_CIFRATURA;
                  ,AZRAGAZI = this.w_CIFRATURA;
                  &i_ccchkf. ;
               where;
                  AZCODAZI = _Curs_TMP_PART.AZCODAZI;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          else
            * --- Write into TMP_PART
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TMP_PART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMP_PART_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_PART_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"AZCODNAZ ="+cp_NullLink(cp_ToStrODBC(this.w_CODISO),'TMP_PART','AZCODNAZ');
                  +i_ccchkf ;
              +" where ";
                  +"AZCODAZI = "+cp_ToStrODBC(_Curs_TMP_PART.AZCODAZI);
                     )
            else
              update (i_cTable) set;
                  AZCODNAZ = this.w_CODISO;
                  &i_ccchkf. ;
               where;
                  AZCODAZI = _Curs_TMP_PART.AZCODAZI;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
            select _Curs_TMP_PART
            continue
          enddo
          use
        endif
      case this.pPARAM="D"
        * --- Drop temporary table TMP_PART
        i_nIdx=cp_GetTableDefIdx('TMP_PART')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMP_PART')
        endif
      case this.pPARAM = "Z"
        SELECT (this.w_ZOOMCUR)
        if empty(nvl(azcofazi," ")) and empty(nvl(azpivazi," "))
          this.w_MSK.w_ZOOMAZI.grd.value = 0
          ah_errormsg("Selezione non ammessa, azienda priva di cod. fiscale e partita iva",48)
          replace XCHK with 0
          this.w_MSK.w_ZOOMAZI.grd.Refresh()     
        endif
        * --- Controllo duplicazione C.F. e P.I. e controllo presenza caratteri speciali
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      otherwise
        SELECT (this.w_ZOOMCUR) 
 Locate for xchk=1
        if !found()
          ah_errormsg("Selezionare almeno un elemento da esportare",48)
          i_retcode = 'stop'
          return
        endif
        if this.pPARAM="I"
          this.w_FILENAME = addbs(tempadhoc())+this.oParentObject.w_DIR
        else
          if DIRECTORY( JUSTPATH(this.oParentObject.w_DIR2) )
            this.w_FILENAME = this.oParentObject.w_DIR2
          else
            ah_errormsg("Il percorso specificato non esiste",48)
            i_retcode = 'stop'
            return
          endif
        endif
        if FILE(this.w_FILENAME)
          Erase this.w_FILENAME
        endif
        this.w_FOUTFILE = FCreate(this.w_FILENAME)
        Select (this.w_ZOOMCUR) 
 SCAN FOR XCHK=1
        this.w_CF = LEFT(NVL(AZCOFAZI,"")+space(20),20)
        this.w_PI = LEFT(NVL(AZPIVAZI,"")+space(20),20)
        this.w_RS = LEFT(NVL(AZRAGAZI,"")+space(80),80)
        this.w_NA = LEFT(NVL(AZCODNAZ,"")+space(3),3)
        this.w_OUTSTR = left(NVL(this.oParentObject.w_MATPRO,"")+space(10),10) + this.w_CF + this.w_PI + this.w_RS + this.w_NA +chr(13)+chr(10)
        write=fWrite(this.w_FOUTFILE,this.w_OUTSTR)
        ENDSCAN
        close=fclose(this.w_FOUTFILE)
        if this.pPARAM="I"
           EMAIL( this.w_FILENAME ,"S")
        else
          ah_errormsg("Esportazione eseguita con successo",48)
          i_retcode = 'stop'
          return
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica correttezza selezione/i
    this.w_WARNMSG = .F.
    this.w_OLDCODFIS = ""
    this.w_OLDPARIVA = ""
     
 Select (this.w_ZOOMCUR) 
 Go Top
    Scan for XCHK=1
    if alltrim(this.w_OLDCODFIS) == alltrim(nvl(azcofazi,"")) and alltrim(this.w_OLDPARIVA) == alltrim(nvl(azpivazi,"")) and ! empty(nvl(azcofazi,"")+nvl(azpivazi,""))
      this.w_WARNMSG = .T.
    endif
    if this.w_WARNMSG
      this.w_WARNMSG = .F.
      this.w_MSK.w_ZOOMAZI.grd.value = 0
      if this.pParam="Z"
        ah_errormsg("Non � possibile esportare aziende che hanno la stessa partita iva e codice fiscale.",48)
      endif
      replace XCHK with 0
      this.w_MSK.w_ZOOMAZI.grd.Refresh()     
    endif
    * --- Controllo presenza di caratteri non ammessi in ragione sociale, C.F. e P.I.
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_OLDCODFIS = NVL(azcofazi,"")
    this.w_OLDPARIVA = nvl(azpivazi,"")
    Endscan
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo presenza di caratteri non ammessi in ragione sociale, C.F. e P.I.
    this.w_SPECCHAR = CHRTRAN(nvl(AZRAGAZI," "),CHR(39)+'!"#$%&\\()*+,-./0123456789:;<=>?@abcdefghijklmnopqrstuvwxyz[]^_`\{|}~*ABCDEFGHIJKLMNOPQRSTUVWXYZ ',"")
    this.w_SPECCHAR = this.w_SPECCHAR+CHRTRAN(nvl(AZCOFAZI," "),CHR(39)+'!"#$%&\\()*+,-./0123456789:;<=>?@abcdefghijklmnopqrstuvwxyz[]^_`\{|}~*ABCDEFGHIJKLMNOPQRSTUVWXYZ ',"")
    this.w_SPECCHAR = this.w_SPECCHAR+CHRTRAN(nvl(AZPIVAZI," "),CHR(39)+'!"#$%&\\()*+,-./0123456789:;<=>?@abcdefghijklmnopqrstuvwxyz[]^_`\{|}~*ABCDEFGHIJKLMNOPQRSTUVWXYZ ',"")
    if ! empty(this.w_SPECCHAR)
      this.w_MSK.w_ZOOMAZI.grd.value = 0
      if this.pParam="Z"
        ah_errormsg("Non � possibile inserire nei dati azienda ragione sociale, partita iva e codice fiscale che contengono caratteri speciali(es. lettere accentate): %1 ",48," ",this.w_SPECCHAR)
      endif
      replace XCHK with 0
      this.w_MSK.w_ZOOMAZI.grd.Refresh()     
    endif
  endproc


  proc Init(oParentObject,pPARAM)
    this.pPARAM=pPARAM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='NAZIONI'
    this.cWorkTables[3]='*TMP_PART'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_TMP_PART')
      use in _Curs_TMP_PART
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- gsut_blz
  PROCEDURE CHECKAZI
    parameters caller
    do case
      case alltrim(_Curs_TMP_PART.AZPIVAZI) == "01645690130"
        caller.w_CIFRATURA = '58244' + right(alltrim(_Curs_TMP_PART.AZPIVAZI),6)
      case alltrim(_Curs_TMP_PART.AZPIVAZI) == "00577140346"
        caller.w_CIFRATURA = '54389' + right(alltrim(_Curs_TMP_PART.AZPIVAZI),6)
      otherwise  
        caller.w_CIFRATURA = ''
    endcase  
  ENDPROC
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPARAM"
endproc
