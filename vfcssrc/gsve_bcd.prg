* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bcd                                                        *
*              Calcola prezzo listino da doc.                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_792]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1997-07-04                                                      *
* Last revis.: 2015-10-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pExec
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bcd",oParentObject,m.pExec)
return(i_retval)

define class tgsve_bcd as StdBatch
  * --- Local variables
  pExec = space(1)
  w_oMESS = .NULL.
  w_oPART = .NULL.
  w_SRV = space(1)
  w_GSAC_MMP = .NULL.
  w_SEROCL = space(15)
  w_OBJCTRL = .NULL.
  w_CHECK = .f.
  w_cKey = space(20)
  w_ARTICOLO = space(20)
  w_FLCESP = space(1)
  w_CODMAT = space(40)
  w_ARTCLI = space(1)
  w_FLLMAG = space(1)
  w_OKSCA = space(1)
  w_FLAVAL = space(1)
  w_FLCAUM = space(1)
  w_F2RISE = space(1)
  w_F2CASC = space(1)
  w_DATREG = ctod("  /  /  ")
  w_AGGDES = .f.
  w_PROG = space(3)
  w_DATCAL = ctod("  /  /  ")
  w_OLDTIPPRO = space(1)
  w_OLDTIPPR2 = space(1)
  w_ROWNUM = 0
  w_OK = .f.
  w_DESCOD = space(41)
  w_APPO = space(12)
  w_DESSUP = space(41)
  w_QTAUM1 = 0
  w_CODART = space(20)
  w_CODLIS = space(5)
  w_CODCON = space(15)
  w_CATCLI = space(5)
  w_CATART = space(5)
  w_ARRSUP = 0
  w_APPO1 = 0
  w_CODVAL = space(3)
  w_SCOCON = .f.
  w_CODGRU = space(5)
  w_OLIPREZ = 0
  w_QUACON = space(1)
  w_OCONTRA = space(15)
  w_OPREZZO = 0
  w_FLUCOA = space(1)
  w_OSCONT1 = 0
  w_OSCONT2 = 0
  w_OSCONT3 = 0
  w_OSCONT4 = 0
  w_VALCONTR = space(3)
  w_CODMAG = space(5)
  w_MLCODICE = space(20)
  w_VALULT = 0
  w_SCONT1 = 0
  w_SCONT2 = 0
  w_SCONT3 = 0
  w_SCONT4 = 0
  w_CODICE = space(5)
  w_FLARTI = space(1)
  w_CALCOLA = 0
  w_QTAMOV = 0
  w_PREZZO = 0
  w_NETTO = 0
  w_CALPRZ = 0
  w_RICSCO = .f.
  w_RICPRE = .f.
  w_RICTOT = .f.
  w_ORIF = space(1)
  w_MESS = space(1)
  w_QTAORI = 0
  w_QTAEVA = 0
  w_QTARIM = 0
  w_QTAIMP = 0
  w_QTAOLD = 0
  w_OQTAIMP = 0
  w_NQTAIMP = 0
  w_OLDIMP = 0
  w_QTARIM1 = 0
  W_DTOBS1 = ctod("  /  /  ")
  w_PADRE = .NULL.
  w_FLFRAZ1 = space(1)
  w_MODUM2 = space(1)
  w_QTATEST = 0
  w_MAGSAL = space(5)
  w_TESTPRZ = 0
  w_ORIGINE = space(10)
  w_LENSCF = 0
  w_O_OLDEVAS = space(1)
  w_RESIDUO = 0
  w_ROWPADRE = 0
  w_ROWINDEX = 0
  w_PMVFLERIF = space(1)
  w_PFLNOEV = space(1)
  w_PMQTAIMP = 0
  w_PMQTAIM1 = 0
  w_SERRIF = space(10)
  w_ROWRIF = 0
  w_NUMREC = 0
  w_TmpN = 0
  w_QTAUM3 = 0
  w_QTALIS = 0
  * --- WorkFile variables
  TRADARTI_idx=0
  ART_ICOL_idx=0
  LOTTIART_idx=0
  DOC_DETT_idx=0
  CAM_AGAZ_idx=0
  UNIMIS_idx=0
  SALDIART_idx=0
  KEY_ARTI_idx=0
  VOCIIVA_idx=0
  ART_ALTE_idx=0
  MOVIMATR_idx=0
  PAR_RIOR_idx=0
  runtime_filters = 2

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola Listino o esegue lo Scorporo (da GSVE_MDV, GSOR_MDV, GSAC_MDV)
    * --- Lanciato da:
    * --- M = Cambio Articolo;
    *     A = Cambio Quantita';
    *     R = Automatismi (ev. Ricalcola);
    *     S = Zoom su Prezzo (Scorporo);
    *     U = Unita' di Misura; 
    *     P = Qta in 1^ U.M.
    *     Z = Automatismi (ev. RicNoCont) non considera gestione contratti;
    *     Q = Da Import (GSVE_BI3) CALPRZLI Ricalcola solo le Quantit�
    *     C= Commessa
    * --- w_PADRE = potrebbe essere GSVE_MDV, GSAC_MDV, GSOR_MDV
    this.w_PADRE = this.oParentObject
    if this.pExec="S" AND IsAlt()
      * --- Se Alter Ego allora non esegue lo scorporo
      * --- Notifico che la riga � variata
      this.oParentObject.w_MVPREZZO = gsag_bmp(.Null.,"V",this.oParentObject.w_MVPREZZO,this.oParentObject.w_DEPREMAX,this.oParentObject.w_DEPREMIN,this.oParentObject.w_MVCODART)
      this.w_PADRE.SetupdateRow()     
      i_retcode = 'stop'
      return
    endif
    if this.pExec="C" AND g_AGEN="S"
      DO GSAG_BDD WITH this.w_PADRE, "PRA"
      i_retcode = 'stop'
      return
    endif
    * --- istanzio oggetto per mess incrementali
    this.w_oMESS=createobject("ah_message")
    this.w_SRV = this.w_PADRE.RowStatus()
    msg=""
    this.w_DATREG = IIF(EMPTY(this.oParentObject.w_MVDATDOC), MAX(this.oParentObject.w_MVDATREG,this.oParentObject.w_MVDATEVA), MAX(this.oParentObject.w_MVDATDOC,this.oParentObject.w_MVDATEVA))
    * --- Read from UNIMIS
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.UNIMIS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "UMFLFRAZ,UMMODUM2"+;
        " from "+i_cTable+" UNIMIS where ";
            +"UMCODICE = "+cp_ToStrODBC(this.oParentObject.w_UNMIS1);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        UMFLFRAZ,UMMODUM2;
        from (i_cTable) where;
            UMCODICE = this.oParentObject.w_UNMIS1;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_FLFRAZ1 = NVL(cp_ToDate(_read_.UMFLFRAZ),cp_NullValue(_read_.UMFLFRAZ))
      this.w_MODUM2 = NVL(cp_ToDate(_read_.UMMODUM2),cp_NullValue(_read_.UMMODUM2))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if Not(this.pExec$"PR") OR this.oParentObject.w_MVQTAUM1=0 OR this.oParentObject.w_FLUSEP=" "
      * --- Se non modifico a mano la Qt� nella Prima U.M.
      *     La ricalcolo poich� non � ancora stata calcolata dall'mCalc
      this.oParentObject.w_MVQTAUM1 = IIF(this.oParentObject.w_MVTIPRIG="F" or (this.oParentObject.w_MVTIPRIG="M" AND IsAlt() and this.pExec="M"), 1, CALQTA(this.oParentObject.w_MVQTAMOV,this.oParentObject.w_MVUNIMIS,this.oParentObject.w_UNMIS2,this.oParentObject.w_OPERAT, this.oParentObject.w_MOLTIP, this.oParentObject.w_FLUSEP, this.w_FLFRAZ1, this.w_MODUM2, @msg, this.oParentObject.w_UNMIS3, this.oParentObject.w_OPERA3, this.oParentObject.w_MOLTI3))
    endif
    * --- Modifica della quantit� con Qt�<>0 e non movimentata la prima U.M. non per Servizi a valore
    *     Solo se non attivato check Ricalcolo qt� da Lotto di Riordino. 
    *     In questo caso Pag 5 viene lanciata dopo il lancio della funzione CALPRZLI
    if this.pExec$"AU" And this.oParentObject.w_MVQTAMOV<>0 And this.oParentObject.w_MVUNIMIS<>this.oParentObject.w_UNMIS1 And this.oParentObject.w_MVTIPRIG<>"F" And this.oParentObject.w_FLQRIO <>"S"
      this.Page_5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- w_NUMGES= 0 -> se proveniente da caricamento rapido =1 non deve eseguire questa parte
    if this.pExec="A" and NOT EMPTY(this.oParentObject.w_MVSERRIF) AND this.oParentObject.w_MVROWRIF<>0 And this.oParentObject.w_NUMGES=0
      * --- Cambio quantit� in una riga importata da altro documento
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Aggiorno la lista materiali per il rientro conto lavoro
      if VARTYPE( this.oParentObject.w_AVASERIA ) <> "U"
        if g_COLA="S" and this.oParentObject.w_MVFLVEAC="A" and (NOT EMPTY(this.oParentObject.w_AVASERIA) or this.pExec="A") and this.w_PADRE.w_MVCLADOC $ "DT-FA"
          * --- Read from DOC_DETT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DOC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MVCODODL"+;
              " from "+i_cTable+" DOC_DETT where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MVSERRIF);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_MVROWRIF);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MVCODODL;
              from (i_cTable) where;
                  MVSERIAL = this.oParentObject.w_MVSERRIF;
                  and CPROWNUM = this.oParentObject.w_MVROWRIF;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_SEROCL = NVL(cp_ToDate(_read_.MVCODODL),cp_NullValue(_read_.MVCODODL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if not empty(this.w_SEROCL)
            * --- Faccio una query per verificare la provenienza dell'OCL/ODA per evitare che la tabella ODL_MAST dia errore a chi non ha la produzione
            * --- Select from query\gsvepbcd
            do vq_exec with 'query\gsvepbcd',this,'_Curs_query_gsvepbcd','',.f.,.t.
            if used('_Curs_query_gsvepbcd')
              select _Curs_query_gsvepbcd
              locate for 1=1
              do while not(eof())
              if OLTPROVE="L"
                this.w_GSAC_MMP = this.oparentobject.GSAC_MMP
                * --- Istanzio l'oggetto cos� sono sicuro che sia presente
                this.w_GSAC_MMP.LinkPCClick()     
                * --- Nuovo Oggetto
                this.w_GSAC_MMP = this.oparentobject.GSAC_MMP
                * --- Chiudo la maschera
                this.w_GSAC_MMP.LinkPCClick()     
                * --- Ricalcolo il detail
                GSAC_BM1(this,"Ricalcola")
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
                select _Curs_query_gsvepbcd
                continue
              enddo
              use
            endif
          endif
        endif
      endif
      * --- Rimetto a 0 le variabili che vengono valorizzate in pagina 3 e 4 e poi riutilizzate in pag 1
      this.w_QTAUM1 = 0
      this.w_QTAMOV = 0
      this.w_ROWNUM = 0
      if this.oParentObject.w_MVFLERIF<>"S"
        * --- Solo in Importazione documenti.
        *     Se non forzo l'evasione della riga del documento di Origine non devo rifare i calcoli.
        *     In questo caso non deve nemmeno lanciare la mCalc()
        *     
        *     w_MVFLERIF = 'S' se 
        *     -  Evado per una quantit� maggiore rispetto al documento di origine: non fa la domanda
        *               'Evado la Quantita Rimanente sul Documento di Origine?' ma potrebbero esserci condizioni pi� vantaggiose
        *               (altro scaglione sul contratto o listino) 
        *     -  Evdo per una quantit� minore rispetto al documento di origine e rispondo di si alla domanda
        *               'Evado la Quantita Rimanente sul Documento di Origine?' 
        *     
        *     In questi casi devo ricalcolare il Prezzo/Sconti
        * --- Non esegue la mCalc
        this.bUpdateParentObject = .F.
        i_retcode = 'stop'
        return
      endif
    endif
    if ( this.pExec="U" and empty(this.oParentObject.w_MVUNIMIS) ) Or (this.pExec="M" and empty(this.oParentObject.w_MVCODICE))
      * --- Cambio unit� di misura da cp_zoom, il campo � ancora vuoto e bisogna ancora selezionare l'unit�.
      *     oppure
      *     Nel caso viene notificato un cambio articolo per posizionamento automatico su riga di append con codice articolo vuoto
      *     In questo caso non deve eseguire calcoli
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_MVTIPRIG $ "RMAF" AND (this.pExec="M" OR this.oParentObject.w_MVQTAMOV=0) AND NOT EMPTY(this.oParentObject.w_MVCODICE)
      * --- Eventuale Default da Causale Documento
      this.oParentObject.w_MVQTAMOV = IIF(this.oParentObject.w_MVQTAMOV=0, this.oParentObject.w_QTADEF, this.oParentObject.w_MVQTAMOV)
      if this.pExec="M"
        this.oParentObject.w_MVQTAMOV = IIF(this.oParentObject.w_MVTIPRIG="F", 1, this.oParentObject.w_MVQTAMOV)
      endif
      * --- Devo leggere i dati dell'articolo in caso di quantita' proposta in automatico 
      *     Lasciando invariato l'input sulla qta non verrebbero inizializzati gli sconti ecc.
      if this.pExec="M" AND ( NOT EMPTY(this.oParentObject.w_MVCODART) AND (this.oParentObject.w_MVQTAMOV<>0 OR (g_PERLOT="S")))
        * --- Rileggo flag causale magazzino poich� nel caso di causale predefinita 
        *     diversa da quella utilizzata non sono ancora risettati
        * --- Read from CAM_AGAZ
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CMFLRISE,CMFLCASC"+;
            " from "+i_cTable+" CAM_AGAZ where ";
                +"CMCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVCAUMAG);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CMFLRISE,CMFLCASC;
            from (i_cTable) where;
                CMCODICE = this.oParentObject.w_MVCAUMAG;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_MVFLRISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
          this.oParentObject.w_MVFLCASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.oParentObject.w_MVQTAMOV<>0 
          * --- Svolgo il Link su MVCODART (On Changed MVCODICE non aggiorna Link collegati)
          this.w_OBJCTRL = this.w_PADRE.GetCtrl( "w_MVCODART" )
          L_Method_Name="this.w_PADRE.Link"+Right( this.w_OBJCTRL.Name , LEN( this.w_OBJCTRL.Name ) - rat("_" , this.w_OBJCTRL.Name , 2)+1)+"('Full',this)"
          this.w_CHECK = &L_Method_Name
          * --- Determino il codice IVA
          this.oParentObject.w_MVCODIVA = CALCODIV(this.oParentObject.w_MVCODART, this.oParentObject.w_MVTIPCON, this.oParentObject.w_XCONORN, this.oParentObject.w_MVTIPOPE, IIF( this.oParentObject.w_MVFLVEAC="V", this.oParentObject.w_MVDATREG , this.oParentObject.w_MVDATDOC ))
          this.oParentObject.w_MVUNIMIS = IIF(NOT EMPTY(this.oParentObject.w_UNMIS3) AND this.oParentObject.w_MOLTI3<>0, this.oParentObject.w_UNMIS3, this.oParentObject.w_UNMIS1)
          this.oParentObject.w_MVCODLIS = this.oParentObject.w_MVTCOLIS
          * --- Rileggo anche l'ultimo costo di acquisto che nel caso di quantit� proposta in automatico 
          *     non sarebbe ancora valorizzato
          * --- Read from SALDIART
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.SALDIART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "SLVALUCA,SLCODVAA,SLCODVAV"+;
              " from "+i_cTable+" SALDIART where ";
                  +"SLCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVCODART);
                  +" and SLCODMAG = "+cp_ToStrODBC(this.oParentObject.w_MVCODMAG);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              SLVALUCA,SLCODVAA,SLCODVAV;
              from (i_cTable) where;
                  SLCODICE = this.oParentObject.w_MVCODART;
                  and SLCODMAG = this.oParentObject.w_MVCODMAG;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_VALUCA = NVL(cp_ToDate(_read_.SLVALUCA),cp_NullValue(_read_.SLVALUCA))
            this.oParentObject.w_CODVAA = NVL(cp_ToDate(_read_.SLCODVAA),cp_NullValue(_read_.SLCODVAA))
            this.oParentObject.w_CODVAV = NVL(cp_ToDate(_read_.SLCODVAV),cp_NullValue(_read_.SLCODVAV))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Rileggo anche la percentuale Iva 
          * --- Read from VOCIIVA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VOCIIVA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "IVPERIVA"+;
              " from "+i_cTable+" VOCIIVA where ";
                  +"IVCODIVA = "+cp_ToStrODBC(this.oParentObject.w_MVCODIVA);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              IVPERIVA;
              from (i_cTable) where;
                  IVCODIVA = this.oParentObject.w_MVCODIVA;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_PERIVA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Ricalcolo anche la prima UM
          this.oParentObject.w_MVQTAUM1 = IIF(this.oParentObject.w_MVTIPRIG="F", 1, CALQTA(this.oParentObject.w_MVQTAMOV,this.oParentObject.w_MVUNIMIS,this.oParentObject.w_UNMIS2,this.oParentObject.w_OPERAT, this.oParentObject.w_MOLTIP, this.oParentObject.w_FLUSEP, this.w_FLFRAZ1, this.w_MODUM2, @msg, this.oParentObject.w_UNMIS3, this.oParentObject.w_OPERA3, this.oParentObject.w_MOLTI3))
        endif
      endif
    endif
    if this.pExec= "P" And this.oParentObject.w_FLUSEP = "Q"
      * --- Cambio della quantit� della prima unit� di misura con U.M. Separate su Per Quantit�
      *     Non devo effettuare nessun ricalcolo.
      i_retcode = 'stop'
      return
    endif
    if NOT EMPTY(msg)
      * --- Errore relativo alla quantit� della 1a UM
      Ah_ErrorMsg(msg)
    endif
    * --- Attiva il Post-In relativo al Codice Articolo associato al Codice di Ricerca
    if this.pExec="M" AND NOT EMPTY(this.oParentObject.w_MVCODART)
      this.w_cKey = cp_setAzi(i_TableProp[this.w_PADRE.ART_ICOL_IDX, 2]) + "\" + cp_ToStr(this.oParentObject.w_MVCODART, 1)
      cp_ShowWarn(this.w_cKey, this.w_PADRE.ART_ICOL_IDX)
      if this.oParentObject.w_MVTIPRIG="D"
        * --- Nel caso di modifica codice articolo e inserita riga descrittiva, azzero il prezzo
        *     che altrimenti rimarrebbe quello dell'articolo.
        *     Questo errore si presenta solo se il prezzo precedente era stato scritto a mano e non calcolato 
        *     da Contratto/listino o altro.
        this.oParentObject.w_MVPREZZO = 0
      endif
    endif
    if this.oParentObject.w_MVTIPRIG = "F" 
      this.oParentObject.w_MVCODLIS = this.oParentObject.w_MVTCOLIS
    endif
    this.w_CODART = this.oParentObject.w_MVCODART
    if this.pExec="M" 
      if g_CESP="S" And (NOT EMPTY(this.oParentObject.w_MVCESSER) OR NOT EMPTY(this.oParentObject.w_MVCODCES))
        * --- Leggo il codice articolo associato al codice di ricerca presente prima
        *     della modifica. Se quello precedente gestiva i cespiti non posso modificarlo
        * --- Read from KEY_ARTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CACODART"+;
            " from "+i_cTable+" KEY_ARTI where ";
                +"CACODICE = "+cp_ToStrODBC(this.oParentObject.o_MVCODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CACODART;
            from (i_cTable) where;
                CACODICE = this.oParentObject.o_MVCODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_ARTICOLO = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARFLCESP"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_ARTICOLO);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARFLCESP;
            from (i_cTable) where;
                ARCODART = this.w_ARTICOLO;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FLCESP = NVL(cp_ToDate(_read_.ARFLCESP),cp_NullValue(_read_.ARFLCESP))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_FLCESP="S"
          * --- Eseguo Controllo riga di tipo cespite
          ah_ErrorMsg("Attenzione, servizio di tipologia cespite impossibile modificare","i")
          this.w_AGGDES = .T.
        endif
      endif
      if g_MATR="S" AND this.oParentObject.w_GESMAT="S" 
        if this.oParentObject.w_TOTMAT<0
          * --- Read from MOVIMATR
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.MOVIMATR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MTCODMAT"+;
              " from "+i_cTable+" MOVIMATR where ";
                  +"MTSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL);
                  +" and MTROWNUM = "+cp_ToStrODBC(this.oParentObject.w_CPROWNUM);
                  +" and MTNUMRIF = "+cp_ToStrODBC(this.oParentObject.w_MVNUMRIF);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MTCODMAT;
              from (i_cTable) where;
                  MTSERIAL = this.oParentObject.w_MVSERIAL;
                  and MTROWNUM = this.oParentObject.w_CPROWNUM;
                  and MTNUMRIF = this.oParentObject.w_MVNUMRIF;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CODMAT = NVL(cp_ToDate(_read_.MTCODMAT),cp_NullValue(_read_.MTCODMAT))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if this.oParentObject.w_TOTMAT>0 OR NOT EMPTY(this.w_CODMAT)
          * --- Eseguo Controllo riga movimentata a matricole
          ah_ErrorMsg("Attenzione, articolo associato a movimenti matricole impossibile modificare","i")
          this.w_AGGDES = .T.
        endif
      endif
      if g_EACD="S" AND this.oParentObject.w_TESCOMP
        ah_ErrorMsg("Attenzione, riga non modificabile in quanto associata a documenti di esplosione componenti evasi","i","")
        this.oParentObject.w_MVCODICE = this.oParentObject.o_MVCODICE
        this.w_AGGDES = .T.
      endif
      if Isalt() and this.oParentObject.w_MVRIGPRE="S"
        this.oParentObject.w_MVCODICE = this.oParentObject.o_MVCODICE
        this.w_AGGDES = .T.
      endif
      if g_VEFA="S" AND Not Empty(this.oParentObject.w_MVRIFESC) And this.oParentObject.w_MVTIPIMB<>"N"
        ah_ErrorMsg("Attenzione, riga non modificabile in quanto ha associato un documento di imballi","i","")
        this.w_AGGDES = .T.
      endif
      if this.w_AGGDES
        * --- Non posso modificare l'articolo, ripristino il vecchio codice...
        if Isalt()
          Ah_ErrorMsg("Attenzione riga collegata a righe spese\anticipazioni.%0Modifica articolo non consentita!",48,"")
        else
          Ah_ErrorMsg("Modifica articolo non consentita!",48,"")
        endif
        this.oParentObject.w_MVCODICE = this.oParentObject.o_MVCODICE
        * --- Read from KEY_ARTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CADESART,CADESSUP,CA__TIPO,CACODART"+;
            " from "+i_cTable+" KEY_ARTI where ";
                +"CACODICE = "+cp_ToStrODBC(this.oParentObject.w_MVCODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CADESART,CADESSUP,CA__TIPO,CACODART;
            from (i_cTable) where;
                CACODICE = this.oParentObject.w_MVCODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_MVDESART = NVL(cp_ToDate(_read_.CADESART),cp_NullValue(_read_.CADESART))
          this.oParentObject.w_MVDESSUP = NVL(cp_ToDate(_read_.CADESSUP),cp_NullValue(_read_.CADESSUP))
          this.oParentObject.w_MVTIPRIG = NVL(cp_ToDate(_read_.CA__TIPO),cp_NullValue(_read_.CA__TIPO))
          this.oParentObject.w_MVCODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        i_retcode = 'stop'
        return
      endif
      * --- Read from KEY_ARTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CADTOBSO,CALENSCF,CATIPCON"+;
          " from "+i_cTable+" KEY_ARTI where ";
              +"CACODICE = "+cp_ToStrODBC(this.oParentObject.w_MVCODICE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CADTOBSO,CALENSCF,CATIPCON;
          from (i_cTable) where;
              CACODICE = this.oParentObject.w_MVCODICE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DTOBS1 = NVL(cp_ToDate(_read_.CADTOBSO),cp_NullValue(_read_.CADTOBSO))
        this.w_LENSCF = NVL(cp_ToDate(_read_.CALENSCF),cp_NullValue(_read_.CALENSCF))
        this.w_ARTCLI = NVL(cp_ToDate(_read_.CATIPCON),cp_NullValue(_read_.CATIPCON))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Se � stata modificata la data di obsolescenza di un articolo in un ordine allora non deve essere visualizzato
      if NOT EMPTY(this.W_DTOBS1) AND this.W_DTOBS1<=this.oParentObject.W_OBTEST AND NOT EMPTY(this.oParentObject.w_MVCODICE)
        this.oParentObject.w_MVCODICE = Space(20)
        this.oParentObject.w_MVDESART = Space(40)
        ah_ErrorMsg("Codice articolo obsoleto dal %1",,"", dtoc(this.w_dtobs1) )
        i_retcode = 'stop'
        return
      endif
      if g_FLCESC = "S" And ((this.w_LENSCF<>0 And Right(Alltrim(this.oParentObject.w_MVCODICE), this.w_LENSCF) <> IIF(Empty(Alltrim(this.oParentObject.w_CODESC)),"######",Alltrim(this.oParentObject.w_CODESC))) Or (this.w_LENSCF=0 And Not Empty(this.oParentObject.w_CODESC) And this.w_ARTCLI$"C-F"))
        this.w_oMESS.AddMsgPartNL("Codice articolo incongruente con intestatario")     
        if Empty(this.oParentObject.w_CODESC)
          this.w_oPART = this.w_oMESS.addmsgpartNL("Nessun suffisso codice di ricerca legato a %1")
          this.w_oPART.addParam(Alltrim(this.oParentObject.w_MVCODCON))     
        else
          this.w_oPART = this.w_oMESS.addmsgpartNL("Suffisso codice di ricerca legato a %1 = %2")
          this.w_oPART.addParam(Alltrim(this.oParentObject.w_MVCODCON))     
          this.w_oPART.addParam(this.oParentObject.w_CODESC)     
        endif
        if this.w_LENSCF = 0
          this.w_oMESS.AddMsgPartNL("Nessun suffisso assegnato al codice di ricerca di tipo cliente")     
        else
          this.w_oPART = this.w_oMESS.addmsgpartNL("Suffisso assegnato al codice di ricerca = %1")
          this.w_oPART.addParam(Right(Alltrim(this.oParentObject.w_MVCODICE),this.w_LENSCF))     
        endif
        this.w_oMESS.Ah_ErrorMsg()     
        this.oParentObject.w_MVCODICE = Space(20)
        this.oParentObject.w_MVDESART = Space(40)
        i_retcode = 'stop'
        return
      endif
      * --- Consumo Automatico
      if g_PERLOT="S" AND (this.oParentObject.w_MVFLCASC="-" OR this.oParentObject.w_MVFLRISE="+") AND this.oParentObject.w_MVTIPRIG $ "RMA"
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARFLLMAG,ARFLLOTT"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_MVCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARFLLMAG,ARFLLOTT;
            from (i_cTable) where;
                ARCODART = this.oParentObject.w_MVCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FLLMAG = NVL(cp_ToDate(_read_.ARFLLMAG),cp_NullValue(_read_.ARFLLMAG))
          this.oParentObject.w_FLLOTT = NVL(cp_ToDate(_read_.ARFLLOTT),cp_NullValue(_read_.ARFLLOTT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.oParentObject.w_FLLOTT="C"
          * --- Lotti a Consumo Automatico
          this.w_OKSCA = "F"
          if Not Empty(this.oParentObject.w_MVCAUMAG)
            this.w_FLCAUM = Left(Alltrim(this.oParentObject.w_MVFLCASC)+IIF(this.oParentObject.w_MVFLRISE="+", "-", IIF(this.oParentObject.w_MVFLRISE="-", "+", " ")),1)
            this.w_OKSCA = IIF((this.oParentObject.w_FLAVA1="A" and this.w_FLCAUM="-") or (this.oParentObject.w_FLAVA1="V" and this.w_FLCAUM="+") ,"T","F")
          endif
          if Not Empty(this.oParentObject.w_MVCAUCOL) AND this.w_OKSCA="F"
            * --- Read from CAM_AGAZ
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CMFLAVAL,CMFLRISE,CMFLCASC"+;
                " from "+i_cTable+" CAM_AGAZ where ";
                    +"CMCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVCAUCOL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CMFLAVAL,CMFLRISE,CMFLCASC;
                from (i_cTable) where;
                    CMCODICE = this.oParentObject.w_MVCAUCOL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_FLAVAL = NVL(cp_ToDate(_read_.CMFLAVAL),cp_NullValue(_read_.CMFLAVAL))
              this.w_F2RISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
              this.w_F2CASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_FLCAUM = Left(Alltrim(this.w_F2CASC)+IIF(this.w_F2RISE="+", "-", IIF(this.w_F2RISE="-", "+", " ")),1)
            this.w_OKSCA = IIF((this.w_FLAVAL="A" and this.w_FLCAUM="-") or (this.w_FLAVAL="V" and this.w_FLCAUM="+") ,"T","F")
          endif
          * --- Se No Forza Magazzino Lotto cerco movimenti di carico del Lotto con il magazzino
          *     impostato di Default
          this.w_CODMAG = IIF(this.w_FLLMAG="S",Space(5),CALCMAG(1, this.oParentObject.w_FLMGPR, "     ", this.oParentObject.w_COMAG, this.oParentObject.w_OMAG, this.oParentObject.w_MAGPRE, this.oParentObject.w_MAGTER))
          this.oParentObject.w_MVCODMAG = IIF(this.w_FLLMAG="S",this.oParentObject.w_MVCODMAG,CALCMAG(1, this.oParentObject.w_FLMGPR, "     ", this.oParentObject.w_COMAG, this.oParentObject.w_OMAG, this.oParentObject.w_MAGPRE, this.oParentObject.w_MAGTER))
          this.w_DATREG = IIF(EMPTY(this.oParentObject.w_MVDATDOC), MAX(this.oParentObject.w_MVDATREG,this.oParentObject.w_MVDATEVA), MAX(this.oParentObject.w_MVDATDOC,this.oParentObject.w_MVDATEVA))
          vq_exec("..\MADV\EXE\QUERY\GSMD_QCA.VQR",this,"LOTTI")
          * --- Cerco il Lotto con data scadenza pi� prossima alla data registrazione e 
          *     a parit� di  data scadenza con data creazione pi� vecchia.
          this.oParentObject.w_MVCODLOT = CERCLOT(MAX(this.oParentObject.w_MVDATREG, this.oParentObject.w_MVDATEVA),this.w_CODART,"LOTTI")
          if NOT EMPTY(this.oParentObject.w_MVCODLOT)
            * --- Assegno Magazzino e Ubicazione congruenti al lotto se consumo automatico
            if RECCOUNT("LOTTI")>0
              SELECT LOTTI
              LOCATE FOR LOTTI.LOCODICE = this.oParentObject.w_MVCODLOT AND LOTTI.CODART=this.oParentObject.w_MVCODART
              if FOUND()
                * --- Assegno Magazzino e Ubicazione congruenti al lotto se consumo automatico
                this.oParentObject.w_MVCODMAG = NVL(LOTTI.CODMAG,SPACE(5))
                this.oParentObject.w_MVCODUBI = NVL(LOTTI.LOCODUBI,SPACE(20))
                this.oParentObject.w_DATLOT = CP_TODATE(LOTTI.LODATSCA)
                this.oParentObject.w_FLSTAT = "D"
              endif
            endif
            SELECT LOTTI
            USE
          endif
        endif
      endif
      if this.oParentObject.w_FLLOTT <>"C" OR this.oParentObject.w_MVTIPRIG $ "DF"
        this.oParentObject.w_MVCODLOT = SPACE(20)
      endif
    endif
    * --- Traduzione in Lingua della Descrizione Articoli (da GSVE_MDV)
    if this.pExec="M" AND (NOT (EMPTY(this.oParentObject.w_MVCODICE) OR EMPTY(this.oParentObject.w_CODLIN) OR this.oParentObject.w_CODLIN=g_CODLIN))
      * --- Legge la Traduzione
      DECLARE ARRRIS (2) 
 a=Tradlin(this.oParentObject.w_MVCODICE,this.oParentObject.w_CODLIN,"TRADARTI",@ARRRIS,"B") 
 this.w_DESCOD=ARRRIS(1) 
 this.w_DESSUP=ARRRIS(2)
      if Not Empty(this.w_DESCOD)
        this.oParentObject.w_MVDESART = this.w_DESCOD
        this.oParentObject.w_MVDESSUP = this.w_DESSUP
      endif
    endif
    if this.oParentObject.w_NOPRSC="S" Or ( this.oParentObject.w_ARPUNPAD And g_COAC="S")
      * --- Se la causale documento non gestisc prezzi/sconti o
      *     riga di tipo contributo accessorio esce senza calcolare i prezzi
      i_retcode = 'stop'
      return
    endif
    this.w_AGGDES = .F.
    this.w_CODLIS = this.oParentObject.w_MVCODLIS
    this.w_CODCON = this.oParentObject.w_MVCONTRA
    this.w_CODVAL = this.oParentObject.w_MVCODVAL
    this.w_CATCLI = this.oParentObject.w_CATSCC
    this.w_ARRSUP = IIF(this.oParentObject.w_DECTOT=0, .499, .00499)
    this.w_SCOCON = .F.
    this.w_OLIPREZ = this.oParentObject.w_MVPREZZO
    this.w_OCONTRA = this.oParentObject.w_MVCONTRA
    this.w_OSCONT1 = this.oParentObject.w_MVSCONT1
    this.w_OSCONT2 = this.oParentObject.w_MVSCONT2
    this.w_OSCONT3 = this.oParentObject.w_MVSCONT3
    this.w_OSCONT4 = this.oParentObject.w_MVSCONT4
    this.w_OPREZZO = -9876
    if (this.pExec="A" OR this.pExec="P" OR this.pExec="U" Or this.pExec="M") AND this.oParentObject.w_MVPREZZO<>0 AND (NOT EMPTY(this.w_CODLIS) OR NOT EMPTY(this.w_CODCON) Or this.oParentObject.w_PRZVAC = "S")
      * --- Segna il Prezzo di origine per eventuale non Conferma finale
      this.w_OPREZZO = this.oParentObject.w_MVPREZZO
    endif
    if this.pExec="S"AND this.oParentObject.w_ACTSCOR=.F. And this.oParentObject.w_MVPREZZO<>0
      if this.oParentObject.w_MVFLSCOR = "S"
        this.w_MESS = "Vuoi incorporare l'IVA nel prezzo di riga?"
        this.w_NETTO = cp_ROUND(this.oParentObject.w_MVPREZZO * (1 + (this.oParentObject.w_PERIVA / 100)),this.oParentObject.w_DECUNI)
      else
        this.w_MESS = "Vuoi scorporare il prezzo di riga?"
        this.w_NETTO = cp_ROUND(this.oParentObject.w_MVPREZZO / (1 + (this.oParentObject.w_PERIVA / 100)),this.oParentObject.w_DECUNI)
      endif
      if ah_YesNo(this.w_MESS)
        * --- Scorpora
        this.oParentObject.w_ACTSCOR = .T.
        this.oParentObject.w_IVASCOR = this.oParentObject.w_MVPREZZO - this.w_NETTO
        this.oParentObject.w_MVPREZZO = this.w_NETTO
        * --- Notifico che la riga � variata
        this.w_PADRE.SetupdateRow()     
      endif
      i_retcode = 'stop'
      return
    endif
    if this.pExec="S" AND this.oParentObject.w_ACTSCOR And Not(ah_YesNo("Vuoi ricalcolare il prezzo di riga?"))
      * --- F9 sul prezzo per la seconda volta (ACTSCOR=.T.) riporto il prezzo nella situazione originaria (lo ricalcolo)
      * --- Valorizzo o_LIPREZZO con w_LIPREZZO per evitare il ricalcolo
      *     del prezzo nel caso in cui l'utente modifica il prezzo a mano
      *     a seguito di un F9 sul prezzo.
      if empty(this.oParentObject.w_LIPREZZO)
        this.oParentObject.o_LIPREZZO = this.oParentObject.w_LIPREZZO
      endif
      i_retcode = 'stop'
      return
    endif
    if (this.oParentObject.w_MVQTAMOV=0 AND this.oParentObject.w_MVTIPRIG $ "RMA") OR EMPTY(this.w_CODART)
      * --- Se non Specifico la Quantita' o l'Articolo esce
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_MVQTAMOV<>0 AND this.oParentObject.w_MVTIPRIG $ "RMA" And this.oParentObject.w_MVUNIMIS <> this.oParentObject.o_MVUNIMIS And this.pExec="M"
      * --- Se a riga gi� valorizzata,  modifico il codice articolo con un suo codice di ricerca che ha altra unit� di misura,
      *     rilancio questo batch GSVE_BCD con parametro U per ricalcolare il giusto prezzo secondo la nuova unit� di misura
      this.w_PADRE.NotifyEvent("w_MVUNIMIS Changed")     
      i_retcode = 'stop'
      return
    endif
    * --- Le lettura precedente su ART_ICOL nel caso di unit� di misura vuota esegue L'NVL con ' ' come secondo parametro
    *     UNMISx deve essere obbligatoriamente di 3 caratteri par far si che CALMMLIS funzioni correttamente
    this.w_APPO = (IIF(Empty(this.oParentObject.w_UNMIS1),Space(3),this.oParentObject.w_UNMIS1))+(IIF(Empty(this.oParentObject.w_UNMIS2),Space(3),this.oParentObject.w_UNMIS2))+(IIF(Empty(this.oParentObject.w_UNMIS3),Space(3),this.oParentObject.w_UNMIS3))
    * --- Ricalcolo Quantit� nella Prima unit� di misura
    *     Solo se non � gi� stata calcolata in precedenza per attivazione flag:
    *     Forza U.M. secondaria 
    if this.w_QTAUM1=0
      if this.pExec $ "PRSZQ" AND this.oParentObject.w_MVQTAUM1<>0
        * --- Modificata 1^UM
        this.w_QTAUM1 = this.oParentObject.w_MVQTAUM1
      else
        this.w_QTAUM1 = CALQTA(this.oParentObject.w_MVQTAMOV,this.oParentObject.w_MVUNIMIS,this.oParentObject.w_UNMIS2,this.oParentObject.w_OPERAT, this.oParentObject.w_MOLTIP, this.oParentObject.w_FLUSEP, this.w_FLFRAZ1, this.w_MODUM2, , this.oParentObject.w_UNMIS3, this.oParentObject.w_OPERA3, this.oParentObject.w_MOLTI3,,"I")
      endif
    endif
    DECLARE ARRCALC (16,1)
    DIMENSION pArrUm[9]
    * --- Memorizzo la qt� nella 1^ UM prima di lanciare CALPRZLI poich� all'interno
    *     della funzione potrebbe essere modificata da il Ricalcolo da Lotto di Riordino
    this.w_QTATEST = this.w_QTAUM1
    this.w_PROG = "V" + IIF(Empty(this.oParentObject.w_FLQRIO)," ",this.oParentObject.w_FLQRIO) + IIF(Empty(this.oParentObject.w_FLPREV)," ",this.oParentObject.w_FLPREV)+ IIF(this.pExec="Q","S"," ")
    this.w_DATCAL = IIF(EMPTY(this.oParentObject.w_MVDATDOC), this.oParentObject.w_MVDATREG, this.oParentObject.w_MVDATDOC)
    this.Pag6()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.w_QTATEST <> ARRCALC(13) And ARRCALC(13)<>0
      if this.pExec="R" Or this.pExec="Q" Or ah_YesNo("Ricalcolare la quantit� da lotto di riordino?")
        this.w_QTAUM1 = ARRCALC(13)
        if this.oParentObject.w_UNMIS1 <> this.oParentObject.w_MVUNIMIS
          * --- Se ho movimentato una U.M diversa dalla prima devo ricalcolare la Qt� Movimentata
          *     Utilizzo CALMMLIS invertendo gli operatori
          this.oParentObject.w_MVQTAMOV = CALQTA(this.w_QTAUM1,this.oParentObject.w_MVUNIMIS,IIF(Empty(this.oParentObject.w_UNMIS2),Space(3),this.oParentObject.w_UNMIS2),IIF(this.oParentObject.w_OPERAT="/","*","/"), this.oParentObject.w_MOLTIP, this.oParentObject.w_FLUSEP, "N", this.w_MODUM2, , IIF(Empty(this.oParentObject.w_UNMIS3),Space(3),this.oParentObject.w_UNMIS3), IIF(this.oParentObject.w_OPERA3="/","*","/"), this.oParentObject.w_MOLTI3)
          * --- Se unit� di misura non frazionabile porto all'intero superiore
          if this.oParentObject.w_FLFRAZ="S" And this.oParentObject.w_MVQTAMOV <> Int(this.oParentObject.w_MVQTAMOV)
            this.oParentObject.w_MVQTAMOV = Int(this.oParentObject.w_MVQTAMOV) + 1
          endif
        else
          * --- Altrimenti riassegno alla Qt� movimentata quella ricalcolata da Lotto di Riordino
          this.oParentObject.w_MVQTAMOV = this.w_QTAUM1
        endif
        * --- A questo punto ricalcolo anche la quantit� nella prima U.M.
        if Not(this.pExec$"PR") OR this.oParentObject.w_MVQTAUM1=0 OR this.oParentObject.w_FLUSEP=" "
          this.oParentObject.w_MVQTAUM1 = IIF(this.oParentObject.w_MVTIPRIG="F", 1, CALQTA(this.oParentObject.w_MVQTAMOV,this.oParentObject.w_MVUNIMIS,this.oParentObject.w_UNMIS2,this.oParentObject.w_OPERAT, this.oParentObject.w_MOLTIP, this.oParentObject.w_FLUSEP, this.w_FLFRAZ1, this.w_MODUM2, @msg, this.oParentObject.w_UNMIS3, this.oParentObject.w_OPERA3, this.oParentObject.w_MOLTI3))
        endif
        if this.pExec$"AU" And this.oParentObject.w_MVQTAMOV<>0 And this.oParentObject.w_MVUNIMIS<>this.oParentObject.w_UNMIS1 And this.oParentObject.w_MVTIPRIG<>"F"
          * --- Ricalcolo ancora Qt� se attivato Check nelle Unit� di Misura: 
          *     Forza U.M. Secondaria
          *     In questo caso il prezzo � ricalcolato su Quantit� da Lotto di Riordino.
          *     Questo ultimo ricalcolo della quantit� non ha conseguenze sul prezzo
          this.Page_5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        this.Pag6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    * --- Ricalcolo la data prevista evasione
    if this.oParentObject.w_FLPREV = "S"
      * --- Se attivo Check Calcola Data Prevista Evasione 
      *     la calcolo su Data Documento + Giorni Approvvigionamento
      *     ARRCALC(14) -> GG approvv. letti da contratto o Articolo. Possono anche essere 0
      this.oParentObject.w_MVDATEVA = this.oParentObject.w_MVDATDOC + ARRCALC(14)
    endif
    * --- MODPRO
    *     0 = Situazione Normale
    *     1 = Lancio il GSVE_BCD('R') solo per ricalcolo percentuale provvigione da contratto ( solo da GSVE_BFC )
    *     Operazione eseguita in CALPRZLI
    if this.oParentObject.w_MODPRO=0
      * --- Controllo se sono stati modificati prezzo o sconti
      this.w_RICSCO = IIF(this.oParentObject.w_NUMSCO>0, this.oParentObject.w_MVSCONT1, 0)<>this.w_OSCONT1 And this.w_OSCONT1<>0
      this.w_RICSCO = this.w_RICSCO OR (IIF(this.oParentObject.w_NUMSCO>1, this.oParentObject.w_MVSCONT2, 0)<>this.w_OSCONT2 And this.w_OSCONT2<>0)
      this.w_RICSCO = this.w_RICSCO OR (IIF(this.oParentObject.w_NUMSCO>2, this.oParentObject.w_MVSCONT3, 0)<>this.w_OSCONT3 And this.w_OSCONT3<>0)
      this.w_RICSCO = this.w_RICSCO OR (IIF(this.oParentObject.w_NUMSCO>3, this.oParentObject.w_MVSCONT4, 0)<>this.w_OSCONT4 And this.w_OSCONT4<>0)
      * --- Eseguo l'arrotondamento per i problemi conosciuti sui decimali del Fox(in alcuni casi ne tiene pi� del dovuto)
      if this.pExec$"P" and this.oParentObject.w_FLUSEP<>" "
        * --- Nel caso cambio quantit� nella prima U.M ricalcolo il prezzo con CALMMPZ che non considera 
        *     il moltiplicatore ma le quantit� effettivamente digitate
        if this.oParentObject.w_UNMIS1=this.oParentObject.w_CLUNIMIS AND this.oParentObject.w_MVUNIMIS<>this.oParentObject.w_UNMIS1
          this.w_TESTPRZ = cp_Round(CALMMPZ(this.oParentObject.w_LIPREZZO, this.oParentObject.w_MVQTAMOV, this.oParentObject.w_MVQTAUM1, IIF(this.oParentObject.w_LISCON=2, this.oParentObject.w_IVALIS, IIF(this.oParentObject.w_LISCON=1, this.oParentObject.w_IVACON, "N")), IIF(this.oParentObject.w_MVFLSCOR="S", 0, this.oParentObject.w_PERIVA), this.oParentObject.w_DECUNI),this.oParentObject.w_DECUNI)
        else
          this.w_TESTPRZ = cp_Round(CALMMPZ(this.oParentObject.w_LIPREZZO, 1, 1, IIF(this.oParentObject.w_LISCON=2, this.oParentObject.w_IVALIS, IIF(this.oParentObject.w_LISCON=1, this.oParentObject.w_IVACON, "N")), IIF(this.oParentObject.w_MVFLSCOR="S", 0, this.oParentObject.w_PERIVA), this.oParentObject.w_DECUNI),this.oParentObject.w_DECUNI)
        endif
      else
        * --- Negli altri casi calcolo tramite CALMMLIS
        if EMPTY(this.oParentObject.w_CLUNIMIS) 
          this.w_TESTPRZ = cp_Round(CALMMLIS(this.oParentObject.w_LIPREZZO, this.w_APPO+this.oParentObject.w_MVUNIMIS+this.oParentObject.w_OPERAT+this.oParentObject.w_OPERA3+IIF(this.oParentObject.w_LISCON=2, this.oParentObject.w_IVALIS, IIF(this.oParentObject.w_LISCON=1, this.oParentObject.w_IVACON, "N"))+"P"+ALLTRIM(STR(this.oParentObject.w_DECUNI)), this.oParentObject.w_MOLTIP, this.oParentObject.w_MOLTI3, IIF(this.oParentObject.w_MVFLSCOR="S",0,this.oParentObject.w_PERIVA)),this.oParentObject.w_DECUNI)
        else
          this.w_TESTPRZ = cp_Round(CALMMLIS(this.oParentObject.w_LIPREZZO, this.w_APPO+this.oParentObject.w_MVUNIMIS+this.oParentObject.w_OPERAT+this.oParentObject.w_OPERA3+IIF(this.oParentObject.w_LISCON=2, this.oParentObject.w_IVALIS, IIF(this.oParentObject.w_LISCON=1, this.oParentObject.w_IVACON, "N"))+"P"+ALLTRIM(STR(this.oParentObject.w_DECUNI)), 1, 1, IIF(this.oParentObject.w_MVFLSCOR="S",0,this.oParentObject.w_PERIVA)),this.oParentObject.w_DECUNI)
        endif
      endif
      this.w_RICPRE = (this.w_TESTPRZ <>this.oParentObject.w_MVPREZZO) 
      * --- Al cambio della quantit� se modificato il prezzo e il Listino/Contratto sono gestiti a sconti 
      *     oppure gli sonti derivano dalla tabella Sconti Maggiorazioni chiedo se si vogliono modificare
      if (this.pExec="A" OR this.pExec="P" OR this.pExec="U" OR this.pExec="M") AND ((this.w_OPREZZO<>-9876 And this.w_RICPRE) Or this.w_RICSCO) AND !IsAlt()
        * --- Nella domanda di ricalcolo prezzi evidenzio anche il nuovo prezzo e sconti e l'origine di questi
        this.w_oMESS.AddMsgPartNL("Ricalcolo il prezzo e gli sconti in base al listino/contratto/tabella sconti maggiorazioni?")     
        do case
          case Arrcalc(7)=1
            this.w_oPART = this.w_oMESS.addmsgpartNL("Nuovo prezzo: %1 calcolato da contratto")
            this.w_oPART.addParam(Alltrim(Str(this.w_TESTPRZ,18,5)))     
          case Arrcalc(7)=2
            this.w_oPART = this.w_oMESS.addmsgpartNL("Nuovo prezzo: %1 calcolato da listino")
            this.w_oPART.addParam(Alltrim(Str(this.w_TESTPRZ,18,5)))     
          case Arrcalc(7)=3
            this.w_oPART = this.w_oMESS.addmsgpartNL("Nuovo prezzo: %1 calcolato da U.C.A., U.P.V")
            this.w_oPART.addParam(Alltrim(Str(this.w_TESTPRZ,18,5)))     
          otherwise
            this.w_oPART = this.w_oMESS.addmsgpartNL("Nuovo prezzo: %1 nessuna origine")
            this.w_oPART.addParam(Alltrim(Str(this.w_TESTPRZ,18,5)))     
        endcase
        if this.oParentObject.w_NUMSCO>0
          do case
            case Arrcalc(8)=1
              this.w_oPART = this.w_oMESS.addmsgpartNL("Nuovi sconti: %1 calcolati da contratto")
              this.w_oPART.addParam(Alltrim(Str(this.oParentObject.w_MVSCONT1,6,2))+IIF(this.oParentObject.w_NUMSCO>1,"   "+Alltrim(Str(this.oParentObject.w_MVSCONT2,6,2)),"") + IIF(this.oParentObject.w_NUMSCO>2,"   "+Alltrim(Str(this.oParentObject.w_MVSCONT3,6,2)),"")+IIF(this.oParentObject.w_NUMSCO>3,"   "+Alltrim(Str(this.oParentObject.w_MVSCONT4,6,2)),""))     
            case Arrcalc(8)=3
              this.w_oPART = this.w_oMESS.addmsgpartNL("Nuovi sconti: %1 calcolati da listino")
              this.w_oPART.addParam(Alltrim(Str(this.oParentObject.w_MVSCONT1,6,2))+IIF(this.oParentObject.w_NUMSCO>1,"   "+Alltrim(Str(this.oParentObject.w_MVSCONT2,6,2)),"") + IIF(this.oParentObject.w_NUMSCO>2,"   "+Alltrim(Str(this.oParentObject.w_MVSCONT3,6,2)),"")+IIF(this.oParentObject.w_NUMSCO>3,"   "+Alltrim(Str(this.oParentObject.w_MVSCONT4,6,2)),""))     
            case Arrcalc(8)=4
              this.w_oPART = this.w_oMESS.addmsgpartNL("Nuovi sconti: %1 calcolati da U.C.A., U.P.V")
              this.w_oPART.addParam(Alltrim(Str(this.oParentObject.w_MVSCONT1,6,2))+IIF(this.oParentObject.w_NUMSCO>1,"   "+Alltrim(Str(this.oParentObject.w_MVSCONT2,6,2)),"") + IIF(this.oParentObject.w_NUMSCO>2,"   "+Alltrim(Str(this.oParentObject.w_MVSCONT3,6,2)),"")+IIF(this.oParentObject.w_NUMSCO>3,"   "+Alltrim(Str(this.oParentObject.w_MVSCONT4,6,2)),""))     
            case Arrcalc(8)=2
              this.w_oPART = this.w_oMESS.addmsgpartNL("Nuovi sconti: %1 calcolati da tab. sconti/magg.")
              this.w_oPART.addParam(Alltrim(Str(this.oParentObject.w_MVSCONT1,6,2))+IIF(this.oParentObject.w_NUMSCO>1,"   "+Alltrim(Str(this.oParentObject.w_MVSCONT2,6,2)),"") + IIF(this.oParentObject.w_NUMSCO>2,"   "+Alltrim(Str(this.oParentObject.w_MVSCONT3,6,2)),"")+IIF(this.oParentObject.w_NUMSCO>3,"   "+Alltrim(Str(this.oParentObject.w_MVSCONT4,6,2)),""))     
            otherwise
              this.w_oPART = this.w_oMESS.addmsgpartNL("Nuovi sconti: %1 nessuna origine")
              this.w_oPART.addParam(Alltrim(Str(this.oParentObject.w_MVSCONT1,6,2))+IIF(this.oParentObject.w_NUMSCO>1,"   "+Alltrim(Str(this.oParentObject.w_MVSCONT2,6,2)),"") + IIF(this.oParentObject.w_NUMSCO>2,"   "+Alltrim(Str(this.oParentObject.w_MVSCONT3,6,2)),"")+IIF(this.oParentObject.w_NUMSCO>3,"   "+Alltrim(Str(this.oParentObject.w_MVSCONT4,6,2)),""))     
          endcase
        endif
        if NOT this.w_oMESS.ah_YesNo()
          * --- Ripristina
          this.oParentObject.w_LIPREZZO = this.w_OLIPREZ
          this.oParentObject.o_LIPREZZO = this.oParentObject.w_LIPREZZO
          this.oParentObject.w_MVCONTRA = this.w_OCONTRA
          this.oParentObject.w_MVSCONT1 = this.w_OSCONT1
          this.oParentObject.w_MVSCONT2 = this.w_OSCONT2
          this.oParentObject.w_MVSCONT3 = this.w_OSCONT3
          this.oParentObject.w_MVSCONT4 = this.w_OSCONT4
          i_retcode = 'stop'
          return
        else
          if (this.pExec="A" OR this.pExec="P" OR this.pExec="U" OR this.pExec="M") AND this.oParentObject.o_LIPREZZO=this.oParentObject.w_LIPREZZO
            * --- Forza il Ricalcolo se variate QTA
            this.oParentObject.o_LIPREZZO = this.oParentObject.w_LIPREZZO + 1
          endif
        endif
      endif
      this.w_RICTOT = .F.
      if this.pExec="S" AND this.w_OK=.F. AND this.oParentObject.w_ACTSCOR=.F. And this.oParentObject.w_MVPREZZO<>0
        this.w_RICTOT = .T.
        * --- Se non presente Listino Ripristina situazione pre Scorporo
        this.oParentObject.w_MVPREZZO = this.oParentObject.w_MVPREZZO+this.oParentObject.w_IVASCOR
        this.oParentObject.w_IVASCOR = 0
        * --- Notifico che la riga � variata
        this.w_PADRE.SetupdateRow()     
      endif
      if this.pExec $ "PRSUZM" AND (this.w_OK Or this.w_RICTOT)
        if Not this.w_RICTOT
          * --- Ricalcolo il prezzo solo se non deriva da Reincorporo dell'Iva 
          if this.pExec$"P" AND this.oParentObject.w_FLUSEP<>" "
            * --- Modificata 1^UM
            *     Ricalcolo il prezzo con CALMMPZ per U.M. separate
            *     e imposto o_LIPREZZO = w_LIPREZZO per evitare il ricalcolo effettuato sui documenti
            if this.oParentObject.w_UNMIS1=this.oParentObject.w_CLUNIMIS AND this.oParentObject.w_MVUNIMIS<>this.oParentObject.w_UNMIS1
              this.oParentObject.w_MVPREZZO = cp_Round(CALMMPZ(this.oParentObject.w_LIPREZZO, this.oParentObject.w_MVQTAMOV, this.oParentObject.w_MVQTAUM1, IIF(this.oParentObject.w_LISCON=2, this.oParentObject.w_IVALIS, IIF(this.oParentObject.w_LISCON=1, this.oParentObject.w_IVACON, "N")), IIF(this.oParentObject.w_MVFLSCOR="S", 0, this.oParentObject.w_PERIVA), this.oParentObject.w_DECUNI),this.oParentObject.w_DECUNI)
            else
              this.oParentObject.w_MVPREZZO = cp_Round(CALMMPZ(this.oParentObject.w_LIPREZZO, 1, 1, IIF(this.oParentObject.w_LISCON=2, this.oParentObject.w_IVALIS, IIF(this.oParentObject.w_LISCON=1, this.oParentObject.w_IVACON, "N")), IIF(this.oParentObject.w_MVFLSCOR="S", 0, this.oParentObject.w_PERIVA), this.oParentObject.w_DECUNI),this.oParentObject.w_DECUNI)
            endif
            this.oParentObject.o_LIPREZZO = this.oParentObject.w_LIPREZZO
          else
            * --- Negli altri casi ricalcolo con CALMMLIS che esegue il corretto arrotondamento
            if EMPTY(this.oParentObject.w_CLUNIMIS)
              this.oParentObject.w_MVPREZZO = cp_Round(CALMMLIS(this.oParentObject.w_LIPREZZO, this.w_APPO+this.oParentObject.w_MVUNIMIS+this.oParentObject.w_OPERAT+this.oParentObject.w_OPERA3+IIF(this.oParentObject.w_LISCON=2, this.oParentObject.w_IVALIS, IIF(this.oParentObject.w_LISCON=1, this.oParentObject.w_IVACON, "N"))+"P"+ALLTRIM(STR(this.oParentObject.w_DECUNI)), this.oParentObject.w_MOLTIP, this.oParentObject.w_MOLTI3, IIF(this.oParentObject.w_MVFLSCOR="S",0,this.oParentObject.w_PERIVA)),this.oParentObject.w_DECUNI)
            else
              this.oParentObject.w_MVPREZZO = cp_Round(CALMMLIS(this.oParentObject.w_LIPREZZO, this.w_APPO+this.oParentObject.w_MVUNIMIS+this.oParentObject.w_OPERAT+this.oParentObject.w_OPERA3+IIF(this.oParentObject.w_LISCON=2, this.oParentObject.w_IVALIS, IIF(this.oParentObject.w_LISCON=1, this.oParentObject.w_IVACON, "N"))+"P"+ALLTRIM(STR(this.oParentObject.w_DECUNI)), 1, 1, IIF(this.oParentObject.w_MVFLSCOR="S",0,this.oParentObject.w_PERIVA)),this.oParentObject.w_DECUNI)
            endif
          endif
        endif
        this.oParentObject.w_PREUM1 = IIF(this.oParentObject.w_MVQTAUM1=0, 0, cp_ROUND((this.oParentObject.w_MVPREZZO * this.oParentObject.w_MVQTAMOV) / this.oParentObject.w_MVQTAUM1, this.oParentObject.w_DECUNI))
        * --- Storno dalla variabile TOTALE il valore precedente di MVVALRIG e dopo gli riassegno quello nuovo
        this.oParentObject.w_TOTALE = this.oParentObject.w_TOTALE - this.oParentObject.w_MVVALRIG
        this.oParentObject.w_TOTMERCE = this.oParentObject.w_TOTMERCE - this.oParentObject.w_RIGNET
        this.oParentObject.w_MVVALRIG = CAVALRIG(this.oParentObject.w_MVPREZZO,this.oParentObject.w_MVQTAMOV, this.oParentObject.w_MVSCONT1,this.oParentObject.w_MVSCONT2,this.oParentObject.w_MVSCONT3,this.oParentObject.w_MVSCONT4,this.oParentObject.w_DECTOT)
        this.oParentObject.w_MVVALMAG = CAVALMAG(this.oParentObject.w_MVFLSCOR, this.oParentObject.w_MVVALRIG, this.oParentObject.w_MVIMPSCO, this.oParentObject.w_MVIMPACC, this.oParentObject.w_PERIVA, this.oParentObject.w_DECTOT, this.oParentObject.w_MVCODIVE, this.oParentObject.w_PERIVE )
        this.oParentObject.w_MVIMPNAZ = CAIMPNAZ(this.oParentObject.w_MVFLVEAC, this.oParentObject.w_MVVALMAG, this.oParentObject.w_MVCAOVAL, this.oParentObject.w_CAONAZ, this.oParentObject.w_MVDATDOC, this.oParentObject.w_MVVALNAZ, this.oParentObject.w_MVCODVAL, this.oParentObject.w_MVCODIVE, this.oParentObject.w_PERIVE, this.oParentObject.w_INDIVE, this.oParentObject.w_PERIVA, this.oParentObject.w_INDIVA )
        this.oParentObject.w_VISNAZ = iif(this.oParentObject.w_FLSCOR="S",this.oParentObject.w_MVIMPNAZ,cp_ROUND(this.oParentObject.w_MVIMPNAZ, this.oParentObject.w_DECTOP))
        this.oParentObject.w_MVIMPCOM = CAIMPCOM( IIF(Empty(this.oParentObject.w_MVCODCOM),"S", iif(this.oParentObject.w_MVFLEVAS="S" And Not Empty( this.oParentObject.w_MVFLORCO ),"S","N") ), this.oParentObject.w_MVVALNAZ, this.oParentObject.w_COCODVAL, this.oParentObject.w_MVIMPNAZ, this.oParentObject.w_CAONAZ, IIF(EMPTY(this.oParentObject.w_MVDATDOC), this.oParentObject.w_MVDATREG, this.oParentObject.w_MVDATDOC), this.oParentObject.w_DECCOM ) 
        this.oParentObject.w_RIGNET = IIF(this.oParentObject.w_MVFLOMAG="X" AND this.oParentObject.w_FLSERA<>"S", this.oParentObject.w_MVVALRIG, 0)
        this.oParentObject.w_TOTMERCE = this.oParentObject.w_TOTMERCE + this.oParentObject.w_RIGNET
        * --- Riassegno il nuovo valore di MVVALRIG
        this.oParentObject.w_TOTALE = this.oParentObject.w_TOTALE + this.oParentObject.w_MVVALRIG
        * --- Notifica che la Riga e' Stata Variata
        this.w_PADRE.SetupdateRow()     
      endif
      * --- Num. Max. Sconti Consentiti
      this.oParentObject.w_MVSCONT1 = IIF(this.oParentObject.w_NUMSCO>0, this.oParentObject.w_MVSCONT1, 0)
      this.oParentObject.w_MVSCONT2 = IIF(this.oParentObject.w_NUMSCO>1, this.oParentObject.w_MVSCONT2, 0)
      this.oParentObject.w_MVSCONT3 = IIF(this.oParentObject.w_NUMSCO>2, this.oParentObject.w_MVSCONT3, 0)
      this.oParentObject.w_MVSCONT4 = IIF(this.oParentObject.w_NUMSCO>3, this.oParentObject.w_MVSCONT4, 0)
    endif
    * --- Risetto MODPRO a 0 per eventuali successive modifiche nella maschera dei dati di riga
    this.oParentObject.w_MODPRO = 0
    * --- Se sul contratto trovo una percentuale provvigione diversa da 0 
    *     la applico cambiandoil tipo provvigione
    *     RICPROV valorizzata nel GSVE_BI3 in fase di importazione
    *     Se C caso di caricamento normale documento. Deve ricalcolare sempre
    *     Se N importazione documenti senza ricalcolo provvigioni. Questo batch non verr� lanciato in Importazine.
    *     Se P questo batch viene lanciato in importazione per ricalcolo prezzi, quindi, se ho attivo anche genera provvigioni faccio eseguire anche calcolo provvigioni 
    *     Se S solo ricalcolo provvigioni senza calcolo prezzo. questo batch non viene lanciato in import
    if this.oParentObject.w_MVFLVEAC<>"A" AND this.oParentObject.w_MVTIPRIG $ "RFMA" AND g_PERAGE="S" and this.pExec<>"Q" And (this.oParentObject.w_RICPROV = "C" Or this.oParentObject.w_RICPROV = "P")
      * --- Percentuale provvigione agente
      this.w_OLDTIPPRO = this.w_PADRE.Get( "w_MVTIPPRO" )
      * --- Se il batch � stato lanciato in importazione con ricalcolo prezzi e provvigioni (RICPROV$'P-C')
      *     non devo ricalcolare le provvigioni se nel documento precedente erano Forzate o Escluse
      if Not (this.w_OLDTIPPRO$"FO-ES" And this.oParentObject.w_RICPROV$"P-C")
        if ARRCALC(6)<>0 and this.oParentObject.w_MVTIPPRO$"CT-CC-ST-DC-ET" 
          * --- Percentuale Provvigione <>0.
          *     Imposto TIPPRO ='CC' : Calcolata da contratto
          this.oParentObject.w_MVPERPRO = ARRCALC(6)
          this.oParentObject.w_MVIMPPRO = 0
          this.oParentObject.w_MVTIPPRO = "CC"
        else
          * --- Azzera la percentuale provvigione e reimposta il tipo provvigione
          this.oParentObject.w_MVPERPRO = 0
          this.oParentObject.w_MVIMPPRO = 0
          if NOT ( this.w_OLDTIPPRO<>this.oParentObject.w_MVTIPPRO AND this.oParentObject.w_MVTIPPRO="CC" )
            * --- Se da Contratto o da Tabella Provvigioni con percentuale 0,
            *     reimposto da tabella provvigioni e ricalcolo la percentuale.
            *     (Se da contratto = 0, ricalcolo da tabella provvigioni dove potrebbe esistere una percentuale)
            this.oParentObject.w_MVTIPPRO = IIF(g_CALPRO="GI","CT",IIF(g_CALPRO="GD","ST","DC"))
          endif
          this.w_PADRE.Set("w_MVTIPPRO" , this.oParentObject.w_MVTIPPRO)     
        endif
      endif
      * --- Percentuale provvigione capoarea
      this.w_OLDTIPPR2 = this.w_PADRE.Get( "w_MVTIPPR2" )
      * --- Se il batch � stato lanciato in importazione con ricalcolo prezzi e provvigioni (RICPROV$'P-C')
      *     non devo ricalcolare le provvigioni se nel documento precedente erano Forzate o Escluse
      if Not (this.w_OLDTIPPR2$"FO-ES" And this.oParentObject.w_RICPROV$"P-C")
        if ARRCALC(15)<>0 and this.oParentObject.w_MVTIPPR2$"CT-CC-ST-DC-ET" 
          this.oParentObject.w_MVPROCAP = ARRCALC(15)
          this.oParentObject.w_MVIMPCAP = 0
          this.oParentObject.w_MVTIPPR2 = "CC"
        else
          * --- Azzera la percentuale provvigione e reimposta il tipo provvigione
          this.oParentObject.w_MVPROCAP = 0
          this.oParentObject.w_MVIMPCAP = 0
          if NOT ( this.w_OLDTIPPR2<>this.oParentObject.w_MVTIPPR2 AND this.oParentObject.w_MVTIPPR2="CC" )
            this.oParentObject.w_MVTIPPR2 = IIF(g_CALPRO="GI","CT",IIF(g_CALPRO="GD","ST","DC"))
            * --- Se da Contratto o da Tabella Provvigioni con percentuale 0,
            *     reimposto da tabella provvigioni e ricalcolo la percentuale.
            *     (Se da contratto = 0, ricalcolo da tabella provvigioni dove potrebbe esistere una percentuale)
          endif
          this.w_PADRE.Set("w_MVTIPPR2" , this.oParentObject.w_MVTIPPR2)     
        endif
      endif
    endif
    if this.pExec="S"
      * --- Valorizzo o_LIPREZZO con w_LIPREZZO per evitare il ricalcolo
      *     del prezzo nel caso in cui l'utente modifica il prezzo a mano
      *     a seguito di un F9 sul prezzo.
      this.oParentObject.o_LIPREZZO = this.oParentObject.w_LIPREZZO
    endif
    if this.pExec="M" AND g_AGEN="S"
      DO GSAG_BDD WITH this.w_PADRE, "PRE"
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inizializzazione Variabili Locali e Globali
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli al Cambio Quantita' sulle Righe (da GSVE_MDV, GSAC_MDV, GSOR_MDV)
    *     Check quantit� impegnata evasione
    if this.oParentObject.w_MVFLARIF<>" " AND this.w_PADRE.cFunction<>"Query"
      * --- Read from DOC_DETT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MVQTAMOV,MVQTAEVA,MVQTAUM1"+;
          " from "+i_cTable+" DOC_DETT where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MVSERRIF);
              +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_MVROWRIF);
              +" and MVNUMRIF = "+cp_ToStrODBC(this.oParentObject.w_MVNUMRIF);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MVQTAMOV,MVQTAEVA,MVQTAUM1;
          from (i_cTable) where;
              MVSERIAL = this.oParentObject.w_MVSERRIF;
              and CPROWNUM = this.oParentObject.w_MVROWRIF;
              and MVNUMRIF = this.oParentObject.w_MVNUMRIF;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_QTAORI = NVL(cp_ToDate(_read_.MVQTAMOV),cp_NullValue(_read_.MVQTAMOV))
        this.w_QTAEVA = NVL(cp_ToDate(_read_.MVQTAEVA),cp_NullValue(_read_.MVQTAEVA))
        this.w_QTAMOV = NVL(cp_ToDate(_read_.MVQTAMOV),cp_NullValue(_read_.MVQTAMOV))
        this.w_QTAUM1 = NVL(cp_ToDate(_read_.MVQTAUM1),cp_NullValue(_read_.MVQTAUM1))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Determino Quantit� totale importata dalle righe collegate alla stessa riga di origine
      this.w_QTAIMP = 0
      this.w_QTAOLD = 0
      this.w_OQTAIMP = this.oParentObject.w_MVQTAIMP
      this.w_QTARIM1 = this.oParentObject.o_MVQTAMOV-this.oParentObject.w_MVQTAMOV
      this.w_ROWNUM = this.oParentObject.w_CPROWNUM
      * --- Sommo le quantit� impegnate attuali ed originali..
      this.w_PADRE.Exec_Select("_Tmp_Sq_","Sum(t_MVQTAIMP) As t_IMP, Sum(MVQTAIMP) As Imp","NVL(t_MVSERRIF, SPACE(10)) = "+cp_ToStrODBC( this.oParentObject.w_MVSERRIF ) +" AND NVL(t_MVROWRIF, 0) = "+cp_ToStrODBC( this.oParentObject.w_MVROWRIF ) +" AND CPROWNUM <> "+cp_ToStrODBC( this.w_ROWNUM ) +" And Not Deleted() ","","","")     
       
 Select _Tmp_Sq_ 
 Go Top
      this.w_QTAIMP = Nvl( _Tmp_Sq_.t_IMP , 0 )
      this.w_QTAOLD = Nvl( _Tmp_Sq_.IMP , 0 )
      Use in _Tmp_Sq_
      this.w_ORIF = this.oParentObject.w_MVFLERIF
      this.w_O_OLDEVAS = this.oParentObject.w_OLDEVAS
      * --- Determino Quantit� rimanente da evadere
      *     w_QTAOLD: somma quantit�  importate (pre-modifica , 0 append) righe associate alla stessa riga di originaria
      *                          tranne la corrente 
      *     w_QTAIMP: somma quantit� importate righe associate alla stessa riga di originaria
      *                          tranne la corrente
      *     w_QTARIM: quantit� residua evadibile
      *     MVQTAIMP: vale 0 in Append e la quantit� importata old in caso di modifica
      this.w_QTARIM = (this.w_QTAORI-(this.w_QTAEVA-this.w_QTAOLD+this.w_QTAIMP)) + NVL( this.w_PADRE.Get( "MVQTAIMP" ) ,0)
      * --- Se non supero il tetto della quantit� movimentata del documento di origine confermo la quantit� movimentata
      this.oParentObject.w_MVQTAIMP = MIN(this.oParentObject.w_MVQTAMOV, this.w_QTARIM)
      this.w_RESIDUO = this.w_QTARIM-this.oParentObject.w_MVQTAIMP
      * --- Se la 1um non � frazionabile verifico se il residuo decimale � troppo piccolo per essere rievaso e lo aggiungo all'ultima riga evasa del documento
      if this.w_FLFRAZ1="S"
        this.oParentObject.w_MVQTAIMP = IIF( this.oParentObject.w_MVTIPRIG="D", 0 , CALQTAIMP(this.oParentObject.w_MVQTAIMP, this.w_RESIDUO, this.oParentObject.w_MVUNIMIS,this.oParentObject.w_UNMIS2,this.oParentObject.w_OPERAT, this.oParentObject.w_MOLTIP, this.oParentObject.w_FLUSEP, this.w_FLFRAZ1, this.oParentObject.w_UNMIS3, this.oParentObject.w_OPERA3, this.oParentObject.w_MOLTI3))
      endif
      this.w_NQTAIMP = this.oParentObject.w_MVQTAIMP
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Se la riga non � di tipo controibuto accessorio allora
      *     non faccio domande ma ricerco il padre e applico la solita regola
      *     di evasione, questo cmq sia in fase di aggiornamento della qt� in
      *     automatico che manuale da parte dell'utente
      *     (se modulo disattivo allora mi comporto sempre normalmente
      if this.oParentObject.w_MVRIFCAC=0 Or g_COAC="N"
        if this.oParentObject.w_MVQTAIMP<this.w_QTARIM
          if this.w_PADRE.cFunction="Load"
            this.oParentObject.w_MVFLERIF = IIF(ah_YESNO("Evado la quantit� rimanente sul documento di origine?"), "S", " ")
          else
            if this.oParentObject.w_FLGEFA<>"B"
              this.oParentObject.w_MVFLERIF = IIF(ah_YESNO("Evado la quantit� rimanente sul documento di origine?"), "S", " ")
            endif
          endif
        else
          * --- Se la quantit� impegnato coincide con la qt� rimanente la riga � evasa
          this.oParentObject.w_MVFLERIF = "S"
        endif
      else
        * --- Se la riga non � di tipo contributo accessorio allora
        *     non faccio domande ma ricerco il padre e applico la solita regola
        *     di evasione, questo cmq sia in fase di aggiornamento della qt� in
        *     automatico che manuale da parte dell'utente
        *     (se modulo disattivo allora mi comporto sempre normalmente
        * --- Vado a recuperare il padre...
        this.w_ROWPADRE = this.w_PADRE.Search(" CPROWNUM= "+Cp_ToStr( this.oParentObject.w_MVRIFCAC ))
        if this.w_ROWPADRE>0
          this.w_ROWINDEX = this.w_PADRE.RowIndex()
          this.w_PADRE.SetRow(this.w_ROWPADRE)     
          this.w_PMVFLERIF = this.w_PADRE.Get("w_MVFLERIF")
          this.w_PMQTAIMP = this.w_PADRE.Get("w_MVQTAIMP")
          this.w_PMQTAIM1 = this.w_PADRE.Get("w_MVQTAIM1")
          * --- Mi riposiziono nella riga di partenza...
          this.w_PADRE.SetRow(this.w_ROWINDEX)     
          this.oParentObject.w_MVFLERIF = this.w_PMVFLERIF
          this.oParentObject.w_MVQTAIMP = this.w_PMQTAIMP
          this.oParentObject.w_MVQTAIM1 = this.w_PMQTAIM1
        endif
      endif
      this.oParentObject.w_OLDEVAS = IIF( this.oParentObject.w_MVFLERIF="S" , "E" , "R" )
      if ( this.oParentObject.w_MVFLERIF <> this.w_ORIF ) Or ( this.w_O_OLDEVAS<>this.oParentObject.w_OLDEVAS )
        * --- Uniformo tutte le righe del documento che evadono la stessa riga con lo stesso valore
        *     di MVFLERIF
        GSAR_BCM(this, this.w_PADRE , this.oParentObject.w_CPROWNUM , this.oParentObject.w_MVSERRIF, this.oParentObject.w_MVROWRIF , this.oParentObject.w_OLDEVAS )
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    this.w_PADRE.TrsFromWork()     
    if this.w_QTARIM1>0
      this.w_SERRIF = this.oParentObject.w_MVSERRIF
      this.w_ROWRIF = this.oParentObject.w_MVROWRIF
      * --- Aggiorno Quantit� importata nelle righe collegate alla stessa riga documento di origine dove
      *     la quantit� movimentata � inferiore alla quantit� importata.
      *     w_QTARIM1= decremento quantit� importata riga variata
      *     w_NQTAIMP= nuova quantit� importata riga variata
      *     w_OQTAIMP= old quantit� importata della riga variata
      *     w_OLDIMP= old quantit� importata della riga collegata
      this.w_QTARIM1 = (this.w_OQTAIMP-this.w_NQTAIMP) 
      this.w_PADRE.MarkPos()     
      this.w_NUMREC = 0
      do while this.w_NUMREC<>-1
        this.w_NUMREC = this.w_PADRE.Search( "NVL(t_MVSERRIF, SPACE(10)) = "+cp_ToStrODBC( this.w_SERRIF ) +" AND NVL(t_MVROWRIF, 0) = "+cp_ToStrODBC( this.w_ROWRIF ) +" AND CPROWNUM <> "+cp_ToStrODBC( this.w_ROWNUM ) +" AND NVL(t_MVQTAMOV,0) > NVL(t_MVQTAIMP,0) " , this.w_NUMREC ) 
        if this.w_NUMREC<>-1
          this.w_PADRE.SetRow(this.w_NUMREC)     
          this.w_QTAIMP = this.oParentObject.w_MVQTAIMP+this.w_QTARIM1
          this.w_OLDIMP = this.oParentObject.w_MVQTAIMP
          this.oParentObject.w_MVQTAIMP = MIN(this.oParentObject.w_MVQTAMOV, this.w_QTAIMP)
          if this.w_QTARIM1>0
            this.Page_4()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          this.w_QTARIM1 = this.w_QTARIM1-(this.oParentObject.w_MVQTAIMP-this.w_OLDIMP)
          this.w_PADRE.SaveRow()     
        endif
      enddo
      * --- Non rilancio la SaveDependsOn, all'uscita del batch deve essere sempre lanciata la mCalc..
      this.w_PADRE.RePos(.T.)     
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.oParentObject.w_MVUNIMIS<>this.oParentObject.w_UNMIS1
      * --- Ricalcolo la qt� movimentata del documento di origine nella prima unit� di misura
      *     e la confronto con la qt� nella prima unit� di misura sempre del doc
      *     di origine. Se diverse allora l'utente ha modificato la qt� nella prima unit� di misura 
      *     quindi nel doc. di destinazione calcolo MVQTAUM1 come rapporto dato da:
      *     
      *     ( Qt� 1 Mis. Origine / Qt� movimenta Origine ) * Qta movimentata Destinazione
      *     
      *     Ricalcolo la qt� nella prima unit� di misura del doc. di origine
      this.w_TmpN = CALQTA(this.w_QTAMOV,this.oParentObject.w_MVUNIMIS,this.oParentObject.w_UNMIS2,this.oParentObject.w_OPERAT, this.oParentObject.w_MOLTIP, this.oParentObject.w_FLUSEP, this.w_FLFRAZ1, this.w_MODUM2, , this.oParentObject.w_UNMIS3, this.oParentObject.w_OPERA3, this.oParentObject.w_MOLTI3)
      if this.w_TmpN<>this.w_QTAUM1
        * --- Eseguo la proporzione..
        this.oParentObject.w_MVQTAIM1 = ( this.w_QTAUM1 / this.w_QTAMOV ) * this.oParentObject.w_MVQTAIMP
        this.oParentObject.w_MVQTAUM1 = ( this.w_QTAUM1 / this.w_QTAMOV ) * this.oParentObject.w_MVQTAMOV
        * --- Evito il ricalcolo della quantit� nella prima unti� di misura da parte della mCalc
        this.oParentObject.w_NOCALUM1 = .T.
        if this.w_FLFRAZ1="S" AND this.oParentObject.w_MVQTAUM1<>INT(this.oParentObject.w_MVQTAUM1)
          if this.w_MODUM2="S" 
            * --- Se sulla 1 UM ho il flag forza UM secondarie arrotondo la 1UM all'intero superiore  
            *     e riproporziono l'UM movimentata
            this.Page_5()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            if this.oParentObject.w_FLUSEP<>" "
              * --- Se UM separate arrotondo la qt� della 1UM
              this.oParentObject.w_MVQTAUM1 = cp_ROUND(this.oParentObject.w_MVQTAUM1,0)
              this.oParentObject.w_MVQTAIM1 = cp_ROUND(this.oParentObject.w_MVQTAIM1,0)
            else
              * --- segnalo errore
              msg="Attenzione: articolo privo di unit� di misura separate e prima unit� di misura non frazionabile%0La quantit� nella 1a UM non pu� essere arrotondata all'intero"
              this.oParentObject.w_MVQTAUM1 = 0
              this.oParentObject.w_MVQTAIM1 = 0
            endif
          endif
        endif
        this.oParentObject.w_PREUM1 = IIF(this.oParentObject.w_MVQTAUM1=0, 0, cp_ROUND((this.oParentObject.w_MVPREZZO * this.oParentObject.w_MVQTAMOV) / this.oParentObject.w_MVQTAUM1, this.oParentObject.w_DECUNI))
      else
        this.oParentObject.w_MVQTAIM1 = IIF(this.oParentObject.w_MVTIPRIG="F", 1, CALQTA(this.oParentObject.w_MVQTAIMP,this.oParentObject.w_MVUNIMIS,this.oParentObject.w_UNMIS2,this.oParentObject.w_OPERAT, this.oParentObject.w_MOLTIP, this.oParentObject.w_FLUSEP, this.w_FLFRAZ1, this.w_MODUM2, "", this.oParentObject.w_UNMIS3, this.oParentObject.w_OPERA3, this.oParentObject.w_MOLTI3))
      endif
    else
      this.oParentObject.w_MVQTAIM1 = IIF(this.oParentObject.w_MVTIPRIG="F", 1, CALQTA(this.oParentObject.w_MVQTAIMP,this.oParentObject.w_MVUNIMIS,this.oParentObject.w_UNMIS2,this.oParentObject.w_OPERAT, this.oParentObject.w_MOLTIP, this.oParentObject.w_FLUSEP, this.w_FLFRAZ1, this.w_MODUM2, "", this.oParentObject.w_UNMIS3, this.oParentObject.w_OPERA3, this.oParentObject.w_MOLTI3))
    endif
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lettura dei flag U.M. frazionabile e Forza 2^ U.M.
    * --- Ricalcolo della Qt� nella Prima U.M. poich� al lancio del batch non � ancora stata calcolata
    this.w_QTATEST = CALQTAADV(this.oParentObject.w_MVQTAMOV,this.oParentObject.w_MVUNIMIS,this.oParentObject.w_UNMIS2,this.oParentObject.w_OPERAT, this.oParentObject.w_MOLTIP, this.oParentObject.w_FLUSEP, this.w_FLFRAZ1, this.w_MODUM2, , this.oParentObject.w_UNMIS3, this.oParentObject.w_OPERA3, this.oParentObject.w_MOLTI3,,,This.oparentobject, "MVQTAMOV", .T. )
    this.oParentObject.w_MVQTAUM1 = this.w_QTATEST
    this.w_QTAUM1 = this.oParentObject.w_MVQTAUM1
  endproc


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Azzero l'Array che verr� riempito dalla Funzione
    ARRCALC(1)=0
    * --- Se parametro='Z' allora non devo calcolare il contratto...
    *     Al posto del Gruppo merceologico passo 'XXXXXX' per eliminazione variabile GRUMER dal Body dei documenti.
    *     In questo caso rileggo il gruppo merceologico all'interno di CALPRZLI
    *     Stessa cosa per Categoria Sconti maggiorazioni dell'articolo
    this.w_QTAUM3 = CALQTA(this.w_QTAUM1,this.oParentObject.w_UNMIS3, Space(3),IIF(this.oParentObject.w_OPERAT="/","*","/"), this.oParentObject.w_MOLTIP, "", "", "", , this.oParentObject.w_UNMIS3, IIF(this.oParentObject.w_OPERA3="/","*","/"), this.oParentObject.w_MOLTI3)
    this.oParentObject.w_QTAUM2 = CALQTA(this.w_QTAUM1,this.oParentObject.w_UNMIS2, this.oParentObject.w_UNMIS2,IIF(this.oParentObject.w_OPERAT="/","*","/"), this.oParentObject.w_MOLTIP, "", "", "", , this.oParentObject.w_UNMIS3, IIF(this.oParentObject.w_OPERA3="/","*","/"), this.oParentObject.w_MOLTI3)
    pArrUm [1] = this.oParentObject.w_PREZUM 
 pArrUm [2] = this.oParentObject.w_MVUNIMIS 
 pArrUm [3] = this.oParentObject.w_MVQTAMOV 
 pArrUm [4] = this.oParentObject.w_UNMIS1 
 pArrUm [5] = this.oParentObject.w_MVQTAUM1 
 pArrUm [6] = this.oParentObject.w_UNMIS2 
 pArrUm [7] = this.oParentObject.w_QTAUM2 
 pArrUm [8] = this.oParentObject.w_UNMIS3 
 pArrUm[9] = this.w_QTAUM3
    this.w_CALPRZ = CalPrzli( IIF(this.pExec="Z",REPL("X",16),this.oParentObject.w_MVCONTRA) , this.oParentObject.w_MVTIPCON , this.oParentObject.w_MVCODLIS , this.oParentObject.w_MVCODART , "XXXXXX" , this.w_QTAUM1 , this.oParentObject.w_MVCODVAL , this.oParentObject.w_MVCAOVAL , this.w_DATCAL , this.w_CATCLI , "XXXXXX", iif(this.oParentObject.w_MVFLVEAC="A",this.oParentObject.w_CODVAA,this.oParentObject.w_CODVAV), this.oParentObject.w_XCONORN, this.oParentObject.w_CATCOM, this.oParentObject.w_MVFLSCOR, this.oParentObject.w_SCOLIS, this.oParentObject.w_VALUCA,this.w_PROG, @ARRCALC, this.oParentObject.w_PRZVAC, this.oParentObject.w_MVFLVEAC,"N", @pArrUm)
    this.oParentObject.w_CLUNIMIS = ARRCALC(16)
    this.oParentObject.w_MVCONTRA = IIF(ARRCALC(9)="XXXXXXXXXXXXXXXX",SPACE(15),ARRCALC(9))
    * --- Se il prezzo � calcolato da Listino/Contratto/U.C.A., U.P.V. LIPREZZO = al prezzo calcolato
    *     Nel caso in cui � presente un contratto valido, in ogni caso LIPREZZO = prezzo da contratto anche se 0
    *     Se non trovo un prezzo e non esiste contratto reimposto il prezzo precedentemente calcolato o inserito a mano
    *     Se lancio questo batch da Import documenti nel caso di Ricalcolo Qt� da Lotto di Riordino non devo ricalcolare il prezzo
    this.oParentObject.w_LIPREZZO = IIF(this.pExec="Q",this.oParentObject.w_LIPREZZO, ARRCALC(5) )
    this.oParentObject.w_LISCON = ARRCALC(7)
    this.w_QTALIS = IIF(this.oParentObject.w_CLUNIMIS=this.oParentObject.w_UNMIS1,this.oParentObject.w_MVQTAUM1, IIF(this.oParentObject.w_CLUNIMIS=this.oParentObject.w_UNMIS2, this.oParentObject.w_QTAUM2, this.w_QTAUM3))
    if this.oParentObject.w_MVUNIMIS<>this.oParentObject.w_CLUNIMIS AND NOT EMPTY(this.oParentObject.w_CLUNIMIS)
      if EMPTY(this.oParentObject.w_FLUSEP) AND this.oParentObject.w_PREZUM="N"
        * --- Se in anagrafica articolo la combo  U.M. Separate � impostata a No, 
        *     per determinare il prezzo di riga nella seconda, o terza u.m. si utilizza il fattore di conversione tra prima e seconda \ terza u.m..
        this.oParentObject.w_LIPREZZO = cp_Round(CALMMLIS(this.oParentObject.w_LIPREZZO, this.w_APPO+this.oParentObject.w_MVUNIMIS+this.oParentObject.w_OPERAT+this.oParentObject.w_OPERA3+ "N"+"P"+ALLTRIM(STR(this.oParentObject.w_DECUNI)), this.oParentObject.w_MOLTIP, this.oParentObject.w_MOLTI3, 0),this.oParentObject.w_DECUNI)
      else
        if ! this.pExec="P" 
          if this.oParentObject.w_MVUNIMIS=this.oParentObject.w_UNMIS1 or this.oParentObject.w_CLUNIMIS<>this.oParentObject.w_UNMIS3
            this.oParentObject.w_LIPREZZO = cp_Round(CALMMPZ(this.oParentObject.w_LIPREZZO, this.oParentObject.w_MVQTAMOV, this.w_QTALIS, "N", 0, this.oParentObject.w_DECUNI),this.oParentObject.w_DECUNI)
          else
            this.oParentObject.w_LIPREZZO = cp_Round(UM2UM(this.oParentObject.w_MVCODART,this.oParentObject.w_LIPREZZO, this.oParentObject.w_MVUNIMIS, this.oParentObject.w_CLUNIMIS,this.oParentObject.w_UNMIS1, this.oParentObject.w_UNMIS2, this.oParentObject.w_OPERAT,this.oParentObject.w_MOLTIP),this.oParentObject.w_DECUNI)
          endif
        endif
      endif
    endif
    if this.oParentObject.w_LIPREZZO = 0 AND NOT EMPTY(this.oParentObject.w_MVCODLIS) AND (this.pExec="A" OR this.pExec="P" OR this.pExec="U" OR this.pExec="M")
      * --- Quando w_LIPREZZO vene variato il ricalcolo � forzato dall'assegnamento
      *     o_LIPREZZO = 0
      *     nella routine GSVE_BES.
      *     Solo nel caso in cui w_LIPREZZO = 0, per eseguire il ricalcolo, occorre che
      *     o_LIPREZZO <> 0
      this.oParentObject.o_LIPREZZO = this.oParentObject.w_LIPREZZO + 1
    endif
    this.w_OK = ARRCALC(7)=2 OR ARRCALC(8)=1
    * --- Aggiorno gli sconti solo se arrivano da Listino/Contratto/Tabella ScontiMagg.
    *     solo se non sono nel ricalcolo Qt� da Lotto di Riordino in fase di Import
    this.oParentObject.w_MVSCONT1 = IIF(this.pExec="Q" OR (this.oParentObject.w_LIPREZZO=0 AND EMPTY(this.oParentObject.w_MVCODLIS) AND this.oParentObject.w_MVSCONT1<>0 AND ARRCALC(1)=0),this.oParentObject.w_MVSCONT1, ARRCALC(1) )
    this.oParentObject.w_MVSCONT2 = IIF(this.pExec="Q" OR (this.oParentObject.w_LIPREZZO=0 AND EMPTY(this.oParentObject.w_MVCODLIS) AND this.oParentObject.w_MVSCONT2<>0 AND ARRCALC(2)=0),this.oParentObject.w_MVSCONT2, ARRCALC(2) )
    this.oParentObject.w_MVSCONT3 = IIF(this.pExec="Q" OR (this.oParentObject.w_LIPREZZO=0 AND EMPTY(this.oParentObject.w_MVCODLIS) AND this.oParentObject.w_MVSCONT3<>0 AND ARRCALC(3)=0), this.oParentObject.w_MVSCONT3, ARRCALC(3) )
    this.oParentObject.w_MVSCONT4 = IIF(this.pExec="Q" OR (this.oParentObject.w_LIPREZZO=0 AND EMPTY(this.oParentObject.w_MVCODLIS) AND this.oParentObject.w_MVSCONT4<>0 AND ARRCALC(4)=0),this.oParentObject.w_MVSCONT4, ARRCALC(4) )
    this.oParentObject.w_ACTSCOR = IIF(this.pExec="Q",this.oParentObject.w_ACTSCOR, ARRCALC(10) )
    this.oParentObject.w_IVACON = IIF(this.pExec="Q" or empty(this.oParentObject.w_MVCONTRA), this.oParentObject.w_IVACON, ARRCALC(12) )
    * --- Modifica qt� da Ricalcolo Qt� da Lotto di Riordino
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pExec)
    this.pExec=pExec
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,12)]
    this.cWorkTables[1]='TRADARTI'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='LOTTIART'
    this.cWorkTables[4]='DOC_DETT'
    this.cWorkTables[5]='CAM_AGAZ'
    this.cWorkTables[6]='UNIMIS'
    this.cWorkTables[7]='SALDIART'
    this.cWorkTables[8]='KEY_ARTI'
    this.cWorkTables[9]='VOCIIVA'
    this.cWorkTables[10]='ART_ALTE'
    this.cWorkTables[11]='MOVIMATR'
    this.cWorkTables[12]='PAR_RIOR'
    return(this.OpenAllTables(12))

  proc CloseCursors()
    if used('_Curs_query_gsvepbcd')
      use in _Curs_query_gsvepbcd
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pExec"
endproc
