* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_kiu                                                        *
*              Dettaglio                                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-02-02                                                      *
* Last revis.: 2012-08-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsve_kiu",oParentObject))

* --- Class definition
define class tgsve_kiu as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 699
  Height = 356
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-08-27"
  HelpContextID=267006569
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=65

  * --- Constant Properties
  _IDX = 0
  ART_ICOL_IDX = 0
  CLA_RICA_IDX = 0
  KEY_ARTI_IDX = 0
  CONTI_IDX = 0
  cPrg = "gsve_kiu"
  cComment = "Dettaglio"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CPROWORD = 0
  w_LICODICE = space(41)
  w_LIDESART = space(40)
  w_ARCODART = space(41)
  w_ARCODRIC = space(5)
  w_LICODRIC = space(5)
  o_LICODRIC = space(5)
  w_CRDESCRI = space(35)
  w_CRPERCEN = 0
  w_LIIMPUCA = 0
  w_LIIMPPRO = 0
  w_LIPERPRO = 0
  w_LIPREZZO = 0
  w_LIMRGPRZ = 0
  w_LIPERPRZ = 0
  w_LIIMPCOL = 0
  w_LIMRGCOL = 0
  w_LIPERCOL = 0
  w_LIIMPSCO = 0
  w_LIIMPNAZ = 0
  w_LIMRGNAZ = 0
  w_LIPERNAZ = 0
  w_LIFLCPRO = space(1)
  w_LIIMPNPR = 0
  w_LIMRGNPR = 0
  w_LIPERNPR = 0
  w_LIMARGIN = 0
  w_LITIPMAR = space(1)
  w_LITIPMAR = space(1)
  w_LIPRZPRO = 0
  w_LISCONT1 = 0
  w_LISCONT2 = 0
  w_LISCONT3 = 0
  w_LISCONT4 = 0
  w_LISCOCL1 = 0
  w_LISCOCL2 = 0
  w_LIDATUCA = ctod('  /  /  ')
  w_GESTGUID = space(14)
  w_ARUNMIS1 = space(3)
  w_ARUNMIS2 = space(3)
  w_LICODLIS = space(5)
  w_LIKEYLIS = space(10)
  w_LISCOLIS = space(5)
  w_LIPROLIS = space(5)
  w_LIPROSCO = space(5)
  w_CACODVAR = space(20)
  w_LIQTAMOV = 0
  w_LICODMAG = space(5)
  w_LICODVAL = space(3)
  w_LITIPCON = space(1)
  w_LICODCON = space(15)
  w_LIQTAUM1 = 0
  w_LIUNIMIS = space(3)
  w_GUID_CLF = space(14)
  w_CAPREZUM = space(1)
  w_LICAOVAL = 0
  w_LIDATDOC = ctod('  /  /  ')
  w_LILISCHK = space(5)
  w_LICHKUCA = space(1)
  w_CAUNIMIS = space(3)
  w_CAOPERAT = space(1)
  w_CAMOLTIP = 0
  w_AROPERAT = space(1)
  w_ARMOLTIP = 0
  w_ARTIPART = space(2)
  w_PARENTCL = space(10)
  w_PRZINUM1 = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_kiuPag1","gsve_kiu",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oLIMARGIN_1_47
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_PRZINUM1 = this.oPgFrm.Pages(1).oPag.PRZINUM1
    DoDefault()
    proc Destroy()
      this.w_PRZINUM1 = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='CLA_RICA'
    this.cWorkTables[3]='KEY_ARTI'
    this.cWorkTables[4]='CONTI'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSVE_BIU(this,"SAVE")
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CPROWORD=0
      .w_LICODICE=space(41)
      .w_LIDESART=space(40)
      .w_ARCODART=space(41)
      .w_ARCODRIC=space(5)
      .w_LICODRIC=space(5)
      .w_CRDESCRI=space(35)
      .w_CRPERCEN=0
      .w_LIIMPUCA=0
      .w_LIIMPPRO=0
      .w_LIPERPRO=0
      .w_LIPREZZO=0
      .w_LIMRGPRZ=0
      .w_LIPERPRZ=0
      .w_LIIMPCOL=0
      .w_LIMRGCOL=0
      .w_LIPERCOL=0
      .w_LIIMPSCO=0
      .w_LIIMPNAZ=0
      .w_LIMRGNAZ=0
      .w_LIPERNAZ=0
      .w_LIFLCPRO=space(1)
      .w_LIIMPNPR=0
      .w_LIMRGNPR=0
      .w_LIPERNPR=0
      .w_LIMARGIN=0
      .w_LITIPMAR=space(1)
      .w_LITIPMAR=space(1)
      .w_LIPRZPRO=0
      .w_LISCONT1=0
      .w_LISCONT2=0
      .w_LISCONT3=0
      .w_LISCONT4=0
      .w_LISCOCL1=0
      .w_LISCOCL2=0
      .w_LIDATUCA=ctod("  /  /  ")
      .w_GESTGUID=space(14)
      .w_ARUNMIS1=space(3)
      .w_ARUNMIS2=space(3)
      .w_LICODLIS=space(5)
      .w_LIKEYLIS=space(10)
      .w_LISCOLIS=space(5)
      .w_LIPROLIS=space(5)
      .w_LIPROSCO=space(5)
      .w_CACODVAR=space(20)
      .w_LIQTAMOV=0
      .w_LICODMAG=space(5)
      .w_LICODVAL=space(3)
      .w_LITIPCON=space(1)
      .w_LICODCON=space(15)
      .w_LIQTAUM1=0
      .w_LIUNIMIS=space(3)
      .w_GUID_CLF=space(14)
      .w_CAPREZUM=space(1)
      .w_LICAOVAL=0
      .w_LIDATDOC=ctod("  /  /  ")
      .w_LILISCHK=space(5)
      .w_LICHKUCA=space(1)
      .w_CAUNIMIS=space(3)
      .w_CAOPERAT=space(1)
      .w_CAMOLTIP=0
      .w_AROPERAT=space(1)
      .w_ARMOLTIP=0
      .w_ARTIPART=space(2)
      .w_PARENTCL=space(10)
        .w_CPROWORD = ICASE( lower(this.oParentObject.class) == "tgsve_miu", this.oparentobject.w_LIROWORD, lower(this.oParentObject.class) == "tgsve_kas", this.oparentobject.w_ATROWORD, '')
        .w_LICODICE = ICASE( lower(this.oParentObject.class) == "tgsve_miu", this.oparentobject.w_LICODICE, lower(this.oParentObject.class) == "tgsve_kas", this.oparentobject.w_ATCODICE, '')
      .oPgFrm.Page1.oPag.oObj_1_5.Calculate('Valori espressi in valuta nazionale ('+ALLTRIM(g_PERVAL)+')')
          .DoRTCalc(3,5,.f.)
        .w_LICODRIC = .w_ARCODRIC
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_LICODRIC))
          .link_1_10('Full')
        endif
          .DoRTCalc(7,8,.f.)
        .w_LIIMPUCA = ICASE( lower(this.oParentObject.class) == "tgsve_miu", this.oparentobject.w_LIIMPUCA, lower(this.oParentObject.class) == "tgsve_kas", this.oparentobject.w_ATPRZUCA, 0)
        .w_LIIMPPRO = ICASE( lower(this.oParentObject.class) == "tgsve_miu", this.oparentobject.w_LIIMPPRO, lower(this.oParentObject.class) == "tgsve_kas", this.oparentobject.w_ATIMPPRO, 0)
        .w_LIPERPRO = ICASE( lower(this.oParentObject.class) == "tgsve_miu", this.oparentobject.w_LIPERPRO,  lower(this.oParentObject.class) == "tgsve_kas", this.oparentobject.w_ATPERPRO, 0)
        .w_LIPREZZO = ICASE( lower(this.oParentObject.class) == "tgsve_miu", this.oparentobject.w_LIPREZZO, lower(this.oParentObject.class) == "tgsve_kas", this.oparentobject.w_ATPREZZO, 0)
        .w_LIMRGPRZ = .w_LIPREZZO-.w_LIIMPUCA
        .w_LIPERPRZ = (.w_LIPREZZO - .w_LIIMPUCA) * 100 / .w_LIIMPUCA
        .w_LIIMPCOL = ICASE( lower(this.oParentObject.class) == "tgsve_miu", this.oparentobject.w_LIIMPCOL, lower(this.oParentObject.class) == "tgsve_kas", this.oparentobject.w_ATVALRIG, 0)
        .w_LIMRGCOL = .w_LIIMPCOL-.w_LIIMPUCA
        .w_LIPERCOL = (.w_LIIMPCOL - .w_LIIMPUCA) * 100 / .w_LIIMPUCA
        .w_LIIMPSCO = ICASE( lower(this.oParentObject.class) == "tgsve_miu", this.oparentobject.w_LIIMPSCO, lower(this.oParentObject.class) == "tgsve_kas", this.oparentobject.w_ATIMPSCO, 0)
        .w_LIIMPNAZ = ICASE( lower(this.oParentObject.class) == "tgsve_miu", this.oparentobject.w_LIIMPNAZ/this.oparentobject.w_LIQTAMOV,  lower(this.oParentObject.class) == "tgsve_kas", this.oparentobject.w_ATIMPNAZ/this.oparentobject.w_ATQTAMOV, 0)
        .w_LIMRGNAZ = .w_LIIMPNAZ-.w_LIIMPUCA
        .w_LIPERNAZ = (.w_LIIMPNAZ - .w_LIIMPUCA) * 100 / .w_LIIMPUCA
        .w_LIFLCPRO = ICASE( lower(this.oParentObject.class) == "tgsve_miu", this.oparentobject.w_LIFLCPRO, lower(this.oParentObject.class) == "tgsve_kas", this.oparentobject.w_ATFLCPRO, '')
        .w_LIIMPNPR = ICASE( lower(this.oParentObject.class) == "tgsve_miu", this.oparentobject.w_LIIMPNAZ/this.oparentobject.w_LIQTAMOV - IIF(EMPTY(this.oparentobject.w_LIPERPRO) , this.oparentobject.w_LIIMPPRO, cp_Round(this.oparentobject.w_LIPREZZO * this.oparentobject.w_LIPERPRO/100 ,5) ), lower(this.oParentObject.class) == "tgsve_kas", this.oparentobject.w_ATPRZCHK, 0)
        .w_LIMRGNPR = .w_LIIMPNPR-.w_LIIMPUCA
        .w_LIPERNPR = (.w_LIIMPNPR - .w_LIIMPUCA) * 100 / .w_LIIMPUCA
        .w_LIMARGIN = .w_CRPERCEN
        .w_LITIPMAR = 'D'
        .w_LITIPMAR = 'D'
          .DoRTCalc(29,29,.f.)
        .w_LISCONT1 = ICASE( lower(this.oParentObject.class) == "tgsve_miu", this.oparentobject.w_LISCONT1, lower(this.oParentObject.class) == "tgsve_kas", this.oparentobject.w_ATSCONT1, 0)
        .w_LISCONT2 = ICASE( lower(this.oParentObject.class) == "tgsve_miu", this.oparentobject.w_LISCONT2, lower(this.oParentObject.class) == "tgsve_kas", this.oparentobject.w_ATSCONT2, 0)
        .w_LISCONT3 = ICASE( lower(this.oParentObject.class) == "tgsve_miu", this.oparentobject.w_LISCONT3, lower(this.oParentObject.class) == "tgsve_kas", this.oparentobject.w_ATSCONT3, 0)
        .w_LISCONT4 = ICASE( lower(this.oParentObject.class) == "tgsve_miu", this.oparentobject.w_LISCONT4, lower(this.oParentObject.class) == "tgsve_kas", this.oparentobject.w_ATSCONT4, 0)
        .w_LISCOCL1 = ICASE( lower(this.oParentObject.class) == "tgsve_miu", this.oparentobject.w_LISCOCL1, lower(this.oParentObject.class) == "tgsve_kas", this.oparentobject.w_ATSCOCL1, 0)
        .w_LISCOCL2 = ICASE( lower(this.oParentObject.class) == "tgsve_miu", this.oparentobject.w_LISCOCL2, lower(this.oParentObject.class) == "tgsve_kas", this.oparentobject.w_ATSCOCL2, 0)
        .w_LIDATUCA = ICASE( lower(this.oParentObject.class) == "tgsve_miu", this.oparentobject.w_LIDATUCA,  lower(this.oParentObject.class) == "tgsve_kas", this.oparentobject.w_ATDATUCA, cp_CharTodate("  /  /    ") )
          .DoRTCalc(37,39,.f.)
        .w_LICODLIS = ICASE( lower(this.oParentObject.class) == "tgsve_miu", this.oparentobject.w_LICODLIS, lower(this.oParentObject.class) == "tgsve_kas", this.oparentobject.w_ATCODLIS, '')
        .w_LIKEYLIS = IIF( lower(this.oParentObject.class) == "tgsve_miu", this.oparentobject.w_LIKEYLIS, '')
        .w_LISCOLIS = ICASE( lower(this.oParentObject.class) == "tgsve_miu", this.oparentobject.w_LISCOLIS, lower(this.oParentObject.class) == "tgsve_kas", this.oparentobject.w_ATSCOLIS, '')
        .w_LIPROLIS = ICASE( lower(this.oParentObject.class) == "tgsve_miu", this.oparentobject.w_LIPROLIS, lower(this.oParentObject.class) == "tgsve_kas", this.oparentobject.w_ATPROLIS, '')
        .w_LIPROSCO = ICASE( lower(this.oParentObject.class) == "tgsve_miu", this.oparentobject.w_LIPROSCO, lower(this.oParentObject.class) == "tgsve_kas", this.oparentobject.w_ATPROSCO, '')
          .DoRTCalc(45,45,.f.)
        .w_LIQTAMOV = ICASE( lower(this.oParentObject.class) == "tgsve_miu", this.oparentobject.w_LIQTAMOV, lower(this.oParentObject.class) == "tgsve_kas", this.oparentobject.w_ATQTAMOV, 0)
        .w_LICODMAG = ICASE( lower(this.oParentObject.class) == "tgsve_miu", this.oparentobject.w_LICODMAG, lower(this.oParentObject.class) == "tgsve_kas", this.oparentobject.w_ATCODMAG, '')
        .w_LICODVAL = ICASE( lower(this.oParentObject.class) == "tgsve_miu", this.oparentobject.w_LICODVAL, lower(this.oParentObject.class) == "tgsve_kas", this.oparentobject.w_ATCODVAL, '')
        .w_LITIPCON = ICASE( lower(this.oParentObject.class) == "tgsve_miu", this.oparentobject.w_LITIPCON, lower(this.oParentObject.class) == "tgsve_kas", this.oparentobject.w_ATTIPCON, '')
        .w_LICODCON = IIF( UPPER(g_APPLICATION)='AD HOC ENTERPRISE', ICASE(lower(this.oParentObject.class) == "tgsve_miu", this.oparentobject.w_LICODCON, lower(this.oParentObject.class) == "tgsve_kas", this.oparentobject.w_ATCODCON, ''), '')
        .DoRTCalc(50,50,.f.)
        if not(empty(.w_LICODCON))
          .link_1_82('Full')
        endif
        .w_LIQTAUM1 = ICASE( lower(this.oParentObject.class) == "tgsve_miu", this.oparentobject.w_LIQTAUM1, lower(this.oParentObject.class) == "tgsve_kas", this.oparentobject.w_ATQTAUM1, 0)
        .w_LIUNIMIS = ICASE( lower(this.oParentObject.class) == "tgsve_miu", this.oparentobject.w_LIUNIMIS, lower(this.oParentObject.class) == "tgsve_kas", this.oparentobject.w_ATUNIMIS, '')
          .DoRTCalc(53,54,.f.)
        .w_LICAOVAL = ICASE( lower(this.oParentObject.class) == "tgsve_miu", this.oparentobject.w_LICAOVAL, lower(this.oParentObject.class) == "tgsve_kas", this.oparentobject.w_ATCAOVAL, '')
        .w_LIDATDOC = ICASE( lower(this.oParentObject.class) == "tgsve_miu", this.oparentobject.w_LIDATDOC, lower(this.oParentObject.class) == "tgsve_kas", this.oparentobject.w_ATDATDOC, cp_CharToDate("  /  /    ") )
        .w_LILISCHK = ICASE( lower(this.oParentObject.class) == "tgsve_miu", this.oParentObject.w_LILISCHK,  lower(this.oParentObject.class) == "tgsve_kas", this.oparentobject.w_ATLISCHK, '')
        .w_LICHKUCA = ICASE( lower(this.oParentObject.class) == "tgsve_miu", this.oParentObject.w_LICHKUCA, lower(this.oParentObject.class) == "tgsve_kas", this.oparentobject.w_ATCHKUCA, '')
      .oPgFrm.Page1.oPag.PRZINUM1.Calculate('Prezzo proposto in prima unit� di misura ('+ALLTRIM(.w_ARUNMIS1)+')')
          .DoRTCalc(59,64,.f.)
        .w_PARENTCL = lower(this.oParentObject.class)
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_54.enabled = this.oPgFrm.Page1.oPag.oBtn_1_54.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_63.enabled = this.oPgFrm.Page1.oPag.oBtn_1_63.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_64.enabled = this.oPgFrm.Page1.oPag.oBtn_1_64.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_65.enabled = this.oPgFrm.Page1.oPag.oBtn_1_65.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_66.enabled = this.oPgFrm.Page1.oPag.oBtn_1_66.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_67.enabled = this.oPgFrm.Page1.oPag.oBtn_1_67.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_105.enabled = this.oPgFrm.Page1.oPag.oBtn_1_105.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_106.enabled = this.oPgFrm.Page1.oPag.oBtn_1_106.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate('Valori espressi in valuta nazionale ('+ALLTRIM(g_PERVAL)+')')
        .DoRTCalc(1,5,.t.)
          .link_1_10('Full')
        .DoRTCalc(7,12,.t.)
            .w_LIMRGPRZ = .w_LIPREZZO-.w_LIIMPUCA
            .w_LIPERPRZ = (.w_LIPREZZO - .w_LIIMPUCA) * 100 / .w_LIIMPUCA
        .DoRTCalc(15,15,.t.)
            .w_LIMRGCOL = .w_LIIMPCOL-.w_LIIMPUCA
            .w_LIPERCOL = (.w_LIIMPCOL - .w_LIIMPUCA) * 100 / .w_LIIMPUCA
        .DoRTCalc(18,19,.t.)
            .w_LIMRGNAZ = .w_LIIMPNAZ-.w_LIIMPUCA
            .w_LIPERNAZ = (.w_LIIMPNAZ - .w_LIIMPUCA) * 100 / .w_LIIMPUCA
        .DoRTCalc(22,23,.t.)
            .w_LIMRGNPR = .w_LIIMPNPR-.w_LIIMPUCA
            .w_LIPERNPR = (.w_LIIMPNPR - .w_LIIMPUCA) * 100 / .w_LIIMPUCA
        if .o_LICODRIC<>.w_LICODRIC
            .w_LIMARGIN = .w_CRPERCEN
        endif
        .DoRTCalc(27,49,.t.)
          .link_1_82('Full')
        .oPgFrm.Page1.oPag.PRZINUM1.Calculate('Prezzo proposto in prima unit� di misura ('+ALLTRIM(.w_ARUNMIS1)+')')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(51,65,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate('Valori espressi in valuta nazionale ('+ALLTRIM(g_PERVAL)+')')
        .oPgFrm.Page1.oPag.PRZINUM1.Calculate('Prezzo proposto in prima unit� di misura ('+ALLTRIM(.w_ARUNMIS1)+')')
    endwith
  return

  proc Calculate_UJGFEYFITM()
    with this
          * --- Visualizza informazioni su prezzo in prima unit� di misura
          .w_PRZINUM1.Visible = .w_ARUNMIS1<>.w_LIUNIMIS
    endwith
  endproc
  proc Calculate_JJONOUXNQH()
    with this
          * --- Link codice articolo/codice di ricerca in avvio maschera
          gsve_biu(this;
              ,'BLANK';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oLITIPMAR_1_49.enabled = this.oPgFrm.Page1.oPag.oLITIPMAR_1_49.mCond()
    this.oPgFrm.Page1.oPag.oLITIPMAR_1_50.enabled = this.oPgFrm.Page1.oPag.oLITIPMAR_1_50.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_63.enabled = this.oPgFrm.Page1.oPag.oBtn_1_63.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_67.enabled = this.oPgFrm.Page1.oPag.oBtn_1_67.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_71.enabled = this.oPgFrm.Page1.oPag.oBtn_1_71.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_18.visible=!this.oPgFrm.Page1.oPag.oStr_1_18.mHide()
    this.oPgFrm.Page1.oPag.oLIIMPNPR_1_39.visible=!this.oPgFrm.Page1.oPag.oLIIMPNPR_1_39.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_40.visible=!this.oPgFrm.Page1.oPag.oStr_1_40.mHide()
    this.oPgFrm.Page1.oPag.oLIMRGNPR_1_41.visible=!this.oPgFrm.Page1.oPag.oLIMRGNPR_1_41.mHide()
    this.oPgFrm.Page1.oPag.oLIPERNPR_1_42.visible=!this.oPgFrm.Page1.oPag.oLIPERNPR_1_42.mHide()
    this.oPgFrm.Page1.oPag.oLITIPMAR_1_49.visible=!this.oPgFrm.Page1.oPag.oLITIPMAR_1_49.mHide()
    this.oPgFrm.Page1.oPag.oLITIPMAR_1_50.visible=!this.oPgFrm.Page1.oPag.oLITIPMAR_1_50.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_61.visible=!this.oPgFrm.Page1.oPag.oStr_1_61.mHide()
    this.oPgFrm.Page1.oPag.oLIDATUCA_1_62.visible=!this.oPgFrm.Page1.oPag.oLIDATUCA_1_62.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_65.visible=!this.oPgFrm.Page1.oPag.oBtn_1_65.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_66.visible=!this.oPgFrm.Page1.oPag.oBtn_1_66.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_67.visible=!this.oPgFrm.Page1.oPag.oBtn_1_67.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_71.visible=!this.oPgFrm.Page1.oPag.oBtn_1_71.mHide()
    this.oPgFrm.Page1.oPag.oLILISCHK_1_89.visible=!this.oPgFrm.Page1.oPag.oLILISCHK_1_89.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_91.visible=!this.oPgFrm.Page1.oPag.oStr_1_91.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_92.visible=!this.oPgFrm.Page1.oPag.oStr_1_92.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_93.visible=!this.oPgFrm.Page1.oPag.oStr_1_93.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_94.visible=!this.oPgFrm.Page1.oPag.oStr_1_94.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_105.visible=!this.oPgFrm.Page1.oPag.oBtn_1_105.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_106.visible=!this.oPgFrm.Page1.oPag.oBtn_1_106.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_5.Event(cEvent)
      .oPgFrm.Page1.oPag.PRZINUM1.Event(cEvent)
        if lower(cEvent)==lower("Blank")
          .Calculate_UJGFEYFITM()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank")
          .Calculate_JJONOUXNQH()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=LICODRIC
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_RICA_IDX,3]
    i_lTable = "CLA_RICA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_RICA_IDX,2], .t., this.CLA_RICA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_RICA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LICODRIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LICODRIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CRCODICE,CRDESCRI,CRPERCEN";
                   +" from "+i_cTable+" "+i_lTable+" where CRCODICE="+cp_ToStrODBC(this.w_LICODRIC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CRCODICE',this.w_LICODRIC)
            select CRCODICE,CRDESCRI,CRPERCEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LICODRIC = NVL(_Link_.CRCODICE,space(5))
      this.w_CRDESCRI = NVL(_Link_.CRDESCRI,space(35))
      this.w_CRPERCEN = NVL(_Link_.CRPERCEN,0)
    else
      if i_cCtrl<>'Load'
        this.w_LICODRIC = space(5)
      endif
      this.w_CRDESCRI = space(35)
      this.w_CRPERCEN = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_RICA_IDX,2])+'\'+cp_ToStr(_Link_.CRCODICE,1)
      cp_ShowWarn(i_cKey,this.CLA_RICA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LICODRIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LICODCON
  func Link_1_82(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LICODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LICODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,GESTGUID";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_LICODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_LITIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_LITIPCON;
                       ,'ANCODICE',this.w_LICODCON)
            select ANTIPCON,ANCODICE,GESTGUID;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LICODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_GUID_CLF = NVL(_Link_.GESTGUID,space(14))
    else
      if i_cCtrl<>'Load'
        this.w_LICODCON = space(15)
      endif
      this.w_GUID_CLF = space(14)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LICODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCPROWORD_1_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oCPROWORD_1_1.value=this.w_CPROWORD
    endif
    if not(this.oPgFrm.Page1.oPag.oLICODICE_1_3.value==this.w_LICODICE)
      this.oPgFrm.Page1.oPag.oLICODICE_1_3.value=this.w_LICODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oLIDESART_1_6.value==this.w_LIDESART)
      this.oPgFrm.Page1.oPag.oLIDESART_1_6.value=this.w_LIDESART
    endif
    if not(this.oPgFrm.Page1.oPag.oLICODRIC_1_10.value==this.w_LICODRIC)
      this.oPgFrm.Page1.oPag.oLICODRIC_1_10.value=this.w_LICODRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oCRDESCRI_1_11.value==this.w_CRDESCRI)
      this.oPgFrm.Page1.oPag.oCRDESCRI_1_11.value=this.w_CRDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCRPERCEN_1_13.value==this.w_CRPERCEN)
      this.oPgFrm.Page1.oPag.oCRPERCEN_1_13.value=this.w_CRPERCEN
    endif
    if not(this.oPgFrm.Page1.oPag.oLIIMPUCA_1_15.value==this.w_LIIMPUCA)
      this.oPgFrm.Page1.oPag.oLIIMPUCA_1_15.value=this.w_LIIMPUCA
    endif
    if not(this.oPgFrm.Page1.oPag.oLIPREZZO_1_19.value==this.w_LIPREZZO)
      this.oPgFrm.Page1.oPag.oLIPREZZO_1_19.value=this.w_LIPREZZO
    endif
    if not(this.oPgFrm.Page1.oPag.oLIMRGPRZ_1_22.value==this.w_LIMRGPRZ)
      this.oPgFrm.Page1.oPag.oLIMRGPRZ_1_22.value=this.w_LIMRGPRZ
    endif
    if not(this.oPgFrm.Page1.oPag.oLIPERPRZ_1_24.value==this.w_LIPERPRZ)
      this.oPgFrm.Page1.oPag.oLIPERPRZ_1_24.value=this.w_LIPERPRZ
    endif
    if not(this.oPgFrm.Page1.oPag.oLIIMPCOL_1_29.value==this.w_LIIMPCOL)
      this.oPgFrm.Page1.oPag.oLIIMPCOL_1_29.value=this.w_LIIMPCOL
    endif
    if not(this.oPgFrm.Page1.oPag.oLIMRGCOL_1_31.value==this.w_LIMRGCOL)
      this.oPgFrm.Page1.oPag.oLIMRGCOL_1_31.value=this.w_LIMRGCOL
    endif
    if not(this.oPgFrm.Page1.oPag.oLIPERCOL_1_32.value==this.w_LIPERCOL)
      this.oPgFrm.Page1.oPag.oLIPERCOL_1_32.value=this.w_LIPERCOL
    endif
    if not(this.oPgFrm.Page1.oPag.oLIIMPNAZ_1_34.value==this.w_LIIMPNAZ)
      this.oPgFrm.Page1.oPag.oLIIMPNAZ_1_34.value=this.w_LIIMPNAZ
    endif
    if not(this.oPgFrm.Page1.oPag.oLIMRGNAZ_1_36.value==this.w_LIMRGNAZ)
      this.oPgFrm.Page1.oPag.oLIMRGNAZ_1_36.value=this.w_LIMRGNAZ
    endif
    if not(this.oPgFrm.Page1.oPag.oLIPERNAZ_1_37.value==this.w_LIPERNAZ)
      this.oPgFrm.Page1.oPag.oLIPERNAZ_1_37.value=this.w_LIPERNAZ
    endif
    if not(this.oPgFrm.Page1.oPag.oLIIMPNPR_1_39.value==this.w_LIIMPNPR)
      this.oPgFrm.Page1.oPag.oLIIMPNPR_1_39.value=this.w_LIIMPNPR
    endif
    if not(this.oPgFrm.Page1.oPag.oLIMRGNPR_1_41.value==this.w_LIMRGNPR)
      this.oPgFrm.Page1.oPag.oLIMRGNPR_1_41.value=this.w_LIMRGNPR
    endif
    if not(this.oPgFrm.Page1.oPag.oLIPERNPR_1_42.value==this.w_LIPERNPR)
      this.oPgFrm.Page1.oPag.oLIPERNPR_1_42.value=this.w_LIPERNPR
    endif
    if not(this.oPgFrm.Page1.oPag.oLIMARGIN_1_47.value==this.w_LIMARGIN)
      this.oPgFrm.Page1.oPag.oLIMARGIN_1_47.value=this.w_LIMARGIN
    endif
    if not(this.oPgFrm.Page1.oPag.oLITIPMAR_1_49.RadioValue()==this.w_LITIPMAR)
      this.oPgFrm.Page1.oPag.oLITIPMAR_1_49.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLITIPMAR_1_50.RadioValue()==this.w_LITIPMAR)
      this.oPgFrm.Page1.oPag.oLITIPMAR_1_50.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLIPRZPRO_1_52.value==this.w_LIPRZPRO)
      this.oPgFrm.Page1.oPag.oLIPRZPRO_1_52.value=this.w_LIPRZPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oLIDATUCA_1_62.value==this.w_LIDATUCA)
      this.oPgFrm.Page1.oPag.oLIDATUCA_1_62.value=this.w_LIDATUCA
    endif
    if not(this.oPgFrm.Page1.oPag.oLIUNIMIS_1_84.value==this.w_LIUNIMIS)
      this.oPgFrm.Page1.oPag.oLIUNIMIS_1_84.value=this.w_LIUNIMIS
    endif
    if not(this.oPgFrm.Page1.oPag.oLILISCHK_1_89.value==this.w_LILISCHK)
      this.oPgFrm.Page1.oPag.oLILISCHK_1_89.value=this.w_LILISCHK
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_LICODRIC = this.w_LICODRIC
    return

enddefine

* --- Define pages as container
define class tgsve_kiuPag1 as StdContainer
  Width  = 695
  height = 356
  stdWidth  = 695
  stdheight = 356
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCPROWORD_1_1 as StdField with uid="JVCJIQLOQW",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CPROWORD", cQueryName = "CPROWORD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Per evidenziare l'attuale riga di lavoro",;
    HelpContextID = 81412714,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=187, Top=12, cSayPict='"99999"', cGetPict='"99999"'

  add object oLICODICE_1_3 as StdField with uid="JYFFUXINNN",rtseq=2,rtrep=.f.,;
    cFormVar = "w_LICODICE", cQueryName = "LICODICE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(41), bMultilanguage =  .f.,;
    ToolTipText = "Codice articolo",;
    HelpContextID = 229198843,;
   bGlobalFont=.t.,;
    Height=21, Width=306, Left=187, Top=38, InputMask=replicate('X',41)


  add object oObj_1_5 as cp_calclbl with uid="FENHQUFYRF",left=235, top=13, width=247,height=18,;
    caption='Valori espressi in valuta nazionale',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=1;
    , HelpContextID = 249547165

  add object oLIDESART_1_6 as StdField with uid="XTBCPJTQNX",rtseq=3,rtrep=.f.,;
    cFormVar = "w_LIDESART", cQueryName = "LIDESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione articolo",;
    HelpContextID = 110058506,;
   bGlobalFont=.t.,;
    Height=21, Width=306, Left=187, Top=64, InputMask=replicate('X',40)

  add object oLICODRIC_1_10 as StdField with uid="NPMWABJAGW",rtseq=6,rtrep=.f.,;
    cFormVar = "w_LICODRIC", cQueryName = "LICODRIC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Classe di ricarico",;
    HelpContextID = 156677127,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=187, Top=90, InputMask=replicate('X',5), cLinkFile="CLA_RICA", cZoomOnZoom="GSAR_ACR", oKey_1_1="CRCODICE", oKey_1_2="this.w_LICODRIC"

  func oLICODRIC_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oCRDESCRI_1_11 as StdField with uid="PCTPYSTQNT",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CRDESCRI", cQueryName = "CRDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione classe di ricarico",;
    HelpContextID = 143615087,;
   bGlobalFont=.t.,;
    Height=21, Width=234, Left=258, Top=90, InputMask=replicate('X',35)

  add object oCRPERCEN_1_13 as StdField with uid="SUTTHPEQRH",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CRPERCEN", cQueryName = "CRPERCEN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale di ricarico",;
    HelpContextID = 142615668,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=539, Top=90, cSayPict='"999.99"', cGetPict='"999.99"'

  add object oLIIMPUCA_1_15 as StdField with uid="BYRBLAKGHN",rtseq=9,rtrep=.f.,;
    cFormVar = "w_LIIMPUCA", cQueryName = "LIIMPUCA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ultimo costo d'acquisto",;
    HelpContextID = 174566391,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=187, Top=116

  add object oLIPREZZO_1_19 as StdField with uid="JQOYHYXIAN",rtseq=12,rtrep=.f.,;
    cFormVar = "w_LIPREZZO", cQueryName = "LIPREZZO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 21160955,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=187, Top=166, cSayPict='"999999999999.99999"', cGetPict='"999999999999.99999"'

  add object oLIMRGPRZ_1_22 as StdField with uid="VHFNKMLEVU",rtseq=13,rtrep=.f.,;
    cFormVar = "w_LIMRGPRZ", cQueryName = "LIMRGPRZ",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 81587216,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=330, Top=166, cSayPict='"999999999999.99999"', cGetPict='"999999999999.99999"'

  add object oLIPERPRZ_1_24 as StdField with uid="HGVTNBSQBN",rtseq=14,rtrep=.f.,;
    cFormVar = "w_LIPERPRZ", cQueryName = "LIPERPRZ",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 92281872,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=473, Top=166, cSayPict='"999.99"', cGetPict='"999.99"'

  add object oLIIMPCOL_1_29 as StdField with uid="IRBXQRUVLN",rtseq=15,rtrep=.f.,;
    cFormVar = "w_LIIMPCOL", cQueryName = "LIIMPCOL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 127423486,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=187, Top=189, cSayPict='"999999999999.99999"', cGetPict='"999999999999.99999"'

  add object oLIMRGCOL_1_31 as StdField with uid="HOIXDZXFYI",rtseq=16,rtrep=.f.,;
    cFormVar = "w_LIMRGCOL", cQueryName = "LIMRGCOL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 136516606,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=330, Top=189, cSayPict='"999999999999.99999"', cGetPict='"999999999999.99999"'

  add object oLIPERCOL_1_32 as StdField with uid="CBVOESWSPB",rtseq=17,rtrep=.f.,;
    cFormVar = "w_LIPERCOL", cQueryName = "LIPERCOL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 125821950,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=473, Top=189, cSayPict='"999.99"', cGetPict='"999.99"'

  add object oLIIMPNAZ_1_34 as StdField with uid="NGZUIULSFJ",rtseq=19,rtrep=.f.,;
    cFormVar = "w_LIIMPNAZ", cQueryName = "LIIMPNAZ",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 57125904,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=187, Top=212, cSayPict='"999999999999.99999"', cGetPict='"999999999999.99999"'

  add object oLIMRGNAZ_1_36 as StdField with uid="ABDIWYDCLX",rtseq=20,rtrep=.f.,;
    cFormVar = "w_LIMRGNAZ", cQueryName = "LIMRGNAZ",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 48032784,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=330, Top=212, cSayPict='"999999999999.99999"', cGetPict='"999999999999.99999"'

  add object oLIPERNAZ_1_37 as StdField with uid="REHZLHOADQ",rtseq=21,rtrep=.f.,;
    cFormVar = "w_LIPERNAZ", cQueryName = "LIPERNAZ",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 58727440,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=473, Top=212, cSayPict='"999.99"', cGetPict='"999.99"'

  add object oLIIMPNPR_1_39 as StdField with uid="QPACIOJKFE",rtseq=23,rtrep=.f.,;
    cFormVar = "w_LIIMPNPR", cQueryName = "LIIMPNPR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 57125896,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=187, Top=235, cSayPict='"999999999999.99999"', cGetPict='"999999999999.99999"'

  func oLIIMPNPR_1_39.mHide()
    with this.Parent.oContained
      return (.w_LIFLCPRO<>'S')
    endwith
  endfunc

  add object oLIMRGNPR_1_41 as StdField with uid="AHRVCGLZWS",rtseq=24,rtrep=.f.,;
    cFormVar = "w_LIMRGNPR", cQueryName = "LIMRGNPR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 48032776,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=330, Top=235, cSayPict='"999999999999.99999"', cGetPict='"999999999999.99999"'

  func oLIMRGNPR_1_41.mHide()
    with this.Parent.oContained
      return (.w_LIFLCPRO<>'S')
    endwith
  endfunc

  add object oLIPERNPR_1_42 as StdField with uid="VORFEHTQCJ",rtseq=25,rtrep=.f.,;
    cFormVar = "w_LIPERNPR", cQueryName = "LIPERNPR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 58727432,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=473, Top=235, cSayPict='"999.99"', cGetPict='"999.99"'

  func oLIPERNPR_1_42.mHide()
    with this.Parent.oContained
      return (.w_LIFLCPRO<>'S')
    endwith
  endfunc

  add object oLIMARGIN_1_47 as StdField with uid="YHJVSMQLQC",rtseq=26,rtrep=.f.,;
    cFormVar = "w_LIMARGIN", cQueryName = "LIMARGIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Margine di ricarico",;
    HelpContextID = 58987516,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=187, Top=298, cSayPict='"999.99"', cGetPict='"999.99"'


  add object oLITIPMAR_1_49 as StdCombo with uid="IPFNIAMBFF",rtseq=27,rtrep=.f.,left=308,top=298,width=161,height=21;
    , ToolTipText = "Tipologia calcolo margine";
    , HelpContextID = 40131592;
    , cFormVar="w_LITIPMAR",RowSource=""+"Netto riga,"+"Netto documento", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oLITIPMAR_1_49.RadioValue()
    return(iif(this.value =1,'R',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oLITIPMAR_1_49.GetRadio()
    this.Parent.oContained.w_LITIPMAR = this.RadioValue()
    return .t.
  endfunc

  func oLITIPMAR_1_49.SetRadio()
    this.Parent.oContained.w_LITIPMAR=trim(this.Parent.oContained.w_LITIPMAR)
    this.value = ;
      iif(this.Parent.oContained.w_LITIPMAR=='R',1,;
      iif(this.Parent.oContained.w_LITIPMAR=='D',2,;
      0))
  endfunc

  func oLITIPMAR_1_49.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_LIFLCPRO<>'S')
    endwith
   endif
  endfunc

  func oLITIPMAR_1_49.mHide()
    with this.Parent.oContained
      return (.w_LIFLCPRO='S')
    endwith
  endfunc


  add object oLITIPMAR_1_50 as StdCombo with uid="DNLBWJALHB",rtseq=28,rtrep=.f.,left=308,top=298,width=161,height=21;
    , ToolTipText = "Tipologia calcolo margine";
    , HelpContextID = 40131592;
    , cFormVar="w_LITIPMAR",RowSource=""+"Netto riga,"+"Netto documento,"+"Netto provvigioni", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oLITIPMAR_1_50.RadioValue()
    return(iif(this.value =1,'R',;
    iif(this.value =2,'D',;
    iif(this.value =3,'P',;
    space(1)))))
  endfunc
  func oLITIPMAR_1_50.GetRadio()
    this.Parent.oContained.w_LITIPMAR = this.RadioValue()
    return .t.
  endfunc

  func oLITIPMAR_1_50.SetRadio()
    this.Parent.oContained.w_LITIPMAR=trim(this.Parent.oContained.w_LITIPMAR)
    this.value = ;
      iif(this.Parent.oContained.w_LITIPMAR=='R',1,;
      iif(this.Parent.oContained.w_LITIPMAR=='D',2,;
      iif(this.Parent.oContained.w_LITIPMAR=='P',3,;
      0)))
  endfunc

  func oLITIPMAR_1_50.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_LIFLCPRO='S')
    endwith
   endif
  endfunc

  func oLITIPMAR_1_50.mHide()
    with this.Parent.oContained
      return (.w_LIFLCPRO<>'S')
    endwith
  endfunc

  add object oLIPRZPRO_1_52 as StdField with uid="OBHEXMXTKM",rtseq=29,rtrep=.f.,;
    cFormVar = "w_LIPRZPRO", cQueryName = "LIPRZPRO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Prezzo proposto",;
    HelpContextID = 101522437,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=187, Top=321, cSayPict='"999999999999.99999"', cGetPict='"999999999999.99999"'


  add object oBtn_1_54 as StdButton with uid="DGTXJYSTJS",left=473, top=298, width=48,height=45,;
    CpPicture="BMP\GENERA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la simulazione del prezzo in base al margine impostato";
    , HelpContextID = 139594790;
    , caption='\<Simula';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_54.Click()
      with this.Parent.oContained
        GSVE_BIU(this.Parent.oContained,"SIMULATE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oLIDATUCA_1_62 as StdField with uid="BXTPVDUZSM",rtseq=36,rtrep=.f.,;
    cFormVar = "w_LIDATUCA", cQueryName = "LIDATUCA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "alla data",;
    HelpContextID = 177953783,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=377, Top=116

  func oLIDATUCA_1_62.mHide()
    with this.Parent.oContained
      return (.w_LICHKUCA<>'U')
    endwith
  endfunc


  add object oBtn_1_63 as StdButton with uid="MKFOXMDXZW",left=584, top=298, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 266977818;
    , caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_63.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_63.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!.w_PARENTCL == "tgsve_kas")
      endwith
    endif
  endfunc


  add object oBtn_1_64 as StdButton with uid="TVUKKFLAOR",left=635, top=298, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 55713798;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_64.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_65 as StdButton with uid="TIAPNCGUCU",left=635, top=163, width=48,height=45,;
    CpPicture="BMP\ULTVEN.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per consultare le ultime vendite";
    , HelpContextID = 11269206;
    , Caption='\<Ult. Ven.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_65.Click()
      with this.Parent.oContained
        do gsma1bzv with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_65.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (UPPER(g_APPLICATION)='ADHOC REVOLUTION')
     endwith
    endif
  endfunc


  add object oBtn_1_66 as StdButton with uid="FDZCOJWBVJ",left=635, top=212, width=48,height=45,;
    CpPicture="BMP\ULTACQ.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per consultare gli ultimi acquisti";
    , HelpContextID = 11269206;
    , Caption='Ult. \<Acq.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_66.Click()
      with this.Parent.oContained
        do gsve_bza with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_66.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (UPPER(g_APPLICATION)='ADHOC REVOLUTION')
     endwith
    endif
  endfunc


  add object oBtn_1_67 as StdButton with uid="DVFYFXDEXI",left=584, top=212, width=48,height=45,;
    CpPicture="BMP\INVENTAR.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere al dettaglio listini articolo di vendita/entrambi";
    , HelpContextID = 14535206;
    , tabstop=.f., caption='Lis.ve\<nd.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_67.Click()
      with this.Parent.oContained
        GSVE_BLI(this.Parent.oContained,"LISV")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_67.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_ARCODART) AND .cFunction<>'Load')
      endwith
    endif
  endfunc

  func oBtn_1_67.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (UPPER(g_APPLICATION)='ADHOC REVOLUTION' OR (g_CVEN <> 'S' And g_CACQ = 'S'))
     endwith
    endif
  endfunc


  add object oBtn_1_71 as StdButton with uid="ZJTLOLYKFB",left=523, top=298, width=48,height=45,;
    CpPicture="BMP\INVENTADD.BMP", caption="", nPag=1,tabstop=.f.;
    , ToolTipText = "Premere per aggiungere un dettaglio listino con il prezzo calcolato";
    , HelpContextID = 21300295;
    , caption='\<Agg. list.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_71.Click()
      with this.Parent.oContained
        GSVE_BIU(this.Parent.oContained,"UPDLIST")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_71.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_LIPRZPRO<>0 AND !.w_PARENTCL == "tgsve_kas")
      endwith
    endif
  endfunc

  func oBtn_1_71.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (UPPER(g_APPLICATION)='ADHOC REVOLUTION')
     endwith
    endif
  endfunc

  add object oLIUNIMIS_1_84 as StdField with uid="QEGNRNJWAY",rtseq=52,rtrep=.f.,;
    cFormVar = "w_LIUNIMIS", cQueryName = "LIUNIMIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 235312119,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=539, Top=38, InputMask=replicate('X',3)

  add object oLILISCHK_1_89 as StdField with uid="UYSIRZBSRQ",rtseq=57,rtrep=.f.,;
    cFormVar = "w_LILISCHK", cQueryName = "LILISCHK",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 124527615,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=539, Top=116, InputMask=replicate('X',5)

  func oLILISCHK_1_89.mHide()
    with this.Parent.oContained
      return (.w_LICHKUCA='U')
    endwith
  endfunc


  add object PRZINUM1 as cp_calclbl with uid="ZUDTOTCZJJ",left=187, top=276, width=281,height=18,;
    caption='Prezzo proposto in prima unit� di misura',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=1;
    , HelpContextID = 130010860


  add object oBtn_1_105 as StdButton with uid="JWPHLAYHLG",left=635, top=163, width=48,height=45,;
    CpPicture="BMP\ULTVEN.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per consultare le ultime vendite";
    , HelpContextID = 11269206;
    , Caption='\<Ult. Ven.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_105.Click()
      with this.Parent.oContained
        gsar_blu(this.Parent.oContained,"UU")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_105.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (UPPER(g_APPLICATION)<>'ADHOC REVOLUTION')
     endwith
    endif
  endfunc


  add object oBtn_1_106 as StdButton with uid="OJIYENDNDD",left=635, top=212, width=48,height=45,;
    CpPicture="BMP\ULTACQ.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per consultare gli ultimi acquisti";
    , HelpContextID = 11269206;
    , Caption='Ult. \<Acq.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_106.Click()
      with this.Parent.oContained
        gsar_blu(this.Parent.oContained,"UA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_106.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (UPPER(g_APPLICATION)<>'ADHOC REVOLUTION')
     endwith
    endif
  endfunc

  add object oStr_1_2 as StdString with uid="CXUEUZCWFM",Visible=.t., Left=21, Top=13,;
    Alignment=1, Width=162, Height=18,;
    Caption="Riga:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="FZHEHTQBYT",Visible=.t., Left=21, Top=39,;
    Alignment=1, Width=162, Height=18,;
    Caption="Codice articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="MDRGASEZUD",Visible=.t., Left=21, Top=65,;
    Alignment=1, Width=162, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="NEFEZVVXBD",Visible=.t., Left=21, Top=92,;
    Alignment=1, Width=162, Height=18,;
    Caption="Classe di ricarico:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="QHWQDUQWMM",Visible=.t., Left=508, Top=92,;
    Alignment=1, Width=27, Height=18,;
    Caption="%:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="TUSUVZOXGQ",Visible=.t., Left=21, Top=118,;
    Alignment=1, Width=162, Height=18,;
    Caption="Ultimo costo d'acquisto:"  ;
  , bGlobalFont=.t.

  func oStr_1_18.mHide()
    with this.Parent.oContained
      return (.w_LICHKUCA<>'U')
    endwith
  endfunc

  add object oStr_1_20 as StdString with uid="IUOZJIXXXU",Visible=.t., Left=21, Top=169,;
    Alignment=1, Width=162, Height=18,;
    Caption="Lordo riga:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="JAHAHRVUPK",Visible=.t., Left=183, Top=144,;
    Alignment=2, Width=139, Height=18,;
    Caption="Prezzo"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="GWDFDRERHA",Visible=.t., Left=326, Top=144,;
    Alignment=2, Width=139, Height=18,;
    Caption="Margine"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="GTBNFLZCZV",Visible=.t., Left=473, Top=144,;
    Alignment=2, Width=76, Height=18,;
    Caption="%"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="PEEECODDIJ",Visible=.t., Left=21, Top=192,;
    Alignment=1, Width=162, Height=18,;
    Caption="Netto riga:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="PBVOHPJFYT",Visible=.t., Left=21, Top=215,;
    Alignment=1, Width=162, Height=18,;
    Caption="Netto documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="WMTNVWNKJT",Visible=.t., Left=21, Top=238,;
    Alignment=1, Width=162, Height=18,;
    Caption="Netto provvigioni:"  ;
  , bGlobalFont=.t.

  func oStr_1_40.mHide()
    with this.Parent.oContained
      return (.w_LIFLCPRO<>'S')
    endwith
  endfunc

  add object oStr_1_45 as StdString with uid="IAITKSDLIC",Visible=.t., Left=14, Top=274,;
    Alignment=0, Width=219, Height=18,;
    Caption="Simulazione"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="IGBJLCTZRJ",Visible=.t., Left=21, Top=300,;
    Alignment=1, Width=162, Height=18,;
    Caption="Margine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="ZIOYPJDXAI",Visible=.t., Left=266, Top=300,;
    Alignment=0, Width=14, Height=18,;
    Caption="%"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="HEVGVOEMCR",Visible=.t., Left=282, Top=300,;
    Alignment=1, Width=24, Height=18,;
    Caption="su:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="SSBRGBCWIF",Visible=.t., Left=21, Top=323,;
    Alignment=1, Width=162, Height=18,;
    Caption="Prezzo proposto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="BDGKOBXRXF",Visible=.t., Left=329, Top=118,;
    Alignment=1, Width=45, Height=18,;
    Caption="del:"  ;
  , bGlobalFont=.t.

  func oStr_1_61.mHide()
    with this.Parent.oContained
      return (.w_LICHKUCA<>'U')
    endwith
  endfunc

  add object oStr_1_91 as StdString with uid="QNLGKKTPIW",Visible=.t., Left=21, Top=118,;
    Alignment=1, Width=162, Height=18,;
    Caption="Prezzo da listino statistico:"  ;
  , bGlobalFont=.t.

  func oStr_1_91.mHide()
    with this.Parent.oContained
      return (.w_LICHKUCA<>'S')
    endwith
  endfunc

  add object oStr_1_92 as StdString with uid="NGGYDKNMHQ",Visible=.t., Left=347, Top=118,;
    Alignment=1, Width=188, Height=18,;
    Caption="Codice listino statistico:"  ;
  , bGlobalFont=.t.

  func oStr_1_92.mHide()
    with this.Parent.oContained
      return (.w_LICHKUCA<>'S')
    endwith
  endfunc

  add object oStr_1_93 as StdString with uid="JMDWOSDMJQ",Visible=.t., Left=347, Top=118,;
    Alignment=1, Width=188, Height=18,;
    Caption="Codice listino fornitore:"  ;
  , bGlobalFont=.t.

  func oStr_1_93.mHide()
    with this.Parent.oContained
      return (.w_LICHKUCA<>'F')
    endwith
  endfunc

  add object oStr_1_94 as StdString with uid="MCZYFSYRQD",Visible=.t., Left=21, Top=118,;
    Alignment=1, Width=162, Height=18,;
    Caption="Prezzo da listino fornitore:"  ;
  , bGlobalFont=.t.

  func oStr_1_94.mHide()
    with this.Parent.oContained
      return (.w_LICHKUCA<>'F')
    endwith
  endfunc

  add object oStr_1_95 as StdString with uid="TYIOOPYEIN",Visible=.t., Left=495, Top=39,;
    Alignment=1, Width=40, Height=18,;
    Caption="U.m.:"  ;
  , bGlobalFont=.t.

  add object oBox_1_26 as StdBox with uid="MSLWJEJKTG",left=184, top=142, width=370,height=22

  add object oBox_1_27 as StdBox with uid="XFQXXCQSBZ",left=327, top=142, width=2,height=118

  add object oBox_1_28 as StdBox with uid="SQSFQKMCKX",left=470, top=142, width=2,height=116

  add object oBox_1_43 as StdBox with uid="SQDREZFXKT",left=184, top=162, width=370,height=98

  add object oBox_1_44 as StdBox with uid="LDZKKYPNYF",left=10, top=268, width=675,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_kiu','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
