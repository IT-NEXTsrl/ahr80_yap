* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bns                                                        *
*              Filtro numdis su accorpa scad.                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_35]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-14                                                      *
* Last revis.: 2013-12-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bns",oParentObject)
return(i_retval)

define class tgste_bns as StdBatch
  * --- Local variables
  w_MESS = space(10)
  w_PADRE = .NULL.
  w_CODTRI = space(10)
  w_SERIAL = space(10)
  w_SERRIF = space(10)
  w_CODCAU = space(5)
  w_FLIVDF = space(1)
  * --- WorkFile variables
  DATIRITE_idx=0
  PNT_MAST_idx=0
  CAU_CONT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Non permette di accorpare una scadenza associata ad una distinta (da GSTE_KUS)
    this.w_PADRE = this.oParentObject
    NM = this.w_PADRE.w_CalcZoom.cCursor
    do case
      case NVL(&NM..CCFLIVDF," ")="S" or NVL(&NM..CCFLINSO," ")="S"
        this.w_SERRIF = NVL(&NM..PTSERRIF," ")
        if NVL(&NM..CCFLINSO," ")="S" AND NVL(&NM..PTORDRIF,0)>0 AND Not Empty(this.w_SERRIF)
          * --- Read from PNT_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PNT_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2],.t.,this.PNT_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PNCODCAU"+;
              " from "+i_cTable+" PNT_MAST where ";
                  +"PNSERIAL = "+cp_ToStrODBC(this.w_SERRIF);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PNCODCAU;
              from (i_cTable) where;
                  PNSERIAL = this.w_SERRIF;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CODCAU = NVL(cp_ToDate(_read_.PNCODCAU),cp_NullValue(_read_.PNCODCAU))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from CAU_CONT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAU_CONT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2],.t.,this.CAU_CONT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CCFLIVDF"+;
              " from "+i_cTable+" CAU_CONT where ";
                  +"CCCODICE = "+cp_ToStrODBC(this.w_CODCAU);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CCFLIVDF;
              from (i_cTable) where;
                  CCCODICE = this.w_CODCAU;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FLIVDF = NVL(cp_ToDate(_read_.CCFLIVDF),cp_NullValue(_read_.CCFLIVDF))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        else
          this.w_FLIVDF = NVL(&NM..CCFLIVDF," ")
        endif
        if this.w_FLIVDF="S"
          AA=ALLTRIM(STR(this.w_PADRE.w_CalcZoom.grd.ColumnCount))
          this.w_PADRE.w_CalcZoom.grd.Column&AA..chk.Value = 0
          ah_ErrorMsg("Impossibile accorpare una scadenza proveniente/associata ad una fattura ad esigibilitÓ differita",48)
        endif
      case NOT EMPTY(NVL(&NM..PTNUMDIS," "))
        AA=ALLTRIM(STR(this.w_PADRE.w_CalcZoom.grd.ColumnCount))
        this.w_PADRE.w_CalcZoom.grd.Column&AA..chk.Value = 0
        ah_ErrorMsg("Impossibile accorpare una scadenza associata ad una distinta",48)
      case g_RITE="S" AND NVL(&NM..ANRITENU,"N") $ "S-C"
        this.w_SERIAL = &NM..PTSERIAL
        * --- Read from DATIRITE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DATIRITE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DATIRITE_idx,2],.t.,this.DATIRITE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DRCODTRI"+;
            " from "+i_cTable+" DATIRITE where ";
                +"DRSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DRCODTRI;
            from (i_cTable) where;
                DRSERIAL = this.w_SERIAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CODTRI = NVL(cp_ToDate(_read_.DRCODTRI),cp_NullValue(_read_.DRCODTRI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_ROWS>0
          AA=ALLTRIM(STR(this.w_PADRE.w_CalcZoom.grd.ColumnCount))
          this.w_PADRE.w_CalcZoom.grd.Column&AA..chk.Value = 0
          if &NM..TIPCON="C"
            ah_ErrorMsg("Impossibile accorpare partite relative a clienti percipienti generate da movimenti contabili con gestione ritenute",48) 
 
          else
            ah_ErrorMsg("Impossibile accorpare partite relative a fornitori percipienti generate da movimenti contabili con gestione ritenute",48) 
 
          endif
        endif
    endcase
    * --- !!!ATTENZIONE: Eliminare l'esecuzione della mCalc dalla Maschera
    * --- perche per alcuni eventi, richiamerebbe questo batch, entrando in Loop...
    this.bUpdateParentObject = .F.
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='DATIRITE'
    this.cWorkTables[2]='PNT_MAST'
    this.cWorkTables[3]='CAU_CONT'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
