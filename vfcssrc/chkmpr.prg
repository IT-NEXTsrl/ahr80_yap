* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: chkmpr                                                          *
*              Calcola tipo pagamento                                          *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_4]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-11                                                      *
* Last revis.: 2015-03-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tchkmpr",oParentObject,m.pOper)
return(i_retval)

define class tchkmpr as StdBatch
  * --- Local variables
  pOper = space(1)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola Descrizione Tipo Pagamento (da GSAR_MPR)
    do case
      case this.pOper="1"
        * --- Al cambio di un pagamento
        this.oParentObject.w_DESPAG = SPACE(25)
        do case
          case this.oParentObject.w_PRTIPPAG="RB"
            if g_APPLICATION = "ad hoc ENTERPRISE"
              this.oParentObject.w_DESPAG = "Ricevute Bancarie"
            else
              this.oParentObject.w_DESPAG = "Ric.Bancarie/RIBA"
            endif
          case this.oParentObject.w_PRTIPPAG="BO"
            this.oParentObject.w_DESPAG = "Bonifici"
          case this.oParentObject.w_PRTIPPAG="MA"
            this.oParentObject.w_DESPAG = "M.AV."
          case this.oParentObject.w_PRTIPPAG="CA"
            this.oParentObject.w_DESPAG = "Cambiali/Tratte"
          case this.oParentObject.w_PRTIPPAG="RD"
            this.oParentObject.w_DESPAG = "Rimesse Dirette"
          case this.oParentObject.w_PRTIPPAG="RI"
            this.oParentObject.w_DESPAG = "R.I.D."
          case this.oParentObject.w_PRTIPPAG="AB"
            this.oParentObject.w_DESPAG = "Avviso di Bonifico"
          case this.oParentObject.w_PRTIPPAG="AD"
            this.oParentObject.w_DESPAG = "Anticip. c/documenti"
          case this.oParentObject.w_PRTIPPAG="BE"
            this.oParentObject.w_DESPAG = "Bonifici esteri"
          case this.oParentObject.w_PRTIPPAG="SD"
            this.oParentObject.w_DESPAG = "SDD - Sepa Direct Debit"
          case this.oParentObject.w_PRTIPPAG="SC"
            this.oParentObject.w_DESPAG = "SCT - Sepa Credit Transfer"
          case this.oParentObject.w_PRTIPPAG="SE"
            this.oParentObject.w_DESPAG = "SCT estero - Sepa Credit Transfer"
        endcase
      case this.pOper="0"
        * --- Dal Metodo Load
        SELECT (this.oParentObject.cTrsName)
        GO TOP
        SCAN
        this.oParentObject.WorkFromTrs()
        this.oParentObject.w_DESPAG = SPACE(25)
        do case
          case this.oParentObject.w_PRTIPPAG="RB"
            if g_APPLICATION = "ad hoc ENTERPRISE"
              this.oParentObject.w_DESPAG = "Ricevute Bancarie"
            else
              this.oParentObject.w_DESPAG = "Ric.Bancarie/RIBA"
            endif
          case this.oParentObject.w_PRTIPPAG="BO"
            this.oParentObject.w_DESPAG = "Bonifici"
          case this.oParentObject.w_PRTIPPAG="MA"
            this.oParentObject.w_DESPAG = "M.AV."
          case this.oParentObject.w_PRTIPPAG="CA"
            this.oParentObject.w_DESPAG = "Cambiali/Tratte"
          case this.oParentObject.w_PRTIPPAG="RD"
            this.oParentObject.w_DESPAG = "Rimesse Dirette"
          case this.oParentObject.w_PRTIPPAG="RI"
            this.oParentObject.w_DESPAG = "R.I.D."
          case this.oParentObject.w_PRTIPPAG="AB"
            this.oParentObject.w_DESPAG = "Avviso di Bonifico"
          case this.oParentObject.w_PRTIPPAG="AD"
            this.oParentObject.w_DESPAG = "Anticip. c/documenti"
          case this.oParentObject.w_PRTIPPAG="BE"
            this.oParentObject.w_DESPAG = "Bonifici esteri"
          case this.oParentObject.w_PRTIPPAG="SD"
            this.oParentObject.w_DESPAG = "SDD - Sepa Direct Debit"
          case this.oParentObject.w_PRTIPPAG="SC"
            this.oParentObject.w_DESPAG = "SCT - Sepa Credit Transfer"
          case this.oParentObject.w_PRTIPPAG="SE"
            this.oParentObject.w_DESPAG = "SCT estero - Sepa Credit Transfer"
        endcase
        this.oParentObject.TrsFromWork()
        ENDSCAN
        * --- Questa Parte derivata dal Metodo LoadRec
        SELECT (this.oParentObject.cTrsName)
        GO TOP
        With this.oParentObject
        .WorkFromTrs()
        .SetControlsValue()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        EndWith
    endcase
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
