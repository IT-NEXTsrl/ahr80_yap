* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bkd                                                        *
*              Caricamento dizionario dati                                     *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][101]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-23                                                      *
* Last revis.: 2013-10-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bkd",oParentObject)
return(i_retval)

define class tgsut_bkd as StdBatch
  * --- Local variables
  nTable = 0
  ni = 0
  cTable = space(15)
  cTableComment = space(15)
  cPhTable = space(15)
  cTimeStamp = space(14)
  nStart = 0
  nField = 0
  nEnd = 0
  cField = space(15)
  nRowNum = 0
  w_FLCOMMEN = space(80)
  * --- WorkFile variables
  XDC_FIELDS_idx=0
  XDC_TABLE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- CARICAMENTO DIZIONARIO DATI
    * --- Variabili Globali
    *     w_FlagUpdate per effettuare aggiornamento (valori S e N)
    *     w_FlagDelete per cancellare dizionario dati corrente prima di procedere con l'aggiornamento (valori S e N)
    if not upper(this.oParentObject.class)="TGSUT_BDA"
      this.oParentObject.Mousepointer = 11
    endif
    if this.oParentObject.w_FlagDelete="S"
      * --- Elimina tabella XDC_FIELD e XDC_TABLE
      * --- Try
      local bErr_03C1AE40
      bErr_03C1AE40=bTrsErr
      this.Try_03C1AE40()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
      endif
      bTrsErr=bTrsErr or bErr_03C1AE40
      * --- End
    endif
    if type("i_dcx")="U"
      * --- Lettura dizionario dati da XDC
      cp_ReadXDC()
    endif
    this.nTable = alen(i_dcx.tbls,1)
    for this.ni=1 to this.nTable
    if !empty(i_dcx.tbls[this.ni,1])
      this.cTable = upper(i_Dcx.tbls[this.ni,1]) 
      this.cTableComment=i_Dcx.tbls[this.ni,12]
      this.cTimeStamp = ""
      * --- Read from XDC_TABLE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.XDC_TABLE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.XDC_TABLE_idx,2],.t.,this.XDC_TABLE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TIMESTAMP,TBPHNAME"+;
          " from "+i_cTable+" XDC_TABLE where ";
              +"TBNAME = "+cp_ToStrODBC(this.cTable);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TIMESTAMP,TBPHNAME;
          from (i_cTable) where;
              TBNAME = this.cTable;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.cTimeStamp = NVL(cp_ToDate(_read_.TIMESTAMP),cp_NullValue(_read_.TIMESTAMP))
        cPhName = NVL(cp_ToDate(_read_.TBPHNAME),cp_NullValue(_read_.TBPHNAME))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if i_Dcx.tbls[this.ni,13] > this.cTimestamp or this.oParentObject.w_FlagUpdate="S"
        ah_msg( "Caricamento tabella %1...",.t.,.f.,.f.,alltrim(this.cTable) +chr(32)+chr(46)+chr(32)+ alltrim(this.cTableComment))
        * --- Try
        local bErr_039B15F0
        bErr_039B15F0=bTrsErr
        this.Try_039B15F0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
        endif
        bTrsErr=bTrsErr or bErr_039B15F0
        * --- End
      endif
    endif
    next
    if g_VEFA="S"
      * --- Se il modulo VEFA (EDI) � attivo carico anche le tabelle EDI
      do GSVA_BCX with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if not upper(this.oParentObject.class)="TGSUT_BDA"
      this.oParentObject.Mousepointer = 0
    endif
    ah_msg( "Caricamento terminato",.f.,.f.,2)
  endproc
  proc Try_03C1AE40()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Delete from XDC_FIELDS
    i_nConn=i_TableProp[this.XDC_FIELDS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.XDC_FIELDS_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"1 = "+cp_ToStrODBC(1);
             )
    else
      delete from (i_cTable) where;
            1 = 1;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from XDC_TABLE
    i_nConn=i_TableProp[this.XDC_TABLE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.XDC_TABLE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"1 = "+cp_ToStrODBC(1);
             )
    else
      delete from (i_cTable) where;
            1 = 1;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_039B15F0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Try
    local bErr_039583C0
    bErr_039583C0=bTrsErr
    this.Try_039583C0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_039583C0
    * --- End
    * --- Write into XDC_TABLE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.XDC_TABLE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.XDC_TABLE_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.XDC_TABLE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"TBPHNAME ="+cp_NullLink(cp_ToStrODBC(i_dcx.tbls[this.ni,2]),'XDC_TABLE','TBPHNAME');
      +",TBCOMMENT ="+cp_NullLink(cp_ToStrODBC(i_dcx.tbls[this.ni,12]),'XDC_TABLE','TBCOMMENT');
      +",TBCOMPANY ="+cp_NullLink(cp_ToStrODBC(i_dcx.tbls[this.ni,5]),'XDC_TABLE','TBCOMPANY');
      +",TBUSER ="+cp_NullLink(cp_ToStrODBC(i_dcx.tbls[this.ni,4]),'XDC_TABLE','TBUSER');
      +",TBYEAR ="+cp_NullLink(cp_ToStrODBC(i_dcx.tbls[this.ni,3]),'XDC_TABLE','TBYEAR');
      +",TIMESTAMP ="+cp_NullLink(cp_ToStrODBC(i_dcx.tbls[this.ni,13]),'XDC_TABLE','TIMESTAMP');
          +i_ccchkf ;
      +" where ";
          +"TBNAME = "+cp_ToStrODBC(this.cTable);
             )
    else
      update (i_cTable) set;
          TBPHNAME = i_dcx.tbls[this.ni,2];
          ,TBCOMMENT = i_dcx.tbls[this.ni,12];
          ,TBCOMPANY = i_dcx.tbls[this.ni,5];
          ,TBUSER = i_dcx.tbls[this.ni,4];
          ,TBYEAR = i_dcx.tbls[this.ni,3];
          ,TIMESTAMP = i_dcx.tbls[this.ni,13];
          &i_ccchkf. ;
       where;
          TBNAME = this.cTable;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_039583C0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into XDC_TABLE
    i_nConn=i_TableProp[this.XDC_TABLE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.XDC_TABLE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.XDC_TABLE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TBNAME"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.cTable),'XDC_TABLE','TBNAME');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TBNAME',this.cTable)
      insert into (i_cTable) (TBNAME &i_ccchkf. );
         values (;
           this.cTable;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento campi tabella
    * --- Elimina contenuto tabella
    * --- Delete from XDC_FIELDS
    i_nConn=i_TableProp[this.XDC_FIELDS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.XDC_FIELDS_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"TBNAME = "+cp_ToStrODBC(this.cTable);
             )
    else
      delete from (i_cTable) where;
            TBNAME = this.cTable;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    this.nStart = i_dcx.tbls[this.ni,7]+1
    this.nEnd = i_dcx.tbls[this.ni,6]+i_dcx.tbls[this.ni,7]
    this.nRowNum = 0
    for this.nField=this.nStart to this.nEnd
    this.cField = i_dcx.flds[this.nField,1]
    if !empty(this.cField)
      this.nRowNum = this.nRowNum + 1
      this.w_FLCOMMEN = left(nvl(i_dcx.fldscomment[this.nField],"")+" ",80)
      * --- Insert into XDC_FIELDS
      i_nConn=i_TableProp[this.XDC_FIELDS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.XDC_FIELDS_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.XDC_FIELDS_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"TBNAME"+",CPROWNUM"+",FLNAME"+",FLTYPE"+",FLLENGHT"+",FLDECIMA"+",FLCOMMEN"+",FLCHECK"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.cTable),'XDC_FIELDS','TBNAME');
        +","+cp_NullLink(cp_ToStrODBC(this.nRowNum),'XDC_FIELDS','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(i_dcx.flds[this.nField,1]),'XDC_FIELDS','FLNAME');
        +","+cp_NullLink(cp_ToStrODBC(i_dcx.fldstype[this.nField]),'XDC_FIELDS','FLTYPE');
        +","+cp_NullLink(cp_ToStrODBC(i_dcx.fldsLen[this.nField]),'XDC_FIELDS','FLLENGHT');
        +","+cp_NullLink(cp_ToStrODBC(i_dcx.fldsDec[this.nField]),'XDC_FIELDS','FLDECIMA');
        +","+cp_NullLink(cp_ToStrODBC(this.w_FLCOMMEN),'XDC_FIELDS','FLCOMMEN');
        +","+cp_NullLink(cp_ToStrODBC(i_dcx.fldsCheck[this.nField]),'XDC_FIELDS','FLCHECK');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'TBNAME',this.cTable,'CPROWNUM',this.nRowNum,'FLNAME',i_dcx.flds[this.nField,1],'FLTYPE',i_dcx.fldstype[this.nField],'FLLENGHT',i_dcx.fldsLen[this.nField],'FLDECIMA',i_dcx.fldsDec[this.nField],'FLCOMMEN',this.w_FLCOMMEN,'FLCHECK',i_dcx.fldsCheck[this.nField])
        insert into (i_cTable) (TBNAME,CPROWNUM,FLNAME,FLTYPE,FLLENGHT,FLDECIMA,FLCOMMEN,FLCHECK &i_ccchkf. );
           values (;
             this.cTable;
             ,this.nRowNum;
             ,i_dcx.flds[this.nField,1];
             ,i_dcx.fldstype[this.nField];
             ,i_dcx.fldsLen[this.nField];
             ,i_dcx.fldsDec[this.nField];
             ,this.w_FLCOMMEN;
             ,i_dcx.fldsCheck[this.nField];
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    next
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='XDC_FIELDS'
    this.cWorkTables[2]='XDC_TABLE'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
