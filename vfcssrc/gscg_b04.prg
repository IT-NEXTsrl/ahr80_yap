* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_b04                                                        *
*              Elabora S.A. cespiti                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_72]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-02-12                                                      *
* Last revis.: 2014-08-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_b04",oParentObject)
return(i_retval)

define class tgscg_b04 as StdBatch
  * --- Local variables
  w_ERRDESCRI = space(50)
  w_CAUCES = space(5)
  w_APPO1 = 0
  w_ERRORE = 0
  w_CODBUN = space(3)
  w_IMPRIG = 0
  w_MESS = space(10)
  w_COMPET = space(4)
  w_APPVAL = 0
  w_CODVAL = space(3)
  w_APPSEZ = space(1)
  w_CODCEN = space(15)
  w_CAOVAL = 0
  w_CODICE = space(15)
  w_VOCCEN = space(15)
  w_INIESE = ctod("  /  /  ")
  w_IMPDAR = 0
  w_CODCOM = space(15)
  w_FINESE = ctod("  /  /  ")
  w_IMPAVE = 0
  w_DATCIN = ctod("  /  /  ")
  w_ACOD = space(3)
  w_DATREG = ctod("  /  /  ")
  w_DATCFI = ctod("  /  /  ")
  w_CDAR = space(3)
  w_CODGRU = space(15)
  w_CAVE = space(3)
  w_CODCAT = space(15)
  w_PADATINI = ctod("  /  /  ")
  w_PADATFIN = ctod("  /  /  ")
  w_oMess = .NULL.
  w_oPart = .NULL.
  * --- WorkFile variables
  VALUTE_idx=0
  PAR_CESP_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questo batch viene usato in Elaborazione Scritture di Assestamento (da GSCG_BAT)
    *     Viene impostato nelle regole cablate nel programma di lancio relative ai Cespiti
    this.w_oMess=createobject("Ah_Message")
    this.w_oMess.AddMsgPartNL("Elaborazione cespiti")     
    this.w_INIESE = g_INIESE
    this.w_FINESE = g_FINESE
    this.w_ERRORE = 0
    this.w_MESS = " "
    DIMENSION IMP[16], GPC[10],CONCESP[10]
    * --- Read from PAR_CESP
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_CESP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_CESP_idx,2],.t.,this.PAR_CESP_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PCCONC01,PCCONC02,PCCONC03,PCCONC04,PCCONC05,PCCONC06,PCCONC07,PCCONC08,PCCONC09,PCCONC10"+;
        " from "+i_cTable+" PAR_CESP where ";
            +"PCCODAZI = "+cp_ToStrODBC(i_codazi);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PCCONC01,PCCONC02,PCCONC03,PCCONC04,PCCONC05,PCCONC06,PCCONC07,PCCONC08,PCCONC09,PCCONC10;
        from (i_cTable) where;
            PCCODAZI = i_codazi;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      CONCESP[1] = NVL(cp_ToDate(_read_.PCCONC01),cp_NullValue(_read_.PCCONC01))
      CONCESP[2] = NVL(cp_ToDate(_read_.PCCONC02),cp_NullValue(_read_.PCCONC02))
      CONCESP[3] = NVL(cp_ToDate(_read_.PCCONC03),cp_NullValue(_read_.PCCONC03))
      CONCESP[4] = NVL(cp_ToDate(_read_.PCCONC04),cp_NullValue(_read_.PCCONC04))
      CONCESP[5] = NVL(cp_ToDate(_read_.PCCONC05),cp_NullValue(_read_.PCCONC05))
      CONCESP[6] = NVL(cp_ToDate(_read_.PCCONC06),cp_NullValue(_read_.PCCONC06))
      CONCESP[7] = NVL(cp_ToDate(_read_.PCCONC07),cp_NullValue(_read_.PCCONC07))
      CONCESP[8] = NVL(cp_ToDate(_read_.PCCONC08),cp_NullValue(_read_.PCCONC08))
      CONCESP[9] = NVL(cp_ToDate(_read_.PCCONC09),cp_NullValue(_read_.PCCONC09))
      CONCESP[10] = NVL(cp_ToDate(_read_.PCCONC10),cp_NullValue(_read_.PCCONC10))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if USED("EXTCE001")
      * --- Crea il Temporaneo di Elaborazione Primanota
      CREATE CURSOR EXTCG001 ;
      (TIPCON C(1), CODICE C(15), CODBUN C(3), INICOM D(8), FINCOM D(8), ;
      VALNAZ C(3), DATREG D(8), IMPDAR N(18,4), IMPAVE N(18,4), ;
      CODCEN C(15), VOCCEN C(15), CODCOM C(15), DATCIN D(8), DATCFI D(8))
      * --- Inizio Aggiornamento vero e proprio
      ah_Msg("Elaborazione movimenti cespite da generare...",.T.)
      SELECT EXTCE001
      GO TOP
      SCAN FOR NOT EMPTY(NVL(MCSERIAL," ")) AND NOT EMPTY(NVL(MCCODCAU," "))
      this.w_CAUCES = MCCODCAU
      this.w_DATREG = NVL(MCDATREG, i_datsys)
      if g_APPLICATION <> "ADHOC REVOLUTION"
        this.w_CODBUN = NVL(MCCODBUN, g_CODBUN)
        this.w_DATCIN = NVL(MCDATCIN, cp_CharToDate("  -  -    "))
        this.w_DATCFI = NVL(MCDATCFI, cp_CharToDate("  -  -    "))
        if NOT EMPTY(NVL(CEDTPRIU, cp_CharToDate("  -  -    "))) AND NVL(PADATINI, cp_CharToDate("  -  -    "))<NVL(CEDTPRIU, cp_CharToDate("  -  -    ")) 
          this.w_PADATINI = NVL(CEDTPRIU, cp_CharToDate("  -  -    "))
        else
          this.w_PADATINI = NVL(PADATINI, cp_CharToDate("  -  -    "))
        endif
        this.w_PADATFIN = NVL(PADATFIN, cp_CharToDate("  -  -    "))
      else
        this.w_CODBUN = Space(3)
        this.w_DATCIN = cp_CharToDate("  -  -    ")
        this.w_DATCFI = cp_CharToDate("  -  -    ")
        this.w_PADATINI = cp_CharToDate("  -  -    ")
        this.w_PADATFIN = cp_CharToDate("  -  -    ")
      endif
      this.w_COMPET = NVL(MCCOMPET, SPACE(4))
      this.w_CODVAL = NVL(MCCODVAL, g_PERVAL)
      this.w_CAOVAL = NVL(MCCAOVAL, g_CAOVAL)
      this.w_CODGRU = NVL(GPCODICE, SPACE(15))
      this.w_CODCAT = NVL(CCCODICE, SPACE(15))
      this.w_CODCEN = NVL(MCCODCEN, SPACE(15))
      this.w_VOCCEN = NVL(MCVOCCEN, SPACE(15))
      this.w_CODCOM = NVL(MCCODCOM, SPACE(15))
      if NOT EMPTY(this.w_CODGRU)
        GPC[1] = NVL(GPCONC01, SPACE(15))
        GPC[2] = NVL(GPCONC02, SPACE(15))
        GPC[3] = NVL(GPCONC03, SPACE(15))
        GPC[4] = NVL(GPCONC04, SPACE(15))
        GPC[5] = NVL(GPCONC05, SPACE(15))
        GPC[6] = NVL(GPCONC06, SPACE(15))
        GPC[7] = NVL(GPCONC07, SPACE(15))
        GPC[8] = NVL(GPCONC08, SPACE(15))
        GPC[9] = NVL(GPCONC09, SPACE(15))
        GPC[10] = NVL(GPCONC10, SPACE(15))
        IMP[1] = NVL(MCIMPA01, 0)
        IMP[2] = NVL(MCIMPA02, 0)
        IMP[3] = NVL(MCIMPA03, 0)
        IMP[4] = NVL(MCIMPA04, 0)
        IMP[5] = NVL(MCIMPA05, 0)
        IMP[6] = NVL(MCIMPA06, 0)
        IMP[7] = NVL(MCIMPA07, 0)
        IMP[8] = NVL(MCIMPA08, 0)
        IMP[9] = NVL(MCIMPA09, 0)
        IMP[10] = NVL(MCIMPA10, 0)
        IMP[11] = NVL(MCIMPA11, 0)
        IMP[12] = NVL(MCIMPA12, 0)
        IMP[13] = NVL(MCIMPA13, 0)
        IMP[14] = NVL(MCIMPA14, 0)
        IMP[15] = NVL(MCIMPA15, 0)
        IMP[16] = NVL(MCIMPA16, 0)
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        this.w_ERRORE = 1
      endif
      SELECT EXTCE001
      ENDSCAN
      * --- Elimina il Cursore
      SELECT EXTCE001
      USE
    endif
    do case
      case this.w_ERRORE=1
        this.w_oMess.AddMsgPartNL("ATTENZIONE:")     
        this.w_oMess.AddMsgPartNL("Alcuni articoli non sono associati a categorie omogenee")     
        this.w_oMess.Ah_ErrorMsg()     
      case this.w_ERRORE=2
        this.w_oMess.Ah_ErrorMsg()     
    endcase
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Cicla Sui Modelli Contabili
    * --- Select from GSCG_B04
    do vq_exec with 'GSCG_B04',this,'_Curs_GSCG_B04','',.f.,.t.
    if used('_Curs_GSCG_B04')
      select _Curs_GSCG_B04
      locate for 1=1
      do while not(eof())
      this.w_ACOD = NVL(_Curs_GSCG_B04.MCCODICE,"   ")
      this.w_CDAR = NVL(_Curs_GSCG_B04.MCCONDAR, "   ")
      this.w_CAVE = NVL(_Curs_GSCG_B04.MCCONAVE, "   ")
      if NOT EMPTY(this.w_ACOD) AND LEN(this.w_ACOD)=3
        this.w_APPO1 = VAL(RIGHT(this.w_ACOD,2))
        if this.w_APPO1>0 AND this.w_APPO1<17
          if IMP[this.w_APPO1]<>0
            * --- Importo da Scrivere
            this.w_IMPRIG = IMP[this.w_APPO1]
            IMP[this.w_APPO1] = 0
            * --- Cerca Conto DARE
            if NOT EMPTY(this.w_CDAR) AND LEN(this.w_CDAR)=3
              this.w_APPO1 = VAL(RIGHT(this.w_CDAR,2))
              if this.w_APPO1>0 AND this.w_APPO1<11
                if NOT EMPTY(GPC[this.w_APPO1])
                  this.w_APPVAL = this.w_IMPRIG
                  this.w_APPSEZ = "D"
                  this.w_CODICE = GPC[this.w_APPO1]
                  this.Page_3()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                else
                  this.oParentObject.w_ERROR = .T.
                  this.w_ERRDESCRI = AH_MsgFormat('Manca la contropartita %1 "%3" nel gruppo contabile %2',this.w_CDAR,alltrim(this.w_CODGRU) , alltrim ( NVL(CONCESP[this.w_APPO1],"")) )
                  INSERT INTO CursError (REGOLA,CODICE,CPROWNUM,DESCRI,TIPCON) ;
                  VALUES (AH_Msgformat(this.oParentObject.w_REGOLA),"",0, this.w_ERRDESCRI, "")
                endif
              endif
            endif
            * --- Cerca Conto AVERE
            if NOT EMPTY(this.w_CAVE) AND LEN(this.w_CAVE)=3
              this.w_APPO1 = VAL(RIGHT(this.w_CAVE,2))
              if this.w_APPO1>0 AND this.w_APPO1<11
                if NOT EMPTY(GPC[this.w_APPO1])
                  this.w_APPVAL = this.w_IMPRIG
                  this.w_APPSEZ = "A"
                  this.w_CODICE = GPC[this.w_APPO1]
                  this.Page_3()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                else
                  this.oParentObject.w_ERROR = .T.
                  this.w_ERRDESCRI = AH_MsgFormat('Manca la contropartita %1 "%3" nel gruppo contabile %2',this.w_CAVE,alltrim(this.w_CODGRU) , alltrim ( NVL(CONCESP[this.w_APPO1],"")) )
                  INSERT INTO CursError (REGOLA,CODICE,CPROWNUM,DESCRI,TIPCON) ;
                  VALUES (AH_Msgformat(this.oParentObject.w_REGOLA),"",0, this.w_ERRDESCRI, "")
                endif
              endif
            endif
          endif
        endif
      endif
        select _Curs_GSCG_B04
        continue
      enddo
      use
    endif
    * --- Verifica 'Orfani'
    if this.w_ERRORE=0
      FOR L_i = 1 TO 16
      if IMP[L_i]<>0
        this.w_ERRORE = 2
        this.w_oPart = this.w_oMess.AddMsgPartNL("Verificare modelli contabili causale cespite:%1")
        this.w_oPart.AddParam(this.w_CAUCES)     
        this.w_oPart = this.w_oMess.AddMsgPartNL("Riferimento: a %1 non definito!")
        this.w_oPart.AddParam(RIGHT("00"+ALLTRIM(STR(L_i)), 2))     
      endif
      ENDFOR
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna Temporaneo di Elaborazione Primanota
    if this.w_CODVAL<>g_PERVAL
      * --- Se arriva un Importo in Valuta e il Documento e' in Valuta Converte in Moneta di Conto
      this.w_APPVAL = VAL2MON(this.w_APPVAL,this.w_CAOVAL,GETCAM(g_PERVAL,this.w_DATREG),this.w_DATREG,g_PERVAL, g_PERPVL)
    endif
    this.w_APPSEZ = IIF(this.w_APPVAL<0, IIF(this.w_APPSEZ="D", "A", "D"), this.w_APPSEZ)
    if this.w_APPSEZ="D"
      this.w_IMPDAR = ABS(this.w_APPVAL)
      this.w_IMPAVE = 0
    else
      this.w_IMPDAR = 0
      this.w_IMPAVE = ABS(this.w_APPVAL)
    endif
    INSERT INTO EXTCG001 (TIPCON, CODICE, CODBUN, INICOM, FINCOM, ; 
 VALNAZ, DATREG, IMPDAR, IMPAVE, ; 
 CODCEN, VOCCEN, CODCOM, DATCIN, DATCFI) ; 
 VALUES ("G", this.w_CODICE, this.w_CODBUN, IIF(EMPTY(this.w_PADATINI),this.w_INIESE,this.w_PADATINI), ; 
 IIF(EMPTY(this.w_PADATFIN), this.w_FINESE, this.w_PADATFIN), ; 
 g_PERVAL, this.w_DATREG, this.w_IMPDAR, this.w_IMPAVE, ; 
 this.w_CODCEN, this.w_VOCCEN, this.w_CODCOM, this.w_DATCIN, this.w_DATCFI)
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='VALUTE'
    this.cWorkTables[2]='PAR_CESP'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_GSCG_B04')
      use in _Curs_GSCG_B04
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
