* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsca_scp                                                        *
*              Bilancio per voce C. C/R e conto                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_30]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-03-18                                                      *
* Last revis.: 2009-12-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsca_scp",oParentObject))

* --- Class definition
define class tgsca_scp as StdForm
  Top    = 39
  Left   = 71

  * --- Standard Properties
  Width  = 520
  Height = 267
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-12-01"
  HelpContextID=177568407
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=15

  * --- Constant Properties
  _IDX = 0
  AZIENDA_IDX = 0
  CAL_RIPA_IDX = 0
  VOC_COST_IDX = 0
  cPrg = "gsca_scp"
  cComment = "Bilancio per voce C. C/R e conto"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_AZIENDA = space(5)
  w_VOCE1 = space(15)
  o_VOCE1 = space(15)
  w_VOCE2 = space(15)
  w_DESCRI = space(35)
  w_DATA1 = ctod('  /  /  ')
  o_DATA1 = ctod('  /  /  ')
  w_DATA2 = ctod('  /  /  ')
  w_TIPO = space(1)
  w_PROVE = space(1)
  w_COMPET = space(1)
  w_DESCRI2 = space(35)
  w_FLANAL = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_tipo1 = space(1)
  w_PERCOMPE = space(1)
  o_PERCOMPE = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsca_scpPag1","gsca_scp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oVOCE1_1_5
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='CAL_RIPA'
    this.cWorkTables[3]='VOC_COST'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSCA_BCD with this
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsca_scp
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AZIENDA=space(5)
      .w_VOCE1=space(15)
      .w_VOCE2=space(15)
      .w_DESCRI=space(35)
      .w_DATA1=ctod("  /  /  ")
      .w_DATA2=ctod("  /  /  ")
      .w_TIPO=space(1)
      .w_PROVE=space(1)
      .w_COMPET=space(1)
      .w_DESCRI2=space(35)
      .w_FLANAL=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_tipo1=space(1)
      .w_PERCOMPE=space(1)
        .w_AZIENDA = i_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_AZIENDA))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_VOCE1))
          .link_1_5('Full')
        endif
        .w_VOCE2 = .w_VOCE1
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_VOCE2))
          .link_1_6('Full')
        endif
          .DoRTCalc(4,4,.f.)
        .w_DATA1 = g_iniese
        .w_DATA2 = g_finese
        .w_TIPO = 'E'
        .w_PROVE = 'O'
        .w_COMPET = IIF(.w_PERCOMPE='S',.w_PERCOMPE,'N')
      .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
          .DoRTCalc(10,11,.f.)
        .w_OBTEST = .w_DATA1
          .DoRTCalc(13,13,.f.)
        .w_tipo1 = 'G'
    endwith
    this.DoRTCalc(15,15,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,2,.t.)
        if .o_VOCE1<>.w_VOCE1
            .w_VOCE2 = .w_VOCE1
          .link_1_6('Full')
        endif
        .DoRTCalc(4,8,.t.)
        if .o_PERCOMPE<>.w_PERCOMPE
            .w_COMPET = IIF(.w_PERCOMPE='S',.w_PERCOMPE,'N')
        endif
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        .DoRTCalc(10,11,.t.)
        if .o_DATA1<>.w_DATA1
            .w_OBTEST = .w_DATA1
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(13,15,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCOMPET_1_12.enabled = this.oPgFrm.Page1.oPag.oCOMPET_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCOMPET_1_12.visible=!this.oPgFrm.Page1.oPag.oCOMPET_1_12.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_13.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=AZIENDA
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZIENDA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZIENDA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRIPCOM";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_AZIENDA)
            select AZCODAZI,AZRIPCOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZIENDA = NVL(_Link_.AZCODAZI,space(5))
      this.w_PERCOMPE = NVL(_Link_.AZRIPCOM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_AZIENDA = space(5)
      endif
      this.w_PERCOMPE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZIENDA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VOCE1
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_COST_IDX,3]
    i_lTable = "VOC_COST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2], .t., this.VOC_COST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VOCE1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_AVC',True,'VOC_COST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VCCODICE like "+cp_ToStrODBC(trim(this.w_VOCE1)+"%");

          i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VCCODICE',trim(this.w_VOCE1))
          select VCCODICE,VCDESCRI,VCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VOCE1)==trim(_Link_.VCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VOCE1) and !this.bDontReportError
            deferred_cp_zoom('VOC_COST','*','VCCODICE',cp_AbsName(oSource.parent,'oVOCE1_1_5'),i_cWhere,'GSCA_AVC',"Voci centri di costo/ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',oSource.xKey(1))
            select VCCODICE,VCDESCRI,VCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VOCE1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(this.w_VOCE1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',this.w_VOCE1)
            select VCCODICE,VCDESCRI,VCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VOCE1 = NVL(_Link_.VCCODICE,space(15))
      this.w_DESCRI = NVL(_Link_.VCDESCRI,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.VCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_VOCE1 = space(15)
      endif
      this.w_DESCRI = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(empty(.w_VOCE2) OR .w_VOCE2>=.w_VOCE1) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure obsoleto")
        endif
        this.w_VOCE1 = space(15)
        this.w_DESCRI = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])+'\'+cp_ToStr(_Link_.VCCODICE,1)
      cp_ShowWarn(i_cKey,this.VOC_COST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VOCE1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VOCE2
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_COST_IDX,3]
    i_lTable = "VOC_COST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2], .t., this.VOC_COST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VOCE2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_AVC',True,'VOC_COST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VCCODICE like "+cp_ToStrODBC(trim(this.w_VOCE2)+"%");

          i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VCCODICE',trim(this.w_VOCE2))
          select VCCODICE,VCDESCRI,VCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VOCE2)==trim(_Link_.VCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VOCE2) and !this.bDontReportError
            deferred_cp_zoom('VOC_COST','*','VCCODICE',cp_AbsName(oSource.parent,'oVOCE2_1_6'),i_cWhere,'GSCA_AVC',"Voci centri di costo/ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',oSource.xKey(1))
            select VCCODICE,VCDESCRI,VCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VOCE2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(this.w_VOCE2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',this.w_VOCE2)
            select VCCODICE,VCDESCRI,VCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VOCE2 = NVL(_Link_.VCCODICE,space(15))
      this.w_DESCRI2 = NVL(_Link_.VCDESCRI,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.VCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_VOCE2 = space(15)
      endif
      this.w_DESCRI2 = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(empty(.w_VOCE1) OR .w_VOCE2>=.w_VOCE1) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure obsoleto")
        endif
        this.w_VOCE2 = space(15)
        this.w_DESCRI2 = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])+'\'+cp_ToStr(_Link_.VCCODICE,1)
      cp_ShowWarn(i_cKey,this.VOC_COST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VOCE2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oVOCE1_1_5.value==this.w_VOCE1)
      this.oPgFrm.Page1.oPag.oVOCE1_1_5.value=this.w_VOCE1
    endif
    if not(this.oPgFrm.Page1.oPag.oVOCE2_1_6.value==this.w_VOCE2)
      this.oPgFrm.Page1.oPag.oVOCE2_1_6.value=this.w_VOCE2
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_7.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_7.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA1_1_8.value==this.w_DATA1)
      this.oPgFrm.Page1.oPag.oDATA1_1_8.value=this.w_DATA1
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA2_1_9.value==this.w_DATA2)
      this.oPgFrm.Page1.oPag.oDATA2_1_9.value=this.w_DATA2
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPO_1_10.RadioValue()==this.w_TIPO)
      this.oPgFrm.Page1.oPag.oTIPO_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPROVE_1_11.RadioValue()==this.w_PROVE)
      this.oPgFrm.Page1.oPag.oPROVE_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOMPET_1_12.RadioValue()==this.w_COMPET)
      this.oPgFrm.Page1.oPag.oCOMPET_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI2_1_18.value==this.w_DESCRI2)
      this.oPgFrm.Page1.oPag.oDESCRI2_1_18.value=this.w_DESCRI2
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((empty(.w_VOCE2) OR .w_VOCE2>=.w_VOCE1) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_VOCE1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oVOCE1_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure obsoleto")
          case   not((empty(.w_VOCE1) OR .w_VOCE2>=.w_VOCE1) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_VOCE2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oVOCE2_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure obsoleto")
          case   ((empty(.w_DATA1)) or not( .w_data1<=.w_data2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATA1_1_8.SetFocus()
            i_bnoObbl = !empty(.w_DATA1)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   ((empty(.w_DATA2)) or not(.w_data1<=.w_data2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATA2_1_9.SetFocus()
            i_bnoObbl = !empty(.w_DATA2)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_VOCE1 = this.w_VOCE1
    this.o_DATA1 = this.w_DATA1
    this.o_PERCOMPE = this.w_PERCOMPE
    return

enddefine

* --- Define pages as container
define class tgsca_scpPag1 as StdContainer
  Width  = 516
  height = 267
  stdWidth  = 516
  stdheight = 267
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oVOCE1_1_5 as StdField with uid="SOJXJTMUYF",rtseq=2,rtrep=.f.,;
    cFormVar = "w_VOCE1", cQueryName = "VOCE1",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore del codice finale oppure obsoleto",;
    ToolTipText = "Voce di inizio selezione",;
    HelpContextID = 233766742,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=101, Top=24, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="VOC_COST", cZoomOnZoom="GSCA_AVC", oKey_1_1="VCCODICE", oKey_1_2="this.w_VOCE1"

  func oVOCE1_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oVOCE1_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVOCE1_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_COST','*','VCCODICE',cp_AbsName(this.parent,'oVOCE1_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_AVC',"Voci centri di costo/ricavo",'',this.parent.oContained
  endproc
  proc oVOCE1_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSCA_AVC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VCCODICE=this.parent.oContained.w_VOCE1
     i_obj.ecpSave()
  endproc

  add object oVOCE2_1_6 as StdField with uid="FMSXRKXFUE",rtseq=3,rtrep=.f.,;
    cFormVar = "w_VOCE2", cQueryName = "VOCE2",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore del codice finale oppure obsoleto",;
    ToolTipText = "Voce di fine selezione",;
    HelpContextID = 234815318,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=101, Top=52, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="VOC_COST", cZoomOnZoom="GSCA_AVC", oKey_1_1="VCCODICE", oKey_1_2="this.w_VOCE2"

  func oVOCE2_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oVOCE2_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVOCE2_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_COST','*','VCCODICE',cp_AbsName(this.parent,'oVOCE2_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_AVC',"Voci centri di costo/ricavo",'',this.parent.oContained
  endproc
  proc oVOCE2_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSCA_AVC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VCCODICE=this.parent.oContained.w_VOCE2
     i_obj.ecpSave()
  endproc

  add object oDESCRI_1_7 as StdField with uid="LJWKQNWMSX",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 117574602,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=245, Top=24, InputMask=replicate('X',35)

  add object oDATA1_1_8 as StdField with uid="BKYRQPGBXB",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DATA1", cQueryName = "DATA1",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data movimento di inizio stampa",;
    HelpContextID = 233570358,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=101, Top=80

  func oDATA1_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ( .w_data1<=.w_data2)
    endwith
    return bRes
  endfunc

  add object oDATA2_1_9 as StdField with uid="WDJKFGOKJX",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DATA2", cQueryName = "DATA2",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data movimento di fine stampa",;
    HelpContextID = 234618934,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=101, Top=108

  func oDATA2_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_data1<=.w_data2)
    endwith
    return bRes
  endfunc


  add object oTIPO_1_10 as StdCombo with uid="RLFGDHDCSD",rtseq=7,rtrep=.f.,left=101,top=136,width=128,height=21;
    , ToolTipText = "Tipo di movimento selezionato";
    , HelpContextID = 183093558;
    , cFormVar="w_TIPO",RowSource=""+"Effettivi,"+"Previsionali", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPO_1_10.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'P',;
    space(1))))
  endfunc
  func oTIPO_1_10.GetRadio()
    this.Parent.oContained.w_TIPO = this.RadioValue()
    return .t.
  endfunc

  func oTIPO_1_10.SetRadio()
    this.Parent.oContained.w_TIPO=trim(this.Parent.oContained.w_TIPO)
    this.value = ;
      iif(this.Parent.oContained.w_TIPO=='E',1,;
      iif(this.Parent.oContained.w_TIPO=='P',2,;
      0))
  endfunc


  add object oPROVE_1_11 as StdCombo with uid="EZQZCVXWGD",rtseq=8,rtrep=.f.,left=245,top=136,width=128,height=21;
    , ToolTipText = "Natura dei movimenti selezionati";
    , HelpContextID = 255902198;
    , cFormVar="w_PROVE",RowSource=""+"Originari,"+"Escluso ripartiti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPROVE_1_11.RadioValue()
    return(iif(this.value =1,'O',;
    iif(this.value =2,'E',;
    space(1))))
  endfunc
  func oPROVE_1_11.GetRadio()
    this.Parent.oContained.w_PROVE = this.RadioValue()
    return .t.
  endfunc

  func oPROVE_1_11.SetRadio()
    this.Parent.oContained.w_PROVE=trim(this.Parent.oContained.w_PROVE)
    this.value = ;
      iif(this.Parent.oContained.w_PROVE=='O',1,;
      iif(this.Parent.oContained.w_PROVE=='E',2,;
      0))
  endfunc

  add object oCOMPET_1_12 as StdCheck with uid="KOOXIGRZGQ",rtseq=9,rtrep=.f.,left=101, top=164, caption="Competenza",;
    ToolTipText = "Considera solo la parte di competenza del periodo impostato",;
    HelpContextID = 214262234,;
    cFormVar="w_COMPET", bObbl = .f. , nPag = 1;
    , TabStop=.f.;
   , bGlobalFont=.t.


  func oCOMPET_1_12.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCOMPET_1_12.GetRadio()
    this.Parent.oContained.w_COMPET = this.RadioValue()
    return .t.
  endfunc

  func oCOMPET_1_12.SetRadio()
    this.Parent.oContained.w_COMPET=trim(this.Parent.oContained.w_COMPET)
    this.value = ;
      iif(this.Parent.oContained.w_COMPET=='S',1,;
      0)
  endfunc

  func oCOMPET_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERCOMPE<>'S')
    endwith
   endif
  endfunc

  func oCOMPET_1_12.mHide()
    with this.Parent.oContained
      return (.w_PERCOMPE='S')
    endwith
  endfunc


  add object oObj_1_13 as cp_outputCombo with uid="ZLERXIVPWT",left=101, top=186, width=407,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 181304858


  add object oBtn_1_14 as StdButton with uid="TTRYGIPBAR",left=404, top=216, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , HelpContextID = 217512922;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      with this.Parent.oContained
        do GSCA_BCD with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_14.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_data1) and not empty(.w_data2))
      endwith
    endif
  endfunc


  add object oBtn_1_15 as StdButton with uid="SXCKQIHRAQ",left=458, top=216, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 184885830;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCRI2_1_18 as StdField with uid="DDOVHZVGUS",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DESCRI2", cQueryName = "DESCRI2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 117574602,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=245, Top=52, InputMask=replicate('X',35)

  add object oStr_1_2 as StdString with uid="QCOGBYVDIV",Visible=.t., Left=4, Top=24,;
    Alignment=1, Width=96, Height=15,;
    Caption="Da voce:"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="KTMKHTKYFA",Visible=.t., Left=4, Top=80,;
    Alignment=1, Width=96, Height=15,;
    Caption="Periodo dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="SMQYMYCUQT",Visible=.t., Left=4, Top=108,;
    Alignment=1, Width=96, Height=15,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="UFQOCOOPWV",Visible=.t., Left=4, Top=136,;
    Alignment=1, Width=96, Height=15,;
    Caption="Tipo movimenti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="JTAYAEXLYY",Visible=.t., Left=4, Top=52,;
    Alignment=1, Width=96, Height=15,;
    Caption="A voce:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="BHMPGKRLAY",Visible=.t., Left=4, Top=186,;
    Alignment=1, Width=96, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsca_scp','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
