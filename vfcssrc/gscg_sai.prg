* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_sai                                                        *
*              Stampa acconto IVA                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_132]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-21                                                      *
* Last revis.: 2015-12-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_sai",oParentObject))

* --- Class definition
define class tgscg_sai as StdForm
  Top    = 50
  Left   = 100

  * --- Standard Properties
  Width  = 539
  Height = 326
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-12-22"
  HelpContextID=124396905
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=34

  * --- Constant Properties
  _IDX = 0
  VALUTE_IDX = 0
  COC_MAST_IDX = 0
  DAT_IVAN_IDX = 0
  AZIENDA_IDX = 0
  ATTIDETT_IDX = 0
  cPrg = "gscg_sai"
  cComment = "Stampa acconto IVA"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CODAZI1 = space(5)
  o_CODAZI1 = space(5)
  w_FLREGI = space(1)
  w_CODATT1 = space(5)
  o_CODATT1 = space(5)
  w_DATSTA = ctod('  /  /  ')
  o_DATSTA = ctod('  /  /  ')
  w_DATVER = ctod('  /  /  ')
  w_ANNRIF = space(4)
  o_ANNRIF = space(4)
  w_CODBAN1 = space(15)
  w_CODBAN = space(15)
  w_METODO = space(1)
  o_METODO = space(1)
  w_DESMET = space(25)
  w_MACIV1 = 0
  w_MACIV2 = 0
  w_MACIVA = 0
  w_NOSPLIT1 = space(1)
  w_ACCIVA = 0
  w_INTLIG2 = space(1)
  w_TIPREG = space(1)
  w_PRPARI1 = 0
  w_PRPARI2 = 0
  w_VALIVA1 = space(3)
  w_OLDACC = 0
  w_VALIVA = space(3)
  w_CALCPICT = 0
  w_DESBAN1 = space(50)
  w_DESBAN = space(50)
  w_OBTEST = ctod('  /  /  ')
  w_DECTOT = 0
  o_DECTOT = 0
  w_CAOVAL = 0
  w_SIMVAL = space(5)
  w_PREFIS2 = space(20)
  w_NUMREG = 0
  w_DATSYS = ctod('  /  /  ')
  w_PEAIVA = 0
  w_CONCES1 = space(3)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_saiPag1","gscg_sai",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDATSTA_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='VALUTE'
    this.cWorkTables[2]='COC_MAST'
    this.cWorkTables[3]='DAT_IVAN'
    this.cWorkTables[4]='AZIENDA'
    this.cWorkTables[5]='ATTIDETT'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI1=space(5)
      .w_FLREGI=space(1)
      .w_CODATT1=space(5)
      .w_DATSTA=ctod("  /  /  ")
      .w_DATVER=ctod("  /  /  ")
      .w_ANNRIF=space(4)
      .w_CODBAN1=space(15)
      .w_CODBAN=space(15)
      .w_METODO=space(1)
      .w_DESMET=space(25)
      .w_MACIV1=0
      .w_MACIV2=0
      .w_MACIVA=0
      .w_NOSPLIT1=space(1)
      .w_ACCIVA=0
      .w_INTLIG2=space(1)
      .w_TIPREG=space(1)
      .w_PRPARI1=0
      .w_PRPARI2=0
      .w_VALIVA1=space(3)
      .w_OLDACC=0
      .w_VALIVA=space(3)
      .w_CALCPICT=0
      .w_DESBAN1=space(50)
      .w_DESBAN=space(50)
      .w_OBTEST=ctod("  /  /  ")
      .w_DECTOT=0
      .w_CAOVAL=0
      .w_SIMVAL=space(5)
      .w_PREFIS2=space(20)
      .w_NUMREG=0
      .w_DATSYS=ctod("  /  /  ")
      .w_PEAIVA=0
      .w_CONCES1=space(3)
        .w_CODAZI1 = i_codazi
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODAZI1))
          .link_1_1('Full')
        endif
        .w_FLREGI = 'S'
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODATT1))
          .link_1_3('Full')
        endif
        .w_DATSTA = i_datsys
        .w_DATVER = i_datsys
        .w_ANNRIF = STR(YEAR(.w_DATSTA), 4, 0)
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_ANNRIF))
          .link_1_6('Full')
        endif
          .DoRTCalc(7,7,.f.)
        .w_CODBAN = .w_CODBAN1
        .w_METODO = 'S'
          .DoRTCalc(10,12,.f.)
        .w_MACIVA = IIF(.w_VALIVA1=g_PERVAL, .w_MACIV2, .w_MACIV1)
        .w_NOSPLIT1 = IIF(.w_METODO <>'O','N',.w_NOSPLIT1)
        .w_ACCIVA = 0
          .DoRTCalc(16,18,.f.)
        .w_PRPARI2 = .w_PRPARI1
          .DoRTCalc(20,21,.f.)
        .w_VALIVA = .w_VALIVA1
        .DoRTCalc(22,22,.f.)
        if not(empty(.w_VALIVA))
          .link_1_23('Full')
        endif
        .w_CALCPICT = DEFPIC(.w_DECTOT)
          .DoRTCalc(24,24,.f.)
        .w_DESBAN = .w_DESBAN1
          .DoRTCalc(26,27,.f.)
        .w_CAOVAL = GETCAM(.w_VALIVA,.w_DATVER)
          .DoRTCalc(29,31,.f.)
        .w_DATSYS = i_datsys
      .oPgFrm.Page1.oPag.oObj_1_53.Calculate()
    endwith
    this.DoRTCalc(33,34,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_28.enabled = this.oPgFrm.Page1.oPag.oBtn_1_28.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_52.enabled = this.oPgFrm.Page1.oPag.oBtn_1_52.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,2,.t.)
          .link_1_3('Full')
        .DoRTCalc(4,5,.t.)
        if .o_CODAZI1<>.w_CODAZI1.or. .o_DATSTA<>.w_DATSTA
            .w_ANNRIF = STR(YEAR(.w_DATSTA), 4, 0)
          .link_1_6('Full')
        endif
        .DoRTCalc(7,12,.t.)
            .w_MACIVA = IIF(.w_VALIVA1=g_PERVAL, .w_MACIV2, .w_MACIV1)
        if .o_METODO<>.w_METODO
            .w_NOSPLIT1 = IIF(.w_METODO <>'O','N',.w_NOSPLIT1)
        endif
        if .o_METODO<>.w_METODO
            .w_ACCIVA = 0
        endif
        .DoRTCalc(16,18,.t.)
        if .o_CODATT1<>.w_CODATT1
            .w_PRPARI2 = .w_PRPARI1
        endif
        .DoRTCalc(20,21,.t.)
        if .o_ANNRIF<>.w_ANNRIF
            .w_VALIVA = .w_VALIVA1
          .link_1_23('Full')
        endif
        if .o_DECTOT<>.w_DECTOT
            .w_CALCPICT = DEFPIC(.w_DECTOT)
        endif
        .DoRTCalc(24,27,.t.)
            .w_CAOVAL = GETCAM(.w_VALIVA,.w_DATVER)
        .oPgFrm.Page1.oPag.oObj_1_53.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(29,34,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_53.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDESMET_1_10.enabled = this.oPgFrm.Page1.oPag.oDESMET_1_10.mCond()
    this.oPgFrm.Page1.oPag.oACCIVA_1_15.enabled = this.oPgFrm.Page1.oPag.oACCIVA_1_15.mCond()
    this.oPgFrm.Page1.oPag.oPRPARI2_1_19.enabled = this.oPgFrm.Page1.oPag.oPRPARI2_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_52.enabled = this.oPgFrm.Page1.oPag.oBtn_1_52.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oDESMET_1_10.visible=!this.oPgFrm.Page1.oPag.oDESMET_1_10.mHide()
    this.oPgFrm.Page1.oPag.oNOSPLIT1_1_14.visible=!this.oPgFrm.Page1.oPag.oNOSPLIT1_1_14.mHide()
    this.oPgFrm.Page1.oPag.oACCIVA_1_15.visible=!this.oPgFrm.Page1.oPag.oACCIVA_1_15.mHide()
    this.oPgFrm.Page1.oPag.oPEAIVA_1_48.visible=!this.oPgFrm.Page1.oPag.oPEAIVA_1_48.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_49.visible=!this.oPgFrm.Page1.oPag.oStr_1_49.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_50.visible=!this.oPgFrm.Page1.oPag.oStr_1_50.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_52.visible=!this.oPgFrm.Page1.oPag.oBtn_1_52.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_53.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI1
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZCATAZI,AZIVACON";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI1)
            select AZCODAZI,AZCATAZI,AZIVACON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI1 = NVL(_Link_.AZCODAZI,space(5))
      this.w_CODATT1 = NVL(_Link_.AZCATAZI,space(5))
      this.w_CONCES1 = NVL(_Link_.AZIVACON,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI1 = space(5)
      endif
      this.w_CODATT1 = space(5)
      this.w_CONCES1 = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATT1
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIDETT_IDX,3]
    i_lTable = "ATTIDETT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIDETT_IDX,2], .t., this.ATTIDETT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIDETT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATT1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATT1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODATT,ATFLREGI,ATPREFIS,ATPRPARI,ATSTAINT,ATNUMREG,ATTIPREG";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_CODATT1);
                   +" and ATCODATT="+cp_ToStrODBC(this.w_CODATT1);
                   +" and ATFLREGI="+cp_ToStrODBC(this.w_FLREGI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODATT',this.w_CODATT1;
                       ,'ATFLREGI',this.w_FLREGI;
                       ,'ATCODATT',this.w_CODATT1)
            select ATCODATT,ATFLREGI,ATPREFIS,ATPRPARI,ATSTAINT,ATNUMREG,ATTIPREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATT1 = NVL(_Link_.ATCODATT,space(5))
      this.w_PREFIS2 = NVL(_Link_.ATPREFIS,space(20))
      this.w_PRPARI1 = NVL(_Link_.ATPRPARI,0)
      this.w_INTLIG2 = NVL(_Link_.ATSTAINT,space(1))
      this.w_NUMREG = NVL(_Link_.ATNUMREG,0)
      this.w_TIPREG = NVL(_Link_.ATTIPREG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODATT1 = space(5)
      endif
      this.w_PREFIS2 = space(20)
      this.w_PRPARI1 = 0
      this.w_INTLIG2 = space(1)
      this.w_NUMREG = 0
      this.w_TIPREG = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIDETT_IDX,2])+'\'+cp_ToStr(_Link_.ATCODATT,1)+'\'+cp_ToStr(_Link_.ATFLREGI,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIDETT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATT1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANNRIF
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DAT_IVAN_IDX,3]
    i_lTable = "DAT_IVAN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DAT_IVAN_IDX,2], .t., this.DAT_IVAN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DAT_IVAN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANNRIF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANNRIF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IACODAZI,IA__ANNO,IAPEAIVA,IAVALIVA,IAMACIVA,IAMACIVE,IACODBAN,IADESBAN";
                   +" from "+i_cTable+" "+i_lTable+" where IA__ANNO="+cp_ToStrODBC(this.w_ANNRIF);
                   +" and IACODAZI="+cp_ToStrODBC(this.w_CODAZI1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IACODAZI',this.w_CODAZI1;
                       ,'IA__ANNO',this.w_ANNRIF)
            select IACODAZI,IA__ANNO,IAPEAIVA,IAVALIVA,IAMACIVA,IAMACIVE,IACODBAN,IADESBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANNRIF = NVL(_Link_.IA__ANNO,space(4))
      this.w_PEAIVA = NVL(_Link_.IAPEAIVA,0)
      this.w_VALIVA1 = NVL(_Link_.IAVALIVA,space(3))
      this.w_MACIV1 = NVL(_Link_.IAMACIVA,0)
      this.w_MACIV2 = NVL(_Link_.IAMACIVE,0)
      this.w_CODBAN1 = NVL(_Link_.IACODBAN,space(15))
      this.w_DESBAN1 = NVL(_Link_.IADESBAN,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_ANNRIF = space(4)
      endif
      this.w_PEAIVA = 0
      this.w_VALIVA1 = space(3)
      this.w_MACIV1 = 0
      this.w_MACIV2 = 0
      this.w_CODBAN1 = space(15)
      this.w_DESBAN1 = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DAT_IVAN_IDX,2])+'\'+cp_ToStr(_Link_.IACODAZI,1)+'\'+cp_ToStr(_Link_.IA__ANNO,1)
      cp_ShowWarn(i_cKey,this.DAT_IVAN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANNRIF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALIVA
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VACAOVAL,VADECTOT,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALIVA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALIVA)
            select VACODVAL,VACAOVAL,VADECTOT,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALIVA = NVL(_Link_.VACODVAL,space(3))
      this.w_CAOVAL = NVL(_Link_.VACAOVAL,0)
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_VALIVA = space(3)
      endif
      this.w_CAOVAL = 0
      this.w_DECTOT = 0
      this.w_SIMVAL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATSTA_1_4.value==this.w_DATSTA)
      this.oPgFrm.Page1.oPag.oDATSTA_1_4.value=this.w_DATSTA
    endif
    if not(this.oPgFrm.Page1.oPag.oDATVER_1_5.value==this.w_DATVER)
      this.oPgFrm.Page1.oPag.oDATVER_1_5.value=this.w_DATVER
    endif
    if not(this.oPgFrm.Page1.oPag.oCODBAN_1_8.value==this.w_CODBAN)
      this.oPgFrm.Page1.oPag.oCODBAN_1_8.value=this.w_CODBAN
    endif
    if not(this.oPgFrm.Page1.oPag.oMETODO_1_9.RadioValue()==this.w_METODO)
      this.oPgFrm.Page1.oPag.oMETODO_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMET_1_10.value==this.w_DESMET)
      this.oPgFrm.Page1.oPag.oDESMET_1_10.value=this.w_DESMET
    endif
    if not(this.oPgFrm.Page1.oPag.oMACIVA_1_13.value==this.w_MACIVA)
      this.oPgFrm.Page1.oPag.oMACIVA_1_13.value=this.w_MACIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oNOSPLIT1_1_14.RadioValue()==this.w_NOSPLIT1)
      this.oPgFrm.Page1.oPag.oNOSPLIT1_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oACCIVA_1_15.value==this.w_ACCIVA)
      this.oPgFrm.Page1.oPag.oACCIVA_1_15.value=this.w_ACCIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oPRPARI2_1_19.value==this.w_PRPARI2)
      this.oPgFrm.Page1.oPag.oPRPARI2_1_19.value=this.w_PRPARI2
    endif
    if not(this.oPgFrm.Page1.oPag.oDESBAN_1_29.value==this.w_DESBAN)
      this.oPgFrm.Page1.oPag.oDESBAN_1_29.value=this.w_DESBAN
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_36.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_36.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oPREFIS2_1_38.value==this.w_PREFIS2)
      this.oPgFrm.Page1.oPag.oPREFIS2_1_38.value=this.w_PREFIS2
    endif
    if not(this.oPgFrm.Page1.oPag.oPEAIVA_1_48.value==this.w_PEAIVA)
      this.oPgFrm.Page1.oPag.oPEAIVA_1_48.value=this.w_PEAIVA
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DATSTA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATSTA_1_4.SetFocus()
            i_bnoObbl = !empty(.w_DATSTA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DATVER))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATVER_1_5.SetFocus()
            i_bnoObbl = !empty(.w_DATVER)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODAZI1 = this.w_CODAZI1
    this.o_CODATT1 = this.w_CODATT1
    this.o_DATSTA = this.w_DATSTA
    this.o_ANNRIF = this.w_ANNRIF
    this.o_METODO = this.w_METODO
    this.o_DECTOT = this.w_DECTOT
    return

enddefine

* --- Define pages as container
define class tgscg_saiPag1 as StdContainer
  Width  = 535
  height = 326
  stdWidth  = 535
  stdheight = 326
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATSTA_1_4 as StdField with uid="HAHWDWSANR",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DATSTA", cQueryName = "DATSTA",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di riferimento per la stampa acconto IVA",;
    HelpContextID = 254697526,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=165, Top=29

  add object oDATVER_1_5 as StdField with uid="XMPFVGSEFQ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DATVER", cQueryName = "DATVER",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 255942710,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=444, Top=29

  add object oCODBAN_1_8 as StdField with uid="HYGZVDKWIF",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CODBAN", cQueryName = "CODBAN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 183266854,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=165, Top=59, InputMask=replicate('X',15), bHasZoom = .t. 

  proc oCODBAN_1_8.mZoom
      with this.Parent.oContained
        GSCG_BBB(this.Parent.oContained,"C")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc


  add object oMETODO_1_9 as StdCombo with uid="UXXHHJOYVD",rtseq=9,rtrep=.f.,left=143,top=117,width=135,height=21;
    , ToolTipText = "Selezione metodo di calcolo acconto IVA";
    , HelpContextID = 204104902;
    , cFormVar="w_METODO",RowSource=""+"Storico,"+"Operazioni effettuate,"+"Altro", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMETODO_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'O',;
    iif(this.value =3,'A',;
    space(1)))))
  endfunc
  func oMETODO_1_9.GetRadio()
    this.Parent.oContained.w_METODO = this.RadioValue()
    return .t.
  endfunc

  func oMETODO_1_9.SetRadio()
    this.Parent.oContained.w_METODO=trim(this.Parent.oContained.w_METODO)
    this.value = ;
      iif(this.Parent.oContained.w_METODO=='S',1,;
      iif(this.Parent.oContained.w_METODO=='O',2,;
      iif(this.Parent.oContained.w_METODO=='A',3,;
      0)))
  endfunc

  add object oDESMET_1_10 as StdField with uid="ANWEETPPMC",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DESMET", cQueryName = "DESMET",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione metodo utilizzato",;
    HelpContextID = 20468790,;
   bGlobalFont=.t.,;
    Height=21, Width=197, Left=279, Top=117, InputMask=replicate('X',25)

  func oDESMET_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_METODO='A')
    endwith
   endif
  endfunc

  func oDESMET_1_10.mHide()
    with this.Parent.oContained
      return (.w_METODO<>'A')
    endwith
  endfunc

  add object oMACIVA_1_13 as StdField with uid="LFHPCODUEQ",rtseq=13,rtrep=.f.,;
    cFormVar = "w_MACIVA", cQueryName = "MACIVA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Versamento minimo",;
    HelpContextID = 256069830,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=143, Top=143, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  add object oNOSPLIT1_1_14 as StdCheck with uid="RIOZTJXEVI",rtseq=14,rtrep=.f.,left=143, top=166, caption="Escludi codice iva che non computa in liquidazione",;
    ToolTipText = "Se attivo esclude codice iva che non computa in liquidazione",;
    HelpContextID = 156541177,;
    cFormVar="w_NOSPLIT1", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oNOSPLIT1_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oNOSPLIT1_1_14.GetRadio()
    this.Parent.oContained.w_NOSPLIT1 = this.RadioValue()
    return .t.
  endfunc

  func oNOSPLIT1_1_14.SetRadio()
    this.Parent.oContained.w_NOSPLIT1=trim(this.Parent.oContained.w_NOSPLIT1)
    this.value = ;
      iif(this.Parent.oContained.w_NOSPLIT1=='S',1,;
      0)
  endfunc

  func oNOSPLIT1_1_14.mHide()
    with this.Parent.oContained
      return (.w_METODO <> 'O')
    endwith
  endfunc

  add object oACCIVA_1_15 as StdField with uid="ESHFBINDCQ",rtseq=15,rtrep=.f.,;
    cFormVar = "w_ACCIVA", cQueryName = "ACCIVA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Importo acconto IVA inferiore al versamento minimo",;
    ToolTipText = "Acconto da versare",;
    HelpContextID = 256070150,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=143, Top=191, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  func oACCIVA_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_METODO='A')
    endwith
   endif
  endfunc

  func oACCIVA_1_15.mHide()
    with this.Parent.oContained
      return (.w_METODO='O')
    endwith
  endfunc

  add object oPRPARI2_1_19 as StdField with uid="GZVSBCMEQD",rtseq=19,rtrep=.f.,;
    cFormVar = "w_PRPARI2", cQueryName = "PRPARI2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ultima pagina stampata nel corrispondente R.I.",;
    HelpContextID = 117191158,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=116, Top=239, cSayPict='"9999999"', cGetPict='"9999999"'

  func oPRPARI2_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_INTLIG2='S')
    endwith
   endif
  endfunc


  add object oBtn_1_20 as StdButton with uid="GHEVIDFOUV",left=428, top=275, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la stampa";
    , HelpContextID = 124368154;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      with this.Parent.oContained
        GSCG_BAI(this.Parent.oContained,"S")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_28 as StdButton with uid="GNIBZHELYY",left=480, top=275, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 117079482;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_28.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESBAN_1_29 as StdField with uid="FUSVBBKSTN",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESBAN", cQueryName = "DESBAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 183325750,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=287, Top=59, InputMask=replicate('X',50)

  add object oSIMVAL_1_36 as StdField with uid="EAAGAZXWSU",rtseq=29,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Valuta",;
    HelpContextID = 151058726,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=296, Top=143, InputMask=replicate('X',5)

  add object oPREFIS2_1_38 as StdField with uid="SJBUMXHJGU",rtseq=30,rtrep=.f.,;
    cFormVar = "w_PREFIS2", cQueryName = "PREFIS2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Prefisso che comparirÓ nella stampa prima del numero di pagina",;
    HelpContextID = 7373302,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=318, Top=239, InputMask=replicate('X',20)

  add object oPEAIVA_1_48 as StdField with uid="SEORQSEEDE",rtseq=33,rtrep=.f.,;
    cFormVar = "w_PEAIVA", cQueryName = "PEAIVA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale di legge",;
    HelpContextID = 256062710,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=451, Top=191, cSayPict='"99.99"', cGetPict='"99.99"'

  func oPEAIVA_1_48.mHide()
    with this.Parent.oContained
      return (.w_METODO='O')
    endwith
  endfunc


  add object oBtn_1_52 as StdButton with uid="IQUKJFYRVV",left=463, top=143, width=48,height=45,;
    CpPicture="BMP\CALCOLA.BMP", caption="", nPag=1;
    , HelpContextID = 200278054;
    , caption='\<Calcola';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_52.Click()
      with this.Parent.oContained
        GSCG_BAI(this.Parent.oContained,"C")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_52.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_METODO='S')
      endwith
    endif
  endfunc

  func oBtn_1_52.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_METODO<>'S')
     endwith
    endif
  endfunc


  add object oObj_1_53 as cp_runprogram with uid="DNJIVDYJWH",left=605, top=423, width=135,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCG_BBB('V')",;
    cEvent = "w_CODBAN Changed",;
    nPag=1;
    , HelpContextID = 53600742

  add object oStr_1_25 as StdString with uid="FHUBBCNIYM",Visible=.t., Left=18, Top=143,;
    Alignment=1, Width=123, Height=15,;
    Caption="Versamento minimo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="RFOMRTVMXC",Visible=.t., Left=20, Top=60,;
    Alignment=1, Width=143, Height=15,;
    Caption="C/C versamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="ZTZODJRKYO",Visible=.t., Left=320, Top=30,;
    Alignment=1, Width=123, Height=15,;
    Caption="Data versamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="SNWNWDUSMM",Visible=.t., Left=60, Top=30,;
    Alignment=1, Width=103, Height=15,;
    Caption="Data di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="ZMAZDZTSBB",Visible=.t., Left=272, Top=143,;
    Alignment=1, Width=23, Height=18,;
    Caption="in:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="FWNKOATCCI",Visible=.t., Left=31, Top=240,;
    Alignment=1, Width=84, Height=15,;
    Caption="Ultima pagina:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="QBIQLOVEJE",Visible=.t., Left=208, Top=240,;
    Alignment=1, Width=110, Height=15,;
    Caption="Prefisso num. pag.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="UCZSILWULG",Visible=.t., Left=10, Top=8,;
    Alignment=0, Width=272, Height=18,;
    Caption="Dati del versamento"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_45 as StdString with uid="RZVQKQVRXW",Visible=.t., Left=10, Top=92,;
    Alignment=0, Width=290, Height=18,;
    Caption="Acconto da versare"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_47 as StdString with uid="FGXIBPIGCL",Visible=.t., Left=17, Top=119,;
    Alignment=1, Width=123, Height=18,;
    Caption="Metodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="GUPFZRBLIQ",Visible=.t., Left=285, Top=191,;
    Alignment=1, Width=167, Height=15,;
    Caption="Percentuale di legge:"  ;
  , bGlobalFont=.t.

  func oStr_1_49.mHide()
    with this.Parent.oContained
      return (.w_METODO='O')
    endwith
  endfunc

  add object oStr_1_50 as StdString with uid="JHSUHWZTID",Visible=.t., Left=19, Top=191,;
    Alignment=1, Width=123, Height=15,;
    Caption="Acconto IVA:"  ;
  , bGlobalFont=.t.

  func oStr_1_50.mHide()
    with this.Parent.oContained
      return (.w_METODO='O')
    endwith
  endfunc

  add object oStr_1_51 as StdString with uid="ODCFYZMUHB",Visible=.t., Left=10, Top=217,;
    Alignment=0, Width=290, Height=18,;
    Caption="Numerazione pagine"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_40 as StdBox with uid="PRATSRSFYA",left=10, top=232, width=516,height=36

  add object oBox_1_44 as StdBox with uid="AQQXHGXIBC",left=10, top=24, width=516,height=65

  add object oBox_1_46 as StdBox with uid="TRKZOQEWFO",left=10, top=108, width=516,height=109
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_sai','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
