* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bnm                                                        *
*              Controllo nomenclature                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_9]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-07-22                                                      *
* Last revis.: 2009-11-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPar
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bnm",oParentObject,m.pPar)
return(i_retval)

define class tgsar_bnm as StdBatch
  * --- Local variables
  pPar = space(3)
  w_MESS = space(10)
  w_CONTA = 0
  * --- WorkFile variables
  ART_ICOL_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- CONTROLLO CANCELLAZIONE NOMENCLATURE DA GSAR_ANM
    this.w_CONTA = 0
    do case
      case this.pPar="DEL"
        * --- Select from GSAR1SNM
        do vq_exec with 'GSAR1SNM',this,'_Curs_GSAR1SNM','',.f.,.t.
        if used('_Curs_GSAR1SNM')
          select _Curs_GSAR1SNM
          locate for 1=1
          do while not(eof())
          this.w_CONTA = NVL(_Curs_GSAR1SNM.CONTA, 0)
            select _Curs_GSAR1SNM
            continue
          enddo
          use
        endif
        if this.w_CONTA>0
          this.w_MESS = "La nomenclatura che si intende cancellare%0� presente in documenti, articoli o elenchi INTRA%0Impossibile cancellare"
          ah_ErrorMsg(this.w_MESS,,"")
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=ah_Msgformat("Eliminazione annullata")
          i_retcode = 'stop'
          return
        endif
      case this.pPar="UPD"
        * --- Aggiorna le unita di misura supplementari degli arcticoli che hanno associato la nomenclatura 
        * --- Write into ART_ICOL
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_ICOL_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ARUMSUPP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NMUNISUP),'ART_ICOL','ARUMSUPP');
              +i_ccchkf ;
          +" where ";
              +"ARNOMENC = "+cp_ToStrODBC(this.oParentObject.w_NMCODICE);
                 )
        else
          update (i_cTable) set;
              ARUMSUPP = this.oParentObject.w_NMUNISUP;
              &i_ccchkf. ;
           where;
              ARNOMENC = this.oParentObject.w_NMCODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
    endcase
  endproc


  proc Init(oParentObject,pPar)
    this.pPar=pPar
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ART_ICOL'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_GSAR1SNM')
      use in _Curs_GSAR1SNM
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPar"
endproc
