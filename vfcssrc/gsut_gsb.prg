* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_gsb                                                        *
*              Gadget SpeedBar                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-12-16                                                      *
* Last revis.: 2015-11-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_gsb",oParentObject))

* --- Class definition
define class tgsut_gsb as StdGadgetForm
  Top    = 2
  Left   = 1

  * --- Standard Properties
  Width  = 120
  Height = 83
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-11-17"
  HelpContextID=103367529
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=29

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsut_gsb"
  cComment = "Gadget SpeedBar"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_VALUE = space(10)
  w_TITLE = space(254)
  o_TITLE = space(254)
  w_QUERY = space(254)
  w_RET = .F.
  w_DATETIME = space(20)
  w_PROGRAM = space(250)
  w_MIN = 0
  o_MIN = 0
  w_MAX = 0
  o_MAX = 0
  w_BARBACKCOLOR = 0
  w_PROGRESSVALUETYPE = space(1)
  w_PROGRESSHEIGHT = 0
  w_PROGRESSVALUECOLOR = 0
  w_BARSCALETAG = 0
  w_BARSCALEINPUT = space(254)
  o_BARSCALEINPUT = space(254)
  w_BARSCALETAGCOLOR = 0
  w_ARROWCOLOR = 0
  w_PROGRESSVALUESIZE = 0
  w_PROGRESSINPUTTYPE = 0
  w_BARTRANSITION = .F.
  w_PROGRESSINPUT = space(10)
  o_PROGRESSINPUT = space(10)
  w_BARFORMAT = space(20)
  w_PROGRESSVALUESYMBOL = space(10)
  w_PRGDESCRI = space(50)
  w_BARSCALETAGSYMBOL = space(10)
  w_PROGRESSVALUEFONT = space(30)
  w_PROGRESSVALUEBOLD = .F.
  w_BARSCALETAGFONT = space(30)
  w_BARSCALETAGSIZE = 0
  w_PERCENTVALUE = space(30)
  w_oBar = .NULL.
  w_oFooter = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsut_gsb
  *--- ApplyTheme
  Proc ApplyTheme()
     DoDefault()
     This.NotifyEvent("ValuesByTheme")
     This.mCalc(.t.)
     This.SaveDependsOn()
  EndProc
  * --- Fine Area Manuale
  *--- Define Header
  add object oHeader as cp_headergadget with Width=this.Width, Left=0, Top=0, nNumPages=1 
  Height = This.Height + this.oHeader.Height

  * --- Define Page Frame
  add object oPgFrm as cp_oPgFrm with PageCount=1, Top=this.oHeader.Height, Width=this.Width, Height=this.Height-this.oHeader.Height
  proc oPgFrm.Init
    cp_oPgFrm::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_gsbPag1","gsut_gsb",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oBar = this.oPgFrm.Pages(1).oPag.oBar
    this.w_oFooter = this.oPgFrm.Pages(1).oPag.oFooter
    DoDefault()
    proc Destroy()
      this.w_oBar = .NULL.
      this.w_oFooter = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsut_gsb
    BindEvent(This.oPgFrm.Page1.Opag, "Click", This, "Click")
    BindEvent(This.w_oBar, "Click", This, "Click")
    
    Thisform.w_oBar.bVisibleOnResize=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_VALUE=space(10)
      .w_TITLE=space(254)
      .w_QUERY=space(254)
      .w_RET=.f.
      .w_DATETIME=space(20)
      .w_PROGRAM=space(250)
      .w_MIN=0
      .w_MAX=0
      .w_BARBACKCOLOR=0
      .w_PROGRESSVALUETYPE=space(1)
      .w_PROGRESSHEIGHT=0
      .w_PROGRESSVALUECOLOR=0
      .w_BARSCALETAG=0
      .w_BARSCALEINPUT=space(254)
      .w_BARSCALETAGCOLOR=0
      .w_ARROWCOLOR=0
      .w_PROGRESSVALUESIZE=0
      .w_PROGRESSINPUTTYPE=0
      .w_BARTRANSITION=.f.
      .w_PROGRESSINPUT=space(10)
      .w_BARFORMAT=space(20)
      .w_PROGRESSVALUESYMBOL=space(10)
      .w_PRGDESCRI=space(50)
      .w_BARSCALETAGSYMBOL=space(10)
      .w_PROGRESSVALUEFONT=space(30)
      .w_PROGRESSVALUEBOLD=.f.
      .w_BARSCALETAGFONT=space(30)
      .w_BARSCALETAGSIZE=0
      .w_PERCENTVALUE=space(30)
          .DoRTCalc(1,4,.f.)
        .w_DATETIME = Alltrim(TTOC(DATETIME()))+' '
      .oPgFrm.Page1.oPag.oBar.Calculate(.w_PROGRESSINPUT, .w_BARSCALEINPUT, .w_MIN, .w_MAX,  .T.)
          .DoRTCalc(6,6,.f.)
        .w_MIN = 0
        .w_MAX = 100
        .w_BARBACKCOLOR = iif(i_ThemesManager.GetProp(127)<> -1,i_ThemesManager.GetProp(127),RGBAlphaBlending(RGB(255,130,0), Thisform.backcolor, 0.6))
          .DoRTCalc(10,10,.f.)
        .w_PROGRESSHEIGHT = 50
        .w_PROGRESSVALUECOLOR = RGB(255,255,255)
        .w_BARSCALETAG = 1
          .DoRTCalc(14,14,.f.)
        .w_BARSCALETAGCOLOR = RGB(255,255,255)
        .w_ARROWCOLOR = RGB(255,255,255)
        .w_PROGRESSVALUESIZE = 9
      .oPgFrm.Page1.oPag.oFooter.Calculate(.w_DATETIME, .w_PROGRAM, .w_PRGDESCRI)
          .DoRTCalc(18,20,.f.)
        .w_BARFORMAT = "99999999"
        .w_PROGRESSVALUESYMBOL = ''
          .DoRTCalc(23,23,.f.)
        .w_BARSCALETAGSYMBOL = ''
        .w_PROGRESSVALUEFONT = "Arial"
          .DoRTCalc(26,27,.f.)
        .w_BARSCALETAGSIZE = 9
    endwith
    this.DoRTCalc(29,29,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        if .o_PROGRESSINPUT<>.w_PROGRESSINPUT.or. .o_MAX<>.w_MAX.or. .o_MIN<>.w_MIN.or. .o_BARSCALEINPUT<>.w_BARSCALEINPUT
          .Calculate_WWQXQCHPCP()
        endif
      	*--- Assegnamento caption ad oggetto header
          If .o_TITLE <> .w_TITLE
			   .oHeader.Calculate(.oPgFrm.Page1.oPag.oContained.w_TITLE)
          EndIf
        if .o_PROGRESSINPUT<>.w_PROGRESSINPUT.or. .o_MAX<>.w_MAX.or. .o_MIN<>.w_MIN.or. .o_BARSCALEINPUT<>.w_BARSCALEINPUT
        .oPgFrm.Page1.oPag.oBar.Calculate(.w_PROGRESSINPUT, .w_BARSCALEINPUT, .w_MIN, .w_MAX,  .T.)
        endif
        .oPgFrm.Page1.oPag.oFooter.Calculate(.w_DATETIME, .w_PROGRAM, .w_PRGDESCRI)
        if .o_PROGRESSINPUT<>.w_PROGRESSINPUT.or. .o_MAX<>.w_MAX.or. .o_MIN<>.w_MIN.or. .o_BARSCALEINPUT<>.w_BARSCALEINPUT
          .Calculate_MQWLJRAFDH()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,29,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oBar.Calculate(.w_PROGRESSINPUT, .w_BARSCALEINPUT, .w_MIN, .w_MAX,  .T.)
        .oPgFrm.Page1.oPag.oFooter.Calculate(.w_DATETIME, .w_PROGRAM, .w_PRGDESCRI)
    endwith
  return

  proc Calculate_WWQXQCHPCP()
    with this
          * --- Passaggio variabili
          .w_oBar.nSourceType = .w_PROGRESSINPUTTYPE
          .w_oBar.cSymbol = .w_PROGRESSVALUESYMBOL
          .w_oBar.cValBar = .w_PROGRESSVALUETYPE
          .w_oBar.nMainBarColor = .w_BARBACKCOLOR
          .w_oBar.nMainBarBorderColor = .w_BARBACKCOLOR
          .w_oBar.nArrowColor = .w_ARROWCOLOR
          .w_oBar.nBookMarksFontColor = .w_BARSCALETAGCOLOR
          .w_oBar.nPerCentFontColor = .w_PROGRESSVALUECOLOR
          .w_oBar.bTransition = .w_BARTRANSITION
          .w_oBar.nBookMark = .w_BARSCALETAG
          .w_oBar.bWaitTransition = .F.
          .w_oBar.SpeedBar.nProgressBarHeight = .w_PROGRESSHEIGHT
          .w_oBar.cStringFormat = .w_BARFORMAT
          .w_oBar.cTagSymbol = .w_BARSCALETAGSYMBOL
          .w_oBar.nPerCentFontSize = .w_PROGRESSVALUESIZE
          .w_oBar.cPerCentFontName = .w_PROGRESSVALUEFONT
          .w_oBar.bPerCentFontBold = .w_PROGRESSVALUEBOLD
          .w_oBar.cBookMarksFontName = .w_BARSCALETAGFONT
          .w_oBar.nBookMarksFontSize = .w_BARSCALETAGSIZE
    endwith
  endproc
  proc Calculate_MQWLJRAFDH()
    with this
          * --- Aggiornamento w_Value
          .w_VALUE = .w_oBar.nValue
          .w_PERCENTVALUE = .w_oBar.SpeedBar.PerCentLbl.Caption+.w_oBar.SpeedBar.Symbol.Caption
    endwith
  endproc
  proc Calculate_UVOCEOFISK()
    with this
          * --- Gadget Option
          .w_RET = This.GadgetOption()
          .w_RET = RefreshAll(This)
    endwith
  endproc
  proc Calculate_OGRWDURRLK()
    with this
          * --- Refresh Label
          .w_RET = RefreshAll(This)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("ValuesByTheme")
          .Calculate_WWQXQCHPCP()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oBar.Event(cEvent)
      .oPgFrm.Page1.oPag.oFooter.Event(cEvent)
        if lower(cEvent)==lower("AfterGadgetOption") or lower(cEvent)==lower("GadgetArranged") or lower(cEvent)==lower("GadgetOnDemand") or lower(cEvent)==lower("GadgetOnTimer") or lower(cEvent)==lower("oheader RefreshOnDemand") or lower(cEvent)==lower("oheader Collapsed Init")
          .Calculate_MQWLJRAFDH()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("GadgetOption")
          .Calculate_UVOCEOFISK()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("GadgetArranged") or lower(cEvent)==lower("GadgetOnDemand") or lower(cEvent)==lower("GadgetOnTimer") or lower(cEvent)==lower("oheader RefreshOnDemand")
          .Calculate_OGRWDURRLK()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsut_gsb
    If Lower(cEvent) = "barupdated"
       With This
         Local cCursor,l_oldArea,l_oldError,ggname
         m.ggname = 'frm'+Alltrim(.Name)
         m.l_oldError = On("Error")
         On Error &ggname..cAlertMsg=Message()
       
         .w_DATETIME = Alltrim(TTOC( DateTime() ))+' '
       
         On error &l_oldError
       
         .mCalc(.T.)
         .SaveDependsOn()
       EndWith
    EndIf
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TITLE = this.w_TITLE
    this.o_MIN = this.w_MIN
    this.o_MAX = this.w_MAX
    this.o_BARSCALEINPUT = this.w_BARSCALEINPUT
    this.o_PROGRESSINPUT = this.w_PROGRESSINPUT
    return

enddefine

* --- Define pages as container
define class tgsut_gsbPag1 as StdContainer
  Width  = 120
  height = 83
  stdWidth  = 120
  stdheight = 83
  resizeXpos=57
  resizeYpos=40
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBar as cp_SpeedMeterBar with uid="KBUWTNXFZL",left=4, top=2, width=109,height=45,;
    caption='oBar',;
   bGlobalFont=.t.,;
    visible=.f.,;
    cEvent = "GadgetArranged,GadgetOnTimer,GadgetOnDemand,oheader RefreshOnDemand,AfterGadgetOption",;
    nPag=1;
    , HelpContextID = 95480346


  add object oFooter as cp_FooterGadget with uid="UMXGAILDJX",left=2, top=47, width=111,height=35,;
    caption='Object',;
   bGlobalFont=.t.,;
    nPag=1;
    , HelpContextID = 74630118
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_gsb','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut_gsb
*--- Refresh
Proc RefreshAll(pParent)
  Local l_oldError,ggname
  With m.pParent
    m.ggname = 'frm'+Alltrim(.Name)
    m.l_oldError = On("Error")
    On Error &ggname..cAlertMsg=Message()

    .NotifyEvent('AfterGadgetOption')
    *--- Aggiorno la data di ultimo aggiornamento
    .tLastUpdate = DateTime()
    .w_DATETIME = Alltrim(TTOC(.tLastUpdate))+' '
    
    On error &l_oldError
    
    .mCalc(.T.)
    .SaveDependsOn()
  EndWith
EndProc
* --- Fine Area Manuale
