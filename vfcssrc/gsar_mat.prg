* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_mat                                                        *
*              Attivit�                                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_99]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-21                                                      *
* Last revis.: 2018-01-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_mat"))

* --- Class definition
define class tgsar_mat as StdTrsForm
  Top    = 2
  Left   = 12

  * --- Standard Properties
  Width  = 822
  Height = 254+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-01-03"
  HelpContextID=130643817
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=25

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  ATTIMAST_IDX = 0
  ATTIDETT_IDX = 0
  REG_PROV_IDX = 0
  cFile = "ATTIMAST"
  cFileDetail = "ATTIDETT"
  cKeySelect = "ATCODATT"
  cKeyWhere  = "ATCODATT=this.w_ATCODATT"
  cKeyDetail  = "ATCODATT=this.w_ATCODATT and ATNUMREG=this.w_ATNUMREG and ATTIPREG=this.w_ATTIPREG"
  cKeyWhereODBC = '"ATCODATT="+cp_ToStrODBC(this.w_ATCODATT)';

  cKeyDetailWhereODBC = '"ATCODATT="+cp_ToStrODBC(this.w_ATCODATT)';
      +'+" and ATNUMREG="+cp_ToStrODBC(this.w_ATNUMREG)';
      +'+" and ATTIPREG="+cp_ToStrODBC(this.w_ATTIPREG)';

  cKeyWhereODBCqualified = '"ATTIDETT.ATCODATT="+cp_ToStrODBC(this.w_ATCODATT)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gsar_mat"
  cComment = "Attivit�"
  i_nRowNum = 0
  i_nRowPerPage = 6
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_ATCODATT = space(5)
  w_ATDESATT = space(35)
  w_ATFLAGRI = space(1)
  w_ATPERPRO = space(1)
  w_ATREGMAR = space(1)
  o_ATREGMAR = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_ATNUMREG = 0
  w_ATTIPREG = space(1)
  o_ATTIPREG = space(1)
  w_ATCODREG = space(2)
  w_ATDATSTA = ctod('  /  /  ')
  w_ATDATCAL = ctod('  /  /  ')
  w_ATDATBLO = ctod('  /  /  ')
  w_ATPRPARI = 0
  w_ATSTAINT = space(1)
  o_ATSTAINT = space(1)
  w_ATPREFIS = space(20)
  w_ATFLREGI = space(1)
  w_ATCODSEZ = 0
  w_CTRL = .F.
  w_PIUATT = .F.
  w_ATATTIVA = 0
  w_REDESC = space(30)
  w_ATRIFFAZ = space(3)
  w_CHKMARG = .F.
  w_ATMARREG = space(1)
  w_ATLIMCOM = space(1)

  * --- Children pointers
  GSAR_MAI = .NULL.
  GSAR_MPM = .NULL.
  GSAR_MCA = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'ATTIMAST','gsar_mat')
    stdPageFrame::Init()
    *set procedure to GSAR_MAI additive
    *set procedure to GSAR_MPM additive
    *set procedure to GSAR_MCA additive
    with this
      .Pages(1).addobject("oPag","tgsar_matPag1","gsar_mat",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Attivit�")
      .Pages(1).HelpContextID = 151470566
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oATCODATT_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSAR_MAI
    *release procedure GSAR_MPM
    *release procedure GSAR_MCA
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='REG_PROV'
    this.cWorkTables[2]='ATTIMAST'
    this.cWorkTables[3]='ATTIDETT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ATTIMAST_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ATTIMAST_IDX,3]
  return

  function CreateChildren()
    this.GSAR_MAI = CREATEOBJECT('stdLazyChild',this,'GSAR_MAI')
    this.GSAR_MPM = CREATEOBJECT('stdLazyChild',this,'GSAR_MPM')
    this.GSAR_MCA = CREATEOBJECT('stdLazyChild',this,'GSAR_MCA')
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSAR_MAI)
      this.GSAR_MAI.DestroyChildrenChain()
      this.GSAR_MAI=.NULL.
    endif
    if !ISNULL(this.GSAR_MPM)
      this.GSAR_MPM.DestroyChildrenChain()
      this.GSAR_MPM=.NULL.
    endif
    if !ISNULL(this.GSAR_MCA)
      this.GSAR_MCA.DestroyChildrenChain()
      this.GSAR_MCA=.NULL.
    endif
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSAR_MAI.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSAR_MPM.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSAR_MCA.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSAR_MAI.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSAR_MPM.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSAR_MCA.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSAR_MAI.NewDocument()
    this.GSAR_MPM.NewDocument()
    this.GSAR_MCA.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .GSAR_MAI.ChangeRow(this.cRowID+'      1',1;
             ,.w_ATCODATT,"AICODATT";
             )
      .GSAR_MPM.ChangeRow(this.cRowID+'      1',1;
             ,.w_ATCODATT,"MACODATT";
             )
      .GSAR_MCA.ChangeRow(this.cRowID+'      1',1;
             ,.w_ATCODATT,"CACODATT";
             )
    endwith
    select (i_cOldSel)
    return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_ATCODATT = NVL(ATCODATT,space(5))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_3_joined
    link_2_3_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from ATTIMAST where ATCODATT=KeySet.ATCODATT
    *
    i_nConn = i_TableProp[this.ATTIMAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2],this.bLoadRecFilter,this.ATTIMAST_IDX,"gsar_mat")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ATTIMAST')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ATTIMAST.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"ATTIDETT.","ATTIMAST.")
      i_cTable = i_cTable+' ATTIMAST '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'ATCODATT',this.w_ATCODATT  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_OBTEST = i_INIDAT
        .w_CTRL = .F.
        .w_PIUATT = .F.
        .w_CHKMARG = .F.
        .w_ATCODATT = NVL(ATCODATT,space(5))
        .w_ATDESATT = NVL(ATDESATT,space(35))
        .w_ATFLAGRI = NVL(ATFLAGRI,space(1))
        .w_ATPERPRO = NVL(ATPERPRO,space(1))
        .w_ATREGMAR = NVL(ATREGMAR,space(1))
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .w_ATATTIVA = NVL(ATATTIVA,0)
        .w_ATRIFFAZ = NVL(ATRIFFAZ,space(3))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'ATTIMAST')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from ATTIDETT where ATCODATT=KeySet.ATCODATT
      *                            and ATNUMREG=KeySet.ATNUMREG
      *                            and ATTIPREG=KeySet.ATTIPREG
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.ATTIDETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIDETT_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('ATTIDETT')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "ATTIDETT.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" ATTIDETT"
        link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'ATCODATT',this.w_ATCODATT  )
        select * from (i_cTable) ATTIDETT where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_REDESC = space(30)
          .w_ATNUMREG = NVL(ATNUMREG,0)
          .w_ATTIPREG = NVL(ATTIPREG,space(1))
          .w_ATCODREG = NVL(ATCODREG,space(2))
          if link_2_3_joined
            this.w_ATCODREG = NVL(RPCODICE203,NVL(this.w_ATCODREG,space(2)))
            this.w_REDESC = NVL(RPDESCRI203,space(30))
          else
          .link_2_3('Load')
          endif
          .w_ATDATSTA = NVL(cp_ToDate(ATDATSTA),ctod("  /  /  "))
          .w_ATDATCAL = NVL(cp_ToDate(ATDATCAL),ctod("  /  /  "))
          .w_ATDATBLO = NVL(cp_ToDate(ATDATBLO),ctod("  /  /  "))
          .w_ATPRPARI = NVL(ATPRPARI,0)
          .w_ATSTAINT = NVL(ATSTAINT,space(1))
          .w_ATPREFIS = NVL(ATPREFIS,space(20))
          .w_ATFLREGI = NVL(ATFLREGI,space(1))
          .w_ATCODSEZ = NVL(ATCODSEZ,0)
          .w_ATMARREG = NVL(ATMARREG,space(1))
          .w_ATLIMCOM = NVL(ATLIMCOM,space(1))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace ATNUMREG with .w_ATNUMREG
          replace ATTIPREG with .w_ATTIPREG
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_ATCODATT=space(5)
      .w_ATDESATT=space(35)
      .w_ATFLAGRI=space(1)
      .w_ATPERPRO=space(1)
      .w_ATREGMAR=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_ATNUMREG=0
      .w_ATTIPREG=space(1)
      .w_ATCODREG=space(2)
      .w_ATDATSTA=ctod("  /  /  ")
      .w_ATDATCAL=ctod("  /  /  ")
      .w_ATDATBLO=ctod("  /  /  ")
      .w_ATPRPARI=0
      .w_ATSTAINT=space(1)
      .w_ATPREFIS=space(20)
      .w_ATFLREGI=space(1)
      .w_ATCODSEZ=0
      .w_CTRL=.f.
      .w_PIUATT=.f.
      .w_ATATTIVA=0
      .w_REDESC=space(30)
      .w_ATRIFFAZ=space(3)
      .w_CHKMARG=.f.
      .w_ATMARREG=space(1)
      .w_ATLIMCOM=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,5,.f.)
        .w_OBTEST = i_INIDAT
        .DoRTCalc(7,9,.f.)
        if not(empty(.w_ATCODREG))
         .link_2_3('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .DoRTCalc(10,14,.f.)
        .w_ATPREFIS = IIF(.w_ATSTAINT<>'S',SPACE(20),.w_ATPREFIS)
        .w_ATFLREGI = IIF(.w_ATTIPREG='A',' ',.w_ATFLREGI)
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .DoRTCalc(17,17,.f.)
        .w_CTRL = .F.
        .w_PIUATT = .F.
        .DoRTCalc(20,22,.f.)
        .w_CHKMARG = .F.
        .w_ATMARREG = 'N'
        .w_ATLIMCOM = 'N'
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'ATTIMAST')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oATCODATT_1_1.enabled = i_bVal
      .Page1.oPag.oATDESATT_1_2.enabled = i_bVal
      .Page1.oPag.oATFLAGRI_1_3.enabled = i_bVal
      .Page1.oPag.oATPERPRO_1_5.enabled = i_bVal
      .Page1.oPag.oATREGMAR_1_6.enabled = i_bVal
      .Page1.oPag.oATPREFIS_2_9.enabled = i_bVal
      .Page1.oPag.oATATTIVA_1_17.enabled = i_bVal
      .Page1.oPag.oATRIFFAZ_1_19.enabled = i_bVal
      .Page1.oPag.oObj_1_11.enabled = i_bVal
      .Page1.oPag.oObj_1_12.enabled = i_bVal
      .Page1.oPag.oObj_1_13.enabled = i_bVal
      .Page1.oPag.oObj_1_14.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oATCODATT_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oATCODATT_1_1.enabled = .t.
      endif
    endwith
    this.GSAR_MAI.SetStatus(i_cOp)
    this.GSAR_MPM.SetStatus(i_cOp)
    this.GSAR_MCA.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'ATTIMAST',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSAR_MAI.SetChildrenStatus(i_cOp)
  *  this.GSAR_MPM.SetChildrenStatus(i_cOp)
  *  this.GSAR_MCA.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ATTIMAST_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCODATT,"ATCODATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATDESATT,"ATDESATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATFLAGRI,"ATFLAGRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATPERPRO,"ATPERPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATREGMAR,"ATREGMAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATATTIVA,"ATATTIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATRIFFAZ,"ATRIFFAZ",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ATTIMAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2])
    i_lTable = "ATTIMAST"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.ATTIMAST_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_ATNUMREG N(2);
      ,t_ATTIPREG N(3);
      ,t_ATCODREG C(2);
      ,t_ATDATSTA D(8);
      ,t_ATDATCAL D(8);
      ,t_ATDATBLO D(8);
      ,t_ATPRPARI N(7);
      ,t_ATSTAINT N(3);
      ,t_ATPREFIS C(20);
      ,t_ATFLREGI N(3);
      ,t_ATCODSEZ N(2);
      ,t_REDESC C(30);
      ,t_ATMARREG N(3);
      ,t_ATLIMCOM N(3);
      ,ATNUMREG N(2);
      ,ATTIPREG C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsar_matbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oATNUMREG_2_1.controlsource=this.cTrsName+'.t_ATNUMREG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oATTIPREG_2_2.controlsource=this.cTrsName+'.t_ATTIPREG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oATCODREG_2_3.controlsource=this.cTrsName+'.t_ATCODREG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oATDATSTA_2_4.controlsource=this.cTrsName+'.t_ATDATSTA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oATDATCAL_2_5.controlsource=this.cTrsName+'.t_ATDATCAL'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oATDATBLO_2_6.controlsource=this.cTrsName+'.t_ATDATBLO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oATPRPARI_2_7.controlsource=this.cTrsName+'.t_ATPRPARI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oATSTAINT_2_8.controlsource=this.cTrsName+'.t_ATSTAINT'
    this.oPgFRm.Page1.oPag.oATPREFIS_2_9.controlsource=this.cTrsName+'.t_ATPREFIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oATFLREGI_2_11.controlsource=this.cTrsName+'.t_ATFLREGI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oATCODSEZ_2_12.controlsource=this.cTrsName+'.t_ATCODSEZ'
    this.oPgFRm.Page1.oPag.oREDESC_2_13.controlsource=this.cTrsName+'.t_REDESC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oATMARREG_2_15.controlsource=this.cTrsName+'.t_ATMARREG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oATLIMCOM_2_16.controlsource=this.cTrsName+'.t_ATLIMCOM'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(40)
    this.AddVLine(168)
    this.AddVLine(219)
    this.AddVLine(302)
    this.AddVLine(385)
    this.AddVLine(468)
    this.AddVLine(514)
    this.AddVLine(560)
    this.AddVLine(627)
    this.AddVLine(738)
    this.AddVLine(774)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATNUMREG_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ATTIMAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into ATTIMAST
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ATTIMAST')
        i_extval=cp_InsertValODBCExtFlds(this,'ATTIMAST')
        local i_cFld
        i_cFld=" "+;
                  "(ATCODATT,ATDESATT,ATFLAGRI,ATPERPRO,ATREGMAR"+;
                  ",ATATTIVA,ATRIFFAZ"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_ATCODATT)+;
                    ","+cp_ToStrODBC(this.w_ATDESATT)+;
                    ","+cp_ToStrODBC(this.w_ATFLAGRI)+;
                    ","+cp_ToStrODBC(this.w_ATPERPRO)+;
                    ","+cp_ToStrODBC(this.w_ATREGMAR)+;
                    ","+cp_ToStrODBC(this.w_ATATTIVA)+;
                    ","+cp_ToStrODBC(this.w_ATRIFFAZ)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ATTIMAST')
        i_extval=cp_InsertValVFPExtFlds(this,'ATTIMAST')
        cp_CheckDeletedKey(i_cTable,0,'ATCODATT',this.w_ATCODATT)
        INSERT INTO (i_cTable);
              (ATCODATT,ATDESATT,ATFLAGRI,ATPERPRO,ATREGMAR,ATATTIVA,ATRIFFAZ &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_ATCODATT;
                  ,this.w_ATDESATT;
                  ,this.w_ATFLAGRI;
                  ,this.w_ATPERPRO;
                  ,this.w_ATREGMAR;
                  ,this.w_ATATTIVA;
                  ,this.w_ATRIFFAZ;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ATTIDETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIDETT_IDX,2])
      *
      * insert into ATTIDETT
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(ATCODATT,ATNUMREG,ATTIPREG,ATCODREG,ATDATSTA"+;
                  ",ATDATCAL,ATDATBLO,ATPRPARI,ATSTAINT,ATPREFIS"+;
                  ",ATFLREGI,ATCODSEZ,ATMARREG,ATLIMCOM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_ATCODATT)+","+cp_ToStrODBC(this.w_ATNUMREG)+","+cp_ToStrODBC(this.w_ATTIPREG)+","+cp_ToStrODBCNull(this.w_ATCODREG)+","+cp_ToStrODBC(this.w_ATDATSTA)+;
             ","+cp_ToStrODBC(this.w_ATDATCAL)+","+cp_ToStrODBC(this.w_ATDATBLO)+","+cp_ToStrODBC(this.w_ATPRPARI)+","+cp_ToStrODBC(this.w_ATSTAINT)+","+cp_ToStrODBC(this.w_ATPREFIS)+;
             ","+cp_ToStrODBC(this.w_ATFLREGI)+","+cp_ToStrODBC(this.w_ATCODSEZ)+","+cp_ToStrODBC(this.w_ATMARREG)+","+cp_ToStrODBC(this.w_ATLIMCOM)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,0,'ATCODATT',this.w_ATCODATT,'ATNUMREG',this.w_ATNUMREG,'ATTIPREG',this.w_ATTIPREG)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_ATCODATT,this.w_ATNUMREG,this.w_ATTIPREG,this.w_ATCODREG,this.w_ATDATSTA"+;
                ",this.w_ATDATCAL,this.w_ATDATBLO,this.w_ATPRPARI,this.w_ATSTAINT,this.w_ATPREFIS"+;
                ",this.w_ATFLREGI,this.w_ATCODSEZ,this.w_ATMARREG,this.w_ATLIMCOM,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.ATTIMAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update ATTIMAST
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'ATTIMAST')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " ATDESATT="+cp_ToStrODBC(this.w_ATDESATT)+;
             ",ATFLAGRI="+cp_ToStrODBC(this.w_ATFLAGRI)+;
             ",ATPERPRO="+cp_ToStrODBC(this.w_ATPERPRO)+;
             ",ATREGMAR="+cp_ToStrODBC(this.w_ATREGMAR)+;
             ",ATATTIVA="+cp_ToStrODBC(this.w_ATATTIVA)+;
             ",ATRIFFAZ="+cp_ToStrODBC(this.w_ATRIFFAZ)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'ATTIMAST')
          i_cWhere = cp_PKFox(i_cTable  ,'ATCODATT',this.w_ATCODATT  )
          UPDATE (i_cTable) SET;
              ATDESATT=this.w_ATDESATT;
             ,ATFLAGRI=this.w_ATFLAGRI;
             ,ATPERPRO=this.w_ATPERPRO;
             ,ATREGMAR=this.w_ATREGMAR;
             ,ATATTIVA=this.w_ATATTIVA;
             ,ATRIFFAZ=this.w_ATRIFFAZ;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_ATNUMREG<>0) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.ATTIDETT_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.ATTIDETT_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from ATTIDETT
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and ATNUMREG="+cp_ToStrODBC(&i_TN.->ATNUMREG)+;
                            " and ATTIPREG="+cp_ToStrODBC(&i_TN.->ATTIPREG)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and ATNUMREG=&i_TN.->ATNUMREG;
                            and ATTIPREG=&i_TN.->ATTIPREG;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = 0
              replace ATNUMREG with this.w_ATNUMREG
              replace ATTIPREG with this.w_ATTIPREG
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update ATTIDETT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " ATCODREG="+cp_ToStrODBCNull(this.w_ATCODREG)+;
                     ",ATDATSTA="+cp_ToStrODBC(this.w_ATDATSTA)+;
                     ",ATDATCAL="+cp_ToStrODBC(this.w_ATDATCAL)+;
                     ",ATDATBLO="+cp_ToStrODBC(this.w_ATDATBLO)+;
                     ",ATPRPARI="+cp_ToStrODBC(this.w_ATPRPARI)+;
                     ",ATSTAINT="+cp_ToStrODBC(this.w_ATSTAINT)+;
                     ",ATPREFIS="+cp_ToStrODBC(this.w_ATPREFIS)+;
                     ",ATFLREGI="+cp_ToStrODBC(this.w_ATFLREGI)+;
                     ",ATCODSEZ="+cp_ToStrODBC(this.w_ATCODSEZ)+;
                     ",ATMARREG="+cp_ToStrODBC(this.w_ATMARREG)+;
                     ",ATLIMCOM="+cp_ToStrODBC(this.w_ATLIMCOM)+;
                     " ,ATNUMREG="+cp_ToStrODBC(this.w_ATNUMREG)+;
                     " ,ATTIPREG="+cp_ToStrODBC(this.w_ATTIPREG)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+;
                             " and ATNUMREG="+cp_ToStrODBC(ATNUMREG)+;
                             " and ATTIPREG="+cp_ToStrODBC(ATTIPREG)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      ATCODREG=this.w_ATCODREG;
                     ,ATDATSTA=this.w_ATDATSTA;
                     ,ATDATCAL=this.w_ATDATCAL;
                     ,ATDATBLO=this.w_ATDATBLO;
                     ,ATPRPARI=this.w_ATPRPARI;
                     ,ATSTAINT=this.w_ATSTAINT;
                     ,ATPREFIS=this.w_ATPREFIS;
                     ,ATFLREGI=this.w_ATFLREGI;
                     ,ATCODSEZ=this.w_ATCODSEZ;
                     ,ATMARREG=this.w_ATMARREG;
                     ,ATLIMCOM=this.w_ATLIMCOM;
                     ,ATNUMREG=this.w_ATNUMREG;
                     ,ATTIPREG=this.w_ATTIPREG;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere;
                                      and ATNUMREG=&i_TN.->ATNUMREG;
                                      and ATTIPREG=&i_TN.->ATTIPREG;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask header and footer children to save themselves
      * --- GSAR_MAI : Saving
      this.GSAR_MAI.ChangeRow(this.cRowID+'      1',0;
             ,this.w_ATCODATT,"AICODATT";
             )
      this.GSAR_MAI.mReplace()
      * --- GSAR_MPM : Saving
      this.GSAR_MPM.ChangeRow(this.cRowID+'      1',0;
             ,this.w_ATCODATT,"MACODATT";
             )
      this.GSAR_MPM.mReplace()
      * --- GSAR_MCA : Saving
      this.GSAR_MCA.ChangeRow(this.cRowID+'      1',0;
             ,this.w_ATCODATT,"CACODATT";
             )
      this.GSAR_MCA.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSAR_MAI : Deleting
    this.GSAR_MAI.ChangeRow(this.cRowID+'      1',0;
           ,this.w_ATCODATT,"AICODATT";
           )
    this.GSAR_MAI.mDelete()
    * --- GSAR_MPM : Deleting
    this.GSAR_MPM.ChangeRow(this.cRowID+'      1',0;
           ,this.w_ATCODATT,"MACODATT";
           )
    this.GSAR_MPM.mDelete()
    * --- GSAR_MCA : Deleting
    this.GSAR_MCA.ChangeRow(this.cRowID+'      1',0;
           ,this.w_ATCODATT,"CACODATT";
           )
    this.GSAR_MCA.mDelete()
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_ATNUMREG<>0) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.ATTIDETT_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.ATTIDETT_IDX,2])
        *
        * delete ATTIDETT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and ATNUMREG="+cp_ToStrODBC(&i_TN.->ATNUMREG)+;
                            " and ATTIPREG="+cp_ToStrODBC(&i_TN.->ATTIPREG)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and ATNUMREG=&i_TN.->ATNUMREG;
                              and ATTIPREG=&i_TN.->ATTIPREG;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.ATTIMAST_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2])
        *
        * delete ATTIMAST
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_ATNUMREG<>0) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ATTIMAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .DoRTCalc(1,14,.t.)
        if .o_ATSTAINT<>.w_ATSTAINT
          .w_ATPREFIS = IIF(.w_ATSTAINT<>'S',SPACE(20),.w_ATPREFIS)
        endif
        if .o_ATTIPREG<>.w_ATTIPREG
          .w_ATFLREGI = IIF(.w_ATTIPREG='A',' ',.w_ATFLREGI)
        endif
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        if .o_ATREGMAR<>.w_ATREGMAR
          .Calculate_KDPCESYLSK()
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(17,25,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_KDPCESYLSK()
    with this
          * --- Sbianco i check sul regime del margine nel transitorio
          SBIANCATRS(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oLinkPC_1_7.enabled = this.oPgFrm.Page1.oPag.oLinkPC_1_7.mCond()
    this.oPgFrm.Page1.oPag.oLinkPC_1_8.enabled = this.oPgFrm.Page1.oPag.oLinkPC_1_8.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oATPRPARI_2_7.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oATPRPARI_2_7.mCond()
    this.oPgFrm.Page1.oPag.oATPREFIS_2_9.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oATPREFIS_2_9.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oATFLREGI_2_11.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oATFLREGI_2_11.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oATCODSEZ_2_12.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oATCODSEZ_2_12.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oATMARREG_2_15.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oATMARREG_2_15.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oATLIMCOM_2_16.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oATLIMCOM_2_16.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_11.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_12.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_13.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_14.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ATCODREG
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.REG_PROV_IDX,3]
    i_lTable = "REG_PROV"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2], .t., this.REG_PROV_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCODREG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ARP',True,'REG_PROV')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RPCODICE like "+cp_ToStrODBC(trim(this.w_ATCODREG)+"%");

          i_ret=cp_SQL(i_nConn,"select RPCODICE,RPDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RPCODICE',trim(this.w_ATCODREG))
          select RPCODICE,RPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCODREG)==trim(_Link_.RPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATCODREG) and !this.bDontReportError
            deferred_cp_zoom('REG_PROV','*','RPCODICE',cp_AbsName(oSource.parent,'oATCODREG_2_3'),i_cWhere,'GSCG_ARP',"Regioni e provincie autonome",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RPCODICE,RPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where RPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RPCODICE',oSource.xKey(1))
            select RPCODICE,RPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCODREG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RPCODICE,RPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where RPCODICE="+cp_ToStrODBC(this.w_ATCODREG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RPCODICE',this.w_ATCODREG)
            select RPCODICE,RPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCODREG = NVL(_Link_.RPCODICE,space(2))
      this.w_REDESC = NVL(_Link_.RPDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_ATCODREG = space(2)
      endif
      this.w_REDESC = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2])+'\'+cp_ToStr(_Link_.RPCODICE,1)
      cp_ShowWarn(i_cKey,this.REG_PROV_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCODREG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.REG_PROV_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.RPCODICE as RPCODICE203"+ ",link_2_3.RPDESCRI as RPDESCRI203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on ATTIDETT.ATCODREG=link_2_3.RPCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and ATTIDETT.ATCODREG=link_2_3.RPCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oATCODATT_1_1.value==this.w_ATCODATT)
      this.oPgFrm.Page1.oPag.oATCODATT_1_1.value=this.w_ATCODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oATDESATT_1_2.value==this.w_ATDESATT)
      this.oPgFrm.Page1.oPag.oATDESATT_1_2.value=this.w_ATDESATT
    endif
    if not(this.oPgFrm.Page1.oPag.oATFLAGRI_1_3.RadioValue()==this.w_ATFLAGRI)
      this.oPgFrm.Page1.oPag.oATFLAGRI_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATPERPRO_1_5.RadioValue()==this.w_ATPERPRO)
      this.oPgFrm.Page1.oPag.oATPERPRO_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATREGMAR_1_6.RadioValue()==this.w_ATREGMAR)
      this.oPgFrm.Page1.oPag.oATREGMAR_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATPREFIS_2_9.value==this.w_ATPREFIS)
      this.oPgFrm.Page1.oPag.oATPREFIS_2_9.value=this.w_ATPREFIS
      replace t_ATPREFIS with this.oPgFrm.Page1.oPag.oATPREFIS_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oATATTIVA_1_17.value==this.w_ATATTIVA)
      this.oPgFrm.Page1.oPag.oATATTIVA_1_17.value=this.w_ATATTIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oREDESC_2_13.value==this.w_REDESC)
      this.oPgFrm.Page1.oPag.oREDESC_2_13.value=this.w_REDESC
      replace t_REDESC with this.oPgFrm.Page1.oPag.oREDESC_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oATRIFFAZ_1_19.value==this.w_ATRIFFAZ)
      this.oPgFrm.Page1.oPag.oATRIFFAZ_1_19.value=this.w_ATRIFFAZ
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATNUMREG_2_1.value==this.w_ATNUMREG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATNUMREG_2_1.value=this.w_ATNUMREG
      replace t_ATNUMREG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATNUMREG_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATTIPREG_2_2.RadioValue()==this.w_ATTIPREG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATTIPREG_2_2.SetRadio()
      replace t_ATTIPREG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATTIPREG_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATCODREG_2_3.value==this.w_ATCODREG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATCODREG_2_3.value=this.w_ATCODREG
      replace t_ATCODREG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATCODREG_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATDATSTA_2_4.value==this.w_ATDATSTA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATDATSTA_2_4.value=this.w_ATDATSTA
      replace t_ATDATSTA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATDATSTA_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATDATCAL_2_5.value==this.w_ATDATCAL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATDATCAL_2_5.value=this.w_ATDATCAL
      replace t_ATDATCAL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATDATCAL_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATDATBLO_2_6.value==this.w_ATDATBLO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATDATBLO_2_6.value=this.w_ATDATBLO
      replace t_ATDATBLO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATDATBLO_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATPRPARI_2_7.value==this.w_ATPRPARI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATPRPARI_2_7.value=this.w_ATPRPARI
      replace t_ATPRPARI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATPRPARI_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATSTAINT_2_8.RadioValue()==this.w_ATSTAINT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATSTAINT_2_8.SetRadio()
      replace t_ATSTAINT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATSTAINT_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATFLREGI_2_11.RadioValue()==this.w_ATFLREGI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATFLREGI_2_11.SetRadio()
      replace t_ATFLREGI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATFLREGI_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATCODSEZ_2_12.value==this.w_ATCODSEZ)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATCODSEZ_2_12.value=this.w_ATCODSEZ
      replace t_ATCODSEZ with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATCODSEZ_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATMARREG_2_15.RadioValue()==this.w_ATMARREG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATMARREG_2_15.SetRadio()
      replace t_ATMARREG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATMARREG_2_15.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATLIMCOM_2_16.RadioValue()==this.w_ATLIMCOM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATLIMCOM_2_16.SetRadio()
      replace t_ATLIMCOM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATLIMCOM_2_16.value
    endif
    cp_SetControlsValueExtFlds(this,'ATTIMAST')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_ATCODATT))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oATCODATT_1_1.SetFocus()
            i_bnoObbl = !empty(.w_ATCODATT)
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      *i_bRes = i_bRes .and. .GSAR_MAI.CheckForm()
      if i_bres
        i_bres=  .GSAR_MAI.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      *i_bRes = i_bRes .and. .GSAR_MPM.CheckForm()
      if i_bres
        i_bres=  .GSAR_MPM.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      *i_bRes = i_bRes .and. .GSAR_MCA.CheckForm()
      if i_bres
        i_bres=  .GSAR_MCA.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsar_mat
      this.NotifyEvent('Controllo')
      if THIS.w_CTRL=1
         i_bRes=.f.
         i_bnoChk = .f.	
         i_cErrorMsg = "Definire un solo registro principale (Princ.) per ogni attivit� ! "
      endif
      if THIS.w_CTRL=2
         i_bRes=.f.
         i_bnoChk = .f.	
         i_cErrorMsg = "E' possibile attivare il flag fuori anno su un solo registro acquisti! "
      endif
      if THIS.w_PIUATT
         i_bRes=.f.
         i_bnoChk = .f.	
         i_cErrorMsg = Ah_MsgFormat("Esistono piu' attivita' associate allo stesso registro")
      endif
      if THIS.w_CHKMARG
         i_bRes=.f.   
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   (empty(.w_ATNUMREG) or not(.w_ATNUMREG<>0)) and (.w_ATNUMREG<>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATNUMREG_2_1
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
        case   empty(.w_ATTIPREG) and (.w_ATNUMREG<>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATTIPREG_2_2
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
      endcase
      if .w_ATNUMREG<>0
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ATREGMAR = this.w_ATREGMAR
    this.o_ATTIPREG = this.w_ATTIPREG
    this.o_ATSTAINT = this.w_ATSTAINT
    * --- GSAR_MAI : Depends On
    this.GSAR_MAI.SaveDependsOn()
    * --- GSAR_MPM : Depends On
    this.GSAR_MPM.SaveDependsOn()
    * --- GSAR_MCA : Depends On
    this.GSAR_MCA.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_ATNUMREG<>0)
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_ATNUMREG=0
      .w_ATTIPREG=space(1)
      .w_ATCODREG=space(2)
      .w_ATDATSTA=ctod("  /  /  ")
      .w_ATDATCAL=ctod("  /  /  ")
      .w_ATDATBLO=ctod("  /  /  ")
      .w_ATPRPARI=0
      .w_ATSTAINT=space(1)
      .w_ATPREFIS=space(20)
      .w_ATFLREGI=space(1)
      .w_ATCODSEZ=0
      .w_REDESC=space(30)
      .w_ATMARREG=space(1)
      .w_ATLIMCOM=space(1)
      .DoRTCalc(1,9,.f.)
      if not(empty(.w_ATCODREG))
        .link_2_3('Full')
      endif
      .DoRTCalc(10,14,.f.)
        .w_ATPREFIS = IIF(.w_ATSTAINT<>'S',SPACE(20),.w_ATPREFIS)
        .w_ATFLREGI = IIF(.w_ATTIPREG='A',' ',.w_ATFLREGI)
      .DoRTCalc(17,23,.f.)
        .w_ATMARREG = 'N'
        .w_ATLIMCOM = 'N'
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_ATNUMREG = t_ATNUMREG
    this.w_ATTIPREG = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATTIPREG_2_2.RadioValue(.t.)
    this.w_ATCODREG = t_ATCODREG
    this.w_ATDATSTA = t_ATDATSTA
    this.w_ATDATCAL = t_ATDATCAL
    this.w_ATDATBLO = t_ATDATBLO
    this.w_ATPRPARI = t_ATPRPARI
    this.w_ATSTAINT = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATSTAINT_2_8.RadioValue(.t.)
    this.w_ATPREFIS = t_ATPREFIS
    this.w_ATFLREGI = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATFLREGI_2_11.RadioValue(.t.)
    this.w_ATCODSEZ = t_ATCODSEZ
    this.w_REDESC = t_REDESC
    this.w_ATMARREG = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATMARREG_2_15.RadioValue(.t.)
    this.w_ATLIMCOM = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATLIMCOM_2_16.RadioValue(.t.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_ATNUMREG with this.w_ATNUMREG
    replace t_ATTIPREG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATTIPREG_2_2.ToRadio()
    replace t_ATCODREG with this.w_ATCODREG
    replace t_ATDATSTA with this.w_ATDATSTA
    replace t_ATDATCAL with this.w_ATDATCAL
    replace t_ATDATBLO with this.w_ATDATBLO
    replace t_ATPRPARI with this.w_ATPRPARI
    replace t_ATSTAINT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATSTAINT_2_8.ToRadio()
    replace t_ATPREFIS with this.w_ATPREFIS
    replace t_ATFLREGI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATFLREGI_2_11.ToRadio()
    replace t_ATCODSEZ with this.w_ATCODSEZ
    replace t_REDESC with this.w_REDESC
    replace t_ATMARREG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATMARREG_2_15.ToRadio()
    replace t_ATLIMCOM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATLIMCOM_2_16.ToRadio()
    if i_srv='A'
      replace ATNUMREG with this.w_ATNUMREG
      replace ATTIPREG with this.w_ATTIPREG
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsar_matPag1 as StdContainer
  Width  = 818
  height = 254
  stdWidth  = 818
  stdheight = 254
  resizeYpos=144
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oATCODATT_1_1 as StdField with uid="OBPUIMCAED",rtseq=1,rtrep=.f.,;
    cFormVar = "w_ATCODATT", cQueryName = "ATCODATT",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit� azienda",;
    HelpContextID = 231346522,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=54, Top=8, InputMask=replicate('X',5)

  add object oATDESATT_1_2 as StdField with uid="ZMZVYHATFC",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ATDESATT", cQueryName = "ATDESATT",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione attivit�",;
    HelpContextID = 246423898,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=120, Top=8, InputMask=replicate('X',35)

  add object oATFLAGRI_1_3 as StdCheck with uid="DQISQEQFHQ",rtseq=3,rtrep=.f.,left=54, top=38, caption="Produttore agricolo",;
    ToolTipText = "Se attivo: attivit� legata a regime di produttore agricolo con IVA agevolata e in % di compensazione",;
    HelpContextID = 60244303,;
    cFormVar="w_ATFLAGRI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oATFLAGRI_1_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..ATFLAGRI,&i_cF..t_ATFLAGRI),this.value)
    return(iif(xVal =1,'P',;
    ' '))
  endfunc
  func oATFLAGRI_1_3.GetRadio()
    this.Parent.oContained.w_ATFLAGRI = this.RadioValue()
    return .t.
  endfunc

  func oATFLAGRI_1_3.ToRadio()
    this.Parent.oContained.w_ATFLAGRI=trim(this.Parent.oContained.w_ATFLAGRI)
    return(;
      iif(this.Parent.oContained.w_ATFLAGRI=='P',1,;
      0))
  endfunc

  func oATFLAGRI_1_3.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oATPERPRO_1_5 as StdCheck with uid="HSQKKIWJGY",rtseq=4,rtrep=.f.,left=229, top=38, caption="Prorata",;
    ToolTipText = "Se attivo: l'attivit� gestisce il prorata",;
    HelpContextID = 228647253,;
    cFormVar="w_ATPERPRO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oATPERPRO_1_5.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..ATPERPRO,&i_cF..t_ATPERPRO),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oATPERPRO_1_5.GetRadio()
    this.Parent.oContained.w_ATPERPRO = this.RadioValue()
    return .t.
  endfunc

  func oATPERPRO_1_5.ToRadio()
    this.Parent.oContained.w_ATPERPRO=trim(this.Parent.oContained.w_ATPERPRO)
    return(;
      iif(this.Parent.oContained.w_ATPERPRO=='S',1,;
      0))
  endfunc

  func oATPERPRO_1_5.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oATREGMAR_1_6 as StdCheck with uid="LASXEDSUHB",rtseq=5,rtrep=.f.,left=362, top=38, caption="Regime del margine",;
    ToolTipText = "Flag regime del margine",;
    HelpContextID = 166789464,;
    cFormVar="w_ATREGMAR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oATREGMAR_1_6.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..ATREGMAR,&i_cF..t_ATREGMAR),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oATREGMAR_1_6.GetRadio()
    this.Parent.oContained.w_ATREGMAR = this.RadioValue()
    return .t.
  endfunc

  func oATREGMAR_1_6.ToRadio()
    this.Parent.oContained.w_ATREGMAR=trim(this.Parent.oContained.w_ATREGMAR)
    return(;
      iif(this.Parent.oContained.w_ATREGMAR=='S',1,;
      0))
  endfunc

  func oATREGMAR_1_6.SetRadio()
    this.value=this.ToRadio()
  endfunc


  add object oLinkPC_1_7 as StdButton with uid="KJHXXSLCXQ",left=659, top=10, width=48,height=45,;
    CpPicture="bmp\ClasTass.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere ai dati del prorata";
    , HelpContextID = 46133238;
    , Caption='\<Prorata', tabstop=.f.;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_1_7.Click()
      this.Parent.oContained.GSAR_MAI.LinkPCClick()
    endproc

  func oLinkPC_1_7.mCond()
    with this.Parent.oContained
      return (.w_ATPERPRO='S' AND NOT EMPTY(.w_ATCODATT))
    endwith
  endfunc


  add object oLinkPC_1_8 as StdButton with uid="DWAAUIFIRV",left=712, top=10, width=48,height=45,;
    CpPicture="bmp\parametri.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere ai parametri Regime del magine";
    , HelpContextID = 74388984;
    , Caption='\<Parametri', tabstop=.f.;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_1_8.Click()
      this.Parent.oContained.GSAR_MPM.LinkPCClick()
    endproc

  func oLinkPC_1_8.mCond()
    with this.Parent.oContained
      return (.w_ATREGMAR = 'S')
    endwith
  endfunc


  add object oLinkPC_1_9 as StdButton with uid="VKMTBCOXVB",left=765, top=10, width=48,height=45,;
    CpPicture="bmp\Modifica.bmp", caption="", nPag=1;
    , ToolTipText = "Codici attivit� IVA ministeriali (Ateco), da stampare nella liquidazione periodica e nella liquidazione annuale in sostituzione del codice ad uso interno definito sulla maschera principale dell'attivit�.";
    , HelpContextID = 131480614;
    , Caption='\<Codici', tabstop=.f.;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_1_9.Click()
      this.Parent.oContained.GSAR_MCA.LinkPCClick()
    endproc


  add object oObj_1_11 as cp_runprogram with uid="CWAJCLZOSH",left=845, top=3, width=158,height=24,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSAR_BAT('R')",;
    cEvent = "Delete row start",;
    nPag=1;
    , HelpContextID = 47353830


  add object oObj_1_12 as cp_runprogram with uid="FFEZMNOEBS",left=845, top=28, width=158,height=24,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSAR_BAT('D')",;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 47353830


  add object oObj_1_13 as cp_runprogram with uid="PAQIDGYKPM",left=845, top=-22, width=158,height=24,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSAR_BAT('I')",;
    cEvent = "Insert start",;
    nPag=1;
    , HelpContextID = 47353830


  add object oObj_1_14 as cp_runprogram with uid="OTPETFWNEE",left=845, top=54, width=158,height=24,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSAR_BAT('F')",;
    cEvent = "Controllo",;
    nPag=1;
    , HelpContextID = 47353830

  add object oATATTIVA_1_17 as StdField with uid="GQALIWEOKD",rtseq=20,rtrep=.f.,;
    cFormVar = "w_ATATTIVA", cQueryName = "ATATTIVA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Nel caso in cui sia attivo il modulo Trasferimento Studio e selezionato l�Export Avanzato con tipo protocollo Contb in tale campo viene riportato il codice attivit� dello studio importato dall�apposito tracciato",;
    HelpContextID = 114225479,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=143, Top=62

  add object oATRIFFAZ_1_19 as StdField with uid="HKBUXOYZSC",rtseq=22,rtrep=.f.,;
    cFormVar = "w_ATRIFFAZ", cQueryName = "ATRIFFAZ",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 48562528,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=362, Top=62, InputMask=replicate('X',3)


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=8, top=90, width=806,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=12,Field1="ATNUMREG",Label1="Num.",Field2="ATTIPREG",Label2="Tipo registro IVA",Field3="ATCODREG",Label3="Regione",Field4="ATDATSTA",Label4="St.reg.IVA",Field5="ATDATCAL",Label5="Calcolo saldi",Field6="ATDATBLO",Label6="Blocco P.N.",Field7="ATPRPARI",Label7="N.pag.R.I.",Field8="ATSTAINT",Label8="Intest.",Field9="ATFLREGI",Label9="Princ.",Field10="ATCODSEZ",Label10="Num. sez. studio",Field11="ATMARREG",Label11="R. M.",Field12="ATLIMCOM",Label12="F. A.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 185447034

  add object oStr_1_4 as StdString with uid="UOLOAQTIAX",Visible=.t., Left=2, Top=8,;
    Alignment=1, Width=50, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_18 as StdString with uid="EKZPYBTDRJ",Visible=.t., Left=40, Top=65,;
    Alignment=1, Width=98, Height=17,;
    Caption="Cod. attivit� IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="YXPCYCCVXK",Visible=.t., Left=185, Top=65,;
    Alignment=1, Width=173, Height=18,;
    Caption="Cod. attivit� fisco azienda:"  ;
  , bGlobalFont=.t.

  add object oBox_1_23 as StdBox with uid="IGXNMNAFOY",left=766, top=91, width=1,height=134

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-2,top=109,;
    width=801+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*6*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-1,top=110,width=800+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*6*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='REG_PROV|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oATPREFIS_2_9.Refresh()
      this.Parent.oREDESC_2_13.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='REG_PROV'
        oDropInto=this.oBodyCol.oRow.oATCODREG_2_3
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oATPREFIS_2_9 as StdTrsField with uid="WLSMZHSCRS",rtseq=15,rtrep=.t.,;
    cFormVar="w_ATPREFIS",value=space(20),;
    ToolTipText = "Editabile se � attivato il check Intestazione. Permette di impostare il prefisso alla numerazione da stampare nel registro Iva. (es. Anno 2014/1, Anno 2014/2 ecc).",;
    HelpContextID = 220339879,;
    cTotal="", bFixedPos=.t., cQueryName = "ATPREFIS",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=493, Top=231, InputMask=replicate('X',20)

  func oATPREFIS_2_9.mCond()
    with this.Parent.oContained
      return (.w_ATSTAINT='S')
    endwith
  endfunc

  add object oREDESC_2_13 as StdTrsField with uid="ILRDHUKOAK",rtseq=21,rtrep=.t.,;
    cFormVar="w_REDESC",value=space(30),enabled=.f.,;
    ToolTipText = "Descrizione regione\provincia autonoma",;
    HelpContextID = 256896234,;
    cTotal="", bFixedPos=.t., cQueryName = "REDESC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=118, Top=231, InputMask=replicate('X',30)

  add object oStr_2_10 as StdString with uid="KDPNCDLYQL",Visible=.t., Left=343, Top=233,;
    Alignment=1, Width=148, Height=18,;
    Caption="Prefisso per num. pagine:"  ;
  , bGlobalFont=.t.

  add object oStr_2_14 as StdString with uid="FHVBBZJAZJ",Visible=.t., Left=2, Top=233,;
    Alignment=1, Width=113, Height=18,;
    Caption="Descrizione regione:"  ;
  , bGlobalFont=.t.

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsar_matBodyRow as CPBodyRowCnt
  Width=791
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oATNUMREG_2_1 as StdTrsField with uid="HNEPRPVPNP",rtseq=7,rtrep=.t.,;
    cFormVar="w_ATNUMREG",value=0,isprimarykey=.t.,;
    ToolTipText = "Numero registro IVA associato all'attivit�",;
    HelpContextID = 257999181,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=30, Left=-2, Top=0, cSayPict=["99"], cGetPict=["99"]

  func oATNUMREG_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ATNUMREG<>0)
    endwith
    return bRes
  endfunc

  add object oATTIPREG_2_2 as StdTrsCombo with uid="EWUPSUILMJ",rtrep=.t.,;
    cFormVar="w_ATTIPREG", RowSource=""+"Vendite,"+"Acquisti,"+"Corr.scorporo,"+"Corr.ventilazione" , ;
    ToolTipText = "Tipo registro IVA associato all'attivit�",;
    HelpContextID = 260383053,;
    Height=21, Width=123, Left=32, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oATTIPREG_2_2.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..ATTIPREG,&i_cF..t_ATTIPREG),this.value)
    return(iif(xVal =1,'V',;
    iif(xVal =2,'A',;
    iif(xVal =3,'C',;
    iif(xVal =4,'E',;
    space(1))))))
  endfunc
  func oATTIPREG_2_2.GetRadio()
    this.Parent.oContained.w_ATTIPREG = this.RadioValue()
    return .t.
  endfunc

  func oATTIPREG_2_2.ToRadio()
    this.Parent.oContained.w_ATTIPREG=trim(this.Parent.oContained.w_ATTIPREG)
    return(;
      iif(this.Parent.oContained.w_ATTIPREG=='V',1,;
      iif(this.Parent.oContained.w_ATTIPREG=='A',2,;
      iif(this.Parent.oContained.w_ATTIPREG=='C',3,;
      iif(this.Parent.oContained.w_ATTIPREG=='E',4,;
      0)))))
  endfunc

  func oATTIPREG_2_2.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oATCODREG_2_3 as StdTrsField with uid="NHUYVQWYRW",rtseq=9,rtrep=.t.,;
    cFormVar="w_ATCODREG",value=space(2),;
    ToolTipText = "E' possibile associare ad ogni registro iva la regione nella quale si svolge la propria attivit� al fine di poter suddividere per regioni le operazioni imponibili verso i consumatori finali ",;
    HelpContextID = 248123725,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=46, Left=161, Top=1, InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="REG_PROV", cZoomOnZoom="GSCG_ARP", oKey_1_1="RPCODICE", oKey_1_2="this.w_ATCODREG"

  func oATCODREG_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oATCODREG_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oATCODREG_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'REG_PROV','*','RPCODICE',cp_AbsName(this.parent,'oATCODREG_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ARP',"Regioni e provincie autonome",'',this.parent.oContained
  endproc
  proc oATCODREG_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ARP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RPCODICE=this.parent.oContained.w_ATCODREG
    i_obj.ecpSave()
  endproc

  add object oATDATSTA_2_4 as StdTrsField with uid="WQDFFAXCPX",rtseq=10,rtrep=.t.,;
    cFormVar="w_ATDATSTA",value=ctod("  /  /  "),;
    ToolTipText = "Data ultima stampa registri IVA",;
    HelpContextID = 12329287,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=79, Left=211, Top=0

  add object oATDATCAL_2_5 as StdTrsField with uid="RFYRVRJLGF",rtseq=11,rtrep=.t.,;
    cFormVar="w_ATDATCAL",value=ctod("  /  /  "),;
    ToolTipText = "Data ultimo calcolo dei saldi IVA",;
    HelpContextID = 12329298,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=79, Left=294, Top=0

  add object oATDATBLO_2_6 as StdTrsField with uid="WQZIVZVHRU",rtseq=12,rtrep=.t.,;
    cFormVar="w_ATDATBLO",value=ctod("  /  /  "),;
    ToolTipText = "Data per blocco prima nota",;
    HelpContextID = 4447915,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=79, Left=377, Top=0

  add object oATPRPARI_2_7 as StdTrsField with uid="LJIIFKXFYD",rtseq=13,rtrep=.t.,;
    cFormVar="w_ATPRPARI",value=0,;
    ToolTipText = "Editabile se � attivato il check Intestazione. Rappresenta il numero dell�ultima pagina stampata sul registro. E� un campo modificabile dall�utente ed � progressivo per anno. ",;
    HelpContextID = 244179279,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=62, Left=553, Top=0, cSayPict=["9999999"], cGetPict=["9999999"]

  func oATPRPARI_2_7.mCond()
    with this.Parent.oContained
      return (.w_ATSTAINT='S')
    endwith
  endfunc

  add object oATSTAINT_2_8 as StdTrsCheck with uid="ZXDIRKFNHW",rtrep=.t.,;
    cFormVar="w_ATSTAINT",  caption="",;
    ToolTipText = "L�attivazione consente di numerare le pagine dei registri contestualmente alla stampa. Viene stampata l�intestazione dell�azienda, come risulta nei Dati Azienda",;
    HelpContextID = 174059174,;
    Left=461, Top=0, Width=38,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , AutoSize=.F.;
   , bGlobalFont=.t.


  func oATSTAINT_2_8.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..ATSTAINT,&i_cF..t_ATSTAINT),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oATSTAINT_2_8.GetRadio()
    this.Parent.oContained.w_ATSTAINT = this.RadioValue()
    return .t.
  endfunc

  func oATSTAINT_2_8.ToRadio()
    this.Parent.oContained.w_ATSTAINT=trim(this.Parent.oContained.w_ATSTAINT)
    return(;
      iif(this.Parent.oContained.w_ATSTAINT=='S',1,;
      0))
  endfunc

  func oATSTAINT_2_8.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oATFLREGI_2_11 as StdTrsCheck with uid="XWEUUMXNFW",rtrep=.t.,;
    cFormVar="w_ATFLREGI",  caption="",;
    ToolTipText = "Identifica il registro principale (vendite o corrispettivi). Il suo progressivo di pagina viene utilizzato per la stampa delle liquidazioni, del plafond sospensione Iva e dell�acconto Iva.",;
    HelpContextID = 44515663,;
    Left=509, Top=0, Width=38,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , AutoSize=.F.;
   , bGlobalFont=.t.


  func oATFLREGI_2_11.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..ATFLREGI,&i_cF..t_ATFLREGI),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oATFLREGI_2_11.GetRadio()
    this.Parent.oContained.w_ATFLREGI = this.RadioValue()
    return .t.
  endfunc

  func oATFLREGI_2_11.ToRadio()
    this.Parent.oContained.w_ATFLREGI=trim(this.Parent.oContained.w_ATFLREGI)
    return(;
      iif(this.Parent.oContained.w_ATFLREGI=='S',1,;
      0))
  endfunc

  func oATFLREGI_2_11.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oATFLREGI_2_11.mCond()
    with this.Parent.oContained
      return (.w_ATTIPREG<>'A')
    endwith
  endfunc

  add object oATCODSEZ_2_12 as StdTrsField with uid="KOCQTGFMAZ",rtseq=17,rtrep=.t.,;
    cFormVar="w_ATCODSEZ",value=0,;
    HelpContextID = 264900960,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=95, Left=620, Top=0

  func oATCODSEZ_2_12.mCond()
    with this.Parent.oContained
      return (g_LEMC = 'S')
    endwith
  endfunc

  add object oATMARREG_2_15 as StdTrsCheck with uid="EUZECXBSDL",rtrep=.t.,;
    cFormVar="w_ATMARREG",  caption="",;
    ToolTipText = "Flag regime del margine su registro",;
    HelpContextID = 261927245,;
    Left=742, Top=1, Width=20,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , AutoSize=.F.;
   , bGlobalFont=.t.


  func oATMARREG_2_15.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..ATMARREG,&i_cF..t_ATMARREG),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oATMARREG_2_15.GetRadio()
    this.Parent.oContained.w_ATMARREG = this.RadioValue()
    return .t.
  endfunc

  func oATMARREG_2_15.ToRadio()
    this.Parent.oContained.w_ATMARREG=trim(this.Parent.oContained.w_ATMARREG)
    return(;
      iif(this.Parent.oContained.w_ATMARREG=='S',1,;
      0))
  endfunc

  func oATMARREG_2_15.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oATMARREG_2_15.mCond()
    with this.Parent.oContained
      return (.w_ATREGMAR = 'S')
    endwith
  endfunc

  add object oATLIMCOM_2_16 as StdTrsCheck with uid="QVOTWULPYH",rtrep=.t.,;
    cFormVar="w_ATLIMCOM",  caption="",;
    ToolTipText = "Registro nel quale verranno riportate le scritture di competenza dell'esercizio ma registrate l'anno successivo",;
    HelpContextID = 262889133,;
    Left=766, Top=1, Width=20,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oATLIMCOM_2_16.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..ATLIMCOM,&i_cF..t_ATLIMCOM),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oATLIMCOM_2_16.GetRadio()
    this.Parent.oContained.w_ATLIMCOM = this.RadioValue()
    return .t.
  endfunc

  func oATLIMCOM_2_16.ToRadio()
    this.Parent.oContained.w_ATLIMCOM=trim(this.Parent.oContained.w_ATLIMCOM)
    return(;
      iif(this.Parent.oContained.w_ATLIMCOM=='S',1,;
      0))
  endfunc

  func oATLIMCOM_2_16.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oATLIMCOM_2_16.mCond()
    with this.Parent.oContained
      return (.w_ATTIPREG='A')
    endwith
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oATNUMREG_2_1.When()
    return(.t.)
  proc oATNUMREG_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oATNUMREG_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=5
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_mat','ATTIMAST','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".ATCODATT=ATTIMAST.ATCODATT";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsar_mat
Procedure SBIANCATRS()
  parameters padre
  if padre.NumRow() > 0 and padre.w_ATREGMAR # 'S' and Ah_YesNo('Deseleziono il check anche sui singoli registri?')
    padre.MarkPos()
    padre.FirstRow()
    do while not padre.eof_trs()
       padre.Set('w_ATMARREG',0) 
       padre.NextRow() 
    enddo
    padre.SetControlsValue()
    padre.Repos()
  endif  
Endproc
* --- Fine Area Manuale
