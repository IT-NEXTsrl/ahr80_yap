* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bpl                                                        *
*              Eliminazione prezzi/listini                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_41]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-24                                                      *
* Last revis.: 2010-01-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bpl",oParentObject)
return(i_retval)

define class tgsma_bpl as StdBatch
  * --- Local variables
  w_OLDART = space(20)
  w_OLDLIS = space(5)
  w_ROWNUM = 0
  w_TROVATO = .f.
  w_TIPODEL = space(30)
  w_DATATT = ctod("  /  /  ")
  w_DATDIS = ctod("  /  /  ")
  w_MESS = space(0)
  w_OLDKEY = space(25)
  w_QUANTI = space(15)
  w_PREZZO = space(23)
  w_SCONTI = space(254)
  * --- WorkFile variables
  LIS_TINI_idx=0
  LIS_SCAG_idx=0
  TMPVEND1_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questo batch permette l'eliminazione dei Prezzi o dei Listini in base
    * --- a quanto � stato selezionato sulla maschera (GSMA_KPL)
    * --- Lancio l'elaborazione
    ah_Msg("Elaborazione in corso...")
    * --- Scrivo log
    do case
      case this.oParentObject.w_TIPDEL = "O"
        this.w_TIPODEL = ah_MsgFormat("Obsoleti")
      case this.oParentObject.w_TIPDEL = "C"
        this.w_TIPODEL = ah_MsgFormat("Intervallo compreso")
      case this.oParentObject.w_TIPDEL = "E"
        this.w_TIPODEL = ah_MsgFormat("Intervallo esatto")
    endcase
    this.oParentObject.w_MSG = ""
    AddMsgNL("Eliminazione prezzi: %1",this, this.w_TIPODEL)
    AddMsgNL("Data inizio: %1 Data fine: %2",this, DTOC(this.oParentObject.w_DATINI), DTOC(this.oParentObject.w_DATFIN))
    AddMsgNL("%1",this, REPLICATE("=", 60))
    if not empty(this.oParentObject.w_CODLIS)
      AddMsgNL("Listino: %1 %2",this, this.oParentObject.w_CODLIS, this.oParentObject.w_DESLIS)
    endif
    * --- Inizio elaborazione
    this.w_TROVATO = .F.
    this.w_MESS = ""
    this.w_OLDKEY = REPLICATE("#", 25)
    * --- Create temporary table TMPVEND1
    i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('QUERY\GSMA_KPL.VQR',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPVEND1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Select from QUERY\GSMA1KPL
    do vq_exec with 'QUERY\GSMA1KPL',this,'_Curs_QUERY_GSMA1KPL','',.f.,.t.
    if used('_Curs_QUERY_GSMA1KPL')
      select _Curs_QUERY_GSMA1KPL
      locate for 1=1
      do while not(eof())
      * --- SCRIVO LOG
      if this.w_OLDKEY <> ALLTRIM(LICODART)+ALLTRIM(STR(CPROWNUM))
        this.w_OLDART = LICODART
        this.w_OLDLIS = LICODLIS
        this.w_ROWNUM = CPROWNUM
        this.w_DATATT = LIDATATT
        this.w_DATDIS = LIDATDIS
        this.w_MESS = this.w_MESS + ah_MsgFormat("Eliminazione listino: %1 - articolo: %2%0", this.w_OLDLIS, ALLTR(this.w_OLDART))
        this.w_MESS = this.w_MESS + ah_MsgFormat("%1Valido dal: %2 - fino a: %3%0", SPACE(8), DTOC(this.w_DATATT), DTOC(this.w_DATDIS))
        this.w_OLDKEY = ALLTRIM(LICODART)+ALLTRIM(STR(CPROWNUM))
      endif
      this.w_SCONTI = ah_MsgFormat("%1 ; %2 ; %3 ; %4", ALLTRIM(STR(LISCONT1, 8,2)), ALLTRIM(STR(LISCONT2, 8,2)), ALLTRIM(STR(LISCONT3, 8,2)), ALLTRIM(STR(LISCONT4, 8,2)))
      this.w_PREZZO = ALLTRIM(STR(LIPREZZO, 23, 5))
      this.w_QUANTI = IIF(LIQUANTI<>0, ALLTRIM(STR(LIQUANTI, 10, 3)), ah_MsgFormat("Oltre"))
      if NOT EMPTY(this.w_SCONTI)
        this.w_MESS = this.w_MESS + ah_MsgFormat("%1Prezzo: %2 - fino a qta: %3 - sconti/magg.: %4%0", SPACE(8), this.w_PREZZO, this.w_QUANTI, this.w_SCONTI)
      else
        this.w_MESS = this.w_MESS + ah_MsgFormat("%1Prezzo: %2 - fino a qta: %3%0", SPACE(8), this.w_PREZZO, this.w_QUANTI)
      endif
      this.w_TROVATO = .T.
        select _Curs_QUERY_GSMA1KPL
        continue
      enddo
      use
    endif
    if this.w_TROVATO = .T.
      * --- Elimino il listino selezionato .
      * --- Delete from LIS_SCAG
      i_nConn=i_TableProp[this.LIS_SCAG_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.LIS_SCAG_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex
        declare i_aIndex[1]
        i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
        i_cWhere=i_cTable+".LICODART = "+i_cQueryTable+".LICODART";
              +" and "+i_cTable+".LIROWNUM = "+i_cQueryTable+".LIROWNUM";
      
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where ";
              +""+i_cTable+".LICODART = "+i_cQueryTable+".LICODART";
              +" and "+i_cTable+".LIROWNUM = "+i_cQueryTable+".LIROWNUM";
              +")")
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Delete from LIS_TINI
      i_nConn=i_TableProp[this.LIS_TINI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.LIS_TINI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex
        declare i_aIndex[1]
        i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
        i_cWhere=i_cTable+".LICODART = "+i_cQueryTable+".LICODART";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
      
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where ";
              +""+i_cTable+".LICODART = "+i_cQueryTable+".LICODART";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
              +")")
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      AddMsg(this.w_MESS, this, , , , , ,.T.)
      ah_ErrorMsg("Elaborazione terminata")
    else
      this.oParentObject.w_MSG = ""
      ah_ErrorMsg("Per l'intervallo selezionato non esistono dati da eliminare")
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='LIS_TINI'
    this.cWorkTables[2]='LIS_SCAG'
    this.cWorkTables[3]='*TMPVEND1'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_QUERY_GSMA1KPL')
      use in _Curs_QUERY_GSMA1KPL
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
