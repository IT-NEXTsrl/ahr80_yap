* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bun                                                        *
*              Verifica univocit�                                              *
*                                                                              *
*      Author: Zucchetti S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-12-01                                                      *
* Last revis.: 2013-06-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPARAM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bun",oParentObject,m.pPARAM)
return(i_retval)

define class tgsut_bun as StdBatch
  * --- Local variables
  pPARAM = space(1)
  w_CLAALLE = space(5)
  w_CHIAVE = space(15)
  w_OBJECT = .NULL.
  w_COPATHDO = space(100)
  w_estensione = space(10)
  w_DIRLAV = space(10)
  w_ERRODIR = .f.
  w_FILENAMECRC = space(254)
  w_DOCUM = space(254)
  w_FILENAME = space(60)
  w_posizpunto = 0
  w_estensione = space(10)
  w_ERRORE = .f.
  w_TIPOPERAZ = space(1)
  w_PATH = space(220)
  w_ORIFIL = space(100)
  w_FOLDER = space(254)
  w_GESTEXT = space(5)
  w_DELORI = .f.
  w_STRINGA = space(100)
  w_ERRODIR = .f.
  w_PATH_KRF = space(254)
  * --- WorkFile variables
  PRODINDI_idx=0
  PROMINDI_idx=0
  CONTROPA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifico se richiesta l'univocit� dell'allegato
    * --- U  verifica univocita
    *     C  verifica creazione file
    *     D esegue cancellazione
    * --- Valorizzo i parametri richiesti nella query GSUT1BCU
    do case
      case this.pPARAM="U"
        this.w_OBJECT = this.oParentObject.GSUT_MAA
        this.w_CLAALLE = this.oParentObject.w_IDCLAALL
        this.w_OBJECT.MarkPos()
        this.w_OBJECT.FirstRow()
        do while ! this.w_OBJECT.Eof_Trs()
          if upper(alltrim(this.w_OBJECT.w_IDTABKEY)) = upper(alltrim(this.oParentObject.w_ARCHIVIO))
            this.w_CHIAVE = this.w_OBJECT.w_IDVALATT
            exit
          endif
          this.w_OBJECT.NextRow()
        enddo
        this.w_OBJECT.RePos()
        this.oParentObject.w_UNIVOC = .T.
        * --- Select from GSUT1BCU
        do vq_exec with 'GSUT1BCU',this,'_Curs_GSUT1BCU','',.f.,.t.
        if used('_Curs_GSUT1BCU')
          select _Curs_GSUT1BCU
          locate for 1=1
          do while not(eof())
          AH_ERRORMSG("E' gi� stato allegato un file, l'univocit� della classe non consente ulteriori allegati!",48,"","")
          this.oParentObject.w_UNIVOC = .F.
          exit
            select _Curs_GSUT1BCU
            continue
          enddo
          use
        endif
      case this.pPARAM="D"
        * --- Elimino indice nel caso operazione annullata da gsut_bun(U) 
        if this.oParentObject.w_ESE_DEL
          * --- Try
          local bErr_0399ED00
          bErr_0399ED00=bTrsErr
          this.Try_0399ED00()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_0399ED00
          * --- End
        else
          * --- Record updated
          if ((!this.oParentObject.w_IDWEBAPP==this.oParentObject.w_OldWebApp) or !(this.oParentObject.w_IDOGGETT==this.oParentObject.w_Oldogg) ) and Isalt()
            * --- E' stata modificata la modalit� di archiviazione - Prima era standard ed ora invece � selezionata anche la Web
            * --- Read from CONTROPA
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CONTROPA_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2],.t.,this.CONTROPA_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "COPATHDO"+;
                " from "+i_cTable+" CONTROPA where ";
                    +"COCODAZI = "+cp_ToStrODBC(i_CODAZI);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                COPATHDO;
                from (i_cTable) where;
                    COCODAZI = i_CODAZI;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_COPATHDO = NVL(cp_ToDate(_read_.COPATHDO),cp_NullValue(_read_.COPATHDO))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_FILENAME = Alltrim(this.oParentObject.w_IDNOMFIL)
            if this.oParentObject.w_IDMODALL="L"
              this.w_PATH = ADDBS(JUSTPATH(this.oParentObject.w_IDORIFIL))
            else
              this.w_PATH = ADDBS(this.oParentObject.w_IDPATALL)
              this.w_GESTEXT = justDrive(this.w_PATH)
              if Empty(this.w_GESTEXT) and !Directory(this.w_PATH)
                this.w_PATH = addbs(sys(5)+sys(2003))+Alltrim(this.w_PATH)
              endif
            endif
            * --- Aggiunge barra finale
            this.w_COPATHDO = ADDBS(this.w_COPATHDO)
            * --- Sono nel caso di archiviazione solo web
            this.w_estensione = "."+ Justext(this.w_FILENAME)
            * --- Archiviazione manuale
            *     Archiviazione da print system
            *     Archiviazione da processo documentale solo web
            this.w_ERRORE = .F.
            w_ErrorHandler = on("ERROR")
            on error this.w_ERRORE = .T.
            on error &w_ErrorHandler
            if g_CPIN="S" 
              * --- Archiviazione standard+web nel caso di archiviazione manuale
              * --- Legge percorso per documenti
              * --- Aggiunge barra finale
              this.w_COPATHDO = ADDBS(this.w_COPATHDO)
              * --- Elimina eventuale \ finale, poi la rimette ...
              this.w_DIRLAV = this.w_COPATHDO
              * --- Se non c'� crea la cartella ...
              if !DIRECTORY(this.w_DIRLAV)
                this.w_ERRODIR = Not(this.AH_CreateFolder(this.w_DIRLAV))
                if this.w_ERRODIR
                  Ah_ErrorMsg("Errore nella creazione directory",48,"")
                  i_retcode = 'stop'
                  return
                endif
              endif
              if cp_fileexist(this.w_COPATHDO+this.oParentObject.w_IDWEBFIL)
                delete file (Alltrim(this.w_COPATHDO)+Alltrim(this.oParentObject.w_IDWEBFIL))
              endif
              if Not EMpty(this.oParentObject.w_IDOGGETT)
                this.oParentObject.w_IDWEBFIL = Alltrim(this.oParentObject.w_IDOGGETT)+"_"+Alltrim(this.oParentObject.w_IDSERIAL)+Alltrim(this.w_ESTENSIONE)
              else
                this.oParentObject.w_IDWEBFIL = Alltrim(this.oParentObject.w_IDSERIAL)+Alltrim(this.w_ESTENSIONE)
              endif
              if this.oParentObject.w_IDWEBAPP="A"
                copy file (Alltrim(this.w_PATH)+this.w_FILENAME) to (this.w_COPATHDO+this.oParentObject.w_IDWEBFIL) 
                this.w_FILENAMECRC = Alltrim(this.w_PATH)+this.w_FILENAME
                this.w_FILENAMECRC = iif(cp_fileexist(this.w_FILENAMECRC), SYS(2007, FILETOSTR(this.w_FILENAMECRC), -1, 1), space(20))
              else
                this.w_FILENAMECRC = " "
                this.oParentObject.w_IDWEBFIL = " "
              endif
              if NOT(g_DMIP="S")
                * --- Creo CRC e setto flag cancellazione
                * --- Write into PROMINDI
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.PROMINDI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"IDCRCALL ="+cp_NullLink(cp_ToStrODBC(this.w_FILENAMECRC),'PROMINDI','IDCRCALL');
                  +",IDWEBFIL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_IDWEBFIL),'PROMINDI','IDWEBFIL');
                      +i_ccchkf ;
                  +" where ";
                      +"IDSERIAL = "+cp_ToStrODBC(this.oParentObject.w_IDSERIAL);
                         )
                else
                  update (i_cTable) set;
                      IDCRCALL = this.w_FILENAMECRC;
                      ,IDWEBFIL = this.oParentObject.w_IDWEBFIL;
                      &i_ccchkf. ;
                   where;
                      IDSERIAL = this.oParentObject.w_IDSERIAL;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
            endif
          endif
        endif
      otherwise
        if (this.oParentObject.w_OLDFIL<>this.oParentObject.w_IDNOMFIL) OR (this.oParentObject.w_OLDPAT<>this.oParentObject.w_IDPATALL AND this.oParentObject.w_IDMODALL="F")
          * --- Se ho modificato 
          *     -il nome file di destinazione  
          *     - il path di destinazione nel caso di copia
          *     - modalit� di archiviazione
          this.w_FILENAME = Alltrim(this.oParentObject.w_IDNOMFIL)
          this.w_posizpunto = rat(".", this.w_DOCUM)
          this.w_estensione = alltrim(lower(substr(this.w_DOCUM,this.w_posizpunto)))
          this.w_STRINGA = this.oParentObject.w_IDNOMFIL
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.oParentObject.w_IDNOMFIL = this.w_STRINGA
          if this.oParentObject.w_IDMODALL="L"
            this.w_ORIFIL = this.oParentObject.w_IDORIFIL
            this.oParentObject.w_IDNOMFIL = iif(empty(justext(this.oParentObject.w_IDNOMFIL)) and Not Empty(this.oParentObject.w_IDNOMFIL),alltrim(this.oParentObject.w_IDNOMFIL)+"."+Alltrim(justext(this.w_ORIFIL)),this.oParentObject.w_IDNOMFIL)
            this.w_DOCUM = addbs(Alltrim(JUSTPATH(this.oParentObject.w_IDORIFIL)))+Alltrim(this.oParentObject.w_IDNOMFIL)
            this.w_PATH = ADDBS(JUSTPATH(this.oParentObject.w_IDORIFIL))
          else
            * --- Sono in copia
            this.w_PATH = ADDBS(this.oParentObject.w_IDPATALL)
            this.w_GESTEXT = justDrive(this.w_PATH)
            if Empty(this.w_GESTEXT) and !Directory(this.w_PATH)
              this.w_PATH = addbs(sys(5)+sys(2003))+Alltrim(this.w_PATH)
            endif
            if this.oParentObject.w_IDORIFIL <>"@@@"
              this.w_ORIFIL = this.oParentObject.w_IDORIFIL
            else
              this.w_ORIFIL = ADDBS(Alltrim(this.oParentObject.w_OLDPAT))+Alltrim(this.oParentObject.w_IDORIGIN)
              this.w_ORIFIL = iif(empty(justext(this.w_ORIFIL)) and Not Empty(this.w_ORIFIL),alltrim(this.w_ORIFIL)+"."+Alltrim(justext(this.oParentObject.w_OLDFIL)),this.w_ORIFIL)
              if (this.oParentObject.w_OLDFIL<>this.oParentObject.w_IDORIGIN) AND !cp_fileexist(this.w_ORIFIL)
                * --- Ho modificato il nome file originario se non presente devo crearlo
                *     col nuovo nome nella stessa directory del file di origine
                 
 copy file (Alltrim(this.oParentObject.w_OLDPAT)+Alltrim(this.oParentObject.w_OLDFIL)) to (this.w_ORIFIL) 
                this.w_DELORI = .T.
              endif
            endif
            this.oParentObject.w_IDNOMFIL = iif(empty(justext(this.oParentObject.w_IDNOMFIL)) and Not Empty(this.oParentObject.w_IDNOMFIL),alltrim(this.oParentObject.w_IDNOMFIL)+"."+Alltrim(justext(this.w_ORIFIL)),this.oParentObject.w_IDNOMFIL)
            this.w_DOCUM = addbs(Alltrim(this.w_PATH))+Alltrim(this.oParentObject.w_IDNOMFIL)
          endif
          if this.oParentObject.w_IDMODALL="F" and (.not.directory(alltrim( this.w_PATH )))
            * --- Creazione directory immagini (LA GENERALE)
            this.w_FOLDER = left(this.w_PATH , len(this.w_PATH )-1)
            * --- Crea la cartella specificata nella variabile w_FOLDER
            *     Imposta la variabile w_ERRORE in caso di errore.
            this.w_ERRODIR = Not(this.AH_CreateFolder(this.w_FOLDER))
            if this.w_ERRODIR
              * --- Nessun messaggio se sotto transazione
              AH_ERRORMSG("Impossibile creare la cartella %1",48,"",this.w_FOLDER)
              i_retcode = 'stop'
              return
            else
              * --- Nessun messaggio se sotto transazione
              ah_msg("Creata nuova cartella %1",.t.,.f.,.f., rtrim(this.w_FOLDER))
            endif
          endif
           
 this.w_ERRORE = .F. 
 w_ErrorHandler = on("ERROR") 
 on error this.w_ERRORE = .T.
          if cp_fileexist(this.w_DOCUM)
            if this.w_ORIFIL<>this.w_DOCUM
              this.w_PATH_KRF = this.w_PATH
              do GSUT_KRF with this
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              if this.w_TIPOPERAZ<>"S" 
                if empty(this.w_FILENAME) or cp_fileexist(Alltrim(this.w_PATH)+Alltrim(this.w_FILENAME)) 
                  Ah_ErrorMsg("Operazione annullata",48,"")
                  on error &w_ErrorHandler
                  this.oParentObject.w_ESE_DEL = .t.
                  if cp_fileexist(Alltrim(Fullpath(this.oParentObject.w_OLDPAT))+Alltrim(this.oParentObject.w_OLDFIL))
                    * --- Ho annullato l'elaborazione devo eliminare file gi� creato
                    Delete file (Alltrim(Fullpath(this.oParentObject.w_OLDPAT))+Alltrim(this.oParentObject.w_OLDFIL))
                  endif
                  i_retcode = 'stop'
                  return
                else
                  this.oParentObject.w_IDNOMFIL = this.w_FILENAME
                endif
                this.w_STRINGA = this.w_FILENAME
                this.Pag2()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                this.oParentObject.w_IDNOMFIL = this.w_FILENAME
                this.oParentObject.w_IDNOMFIL = iif(empty(justext(this.oParentObject.w_IDNOMFIL)) and Not Empty(this.oParentObject.w_IDNOMFIL),alltrim(this.oParentObject.w_IDNOMFIL)+"."+Alltrim(justext(this.w_ORIFIL)),this.oParentObject.w_IDNOMFIL)
                this.w_DOCUM = Alltrim(this.w_PATH)+this.oParentObject.w_IDNOMFIL
              endif
            endif
          endif
          if cp_fileexist(this.w_ORIFIL) and (this.w_ORIFIL<>this.w_DOCUM) 
             
 copy file (this.w_ORIFIL) to (this.w_DOCUM) 
            if cp_fileexist(this.w_DOCUM) and cp_fileexist(Alltrim(Fullpath(this.oParentObject.w_OLDPAT))+Alltrim(this.oParentObject.w_OLDFIL)) and (Alltrim(Fullpath(this.oParentObject.w_OLDPAT))+Alltrim(this.oParentObject.w_OLDFIL) <> Fullpath(this.w_ORIFIL)) 
              Delete File (Alltrim(Fullpath(this.oParentObject.w_OLDPAT))+Alltrim(this.oParentObject.w_OLDFIL)))
            endif
          endif
          if this.w_DELORI and cp_fileexist(this.w_ORIFIL)
            Delete File (this.w_ORIFIL)
          endif
          on error &w_ErrorHandler
          * --- Assegno il nuovo path calcolato e la tipologia file
          this.oParentObject.w_IDPATALL = this.w_PATH
          this.oParentObject.origfilepath = this.oParentObject.w_IDPATALL
          this.oParentObject.w_IDTIPFIL = Alltrim(justext(this.oParentObject.w_IDNOMFIL))
          if Empty(Alltrim(justext(this.oParentObject.w_IDORIGIN)))
            this.oParentObject.w_IDORIGIN = Alltrim(this.oParentObject.w_IDORIGIN)+"."+Alltrim(this.oParentObject.w_IDTIPFIL)
          endif
        endif
    endcase
  endproc
  proc Try_0399ED00()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from PRODINDI
    i_nConn=i_TableProp[this.PRODINDI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"IDSERIAL = "+cp_ToStrODBC(this.oParentObject.w_IDSERIAL);
             )
    else
      delete from (i_cTable) where;
            IDSERIAL = this.oParentObject.w_IDSERIAL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from PROMINDI
    i_nConn=i_TableProp[this.PROMINDI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"IDSERIAL = "+cp_ToStrODBC(this.oParentObject.w_IDSERIAL);
             )
    else
      delete from (i_cTable) where;
            IDSERIAL = this.oParentObject.w_IDSERIAL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Il nome utilizzato per il file non � valido, o perch� contiene caratteri non consentiti o perch� � pi� lungo di 256 caratteri. 
    *     I nomi di file non possono contenere i caratteri \, /, :, *, ?, <, > e |.
    this.w_STRINGA = Strtran(this.w_STRINGA,"\","_")
    this.w_STRINGA = Strtran(this.w_STRINGA,"/","_")
    this.w_STRINGA = Strtran(this.w_STRINGA,":","_")
    this.w_STRINGA = Strtran(this.w_STRINGA,"*","_")
    this.w_STRINGA = Strtran(this.w_STRINGA,"?","_")
    this.w_STRINGA = Strtran(this.w_STRINGA,"<","_")
    this.w_STRINGA = Strtran(this.w_STRINGA,">","_")
    this.w_STRINGA = Strtran(this.w_STRINGA,"|","_")
    if !Isalt()
      this.w_STRINGA = Strtran(this.w_STRINGA,"&","_")
      this.w_STRINGA = Strtran(this.w_STRINGA,"^","_")
      this.w_STRINGA = Strtran(this.w_STRINGA, chr(34) ,"_")
    endif
  endproc


  proc Init(oParentObject,pPARAM)
    this.pPARAM=pPARAM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='PRODINDI'
    this.cWorkTables[2]='PROMINDI'
    this.cWorkTables[3]='CONTROPA'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_GSUT1BCU')
      use in _Curs_GSUT1BCU
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- gsut_bun
  * ===============================================================================
    * AH_CreateFolder() - Crea la cartella passata come parametro
    *
    Function AH_CreateFolder(pDirectory)
    private lOldErr,lRet
    * On Error
    lOldErr=ON("Error")
    * Init Risultato
    lRet = True
    * Crea
    on error lRet=False
    *
    pDirectory=JUSTPATH(ADDBS(pDirectory))
    *
    MD (pDirectory)
    *
    if not(Empty(lOldErr))
      on error &lOldErr
    else
      on error
    endif   
    * 
    if not(lRet)
      = Ah_ErrorMsg("Impossibile creare la cartella %1",16,,pDirectory)
    endif
    * Risultato
    Return (lRet)
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPARAM"
endproc
