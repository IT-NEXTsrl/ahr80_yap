* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bmu                                                        *
*              Check multi ut. import                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_28]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-18                                                      *
* Last revis.: 2008-06-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bmu",oParentObject)
return(i_retval)

define class tgsve_bmu as StdBatch
  * --- Local variables
  w_IMPEVA = 0
  w_QTAEV1 = 0
  w_FLEVAS = space(1)
  w_MESS = space(10)
  w_PADRE = .NULL.
  w_NUMEST = 0
  w_CFUNC = space(10)
  * --- WorkFile variables
  DOC_DETT_idx=0
  DOC_MAST_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Prima di inserire una riga controlla, se creata da import, che nessun altro utente l'abbia importata in precedenza
    this.w_PADRE = This.oParentObject
    * --- Non eseguo la mcalc all'uscita
    This.bUpdateParentObject=.f.
    this.w_PADRE.MarkPos(.T.)     
    this.w_PADRE.FirstRow()     
    do while Not this.w_PADRE.Eof_Trs()
      this.w_PADRE.SetRow()     
      if Not Empty( this.oParentObject.w_MVSERRIF ) And this.oParentObject.w_MVTIPRIG<>"D" And this.oParentObject.w_MVFLARIF $ "-+" And this.w_PADRE.RowStatus()="A" And this.w_PADRE.FullRow()
        * --- Read from DOC_DETT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MVQTAEV1,MVIMPEVA,MVFLEVAS"+;
            " from "+i_cTable+" DOC_DETT where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MVSERRIF);
                +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_MVROWRIF);
                +" and MVNUMRIF = "+cp_ToStrODBC(this.oParentObject.w_MVNUMRIF);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MVQTAEV1,MVIMPEVA,MVFLEVAS;
            from (i_cTable) where;
                MVSERIAL = this.oParentObject.w_MVSERRIF;
                and CPROWNUM = this.oParentObject.w_MVROWRIF;
                and MVNUMRIF = this.oParentObject.w_MVNUMRIF;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_QTAEV1 = NVL(cp_ToDate(_read_.MVQTAEV1),cp_NullValue(_read_.MVQTAEV1))
          this.w_IMPEVA = NVL(cp_ToDate(_read_.MVIMPEVA),cp_NullValue(_read_.MVIMPEVA))
          this.w_FLEVAS = NVL(cp_ToDate(_read_.MVFLEVAS),cp_NullValue(_read_.MVFLEVAS))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Arrotondo le Quantit� (nel caso non si utilizzi l'unit� di misura principale)
        if ( cp_Round( this.w_IMPEVA , 2)<>cp_Round( this.oParentObject.w_DOIMPEVA , 2) And this.oParentObject.w_MVTIPRIG="F") Or (cp_Round(this.w_QTAEV1,3)<>cp_Round(this.oParentObject.w_DOQTAEV1,3) And this.oParentObject.w_MVTIPRIG<>"F") Or this.w_FLEVAS = "S"
          * --- Abortisco il documento
          this.w_MESS = ah_Msgformat("Riga %1 gi� evasa da un altro utente", Alltrim(Str(this.oParentObject.w_CPROWORD)) )
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
          * --- Esco dal ciclo While..
          Exit
        endif
      endif
      this.w_PADRE.NextRow()     
    enddo
    this.w_PADRE.RePos()     
    * --- Eseguo l'aggiornamento dei progressivi sotto transazione
    this.w_CFUNC = this.w_PADRE.cFunction
    if this.oParentObject.w_MVCLADOC<>"OR" AND NOT EMPTY(this.oParentObject.w_MVANNPRO) AND this.w_CFUNC="Edit" And this.oParentObject.w_MVNUMEST<>this.oParentObject.w_ONUMEST AND this.oParentObject.w_MVNUMEST>=this.oParentObject.op_MVNUMEST
      * --- Variato Protocollo
      this.w_NUMEST = this.oParentObject.w_MVNUMEST
      i_Conn=i_TableProp[this.DOC_MAST_IDX, 3]
      cp_NextTableProg(this.oParentObject, i_Conn, "PRPRO", "i_codazi,w_MVANNPRO,w_MVPRP,w_MVALFEST,w_MVNUMEST")
      * --- Se il progressivo viene variato significa che non ho inserito un progressivo pi� grande 
      *     dell'ultimo disponibile
      if this.w_NUMEST<>this.oParentObject.w_MVNUMEST
        * --- Abortisco il documento
        this.w_MESS = ah_Msgformat("Impossibile aggiornare il progressivo protocollo perch� inferiore all'ultimo utilizzato")
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=this.w_MESS
      endif
    endif
    * --- Aggiornamento progressivo in variazione
    *     Svolto al termine non pregiudica i controlli sull'univocit� in quanto scrive
    *     nei progressivi il MVNUMDOC presente sul documento
    if this.w_CFUNC="Edit" And this.oParentObject.w_MVNUMDOC<>this.oParentObject.w_ONUMDOC AND this.oParentObject.w_MVNUMDOC>=this.oParentObject.op_MVNUMDOC AND NOT EMPTY(this.oParentObject.w_MVANNDOC) And (( this.oParentObject.w_MVFLVEAC="V" AND this.oParentObject.w_MVPRD<>"NN" ) Or ( this.oParentObject.w_MVFLVEAC="A" AND (this.oParentObject.w_MVPRD="DV" OR this.oParentObject.w_MVPRD="IV" OR this.oParentObject.w_MVPRD="OA") ))
      * --- Aggiorno Progressivo Numero documento se l'utente imposta a mano un
      *     numero pi� alto del prossimo progressivo disponibile
      this.w_NUMEST = this.oParentObject.w_MVNUMDOC
      i_Conn=i_TableProp[this.DOC_MAST_IDX, 3]
      cp_NextTableProg(this.oParentObject, i_Conn, "PRDOC", "i_codazi,w_MVANNDOC,w_MVPRD,w_MVALFDOC,w_MVNUMDOC")
      * --- Se il progressivo viene variato significa che non ho inserito un progressivo pi� grande 
      *     dell'ultimo disponibile
      if this.w_NUMEST<>this.oParentObject.w_MVNUMDOC
        * --- Abortisco il documento
        this.w_MESS = ah_Msgformat("Impossibile aggiornare il progressivo documento perch� inferiore all'ultimo utilizzato")
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=this.w_MESS
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='DOC_DETT'
    this.cWorkTables[2]='DOC_MAST'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
