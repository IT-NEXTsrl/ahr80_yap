* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_mtt                                                        *
*              Tariffe avvocati                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-12-13                                                      *
* Last revis.: 2012-10-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsma_mtt")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsma_mtt")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsma_mtt")
  return

* --- Class definition
define class tgsma_mtt as StdPCForm
  Width  = 653
  Height = 529
  Top    = 39
  Left   = 207
  cComment = "Tariffe avvocati"
  cPrg = "gsma_mtt"
  HelpContextID=80378729
  add object cnt as tcgsma_mtt
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsma_mtt as PCContext
  w_TACODART = space(20)
  w_CODAZI = space(5)
  w_CODAZI2 = space(5)
  w_TACODLIS = space(5)
  w_TADATATT = space(8)
  w_TADATDIS = space(8)
  w_PRESTA = space(1)
  w_TAAUTORI = space(10)
  w_TAGAZUFF = space(6)
  w_TARESPON = space(5)
  w_TAMANSION = space(5)
  w_DESLIS = space(40)
  w_VALLIS = space(3)
  w_DTINVA = space(8)
  w_DTFINV = space(8)
  w_SIMVAL = space(5)
  w_TIPONO = space(1)
  w_CODLIS = space(5)
  w_RESCHK = 0
  w_EPTIPO = space(10)
  w_DESAUT = space(60)
  w_DATINI = space(8)
  w_DATFIN = space(8)
  w_COGNOME = space(40)
  w_NOME = space(40)
  w_DENOM_RES = space(39)
  w_DESMANS = space(40)
  w_TipoPersona = space(1)
  w_OB_TEST = space(8)
  w_DATOBSO = space(8)
  proc Save(i_oFrom)
    this.w_TACODART = i_oFrom.w_TACODART
    this.w_CODAZI = i_oFrom.w_CODAZI
    this.w_CODAZI2 = i_oFrom.w_CODAZI2
    this.w_TACODLIS = i_oFrom.w_TACODLIS
    this.w_TADATATT = i_oFrom.w_TADATATT
    this.w_TADATDIS = i_oFrom.w_TADATDIS
    this.w_PRESTA = i_oFrom.w_PRESTA
    this.w_TAAUTORI = i_oFrom.w_TAAUTORI
    this.w_TAGAZUFF = i_oFrom.w_TAGAZUFF
    this.w_TARESPON = i_oFrom.w_TARESPON
    this.w_TAMANSION = i_oFrom.w_TAMANSION
    this.w_DESLIS = i_oFrom.w_DESLIS
    this.w_VALLIS = i_oFrom.w_VALLIS
    this.w_DTINVA = i_oFrom.w_DTINVA
    this.w_DTFINV = i_oFrom.w_DTFINV
    this.w_SIMVAL = i_oFrom.w_SIMVAL
    this.w_TIPONO = i_oFrom.w_TIPONO
    this.w_CODLIS = i_oFrom.w_CODLIS
    this.w_RESCHK = i_oFrom.w_RESCHK
    this.w_EPTIPO = i_oFrom.w_EPTIPO
    this.w_DESAUT = i_oFrom.w_DESAUT
    this.w_DATINI = i_oFrom.w_DATINI
    this.w_DATFIN = i_oFrom.w_DATFIN
    this.w_COGNOME = i_oFrom.w_COGNOME
    this.w_NOME = i_oFrom.w_NOME
    this.w_DENOM_RES = i_oFrom.w_DENOM_RES
    this.w_DESMANS = i_oFrom.w_DESMANS
    this.w_TipoPersona = i_oFrom.w_TipoPersona
    this.w_OB_TEST = i_oFrom.w_OB_TEST
    this.w_DATOBSO = i_oFrom.w_DATOBSO
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_TACODART = this.w_TACODART
    i_oTo.w_CODAZI = this.w_CODAZI
    i_oTo.w_CODAZI2 = this.w_CODAZI2
    i_oTo.w_TACODLIS = this.w_TACODLIS
    i_oTo.w_TADATATT = this.w_TADATATT
    i_oTo.w_TADATDIS = this.w_TADATDIS
    i_oTo.w_PRESTA = this.w_PRESTA
    i_oTo.w_TAAUTORI = this.w_TAAUTORI
    i_oTo.w_TAGAZUFF = this.w_TAGAZUFF
    i_oTo.w_TARESPON = this.w_TARESPON
    i_oTo.w_TAMANSION = this.w_TAMANSION
    i_oTo.w_DESLIS = this.w_DESLIS
    i_oTo.w_VALLIS = this.w_VALLIS
    i_oTo.w_DTINVA = this.w_DTINVA
    i_oTo.w_DTFINV = this.w_DTFINV
    i_oTo.w_SIMVAL = this.w_SIMVAL
    i_oTo.w_TIPONO = this.w_TIPONO
    i_oTo.w_CODLIS = this.w_CODLIS
    i_oTo.w_RESCHK = this.w_RESCHK
    i_oTo.w_EPTIPO = this.w_EPTIPO
    i_oTo.w_DESAUT = this.w_DESAUT
    i_oTo.w_DATINI = this.w_DATINI
    i_oTo.w_DATFIN = this.w_DATFIN
    i_oTo.w_COGNOME = this.w_COGNOME
    i_oTo.w_NOME = this.w_NOME
    i_oTo.w_DENOM_RES = this.w_DENOM_RES
    i_oTo.w_DESMANS = this.w_DESMANS
    i_oTo.w_TipoPersona = this.w_TipoPersona
    i_oTo.w_OB_TEST = this.w_OB_TEST
    i_oTo.w_DATOBSO = this.w_DATOBSO
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsma_mtt as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 653
  Height = 529
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-10-30"
  HelpContextID=80378729
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=30

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  TAR_IFFE_IDX = 0
  LISTINI_IDX = 0
  ART_ICOL_IDX = 0
  VALUTE_IDX = 0
  PRA_ENTI_IDX = 0
  DIPENDEN_IDX = 0
  MANSIONI_IDX = 0
  PAR_AGEN_IDX = 0
  PAR_PRAT_IDX = 0
  PAR_ALTE_IDX = 0
  cFile = "TAR_IFFE"
  cKeySelect = "TACODART"
  cKeyWhere  = "TACODART=this.w_TACODART"
  cKeyDetail  = "TACODART=this.w_TACODART"
  cKeyWhereODBC = '"TACODART="+cp_ToStrODBC(this.w_TACODART)';

  cKeyDetailWhereODBC = '"TACODART="+cp_ToStrODBC(this.w_TACODART)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"TAR_IFFE.TACODART="+cp_ToStrODBC(this.w_TACODART)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'TAR_IFFE.CPROWNUM '
  cPrg = "gsma_mtt"
  cComment = "Tariffe avvocati"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 7
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TACODART = space(20)
  w_CODAZI = space(5)
  w_CODAZI2 = space(5)
  w_TACODLIS = space(5)
  o_TACODLIS = space(5)
  w_TADATATT = ctod('  /  /  ')
  w_TADATDIS = ctod('  /  /  ')
  w_PRESTA = space(1)
  w_TAAUTORI = space(10)
  w_TAGAZUFF = space(6)
  w_TARESPON = space(5)
  o_TARESPON = space(5)
  w_TAMANSION = space(5)
  w_DESLIS = space(40)
  w_VALLIS = space(3)
  w_DTINVA = ctod('  /  /  ')
  w_DTFINV = ctod('  /  /  ')
  w_SIMVAL = space(5)
  w_TIPONO = space(1)
  w_CODLIS = space(5)
  w_RESCHK = 0
  w_EPTIPO = space(10)
  w_DESAUT = space(60)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_COGNOME = space(40)
  w_NOME = space(40)
  w_DENOM_RES = space(39)
  w_DESMANS = space(40)
  w_TipoPersona = space(1)
  w_OB_TEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')

  * --- Children pointers
  gsma_mdi = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    *set procedure to gsma_mdi additive
    with this
      .Pages(1).addobject("oPag","tgsma_mttPag1","gsma_mtt",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    *release procedure gsma_mdi
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[10]
    this.cWorkTables[1]='LISTINI'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='VALUTE'
    this.cWorkTables[4]='PRA_ENTI'
    this.cWorkTables[5]='DIPENDEN'
    this.cWorkTables[6]='MANSIONI'
    this.cWorkTables[7]='PAR_AGEN'
    this.cWorkTables[8]='PAR_PRAT'
    this.cWorkTables[9]='PAR_ALTE'
    this.cWorkTables[10]='TAR_IFFE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(10))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.TAR_IFFE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.TAR_IFFE_IDX,3]
  return

  function CreateChildren()
    this.gsma_mdi = CREATEOBJECT('stdDynamicChild',this,'gsma_mdi',this.oPgFrm.Page1.oPag.oLinkPC_2_9)
    this.gsma_mdi.createrealchild()
    return

  procedure NewContext()
    return(createobject('tsgsma_mtt'))

  function DestroyChildrenChain()
    this.oParentObject=.NULL.
    if !ISNULL(this.gsma_mdi)
      this.gsma_mdi.DestroyChildrenChain()
    endif
    return

  function HideChildrenChain()
    *this.Hide()
    this.bOnScreen = .f.
    this.gsma_mdi.HideChildrenChain()
    return

  function ShowChildrenChain()
    this.bOnScreen=.t.
    this.gsma_mdi.ShowChildrenChain()
    DoDefault()
    return
  procedure DestroyChildren()
    if !ISNULL(this.gsma_mdi)
      this.gsma_mdi.DestroyChildrenChain()
      this.gsma_mdi=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_2_9')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.gsma_mdi.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.gsma_mdi.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.gsma_mdi.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .gsma_mdi.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_TACODART,"DECODART";
             ,.w_CPROWNUM,"DEROWNUM";
             )
    endwith
    select (i_cOldSel)
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_1_joined
    link_2_1_joined=.f.
    local link_2_5_joined
    link_2_5_joined=.f.
    local link_2_8_joined
    link_2_8_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from TAR_IFFE where TACODART=KeySet.TACODART
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- gsma_mtt
      * --- Setta Ordine per Listino, Date Attivazione
      i_cOrder = 'order by TACODLIS, TADATATT DESC, TADATDIS '
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.TAR_IFFE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TAR_IFFE_IDX,2],this.bLoadRecFilter,this.TAR_IFFE_IDX,"gsma_mtt")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('TAR_IFFE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "TAR_IFFE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' TAR_IFFE '
      link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_5_joined=this.AddJoinedLink_2_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_8_joined=this.AddJoinedLink_2_8(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'TACODART',this.w_TACODART  )
      select * from (i_cTable) TAR_IFFE where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CODAZI = i_CODAZI
        .w_CODAZI2 = i_CODAZI
        .w_CODLIS = space(5)
        .w_RESCHK = 0
        .w_DATINI = ctod("  /  /  ")
        .w_DATFIN = ctod("  /  /  ")
        .w_TipoPersona = 'P'
        .w_OB_TEST = i_datsys
        .w_TACODART = NVL(TACODART,space(20))
          .link_1_2('Load')
          .link_1_3('Load')
        .w_TIPONO = IIF(.w_PRESTA='I','C',IIF(.w_PRESTA='E','P',' '))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'TAR_IFFE')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_PRESTA = space(1)
          .w_DESLIS = space(40)
          .w_VALLIS = space(3)
          .w_DTINVA = ctod("  /  /  ")
          .w_DTFINV = ctod("  /  /  ")
          .w_SIMVAL = space(5)
          .w_EPTIPO = space(10)
          .w_DESAUT = space(60)
          .w_COGNOME = space(40)
          .w_NOME = space(40)
          .w_DESMANS = space(40)
          .w_DATOBSO = ctod("  /  /  ")
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_TACODLIS = NVL(TACODLIS,space(5))
          if link_2_1_joined
            this.w_TACODLIS = NVL(LSCODLIS201,NVL(this.w_TACODLIS,space(5)))
            this.w_DESLIS = NVL(LSDESLIS201,space(40))
            this.w_VALLIS = NVL(LSVALLIS201,space(3))
            this.w_DTINVA = NVL(cp_ToDate(LSDTINVA201),ctod("  /  /  "))
            this.w_DTFINV = NVL(cp_ToDate(LSDTOBSO201),ctod("  /  /  "))
            this.w_TADATATT = NVL(cp_ToDate(LSDTINVA201),ctod("  /  /  "))
            this.w_TADATDIS = NVL(cp_ToDate(LSDTOBSO201),ctod("  /  /  "))
          else
          .link_2_1('Load')
          endif
          .w_TADATATT = NVL(cp_ToDate(TADATATT),ctod("  /  /  "))
          .w_TADATDIS = NVL(cp_ToDate(TADATDIS),ctod("  /  /  "))
          .w_TAAUTORI = NVL(TAAUTORI,space(10))
          if link_2_5_joined
            this.w_TAAUTORI = NVL(EPCODICE205,NVL(this.w_TAAUTORI,space(10)))
            this.w_DESAUT = NVL(EPDESCRI205,space(60))
            this.w_EPTIPO = NVL(EP__TIPO205,space(10))
          else
          .link_2_5('Load')
          endif
          .w_TAGAZUFF = NVL(TAGAZUFF,space(6))
          .w_TARESPON = NVL(TARESPON,space(5))
          .link_2_7('Load')
          .w_TAMANSION = NVL(TAMANSION,space(5))
          if link_2_8_joined
            this.w_TAMANSION = NVL(MNCODICE208,NVL(this.w_TAMANSION,space(5)))
            this.w_DESMANS = NVL(MNDESCRI208,space(40))
          else
          .link_2_8('Load')
          endif
          .link_2_11('Load')
        .w_DENOM_RES = alltrim(.w_COGNOME)+space(1)+alltrim(.w_NOME)
          select (this.cTrsName)
          append blank
          replace TACODART with .w_TACODART
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_TIPONO = IIF(.w_PRESTA='I','C',IIF(.w_PRESTA='E','P',' '))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_1_11.enabled = .oPgFrm.Page1.oPag.oBtn_1_11.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_TACODART=space(20)
      .w_CODAZI=space(5)
      .w_CODAZI2=space(5)
      .w_TACODLIS=space(5)
      .w_TADATATT=ctod("  /  /  ")
      .w_TADATDIS=ctod("  /  /  ")
      .w_PRESTA=space(1)
      .w_TAAUTORI=space(10)
      .w_TAGAZUFF=space(6)
      .w_TARESPON=space(5)
      .w_TAMANSION=space(5)
      .w_DESLIS=space(40)
      .w_VALLIS=space(3)
      .w_DTINVA=ctod("  /  /  ")
      .w_DTFINV=ctod("  /  /  ")
      .w_SIMVAL=space(5)
      .w_TIPONO=space(1)
      .w_CODLIS=space(5)
      .w_RESCHK=0
      .w_EPTIPO=space(10)
      .w_DESAUT=space(60)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_COGNOME=space(40)
      .w_NOME=space(40)
      .w_DENOM_RES=space(39)
      .w_DESMANS=space(40)
      .w_TipoPersona=space(1)
      .w_OB_TEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        .w_CODAZI = i_CODAZI
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODAZI))
         .link_1_2('Full')
        endif
        .w_CODAZI2 = i_CODAZI
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODAZI2))
         .link_1_3('Full')
        endif
        .w_TACODLIS = IIF(EMPTY(.w_TACODLIS),.w_CODLIS,.w_TACODLIS)
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_TACODLIS))
         .link_2_1('Full')
        endif
        .w_TADATATT = iif(EMPTY(.w_DTINVA), .w_DATINI, .w_DTINVA)
        .w_TADATDIS = iif(EMPTY(.w_DTFINV), .w_DATFIN, .w_DTFINV)
        .DoRTCalc(7,8,.f.)
        if not(empty(.w_TAAUTORI))
         .link_2_5('Full')
        endif
        .DoRTCalc(9,10,.f.)
        if not(empty(.w_TARESPON))
         .link_2_7('Full')
        endif
        .w_TAMANSION = IIF(EMPTY(.w_TARESPON),.w_TAMANSION,'')
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_TAMANSION))
         .link_2_8('Full')
        endif
        .DoRTCalc(12,13,.f.)
        if not(empty(.w_VALLIS))
         .link_2_11('Full')
        endif
        .DoRTCalc(14,16,.f.)
        .w_TIPONO = IIF(.w_PRESTA='I','C',IIF(.w_PRESTA='E','P',' '))
        .DoRTCalc(18,18,.f.)
        .w_RESCHK = 0
        .DoRTCalc(20,25,.f.)
        .w_DENOM_RES = alltrim(.w_COGNOME)+space(1)+alltrim(.w_NOME)
        .DoRTCalc(27,27,.f.)
        .w_TipoPersona = 'P'
        .w_OB_TEST = i_datsys
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'TAR_IFFE')
    this.DoRTCalc(30,30,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBtn_1_11.enabled = .Page1.oPag.oBtn_1_11.mCond()
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    this.gsma_mdi.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'TAR_IFFE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.gsma_mdi.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.TAR_IFFE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TACODART,"TACODART",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_TACODLIS C(5);
      ,t_TADATATT D(8);
      ,t_TADATDIS D(8);
      ,t_TAAUTORI C(10);
      ,t_TAGAZUFF C(6);
      ,t_TARESPON C(5);
      ,t_TAMANSION C(5);
      ,t_DESLIS C(40);
      ,t_SIMVAL C(5);
      ,t_DESAUT C(60);
      ,t_DENOM_RES C(39);
      ,t_DESMANS C(40);
      ,TACODART C(20);
      ,CPROWNUM N(10);
      ,t_PRESTA C(1);
      ,t_VALLIS C(3);
      ,t_DTINVA D(8);
      ,t_DTFINV D(8);
      ,t_EPTIPO C(10);
      ,t_COGNOME C(40);
      ,t_NOME C(40);
      ,t_DATOBSO D(8);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsma_mttbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTACODLIS_2_1.controlsource=this.cTrsName+'.t_TACODLIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTADATATT_2_2.controlsource=this.cTrsName+'.t_TADATATT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTADATDIS_2_3.controlsource=this.cTrsName+'.t_TADATDIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTAAUTORI_2_5.controlsource=this.cTrsName+'.t_TAAUTORI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTAGAZUFF_2_6.controlsource=this.cTrsName+'.t_TAGAZUFF'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTARESPON_2_7.controlsource=this.cTrsName+'.t_TARESPON'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTAMANSION_2_8.controlsource=this.cTrsName+'.t_TAMANSION'
    this.oPgFRm.Page1.oPag.oDESLIS_2_10.controlsource=this.cTrsName+'.t_DESLIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSIMVAL_2_14.controlsource=this.cTrsName+'.t_SIMVAL'
    this.oPgFRm.Page1.oPag.oDESAUT_2_16.controlsource=this.cTrsName+'.t_DESAUT'
    this.oPgFRm.Page1.oPag.oDENOM_RES_2_19.controlsource=this.cTrsName+'.t_DENOM_RES'
    this.oPgFRm.Page1.oPag.oDESMANS_2_20.controlsource=this.cTrsName+'.t_DESMANS'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(74)
    this.AddVLine(153)
    this.AddVLine(232)
    this.AddVLine(286)
    this.AddVLine(386)
    this.AddVLine(444)
    this.AddVLine(509)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTACODLIS_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.TAR_IFFE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TAR_IFFE_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.TAR_IFFE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TAR_IFFE_IDX,2])
      *
      * insert into TAR_IFFE
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'TAR_IFFE')
        i_extval=cp_InsertValODBCExtFlds(this,'TAR_IFFE')
        i_cFldBody=" "+;
                  "(TACODART,TACODLIS,TADATATT,TADATDIS,TAAUTORI"+;
                  ",TAGAZUFF,TARESPON,TAMANSION,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_TACODART)+","+cp_ToStrODBCNull(this.w_TACODLIS)+","+cp_ToStrODBC(this.w_TADATATT)+","+cp_ToStrODBC(this.w_TADATDIS)+","+cp_ToStrODBCNull(this.w_TAAUTORI)+;
             ","+cp_ToStrODBC(this.w_TAGAZUFF)+","+cp_ToStrODBCNull(this.w_TARESPON)+","+cp_ToStrODBCNull(this.w_TAMANSION)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'TAR_IFFE')
        i_extval=cp_InsertValVFPExtFlds(this,'TAR_IFFE')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'TACODART',this.w_TACODART)
        INSERT INTO (i_cTable) (;
                   TACODART;
                  ,TACODLIS;
                  ,TADATATT;
                  ,TADATDIS;
                  ,TAAUTORI;
                  ,TAGAZUFF;
                  ,TARESPON;
                  ,TAMANSION;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_TACODART;
                  ,this.w_TACODLIS;
                  ,this.w_TADATATT;
                  ,this.w_TADATDIS;
                  ,this.w_TAAUTORI;
                  ,this.w_TAGAZUFF;
                  ,this.w_TARESPON;
                  ,this.w_TAMANSION;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.TAR_IFFE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TAR_IFFE_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
          if this.w_TACODART<>TACODART
            i_bUpdAll = .t.
          endif
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_TACODLIS))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'TAR_IFFE')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'TAR_IFFE')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
          if this.w_TACODART<>TACODART
            i_bUpdAll = .t.
          endif
        endif
        scan for (not(Empty(t_TACODLIS))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete row children
              this.gsma_mdi.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_TACODART,"DECODART";
                     ,this.w_CPROWNUM,"DEROWNUM";
                     )
              this.gsma_mdi.mDelete()
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update TAR_IFFE
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'TAR_IFFE')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " TACODLIS="+cp_ToStrODBCNull(this.w_TACODLIS)+;
                     ",TADATATT="+cp_ToStrODBC(this.w_TADATATT)+;
                     ",TADATDIS="+cp_ToStrODBC(this.w_TADATDIS)+;
                     ",TAAUTORI="+cp_ToStrODBCNull(this.w_TAAUTORI)+;
                     ",TAGAZUFF="+cp_ToStrODBC(this.w_TAGAZUFF)+;
                     ",TARESPON="+cp_ToStrODBCNull(this.w_TARESPON)+;
                     ",TAMANSION="+cp_ToStrODBCNull(this.w_TAMANSION)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'TAR_IFFE')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      TACODLIS=this.w_TACODLIS;
                     ,TADATATT=this.w_TADATATT;
                     ,TADATDIS=this.w_TADATDIS;
                     ,TAAUTORI=this.w_TAAUTORI;
                     ,TAGAZUFF=this.w_TAGAZUFF;
                     ,TARESPON=this.w_TARESPON;
                     ,TAMANSION=this.w_TAMANSION;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask to row children to save themselves
      select (this.cTrsName)
      i_TN = this.cTrsName
      scan for (not(Empty(t_TACODLIS)))
        * --- > Optimize children saving
        i_nRec = recno()
        this.WorkFromTrs()
        if not(deleted())
          this.gsma_mdi.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_TACODART,"DECODART";
               ,this.w_CPROWNUM,"DEROWNUM";
               )
          this.gsma_mdi.mReplace()
          this.gsma_mdi.bSaveContext=.f.
        endif
      endscan
     this.gsma_mdi.bSaveContext=.t.
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.TAR_IFFE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TAR_IFFE_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_TACODLIS))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_nRec = recno()
        * --- gsma_mdi : Deleting
        this.gsma_mdi.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_TACODART,"DECODART";
               ,this.w_CPROWNUM,"DEROWNUM";
               )
        this.gsma_mdi.mDelete()
        if bTrsErr
          i_nModRow = -1
          exit
        endif
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete TAR_IFFE
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_TACODLIS))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.TAR_IFFE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TAR_IFFE_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
          .link_1_2('Full')
          .link_1_3('Full')
        .DoRTCalc(4,4,.t.)
        if .o_TACODLIS<>.w_TACODLIS
          .w_TADATATT = iif(EMPTY(.w_DTINVA), .w_DATINI, .w_DTINVA)
        endif
        if .o_TACODLIS<>.w_TACODLIS
          .w_TADATDIS = iif(EMPTY(.w_DTFINV), .w_DATFIN, .w_DTFINV)
        endif
        .DoRTCalc(7,10,.t.)
        if .o_TARESPON<>.w_TARESPON
          .w_TAMANSION = IIF(EMPTY(.w_TARESPON),.w_TAMANSION,'')
          .link_2_8('Full')
        endif
        .DoRTCalc(12,12,.t.)
          .link_2_11('Full')
        .DoRTCalc(14,16,.t.)
          .w_TIPONO = IIF(.w_PRESTA='I','C',IIF(.w_PRESTA='E','P',' '))
        .DoRTCalc(18,25,.t.)
        if .o_TARESPON<>.w_TARESPON
          .w_DENOM_RES = alltrim(.w_COGNOME)+space(1)+alltrim(.w_NOME)
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(27,30,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_PRESTA with this.w_PRESTA
      replace t_VALLIS with this.w_VALLIS
      replace t_DTINVA with this.w_DTINVA
      replace t_DTFINV with this.w_DTFINV
      replace t_EPTIPO with this.w_EPTIPO
      replace t_COGNOME with this.w_COGNOME
      replace t_NOME with this.w_NOME
      replace t_DATOBSO with this.w_DATOBSO
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTAMANSION_2_8.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTAMANSION_2_8.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_11.visible=!this.oPgFrm.Page1.oPag.oBtn_1_11.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_PRAT_IDX,3]
    i_lTable = "PAR_PRAT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_PRAT_IDX,2], .t., this.PAR_PRAT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_PRAT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PACODLIS";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_CODAZI)
            select PACODAZI,PACODLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.PACODAZI,space(5))
      this.w_CODLIS = NVL(_Link_.PACODLIS,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_CODLIS = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_PRAT_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_PRAT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAZI2
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_ALTE_IDX,3]
    i_lTable = "PAR_ALTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2], .t., this.PAR_ALTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PADATINI,PADATFIN";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_CODAZI2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_CODAZI2)
            select PACODAZI,PADATINI,PADATFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI2 = NVL(_Link_.PACODAZI,space(5))
      this.w_DATINI = NVL(cp_ToDate(_Link_.PADATINI),ctod("  /  /  "))
      this.w_DATFIN = NVL(cp_ToDate(_Link_.PADATFIN),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI2 = space(5)
      endif
      this.w_DATINI = ctod("  /  /  ")
      this.w_DATFIN = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_ALTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TACODLIS
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TACODLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_TACODLIS)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSDTINVA,LSDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_TACODLIS))
          select LSCODLIS,LSDESLIS,LSVALLIS,LSDTINVA,LSDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TACODLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStrODBC(trim(this.w_TACODLIS)+"%");

            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSDTINVA,LSDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStr(trim(this.w_TACODLIS)+"%");

            select LSCODLIS,LSDESLIS,LSVALLIS,LSDTINVA,LSDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_TACODLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oTACODLIS_2_1'),i_cWhere,'GSAR_ALI',"Listini",'listob1.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSDTINVA,LSDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSVALLIS,LSDTINVA,LSDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TACODLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSDTINVA,LSDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_TACODLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_TACODLIS)
            select LSCODLIS,LSDESLIS,LSVALLIS,LSDTINVA,LSDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TACODLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLIS = NVL(_Link_.LSDESLIS,space(40))
      this.w_VALLIS = NVL(_Link_.LSVALLIS,space(3))
      this.w_DTINVA = NVL(cp_ToDate(_Link_.LSDTINVA),ctod("  /  /  "))
      this.w_DTFINV = NVL(cp_ToDate(_Link_.LSDTOBSO),ctod("  /  /  "))
      this.w_TADATATT = NVL(cp_ToDate(_Link_.LSDTINVA),ctod("  /  /  "))
      this.w_TADATDIS = NVL(cp_ToDate(_Link_.LSDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_TACODLIS = space(5)
      endif
      this.w_DESLIS = space(40)
      this.w_VALLIS = space(3)
      this.w_DTINVA = ctod("  /  /  ")
      this.w_DTFINV = ctod("  /  /  ")
      this.w_TADATATT = ctod("  /  /  ")
      this.w_TADATDIS = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TACODLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 7 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.LISTINI_IDX,3] and i_nFlds+7<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.LSCODLIS as LSCODLIS201"+ ",link_2_1.LSDESLIS as LSDESLIS201"+ ",link_2_1.LSVALLIS as LSVALLIS201"+ ",link_2_1.LSDTINVA as LSDTINVA201"+ ",link_2_1.LSDTOBSO as LSDTOBSO201"+ ",link_2_1.LSDTINVA as LSDTINVA201"+ ",link_2_1.LSDTOBSO as LSDTOBSO201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on TAR_IFFE.TACODLIS=link_2_1.LSCODLIS"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and TAR_IFFE.TACODLIS=link_2_1.LSCODLIS(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=TAAUTORI
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PRA_ENTI_IDX,3]
    i_lTable = "PRA_ENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PRA_ENTI_IDX,2], .t., this.PRA_ENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PRA_ENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TAAUTORI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPR_AEP',True,'PRA_ENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" EPCODICE like "+cp_ToStrODBC(trim(this.w_TAAUTORI)+"%");

          i_ret=cp_SQL(i_nConn,"select EPCODICE,EPDESCRI,EP__TIPO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by EPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'EPCODICE',trim(this.w_TAAUTORI))
          select EPCODICE,EPDESCRI,EP__TIPO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by EPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TAAUTORI)==trim(_Link_.EPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" EPDESCRI like "+cp_ToStrODBC(trim(this.w_TAAUTORI)+"%");

            i_ret=cp_SQL(i_nConn,"select EPCODICE,EPDESCRI,EP__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" EPDESCRI like "+cp_ToStr(trim(this.w_TAAUTORI)+"%");

            select EPCODICE,EPDESCRI,EP__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_TAAUTORI) and !this.bDontReportError
            deferred_cp_zoom('PRA_ENTI','*','EPCODICE',cp_AbsName(oSource.parent,'oTAAUTORI_2_5'),i_cWhere,'GSPR_AEP',"Enti/uffici giudiziari",'GSAL_ENP.PRA_ENTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EPCODICE,EPDESCRI,EP__TIPO";
                     +" from "+i_cTable+" "+i_lTable+" where EPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'EPCODICE',oSource.xKey(1))
            select EPCODICE,EPDESCRI,EP__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TAAUTORI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EPCODICE,EPDESCRI,EP__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where EPCODICE="+cp_ToStrODBC(this.w_TAAUTORI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'EPCODICE',this.w_TAAUTORI)
            select EPCODICE,EPDESCRI,EP__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TAAUTORI = NVL(_Link_.EPCODICE,space(10))
      this.w_DESAUT = NVL(_Link_.EPDESCRI,space(60))
      this.w_EPTIPO = NVL(_Link_.EP__TIPO,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_TAAUTORI = space(10)
      endif
      this.w_DESAUT = space(60)
      this.w_EPTIPO = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_PRESTA='I' AND .w_EPTIPO='C') OR (.w_PRESTA='E' AND .w_EPTIPO='P') OR NOT(.w_PRESTA $ 'IE')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Autorit� non definita o non congruente per tipologia (Civile/Penale)")
        endif
        this.w_TAAUTORI = space(10)
        this.w_DESAUT = space(60)
        this.w_EPTIPO = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PRA_ENTI_IDX,2])+'\'+cp_ToStr(_Link_.EPCODICE,1)
      cp_ShowWarn(i_cKey,this.PRA_ENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TAAUTORI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PRA_ENTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PRA_ENTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_5.EPCODICE as EPCODICE205"+ ",link_2_5.EPDESCRI as EPDESCRI205"+ ",link_2_5.EP__TIPO as EP__TIPO205"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_5 on TAR_IFFE.TAAUTORI=link_2_5.EPCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_5"
          i_cKey=i_cKey+'+" and TAR_IFFE.TAAUTORI=link_2_5.EPCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=TARESPON
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TARESPON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BDZ',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_TARESPON)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoPersona);

          i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPTIPRIS,DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPTIPRIS',this.w_TipoPersona;
                     ,'DPCODICE',trim(this.w_TARESPON))
          select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPTIPRIS,DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TARESPON)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStrODBC(trim(this.w_TARESPON)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoPersona);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStr(trim(this.w_TARESPON)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_TipoPersona);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStrODBC(trim(this.w_TARESPON)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoPersona);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStr(trim(this.w_TARESPON)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_TipoPersona);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_TARESPON) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(oSource.parent,'oTARESPON_2_7'),i_cWhere,'GSAR_BDZ',"Responsabili",'GSARPADP.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TipoPersona<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoPersona);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',oSource.xKey(1);
                       ,'DPCODICE',oSource.xKey(2))
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TARESPON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_TARESPON);
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoPersona);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',this.w_TipoPersona;
                       ,'DPCODICE',this.w_TARESPON)
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TARESPON = NVL(_Link_.DPCODICE,space(5))
      this.w_COGNOME = NVL(_Link_.DPCOGNOM,space(40))
      this.w_NOME = NVL(_Link_.DPNOME,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.DPDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_TARESPON = space(5)
      endif
      this.w_COGNOME = space(40)
      this.w_NOME = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OB_TEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto")
        endif
        this.w_TARESPON = space(5)
        this.w_COGNOME = space(40)
        this.w_NOME = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPTIPRIS,1)+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TARESPON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TAMANSION
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MANSIONI_IDX,3]
    i_lTable = "MANSIONI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MANSIONI_IDX,2], .t., this.MANSIONI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MANSIONI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TAMANSION) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMN',True,'MANSIONI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MNCODICE like "+cp_ToStrODBC(trim(this.w_TAMANSION)+"%");

          i_ret=cp_SQL(i_nConn,"select MNCODICE,MNDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MNCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MNCODICE',trim(this.w_TAMANSION))
          select MNCODICE,MNDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MNCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TAMANSION)==trim(_Link_.MNCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TAMANSION) and !this.bDontReportError
            deferred_cp_zoom('MANSIONI','*','MNCODICE',cp_AbsName(oSource.parent,'oTAMANSION_2_8'),i_cWhere,'GSAR_AMN',"Mansioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MNCODICE,MNDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MNCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MNCODICE',oSource.xKey(1))
            select MNCODICE,MNDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TAMANSION)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MNCODICE,MNDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MNCODICE="+cp_ToStrODBC(this.w_TAMANSION);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MNCODICE',this.w_TAMANSION)
            select MNCODICE,MNDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TAMANSION = NVL(_Link_.MNCODICE,space(5))
      this.w_DESMANS = NVL(_Link_.MNDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_TAMANSION = space(5)
      endif
      this.w_DESMANS = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MANSIONI_IDX,2])+'\'+cp_ToStr(_Link_.MNCODICE,1)
      cp_ShowWarn(i_cKey,this.MANSIONI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TAMANSION Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_8(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MANSIONI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MANSIONI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_8.MNCODICE as MNCODICE208"+ ",link_2_8.MNDESCRI as MNDESCRI208"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_8 on TAR_IFFE.TAMANSION=link_2_8.MNCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_8"
          i_cKey=i_cKey+'+" and TAR_IFFE.TAMANSION=link_2_8.MNCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=VALLIS
  func Link_2_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALLIS)
            select VACODVAL,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALLIS = NVL(_Link_.VACODVAL,space(3))
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_VALLIS = space(3)
      endif
      this.w_SIMVAL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oDESLIS_2_10.value==this.w_DESLIS)
      this.oPgFrm.Page1.oPag.oDESLIS_2_10.value=this.w_DESLIS
      replace t_DESLIS with this.oPgFrm.Page1.oPag.oDESLIS_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESAUT_2_16.value==this.w_DESAUT)
      this.oPgFrm.Page1.oPag.oDESAUT_2_16.value=this.w_DESAUT
      replace t_DESAUT with this.oPgFrm.Page1.oPag.oDESAUT_2_16.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDENOM_RES_2_19.value==this.w_DENOM_RES)
      this.oPgFrm.Page1.oPag.oDENOM_RES_2_19.value=this.w_DENOM_RES
      replace t_DENOM_RES with this.oPgFrm.Page1.oPag.oDENOM_RES_2_19.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMANS_2_20.value==this.w_DESMANS)
      this.oPgFrm.Page1.oPag.oDESMANS_2_20.value=this.w_DESMANS
      replace t_DESMANS with this.oPgFrm.Page1.oPag.oDESMANS_2_20.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTACODLIS_2_1.value==this.w_TACODLIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTACODLIS_2_1.value=this.w_TACODLIS
      replace t_TACODLIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTACODLIS_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTADATATT_2_2.value==this.w_TADATATT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTADATATT_2_2.value=this.w_TADATATT
      replace t_TADATATT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTADATATT_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTADATDIS_2_3.value==this.w_TADATDIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTADATDIS_2_3.value=this.w_TADATDIS
      replace t_TADATDIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTADATDIS_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTAAUTORI_2_5.value==this.w_TAAUTORI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTAAUTORI_2_5.value=this.w_TAAUTORI
      replace t_TAAUTORI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTAAUTORI_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTAGAZUFF_2_6.value==this.w_TAGAZUFF)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTAGAZUFF_2_6.value=this.w_TAGAZUFF
      replace t_TAGAZUFF with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTAGAZUFF_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTARESPON_2_7.value==this.w_TARESPON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTARESPON_2_7.value=this.w_TARESPON
      replace t_TARESPON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTARESPON_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTAMANSION_2_8.value==this.w_TAMANSION)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTAMANSION_2_8.value=this.w_TAMANSION
      replace t_TAMANSION with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTAMANSION_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSIMVAL_2_14.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSIMVAL_2_14.value=this.w_SIMVAL
      replace t_SIMVAL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSIMVAL_2_14.value
    endif
    cp_SetControlsValueExtFlds(this,'TAR_IFFE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   empty(.w_TADATATT) and (not(Empty(.w_TACODLIS)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTADATATT_2_2
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
        case   (empty(.w_TADATDIS) or not(.w_TADATDIS>=.w_TADATATT)) and (not(Empty(.w_TACODLIS)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTADATDIS_2_3
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("La data di fine non deve essere minore della data inizio")
        case   not((.w_PRESTA='I' AND .w_EPTIPO='C') OR (.w_PRESTA='E' AND .w_EPTIPO='P') OR NOT(.w_PRESTA $ 'IE')) and not(empty(.w_TAAUTORI)) and (not(Empty(.w_TACODLIS)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTAAUTORI_2_5
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Autorit� non definita o non congruente per tipologia (Civile/Penale)")
        case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OB_TEST) and not(empty(.w_TARESPON)) and (not(Empty(.w_TACODLIS)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTARESPON_2_7
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice inesistente o obsoleto")
      endcase
      i_bRes = i_bRes .and. .gsma_mdi.CheckForm()
      if not(Empty(.w_TACODLIS))
        * --- Area Manuale = Check Row
        * --- gsma_mtt
        * --- Controlli di riga
        IF i_bRes=.t.
           .w_RESCHK=0
           Ah_Msg('Controlli di riga...',.T.)
           IF Empty(.w_TAAUTORI) and .w_PRESTA $ 'IE'
              ah_ErrorMsg('Ente/ufficio giudiziario obbligatorio nel caso di prestazione con tipologia onorario civile o penale',,'')
             .w_RESCHK=1
           ENDIF
           WAIT CLEAR
           IF .w_RESCHK<>0
             i_bRes=.f.
           ENDIF
        ENDIF
        IF i_bRes=.t.
           IF upper(.cFunction)# 'QUERY'
              * --- Controllo movimentazione tariffe avvocati
              * -- Per lanciare la routine non si deve notificare un evento poich� la NotifyEvent lancerebbe la SetControlsValue,
              * -- la quale valorizzerebbe erroneamente le variabili della nuova riga con quelle della riga precedente.
              .w_RESCHK = GSAL_BCT(this)
              if .w_RESCHK<>0
                i_bRes=.f.
              endif
            ENDIF
        ENDIF
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TACODLIS = this.w_TACODLIS
    this.o_TARESPON = this.w_TARESPON
    * --- gsma_mdi : Depends On
    this.gsma_mdi.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_TACODLIS)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_TACODLIS=space(5)
      .w_TADATATT=ctod("  /  /  ")
      .w_TADATDIS=ctod("  /  /  ")
      .w_PRESTA=space(1)
      .w_TAAUTORI=space(10)
      .w_TAGAZUFF=space(6)
      .w_TARESPON=space(5)
      .w_TAMANSION=space(5)
      .w_DESLIS=space(40)
      .w_VALLIS=space(3)
      .w_DTINVA=ctod("  /  /  ")
      .w_DTFINV=ctod("  /  /  ")
      .w_SIMVAL=space(5)
      .w_EPTIPO=space(10)
      .w_DESAUT=space(60)
      .w_COGNOME=space(40)
      .w_NOME=space(40)
      .w_DENOM_RES=space(39)
      .w_DESMANS=space(40)
      .w_DATOBSO=ctod("  /  /  ")
      .DoRTCalc(1,3,.f.)
        .w_TACODLIS = IIF(EMPTY(.w_TACODLIS),.w_CODLIS,.w_TACODLIS)
      .DoRTCalc(4,4,.f.)
      if not(empty(.w_TACODLIS))
        .link_2_1('Full')
      endif
        .w_TADATATT = iif(EMPTY(.w_DTINVA), .w_DATINI, .w_DTINVA)
        .w_TADATDIS = iif(EMPTY(.w_DTFINV), .w_DATFIN, .w_DTFINV)
      .DoRTCalc(7,8,.f.)
      if not(empty(.w_TAAUTORI))
        .link_2_5('Full')
      endif
      .DoRTCalc(9,10,.f.)
      if not(empty(.w_TARESPON))
        .link_2_7('Full')
      endif
        .w_TAMANSION = IIF(EMPTY(.w_TARESPON),.w_TAMANSION,'')
      .DoRTCalc(11,11,.f.)
      if not(empty(.w_TAMANSION))
        .link_2_8('Full')
      endif
      .DoRTCalc(12,13,.f.)
      if not(empty(.w_VALLIS))
        .link_2_11('Full')
      endif
      .DoRTCalc(14,25,.f.)
        .w_DENOM_RES = alltrim(.w_COGNOME)+space(1)+alltrim(.w_NOME)
    endwith
    this.DoRTCalc(27,30,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_TACODLIS = t_TACODLIS
    this.w_TADATATT = t_TADATATT
    this.w_TADATDIS = t_TADATDIS
    this.w_PRESTA = t_PRESTA
    this.w_TAAUTORI = t_TAAUTORI
    this.w_TAGAZUFF = t_TAGAZUFF
    this.w_TARESPON = t_TARESPON
    this.w_TAMANSION = t_TAMANSION
    this.w_DESLIS = t_DESLIS
    this.w_VALLIS = t_VALLIS
    this.w_DTINVA = t_DTINVA
    this.w_DTFINV = t_DTFINV
    this.w_SIMVAL = t_SIMVAL
    this.w_EPTIPO = t_EPTIPO
    this.w_DESAUT = t_DESAUT
    this.w_COGNOME = t_COGNOME
    this.w_NOME = t_NOME
    this.w_DENOM_RES = t_DENOM_RES
    this.w_DESMANS = t_DESMANS
    this.w_DATOBSO = t_DATOBSO
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_TACODLIS with this.w_TACODLIS
    replace t_TADATATT with this.w_TADATATT
    replace t_TADATDIS with this.w_TADATDIS
    replace t_PRESTA with this.w_PRESTA
    replace t_TAAUTORI with this.w_TAAUTORI
    replace t_TAGAZUFF with this.w_TAGAZUFF
    replace t_TARESPON with this.w_TARESPON
    replace t_TAMANSION with this.w_TAMANSION
    replace t_DESLIS with this.w_DESLIS
    replace t_VALLIS with this.w_VALLIS
    replace t_DTINVA with this.w_DTINVA
    replace t_DTFINV with this.w_DTFINV
    replace t_SIMVAL with this.w_SIMVAL
    replace t_EPTIPO with this.w_EPTIPO
    replace t_DESAUT with this.w_DESAUT
    replace t_COGNOME with this.w_COGNOME
    replace t_NOME with this.w_NOME
    replace t_DENOM_RES with this.w_DENOM_RES
    replace t_DESMANS with this.w_DESMANS
    replace t_DATOBSO with this.w_DATOBSO
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsma_mttPag1 as StdContainer
  Width  = 649
  height = 529
  stdWidth  = 649
  stdheight = 529
  resizeYpos=150
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_11 as StdButton with uid="JCVLIDPBWQ",left=594, top=172, width=48,height=45,;
    CpPicture="COPY.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per duplicare la tariffa selezionata";
    , HelpContextID = 87621174;
    , tabstop=.f., Caption='\<Duplica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      do GSAL_KDT with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_11.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction#"Query" OR (.cFunction="Query" AND EMPTY(.w_TACODART)))
    endwith
   endif
  endfunc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=11, top=0, width=579,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=8,Field1="TACODLIS",Label1="Listino",Field2="TADATATT",Label2="Valido dal",Field3="TADATDIS",Label3="Fino al",Field4="SIMVAL",Label4="Valuta",Field5="TAAUTORI",Label5="Ente/Uff. Giudiz.",Field6="TAGAZUFF",Label6="G.U.",Field7="TARESPON",Label7="Resp.",Field8="TAMANSION",Label8="Mansione",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 235712122

  add object oStr_1_4 as StdString with uid="GTBNZCXVXW",Visible=.t., Left=4, Top=159,;
    Alignment=1, Width=57, Height=18,;
    Caption="Listino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="YDKFFFFTMB",Visible=.t., Left=284, Top=159,;
    Alignment=1, Width=89, Height=18,;
    Caption="Ente/Uff. Giudiz.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="CHXWMJVMVC",Visible=.t., Left=4, Top=183,;
    Alignment=1, Width=57, Height=18,;
    Caption="Resp.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="OJBWDENMIT",Visible=.t., Left=284, Top=183,;
    Alignment=1, Width=89, Height=18,;
    Caption="Mansione:"  ;
  , bGlobalFont=.t.
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsma_mdi",lower(this.oContained.gsma_mdi.class))=0
        this.oContained.gsma_mdi.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=1,top=19,;
    width=575+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*7*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=2,top=20,width=574+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*7*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='LISTINI|PRA_ENTI|DIPENDEN|MANSIONI|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDESLIS_2_10.Refresh()
      this.Parent.oDESAUT_2_16.Refresh()
      this.Parent.oDENOM_RES_2_19.Refresh()
      this.Parent.oDESMANS_2_20.Refresh()
      this.Parent.oContained.ChildrenChangeRow()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='LISTINI'
        oDropInto=this.oBodyCol.oRow.oTACODLIS_2_1
      case cFile='PRA_ENTI'
        oDropInto=this.oBodyCol.oRow.oTAAUTORI_2_5
      case cFile='DIPENDEN'
        oDropInto=this.oBodyCol.oRow.oTARESPON_2_7
      case cFile='MANSIONI'
        oDropInto=this.oBodyCol.oRow.oTAMANSION_2_8
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oLinkPC_2_9 as stdDynamicChildContainer with uid="NLQCIAEJJG",bOnScreen=.t.,width=645,height=306,;
   left=0, top=221;


  add object oDESLIS_2_10 as StdTrsField with uid="EDRITDNDSF",rtseq=12,rtrep=.t.,;
    cFormVar="w_DESLIS",value=space(40),enabled=.f.,;
    HelpContextID = 51838518,;
    cTotal="", bFixedPos=.t., cQueryName = "DESLIS",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=9, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=215, Left=63, Top=158, InputMask=replicate('X',40)

  add object oDESAUT_2_16 as StdTrsField with uid="ZWCRGVIOJF",rtseq=21,rtrep=.t.,;
    cFormVar="w_DESAUT",value=space(60),enabled=.f.,;
    HelpContextID = 80477750,;
    cTotal="", bFixedPos=.t., cQueryName = "DESAUT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=9, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=215, Left=374, Top=158, InputMask=replicate('X',60)

  add object oDENOM_RES_2_19 as StdTrsField with uid="QQBATUKLEW",rtseq=26,rtrep=.t.,;
    cFormVar="w_DENOM_RES",value=space(39),enabled=.f.,;
    HelpContextID = 257536939,;
    cTotal="", bFixedPos=.t., cQueryName = "DENOM_RES",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=215, Left=63, Top=182, InputMask=replicate('X',39)

  add object oDESMANS_2_20 as StdTrsField with uid="HWDLHXWUIW",rtseq=27,rtrep=.t.,;
    cFormVar="w_DESMANS",value=space(40),enabled=.f.,;
    HelpContextID = 228064822,;
    cTotal="", bFixedPos=.t., cQueryName = "DESMANS",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=215, Left=374, Top=182, InputMask=replicate('X',40)

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsma_mttBodyRow as CPBodyRowCnt
  Width=565
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oTACODLIS_2_1 as StdTrsField with uid="NASKDAPKRA",rtseq=4,rtrep=.t.,;
    cFormVar="w_TACODLIS",value=space(5),;
    ToolTipText = "Codice listino",;
    HelpContextID = 70714487,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=62, Left=-2, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_TACODLIS"

  func oTACODLIS_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oTACODLIS_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oTACODLIS_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oTACODLIS_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"Listini",'listob1.LISTINI_VZM',this.parent.oContained
  endproc
  proc oTACODLIS_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LSCODLIS=this.parent.oContained.w_TACODLIS
    i_obj.ecpSave()
  endproc

  add object oTADATATT_2_2 as StdTrsField with uid="HSOWXGUWGD",rtseq=5,rtrep=.t.,;
    cFormVar="w_TADATATT",value=ctod("  /  /  "),;
    ToolTipText = "Data inizio",;
    HelpContextID = 29035402,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=76, Left=63, Top=0, TabStop=.f.

  add object oTADATDIS_2_3 as StdTrsField with uid="AMANWHXMHS",rtseq=6,rtrep=.t.,;
    cFormVar="w_TADATDIS",value=ctod("  /  /  "),;
    ToolTipText = "Data fine",;
    HelpContextID = 189068407,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "La data di fine non deve essere minore della data inizio",;
   bGlobalFont=.t.,;
    Height=17, Width=76, Left=142, Top=0, TabStop=.f.

  func oTADATDIS_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_TADATDIS>=.w_TADATATT)
    endwith
    return bRes
  endfunc

  add object oTAAUTORI_2_5 as StdTrsField with uid="NWUNPZBVFQ",rtseq=8,rtrep=.t.,;
    cFormVar="w_TAAUTORI",value=space(10),;
    ToolTipText = "Autorit�",;
    HelpContextID = 265214847,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Autorit� non definita o non congruente per tipologia (Civile/Penale)",;
   bGlobalFont=.t.,;
    Height=17, Width=97, Left=275, Top=0, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="PRA_ENTI", cZoomOnZoom="GSPR_AEP", oKey_1_1="EPCODICE", oKey_1_2="this.w_TAAUTORI"

  func oTAAUTORI_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oTAAUTORI_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oTAAUTORI_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PRA_ENTI','*','EPCODICE',cp_AbsName(this.parent,'oTAAUTORI_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPR_AEP',"Enti/uffici giudiziari",'GSAL_ENP.PRA_ENTI_VZM',this.parent.oContained
  endproc
  proc oTAAUTORI_2_5.mZoomOnZoom
    local i_obj
    i_obj=GSPR_AEP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_EPCODICE=this.parent.oContained.w_TAAUTORI
    i_obj.ecpSave()
  endproc

  add object oTAGAZUFF_2_6 as StdTrsField with uid="FESOUKPKQR",rtseq=9,rtrep=.t.,;
    cFormVar="w_TAGAZUFF",value=space(6),;
    ToolTipText = "Gazzetta ufficiale",;
    HelpContextID = 102447996,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=375, Top=0, InputMask=replicate('X',6)

  add object oTARESPON_2_7 as StdTrsField with uid="NLGFRTBCPJ",rtseq=10,rtrep=.t.,;
    cFormVar="w_TARESPON",value=space(5),nZero=5,;
    ToolTipText = "Responsabile",;
    HelpContextID = 11529092,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente o obsoleto",;
   bGlobalFont=.t.,;
    Height=17, Width=62, Left=433, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", cZoomOnZoom="GSAR_BDZ", oKey_1_1="DPTIPRIS", oKey_1_2="this.w_TipoPersona", oKey_2_1="DPCODICE", oKey_2_2="this.w_TARESPON"

  func oTARESPON_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oTARESPON_2_7.ecpDrop(oSource)
    this.Parent.oContained.link_2_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oTARESPON_2_7.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DIPENDEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStrODBC(this.Parent.oContained.w_TipoPersona)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStr(this.Parent.oContained.w_TipoPersona)
    endif
    do cp_zoom with 'DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(this.parent,'oTARESPON_2_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BDZ',"Responsabili",'GSARPADP.DIPENDEN_VZM',this.parent.oContained
  endproc
  proc oTARESPON_2_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BDZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.DPTIPRIS=w_TipoPersona
     i_obj.w_DPCODICE=this.parent.oContained.w_TARESPON
    i_obj.ecpSave()
  endproc

  add object oTAMANSION_2_8 as StdTrsField with uid="EJNZDTUCLN",rtseq=11,rtrep=.t.,;
    cFormVar="w_TAMANSION",value=space(5),;
    ToolTipText = "Mansione",;
    HelpContextID = 212098971,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=62, Left=498, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MANSIONI", cZoomOnZoom="GSAR_AMN", oKey_1_1="MNCODICE", oKey_1_2="this.w_TAMANSION"

  func oTAMANSION_2_8.mCond()
    with this.Parent.oContained
      return (EMPTY(.w_TARESPON))
    endwith
  endfunc

  func oTAMANSION_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oTAMANSION_2_8.ecpDrop(oSource)
    this.Parent.oContained.link_2_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oTAMANSION_2_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MANSIONI','*','MNCODICE',cp_AbsName(this.parent,'oTAMANSION_2_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMN',"Mansioni",'',this.parent.oContained
  endproc
  proc oTAMANSION_2_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MNCODICE=this.parent.oContained.w_TAMANSION
    i_obj.ecpSave()
  endproc

  add object oSIMVAL_2_14 as StdTrsField with uid="IKRFZQKZAA",rtseq=16,rtrep=.t.,;
    cFormVar="w_SIMVAL",value=space(5),enabled=.f.,;
    ToolTipText = "Valuta",;
    HelpContextID = 195076902,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=51, Left=221, Top=0, InputMask=replicate('X',5)
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oTACODLIS_2_1.When()
    return(.t.)
  proc oTACODLIS_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oTACODLIS_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=6
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_mtt','TAR_IFFE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".TACODART=TAR_IFFE.TACODART";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
