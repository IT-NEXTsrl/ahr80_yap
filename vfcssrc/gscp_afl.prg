* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscp_afl                                                        *
*              Folders portale                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_20]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-04-01                                                      *
* Last revis.: 2005-09-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscp_afl"))

* --- Class definition
define class tgscp_afl as StdForm
  Top    = 55
  Left   = 65

  * --- Standard Properties
  Width  = 581
  Height = 101+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2005-09-19"
  HelpContextID=209087127
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  ZFOLDERS_IDX = 0
  cFile = "ZFOLDERS"
  cKeySelect = "FOSERIAL"
  cKeyWhere  = "FOSERIAL=this.w_FOSERIAL"
  cKeyWhereODBC = '"FOSERIAL="+cp_ToStrODBC(this.w_FOSERIAL)';

  cKeyWhereODBCqualified = '"ZFOLDERS.FOSERIAL="+cp_ToStrODBC(this.w_FOSERIAL)';

  cPrg = "gscp_afl"
  cComment = "Folders portale"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_FOSERIAL = 0
  w_FODESCRI = space(50)
  w_FO__TIPO = space(1)
  w_FO__PATH = space(250)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_FOSERIAL = this.W_FOSERIAL
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'ZFOLDERS','gscp_afl')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscp_aflPag1","gscp_afl",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Folders")
      .Pages(1).HelpContextID = 181297578
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFOSERIAL_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='ZFOLDERS'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ZFOLDERS_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ZFOLDERS_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_FOSERIAL = NVL(FOSERIAL,0)
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from ZFOLDERS where FOSERIAL=KeySet.FOSERIAL
    *
    i_nConn = i_TableProp[this.ZFOLDERS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ZFOLDERS_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ZFOLDERS')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ZFOLDERS.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ZFOLDERS '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'FOSERIAL',this.w_FOSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_FOSERIAL = NVL(FOSERIAL,0)
        .op_FOSERIAL = .w_FOSERIAL
        .w_FODESCRI = NVL(FODESCRI,space(50))
        .w_FO__TIPO = NVL(FO__TIPO,space(1))
        .w_FO__PATH = NVL(FO__PATH,space(250))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'ZFOLDERS')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_FOSERIAL = 0
      .w_FODESCRI = space(50)
      .w_FO__TIPO = space(1)
      .w_FO__PATH = space(250)
      if .cFunction<>"Filter"
          .DoRTCalc(1,2,.f.)
        .w_FO__TIPO = "S"
        .w_FO__PATH = '/'
      endif
    endwith
    cp_BlankRecExtFlds(this,'ZFOLDERS')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ZFOLDERS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ZFOLDERS_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"ZSFOLDER","i_codazi,w_FOSERIAL")
      .op_codazi = .w_codazi
      .op_FOSERIAL = .w_FOSERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oFOSERIAL_1_1.enabled = i_bVal
      .Page1.oPag.oFODESCRI_1_3.enabled = i_bVal
      .Page1.oPag.oFO__TIPO_1_4.enabled = i_bVal
      .Page1.oPag.oFO__PATH_1_5.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oFOSERIAL_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oFOSERIAL_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'ZFOLDERS',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ZFOLDERS_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FOSERIAL,"FOSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FODESCRI,"FODESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FO__TIPO,"FO__TIPO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FO__PATH,"FO__PATH",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ZFOLDERS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ZFOLDERS_IDX,2])
    i_lTable = "ZFOLDERS"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.ZFOLDERS_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ZFOLDERS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ZFOLDERS_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.ZFOLDERS_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"ZSFOLDER","i_codazi,w_FOSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into ZFOLDERS
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ZFOLDERS')
        i_extval=cp_InsertValODBCExtFlds(this,'ZFOLDERS')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(FOSERIAL,FODESCRI,FO__TIPO,FO__PATH "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_FOSERIAL)+;
                  ","+cp_ToStrODBC(this.w_FODESCRI)+;
                  ","+cp_ToStrODBC(this.w_FO__TIPO)+;
                  ","+cp_ToStrODBC(this.w_FO__PATH)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ZFOLDERS')
        i_extval=cp_InsertValVFPExtFlds(this,'ZFOLDERS')
        cp_CheckDeletedKey(i_cTable,0,'FOSERIAL',this.w_FOSERIAL)
        INSERT INTO (i_cTable);
              (FOSERIAL,FODESCRI,FO__TIPO,FO__PATH  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_FOSERIAL;
                  ,this.w_FODESCRI;
                  ,this.w_FO__TIPO;
                  ,this.w_FO__PATH;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.ZFOLDERS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ZFOLDERS_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.ZFOLDERS_IDX,i_nConn)
      *
      * update ZFOLDERS
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'ZFOLDERS')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " FODESCRI="+cp_ToStrODBC(this.w_FODESCRI)+;
             ",FO__TIPO="+cp_ToStrODBC(this.w_FO__TIPO)+;
             ",FO__PATH="+cp_ToStrODBC(this.w_FO__PATH)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'ZFOLDERS')
        i_cWhere = cp_PKFox(i_cTable  ,'FOSERIAL',this.w_FOSERIAL  )
        UPDATE (i_cTable) SET;
              FODESCRI=this.w_FODESCRI;
             ,FO__TIPO=this.w_FO__TIPO;
             ,FO__PATH=this.w_FO__PATH;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ZFOLDERS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ZFOLDERS_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.ZFOLDERS_IDX,i_nConn)
      *
      * delete ZFOLDERS
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'FOSERIAL',this.w_FOSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ZFOLDERS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ZFOLDERS_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"ZSFOLDER","i_codazi,w_FOSERIAL")
          .op_FOSERIAL = .w_FOSERIAL
        endif
        .op_codazi = .w_codazi
      endwith
      this.DoRTCalc(1,4,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFOSERIAL_1_1.value==this.w_FOSERIAL)
      this.oPgFrm.Page1.oPag.oFOSERIAL_1_1.value=this.w_FOSERIAL
    endif
    if not(this.oPgFrm.Page1.oPag.oFODESCRI_1_3.value==this.w_FODESCRI)
      this.oPgFrm.Page1.oPag.oFODESCRI_1_3.value=this.w_FODESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oFO__TIPO_1_4.RadioValue()==this.w_FO__TIPO)
      this.oPgFrm.Page1.oPag.oFO__TIPO_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFO__PATH_1_5.value==this.w_FO__PATH)
      this.oPgFrm.Page1.oPag.oFO__PATH_1_5.value=this.w_FO__PATH
    endif
    cp_SetControlsValueExtFlds(this,'ZFOLDERS')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(EMPTY(.w_FO__PATH) OR (.w_FO__TIPO='C' OR (.w_FO__TIPO='S' AND NOT( '%' $ .w_FO__PATH))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFO__TIPO_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impossibile impostareil tipo folder a common se nel path web folder sono presenti uno o pi� parametri")
          case   not(.w_FO__TIPO='C' OR (.w_FO__TIPO='S' AND NOT('%' $ .w_FO__PATH)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFO__PATH_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("I percorsi con parametri sono utilizzabili esclusivamente nelle folder di tipo company")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgscp_aflPag1 as StdContainer
  Width  = 577
  height = 101
  stdWidth  = 577
  stdheight = 101
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oFOSERIAL_1_1 as StdField with uid="LOJMBUPBDS",rtseq=1,rtrep=.f.,;
    cFormVar = "w_FOSERIAL", cQueryName = "FOSERIAL",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice folder",;
    HelpContextID = 85922142,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=84, Left=122, Top=15, cSayPict='"@Z 9999999999"', cGetPict='"9999999999"'

  add object oFODESCRI_1_3 as StdField with uid="LDARSNINOT",rtseq=2,rtrep=.f.,;
    cFormVar = "w_FODESCRI", cQueryName = "FODESCRI",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione folder",;
    HelpContextID = 185598305,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=211, Top=15, InputMask=replicate('X',50)


  add object oFO__TIPO_1_4 as StdCombo with uid="ZMWWTDIDEB",rtseq=3,rtrep=.f.,left=122,top=44,width=138,height=21;
    , ToolTipText = "Tipo folder";
    , HelpContextID = 186363557;
    , cFormVar="w_FO__TIPO",RowSource=""+"Common folder,"+"Company folder", bObbl = .f. , nPag = 1;
    , sErrorMsg = "Impossibile impostareil tipo folder a common se nel path web folder sono presenti uno o pi� parametri";
  , bGlobalFont=.t.


  func oFO__TIPO_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'C',;
    space(1))))
  endfunc
  func oFO__TIPO_1_4.GetRadio()
    this.Parent.oContained.w_FO__TIPO = this.RadioValue()
    return .t.
  endfunc

  func oFO__TIPO_1_4.SetRadio()
    this.Parent.oContained.w_FO__TIPO=trim(this.Parent.oContained.w_FO__TIPO)
    this.value = ;
      iif(this.Parent.oContained.w_FO__TIPO=='S',1,;
      iif(this.Parent.oContained.w_FO__TIPO=='C',2,;
      0))
  endfunc

  func oFO__TIPO_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_FO__PATH) OR (.w_FO__TIPO='C' OR (.w_FO__TIPO='S' AND NOT( '%' $ .w_FO__PATH))))
    endwith
    return bRes
  endfunc

  add object oFO__PATH_1_5 as StdField with uid="VBBZJWPBUT",rtseq=4,rtrep=.f.,;
    cFormVar = "w_FO__PATH", cQueryName = "FO__PATH",;
    bObbl = .f. , nPag = 1, value=space(250), bMultilanguage =  .f.,;
    sErrorMsg = "I percorsi con parametri sono utilizzabili esclusivamente nelle folder di tipo company",;
    ToolTipText = "Path webfolder su Corporate Portal, deve iniziare sempre con il carattere /, esempio: '/stampe ad hoc'",;
    HelpContextID = 220483938,;
   bGlobalFont=.t.,;
    Height=21, Width=452, Left=122, Top=76, InputMask=replicate('X',250)

  func oFO__PATH_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_FO__TIPO='C' OR (.w_FO__TIPO='S' AND NOT('%' $ .w_FO__PATH)))
    endwith
    return bRes
  endfunc

  add object oStr_1_2 as StdString with uid="COHBXLJZKN",Visible=.t., Left=9, Top=15,;
    Alignment=1, Width=111, Height=18,;
    Caption="Webfolder portale:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_6 as StdString with uid="WBVXWMWJTM",Visible=.t., Left=29, Top=79,;
    Alignment=1, Width=91, Height=18,;
    Caption="Path webfolder:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="CZHNLPJKRJ",Visible=.t., Left=40, Top=44,;
    Alignment=1, Width=80, Height=18,;
    Caption="Tipo folder:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscp_afl','ZFOLDERS','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".FOSERIAL=ZFOLDERS.FOSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
