* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_btd                                                        *
*              Controlli causali documenti                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_6]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-01-25                                                      *
* Last revis.: 2002-01-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_btd",oParentObject,m.pOper)
return(i_retval)

define class tgsve_btd as StdBatch
  * --- Local variables
  pOper = space(1)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli al cambio riferimetri causale documenti (da GSVE_ATD)
    do case
      case this.pOper="I"
        * --- Cambio Intestatario
        if g_ACQU="S"
          this.oParentObject.w_TDCATDOC = IIF(this.oParentObject.w_TDFLINTE="C", this.oParentObject.w_TDCATDOC, "DI")
        else
          this.oParentObject.w_TDCATDOC = IIF(this.oParentObject.w_TDFLINTE="C", this.oParentObject.w_TDCATDOC, IIF(this.oParentObject.w_TDFLINTE="F" AND this.oParentObject.w_TDCATDOC = "DT", "DT", "DI"))
        endif
      case this.pOper="T"
        * --- Cambio Tipo Documento
        if g_ACQU="S"
          this.oParentObject.w_TDFLINTE = IIF(this.oParentObject.w_TDCATDOC="DI", this.oParentObject.w_TDFLINTE, "C")
        else
          this.oParentObject.w_TDFLINTE = IIF(this.oParentObject.w_TDCATDOC = "DI" OR (this.oParentObject.w_TDCATDOC="DT" AND this.oParentObject.w_TDFLINTE="F"), this.oParentObject.w_TDFLINTE, "C")
        endif
    endcase
    this.oParentObject.w_TDFLVEAC = IIF(this.oParentObject.w_TDFLINTE="F", "A", "V")
    this.oParentObject.GSVE_MDC.mCalc(.T.)
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
