* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_aap                                                        *
*              Localit�                                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_26]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-13                                                      *
* Last revis.: 2011-01-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_aap"))

* --- Class definition
define class tgsar_aap as StdForm
  Top    = 17
  Left   = 58

  * --- Standard Properties
  Width  = 495
  Height = 229+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-01-31"
  HelpContextID=125208727
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Constant Properties
  ANAG_CAP_IDX = 0
  cFile = "ANAG_CAP"
  cKeySelect = "CPCODCAP,CPCODLOC"
  cKeyWhere  = "CPCODCAP=this.w_CPCODCAP and CPCODLOC=this.w_CPCODLOC"
  cKeyWhereODBC = '"CPCODCAP="+cp_ToStrODBC(this.w_CPCODCAP)';
      +'+" and CPCODLOC="+cp_ToStrODBC(this.w_CPCODLOC)';

  cKeyWhereODBCqualified = '"ANAG_CAP.CPCODCAP="+cp_ToStrODBC(this.w_CPCODCAP)';
      +'+" and ANAG_CAP.CPCODLOC="+cp_ToStrODBC(this.w_CPCODLOC)';

  cPrg = "gsar_aap"
  cComment = "Localit�"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CPCODCAP = space(8)
  w_CPCODLOC = space(10)
  w_CPDESLOC = space(50)
  w_CPDESFRA = space(50)
  w_CPDESIND = space(154)
  w_CPCODPRO = space(2)
  w_CPDESPRO = space(30)
  w_CPCODFIS = space(4)
  w_CPUFFGIU = space(1)

  * --- Autonumbered Variables
  op_CPCODLOC = this.W_CPCODLOC
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'ANAG_CAP','gsar_aap')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_aapPag1","gsar_aap",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Localit�")
      .Pages(1).HelpContextID = 127806870
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCPCODCAP_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='ANAG_CAP'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ANAG_CAP_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ANAG_CAP_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_CPCODCAP = NVL(CPCODCAP,space(8))
      .w_CPCODLOC = NVL(CPCODLOC,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from ANAG_CAP where CPCODCAP=KeySet.CPCODCAP
    *                            and CPCODLOC=KeySet.CPCODLOC
    *
    i_nConn = i_TableProp[this.ANAG_CAP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANAG_CAP_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ANAG_CAP')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ANAG_CAP.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ANAG_CAP '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CPCODCAP',this.w_CPCODCAP  ,'CPCODLOC',this.w_CPCODLOC  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CPCODCAP = NVL(CPCODCAP,space(8))
        .w_CPCODLOC = NVL(CPCODLOC,space(10))
        .op_CPCODLOC = .w_CPCODLOC
        .w_CPDESLOC = NVL(CPDESLOC,space(50))
        .w_CPDESFRA = NVL(CPDESFRA,space(50))
        .w_CPDESIND = NVL(CPDESIND,space(154))
        .w_CPCODPRO = NVL(CPCODPRO,space(2))
        .w_CPDESPRO = NVL(CPDESPRO,space(30))
        .w_CPCODFIS = NVL(CPCODFIS,space(4))
        .w_CPUFFGIU = NVL(CPUFFGIU,space(1))
        cp_LoadRecExtFlds(this,'ANAG_CAP')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CPCODCAP = space(8)
      .w_CPCODLOC = space(10)
      .w_CPDESLOC = space(50)
      .w_CPDESFRA = space(50)
      .w_CPDESIND = space(154)
      .w_CPCODPRO = space(2)
      .w_CPDESPRO = space(30)
      .w_CPCODFIS = space(4)
      .w_CPUFFGIU = space(1)
      if .cFunction<>"Filter"
          .DoRTCalc(1,8,.f.)
        .w_CPUFFGIU = 'N'
      endif
    endwith
    cp_BlankRecExtFlds(this,'ANAG_CAP')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ANAG_CAP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANAG_CAP_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"NUCAP","w_CPCODLOC")
      .op_CPCODLOC = .w_CPCODLOC
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCPCODCAP_1_1.enabled = i_bVal
      .Page1.oPag.oCPDESLOC_1_3.enabled = i_bVal
      .Page1.oPag.oCPDESFRA_1_4.enabled = i_bVal
      .Page1.oPag.oCPDESIND_1_5.enabled = i_bVal
      .Page1.oPag.oCPCODPRO_1_6.enabled = i_bVal
      .Page1.oPag.oCPDESPRO_1_7.enabled = i_bVal
      .Page1.oPag.oCPCODFIS_1_8.enabled = i_bVal
      .Page1.oPag.oCPUFFGIU_1_9.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oCPCODCAP_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oCPCODCAP_1_1.enabled = .t.
        .Page1.oPag.oCPDESLOC_1_3.enabled = .t.
        .Page1.oPag.oCPDESFRA_1_4.enabled = .t.
        .Page1.oPag.oCPDESIND_1_5.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'ANAG_CAP',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ANAG_CAP_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CPCODCAP,"CPCODCAP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CPCODLOC,"CPCODLOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CPDESLOC,"CPDESLOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CPDESFRA,"CPDESFRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CPDESIND,"CPDESIND",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CPCODPRO,"CPCODPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CPDESPRO,"CPDESPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CPCODFIS,"CPCODFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CPUFFGIU,"CPUFFGIU",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ANAG_CAP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANAG_CAP_IDX,2])
    i_lTable = "ANAG_CAP"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.ANAG_CAP_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ANAG_CAP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANAG_CAP_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.ANAG_CAP_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"NUCAP","w_CPCODLOC")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into ANAG_CAP
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ANAG_CAP')
        i_extval=cp_InsertValODBCExtFlds(this,'ANAG_CAP')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(CPCODCAP,CPCODLOC,CPDESLOC,CPDESFRA,CPDESIND"+;
                  ",CPCODPRO,CPDESPRO,CPCODFIS,CPUFFGIU "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_CPCODCAP)+;
                  ","+cp_ToStrODBC(this.w_CPCODLOC)+;
                  ","+cp_ToStrODBC(this.w_CPDESLOC)+;
                  ","+cp_ToStrODBC(this.w_CPDESFRA)+;
                  ","+cp_ToStrODBC(this.w_CPDESIND)+;
                  ","+cp_ToStrODBC(this.w_CPCODPRO)+;
                  ","+cp_ToStrODBC(this.w_CPDESPRO)+;
                  ","+cp_ToStrODBC(this.w_CPCODFIS)+;
                  ","+cp_ToStrODBC(this.w_CPUFFGIU)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ANAG_CAP')
        i_extval=cp_InsertValVFPExtFlds(this,'ANAG_CAP')
        cp_CheckDeletedKey(i_cTable,0,'CPCODCAP',this.w_CPCODCAP,'CPCODLOC',this.w_CPCODLOC)
        INSERT INTO (i_cTable);
              (CPCODCAP,CPCODLOC,CPDESLOC,CPDESFRA,CPDESIND,CPCODPRO,CPDESPRO,CPCODFIS,CPUFFGIU  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_CPCODCAP;
                  ,this.w_CPCODLOC;
                  ,this.w_CPDESLOC;
                  ,this.w_CPDESFRA;
                  ,this.w_CPDESIND;
                  ,this.w_CPCODPRO;
                  ,this.w_CPDESPRO;
                  ,this.w_CPCODFIS;
                  ,this.w_CPUFFGIU;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.ANAG_CAP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANAG_CAP_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.ANAG_CAP_IDX,i_nConn)
      *
      * update ANAG_CAP
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'ANAG_CAP')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " CPDESLOC="+cp_ToStrODBC(this.w_CPDESLOC)+;
             ",CPDESFRA="+cp_ToStrODBC(this.w_CPDESFRA)+;
             ",CPDESIND="+cp_ToStrODBC(this.w_CPDESIND)+;
             ",CPCODPRO="+cp_ToStrODBC(this.w_CPCODPRO)+;
             ",CPDESPRO="+cp_ToStrODBC(this.w_CPDESPRO)+;
             ",CPCODFIS="+cp_ToStrODBC(this.w_CPCODFIS)+;
             ",CPUFFGIU="+cp_ToStrODBC(this.w_CPUFFGIU)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'ANAG_CAP')
        i_cWhere = cp_PKFox(i_cTable  ,'CPCODCAP',this.w_CPCODCAP  ,'CPCODLOC',this.w_CPCODLOC  )
        UPDATE (i_cTable) SET;
              CPDESLOC=this.w_CPDESLOC;
             ,CPDESFRA=this.w_CPDESFRA;
             ,CPDESIND=this.w_CPDESIND;
             ,CPCODPRO=this.w_CPCODPRO;
             ,CPDESPRO=this.w_CPDESPRO;
             ,CPCODFIS=this.w_CPCODFIS;
             ,CPUFFGIU=this.w_CPUFFGIU;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ANAG_CAP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANAG_CAP_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.ANAG_CAP_IDX,i_nConn)
      *
      * delete ANAG_CAP
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'CPCODCAP',this.w_CPCODCAP  ,'CPCODLOC',this.w_CPCODLOC  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ANAG_CAP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANAG_CAP_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,9,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCPUFFGIU_1_9.visible=!this.oPgFrm.Page1.oPag.oCPUFFGIU_1_9.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCPCODCAP_1_1.value==this.w_CPCODCAP)
      this.oPgFrm.Page1.oPag.oCPCODCAP_1_1.value=this.w_CPCODCAP
    endif
    if not(this.oPgFrm.Page1.oPag.oCPDESLOC_1_3.value==this.w_CPDESLOC)
      this.oPgFrm.Page1.oPag.oCPDESLOC_1_3.value=this.w_CPDESLOC
    endif
    if not(this.oPgFrm.Page1.oPag.oCPDESFRA_1_4.value==this.w_CPDESFRA)
      this.oPgFrm.Page1.oPag.oCPDESFRA_1_4.value=this.w_CPDESFRA
    endif
    if not(this.oPgFrm.Page1.oPag.oCPDESIND_1_5.value==this.w_CPDESIND)
      this.oPgFrm.Page1.oPag.oCPDESIND_1_5.value=this.w_CPDESIND
    endif
    if not(this.oPgFrm.Page1.oPag.oCPCODPRO_1_6.value==this.w_CPCODPRO)
      this.oPgFrm.Page1.oPag.oCPCODPRO_1_6.value=this.w_CPCODPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oCPDESPRO_1_7.value==this.w_CPDESPRO)
      this.oPgFrm.Page1.oPag.oCPDESPRO_1_7.value=this.w_CPDESPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oCPCODFIS_1_8.value==this.w_CPCODFIS)
      this.oPgFrm.Page1.oPag.oCPCODFIS_1_8.value=this.w_CPCODFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oCPUFFGIU_1_9.RadioValue()==this.w_CPUFFGIU)
      this.oPgFrm.Page1.oPag.oCPUFFGIU_1_9.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'ANAG_CAP')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CPCODCAP))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCPCODCAP_1_1.SetFocus()
            i_bnoObbl = !empty(.w_CPCODCAP)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_aapPag1 as StdContainer
  Width  = 491
  height = 230
  stdWidth  = 491
  stdheight = 230
  resizeXpos=355
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCPCODCAP_1_1 as StdField with uid="TXJPYMVITQ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CPCODCAP", cQueryName = "CPCODCAP",;
    bObbl = .t. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Codice CAP",;
    HelpContextID = 16118410,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=167, Top=18, InputMask=replicate('X',8)

  add object oCPDESLOC_1_3 as StdField with uid="KKJCHKUPZB",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CPDESLOC", cQueryName = "CPDESLOC",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione comune",;
    HelpContextID = 118481559,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=167, Top=45, InputMask=replicate('X',50)

  add object oCPDESFRA_1_4 as StdField with uid="FKSKNJMBSB",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CPDESFRA", cQueryName = "CPDESFRA",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione frazione",;
    HelpContextID = 219144857,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=167, Top=72, InputMask=replicate('X',50)

  add object oCPDESIND_1_5 as StdField with uid="DMCQGJXEHD",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CPDESIND", cQueryName = "CPDESIND",;
    bObbl = .f. , nPag = 1, value=space(154), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione indirizzo",;
    HelpContextID = 168813206,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=167, Top=99, InputMask=replicate('X',154)

  add object oCPCODPRO_1_6 as StdField with uid="FHCVTPUCQC",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CPCODPRO", cQueryName = "CPCODPRO",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia",;
    HelpContextID = 66450059,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=167, Top=126, InputMask=replicate('X',2), bHasZoom = .t. 

  proc oCPCODPRO_1_6.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P"," "," ",".w_CPCODPRO",".w_CPDESPRO")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oCPDESPRO_1_7 as StdField with uid="JCQVBDUCKT",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CPDESPRO", cQueryName = "CPDESPRO",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione provincia",;
    HelpContextID = 51372683,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=167, Top=153, InputMask=replicate('X',30)

  add object oCPCODFIS_1_8 as StdField with uid="MBGSOLKASA",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CPCODFIS", cQueryName = "CPCODFIS",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Codice comune per il calcolo del codice fiscale",;
    HelpContextID = 34213241,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=167, Top=180, cSayPict='"!!!!"', cGetPict='"!!!!"', InputMask=replicate('X',4)

  add object oCPUFFGIU_1_9 as StdCheck with uid="TLIZRZQIYL",rtseq=9,rtrep=.f.,left=167, top=207, caption="Sede di ufficio giudiziario",;
    ToolTipText = "Se attivo, la localit� � sede di un ufficio giudiziario",;
    HelpContextID = 52571515,;
    cFormVar="w_CPUFFGIU", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCPUFFGIU_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCPUFFGIU_1_9.GetRadio()
    this.Parent.oContained.w_CPUFFGIU = this.RadioValue()
    return .t.
  endfunc

  func oCPUFFGIU_1_9.SetRadio()
    this.Parent.oContained.w_CPUFFGIU=trim(this.Parent.oContained.w_CPUFFGIU)
    this.value = ;
      iif(this.Parent.oContained.w_CPUFFGIU=='S',1,;
      0)
  endfunc

  func oCPUFFGIU_1_9.mHide()
    with this.Parent.oContained
      return (!isAlt())
    endwith
  endfunc

  add object oStr_1_10 as StdString with uid="XSCRNYLCJI",Visible=.t., Left=41, Top=22,;
    Alignment=1, Width=121, Height=18,;
    Caption="Codice CAP:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="WJUOSKNJBY",Visible=.t., Left=41, Top=49,;
    Alignment=1, Width=121, Height=18,;
    Caption="Comune:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="ALWROSBYCR",Visible=.t., Left=41, Top=76,;
    Alignment=1, Width=121, Height=18,;
    Caption="Frazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="CVXZSLBDFO",Visible=.t., Left=41, Top=103,;
    Alignment=1, Width=121, Height=18,;
    Caption="Indirizzo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="GIGTQMGPLX",Visible=.t., Left=41, Top=130,;
    Alignment=1, Width=121, Height=18,;
    Caption="Provincia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="BLRLILLXDR",Visible=.t., Left=41, Top=157,;
    Alignment=1, Width=121, Height=18,;
    Caption="Descrizione provincia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="POJONYFDQA",Visible=.t., Left=9, Top=184,;
    Alignment=1, Width=153, Height=18,;
    Caption="Codice comune per il CF:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_aap','ANAG_CAP','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CPCODCAP=ANAG_CAP.CPCODCAP";
  +" and "+i_cAliasName2+".CPCODLOC=ANAG_CAP.CPCODLOC";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
