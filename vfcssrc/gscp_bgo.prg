* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscp_bgo                                                        *
*              Generazione ordini da web                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_976]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-12-22                                                      *
* Last revis.: 2018-03-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_DOSERIAL
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscp_bgo",oParentObject,m.w_DOSERIAL)
return(i_retval)

define class tgscp_bgo as StdBatch
  * --- Local variables
  w_DOSERIAL = space(10)
  w_MVTIPDOC = space(5)
  w_MVDATREG = ctod("  /  /  ")
  w_MVSERIAL = space(10)
  w_COMMDEFA = space(15)
  w_SALCOM = space(1)
  w_COMMAPPO = space(15)
  DR = space(10)
  w_LOOP = 0
  w_ORSERIAL = space(10)
  w_ORMERISP = space(0)
  w_FLAGEN = space(1)
  w_TDFLMGPR = space(1)
  w_TDALFDOC = space(10)
  w_FLPPRO = space(1)
  w_MVCODORN = space(15)
  w_MVCAUIMB = 0
  w_BOLCAU = space(1)
  w_REVCAU = space(1)
  w_MVIVACAU = space(5)
  w_CCTIPREG = space(1)
  w_CODAZI = space(5)
  w_NUMCOR = space(25)
  w_MAGCLI = space(5)
  w_PPGENDOC = space(1)
  w_CENCOS = space(15)
  w_ARTCODIC = space(20)
  w_ARTIPART = space(2)
  w_ART_KIT = space(1)
  w_MAGPRE = space(5)
  w_OMAG = space(5)
  w_MVPROORD = space(2)
  w_CPROWORD = 0
  w_MVFLFOSC = space(1)
  w_MVCONTRA = space(15)
  w_MVFLTRAS = space(1)
  w_MVSERRIF = space(10)
  w_MVCONCON = space(1)
  w_MVIVAINC = space(5)
  w_CLBOLFAT = space(1)
  w_MVSPEINC = 0
  w_MVNUMRIF = 0
  w_MVCONIND = space(15)
  w_MVROWRIF = 0
  w_MVFLRINC = space(1)
  w_CPROWNUM = 0
  w_MVTIPRIG = space(1)
  w_MVCODLIS = space(5)
  w_MVTCOMAG = space(5)
  w_MVSPEIMB = 0
  w_MVFLORDI = space(1)
  w_MVCODICE = space(20)
  w_MVQTAMOV = 0
  w_MVDATEST = ctod("  /  /  ")
  w_MVTINCOM = ctod("  /  /  ")
  w_MVIVAIMB = space(5)
  w_MVFLIMPE = space(1)
  w_MVCODART = space(20)
  w_MVQTAUM1 = 0
  w_MVACCONT = 0
  w_MVFLRIMB = space(1)
  w_MVCODMAG = space(5)
  w_MVCODVAR = space(20)
  w_MVPREZZO = 0
  w_MVSCOCL1 = 0
  w_MVSPETRA = 0
  w_MVVOCCEN = space(15)
  w_MVDESART = space(40)
  w_MVSCONT1 = 0
  w_MVSCOCL2 = 0
  w_MVIVATRA = space(5)
  w_MVCODCEN = space(15)
  w_MVDESSUP = space(10)
  w_MVSCONT2 = 0
  w_MVSCOPAG = 0
  w_MVFLRTRA = space(1)
  w_MVUNIMIS = space(3)
  w_MVSCONT3 = 0
  w_MVCAUMAG = space(5)
  w_MVSPEBOL = 0
  w_MVRIFKIT = 0
  w_MVCATCON = space(5)
  w_MVSCONT4 = 0
  w_MVMOLSUP = 0
  w_MVIVABOL = space(5)
  w_MVFLSCOR = space(1)
  w_MVCODCLA = space(3)
  w_MVFLOMAG = space(1)
  w_MVSCONTI = 0
  w_MVDATDIV = ctod("  /  /  ")
  w_MVCODCON = space(15)
  w_MVCODIVA = space(5)
  w_MVDATDOC = ctod("  /  /  ")
  w_MFLELAN = space(1)
  w_MVCODIVE = space(5)
  w_MVCODPAG = space(5)
  w_MVKEYSAL = space(40)
  w_MFLAVAL = space(1)
  w_MVCODAGE = space(5)
  w_MVCODBAN = space(10)
  w_MVCODBA2 = space(10)
  w_MVCODPOR = space(1)
  w_MVCAOVAL = 0
  w_MV_SEGNO = space(1)
  w_MVTCONTR = space(15)
  w_MVCODVAL = space(3)
  w_MVCODVET = space(5)
  w_MVVALRIG = 0
  w_MVFLELGM = space(1)
  w_MVTCOCEN = space(15)
  w_MVPESNET = 0
  w_MVCODSPE = space(3)
  w_MVIMPSCO = 0
  w_MVFLELAN = space(1)
  w_MVRIFDIC = space(10)
  w_MVTIPATT = space(1)
  w_MVVALMAG = 0
  w_MVPESNET = 0
  w_MVTCOLIS = space(5)
  w_MVNOMENC = space(8)
  w_MVCODATT = space(15)
  w_MVIMPNAZ = 0
  w_MVACIVA1 = space(5)
  w_MVAIMPN1 = 0
  w_MVAFLOM1 = space(1)
  w_MVAIMPS1 = 0
  w_MVUMSUPP = space(3)
  w_MVACIVA2 = space(5)
  w_MVAIMPN2 = 0
  w_MVAFLOM2 = space(1)
  w_MVAIMPS2 = 0
  w_MVACIVA3 = space(5)
  w_MVAIMPN3 = 0
  w_MVAFLOM3 = space(1)
  w_MVAIMPS3 = 0
  w_MVCODDES = space(5)
  w_MVACIVA4 = space(5)
  w_MVAIMPN4 = 0
  w_MVAFLOM4 = space(1)
  w_MVAIMPS4 = 0
  w_MVIMPARR = 0
  w_MVACIVA5 = space(5)
  w_MVAIMPN5 = 0
  w_MVAFLOM5 = space(1)
  w_MVAIMPS5 = 0
  w_MVDATEVA = ctod("  /  /  ")
  w_MVACIVA6 = space(5)
  w_MVAIMPN6 = 0
  w_MVAFLOM6 = space(1)
  w_MVAIMPS6 = 0
  w_MVACCPRE = 0
  w_MVTOTRIT = 0
  w_MVTOTENA = 0
  w_MVIMPACC = 0
  w_MVIMPFIN = 0
  w_MVVOCCOS = space(15)
  w_CAOVAL = 0
  w_BOLARR = 0
  w_PERIVA = 0
  w_PEIINC = 0
  w_MFLCASC = space(1)
  w_IMPARR = 0
  w_BOLMIN = 0
  w_BOLIVA = space(1)
  w_BOLINC = space(1)
  w_MFLORDI = space(1)
  w_RSIMPRAT = 0
  w_TOTMERCE = 0
  w_MESE1 = 0
  w_PEIIMB = 0
  w_MFLIMPE = space(1)
  w_DECTOT = 0
  w_TOTALE = 0
  w_MESE2 = 0
  w_BOLIMB = space(1)
  w_MFLRISE = space(1)
  w_BOLESE = 0
  w_TOTIMPON = 0
  w_GIORN1 = 0
  w_PEITRA = 0
  w_MFLELGM = space(1)
  w_BOLSUP = 0
  w_TOTIMPOS = 0
  w_GIORN2 = 0
  w_BOLTRA = space(1)
  w_PERIVE = 0
  w_BOLCAM = 0
  w_TOTFATTU = 0
  w_GIOFIS = 0
  w_BOLBOL = space(1)
  w_MVTDTEVA = ctod("  /  /  ")
  w_MCAUCOL = space(5)
  w_CAONAZ = 0
  w_BOLIVE = space(1)
  w_MVDATEVA = ctod("  /  /  ")
  w_CODVOC = space(15)
  w_RSMODPAG = space(10)
  w_RSFLPROV = space(1)
  w_RSNUMRAT = 0
  w_MFLANAL = space(1)
  w_MTIPVOC = space(1)
  w_VALUNI = 0
  w_ACCPRE = 0
  w_CODVOR = space(15)
  w_CODCAA = space(5)
  w_RSDATRAT = ctod("  /  /  ")
  w_CODESE = space(4)
  w_CODNAZ = space(3)
  w_APPO1 = 0
  w_NUCON = 0
  w_FLFOBO = space(1)
  w_MVFLFOBO = space(1)
  w_ACQINT = space(1)
  w_MCAUCOL = space(5)
  w_CATCOM = space(3)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_CODART = space(20)
  w_CATSCC = space(5)
  w_UNMIS3 = space(3)
  w_CODVAR = space(20)
  w_CATSCM = space(5)
  w_DECUNI = 0
  w_MOLTIP = 0
  w_DATREG = ctod("  /  /  ")
  w_GRUMER = space(5)
  w_QUAN = 0
  w_MOLTI3 = 0
  w_CODVAL = space(3)
  w_ROWNUM = 0
  w_QTAUM1 = 0
  w_OPERAT = space(1)
  w_CODLIS = space(5)
  w_ANCODIVA = space(5)
  w_OPERA3 = space(1)
  w_CODCON = space(15)
  w_CATCLI = space(5)
  w_CATART = space(5)
  w_SCOCON = .f.
  w_CODGRU = space(5)
  w_MVFLVEAC = space(1)
  w_MVEMERIC = space(1)
  w_MVALFDOC = space(10)
  w_MVANNPRO = space(4)
  w_MVNUMDOC = 0
  w_MVANNDOC = space(4)
  w_MVPRP = space(2)
  w_MVNUMEST = 0
  w_MVALFEST = space(10)
  w_MVTIPCON = space(1)
  w_MVNUMCOL = 0
  w_MVQTACOL = 0
  w_MVQTAPES = 0
  w_MVQTALOR = 0
  w_APPO = space(10)
  w_MVCLADOC = space(2)
  w_ROWNUM = 0
  w_NUDOC = 0
  w_MVRITPRE = 0
  w_CODART = space(20)
  w_OKDOC = 0
  w_KODOC = 0
  w_WRDOC = 0
  w_CODVAR = space(20)
  w_DESART = space(40)
  w_DESSUP = space(0)
  w_CODCON = space(15)
  Padre = .NULL.
  w_TIPCON = space(1)
  NC = space(10)
  w_MVFLPROV = space(1)
  w_APPLICATION = space(2)
  w_MVVALNAZ = space(3)
  w_ORUTCC = 0
  w_GRPCLI = space(5)
  w_GRPART = space(5)
  w_GRPAGE = space(5)
  w_MVCODAG2 = space(5)
  w_APPART = space(10)
  w_MVPERPRO = 0
  w_MVIMPPRO = 0
  w_PPACCONT = space(1)
  w_APPCONTRA = space(5)
  w_APPVAR = space(5)
  w_MVRIFEST = space(30)
  w_MVDINTRA = space(1)
  w_ARUTISER = space(1)
  w_ARDATINT = space(1)
  w_MVROWWEB = 0
  w_TOTALPOS = 0
  w_MVFLSALD = space(1)
  w_FLSERA = space(1)
  w_CAUPFI = space(5)
  w_CAUCOD = space(5)
  w_FLARCO = space(1)
  w_TDFLEXPL = space(1)
  w_TDCOSEPL = space(1)
  w_FLUBIC = space(1)
  w_F2UBIC = space(1)
  w_PRQTAUM1 = 0
  w_RIFKIT = 0
  w_MVFLCAPA = space(1)
  w_INDIVE = 0
  w_MVDATOAI = ctod("  /  /  ")
  w_MVASPEST = space(30)
  w_MVTIPOPE = space(10)
  w_RATEVA = space(1)
  w_DOCLIS = space(5)
  w_MVFLVABD = space(1)
  w_MVFLSCOM = space(1)
  w_MVESEUTE = 0
  w_ANNAZION = space(3)
  w_NACODVAL = space(3)
  w_VACAOVAL = 0
  w_MVTFRAGG = space(1)
  w_MVPRD = space(2)
  w_MVFLINTE = space(1)
  w_MVFLACCO = space(1)
  w_MVCLADOC = space(2)
  w_MVCAUCON = space(5)
  w_CODMAG = space(3)
  w_MVTCAMAG = space(5)
  w_FLVEAC = space(1)
  w_MVAGG_01 = space(15)
  w_MVAGG_02 = space(15)
  w_MVAGG_03 = space(15)
  w_MVAGG_04 = space(15)
  w_MVAGG_05 = ctod("  /  /  ")
  w_MVAGG_06 = ctod("  /  /  ")
  w_MV_FLAGG = space(1)
  w_MVVALULT = 0
  w_MVTIPPRO = space(2)
  w_MVTIPPR2 = space(2)
  w_MGPROMAG = space(2)
  w_TIPDOC = space(5)
  w_LOCENCOS = space(15)
  w_PPCALPRO = space(2)
  w_OLDCALPRO = space(2)
  w_PPCALSCO = space(1)
  w_PPLISRIF = space(5)
  w_MASSGEN = space(1)
  w_SERPRO = space(10)
  w_RET = .NULL.
  pDOCINFO = .NULL.
  w_contrate = 0
  w_TOTDOC = 0
  w_MINRATA = 0
  w_MSGRET = space(254)
  w_TIMPSCO = 0
  w_PRES = 0
  w_MVTIPIMB = space(1)
  w_MAXLEV = 0
  w_VALCOM = space(1)
  w_SPETRA = space(20)
  w_SPEINC = space(20)
  w_SPEBOL = space(20)
  w_SPEIMB = space(20)
  * --- WorkFile variables
  CAM_AGAZ_idx=0
  CONTI_idx=0
  DOC_DETT_idx=0
  DOC_MAST_idx=0
  DOC_RATE_idx=0
  SALDIART_idx=0
  VALUTE_idx=0
  VOCIIVA_idx=0
  ZORDWEBD_idx=0
  ZORDWEBM_idx=0
  TIP_DOCU_idx=0
  AGENTI_idx=0
  ART_ICOL_idx=0
  PAR_PROV_idx=0
  KEY_ARTI_idx=0
  FAM_ARTI_idx=0
  NAZIONI_idx=0
  MAGAZZIN_idx=0
  PAG_2AME_idx=0
  DES_DIVE_idx=0
  PAR_PROD_idx=0
  SALDICOM_idx=0
  AZIENDA_idx=0
  SERVAL_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Generazione Ordini Web
    * --- --
    if g_REVICRM<>"S"
      this.w_DOSERIAL = space(10)
    endif
    * --- Queste variabili erano caller. Per garantire un corretto funzionamento anche nel caso
    *     in cui si importi un ordine con Causale Documento = PROCL devono essere dichiarate
    *     local perch� riferite anche dal batch GSAR_BEA richiamato appositamente per l'esplosione
    *     della Distina Base degli articoli in ordine.
    this.w_MVDATREG = this.oParentObject.w_MVDATREG
    * --- Read from PAR_PROD
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_PROD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2],.t.,this.PAR_PROD_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PPCODCOM"+;
        " from "+i_cTable+" PAR_PROD where ";
            +"PPCODICE = "+cp_ToStrODBC("PP");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PPCODCOM;
        from (i_cTable) where;
            PPCODICE = "PP";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_COMMDEFA = NVL(cp_ToDate(_read_.PPCODCOM),cp_NullValue(_read_.PPCODCOM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if empty(nvl(this.w_COMMDEFA,""))
      * --- Mi assicuro che la variabile sia space di 15 (per evitare problemi con Oracle)
      this.w_COMMDEFA = space(15)
    endif
    * --- --
    if Empty(this.w_DOSERIAL)
      this.Padre = this.oParentObject
      this.Padre.Bupdated = .t.
      * --- Verifico la maschera
      if Not this.Padre.CheckForm()
        i_retcode = 'stop'
        return
      endif
      this.NC = this.Padre.w_ZoomSel.cCursor
      * --- Carica i Documenti da Generare
      ah_Msg("Ricerca documenti Web da elaborare...")
      select * From ( this.NC ) Where Xchk=1 into Cursor __Tem__ NoFilter
    else
      * --- Generazione automatica da profilo: genero un solo ordine ignorando le regole di validazione
      vq_exec("query\gscpabgo.vqr", this, "__Tem__")
    endif
    * --- Necessario in quanto la Var w_MVVALNAZ � usata in GSAR_BFA
    this.w_MVVALNAZ = this.oParentObject.w_VALNAZ
    this.w_MVDATDIV = this.oParentObject.w_DATDIV
    this.w_MVRITPRE = 0
    this.w_MVFLFOSC = " "
    this.w_CAONAZ = GETCAM(this.w_MVVALNAZ, this.w_MVDATDOC, 0)
    this.w_CODESE = this.oParentObject.w_MVCODESE
    this.w_APPLICATION = iif(isahr(), "AR", "AE")
    * --- Variabili utilizzate in GSAR_BFA e non inizializzate dal batch
    this.w_FLFOBO = " "
    this.w_MVFLFOBO = " "
    this.w_CLBOLFAT = " "
    this.w_ACQINT = "N"
    this.w_MVCLADOC = "OR"
    this.w_MVPRP = "NN"
    * --- Rate Scadenze
    DIMENSION DR[51, 9]
    this.w_LOOP = 1
    do while this.w_LOOP <= 51
      DR[ this.w_LOOP , 1] = cp_CharToDate("  -  -  ")
      DR[ this.w_LOOP , 2] = 0
      DR[ this.w_LOOP , 3] = "  "
      DR[ this.w_LOOP , 4] = "  "
      DR[ this.w_LOOP , 5] = "  "
      DR[ this.w_LOOP , 6] = "  "
      DR[ this.w_LOOP , 7] = " "
      DR[ this.w_LOOP , 8] = " "
      DR[ this.w_LOOP , 9] = " "
      this.w_LOOP = this.w_LOOP + 1
    enddo
    * --- Legge Informazioni di Riga di Default
    this.w_MFLCASC = " "
    this.w_MFLORDI = " "
    this.w_MFLIMPE = " "
    this.w_MFLRISE = " "
    this.w_MFLELGM = " "
    this.w_MFLANAL = " "
    this.w_MFLELAN = " "
    this.w_MFLAVAL = " "
    this.w_MTIPVOC = " "
    this.w_MCAUCOL = SPACE(5)
    * --- --
    this.w_MVCODAG2 = Null
    this.w_MVDATOAI = i_DATSYS
    this.w_PPACCONT = " "
    if USED("__TEM__")
      * --- Inizio Aggiornamento
      * --- Cursore contenente risultato ed errori di elaborazione
      Create Cursor Resoconto (SERIAL C(10), TIPO C(1), NUMDOC N(15,0), ALFDOC C(10), DATDOC D, RAGSOC C(40), LIVCON N(3,0), MSG M(10), MERISP M(10) NULL, NOTECONT M(10) NULL)
      * --- TIPO assume i valori:
      *     'E'= Errore
      *     'A'= Ordine accettato
      *     'R'= Ordine rifiutato
      this.w_OKDOC = 0
      this.w_KODOC = 0
      this.w_WRDOC = 0
      this.w_NUDOC = RECCOUNT( "__TEM__" )
      if this.w_NUDOC > 0
        ah_Msg("Inizio fase di generazione...")
        do while Not Eof ( "__TEM__" )
          * --- Query per recuperare tutte le infomazioni relative all'ordine in esame
          this.w_ORSERIAL = __TEM__.ORSERIAL
          this.w_ORMERISP = __TEM__.ORMERISP
          * --- Registra informazioni in cursore Resoconto per resoconto a fine elaborazione
          INSERT INTO Resoconto (SERIAL, TIPO, NUMDOC, ALFDOC, DATDOC, RAGSOC, LIVCON, MSG, MERISP, NOTECONT) VALUES (this.w_ORSERIAL, space(1),__TEM__.ORNUMDOC, __TEM__.ORALFDOC, __TEM__.ORDATDOC, __TEM__.ANDESCRI, __TEM__.ORLIVCON, space(1), __TEM__.ORMERISP, __TEM__.NOTECONT)
          * --- --
          if Empty(this.w_DOSERIAL)
            FLPROV = "this.oParentObject.w_MVFLPROV"+ALLTRIM(STR(__TEM__.ORLIVCON,1,0))
            this.w_MVFLPROV = &FLPROV
            this.w_MVFLPROV = IIF(this.w_MVFLPROV="R","S",this.w_MVFLPROV)
            FLPROV = "this.oParentObject.w_MVTIPDOC"+ALLTRIM(STR(__TEM__.ORLIVCON,1,0))
            this.w_MVTIPDOC = &FLPROV
            FLPROV = "this.oParentObject.w_FLAGEN"+ALLTRIM(STR(__TEM__.ORLIVCON,1,0))
            this.w_FLAGEN = &FLPROV
            FLPROV = "this.oParentObject.w_FLVEAC"+ALLTRIM(STR(__TEM__.ORLIVCON,1,0))
            this.w_MVFLVEAC = &FLPROV
            FLPROV = "this.oParentObject.w_EMERIC"+ALLTRIM(STR(__TEM__.ORLIVCON,1,0))
            this.w_MVEMERIC = &FLPROV
            FLPROV = "this.oParentObject.w_CENCOS"+ALLTRIM(STR(__TEM__.ORLIVCON,1,0))
            this.w_MVTCOCEN = &FLPROV
            FLPROV = "this.oParentObject.w_CODMAG"+ALLTRIM(STR(__TEM__.ORLIVCON,1,0))
            this.w_MVTCOMAG = &FLPROV
            FLPROV = "this.oParentObject.w_MVTCAMAG"+ALLTRIM(STR(__TEM__.ORLIVCON,1,0))
            this.w_MVCAUMAG = &FLPROV
          else
            * --- Generazione automatica dal batch di profilo iahr_get_orders
            this.w_MVTIPDOC = this.oParentObject.w_MVTIPDOC
            * --- Read from TIP_DOCU
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "TDFLVEAC,TDPRODOC,TDCATDOC,TDFLACCO,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDFLPPRO,TDFLPROV,TDEMERIC,TDCODMAG,TDPROVVI,TDFLMGPR,TDALFDOC"+;
                " from "+i_cTable+" TIP_DOCU where ";
                    +"TDTIPDOC = "+cp_ToStrODBC(this.w_MVTIPDOC);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                TDFLVEAC,TDPRODOC,TDCATDOC,TDFLACCO,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDFLPPRO,TDFLPROV,TDEMERIC,TDCODMAG,TDPROVVI,TDFLMGPR,TDALFDOC;
                from (i_cTable) where;
                    TDTIPDOC = this.w_MVTIPDOC;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_FLVEAC = NVL(cp_ToDate(_read_.TDFLVEAC),cp_NullValue(_read_.TDFLVEAC))
              this.w_MVPRD = NVL(cp_ToDate(_read_.TDPRODOC),cp_NullValue(_read_.TDPRODOC))
              this.w_MVFLVEAC = NVL(cp_ToDate(_read_.TDFLVEAC),cp_NullValue(_read_.TDFLVEAC))
              this.w_MVCLADOC = NVL(cp_ToDate(_read_.TDCATDOC),cp_NullValue(_read_.TDCATDOC))
              this.w_MVFLACCO = NVL(cp_ToDate(_read_.TDFLACCO),cp_NullValue(_read_.TDFLACCO))
              this.w_MVFLINTE = NVL(cp_ToDate(_read_.TDFLINTE),cp_NullValue(_read_.TDFLINTE))
              this.w_MVTCAMAG = NVL(cp_ToDate(_read_.TDCAUMAG),cp_NullValue(_read_.TDCAUMAG))
              this.w_MVTFRAGG = NVL(cp_ToDate(_read_.TFFLRAGG),cp_NullValue(_read_.TFFLRAGG))
              this.w_MVCAUCON = NVL(cp_ToDate(_read_.TDCAUCON),cp_NullValue(_read_.TDCAUCON))
              this.w_FLPPRO = NVL(cp_ToDate(_read_.TDFLPPRO),cp_NullValue(_read_.TDFLPPRO))
              this.w_FLAGEN = NVL(cp_ToDate(_read_.TDFLPROV),cp_NullValue(_read_.TDFLPROV))
              this.w_MVEMERIC = NVL(cp_ToDate(_read_.TDEMERIC),cp_NullValue(_read_.TDEMERIC))
              this.w_MVTCOMAG = NVL(cp_ToDate(_read_.TDCODMAG),cp_NullValue(_read_.TDCODMAG))
              this.w_MVFLPROV = NVL(cp_ToDate(_read_.TDPROVVI),cp_NullValue(_read_.TDPROVVI))
              this.w_TDFLMGPR = NVL(cp_ToDate(_read_.TDFLMGPR),cp_NullValue(_read_.TDFLMGPR))
              this.w_TDALFDOC = NVL(cp_ToDate(_read_.TDALFDOC),cp_NullValue(_read_.TDALFDOC))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_MVCAUMAG = this.w_MVTCAMAG
            this.w_CODMAG = this.w_MVTCOMAG
            this.w_MVTCOCEN = space(15)
            this.w_CENCOS = this.w_MVTCOCEN
          endif
          * --- Read from CAM_AGAZ
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CMCAUCOL,CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLAVAL,CMFLELGM"+;
              " from "+i_cTable+" CAM_AGAZ where ";
                  +"CMCODICE = "+cp_ToStrODBC(this.w_MVCAUMAG);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CMCAUCOL,CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLAVAL,CMFLELGM;
              from (i_cTable) where;
                  CMCODICE = this.w_MVCAUMAG;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_MCAUCOL = NVL(cp_ToDate(_read_.CMCAUCOL),cp_NullValue(_read_.CMCAUCOL))
            this.w_MFLCASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
            this.w_MFLORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
            this.w_MFLIMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
            this.w_MFLRISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
            this.w_MFLAVAL = NVL(cp_ToDate(_read_.CMFLAVAL),cp_NullValue(_read_.CMFLAVAL))
            this.w_MFLELGM = NVL(cp_ToDate(_read_.CMFLELGM),cp_NullValue(_read_.CMFLELGM))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          vq_exec("query\GSCP_BGO.VQR",this,"GENEORDI")
          if RecCount( "GENEORDI" )>0
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            * --- Documento preso in esame da un'altra elaborazione della procedura
            select Resoconto
            REPLACE MSG with ah_msgformat("Documento generato da elaborazione concorrente")
            REPLACE TIPO with "E"
          endif
          if not Eof( "__TEM__")
            Select __tem__
            Skip
          endif
        enddo
        * --- Operazione completata
        wait clear
        if this.w_WRDOC=0
          this.w_APPO = "Operazione completata%0%0N. %1 ordini generati, n. %2 ordini rifiutati su %3 ordini da elaborare"
        else
          this.w_APPO = "Operazione completata%0%0N. %1 ordini generati con n. %4 anomalie, n. %2 ordini rifiutati su %3 ordini da elaborare"
        endif
        if Empty(this.w_DOSERIAL)
          if this.w_KODOC>0
            ah_ErrorMsg(this.w_APPO,"!","", ALLTRIM(STR(this.w_OKDOC)), ALLTRIM(STR(this.w_KODOC)),ALLTRIM(STR(this.w_NUDOC)) )
          else
            ah_ErrorMsg(this.w_APPO,"i","", ALLTRIM(STR(this.w_OKDOC)), ALLTRIM(STR(this.w_KODOC)),ALLTRIM(STR(this.w_NUDOC)) )
          endif
          if RECCOUNT("Resoconto") > 0
            if ah_YesNo("Vuoi stampare il resoconto della generazione ordini via Web?")
              * --- Variabili per stampa
              p_OKDOC = this.w_OKDOC
              p_KODOC = this.w_KODOC
              p_ERDOC = this.w_NUDOC - this.w_OKDOC - this.w_KODOC
              p_NUDOC = this.w_NUDOC
              * --- Crea cursore per stampa
              SELECT * FROM Resoconto INTO CURSOR __TMP__
              * --- Esegue stampa
              CP_CHPRN("query\GSCP_BGO.FRX", " ", this)
            endif
          endif
          this.Padre.NotifyEvent("Interroga")     
        endif
      else
        this.w_APPO = "Per l'intervallo selezionato non esistono documenti da generare"
        if Empty(this.w_DOSERIAL)
          ah_ErrorMsg(this.w_APPO,"!","")
        endif
      endif
    endif
    * --- Chiude i Cursori lasciati Aperti
    if USED("__tem__")
      * --- Elimina Cursore di Appoggio se esiste ancora
      use in __tem__
    endif
    if USED("GeneOrdi")
      * --- Elimina Cursore di Appoggio se esiste ancora
      use in GeneOrdi
    endif
    if USED("GeneApp")
      * --- Elimina Cursore di Appoggio se esiste ancora
      use in GeneApp
    endif
    if USED("Resoconto")
      * --- Elimina Cursore di Appoggio se esiste ancora
      use in Resoconto
    endif
    if USED("__tmp__")
      * --- Elimina Cursore di Appoggio se esiste ancora
      use in __tmp__
    endif
    if Not Empty(this.w_DOSERIAL)
      i_retcode = 'stop'
      i_retval = this.w_APPO
      return
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Genera i Documenti di Ordine da Web
     CREATE CURSOR GeneApp ; 
 (CPROWNUM N(4), CPROWORD N(5,0), t_MVTIPRIG C(1), t_MVCODICE C(20), t_MVCODART C(20), t_MVCODVAR C(20), ; 
 t_MVDESART C(40), t_MVDESSUP M(10), t_MVUNIMIS C(3), t_MVCATCON C(5), ; 
 t_MVRIFKIT N(4,0), t_MVCODCLA C(3), t_MVCONTRA C(15), t_MVCODLIS C(5), ; 
 t_MVQTAMOV N(12,3), t_MVQTAUM1 N(12,3), t_MVPREZZO N(18,5), t_MVNUMCOL N(5,0),; 
 t_MVSCONT1 N(6,2), t_MVSCONT2 N(6,2), t_MVSCONT3 N(6,2), t_MVSCONT4 N(6,2), ; 
 t_MVFLOMAG C(1), t_MVCODIVA C(5), t_PERIVA N(5,2), t_BOLIVA C(1), t_MVVALRIG N(18,4), ; 
 t_MVPESNET N(9,3), t_MVNOMENC C(8), t_MVUMSUPP C(3), t_MVMOLSUP N(8,3), ; 
 t_MVIMPACC N(18,4), t_MVIMPSCO N(18,4), t_MVVALMAG N(18,4), t_MVIMPNAZ N(18,4), ; 
 t_MVCODMAG C(5), t_MVCODCEN C(15), t_CODCAA C(5), t_MVVOCCOS C(15), ; 
 t_MVCODCOM C(15), t_MVTIPATT C(1), t_MVCODATT C(15), t_MVDATEVA D(8),; 
 t_MVNAZPRO C(3), t_FLSERA C(1), t_MVIMPPRO N(18,4), t_MVPERPRO N(12,7), t_MVIMPCOM N(18,4) DEFAULT 0)
    * --- Definizione nuove variabile richieste in GSAR_BFA (AHR 5.0)
    this.w_CODAZI = i_CODAZI
    if NOT Empty(this.w_DOSERIAL)
      this.Page_9()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    this.w_CPROWORD = 0
    SELECT GENEORDI
    GO TOP
    SCAN 
    if RecNo( "GENEORDI" ) = 1
      * --- Inizializza i dati di Testata del Nuovo Documento
      this.w_MVCODCON = NVL(GENEORDI.ORCODCON," ")
      this.w_MVDATDOC = Iif(Empty(this.w_DOSERIAL), NVL(GENEORDI.ORDATDOC, this.w_MVDATREG ), this.w_MVDATREG )
      this.w_MVANNDOC = STR(YEAR(this.w_MVDATDOC), 4, 0)
      this.w_MVALFDOC = Iif(Empty(this.w_DOSERIAL), Nvl( ORALFDOC, "" ), this.w_TDALFDOC)
      this.w_MVNUMDOC = Iif(Empty(this.w_DOSERIAL), Nvl( ORNUMDOC, 0 ), cp_GetProg("DOC_MAST", "PRDOC", this.w_MVNUMDOC, i_codazi, this.w_MVANNDOC, this.w_MVPRD, this.w_MVALFDOC))
      this.w_MVTDTEVA = NVL(GENEORDI.ORTDTEVA, this.w_MVDATDOC ) 
      this.w_MVNUMEST = NVL( GENEORDI.ORNUMEST ,0 )
      this.w_MVALFEST = NVL ( GENEORDI.ORALFEST, "" )
      this.w_MVDATEST = NVL(GENEORDI.ORDATEST, {} )
      this.w_MVTIPCON = NVL ( GENEORDI.ORTIPCON ,"" ) 
      * --- Assegna se presente codice esenzione IVA 
      this.w_MVCODIVE = NVL ( GENEORDI.ANCODIVA ,"" ) 
      * --- lettura categoria provvigioni cliente
      * --- Leggo anche il conto corrente.
      *     ANNUMCOR viene sempre valorizzato con il conto corrente di default della tabella BAN_CONTI
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANGRUPRO,AFFLINTR,ANNUMCOR,ANCODORN,ANMAGTER"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_MVTIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_MVCODCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANGRUPRO,AFFLINTR,ANNUMCOR,ANCODORN,ANMAGTER;
          from (i_cTable) where;
              ANTIPCON = this.w_MVTIPCON;
              and ANCODICE = this.w_MVCODCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_GRPCLI = NVL(cp_ToDate(_read_.ANGRUPRO),cp_NullValue(_read_.ANGRUPRO))
        this.w_MVDINTRA = NVL(cp_ToDate(_read_.AFFLINTR),cp_NullValue(_read_.AFFLINTR))
        this.w_NUMCOR = NVL(cp_ToDate(_read_.ANNUMCOR),cp_NullValue(_read_.ANNUMCOR))
        this.w_MVCODORN = NVL(cp_ToDate(_read_.ANCODORN),cp_NullValue(_read_.ANCODORN))
        this.w_MAGCLI = NVL(cp_ToDate(_read_.ANMAGTER),cp_NullValue(_read_.ANMAGTER))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Assegna se presente codice esenzione IVA a MVIVAINC, MVIVAIMB e MVIVATRA
      this.w_MVIVAINC = this.w_MVCODIVE
      this.w_MVIVAIMB = this.w_MVCODIVE
      this.w_MVIVATRA = this.w_MVCODIVE
      this.w_MVACCONT = NVL(GENEORDI.ORACCONT,0)
      if this.w_MVACCONT > 0
        this.w_MVFLCAPA = "S"
      else
        this.w_MVFLCAPA = " "
      endif
      if this.w_MVACCONT>0 AND GENEORDI.ORVALACC==GENEORDI.ORVALNAZ
        this.w_MVACCONT = val2cam(this.w_MVACCONT, GENEORDI.ORVALACC, GENEORDI.ORVALNAZ, GENEORDI.ORCAOVAL, this.w_MVDATDOC)
      endif
      this.w_MVRIFDIC = Space(10)
      this.w_ORUTCC = i_CODUTE
      * --- Se nell'anagrafica conti non � presente l'esenzione controllo se � presente la dichiarazione di intento
      if Empty(NVL(this.w_MVCODIVE,space(3)))
        * --- Gestisco la dichiarazione di intento in base al tipo di gestionale
        gscp_bgr(this,"DICHIARAZIONE INTENTO")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      if EMPTY(NVL(this.w_MVCODIVE,""))
        this.w_MVCODIVE = Null
      endif
      SELECT GENEORDI
      this.w_MVCODBAN = Nvl( ANCODBAN ,Space(10) )
      if EMPTY(NVL(this.w_NUMCOR,""))
        this.w_NUMCOR = Null
      endif
      if Empty(this.w_MVCODBAN)
        * --- Il numero di conto corrente devo riportarlo solo se c'� la banca 
        this.w_NUMCOR = Null
      endif
      this.w_MVCODBA2 = Nvl( ANCODBA2 ,Space(10) )
      this.w_MVCODPAG = GENEORDI.ORCODPAG
      this.w_MVCODVAL = Nvl( GENEORDI.ORCODVAL , g_PERVAL )
      this.w_MVCODAGE = iif(NOT EMPTY(Nvl( ORCODAGE , Space(5))), RIGHT(ORCODAGE,5), Null)
      this.w_MVRIFEST = ALLTRIM(NVL(ORRIFEST," "))
      if NOT EMPTY(NVL(this.w_MVCODAGE,""))
        if this.w_FLAGEN = "S"
          * --- categoria provvigione agente
          * --- Read from AGENTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.AGENTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2],.t.,this.AGENTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "AGCATPRO,AGCZOAGE"+;
              " from "+i_cTable+" AGENTI where ";
                  +"AGCODAGE = "+cp_ToStrODBC(this.w_MVCODAGE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              AGCATPRO,AGCZOAGE;
              from (i_cTable) where;
                  AGCODAGE = this.w_MVCODAGE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_GRPAGE = NVL(cp_ToDate(_read_.AGCATPRO),cp_NullValue(_read_.AGCATPRO))
            this.w_MVCODAG2 = NVL(cp_ToDate(_read_.AGCZOAGE),cp_NullValue(_read_.AGCZOAGE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if empty(nvl(this.w_MVCODAG2,""))
            * --- Viene rimesso a null in quanto se space, non viene 
            *     eseguita correttamente la rottura nel piano di spedizione.
            this.w_MVCODAG2 = Null
          endif
          * --- Lettura flag per elaborazione provvigioni
        else
          * --- Read from AGENTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.AGENTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2],.t.,this.AGENTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "AGCZOAGE"+;
              " from "+i_cTable+" AGENTI where ";
                  +"AGCODAGE = "+cp_ToStrODBC(this.w_MVCODAGE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              AGCZOAGE;
              from (i_cTable) where;
                  AGCODAGE = this.w_MVCODAGE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_MVCODAG2 = NVL(cp_ToDate(_read_.AGCZOAGE),cp_NullValue(_read_.AGCZOAGE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if empty(nvl(this.w_MVCODAG2,""))
            * --- Viene rimesso a null in quanto se space, non viene 
            *     eseguita correttamente la rottura nel piano di spedizione.
            this.w_MVCODAG2 = Null
          endif
        endif
      endif
      this.w_MVCODDES = Nvl( ORCODDES , Space(5) )
      * --- Calcolo il cambio
      if this.w_MVCODVAL<>this.w_MVVALNAZ
        this.w_MVCAOVAL = GENEORDI.ORCAOVAL
      else
        this.w_MVCAOVAL = this.w_CAONAZ
      endif
      this.w_MVCAOVAL = IIF(this.w_MVCAOVAL=0, g_CAOVAL, this.w_MVCAOVAL)
      this.w_MVSCOCL1 = NVL(ORSCOCL1,0)
      this.w_MVSCOCL2 = NVL(ORSCOCL2,0)
      this.w_MVSCOPAG = NVL(ORSCOPAG,0)
      this.w_MVFLSCOR = IIF(EMPTY(NVL(ORFLSCOR , "N" )),"N",NVL(ORFLSCOR , "N" ))
      if Empty(this.w_DOSERIAL)
        FLPROV = "this.oParentObject.w_FLPPRO"+ALLTRIM(STR(__TEM__.ORLIVCON,1,0))
        this.w_FLPPRO = &FLPROV
      endif
      this.w_MVANNPRO = CALPRO(this.w_MVDATREG,this.oParentObject.w_MVCODESE,this.w_FLPPRO)
      * --- Inizializzo il listino ed il contratto di testata con il listino ed il contratto della prima riga
      this.w_MVTCOLIS = Nvl( ORCODLIS , Space(5) )
      this.w_MVTCONTR = ORCONTRA
      if Empty(this.w_DOSERIAL)
        FLPROV = "this.oParentObject.w_CENCOS"+ALLTRIM(STR(__TEM__.ORLIVCON,1,0))
        this.w_CENCOS = &FLPROV
      endif
      this.w_MVTCOCEN = this.w_CENCOS
      * --- Legge Dati Associati alla Valuta
      this.w_DECTOT = 0
      this.w_DECUNI = 0
      this.w_BOLESE = 0
      this.w_BOLSUP = 0
      this.w_BOLCAM = 0
      this.w_BOLARR = 0
      this.w_BOLMIN = 0
      * --- Read from VALUTE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VALUTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "VADECTOT,VABOLESE,VABOLSUP,VABOLCAM,VABOLARR,VABOLMIM,VADECUNI"+;
          " from "+i_cTable+" VALUTE where ";
              +"VACODVAL = "+cp_ToStrODBC(this.w_MVCODVAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          VADECTOT,VABOLESE,VABOLSUP,VABOLCAM,VABOLARR,VABOLMIM,VADECUNI;
          from (i_cTable) where;
              VACODVAL = this.w_MVCODVAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
        this.w_BOLESE = NVL(cp_ToDate(_read_.VABOLESE),cp_NullValue(_read_.VABOLESE))
        this.w_BOLSUP = NVL(cp_ToDate(_read_.VABOLSUP),cp_NullValue(_read_.VABOLSUP))
        this.w_BOLCAM = NVL(cp_ToDate(_read_.VABOLCAM),cp_NullValue(_read_.VABOLCAM))
        this.w_BOLARR = NVL(cp_ToDate(_read_.VABOLARR),cp_NullValue(_read_.VABOLARR))
        this.w_BOLMIN = NVL(cp_ToDate(_read_.VABOLMIM),cp_NullValue(_read_.VABOLMIM))
        this.w_DECUNI = NVL(cp_ToDate(_read_.VADECUNI),cp_NullValue(_read_.VADECUNI))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      SELECT GENEORDI
      * --- calcola le Spese di Incasso
      this.w_MVSPEINC = NVL(ORSPEINC,0)
      this.w_MVSPEIMB = NVL(ORSPEIMB,0)
      this.w_MVSPETRA = NVL(ORSPETRA,0)
      this.w_MVSPEBOL = NVL(ORSPEBOL,0)
      this.w_MVFLFOSC = NVL(ORFLFOSC," ")
      this.w_MVSCONTI = NVL(ORSCONTI,0)
      this.w_MVIMPARR = 0
      this.w_MVACCPRE = 0
      * --- I codici Iva associati alle spese non vengono specificati, quindi le spese 
      *     verrano ripartite sui vari codici IVA presenti sul dettaglio
      * --- Dichiarazione di intento non applicata
      if Empty(this.w_MVIVAINC)
        this.w_MVIVAINC = g_COIINC
      endif
      if Empty(this.w_MVIVAIMB)
        this.w_MVIVAIMB = g_COIIMB
      endif
      if Empty(this.w_MVIVATRA)
        this.w_MVIVATRA = g_COITRA
      endif
      this.w_MVIVABOL = g_COIBOL
      this.w_MVCODVET = NVL( ORCODVET ,"" )
      this.w_MVCODSPE = NVL( ORCODSPE ,"" )
      this.w_MVCODPOR = IIF(EMPTY(NVL( ORCODPOR ,"" )), .NULL. , ORCODPOR)
      this.w_MVCONCON = NVL( ANCONCON, " " )
      this.w_GIORN1 = NVL(ANGIOSC1, 0)
      this.w_GIORN2 = NVL(ANGIOSC2, 0)
      this.w_MESE1 = NVL(AN1MESCL, 0)
      this.w_MESE2 = NVL(AN2MESCL, 0)
      this.w_GIOFIS = NVL(ANGIOFIS, 0)
      this.w_CODNAZ = NVL(ANNAZION, g_CODNAZ)
      this.w_CATSCC = NVL(ANCATSCM, SPACE(5))
      this.w_BOLINC = " "
      this.w_BOLIMB = " "
      this.w_BOLTRA = " "
      this.w_BOLBOL = " "
      this.w_BOLIVE = " "
      this.w_PEIINC = 0
      this.w_PEIIMB = 0
      this.w_PEITRA = 0
      this.w_PERIVE = 0
      this.w_MVTCONTR = null
      this.w_MVQTACOL = 0
      * --- Creo una nuova variabile ARTCODIC.Questa variabile serve per poter leggere il codice articolo assocciato al codice di ricerca letto nelle contropartite e vincoli.
      * --- Generazione delle note di un ordine web
      if Not Empty (g_Artdes) and Not Empty (alltrim(NVL(OR__NOTE," ")))
        * --- Read from KEY_ARTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CACODART"+;
            " from "+i_cTable+" KEY_ARTI where ";
                +"CACODICE = "+cp_ToStrODBC(g_Artdes);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CACODART;
            from (i_cTable) where;
                CACODICE = g_Artdes;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_ARTCODIC = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARCODCLA"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_ARTCODIC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARCODCLA;
            from (i_cTable) where;
                ARCODART = this.w_ARTCODIC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MVCODCLA = NVL(cp_ToDate(_read_.ARCODCLA),cp_NullValue(_read_.ARCODCLA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_MVTIPRIG = "D"
        this.w_MVCODICE = g_ARTDES
        this.w_MVCODART = IIF(EMPTY(ALLTRIM(this.w_ARTCODIC)),g_ARTDES,this.w_ARTCODIC)
        this.w_MVDESSUP = NVL(OR__NOTE, " ")
        this.w_MVDESART = ALLTRIM(IIF(EMPTY(Left(this.w_MVDESSUP, 40)),g_ARTDES,Left(this.w_MVDESSUP, 40)))
        this.w_MVDESART = NVL(this.w_MVDESART,".")
        this.w_MVDATEVA = NVL( ORDATEVA , this.w_MVTDTEVA ) 
        this.w_CODNAZ = NVL(ANNAZION, g_CODNAZ)
        this.w_MVTIPATT = "A"
        this.w_MVFLOMAG = IIF(EMPTY(Nvl ( ORFLOMAG , "X" )),"X",Nvl ( ORFLOMAG , "X" ))
        this.w_MVFLOMAG = IIF(this.w_MVFLOMAG="O","S",this.w_MVFLOMAG)
        INSERT INTO GeneApp ; 
 (CPROWNUM, CPROWORD, t_MVTIPRIG, t_MVCODICE, t_MVCODART, t_MVDESSUP, t_MVDESART, t_MVDATEVA, t_MVNAZPRO, t_MVTIPATT, t_MVFLOMAG, t_MVCODCLA); 
 VALUES(-1,5,this.w_MVTIPRIG,this.w_MVCODICE,this.w_MVCODART,this.w_MVDESSUP, this.w_MVDESART, this.w_MVDATEVA, this.w_CODNAZ, this.w_MVTIPATT, this.w_MVFLOMAG, this.w_MVCODCLA)
        SELECT GENEORDI
      endif
    endif
    this.w_CPROWORD = this.w_CPROWORD+10
    this.w_CPROWNUM = NVL(CPROWNUM, 0)
    * --- Scrive nuova Riga sul Temporaneo di Appoggio
    this.w_MVTIPRIG = IIF(g_CPIN="S" OR g_REVICRM="S", Nvl(ORTIPRIG, "D"), IIF(Nvl(ORTIPRIG," ")="R","R","D"))
    * --- Codice ricerca
    this.w_MVCODICE = NVL(ORCODICE, SPACE(41))
    this.w_MVCODART = NVL(ORCODART, SPACE(20))
    * --- Read from ART_ICOL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ARTIPART,ARFLCOMP,ARMAGPRE"+;
        " from "+i_cTable+" ART_ICOL where ";
            +"ARCODART = "+cp_ToStrODBC(this.w_MVCODICE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ARTIPART,ARFLCOMP,ARMAGPRE;
        from (i_cTable) where;
            ARCODART = this.w_MVCODICE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ARTIPART = NVL(cp_ToDate(_read_.ARTIPART),cp_NullValue(_read_.ARTIPART))
      this.w_ART_KIT = NVL(cp_ToDate(_read_.ARFLCOMP),cp_NullValue(_read_.ARFLCOMP))
      this.w_MAGPRE = NVL(cp_ToDate(_read_.ARMAGPRE),cp_NullValue(_read_.ARMAGPRE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_ART_KIT="S"
      * --- Articolo composto - KIT
      this.w_ARTIPART = "AC"
    endif
    this.w_MVDESART = NVL(ORDESART, SPACE(40))
    this.w_MVDESSUP = NVL(ORDESSUP, SPACE(10))
    if this.w_MVTIPRIG="D"
      * --- Insert righe descrittive
      this.w_MVCODICE = g_ARTDES
      this.w_MVCODART = g_ARTDES
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARCODCLA"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.w_MVCODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARCODCLA;
          from (i_cTable) where;
              ARCODART = this.w_MVCODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MVCODCLA = NVL(cp_ToDate(_read_.ARCODCLA),cp_NullValue(_read_.ARCODCLA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_MVDATEVA = NVL( ORDATEVA , this.w_MVTDTEVA ) 
      INSERT INTO GeneApp ; 
 (CPROWNUM, CPROWORD, t_MVTIPRIG, t_MVCODICE, t_MVCODART, t_MVDESSUP, t_MVDESART, t_MVDATEVA, t_MVNAZPRO, t_MVTIPATT, t_MVFLOMAG, t_MVDATEVA, t_MVCODCLA); 
 VALUES(this.w_CPROWNUM, this.w_CPROWORD,this.w_MVTIPRIG,this.w_MVCODICE,this.w_MVCODART,this.w_MVDESSUP, this.w_MVDESART, this.w_MVDATEVA, this.w_CODNAZ, this.w_MVTIPATT, "X", this.w_MVDATEVA,this.w_MVCODCLA)
    else
      this.w_FLSERA = SPACE(1)
      * --- Codice variante
      this.w_MVCODVAR = NVL(ORCODVAR, SPACE(20))
      this.w_MVUNIMIS = NVL(ORUNIMIS, SPACE(3))
      * --- Categoria contabile articolo
      this.w_MVCATCON = NVL(ARCATCON, SPACE(5))
      * --- Classe articolo
      this.w_MVCODCLA = NVL(ARCODCLA, SPACE(3))
      * --- Contratto di riga
      this.w_MVCONTRA = Nvl( ORCONTRA ,Space(15) )
      * --- Categoria Sconti e Maggiorazioni
      this.w_CATSCM = NVL(ARCATSCM, SPACE(5))
      this.w_GRUMER = NVL(ARGRUMER, SPACE(5))
      this.w_MVCODLIS = Nvl( ORCODLIS , Space(5) )
      this.w_MVQTAMOV = NVL( ORQTAMOV , 0)
      this.w_MVQTAUM1 = NVL( ORQTAUM1 , 0)
      this.w_MVDATEVA = NVL( ORDATEVA , this.w_MVTDTEVA ) 
      this.w_MVPREZZO = cp_ROUND(Nvl( ORPREZZO , 0 ), this.w_DECUNI )
      * --- Numero di colli di riga
      this.w_MVNUMCOL = 0
      this.w_MVSCONT1 = Nvl ( ORSCONT1 ,0 )
      this.w_MVSCONT2 = Nvl ( ORSCONT2 ,0 )
      this.w_MVSCONT3 = Nvl ( ORSCONT3 ,0 )
      this.w_MVSCONT4 = Nvl ( ORSCONT4 ,0 )
      this.w_MVFLOMAG = IIF(EMPTY(Nvl ( ORFLOMAG , "X" )),"X",Nvl ( ORFLOMAG , "X" ))
      this.w_MVFLOMAG = IIF(this.w_MVFLOMAG="O","S",this.w_MVFLOMAG)
      this.w_MVCODIVA = NVL(ORCODIVA, SPACE(5))
      this.w_PERIVA = NVL(IVPERIVA, 0)
      this.w_BOLIVA = NVL(IVBOLIVA, " ")
      this.w_MVVALRIG = 0
      this.w_MVIMPACC = 0
      this.w_MVIMPSCO = 0
      this.w_MVVALMAG = 0
      this.w_MVIMPNAZ = 0
      this.w_UNMIS1 = NVL(ARUNMIS1, SPACE(3))
      this.w_UNMIS2 = NVL(ARUNMIS2, SPACE(3))
      this.w_UNMIS3 = this.w_UNMIS1
      this.w_MOLTIP = NVL(ARMOLTIP, 0)
      this.w_MOLTI3 = NVL(ARMOLTIP, 0)
      this.w_OPERAT = NVL(AROPERAT, " ")
      this.w_OPERA3 = NVL(AROPERAT, " ")
      this.w_VALUNI = cp_Round(this.w_MVPREZZO * (1+this.w_MVSCONT1/100)*(1+this.w_MVSCONT2/100)*(1+this.w_MVSCONT3/100)*(1+this.w_MVSCONT4/100),5)
      this.w_MVVALRIG = cp_ROUND(this.w_MVQTAMOV * this.w_VALUNI, this.w_DECTOT)
      this.w_MVVALMAG = this.w_MVVALRIG + this.w_MVIMPSCO + this.w_MVIMPACC
      * --- Se Scorporo a Piede Fattura
      if this.w_MVFLSCOR="S"
        this.w_MVVALMAG = CALNET(this.w_MVVALMAG, NVL(this.w_PERIVA, 0), this.w_DECTOT, this.w_MVCODIVE, this.w_PERIVE)
      endif
      this.w_MVIMPNAZ = this.w_MVVALMAG
      if this.w_MVVALNAZ<>this.w_MVCODVAL
        * --- (converto in valuta di conto e arrotondo ai decimali unitari della valuta di conto)
        this.w_MVIMPNAZ = cp_round(VAL2MON(this.w_MVVALMAG, this.w_MVCAOVAL, this.w_CAONAZ, this.w_MVDATDOC, this.w_MVVALNAZ),g_PERPVL)
      endif
      this.w_MVPESNET = NVL(ARPESNET, 0)
      this.w_MVNOMENC = NVL(ARNOMENC, SPACE(8))
      this.w_MVUMSUPP = NVL(ARUMSUPP, SPACE(3))
      this.w_MVMOLSUP = NVL(ARMOLSUP, 0)
      if Empty(this.w_DOSERIAL)
        FLPROV = "this.oParentObject.w_CODMAG"+ALLTRIM(STR(__TEM__.ORLIVCON,1,0))
        this.w_CODMAG = &FLPROV
      else
        if this.w_MVTIPRIG="R"
          this.w_OMAG = EVL(this.w_MVCODMAG, EVL(this.w_OMAG, EVL(this.w_MVTCOMAG,g_MAGAZI)))
          this.w_CODMAG = CALCMAG(1, this.w_TDFLMGPR, "     ", this.w_MVTCOMAG, this.w_OMAG, this.w_MAGPRE, this.w_MAGCLI)
        else
          this.w_OMAG = SPACE(5)
          this.w_CODMAG = SPACE(5)
        endif
      endif
      this.w_MVCODMAG = this.w_CODMAG
      if not empty(this.w_MVCODMAG)
        * --- Read from MAGAZZIN
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MGPROMAG"+;
            " from "+i_cTable+" MAGAZZIN where ";
                +"MGCODMAG = "+cp_ToStrODBC(this.w_MVCODMAG);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MGPROMAG;
            from (i_cTable) where;
                MGCODMAG = this.w_MVCODMAG;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MGPROMAG = NVL(cp_ToDate(_read_.MGPROMAG),cp_NullValue(_read_.MGPROMAG))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      this.w_MVPROORD = IIF(EMPTY(this.w_MGPROMAG), g_PROAZI, this.w_MGPROMAG)
      this.w_MVCODCEN = this.w_CENCOS
      this.w_MVVOCCOS = SPACE(15)
      this.w_MVTIPATT = "A"
      this.w_MVCODATT = this.oParentObject.w_MVTCOATT
      * --- Totalizzatori
      this.w_TOTALE = this.w_TOTALE + this.w_MVVALRIG
      this.w_TOTMERCE = this.w_TOTMERCE + IIF(this.w_MVFLOMAG="X", this.w_MVVALRIG, 0)
      this.w_TOTALPOS = this.w_TOTALPOS +IIF(this.w_MVFLOMAG="X" And Empty(this.w_MVRIFKIT) And this.w_MVVALRIG>0 AND this.w_FLSERA<>"S" , this.w_MVVALRIG, 0)
      this.w_MVRIFKIT = 0
      this.w_MVTIPRIG = IIF(g_CPIN="S" OR g_REVICRM="S", this.w_MVTIPRIG, "R")
      INSERT INTO GeneApp ; 
 (CPROWNUM, CPROWORD,t_MVTIPRIG, t_MVCODICE, t_MVCODART, t_MVCODVAR, ; 
 t_MVDESART, t_MVDESSUP, t_MVUNIMIS, t_MVCATCON, ; 
 t_MVRIFKIT, t_MVCODCLA, t_MVCONTRA, t_MVCODLIS, ; 
 t_MVQTAMOV, t_MVQTAUM1, t_MVPREZZO, t_MVNUMCOL,; 
 t_MVSCONT1, t_MVSCONT2, t_MVSCONT3, t_MVSCONT4, ; 
 t_MVFLOMAG, t_MVCODIVA, t_PERIVA, t_BOLIVA, t_MVVALRIG, ; 
 t_MVPESNET, t_MVNOMENC, t_MVUMSUPP, t_MVMOLSUP, ; 
 t_MVIMPACC, t_MVIMPSCO, t_MVVALMAG, t_MVIMPNAZ, ; 
 t_MVCODMAG, t_MVCODCEN, t_CODCAA, t_MVVOCCOS, ; 
 t_MVCODCOM, t_MVTIPATT, t_MVCODATT, t_MVDATEVA,; 
 t_MVNAZPRO, t_FLSERA ) ; 
 VALUES (this.w_CPROWNUM, this.w_CPROWORD,this.w_MVTIPRIG, this.w_MVCODICE, this.w_MVCODART, this.w_MVCODVAR, ; 
 this.w_MVDESART, this.w_MVDESSUP, this.w_MVUNIMIS, this.w_MVCATCON, ; 
 this.w_MVRIFKIT, this.w_MVCODCLA, this.w_MVCONTRA, this.w_MVCODLIS, ; 
 this.w_MVQTAMOV, this.w_MVQTAUM1, this.w_MVPREZZO, this.w_MVNUMCOL,; 
 this.w_MVSCONT1, this.w_MVSCONT2, this.w_MVSCONT3, this.w_MVSCONT4, ; 
 this.w_MVFLOMAG, this.w_MVCODIVA, this.w_PERIVA, this.w_BOLIVA, this.w_MVVALRIG, ; 
 this.w_MVPESNET, this.w_MVNOMENC, this.w_MVUMSUPP, this.w_MVMOLSUP, ; 
 this.w_MVIMPACC, this.w_MVIMPSCO, this.w_MVVALMAG, this.w_MVIMPNAZ, ; 
 this.w_MVCODMAG, this.w_MVCODCEN, this.w_CODCAA, this.w_MVVOCCOS, ; 
 this.oParentObject.w_MVTCOMME,this.w_MVTIPATT, this.oParentObject.w_MVTCOATT, this.w_MVDATEVA, ; 
 this.w_CODNAZ, this.w_FLSERA )
    endif
    SELECT GENEORDI
    ENDSCAN
    this.Page_4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Variabili Locali
    * --- Solo per adhoc Revolution --------------------------------
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se non e' il Primo Ingresso, Scrive il Nuovo Documento di Destinazione
    * --- Aggiorna le Spese Accessorie e gli Sconti Finali - Li ricalcola non li prende dalla query
    if this.w_MVFLFOSC <> "S"
      this.w_MVSCONTI = CalSco(this.w_TOTMERCE , this.w_MVSCOCL1 , this.w_MVSCOCL2 , this.w_MVSCOPAG, this.w_DECTOT)
    endif
    this.Page_5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Lettura Percentuale IVA e Bolli Imp. Es.
    if NOT EMPTY(this.w_MVIVAINC)
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVPERIVA,IVBOLIVA"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.w_MVIVAINC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVPERIVA,IVBOLIVA;
          from (i_cTable) where;
              IVCODIVA = this.w_MVIVAINC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PEIINC = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
        this.w_BOLINC = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    if NOT EMPTY(this.w_MVIVAIMB)
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVPERIVA,IVBOLIVA"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.w_MVIVAIMB);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVPERIVA,IVBOLIVA;
          from (i_cTable) where;
              IVCODIVA = this.w_MVIVAIMB;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PEIIMB = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
        this.w_BOLIMB = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    if NOT EMPTY(this.w_MVIVATRA)
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVPERIVA,IVBOLIVA"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.w_MVIVATRA);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVPERIVA,IVBOLIVA;
          from (i_cTable) where;
              IVCODIVA = this.w_MVIVATRA;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PEITRA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
        this.w_BOLTRA = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    if NOT EMPTY(this.w_MVIVABOL)
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVBOLIVA"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.w_MVIVABOL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVBOLIVA;
          from (i_cTable) where;
              IVCODIVA = this.w_MVIVABOL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_BOLBOL = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    if NOT EMPTY(NVL(this.w_MVCODIVE,space(3)))
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVBOLIVA,IVPERIVA,IVPERIND"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.w_MVCODIVE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVBOLIVA,IVPERIVA,IVPERIND;
          from (i_cTable) where;
              IVCODIVA = this.w_MVCODIVE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_BOLIVE = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
        this.w_PERIVE = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
        this.w_INDIVE = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    if g_typebutton=11
      * --- Read from DES_DIVE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DES_DIVE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "DDTIPOPE"+;
          " from "+i_cTable+" DES_DIVE where ";
              +"DDTIPCON = "+cp_ToStrODBC("C");
              +" and DDCODICE = "+cp_ToStrODBC(this.w_MVCODCON);
              +" and DDCODDES = "+cp_ToStrODBC(this.w_MVCODDES);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          DDTIPOPE;
          from (i_cTable) where;
              DDTIPCON = "C";
              and DDCODICE = this.w_MVCODCON;
              and DDCODDES = this.w_MVCODDES;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MVTIPOPE = NVL(cp_ToDate(_read_.DDTIPOPE),cp_NullValue(_read_.DDTIPOPE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from TIP_DOCU
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TDRATEVA"+;
          " from "+i_cTable+" TIP_DOCU where ";
              +"TDTIPDOC = "+cp_ToStrODBC(this.w_MVTIPDOC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TDRATEVA;
          from (i_cTable) where;
              TDTIPDOC = this.w_MVTIPDOC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_RATEVA = NVL(cp_ToDate(_read_.TDRATEVA),cp_NullValue(_read_.TDRATEVA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      * --- Read from TIP_DOCU
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TD_SEGNO,TDCODLIS"+;
          " from "+i_cTable+" TIP_DOCU where ";
              +"TDTIPDOC = "+cp_ToStrODBC(this.w_MVTIPDOC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TD_SEGNO,TDCODLIS;
          from (i_cTable) where;
              TDTIPDOC = this.w_MVTIPDOC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MV_SEGNO = NVL(cp_ToDate(_read_.TD_SEGNO),cp_NullValue(_read_.TD_SEGNO))
        this.w_DOCLIS = NVL(cp_ToDate(_read_.TDCODLIS),cp_NullValue(_read_.TDCODLIS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZFLVEBD"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZFLVEBD;
        from (i_cTable) where;
            AZCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MVFLVABD = NVL(cp_ToDate(_read_.AZFLVEBD),cp_NullValue(_read_.AZFLVEBD))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_MVIVACAU = g_COICAU
    * --- Calcola Sconti su Omaggio
    this.w_MVFLSCOM = IIF(g_FLSCOM="S" AND this.w_MVDATDOC<g_DTSCOM AND !empty(nvl(g_DTSCOM,cp_CharToDate("  /  /    ")))," ",g_FLSCOM)
    * --- Calcoli Finali
    * --- Calcola MVSERIAL, MVNUMREG, MVNUMDOC
    this.w_MVSERIAL = SPACE(10)
    this.oParentObject.w_MVNUMREG = 0
    this.w_CPROWNUM = 0
    this.w_MVNUMRIF = -20
    this.w_RIFKIT = 0
    * --- Try
    local bErr_048C9640
    bErr_048C9640=bTrsErr
    this.Try_048C9640()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Registra errore su cursore Resoconto
      select Resoconto
      REPLACE MSG with ah_msgformat("L'ordine non � stato generato a causa dell'errore:%0%1", EVL(i_ErrMsg, message()))
      REPLACE TIPO with "E"
    endif
    bTrsErr=bTrsErr or bErr_048C9640
    * --- End
    * --- Reinizializza le Variabili di Lavoro
    this.w_TOTMERCE = 0
    this.w_TOTALPOS = 0
    this.w_MVIVAIMB = SPACE(5)
    this.w_MVACIVA1 = SPACE(5)
    this.w_MVAIMPS1 = 0
    this.w_TOTALE = 0
    this.w_MVIVAINC = SPACE(5)
    this.w_MVACIVA2 = SPACE(5)
    this.w_MVAIMPS2 = 0
    this.w_MVSPEINC = 0
    this.w_MVIVATRA = SPACE(5)
    this.w_MVACIVA3 = SPACE(5)
    this.w_MVAIMPS3 = 0
    this.w_MVSPEIMB = 0
    this.w_MVIVABOL = SPACE(5)
    this.w_MVACIVA4 = SPACE(5)
    this.w_MVAIMPS4 = 0
    this.w_MVSPETRA = 0
    this.w_MVFLRINC = " "
    this.w_MVACIVA5 = SPACE(5)
    this.w_MVAIMPS5 = 0
    this.w_MVSPEBOL = 0
    this.w_MVFLRIMB = " "
    this.w_MVACIVA6 = SPACE(5)
    this.w_MVAIMPS6 = 0
    this.w_MVFLRTRA = " "
    this.w_MVAFLOM1 = SPACE(1)
    this.w_MVAIMPN1 = 0
    this.w_TOTIMPON = 0
    this.w_MVTCOCEN = SPACE(15)
    this.w_MVAFLOM2 = SPACE(1)
    this.w_MVAIMPN2 = 0
    this.w_TOTIMPOS = 0
    this.w_MVAFLOM3 = SPACE(1)
    this.w_MVAIMPN3 = 0
    this.w_TOTFATTU = 0
    this.w_MVAFLOM4 = SPACE(1)
    this.w_MVAIMPN4 = 0
    this.w_MVIMPARR = 0
    this.w_MVAFLOM5 = SPACE(1)
    this.w_MVAIMPN5 = 0
    this.w_MVACCPRE = 0
    this.w_MVAFLOM6 = SPACE(1)
    this.w_MVAIMPN6 = 0
    this.w_MVTOTRIT = 0
    this.w_MVTOTENA = 0
    this.oParentObject.w_MVTCOMME = Space(15)
    this.oParentObject.w_MVTCOATT = Space(15)
    this.w_MVIVAINC = Space(5)
    this.w_MVIVATRA = Space(5)
    this.w_MVIVAIMB = Space(5)
    this.w_MVIVABOL = Space(5)
    this.w_MVCODSPE = Space(3)
    this.w_MVCODVET = Space(5)
    this.w_MVESEUTE = 0
    this.w_MVUMSUPP = Space(3)
    this.w_MVROWRIF = 0
    this.w_MVCODATT = " "
    this.w_MVIMPFIN = 0
  endproc
  proc Try_048C9640()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_MVSERIAL = cp_GetProg("DOC_MAST","SEDOC",this.w_MVSERIAL,i_codazi)
    this.oParentObject.w_MVNUMREG = this.w_MVNUMDOC
    * --- -- Gestione per adhoc Enterprise 4.0
    this.w_MVTCONTR = iif(Empty(this.w_MVTCONTR), Null, this.w_MVTCONTR)
    this.w_MVTCOCEN = iif(Empty(this.w_MVTCOCEN), Null, this.w_MVTCOCEN)
    this.oParentObject.w_MVTCOMME = iif(Empty(this.oParentObject.w_MVTCOMME), Null, this.oParentObject.w_MVTCOMME)
    this.w_MVCODBAN = iif(Empty(this.w_MVCODBAN), Null, this.w_MVCODBAN)
    this.w_MVCODBA2 = iif(Empty(this.w_MVCODBA2), Null, this.w_MVCODBA2)
    this.w_MVRIFDIC = iif(Empty(this.w_MVRIFDIC), Null, this.w_MVRIFDIC)
    this.oParentObject.w_MVTCOATT = iif(Empty(this.oParentObject.w_MVTCOATT), Null, this.oParentObject.w_MVTCOATT)
    this.w_MVIVAINC = iif(Empty(this.w_MVIVAINC), Null, this.w_MVIVAINC)
    this.w_MVIVATRA = iif(Empty(this.w_MVIVATRA), Null, this.w_MVIVATRA)
    this.w_MVIVAIMB = iif(Empty(this.w_MVIVAIMB), Null, this.w_MVIVAIMB)
    this.w_MVIVABOL = iif(Empty(this.w_MVIVABOL), Null, this.w_MVIVABOL)
    this.w_MVCODSPE = iif(Empty(this.w_MVCODSPE), Null, this.w_MVCODSPE)
    this.w_MVCODVET = iif(Empty(this.w_MVCODVET), Null, this.w_MVCODVET)
    this.w_MVESEUTE = iif(Empty(this.w_MVESEUTE), Null, this.w_MVESEUTE)
    * --- Leggo la nazione del cliente
    * --- Read from CONTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ANNAZION"+;
        " from "+i_cTable+" CONTI where ";
            +"ANCODICE = "+cp_ToStrODBC(this.w_MVCODCON);
            +" and ANTIPCON = "+cp_ToStrODBC("C");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ANNAZION;
        from (i_cTable) where;
            ANCODICE = this.w_MVCODCON;
            and ANTIPCON = "C";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ANNAZION = NVL(cp_ToDate(_read_.ANNAZION),cp_NullValue(_read_.ANNAZION))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Leggo il codice valuta
    * --- Read from NAZIONI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.NAZIONI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.NAZIONI_idx,2],.t.,this.NAZIONI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "NACODVAL"+;
        " from "+i_cTable+" NAZIONI where ";
            +"NACODNAZ = "+cp_ToStrODBC(this.w_ANNAZION);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        NACODVAL;
        from (i_cTable) where;
            NACODNAZ = this.w_ANNAZION;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_NACODVAL = NVL(cp_ToDate(_read_.NACODVAL),cp_NullValue(_read_.NACODVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_NACODVAL = iif(Empty(this.w_NACODVAL), Null, this.w_NACODVAL)
    * --- Leggo il tasso di conversione
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VACAOVAL"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_NACODVAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VACAOVAL;
        from (i_cTable) where;
            VACODVAL = this.w_NACODVAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_VACAOVAL = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- -- Fine Gestione per adhoc Enterprise 4.0
    * --- Scrive la Testata
    if Empty(this.w_DOSERIAL)
      FLPROV = "this.oParentObject.w_MVTFRAGG"+ALLTRIM(STR(__TEM__.ORLIVCON,1,0))
      this.w_MVTFRAGG = &FLPROV
      FLPROV = "this.oParentObject.w_MVPRD"+ALLTRIM(STR(__TEM__.ORLIVCON,1,0))
      this.w_MVPRD = &FLPROV
      FLPROV = "this.oParentObject.w_MVFLINTE"+ALLTRIM(STR(__TEM__.ORLIVCON,1,0))
      this.w_MVFLINTE = &FLPROV
      FLPROV = "this.oParentObject.w_MVFLACCO"+ALLTRIM(STR(__TEM__.ORLIVCON,1,0))
      this.w_MVFLACCO = &FLPROV
      FLPROV = "this.oParentObject.w_MVCLADOC"+ALLTRIM(STR(__TEM__.ORLIVCON,1,0))
      this.w_MVCLADOC = &FLPROV
      FLPROV = "this.oParentObject.w_MVCAUCON"+ALLTRIM(STR(__TEM__.ORLIVCON,1,0))
      this.w_MVCAUCON = &FLPROV
      FLPROV = "this.oParentObject.w_CODMAG"+ALLTRIM(STR(__TEM__.ORLIVCON,1,0))
      this.w_CODMAG = &FLPROV
      FLPROV = "this.oParentObject.w_MVTCAMAG"+ALLTRIM(STR(__TEM__.ORLIVCON,1,0))
      this.w_MVTCAMAG = &FLPROV
      FLPROV = "this.oParentObject.w_FLVEAC"+ALLTRIM(STR(__TEM__.ORLIVCON,1,0))
      this.w_FLVEAC = &FLPROV
    endif
    this.w_MVAGG_01 = SPACE(15)
    this.w_MVAGG_02 = SPACE(15)
    this.w_MVAGG_03 = SPACE(15)
    this.w_MVAGG_04 = SPACE(15)
    this.w_MVAGG_05 = {}
    this.w_MVAGG_06 = {}
    this.w_MVASPEST = SPACE(30)
    * --- Insert into DOC_MAST
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOC_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MVTIPORN"+",MVTIPDOC"+",MVTIPCON"+",MVTFRAGG"+",MVTCONTR"+",MVTCOLIS"+",MVTCAMAG"+",MVSERIAL"+",MVRIFDIC"+",MVPRD"+",MVNUMREG"+",MVNUMEST"+",MVNUMDOC"+",MVGENPRO"+",MVGENEFF"+",MVFLVEAC"+",MVEMERIC"+",MVFLSCOR"+",MVFLPROV"+",MVFLINTE"+",MVFLGIOM"+",MVFLCONT"+",MVFLACCO"+",MVDATREG"+",MVDATEST"+",MVDATDOC"+",MVDATCIV"+",MVCONCON"+",MVCODVET"+",MVCODUTE"+",MVCODSPE"+",MVCODPOR"+",MVCODESE"+",MVCODDES"+",MVCODCON"+",MVCODAGE"+",MVCLADOC"+",MVCAUCON"+",MVANNDOC"+",MVALFEST"+",MVALFDOC"+",MVCODAG2"+",MVAGG_01"+",MVAGG_02"+",MVAGG_03"+",MVAGG_04"+",MVAGG_05"+",MVAGG_06"+",MVASPEST"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MVTIPCON),'DOC_MAST','MVTIPORN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVTIPDOC),'DOC_MAST','MVTIPDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVTIPCON),'DOC_MAST','MVTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVTFRAGG),'DOC_MAST','MVTFRAGG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVTCONTR),'DOC_MAST','MVTCONTR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVTCOLIS),'DOC_MAST','MVTCOLIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVTCAMAG),'DOC_MAST','MVTCAMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVSERIAL),'DOC_MAST','MVSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'DOC_MAST','MVRIFDIC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVPRD),'DOC_MAST','MVPRD');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVNUMREG),'DOC_MAST','MVNUMREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVNUMEST),'DOC_MAST','MVNUMEST');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVNUMDOC),'DOC_MAST','MVNUMDOC');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVGENPRO');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVGENEFF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLVEAC),'DOC_MAST','MVFLVEAC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVEMERIC),'DOC_MAST','MVEMERIC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLSCOR),'DOC_MAST','MVFLSCOR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLPROV),'DOC_MAST','MVFLPROV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLINTE),'DOC_MAST','MVFLINTE');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVFLGIOM');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVFLCONT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLACCO),'DOC_MAST','MVFLACCO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVDATREG),'DOC_MAST','MVDATREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVDATEST),'DOC_MAST','MVDATEST');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVDATDOC),'DOC_MAST','MVDATDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVDATCIV),'DOC_MAST','MVDATCIV');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVCONCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODVET),'DOC_MAST','MVCODVET');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVCODUTE),'DOC_MAST','MVCODUTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODSPE),'DOC_MAST','MVCODSPE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODPOR),'DOC_MAST','MVCODPOR');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVCODESE),'DOC_MAST','MVCODESE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODDES),'DOC_MAST','MVCODDES');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODCON),'DOC_MAST','MVCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODAGE),'DOC_MAST','MVCODAGE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCLADOC),'DOC_MAST','MVCLADOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCAUCON),'DOC_MAST','MVCAUCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVANNDOC),'DOC_MAST','MVANNDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVALFEST),'DOC_MAST','MVALFEST');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVALFDOC),'DOC_MAST','MVALFDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODAG2),'DOC_MAST','MVCODAG2');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVAGG_01),'DOC_MAST','MVAGG_01');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVAGG_02),'DOC_MAST','MVAGG_02');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVAGG_03),'DOC_MAST','MVAGG_03');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVAGG_04),'DOC_MAST','MVAGG_04');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVAGG_05),'DOC_MAST','MVAGG_05');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVAGG_06),'DOC_MAST','MVAGG_06');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVASPEST),'DOC_MAST','MVASPEST');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MVTIPORN',this.w_MVTIPCON,'MVTIPDOC',this.w_MVTIPDOC,'MVTIPCON',this.w_MVTIPCON,'MVTFRAGG',this.w_MVTFRAGG,'MVTCONTR',this.w_MVTCONTR,'MVTCOLIS',this.w_MVTCOLIS,'MVTCAMAG',this.w_MVTCAMAG,'MVSERIAL',this.w_MVSERIAL,'MVRIFDIC',SPACE(10),'MVPRD',this.w_MVPRD,'MVNUMREG',this.oParentObject.w_MVNUMREG,'MVNUMEST',this.w_MVNUMEST)
      insert into (i_cTable) (MVTIPORN,MVTIPDOC,MVTIPCON,MVTFRAGG,MVTCONTR,MVTCOLIS,MVTCAMAG,MVSERIAL,MVRIFDIC,MVPRD,MVNUMREG,MVNUMEST,MVNUMDOC,MVGENPRO,MVGENEFF,MVFLVEAC,MVEMERIC,MVFLSCOR,MVFLPROV,MVFLINTE,MVFLGIOM,MVFLCONT,MVFLACCO,MVDATREG,MVDATEST,MVDATDOC,MVDATCIV,MVCONCON,MVCODVET,MVCODUTE,MVCODSPE,MVCODPOR,MVCODESE,MVCODDES,MVCODCON,MVCODAGE,MVCLADOC,MVCAUCON,MVANNDOC,MVALFEST,MVALFDOC,MVCODAG2,MVAGG_01,MVAGG_02,MVAGG_03,MVAGG_04,MVAGG_05,MVAGG_06,MVASPEST &i_ccchkf. );
         values (;
           this.w_MVTIPCON;
           ,this.w_MVTIPDOC;
           ,this.w_MVTIPCON;
           ,this.w_MVTFRAGG;
           ,this.w_MVTCONTR;
           ,this.w_MVTCOLIS;
           ,this.w_MVTCAMAG;
           ,this.w_MVSERIAL;
           ,SPACE(10);
           ,this.w_MVPRD;
           ,this.oParentObject.w_MVNUMREG;
           ,this.w_MVNUMEST;
           ,this.w_MVNUMDOC;
           ," ";
           ," ";
           ,this.w_MVFLVEAC;
           ,this.w_MVEMERIC;
           ,this.w_MVFLSCOR;
           ,this.w_MVFLPROV;
           ,this.w_MVFLINTE;
           ," ";
           ," ";
           ,this.w_MVFLACCO;
           ,this.w_MVDATREG;
           ,this.w_MVDATEST;
           ,this.w_MVDATDOC;
           ,this.oParentObject.w_MVDATCIV;
           ," ";
           ,this.w_MVCODVET;
           ,this.oParentObject.w_MVCODUTE;
           ,this.w_MVCODSPE;
           ,this.w_MVCODPOR;
           ,this.oParentObject.w_MVCODESE;
           ,this.w_MVCODDES;
           ,this.w_MVCODCON;
           ,this.w_MVCODAGE;
           ,this.w_MVCLADOC;
           ,this.w_MVCAUCON;
           ,this.w_MVANNDOC;
           ,this.w_MVALFEST;
           ,this.w_MVALFDOC;
           ,this.w_MVCODAG2;
           ,this.w_MVAGG_01;
           ,this.w_MVAGG_02;
           ,this.w_MVAGG_03;
           ,this.w_MVAGG_04;
           ,this.w_MVAGG_05;
           ,this.w_MVAGG_06;
           ,this.w_MVASPEST;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Se l ordine � stato rifiutato allora nelle note inserisco la causa del rifiuto (le note inserite all atto del rifiuto)
    if __TEM__.ORLIVCON=2
      this.w_MVRIFEST = __TEM__.ORMERISP
    endif
    * --- Write into DOC_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MV__NOTE ="+cp_NullLink(cp_ToStrODBC(this.w_MVRIFEST),'DOC_MAST','MV__NOTE');
      +",MVFLSCOM ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLSCOM),'DOC_MAST','MVFLSCOM');
      +",MVNUMCOR ="+cp_NullLink(cp_ToStrODBC(this.w_NUMCOR),'DOC_MAST','MVNUMCOR');
      +",MVFLSALD ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVFLSALD');
      +",MVFLSCAF ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVFLSCAF');
      +",MVFLBLOC ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVFLBLOC');
      +",MVFLOFFE ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVFLOFFE');
      +",MVFLSFIN ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVFLSFIN');
      +",MVFLVABD ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLVABD),'DOC_MAST','MVFLVABD');
      +",MVFLINTR ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVFLINTR');
      +",MVCODPAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODPAG),'DOC_MAST','MVCODPAG');
      +",MVCODIVE ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODIVE),'DOC_MAST','MVCODIVE');
      +",MVSCONTI ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCONTI),'DOC_MAST','MVSCONTI');
      +",MVSPEINC ="+cp_NullLink(cp_ToStrODBC(this.w_MVSPEINC),'DOC_MAST','MVSPEINC');
      +",MVIVAINC ="+cp_NullLink(cp_ToStrODBC(this.w_MVIVAINC),'DOC_MAST','MVIVAINC');
      +",MVFLRINC ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLRINC),'DOC_MAST','MVFLRINC');
      +",MVSPETRA ="+cp_NullLink(cp_ToStrODBC(this.w_MVSPETRA),'DOC_MAST','MVSPETRA');
      +",MVIVATRA ="+cp_NullLink(cp_ToStrODBC(this.w_MVIVATRA),'DOC_MAST','MVIVATRA');
      +",MVFLRTRA ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLRTRA),'DOC_MAST','MVFLRTRA');
      +",MVSPEIMB ="+cp_NullLink(cp_ToStrODBC(this.w_MVSPEIMB),'DOC_MAST','MVSPEIMB');
      +",MVIVAIMB ="+cp_NullLink(cp_ToStrODBC(this.w_MVIVAIMB),'DOC_MAST','MVIVAIMB');
      +",MVFLRIMB ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLRIMB),'DOC_MAST','MVFLRIMB');
      +",MVSPEBOL ="+cp_NullLink(cp_ToStrODBC(this.w_MVSPEBOL),'DOC_MAST','MVSPEBOL');
      +",MVDATDIV ="+cp_NullLink(cp_ToStrODBC(this.w_MVDATDIV),'DOC_MAST','MVDATDIV');
      +",MVCODBAN ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODBAN),'DOC_MAST','MVCODBAN');
      +",MVVALNAZ ="+cp_NullLink(cp_ToStrODBC(this.w_MVVALNAZ),'DOC_MAST','MVVALNAZ');
      +",MVCODVAL ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODVAL),'DOC_MAST','MVCODVAL');
      +",MVCAOVAL ="+cp_NullLink(cp_ToStrODBC(this.w_MVCAOVAL),'DOC_MAST','MVCAOVAL');
      +",MVSCOCL1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCOCL1),'DOC_MAST','MVSCOCL1');
      +",MVSCOCL2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCOCL2),'DOC_MAST','MVSCOCL2');
      +",MVSCOPAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCOPAG),'DOC_MAST','MVSCOPAG');
      +",MVPRP ="+cp_NullLink(cp_ToStrODBC(this.w_MVPRP),'DOC_MAST','MVPRP');
      +",MVANNPRO ="+cp_NullLink(cp_ToStrODBC(this.w_MVANNPRO),'DOC_MAST','MVANNPRO');
      +",MVFLFOSC ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLFOSC),'DOC_MAST','MVFLFOSC');
      +",MVNOTAGG ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVNOTAGG');
      +",MVCODBA2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODBA2),'DOC_MAST','MVCODBA2');
      +",MVIMPARR ="+cp_NullLink(cp_ToStrODBC(this.w_MVIMPARR),'DOC_MAST','MVIMPARR');
      +",MVIVABOL ="+cp_NullLink(cp_ToStrODBC(this.w_MVIVABOL),'DOC_MAST','MVIVABOL');
      +",MVACCONT ="+cp_NullLink(cp_ToStrODBC(this.w_MVACCONT),'DOC_MAST','MVACCONT');
      +",UTCC ="+cp_NullLink(cp_ToStrODBC(this.w_ORUTCC),'DOC_MAST','UTCC');
      +",UTCV ="+cp_NullLink(cp_ToStrODBC(0),'DOC_MAST','UTCV');
      +",UTDC ="+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'DOC_MAST','UTDC');
      +",UTDV ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'DOC_MAST','UTDV');
      +",MVACIVA1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA1),'DOC_MAST','MVACIVA1');
      +",MVACIVA2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA2),'DOC_MAST','MVACIVA2');
      +",MVACIVA3 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA3),'DOC_MAST','MVACIVA3');
      +",MVACIVA4 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA4),'DOC_MAST','MVACIVA4');
      +",MVACIVA5 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA5),'DOC_MAST','MVACIVA5');
      +",MVACIVA6 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA6),'DOC_MAST','MVACIVA6');
      +",MVAIMPN1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN1),'DOC_MAST','MVAIMPN1');
      +",MVAIMPN2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN2),'DOC_MAST','MVAIMPN2');
      +",MVAIMPN3 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN3),'DOC_MAST','MVAIMPN3');
      +",MVAIMPN4 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN4),'DOC_MAST','MVAIMPN4');
      +",MVAIMPN5 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN5),'DOC_MAST','MVAIMPN5');
      +",MVAIMPN6 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN6),'DOC_MAST','MVAIMPN6');
      +",MVAIMPS1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS1),'DOC_MAST','MVAIMPS1');
      +",MVAIMPS2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS2),'DOC_MAST','MVAIMPS2');
      +",MVAIMPS3 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS3),'DOC_MAST','MVAIMPS3');
      +",MVAIMPS4 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS4),'DOC_MAST','MVAIMPS4');
      +",MVAIMPS5 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS5),'DOC_MAST','MVAIMPS5');
      +",MVAIMPS6 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS6),'DOC_MAST','MVAIMPS6');
      +",MVAFLOM1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM1),'DOC_MAST','MVAFLOM1');
      +",MVAFLOM2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM2),'DOC_MAST','MVAFLOM2');
      +",MVAFLOM3 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM3),'DOC_MAST','MVAFLOM3');
      +",MVAFLOM4 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM4),'DOC_MAST','MVAFLOM4');
      +",MVAFLOM5 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM5),'DOC_MAST','MVAFLOM5');
      +",MVAFLOM6 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM6),'DOC_MAST','MVAFLOM6');
      +",MVACCPRE ="+cp_NullLink(cp_ToStrODBC(this.w_MVACCPRE),'DOC_MAST','MVACCPRE');
      +",MVQTACOL ="+cp_NullLink(cp_ToStrODBC(this.w_MVQTACOL),'DOC_MAST','MVQTACOL');
      +",MVCONCON ="+cp_NullLink(cp_ToStrODBC(this.w_MVCONCON),'DOC_MAST','MVCONCON');
      +",MVRIFDIC ="+cp_NullLink(cp_ToStrODBC(this.w_MVRIFDIC),'DOC_MAST','MVRIFDIC');
      +",MVSERWEB ="+cp_NullLink(cp_ToStrODBC(this.w_ORSERIAL),'DOC_MAST','MVSERWEB');
      +",MVFLCAPA ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLCAPA),'DOC_MAST','MVFLCAPA');
      +",MVIVACAU ="+cp_NullLink(cp_ToStrODBC(this.w_MVIVACAU),'DOC_MAST','MVIVACAU');
          +i_ccchkf ;
      +" where ";
          +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
             )
    else
      update (i_cTable) set;
          MV__NOTE = this.w_MVRIFEST;
          ,MVFLSCOM = this.w_MVFLSCOM;
          ,MVNUMCOR = this.w_NUMCOR;
          ,MVFLSALD = " ";
          ,MVFLSCAF = " ";
          ,MVFLBLOC = " ";
          ,MVFLOFFE = " ";
          ,MVFLSFIN = " ";
          ,MVFLVABD = this.w_MVFLVABD;
          ,MVFLINTR = " ";
          ,MVCODPAG = this.w_MVCODPAG;
          ,MVCODIVE = this.w_MVCODIVE;
          ,MVSCONTI = this.w_MVSCONTI;
          ,MVSPEINC = this.w_MVSPEINC;
          ,MVIVAINC = this.w_MVIVAINC;
          ,MVFLRINC = this.w_MVFLRINC;
          ,MVSPETRA = this.w_MVSPETRA;
          ,MVIVATRA = this.w_MVIVATRA;
          ,MVFLRTRA = this.w_MVFLRTRA;
          ,MVSPEIMB = this.w_MVSPEIMB;
          ,MVIVAIMB = this.w_MVIVAIMB;
          ,MVFLRIMB = this.w_MVFLRIMB;
          ,MVSPEBOL = this.w_MVSPEBOL;
          ,MVDATDIV = this.w_MVDATDIV;
          ,MVCODBAN = this.w_MVCODBAN;
          ,MVVALNAZ = this.w_MVVALNAZ;
          ,MVCODVAL = this.w_MVCODVAL;
          ,MVCAOVAL = this.w_MVCAOVAL;
          ,MVSCOCL1 = this.w_MVSCOCL1;
          ,MVSCOCL2 = this.w_MVSCOCL2;
          ,MVSCOPAG = this.w_MVSCOPAG;
          ,MVPRP = this.w_MVPRP;
          ,MVANNPRO = this.w_MVANNPRO;
          ,MVFLFOSC = this.w_MVFLFOSC;
          ,MVNOTAGG = " ";
          ,MVCODBA2 = this.w_MVCODBA2;
          ,MVIMPARR = this.w_MVIMPARR;
          ,MVIVABOL = this.w_MVIVABOL;
          ,MVACCONT = this.w_MVACCONT;
          ,UTCC = this.w_ORUTCC;
          ,UTCV = 0;
          ,UTDC = SetInfoDate( g_CALUTD );
          ,UTDV = cp_CharToDate("  -  -    ");
          ,MVACIVA1 = this.w_MVACIVA1;
          ,MVACIVA2 = this.w_MVACIVA2;
          ,MVACIVA3 = this.w_MVACIVA3;
          ,MVACIVA4 = this.w_MVACIVA4;
          ,MVACIVA5 = this.w_MVACIVA5;
          ,MVACIVA6 = this.w_MVACIVA6;
          ,MVAIMPN1 = this.w_MVAIMPN1;
          ,MVAIMPN2 = this.w_MVAIMPN2;
          ,MVAIMPN3 = this.w_MVAIMPN3;
          ,MVAIMPN4 = this.w_MVAIMPN4;
          ,MVAIMPN5 = this.w_MVAIMPN5;
          ,MVAIMPN6 = this.w_MVAIMPN6;
          ,MVAIMPS1 = this.w_MVAIMPS1;
          ,MVAIMPS2 = this.w_MVAIMPS2;
          ,MVAIMPS3 = this.w_MVAIMPS3;
          ,MVAIMPS4 = this.w_MVAIMPS4;
          ,MVAIMPS5 = this.w_MVAIMPS5;
          ,MVAIMPS6 = this.w_MVAIMPS6;
          ,MVAFLOM1 = this.w_MVAFLOM1;
          ,MVAFLOM2 = this.w_MVAFLOM2;
          ,MVAFLOM3 = this.w_MVAFLOM3;
          ,MVAFLOM4 = this.w_MVAFLOM4;
          ,MVAFLOM5 = this.w_MVAFLOM5;
          ,MVAFLOM6 = this.w_MVAFLOM6;
          ,MVACCPRE = this.w_MVACCPRE;
          ,MVQTACOL = this.w_MVQTACOL;
          ,MVCONCON = this.w_MVCONCON;
          ,MVRIFDIC = this.w_MVRIFDIC;
          ,MVSERWEB = this.w_ORSERIAL;
          ,MVFLCAPA = this.w_MVFLCAPA;
          ,MVIVACAU = this.w_MVIVACAU;
          &i_ccchkf. ;
       where;
          MVSERIAL = this.w_MVSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Cicla sulle Righe Documento
    ah_Msg("Generazione documento:%1",.T.,.F.,.F.,ALLTRIM(STR(this.w_MVNUMDOC,15)) )
    SELECT GeneApp
    GO TOP
    SCAN FOR t_MVTIPRIG<>" " AND NOT EMPTY(t_MVCODICE)
    * --- Scrive il Dettaglio
    if NVL(CPROWNUM,-1)>0
      this.w_CPROWNUM = CPROWNUM
      this.w_MVROWWEB = CPROWNUM
    else
      SELECT MAX(CPROWNUM)+1 AS CPROWNUM FROM GeneApp into cursor Appoggio
      if USED("Appoggio")
        SELECT Appoggio
        this.w_CPROWNUM = CPROWNUM
        USE
      else
        this.w_CPROWNUM = this.w_CPROWNUM+1
      endif
      SELECT GeneApp
      REPLACE CPROWNUM WITH this.w_CPROWNUM
      this.w_MVROWWEB = 0
    endif
    this.w_CPROWORD = CPROWORD
    this.w_MVTIPRIG = t_MVTIPRIG
    this.w_MVUNIMIS = t_MVUNIMIS
    if this.w_MVTIPRIG<>"D"
      this.w_MVCODICE = LEFT(t_MVCODICE,20)
      this.w_MVCODART = t_MVCODART
    else
      this.w_MVCODICE = LEFT(g_ARTDES,20)
      this.w_MVCODART = IIF(EMPTY(ALLTRIM(this.w_ARTCODIC)),g_ARTDES,this.w_ARTCODIC)
      this.w_MVUNIMIS = null
    endif
    * --- Read from ART_ICOL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ARUTISER,ARDATINT"+;
        " from "+i_cTable+" ART_ICOL where ";
            +"ARCODART = "+cp_ToStrODBC(this.w_MVCODART);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ARUTISER,ARDATINT;
        from (i_cTable) where;
            ARCODART = this.w_MVCODART;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ARUTISER = NVL(cp_ToDate(_read_.ARUTISER),cp_NullValue(_read_.ARUTISER))
      this.w_ARDATINT = NVL(cp_ToDate(_read_.ARDATINT),cp_NullValue(_read_.ARDATINT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_MVFLTRAS = iif(this.w_ARUTISER="S","S",IIF( this.w_ARDATINT="S","Z",IIF( this.w_ARDATINT="F"," ",IIF( this.w_ARDATINT="I","I","S"))))
    this.w_MVCODVAR = t_MVCODVAR
    this.w_MVDESART = t_MVDESART
    this.w_MVDESSUP = t_MVDESSUP
    this.w_MVUNIMIS = t_MVUNIMIS
    this.w_MVCATCON = t_MVCATCON
    this.w_MVCODCLA = t_MVCODCLA
    this.w_MVCONTRA = t_MVCONTRA
    this.w_MVCODLIS = t_MVCODLIS
    this.w_MVQTAMOV = t_MVQTAMOV
    this.w_MVQTAUM1 = t_MVQTAUM1
    this.w_MVPREZZO = t_MVPREZZO
    this.w_MVNUMCOL = t_MVNUMCOL
    this.w_MVIMPACC = t_MVIMPACC
    this.w_MVIMPSCO = t_MVIMPSCO
    this.w_MVVALMAG = t_MVVALMAG
    this.w_MVIMPNAZ = t_MVIMPNAZ
    this.w_MVIMPPRO = t_MVIMPPRO
    this.w_MVPERPRO = t_MVPERPRO
    this.w_MVSCONT1 = t_MVSCONT1
    this.w_MVSCONT2 = t_MVSCONT2
    this.w_MVSCONT3 = t_MVSCONT3
    this.w_MVSCONT4 = t_MVSCONT4
    this.w_MVFLOMAG = t_MVFLOMAG
    this.w_MVCODIVA = t_MVCODIVA
    this.w_MVVALRIG = t_MVVALRIG
    this.w_MVPESNET = t_MVPESNET
    this.w_MVNOMENC = t_MVNOMENC
    this.w_MVUMSUPP = t_MVUMSUPP
    this.w_MVMOLSUP = t_MVMOLSUP
    this.w_MVCODMAG = IIF(this.w_MVTIPRIG="A", null, t_MVCODMAG)
    this.w_MVCODCEN = IIF(this.w_MVTIPRIG="D", null, t_MVCODCEN)
    this.w_CODCAA = t_CODCAA
    this.w_MVVOCCOS = t_MVVOCCOS
    this.w_MVTIPATT = t_MVTIPATT
    this.w_MVCODATT = t_MVCODATT
    this.w_MVDATEVA = t_MVDATEVA
    this.w_MVRIFKIT = t_MVRIFKIT
    this.w_MVCAUMAG = this.w_MVTCAMAG
    this.w_MVFLORDI = this.w_MFLORDI
    this.w_MVFLIMPE = this.w_MFLIMPE
    this.w_MVFLELGM = this.w_MFLELGM
    this.w_MVFLELAN = this.w_MFLELAN
    this.w_CODNAZ = t_MVNAZPRO
    this.w_MV_FLAGG = SPACE(1)
    this.w_MVVALULT = IIF(this.w_MVQTAUM1=0, 0, cp_ROUND(this.w_MVIMPNAZ/this.w_MVQTAUM1, this.w_DECUNI))
    this.w_MVKEYSAL = IIF(this.w_MVTIPRIG="R" AND this.w_MFLAVAL<>"N", this.w_MVCODART, SPACE(20))
    this.w_MVCODMAG = IIF(EMPTY(this.w_MVKEYSAL), null, this.w_MVCODMAG)
    this.w_MVTIPPRO = IIF(g_CALPRO="DI","DC",IIF(g_CALPRO="GD","ST","CT"))
    this.w_MVTIPPR2 = IIF(g_CALPRO="DI","DC",IIF(g_CALPRO="GD","ST","CT"))
    * --- Gestione Analitica
    this.w_MVVOCCEN = SPACE(15)
    this.w_TIPDOC = this.w_MVTIPDOC
    this.w_LOCENCOS = this.w_CENCOS
    GSCP_BGR(this,"VOCI ANALITICA")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Gestione per adhoc Enterprise 4.0
    this.w_MVUMSUPP = iif(isNull(this.w_MVUMSUPP), " ", this.w_MVUMSUPP)
    this.oParentObject.w_MVTCOMME = iif(Empty(this.oParentObject.w_MVTCOMME), Null, this.oParentObject.w_MVTCOMME)
    this.w_MVROWRIF = iif(Empty(this.w_MVROWRIF), Null, this.w_MVROWRIF)
    this.w_MVCODATT = iif(Empty(this.w_MVCODATT), Null, this.w_MVCODATT)
    * --- --
    * --- Se la riga � un kit-padre memorizzo il suo cprownum per poi inserirlo nei componenti
    if g_CPIN<>"S"
      if this.w_MVRIFKIT>=0
        this.w_RIFKIT = this.w_CPROWNUM
        this.w_MVRIFKIT = 0
      else
        this.w_MVRIFKIT = this.w_RIFKIT
      endif
    endif
    * --- Insert into DOC_DETT
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOC_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MVVALRIG"+",MVVALMAG"+",MVUNIMIS"+",MVTIPRIG"+",MVSERRIF"+",MVSERIAL"+",MVSCONT4"+",MVSCONT3"+",MVSCONT2"+",MVSCONT1"+",MVROWRIF"+",MVQTAUM1"+",MVQTASAL"+",MVQTAMOV"+",MVPREZZO"+",MVPESNET"+",MVNUMRIF"+",MVNUMCOL"+",MVNOMENC"+",MVMOLSUP"+",MVIMPSCO"+",MVIMPNAZ"+",MVIMPACC"+",MVFLTRAS"+",MVFLRAGG"+",MVFLOMAG"+",MVFLARIF"+",MVDESSUP"+",MVDESART"+",MVCONTRO"+",MVCONTRA"+",MVCONIND"+",MVCODLIS"+",MVCODIVA"+",MVCODICE"+",MVCODCLA"+",MVCODART"+",MVCAUMAG"+",MVCATCON"+",CPROWORD"+",CPROWNUM"+",MVIMPPRO"+",MVPERPRO"+",MVPROORD"+",MVROWWEB"+",MV_FLAGG"+",MVTIPPRO"+",MV_SEGNO"+",MVTIPPR2"+",MVVALULT"+",MVDATOAI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MVVALRIG),'DOC_DETT','MVVALRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVVALMAG),'DOC_DETT','MVVALMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVUNIMIS),'DOC_DETT','MVUNIMIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVTIPRIG),'DOC_DETT','MVTIPRIG');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'DOC_DETT','MVSERRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVSERIAL),'DOC_DETT','MVSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT4),'DOC_DETT','MVSCONT4');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT3),'DOC_DETT','MVSCONT3');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT2),'DOC_DETT','MVSCONT2');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT1),'DOC_DETT','MVSCONT1');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVROWRIF),'DOC_DETT','MVROWRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAUM1),'DOC_DETT','MVQTAUM1');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAUM1),'DOC_DETT','MVQTASAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAMOV),'DOC_DETT','MVQTAMOV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVPREZZO),'DOC_DETT','MVPREZZO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVPESNET),'DOC_DETT','MVPESNET');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVNUMRIF),'DOC_DETT','MVNUMRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVNUMCOL),'DOC_DETT','MVNUMCOL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVNOMENC),'DOC_DETT','MVNOMENC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVMOLSUP),'DOC_DETT','MVMOLSUP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVIMPSCO),'DOC_DETT','MVIMPSCO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVIMPNAZ),'DOC_DETT','MVIMPNAZ');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVIMPACC),'DOC_DETT','MVIMPACC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLTRAS),'DOC_DETT','MVFLTRAS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVTFRAGG),'DOC_DETT','MVFLRAGG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLOMAG),'DOC_DETT','MVFLOMAG');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLARIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVDESSUP),'DOC_DETT','MVDESSUP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVDESART),'DOC_DETT','MVDESART');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(15)),'DOC_DETT','MVCONTRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCONTRA),'DOC_DETT','MVCONTRA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCONIND),'DOC_DETT','MVCONIND');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODLIS),'DOC_DETT','MVCODLIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODIVA),'DOC_DETT','MVCODIVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODICE),'DOC_DETT','MVCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODCLA),'DOC_DETT','MVCODCLA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODART),'DOC_DETT','MVCODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCAUMAG),'DOC_DETT','MVCAUMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCATCON),'DOC_DETT','MVCATCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'DOC_DETT','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'DOC_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVIMPPRO),'DOC_DETT','MVIMPPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVPERPRO),'DOC_DETT','MVPERPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVPROORD),'DOC_DETT','MVPROORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVROWWEB ),'DOC_DETT','MVROWWEB');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MV_FLAGG),'DOC_DETT','MV_FLAGG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVTIPPRO),'DOC_DETT','MVTIPPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MV_SEGNO),'DOC_DETT','MV_SEGNO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVTIPPR2),'DOC_DETT','MVTIPPR2');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVVALULT),'DOC_DETT','MVVALULT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVDATOAI),'DOC_DETT','MVDATOAI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MVVALRIG',this.w_MVVALRIG,'MVVALMAG',this.w_MVVALMAG,'MVUNIMIS',this.w_MVUNIMIS,'MVTIPRIG',this.w_MVTIPRIG,'MVSERRIF',SPACE(10),'MVSERIAL',this.w_MVSERIAL,'MVSCONT4',this.w_MVSCONT4,'MVSCONT3',this.w_MVSCONT3,'MVSCONT2',this.w_MVSCONT2,'MVSCONT1',this.w_MVSCONT1,'MVROWRIF',this.w_MVROWRIF,'MVQTAUM1',this.w_MVQTAUM1)
      insert into (i_cTable) (MVVALRIG,MVVALMAG,MVUNIMIS,MVTIPRIG,MVSERRIF,MVSERIAL,MVSCONT4,MVSCONT3,MVSCONT2,MVSCONT1,MVROWRIF,MVQTAUM1,MVQTASAL,MVQTAMOV,MVPREZZO,MVPESNET,MVNUMRIF,MVNUMCOL,MVNOMENC,MVMOLSUP,MVIMPSCO,MVIMPNAZ,MVIMPACC,MVFLTRAS,MVFLRAGG,MVFLOMAG,MVFLARIF,MVDESSUP,MVDESART,MVCONTRO,MVCONTRA,MVCONIND,MVCODLIS,MVCODIVA,MVCODICE,MVCODCLA,MVCODART,MVCAUMAG,MVCATCON,CPROWORD,CPROWNUM,MVIMPPRO,MVPERPRO,MVPROORD,MVROWWEB,MV_FLAGG,MVTIPPRO,MV_SEGNO,MVTIPPR2,MVVALULT,MVDATOAI &i_ccchkf. );
         values (;
           this.w_MVVALRIG;
           ,this.w_MVVALMAG;
           ,this.w_MVUNIMIS;
           ,this.w_MVTIPRIG;
           ,SPACE(10);
           ,this.w_MVSERIAL;
           ,this.w_MVSCONT4;
           ,this.w_MVSCONT3;
           ,this.w_MVSCONT2;
           ,this.w_MVSCONT1;
           ,this.w_MVROWRIF;
           ,this.w_MVQTAUM1;
           ,this.w_MVQTAUM1;
           ,this.w_MVQTAMOV;
           ,this.w_MVPREZZO;
           ,this.w_MVPESNET;
           ,this.w_MVNUMRIF;
           ,this.w_MVNUMCOL;
           ,this.w_MVNOMENC;
           ,this.w_MVMOLSUP;
           ,this.w_MVIMPSCO;
           ,this.w_MVIMPNAZ;
           ,this.w_MVIMPACC;
           ,this.w_MVFLTRAS;
           ,this.w_MVTFRAGG;
           ,this.w_MVFLOMAG;
           ," ";
           ,this.w_MVDESSUP;
           ,this.w_MVDESART;
           ,SPACE(15);
           ,this.w_MVCONTRA;
           ,this.w_MVCONIND;
           ,this.w_MVCODLIS;
           ,this.w_MVCODIVA;
           ,this.w_MVCODICE;
           ,this.w_MVCODCLA;
           ,this.w_MVCODART;
           ,this.w_MVCAUMAG;
           ,this.w_MVCATCON;
           ,this.w_CPROWORD;
           ,this.w_CPROWNUM;
           ,this.w_MVIMPPRO;
           ,this.w_MVPERPRO;
           ,this.w_MVPROORD;
           ,this.w_MVROWWEB ;
           ,this.w_MV_FLAGG;
           ,this.w_MVTIPPRO;
           ,this.w_MV_SEGNO;
           ,this.w_MVTIPPR2;
           ,this.w_MVVALULT;
           ,this.w_MVDATOAI;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    if left(alltrim(strtran(lower(g_VERSION), "rel.","")),3) = "1.2" 
      * --- Gestionale ad hoc Revolution Rel. 1.2 + SP3 o SP4
      * --- Write into DOC_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVCAUMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVCAUMAG),'DOC_DETT','MVCAUMAG');
        +",MVCAUCOL ="+cp_NullLink(cp_ToStrODBC(SPACE(5)),'DOC_DETT','MVCAUCOL');
        +",MVCODMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODMAG),'DOC_DETT','MVCODMAG');
        +",MVCODMAT ="+cp_NullLink(cp_ToStrODBC(SPACE(5)),'DOC_DETT','MVCODMAT');
        +",MVFLCASC ="+cp_NullLink(cp_ToStrODBC(this.w_MFLCASC),'DOC_DETT','MVFLCASC');
        +",MVF2CASC ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVF2CASC');
        +",MVFLORDI ="+cp_NullLink(cp_ToStrODBC(this.w_MFLORDI),'DOC_DETT','MVFLORDI');
        +",MVF2ORDI ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVF2ORDI');
        +",MVFLIMPE ="+cp_NullLink(cp_ToStrODBC(this.w_MFLIMPE),'DOC_DETT','MVFLIMPE');
        +",MVF2IMPE ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVF2IMPE');
        +",MVFLRISE ="+cp_NullLink(cp_ToStrODBC(this.w_MFLRISE),'DOC_DETT','MVFLRISE');
        +",MVF2RISE ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVF2RISE');
        +",MVTIPATT ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPATT),'DOC_DETT','MVTIPATT');
        +",MVCODATT ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODATT),'DOC_DETT','MVCODATT');
        +",MVKEYSAL ="+cp_NullLink(cp_ToStrODBC(this.w_MVKEYSAL),'DOC_DETT','MVKEYSAL');
        +",MVIMPEVA ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVIMPEVA');
        +",MVFLERIF ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLERIF');
        +",MVFLARIF ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLARIF');
        +",MVFLELGM ="+cp_NullLink(cp_ToStrODBC(this.w_MFLELGM),'DOC_DETT','MVFLELGM');
        +",MVFLELAN ="+cp_NullLink(cp_ToStrODBC(this.w_MFLELAN),'DOC_DETT','MVFLELAN');
        +",MVQTAEVA ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAEVA');
        +",MVQTAEV1 ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAEV1');
        +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLEVAS');
        +",MVDATEVA ="+cp_NullLink(cp_ToStrODBC(this.w_MVDATEVA),'DOC_DETT','MVDATEVA');
        +",MVVOCCEN ="+cp_NullLink(cp_ToStrODBC(this.w_MVVOCCEN),'DOC_DETT','MVVOCCEN');
        +",MVCODCEN ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODCEN),'DOC_DETT','MVCODCEN');
        +",MVCODCOM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVTCOMME),'DOC_DETT','MVCODCOM');
        +",MVFLRIPA ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLRIPA');
        +",MVNAZPRO ="+cp_NullLink(cp_ToStrODBC(this.w_CODNAZ),'DOC_DETT','MVNAZPRO');
        +",MVFLTRAS ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLTRAS),'DOC_DETT','MVFLTRAS');
            +i_ccchkf ;
        +" where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
            +" and MVNUMRIF = "+cp_ToStrODBC(this.w_MVNUMRIF);
               )
      else
        update (i_cTable) set;
            MVCAUMAG = this.w_MVCAUMAG;
            ,MVCAUCOL = SPACE(5);
            ,MVCODMAG = this.w_MVCODMAG;
            ,MVCODMAT = SPACE(5);
            ,MVFLCASC = this.w_MFLCASC;
            ,MVF2CASC = " ";
            ,MVFLORDI = this.w_MFLORDI;
            ,MVF2ORDI = " ";
            ,MVFLIMPE = this.w_MFLIMPE;
            ,MVF2IMPE = " ";
            ,MVFLRISE = this.w_MFLRISE;
            ,MVF2RISE = " ";
            ,MVTIPATT = this.w_MVTIPATT;
            ,MVCODATT = this.w_MVCODATT;
            ,MVKEYSAL = this.w_MVKEYSAL;
            ,MVIMPEVA = 0;
            ,MVFLERIF = " ";
            ,MVFLARIF = " ";
            ,MVFLELGM = this.w_MFLELGM;
            ,MVFLELAN = this.w_MFLELAN;
            ,MVQTAEVA = 0;
            ,MVQTAEV1 = 0;
            ,MVFLEVAS = " ";
            ,MVDATEVA = this.w_MVDATEVA;
            ,MVVOCCEN = this.w_MVVOCCEN;
            ,MVCODCEN = this.w_MVCODCEN;
            ,MVCODCOM = this.oParentObject.w_MVTCOMME;
            ,MVFLRIPA = " ";
            ,MVNAZPRO = this.w_CODNAZ;
            ,MVFLTRAS = this.w_MVFLTRAS;
            &i_ccchkf. ;
         where;
            MVSERIAL = this.w_MVSERIAL;
            and CPROWNUM = this.w_CPROWNUM;
            and MVNUMRIF = this.w_MVNUMRIF;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Gestionale ad hoc Revolution Rel. 2.0
      * --- Write into DOC_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVCAUMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVCAUMAG),'DOC_DETT','MVCAUMAG');
        +",MVCAUCOL ="+cp_NullLink(cp_ToStrODBC(SPACE(5)),'DOC_DETT','MVCAUCOL');
        +",MVCODMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODMAG),'DOC_DETT','MVCODMAG');
        +",MVCODMAT ="+cp_NullLink(cp_ToStrODBC(SPACE(5)),'DOC_DETT','MVCODMAT');
        +",MVFLCASC ="+cp_NullLink(cp_ToStrODBC(this.w_MFLCASC),'DOC_DETT','MVFLCASC');
        +",MVF2CASC ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVF2CASC');
        +",MVFLORDI ="+cp_NullLink(cp_ToStrODBC(this.w_MFLORDI),'DOC_DETT','MVFLORDI');
        +",MVF2ORDI ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVF2ORDI');
        +",MVFLIMPE ="+cp_NullLink(cp_ToStrODBC(this.w_MFLIMPE),'DOC_DETT','MVFLIMPE');
        +",MVF2IMPE ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVF2IMPE');
        +",MVFLRISE ="+cp_NullLink(cp_ToStrODBC(this.w_MFLRISE),'DOC_DETT','MVFLRISE');
        +",MVF2RISE ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVF2RISE');
        +",MVTIPATT ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPATT),'DOC_DETT','MVTIPATT');
        +",MVCODATT ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODATT),'DOC_DETT','MVCODATT');
        +",MVKEYSAL ="+cp_NullLink(cp_ToStrODBC(this.w_MVKEYSAL),'DOC_DETT','MVKEYSAL');
        +",MVIMPEVA ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVIMPEVA');
        +",MVFLERIF ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLERIF');
        +",MVFLARIF ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLARIF');
        +",MVFLELGM ="+cp_NullLink(cp_ToStrODBC(this.w_MFLELGM),'DOC_DETT','MVFLELGM');
        +",MVFLELAN ="+cp_NullLink(cp_ToStrODBC(this.w_MFLELAN),'DOC_DETT','MVFLELAN');
        +",MVQTAEVA ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAEVA');
        +",MVQTAEV1 ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAEV1');
        +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLEVAS');
        +",MVDATEVA ="+cp_NullLink(cp_ToStrODBC(this.w_MVDATEVA),'DOC_DETT','MVDATEVA');
        +",MVVOCCEN ="+cp_NullLink(cp_ToStrODBC(this.w_MVVOCCEN),'DOC_DETT','MVVOCCEN');
        +",MVCODCEN ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODCEN),'DOC_DETT','MVCODCEN');
        +",MVCODCOM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVTCOMME),'DOC_DETT','MVCODCOM');
        +",MVFLRIPA ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLRIPA');
        +",MVNAZPRO ="+cp_NullLink(cp_ToStrODBC(this.w_CODNAZ),'DOC_DETT','MVNAZPRO');
        +",MVFLTRAS ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLTRAS),'DOC_DETT','MVFLTRAS');
        +",MVCODUBI ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVCODUBI');
        +",MVFLORCO ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLORCO');
        +",MVFLCOCO ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLCOCO');
        +",MVFLULPV ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLULPV');
        +",MVFLULCA ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLULCA');
        +",MVF2LOTT ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVF2LOTT');
        +",MVFLLOTT ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLLOTT');
            +i_ccchkf ;
        +" where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
            +" and MVNUMRIF = "+cp_ToStrODBC(this.w_MVNUMRIF);
               )
      else
        update (i_cTable) set;
            MVCAUMAG = this.w_MVCAUMAG;
            ,MVCAUCOL = SPACE(5);
            ,MVCODMAG = this.w_MVCODMAG;
            ,MVCODMAT = SPACE(5);
            ,MVFLCASC = this.w_MFLCASC;
            ,MVF2CASC = " ";
            ,MVFLORDI = this.w_MFLORDI;
            ,MVF2ORDI = " ";
            ,MVFLIMPE = this.w_MFLIMPE;
            ,MVF2IMPE = " ";
            ,MVFLRISE = this.w_MFLRISE;
            ,MVF2RISE = " ";
            ,MVTIPATT = this.w_MVTIPATT;
            ,MVCODATT = this.w_MVCODATT;
            ,MVKEYSAL = this.w_MVKEYSAL;
            ,MVIMPEVA = 0;
            ,MVFLERIF = " ";
            ,MVFLARIF = " ";
            ,MVFLELGM = this.w_MFLELGM;
            ,MVFLELAN = this.w_MFLELAN;
            ,MVQTAEVA = 0;
            ,MVQTAEV1 = 0;
            ,MVFLEVAS = " ";
            ,MVDATEVA = this.w_MVDATEVA;
            ,MVVOCCEN = this.w_MVVOCCEN;
            ,MVCODCEN = this.w_MVCODCEN;
            ,MVCODCOM = this.oParentObject.w_MVTCOMME;
            ,MVFLRIPA = " ";
            ,MVNAZPRO = this.w_CODNAZ;
            ,MVFLTRAS = this.w_MVFLTRAS;
            ,MVCODUBI = " ";
            ,MVFLORCO = " ";
            ,MVFLCOCO = " ";
            ,MVFLULPV = " ";
            ,MVFLULCA = " ";
            ,MVF2LOTT = " ";
            ,MVFLLOTT = " ";
            &i_ccchkf. ;
         where;
            MVSERIAL = this.w_MVSERIAL;
            and CPROWNUM = this.w_CPROWNUM;
            and MVNUMRIF = this.w_MVNUMRIF;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Aggiorna Saldi
    if this.w_MVQTAUM1<>0 AND NOT EMPTY(this.w_MVKEYSAL) AND NOT EMPTY(this.w_MVCODMAG)
      if NOT EMPTY(this.w_MFLCASC+this.w_MFLRISE+this.w_MFLORDI+this.w_MFLIMPE)
        * --- Try
        local bErr_048A08A0
        bErr_048A08A0=bTrsErr
        this.Try_048A08A0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_048A08A0
        * --- End
        * --- Write into SALDIART
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDIART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_MFLCASC,'SLQTAPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(this.w_MFLRISE,'SLQTRPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
          i_cOp3=cp_SetTrsOp(this.w_MFLORDI,'SLQTOPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
          i_cOp4=cp_SetTrsOp(this.w_MFLIMPE,'SLQTIPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLQTAPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTAPER');
          +",SLQTRPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTRPER');
          +",SLQTOPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTOPER');
          +",SLQTIPER ="+cp_NullLink(i_cOp4,'SALDIART','SLQTIPER');
              +i_ccchkf ;
          +" where ";
              +"SLCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
              +" and SLCODMAG = "+cp_ToStrODBC(this.w_MVCODMAG);
                 )
        else
          update (i_cTable) set;
              SLQTAPER = &i_cOp1.;
              ,SLQTRPER = &i_cOp2.;
              ,SLQTOPER = &i_cOp3.;
              ,SLQTIPER = &i_cOp4.;
              &i_ccchkf. ;
           where;
              SLCODICE = this.w_MVKEYSAL;
              and SLCODMAG = this.w_MVCODMAG;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARSALCOM"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_MVCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARSALCOM;
            from (i_cTable) where;
                ARCODART = this.w_MVCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_SALCOM="S"
          if empty(nvl(this.oParentObject.w_MVTCOMME,""))
            this.w_COMMAPPO = this.w_COMMDEFA
          else
            this.w_COMMAPPO = this.oParentObject.w_MVTCOMME
          endif
          * --- Try
          local bErr_048AFD20
          bErr_048AFD20=bTrsErr
          this.Try_048AFD20()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_048AFD20
          * --- End
          * --- Write into SALDICOM
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDICOM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_MFLCASC,'SCQTAPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_MFLRISE,'SCQTRPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
            i_cOp3=cp_SetTrsOp(this.w_MFLORDI,'SCQTOPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
            i_cOp4=cp_SetTrsOp(this.w_MFLIMPE,'SCQTIPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
            +",SCQTRPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTRPER');
            +",SCQTOPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTOPER');
            +",SCQTIPER ="+cp_NullLink(i_cOp4,'SALDICOM','SCQTIPER');
                +i_ccchkf ;
            +" where ";
                +"SCCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
                +" and SCCODMAG = "+cp_ToStrODBC(this.w_MVCODMAG);
                +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                   )
          else
            update (i_cTable) set;
                SCQTAPER = &i_cOp1.;
                ,SCQTRPER = &i_cOp2.;
                ,SCQTOPER = &i_cOp3.;
                ,SCQTIPER = &i_cOp4.;
                &i_ccchkf. ;
             where;
                SCCODICE = this.w_MVKEYSAL;
                and SCCODMAG = this.w_MVCODMAG;
                and SCCODCAN = this.w_COMMAPPO;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Errore Aggiornamento Saldi Commessa'
            return
          endif
        endif
      endif
    endif
    SELECT GeneApp
    ENDSCAN
    if g_PROGEN= "S"
      this.w_SERPRO = this.w_MVSERIAL
      this.w_MASSGEN = "S"
      * --- Considero sempre come se precedentemente avesi avuto disattivo
      *     In questo modo mi aggiorna il documento generato nel modo corretto
      this.w_OLDCALPRO = "DI"
      * --- Read from PAR_PROV
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_PROV_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROV_idx,2],.t.,this.PAR_PROV_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PPCALPRO,PPCALSCO,PPLISRIF"+;
          " from "+i_cTable+" PAR_PROV where ";
              +"PPCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PPCALPRO,PPCALSCO,PPLISRIF;
          from (i_cTable) where;
              PPCODAZI = this.w_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PPCALPRO = NVL(cp_ToDate(_read_.PPCALPRO),cp_NullValue(_read_.PPCALPRO))
        this.w_PPCALSCO = NVL(cp_ToDate(_read_.PPCALSCO),cp_NullValue(_read_.PPCALSCO))
        this.w_PPLISRIF = NVL(cp_ToDate(_read_.PPLISRIF),cp_NullValue(_read_.PPLISRIF))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      do GSVE_BPP with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.w_CPROWNUM=0
      * --- NESSUNA RIGA NEL DETTAGLIO
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg="Nessuna riga nel dettaglio"
      i_Error=i_TrsMsg
      return
    else
      * --- Cicla Sul Vettore delle Rate (Calcolato in GSAR_BFA)
      this.w_TOTDOC = 0
      this.w_MINRATA = 0
      this.pDOCINFO=createobject("DOCOBJ",this)
      this.pDOCINFO.cMVSERIAL = this.w_MVSERIAL
      * --- Rileggo il documento dal database...
      this.pDOCINFO.cRead = "A"
      this.w_RET = CASTIVARATE(.null., this.pDOCINFO)
      if Empty(this.w_RET.nErrorLog)
        * --- Azzero rate importate ...
        this.w_contrate = 0
        this.w_RET.mcRate.gotop()     
        do while not this.w_RET.mcRate.eof()
          this.w_contrate = this.w_contrate + 1
          if NOT EMPTY(this.w_RET.mcRate.DATRAT) AND Nvl(this.w_RET.mcRate.IMPNET,0) <>0
            this.w_TOTDOC = this.w_TOTDOC + this.w_RET.mcRate.IMPNET
            this.w_MINRATA = MIN(this.w_MINRATA, this.w_RET.mcRate.IMPNET)
            * --- Insert into DOC_RATE
            i_nConn=i_TableProp[this.DOC_RATE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_RATE_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOC_RATE_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"RSSERIAL"+",RSNUMRAT"+",RSDATRAT"+",RSIMPRAT"+",RSMODPAG"+",RSFLPROV"+",RSFLSOSP"+",RSDESRIG"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_MVSERIAL),'DOC_RATE','RSSERIAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_contrate),'DOC_RATE','RSNUMRAT');
              +","+cp_NullLink(cp_ToStrODBC(this.w_RET.mcRate.DATRAT),'DOC_RATE','RSDATRAT');
              +","+cp_NullLink(cp_ToStrODBC(this.w_RET.mcRate.IMPNET),'DOC_RATE','RSIMPRAT');
              +","+cp_NullLink(cp_ToStrODBC(this.w_RET.mcRate.MODPAG),'DOC_RATE','RSMODPAG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_RET.mcRate.FLPROV),'DOC_RATE','RSFLPROV');
              +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_RATE','RSFLSOSP');
              +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_RATE','RSDESRIG');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'RSSERIAL',this.w_MVSERIAL,'RSNUMRAT',this.w_contrate,'RSDATRAT',this.w_RET.mcRate.DATRAT,'RSIMPRAT',this.w_RET.mcRate.IMPNET,'RSMODPAG',this.w_RET.mcRate.MODPAG,'RSFLPROV',this.w_RET.mcRate.FLPROV,'RSFLSOSP'," ",'RSDESRIG'," ")
              insert into (i_cTable) (RSSERIAL,RSNUMRAT,RSDATRAT,RSIMPRAT,RSMODPAG,RSFLPROV,RSFLSOSP,RSDESRIG &i_ccchkf. );
                 values (;
                   this.w_MVSERIAL;
                   ,this.w_contrate;
                   ,this.w_RET.mcRate.DATRAT;
                   ,this.w_RET.mcRate.IMPNET;
                   ,this.w_RET.mcRate.MODPAG;
                   ,this.w_RET.mcRate.FLPROV;
                   ," ";
                   ," ";
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
          this.w_RET.mcRate.Next()     
        enddo
        if this.w_TOTDOC<0 && oppure verifica per singola rata: this.w_MINRATA<0
          * --- Documento non Accettato
          if this.w_TOTDOC<0
            this.w_MSGRET = "importo totale negativo"
          else
            this.w_MSGRET = "rate con importo negativo"
          endif
          * --- Raise
          i_Error=ah_msgformat("Errore inserimento rate: %1", this.w_MSGRET)
          return
        endif
        * --- Aggiorno i totali con il risultato di Castiva presente
        *     nei memory cursor dell'oggetto
        this.w_contrate = 1
        this.w_RET.mcCastiva.gotop()     
        do while not this.w_RET.mcCastiva.eof()
          do case
            case this.w_contrate=1
              * --- Write into DOC_MAST
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.DOC_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"MVAIMPN1 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPON),'DOC_MAST','MVAIMPN1');
                +",MVAIMPS1 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPOS),'DOC_MAST','MVAIMPS1');
                +",MVAFLOM1 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.FLOMAG),'DOC_MAST','MVAFLOM1');
                +",MVACIVA1 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.CODIVA),'DOC_MAST','MVACIVA1');
                    +i_ccchkf ;
                +" where ";
                    +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
                       )
              else
                update (i_cTable) set;
                    MVAIMPN1 = this.w_RET.mcCastiva.IMPON;
                    ,MVAIMPS1 = this.w_RET.mcCastiva.IMPOS;
                    ,MVAFLOM1 = this.w_RET.mcCastiva.FLOMAG;
                    ,MVACIVA1 = this.w_RET.mcCastiva.CODIVA;
                    &i_ccchkf. ;
                 where;
                    MVSERIAL = this.w_MVSERIAL;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            case this.w_contrate=2
              * --- Write into DOC_MAST
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.DOC_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"MVAIMPN2 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPON),'DOC_MAST','MVAIMPN2');
                +",MVAIMPS2 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPOS),'DOC_MAST','MVAIMPS2');
                +",MVAFLOM2 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.FLOMAG),'DOC_MAST','MVAFLOM2');
                +",MVACIVA2 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.CODIVA),'DOC_MAST','MVACIVA2');
                    +i_ccchkf ;
                +" where ";
                    +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
                       )
              else
                update (i_cTable) set;
                    MVAIMPN2 = this.w_RET.mcCastiva.IMPON;
                    ,MVAIMPS2 = this.w_RET.mcCastiva.IMPOS;
                    ,MVAFLOM2 = this.w_RET.mcCastiva.FLOMAG;
                    ,MVACIVA2 = this.w_RET.mcCastiva.CODIVA;
                    &i_ccchkf. ;
                 where;
                    MVSERIAL = this.w_MVSERIAL;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            case this.w_contrate=3
              * --- Write into DOC_MAST
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.DOC_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"MVAIMPN3 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPON),'DOC_MAST','MVAIMPN3');
                +",MVAIMPS3 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPOS),'DOC_MAST','MVAIMPS3');
                +",MVAFLOM3 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.FLOMAG),'DOC_MAST','MVAFLOM3');
                +",MVACIVA3 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.CODIVA),'DOC_MAST','MVACIVA3');
                    +i_ccchkf ;
                +" where ";
                    +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
                       )
              else
                update (i_cTable) set;
                    MVAIMPN3 = this.w_RET.mcCastiva.IMPON;
                    ,MVAIMPS3 = this.w_RET.mcCastiva.IMPOS;
                    ,MVAFLOM3 = this.w_RET.mcCastiva.FLOMAG;
                    ,MVACIVA3 = this.w_RET.mcCastiva.CODIVA;
                    &i_ccchkf. ;
                 where;
                    MVSERIAL = this.w_MVSERIAL;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            case this.w_contrate=4
              * --- Write into DOC_MAST
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.DOC_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"MVAIMPN4 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPON),'DOC_MAST','MVAIMPN4');
                +",MVAIMPS4 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPOS),'DOC_MAST','MVAIMPS4');
                +",MVAFLOM4 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.FLOMAG),'DOC_MAST','MVAFLOM4');
                +",MVACIVA4 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.CODIVA),'DOC_MAST','MVACIVA4');
                    +i_ccchkf ;
                +" where ";
                    +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
                       )
              else
                update (i_cTable) set;
                    MVAIMPN4 = this.w_RET.mcCastiva.IMPON;
                    ,MVAIMPS4 = this.w_RET.mcCastiva.IMPOS;
                    ,MVAFLOM4 = this.w_RET.mcCastiva.FLOMAG;
                    ,MVACIVA4 = this.w_RET.mcCastiva.CODIVA;
                    &i_ccchkf. ;
                 where;
                    MVSERIAL = this.w_MVSERIAL;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            case this.w_contrate=5
              * --- Write into DOC_MAST
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.DOC_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"MVAIMPN5 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPON),'DOC_MAST','MVAIMPN5');
                +",MVAIMPS5 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPOS),'DOC_MAST','MVAIMPS5');
                +",MVAFLOM5 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.FLOMAG),'DOC_MAST','MVAFLOM5');
                +",MVACIVA5 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.CODIVA),'DOC_MAST','MVACIVA5');
                    +i_ccchkf ;
                +" where ";
                    +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
                       )
              else
                update (i_cTable) set;
                    MVAIMPN5 = this.w_RET.mcCastiva.IMPON;
                    ,MVAIMPS5 = this.w_RET.mcCastiva.IMPOS;
                    ,MVAFLOM5 = this.w_RET.mcCastiva.FLOMAG;
                    ,MVACIVA5 = this.w_RET.mcCastiva.CODIVA;
                    &i_ccchkf. ;
                 where;
                    MVSERIAL = this.w_MVSERIAL;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            case this.w_contrate=6
              * --- Write into DOC_MAST
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.DOC_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"MVAIMPN6 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPON),'DOC_MAST','MVAIMPN6');
                +",MVAIMPS6 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPOS),'DOC_MAST','MVAIMPS6');
                +",MVAFLOM6 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.FLOMAG),'DOC_MAST','MVAFLOM6');
                +",MVACIVA6 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.CODIVA),'DOC_MAST','MVACIVA6');
                    +i_ccchkf ;
                +" where ";
                    +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
                       )
              else
                update (i_cTable) set;
                    MVAIMPN6 = this.w_RET.mcCastiva.IMPON;
                    ,MVAIMPS6 = this.w_RET.mcCastiva.IMPOS;
                    ,MVAFLOM6 = this.w_RET.mcCastiva.FLOMAG;
                    ,MVACIVA6 = this.w_RET.mcCastiva.CODIVA;
                    &i_ccchkf. ;
                 where;
                    MVSERIAL = this.w_MVSERIAL;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
          endcase
          this.w_contrate = this.w_contrate+1
          this.w_RET.mcCastiva.Next()     
        enddo
        this.w_TOTFATTU = this.w_RET.mcCastiva.oParentObject.w_TOTFATTU
        this.w_MVTOTRIT = this.w_RET.mcCastiva.oParentObject.w_MVTOTRIT
        this.w_MVTOTENA = this.w_RET.mcCastiva.oParentObject.w_MVTOTENA
        this.w_MVIMPFIN = this.w_RET.mcCastiva.oParentObject.w_MVIMPFIN
        this.w_MVACCONT = this.w_RET.mcCastiva.oParentObject.w_MVACCONT
        this.w_MVACCPRE = this.w_RET.mcCastiva.oParentObject.w_MVACCPRE
        this.w_RET.Done()     
        this.w_RET.Destroy()     
        this.w_RET = .NULL.
        this.pDOCINFO.Done()     
        this.pDOCINFO.Destroy()     
        this.pDOCINFO = .NULL.
        if this.w_TOTFATTU-(this.w_MVTOTRIT+this.w_MVTOTENA+this.w_MVIMPFIN)<(this.w_MVACCONT+this.w_MVACCPRE)
          * --- Se Gli Acconti sono Maggiori del Documento Avverte
          * --- Raise
          i_Error=ah_msgformat("Totale acconti e/o importi ritenute superiori al totale documento!")
          return
        endif
      else
        * --- Scrivo errore nel campo di log
        * --- Raise
        i_Error=this.w_RET.nErrorLog
        return
      endif
      * --- Aggiorna Ordini via Web
      * --- Aggiorna RESOCONTO per ORDINI via WEB
      select Resoconto
      REPLACE TIPO with this.w_MVFLPROV
      * --- Delete from ZORDWEBD
      i_nConn=i_TableProp[this.ZORDWEBD_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ZORDWEBD_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"ORSERIAL = "+cp_ToStrODBC(this.w_ORSERIAL);
               )
      else
        delete from (i_cTable) where;
              ORSERIAL = this.w_ORSERIAL;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Delete from ZORDWEBM
      i_nConn=i_TableProp[this.ZORDWEBM_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ZORDWEBM_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"ORSERIAL = "+cp_ToStrODBC(this.w_ORSERIAL);
               )
      else
        delete from (i_cTable) where;
              ORSERIAL = this.w_ORSERIAL;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- commit
      cp_EndTrs(.t.)
      if __TEM__.ORLIVCON=2
        * --- Rifiuta ordine
        this.w_KODOC = this.w_KODOC + 1
      else
        * --- Accetta ordine
        this.w_OKDOC = this.w_OKDOC + 1
      endif
    endif
    * --- Se il gestionale � adhoc Revolution e se � attivo il modulo della distinta base
    *     allora si richiama la pagina 8.
    if g_EACD = "S"
      this.Page_8()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    return
  proc Try_048A08A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDIART
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLCODICE"+",SLCODMAG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MVKEYSAL),'SALDIART','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODMAG),'SALDIART','SLCODMAG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLCODICE',this.w_MVKEYSAL,'SLCODMAG',this.w_MVCODMAG)
      insert into (i_cTable) (SLCODICE,SLCODMAG &i_ccchkf. );
         values (;
           this.w_MVKEYSAL;
           ,this.w_MVCODMAG;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_048AFD20()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICOM
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SCCODICE"+",SCCODMAG"+",SCCODCAN"+",SCCODART"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MVKEYSAL),'SALDICOM','SCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODMAG),'SALDICOM','SCCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COMMAPPO),'SALDICOM','SCCODCAN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODART),'SALDICOM','SCCODART');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SCCODICE',this.w_MVKEYSAL,'SCCODMAG',this.w_MVCODMAG,'SCCODCAN',this.w_COMMAPPO,'SCCODART',this.w_MVCODART)
      insert into (i_cTable) (SCCODICE,SCCODMAG,SCCODCAN,SCCODART &i_ccchkf. );
         values (;
           this.w_MVKEYSAL;
           ,this.w_MVCODMAG;
           ,this.w_COMMAPPO;
           ,this.w_MVCODART;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ripartisce Sconti
    * --- Totalizzatori
    this.w_TIMPSCO = 0
    this.w_PRES = 0
    * --- Cicla sulle Righe Documento
    SELECT GeneApp
    GO TOP
    SCAN FOR t_MVTIPRIG<>" " AND t_MVTIPRIG<>"D" AND NOT EMPTY(t_MVCODICE)
    * --- Riasssegna Sconti
    this.w_MVIMPSCO = 0
    * --- Riasssegna Sconti
    if this.w_MVSCONTI<>0 AND this.w_TOTMERCE<>0 AND t_MVFLOMAG="X"
      this.w_MVIMPSCO = cp_ROUND((t_MVVALRIG * this.w_MVSCONTI) / this.w_TOTMERCE, this.w_DECTOT)
      * --- Riga su cui ripartire l'eventuale Resto
      this.w_TIMPSCO = this.w_TIMPSCO + this.w_MVIMPSCO
      this.w_PRES = IIF(this.w_PRES=0 AND this.w_MVIMPSCO<>0, RECNO(), this.w_PRES)
    endif
    this.w_MVVALMAG = t_MVVALRIG + this.w_MVIMPSCO
    * --- Se Scorporo a Piede Fattura
    if this.w_MVFLSCOR="S"
      this.w_MVVALMAG = CALNET(this.w_MVVALMAG, NVL(t_PERIVA, 0), this.w_DECTOT, this.w_MVCODIVE, this.w_PERIVE)
    endif
    * --- Calcola Provvigioni: 
    *     - Causale Documento gestisce provvigioni
    *     - Nei Parametri Provvigioni � attivo flag generazione su documento
    *     - Nella riga del documento non � attivo il flag ricalcola
    if this.w_FLAGEN = "S" and this.w_PPGENDOC="S"
      * --- Ricalcolo Provvigioni
      this.w_APPCONTRA = NVL(t_MVCONTRA, " ")
      this.w_APPVAR = NVL(t_MVCODVAR," ")
      this.w_APPART = t_MVCODART
      this.w_PRQTAUM1 = t_MVQTAUM1
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARGRUPRO"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.w_APPART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARGRUPRO;
          from (i_cTable) where;
              ARCODART = this.w_APPART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_GRPART = NVL(cp_ToDate(_read_.ARGRUPRO),cp_NullValue(_read_.ARGRUPRO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Batch Calcolo Provvigioni
      do GSVE_BEP with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    this.w_MVIMPNAZ = this.w_MVVALMAG
    if this.w_MVVALNAZ<>this.w_MVCODVAL AND this.w_MVIMPNAZ<>0
      this.w_MVIMPNAZ = cp_round(VAL2MON(this.w_MVVALMAG, this.w_MVCAOVAL, this.w_CAONAZ, this.w_MVDATDOC, this.w_MVVALNAZ),g_PERPUL)
    endif
    * --- Carica il Temporaneo dei Dati
    REPLACE t_MVIMPACC WITH this.w_MVIMPACC, ;
    t_MVIMPSCO WITH this.w_MVIMPSCO, ;
    t_MVVALMAG WITH this.w_MVVALMAG, ;
    t_MVIMPNAZ WITH this.w_MVIMPNAZ,;
    t_MVPERPRO WITH this.w_MVPERPRO, ;
    t_MVIMPPRO WITH this.w_MVIMPPRO
    ENDSCAN
    * --- Se c'e' del Resto sugli Sconti Finali...
    if this.w_MVSCONTI<>this.w_TIMPSCO AND this.w_PRES<>0
      * --- Lo Scrive sulla prima Riga
      SELECT GeneApp
      GOTO this.w_PRES
      this.w_MVIMPSCO = t_MVIMPSCO + (this.w_MVSCONTI - this.w_TIMPSCO)
      this.w_MVVALMAG = t_MVVALRIG + this.w_MVIMPSCO
      * --- Se Scorporo a Piede Fattura
      if this.w_MVFLSCOR="S"
        this.w_MVVALMAG = CALNET(this.w_MVVALMAG, NVL(t_PERIVA, 0), this.w_DECTOT, this.w_MVCODIVE, this.w_PERIVE)
      endif
      * --- Calcola Provvigioni: 
      *     - Causale Documento gestisce provvigioni
      *     - Nei Parametri Provvigioni � attivo flag generazione su documento
      *     - Nella riga del documento non � attivo il flag ricalcola
      if this.w_FLAGEN = "S" and this.w_PPGENDOC="S"
        * --- Ricalcolo Provvigioni
        this.w_APPCONTRA = NVL(t_MVCONTRA, " ")
        this.w_APPVAR = NVL(t_MVCODVAR," ")
        this.w_APPART = t_MVCODART
        this.w_PRQTAUM1 = t_MVQTAUM1
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARGRUPRO"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_APPART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARGRUPRO;
            from (i_cTable) where;
                ARCODART = this.w_APPART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_GRPART = NVL(cp_ToDate(_read_.ARGRUPRO),cp_NullValue(_read_.ARGRUPRO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Batch Calcolo Provvigioni
        do GSVE_BEP with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      this.w_MVIMPNAZ = this.w_MVVALMAG
      if this.w_MVVALNAZ<>this.w_MVCODVAL
        this.w_MVIMPNAZ = cp_round(VAL2MON(this.w_MVVALMAG, this.w_MVCAOVAL, this.w_CAONAZ, this.w_MVDATDOC, this.w_MVVALNAZ),g_PERPUL)
      endif
      * --- Carica il Temporaneo dei Dati
      REPLACE t_MVIMPSCO WITH this.w_MVIMPSCO, ;
      t_MVVALMAG WITH this.w_MVVALMAG, ;
      t_MVPERPRO WITH this.w_MVPERPRO, ;
      t_MVIMPPRO WITH this.w_MVIMPPRO,;
      t_MVIMPNAZ WITH this.w_MVIMPNAZ
    endif
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
  endproc


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna RESOCONTO per ORDINI via WEB
  endproc


  procedure Page_8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Generazione ordini con distinta base
    * --- Read from TIP_DOCU
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "TDCAUCOD,TDCAUPFI,TDFLARCO,TDFLEXPL,TDTIPIMB,TDVALCOM,TDCOSEPL"+;
        " from "+i_cTable+" TIP_DOCU where ";
            +"TDTIPDOC = "+cp_ToStrODBC(this.w_MVTIPDOC);
            +" and TDFLEXPL = "+cp_ToStrODBC("N");
            +" and TDEXPAUT = "+cp_ToStrODBC("S");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        TDCAUCOD,TDCAUPFI,TDFLARCO,TDFLEXPL,TDTIPIMB,TDVALCOM,TDCOSEPL;
        from (i_cTable) where;
            TDTIPDOC = this.w_MVTIPDOC;
            and TDFLEXPL = "N";
            and TDEXPAUT = "S";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CAUCOD = NVL(cp_ToDate(_read_.TDCAUCOD),cp_NullValue(_read_.TDCAUCOD))
      this.w_CAUPFI = NVL(cp_ToDate(_read_.TDCAUPFI),cp_NullValue(_read_.TDCAUPFI))
      this.w_FLARCO = NVL(cp_ToDate(_read_.TDFLARCO),cp_NullValue(_read_.TDFLARCO))
      this.w_TDFLEXPL = NVL(cp_ToDate(_read_.TDFLEXPL),cp_NullValue(_read_.TDFLEXPL))
      this.w_MVTIPIMB = NVL(cp_ToDate(_read_.TDTIPIMB),cp_NullValue(_read_.TDTIPIMB))
      this.w_VALCOM = NVL(cp_ToDate(_read_.TDVALCOM),cp_NullValue(_read_.TDVALCOM))
      this.w_TDCOSEPL = NVL(cp_ToDate(_read_.TDCOSEPL),cp_NullValue(_read_.TDCOSEPL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if not(Empty(this.w_MVSERIAL)) and (this.w_MVFLPROV="N")
      if this.w_FLARCO="S" and (NOT EMPTY(this.w_CAUPFI) or NOT EMPTY(this.w_CAUCOD))
        do GSAR_BEA with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
  endproc


  procedure Page_9
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Cancella dal cursore le righe relative ai servizi spese
    * --- Read from SERVAL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.SERVAL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SERVAL_idx,2],.t.,this.SERVAL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "SVSERTRA,SVSERINC,SVSERBOL,SVSERIMB"+;
        " from "+i_cTable+" SERVAL where ";
            +"SVCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        SVSERTRA,SVSERINC,SVSERBOL,SVSERIMB;
        from (i_cTable) where;
            SVCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_SPETRA = NVL(cp_ToDate(_read_.SVSERTRA),cp_NullValue(_read_.SVSERTRA))
      this.w_SPEINC = NVL(cp_ToDate(_read_.SVSERINC),cp_NullValue(_read_.SVSERINC))
      this.w_SPEBOL = NVL(cp_ToDate(_read_.SVSERBOL),cp_NullValue(_read_.SVSERBOL))
      this.w_SPEIMB = NVL(cp_ToDate(_read_.SVSERIMB),cp_NullValue(_read_.SVSERIMB))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    SELECT GENEORDI
    GO TOP
    DELETE FROM GENEORDI Where InList(ORCODART, this.w_SPETRA, this.w_SPEINC, this.w_SPEBOL, this.w_SPEIMB)
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,w_DOSERIAL)
    this.w_DOSERIAL=w_DOSERIAL
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,24)]
    this.cWorkTables[1]='CAM_AGAZ'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='DOC_DETT'
    this.cWorkTables[4]='DOC_MAST'
    this.cWorkTables[5]='DOC_RATE'
    this.cWorkTables[6]='SALDIART'
    this.cWorkTables[7]='VALUTE'
    this.cWorkTables[8]='VOCIIVA'
    this.cWorkTables[9]='ZORDWEBD'
    this.cWorkTables[10]='ZORDWEBM'
    this.cWorkTables[11]='TIP_DOCU'
    this.cWorkTables[12]='AGENTI'
    this.cWorkTables[13]='ART_ICOL'
    this.cWorkTables[14]='PAR_PROV'
    this.cWorkTables[15]='KEY_ARTI'
    this.cWorkTables[16]='FAM_ARTI'
    this.cWorkTables[17]='NAZIONI'
    this.cWorkTables[18]='MAGAZZIN'
    this.cWorkTables[19]='PAG_2AME'
    this.cWorkTables[20]='DES_DIVE'
    this.cWorkTables[21]='PAR_PROD'
    this.cWorkTables[22]='SALDICOM'
    this.cWorkTables[23]='AZIENDA'
    this.cWorkTables[24]='SERVAL'
    return(this.OpenAllTables(24))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_DOSERIAL"
endproc
