* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bzz                                                        *
*              Doppio zoom pratiche\commesse                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [39][VRS_13]                                                    *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-11-22                                                      *
* Last revis.: 2008-07-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bzz",oParentObject)
return(i_retval)

define class tgsar_bzz as StdBatch
  * --- Local variables
  w_CODICE = space(10)
  w_TIPENT = space(1)
  w_PROG = space(8)
  w_DXBTN = .f.
  * --- WorkFile variables
  ENT_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch utilizzato negli Zoom on Zoom
    if i_curform=.NULL.
      i_retcode = 'stop'
      return
    endif
    this.w_DXBTN = .F.
    if Type("g_oMenu.oKey")<>"U"
      this.w_CODICE = Nvl(g_oMenu.oKey(1,3),"")
      this.w_DXBTN = .T.
    else
      * --- Puntatore al nome del Cursore da cui e' stato lanciato
      cCurs = i_curform.z1.cCursor
      * --- Tipo di Conto
      this.w_CODICE = &cCurs..CNCODCAN
    endif
    if Isalt()
      this.w_PROG = "GSPR_ACN()"
    else
      this.w_PROG = "GSAR_ACN()"
    endif
    * --- Nel caso ho premuto tasto destro sul controll apro la anagrafica sul codice 
    *     presente nella variabile
    OpenGest(IIF(this.w_DXBTN,g_oMenu.cBatchType,"S"),this.w_PROG,"CNCODCAN",this.w_CODICE)
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ENT_MAST'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
