* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bex                                                        *
*              Soggetto a codifica plurima                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-07-26                                                      *
* Last revis.: 2007-09-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bex",oParentObject)
return(i_retval)

define class tgscg_bex as StdBatch
  * --- Local variables
  * --- WorkFile variables
  DAELCLFO_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se flag codifica plurima non � attivo faccio la verifica e cerco se � da attivare
    if NVL(this.oParentObject.w_DECONPLU," ")<>"S"
      VQ_EXEC("query\gscg5bex.vqr", this, "CursCodPluri")
      if USED("CursCodPluri")
        Select CursCodPluri
        if UNIVO>1
          * --- Try
          local bErr_035CB4A8
          bErr_035CB4A8=bTrsErr
          this.Try_035CB4A8()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_035CB4A8
          * --- End
        endif
        Select CursCodPluri 
 Use
      endif
    endif
    this.bUpdateParentObject=.f.
  endproc
  proc Try_035CB4A8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Write into DAELCLFO
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DAELCLFO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DAELCLFO_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DAELCLFO_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"DECONPLU ="+cp_NullLink(cp_ToStrODBC("S"),'DAELCLFO','DECONPLU');
          +i_ccchkf ;
      +" where ";
          +"DEPARIVA = "+cp_ToStrODBC(this.oParentObject.w_DEPARIVA);
          +" and DETIPSOG = "+cp_ToStrODBC(this.oParentObject.w_DETIPSOG);
          +" and DE__ANNO = "+cp_ToStrODBC(this.oParentObject.w_DE__ANNO);
             )
    else
      update (i_cTable) set;
          DECONPLU = "S";
          &i_ccchkf. ;
       where;
          DEPARIVA = this.oParentObject.w_DEPARIVA;
          and DETIPSOG = this.oParentObject.w_DETIPSOG;
          and DE__ANNO = this.oParentObject.w_DE__ANNO;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    cp_ErrorMsg("Attivato flag codifica non univoca")
    * --- commit
    cp_EndTrs(.t.)
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='DAELCLFO'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
