* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bdg                                                        *
*              Comunicazione lettere intento ricevute                          *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][147]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-03-30                                                      *
* Last revis.: 2011-05-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_TIPOPE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bdg",oParentObject,m.w_TIPOPE)
return(i_retval)

define class tgscg_bdg as StdBatch
  * --- Local variables
  w_LENFILLE = 0
  w_TIPOPE = space(1)
  w_Ingresso = space(254)
  w_ParScri = space(10)
  w_FLFLDPOS = .f.
  w_CATFLD = space(2)
  w_FIELDLEN = 0
  w_oERRORLOG = .NULL.
  w_PARMSG1 = space(10)
  w_MESS = space(254)
  w_NUMRIGHE = 0
  w_FileOut = space(254)
  w_TRASMATT = 0
  w_QUADRO = 0
  w_NrChrWri = 0
  w_TotRecC = 0
  w_CODAZI = space(5)
  w_FLBLGEN = space(1)
  w_DTGENERA = ctod("  /  /  ")
  w_DTAPPO = ctod("  /  /  ")
  w_UTENTE = 0
  w_GCSERIAL = 0
  w_ErrZip = 0
  w_SERDIC = space(4)
  w_SERDOC = space(4)
  * --- WorkFile variables
  AZIENDA_idx=0
  DIC_GENE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- TIPOPE : S = Stampa report con dati di generazione file
    *                      G = Genera file telematico
    *                      F = Stampa Frontespizio invio dichiarazione
    * --- Nel caso di generazione del file eseguo un controllo di congruit� sui dati inseriti nella maschera
    if this.w_TIPOPE = "G"
      * --- Eseguo la CheckForm per verificare i campi obbligatori
      this.oParentObject.bUpdated = .t.
      if !this.oParentObject.CheckForm()
        i_retcode = 'stop'
        return
      endif
    endif
    if this.w_TIPOPE $ "GF" AND (this.oParentObject.w_DATINI<>cp_CharToDate("01-"+STR(this.oParentObject.w_FOMESE)+"-"+this.oParentObject.w_FOANNO) OR this.oParentObject.w_DATFIN<>DATE(INT(VAL(ALLTRIM(this.oParentObject.w_FOANNO)))+IIF(this.oParentObject.w_FOMESE=12,1,0),IIF(this.oParentObject.w_FOMESE=12,1,this.oParentObject.w_FOMESE+1),1)-1)
      if this.oParentObject.w_DATINI<=DATE(INT(VAL(ALLTRIM(this.oParentObject.w_FOANNO)))+IIF(this.oParentObject.w_FOMESE=12,1,0),IIF(this.oParentObject.w_FOMESE=12,1,this.oParentObject.w_FOMESE+1),1)-1 AND this.oParentObject.w_DATFIN<=DATE(INT(VAL(ALLTRIM(this.oParentObject.w_FOANNO)))+IIF(this.oParentObject.w_FOMESE=12,1,0),IIF(this.oParentObject.w_FOMESE=12,1,this.oParentObject.w_FOMESE+1),1)-1
        if not ah_yesno("Attenzione: date di estrazione dati modificate, proseguire?")
          i_retcode = 'stop'
          return
        endif
      else
        this.w_MESS = ""
        this.w_DTAPPO = DATE(INT(VAL(ALLTRIM(this.oParentObject.w_FOANNO))),IIF(this.oParentObject.w_FOMESE=12,1,this.oParentObject.w_FOMESE+1),1)-1
        do case
          case this.oParentObject.w_DATINI>this.w_DTAPPO and this.oParentObject.w_DATFIN>this.w_DTAPPO
            this.w_MESS = "Attenzione:%0la data d'inizio e la data di fine estrazione dati sono maggiori della data di fine periodo impostato. %0Impossibile proseguire"
          case this.oParentObject.w_DATINI>this.w_DTAPPO and !(this.oParentObject.w_DATFIN>this.w_DTAPPO)
            this.w_MESS = "Attenzione: %0la data d'inizio � maggiore della data di fine periodo impostato. %0Impossibile proseguire"
          case !(this.oParentObject.w_DATINI>this.w_DTAPPO) and this.oParentObject.w_DATFIN>this.w_DTAPPO
            this.w_MESS = "Attenzione: %0la data di fine estrazione dati � maggiore della data di fine periodo impostato. %0Impossibile proseguire"
        endcase
        if NOT EMPTY(this.w_MESS)
          ah_errormsg(this.w_MESS)
          i_retcode = 'stop'
          return
        endif
      endif
    endif
    * --- Eseguo la queri per estrarre i dati (sezione C)
    vq_Exec("QUERY\GSCG_BDG.VQR",this,"CursDatC")
    if USED("CursDatC")
      SELECT "CursDatC"
      this.w_NUMRIGHE = RECCOUNT("CursDatC")
      if this.w_NumRighe<1
        ah_errormsg("Per i parametri impostati non esistono dati da elaborare",16)
        i_retcode = 'stop'
        return
      endif
    else
      ah_errormsg("Per i parametri impostati non esistono dati da elaborare",16)
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      i_retcode = 'stop'
      return
    endif
    if this.w_TIPOPE $ "GF"
      * --- Verifico ed eventualmente alzo il flag semaforo
      this.w_CODAZI = i_CODAZI
      this.w_FLBLGEN = "N"
      * --- Read from AZIENDA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AZIENDA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AZBLOGCL"+;
          " from "+i_cTable+" AZIENDA where ";
              +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AZBLOGCL;
          from (i_cTable) where;
              AZCODAZI = this.w_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_FLBLGEN = NVL(cp_ToDate(_read_.AZBLOGCL),cp_NullValue(_read_.AZBLOGCL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_FLBLGEN="S"
        ah_errormsg("Flag blocco generazione attivo. impossibile proseguire",64)
        i_retcode = 'stop'
        return
      else
        * --- Write into AZIENDA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.AZIENDA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"AZBLOGCL ="+cp_NullLink(cp_ToStrODBC("S"),'AZIENDA','AZBLOGCL');
              +i_ccchkf ;
          +" where ";
              +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                 )
        else
          update (i_cTable) set;
              AZBLOGCL = "S";
              &i_ccchkf. ;
           where;
              AZCODAZI = this.w_CODAZI;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    endif
    if this.w_TIPOPE $ "FS"
      * --- Creo il cursore per il riempimento dei campi nel modello PDF e per la stampa FoxPro
      CREATE CURSOR __TMP__ (RAGSOC C(60), CODFIS C(16),RIFANNO C(4),RIFMESE C(2), FLCORR C(1), PARIVA C(11), ; 
 FLEVECC C(1), COMNASC C(30), PROVNASC C(2), GGNASC C(2), MMNASC C(2), AAAANASC C(4), SESSOM C(1), SESSOF C(1), ; 
 DOMFISC C(50), PROVDOMFISC C(2), INDDOMFISC C(50), CAPDOMFISC C(5), STAESTRES C(50), CODSTAEST C(3), ; 
 IVASTAESTRES C(20), NATGIU C(2), COMSEDLEG C(50), PROVSEDLEG C(2), INDSEDLEG C(50), CAPSEDLEG C(5), ; 
 STAESTLEG C(20), CODSTAESTLEG C(3), IVASTAESTLEG C(11), COMFISC C(50), PROVFISC C(2), INDFISC C(50), ; 
 CAPFISC C(5), CFRAP C(16), CODCAR C(1), PIRAP C(11), COGNRAP C(50), NOMRAP C(50), SEXRAPM C(1), ; 
 SEXRAPF C(1), GGDNRAP C(2) , MMDNRAP C(2), AAAADNRAP C(4), COMRAP C(50), PROVRAP C(2), ; 
 COMRESRAP C(50), PROVRESRAP C(2), CAPRESRAP C(5), INDRESRAP C(50), PRERAP C(5), TELRAP C(10), ; 
 NUMRIG C(8), CODFISINT C(16), NCAFINT N(5), FLIMPPRE C(1), FLIMPPREINT C(1), GGIMP C(2), MMIMP C(2), AAAAIMP C(4))
      * --- Popolo il cursore con i dati da stampare
       
 INSERT INTO __TMP__ VALUES (IIF(EMPTY(this.oParentObject.w_FODENOMINA),UPPER(this.oParentObject.w_FOCOGNOME)+" "+ UPPER(this.oParentObject.w_FONOME),upper(this.oParentObject.w_FODENOMINA)), ; 
 this.oParentObject.w_FOCODFIS, this.oParentObject.w_FOANNO, RIGHT("00"+ALLTRIM(STR(this.oParentObject.w_FOMESE)),2), IIF(this.oParentObject.w_FLAGCOR="1","X",""), this.oParentObject.w_FOPARIVA, IIF(this.oParentObject.w_FLAGEVE="1","X",""), this.oParentObject.w_COMUNE, this.oParentObject.w_SIGLA, ; 
 IIF(this.oParentObject.w_Perazi="S",RIGHT("00"+ALLTRIM(STR(DAY(this.oParentObject.w_DATANASC))),2),""), IIF(this.oParentObject.w_Perazi="S",RIGHT("00"+ALLTRIM(STR(MONTH(this.oParentObject.w_DATANASC))),2),""), ; 
 IIF(this.oParentObject.w_Perazi="S",ALLTRIM(STR(YEAR(this.oParentObject.w_DATANASC))),""), IIF(this.oParentObject.w_SESSO="M","X",""), IIF(this.oParentObject.w_SESSO="F","X",""), this.oParentObject.w_RECOMUNE, ; 
 this.oParentObject.w_RESIGLA, this.oParentObject.w_INDIRIZ, this.oParentObject.w_CAP, this.oParentObject.w_STAEST, IIF(this.oParentObject.w_CODEST<>0,RIGHT("000"+ALLTRIM(STR(this.oParentObject.w_CODEST)),3)," "), this.oParentObject.w_IDIVAEST, this.oParentObject.w_NATGIU, this.oParentObject.w_SECOMUNE, this.oParentObject.w_SESIGLA, this.oParentObject.w_SEINDIRI2, this.oParentObject.w_SECAP, ; 
 this.oParentObject.w_SESTAEST, IIF(this.oParentObject.w_SECODEST<>0,RIGHT("000"+ALLTRIM(STR(this.oParentObject.w_SECODEST)),3)," "), this.oParentObject.w_SEIDIVES, this.oParentObject.w_SERCOMUN, this.oParentObject.w_SERSIGLA, this.oParentObject.w_SERINDIR, this.oParentObject.w_SERCAP, this.oParentObject.w_RFCODFIS, this.oParentObject.w_RFCODCAR, this.oParentObject.w_RFPARIVA, ; 
 this.oParentObject.w_RFCOGNOME, this.oParentObject.w_RFNOME, IIF(this.oParentObject.w_RFSESSO="M","X",""), IIF(this.oParentObject.w_RFSESSO="F","X",""), IIF(RIGHT("00"+ALLTRIM(STR(DAY(this.oParentObject.w_RFDATANASC))),2)<>"00",RIGHT("00"+ALLTRIM(STR(DAY(this.oParentObject.w_RFDATANASC))),2),"  "), ; 
 IIF(RIGHT("00"+ALLTRIM(STR(MONTH(this.oParentObject.w_RFDATANASC))),2)<>"00",RIGHT("00"+ALLTRIM(STR(MONTH(this.oParentObject.w_RFDATANASC))),2),"  "), ; 
 IIF(VAL(ALLTRIM(STR(YEAR(this.oParentObject.w_RFDATANASC))))<>0,ALLTRIM(STR(YEAR(this.oParentObject.w_RFDATANASC))),"    "), this.oParentObject.w_RFCOMNAS, this.oParentObject.w_RFSIGNAS, this.oParentObject.w_RFCOMUNE, this.oParentObject.w_RFSIGLA, ; 
 this.oParentObject.w_RFCAP, this.oParentObject.w_RFINDIRIZ, IIF(LEN(this.oParentObject.w_RFTELEFONO)>4,LEFT(this.oParentObject.w_RFTELEFONO,4),""), IIF(LEN(this.oParentObject.w_RFTELEFONO)>4,RIGHT(this.oParentObject.w_RFTELEFONO,LEN(this.oParentObject.w_RFTELEFONO)-4),ALLTRIM(this.oParentObject.w_RFTELEFONO)), ; 
 RIGHT("00000000"+ALLTRIM(STR(this.w_NUMRIGHE)),8), this.oParentObject.w_RFCFINS2, this.oParentObject.w_NUMCAF, ; 
 IIF(this.oParentObject.w_IMPTRTEL="1","X",""), IIF(this.oParentObject.w_IMPTRSOG="1","X",""), RIGHT("00"+ALLTRIM(STR(DAY(this.oParentObject.w_RFDATIMP))),2), RIGHT("00"+ALLTRIM(STR(MONTH(this.oParentObject.w_RFDATIMP))),2), ; 
 ALLTRIM(STR(YEAR(this.oParentObject.w_RFDATIMP))) )
    endif
    do case
      case this.w_TIPOPE="S"
        * --- Creo un cursore con la struttura adatta alla sampa
        *     Il prodotto cartesiano � corretto per la presenza in __TMP__ di un solo record
        SELECT A.*, B.* FROM __TMP__ A, CursDatC B into cursor __TMP__
        SELECT "CursDatC"
        L_PERAZI=this.oParentObject.w_PERAZI
        * --- Lancio la stampa
        cp_chprn("QUERY\GSCG_BDG.FRX", " ", this.oParentObject)
      case this.w_TIPOPE="G"
        if this.oParentObject.w_FLAGCOR="0"
          * --- Verifico che per i dati impostati non sia gi� stato generato un file
          * --- Read from DIC_GENE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DIC_GENE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DIC_GENE_idx,2],.t.,this.DIC_GENE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "GDDATAEL,GSFILEGE"+;
              " from "+i_cTable+" DIC_GENE where ";
                  +"GCANNO = "+cp_ToStrODBC(this.oParentObject.w_FOANNO);
                  +" and GCMESE = "+cp_ToStrODBC(this.oParentObject.w_FOMESE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              GDDATAEL,GSFILEGE;
              from (i_cTable) where;
                  GCANNO = this.oParentObject.w_FOANNO;
                  and GCMESE = this.oParentObject.w_FOMESE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DTGENERA = NVL(cp_ToDate(_read_.GDDATAEL),cp_NullValue(_read_.GDDATAEL))
            this.w_FileOut = NVL(cp_ToDate(_read_.GSFILEGE),cp_NullValue(_read_.GSFILEGE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_rows>0
            ah_errormsg("Per i parametri impostati � gi� stato generato il file telematico %1 in data %2. %0Per generare un altro file per lo stesso periodo occorre attivare il flag correttiva",48,,ALLTRIM(this.w_FILEOUT),DTOC(this.w_DTGENERA))
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            i_retcode = 'stop'
            return
          endif
        endif
        * --- Istanzia l'oggetto per la gestione delle messaggistiche di errore
        this.w_oERRORLOG=createobject("AH_ErrorLog")
        * --- Controlli Sezione A
        if this.oParentObject.w_PERAZI="S" AND LEN(ALLTRIM(this.oParentObject.w_FOCODFIS))<16
          if NOT ah_yesno("Se l'azienda � di tipo persona fisica il codice fiscale di tipologia partita iva pu� generare un file incongruente. Proseguire?")
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            i_retcode = 'stop'
            return
          endif
        endif
        if this.oParentObject.w_PERAZI<>"S" AND LEN(ALLTRIM(this.oParentObject.w_FOCODFIS))>11
          if NOT ah_yesno("Se l'azienda non � di tipo persona fisica non specificando la partita IVA si pu� generare un file incongruente. Proseguire?")
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            i_retcode = 'stop'
            return
          endif
        endif
        if EMPTY(this.oParentObject.w_CODFISIN)
          this.w_oERRORLOG.AddMsgLog("Codice fiscale del fornitore non valorizzato")     
        endif
        if EMPTY(this.oParentObject.w_FOCODFIS)
          this.w_oERRORLOG.AddMsgLog("Codice fiscale del soggetto dichiarante non valorizzato")     
        endif
        if NOT EMPTY(this.oParentObject.w_FONOME) AND EMPTY(this.oParentObject.w_FOCOGNOME)
          this.w_oERRORLOG.AddMsgLog("Cognome del contribuente non valorizzato")     
        endif
        if NOT EMPTY(this.oParentObject.w_FOCOGNOME) AND EMPTY(this.oParentObject.w_FONOME)
          this.w_oERRORLOG.AddMsgLog("Nome del contribuente non valorizzato")     
        endif
        if EMPTY(this.oParentObject.w_FOCOGNOME) AND EMPTY(this.oParentObject.w_FONOME) AND EMPTY(this.oParentObject.w_FODENOMINA)
          this.w_oERRORLOG.AddMsgLog("Denominazione azienda contribuente non valorizzata")     
        endif
        if EMPTY(this.oParentObject.w_FOPARIVA)
          this.w_oERRORLOG.AddMsgLog("Partita IVA contribuente non valorizzata")     
        endif
        if EMPTY(this.oParentObject.w_FOANNO)
          this.w_oERRORLOG.AddMsgLog("Anno di riferimento non valorizzato")     
        endif
        if EMPTY(this.oParentObject.w_FOMESE)
          this.w_oERRORLOG.AddMsgLog("Mese di riferimento non valorizzato")     
        endif
        if this.oParentObject.w_PerAzi="S"
          if EMPTY(this.oParentObject.w_COMUNE)
            this.w_oERRORLOG.AddMsgLog("Comune di nascita contribuente (persona fisica) non valorizzato")     
          endif
          if DTOC(this.oParentObject.w_DATANASC)="  -  -    "
            this.w_oERRORLOG.AddMsgLog("Data di nascita contribuente (persona fisica) non valorizzata")     
          endif
          if EMPTY(this.oParentObject.w_SESSO) OR NOT(this.oParentObject.w_SESSO $ "MF")
            this.w_oERRORLOG.AddMsgLog("Sesso contribuente (persona fisica) non valorizzato correttamente")     
          endif
          if EMPTY(this.oParentObject.w_RECOMUNE)
            this.w_oERRORLOG.AddMsgLog("Comune di residenza contribuente (persona fisica) non valorizzato")     
          endif
          if EMPTY(this.oParentObject.w_INDIRIZ)
            this.w_oERRORLOG.AddMsgLog("Indirizzo residenza non valorizzato")     
          endif
        else
          if EMPTY(this.oParentObject.w_SECOMUNE)
            this.w_oERRORLOG.AddMsgLog("Comune della sede legale contribuente non valorizzato")     
          endif
          if EMPTY(this.oParentObject.w_SERCOMUN) AND NOT(EMPTY(this.oParentObject.w_SERSIGLA) OR EMPTY(this.oParentObject.w_SERINDIR))
            this.w_oERRORLOG.AddMsgLog("Comune domicilio fiscale obbligatorio se presenti provincia e/o indirizzo domicilio fiscale")     
          endif
          if EMPTY(this.oParentObject.w_SERSIGLA) AND NOT(EMPTY(this.oParentObject.w_SERCOMUN) OR EMPTY(this.oParentObject.w_SERINDIR))
            this.w_oERRORLOG.AddMsgLog("Provincia domicilio fiscale obbligatoria se presente comune e/o indirizzo domicilio fiscale")     
          endif
          if EMPTY(this.oParentObject.w_SERINDIR) AND NOT(EMPTY(this.oParentObject.w_SERSIGLA) OR EMPTY(this.oParentObject.w_SERCOMUN))
            this.w_oERRORLOG.AddMsgLog("Indirizzo domicilio fiscale obbligatorio se presente comune e/o provincia domicilio fiscale")     
          endif
          if EMPTY(this.oParentObject.w_NATGIU)
            this.w_oERRORLOG.AddMsgLog("Natura giuridica non valorizzata. Valori ammessi Da 1 a 43 o 50,51,52,53")     
          endif
        endif
        if NOT(EMPTY(this.oParentObject.w_RFCODFIS+this.oParentObject.w_RFCOGNOME+this.oParentObject.w_RFNOME+this.oParentObject.w_RFCOMNAS+this.oParentObject.w_RFSIGNAS+this.oParentObject.w_RFCOMUNE+this.oParentObject.w_RFSIGLA+this.oParentObject.w_RFCAP+this.oParentObject.w_RFINDIRIZ+this.oParentObject.w_RFTELEFONO))
          if EMPTY(this.oParentObject.w_RFCODFIS)
            this.w_oERRORLOG.AddMsgLog("Codice fiscale rappresentante non valorizzato")     
          endif
          if NOT(this.oParentObject.w_RFCODCAR $ "123456789")
            this.w_oERRORLOG.AddMsgLog("Codice carica rappresentante non valorizzato o non impostato correttamente. valori ammessi da 1 a 9")     
          endif
          if EMPTY(this.oParentObject.w_RFCOGNOME)
            this.w_oERRORLOG.AddMsgLog("Cognome rappresentante non valorizzato")     
          endif
          if EMPTY(this.oParentObject.w_RFNOME)
            this.w_oERRORLOG.AddMsgLog("Nome rappresentante non valorizzato")     
          endif
          if EMPTY(this.oParentObject.w_RFSESSO) OR NOT(this.oParentObject.w_RFSESSO $ "MF")
            this.w_oERRORLOG.AddMsgLog("Sesso rappresentante non valorizzato correttamente")     
          endif
          if DTOC(this.oParentObject.w_RFDATANASC)="  -  -    "
            this.w_oERRORLOG.AddMsgLog("Data di nascita rappresentante non valorizzata")     
          endif
          if EMPTY(this.oParentObject.w_RFCOMNAS)
            this.w_oERRORLOG.AddMsgLog("Comune o stato estero rappresentante non valorizzato")     
          endif
          if EMPTY(this.oParentObject.w_RFINDIRIZ)
            this.w_oERRORLOG.AddMsgLog("Indirizzo di residenza rappresentante non valorizzato")     
          endif
          if NOT(EMPTY(this.oParentObject.w_RFCFINS2+IIF(this.oParentObject.w_NUMCAF=0," ","X")+iif(this.oParentObject.w_IMPTRTEL="0" OR this.oParentObject.w_IMPTRTEL==""," ","1")+iif(this.oParentObject.w_IMPTRSOG="0" OR this.oParentObject.w_IMPTRSOG==""," ","1")+iif(this.oParentObject.w_FIRMINT="0"," ","1")) and (empty(this.oParentObject.w_RFDATIMP)))
            if EMPTY(this.oParentObject.w_RFCFINS2)
              this.w_oERRORLOG.AddMsgLog("Codice fiscale dell'intermediario che effettua la trasmissione non valorizzato")     
            endif
            if (this.oParentObject.w_IMPTRTEL="0" OR empty(this.oParentObject.w_IMPTRTEL)) AND (this.oParentObject.w_IMPTRSOG="0" OR empty(this.oParentObject.w_IMPTRSOG))
              this.w_oERRORLOG.AddMsgLog("Almeno una casella tipologia d'impegno a trasmettere la comunicazione deve essere selezionata")     
            endif
            if DTOC(this.oParentObject.w_RFDATIMP)="  -  -    "
              this.w_oERRORLOG.AddMsgLog("Data d'impegno alla trasmissione telematica non valorizzata")     
            endif
          endif
        endif
        if this.oParentObject.w_FIRMDICH<>"1"
          this.w_oERRORLOG.AddMsgLog("Flag firma dichiarazione non attivato")     
        endif
        SELECT "CursDatC"
        GO TOP
        this.w_QUADRO = 1
        SCAN
        this.w_PARMSG1 = STR(this.w_QUADRO)
        if EMPTY(ANPARIVA)
          this.w_oERRORLOG.AddMsgLog("Quadro DI%1: partita IVA committente non valorizzata",alltrim(this.w_PARMSG1))     
        endif
        if EMPTY(ANCOGNOM)
          this.w_oERRORLOG.AddMsgLog("Quadro DI%1: cognome/denominazione committente non valorizzato",alltrim(this.w_PARMSG1))     
        endif
        if EMPTY(ANINDIRI)
          this.w_oERRORLOG.AddMsgLog("Quadro DI%1: indirizzo committente non valorizzato",alltrim(this.w_PARMSG1))     
        endif
        if EMPTY(ANLOCALI)
          this.w_oERRORLOG.AddMsgLog("Quadro DI%1: comune committente non valorizzato",alltrim(this.w_PARMSG1))     
        endif
        if EMPTY(NVL(ANPROVIN," ")) AND EMPTY(NVL(ANCODEST," "))
          this.w_oERRORLOG.AddMsgLog("Quadro DI%1: provincia committente non valorizzata",alltrim(this.w_PARMSG1))     
        endif
        if DIIMPDICO<>0 AND DIFLOPTO=0
          this.w_oERRORLOG.AddMsgLog("Quadro DI%1: flag unica operazione non valorizzato con importo unica operazione valorizzato",alltrim(this.w_PARMSG1))     
        endif
        if DIIMPDICI<>0 AND DIFLOPTI=0
          this.w_oERRORLOG.AddMsgLog("Quadro DI%1: flag operazioni fino a concorrenza non valorizzato con importo concorrenza valorizzato",alltrim(this.w_PARMSG1))     
        endif
        if (NOT(EMPTY(NVL(DIDATINI," "))) OR NOT(EMPTY(NVL(DIDATFIN," ")))) AND DIFLOPTD=0
          this.w_oERRORLOG.AddMsgLog("Quadro DI%1: flag operazioni nel periodo non valorizzato con date periodo valorizzate",alltrim(this.w_PARMSG1))     
        endif
        if DIFLOPTD=1 AND EMPTY(NVL(DIDATINI," "))
          this.w_oERRORLOG.AddMsgLog("Quadro DI%1: data inizio periodo non valorizzata con flag Operazioni nel periodo valorizzato",alltrim(this.w_PARMSG1))     
        endif
        if DIFLOPTD=1 AND EMPTY(NVL(DIDATFIN," "))
          this.w_oERRORLOG.AddMsgLog("Quadro DI%1: data fine periodo non valorizzata con flag Operazioni nel periodo valorizzato",alltrim(this.w_PARMSG1))     
        endif
        if (DIFLOPTD=1 AND NVL(DTOC(DIDATFIN)," ")<>" ") AND (ALLTRIM(STR(YEAR(DIDATFIN)))<>this.oParentObject.w_FOANNO AND this.oParentObject.w_FOMESE<>12)
          this.w_oERRORLOG.AddMsgLog("Quadro DI%1: anno data fine periodo incongruente con l'anno di riferimento impostato sulla maschera",alltrim(this.w_PARMSG1))     
        endif
        this.w_QUADRO = this.w_QUADRO+1
        ENDSCAN
        if this.w_oERRORLOG.IsFullLog()
          * --- Lancio la stampa
          this.w_oERRORLOG.PrintLog(this.oParentObject,"Segnalazioni in fase di generazione",.f.)     
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          i_retcode = 'stop'
          return
        endif
        this.w_FileOut = ALLTRIM(this.oParentObject.w_DIRNAME)+IIF(RIGHT(ALLTRIM(this.oParentObject.w_DIRNAME),1)="\","","\")+ALLTRIM(this.oParentObject.w_FileName)
        if FILE(this.w_FILEOUT)
          if NOT ah_YesNo("Il file esiste gi�, sovrascriverlo?")
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            i_retcode = 'stop'
            return
          endif
        endif
        FH=FCREATE(this.w_FileOut,0)
        if FH<0
          ah_errormsg("Impossibile aprire il file in scrittura",48)
        else
          this.w_Ingresso = "A"
          this.w_ParScri = "PAN1"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = SPACE(14)
          this.w_ParScri = "PAN14"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = "IVD05"
          this.w_ParScri = "PAN5"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = ALLTRIM(this.oParentObject.w_TIPFORN)
          this.w_ParScri = "PNU2"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = IIF(this.oParentObject.w_TIPFORN="10",this.oParentObject.w_CODFISIN,this.oParentObject.w_FOCODFIS)
          this.w_ParScri = "PAN16"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Spazio non utilizzato
          this.w_LENFILLE = 483
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Comunicazione su pi� invii
          this.w_Ingresso = "0"
          this.w_ParScri = "PNU4"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = "0"
          this.w_ParScri = "PNU4"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Spazio a disposizione dell'utente
          * --- Campo 9 lasciato vuoto
          this.w_Ingresso = SPACE(100)
          this.w_ParScri = "PAN100"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Spazio non disponibile
          this.w_LENFILLE = 1068
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = SPACE(200)
          this.w_ParScri = "PAN200"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Ultimi tre caratteri di controllo del record
          this.w_Ingresso = "A"
          this.w_ParScri = "PAN1"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = CHR(13)+CHR(10)
          this.w_ParScri = "PCC2"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = "B"
          this.w_ParScri = "PAN1"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = this.oParentObject.w_FOCODFIS
          this.w_ParScri = "PAN16"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = "00000001"
          this.w_ParScri = "PNU8"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = SPACE(3)
          this.w_ParScri = "PAN3"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = SPACE(25)
          this.w_ParScri = "PAN25"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = SPACE(20)
          this.w_ParScri = "PAN20"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Campo 7 CodFis Zucchetti
          this.w_Ingresso = "05006900962     "
          this.w_ParScri = "PAN16"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Comunicazione di mancata corrispondenza dei dati da trasmettere
          *     con quelli risultanti dalla comunicazione
          this.w_Ingresso = ALLTRIM(this.oParentObject.w_FLNOCOR)
          this.w_ParScri = "PCB1"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Tipo di comunicazione
          *     Dati del frontespizio
          this.w_Ingresso = ALLTRIM(this.oParentObject.w_FLAGCOR)
          this.w_ParScri = "PCB1"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Campi da 10 a 19 Filler di 1 raggruppati
          this.w_Ingresso = SPACE(10)
          this.w_ParScri = "PAN10"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = RIGHT("00"+ALLTRIM(this.oParentObject.w_FLAGEVE),2)
          this.w_ParScri = "PNU2"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Campi da 21 a 24 Filler di 1 raggruppati
          this.w_Ingresso = SPACE(4)
          this.w_ParScri = "PAN4"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Dati del frontespizio
          *     Dati del contribuente
          if this.oParentObject.w_PerAzi="S"
            this.w_Ingresso = this.oParentObject.w_FOCOGNOME
            this.w_ParScri = "PAN24"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_Ingresso = this.oParentObject.w_FONOME
            this.w_ParScri = "PAN20"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_Ingresso = SPACE(60)
            this.w_ParScri = "PAN60"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            this.w_Ingresso = SPACE(24)
            this.w_ParScri = "PAN24"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_Ingresso = SPACE(20)
            this.w_ParScri = "PAN20"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_Ingresso = this.oParentObject.w_FODENOMINA
            this.w_ParScri = "PAN60"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          this.w_Ingresso = this.oParentObject.w_FOPARIVA
          this.w_ParScri = "PPI11"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Campi da 29 a 35 filler 1 1 1 5 12 12 100 raggruppati
          this.w_Ingresso = SPACE(132)
          this.w_ParScri = "PAN132"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = this.oParentObject.w_FOANNO
          this.w_ParScri = "PAN4"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = RIGHT("00"+ALLTRIM(STR(this.oParentObject.w_FOMESE)),2)
          this.w_ParScri = "PNU2"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.oParentObject.w_PerAzi="S"
            * --- Persona fisica
            this.w_Ingresso = this.oParentObject.w_COMUNE
            this.w_ParScri = "PAN40"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_Ingresso = this.oParentObject.w_SIGLA
            this.w_ParScri = "PPR2"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_Ingresso = RIGHT("00"+ALLTRIM(STR(DAY(this.oParentObject.w_DATANASC))),2)+RIGHT("00"+ALLTRIM(STR(MONTH(this.oParentObject.w_DATANASC))),2)+ALLTRIM(STR(YEAR(this.oParentObject.w_DATANASC)))
            this.w_ParScri = "PDT8"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_Ingresso = this.oParentObject.w_SESSO
            this.w_ParScri = "PAN1"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Campi da 42 a 44 Filler 1 1 3 raggruppati
            this.w_Ingresso = SPACE(5)
            this.w_ParScri = "PAN5"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Residenza Anagrafica
            this.w_Ingresso = this.oParentObject.w_RECOMUNE
            this.w_ParScri = "PAN40"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_Ingresso = this.oParentObject.w_RESIGLA
            this.w_ParScri = "PPR2"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_Ingresso = this.oParentObject.w_INDIRIZ
            this.w_ParScri = "PAN35"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_Ingresso = this.oParentObject.w_CAP
            this.w_ParScri = "PNU5"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Campi da 49 a 57 Filler 4 8 1 1 6 40 2 4 10 raggruppati
            this.w_Ingresso = SPACE(76)
            this.w_ParScri = "PAN76"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Residente all'estero
            this.w_Ingresso = this.oParentObject.w_IDIVAEST
            this.w_ParScri = "PAN20"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_Ingresso = this.oParentObject.w_STAEST
            this.w_ParScri = "PAN24"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_Ingresso = RIGHT("00"+ALLTRIM(STR(this.oParentObject.w_CODEST,3)),3)
            this.w_ParScri = "PNU3"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_Ingresso = SPACE(95)
            this.w_ParScri = "PAN95"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Riempimento con spazi della sezione dedicata agli enti
            * --- Campi 68,69,70 e 71 Filler 6, 40,2,35
            this.w_Ingresso = SPACE(83)
            this.w_ParScri = "PAN83"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_Ingresso = "00000"
            this.w_ParScri = "PNU5"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Campi da 73 a 76 Riempio con spazi 6, 40, 2, 35
            this.w_Ingresso = SPACE(83)
            this.w_ParScri = "PAN83"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_Ingresso = "00000"
            this.w_ParScri = "PNU5"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Campi da 78 a 82 Riempio con spazi 8, 8, 8, 8, 1
            this.w_Ingresso = SPACE(33)
            this.w_ParScri = "PAN33"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_Ingresso = "00"
            this.w_ParScri = "PNU2"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Campi da 82 a 86 spazi, 1, 1, 24
            this.w_Ingresso = SPACE(26)
            this.w_ParScri = "PAN26"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_Ingresso = "000"
            this.w_ParScri = "PNU3"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Campi da 88 a 97 Spazi. 20, 16, 11, 1, 2, 11, 18, 11, 11, 8
            this.w_Ingresso = SPACE(109)
            this.w_ParScri = "PAN109"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            * --- Non � persona fisica campi da 38 a 67 Sostituiti da filler
            * --- Campi 38 e 39 spazio 40 2
            this.w_Ingresso = SPACE(42)
            this.w_ParScri = "PAN42"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Campo 40 Data 8 messo a 0
            this.w_Ingresso = "00000000"
            this.w_ParScri = "PAN8"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Campi da 41 a 47 spazio 1 1 1 3 40 2 35
            this.w_Ingresso = SPACE(83)
            this.w_ParScri = "PAN83"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Campo 48 a 0
            this.w_Ingresso = "00000"
            this.w_ParScri = "PNU5"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Campi da 49 a 59 filler
            *     4 8 1 1 6 40 2 4 10 20 24
            this.w_Ingresso = SPACE(120)
            this.w_ParScri = "PAN120"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_Ingresso = "000"
            this.w_ParScri = "PNU3"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Campi da 61 a 67 filler
            *     24 24 35 1 1 1 9
            this.w_Ingresso = SPACE(95)
            this.w_ParScri = "PAN95"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Dati relativi alle Societ� o Ente
            *     Soggetti diversi dalle persone fisiche
            this.w_Ingresso = SPACE(6)
            this.w_ParScri = "PAN6"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_Ingresso = this.oParentObject.w_SECOMUNE
            this.w_ParScri = "PAN40"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_Ingresso = this.oParentObject.w_SESIGLA
            this.w_ParScri = "PPR2"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_Ingresso = this.oParentObject.w_SEINDIRI2
            this.w_ParScri = "PAN35"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_Ingresso = this.oParentObject.w_SECAP
            this.w_ParScri = "PNU5"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_Ingresso = SPACE(6)
            this.w_ParScri = "PAN6"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_Ingresso = this.oParentObject.w_SERCOMUN
            this.w_ParScri = "PAN40"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_Ingresso = this.oParentObject.w_SERSIGLA
            this.w_ParScri = "PPR2"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_Ingresso = this.oParentObject.w_SERINDIR
            this.w_ParScri = "PAN35"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_Ingresso = this.oParentObject.w_SERCAP
            this.w_ParScri = "PNU5"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Campi da 78 a 82 filler 8 8 8 8 1 raggruppati
            this.w_Ingresso = SPACE(33)
            this.w_ParScri = "PAN33"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_Ingresso = RIGHT("00"+ALLTRIM(this.oParentObject.w_NATGIU),2)
            this.w_ParScri = "PNU2"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- campi 84 e 85 filler 1 raggruppati
            this.w_Ingresso = SPACE(2)
            this.w_ParScri = "PAN2"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_Ingresso = this.oParentObject.w_SESTAEST
            this.w_ParScri = "PAN24"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_Ingresso = RIGHT("000"+ALLTRIM(STR(this.oParentObject.w_SECODEST)),3)
            this.w_ParScri = "PNU3"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_Ingresso = this.oParentObject.w_SEIDIVES
            this.w_ParScri = "PAN20"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Campi da 88 a 97 filler 16 11 1 2 11 18 11 11 8 raggruppati
            this.w_Ingresso = SPACE(89)
            this.w_ParScri = "PAN89"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          * --- Rappresentante
          this.w_Ingresso = this.oParentObject.w_RFCODFIS
          this.w_ParScri = "PCF16"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = IIF(this.oParentObject.w_RFCODCAR $ "169",this.oParentObject.w_RFPARIVA,"00000000000")
          this.w_ParScri = "PCN11"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = SPACE(1)
          this.w_ParScri = "PAN1"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = RIGHT("00"+ALLTRIM(this.oParentObject.w_RFCODCAR),2)
          this.w_ParScri = "PNU2"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- campi da 102 a 104 filler 1 8 60 raggruppati
          this.w_Ingresso = SPACE(69)
          this.w_ParScri = "PAN69"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = this.oParentObject.w_RFCOGNOME
          this.w_ParScri = "PAN24"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = this.oParentObject.w_RFNOME
          this.w_ParScri = "PAN20"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = this.oParentObject.w_RFSESSO
          this.w_ParScri = "PAN1"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = RIGHT("00"+ALLTRIM(STR(DAY(this.oParentObject.w_RFDATANASC))),2)+RIGHT("00"+ALLTRIM(STR(MONTH(this.oParentObject.w_RFDATANASC))),2)+RIGHT("0000"+ALLTRIM(STR(YEAR(this.oParentObject.w_RFDATANASC))),4)
          this.w_ParScri = "PDT8"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = this.oParentObject.w_RFCOMNAS
          this.w_ParScri = "PAN40"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = this.oParentObject.w_RFSIGNAS
          this.w_ParScri = "PPR2"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = this.oParentObject.w_RFCOMUNE
          this.w_ParScri = "PAN40"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = this.oParentObject.w_RFSIGLA
          this.w_ParScri = "PPR2"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = this.oParentObject.w_RFCAP
          this.w_ParScri = "PNU5"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = this.oParentObject.w_RFINDIRIZ
          this.w_ParScri = "PAN35"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = this.oParentObject.w_RFTELEFONO
          this.w_ParScri = "PAN12"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Campi da 116 a 119 filler 8 1 8 1 raggruppati
          this.w_Ingresso = SPACE(18)
          this.w_ParScri = "PAN18"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Firma della comunicazione
          this.w_Ingresso = this.oParentObject.w_FIRMDICH
          this.w_ParScri = "PCB1"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Campi da 121 a 190 filler 1, da 191 a 196 filler 8 8 1 1 1 16 raggruppati
          this.w_Ingresso = SPACE(105)
          this.w_ParScri = "PAN105"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Comunicazione dichiarazione di intento
          this.w_Ingresso = RIGHT("00000000"+ALLTRIM(STR(this.w_NUMRIGHE)),8)
          this.w_ParScri = "PNU8"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Impegno alla presentazione telematica
          this.w_Ingresso = this.oParentObject.w_RFCFINS2
          this.w_ParScri = "PCF16"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = RIGHT("00000"+ALLTRIM(STR(this.oParentObject.w_NUMCAF)),5)
          this.w_ParScri = "PNU5"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = this.oParentObject.w_IMPTRTEL
          this.w_ParScri = "PCB1"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = this.oParentObject.w_IMPTRSOG
          this.w_ParScri = "PCB1"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = RIGHT("00"+ALLTRIM(STR(DAY(this.oParentObject.w_RFDATIMP))),2)+RIGHT("00"+ALLTRIM(STR(MONTH(this.oParentObject.w_RFDATIMP))),2)+RIGHT("0000"+ALLTRIM(STR(YEAR(this.oParentObject.w_RFDATIMP))),4)
          this.w_ParScri = "PDT8"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = this.oParentObject.w_FIRMINT
          this.w_ParScri = "PCB1"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Campi da 204 a 225 filler 3 16 1 1 11 16 16 16 1 1 17 6 1 1 8 8 165 1 1 1 1 1 raggruppati
          *     Spazio non utilizzato
          *     Campi da 226 a 238 filler 8 25 20 7 3 5 1 1 1 1 1 1 13 raggruppati
          this.w_LENFILLE = 380
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = "A"
          this.w_ParScri = "PAN1"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = CHR(13)+CHR(10)
          this.w_ParScri = "PCC2"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_TRASMATT = 1
          this.w_TotRecC = 1
          this.w_QUADRO = 1
          this.w_NrChrWri = 0
          this.Pag5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          SELECT "CursDatC"
          GO TOP
          SCAN
          this.w_ParScri = "NONP"
          * --- Elaborazione campi non posizionali
          this.w_SERDIC = IIF(!EMPTY(ALLTRIM(NVL(DISERDIC,""))),ALLTRIM(DISERDIC),"")
          this.w_Ingresso = NoPosValue("DI"+RIGHT("000"+ALLTRIM(STR(this.w_QUADRO)),3)+"001",ALLTRIM(STR(Nvl(DINUMDIC,0)))+this.w_SERDIC,"C")
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_SERDOC = IIF(!EMPTY(ALLTRIM(NVL(DISERDOC,""))),ALLTRIM(DISERDOC),"")
          this.w_Ingresso = NoPosValue("DI"+RIGHT("000"+ALLTRIM(STR(this.w_QUADRO)),3)+"002",ALLTRIM(STR(Nvl(DINUMDOC,0)))+this.w_SERDOC,"C")
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = NoPosValue("DI"+RIGHT("000"+ALLTRIM(STR(this.w_QUADRO)),3)+"003",Nvl(ANPARIVA," "),"P")
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = NoPosValue("DI"+RIGHT("000"+ALLTRIM(STR(this.w_QUADRO)),3)+"004",Nvl(ANCOGNOM," "),"C")
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = NoPosValue("DI"+RIGHT("000"+ALLTRIM(STR(this.w_QUADRO)),3)+"005",Nvl(AN__NOME," "),"C")
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = NoPosValue("DI"+RIGHT("000"+ALLTRIM(STR(this.w_QUADRO)),3)+"006",Nvl(ANINDIRI," "),"C")
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = NoPosValue("DI"+RIGHT("000"+ALLTRIM(STR(this.w_QUADRO)),3)+"007",Nvl(ANLOCALI," "),"C")
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = NoPosValue("DI"+RIGHT("000"+ALLTRIM(STR(this.w_QUADRO)),3)+"008",Nvl(ANPROVIN," "),"C")
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = NoPosValue("DI"+RIGHT("000"+ALLTRIM(STR(this.w_QUADRO)),3)+"009",Nvl(ANCODEST," "),"C")
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          do case
            case DIFLOPTO=1
              this.w_Ingresso = NoPosValue("DI"+RIGHT("000"+ALLTRIM(STR(this.w_QUADRO)),3)+"010",DIFLOPTO,"F")
              this.Pag3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              this.w_Ingresso = NoPosValue("DI"+RIGHT("000"+ALLTRIM(STR(this.w_QUADRO)),3)+"011",DIIMPDICO,"N")
              this.Pag3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            case DIFLOPTI =1
              this.w_Ingresso = NoPosValue("DI"+RIGHT("000"+ALLTRIM(STR(this.w_QUADRO)),3)+"012",DIFLOPTI,"F")
              this.Pag3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              this.w_Ingresso = NoPosValue("DI"+RIGHT("000"+ALLTRIM(STR(this.w_QUADRO)),3)+"013",DIIMPDICI,"N")
              this.Pag3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            otherwise
              this.w_Ingresso = NoPosValue("DI"+RIGHT("000"+ALLTRIM(STR(this.w_QUADRO)),3)+"014",DIFLOPTD,"F")
              this.Pag3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              this.w_Ingresso = NoPosValue("DI"+RIGHT("000"+ALLTRIM(STR(this.w_QUADRO)),3)+"015",DIDATINI,"D")
              this.Pag3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              this.w_Ingresso = NoPosValue("DI"+RIGHT("000"+ALLTRIM(STR(this.w_QUADRO)),3)+"016",DIDATFIN,"D")
              this.Pag3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
          endcase
          this.w_QUADRO = this.w_QUADRO+1
          if this.w_QUADRO>4
            this.w_TotRecC = this.w_TotRecC+1
            this.w_QUADRO = 1
            this.w_TRASMATT = this.w_TRASMATT+1
            * --- Verifica caratteri scritti, se minori di 1890 aggiungo un filler per
            *     ottenere una corretta formattazione dei caratteri di controllo
            if this.w_NrChrWri<1890
              this.w_Ingresso = SPACE(1890-this.w_NrChrWri-16)
              this.w_ParScri = "PAN"+ALLTRIM(STR(1890-this.w_NrChrWri-16))
              this.Pag3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            * --- Terza parte - Carattteri di controllo
            this.w_Ingresso = SPACE(8)
            this.w_ParScri = "PAN8"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_Ingresso = "A"
            this.w_ParScri = "PAN1"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_Ingresso = CHR(13)+CHR(10)
            this.w_ParScri = "PCC2"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.Pag5()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          ENDSCAN
          * --- Verifica caratteri scritti, se minori di 1890 aggiungo un filler per
          *     ottenere una corretta formattazione dei caratteri di controllo
          if this.w_NrChrWri<1890
            this.w_Ingresso = SPACE(1890-this.w_NrChrWri-16)
            this.w_ParScri = "PAN"+ALLTRIM(STR(1890-this.w_NrChrWri-16))
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          * --- Terza parte - Carattteri di controllo
          this.w_Ingresso = SPACE(8)
          this.w_ParScri = "PAN8"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = "A"
          this.w_ParScri = "PAN1"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = CHR(13)+CHR(10)
          this.w_ParScri = "PCC2"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = "Z"
          this.w_ParScri = "PAN1"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = SPACE(14)
          this.w_ParScri = "PAN14"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = "000000001"
          this.w_ParScri = "PNU9"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = RIGHT("000000000"+ALLTRIM(STR(this.w_TotRecC)),9)
          this.w_ParScri = "PNU9"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_LENFILLE = 1864
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Ingresso = "A"
          this.w_ParScri = "PAN1"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_ParScri = "PCC2"
          this.w_Ingresso = CHR(13)+CHR(10)
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Chiudo il file
          =FCLOSE(FH)
          * --- Comprimo il file generato
          *     zipunzip('A',LEFT(ALLTRIM(w_FileOut),LEN(ALLTRIM(w_FileOut))-4)+'.ZIP',ALLTRIM(w_DIRName),ALLTRIM(w_FileName),'',.F.,'',.F.,9)
          if this.w_ErrZip>0
            if this.w_ErrZip<8
              ah_errormsg("File generato con successo. Durante la compressione � stato riscontrato il seguente errore: %0errore funzione zipunzip (%1)",48,,ALLTRIM(STR(this.w_ErrZip,2,0)))
            else
              ah_errormsg("File generato con successo. Durante la compressione � stato riscontrato il seguente errore: %0%1",48,,ALLTRIM(MESSAGE()))
            endif
          else
            this.w_DTGENERA = i_DatSys
            this.w_UTENTE = i_Codute
            this.w_GCSERIAL = 0
            * --- begin transaction
            cp_BeginTrs()
            * --- Try
            local bErr_00F3C0A8
            bErr_00F3C0A8=bTrsErr
            this.Try_00F3C0A8()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- rollback
              bTrsErr=.t.
              cp_EndTrs(.t.)
            endif
            bTrsErr=bTrsErr or bErr_00F3C0A8
            * --- End
            ah_ErrorMsg("File generato con successo",64)
          endif
        endif
      case this.w_TIPOPE="F"
        DECLARE ARRFDF (7,2)
        * --- Riempio l'Array ARRFDF con i nomi dei campi che nel Modello PDF sono rappresentati come N, N>1, celle distinte.
         
 ARRFDF (1,1) = "CODFIS" 
 ARRFDF (1,2) = 16 
 ARRFDF (2,1) = "RIFANNO" 
 ARRFDF (2,2) = 4 
 ARRFDF (3,1) = "RIFMESE" 
 ARRFDF (3,2) = 2 
 ARRFDF (4,1) = "PARIVA" 
 ARRFDF (4,2) = 11 
 ARRFDF (5,1) = "NATGIU" 
 ARRFDF (5,2) = 2 
 ARRFDF (6,1) = "CFRAP" 
 ARRFDF (6,2) = 16 
 ARRFDF (7,1) = "PIRAP" 
 ARRFDF (7,2) = 11
        result1=cp_ffdf(" ","MODLETINT.pdf",this," ",@ARRFDF)
        CP_CHPRN(tempadhoc()+"\MODLETINT1.FDF",.NULL.,2)
        this.Pag6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if USED("__TMP__")
          SELECT "__TMP__"
          USE
        endif
        DECLARE ARRFDF (5,2)
        * --- Riempio l'Array ARRFDF con i nomi dei campi che nel Modello PDF sono rappresentati come N, N>1, celle distinte.
         
 ARRFDF (1,1) = "CODFIS" 
 ARRFDF (1,2) = 16 
 ARRFDF (2,1) = "DI1PIVA" 
 ARRFDF (2,2) = 11 
 ARRFDF (3,1) = "DI2PIVA" 
 ARRFDF (3,2) = 11 
 ARRFDF (4,1) = "DI3PIVA" 
 ARRFDF (4,2) = 11 
 ARRFDF (5,1) = "DI4PIVA" 
 ARRFDF (5,2) = 11
        L_CAMPI="CREATE CURSOR __TMP__ (CODFIS C(16), MODN C(2)"
        this.w_QUADRO = 0
        do while this.w_QUADRO<4
          this.w_QUADRO = this.w_QUADRO+1
          L_CAMPI=L_CAMPI+", DI"+ALLTRIM(STR(this.w_QUADRO))+"PRTOT C(10), DI"+ALLTRIM(STR(this.w_QUADRO))+"PROG C(10), "
          L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"PIVA C(11), DI"+ALLTRIM(STR(this.w_QUADRO))+"COGNOME C(40), "
          L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"NOME C(20), DI"+ALLTRIM(STR(this.w_QUADRO))+"INDIR C(50), "
          L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"COMUNE C(20), DI"+ALLTRIM(STR(this.w_QUADRO))+"PROV C(2), "
          L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"CODSE C(5), DI"+ALLTRIM(STR(this.w_QUADRO))+"TIOPEI C(1), "
          L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"TIOPEO C(1), DI"+ALLTRIM(STR(this.w_QUADRO))+"TIOPED C(1), "
          L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"IMPO C(26), DI"+ALLTRIM(STR(this.w_QUADRO))+"IMPI C(26), "
          L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"GGDA C(2), DI"+ALLTRIM(STR(this.w_QUADRO))+"MMDA C(2), "
          L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"AAAADA C(4), DI"+ALLTRIM(STR(this.w_QUADRO))+"GGA C(2), "
          L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"MMA C(2), DI"+ALLTRIM(STR(this.w_QUADRO))+"AAAAA C(4)"
        enddo
        L_CAMPI=L_CAMPI+")"
        &L_CAMPI
        SELECT "CursDatC"
        GO TOP
        this.w_QUADRO = 0
        this.w_TRASMATT = 1
        L_CAMPI="INSERT INTO __TMP__ (CODFIS, MODN"
        L_VALORI=" VALUES ('"+ALLTRIM(this.oParentObject.w_FOCODFIS)+"', '"+RIGHT("00"+ALLTRIM(STR(this.w_TRASMATT,5,0)),2)+"' "
        do while not eof()
          this.w_QUADRO = this.w_QUADRO+1
          L_CAMPI=L_CAMPI+", DI"+ALLTRIM(STR(this.w_QUADRO,1,0))+"PRTOT, DI"+ALLTRIM(STR(this.w_QUADRO,1,0))+"PROG, DI"+ALLTRIM(STR(this.w_QUADRO,1,0))+"PIVA, DI"+ALLTRIM(STR(this.w_QUADRO,1,0))+"COGNOME, DI"+ALLTRIM(STR(this.w_QUADRO,1,0))+"NOME"
          L_CAMPI=L_CAMPI+", DI"+ALLTRIM(STR(this.w_QUADRO,1,0))+"INDIR, DI"+ALLTRIM(STR(this.w_QUADRO,1,0))+"COMUNE, DI"+ALLTRIM(STR(this.w_QUADRO,1,0))+"PROV"
          L_CAMPI=L_CAMPI+", DI"+ALLTRIM(STR(this.w_QUADRO,1,0))+"CODSE, DI"+ALLTRIM(STR(this.w_QUADRO,1,0))+"TIOPEI, DI"+ALLTRIM(STR(this.w_QUADRO,1,0))+"TIOPEO, DI"+ALLTRIM(STR(this.w_QUADRO,1,0))+"TIOPED, DI"+ALLTRIM(STR(this.w_QUADRO,1,0))+"IMPO, DI"+ALLTRIM(STR(this.w_QUADRO,1,0))+"IMPI"
          L_CAMPI=L_CAMPI+", DI"+ALLTRIM(STR(this.w_QUADRO,1,0))+"GGDA, DI"+ALLTRIM(STR(this.w_QUADRO,1,0))+"MMDA, DI"+ALLTRIM(STR(this.w_QUADRO,1,0))+"AAAADA, DI"+ALLTRIM(STR(this.w_QUADRO,1,0))+"GGA, DI"+ALLTRIM(STR(this.w_QUADRO,1,0))+"MMA, DI"+ALLTRIM(STR(this.w_QUADRO,1,0))+"AAAAA"
          SELECT "CursDatC"
          this.w_SERDIC = IIF(!EMPTY(ALLTRIM(NVL(DISERDIC,""))),"/"+ALLTRIM(DISERDIC),"")
          this.w_SERDOC = IIF(!EMPTY(ALLTRIM(NVL(DISERDOC,""))),"/"+ALLTRIM(DISERDOC),"")
          L_VALORI=L_VALORI+", '"+ALLTRIM(STR(DINUMDIC))+this.w_SERDIC+"', '"+ALLTRIM(STR(DINUMDOC))+this.w_SERDOC+"', '"+ANPARIVA+"', '"+TrasStr(ANCOGNOM)+"', '"+TrasStr(AN__NOME)+"', '"+ TrasStr(ANINDIRI)+"', '"+TrasStr(ANLOCALI)
          L_VALORI=L_VALORI+"', '"+TrasStr(NVL(ANPROVIN,"  "))+"', '"+TrasStr(NVL(ANCODEST,"   "))+"', "+IIF(DIFLOPTO=1,"'X',","' ', ")+IIF(DIFLOPTI=1,"'X', ","' ', ")+ IIF(DIFLOPTD=1,"'X'","' '")
          if DIFLOPTO=1
            L_VALORI=L_VALORI+", '"+ALLTRIM(STR(DIIMPDICO,23,2))+"', ' '"
          endif
          if DIFLOPTI=1
            L_VALORI=L_VALORI+", ' ', '"+ALLTRIM(STR(DIIMPDICI,23,2))+"'"
          endif
          if DIFLOPTD=1
            L_VALORI=L_VALORI+", ' ', ' '"
            L_VALORI=L_VALORI+", '"+RIGHT("00"+ALLTRIM(STR(DAY(DIDATINI))),2)+"', '"+RIGHT("00"+ALLTRIM(STR(MONTH(DIDATINI))),2)+"', '"+ALLTRIM(STR(YEAR(DIDATINI)))
            L_VALORI=L_VALORI+"', '"+RIGHT("00"+ALLTRIM(STR(DAY(DIDATFIN))),2)+"', '"+RIGHT("00"+ALLTRIM(STR(MONTH(DIDATFIN))),2)+"', '"+ALLTRIM(STR(YEAR(DIDATFIN)))+"'"
          else
            L_VALORI=L_VALORI+", '  ', '  ', '    ', '  ', '  ', '    '"
          endif
          if this.w_QUADRO=4
            L_MACRO=L_CAMPI+")"+L_VALORI+")"
            &L_MACRO
            this.w_QUADRO = 0
            this.w_TRASMATT = this.w_TRASMATT+1
            L_CAMPI="INSERT INTO __TMP__ (CODFIS, MODN"
            L_VALORI=" VALUES ('"+ALLTRIM(this.oParentObject.w_FOCODFIS)+"', '"+RIGHT("00"+ALLTRIM(STR(this.w_TRASMATT,5,0)),2)+"' "
          endif
          SELECT "CursDatC"
          SKIP
        enddo
        if this.w_QUADRO>0
          L_MACRO=L_CAMPI+")"+L_VALORI+")"
          &L_MACRO
        endif
        result1=cp_ffdf(" ","QDILETINT.pdf",this," ",@ARRFDF)
        do case
          case Not Left( result1 , 1) $ "0123456789"
            * --- Errore durante creazione di un FDF..
            this.w_MESS = w_RESFDF
            ah_ErrorMsg(this.w_MESS,"stop","")
          case Left( result1 , 1) = "0"
            * --- Nessun FDF creato...
            this.w_MESS = "Nessun file PDF da generare."
            ah_ErrorMsg(this.w_MESS,"stop","")
          otherwise
            this.w_QUADRO = 1
            do while this.w_QUADRO<=Val( result1 )
              CP_CHPRN(tempadhoc()+"\QDILETINT"+ALLTRIM(STR(this.w_QUADRO))+".FDF",.NULL.,2)
              this.Pag6()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              this.w_QUADRO = this.w_QUADRO+1
            enddo
        endcase
    endcase
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc
  proc Try_00F3C0A8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    i_Conn=i_TableProp[this.DIC_GENE_IDX, 3]
    cp_NextTableProg(this, i_Conn, "SEDIGE", "i_codazi,w_GCSERIAL")
    * --- Insert into DIC_GENE
    i_nConn=i_TableProp[this.DIC_GENE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DIC_GENE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DIC_GENE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"GCSERIAL"+",GCANNO"+",GCMESE"+",GDDATAEL"+",GSFILEGE"+",GCDATAIN"+",GCDATAFI"+",GSNUMRIG"+",GCFLCORR"+",UTCC"+",UTCV"+",UTDC"+",UTDV"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_GCSERIAL),'DIC_GENE','GCSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FOANNO),'DIC_GENE','GCANNO');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FOMESE),'DIC_GENE','GCMESE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DTGENERA),'DIC_GENE','GDDATAEL');
      +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(this.w_FileOut)),'DIC_GENE','GSFILEGE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATINI),'DIC_GENE','GCDATAIN');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATFIN),'DIC_GENE','GCDATAFI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGHE),'DIC_GENE','GSNUMRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FLAGCOR),'DIC_GENE','GCFLCORR');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'DIC_GENE','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(0),'DIC_GENE','UTCV');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'DIC_GENE','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'DIC_GENE','UTDV');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'GCSERIAL',this.w_GCSERIAL,'GCANNO',this.oParentObject.w_FOANNO,'GCMESE',this.oParentObject.w_FOMESE,'GDDATAEL',this.w_DTGENERA,'GSFILEGE',ALLTRIM(this.w_FileOut),'GCDATAIN',this.oParentObject.w_DATINI,'GCDATAFI',this.oParentObject.w_DATFIN,'GSNUMRIG',this.w_NUMRIGHE,'GCFLCORR',this.oParentObject.w_FLAGCOR,'UTCC',i_CODUTE,'UTCV',0,'UTDC',SetInfoDate( g_CALUTD ))
      insert into (i_cTable) (GCSERIAL,GCANNO,GCMESE,GDDATAEL,GSFILEGE,GCDATAIN,GCDATAFI,GSNUMRIG,GCFLCORR,UTCC,UTCV,UTDC,UTDV &i_ccchkf. );
         values (;
           this.w_GCSERIAL;
           ,this.oParentObject.w_FOANNO;
           ,this.oParentObject.w_FOMESE;
           ,this.w_DTGENERA;
           ,ALLTRIM(this.w_FileOut);
           ,this.oParentObject.w_DATINI;
           ,this.oParentObject.w_DATFIN;
           ,this.w_NUMRIGHE;
           ,this.oParentObject.w_FLAGCOR;
           ,i_CODUTE;
           ,0;
           ,SetInfoDate( g_CALUTD );
           ,cp_CharToDate("  -  -    ");
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiudo i cursori
    if USED("CursDatC")
      SELECT "CursDatC"
      USE
    endif
    if USED("__TMP__")
      SELECT "__TMP__"
      USE
    endif
    * --- Abbasso il flag semaforo
    if this.w_TIPOPE $ "GF"
      * --- Write into AZIENDA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.AZIENDA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"AZBLOGCL ="+cp_NullLink(cp_ToStrODBC("N"),'AZIENDA','AZBLOGCL');
            +i_ccchkf ;
        +" where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
               )
      else
        update (i_cTable) set;
            AZBLOGCL = "N";
            &i_ccchkf. ;
         where;
            AZCODAZI = this.w_CODAZI;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_FLFLDPOS = LEFT(this.w_ParScri,1)="P"
    this.w_NrChrWri = this.w_NrChrWri+LEN(this.w_INGRESSO)
    if this.w_FLFLDPOS
      this.w_CATFLD = SUBSTR(this.w_ParScri,2,2)
      this.w_FIELDLEN = INT(VAL(ALLTRIM(RIGHT(this.w_ParScri,LEN(this.w_ParScri)-3))))
      do case
        case this.w_CATFLD="AN"
          this.w_Ingresso = LEFT(ALLTRIM(this.w_Ingresso)+REPLICATE(" ",this.w_FIELDLEN),this.w_FIELDLEN)
        case this.w_CATFLD="CF"
          this.w_Ingresso = LEFT(ALLTRIM(this.w_Ingresso)+REPLICATE(" ",this.w_FIELDLEN),this.w_FIELDLEN)
        case this.w_CATFLD="CN"
          this.w_Ingresso = RIGHT(REPLICATE("0",this.w_FIELDLEN)+ALLTRIM(this.w_Ingresso),this.w_FIELDLEN)
        case this.w_CATFLD="PI"
          this.w_Ingresso = LEFT(ALLTRIM(this.w_Ingresso)+REPLICATE(" ",this.w_FIELDLEN),this.w_FIELDLEN)
        case this.w_CATFLD="DT"
          if TYPE("w_INGRESSO")="D"
            this.w_Ingresso = RIGHT("00"+ALLTRIM(STR(DAY(this.w_Ingresso))),2)+RIGHT("00"+ALLTRIM(STR(MONTH(this.w_Ingresso))),2)+RIGHT("0000"+ALLTRIM(STR(YEAR(this.w_Ingresso))),4)
          else
            this.w_Ingresso = ALLTRIM(this.w_Ingresso)
          endif
        case this.w_CATFLD="NU"
          this.w_Ingresso = RIGHT(REPLICATE("0",this.w_FIELDLEN)+ALLTRIM(this.w_INGRESSO),this.w_FIELDLEN)
        case this.w_CATFLD="PR"
          this.w_Ingresso = Right("  " + UPPER(ALLTRIM(this.w_Ingresso)),2)
        case this.w_CATFLD="CB"
          this.w_Ingresso = IIF(ALLTRIM(this.w_Ingresso)="1","1","0")
        case this.w_CATFLD="N2"
          this.w_Ingresso = RIGHT(REPLICATE("0",this.w_FIELDLEN)+ALLTRIM(this.w_INGRESSO),this.w_FIELDLEN)
        case this.w_CATFLD="CC"
          this.w_Ingresso = this.w_Ingresso
      endcase
    endif
    HS=FWRITE(FH,UPPER(this.w_Ingresso))
    if HS<0
      ah_errormsg("Errore durante la scrittura su file")
      =FCLOSE(FH)
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      i_retcode = 'stop'
      return
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do while this.w_LENFILLE>254
      this.w_Ingresso = SPACE(254)
      this.w_ParScri = "PAN254"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_LENFILLE = this.w_LENFILLE-254
    enddo
    this.w_Ingresso = SPACE(this.w_LENFILLE)
    this.w_ParScri = "PAN"+ALLTRIM(STR(this.w_LENFILLE,50,0))
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Campi posizionali
    this.w_Ingresso = "C"
    this.w_ParScri = "PAN1"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_Ingresso = this.oParentObject.w_FOCODFIS
    this.w_ParScri = "PAN16"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_Ingresso = RIGHT("00000000"+ALLTRIM(STR(this.w_TRASMATT)),8)
    this.w_ParScri = "PAN8"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Campi Da 4 a 6 filler 3 25 20 raggruppati
    this.w_Ingresso = SPACE(48)
    this.w_ParScri = "PAN48"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Campo 7 CodFis Zucchetti
    this.w_Ingresso = "05006900962     "
    this.w_ParScri = "PAN16"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_NrChrWri = 74
  endproc


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    wait "" timeout 3
  endproc


  proc Init(oParentObject,w_TIPOPE)
    this.w_TIPOPE=w_TIPOPE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='DIC_GENE'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- gscg_bdg
  enddefine
  ******************************************
  * - Funzioni per campi non posizionali - *
  ******************************************
  
  * - Funzione che ritorna il valore da associare al campo
  func NoPosValue
  parameters pQRC,pValore,pTIPO
  local ValorePlus
  do case
  case pTIPO = 'C'
    if type('pValore')<>'C'
      pValore=ALLTRIM(STR(pvalore))
    endif
    if !empty(pValore) 
      if len(alltrim(pValore)) > 16 && scrivo il dato su pi� campi
        ValorePlus = pQRC+left(pValore,16)
        pValore = substr(pValore,17)
        do while !empty(alltrim(pValore))
          ValorePlus = ValorePlus + pQRC+'+'+left(pValore,15)+space(15-iif(len(pValore)>15,15,len(pValore)))
          pValore = substr(pValore,16)
        enddo
        return ValorePlus
      else
        return (pQRC+left(pValore+space(16),16))
      endif
    else
      return ''
    endif
  case pTIPO = 'N'
    if type('pValore')<>'N'
      if '.' $ pValore Or ',' $ pValore
        pValore=TRANSFORM(pValore,'999999999999999999999999.99')
      else
        pValore=int(val(pvalore))
      endif
    endif
    if pValore <> 0
      return (pQRC+right(space(16)+alltrim(str(pValore,16,2)),16))
    else
      return ''
    endif
  case pTipo ='P'
    if not empty(nvl(pValore,' '))
      return (pQRC+SPACE(5)+right(REPLICATE('0',11)+ALLTRIM(pValore),11))
    else
     return ''
    endif
  case pTIPO = 'D'
    if !empty(pValore)
      if type('pValore')<>'D' and type('pValore')<>'T'
        pValore=cp_CharToDate(pValore)
      endif
      return (pQRC+right(space(16)+left(dtoc(pValore),2)+substr(dtoc(pValore),4,2)+right(dtoc(pValore),4),16))
    else
      return ''
    endif
  case pTIPO = 'F'  
    if pValore=1 
      ValorePlus = pQRC+right(space(16)+alltrim(str(pValore,1,0)),16)
      return ValorePlus
    else
      return ''
    endif
  otherwise
    ah_Msg("Tipo dati non definito: %1",.T.,.F.,.F.,type("pvalore"))
    return ''
  endcase
  endfunc
  
  function TrasStr
  Parameters pIngr
     return STRTRAN( pIngr ,"'","'+"+'"'+"'"+'"+'+"'")
  endfunc
  define class FileTelematico as Custom
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_TIPOPE"
endproc
