* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bdr                                                        *
*              Rettifiche inventariali                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_713]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-02-09                                                      *
* Last revis.: 2017-01-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEvent
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bdr",oParentObject,m.pEvent)
return(i_retval)

define class tgsma_bdr as StdBatch
  * --- Local variables
  pEvent = space(3)
  w_FLLOTT = space(1)
  w_GESMAT = space(1)
  w_DESART = space(40)
  w_DRKEYSAL = space(40)
  w_DRCODRIC = space(41)
  w_DRCODMAT = space(40)
  w_UNIMIS = space(3)
  w_ZOOM = space(10)
  w_DRARTIC = space(20)
  w_DATASTAM = ctod("  /  /  ")
  w_CODLOT = space(20)
  w_CODUBI = space(20)
  w_CODUBF = space(20)
  w_MMFLLOTT = space(1)
  w_MMDESSUP = space(50)
  w_DATREG = ctod("  /  /  ")
  w_SCAR = .f.
  w_CAR = .f.
  w_MMCODUTE = space(4)
  w_MLFLCASC = space(1)
  w_MMDATREG = ctod("  /  /  ")
  w_MMCODESE = space(5)
  w_MMVALNAZ = space(3)
  w_MMTCOMAG = space(5)
  w_MMCODMAG = space(5)
  w_MMDATDOC = ctod("  /  /  ")
  w_MMCODVAL = space(3)
  w_MMCAOVAL = 0
  w_MMTCOCEN = space(15)
  w_MMTCOMME = space(15)
  w_MMTCOATT = space(15)
  w_MMTCOLIS = space(5)
  w_MMTCOMAT = space(5)
  w_MMALFDOC = space(2)
  w_MMTCAMAG = space(5)
  w_MMSERIAL = space(10)
  w_MMNUMREG = 0
  w_MMNUMRIF = 0
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_MMCAUCOL = space(5)
  w_MMFLELAN = space(1)
  w_MMFLELGM = space(1)
  w_MMFLCASC = space(1)
  w_MMFLORDI = space(1)
  w_MMFLIMPE = space(1)
  w_MMFLRISE = space(1)
  w_MM_SEGNO = space(1)
  w_TIPVOC = space(1)
  w_MMFLCLFR = space(1)
  w_MMCAUMAG = space(5)
  w_MMCODMAT = space(5)
  w_MMCODLIS = space(5)
  w_DRUBICAZ = space(20)
  w_DR_LOTTO = space(20)
  w_DRCODVAR = space(20)
  w_DRCODART = space(20)
  w_MMCODICE = space(41)
  w_DRUNIMS = space(3)
  w_DATPROC = 0
  w_QTAESI = 0
  w_QTAAGG = 0
  w_QTAAGGLU = 0
  w_ROWNUM = 0
  w_MMCODICE = space(41)
  w_MMKEYSAL = space(40)
  w_MMUNIMIS = space(3)
  w_MMQTAMOV = 0
  w_MMQTAUM1 = 0
  w_MMTIPATT = space(1)
  w_MMFLOMAG = space(1)
  w_MMCODBUN = space(3)
  w_MMFLORCO = space(1)
  w_MMFLCOCO = space(1)
  w_RECORD = 0
  w_AGGKEYSAL = space(40)
  w_AGGKEYULO = space(40)
  w_AGGMATRI = space(40)
  w_MESS = space(10)
  w_MTMAGCAR = space(5)
  w_MTMAGSCA = space(5)
  w_MTSERRIF = space(10)
  w_MTRIFNUM = 0
  w_MTROWRIF = 0
  w_MTFLCARI = space(1)
  w_MTFLSCAR = space(1)
  w_CDCAUT = space(1)
  w_MMCODCEN = space(15)
  w_DRQTAES1 = 0
  w_SUQTRPER = 0
  w_SUQTAPER = 0
  w_MMLOTMAG = space(5)
  w_REGCARICO = space(1)
  w_GLOB_QTA = 0
  w_COMMDEFA = space(15)
  w_SALCOM = space(1)
  w_AGGCOMM = space(15)
  w_MMF2CASC = space(1)
  w_MMF2ORDI = space(1)
  w_MMF2IMPE = space(1)
  w_MMF2RISE = space(1)
  w_oERRORLOG = .NULL.
  w_VQRGESMAT = space(1)
  w_SLCODMAG = space(20)
  w_ARDTOBSO = ctod("  /  /  ")
  w_MGUBIC = space(1)
  w_ESCLOBSO = space(1)
  w_DRVARIAN = space(20)
  w_MMCODCOM = space(15)
  w_DRUBICAZ_R = space(20)
  w_DR_LOTTO_R = space(20)
  w_ROTLOTUBI = space(40)
  w_COMAG = space(5)
  w_CODUBI = space(20)
  w_CODUB2 = space(20)
  w_COART = space(5)
  w_QTAESI = 0
  w_TESDIS = .f.
  w_NUMDOC = 0
  w_TIPCON = space(1)
  w_GMCODART = space(20)
  w_CODVAR = space(20)
  w_KEYLOTTO = space(20)
  w_KEYULO = space(40)
  w_DRCODCOM = space(15)
  w_DRUNIMIS = space(3)
  w_FLFRAZ = space(1)
  w_CODMAT = space(20)
  w_DRSERIAL = space(15)
  * --- WorkFile variables
  ART_ICOL_idx=0
  CAM_AGAZ_idx=0
  MVM_DETT_idx=0
  MVM_MAST_idx=0
  SALDIART_idx=0
  VALUTE_idx=0
  RILEVAZI_idx=0
  MOVIMATR_idx=0
  TMPVEND3_idx=0
  LOTTIART_idx=0
  SALDILOT_idx=0
  KEY_ARTI_idx=0
  UNIMIS_idx=0
  RILEVAZI_idx=0
  MAGAZZIN_idx=0
  SALDICOM_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Configura lo Zoom ed esegue l'aggiornamento del Magazzino(da GSMA_SDR)
    *     pEvent='MOV' Genera Movimento di Rettifica
    *     pEvent='RIC' Riempio lo Zoom di Visualizzazione
    * --- Dichiarazioni Variabili
    * --- Poich� le query utilizzate in questo batch sono utilizzate anche nel GSMA_BSD
    *     ma i filtri sono differenti per alcune stampe, la variabile ORIG serve per capire che tipo di filtro devo
    *     fare nelle query.
    do case
      case this.pEvent="MOV"
        this.w_oERRORLOG=createobject("AH_ErrorLog")
        * --- Verifico la Data di Consolidamento Magazzino
        if g_APPLICATION = "ADHOC REVOLUTION" And Not Empty(CHKCONS("M",this.oParentObject.w_DATARIL,"B","N"))
          ah_ErrorMsg("Impossibile generare movimenti di magazzino di rettifica%0%1","!","",CHKCONS("M",this.oParentObject.w_DATARIL,"B","N") )
          i_retcode = 'stop'
          return
        endif
        if g_APPLICATION = "ADHOC REVOLUTION" And Not Empty(CHKCONS("M",this.oParentObject.w_DATARIL,"B","N"))
          * --- Mi assicuro che la variabile sia space di 15 (per evitare problemi con Oracle)
          this.w_COMMDEFA = NVL(g_PPCODCOM, SPACE(15))
        endif
        * --- Verifico se le Causali di Magazzino sono avvalorate
        if EMPTY(this.oParentObject.w_CAUCAR) OR EMPTY(this.oParentObject.w_CAUSCA)
          this.w_MESS = "Alla rilevazione %1 non sono associate le causali di magazzino%0Impossibile proseguire"
          ah_ErrorMsg(this.w_MESS,"!","",alltrim(this.oParentObject.w_CODRIL) )
          i_retcode = 'stop'
          return
        endif
        * --- Genera  i movimenti di Magazzino
        MM = this.oParentObject.w_ZoomConf.cCursor
        * --- Articoli non rilevati selezionati, occorre specificare l'addetto alla rilevazione
        if this.oParentObject.w_FLNONRIL="S"
          SELECT * FROM (MM) WHERE XCHK=1 AND FLNONRIL="S" AND PROCEDUR<>DRQTAES1 Into cursor "AppMM"
          if USED("AppMM")
            SELECT "AppMM"
            GO TOP
            if RECCOUNT()>0 AND EMPTY(this.oParentObject.w_CODUTE)
              AH_ERRORMSG("Selezionati articoli non rilevati e addetto alla rilevazione non specificato%0Impossibile proseguire",48,"")
              USE IN SELECT("AppMM")
              i_retcode = 'stop'
              return
            else
              * --- Generazione rilevazioni a zero per articoli selezionati non rilevati
              this.Page_4()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
          USE IN SELECT("AppMM")
        endif
        * --- Righe selezionate
        Select DRCODART, NVL(DRCODVAR,SPACE(20)) AS DRCODVAR,DRUNIMIS, DRKEYSAL,DRKEYULO,DRCODRIC,; 
 NVL(DRCODMAT,SPACE(40)) AS DRCODMAT,DRQTAES1,PROCEDUR,DR_LOTTO,DRUBICAZ,NVL(DRCODCOM,SPACE(15)) as DRCODCOM; 
 From (MM) Where XCHK=1 Into Cursor AppMM
        if g_MATR="S"
          CURTOTAB("AppMM", "TMPVEND1")
          * --- Select from GSMAMSDR
          do vq_exec with 'GSMAMSDR',this,'_Curs_GSMAMSDR','',.f.,.t.
          if used('_Curs_GSMAMSDR')
            select _Curs_GSMAMSDR
            locate for 1=1
            do while not(eof())
            if NVL(MTNUMRIF,0)=-20
              do case
                case not empty(NVL(MTMAGSCA,SPACE(5)))
                  this.w_oERRORLOG.AddMsgLog("Articolo %1 - matricola %2 gi� scaricato nel documento %3 del %4, causale %5", Alltrim(MTKEYSAL), Alltrim(MTCODMAT), Alltrim(Str(NUMREG,15))+ IIF(Not Empty(ALFDOC),"/"+Alltrim(ALFDOC),""), Alltrim(DTOC(CP_TODATE(DATREG))), Alltrim(CAUSALE))     
                case not empty(NVL(MTMAGCAR,SPACE(5)))
                  this.w_oERRORLOG.AddMsgLog("Articolo %1 - matricola %2 gi� caricato nel documento %3 del %4, causale %5", Alltrim(MTKEYSAL), Alltrim(MTCODMAT), Alltrim(Str(NUMREG,15))+ IIF(Not Empty(ALFDOC),"/"+Alltrim(ALFDOC),""), Alltrim(DTOC(CP_TODATE(DATREG))), Alltrim(CAUSALE))     
              endcase
            else
              do case
                case not empty(NVL(MTMAGSCA,SPACE(5)))
                  this.w_oERRORLOG.AddMsgLog("Articolo %1 - matricola %2 gi� scaricato nel movimento di magazzino %3 del %4, causale %5", Alltrim(MTKEYSAL), Alltrim(MTCODMAT), Alltrim(Str(NUMREG,15)), Alltrim(DTOC(CP_TODATE(DATREG))), Alltrim(CAUSALE))     
                case not empty(NVL(MTMAGCAR,SPACE(5)))
                  this.w_oERRORLOG.AddMsgLog("Articolo %1 - matricola %2 gi� caricato nel movimento di magazzino %3 del %4, causale %5", Alltrim(MTKEYSAL), Alltrim(MTCODMAT), Alltrim(Str(NUMREG,15)), Alltrim(DTOC(CP_TODATE(DATREG))), Alltrim(CAUSALE))     
              endcase
            endif
              select _Curs_GSMAMSDR
              continue
            enddo
            use
          endif
          this.w_oERRORLOG.PrintLog(this.oParentObject,"Movimenti incongruenti",.T., "Stampo la situazione dei movimenti matricole incongruenti?")     
          if this.w_oERRORLOG.isfulllog() AND !Ah_YesNo("Confermi la generazione dei movimenti incongruenti?")
            ah_ErrorMsg("Operazione sospesa dall'utente")
            * --- Drop temporary table TMPVEND1
            i_nIdx=cp_GetTableDefIdx('TMPVEND1')
            if i_nIdx<>0
              cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
              cp_RemoveTableDef('TMPVEND1')
            endif
            USE IN SELECT("AppMM")
            i_retcode = 'stop'
            return
          endif
        endif
        * --- Nel Cursore DettCarico sono memorizzate le righe aventi la quantit� rilevata maggiore del dato procedurale
        Select * from AppMM Where nvl(PROCEDUR,0) < nvl(DRQTAES1,0) Into Cursor DettCarico
        if used("DettCarico")
          if g_APPLICATION <> "ADHOC REVOLUTION"
            * --- Si raggruppa per codice articolo + variante per definire la riga da inserire nel movimento di magazzino di carico
            Select DRCODART,NVL(DRCODVAR,SPACE(20)) AS DRCODVAR , MAX(DRUNIMIS) AS DRUNIMIS,; 
 SUM(NVL(PROCEDUR,0)) AS PROCEDUR ,DRKEYSAL,DRCODRIC,SUM(NVL(DRQTAES1,0)) AS DRQTAES1; 
 From DettCarico order by DRCODART, DRCODVAR group by DRCODART,DRCODVAR; 
 Into Cursor Carico 
          else
            * --- Si raggruppa per codice articolo + lotto + ubicazione+commessa per definire la riga da inserire nel movimento di magazzino di carico
            Select DRCODART,NVL(DRCODVAR,SPACE(20)) AS DRCODVAR , MAX(DRUNIMIS) AS DRUNIMIS,; 
 SUM(NVL(PROCEDUR,0)) AS PROCEDUR ,DRKEYSAL,DRCODRIC,SUM(NVL(DRQTAES1,0)) AS DRQTAES1,DR_LOTTO,DRUBICAZ,DRCODCOM; 
 From DettCarico order by DRCODART, DRCODVAR group by DRCODART,DRCODVAR,DR_LOTTO,DRUBICAZ,DRCODCOM; 
 Into Cursor Carico 
          endif
          this.w_CAR = ( _TALLY>0 )
        endif
        * --- Nel Cursore DettScarico sono memorizzate le righe aventi la quantit� rilevata minore del dato procedurale
        Select * from AppMM Where nvl(PROCEDUR,0) > nvl(DRQTAES1,0) Into Cursor DettScarico 
        if used("DettScarico")
          if g_APPLICATION <> "ADHOC REVOLUTION"
            * --- Si raggruppa per codice articolo + variante + matricola per definire la riga da inserire nel movimento di scarico
            Select DRCODART, NVL(DRCODVAR,SPACE(20)) AS DRCODVAR, MAX(DRUNIMIS) AS DRUNIMIS,; 
 SUM(NVL(PROCEDUR,0)) AS PROCEDUR, DRKEYSAL,DRCODRIC, SUM(NVL(DRQTAES1,0)) AS DRQTAES1; 
 From DettScarico order by DRCODART, DRCODVAR group by DRCODART,DRCODVAR; 
 Into Cursor Scarico
          else
            * --- Si raggruppa per codice articolo + matricola + lotto + ubicazione+commessa per definire la riga da inserire nel movimento di scarico
            Select DRCODART, NVL(DRCODVAR,SPACE(20)) AS DRCODVAR, MAX(DRUNIMIS) AS DRUNIMIS,; 
 SUM(NVL(PROCEDUR,0)) AS PROCEDUR, DRKEYSAL,DRCODRIC, SUM(NVL(DRQTAES1,0)) AS DRQTAES1,DR_LOTTO,DRUBICAZ,DRCODCOM; 
 From DettScarico order by DRCODART, DRCODVAR group by DRCODART,DRCODVAR,DR_LOTTO,DRUBICAZ,DRCODCOM; 
 Into Cursor Scarico
          endif
          this.w_SCAR = ( _TALLY>0 )
        endif
        if NOT(this.w_CAR or this.w_SCAR)
          ah_ErrorMsg("Nessun movimento di magazzino da generare","!")
          * --- Disattivo tutti i check
          UPDATE (MM) SET XCHK=0 
        else
          * --- Mi assicuro che la variabile sia space di 15 (per evitare problemi con Oracle)
          this.w_COMMDEFA = NVL(g_PPCODCOM, SPACE(15))
          this.w_DATREG = this.oParentObject.w_DATARIL
          * --- Genezione Movimenti di Magazzino (si ricorda che le causali di Magazzino
          *     sono impostate nell'Anagrafica Codici Rilevazione)
          * --- Try
          local bErr_04038328
          bErr_04038328=bTrsErr
          this.Try_04038328()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
            ah_ErrorMsg("Impossibile aggiornare il magazzino","!")
          endif
          bTrsErr=bTrsErr or bErr_04038328
          * --- End
        endif
      case this.pEvent="RIC"
        this.w_COMMDEFA = SPACE(15)
        * --- Creo una tabella temporanea con gli articoli corretti in base ai filtri di selezione
        * --- Create temporary table TMPVEND3
        i_nIdx=cp_AddTableDef('TMPVEND3') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('QUERY\GSMA9SDP',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPVEND3_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        this.w_DATASTAM = this.oParentObject.w_DATARIL
        this.w_ZOOM = this.oParentObject.w_ZoomConf
        * --- Query per reperire i dati rilevati
        VQ_EXEC("QUERY\GSMA_SDR.VQR",this,"CONFR")
        ah_Msg("Ricerca dati in corso...")
        * --- Cancello il contenuto dello Zoom
        Select (this.w_ZOOM.cCursor)
        Zap
        * --- Parametro per decidere se gestire gli articoli a matricole a parte o meno.
        *     Utilizzato se la dta rilevazione � prima o dopo la data inizio matricole
        this.w_VQRGESMAT = IIF ( this.oParentObject.w_DATARIL>=Nvl( g_DATMAT, cp_CharToDate( "  /  /    " )) , "S", " " )
        * --- Queries per reperire i dati procedurali
        if this.oParentObject.w_UBICAZ = "S" 
          * --- Calcolo dato Procedurale  con ubicazioni e lotti
          vq_exec("query\GSMA4SDP",this,"Ubilot")
        else
          * --- Calcolo dato Procedurale  senza ubicazioni 
          * --- Query relativa ad Articoli non gestiti a Lotti
          vq_exec("query\GSMA_SDP",this,"UbilotN")
          vq_exec("query\GSMA_SDPC",this,"UbilotC")
          * --- Query relativa ad Articoli gestiti a Lotti
          vq_exec("query\GSMA4SDP",this,"UbilotL")
          * --- Union dei due cursori
          select SLCODICE,CACODART,NVL(CACODVAR,SPACE(20)) AS CACODVAR,SLQTAPER,ESICARPRI,ESISCAPRI,ESICARCOL,ESISCACOL,; 
 space(20) as MLCODUBI, space(20) as MLCODLOT,CODCOM from UbilotN; 
 union all; 
 select SLCODICE,CACODART,NVL(CACODVAR,SPACE(20)) AS CACODVAR,SLQTAPER,ESICARPRI,ESISCAPRI,ESICARCOL,ESISCACOL,; 
 space(20) as MLCODUBI, space(20) as MLCODLOT,CODCOM from UbilotC; 
 union all; 
 Select SLCODICE,CACODART,NVL(CACODVAR,SPACE(20)) AS CACODVAR,SLQTAPER,ESICARPRI,ESISCAPRI,ESICARCOL,ESISCACOL,; 
 NVL(MLCODUBI,SPACE(20)) AS MLCODUBI,NVL(MLCODLOT,SPACE(20)) AS MLCODLOT,CODCOM from UbilotL; 
 into cursor Ubilot
          use in select("UbilotN")
          use in select("UbilotC")
          use in select("UbilotL")
        endif
        * --- Gestione Matricole - Se la data rilevazione e maggiore uguale alla data inizio gestione matricole
        if this.w_VQRGESMAT="S"
          vq_exec("query\GSMAMSDP",this,"Matr")
          * --- Ragguppo i dati procedurali per Articolo, Variante,Ubicazione, Lotto, matricola, commessa
          Select slcodice,cacodart,nvl(cacodvar,space(20)) as cacodvar,mlcodubi,nvl(mlcodlot,space(20)) as mlcodlot,; 
 Space(40) as mtcodmat,space(5) as slcodmag, codcom, slqtaper, space(3) as arunmis1, sum(EsiCarPri - EsiScaPri) as EsisPrin,; 
 sum(EsiCarCol - EsiScaCol) as EsisColl from Ubilot group by cacodart, cacodvar, mlcodubi, mlcodlot,mtcodmat,codcom; 
 Union all Select * from Matr into Cursor AppUbilot 
        else
          * --- Ragguppo i dati procedurali per Articolo, Variante,Ubicazione, Lotto, matricola , commessa
          Select slcodice,cacodart,nvl(cacodvar,space(20)) as cacodvar,mlcodubi,nvl(mlcodlot,space(20)) as mlcodlot,; 
 Space(40) as mtcodmat,space(5) as slcodmag, max(codcom) as codcom, slqtaper, space(3) as arunmis1, sum(EsiCarPri - EsiScaPri) as EsisPrin,; 
 sum(EsiCarCol - EsiScaCol) as EsisColl from Ubilot group by cacodart, cacodvar, mlcodubi, mlcodlot,mtcodmat,codcom; 
 into Cursor AppUbilot 
        endif
        * --- Si crea un unico cursore contenente i dati per il calcolo del dato procedurale ed il dato rilevato
         select drcodart,drcodvar,desart,; 
 nvl(drubicaz,space(20)) as drubicaz,; 
 nvl(dr_lotto,space(20)) as dr_lotto, nvl(drCODCOM, space(15)) as DRCODCOM, drkeyulo,nvl(drcodmat, space(40)) as drcodmat, ; 
 drunimis, drqtaes1,drkeysal,drcodric,slqtaper as EsisAttu,; 
 EsisPrin,EsisColl, (0*Slqtaper) as Procedur, (0*slqtaper) as GlProced, "N" as FLNONRIL ; 
 from confr left outer join AppUbilot ; 
 on drkeysal = slcodice and nvl(drubicaz,space(20)) = nvl(mlcodubi,space(20)) and nvl(dr_lotto,space(20)) = nvl(mlcodlot,space(20)); 
 and nvl(drcodmat, space(40))=nvl(mtcodmat, space(40)) AND NVL(drcodcom, SPACE(15))=NVL(codcom, SPACE(15)) into cursor TotCur NoFilter
        if this.oParentObject.w_FLNONRIL="S"
          * --- Aggiungo al cursore gli articoli non rilevati per predisporli alla generazione
          *     della rilevazione a zero e conseguente movimento di rettifica
          ah_Msg("Elaborazione dati...")
          * --- Queries per reperire i dati procedurali (Inventario Fisico)
          * --- Creo una tabella temporanea con gli articoli corretti in base ai filtri di selezione
          * --- Create temporary table TMPVEND3
          i_nIdx=cp_AddTableDef('TMPVEND3') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('query\gsma9sdp',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.TMPVEND3_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          * --- Calcolo dato Procedurale  senza ubicazioni 
          * --- Query relativa ad Articoli non gestiti a Lotti
          if UPPER(g_APPLICATION)="AD HOC ENTERPRISE" AND w_ESISTECOM
            if !Empty(this.oParentObject.w_CODCOM)
              * --- Estrazione saldi per commessa
              vq_exec("query\GSMA_SDPC",this,"Ubilot1C")
              * --- Estrazione movimenti
              vq_exec("query\GSMA1SDP",this,"Ubilot1N")
              Select * from Ubilot1C union all select * from Ubilot1N into cursor Ubilot1 readwrite
            else
              vq_exec("query\GSMA_SDPC",this,"Ubilot1C")
              vq_exec("query\GSMASSDPC",this,"Ubilot1L")
              vq_exec("query\GSMA_SDP",this,"Ubilot1N")
              Select * from Ubilot1N union all select * from Ubilot1C union all select * from Ubilot1L into cursor Ubilot1 readwrite
              * --- Per il resto dell'elaborazione serve solo il cursore Ubilot1, elimino gli altri due
            endif
            use in select("Ubilot1N")
            use in select("Ubilot1C")
            use in select("Ubilot1L")
          else
            vq_exec("query\GSMA_SDP",this,"Ubilot1")
          endif
          * --- Query relativa ad Articoli gestiti a Lotti
          if UPPER(g_APPLICATION)="AD HOC ENTERPRISE"
            if w_ESISTECOM
              if !Empty(this.oParentObject.w_CODCOM)
                * --- Calcolo dato Procedurale  con ubicazioni e lotti e commessa
                vq_exec("query\GSMA4RELU",this,"Ubilot2C")
                vq_exec("query\GSMA1RELU",this,"Ubilot2N")
                Select * from Ubilot2C union all select * from Ubilot2N into cursor Ubilot2 readwrite
                use in select("Ubilot2N")
                use in select("Ubilot2C")
              else
                * --- Se esiste almenu un articolo gestito a commessa eseguo entrambe le query
                * --- Calcolo dato Procedurale  con ubicazioni e lotti e commessa
                vq_exec("query\GSMA4RELU",this,"Ubilot2C")
                vq_exec("query\GSMASRELUC",this,"Ubilot2L")
                * --- Calcolo dato Procedurale  con ubicazioni e lotti
                vq_exec("query\GSMA_RELU",this,"Ubilot2N")
                * --- Devo sottrarre dal saldo per lotti/ubicazione quello per commessa 
                *     in modo da  ricavare il saldo non abbinato a commessa
                * --- Per gli articoli gestiti a commessa devo calcolare la parte non abbinata a commessa
                Select * from Ubilot2N union all select * from Ubilot2C union all select * from Ubilot2L into cursor Ubilot2 readwrite
                use in select("Ubilot2N")
                use in select("Ubilot2C")
                use in select("Ubilot2L")
              endif
            else
              vq_exec("query\GSMA_RELU",this,"Ubilot2")
            endif
            select SLCODICE,CACODART,NVL(CACODVAR,SPACE(20)) AS CACODVAR,ARUNMIS1,SLQTAPER,ESICARPRI,ESISCAPRI,ESICARCOL,ESISCACOL,; 
 SLCODMAG,space(20) as MLCODUBI, space(20) as MLCODLOT, space(20) as KEYLOTTO, CODCOM, FLLOTT, MGUBIC, GESMAT, GESCOM, DESART from Ubilot1; 
 union all; 
 Select SLCODICE, CACODART,NVL(CACODVAR,SPACE(20)) AS CACODVAR,ARUNMIS1,SLQTAPER,ESICARPRI,ESISCAPRI,ESICARCOL,ESISCACOL,; 
 SLCODMAG,NVL(MLCODUBI,SPACE(20)) AS MLCODUBI,NVL(MLCODLOT,SPACE(20)) AS MLCODLOT,NVL(KEYLOTTO,SPACE(20)) AS KEYLOTTO, CODCOM ,; 
 FLLOTT, MGUBIC, GESMAT, GESCOM, DESART from Ubilot2; 
 into cursor Ubilot
          else
            vq_exec("query\GSMA4SDP",this,"Ubilot2")
            select SLCODICE,CACODART,NVL(CACODVAR,SPACE(20)) AS CACODVAR,ARUNMIS1,SLQTAPER,ESICARPRI,ESISCAPRI,ESICARCOL,ESISCACOL,; 
 SLCODMAG,space(20) as MLCODUBI, space(20) as MLCODLOT, CODCOM from Ubilot1; 
 union all; 
 Select SLCODICE, CACODART,NVL(CACODVAR,SPACE(20)) AS CACODVAR,ARUNMIS1,SLQTAPER,ESICARPRI,ESISCAPRI,ESICARCOL,ESISCACOL,; 
 SLCODMAG,NVL(MLCODUBI,SPACE(20)) AS MLCODUBI,NVL(MLCODLOT,SPACE(20)) AS MLCODLOT, CODCOM from Ubilot2; 
 into cursor Ubilot
          endif
          * --- Gestione Matricole
          if UPPER(g_APPLICATION)="AD HOC ENTERPRISE" AND w_ESISTECOM
            if !Empty(this.oParentObject.w_CODCOM)
              vq_exec("query\GSMAMSDPC",this,"Matr")
            else
              vq_exec("query\GSMAMSDP",this,"MatrN")
              vq_exec("query\GSMAMSDPC",this,"MatrC")
              * --- Per gli articoli gestiti a commessa devo calcolare la parte non abbinata a commessa
              select SUM(slqtaper) as slqtaper , SUM(ESISPRIN) AS ESISPRIN, SUM(ESISCOLL) AS ESISCOLL, slcodmag,cacodart, cacodvar, mlcodubi, mlcodlot, mtcodmat ; 
 FROM MatrC INTO CURSOR MatrSC GROUP BY slcodmag,cacodart, cacodvar, mlcodubi, mlcodlot, mtcodmat
              * --- Per gli articoli gestiti a commessa devo calcolare la parte non abbinata a commessa
               SELECT MatrN.SLCODICE, MatrN.CACODART, MatrN.CACODVAR, MatrN.MLCODUBI, MatrN.MLCODLOT,MatrN.mtcodmat,; 
 MatrN.SLCODMAG, MatrN.SLQTAPER - NVL(MatrSC.SLQTAPER,0) as SLQTAPER, MatrN.ARUNMIS1, MatrN.EsisPrin, ; 
 MatrN.EsisColl, MatrN.KEYLOTTO, MatrN.CODCOM, MatrN.FLLOTT, MatrN.MGUBIC, MatrN.GESMAT, MatrN.GESCOM, ; 
 MatrN.DESART FROM MatrN left outer join MatrSC on MatrN.SLCODMAG=MatrSC.SLCODMAG AND; 
 MatrN.CACODART=MatrSC.CACODART AND MatrN.CACODVAR=MatrSC.CACODVAR ; 
 AND MatrN.MLCODUBI=MatrSC.MLCODUBI AND MatrN.MLCODLOT=MatrSC.MLCODLOT AND; 
 MatrN.MTCODMAT=MatrSC.MTCODMAT into cursor MatrSN
              use in select("MatrN")
              use in select("MatrSC")
              Select * from MatrSN union all select * from MatrC into cursor Matr readwrite
              * --- Per il resto dell'elaborazione serve solo il cursore Ubilot1, elimino gli altri due
              use in select("MatrSN")
              use in select("MatrC")
            endif
          else
            vq_exec("query\GSMAMSDP",this,"Matr")
          endif
          * --- Ragguppo i dati procedurali per Articolo, Variante,Ubicazione, Lotto, commessa
          if UPPER(g_APPLICATION)="AD HOC ENTERPRISE"
            Select slcodice,cacodart,nvl(cacodvar,space(20)) as cacodvar,slcodmag,mlcodubi,nvl(mlcodlot, space(20)) as mlcodlot,; 
 space(40) as mtcodmat, arunmis1,slqtaper as EsisAttu, sum(EsiCarPri - EsiScaPri) as EsisPrin,; 
 fllott, mgubic, gesmat,gescom,sum(EsiCarCol - EsiScaCol) as EsisColl, (0*Slqtaper) as Procedur, desart,nvl(keylotto, ; 
 space(20)) as keylotto, codcom ; 
 from Ubilot group by slcodmag,cacodart, cacodvar, mlcodubi, mlcodlot, codcom ; 
 Union all; 
 Select slcodice,cacodart, cacodvar,slcodmag,mlcodubi,mlcodlot,mtcodmat,arunmis1,slqtaper as EsisAttu,EsisPrin,; 
 fllott, mgubic,gesmat,gescom,0 as EsisColl,0 as Procedur, desart, keylotto, codcom from Matr; 
 into Cursor AppUbilot 
          else
            Select slcodice,cacodart,nvl(cacodvar,space(20)) as cacodvar,slcodmag,mlcodubi,nvl(mlcodlot, space(20)) as mlcodlot,; 
 space(40) as mtcodmat, arunmis1,slqtaper as EsisAttu, sum(EsiCarPri - EsiScaPri) as EsisPrin,; 
 space(1) as fllott, space(1) as mgubic, space(1) as gesmat,sum(EsiCarCol - EsiScaCol) as EsisColl, (0*Slqtaper) as Procedur, space(40) as desart, codcom; 
 from Ubilot group by slcodmag,cacodart, cacodvar, mlcodubi, mlcodlot, codcom ; 
 Union all; 
 Select slcodice,cacodart, cacodvar,slcodmag,mlcodubi,mlcodlot,mtcodmat,arunmis1,slqtaper as EsisAttu,EsisPrin,; 
 space(1) as fllott, space(1) as mgubic,space(1) as gesmat,0 as EsisColl,0 as Procedur, space(40) as desart, codcom from Matr; 
 into Cursor AppUbilot 
          endif
          =WRCURSOR("AppUbilot")
          GO TOP
          SCAN
          this.w_DRARTIC = CACODART
          this.w_SLCODMAG = SLCODMAG
          this.w_FLLOTT = " "
          this.w_ARDTOBSO = CTOD("  /  /    ")
          if UPPER(g_APPLICATION)="ADHOC REVOLUTION"
            * --- Verifico se l'Articolo � gestito a lotti o a Matrcole
            * --- Leggo la data di obsolescenza. Se richiesto ( w_ESCLOBSO = "E" ) 
            *     gli articoli obsoleti con dato procedurale a zeo saranno eliminati.
            * --- Read from ART_ICOL
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ART_ICOL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ARFLLOTT,ARGESMAT,ARDESART,ARDTOBSO"+;
                " from "+i_cTable+" ART_ICOL where ";
                    +"ARCODART = "+cp_ToStrODBC(this.w_DRARTIC);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ARFLLOTT,ARGESMAT,ARDESART,ARDTOBSO;
                from (i_cTable) where;
                    ARCODART = this.w_DRARTIC;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_FLLOTT = NVL(cp_ToDate(_read_.ARFLLOTT),cp_NullValue(_read_.ARFLLOTT))
              this.w_GESMAT = NVL(cp_ToDate(_read_.ARGESMAT),cp_NullValue(_read_.ARGESMAT))
              this.w_DESART = NVL(cp_ToDate(_read_.ARDESART),cp_NullValue(_read_.ARDESART))
              this.w_ARDTOBSO = NVL(cp_ToDate(_read_.ARDTOBSO),cp_NullValue(_read_.ARDTOBSO))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Verifico se il Magazzino � gestito ad ubicazioni
            * --- Read from MAGAZZIN
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "MGFLUBIC"+;
                " from "+i_cTable+" MAGAZZIN where ";
                    +"MGCODMAG = "+cp_ToStrODBC(this.w_SLCODMAG);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                MGFLUBIC;
                from (i_cTable) where;
                    MGCODMAG = this.w_SLCODMAG;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_MGUBIC = NVL(cp_ToDate(_read_.MGFLUBIC),cp_NullValue(_read_.MGFLUBIC))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            SELECT AppUbilot
            * --- Calcolo del dato procedurale
            REPLACE GESMAT WITH this.w_GESMAT
            REPLACE FLLOTT WITH this.w_FLLOTT
            REPLACE DESART WITH this.w_DESART
            REPLACE MGUBIC WITH this.w_MGUBIC
          else
            SELECT AppUbilot
            * --- Calcolo del dato procedurale
            this.w_GESMAT = GESMAT
            this.w_FLLOTT = FLLOTT
            this.w_MGUBIC = IIF( NVL(MGUBIC , SPACE(1))<>"S" , SPACE(1) , "S")
          endif
          * --- ----------  Trovare un espressione unica che sostituisca il costrutto IF .. else
          if UPPER(g_APPLICATION)="AD HOC ENTERPRISE"
            if this.w_VQRGESMAT<>"S" OR this.w_GESMAT<>"S"
              if (this.w_MGUBIC = "S" or this.w_FLLOTT = "S") And empty(nvl(MLCODLOT," ")) and empty(nvl(MLCODUBI," "))
                REPLACE Procedur WITH 0
              else
                REPLACE Procedur WITH (EsisAttu - EsisPrin - EsisColl) 
              endif
            else
              if empty(nvl(MTCODMAT," "))
                REPLACE Procedur WITH 0
              else
                REPLACE Procedur WITH EsisPrin
              endif
            endif
          else
            if this.w_VQRGESMAT<>"S" OR this.w_GESMAT<>"S"
              if this.w_MGUBIC = "S" or (this.w_FLLOTT = "S" And g_APPLICATION <> "ADHOC REVOLUTION") Or (this.w_FLLOTT $ "S-C" And g_APPLICATION = "ADHOC REVOLUTION") 
                if empty(nvl(MLCODLOT," ")) and empty(nvl(MLCODUBI," "))
                  REPLACE Procedur WITH 0
                else
                  REPLACE Procedur WITH (EsisPrin + EsisColl) 
                endif
              else
                REPLACE Procedur WITH (EsisAttu - EsisPrin - EsisColl) 
              endif
            else
              if empty(nvl(MTCODMAT," "))
                REPLACE Procedur WITH 0
              else
                REPLACE Procedur WITH EsisPrin
              endif
            endif
          endif
          this.w_ESCLOBSO = "E"
          if PROCEDUR = 0 AND this.w_ESCLOBSO = "E" AND CP_TODATE( this.w_ARDTOBSO ) <= this.oParentObject.w_DATARIL AND !EMPTY( this.w_ARDTOBSO )
            * --- Sbianca gli articoli da eliminare.
            *     Saranno eliminati con una delete fuori dal ciclo SCAN
            REPLACE CACODART WITH SPACE( 20 )
          endif
          if empty(nvl(MLCODUBI," ")) AND this.w_MGUBIC = "S" 
            * --- Sbianca gli articoli da eliminare.
            *     Saranno eliminati con una delete fuori dal ciclo SCAN
            REPLACE CACODART WITH SPACE( 20 )
          endif
          ENDSCAN
          DELETE FROM AppUbilot WHERE EMPTY( CACODART )
          * --- Elimino i dati presenti nelle rilevazioni
          DELETE FROM "Appubilot" where LEFT(ALLTRIM(SLCODICE)+ALLTRIM(NVL(MLCODUBI, " "))+ALLTRIM(NVL(MLCODLOT, " "))+ALLTRIM(NVL(MTCODMAT," "))+ALLTRIM(CODCOM)+SPACE(135),135) ; 
 in (select LEFT(ALLTRIM(DRKEYSAL)+ALLTRIM(NVL(DRUBICAZ, " "))+ALLTRIM(NVL(DR_LOTTO, " "))+ALLTRIM(NVL(DRCODMAT, " "))+ALLTRIM(NVL(DRCODCOM," "))+SPACE(135),135) FROM "Totcur")
          * --- Unisco il cursore d'appoggio al cursore definitivo segnando le righe come non rilevate
          SELECT drcodart,drcodvar,desart,drubicaz,dr_lotto,drcodcom,drkeyulo,drcodmat,drunimis,drqtaes1,drkeysal,drcodric, ; 
 EsisAttu,EsisPrin,EsisColl,Procedur,GlProced, FLNONRIL FROM "Totcur" UNION ALL ; 
 SELECT CACODART AS DRCODART, CACODVAR AS DRCODVAR, DESART, MLCODUBI AS DRUBICAZ, MLCODLOT AS DR_LOTTO, CODCOM, ; 
 LEFT(ALLTRIM(MLCODUBI)+SPACE(20),20)+LEFT( IIF(FLLOTT="S", ALLTRIM(MLCODLOT)+SPACE(20) , REPL("#",20)) ,20) AS DRKEYULO, ; 
 MTCODMAT AS DRCODMAT, ARUNMIS1 AS DRUNIMIS, 0 AS DRQTAES1, SLCODICE AS DRKEYSAL, CACODART AS DRCODRIC, ESISATTU, ; 
 ESISPRIN, ESISCOLL, PROCEDUR, 0 AS GLPROCED, "S" AS FLNONRIL FROM "Appubilot" WHERE PROCEDUR<>0 INTO CURSOR "Totcur"
        endif
        SELECT TotCur
        =WRCURSOR("TotCur")
        GO TOP
        SCAN
        * --- Sostituisco l'unit� di misura selezionata con la prima unita di misura associata all'articolo
        this.w_DRARTIC = DRCODART
        this.w_DRVARIAN = ""
        this.w_FLLOTT = " "
        * --- Select from gsma_bdr
        do vq_exec with 'gsma_bdr',this,'_Curs_gsma_bdr','',.f.,.t.
        if used('_Curs_gsma_bdr')
          select _Curs_gsma_bdr
          locate for 1=1
          do while not(eof())
          this.w_UNIMIS = ALLTRIM(NVL(_Curs_gsma_bdr.ARUNMIS1, " "))
          this.w_FLLOTT = NVL(_Curs_gsma_bdr.ARFLLOTT, " ")
          this.w_GLOB_QTA = NVL(_Curs_gsma_bdr.GLPROCED, 0 )
            select _Curs_gsma_bdr
            continue
          enddo
          use
        endif
        SELECT TotCur
        REPLACE DRUNIMIS WITH this.w_UNIMIS 
        REPLACE DRUNIMIS WITH this.w_UNIMIS, GLPROCED WITH this.w_GLOB_QTA, Procedur WITH IIF(Empty(DRCODMAT), (EsisAttu - EsisPrin - EsisColl), EsisPrin )
        * --- Calcolo del dato procedurale
        if !Empty(DRCODMAT)
          REPLACE Procedur WITH EsisPrin
        else
          if this.oParentObject.w_UBICAZ = "S" or (this.w_FLLOTT = "S" And g_APPLICATION <> "ADHOC REVOLUTION") Or (this.w_FLLOTT $ "S-C" And g_APPLICATION = "ADHOC REVOLUTION") 
            REPLACE Procedur WITH (EsisPrin + EsisColl) 
          else
            REPLACE Procedur WITH (EsisAttu - EsisPrin - EsisColl) 
          endif
        endif
        ENDSCAN
        * --- Aggioro il cursore dei totali per i record non movimentati, la giacenza a null non fa apparire lo zero nella colonna dello zoom
        UPDATE "TOTCUR" SET PROCEDUR=0 WHERE PROCEDUR is null
        * --- Raggruppo per ARTICOLO+VARIANTE+LOTTO+UBICAZIONE+ MATRICOLA (RIGA DELLO ZOOM)
        SELECT DRCODART, DRCODVAR,DR_LOTTO,DRUBICAZ,DRCODMAT,DRUNIMIS,MAX(PROCEDUR) AS PROCEDUR,; 
 SUM(DRQTAES1) AS DRQTAES1,DRKEYSAL,DRCODRIC,DESART,DRKEYULO, MAX(GLPROCED) AS GLPROCED, ; 
 MAX(FLNONRIL) AS FLNONRIL,DRCODCOM FROM TOTCUR INTO CURSOR __TMP__; 
 ORDER BY DRKEYSAL,DRKEYULO,DRCODMAT GROUP BY DRKEYSAL,DRKEYULO,DRCODMAT,DRCODCOM
        * --- Inserisco il risultato del cursore Visu nello zoom
        Select __TMP__
        Go Top
        Scan
        Scatter Memvar
        m.ARCODART = m.DRCODART
        Select (this.w_ZOOM.cCursor)
        Append blank
        Gather memvar
        Endscan
        SELECT __TMP__
        USE
        SELECT ( this.w_ZOOM.cCursor )
        GO TOP
        * --- Refresh della griglia
        this.w_zoom.grd.refresh
        * --- Drop temporary table TMPVEND3
        i_nIdx=cp_GetTableDefIdx('TMPVEND3')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPVEND3')
        endif
    endcase
    USE IN SELECT("CONFR")
    USE IN SELECT("UBILOT")
    USE IN SELECT("UBILOT1")
    USE IN SELECT("UBILOT2")
    USE IN SELECT("TOTCUR")
    USE IN SELECT("Carico")
    USE IN SELECT("Scarico")
    USE IN SELECT("DettCarico")
    USE IN SELECT("DettScarico")
    USE IN SELECT("Matr")
    USE IN SELECT("AppMM")
    USE IN SELECT("AppUbilot")
  endproc
  proc Try_04038328()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    ah_ErrorMsg("Generazione terminata","!")
    * --- Disattivo tutti i check
    UPDATE (MM) SET XCHK=0, PROCEDUR=DRQTAES1 WHERE XCHK=1
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrive Movimenti di Magazzino
    this.w_MMCODUTE = IIF(g_MAGUTE="S", 0, i_CODUTE)
    this.w_MMDATREG = this.w_DATREG
    this.w_MMCODESE = this.oParentObject.w_CODESE
    this.w_MMVALNAZ = this.oParentObject.w_ESVALNAZ
    this.w_MMDESSUP = ah_msgformat("Movimento automatico di rettifica")
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VADECTOT"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_MMVALNAZ);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VADECTOT;
        from (i_cTable) where;
            VACODVAL = this.w_MMVALNAZ;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_MMTCOMAG = this.oParentObject.w_CODMAG
    this.w_MMDATDOC = this.w_DATREG
    this.w_MMCODVAL = this.oParentObject.w_ESVALNAZ
    this.w_MMCAOVAL = GETCAM(this.w_MMCODVAL, IIF(EMPTY(this.w_MMDATDOC), this.w_MMDATREG, this.w_MMDATDOC), 7)
    if this.w_CAR
      this.w_REGCARICO = "S"
      SELECT Carico
      GO TOP
      SCAN
      if this.w_CPROWNUM=0
        this.w_MMTCAMAG = this.oParentObject.w_CAUCAR
        this.w_MMSERIAL = cp_GetProg("MVM_MAST","SEMVM",this.w_MMSERIAL,i_CODAZI)
        this.w_MMNUMREG = cp_GetProg("MVM_MAST","PRMVM",this.w_MMNUMREG,i_CODAZI,this.w_MMCODESE,this.w_MMCODUTE)
        if g_APPLICATION <> "ADHOC REVOLUTION"
          * --- Insert into MVM_MAST
          i_nConn=i_TableProp[this.MVM_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MVM_MAST_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MVM_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"MMCODESE"+",MMSERIAL"+",MMVALNAZ"+",MMCODUTE"+",MMNUMREG"+",MMDATREG"+",MMTCAMAG"+",MMTCOMAG"+",MMFLCLFR"+",MMTIPCON"+",MMCODCON"+",MMDESSUP"+",MMCODVAL"+",MMCAOVAL"+",UTCC"+",UTCV"+",UTDC"+",UTDV"+",MMALFDOC"+",MMFLGIOM"+",MMSERDDT"+",MMFLBLOC"+",MMFLRETT"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_MMCODESE),'MVM_MAST','MMCODESE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MMSERIAL),'MVM_MAST','MMSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MMVALNAZ),'MVM_MAST','MMVALNAZ');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODUTE),'MVM_MAST','MMCODUTE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MMNUMREG),'MVM_MAST','MMNUMREG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MMDATREG),'MVM_MAST','MMDATREG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MMTCAMAG),'MVM_MAST','MMTCAMAG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MMTCOMAG),'MVM_MAST','MMTCOMAG');
            +","+cp_NullLink(cp_ToStrODBC("N"),'MVM_MAST','MMFLCLFR');
            +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_MAST','MMTIPCON');
            +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_MAST','MMCODCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MMDESSUP),'MVM_MAST','MMDESSUP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODVAL),'MVM_MAST','MMCODVAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MMCAOVAL),'MVM_MAST','MMCAOVAL');
            +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'MVM_MAST','UTCC');
            +","+cp_NullLink(cp_ToStrODBC(0),'MVM_MAST','UTCV');
            +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'MVM_MAST','UTDC');
            +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'MVM_MAST','UTDV');
            +","+cp_NullLink(cp_ToStrODBC("  "),'MVM_MAST','MMALFDOC');
            +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_MAST','MMFLGIOM');
            +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_MAST','MMSERDDT');
            +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_MAST','MMFLBLOC');
            +","+cp_NullLink(cp_ToStrODBC("S"),'MVM_MAST','MMFLRETT');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'MMCODESE',this.w_MMCODESE,'MMSERIAL',this.w_MMSERIAL,'MMVALNAZ',this.w_MMVALNAZ,'MMCODUTE',this.w_MMCODUTE,'MMNUMREG',this.w_MMNUMREG,'MMDATREG',this.w_MMDATREG,'MMTCAMAG',this.w_MMTCAMAG,'MMTCOMAG',this.w_MMTCOMAG,'MMFLCLFR',"N",'MMTIPCON'," ",'MMCODCON'," ",'MMDESSUP',this.w_MMDESSUP)
            insert into (i_cTable) (MMCODESE,MMSERIAL,MMVALNAZ,MMCODUTE,MMNUMREG,MMDATREG,MMTCAMAG,MMTCOMAG,MMFLCLFR,MMTIPCON,MMCODCON,MMDESSUP,MMCODVAL,MMCAOVAL,UTCC,UTCV,UTDC,UTDV,MMALFDOC,MMFLGIOM,MMSERDDT,MMFLBLOC,MMFLRETT &i_ccchkf. );
               values (;
                 this.w_MMCODESE;
                 ,this.w_MMSERIAL;
                 ,this.w_MMVALNAZ;
                 ,this.w_MMCODUTE;
                 ,this.w_MMNUMREG;
                 ,this.w_MMDATREG;
                 ,this.w_MMTCAMAG;
                 ,this.w_MMTCOMAG;
                 ,"N";
                 ," ";
                 ," ";
                 ,this.w_MMDESSUP;
                 ,this.w_MMCODVAL;
                 ,this.w_MMCAOVAL;
                 ,i_CODUTE;
                 ,0;
                 ,SetInfoDate( g_CALUTD );
                 ,cp_CharToDate("  -  -    ");
                 ,"  ";
                 ," ";
                 ," ";
                 ," ";
                 ,"S";
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        else
          * --- Insert into MVM_MAST
          i_nConn=i_TableProp[this.MVM_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MVM_MAST_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MVM_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"MMCODESE"+",MMSERIAL"+",MMVALNAZ"+",MMCODUTE"+",MMNUMREG"+",MMDATREG"+",MMTCAMAG"+",MMFLCLFR"+",MMTIPCON"+",MMCODCON"+",MMDESSUP"+",MMCODVAL"+",MMCAOVAL"+",UTCC"+",UTCV"+",UTDC"+",UTDV"+",MMALFDOC"+",MMFLGIOM"+",MMFLRETT"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_MMCODESE),'MVM_MAST','MMCODESE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MMSERIAL),'MVM_MAST','MMSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MMVALNAZ),'MVM_MAST','MMVALNAZ');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODUTE),'MVM_MAST','MMCODUTE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MMNUMREG),'MVM_MAST','MMNUMREG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MMDATREG),'MVM_MAST','MMDATREG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MMTCAMAG),'MVM_MAST','MMTCAMAG');
            +","+cp_NullLink(cp_ToStrODBC("N"),'MVM_MAST','MMFLCLFR');
            +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_MAST','MMTIPCON');
            +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_MAST','MMCODCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MMDESSUP),'MVM_MAST','MMDESSUP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODVAL),'MVM_MAST','MMCODVAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MMCAOVAL),'MVM_MAST','MMCAOVAL');
            +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'MVM_MAST','UTCC');
            +","+cp_NullLink(cp_ToStrODBC(0),'MVM_MAST','UTCV');
            +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'MVM_MAST','UTDC');
            +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'MVM_MAST','UTDV');
            +","+cp_NullLink(cp_ToStrODBC("  "),'MVM_MAST','MMALFDOC');
            +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_MAST','MMFLGIOM');
            +","+cp_NullLink(cp_ToStrODBC("S"),'MVM_MAST','MMFLRETT');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'MMCODESE',this.w_MMCODESE,'MMSERIAL',this.w_MMSERIAL,'MMVALNAZ',this.w_MMVALNAZ,'MMCODUTE',this.w_MMCODUTE,'MMNUMREG',this.w_MMNUMREG,'MMDATREG',this.w_MMDATREG,'MMTCAMAG',this.w_MMTCAMAG,'MMFLCLFR',"N",'MMTIPCON'," ",'MMCODCON'," ",'MMDESSUP',this.w_MMDESSUP,'MMCODVAL',this.w_MMCODVAL)
            insert into (i_cTable) (MMCODESE,MMSERIAL,MMVALNAZ,MMCODUTE,MMNUMREG,MMDATREG,MMTCAMAG,MMFLCLFR,MMTIPCON,MMCODCON,MMDESSUP,MMCODVAL,MMCAOVAL,UTCC,UTCV,UTDC,UTDV,MMALFDOC,MMFLGIOM,MMFLRETT &i_ccchkf. );
               values (;
                 this.w_MMCODESE;
                 ,this.w_MMSERIAL;
                 ,this.w_MMVALNAZ;
                 ,this.w_MMCODUTE;
                 ,this.w_MMNUMREG;
                 ,this.w_MMDATREG;
                 ,this.w_MMTCAMAG;
                 ,"N";
                 ," ";
                 ," ";
                 ,this.w_MMDESSUP;
                 ,this.w_MMCODVAL;
                 ,this.w_MMCAOVAL;
                 ,i_CODUTE;
                 ,0;
                 ,SetInfoDate( g_CALUTD );
                 ,cp_CharToDate("  -  -    ");
                 ,"  ";
                 ," ";
                 ,"S";
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
      endif
      * --- Scrivo le righe di Dettaglio
      this.w_MMNUMRIF = -10
      SELECT Carico
      this.w_DRCODART = DRCODART
      this.w_DRCODVAR = DRCODVAR
      this.w_DRKEYSAL = DRKEYSAL
      this.w_DRCODRIC = DRCODRIC
      this.w_DRUBICAZ_R = NVL(DRUBICAZ,SPACE(20))
      this.w_DR_LOTTO_R = NVL(DR_LOTTO,SPACE(20))
      ah_Msg("Elabora articolo: %1",.T.,.F.,.F., ALLTRIM(this.w_DRCODRIC) )
      this.w_QTAAGG = DRQTAES1 - PROCEDUR
      * --- Memorizza l'esistenza rilevata, in quanto occorre aggiornare lo stato del lotto (disponibile/esaurito)
      this.w_DRQTAES1 = DRQTAES1
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      SELECT Carico
      ENDSCAN
      * --- Ripristino tutte le variabili necessarie per poter gestire i movimentoi di scarico
      this.w_CAR = .f.
      this.w_CPROWNUM = 0
      this.w_CPROWORD = 0
    endif
    if this.w_SCAR
      this.w_REGCARICO = "N"
      SELECT Scarico
      GO TOP
      SCAN
      if this.w_CPROWNUM=0
        this.w_MMTCAMAG = this.oParentObject.w_CAUSCA
        this.w_MMSERIAL = cp_GetProg("MVM_MAST","SEMVM",this.w_MMSERIAL,i_CODAZI)
        this.w_MMNUMREG = cp_GetProg("MVM_MAST","PRMVM",this.w_MMNUMREG,i_CODAZI,this.w_MMCODESE,this.w_MMCODUTE)
        if g_APPLICATION <> "ADHOC REVOLUTION"
          * --- Insert into MVM_MAST
          i_nConn=i_TableProp[this.MVM_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MVM_MAST_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MVM_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"MMSERIAL"+",MMCODESE"+",MMVALNAZ"+",MMCODUTE"+",MMNUMREG"+",MMDATREG"+",MMTCAMAG"+",MMTCOMAG"+",MMFLCLFR"+",MMTIPCON"+",MMCODCON"+",MMDESSUP"+",MMCODVAL"+",MMCAOVAL"+",UTCC"+",UTCV"+",UTDC"+",UTDV"+",MMALFDOC"+",MMFLGIOM"+",MMSERDDT"+",MMFLBLOC"+",MMFLRETT"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_MMSERIAL),'MVM_MAST','MMSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODESE),'MVM_MAST','MMCODESE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MMVALNAZ),'MVM_MAST','MMVALNAZ');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODUTE),'MVM_MAST','MMCODUTE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MMNUMREG),'MVM_MAST','MMNUMREG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MMDATREG),'MVM_MAST','MMDATREG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MMTCAMAG),'MVM_MAST','MMTCAMAG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MMTCOMAG),'MVM_MAST','MMTCOMAG');
            +","+cp_NullLink(cp_ToStrODBC("N"),'MVM_MAST','MMFLCLFR');
            +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_MAST','MMTIPCON');
            +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_MAST','MMCODCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MMDESSUP),'MVM_MAST','MMDESSUP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODVAL),'MVM_MAST','MMCODVAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MMCAOVAL),'MVM_MAST','MMCAOVAL');
            +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'MVM_MAST','UTCC');
            +","+cp_NullLink(cp_ToStrODBC(0),'MVM_MAST','UTCV');
            +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'MVM_MAST','UTDC');
            +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'MVM_MAST','UTDV');
            +","+cp_NullLink(cp_ToStrODBC("  "),'MVM_MAST','MMALFDOC');
            +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_MAST','MMFLGIOM');
            +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_MAST','MMSERDDT');
            +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_MAST','MMFLBLOC');
            +","+cp_NullLink(cp_ToStrODBC("S"),'MVM_MAST','MMFLRETT');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'MMSERIAL',this.w_MMSERIAL,'MMCODESE',this.w_MMCODESE,'MMVALNAZ',this.w_MMVALNAZ,'MMCODUTE',this.w_MMCODUTE,'MMNUMREG',this.w_MMNUMREG,'MMDATREG',this.w_MMDATREG,'MMTCAMAG',this.w_MMTCAMAG,'MMTCOMAG',this.w_MMTCOMAG,'MMFLCLFR',"N",'MMTIPCON'," ",'MMCODCON'," ",'MMDESSUP',this.w_MMDESSUP)
            insert into (i_cTable) (MMSERIAL,MMCODESE,MMVALNAZ,MMCODUTE,MMNUMREG,MMDATREG,MMTCAMAG,MMTCOMAG,MMFLCLFR,MMTIPCON,MMCODCON,MMDESSUP,MMCODVAL,MMCAOVAL,UTCC,UTCV,UTDC,UTDV,MMALFDOC,MMFLGIOM,MMSERDDT,MMFLBLOC,MMFLRETT &i_ccchkf. );
               values (;
                 this.w_MMSERIAL;
                 ,this.w_MMCODESE;
                 ,this.w_MMVALNAZ;
                 ,this.w_MMCODUTE;
                 ,this.w_MMNUMREG;
                 ,this.w_MMDATREG;
                 ,this.w_MMTCAMAG;
                 ,this.w_MMTCOMAG;
                 ,"N";
                 ," ";
                 ," ";
                 ,this.w_MMDESSUP;
                 ,this.w_MMCODVAL;
                 ,this.w_MMCAOVAL;
                 ,i_CODUTE;
                 ,0;
                 ,SetInfoDate( g_CALUTD );
                 ,cp_CharToDate("  -  -    ");
                 ,"  ";
                 ," ";
                 ," ";
                 ," ";
                 ,"S";
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        else
          * --- Insert into MVM_MAST
          i_nConn=i_TableProp[this.MVM_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MVM_MAST_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MVM_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"MMSERIAL"+",MMCODESE"+",MMVALNAZ"+",MMCODUTE"+",MMNUMREG"+",MMDATREG"+",MMTCAMAG"+",MMFLCLFR"+",MMTIPCON"+",MMCODCON"+",MMDESSUP"+",MMCODVAL"+",MMCAOVAL"+",UTCC"+",UTCV"+",UTDC"+",UTDV"+",MMALFDOC"+",MMFLGIOM"+",MMFLRETT"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_MMSERIAL),'MVM_MAST','MMSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODESE),'MVM_MAST','MMCODESE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MMVALNAZ),'MVM_MAST','MMVALNAZ');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODUTE),'MVM_MAST','MMCODUTE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MMNUMREG),'MVM_MAST','MMNUMREG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MMDATREG),'MVM_MAST','MMDATREG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MMTCAMAG),'MVM_MAST','MMTCAMAG');
            +","+cp_NullLink(cp_ToStrODBC("N"),'MVM_MAST','MMFLCLFR');
            +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_MAST','MMTIPCON');
            +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_MAST','MMCODCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MMDESSUP),'MVM_MAST','MMDESSUP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODVAL),'MVM_MAST','MMCODVAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MMCAOVAL),'MVM_MAST','MMCAOVAL');
            +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'MVM_MAST','UTCC');
            +","+cp_NullLink(cp_ToStrODBC(0),'MVM_MAST','UTCV');
            +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'MVM_MAST','UTDC');
            +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'MVM_MAST','UTDV');
            +","+cp_NullLink(cp_ToStrODBC("  "),'MVM_MAST','MMALFDOC');
            +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_MAST','MMFLGIOM');
            +","+cp_NullLink(cp_ToStrODBC("S"),'MVM_MAST','MMFLRETT');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'MMSERIAL',this.w_MMSERIAL,'MMCODESE',this.w_MMCODESE,'MMVALNAZ',this.w_MMVALNAZ,'MMCODUTE',this.w_MMCODUTE,'MMNUMREG',this.w_MMNUMREG,'MMDATREG',this.w_MMDATREG,'MMTCAMAG',this.w_MMTCAMAG,'MMFLCLFR',"N",'MMTIPCON'," ",'MMCODCON'," ",'MMDESSUP',this.w_MMDESSUP,'MMCODVAL',this.w_MMCODVAL)
            insert into (i_cTable) (MMSERIAL,MMCODESE,MMVALNAZ,MMCODUTE,MMNUMREG,MMDATREG,MMTCAMAG,MMFLCLFR,MMTIPCON,MMCODCON,MMDESSUP,MMCODVAL,MMCAOVAL,UTCC,UTCV,UTDC,UTDV,MMALFDOC,MMFLGIOM,MMFLRETT &i_ccchkf. );
               values (;
                 this.w_MMSERIAL;
                 ,this.w_MMCODESE;
                 ,this.w_MMVALNAZ;
                 ,this.w_MMCODUTE;
                 ,this.w_MMNUMREG;
                 ,this.w_MMDATREG;
                 ,this.w_MMTCAMAG;
                 ,"N";
                 ," ";
                 ," ";
                 ,this.w_MMDESSUP;
                 ,this.w_MMCODVAL;
                 ,this.w_MMCAOVAL;
                 ,i_CODUTE;
                 ,0;
                 ,SetInfoDate( g_CALUTD );
                 ,cp_CharToDate("  -  -    ");
                 ,"  ";
                 ," ";
                 ,"S";
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
      endif
      * --- Scrivo le righe di Dettaglio
      this.w_MMNUMRIF = -10
      SELECT Scarico
      this.w_DRCODART = DRCODART
      this.w_DRCODVAR = DRCODVAR
      this.w_DRKEYSAL = DRKEYSAL
      this.w_DRCODRIC = DRCODRIC
      this.w_DRUBICAZ_R = NVL(DRUBICAZ,SPACE(20))
      this.w_DR_LOTTO_R = NVL(DR_LOTTO,SPACE(20))
      ah_Msg("Elabora articolo: %1",.T.,.F.,.F., ALLTRIM(this.w_DRCODRIC) )
      this.w_QTAAGG = PROCEDUR - DRQTAES1
      * --- Memorizza l'esistenza rilevata, in quanto occorre aggiornare lo stato del lotto (disponibile/esaurito)
      this.w_DRQTAES1 = DRQTAES1
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      SELECT Scarico
      ENDSCAN
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserimento Record di Dettaglio
    this.w_CPROWNUM = this.w_CPROWNUM + 1
    this.w_CPROWORD = this.w_CPROWORD + 10
    this.w_MMCAUCOL = " "
    this.w_MMFLELAN = " "
    this.w_MMFLELGM = " "
    this.w_MMFLCASC = " "
    this.w_MMFLORDI = " "
    this.w_MMFLIMPE = " "
    this.w_MMFLRISE = " "
    this.w_MM_SEGNO = " "
    this.w_TIPVOC = " "
    this.w_MMFLCLFR = "N"
    if this.w_CAR
      this.w_MMCAUMAG = this.oParentObject.w_CAUCAR
    else
      this.w_MMCAUMAG = this.oParentObject.w_CAUSCA
    endif
    if g_APPLICATION <> "ADHOC REVOLUTION"
      * --- Read from CAM_AGAZ
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLELGM,CMFLELAN,CMCAUCOL,CMTIPVOC,CMFLANAL,CMFLCLFR,CMCDCAUT"+;
          " from "+i_cTable+" CAM_AGAZ where ";
              +"CMCODICE = "+cp_ToStrODBC(this.w_MMCAUMAG);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLELGM,CMFLELAN,CMCAUCOL,CMTIPVOC,CMFLANAL,CMFLCLFR,CMCDCAUT;
          from (i_cTable) where;
              CMCODICE = this.w_MMCAUMAG;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MMFLCASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
        this.w_MMFLORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
        this.w_MMFLIMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
        this.w_MMFLRISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
        this.w_MMFLELGM = NVL(cp_ToDate(_read_.CMFLELGM),cp_NullValue(_read_.CMFLELGM))
        this.w_MMFLELAN = NVL(cp_ToDate(_read_.CMFLELAN),cp_NullValue(_read_.CMFLELAN))
        this.w_MMCAUCOL = NVL(cp_ToDate(_read_.CMCAUCOL),cp_NullValue(_read_.CMCAUCOL))
        this.w_TIPVOC = NVL(cp_ToDate(_read_.CMTIPVOC),cp_NullValue(_read_.CMTIPVOC))
        this.w_MM_SEGNO = NVL(cp_ToDate(_read_.CMFLANAL),cp_NullValue(_read_.CMFLANAL))
        this.w_MMFLCLFR = NVL(cp_ToDate(_read_.CMFLCLFR),cp_NullValue(_read_.CMFLCLFR))
        this.w_CDCAUT = NVL(cp_ToDate(_read_.CMCDCAUT),cp_NullValue(_read_.CMCDCAUT))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      * --- Read from CAM_AGAZ
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLELGM,CMCAUCOL,CMFLCLFR"+;
          " from "+i_cTable+" CAM_AGAZ where ";
              +"CMCODICE = "+cp_ToStrODBC(this.w_MMCAUMAG);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLELGM,CMCAUCOL,CMFLCLFR;
          from (i_cTable) where;
              CMCODICE = this.w_MMCAUMAG;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MMFLCASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
        this.w_MMFLORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
        this.w_MMFLIMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
        this.w_MMFLRISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
        this.w_MMFLELGM = NVL(cp_ToDate(_read_.CMFLELGM),cp_NullValue(_read_.CMFLELGM))
        this.w_MMCAUCOL = NVL(cp_ToDate(_read_.CMCAUCOL),cp_NullValue(_read_.CMCAUCOL))
        this.w_MMFLCLFR = NVL(cp_ToDate(_read_.CMFLCLFR),cp_NullValue(_read_.CMFLCLFR))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if .not. empty(this.w_MMCAUCOL)
        * --- Causale collegata
        * --- Read from CAM_AGAZ
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE"+;
            " from "+i_cTable+" CAM_AGAZ where ";
                +"CMCODICE = "+cp_ToStrODBC(this.w_MMCAUCOL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE;
            from (i_cTable) where;
                CMCODICE = this.w_MMCAUCOL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MMF2CASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
          this.w_MMF2ORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
          this.w_MMF2IMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
          this.w_MMF2RISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
    endif
    this.w_MMCAUCOL = " "
    this.w_MMCODMAT = " "
    this.w_MMCODMAG = this.oParentObject.w_CODMAG
    this.w_MMCODICE = this.w_DRCODRIC
    this.w_MMKEYSAL = this.w_DRKEYSAL
    * --- Leggo l'unit� di misura dell'articolo corrente e verifico se � gestito a lotti o Matricole
    this.w_GESMAT = " "
    this.w_FLLOTT = " "
    * --- Read from ART_ICOL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ARFLLOTT,ARUNMIS1,ARGESMAT,ARSALCOM"+;
        " from "+i_cTable+" ART_ICOL where ";
            +"ARCODART = "+cp_ToStrODBC(this.w_DRCODART);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ARFLLOTT,ARUNMIS1,ARGESMAT,ARSALCOM;
        from (i_cTable) where;
            ARCODART = this.w_DRCODART;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_FLLOTT = NVL(cp_ToDate(_read_.ARFLLOTT),cp_NullValue(_read_.ARFLLOTT))
      this.w_UNIMIS = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
      this.w_GESMAT = NVL(cp_ToDate(_read_.ARGESMAT),cp_NullValue(_read_.ARGESMAT))
      this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_MMUNIMIS = this.w_UNIMIS
    this.w_MMQTAMOV = this.w_QTAAGG
    this.w_MMQTAUM1 = this.w_QTAAGG
    this.w_MMTIPATT = "A"
    this.w_MMFLOMAG = "X"
    this.w_MMCODBUN = g_CODBUN
    if (g_PERLOT="S" AND ( (this.w_FLLOTT = "S" And g_APPLICATION <> "ADHOC REVOLUTION") Or (this.w_FLLOTT $ "S-C" And g_APPLICATION = "ADHOC REVOLUTION") ) ) or (g_PERUBI="S" and this.oParentObject.w_UBICAZ="S")
      this.w_MMFLLOTT = this.w_MMFLCASC
    else
      this.w_MMFLLOTT = " "
    endif
    if this.w_CDCAUT="S"
      this.w_MMCODCEN = CALCENCO(this.w_MMCODICE,"","")
    endif
    * --- Se non valorizzata imposto il valore a null per rispettare l'integrit� referenziale
    this.w_MMCODCOM = nvl(DRCODCOM, space(15))
    if g_APPLICATION <> "ADHOC REVOLUTION"
      * --- Insert into MVM_DETT
      i_nConn=i_TableProp[this.MVM_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MVM_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"CPROWNUM"+",CPROWORD"+",MMNUMRIF"+",MMCAUMAG"+",MMCAUCOL"+",MMCODMAG"+",MMCODMAT"+",MMCODICE"+",MMCODART"+",MMSERIAL"+",MMUNIMIS"+",MMQTAMOV"+",MMQTAUM1"+",MMPREZZO"+",MMCODVAR"+",MMSCONT1"+",MMSCONT2"+",MMSCONT3"+",MMSCONT4"+",MMVALMAG"+",MMIMPNAZ"+",MMKEYSAL"+",MMFLELGM"+",MMFLELAN"+",MMFLCASC"+",MMFLORDI"+",MMFLIMPE"+",MMFLRISE"+",MMFLOMAG"+",MMFLLOTT"+",MMCODBUN"+",MMTIPATT"+",MMIMPCOM"+",MMVALULT"+",MMF2CASC"+",MMF2ORDI"+",MMF2IMPE"+",MMF2RISE"+",MM_SEGNO"+",MMFLRIPA"+",MMF2LOTT"+",MMFLORCO"+",MMFLCOCO"+",MMCODODL"+",MMFLULCA"+",MMFLULPV"+",MMCODCEN"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'MVM_DETT','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'MVM_DETT','CPROWORD');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MMNUMRIF),'MVM_DETT','MMNUMRIF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MMCAUMAG),'MVM_DETT','MMCAUMAG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MMCAUCOL),'MVM_DETT','MMCAUCOL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODMAG),'MVM_DETT','MMCODMAG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODMAT),'MVM_DETT','MMCODMAT');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODICE),'MVM_DETT','MMCODICE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DRCODART),'MVM_DETT','MMCODART');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MMSERIAL),'MVM_DETT','MMSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MMUNIMIS),'MVM_DETT','MMUNIMIS');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MMQTAMOV),'MVM_DETT','MMQTAMOV');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MMQTAUM1),'MVM_DETT','MMQTAUM1');
        +","+cp_NullLink(cp_ToStrODBC(0),'MVM_DETT','MMPREZZO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DRCODVAR),'MVM_DETT','MMCODVAR');
        +","+cp_NullLink(cp_ToStrODBC(0),'MVM_DETT','MMSCONT1');
        +","+cp_NullLink(cp_ToStrODBC(0),'MVM_DETT','MMSCONT2');
        +","+cp_NullLink(cp_ToStrODBC(0),'MVM_DETT','MMSCONT3');
        +","+cp_NullLink(cp_ToStrODBC(0),'MVM_DETT','MMSCONT4');
        +","+cp_NullLink(cp_ToStrODBC(0),'MVM_DETT','MMVALMAG');
        +","+cp_NullLink(cp_ToStrODBC(0),'MVM_DETT','MMIMPNAZ');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MMKEYSAL),'MVM_DETT','MMKEYSAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MMFLELGM),'MVM_DETT','MMFLELGM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MMFLELAN),'MVM_DETT','MMFLELAN');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MMFLCASC),'MVM_DETT','MMFLCASC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MMFLORDI),'MVM_DETT','MMFLORDI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MMFLIMPE),'MVM_DETT','MMFLIMPE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MMFLRISE),'MVM_DETT','MMFLRISE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MMFLOMAG),'MVM_DETT','MMFLOMAG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MMFLLOTT),'MVM_DETT','MMFLLOTT');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODBUN),'MVM_DETT','MMCODBUN');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MMTIPATT),'MVM_DETT','MMTIPATT');
        +","+cp_NullLink(cp_ToStrODBC(0),'MVM_DETT','MMIMPCOM');
        +","+cp_NullLink(cp_ToStrODBC(0),'MVM_DETT','MMVALULT');
        +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_DETT','MMF2CASC');
        +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_DETT','MMF2ORDI');
        +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_DETT','MMF2IMPE');
        +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_DETT','MMF2RISE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MM_SEGNO),'MVM_DETT','MM_SEGNO');
        +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_DETT','MMFLRIPA');
        +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_DETT','MMF2LOTT');
        +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_DETT','MMFLORCO');
        +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_DETT','MMFLCOCO');
        +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_DETT','MMCODODL');
        +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_DETT','MMFLULCA');
        +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_DETT','MMFLULPV');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODCEN),'MVM_DETT','MMCODCEN');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'MMNUMRIF',this.w_MMNUMRIF,'MMCAUMAG',this.w_MMCAUMAG,'MMCAUCOL',this.w_MMCAUCOL,'MMCODMAG',this.w_MMCODMAG,'MMCODMAT',this.w_MMCODMAT,'MMCODICE',this.w_MMCODICE,'MMCODART',this.w_DRCODART,'MMSERIAL',this.w_MMSERIAL,'MMUNIMIS',this.w_MMUNIMIS,'MMQTAMOV',this.w_MMQTAMOV)
        insert into (i_cTable) (CPROWNUM,CPROWORD,MMNUMRIF,MMCAUMAG,MMCAUCOL,MMCODMAG,MMCODMAT,MMCODICE,MMCODART,MMSERIAL,MMUNIMIS,MMQTAMOV,MMQTAUM1,MMPREZZO,MMCODVAR,MMSCONT1,MMSCONT2,MMSCONT3,MMSCONT4,MMVALMAG,MMIMPNAZ,MMKEYSAL,MMFLELGM,MMFLELAN,MMFLCASC,MMFLORDI,MMFLIMPE,MMFLRISE,MMFLOMAG,MMFLLOTT,MMCODBUN,MMTIPATT,MMIMPCOM,MMVALULT,MMF2CASC,MMF2ORDI,MMF2IMPE,MMF2RISE,MM_SEGNO,MMFLRIPA,MMF2LOTT,MMFLORCO,MMFLCOCO,MMCODODL,MMFLULCA,MMFLULPV,MMCODCEN &i_ccchkf. );
           values (;
             this.w_CPROWNUM;
             ,this.w_CPROWORD;
             ,this.w_MMNUMRIF;
             ,this.w_MMCAUMAG;
             ,this.w_MMCAUCOL;
             ,this.w_MMCODMAG;
             ,this.w_MMCODMAT;
             ,this.w_MMCODICE;
             ,this.w_DRCODART;
             ,this.w_MMSERIAL;
             ,this.w_MMUNIMIS;
             ,this.w_MMQTAMOV;
             ,this.w_MMQTAUM1;
             ,0;
             ,this.w_DRCODVAR;
             ,0;
             ,0;
             ,0;
             ,0;
             ,0;
             ,0;
             ,this.w_MMKEYSAL;
             ,this.w_MMFLELGM;
             ,this.w_MMFLELAN;
             ,this.w_MMFLCASC;
             ,this.w_MMFLORDI;
             ,this.w_MMFLIMPE;
             ,this.w_MMFLRISE;
             ,this.w_MMFLOMAG;
             ,this.w_MMFLLOTT;
             ,this.w_MMCODBUN;
             ,this.w_MMTIPATT;
             ,0;
             ,0;
             ," ";
             ," ";
             ," ";
             ," ";
             ,this.w_MM_SEGNO;
             ," ";
             ," ";
             ," ";
             ," ";
             ," ";
             ," ";
             ," ";
             ,this.w_MMCODCEN;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    else
      * --- Insert into MVM_DETT
      i_nConn=i_TableProp[this.MVM_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MVM_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"CPROWNUM"+",CPROWORD"+",MMNUMRIF"+",MMCAUMAG"+",MMCAUCOL"+",MMCODMAG"+",MMCODMAT"+",MMCODICE"+",MMCODART"+",MMSERIAL"+",MMUNIMIS"+",MMQTAMOV"+",MMQTAUM1"+",MMPREZZO"+",MMSCONT1"+",MMSCONT2"+",MMSCONT3"+",MMSCONT4"+",MMVALMAG"+",MMIMPNAZ"+",MMKEYSAL"+",MMFLELGM"+",MMFLCASC"+",MMFLORDI"+",MMFLIMPE"+",MMFLRISE"+",MMFLOMAG"+",MMFLLOTT"+",MMTIPATT"+",MMIMPCOM"+",MMVALULT"+",MMF2CASC"+",MMF2ORDI"+",MMF2IMPE"+",MMF2RISE"+",MMF2LOTT"+",MMFLORCO"+",MMFLCOCO"+",MMFLULCA"+",MMFLULPV"+",MMCODCOM"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'MVM_DETT','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'MVM_DETT','CPROWORD');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MMNUMRIF),'MVM_DETT','MMNUMRIF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MMCAUMAG),'MVM_DETT','MMCAUMAG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MMCAUCOL),'MVM_DETT','MMCAUCOL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODMAG),'MVM_DETT','MMCODMAG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODMAT),'MVM_DETT','MMCODMAT');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODICE),'MVM_DETT','MMCODICE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DRCODART),'MVM_DETT','MMCODART');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MMSERIAL),'MVM_DETT','MMSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MMUNIMIS),'MVM_DETT','MMUNIMIS');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MMQTAMOV),'MVM_DETT','MMQTAMOV');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MMQTAUM1),'MVM_DETT','MMQTAUM1');
        +","+cp_NullLink(cp_ToStrODBC(0),'MVM_DETT','MMPREZZO');
        +","+cp_NullLink(cp_ToStrODBC(0),'MVM_DETT','MMSCONT1');
        +","+cp_NullLink(cp_ToStrODBC(0),'MVM_DETT','MMSCONT2');
        +","+cp_NullLink(cp_ToStrODBC(0),'MVM_DETT','MMSCONT3');
        +","+cp_NullLink(cp_ToStrODBC(0),'MVM_DETT','MMSCONT4');
        +","+cp_NullLink(cp_ToStrODBC(0),'MVM_DETT','MMVALMAG');
        +","+cp_NullLink(cp_ToStrODBC(0),'MVM_DETT','MMIMPNAZ');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MMKEYSAL),'MVM_DETT','MMKEYSAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MMFLELGM),'MVM_DETT','MMFLELGM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MMFLCASC),'MVM_DETT','MMFLCASC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MMFLORDI),'MVM_DETT','MMFLORDI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MMFLIMPE),'MVM_DETT','MMFLIMPE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MMFLRISE),'MVM_DETT','MMFLRISE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MMFLOMAG),'MVM_DETT','MMFLOMAG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MMFLLOTT),'MVM_DETT','MMFLLOTT');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MMTIPATT),'MVM_DETT','MMTIPATT');
        +","+cp_NullLink(cp_ToStrODBC(0),'MVM_DETT','MMIMPCOM');
        +","+cp_NullLink(cp_ToStrODBC(0),'MVM_DETT','MMVALULT');
        +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_DETT','MMF2CASC');
        +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_DETT','MMF2ORDI');
        +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_DETT','MMF2IMPE');
        +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_DETT','MMF2RISE');
        +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_DETT','MMF2LOTT');
        +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_DETT','MMFLORCO');
        +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_DETT','MMFLCOCO');
        +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_DETT','MMFLULCA');
        +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_DETT','MMFLULPV');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODCOM),'MVM_DETT','MMCODCOM');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'MMNUMRIF',this.w_MMNUMRIF,'MMCAUMAG',this.w_MMCAUMAG,'MMCAUCOL',this.w_MMCAUCOL,'MMCODMAG',this.w_MMCODMAG,'MMCODMAT',this.w_MMCODMAT,'MMCODICE',this.w_MMCODICE,'MMCODART',this.w_DRCODART,'MMSERIAL',this.w_MMSERIAL,'MMUNIMIS',this.w_MMUNIMIS,'MMQTAMOV',this.w_MMQTAMOV)
        insert into (i_cTable) (CPROWNUM,CPROWORD,MMNUMRIF,MMCAUMAG,MMCAUCOL,MMCODMAG,MMCODMAT,MMCODICE,MMCODART,MMSERIAL,MMUNIMIS,MMQTAMOV,MMQTAUM1,MMPREZZO,MMSCONT1,MMSCONT2,MMSCONT3,MMSCONT4,MMVALMAG,MMIMPNAZ,MMKEYSAL,MMFLELGM,MMFLCASC,MMFLORDI,MMFLIMPE,MMFLRISE,MMFLOMAG,MMFLLOTT,MMTIPATT,MMIMPCOM,MMVALULT,MMF2CASC,MMF2ORDI,MMF2IMPE,MMF2RISE,MMF2LOTT,MMFLORCO,MMFLCOCO,MMFLULCA,MMFLULPV,MMCODCOM &i_ccchkf. );
           values (;
             this.w_CPROWNUM;
             ,this.w_CPROWORD;
             ,this.w_MMNUMRIF;
             ,this.w_MMCAUMAG;
             ,this.w_MMCAUCOL;
             ,this.w_MMCODMAG;
             ,this.w_MMCODMAT;
             ,this.w_MMCODICE;
             ,this.w_DRCODART;
             ,this.w_MMSERIAL;
             ,this.w_MMUNIMIS;
             ,this.w_MMQTAMOV;
             ,this.w_MMQTAUM1;
             ,0;
             ,0;
             ,0;
             ,0;
             ,0;
             ,0;
             ,0;
             ,this.w_MMKEYSAL;
             ,this.w_MMFLELGM;
             ,this.w_MMFLCASC;
             ,this.w_MMFLORDI;
             ,this.w_MMFLIMPE;
             ,this.w_MMFLRISE;
             ,this.w_MMFLOMAG;
             ,this.w_MMFLLOTT;
             ,this.w_MMTIPATT;
             ,0;
             ,0;
             ," ";
             ," ";
             ," ";
             ," ";
             ," ";
             ," ";
             ," ";
             ," ";
             ," ";
             ,this.w_MMCODCOM;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      if empty (nvl(this.w_MMCODCOM," ")) 
        this.w_MMCODCOM = this.w_COMMDEFA
      endif
    endif
    * --- Assegno il valore 'S' al campo DRCONFER ed assegno il seriale del movimento al campo DRSERMOV dei DATI RILEVATI
    Select AppMM
    go top
    SCAN FOR DRKEYSAL = this.w_MMKEYSAL
    if (this.w_REGCARICO="S" AND nvl(PROCEDUR,0) < nvl(DRQTAES1,0) ) OR (this.w_REGCARICO="N" AND nvl(PROCEDUR,0) > nvl(DRQTAES1,0) )
      this.w_AGGKEYULO = DRKEYULO
      this.w_AGGMATRI = DRCODMAT
      this.w_AGGCOMM = DRCODCOM
      if !Empty(this.w_AGGCOMM) and this.w_SALCOM="S"
        if EMPTY(this.w_AGGMATRI)
          * --- Write into RILEVAZI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.RILEVAZI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.RILEVAZI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.RILEVAZI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DRCONFER ="+cp_NullLink(cp_ToStrODBC("S"),'RILEVAZI','DRCONFER');
            +",DRSERMOV ="+cp_NullLink(cp_ToStrODBC(this.w_MMSERIAL),'RILEVAZI','DRSERMOV');
                +i_ccchkf ;
            +" where ";
                +"DRCODRIL = "+cp_ToStrODBC(this.oParentObject.w_CODRIL);
                +" and DRCODMAG = "+cp_ToStrODBC(this.oParentObject.w_CODMAG);
                +" and DRKEYSAL = "+cp_ToStrODBC(this.w_MMKEYSAL);
                +" and DRKEYULO = "+cp_ToStrODBC(this.w_AGGKEYULO);
                +" and DRCODCOM = "+cp_ToStrODBC(this.w_AGGCOMM);
                +" and DRCONFER <> "+cp_ToStrODBC("S");
                   )
          else
            update (i_cTable) set;
                DRCONFER = "S";
                ,DRSERMOV = this.w_MMSERIAL;
                &i_ccchkf. ;
             where;
                DRCODRIL = this.oParentObject.w_CODRIL;
                and DRCODMAG = this.oParentObject.w_CODMAG;
                and DRKEYSAL = this.w_MMKEYSAL;
                and DRKEYULO = this.w_AGGKEYULO;
                and DRCODCOM = this.w_AGGCOMM;
                and DRCONFER <> "S";

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        else
          * --- Write into RILEVAZI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.RILEVAZI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.RILEVAZI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.RILEVAZI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DRCONFER ="+cp_NullLink(cp_ToStrODBC("S"),'RILEVAZI','DRCONFER');
            +",DRSERMOV ="+cp_NullLink(cp_ToStrODBC(this.w_MMSERIAL),'RILEVAZI','DRSERMOV');
                +i_ccchkf ;
            +" where ";
                +"DRCODRIL = "+cp_ToStrODBC(this.oParentObject.w_CODRIL);
                +" and DRCODMAG = "+cp_ToStrODBC(this.oParentObject.w_CODMAG);
                +" and DRKEYSAL = "+cp_ToStrODBC(this.w_MMKEYSAL);
                +" and DRKEYULO = "+cp_ToStrODBC(this.w_AGGKEYULO);
                +" and DRCODMAT = "+cp_ToStrODBC(this.w_AGGMATRI);
                +" and DRCODCOM = "+cp_ToStrODBC(this.w_AGGCOMM);
                +" and DRCONFER <> "+cp_ToStrODBC("S");
                   )
          else
            update (i_cTable) set;
                DRCONFER = "S";
                ,DRSERMOV = this.w_MMSERIAL;
                &i_ccchkf. ;
             where;
                DRCODRIL = this.oParentObject.w_CODRIL;
                and DRCODMAG = this.oParentObject.w_CODMAG;
                and DRKEYSAL = this.w_MMKEYSAL;
                and DRKEYULO = this.w_AGGKEYULO;
                and DRCODMAT = this.w_AGGMATRI;
                and DRCODCOM = this.w_AGGCOMM;
                and DRCONFER <> "S";

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      else
        if EMPTY(this.w_AGGMATRI)
          * --- Write into RILEVAZI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.RILEVAZI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.RILEVAZI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.RILEVAZI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DRCONFER ="+cp_NullLink(cp_ToStrODBC("S"),'RILEVAZI','DRCONFER');
            +",DRSERMOV ="+cp_NullLink(cp_ToStrODBC(this.w_MMSERIAL),'RILEVAZI','DRSERMOV');
                +i_ccchkf ;
            +" where ";
                +"DRCODRIL = "+cp_ToStrODBC(this.oParentObject.w_CODRIL);
                +" and DRCODMAG = "+cp_ToStrODBC(this.oParentObject.w_CODMAG);
                +" and DRKEYSAL = "+cp_ToStrODBC(this.w_MMKEYSAL);
                +" and DRKEYULO = "+cp_ToStrODBC(this.w_AGGKEYULO);
                +" and DRCONFER <> "+cp_ToStrODBC("S");
                   )
          else
            update (i_cTable) set;
                DRCONFER = "S";
                ,DRSERMOV = this.w_MMSERIAL;
                &i_ccchkf. ;
             where;
                DRCODRIL = this.oParentObject.w_CODRIL;
                and DRCODMAG = this.oParentObject.w_CODMAG;
                and DRKEYSAL = this.w_MMKEYSAL;
                and DRKEYULO = this.w_AGGKEYULO;
                and DRCONFER <> "S";

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        else
          * --- Write into RILEVAZI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.RILEVAZI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.RILEVAZI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.RILEVAZI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DRCONFER ="+cp_NullLink(cp_ToStrODBC("S"),'RILEVAZI','DRCONFER');
            +",DRSERMOV ="+cp_NullLink(cp_ToStrODBC(this.w_MMSERIAL),'RILEVAZI','DRSERMOV');
                +i_ccchkf ;
            +" where ";
                +"DRCODRIL = "+cp_ToStrODBC(this.oParentObject.w_CODRIL);
                +" and DRCODMAG = "+cp_ToStrODBC(this.oParentObject.w_CODMAG);
                +" and DRKEYSAL = "+cp_ToStrODBC(this.w_MMKEYSAL);
                +" and DRKEYULO = "+cp_ToStrODBC(this.w_AGGKEYULO);
                +" and DRCODMAT = "+cp_ToStrODBC(this.w_AGGMATRI);
                +" and DRCONFER <> "+cp_ToStrODBC("S");
                   )
          else
            update (i_cTable) set;
                DRCONFER = "S";
                ,DRSERMOV = this.w_MMSERIAL;
                &i_ccchkf. ;
             where;
                DRCODRIL = this.oParentObject.w_CODRIL;
                and DRCODMAG = this.oParentObject.w_CODMAG;
                and DRKEYSAL = this.w_MMKEYSAL;
                and DRKEYULO = this.w_AGGKEYULO;
                and DRCODMAT = this.w_AGGMATRI;
                and DRCONFER <> "S";

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
    endif
    Select AppMM
    endscan
    * --- Aggiorno i saldi
    * --- Try
    local bErr_03F9C0D0
    bErr_03F9C0D0=bTrsErr
    this.Try_03F9C0D0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_03F9C0D0
    * --- End
    if g_APPLICATION = "ADHOC REVOLUTION" AND this.w_SALCOM="S"
      * --- Try
      local bErr_03F9FD30
      bErr_03F9FD30=bTrsErr
      this.Try_03F9FD30()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_03F9FD30
      * --- End
    endif
    * --- Write into SALDIART
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_MMFLCASC,'SLQTAPER','this.w_QTAAGG',this.w_QTAAGG,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_MMFLRISE,'SLQTRPER','0',0,'update',i_nConn)
      i_cOp3=cp_SetTrsOp(this.w_MMFLORDI,'SLQTOPER','0',0,'update',i_nConn)
      i_cOp4=cp_SetTrsOp(this.w_MMFLIMPE,'SLQTIPER','0',0,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SLQTAPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTAPER');
      +",SLQTRPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTRPER');
      +",SLQTOPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTOPER');
      +",SLQTIPER ="+cp_NullLink(i_cOp4,'SALDIART','SLQTIPER');
          +i_ccchkf ;
      +" where ";
          +"SLCODICE = "+cp_ToStrODBC(this.w_MMKEYSAL);
          +" and SLCODMAG = "+cp_ToStrODBC(this.oParentObject.w_CODMAG);
             )
    else
      update (i_cTable) set;
          SLQTAPER = &i_cOp1.;
          ,SLQTRPER = &i_cOp2.;
          ,SLQTOPER = &i_cOp3.;
          ,SLQTIPER = &i_cOp4.;
          &i_ccchkf. ;
       where;
          SLCODICE = this.w_MMKEYSAL;
          and SLCODMAG = this.oParentObject.w_CODMAG;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if this.w_SALCOM="S"
      * --- Write into SALDICOM
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDICOM_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
        i_cOp1=cp_SetTrsOp(this.w_MMFLCASC,'SCQTAPER','this.w_QTAAGG',this.w_QTAAGG,'update',i_nConn)
        i_cOp2=cp_SetTrsOp(this.w_MMFLRISE,'SCQTRPER','0',0,'update',i_nConn)
        i_cOp3=cp_SetTrsOp(this.w_MMFLORDI,'SCQTOPER','0',0,'update',i_nConn)
        i_cOp4=cp_SetTrsOp(this.w_MMFLIMPE,'SCQTIPER','0',0,'update',i_nConn)
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
        +",SCQTRPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTRPER');
        +",SCQTOPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTOPER');
        +",SCQTIPER ="+cp_NullLink(i_cOp4,'SALDICOM','SCQTIPER');
            +i_ccchkf ;
        +" where ";
            +"SCCODICE = "+cp_ToStrODBC(this.w_MMKEYSAL);
            +" and SCCODMAG = "+cp_ToStrODBC(this.oParentObject.w_CODMAG);
            +" and SCCODCAN = "+cp_ToStrODBC(this.w_MMCODCOM);
               )
      else
        update (i_cTable) set;
            SCQTAPER = &i_cOp1.;
            ,SCQTRPER = &i_cOp2.;
            ,SCQTOPER = &i_cOp3.;
            ,SCQTIPER = &i_cOp4.;
            &i_ccchkf. ;
         where;
            SCCODICE = this.w_MMKEYSAL;
            and SCCODMAG = this.oParentObject.w_CODMAG;
            and SCCODCAN = this.w_MMCODCOM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Se l'articolo � gestito a lotti o il Magazzino ad Ubicazioni inserisco le quantit� nella tabella Movilott
    *     Se 'Articolo � gestito a Matricole inserisco in MOVIMATR
    if (g_PERLOT="S" AND ( (this.w_FLLOTT = "S" And g_APPLICATION <> "ADHOC REVOLUTION") Or (this.w_FLLOTT $ "S-C" And g_APPLICATION = "ADHOC REVOLUTION") ) ) or (g_PERUBI="S" and this.oParentObject.w_UBICAZ="S") or (g_MATR="S" AND this.w_GESMAT="S")
      this.w_QTAAGGLU = 0
      if this.w_CAR
        SELECT DettCarico
      else
        SELECT DettScarico
      endif
      if g_APPLICATION <> "ADHOC REVOLUTION"
        * --- Ricerca su Articolo e Variante
        L_Rottura = " this.w_DRCODART=DRCODART AND this.w_DRCODVAR=NVL(DRCODVAR, SPACE(20)) "
      else
        * --- Ricerca su Articolo, lotto e Ubicazione
        L_Rottura = " this.w_DRCODART=DRCODART AND this.w_DRUBICAZ_R=NVL(DRUBICAZ, SPACE(20)) AND this.w_DR_LOTTO_R=NVL(DR_LOTTO, SPACE(20))"
        this.w_ROTLOTUBI = SPACE(40)
      endif
      GO TOP
      SCAN FOR &L_Rottura
      if this.w_CAR
        this.w_QTAAGGLU = DRQTAES1 - NVL(PROCEDUR,0)
        this.w_MLFLCASC = "C"
      else
        this.w_QTAAGGLU = NVL(PROCEDUR,0) -DRQTAES1
        this.w_MLFLCASC = "S"
      endif
      this.w_DR_LOTTO = NVL(DR_LOTTO,SPACE(20))
      this.w_DRUBICAZ = NVL(DRUBICAZ,SPACE(20))
      this.w_CODLOT = iif(g_PERLOT="S" and ( (this.w_FLLOTT = "S" And g_APPLICATION <> "ADHOC REVOLUTION") Or (this.w_FLLOTT $ "S-C" And g_APPLICATION = "ADHOC REVOLUTION") ) ,this.w_DR_LOTTO,SPACE(20))
      this.w_CODUBI = iif(g_PERUBI="S" and this.oParentObject.w_UBICAZ="S",this.w_DRUBICAZ,SPACE(20))
      * --- w_CODLOT w_CODUBI se vuoti impostati a Null  per rispettare integrit� referenziale
      if .f.
        this.w_CODLOT = iif(Empty(this.w_CODLOT),NULL,this.w_CODLOT)
        this.w_CODUBI = iif(Empty(this.w_CODUBI),NULL,this.w_CODUBI)
      endif
      * --- Se Modulo Matricole attivo e Articolo gestito a Matricole devo popolare MOVIMATR
      if g_MATR="S" and this.w_GESMAT="S" and this.w_MMDATREG>=Nvl( g_DATMAT, cp_CharToDate( "  /  /    " ) )
        this.w_DRCODMAT = DRCODMAT
        if this.w_CAR
          * --- Movimento di Carico
          this.w_MTMAGCAR = this.oParentObject.w_CODMAG
          this.w_MTFLCARI = "E"
          this.w_MTMAGSCA = SPACE(5)
          this.w_MTFLSCAR = " "
        else
          * --- Movimento di Scarico
          this.w_MTMAGSCA = this.oParentObject.w_CODMAG
          this.w_MTMAGCAR = SPACE(5)
          this.w_MTFLSCAR = "E"
          this.w_MTFLCARI = " "
        endif
        * --- Verifico se il Movimento di Rettifica Evade un Movimento di Scarico \ Carico
        this.w_MTSERRIF = SPACE(10)
        this.w_MTRIFNUM = 0
        this.w_MTROWRIF = 0
        * --- Read from MOVIMATR
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.MOVIMATR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2],.t.,this.MOVIMATR_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MTSERIAL,MTROWNUM,MTNUMRIF"+;
            " from "+i_cTable+" MOVIMATR where ";
                +"MTCODMAT = "+cp_ToStrODBC(this.w_DRCODMAT);
                +" and MTKEYSAL = "+cp_ToStrODBC(this.w_MMKEYSAL);
                +" and MT_SALDO = "+cp_ToStrODBC(0);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MTSERIAL,MTROWNUM,MTNUMRIF;
            from (i_cTable) where;
                MTCODMAT = this.w_DRCODMAT;
                and MTKEYSAL = this.w_MMKEYSAL;
                and MT_SALDO = 0;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MTSERRIF = NVL(cp_ToDate(_read_.MTSERIAL),cp_NullValue(_read_.MTSERIAL))
          this.w_MTROWRIF = NVL(cp_ToDate(_read_.MTROWNUM),cp_NullValue(_read_.MTROWNUM))
          this.w_MTRIFNUM = NVL(cp_ToDate(_read_.MTNUMRIF),cp_NullValue(_read_.MTNUMRIF))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_Rows<>0
          * --- Se il Movimento � presente imposto a 1 il campo MT_SALDO del documento di origine
          * --- Write into MOVIMATR
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.MOVIMATR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.MOVIMATR_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MT_SALDO ="+cp_NullLink(cp_ToStrODBC(1),'MOVIMATR','MT_SALDO');
                +i_ccchkf ;
            +" where ";
                +"MTSERIAL = "+cp_ToStrODBC(this.w_MTSERRIF);
                +" and MTROWNUM = "+cp_ToStrODBC(this.w_MTROWRIF);
                +" and MTNUMRIF = "+cp_ToStrODBC(this.w_MTRIFNUM);
                +" and MTKEYSAL = "+cp_ToStrODBC(this.w_MMKEYSAL);
                +" and MTCODMAT = "+cp_ToStrODBC(this.w_DRCODMAT);
                   )
          else
            update (i_cTable) set;
                MT_SALDO = 1;
                &i_ccchkf. ;
             where;
                MTSERIAL = this.w_MTSERRIF;
                and MTROWNUM = this.w_MTROWRIF;
                and MTNUMRIF = this.w_MTRIFNUM;
                and MTKEYSAL = this.w_MMKEYSAL;
                and MTCODMAT = this.w_DRCODMAT;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        * --- Nuovo Record in MOVIMATR
        * --- Quattro Insert perch� non ho Link di analisi tra MOVIMATR e CODUBI e CODLOT.
        *     Questo fa si che il Painter inserisca degli spazi al posto di Null
        do case
          case Empty(this.w_CODUBI ) And Empty( this.w_CODLOT )
            * --- Insert into MOVIMATR
            i_nConn=i_TableProp[this.MOVIMATR_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MOVIMATR_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"MTSERIAL"+",MTROWNUM"+",MTNUMRIF"+",MTKEYSAL"+",MTMAGCAR"+",MTMAGSCA"+",MTFLCARI"+",MTFLSCAR"+",MTCODMAT"+",MTSERRIF"+",MTROWRIF"+",MTRIFNUM"+",MT__FLAG"+",MT_SALDO"+",MTCODCOM"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_MMSERIAL),'MOVIMATR','MTSERIAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'MOVIMATR','MTROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(-10),'MOVIMATR','MTNUMRIF');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MMKEYSAL),'MOVIMATR','MTKEYSAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MTMAGCAR),'MOVIMATR','MTMAGCAR');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MTMAGSCA),'MOVIMATR','MTMAGSCA');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MTFLCARI),'MOVIMATR','MTFLCARI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MTFLSCAR),'MOVIMATR','MTFLSCAR');
              +","+cp_NullLink(cp_ToStrODBC(this.w_DRCODMAT),'MOVIMATR','MTCODMAT');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MTSERRIF),'MOVIMATR','MTSERRIF');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MTROWRIF),'MOVIMATR','MTROWRIF');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MTRIFNUM),'MOVIMATR','MTRIFNUM');
              +","+cp_NullLink(cp_ToStrODBC("+"),'MOVIMATR','MT__FLAG');
              +","+cp_NullLink(cp_ToStrODBC(0),'MOVIMATR','MT_SALDO');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODCOM),'MOVIMATR','MTCODCOM');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'MTSERIAL',this.w_MMSERIAL,'MTROWNUM',this.w_CPROWNUM,'MTNUMRIF',-10,'MTKEYSAL',this.w_MMKEYSAL,'MTMAGCAR',this.w_MTMAGCAR,'MTMAGSCA',this.w_MTMAGSCA,'MTFLCARI',this.w_MTFLCARI,'MTFLSCAR',this.w_MTFLSCAR,'MTCODMAT',this.w_DRCODMAT,'MTSERRIF',this.w_MTSERRIF,'MTROWRIF',this.w_MTROWRIF,'MTRIFNUM',this.w_MTRIFNUM)
              insert into (i_cTable) (MTSERIAL,MTROWNUM,MTNUMRIF,MTKEYSAL,MTMAGCAR,MTMAGSCA,MTFLCARI,MTFLSCAR,MTCODMAT,MTSERRIF,MTROWRIF,MTRIFNUM,MT__FLAG,MT_SALDO,MTCODCOM &i_ccchkf. );
                 values (;
                   this.w_MMSERIAL;
                   ,this.w_CPROWNUM;
                   ,-10;
                   ,this.w_MMKEYSAL;
                   ,this.w_MTMAGCAR;
                   ,this.w_MTMAGSCA;
                   ,this.w_MTFLCARI;
                   ,this.w_MTFLSCAR;
                   ,this.w_DRCODMAT;
                   ,this.w_MTSERRIF;
                   ,this.w_MTROWRIF;
                   ,this.w_MTRIFNUM;
                   ,"+";
                   ,0;
                   ,this.w_MMCODCOM;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          case Empty(this.w_CODUBI ) And !Empty( this.w_CODLOT )
            * --- Insert into MOVIMATR
            i_nConn=i_TableProp[this.MOVIMATR_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MOVIMATR_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"MTSERIAL"+",MTROWNUM"+",MTNUMRIF"+",MTKEYSAL"+",MTMAGCAR"+",MTMAGSCA"+",MTFLCARI"+",MTFLSCAR"+",MTCODMAT"+",MTCODLOT"+",MTSERRIF"+",MTROWRIF"+",MTRIFNUM"+",MT__FLAG"+",MT_SALDO"+",MTCODCOM"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_MMSERIAL),'MOVIMATR','MTSERIAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'MOVIMATR','MTROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(-10),'MOVIMATR','MTNUMRIF');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MMKEYSAL),'MOVIMATR','MTKEYSAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MTMAGCAR),'MOVIMATR','MTMAGCAR');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MTMAGSCA),'MOVIMATR','MTMAGSCA');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MTFLCARI),'MOVIMATR','MTFLCARI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MTFLSCAR),'MOVIMATR','MTFLSCAR');
              +","+cp_NullLink(cp_ToStrODBC(this.w_DRCODMAT),'MOVIMATR','MTCODMAT');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CODLOT),'MOVIMATR','MTCODLOT');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MTSERRIF),'MOVIMATR','MTSERRIF');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MTROWRIF),'MOVIMATR','MTROWRIF');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MTRIFNUM),'MOVIMATR','MTRIFNUM');
              +","+cp_NullLink(cp_ToStrODBC("+"),'MOVIMATR','MT__FLAG');
              +","+cp_NullLink(cp_ToStrODBC(0),'MOVIMATR','MT_SALDO');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODCOM),'MOVIMATR','MTCODCOM');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'MTSERIAL',this.w_MMSERIAL,'MTROWNUM',this.w_CPROWNUM,'MTNUMRIF',-10,'MTKEYSAL',this.w_MMKEYSAL,'MTMAGCAR',this.w_MTMAGCAR,'MTMAGSCA',this.w_MTMAGSCA,'MTFLCARI',this.w_MTFLCARI,'MTFLSCAR',this.w_MTFLSCAR,'MTCODMAT',this.w_DRCODMAT,'MTCODLOT',this.w_CODLOT,'MTSERRIF',this.w_MTSERRIF,'MTROWRIF',this.w_MTROWRIF)
              insert into (i_cTable) (MTSERIAL,MTROWNUM,MTNUMRIF,MTKEYSAL,MTMAGCAR,MTMAGSCA,MTFLCARI,MTFLSCAR,MTCODMAT,MTCODLOT,MTSERRIF,MTROWRIF,MTRIFNUM,MT__FLAG,MT_SALDO,MTCODCOM &i_ccchkf. );
                 values (;
                   this.w_MMSERIAL;
                   ,this.w_CPROWNUM;
                   ,-10;
                   ,this.w_MMKEYSAL;
                   ,this.w_MTMAGCAR;
                   ,this.w_MTMAGSCA;
                   ,this.w_MTFLCARI;
                   ,this.w_MTFLSCAR;
                   ,this.w_DRCODMAT;
                   ,this.w_CODLOT;
                   ,this.w_MTSERRIF;
                   ,this.w_MTROWRIF;
                   ,this.w_MTRIFNUM;
                   ,"+";
                   ,0;
                   ,this.w_MMCODCOM;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          case Empty( this.w_CODLOT ) And !Empty(this.w_CODUBI ) 
            * --- Insert into MOVIMATR
            i_nConn=i_TableProp[this.MOVIMATR_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MOVIMATR_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"MTSERIAL"+",MTROWNUM"+",MTNUMRIF"+",MTKEYSAL"+",MTMAGCAR"+",MTMAGSCA"+",MTFLCARI"+",MTFLSCAR"+",MTCODMAT"+",MTCODUBI"+",MTSERRIF"+",MTROWRIF"+",MTRIFNUM"+",MT__FLAG"+",MT_SALDO"+",MTCODCOM"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_MMSERIAL),'MOVIMATR','MTSERIAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'MOVIMATR','MTROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(-10),'MOVIMATR','MTNUMRIF');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MMKEYSAL),'MOVIMATR','MTKEYSAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MTMAGCAR),'MOVIMATR','MTMAGCAR');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MTMAGSCA),'MOVIMATR','MTMAGSCA');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MTFLCARI),'MOVIMATR','MTFLCARI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MTFLSCAR),'MOVIMATR','MTFLSCAR');
              +","+cp_NullLink(cp_ToStrODBC(this.w_DRCODMAT),'MOVIMATR','MTCODMAT');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CODUBI),'MOVIMATR','MTCODUBI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MTSERRIF),'MOVIMATR','MTSERRIF');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MTROWRIF),'MOVIMATR','MTROWRIF');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MTRIFNUM),'MOVIMATR','MTRIFNUM');
              +","+cp_NullLink(cp_ToStrODBC("+"),'MOVIMATR','MT__FLAG');
              +","+cp_NullLink(cp_ToStrODBC(0),'MOVIMATR','MT_SALDO');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODCOM),'MOVIMATR','MTCODCOM');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'MTSERIAL',this.w_MMSERIAL,'MTROWNUM',this.w_CPROWNUM,'MTNUMRIF',-10,'MTKEYSAL',this.w_MMKEYSAL,'MTMAGCAR',this.w_MTMAGCAR,'MTMAGSCA',this.w_MTMAGSCA,'MTFLCARI',this.w_MTFLCARI,'MTFLSCAR',this.w_MTFLSCAR,'MTCODMAT',this.w_DRCODMAT,'MTCODUBI',this.w_CODUBI,'MTSERRIF',this.w_MTSERRIF,'MTROWRIF',this.w_MTROWRIF)
              insert into (i_cTable) (MTSERIAL,MTROWNUM,MTNUMRIF,MTKEYSAL,MTMAGCAR,MTMAGSCA,MTFLCARI,MTFLSCAR,MTCODMAT,MTCODUBI,MTSERRIF,MTROWRIF,MTRIFNUM,MT__FLAG,MT_SALDO,MTCODCOM &i_ccchkf. );
                 values (;
                   this.w_MMSERIAL;
                   ,this.w_CPROWNUM;
                   ,-10;
                   ,this.w_MMKEYSAL;
                   ,this.w_MTMAGCAR;
                   ,this.w_MTMAGSCA;
                   ,this.w_MTFLCARI;
                   ,this.w_MTFLSCAR;
                   ,this.w_DRCODMAT;
                   ,this.w_CODUBI;
                   ,this.w_MTSERRIF;
                   ,this.w_MTROWRIF;
                   ,this.w_MTRIFNUM;
                   ,"+";
                   ,0;
                   ,this.w_MMCODCOM;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          case !Empty(this.w_CODUBI ) And !Empty( this.w_CODLOT )
            * --- Insert into MOVIMATR
            i_nConn=i_TableProp[this.MOVIMATR_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MOVIMATR_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"MTSERIAL"+",MTROWNUM"+",MTNUMRIF"+",MTKEYSAL"+",MTMAGCAR"+",MTMAGSCA"+",MTFLCARI"+",MTFLSCAR"+",MTCODMAT"+",MTCODLOT"+",MTCODUBI"+",MTSERRIF"+",MTROWRIF"+",MTRIFNUM"+",MT__FLAG"+",MT_SALDO"+",MTCODCOM"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_MMSERIAL),'MOVIMATR','MTSERIAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'MOVIMATR','MTROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(-10),'MOVIMATR','MTNUMRIF');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MMKEYSAL),'MOVIMATR','MTKEYSAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MTMAGCAR),'MOVIMATR','MTMAGCAR');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MTMAGSCA),'MOVIMATR','MTMAGSCA');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MTFLCARI),'MOVIMATR','MTFLCARI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MTFLSCAR),'MOVIMATR','MTFLSCAR');
              +","+cp_NullLink(cp_ToStrODBC(this.w_DRCODMAT),'MOVIMATR','MTCODMAT');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CODLOT),'MOVIMATR','MTCODLOT');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CODUBI),'MOVIMATR','MTCODUBI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MTSERRIF),'MOVIMATR','MTSERRIF');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MTROWRIF),'MOVIMATR','MTROWRIF');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MTRIFNUM),'MOVIMATR','MTRIFNUM');
              +","+cp_NullLink(cp_ToStrODBC("+"),'MOVIMATR','MT__FLAG');
              +","+cp_NullLink(cp_ToStrODBC(0),'MOVIMATR','MT_SALDO');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODCOM),'MOVIMATR','MTCODCOM');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'MTSERIAL',this.w_MMSERIAL,'MTROWNUM',this.w_CPROWNUM,'MTNUMRIF',-10,'MTKEYSAL',this.w_MMKEYSAL,'MTMAGCAR',this.w_MTMAGCAR,'MTMAGSCA',this.w_MTMAGSCA,'MTFLCARI',this.w_MTFLCARI,'MTFLSCAR',this.w_MTFLSCAR,'MTCODMAT',this.w_DRCODMAT,'MTCODLOT',this.w_CODLOT,'MTCODUBI',this.w_CODUBI,'MTSERRIF',this.w_MTSERRIF)
              insert into (i_cTable) (MTSERIAL,MTROWNUM,MTNUMRIF,MTKEYSAL,MTMAGCAR,MTMAGSCA,MTFLCARI,MTFLSCAR,MTCODMAT,MTCODLOT,MTCODUBI,MTSERRIF,MTROWRIF,MTRIFNUM,MT__FLAG,MT_SALDO,MTCODCOM &i_ccchkf. );
                 values (;
                   this.w_MMSERIAL;
                   ,this.w_CPROWNUM;
                   ,-10;
                   ,this.w_MMKEYSAL;
                   ,this.w_MTMAGCAR;
                   ,this.w_MTMAGSCA;
                   ,this.w_MTFLCARI;
                   ,this.w_MTFLSCAR;
                   ,this.w_DRCODMAT;
                   ,this.w_CODLOT;
                   ,this.w_CODUBI;
                   ,this.w_MTSERRIF;
                   ,this.w_MTROWRIF;
                   ,this.w_MTRIFNUM;
                   ,"+";
                   ,0;
                   ,this.w_MMCODCOM;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
        endcase
      endif
      * --- Se presente Lotto  \ Ubicazione inserisco il dato in MOVILOTT
      if not empty(this.w_CODLOT+this.w_CODUBI)
        if g_APPLICATION <> "ADHOC REVOLUTION"
          * --- MOVILOTT non � comune ai due applicativi...
          do GSMA_BD2 with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          * --- Modifico lotto e ubicazione sulla riga del movimento magazzino
          this.w_MMLOTMAG = this.w_MMCODMAG
          * --- Write into MVM_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.MVM_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.MVM_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MMCODUBI ="+cp_NullLink(cp_ToStrODBC(this.w_CODUBI),'MVM_DETT','MMCODUBI');
            +",MMCODLOT ="+cp_NullLink(cp_ToStrODBC(this.w_CODLOT),'MVM_DETT','MMCODLOT');
            +",MMLOTMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MMLOTMAG),'MVM_DETT','MMLOTMAG');
                +i_ccchkf ;
            +" where ";
                +"MMSERIAL = "+cp_ToStrODBC(this.w_MMSERIAL);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                +" and MMNUMRIF = "+cp_ToStrODBC(this.w_MMNUMRIF);
                   )
          else
            update (i_cTable) set;
                MMCODUBI = this.w_CODUBI;
                ,MMCODLOT = this.w_CODLOT;
                ,MMLOTMAG = this.w_MMLOTMAG;
                &i_ccchkf. ;
             where;
                MMSERIAL = this.w_MMSERIAL;
                and CPROWNUM = this.w_CPROWNUM;
                and MMNUMRIF = this.w_MMNUMRIF;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          if g_MADV="S" AND (g_PERLOT="S" or g_PERUBI="S") AND !(this.w_ROTLOTUBI==(NVL(this.w_CODLOT,SPACE(20)) + NVL(this.w_CODUBI, SPACE(20) )) )
            this.w_ROTLOTUBI = nvl(this.w_CODLOT,space(20))+nvl(this.w_CODUBI,space(20))
            * --- Aggiorna saldi lotti per i movimenti di magazzino
            GSMD_BRL (this, this.w_MMSERIAL , "M" , "+" , this.w_CPROWNUM,.T.)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          this.w_TESDIS = .F.
          this.w_COART = this.w_DRCODART
          this.w_COMAG = Space(5)
          this.w_CODUBI = Space(20)
          this.w_CODUB2 = Space(20)
          this.w_QTAESI = 0
          this.w_SUQTRPER = 0
          this.w_SUQTAPER = 0
          * --- Read from SALDILOT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.SALDILOT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDILOT_idx,2],.t.,this.SALDILOT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "SUQTAPER,SUQTRPER"+;
              " from "+i_cTable+" SALDILOT where ";
                  +"SUCODART = "+cp_ToStrODBC(this.w_COART);
                  +" and SUCODMAG = "+cp_ToStrODBC(this.w_COMAG);
                  +" and SUCODLOT = "+cp_ToStrODBC(this.w_CODLOT);
                  +" and SUCODUBI = "+cp_ToStrODBC(this.w_CODUBI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              SUQTAPER,SUQTRPER;
              from (i_cTable) where;
                  SUCODART = this.w_COART;
                  and SUCODMAG = this.w_COMAG;
                  and SUCODLOT = this.w_CODLOT;
                  and SUCODUBI = this.w_CODUBI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_SUQTAPER = NVL(cp_ToDate(_read_.SUQTAPER),cp_NullValue(_read_.SUQTAPER))
            this.w_SUQTRPER = NVL(cp_ToDate(_read_.SUQTRPER),cp_NullValue(_read_.SUQTRPER))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_rows>0
            this.w_TESDIS = .T.
          endif
        endif
      endif
      if this.w_CAR
        SELECT DettCarico
      else
        SELECT DettScarico
      endif
      ENDSCAN
    endif
    if this.w_CPROWNUM=50
      * --- Scrivo un nuovo movimento di magazzino
      this.w_CPROWNUM = 0
      this.w_CPROWORD = 0
    endif
  endproc
  proc Try_03F9C0D0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if g_APPLICATION <> "ADHOC REVOLUTION"
      * --- Insert into SALDIART
      i_nConn=i_TableProp[this.SALDIART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDIART_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"SLCODICE"+",SLCODART"+",SLCODVAR"+",SLCODMAG"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_MMKEYSAL),'SALDIART','SLCODICE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DRCODART),'SALDIART','SLCODART');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DRCODVAR),'SALDIART','SLCODVAR');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'SALDIART','SLCODMAG');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'SLCODICE',this.w_MMKEYSAL,'SLCODART',this.w_DRCODART,'SLCODVAR',this.w_DRCODVAR,'SLCODMAG',this.oParentObject.w_CODMAG)
        insert into (i_cTable) (SLCODICE,SLCODART,SLCODVAR,SLCODMAG &i_ccchkf. );
           values (;
             this.w_MMKEYSAL;
             ,this.w_DRCODART;
             ,this.w_DRCODVAR;
             ,this.oParentObject.w_CODMAG;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    else
      * --- Insert into SALDIART
      i_nConn=i_TableProp[this.SALDIART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDIART_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"SLCODICE"+",SLCODMAG"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_DRCODART),'SALDIART','SLCODICE');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'SALDIART','SLCODMAG');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'SLCODICE',this.w_DRCODART,'SLCODMAG',this.oParentObject.w_CODMAG)
        insert into (i_cTable) (SLCODICE,SLCODMAG &i_ccchkf. );
           values (;
             this.w_DRCODART;
             ,this.oParentObject.w_CODMAG;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    return
  proc Try_03F9FD30()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICOM
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SCCODICE"+",SCCODMAG"+",SCCODCAN"+",SCCODART"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_DRCODART),'SALDICOM','SCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'SALDICOM','SCCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODCOM),'SALDICOM','SCCODCAN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DRCODART),'SALDICOM','SCCODART');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SCCODICE',this.w_DRCODART,'SCCODMAG',this.oParentObject.w_CODMAG,'SCCODCAN',this.w_MMCODCOM,'SCCODART',this.w_DRCODART)
      insert into (i_cTable) (SCCODICE,SCCODMAG,SCCODCAN,SCCODART &i_ccchkf. );
         values (;
           this.w_DRCODART;
           ,this.oParentObject.w_CODMAG;
           ,this.w_MMCODCOM;
           ,this.w_DRCODART;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ultimo Documento generato (associato all'utente w_CODUTE)
    * --- Select from RILEVAZI
    i_nConn=i_TableProp[this.RILEVAZI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RILEVAZI_idx,2],.t.,this.RILEVAZI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select MAX(DRNUMDOC) AS NUMDOC  from "+i_cTable+" RILEVAZI ";
          +" where DRADDRIL="+cp_ToStrODBC(this.oParentObject.w_CODUTE)+"";
           ,"_Curs_RILEVAZI")
    else
      select MAX(DRNUMDOC) AS NUMDOC from (i_cTable);
       where DRADDRIL=this.oParentObject.w_CODUTE;
        into cursor _Curs_RILEVAZI
    endif
    if used('_Curs_RILEVAZI')
      select _Curs_RILEVAZI
      locate for 1=1
      do while not(eof())
      this.w_NUMDOC = INT(NVL(NUMDOC,0))
        select _Curs_RILEVAZI
        continue
      enddo
      use
    endif
    * --- Try
    local bErr_03F80F98
    bErr_03F80F98=bTrsErr
    this.Try_03F80F98()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      ah_ErrorMsg("Impossibile generare dati rilevati","!")
      USE IN SELECT("AppMM")
      i_retcode = 'stop'
      return
    endif
    bTrsErr=bTrsErr or bErr_03F80F98
    * --- End
  endproc
  proc Try_03F80F98()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Generazione Movimento di Rilevazione Dati 
    * --- begin transaction
    cp_BeginTrs()
    Select AppMM 
 Go Top
    Scan
    this.w_TIPCON = "R"
    this.w_NUMDOC = this.w_NUMDOC+1
    this.w_GMCODART = DRCODART
    this.w_CODVAR = LEFT(NVL(DRCODVAR, SPACE(20)) + SPACE(20), 20) 
    this.w_DRCODRIC = alltrim(this.w_GMCODART)+iif(empty(nvl(this.w_CODVAR,"")),"","#"+alltrim(this.w_CODVAR))
    this.w_DRKEYSAL = DRKEYSAL
    this.w_DESART = NVL(DESART,SPACE(40))
    if UPPER(g_APPLICATION)="AD HOC ENTERPRISE"
      * --- Read from KEY_ARTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CACODICE"+;
          " from "+i_cTable+" KEY_ARTI where ";
              +"CACODICE = "+cp_ToStrODBC(this.w_DRCODRIC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CACODICE;
          from (i_cTable) where;
              CACODICE = this.w_DRCODRIC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DRCODRIC = NVL(cp_ToDate(_read_.CACODICE),cp_NullValue(_read_.CACODICE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if empty(nvl(this.w_DRCODRIC, " "))
        * --- Read from KEY_ARTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CACODICE"+;
            " from "+i_cTable+" KEY_ARTI where ";
                +"CAKEYSAL = "+cp_ToStrODBC(this.w_DRKEYSAL);
                +" and CATIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CACODICE;
            from (i_cTable) where;
                CAKEYSAL = this.w_DRKEYSAL;
                and CATIPCON = this.w_TIPCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DRCODRIC = NVL(cp_ToDate(_read_.CACODICE),cp_NullValue(_read_.CACODICE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
    endif
    ah_Msg("Elabora articolo: %1",.T.,.F.,.F.,ALLTRIM(this.w_DRCODRIC))
    this.w_CODLOT = iif(Empty(DR_LOTTO),NULL,alltrim(DR_LOTTO))
    if UPPER(g_APPLICATION)="AD HOC ENTERPRISE"
      this.w_KEYLOTTO = iif(Empty(KEYLOTTO),NULL,KEYLOTTO)
    else
      this.w_KEYLOTTO = this.w_CODLOT
    endif
    this.w_CODUBI = iif(Empty(DRUBICAZ),NULL,DRUBICAZ)
    this.w_KEYULO = DRKEYULO
    this.w_DRUNIMIS = DRUNIMIS
    this.w_DRCODCOM = NVL(DRCODCOM , SPACE(15))
    * --- Controllo se Unita di Misura Frazionabile
    * --- Read from UNIMIS
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.UNIMIS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "UMFLFRAZ"+;
        " from "+i_cTable+" UNIMIS where ";
            +"UMCODICE = "+cp_ToStrODBC(this.w_DRUNIMIS);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        UMFLFRAZ;
        from (i_cTable) where;
            UMCODICE = this.w_DRUNIMIS;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_FLFRAZ = NVL(cp_ToDate(_read_.UMFLFRAZ),cp_NullValue(_read_.UMFLFRAZ))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_CODMAT = EVL(DRCODMAT, NULL )
    this.w_QTAESI = NVL(DRQTAES1,0)
    this.w_DRSERIAL = cp_GetProg("RILEVAZI","RILSER",this.w_DRSERIAL,i_CODAZI)
    if UPPER(g_APPLICATION)="AD HOC ENTERPRISE"
      * --- Insert into RILEVAZI
      i_nConn=i_TableProp[this.RILEVAZI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RILEVAZI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.RILEVAZI_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"DRSERIAL"+",DRCODRIL"+",DRCODMAG"+",DRNUMDOC"+",DRADDRIL"+",DRCODRIC"+",DRCODART"+",DRCODVAR"+",DRKEYSAL"+",DRCODUTE"+",DRUBICAZ"+",DR_LOTTO"+",DRKEYULO"+",DRUNIMIS"+",DRQTAESI"+",DRQTAES1"+",DRCONFER"+",UTCC"+",UTDC"+",DRCODMAT"+",DRLOTART"+",DRCODCOM"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_DRSERIAL),'RILEVAZI','DRSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODRIL),'RILEVAZI','DRCODRIL');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'RILEVAZI','DRCODMAG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NUMDOC),'RILEVAZI','DRNUMDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUTE),'RILEVAZI','DRADDRIL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DRCODRIC),'RILEVAZI','DRCODRIC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_GMCODART),'RILEVAZI','DRCODART');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODVAR),'RILEVAZI','DRCODVAR');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DRKEYSAL),'RILEVAZI','DRKEYSAL');
        +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'RILEVAZI','DRCODUTE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODUBI),'RILEVAZI','DRUBICAZ');
        +","+cp_NullLink(cp_ToStrODBC(this.w_KEYLOTTO),'RILEVAZI','DR_LOTTO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_KEYULO),'RILEVAZI','DRKEYULO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DRUNIMIS),'RILEVAZI','DRUNIMIS');
        +","+cp_NullLink(cp_ToStrODBC(this.w_QTAESI),'RILEVAZI','DRQTAESI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_QTAESI),'RILEVAZI','DRQTAES1');
        +","+cp_NullLink(cp_ToStrODBC(" "),'RILEVAZI','DRCONFER');
        +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'RILEVAZI','UTCC');
        +","+cp_NullLink(cp_ToStrODBC(SetInfoDate(g_CALUTD)),'RILEVAZI','UTDC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODMAT),'RILEVAZI','DRCODMAT');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODLOT),'RILEVAZI','DRLOTART');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DRCODCOM),'RILEVAZI','DRCODCOM');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'DRSERIAL',this.w_DRSERIAL,'DRCODRIL',this.oParentObject.w_CODRIL,'DRCODMAG',this.oParentObject.w_CODMAG,'DRNUMDOC',this.w_NUMDOC,'DRADDRIL',this.oParentObject.w_CODUTE,'DRCODRIC',this.w_DRCODRIC,'DRCODART',this.w_GMCODART,'DRCODVAR',this.w_CODVAR,'DRKEYSAL',this.w_DRKEYSAL,'DRCODUTE',i_CODUTE,'DRUBICAZ',this.w_CODUBI,'DR_LOTTO',this.w_KEYLOTTO)
        insert into (i_cTable) (DRSERIAL,DRCODRIL,DRCODMAG,DRNUMDOC,DRADDRIL,DRCODRIC,DRCODART,DRCODVAR,DRKEYSAL,DRCODUTE,DRUBICAZ,DR_LOTTO,DRKEYULO,DRUNIMIS,DRQTAESI,DRQTAES1,DRCONFER,UTCC,UTDC,DRCODMAT,DRLOTART,DRCODCOM &i_ccchkf. );
           values (;
             this.w_DRSERIAL;
             ,this.oParentObject.w_CODRIL;
             ,this.oParentObject.w_CODMAG;
             ,this.w_NUMDOC;
             ,this.oParentObject.w_CODUTE;
             ,this.w_DRCODRIC;
             ,this.w_GMCODART;
             ,this.w_CODVAR;
             ,this.w_DRKEYSAL;
             ,i_CODUTE;
             ,this.w_CODUBI;
             ,this.w_KEYLOTTO;
             ,this.w_KEYULO;
             ,this.w_DRUNIMIS;
             ,this.w_QTAESI;
             ,this.w_QTAESI;
             ," ";
             ,i_CODUTE;
             ,SetInfoDate(g_CALUTD);
             ,this.w_CODMAT;
             ,this.w_CODLOT;
             ,this.w_DRCODCOM;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    else
      * --- Insert into RILEVAZI
      i_nConn=i_TableProp[this.RILEVAZI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RILEVAZI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.RILEVAZI_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"DRSERIAL"+",DRCODRIL"+",DRCODMAG"+",DRNUMDOC"+",DRADDRIL"+",DRCODRIC"+",DRCODART"+",DRCODVAR"+",DRKEYSAL"+",DRCODUTE"+",DRUBICAZ"+",DR_LOTTO"+",DRKEYULO"+",DRUNIMIS"+",DRQTAESI"+",DRQTAES1"+",DRCONFER"+",UTCC"+",UTDC"+",DRCODMAT"+",DRCODCOM"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_DRSERIAL),'RILEVAZI','DRSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODRIL),'RILEVAZI','DRCODRIL');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'RILEVAZI','DRCODMAG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NUMDOC),'RILEVAZI','DRNUMDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUTE),'RILEVAZI','DRADDRIL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DRCODRIC),'RILEVAZI','DRCODRIC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_GMCODART),'RILEVAZI','DRCODART');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODVAR),'RILEVAZI','DRCODVAR');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DRKEYSAL),'RILEVAZI','DRKEYSAL');
        +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'RILEVAZI','DRCODUTE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODUBI),'RILEVAZI','DRUBICAZ');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODLOT),'RILEVAZI','DR_LOTTO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_KEYULO),'RILEVAZI','DRKEYULO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DRUNIMIS),'RILEVAZI','DRUNIMIS');
        +","+cp_NullLink(cp_ToStrODBC(this.w_QTAESI),'RILEVAZI','DRQTAESI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_QTAESI),'RILEVAZI','DRQTAES1');
        +","+cp_NullLink(cp_ToStrODBC(" "),'RILEVAZI','DRCONFER');
        +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'RILEVAZI','UTCC');
        +","+cp_NullLink(cp_ToStrODBC(SetInfoDate(g_CALUTD)),'RILEVAZI','UTDC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODMAT),'RILEVAZI','DRCODMAT');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DRCODCOM),'RILEVAZI','DRCODCOM');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'DRSERIAL',this.w_DRSERIAL,'DRCODRIL',this.oParentObject.w_CODRIL,'DRCODMAG',this.oParentObject.w_CODMAG,'DRNUMDOC',this.w_NUMDOC,'DRADDRIL',this.oParentObject.w_CODUTE,'DRCODRIC',this.w_DRCODRIC,'DRCODART',this.w_GMCODART,'DRCODVAR',this.w_CODVAR,'DRKEYSAL',this.w_DRKEYSAL,'DRCODUTE',i_CODUTE,'DRUBICAZ',this.w_CODUBI,'DR_LOTTO',this.w_CODLOT)
        insert into (i_cTable) (DRSERIAL,DRCODRIL,DRCODMAG,DRNUMDOC,DRADDRIL,DRCODRIC,DRCODART,DRCODVAR,DRKEYSAL,DRCODUTE,DRUBICAZ,DR_LOTTO,DRKEYULO,DRUNIMIS,DRQTAESI,DRQTAES1,DRCONFER,UTCC,UTDC,DRCODMAT,DRCODCOM &i_ccchkf. );
           values (;
             this.w_DRSERIAL;
             ,this.oParentObject.w_CODRIL;
             ,this.oParentObject.w_CODMAG;
             ,this.w_NUMDOC;
             ,this.oParentObject.w_CODUTE;
             ,this.w_DRCODRIC;
             ,this.w_GMCODART;
             ,this.w_CODVAR;
             ,this.w_DRKEYSAL;
             ,i_CODUTE;
             ,this.w_CODUBI;
             ,this.w_CODLOT;
             ,this.w_KEYULO;
             ,this.w_DRUNIMIS;
             ,this.w_QTAESI;
             ,this.w_QTAESI;
             ," ";
             ,i_CODUTE;
             ,SetInfoDate(g_CALUTD);
             ,this.w_CODMAT;
             ,this.w_DRCODCOM;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    Select AppMM
    EndScan
    * --- commit
    cp_EndTrs(.t.)
    return

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pEvent)
    this.pEvent=pEvent
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,16)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='CAM_AGAZ'
    this.cWorkTables[3]='MVM_DETT'
    this.cWorkTables[4]='MVM_MAST'
    this.cWorkTables[5]='SALDIART'
    this.cWorkTables[6]='VALUTE'
    this.cWorkTables[7]='RILEVAZI'
    this.cWorkTables[8]='MOVIMATR'
    this.cWorkTables[9]='*TMPVEND3'
    this.cWorkTables[10]='LOTTIART'
    this.cWorkTables[11]='SALDILOT'
    this.cWorkTables[12]='KEY_ARTI'
    this.cWorkTables[13]='UNIMIS'
    this.cWorkTables[14]='RILEVAZI'
    this.cWorkTables[15]='MAGAZZIN'
    this.cWorkTables[16]='SALDICOM'
    return(this.OpenAllTables(16))

  proc CloseCursors()
    if used('_Curs_GSMAMSDR')
      use in _Curs_GSMAMSDR
    endif
    if used('_Curs_gsma_bdr')
      use in _Curs_gsma_bdr
    endif
    if used('_Curs_RILEVAZI')
      use in _Curs_RILEVAZI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEvent"
endproc
