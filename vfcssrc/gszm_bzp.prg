* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gszm_bzp                                                        *
*              Tasto destro per PN/sc/dis                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_17]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-09-13                                                      *
* Last revis.: 2012-06-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgszm_bzp",oParentObject)
return(i_retval)

define class tgszm_bzp as StdBatch
  * --- Local variables
  w_SERIALE = space(10)
  w_NUMRIF = 0
  w_ROWORD = 0
  w_ROWNUM = 0
  w_GEST = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Per lanciare gestione  PN/SC/DISL da tasto destro su zoom integrato
    this.w_SERIALE = g_oMenu.getValue("PTSERIAL")
    this.w_ROWORD = g_oMenu.getValue("PTROWORD")
    this.w_ROWNUM = g_oMenu.getValue("CPROWNUM")
    if  Not Empty(this.w_SERIALE)
      this.w_GEST = gste_bzp(g_oMenu.oParentObject,this.w_SERIALE,this.w_ROWORD,this.w_ROWNUM,0,0,this.w_ROWORD)
      if Type("This.w_GEST")="O" 
        do case
          case g_oMenu.cBatchType = "M"
            if this.w_GEST.HasCpEvents("ecpEdit")
              this.w_GEST.ecpEdit()     
            endif
          case g_oMenu.cBatchType = "L"
            if this.w_GEST.HasCpEvents("ecpLoad")
              this.w_GEST.ecpLoad()     
            endif
        endcase
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
