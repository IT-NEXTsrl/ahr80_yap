* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bgs                                                        *
*              Gestione INF caraica salva dati esterni                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][101]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-05-12                                                      *
* Last revis.: 2014-12-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bgs",oParentObject)
return(i_retval)

define class tgsut_bgs as StdBatch
  * --- Local variables
  w_TMPN = 0
  w_TEMP = space(100)
  w_NFCONV = 0
  w_FILENAME = space(20)
  w_CURDIR = space(100)
  w_FILE_SP = space(100)
  w_ERR_MSG = space(200)
  w_LISTA = .NULL.
  * --- WorkFile variables
  OUT_PUTS_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_CURDIR = SYS(5)+SYS(2003)+"\"
    if g_ADHOCONE
      this.w_CURDIR = STRTRAN(this.w_CURDIR,"\EXE\","\..\PCON\CARSAL\")
    else
      this.w_CURDIR = STRTRAN(this.w_CURDIR,"\EXE\","\PCON\CARSAL\")
    endif
    this.w_NFCONV = aDIR(lARRDIR,this.w_CURDIR+"*.INF")
    if this.w_NFCONV>0
      this.w_TMPN = 1
      do while this.w_TMPN <= this.w_NFCONV
        * --- Compone Nome file INF completo
        this.w_FILENAME = alltrim(lARRDIR(this.w_TMPN,1))
        * --- Compone Nome file INF completo
        this.w_FILE_SP = this.w_CURDIR+this.w_FILENAME
        * --- Se non si sono verificati errori rinomina il file
        this.w_ERR_MSG = ""
        * --- Leggo il file inf ed aggiorno la tabella relativa alla combo delle funzionalitą gestite nel carica salva dati esterni in base all'applicativo in uso
        this.w_ERR_MSG = carsaldaInf (this.w_FILE_SP,.F. )
        if EMPTY (this.w_ERR_MSG)
          this.w_TEMP = strtran(upper(this.w_FILE_SP),".INF",".OK")
          if file(this.w_TEMP)
            ERASE (this.w_TEMP)
          endif
          if file(this.w_FILE_SP)
            rename (this.w_FILE_SP) to (this.w_TEMP)
          endif
        endif
        * --- Aggiorna contatore
        this.w_TMPN = this.w_TMPN + 1
      enddo
      this.oParentObject.oPgFrm.Page1.oPag.oTIPARC_1_5.Clear()
      this.oParentObject.oPgFrm.Page1.oPag.oTIPARC_1_5.Init()
      this.oParentObject.oPgFrm.Page2.oPag.oTIPARC_2_2.Clear()
      this.oParentObject.oPgFrm.Page2.oPag.oTIPARC_2_2.Init()
      This.oParentObject.w_TIPARC=iif(ISALT(),"MG","NM")
      This.oParentObject.SetControlsValue()
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='OUT_PUTS'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
