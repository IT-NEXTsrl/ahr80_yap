* ---------------------------------------------------------------------------- *
* #%&%#Build:0000
*                                                                              *
*   Procedure: TX_DDTZU                                                        *
*              Documento Di Trasporto Zucchet                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 31/5/01                                                         *
* Last revis.: 20/5/08                                                         *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
private i_formh13,i_formh14
private i_brk,i_quit,i_row,i_pag,i_oldarea,i_oldrows
private i_rdvars,i_rdkvars,i_rdkey
private w_s,i_rec,i_wait,i_modal
private i_usr_brk          && .T. se l'utente ha interrotto la stampa
private i_frm_rpr          && .T. se � stata stampata la testata del report
private Sm                 && Variabile di appoggio per formattazione memo
private i_form_ph,i_form_phh,i_form_phg,i_form_pf,i_saveh13,i_exec

* --- Variabili per configurazione stampante
private w_t_stdevi,w_t_stmsin,w_t_stnrig
private w_t_strese,w_t_stlung,w_t_st10,w_t_st12,w_t_st15
private w_t_stcomp,w_t_stnorm,w_t_stnlq,w_t_stdraf
private w_t_stpica,w_t_stelit
private w_t_stbold,w_t_stwide,w_t_stital,w_t_stunde
private w_t_stbol_,w_t_stwid_,w_t_stita_,w_t_stund_
private w_stdesc
store " " to w_t_stdevi
store " " to w_t_strese,w_t_stlung,w_t_st10,w_t_st12,w_t_st15
store " " to w_t_stcomp,w_t_stnorm,w_t_stnlq,w_t_stdraf
store " " to w_t_stpica,w_t_stelit, i_rdkey
store " " to w_t_stbold,w_t_stwide,w_t_stital,w_t_stunde
store " " to w_t_stbol_,w_t_stwid_,w_t_stita_,w_t_stund_
store ""  to Sm
store 0 to i_formh13,i_formh14,i_form_phh,i_form_phg,i_saveh13
store space(20) to w_stdesc
i_form_ph = 12
i_form_pf = 13
i_wait = 1
i_modal = .t.
i_oldrows = 0
store .F. to i_usr_brk, i_frm_rpr, w_s
i_exec = ""

dimension i_rdvars[41,2],i_rdkvars[6,2],i_zoompars[3]
store 0 to i_rdvars[1,1],i_rdkvars[1,1]
private w_StComp,w_DATITEST1,w_IVAFIS,w_NOMDES,w_DATITEST
private w_NUMDOC,w_DATDOC,w_DESPOR,w_CAUTRA,w_SERIAL
private w_StComp,w_StComp,w_DATITEST1,w_IVAFIS,w_NOMDES
private w_DATITEST,w_NUMDOC,w_DATDOC,w_DESPOR,w_CAUTRA
private w_SERIAL,w_StComp,w_CODICE,w_MVDESART,w_DESAR2
private w_TXT2,L_NOTE,w_VETDAT3,w_PROVET3,w_VETDAT2
private w_PROVET2,w_VETDAT1,w_PROVE1,SETRIG,w_MVQTACOL
private w_QTAPES,w_QTALOR,w_DESSPE,w_DATTRA,w_DESVET
private w_VETDAT,w_DESVE2,w_VETDA2,w_DESVE3,w_VETDA3

w_StComp = space(2)
w_DATITEST1 = space(35)
w_IVAFIS = space(25)
w_NOMDES = space(40)
w_DATITEST = space(35)
w_NUMDOC = space(10)
w_DATDOC = ctod("  /  /  ")
w_DESPOR = space(20)
w_CAUTRA = space(25)
w_SERIAL = space(10)
w_StComp = space(2)
w_StComp = space(2)
w_DATITEST1 = space(35)
w_IVAFIS = space(25)
w_NOMDES = space(40)
w_DATITEST = space(35)
w_NUMDOC = space(10)
w_DATDOC = ctod("  /  /  ")
w_DESPOR = space(20)
w_CAUTRA = space(25)
w_SERIAL = space(10)
w_StComp = space(2)
w_CODICE = space(18)
w_MVDESART = space(40)
w_DESAR2 = space(0)
w_TXT2 = space(0)
L_NOTE = space(40)
w_VETDAT3 = space(30)
w_PROVET3 = space(30)
w_VETDAT2 = space(30)
w_PROVET2 = space(30)
w_VETDAT1 = space(30)
w_PROVE1 = space(30)
SETRIG = 0
w_MVQTACOL = space(4)
w_QTAPES = space(8)
w_QTALOR = space(8)
w_DESSPE = space(20)
w_DATTRA = space(20)
w_DESVET = space(40)
w_VETDAT = space(40)
w_DESVE2 = space(40)
w_VETDA2 = space(40)
w_DESVE3 = space(40)
w_VETDA3 = space(40)

i_oldarea = select()
select __tmp__
go top

w_t_stnrig = 65
w_t_stmsin = 0
  
  
* --- Inizializza Variabili per configurazione stampante da CP_CHPRN
w_t_stdevi = cFileStampa+'.prn'
w_t_stlung = ts_ForPag
w_t_stnrig = ts_RowOk 
w_t_strese = ts_Reset+ts_Inizia
w_t_st10 = ts_10Cpi
w_t_st12 = ts_12Cpi
w_t_st15 = ts_15Cpi
w_t_stcomp = ts_Comp
w_t_stnorm = ts_RtComp
w_t_stbold = ts_StBold
w_t_stwide = ts_StDoub
w_t_stital = ts_StItal
w_t_stunde = ts_StUnde
w_t_stbol_ = ts_FiBold
w_t_stwid_ = ts_FiDoub
w_t_stita_ = ts_FiItal
w_t_stund_ = ts_FiUnde
* --- non definiti
*w_t_stmsin
*w_t_stnlq
*w_t_stdraf
*w_t_stpica
*w_t_stelit

i_row = 0
i_pag = 1
*---------------------------------------
wait wind "Generazione file appoggio..." nowait
*----------------------------------------
activate screen
* --- Settaggio stampante
set printer to &w_t_stdevi
set device to printer
set margin to w_t_stmsin
if len(trim(w_t_strese))>0
  @ 0,0 say &w_t_strese
endif
if len(trim(w_t_stlung))>0
  @ 0,0 say &w_t_stlung
endif
* --- Inizio stampa
do TX_D4TZU with 1, 0
if i_frm_rpr .and. .not. i_usr_brk
  * stampa il piede del report
  do TX_D4TZU with 14, 0
endif
if i_row<>0 
  @ prow(),pcol() say chr(12)
endif
set device to screen
set printer off
set printer to
if .not. i_frm_rpr
  do cplu_erm with "Non ci sono dati da stampare"
endif
* --- Fine
if .not.(empty(wontop()))
  activate  window (wontop())
endif
i_warea = alltrim(str(i_oldarea))
select (i_oldarea)
return


procedure TX_D4TZU
* === Procedure TX_D4TZU
parameters i_form_id, i_height

private i_currec, i_prevrec, i_formh
private i_frm_brk    && flag che indica il verificarsi di un break interform
                     && anche se la specifica condizione non � soddisfatta
private i_break, i_cond1, i_cond2

do case
  case i_form_id=1
    select __tmp__
    i_warea = '__tmp__'
    * --- inizializza le condizioni dei break interform
    i_cond1 = (MVSERIAL)
    i_cond2 = (MVSERIAL)
    i_frm_brk = .T.
    do while .not. eof() 
     wait wind "Elabora riga:"+str(recno()) +"/"+str(reccount()) nowait
      if .not. i_frm_rpr
        * --- stampa l'intestazione del report
        do TX_D4TZU with 11, 0
        i_frm_rpr = .T.
      endif
      if i_cond1<>(MVSERIAL) .or. i_frm_brk
        i_cond1 = (MVSERIAL)
        i_frm_brk = .T.
        do TX_D5TZU with 1.00, 23
      endif
      if i_cond2<>(MVSERIAL) .or. i_frm_brk
        i_cond2 = (MVSERIAL)
        do TX_D4TZU with 98
        i_form_ph = 1.01
        i_form_phh = 23
        i_form_phg = 435
      endif
      i_frm_brk = .F.
      * stampa del dettaglio
        do TX_D5TZU with 1.02, 0
      if (CTRL_A() or CTRL_D()) and not empty(nvl(mvcodice,' ')) AND CTRL_COAC()
        do TX_D5TZU with 1.03, 1
      endif
      if Ctrl1() AND Fscrdessup() AND NOT EMPTY(ALLTRIM(NVL(mvdessup,' '))) AND g_PERSDA='S' AND CTRL_COAC()
        do TX_D5TZU with 1.04, 1
      endif
      if not empty(nvl(L_NOTE,' ')) AND ULTRIG='S'
        do TX_D5TZU with 1.05, 1
      endif
      if i_usr_brk
        exit
      endif
      * --- passa al record successivo
      i_prevrec = recno()
      if .not. eof()
        skip
      endif
      i_currec = iif(eof(), -1, recno())
      if eof()  .or. i_cond2<>(MVSERIAL) .or. i_cond1<>(MVSERIAL)      
        i_form_ph = 12
        i_form_pf = 13
        i_formh13 = i_saveh13
      endif
      if eof()  .or. i_cond1<>(MVSERIAL)      
        go i_prevrec
        do TX_D5TZU with 1.07, 12
        do cplu_go with i_currec
      endif
    enddo
  case i_form_id=99
    * --- controllo per il salto pagina
    if inkey()=27
      i_usr_brk = .T.
    else
      if i_row+i_height+i_formh13>w_t_stnrig
        * --- stampa il piede di pagina
        i_row = w_t_stnrig-i_formh13
        if i_form_pf=13
          do TX_D4TZU with 13, 0
        else
          do TX_D5TZU with -i_form_pf,i_formh13
        endif
        i_row = 0
        i_pag = i_pag+1
        @ prow(),pcol() say chr(12)+chr(13)
        w_s = 0
        * --- stampa l'intestazione di pagina
        if i_form_ph=12
          do TX_D4TZU with 12, 0
        else
          do TX_D5TZU with -i_form_ph,i_form_phh
        endif
      endif
    endif
  case i_form_id=98
    i_form_pf = 1.06
    i_saveh13 = i_formh13
    i_formh13 = 12
endcase
return

procedure TX_D5TZU
* === Procedure TX_D5TZU
parameters i_form_id, i_form_h

* --- controllo per il salto pagina
if i_form_id<11 .and. i_form_id>0
  do TX_D4TZU with 99, i_form_h
  if i_usr_brk
    return
  endif
endif
if i_form_id<0
  i_form_id = -i_form_id
endif
do case
  * --- 1� form
  case i_form_id=1.0
    do frm1_0
  case i_form_id=1.01
    do frm1_01
  case i_form_id=1.02
    do frm1_02
  case i_form_id=1.03
    do frm1_03
  case i_form_id=1.04
    do frm1_04
  case i_form_id=1.05
    do frm1_05
  case i_form_id=1.06
    do frm1_06
  case i_form_id=1.07
    do frm1_07
endcase
i_row = i_row+i_form_h
return


* --- 1� form
procedure frm1_0
  w_StComp = ' '
  if len(trim('&w_t_stnorm'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stnorm,""
  endif
  i_fn = ''
  do F_Say with i_row+4,i_row+at_y(82-0),37,at_x(301),transform(w_StComp,""),i_fn
  if NOT EMPTY (NVL(ANDESCRI,' '))
  i_fn = ''
  do F_Say with i_row+4,i_row+at_y(82-0),40,at_x(325),"Spett.le",i_fn
  endif
  i_fn = ''
  do F_Say with i_row+5,i_row+at_y(103-0),40,at_x(325),transform(ANCODICE,""),i_fn
  i_fn = ''
  do F_Say with i_row+6,i_row+at_y(122-0),40,at_x(325),transform(ANDESCRI,""),i_fn
  i_fn = ''
  do F_Say with i_row+7,i_row+at_y(141-0),40,at_x(325),transform(ANINDIRI,""),i_fn
  w_DATITEST1 = left(Fdatitest1(),35)
  i_fn = ''
  do F_Say with i_row+8,i_row+at_y(160-0),40,at_x(325),transform(w_DATITEST1,""),i_fn
  w_IVAFIS = ltrim(iif(empty(anpariva),ancodfis,((nvl(looktab('nazioni','nacodiso','nacodnaz',annazion),''))+anpariva)))
  i_fn = ''
  do F_Say with i_row+9,i_row+at_y(179-0),40,at_x(325),transform(w_IVAFIS,""),i_fn
  if TDCATDOC='RF'
  i_fn = ''
  do F_Say with i_row+13,i_row+at_y(254-0),-1,at_x(-9),"X",i_fn
  endif
  w_NOMDES = ALLTRIM(LEFT(DDNOMDES,40))
  i_fn = ''
  do F_Say with i_row+13,i_row+at_y(255-0),40,at_x(325),transform(w_NOMDES,""),i_fn
  if not empty(nvl(mvcoddes,''))
  i_fn = ''
  do F_Say with i_row+14,i_row+at_y(274-0),40,at_x(325),transform(DDINDIRI,""),i_fn
  endif
  w_DATITEST = left(Fdatitest(),35)
  if not empty(nvl(mvcoddes,''))
  i_fn = ''
  do F_Say with i_row+15,i_row+at_y(293-0),40,at_x(325),transform(w_DATITEST,""),i_fn
  endif
  w_NUMDOC = Alltrim(str(mvnumdoc,15))+iif(empty(mvalfdoc),'','/'+Alltrim(mvalfdoc))
  i_fn = ''
  do F_Say with i_row+17,i_row+at_y(331-0),5,at_x(44),transform(w_NUMDOC,""),i_fn
  i_PAG=IIF(w_SERIAL<>MVSERIAL,1,i_PAG)
  i_fn = ''
  do F_Say with i_row+17,i_row+at_y(331-0),23,at_x(188),transform(i_pag,"9999"),i_fn
  w_DATDOC = CP_TODATE(MVDATDOC)
  i_fn = ''
  do F_Say with i_row+18,i_row+at_y(350-0),5,at_x(44),transform(w_DATDOC,""),i_fn
  w_DESPOR = left(NVL(translat(MVTIPCON,MVCODCON,'TRADPORT','lgdescri','LGCODPOR','lgcodlin','PORTI', 'PODESPOR','POCODPOR',mvcodpor),''),20)
  i_fn = ''
  do F_Say with i_row+21,i_row+at_y(413-0),4,at_x(39),transform(w_DESPOR,""),i_fn
  w_CAUTRA = looktab('cam_agaz','cmdescri','cmcodice',mvtcamag)
  i_fn = ''
  do F_Say with i_row+21,i_row+at_y(413-0),25,at_x(203),transform(w_CAUTRA,""),i_fn
   Ambdoc()
  w_SERIAL = MVSERIAL
  IF .F.
  i_fn = ''
  do F_Say with i_row+21,i_row+at_y(413-0),42,at_x(343),transform(w_SERIAL,""),i_fn
   ENDIF
  w_StComp = ' '
  i_fn = ''
  do F_Say with i_row+21,i_row+at_y(413-0),80,at_x(646),transform(w_StComp,""),i_fn
  if len(trim('&w_t_stcomp'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stcomp,""
  endif
return

* --- frm1_01
procedure frm1_01
  w_StComp = ' '
  if len(trim('&w_t_stnorm'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stnorm,""
  endif
  i_fn = ''
  do F_Say with i_row+4,i_row+at_y(541-458),37,at_x(298),transform(w_StComp,""),i_fn
  if NOT EMPTY (NVL(ANDESCRI,' '))
  i_fn = ''
  do F_Say with i_row+4,i_row+at_y(541-458),40,at_x(325),"Spett.le",i_fn
  endif
  i_fn = ''
  do F_Say with i_row+5,i_row+at_y(562-458),40,at_x(325),transform(ANCODICE,""),i_fn
  i_fn = ''
  do F_Say with i_row+6,i_row+at_y(581-458),40,at_x(325),transform(ANDESCRI,""),i_fn
  i_fn = ''
  do F_Say with i_row+7,i_row+at_y(600-458),40,at_x(325),transform(ANINDIRI,""),i_fn
  w_DATITEST1 = left(Fdatitest1(),35)
  i_fn = ''
  do F_Say with i_row+8,i_row+at_y(619-458),40,at_x(325),transform(w_DATITEST1,""),i_fn
  w_IVAFIS = ltrim(iif(empty(anpariva),ancodfis,((nvl(looktab('nazioni','nacodiso','nacodnaz',annazion),''))+anpariva)))
  i_fn = ''
  do F_Say with i_row+9,i_row+at_y(638-458),40,at_x(325),transform(w_IVAFIS,""),i_fn
  w_NOMDES = ALLTRIM(LEFT(DDNOMDES,40))
  i_fn = ''
  do F_Say with i_row+13,i_row+at_y(714-458),40,at_x(325),transform(w_NOMDES,""),i_fn
  if not empty(nvl(mvcoddes,''))
  i_fn = ''
  do F_Say with i_row+14,i_row+at_y(733-458),40,at_x(325),transform(DDINDIRI,""),i_fn
  endif
  w_DATITEST = left(Fdatitest(),35)
  if not empty(nvl(mvcoddes,''))
  i_fn = ''
  do F_Say with i_row+15,i_row+at_y(751-458),40,at_x(325),transform(w_DATITEST,""),i_fn
  endif
  w_NUMDOC = Alltrim(str(mvnumdoc,15))+iif(empty(mvalfdoc),'','/'+Alltrim(mvalfdoc))
  i_fn = ''
  do F_Say with i_row+17,i_row+at_y(791-458),5,at_x(44),transform(w_NUMDOC,""),i_fn
  i_PAG=IIF(w_SERIAL<>MVSERIAL,1,i_PAG)
  i_fn = ''
  do F_Say with i_row+17,i_row+at_y(791-458),23,at_x(188),transform(i_pag,"9999"),i_fn
  w_DATDOC = CP_TODATE(MVDATDOC)
  i_fn = ''
  do F_Say with i_row+18,i_row+at_y(810-458),5,at_x(44),transform(w_DATDOC,""),i_fn
  w_DESPOR = left(nvl(looktab('porti','podespor','pocodpor',mvcodpor),''),20)
  i_fn = ''
  do F_Say with i_row+21,i_row+at_y(866-458),4,at_x(39),transform(w_DESPOR,""),i_fn
  w_CAUTRA = looktab('cam_agaz','cmdescri','cmcodice',mvtcamag)
  i_fn = ''
  do F_Say with i_row+21,i_row+at_y(866-458),25,at_x(203),transform(w_CAUTRA,""),i_fn
   Ambdoc()
  w_SERIAL = MVSERIAL
  IF .F.
  i_fn = ''
  do F_Say with i_row+21,i_row+at_y(866-458),43,at_x(345),transform(w_SERIAL,""),i_fn
   ENDIF
  w_StComp = ' '
  i_fn = ''
  do F_Say with i_row+21,i_row+at_y(866-458),81,at_x(648),transform(w_StComp,""),i_fn
  if len(trim('&w_t_stcomp'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stcomp,""
  endif
return

* --- frm1_02
procedure frm1_02
return

* --- frm1_03
procedure frm1_03
  w_CODICE = iif(nvl(anflcodi,' ')='S',left(mvcodice,18),left(mvcodart,18))
  if nvl(arstacod,' ')<>'S'
  i_fn = ''
  do F_Say with i_row+0,i_row+at_y(933-932),4,at_x(39),transform(w_CODICE,""),i_fn
  endif
  w_MVDESART = left(alltrim(MVDESART),40)
  i_fn = ''
  do F_Say with i_row+0,i_row+at_y(933-932),27,at_x(223),transform(w_MVDESART,""),i_fn
  i_fn = ''
  do F_Say with i_row+0,i_row+at_y(933-932),104,at_x(832),transform(MVUNIMIS,Repl('X',3)),i_fn
  if mvtiprig<>'D' and fcodart()
  i_fn = ''
  do F_Say with i_row+0,i_row+at_y(933-932),118,at_x(950),transform(MVQTAMOV,'@Z '+v_PQ(11)),i_fn
  endif
return

* --- frm1_04
procedure frm1_04
  w_DESAR2 = mvdessup
  i_fn = ''
  w_s = 65
  set memowidth to w_s
  Sm = mline(w_DESAR2,1)+space(w_s-len(mline(w_DESAR2,1)))
  do F_Say with i_row+0,i_row+at_y(970-969),27,at_x(223),alltrim(Sm),i_fn
  i_ml = 0
  w_s = 65
  set memowidth to w_s
  i_ml = max(i_ml,memlines(w_DESAR2))
  i_row = i_row+i_form_h
  for i_i=2 to i_ml
    do TX_D4TZU with 99,1
    if i_usr_brk
      exit
    endif
    w_s = 65
    set memowidth to w_s
      i_fn = ''
    Sm = mline(w_DESAR2,i_i)+space(w_s-len(mline(w_DESAR2,i_i)))
    do F_Say with i_row,i_row,27,at_x(223),alltrim(Sm),i_fn
    i_row = i_row+1
  next
  i_row = i_row-i_form_h
return

* --- frm1_05
procedure frm1_05
  w_TXT2 = L_NOTE
  if ultrig='S'
  i_fn = ''
  w_s = 64
  set memowidth to w_s
  Sm = mline(w_TXT2,1)+space(w_s-len(mline(w_TXT2,1)))
  do F_Say with i_row+0,i_row+at_y(1009-1008),28,at_x(224),alltrim(Sm),i_fn
  endif
  i_ml = 0
  w_s = 64
  set memowidth to w_s
  i_ml = max(i_ml,memlines(w_TXT2))
  i_row = i_row+i_form_h
  for i_i=2 to i_ml
    do TX_D4TZU with 99,1
    if i_usr_brk
      exit
    endif
    w_s = 64
    set memowidth to w_s
      i_fn = ''
    Sm = mline(w_TXT2,i_i)+space(w_s-len(mline(w_TXT2,i_i)))
    do F_Say with i_row,i_row,28,at_x(224),alltrim(Sm),i_fn
    i_row = i_row+1
  next
  i_row = i_row-i_form_h
return

* --- frm1_06
procedure frm1_06
  i_fn = ''
  do F_Say with i_row+6,i_row+at_y(1160-1047),117,at_x(936),"SEGUE =======>",i_fn
return

* --- frm1_07
procedure frm1_07
  L_NOTE = ' '
  IF .F.
  i_fn = ''
  do F_Say with i_row+0,i_row+at_y(1302-1301),9,at_x(72),transform(L_NOTE,""),i_fn
   ENDIF
  w_VETDAT3 = FVetdat3()
  IF .F.
  i_fn = ''
  do F_Say with i_row+0,i_row+at_y(1302-1301),12,at_x(102),transform(w_VETDAT3,""),i_fn
   ENDIF
  w_PROVET3 = NVL(looktab('vettori','vtprovet','vtcodvet',mvcodve3),'')
  IF .F.
  i_fn = ''
  do F_Say with i_row+0,i_row+at_y(1302-1301),15,at_x(123),transform(w_PROVET3,""),i_fn
   ENDIF
  w_VETDAT2 = FVetdat2()
  IF .F.
  i_fn = ''
  do F_Say with i_row+0,i_row+at_y(1302-1301),19,at_x(156),transform(w_VETDAT2,""),i_fn
   ENDIF
  w_PROVET2 = NVL(looktab('vettori','vtprovet','vtcodvet',mvcodve2),'')
  IF .F.
  i_fn = ''
  do F_Say with i_row+0,i_row+at_y(1302-1301),22,at_x(177),transform(w_PROVET2,""),i_fn
   ENDIF
  w_VETDAT1 = FVetdat1()
  IF .F.
  i_fn = ''
  do F_Say with i_row+0,i_row+at_y(1302-1301),24,at_x(197),transform(w_VETDAT1,""),i_fn
   ENDIF
  w_PROVE1 = NVL(looktab('vettori','vtprovet','vtcodvet',mvcodvet),'')
  IF .F.
  i_fn = ''
  do F_Say with i_row+0,i_row+at_y(1302-1301),27,at_x(216),transform(w_PROVE1,""),i_fn
   ENDIF
  SETRIG = ' '
  I_ROW=56
  i_fn = ''
  do F_Say with i_row+0,i_row+at_y(1302-1301),33,at_x(264),transform(SETRIG,""),i_fn
  if NVL(TDFLACCO,' ')='S'
  i_fn = ''
  do F_Say with i_row+1,i_row+at_y(1321-1301),3,at_x(31),transform(MVASPEST,""),i_fn
  endif
  w_MVQTACOL = TRAN(MVQTACOL)
  if NOT EMPTY(MVQTACOL) AND nvl(tdflpack,'')='S'
  i_fn = ''
  do F_Say with i_row+1,i_row+at_y(1321-1301),75,at_x(603),transform(w_MVQTACOL,""),i_fn
  endif
  w_QTAPES = TRAN(mvqtapes,'@KZ '+v_PV(91))
  if NOT EMPTY(NVL(MVQTAPES,'')) and nvl(tdflpack,'')='S'
  i_fn = ''
  do F_Say with i_row+1,i_row+at_y(1321-1301),94,at_x(759),transform(w_QTAPES,""),i_fn
  endif
  w_QTALOR = TRAN(mvqtalor,'@KZ '+v_PV(91))
  if NOT EMPTY(NVL(MVQTALOR,'')) and nvl(tdflpack,'')='S'
  i_fn = ''
  do F_Say with i_row+1,i_row+at_y(1321-1301),121,at_x(975),transform(w_QTALOR,""),i_fn
  endif
  w_DESSPE = NVL(translat(MVTIPCON,MVCODCON,'TRADSPED','lgdescri','LGCODSPE','lgcodlin','MODASPED', 'SPDESSPE','SPCODSPE',mvcodspe),'')
  i_fn = ''
  do F_Say with i_row+3,i_row+at_y(1359-1301),3,at_x(29),transform(w_DESSPE,""),i_fn
  w_DATTRA = dtoc(mvdattra)+'          '+iif(not empty(nvl(mvoratra,0)) and not empty(nvl(mvmintra,0)),mvoratra+':'+mvmintra,space(5))
  if not empty(nvl(dtoc(mvdattra),' '))
  i_fn = ''
  do F_Say with i_row+3,i_row+at_y(1359-1301),70,at_x(563),transform(w_DATTRA,""),i_fn
  endif
  w_DESVET = nvl(looktab('vettori','vtdesvet','vtcodvet',mvcodvet),'')
  i_fn = ''
  do F_Say with i_row+5,i_row+at_y(1397-1301),3,at_x(29),transform(w_DESVET,""),i_fn
  w_VETDAT = w_VETDAT1+alltrim(w_PROVE1)
  i_fn = ''
  do F_Say with i_row+5,i_row+at_y(1397-1301),44,at_x(354),transform(w_VETDAT,""),i_fn
  w_DESVE2 = nvl(looktab('vettori','vtdesvet','vtcodvet',mvcodve2),'')
  i_fn = ''
  do F_Say with i_row+7,i_row+at_y(1435-1301),3,at_x(29),transform(w_DESVE2,""),i_fn
  w_VETDA2 = w_VETDAT2+alltrim(w_PROVET2)
  i_fn = ''
  do F_Say with i_row+7,i_row+at_y(1435-1301),44,at_x(354),transform(w_VETDA2,""),i_fn
  w_DESVE3 = nvl(looktab('vettori','vtdesvet','vtcodvet',mvcodve3),'')
  i_fn = ''
  do F_Say with i_row+9,i_row+at_y(1473-1301),3,at_x(30),transform(w_DESVE3,""),i_fn
  w_VETDA3 = w_VETDAT3+alltrim(w_PROVET3)
  i_fn = ''
  do F_Say with i_row+9,i_row+at_y(1473-1301),44,at_x(355),transform(w_VETDA3,""),i_fn
  i_fn = ''
  do F_Say with i_row+11,i_row+at_y(1511-1301),3,at_x(31),transform(MVNOTAGG,""),i_fn
return

function at_x
parameters i_pos, i_fnt, i_h, i_s
  i_pos = i_pos/6
  return i_pos

function at_y
parameters i_pos, i_fnt, i_h, i_s
  i_pos = i_pos/13
  return i_pos

procedure F_Say
  parameter y,py,x,px,s,f
  * --- Questa funzione corregge un errore del driver "Generica solo testo"
  *     di Windows che aggiunge spazi oltre la 89 colonna
  *     Inoltre in Windows sostituisce il carattere 196 con un '-'
  if y=-1
    y = prow()
    x = pcol()
  endif
  @ y,x say s
  return

PROCEDURE CPLU_GO
parameter i_recpos

if i_recpos<=0 .or. i_recpos>reccount()
  if reccount()<>0
    goto bottom
    if .not. eof()
      skip
    endif
  endif  
else
  goto i_recpos
endif
return

* --- Area Manuale = Functions & Procedures 
* --- TX_DDTZU

FUNCTION CTRL_A
Private w_RET
w_RET=.f.

if empty(mvcodcla) or (mvcodcla<>nvl(tdstacl1,' ') and mvcodcla<>nvl(tdstacl2,' ');
   and mvcodcla<>nvl(tdstacl3,' ') and mvcodcla<>nvl(tdstacl4,' ') and ;
   mvcodcla<>nvl(tdstacl5,' '))
        w_RET=.T.
endif




Return (w_RET)
endfunc

FUNCTION CTRL_D
Private w_RET
w_RET=.f.

if !empty(mvdesart) and ((empty(mvcodcla) or (mvcodcla<>nvl(tdstacl1,' ') and ;
mvcodcla<>nvl(tdstacl2,' ') and mvcodcla<>nvl(tdstacl3,' ') ;
and mvcodcla<>nvl(tdstacl4,' ') and mvcodcla<>nvl(tdstacl5,' '))))
   w_RET=.T.
endif
Return (w_RET)
endfunc

FUNCTION CTRL1
Private w_RET
w_RET=.f.

if ((mvrifkit=0 or (mvrifkit<>0 and arflstco='S')) and empty(mvcodcla) or ;
   (mvcodcla<>nvl(tdstacl1,' ') and mvcodcla<>nvl(tdstacl2,' ') and ; 
   mvcodcla<>nvl(tdstacl3,' ') and mvcodcla<>nvl(tdstacl4,' ') and ;
   mvcodcla<>nvl(tdstacl5,' '))) 
       w_RET=.t.
endif
Return (w_RET)
endfunc

FUNCTION CTRL2
Private w_RET
w_RET=.f.

if (mvrifkit=0 or (mvrifkit<>0 and arflstco='S')) and not empty(mvscont3) or not empty(mvscont4);
   and (empty(mvcodcla) or (mvcodcla<>nvl(tdstacl1,' ');
   and mvcodcla<>nvl(tdstacl2,' ') and mvcodcla<>nvl(tdstacl3,' ');
   and mvcodcla<>nvl(tdstacl4,' ') and mvcodcla<>nvl(tdstacl5,' ')))
       w_RET=.t.
endif
Return (w_RET)
endfunc


FUNCTION CTRL4
Private w_RET
w_RET=.f.

if (mvrifkit=0 or (mvrifkit<>0 and arflstco='S')) and not empty(mvscont2);
   and (empty(mvcodcla) or (mvcodcla<>nvl(tdstacl1,' ');
   and mvcodcla<>nvl(tdstacl2,' ') and mvcodcla<>nvl(tdstacl3,' ');
   and mvcodcla<>nvl(tdstacl4,' ') and mvcodcla<>nvl(tdstacl5,' ')))
       w_RET=.t.
endif
Return (w_RET)
endfunc

FUNCTION CTRL3
Private w_RET
w_RET=.f.

if ((mvrifkit=0 or (mvrifkit<>0 and arflstco='S')) and not empty(mvscont1);
    and (empty(mvcodcla) or (mvcodcla<>nvl(tdstacl1,' ');
    and mvcodcla<>nvl(tdstacl2,' ') and mvcodcla<>nvl(tdstacl3,' ');
    and mvcodcla<>nvl(tdstacl4,' ') and mvcodcla<>nvl(tdstacl5,' '))))
       w_RET=.t.
endif
Return (w_RET)
endfunc


Function FValrig
private w_VALRIG 
w_VALRIG=iif(mvrifkit<>0,'',iif(mvflomag='X',TRAN(mvvalrig,'@Z '+V_PV[18*(VADECTOT+2)]),;
                  iif(mvflomag='S','Sconto Merce',iif(mvflomag='I','Omaggio Imponibile',;
                  iif(mvflomag='E','Omaggio Imp.+IVA','')))))
Return(w_VALRIG)
endfunc

Function FIva
private w_IVA
w_IVA=iif(mvrifkit<>0 or mvtiprig='D','',iif(empty(nvl(mvcodive,' '));
            or (nvl(ivperiva,0)=0 and nvl(looktab('nazioni','nacodiso','nacodnaz',annazion),'')='IT'),;
            mvcodiva,mvcodive))
Return (w_IVA)
endfunc

Function Fscrdessup
Private w_RET
w_RET=.f.
if 'S'=nvl(looktab('ART_ICOL','ARSTASUP','ARCODART',MVCODART),'')
    w_RET=.t.
endif
Return (w_RET)
endfunc

Function Fdatitest1
private w_DATITEST1
w_DATITEST1=alltrim(an___cap)+iif(empty(anlocali),'',' '+alltrim(anlocali ))+;
                      iif(empty(alltrim(anprovin)),'',+' ( ' +anprovin+' ) ');
                      +iif(empty(alltrim(nvl(annazion,''))),'',+' ( ' +nvl(annazion,'')+' ) ')
Return (w_DATITEST1)
endfunc

Function Fdatitest
private w_DATITEST
w_DATITEST=alltrim(dd___cap)+iif(empty(ddlocali),'',' '+alltrim(ddlocali ));
                      +iif(empty(alltrim(ddprovin)),'',+' ( ' +ddprovin+' ) ');
                       +iif(empty(alltrim(nvl(ddcodnaz,''))),'',+' ( ' +nvl(ddcodnaz,'')+' ) ')
return(w_DATITEST)
endfunc


Function FTxt1
Private w_TXT1
w_TXT1='Dichiarazione di intento n� ' + alltrim(str(L_NDIC));
                + ' del ' +dtoc(L_DDIC)+' Art. Es. '+alltrim(L_ARTESE);
                +' del D.P.R. 633/72'+iif(L_TIPOPER='D','  Valida dal '+DTOC(L_DATINI);
                + ' al '+DTOC(L_DATFIN),IIF(L_TIPOPER='I',' Limite es: '+alltrim(IMP),''))
Return (w_TXT1)
Endfunc

Function FImposta
Private imposta
imposta=iif(mvaflom1$'XI',mvaimps1,0)+iif(mvaflom2$'XI',mvaimps2,0);
               +iif(mvaflom3$'XI',mvaimps3,0)+iif(mvaflom4$'XI',mvaimps4,0);
                +iif(mvaflom5$'XI',mvaimps5,0)+iif(mvaflom6$'XI',mvaimps6,0)
Return(imposta)
Endfunc

FUNCTIO FVETDAT1
private w_VETDAT1
w_VETDAT1=alltrim(NVL(looktab('vettori','vtindvet','vtcodvet',mvcodvet),''));
                     +'   '+alltrim(NVL(looktab('vettori','vtvetcap','vtcodvet',mvcodvet),''));
                     +'   '+alltrim(NVL(looktab('vettori','vtlocvet','vtcodvet',mvcodvet),''))+'   '
RETURN (w_VETDAT1)
ENDFUNC

FUNCTION FVETDAT2
private w_VETDAT2
w_VETDAT2=alltrim(NVL(looktab('vettori','vtindvet','vtcodvet',mvcodve2),''));
                     +'   '+alltrim(NVL(looktab('vettori','vtvetcap','vtcodvet',mvcodve2),''));
                     +'   '+alltrim(NVL(looktab('vettori','vtlocvet','vtcodvet',mvcodve2),''))+'   '
RETURN(w_VETDAT2)
ENDFUNC

FUNCTION FVETDAT3
private w_VETDAT3
w_VETDAT3=alltrim(NVL(looktab('vettori','vtindvet','vtcodvet',mvcodve3),''));
                     +'   '+alltrim(NVL(looktab('vettori','vtvetcap','vtcodvet',mvcodve3),''));
                     +'   '+alltrim(NVL(looktab('vettori','vtlocvet','vtcodvet',mvcodve3),''))+'   '
RETURN(w_VETDAT3)
ENDFUNC
FUNCTION FIMPIVA
PRIVATE w_IMPIVA
w_IMPIVA=iif(mvaflom1='X',mvaimpn1,0)+iif(mvaflom2='X',mvaimpn2,0);
                 +iif(mvaflom3='X',mvaimpn3,0)+iif(mvaflom4='X',mvaimpn4,0);
                 +iif(mvaflom5='X',mvaimpn5,0)+iif(mvaflom6='X',mvaimpn6,0)
RETURN(w_IMPIVA)
ENDFUNC

FUNCTION FCODART
Private w_RET
w_RET=.f.

if (not empty(mvqtamov) or not empty(mvprezzo) or empty(mvcodcla)) or (mvcodcla<>nvl(tdstacl1,' ') and mvcodcla<>nvl(tdstacl2,' ');
   and mvcodcla<>nvl(tdstacl3,' ') and mvcodcla<>nvl(tdstacl4,' ');
   and mvcodcla<>nvl(tdstacl5,' '))
       w_RET=.t.
endif
Return (w_RET)
endfunc
* ----------------------------------------------------------------------
* Funzione : Pari_A
* ----------------------------------------------------------------------
*  Esegue traduzione importo documento in EURO o LIRE
* ----------------------------------------------------------------------
Function Pari_a
parameter Valuta
private w_ImpRet,w_TotPar
do case
   case MVCODVAL=g_CODEUR
        w_TotPar =  ROUND(( TOTALEA*p_Cambio),0)
        w_ImpRet = TRAN(w_TotPar,'@Z '+V_PV[(40)])
   case MVCODVAL=g_CODLIR
         w_TotPar =  ROUND((TOTALEA/p_Cambio),2)
         w_ImpRet = TRAN(w_TotPar,'@Z '+V_PV[(80)])
   otherwise
   if mvdatdoc>=g_dateur and vacodval<>g_codeur
        w_TotPar = ROUND((TOTALEA/mvcaoval),2)
        w_ImpRet = TRAN(w_TotPar,'@Z '+V_PV[(80)])
   else
        w_TotPar = ROUND((TOTALEA*p_cambio),0)
        w_ImpRet = TRAN(w_TotPar,'@Z '+V_PV[(80)])
   endif
endcase

return (w_ImpRet)

FUNCTION CTRL_COAC
Private w_RET
w_RET=.t.
* INDICA SE STAMPARE LA RIGA DI CONTRIBUTO ACCESSORIO
if G_COAC = "S"
  if VARTYPE(MVCACONT) = "C"
    if NVL( LOOKTAB( "TIP_DOCU" , "TDNOSTCO"  , "TDTIPDOC" , TDTIPDOC ) , "N" ) = "S" and !EMPTY(MVCACONT)
      * se il flag sulla causale � attivo e siamo su una riga di contributo accessorio,
      * la riga non viene stampata
      w_RET=.f.
    else
      w_RET=.t.
    endif
  else
    w_RET=.t.
  endif
else
  w_RET=.t.
endif

Return (w_RET)
endfunc

* --- Fine Area Manuale 
