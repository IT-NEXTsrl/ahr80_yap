* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bcc                                                        *
*              Crea i codici di ricerca artic                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_112]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-15                                                      *
* Last revis.: 2018-07-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bcc",oParentObject,m.pOper)
return(i_retval)

define class tgsma_bcc as StdBatch
  * --- Local variables
  pOper = space(1)
  w_TIPART = space(2)
  w_AUTOBAR = .f.
  w_TROV = .f.
  w_CODBAR = space(10)
  w___TIPO = space(1)
  w_KEYBAR = space(20)
  w_CODKEY = space(20)
  w_MESS = space(10)
  w_APPO = space(2)
  w_DESART = space(40)
  w_OK = .f.
  w_DESSUP = space(10)
  w_DTINVA = ctod("  /  /  ")
  w_DTOBSO = ctod("  /  /  ")
  w_OKINS = .f.
  w_ERROR = space(100)
  w_MAXCOD = 0
  w_TmpN = 0
  w_OKBAR = .f.
  w_LENSCF = 0
  w_CODTIP = space(35)
  w_CODVAL = space(35)
  w_CODCLF = space(5)
  w_FLGESC = space(1)
  w_APPCODKEY = space(20)
  w_KEYTEST = space(41)
  * --- WorkFile variables
  KEY_ARTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Crea/Aggiorna le Chiavi di Ricerca (Lanciato da GSMA_KCA Record Inserted
    if this.pOper="C"
      * --- Calcola il Codice
      if this.oParentObject.w_TIPBAR $ "12"
        * --- Aggiunge il Barcode Calcolo l'AutoNumber
        * --- L'autonumber viene assegnato alla tabella KEY_ARTI
        * --- Try
        local bErr_0503B450
        bErr_0503B450=bTrsErr
        this.Try_0503B450()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
        endif
        bTrsErr=bTrsErr or bErr_0503B450
        * --- End
      endif
      i_retcode = 'stop'
      return
    endif
    this.w_MESS = ""
    this.w_OK = .T.
    * --- Inizializza Campi da Aggiornare in KEY_ARTI
    this.w_KEYBAR = this.oParentObject.w_BARKEY
    this.w_DESART = this.oParentObject.w_DESCOD
    this.w___TIPO = IIF(this.oParentObject.w_TIPCON="I", "F", IIF(this.oParentObject.w_TIPCON $ "CF", "R", this.oParentObject.w_TIPCON))
    this.oParentObject.w_CODCON = IIF(this.oParentObject.w_TIPCON $ "CF", this.oParentObject.w_CODCON, SPACE(15))
    WITH this.oParentObject.oParentObject
    this.w_CODKEY = .w_ARCODART
    this.w_DESSUP = .w_ARDESSUP
    this.w_TIPART = .w_ARTIPART
    this.w_DTINVA = .w_ARDTINVA
    this.w_DTOBSO = .w_ARDTOBSO
    this.w_CODTIP = .w_ARCODTIP
    this.w_CODVAL = .w_ARCODVAL
    this.w_CODCLF = .w_ARCODCLF
    this.w_FLGESC = .w_ARFLGESC
    ENDWITH
    this.w_TROV = .T.
    this.w_AUTOBAR = .F.
    if NOT CHKTIPO(this.w_TIPART, this.oParentObject.w_TIPCON)
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_TIPCON $ "CF" AND EMPTY(this.oParentObject.w_CODCON)
      if this.oParentObject.w_TIPCON="C"
        ah_ErrorMsg("Nessun cliente di riferimento selezionato")
      else
        ah_ErrorMsg("Nessun fornitore di riferimento selezionato")
      endif
      i_retcode = 'stop'
      return
    endif
    * --- Inserisco/Aggiorno l'eventale Barcode Associato
    * --- Per ora Gestiti i soli Codici EAN8 / EAN13
    if EMPTY(this.w_CODKEY) OR (EMPTY(this.oParentObject.w_BARKEY) AND NOT this.oParentObject.w_TIPBAR $ "12")
      if IsAlt()
        ah_ErrorMsg("Non � stato inserito il nuovo codice di ricerca da creare")
      else
        ah_ErrorMsg("Nessun articolo/codice selezionato")
      endif
    else
      * --- Verifica se esiste gia' il Barcode
      * --- Read from KEY_ARTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CACODICE"+;
          " from "+i_cTable+" KEY_ARTI where ";
              +"CACODART = "+cp_ToStrODBC(this.w_CODKEY);
              +" and CATIPBAR = "+cp_ToStrODBC(this.oParentObject.w_TIPBAR);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CACODICE;
          from (i_cTable) where;
              CACODART = this.w_CODKEY;
              and CATIPBAR = this.oParentObject.w_TIPBAR;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MESS = NVL(cp_ToDate(_read_.CACODICE),cp_NullValue(_read_.CACODICE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Se esiste ..
      if i_Rows>0 And this.oParentObject.w_TIPBAR<>"0"
        if i_Rows=1
          this.w_AUTOBAR = ah_YesNo("Esiste gi� un barcode per l'articolo selezionato (%1)%0Ne vuoi aggiungere un altro?","",ALLTRIM(this.w_MESS))
        else
          this.w_AUTOBAR = ah_YesNo("Esistono gi� %1 barcodes per l'articolo selezionato%0Ne vuoi aggiungere un altro?","", ALLTRIM(STR(i_Rows)) )
        endif
      else
        if this.oParentObject.w_TIPBAR<>"0"
          * --- Se � di tipo Barcode chiedo conferma di inserimento....
          this.w_AUTOBAR = ah_YesNo("Confermi inserimento barcode?")
        else
          * --- ..se non � un barcode (Codifica interna, Cliente, ecc...) inserisco
          this.w_AUTOBAR = .T.
        endif
      endif
      * --- L'operazione avviene fuori transazione e' necessario aprirne una
      this.w_OKBAR = .T.
      if this.w_AUTOBAR
        * --- Try
        local bErr_050559F0
        bErr_050559F0=bTrsErr
        this.Try_050559F0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
        endif
        bTrsErr=bTrsErr or bErr_050559F0
        * --- End
        This.OparentObject.NotifyEvent("Legge")
      endif
    endif
  endproc
  proc Try_0503B450()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_CODBAR = SPACE(10)
    this.oParentObject.w_BARKEY = RIGHT("99"+ALLTRIM(g_PREEAN), 2)
    i_nConn=i_TableProp[this.KEY_ARTI_IDX, 3]
    do case
      case this.oParentObject.w_TIPBAR="1"
        * --- EAN 8
        cp_AskTableProg(this,i_nConn,"COBAR8","i_codazi,w_CODBAR")
        * --- Calcolo la chiave del codice a barre - Aggiungo uno 0 per simulare il CheckSum
        this.oParentObject.w_BARKEY = this.oParentObject.w_BARKEY + RIGHT("00000"+ALLTRIM(this.w_CODBAR), 5) + "0"
      case this.oParentObject.w_TIPBAR="2"
        * --- EAN 13
        cp_AskTableProg(this,i_nConn,"COBAR13","i_codazi,w_CODBAR")
        * --- Calcolo la chiave del codice a barre - Aggiungo uno 0 per simulare il CheckSum
        if this.oParentObject.w_BARKEY="99"
          * --- Se i primi 2 Caratteri sono 99 il Codice e' di tipo Interno, non usa il Cod.Produttore, sfrutta tutto l' Autonumber (10 cifre)
          this.oParentObject.w_BARKEY = this.oParentObject.w_BARKEY + RIGHT("0000000000"+ALLTRIM(this.w_CODBAR), 10) + "0"
        else
          if LEN(ALLTRIM(g_BARPRO))=5
            this.oParentObject.w_BARKEY = this.oParentObject.w_BARKEY + RIGHT("00000"+ALLTRIM(g_BARPRO), 5)
            this.oParentObject.w_BARKEY = this.oParentObject.w_BARKEY + RIGHT("00000"+ALLTRIM(this.w_CODBAR), 5) + "0"
          else
            this.oParentObject.w_BARKEY = this.oParentObject.w_BARKEY + RIGHT("000"+ALLTRIM(g_BARPRO), 7)
            this.oParentObject.w_BARKEY = this.oParentObject.w_BARKEY + RIGHT("000"+ALLTRIM(this.w_CODBAR), 3) + "0"
          endif
        endif
    endcase
    * --- Calcolo il Checksum Corretto
    this.oParentObject.w_BARKEY = CALCBAR(this.oParentObject.w_BARKEY, this.oParentObject.w_TIPBAR, 1)
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_050559F0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Scrittura dei Codici
    if this.oParentObject.w_TIPBAR $ "12" 
      if EMPTY(this.w_KEYBAR)
        * --- Aggiunge il Barcode Calcolo l'AutoNumber
        * --- L'autonumber viene assegnato alla tabella KEY_ARTI
        this.w_OKINS = .T.
        this.w_TmpN = 0
        do while this.w_OKINS
          this.w_CODBAR = SPACE(10)
          this.w_KEYBAR = RIGHT("99"+ALLTRIM(g_PREEAN),2)
          i_nConn=i_TableProp[this.KEY_ARTI_IDX, 3]
          do case
            case this.oParentObject.w_TIPBAR="1"
              * --- EAN 8
              cp_NextTableProg(this,i_nConn,"COBAR8","i_codazi,w_CODBAR")
              * --- Calcolo la chiave del codice a barre - Aggiungo uno 0 per simulare il CheckSum
              this.w_KEYBAR = this.w_KEYBAR+RIGHT("00000"+ALLTRIM(this.w_CODBAR),5)+"0"
              this.w_MAXCOD = 99999
            case this.oParentObject.w_TIPBAR="2"
              * --- EAN 13
              cp_NextTableProg(this,i_nConn,"COBAR13","i_codazi,w_CODBAR")
              * --- Calcolo la chiave del codice a barre - Aggiungo uno 0 per simulare il CheckSum
              if this.w_KEYBAR="99"
                * --- Se i primi 2 Caratteri sono 99 il Codice e' di tipo Interno, non usa il Cod.Produttore, sfrutta tutto l' Autonumber (10 cifre)
                this.w_KEYBAR = this.w_KEYBAR+RIGHT("0000000000"+ALLTRIM(this.w_CODBAR),10)+"0"
                this.w_MAXCOD = 99999999
              else
                if LEN(ALLTRIM(g_BARPRO))=5
                  this.w_KEYBAR = this.w_KEYBAR+RIGHT("00000"+ALLTRIM(g_BARPRO),5)
                  this.w_KEYBAR = this.w_KEYBAR+RIGHT("00000"+ALLTRIM(this.w_CODBAR),5)+"0"
                  this.w_MAXCOD = 99999
                else
                  this.w_KEYBAR = this.w_KEYBAR+RIGHT("000"+ALLTRIM(g_BARPRO),7)
                  this.w_KEYBAR = this.w_KEYBAR+RIGHT("000"+ALLTRIM(this.w_CODBAR),3)+"0"
                  this.w_MAXCOD = 999
                endif
              endif
          endcase
          * --- Read from KEY_ARTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CACODART"+;
              " from "+i_cTable+" KEY_ARTI where ";
                  +"CACODICE = "+cp_ToStrODBC(this.w_KEYBAR);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CACODART;
              from (i_cTable) where;
                  CACODICE = this.w_KEYBAR;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_MESS = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Test Inserimento
          if i_Rows<>0
            this.w_TmpN = this.w_TmpN+1
            this.w_OKINS = .T.
          else
            this.w_OKINS = .F.
          endif
          if this.w_TmpN>this.w_MAXCOD
            this.w_OKINS = .F.
            * --- Errore generazione Codice a Barre - utilizzo una variaible per gestire la messagistica, questo errore dovrebbe essere
            * --- poco frequente, succede solo se utilizzo tutti i codici a barre disponibili
            this.w_ERROR = "Errore non generato codice per %1"
            this.w_APPCODKEY = this.w_CODKEY
            this.w_OKBAR = .F.
          endif
        enddo
        * --- Calcolo il Checksum Corretto
        this.w_KEYBAR = CALCBAR(this.w_KEYBAR, this.oParentObject.w_TIPBAR, 1)
      endif
    endif
    if CALCBAR(this.w_KEYBAR, this.oParentObject.w_TIPBAR, 0) AND this.w_OKBAR
      * --- Calcola il codice di Ricerca comprensivo del Suffisso solo se No Barcode
      *     e lunghezza del codice Stesso
      this.w_LENSCF = 0
      if Not Empty(this.oParentObject.w_CODESC) And g_FLCESC="S" And this.oParentObject.w_TIPBAR="0"
        this.w_LENSCF = Len(Alltrim(this.oParentObject.w_CODESC))
        this.w_KEYTEST = Alltrim(this.w_KEYBAR)+Alltrim(this.oParentObject.w_CODESC)
        if Len(this.w_KEYTEST)>20
          this.w_KEYBAR = Left(Alltrim(this.w_KEYBAR),Len(Alltrim(this.w_KEYBAR))-this.w_LENSCF)+Alltrim(this.oParentObject.w_CODESC)
          if Not ah_YesNo("Lunghezza codice comprensivo di suffisso superiore a 20 caratteri%0Si desidera adeguare il codice in automatico?%0Nuovo codice: %1","",Alltrim(this.w_KEYBAR) )
            this.w_OKBAR = .F.
          endif
        else
          this.w_KEYBAR = this.w_KEYTEST
        endif
      endif
      if this.w_OKBAR
        * --- Verifica Checksum
        * --- Verifica se esiste gia' il Barcode
        * --- Read from KEY_ARTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CACODART"+;
            " from "+i_cTable+" KEY_ARTI where ";
                +"CACODICE = "+cp_ToStrODBC(this.w_KEYBAR);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CACODART;
            from (i_cTable) where;
                CACODICE = this.w_KEYBAR;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MESS = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_Rows=0
          ah_Msg("Scrittura barcode...%1",.T.,.F.,.F.,this.w_KEYBAR)
          if NOT CALCBAR(this.w_KEYBAR, this.oParentObject.w_TIPBAR, 2) AND NOT this.oParentObject.w_TIPBAR $ "AB" 
            this.w_MESS = "Attenzione: carattere checksum errato, si desidera correggerlo automaticamente?"
            if ah_YesNo(this.w_MESS)
              this.w_KEYBAR = CALCBAR(this.w_KEYBAR, this.oParentObject.w_TIPBAR, 1)
            else
              ah_ErrorMsg("Attenzione: carattere checksum errato creato barcode generico")
              this.oParentObject.w_TIPBAR = "0"
            endif
          endif
          if this.w_OKBAR
            * --- Insert into KEY_ARTI
            i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.KEY_ARTI_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"CACODICE"+",CADESART"+",CADESSUP"+",CACODART"+",CATIPCON"+",CA__TIPO"+",CATIPBAR"+",CAFLSTAM"+",CAOPERAT"+",CAMOLTIP"+",UTCC"+",UTCV"+",UTDC"+",UTDV"+",CADTOBSO"+",CACODCON"+",CADTINVA"+",CALENSCF"+",CAFLIMBA"+",CATIPCO3"+",CACODTIP"+",CACODVAL"+",CACODCLF"+",CAFLGESC"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_KEYBAR),'KEY_ARTI','CACODICE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_DESART),'KEY_ARTI','CADESART');
              +","+cp_NullLink(cp_ToStrODBC(this.w_DESSUP),'KEY_ARTI','CADESSUP');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CODKEY),'KEY_ARTI','CACODART');
              +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TIPCON),'KEY_ARTI','CATIPCON');
              +","+cp_NullLink(cp_ToStrODBC(this.w___TIPO),'KEY_ARTI','CA__TIPO');
              +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TIPBAR),'KEY_ARTI','CATIPBAR');
              +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FLSTAM),'KEY_ARTI','CAFLSTAM');
              +","+cp_NullLink(cp_ToStrODBC("*"),'KEY_ARTI','CAOPERAT');
              +","+cp_NullLink(cp_ToStrODBC(1),'KEY_ARTI','CAMOLTIP');
              +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'KEY_ARTI','UTCC');
              +","+cp_NullLink(cp_ToStrODBC(0),'KEY_ARTI','UTCV');
              +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'KEY_ARTI','UTDC');
              +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'KEY_ARTI','UTDV');
              +","+cp_NullLink(cp_ToStrODBC(this.w_DTOBSO),'KEY_ARTI','CADTOBSO');
              +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODCON),'KEY_ARTI','CACODCON');
              +","+cp_NullLink(cp_ToStrODBC(this.w_DTINVA),'KEY_ARTI','CADTINVA');
              +","+cp_NullLink(cp_ToStrODBC(this.w_LENSCF),'KEY_ARTI','CALENSCF');
              +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CAFLIMBA),'KEY_ARTI','CAFLIMBA');
              +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CATIPCO3),'KEY_ARTI','CATIPCO3');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CODTIP),'KEY_ARTI','CACODTIP');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CODVAL),'KEY_ARTI','CACODVAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CODCLF),'KEY_ARTI','CACODCLF');
              +","+cp_NullLink(cp_ToStrODBC(this.w_FLGESC),'KEY_ARTI','CAFLGESC');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'CACODICE',this.w_KEYBAR,'CADESART',this.w_DESART,'CADESSUP',this.w_DESSUP,'CACODART',this.w_CODKEY,'CATIPCON',this.oParentObject.w_TIPCON,'CA__TIPO',this.w___TIPO,'CATIPBAR',this.oParentObject.w_TIPBAR,'CAFLSTAM',this.oParentObject.w_FLSTAM,'CAOPERAT',"*",'CAMOLTIP',1,'UTCC',i_CODUTE,'UTCV',0)
              insert into (i_cTable) (CACODICE,CADESART,CADESSUP,CACODART,CATIPCON,CA__TIPO,CATIPBAR,CAFLSTAM,CAOPERAT,CAMOLTIP,UTCC,UTCV,UTDC,UTDV,CADTOBSO,CACODCON,CADTINVA,CALENSCF,CAFLIMBA,CATIPCO3,CACODTIP,CACODVAL,CACODCLF,CAFLGESC &i_ccchkf. );
                 values (;
                   this.w_KEYBAR;
                   ,this.w_DESART;
                   ,this.w_DESSUP;
                   ,this.w_CODKEY;
                   ,this.oParentObject.w_TIPCON;
                   ,this.w___TIPO;
                   ,this.oParentObject.w_TIPBAR;
                   ,this.oParentObject.w_FLSTAM;
                   ,"*";
                   ,1;
                   ,i_CODUTE;
                   ,0;
                   ,SetInfoDate( g_CALUTD );
                   ,cp_CharToDate("  -  -    ");
                   ,this.w_DTOBSO;
                   ,this.oParentObject.w_CODCON;
                   ,this.w_DTINVA;
                   ,this.w_LENSCF;
                   ,this.oParentObject.w_CAFLIMBA;
                   ,this.oParentObject.w_CATIPCO3;
                   ,this.w_CODTIP;
                   ,this.w_CODVAL;
                   ,this.w_CODCLF;
                   ,this.w_FLGESC;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error='Errore in inserimento nuovo codice'
              return
            endif
            this.oParentObject.w_BARKEY = SPACE(20)
            * --- Aggiorna eventuale Autonumber
            if this.oParentObject.w_AGGPROG="S"
              * --- Se impostato flag specifico aggiorno progressivo Barcode
              do case
                case this.oParentObject.w_TIPBAR="1"
                  * --- EAN 8
                  this.w_CODBAR = SUBSTR(this.w_KEYBAR, 3, 5)
                  cp_NextTableProg(this,i_nConn,"COBAR8","i_codazi,w_CODBAR")
                case this.oParentObject.w_TIPBAR="2"
                  * --- EAN 13
                  this.w_CODBAR = SUBSTR(this.w_KEYBAR, 3, 10)
                  cp_NextTableProg(this,i_nConn,"COBAR13","i_codazi,w_CODBAR")
              endcase
            endif
          endif
        else
          ah_ErrorMsg("Esiste gi� un codice di ricerca associato al barcode che si vuole inserire (%1)%0Selezionare un codice differente",,"",ALLTRIM(this.w_MESS))
          this.oParentObject.w_BARKEY = this.w_KEYBAR
        endif
      endif
    else
      this.oParentObject.w_BARKEY = this.w_KEYBAR
      if Not Empty ( this.w_ERROR ) AND Not this.w_OKBAR
        ah_ErrorMsg(this.w_ERROR,"!","",this.w_APPCODKEY)
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='KEY_ARTI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
