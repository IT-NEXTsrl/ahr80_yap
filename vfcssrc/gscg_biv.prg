* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_biv                                                        *
*              Importi IVA in altra valuta                                     *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][207]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-07                                                      *
* Last revis.: 2008-01-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_biv",oParentObject,m.w_pTipo)
return(i_retval)

define class tgscg_biv as StdBatch
  * --- Local variables
  w_pTipo = space(10)
  w_CONFERMA = .f.
  w_VALNAZ = space(3)
  w_DEC = 0
  w_CAONAZ = 0
  w_DECTOP = 0
  w_CODVAL = space(3)
  w_IVAPER = 0
  w_CAOVAL = 0
  w_DECTOT = 0
  w_DATRIF = ctod("  /  /  ")
  w_PADRE = .NULL.
  w_NONNO = .NULL.
  w_TIPREG = space(1)
  w_IMPIVA = 0
  w_IMPONI = 0
  w_CODIVA = space(5)
  w_IMPOVAL = 0
  w_IMPORTO = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Richiamato da zoom IVIMPONI,PNIMPDAR,PNIMPAVE
    *     Apre la mascehra di calcolo conversioni se valuta
    *     di conto diversa da valuta registrazione prima nota.
    *     Parametro w_pTipo
    *     IMPONI - Da IVIMPONI in GSCG_MIV
    *     DARE - Da PNIMPDAR in GSCG_MPN
    *     AVERE- Da PNIMPAVE in GSCG_MPN
    This.bUpdateParentObject=.f.
    this.w_PADRE = This.oParentObject
    if this.w_pTipo="IMPONI"
      this.w_NONNO = this.w_PADRE.oParentObject
    else
      this.w_NONNO = this.w_PADRE
    endif
    * --- Non rilancio la mcalc all'uscita
    if this.w_NONNO.w_PNCODVAL<>g_PERVAL
      * --- Prende i Decimali della Valuta Nazionale riferita alla Registrazione
      *     Utilizzati per calcolare in automatico l'Imposta dallImponibile e per tradurre gli importi
      this.w_DECTOP = this.w_NONNO.w_DECTOP
      this.w_DECTOT = this.w_NONNO.w_DECTOT
      this.w_VALNAZ = this.w_NONNO.w_PNVALNAZ
      this.w_CAONAZ = this.w_NONNO.w_CAONAZ
      this.w_CODVAL = this.w_NONNO.w_PNCODVAL
      this.w_CAOVAL = this.w_NONNO.w_PNCAOVAL
      this.w_DATRIF = IIF(EMPTY(this.w_NONNO.w_PNDATDOC), this.w_NONNO.w_PNDATREG, this.w_NONNO.w_PNDATDOC)
      * --- Decimali Riferiti alla Moneta di Conto o alla Valuta Documento
      this.w_DEC = IIF(this.w_VALNAZ=this.w_CODVAL, this.w_DECTOP, this.w_DECTOT)
      this.w_CONFERMA = .F.
      do case
        case this.w_pTipo="IMPONI"
          this.w_CODIVA = this.oParentObject.w_IVCODIVA
          this.w_TIPREG = this.oParentObject.w_IVTIPREG
          this.w_IVAPER = this.oParentObject.w_PERIVA
          do GSCG_SIV with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.w_CONFERMA
            * --- Traduco e Calcolo Imponibile e Imposta e li assegno alla movimentazione
            *     Aggiorno le proprietą di GSCG_MIV (dopo aggiorno i Contol - Qui le Picture non modificano gli importi)
            this.oParentObject.w_IVIMPONI = VAL2MON(this.w_IMPONI, this.w_CAOVAL, this.w_CAONAZ, this.w_DATRIF, this.w_VALNAZ, this.w_DECTOP)
            * --- Il totale imponibile si aggiorna da se, mentre il totale imposta (non essendo
            *     l'oggetto dello zoom) va aggiornato a mano.
            this.oParentObject.w_TOTIVA = this.oParentObject.w_TOTIVA - this.oParentObject.w_IVIMPIVA
            this.oParentObject.w_IVIMPIVA = VAL2MON(this.w_IMPIVA, this.w_CAOVAL, this.w_CAONAZ, this.w_DATRIF, this.w_VALNAZ, this.w_DECTOP)
            this.oParentObject.w_TOTIVA = this.oParentObject.w_TOTIVA + this.oParentObject.w_IVIMPIVA
            this.w_PADRE.SaveRow()     
          endif
        case this.w_pTipo="DARE" or this.w_pTipo="AVERE"
          do GSCG_SPV with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.w_CONFERMA
            if this.w_pTipo="DARE"
              * --- Traduco e Calcolo Imponibile e Imposta e li assegno alla movimentazione
              *     Aggiorno le proprietą di GSCG_MIV (dopo aggiorno i Contol - Qui le Picture non modificano gli importi)
              this.oParentObject.w_PNIMPDAR = this.w_IMPORTO
              this.oParentObject.w_TOTAVE = this.oParentObject.w_TOTAVE - this.oParentObject.w_PNIMPAVE
              this.oParentObject.w_PNIMPAVE = 0
            else
              this.oParentObject.w_PNIMPAVE = this.w_IMPORTO
              this.oParentObject.w_TOTDAR = this.oParentObject.w_TOTDAR - this.oParentObject.w_PNIMPDAR
              this.oParentObject.w_PNIMPDAR = 0
            endif
            this.w_PADRE.SaveRow()     
          endif
      endcase
    else
      ah_errormsg( "Funzionalitą non disponibile%0Valuta di conto uguale a valuta registrazione")
    endif
  endproc


  proc Init(oParentObject,w_pTipo)
    this.w_pTipo=w_pTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_pTipo"
endproc
