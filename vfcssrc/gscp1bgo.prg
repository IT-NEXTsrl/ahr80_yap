* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscp1bgo                                                        *
*              generazione ordine da WEB                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2016-01-13                                                      *
* Last revis.: 2016-03-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pDOSERIAL
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
*createobject("tgscp1bgo",oParentObject,m.pDOSERIAL)
return(i_retval)

define class tgscp1bgo as StdBatch
  * --- Local variables
  pDOSERIAL = space(10)
  w_Old_Sched = space(1)
  w_PROG = .NULL.
  * --- WorkFile variables
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eseguito da
    if VARTYPE(g_SCHEDULER)<>"C" 
       Public g_SCHEDULER 
 g_SCHEDULER=" "
    endif
    this.w_Old_Sched = g_SCHEDULER
    g_msg=" " 
 g_SCHEDULER="S"
    this.w_PROG = GSCP_KGO()
    if this.w_PROG.bSec1
      this.w_PROG.w_DOSERIAL = this.pDOSERIAL
      this.w_PROG.Notifyevent("Interroga")     
      this.w_PROG.w_LIVELLO = 8
      this.w_PROG.Notifyevent("w_LIVELLO Changed")     
      this.w_PROG.Notifyevent("Selezionatutto")     
      this.w_PROG.Notifyevent("elabora")     
      this.w_PROG.ecpQuit()     
      g_SCHEDULER=this.w_Old_Sched
    else
      g_SCHEDULER=this.w_Old_Sched
      Ah_ErrorMsg("Impossibile accedere alla contabilizzazione documenti!",48,"")
    endif
    this.w_PROG = Null
  endproc


  proc Init(oParentObject,pDOSERIAL)
    this.pDOSERIAL=pDOSERIAL
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pDOSERIAL"
endproc
