* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bdt                                                        *
*              Aggiorno descrizioni primanota                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-03-26                                                      *
* Last revis.: 2012-03-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bdt",oParentObject)
return(i_retval)

define class tgste_bdt as StdBatch
  * --- Local variables
  w_PNAGG_01 = space(15)
  w_PNAGG_02 = space(15)
  w_PNAGG_03 = space(15)
  w_PNAGG_04 = space(15)
  w_PNAGG_05 = ctod("  /  /  ")
  w_PNAGG_06 = ctod("  /  /  ")
  w_DACAM_01 = space(30)
  w_DACAM_02 = space(30)
  w_DACAM_03 = space(30)
  w_DACAM_04 = space(30)
  w_DACAM_05 = space(30)
  w_DACAM_06 = space(30)
  w_PADRE = .NULL.
  * --- WorkFile variables
  PNT_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorno descrizioni primanota
    this.w_PADRE = this.oparentobject
    this.w_PNAGG_01 = this.w_PADRE.w_PNAGG_01
    this.w_PNAGG_02 = this.w_PADRE.w_PNAGG_02
    this.w_PNAGG_03 = this.w_PADRE.w_PNAGG_03
    this.w_PNAGG_04 = this.w_PADRE.w_PNAGG_04
    this.w_PNAGG_05 = this.w_PADRE.w_PNAGG_05
    this.w_PNAGG_06 = this.w_PADRE.w_PNAGG_06
    this.w_DACAM_01 = this.w_PADRE.w_DACAM_01
    this.w_DACAM_02 = this.w_PADRE.w_DACAM_02
    this.w_DACAM_03 = this.w_PADRE.w_DACAM_03
    this.w_DACAM_04 = this.w_PADRE.w_DACAM_04
    this.w_DACAM_05 = this.w_PADRE.w_DACAM_05
    this.w_DACAM_06 = this.w_PADRE.w_DACAM_06
    do GSTE_KDT with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_PADRE.w_PNAGG_01 = this.w_PNAGG_01
    this.w_PADRE.w_PNAGG_02 = this.w_PNAGG_02
    this.w_PADRE.w_PNAGG_03 = this.w_PNAGG_03
    this.w_PADRE.w_PNAGG_04 = this.w_PNAGG_04
    this.w_PADRE.w_PNAGG_05 = this.w_PNAGG_05
    this.w_PADRE.w_PNAGG_06 = this.w_PNAGG_06
    * --- Try
    local bErr_03D98FD0
    bErr_03D98FD0=bTrsErr
    this.Try_03D98FD0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_03D98FD0
    * --- End
  endproc
  proc Try_03D98FD0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Write into PNT_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PNT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PNAGG_01 ="+cp_NullLink(cp_ToStrODBC(this.w_PNAGG_01),'PNT_MAST','PNAGG_01');
      +",PNAGG_02 ="+cp_NullLink(cp_ToStrODBC(this.w_PNAGG_02),'PNT_MAST','PNAGG_02');
      +",PNAGG_03 ="+cp_NullLink(cp_ToStrODBC(this.w_PNAGG_03),'PNT_MAST','PNAGG_03');
      +",PNAGG_04 ="+cp_NullLink(cp_ToStrODBC(this.w_PNAGG_04),'PNT_MAST','PNAGG_04');
      +",PNAGG_05 ="+cp_NullLink(cp_ToStrODBC(this.w_PNAGG_05),'PNT_MAST','PNAGG_05');
      +",PNAGG_06 ="+cp_NullLink(cp_ToStrODBC(this.w_PNAGG_06),'PNT_MAST','PNAGG_06');
          +i_ccchkf ;
      +" where ";
          +"PNSERIAL = "+cp_ToStrODBC(this.oParentObject.w_PTSERIAL);
             )
    else
      update (i_cTable) set;
          PNAGG_01 = this.w_PNAGG_01;
          ,PNAGG_02 = this.w_PNAGG_02;
          ,PNAGG_03 = this.w_PNAGG_03;
          ,PNAGG_04 = this.w_PNAGG_04;
          ,PNAGG_05 = this.w_PNAGG_05;
          ,PNAGG_06 = this.w_PNAGG_06;
          &i_ccchkf. ;
       where;
          PNSERIAL = this.oParentObject.w_PTSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PNT_MAST'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
