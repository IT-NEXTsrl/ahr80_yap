* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_mta                                                        *
*              Tipologie e classi allegati                                     *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-05-25                                                      *
* Last revis.: 2011-10-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_mta"))

* --- Class definition
define class tgsut_mta as StdTrsForm
  Top    = 12
  Left   = 10

  * --- Standard Properties
  Width  = 724
  Height = 310+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-10-11"
  HelpContextID=80298857
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=12

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  TIP_ALLE_IDX = 0
  CLA_ALLE_IDX = 0
  cFile = "TIP_ALLE"
  cFileDetail = "CLA_ALLE"
  cKeySelect = "TACODICE"
  cKeyWhere  = "TACODICE=this.w_TACODICE"
  cKeyDetail  = "TACODICE=this.w_TACODICE and TACODCLA=this.w_TACODCLA"
  cKeyWhereODBC = '"TACODICE="+cp_ToStrODBC(this.w_TACODICE)';

  cKeyDetailWhereODBC = '"TACODICE="+cp_ToStrODBC(this.w_TACODICE)';
      +'+" and TACODCLA="+cp_ToStrODBC(this.w_TACODCLA)';

  cKeyWhereODBCqualified = '"CLA_ALLE.TACODICE="+cp_ToStrODBC(this.w_TACODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gsut_mta"
  cComment = "Tipologie e classi allegati"
  i_nRowNum = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TACODICE = space(5)
  w_TADESCRI = space(40)
  w_TAPATTIP = space(254)
  o_TAPATTIP = space(254)
  w_TACOPTIP = space(1)
  w_TACODCLA = space(5)
  w_TACLADES = space(40)
  w_TAFLUNIC = space(1)
  w_TAFLDEFA = space(1)
  w_TRSERR = .F.
  w_TDPUBWEB = space(1)
  w_TAPATCLA = space(254)
  o_TAPATCLA = space(254)
  w_TACOPCLA = space(1)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'TIP_ALLE','gsut_mta')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_mtaPag1","gsut_mta",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Tipologie e classi allegati")
      .Pages(1).HelpContextID = 152167159
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTACODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='TIP_ALLE'
    this.cWorkTables[2]='CLA_ALLE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.TIP_ALLE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.TIP_ALLE_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_TACODICE = NVL(TACODICE,space(5))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from TIP_ALLE where TACODICE=KeySet.TACODICE
    *
    i_nConn = i_TableProp[this.TIP_ALLE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_ALLE_IDX,2],this.bLoadRecFilter,this.TIP_ALLE_IDX,"gsut_mta")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('TIP_ALLE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "TIP_ALLE.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"CLA_ALLE.","TIP_ALLE.")
      i_cTable = i_cTable+' TIP_ALLE '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'TACODICE',this.w_TACODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_TRSERR = .F.
        .w_TACODICE = NVL(TACODICE,space(5))
        .w_TADESCRI = NVL(TADESCRI,space(40))
        .w_TAPATTIP = NVL(TAPATTIP,space(254))
        .w_TACOPTIP = NVL(TACOPTIP,space(1))
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'TIP_ALLE')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from CLA_ALLE where TACODICE=KeySet.TACODICE
      *                            and TACODCLA=KeySet.TACODCLA
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.CLA_ALLE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_ALLE_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('CLA_ALLE')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "CLA_ALLE.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" CLA_ALLE"
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'TACODICE',this.w_TACODICE  )
        select * from (i_cTable) CLA_ALLE where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_TACODCLA = NVL(TACODCLA,space(5))
          .w_TACLADES = NVL(TACLADES,space(40))
          .w_TAFLUNIC = NVL(TAFLUNIC,space(1))
          .w_TAFLDEFA = NVL(TAFLDEFA,space(1))
          .w_TDPUBWEB = NVL(TDPUBWEB,space(1))
          .w_TAPATCLA = NVL(TAPATCLA,space(254))
          .w_TACOPCLA = NVL(TACOPCLA,space(1))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace TACODCLA with .w_TACODCLA
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_TACODICE=space(5)
      .w_TADESCRI=space(40)
      .w_TAPATTIP=space(254)
      .w_TACOPTIP=space(1)
      .w_TACODCLA=space(5)
      .w_TACLADES=space(40)
      .w_TAFLUNIC=space(1)
      .w_TAFLDEFA=space(1)
      .w_TRSERR=.f.
      .w_TDPUBWEB=space(1)
      .w_TAPATCLA=space(254)
      .w_TACOPCLA=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        .w_TAPATTIP = addbs(.w_TAPATTIP)
        .w_TACOPTIP = iif(empty(.w_TAPATTIP),'N',.w_TACOPTIP)
        .DoRTCalc(5,8,.f.)
        .w_TRSERR = .F.
        .w_TDPUBWEB = IIF(IsAlt(),'S','N')
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .w_TAPATCLA = addbs(.w_TAPATCLA)
        .w_TACOPCLA = iif(empty(.w_TAPATCLA),'N',.w_TACOPCLA)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'TIP_ALLE')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oTACODICE_1_1.enabled = i_bVal
      .Page1.oPag.oTADESCRI_1_3.enabled = i_bVal
      .Page1.oPag.oTAPATTIP_1_4.enabled = i_bVal
      .Page1.oPag.oTACOPTIP_1_5.enabled = i_bVal
      .Page1.oPag.oTAPATCLA_2_6.enabled = i_bVal
      .Page1.oPag.oTACOPCLA_2_7.enabled = i_bVal
      .Page1.oPag.oObj_1_7.enabled = i_bVal
      .Page1.oPag.oObj_1_8.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oTACODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oTACODICE_1_1.enabled = .t.
        .Page1.oPag.oTADESCRI_1_3.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'TIP_ALLE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.TIP_ALLE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TACODICE,"TACODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TADESCRI,"TADESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TAPATTIP,"TAPATTIP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TACOPTIP,"TACOPTIP",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.TIP_ALLE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_ALLE_IDX,2])
    i_lTable = "TIP_ALLE"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.TIP_ALLE_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_TACODCLA C(5);
      ,t_TACLADES C(40);
      ,t_TAFLUNIC N(3);
      ,t_TAFLDEFA N(3);
      ,t_TDPUBWEB N(3);
      ,t_TAPATCLA C(254);
      ,t_TACOPCLA N(3);
      ,TACODCLA C(5);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsut_mtabodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTACODCLA_2_1.controlsource=this.cTrsName+'.t_TACODCLA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTACLADES_2_2.controlsource=this.cTrsName+'.t_TACLADES'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTAFLUNIC_2_3.controlsource=this.cTrsName+'.t_TAFLUNIC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTAFLDEFA_2_4.controlsource=this.cTrsName+'.t_TAFLDEFA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTDPUBWEB_2_5.controlsource=this.cTrsName+'.t_TDPUBWEB'
    this.oPgFRm.Page1.oPag.oTAPATCLA_2_6.controlsource=this.cTrsName+'.t_TAPATCLA'
    this.oPgFRm.Page1.oPag.oTACOPCLA_2_7.controlsource=this.cTrsName+'.t_TACOPCLA'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(64)
    this.AddVLine(488)
    this.AddVLine(557)
    this.AddVLine(625)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTACODCLA_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.TIP_ALLE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_ALLE_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into TIP_ALLE
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'TIP_ALLE')
        i_extval=cp_InsertValODBCExtFlds(this,'TIP_ALLE')
        local i_cFld
        i_cFld=" "+;
                  "(TACODICE,TADESCRI,TAPATTIP,TACOPTIP"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_TACODICE)+;
                    ","+cp_ToStrODBC(this.w_TADESCRI)+;
                    ","+cp_ToStrODBC(this.w_TAPATTIP)+;
                    ","+cp_ToStrODBC(this.w_TACOPTIP)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'TIP_ALLE')
        i_extval=cp_InsertValVFPExtFlds(this,'TIP_ALLE')
        cp_CheckDeletedKey(i_cTable,0,'TACODICE',this.w_TACODICE)
        INSERT INTO (i_cTable);
              (TACODICE,TADESCRI,TAPATTIP,TACOPTIP &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_TACODICE;
                  ,this.w_TADESCRI;
                  ,this.w_TAPATTIP;
                  ,this.w_TACOPTIP;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CLA_ALLE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_ALLE_IDX,2])
      *
      * insert into CLA_ALLE
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(TACODICE,TACODCLA,TACLADES,TAFLUNIC,TAFLDEFA"+;
                  ",TDPUBWEB,TAPATCLA,TACOPCLA,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_TACODICE)+","+cp_ToStrODBC(this.w_TACODCLA)+","+cp_ToStrODBC(this.w_TACLADES)+","+cp_ToStrODBC(this.w_TAFLUNIC)+","+cp_ToStrODBC(this.w_TAFLDEFA)+;
             ","+cp_ToStrODBC(this.w_TDPUBWEB)+","+cp_ToStrODBC(this.w_TAPATCLA)+","+cp_ToStrODBC(this.w_TACOPCLA)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,0,'TACODICE',this.w_TACODICE,'TACODCLA',this.w_TACODCLA)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_TACODICE,this.w_TACODCLA,this.w_TACLADES,this.w_TAFLUNIC,this.w_TAFLDEFA"+;
                ",this.w_TDPUBWEB,this.w_TAPATCLA,this.w_TACOPCLA,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.TIP_ALLE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_ALLE_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update TIP_ALLE
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'TIP_ALLE')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " TADESCRI="+cp_ToStrODBC(this.w_TADESCRI)+;
             ",TAPATTIP="+cp_ToStrODBC(this.w_TAPATTIP)+;
             ",TACOPTIP="+cp_ToStrODBC(this.w_TACOPTIP)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'TIP_ALLE')
          i_cWhere = cp_PKFox(i_cTable  ,'TACODICE',this.w_TACODICE  )
          UPDATE (i_cTable) SET;
              TADESCRI=this.w_TADESCRI;
             ,TAPATTIP=this.w_TAPATTIP;
             ,TACOPTIP=this.w_TACOPTIP;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_TACODCLA))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.CLA_ALLE_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.CLA_ALLE_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from CLA_ALLE
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and TACODCLA="+cp_ToStrODBC(&i_TN.->TACODCLA)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and TACODCLA=&i_TN.->TACODCLA;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = 0
              replace TACODCLA with this.w_TACODCLA
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update CLA_ALLE
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " TACLADES="+cp_ToStrODBC(this.w_TACLADES)+;
                     ",TAFLUNIC="+cp_ToStrODBC(this.w_TAFLUNIC)+;
                     ",TAFLDEFA="+cp_ToStrODBC(this.w_TAFLDEFA)+;
                     ",TDPUBWEB="+cp_ToStrODBC(this.w_TDPUBWEB)+;
                     ",TAPATCLA="+cp_ToStrODBC(this.w_TAPATCLA)+;
                     ",TACOPCLA="+cp_ToStrODBC(this.w_TACOPCLA)+;
                     " ,TACODCLA="+cp_ToStrODBC(this.w_TACODCLA)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+;
                             " and TACODCLA="+cp_ToStrODBC(TACODCLA)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      TACLADES=this.w_TACLADES;
                     ,TAFLUNIC=this.w_TAFLUNIC;
                     ,TAFLDEFA=this.w_TAFLDEFA;
                     ,TDPUBWEB=this.w_TDPUBWEB;
                     ,TAPATCLA=this.w_TAPATCLA;
                     ,TACOPCLA=this.w_TACOPCLA;
                     ,TACODCLA=this.w_TACODCLA;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere;
                                      and TACODCLA=&i_TN.->TACODCLA;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_TACODCLA))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.CLA_ALLE_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.CLA_ALLE_IDX,2])
        *
        * delete CLA_ALLE
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and TACODCLA="+cp_ToStrODBC(&i_TN.->TACODCLA)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and TACODCLA=&i_TN.->TACODCLA;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.TIP_ALLE_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.TIP_ALLE_IDX,2])
        *
        * delete TIP_ALLE
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_TACODCLA))) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.TIP_ALLE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_ALLE_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_TAPATTIP<>.w_TAPATTIP
          .w_TAPATTIP = addbs(.w_TAPATTIP)
        endif
        if .o_TAPATTIP<>.w_TAPATTIP
          .w_TACOPTIP = iif(empty(.w_TAPATTIP),'N',.w_TACOPTIP)
        endif
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .DoRTCalc(5,10,.t.)
        if .o_TAPATCLA<>.w_TAPATCLA
          .w_TAPATCLA = addbs(.w_TAPATCLA)
        endif
        if .o_TAPATCLA<>.w_TAPATCLA
          .w_TACOPCLA = iif(empty(.w_TAPATCLA),'N',.w_TACOPCLA)
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oTACOPTIP_1_5.enabled = this.oPgFrm.Page1.oPag.oTACOPTIP_1_5.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTDPUBWEB_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTDPUBWEB_2_5.mCond()
    this.oPgFrm.Page1.oPag.oTACOPCLA_2_7.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oTACOPCLA_2_7.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oTAPATTIP_1_4.visible=!this.oPgFrm.Page1.oPag.oTAPATTIP_1_4.mHide()
    this.oPgFrm.Page1.oPag.oTACOPTIP_1_5.visible=!this.oPgFrm.Page1.oPag.oTACOPTIP_1_5.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_9.visible=!this.oPgFrm.Page1.oPag.oStr_1_9.mHide()
    this.oPgFrm.Page1.oPag.oStr_3_1.visible=!this.oPgFrm.Page1.oPag.oStr_3_1.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oTAPATCLA_2_6.visible=!this.oPgFrm.Page1.oPag.oTAPATCLA_2_6.mHide()
    this.oPgFrm.Page1.oPag.oTACOPCLA_2_7.visible=!this.oPgFrm.Page1.oPag.oTACOPCLA_2_7.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_7.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_8.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oTACODICE_1_1.value==this.w_TACODICE)
      this.oPgFrm.Page1.oPag.oTACODICE_1_1.value=this.w_TACODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oTADESCRI_1_3.value==this.w_TADESCRI)
      this.oPgFrm.Page1.oPag.oTADESCRI_1_3.value=this.w_TADESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oTAPATTIP_1_4.value==this.w_TAPATTIP)
      this.oPgFrm.Page1.oPag.oTAPATTIP_1_4.value=this.w_TAPATTIP
    endif
    if not(this.oPgFrm.Page1.oPag.oTACOPTIP_1_5.RadioValue()==this.w_TACOPTIP)
      this.oPgFrm.Page1.oPag.oTACOPTIP_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTAPATCLA_2_6.value==this.w_TAPATCLA)
      this.oPgFrm.Page1.oPag.oTAPATCLA_2_6.value=this.w_TAPATCLA
      replace t_TAPATCLA with this.oPgFrm.Page1.oPag.oTAPATCLA_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTACOPCLA_2_7.RadioValue()==this.w_TACOPCLA)
      this.oPgFrm.Page1.oPag.oTACOPCLA_2_7.SetRadio()
      replace t_TACOPCLA with this.oPgFrm.Page1.oPag.oTACOPCLA_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTACODCLA_2_1.value==this.w_TACODCLA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTACODCLA_2_1.value=this.w_TACODCLA
      replace t_TACODCLA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTACODCLA_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTACLADES_2_2.value==this.w_TACLADES)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTACLADES_2_2.value=this.w_TACLADES
      replace t_TACLADES with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTACLADES_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTAFLUNIC_2_3.RadioValue()==this.w_TAFLUNIC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTAFLUNIC_2_3.SetRadio()
      replace t_TAFLUNIC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTAFLUNIC_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTAFLDEFA_2_4.RadioValue()==this.w_TAFLDEFA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTAFLDEFA_2_4.SetRadio()
      replace t_TAFLDEFA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTAFLDEFA_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTDPUBWEB_2_5.RadioValue()==this.w_TDPUBWEB)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTDPUBWEB_2_5.SetRadio()
      replace t_TDPUBWEB with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTDPUBWEB_2_5.value
    endif
    cp_SetControlsValueExtFlds(this,'TIP_ALLE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(empty(JustDrive(.w_TAPATTIP)) )  and not(!ISALT())
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oTAPATTIP_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non sono amessi percorsi assoluti ")
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsut_mta
      * --- Controllo presenza di pi� Flag Predefiniti
      This.NotifyEvent('Controllo')
      If This.w_TRSERR
         i_bRes = .f.
         i_bnoChk = .f.
      	 i_cErrorMsg = Ah_MsgFormat("Impossibile attivare check predefinito su pi� righe")
      Endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(empty(JustDrive(.w_TAPATCLA)) ) and (not(Empty(.w_TACODCLA)))
          .oNewFocus=.oPgFrm.Page1.oPag.oTAPATCLA_2_6
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Non sono amessi percorsi assoluti ")
      endcase
      if not(Empty(.w_TACODCLA))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TAPATTIP = this.w_TAPATTIP
    this.o_TAPATCLA = this.w_TAPATCLA
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_TACODCLA)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_TACODCLA=space(5)
      .w_TACLADES=space(40)
      .w_TAFLUNIC=space(1)
      .w_TAFLDEFA=space(1)
      .w_TDPUBWEB=space(1)
      .w_TAPATCLA=space(254)
      .w_TACOPCLA=space(1)
      .DoRTCalc(1,9,.f.)
        .w_TDPUBWEB = IIF(IsAlt(),'S','N')
        .w_TAPATCLA = addbs(.w_TAPATCLA)
        .w_TACOPCLA = iif(empty(.w_TAPATCLA),'N',.w_TACOPCLA)
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_TACODCLA = t_TACODCLA
    this.w_TACLADES = t_TACLADES
    this.w_TAFLUNIC = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTAFLUNIC_2_3.RadioValue(.t.)
    this.w_TAFLDEFA = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTAFLDEFA_2_4.RadioValue(.t.)
    this.w_TDPUBWEB = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTDPUBWEB_2_5.RadioValue(.t.)
    this.w_TAPATCLA = t_TAPATCLA
    this.w_TACOPCLA = this.oPgFrm.Page1.oPag.oTACOPCLA_2_7.RadioValue(.t.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_TACODCLA with this.w_TACODCLA
    replace t_TACLADES with this.w_TACLADES
    replace t_TAFLUNIC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTAFLUNIC_2_3.ToRadio()
    replace t_TAFLDEFA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTAFLDEFA_2_4.ToRadio()
    replace t_TDPUBWEB with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTDPUBWEB_2_5.ToRadio()
    replace t_TAPATCLA with this.w_TAPATCLA
    replace t_TACOPCLA with this.oPgFrm.Page1.oPag.oTACOPCLA_2_7.ToRadio()
    if i_srv='A'
      replace TACODCLA with this.w_TACODCLA
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsut_mtaPag1 as StdContainer
  Width  = 720
  height = 310
  stdWidth  = 720
  stdheight = 310
  resizeXpos=280
  resizeYpos=220
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTACODICE_1_1 as StdField with uid="DQHHBBNTFQ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_TACODICE", cQueryName = "TACODICE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipologia allegato",;
    HelpContextID = 147469179,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=127, Top=9, InputMask=replicate('X',5)

  add object oTADESCRI_1_3 as StdField with uid="NJOSHHUMMQ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_TADESCRI", cQueryName = "TADESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 206552193,;
   bGlobalFont=.t.,;
    Height=21, Width=387, Left=189, Top=9, InputMask=replicate('X',40)

  add object oTAPATTIP_1_4 as StdField with uid="PNAMXBWUBR",rtseq=3,rtrep=.f.,;
    cFormVar = "w_TAPATTIP", cQueryName = "TAPATTIP",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    sErrorMsg = "Non sono amessi percorsi assoluti ",;
    ToolTipText = "Percorso da inserire nel path di archiviazione per la tipologia allegato",;
    HelpContextID = 79496070,;
   bGlobalFont=.t.,;
    Height=21, Width=449, Left=127, Top=36, InputMask=replicate('X',254)

  func oTAPATTIP_1_4.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!ISALT())
    endwith
    endif
  endfunc

  func oTAPATTIP_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(JustDrive(.w_TAPATTIP)) )
    endwith
    return bRes
  endfunc

  add object oTACOPTIP_1_5 as StdCheck with uid="MXXHGDXZYF",rtseq=4,rtrep=.f.,left=583, top=36, caption="Completa path file",;
    ToolTipText = "Se attivo, completa il path di archiviazione file con il path definito in tipologia allegato",;
    HelpContextID = 76166022,;
    cFormVar="w_TACOPTIP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTACOPTIP_1_5.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..TACOPTIP,&i_cF..t_TACOPTIP),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oTACOPTIP_1_5.GetRadio()
    this.Parent.oContained.w_TACOPTIP = this.RadioValue()
    return .t.
  endfunc

  func oTACOPTIP_1_5.ToRadio()
    this.Parent.oContained.w_TACOPTIP=trim(this.Parent.oContained.w_TACOPTIP)
    return(;
      iif(this.Parent.oContained.w_TACOPTIP=='S',1,;
      0))
  endfunc

  func oTACOPTIP_1_5.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oTACOPTIP_1_5.mCond()
    with this.Parent.oContained
      return (! empty(.w_TAPATTIP))
    endwith
  endfunc

  func oTACOPTIP_1_5.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!ISALT())
    endwith
    endif
  endfunc


  add object oObj_1_7 as cp_runprogram with uid="BACZSWDBRQ",left=-4, top=324, width=177,height=20,;
    caption='GSUT_BCE(S)',;
   bGlobalFont=.t.,;
    prg="GSUT_BCE('S')",;
    cEvent = "Controllo",;
    nPag=1;
    , HelpContextID = 58935851


  add object oObj_1_8 as cp_runprogram with uid="AJKHTHBFYL",left=-4, top=346, width=177,height=20,;
    caption='GSUT_BCE(U)',;
   bGlobalFont=.t.,;
    prg="GSUT_BCE('U')",;
    cEvent = "w_TAFLUNIC Changed",;
    nPag=1;
    , HelpContextID = 58936363


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=8, top=69, width=708,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=5,Field1="TACODCLA",Label1="Classe",Field2="TACLADES",Label2="Descrizione",Field3="TAFLUNIC",Label3="Univoca",Field4="TAFLDEFA",Label4="Predef.",Field5="TDPUBWEB",Label5="Pub. su web",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 235791994

  add object oStr_1_2 as StdString with uid="GCNISKGTUY",Visible=.t., Left=4, Top=10,;
    Alignment=1, Width=122, Height=18,;
    Caption="Tipologia allegato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="AWTZRMJIBI",Visible=.t., Left=4, Top=38,;
    Alignment=1, Width=122, Height=18,;
    Caption="Path archiviaz.:"  ;
  , bGlobalFont=.t.

  func oStr_1_9.mHide()
    with this.Parent.oContained
      return (!ISALT())
    endwith
  endfunc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-2,top=88,;
    width=704+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-1,top=89,width=703+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oTAPATCLA_2_6.Refresh()
      this.Parent.oTACOPCLA_2_7.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oTAPATCLA_2_6 as StdTrsField with uid="XURBEMAUHS",rtseq=11,rtrep=.t.,;
    cFormVar="w_TAPATCLA",value=space(254),;
    ToolTipText = "Percorso da inserire nel path di archiviazione per la classe allegato",;
    HelpContextID = 205716617,;
    cTotal="", bFixedPos=.t., cQueryName = "TAPATCLA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Non sono amessi percorsi assoluti ",;
   bGlobalFont=.t.,;
    Height=21, Width=422, Left=154, Top=286, InputMask=replicate('X',254)

  func oTAPATCLA_2_6.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!ISALT())
    endwith
    endif
  endfunc

  func oTAPATCLA_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(JustDrive(.w_TAPATCLA)) )
    endwith
    return bRes
  endfunc

  add object oTACOPCLA_2_7 as StdTrsCheck with uid="FIUBGYOWND",rtrep=.t.,;
    cFormVar="w_TACOPCLA",  caption="Completa path file",;
    ToolTipText = "Se attivo, completa il path di archiviazione file con il path definito nella classe allegato",;
    HelpContextID = 209046665,;
    Left=583, Top=285,;
    cTotal="", cQueryName = "TACOPCLA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oTACOPCLA_2_7.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..TACOPCLA,&i_cF..t_TACOPCLA),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oTACOPCLA_2_7.GetRadio()
    this.Parent.oContained.w_TACOPCLA = this.RadioValue()
    return .t.
  endfunc

  func oTACOPCLA_2_7.ToRadio()
    this.Parent.oContained.w_TACOPCLA=trim(this.Parent.oContained.w_TACOPCLA)
    return(;
      iif(this.Parent.oContained.w_TACOPCLA=='S',1,;
      0))
  endfunc

  func oTACOPCLA_2_7.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oTACOPCLA_2_7.mCond()
    with this.Parent.oContained
      return (! empty(.w_TAPATCLA))
    endwith
  endfunc

  func oTACOPCLA_2_7.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!ISALT())
    endwith
    endif
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oStr_3_1 as StdString with uid="WVBXDCHFAO",Visible=.t., Left=7, Top=288,;
    Alignment=1, Width=144, Height=18,;
    Caption="Path archiviaz. allegati:"  ;
  , bGlobalFont=.t.

  func oStr_3_1.mHide()
    with this.Parent.oContained
      return (!isalt())
    endwith
  endfunc
enddefine

* --- Defining Body row
define class tgsut_mtaBodyRow as CPBodyRowCnt
  Width=694
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oTACODCLA_2_1 as StdTrsField with uid="SOTBIGSZDA",rtseq=5,rtrep=.t.,;
    cFormVar="w_TACODCLA",value=space(5),isprimarykey=.t.,;
    ToolTipText = "Codice classe allegato",;
    HelpContextID = 221629577,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=53, Left=-2, Top=0, InputMask=replicate('X',5)

  add object oTACLADES_2_2 as StdTrsField with uid="VIXOWZSMFE",rtseq=6,rtrep=.t.,;
    cFormVar="w_TACLADES",value=space(40),;
    ToolTipText = "Descrizione classe allegato",;
    HelpContextID = 60240777,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=416, Left=57, Top=0, InputMask=replicate('X',40)

  add object oTAFLUNIC_2_3 as StdTrsCheck with uid="NKQNPJYNOH",rtrep=.t.,;
    cFormVar="w_TAFLUNIC",  caption="",;
    ToolTipText = "Attivo: pu� esistere un solo allegato con questa classe per ogni record cui � associato",;
    HelpContextID = 248996729,;
    Left=484, Top=1, Width=56,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , AutoSize=.F.;
   , bGlobalFont=.t.


  func oTAFLUNIC_2_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..TAFLUNIC,&i_cF..t_TAFLUNIC),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oTAFLUNIC_2_3.GetRadio()
    this.Parent.oContained.w_TAFLUNIC = this.RadioValue()
    return .t.
  endfunc

  func oTAFLUNIC_2_3.ToRadio()
    this.Parent.oContained.w_TAFLUNIC=trim(this.Parent.oContained.w_TAFLUNIC)
    return(;
      iif(this.Parent.oContained.w_TAFLUNIC=='S',1,;
      0))
  endfunc

  func oTAFLUNIC_2_3.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oTAFLDEFA_2_4 as StdTrsCheck with uid="MXZAKDHVYR",rtrep=.t.,;
    cFormVar="w_TAFLDEFA",  caption="",;
    ToolTipText = "Attivo: questa classe viene utilizzata come predefinita all'utilizzo di questa tipologia",;
    HelpContextID = 80175991,;
    Left=554, Top=1, Width=54,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , AutoSize=.F.;
   , bGlobalFont=.t.


  func oTAFLDEFA_2_4.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..TAFLDEFA,&i_cF..t_TAFLDEFA),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oTAFLDEFA_2_4.GetRadio()
    this.Parent.oContained.w_TAFLDEFA = this.RadioValue()
    return .t.
  endfunc

  func oTAFLDEFA_2_4.ToRadio()
    this.Parent.oContained.w_TAFLDEFA=trim(this.Parent.oContained.w_TAFLDEFA)
    return(;
      iif(this.Parent.oContained.w_TAFLDEFA=='S',1,;
      0))
  endfunc

  func oTAFLDEFA_2_4.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oTDPUBWEB_2_5 as StdTrsCheck with uid="OIIQFDDXSM",rtrep=.t.,;
    cFormVar="w_TDPUBWEB",  caption="",;
    ToolTipText = "Check pubblica su web",;
    HelpContextID = 112264824,;
    Left=623, Top=1, Width=66,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , AutoSize=.F.;
   , bGlobalFont=.t.


  func oTDPUBWEB_2_5.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..TDPUBWEB,&i_cF..t_TDPUBWEB),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oTDPUBWEB_2_5.GetRadio()
    this.Parent.oContained.w_TDPUBWEB = this.RadioValue()
    return .t.
  endfunc

  func oTDPUBWEB_2_5.ToRadio()
    this.Parent.oContained.w_TDPUBWEB=trim(this.Parent.oContained.w_TDPUBWEB)
    return(;
      iif(this.Parent.oContained.w_TDPUBWEB=='S',1,;
      0))
  endfunc

  func oTDPUBWEB_2_5.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oTDPUBWEB_2_5.mCond()
    with this.Parent.oContained
      return (!IsAlt() AND g_CPIN<>'S')
    endwith
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oTACODCLA_2_1.When()
    return(.t.)
  proc oTACODCLA_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oTACODCLA_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_mta','TIP_ALLE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".TACODICE=TIP_ALLE.TACODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
