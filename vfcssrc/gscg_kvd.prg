* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_kvd                                                        *
*              Variazione dati descrittivi in primanota                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-10-08                                                      *
* Last revis.: 2011-05-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_kvd",oParentObject))

* --- Class definition
define class tgscg_kvd as StdForm
  Top    = -1
  Left   = 10

  * --- Standard Properties
  Width  = 810
  Height = 495
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-05-09"
  HelpContextID=48899433
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=42

  * --- Constant Properties
  _IDX = 0
  CAU_CONT_IDX = 0
  CONTI_IDX = 0
  PNT_MAST_IDX = 0
  ESERCIZI_IDX = 0
  cPrg = "gscg_kvd"
  cComment = "Variazione dati descrittivi in primanota"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODCAU = space(5)
  w_DESCAU = space(35)
  w_TIPCON = space(1)
  o_TIPCON = space(1)
  w_CODCON = space(15)
  w_NREGINI = 0
  w_NREGFIN = 0
  w_CODUTINI = 0
  w_CODUTFIN = 0
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_NUMINI = 0
  w_NUMFIN = 0
  w_ALFINI = space(10)
  w_ALFFIN = space(10)
  w_DADOCINI = ctod('  /  /  ')
  w_CODAZI = space(5)
  w_DADOCFIN = ctod('  /  /  ')
  w_CODESE = space(4)
  w_DESCRI = space(40)
  w_PNTIPCON = space(1)
  w_PNCODCON = space(15)
  w_MASTRO = space(15)
  w_FLANAL = space(1)
  w_CCTAGG = space(1)
  w_SEZB = space(1)
  w_SERIALE = space(35)
  o_SERIALE = space(35)
  w_PNDESSUP = space(50)
  w_AGGRIG = space(1)
  o_AGGRIG = space(1)
  w_ROWNUM = 0
  o_ROWNUM = 0
  w_PNDESRIG = space(50)
  w_PNINICOM = ctod('  /  /  ')
  w_NUMREG = 0
  w_PNFINCOM = ctod('  /  /  ')
  w_UTENTE = 0
  w_AGGANA = space(1)
  w_DATREG = ctod('  /  /  ')
  w_TIPMOV = 0
  w_FLSELE = 0
  w_TIPOCONTO = space(1)
  w_PNSERIAL = space(10)
  w_INIESE = ctod('  /  /  ')
  w_FINESE = ctod('  /  /  ')
  w_Zoom = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_kvdPag1","gscg_kvd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODCAU_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_Zoom = this.oPgFrm.Pages(1).oPag.Zoom
    DoDefault()
    proc Destroy()
      this.w_Zoom = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='CAU_CONT'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='PNT_MAST'
    this.cWorkTables[4]='ESERCIZI'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODCAU=space(5)
      .w_DESCAU=space(35)
      .w_TIPCON=space(1)
      .w_CODCON=space(15)
      .w_NREGINI=0
      .w_NREGFIN=0
      .w_CODUTINI=0
      .w_CODUTFIN=0
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_NUMINI=0
      .w_NUMFIN=0
      .w_ALFINI=space(10)
      .w_ALFFIN=space(10)
      .w_DADOCINI=ctod("  /  /  ")
      .w_CODAZI=space(5)
      .w_DADOCFIN=ctod("  /  /  ")
      .w_CODESE=space(4)
      .w_DESCRI=space(40)
      .w_PNTIPCON=space(1)
      .w_PNCODCON=space(15)
      .w_MASTRO=space(15)
      .w_FLANAL=space(1)
      .w_CCTAGG=space(1)
      .w_SEZB=space(1)
      .w_SERIALE=space(35)
      .w_PNDESSUP=space(50)
      .w_AGGRIG=space(1)
      .w_ROWNUM=0
      .w_PNDESRIG=space(50)
      .w_PNINICOM=ctod("  /  /  ")
      .w_NUMREG=0
      .w_PNFINCOM=ctod("  /  /  ")
      .w_UTENTE=0
      .w_AGGANA=space(1)
      .w_DATREG=ctod("  /  /  ")
      .w_TIPMOV=0
      .w_FLSELE=0
      .w_TIPOCONTO=space(1)
      .w_PNSERIAL=space(10)
      .w_INIESE=ctod("  /  /  ")
      .w_FINESE=ctod("  /  /  ")
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODCAU))
          .link_1_2('Full')
        endif
          .DoRTCalc(2,2,.f.)
        .w_TIPCON = 'C'
        .w_CODCON = ' '
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_CODCON))
          .link_1_5('Full')
        endif
          .DoRTCalc(5,12,.f.)
        .w_ALFINI = ''
        .w_ALFFIN = ''
          .DoRTCalc(15,15,.f.)
        .w_CODAZI = i_CODAZI
        .DoRTCalc(17,18,.f.)
        if not(empty(.w_CODESE))
          .link_1_20('Full')
        endif
          .DoRTCalc(19,19,.f.)
        .w_PNTIPCON = .w_Zoom.getVar('PNTIPCON')
        .w_PNCODCON = .w_Zoom.getVar('PNCODCON')
        .DoRTCalc(21,21,.f.)
        if not(empty(.w_PNCODCON))
          .link_1_30('Full')
        endif
          .DoRTCalc(22,22,.f.)
        .w_FLANAL = .w_Zoom.getVar('CCFLANAL')
        .w_CCTAGG = .w_Zoom.getVar('ANCCTAGG')
        .w_SEZB = IIF(.w_PNTIPCON="G", CALCSEZ(.w_MASTRO), " ")
        .w_SERIALE = .w_Zoom.getVar('PNSERIAL')
        .w_PNDESSUP = .w_Zoom.getVar('PNDESSUP')
        .w_AGGRIG = 'N'
      .oPgFrm.Page1.oPag.Zoom.Calculate()
        .w_ROWNUM = .w_Zoom.getVar('CPROWNUM')
        .w_PNDESRIG = .w_Zoom.getVar('PNDESRIG')
        .w_PNINICOM = CP_TODATE(.w_Zoom.getVar('PNINICOM'))
          .DoRTCalc(32,32,.f.)
        .w_PNFINCOM = CP_TODATE(.w_Zoom.getVar('PNFINCOM'))
          .DoRTCalc(34,34,.f.)
        .w_AGGANA = .w_Zoom.getVar('AGGANA')
          .DoRTCalc(36,36,.f.)
        .w_TIPMOV = .w_Zoom.getVar('PTROWORD')
        .w_FLSELE = 0
      .oPgFrm.Page1.oPag.oObj_1_59.Calculate()
        .w_TIPOCONTO = IIF(.w_TIPCON='N',' ',.w_TIPCON)
    endwith
    this.DoRTCalc(40,42,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_75.enabled = this.oPgFrm.Page1.oPag.oBtn_1_75.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_76.enabled = this.oPgFrm.Page1.oPag.oBtn_1_76.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_77.enabled = this.oPgFrm.Page1.oPag.oBtn_1_77.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_78.enabled = this.oPgFrm.Page1.oPag.oBtn_1_78.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
        if .o_TIPCON<>.w_TIPCON
            .w_CODCON = ' '
          .link_1_5('Full')
        endif
        .DoRTCalc(5,19,.t.)
            .w_PNTIPCON = .w_Zoom.getVar('PNTIPCON')
            .w_PNCODCON = .w_Zoom.getVar('PNCODCON')
          .link_1_30('Full')
        .DoRTCalc(22,22,.t.)
            .w_FLANAL = .w_Zoom.getVar('CCFLANAL')
            .w_CCTAGG = .w_Zoom.getVar('ANCCTAGG')
            .w_SEZB = IIF(.w_PNTIPCON="G", CALCSEZ(.w_MASTRO), " ")
            .w_SERIALE = .w_Zoom.getVar('PNSERIAL')
        if .o_SERIALE<>.w_SERIALE
            .w_PNDESSUP = .w_Zoom.getVar('PNDESSUP')
        endif
        .oPgFrm.Page1.oPag.Zoom.Calculate()
        .DoRTCalc(28,28,.t.)
            .w_ROWNUM = .w_Zoom.getVar('CPROWNUM')
        if .o_ROWNUM<>.w_ROWNUM.or. .o_AGGRIG<>.w_AGGRIG
            .w_PNDESRIG = .w_Zoom.getVar('PNDESRIG')
        endif
        if .o_ROWNUM<>.w_ROWNUM
            .w_PNINICOM = CP_TODATE(.w_Zoom.getVar('PNINICOM'))
        endif
        .DoRTCalc(32,32,.t.)
        if .o_ROWNUM<>.w_ROWNUM
            .w_PNFINCOM = CP_TODATE(.w_Zoom.getVar('PNFINCOM'))
        endif
        .DoRTCalc(34,34,.t.)
        if .o_ROWNUM<>.w_ROWNUM
            .w_AGGANA = .w_Zoom.getVar('AGGANA')
        endif
        .DoRTCalc(36,36,.t.)
            .w_TIPMOV = .w_Zoom.getVar('PTROWORD')
        .oPgFrm.Page1.oPag.oObj_1_59.Calculate()
        .DoRTCalc(38,38,.t.)
            .w_TIPOCONTO = IIF(.w_TIPCON='N',' ',.w_TIPCON)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(40,42,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.Zoom.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_59.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPNDESRIG_1_43.enabled = this.oPgFrm.Page1.oPag.oPNDESRIG_1_43.mCond()
    this.oPgFrm.Page1.oPag.oPNINICOM_1_44.enabled = this.oPgFrm.Page1.oPag.oPNINICOM_1_44.mCond()
    this.oPgFrm.Page1.oPag.oPNFINCOM_1_46.enabled = this.oPgFrm.Page1.oPag.oPNFINCOM_1_46.mCond()
    this.oPgFrm.Page1.oPag.oAGGANA_1_48.enabled = this.oPgFrm.Page1.oPag.oAGGANA_1_48.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCODCON_1_5.visible=!this.oPgFrm.Page1.oPag.oCODCON_1_5.mHide()
    this.oPgFrm.Page1.oPag.oDESCRI_1_23.visible=!this.oPgFrm.Page1.oPag.oDESCRI_1_23.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_24.visible=!this.oPgFrm.Page1.oPag.oStr_1_24.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_25.visible=!this.oPgFrm.Page1.oPag.oStr_1_25.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.Zoom.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_59.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODCAU
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCAU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_CODCAU)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_CODCAU))
          select CCCODICE,CCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCAU)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCAU) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCODCAU_1_2'),i_cWhere,'GSCG_ACC',"Causali contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCAU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CODCAU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CODCAU)
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCAU = NVL(_Link_.CCCODICE,space(5))
      this.w_DESCAU = NVL(_Link_.CCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODCAU = space(5)
      endif
      this.w_DESCAU = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCAU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCON
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CODCON))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCON_1_5'),i_cWhere,'',"CLIENTI/FORNITORI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODCON)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRI = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON = space(15)
      endif
      this.w_DESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODESE
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_CODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_CODESE))
          select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oCODESE_1_20'),i_cWhere,'',"ESERCIZI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_CODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_CODESE)
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_INIESE = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_FINESE = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODESE = space(4)
      endif
      this.w_INIESE = ctod("  /  /  ")
      this.w_FINESE = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PNCODCON
  func Link_1_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PNCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PNCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANCONSUP";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PNCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PNTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_PNTIPCON;
                       ,'ANCODICE',this.w_PNCODCON)
            select ANTIPCON,ANCODICE,ANCONSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PNCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_MASTRO = NVL(_Link_.ANCONSUP,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_PNCODCON = space(15)
      endif
      this.w_MASTRO = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PNCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODCAU_1_2.value==this.w_CODCAU)
      this.oPgFrm.Page1.oPag.oCODCAU_1_2.value=this.w_CODCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAU_1_3.value==this.w_DESCAU)
      this.oPgFrm.Page1.oPag.oDESCAU_1_3.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPCON_1_4.RadioValue()==this.w_TIPCON)
      this.oPgFrm.Page1.oPag.oTIPCON_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCON_1_5.value==this.w_CODCON)
      this.oPgFrm.Page1.oPag.oCODCON_1_5.value=this.w_CODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oNREGINI_1_6.value==this.w_NREGINI)
      this.oPgFrm.Page1.oPag.oNREGINI_1_6.value=this.w_NREGINI
    endif
    if not(this.oPgFrm.Page1.oPag.oNREGFIN_1_7.value==this.w_NREGFIN)
      this.oPgFrm.Page1.oPag.oNREGFIN_1_7.value=this.w_NREGFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODUTINI_1_8.value==this.w_CODUTINI)
      this.oPgFrm.Page1.oPag.oCODUTINI_1_8.value=this.w_CODUTINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODUTFIN_1_9.value==this.w_CODUTFIN)
      this.oPgFrm.Page1.oPag.oCODUTFIN_1_9.value=this.w_CODUTFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_10.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_10.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_11.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_11.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMINI_1_13.value==this.w_NUMINI)
      this.oPgFrm.Page1.oPag.oNUMINI_1_13.value=this.w_NUMINI
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMFIN_1_14.value==this.w_NUMFIN)
      this.oPgFrm.Page1.oPag.oNUMFIN_1_14.value=this.w_NUMFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oALFINI_1_15.value==this.w_ALFINI)
      this.oPgFrm.Page1.oPag.oALFINI_1_15.value=this.w_ALFINI
    endif
    if not(this.oPgFrm.Page1.oPag.oALFFIN_1_16.value==this.w_ALFFIN)
      this.oPgFrm.Page1.oPag.oALFFIN_1_16.value=this.w_ALFFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDADOCINI_1_17.value==this.w_DADOCINI)
      this.oPgFrm.Page1.oPag.oDADOCINI_1_17.value=this.w_DADOCINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDADOCFIN_1_19.value==this.w_DADOCFIN)
      this.oPgFrm.Page1.oPag.oDADOCFIN_1_19.value=this.w_DADOCFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODESE_1_20.value==this.w_CODESE)
      this.oPgFrm.Page1.oPag.oCODESE_1_20.value=this.w_CODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_23.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_23.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oPNDESSUP_1_39.value==this.w_PNDESSUP)
      this.oPgFrm.Page1.oPag.oPNDESSUP_1_39.value=this.w_PNDESSUP
    endif
    if not(this.oPgFrm.Page1.oPag.oAGGRIG_1_40.RadioValue()==this.w_AGGRIG)
      this.oPgFrm.Page1.oPag.oAGGRIG_1_40.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPNDESRIG_1_43.value==this.w_PNDESRIG)
      this.oPgFrm.Page1.oPag.oPNDESRIG_1_43.value=this.w_PNDESRIG
    endif
    if not(this.oPgFrm.Page1.oPag.oPNINICOM_1_44.value==this.w_PNINICOM)
      this.oPgFrm.Page1.oPag.oPNINICOM_1_44.value=this.w_PNINICOM
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMREG_1_45.value==this.w_NUMREG)
      this.oPgFrm.Page1.oPag.oNUMREG_1_45.value=this.w_NUMREG
    endif
    if not(this.oPgFrm.Page1.oPag.oPNFINCOM_1_46.value==this.w_PNFINCOM)
      this.oPgFrm.Page1.oPag.oPNFINCOM_1_46.value=this.w_PNFINCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oUTENTE_1_47.value==this.w_UTENTE)
      this.oPgFrm.Page1.oPag.oUTENTE_1_47.value=this.w_UTENTE
    endif
    if not(this.oPgFrm.Page1.oPag.oAGGANA_1_48.RadioValue()==this.w_AGGANA)
      this.oPgFrm.Page1.oPag.oAGGANA_1_48.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATREG_1_49.value==this.w_DATREG)
      this.oPgFrm.Page1.oPag.oDATREG_1_49.value=this.w_DATREG
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_DATINI<=.w_DATFIN or empty(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   not(.w_DATINI<=.w_DATFIN or empty(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   not((empty(.w_ALFFIN) OR  (.w_ALFINI<=.w_ALFFIN)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oALFINI_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � pi� grande della serie finale")
          case   not((.w_ALFFIN>=.w_ALFINI) or (empty(.w_ALFINI)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oALFFIN_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � maggiore della serie finale")
          case   not(.w_DADOCFIN>=.w_DADOCINI or empty(.w_DADOCFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDADOCINI_1_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   not(.w_DADOCFIN >= .w_DADOCINI or empty(.w_DADOCINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDADOCFIN_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   not(.w_PNINICOM <= .w_PNFINCOM or empty(.w_PNFINCOM))  and (.w_SEZB $ 'CR')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPNINICOM_1_44.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data di inizio competenza � maggiore della data di fine competenza")
          case   not(.w_PNFINCOM >= .w_PNINICOM or empty(.w_PNINICOM))  and (.w_SEZB $ 'CR')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPNFINCOM_1_46.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data di fine competenza � minore della data di inizio competenza")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPCON = this.w_TIPCON
    this.o_SERIALE = this.w_SERIALE
    this.o_AGGRIG = this.w_AGGRIG
    this.o_ROWNUM = this.w_ROWNUM
    return

enddefine

* --- Define pages as container
define class tgscg_kvdPag1 as StdContainer
  Width  = 806
  height = 495
  stdWidth  = 806
  stdheight = 495
  resizeXpos=354
  resizeYpos=399
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODCAU_1_2 as StdField with uid="DXNSSAXDLI",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODCAU", cQueryName = "CODCAU",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice della causale contabile da ricercare",;
    HelpContextID = 107834918,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=77, Top=38, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_CODCAU"

  func oCODCAU_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCAU_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCAU_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCODCAU_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'',this.parent.oContained
  endproc
  proc oCODCAU_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_CODCAU
     i_obj.ecpSave()
  endproc

  add object oDESCAU_1_3 as StdField with uid="YYIALOWEWR",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 107893814,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=157, Top=38, InputMask=replicate('X',35)


  add object oTIPCON_1_4 as StdCombo with uid="HQKDJSKNYG",rtseq=3,rtrep=.f.,left=77,top=64,width=108,height=21;
    , ToolTipText = "Tipo conto";
    , HelpContextID = 5122358;
    , cFormVar="w_TIPCON",RowSource=""+"Clienti,"+"Fornitori,"+"Nessuno", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPCON_1_4.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oTIPCON_1_4.GetRadio()
    this.Parent.oContained.w_TIPCON = this.RadioValue()
    return .t.
  endfunc

  func oTIPCON_1_4.SetRadio()
    this.Parent.oContained.w_TIPCON=trim(this.Parent.oContained.w_TIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCON=='C',1,;
      iif(this.Parent.oContained.w_TIPCON=='F',2,;
      iif(this.Parent.oContained.w_TIPCON=='N',3,;
      0)))
  endfunc

  func oTIPCON_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CODCON)
        bRes2=.link_1_5('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oCODCON_1_5 as StdField with uid="TGLUNQDANV",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CODCON", cQueryName = "CODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice cliente/fornitore intestatario del documento",;
    HelpContextID = 5074470,;
   bGlobalFont=.t.,;
    Height=21, Width=130, Left=77, Top=94, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCON"

  func oCODCON_1_5.mHide()
    with this.Parent.oContained
      return (.w_TIPCON='N')
    endwith
  endfunc

  func oCODCON_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCON_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCON_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCON_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"CLIENTI/FORNITORI",'',this.parent.oContained
  endproc

  add object oNREGINI_1_6 as StdField with uid="FQSYFHTUWP",rtseq=5,rtrep=.f.,;
    cFormVar = "w_NREGINI", cQueryName = "",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero registrazione iniziale",;
    HelpContextID = 949802,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=77, Top=123

  add object oNREGFIN_1_7 as StdField with uid="LDKQYKHTSH",rtseq=6,rtrep=.f.,;
    cFormVar = "w_NREGFIN", cQueryName = "",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero registrazione finale",;
    HelpContextID = 87981610,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=77, Top=148

  add object oCODUTINI_1_8 as StdField with uid="OQVCQVZTTC",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CODUTINI", cQueryName = "CODUTINI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice utente iniziale",;
    HelpContextID = 72389009,;
   bGlobalFont=.t.,;
    Height=21, Width=28, Left=145, Top=123

  add object oCODUTFIN_1_9 as StdField with uid="TLJLRZVVGF",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CODUTFIN", cQueryName = "CODUTFIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice utente finale",;
    HelpContextID = 122720652,;
   bGlobalFont=.t.,;
    Height=21, Width=28, Left=145, Top=148

  add object oDATINI_1_10 as StdField with uid="OEOUAEOZRW",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data registrazione iniziale da ricercare",;
    HelpContextID = 189030454,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=206, Top=123

  func oDATINI_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN or empty(.w_DATFIN))
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_11 as StdField with uid="GQLQIWEQSK",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data registrazione finale da ricercare",;
    HelpContextID = 267477046,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=206, Top=149

  func oDATFIN_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN or empty(.w_DATFIN))
    endwith
    return bRes
  endfunc

  add object oNUMINI_1_13 as StdField with uid="CGTAYVCLKJ",rtseq=11,rtrep=.f.,;
    cFormVar = "w_NUMINI", cQueryName = "NUMINI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento di inizio selezione",;
    HelpContextID = 189007062,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=370, Top=123, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  add object oNUMFIN_1_14 as StdField with uid="PPVPLBFEDU",rtseq=12,rtrep=.f.,;
    cFormVar = "w_NUMFIN", cQueryName = "NUMFIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento di fine selezione",;
    HelpContextID = 267453654,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=370, Top=149, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  add object oALFINI_1_15 as StdField with uid="IKCHOCFECU",rtseq=13,rtrep=.f.,;
    cFormVar = "w_ALFINI", cQueryName = "ALFINI",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � pi� grande della serie finale",;
    ToolTipText = "Serie documento di inizio selezione",;
    HelpContextID = 188975878,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=501, Top=123, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  func oALFINI_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_ALFFIN) OR  (.w_ALFINI<=.w_ALFFIN)))
    endwith
    return bRes
  endfunc

  add object oALFFIN_1_16 as StdField with uid="SNGKDXOYPG",rtseq=14,rtrep=.f.,;
    cFormVar = "w_ALFFIN", cQueryName = "ALFFIN",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � maggiore della serie finale",;
    ToolTipText = "Serie documento di fine selezione",;
    HelpContextID = 267422470,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=501, Top=149, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  func oALFFIN_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_ALFFIN>=.w_ALFINI) or (empty(.w_ALFINI)))
    endwith
    return bRes
  endfunc

  add object oDADOCINI_1_17 as StdField with uid="KTUFDNNKXZ",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DADOCINI", cQueryName = "",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data emissione del documento iniziale",;
    HelpContextID = 90611585,;
   bGlobalFont=.t.,;
    Height=21, Width=71, Left=615, Top=124

  func oDADOCINI_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DADOCFIN>=.w_DADOCINI or empty(.w_DADOCFIN))
    endwith
    return bRes
  endfunc

  add object oDADOCFIN_1_19 as StdField with uid="VNSTMBLDUE",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DADOCFIN", cQueryName = "",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data emissione del documento finale",;
    HelpContextID = 140943228,;
   bGlobalFont=.t.,;
    Height=21, Width=71, Left=615, Top=149

  func oDADOCFIN_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DADOCFIN >= .w_DADOCINI or empty(.w_DADOCINI))
    endwith
    return bRes
  endfunc

  add object oCODESE_1_20 as StdField with uid="ZMIJKNZDWM",rtseq=18,rtrep=.f.,;
    cFormVar = "w_CODESE", cQueryName = "CODESE",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio di competenza dei movimenti da aggiornare",;
    HelpContextID = 126840358,;
   bGlobalFont=.t.,;
    Height=21, Width=60, Left=77, Top=173, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_CODESE"

  func oCODESE_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODESE_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODESE_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oCODESE_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'',"ESERCIZI",'',this.parent.oContained
  endproc

  add object oDESCRI_1_23 as StdField with uid="IBJUQRYNKK",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 192828470,;
   bGlobalFont=.t.,;
    Height=21, Width=285, Left=216, Top=94, InputMask=replicate('X',40)

  func oDESCRI_1_23.mHide()
    with this.Parent.oContained
      return (.w_TIPCON='N')
    endwith
  endfunc

  add object oPNDESSUP_1_39 as StdField with uid="VWBZQAQARC",rtseq=27,rtrep=.f.,;
    cFormVar = "w_PNDESSUP", cQueryName = "PNDESSUP",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Modifica le note generali della registrazione",;
    HelpContextID = 93285958,;
   bGlobalFont=.t.,;
    Height=21, Width=340, Left=457, Top=238, InputMask=replicate('X',50)

  add object oAGGRIG_1_40 as StdCheck with uid="CXHWQEKVNB",rtseq=28,rtrep=.f.,left=457, top=265, caption="Aggiorna descrizione di riga",;
    ToolTipText = "Se attivo aggiorna la descrizione su tutte le righe",;
    HelpContextID = 150771206,;
    cFormVar="w_AGGRIG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAGGRIG_1_40.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAGGRIG_1_40.GetRadio()
    this.Parent.oContained.w_AGGRIG = this.RadioValue()
    return .t.
  endfunc

  func oAGGRIG_1_40.SetRadio()
    this.Parent.oContained.w_AGGRIG=trim(this.Parent.oContained.w_AGGRIG)
    this.value = ;
      iif(this.Parent.oContained.w_AGGRIG=='S',1,;
      0)
  endfunc


  add object Zoom as cp_zoombox with uid="XEJRQZANUZ",left=8, top=227, width=443,height=258,;
    caption='Object',;
   bGlobalFont=.t.,;
    bRetriveAllRows=.f.,cZoomFile="GSCG0KVD",bOptions=.f.,bAdvOptions=.t.,bReadOnly=.f.,cTable="PNT_DETT",cMenuFile="",cZoomOnZoom="",bQueryOnLoad=.f.,;
    cEvent = "Ricerca",;
    nPag=1;
    , HelpContextID = 129098214

  add object oPNDESRIG_1_43 as StdField with uid="PHLSQKLSOM",rtseq=30,rtrep=.f.,;
    cFormVar = "w_PNDESRIG", cQueryName = "PNDESRIG",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Modifica le note di riga della registrazione",;
    HelpContextID = 191926723,;
   bGlobalFont=.t.,;
    Height=21, Width=340, Left=457, Top=313, InputMask=replicate('X',50)

  func oPNDESRIG_1_43.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGRIG<>'S')
    endwith
   endif
  endfunc

  add object oPNINICOM_1_44 as StdField with uid="AFSAGPRLMO",rtseq=31,rtrep=.f.,;
    cFormVar = "w_PNINICOM", cQueryName = "PNINICOM",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data di inizio competenza � maggiore della data di fine competenza",;
    ToolTipText = "Data di inizio competenza contabile",;
    HelpContextID = 185024957,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=593, Top=341

  func oPNINICOM_1_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SEZB $ 'CR')
    endwith
   endif
  endfunc

  func oPNINICOM_1_44.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PNINICOM <= .w_PNFINCOM or empty(.w_PNFINCOM))
    endwith
    return bRes
  endfunc

  add object oNUMREG_1_45 as StdField with uid="EDHNNJPCXK",rtseq=32,rtrep=.f.,;
    cFormVar = "w_NUMREG", cQueryName = "NUMREG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Nostro documento da ricercare",;
    HelpContextID = 146605270,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=526, Top=38

  add object oPNFINCOM_1_46 as StdField with uid="TJLCZJNWVX",rtseq=33,rtrep=.f.,;
    cFormVar = "w_PNFINCOM", cQueryName = "PNFINCOM",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data di fine competenza � minore della data di inizio competenza",;
    ToolTipText = "Data di fine competenza contabile",;
    HelpContextID = 180122045,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=593, Top=368

  func oPNFINCOM_1_46.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SEZB $ 'CR')
    endwith
   endif
  endfunc

  func oPNFINCOM_1_46.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PNFINCOM >= .w_PNINICOM or empty(.w_PNINICOM))
    endwith
    return bRes
  endfunc

  add object oUTENTE_1_47 as StdField with uid="JXVZFOHZEM",rtseq=34,rtrep=.f.,;
    cFormVar = "w_UTENTE", cQueryName = "UTENTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Serie del documento",;
    HelpContextID = 128484422,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=654, Top=38

  add object oAGGANA_1_48 as StdCheck with uid="ZVSOFHZZLN",rtseq=35,rtrep=.f.,left=680, top=369, caption="Aggiorna analitica",;
    ToolTipText = "Se attivo aggiorna le date di inizio e fine competenza per l'analitica",;
    HelpContextID = 54236678,;
    cFormVar="w_AGGANA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAGGANA_1_48.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAGGANA_1_48.GetRadio()
    this.Parent.oContained.w_AGGANA = this.RadioValue()
    return .t.
  endfunc

  func oAGGANA_1_48.SetRadio()
    this.Parent.oContained.w_AGGANA=trim(this.Parent.oContained.w_AGGANA)
    this.value = ;
      iif(this.Parent.oContained.w_AGGANA=='S',1,;
      0)
  endfunc

  func oAGGANA_1_48.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PNTIPCON="G" AND .w_SEZB $ "CR" AND .w_FLANAL="S" AND g_PERCCR="S" AND .w_CCTAGG $ "AM")
    endwith
   endif
  endfunc

  add object oDATREG_1_49 as StdField with uid="HGNHBNUHDL",rtseq=36,rtrep=.f.,;
    cFormVar = "w_DATREG", cQueryName = "DATREG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data del documento",;
    HelpContextID = 146628662,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=722, Top=39


  add object oObj_1_59 as cp_runprogram with uid="USRABMPJFG",left=2, top=507, width=485,height=24,;
    caption='GSCG_BV1(MARCA)',;
   bGlobalFont=.t.,;
    prg="GSCG_BV1('MARCA')",;
    cEvent = "w_PNDESRIG Changed, w_PNINICOM Changed, w_PNFINCOM Changed, w_AGGANA Changed",;
    nPag=1;
    , HelpContextID = 181912087


  add object oBtn_1_75 as StdButton with uid="NDZWWZOFWJ",left=697, top=439, width=48,height=45,;
    CpPicture="bmp\save.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per confermare le scelte impostate";
    , HelpContextID = 97726087;
    , caption='\<Conferma';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_75.Click()
      with this.Parent.oContained
        GSCG_BV1(this.Parent.oContained,"AGGIO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_76 as StdButton with uid="RPFDTUXQSA",left=749, top=439, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 41582010;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_76.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_77 as StdButton with uid="NAEXGOEXQX",left=746, top=68, width=48,height=45,;
    CpPicture="bmp\documenti.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizza il documento associato alla riga selezionata";
    , HelpContextID = 233578010;
    , TabStop=.f.,Caption='\<Origine';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_77.Click()
      with this.Parent.oContained
        GSAR_BZP(this.Parent.oContained,.w_SERIALE)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_78 as StdButton with uid="YXQDZHVTYE",left=392, top=177, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la ricerca";
    , HelpContextID = 128022806;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_78.Click()
      do GSCG_KV1 with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_1 as StdString with uid="KRVAAAMMMN",Visible=.t., Left=23, Top=38,;
    Alignment=1, Width=50, Height=18,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="GVYXQRQEAM",Visible=.t., Left=9, Top=64,;
    Alignment=1, Width=64, Height=18,;
    Caption="Intestatario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="RGJNSAKFNJ",Visible=.t., Left=489, Top=123,;
    Alignment=2, Width=12, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="TQXDLWGEQQ",Visible=.t., Left=489, Top=149,;
    Alignment=2, Width=12, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="AUUWIYKKDR",Visible=.t., Left=11, Top=94,;
    Alignment=1, Width=61, Height=18,;
    Caption="Fornitore:"  ;
  , bGlobalFont=.t.

  func oStr_1_24.mHide()
    with this.Parent.oContained
      return (.w_TIPCON='N' OR .w_TIPCON='C')
    endwith
  endfunc

  add object oStr_1_25 as StdString with uid="RKUSFJMUEB",Visible=.t., Left=13, Top=94,;
    Alignment=1, Width=60, Height=18,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  func oStr_1_25.mHide()
    with this.Parent.oContained
      return (.w_TIPCON='N' OR .w_TIPCON='F')
    endwith
  endfunc

  add object oStr_1_26 as StdString with uid="UKLYXPUCZR",Visible=.t., Left=10, Top=9,;
    Alignment=0, Width=436, Height=18,;
    Caption="Parametri di selezione primanota"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="QHWNFEVOHH",Visible=.t., Left=468, Top=185,;
    Alignment=0, Width=312, Height=18,;
    Caption="Dati da aggiornare"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="LJRHHPQHXF",Visible=.t., Left=457, Top=341,;
    Alignment=1, Width=133, Height=18,;
    Caption="Data inizio competenza.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="XMXVUVPHOU",Visible=.t., Left=468, Top=368,;
    Alignment=1, Width=122, Height=18,;
    Caption="Data fine competenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="OITGWXOZKU",Visible=.t., Left=475, Top=38,;
    Alignment=1, Width=50, Height=18,;
    Caption="Reg. n.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="OOTSBCFEDK",Visible=.t., Left=646, Top=42,;
    Alignment=0, Width=7, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="DZNIVNJNTP",Visible=.t., Left=691, Top=39,;
    Alignment=1, Width=30, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="BKAOPWPHZR",Visible=.t., Left=466, Top=9,;
    Alignment=0, Width=254, Height=18,;
    Caption="Estremi registrazione di partenza"  ;
  , bGlobalFont=.t.

  add object oStr_1_56 as StdString with uid="ZOTHAXBXGW",Visible=.t., Left=458, Top=217,;
    Alignment=1, Width=33, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_1_58 as StdString with uid="XQFLVKAAQI",Visible=.t., Left=459, Top=296,;
    Alignment=1, Width=105, Height=18,;
    Caption="Descrizione di riga:"  ;
  , bGlobalFont=.t.

  add object oStr_1_62 as StdString with uid="KTYIROCBER",Visible=.t., Left=9, Top=124,;
    Alignment=1, Width=64, Height=18,;
    Caption="Da reg. n.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_63 as StdString with uid="HKYRVJNMCC",Visible=.t., Left=17, Top=149,;
    Alignment=1, Width=55, Height=18,;
    Caption="A reg. n.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_64 as StdString with uid="AFLCJPWLZT",Visible=.t., Left=133, Top=123,;
    Alignment=2, Width=18, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_65 as StdString with uid="OJYAHTVKFG",Visible=.t., Left=133, Top=152,;
    Alignment=2, Width=18, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_66 as StdString with uid="VAQQTVQCJN",Visible=.t., Left=182, Top=123,;
    Alignment=1, Width=22, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_67 as StdString with uid="MSYFITATCU",Visible=.t., Left=182, Top=149,;
    Alignment=1, Width=22, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_68 as StdString with uid="DKPVJOYCGC",Visible=.t., Left=289, Top=123,;
    Alignment=1, Width=80, Height=18,;
    Caption="Da doc. n.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_69 as StdString with uid="XCVVRPDOFL",Visible=.t., Left=300, Top=149,;
    Alignment=1, Width=69, Height=18,;
    Caption="A doc. n.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_70 as StdString with uid="DLGWUZZOTK",Visible=.t., Left=591, Top=124,;
    Alignment=1, Width=22, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_71 as StdString with uid="JGWMBFEVVL",Visible=.t., Left=591, Top=153,;
    Alignment=1, Width=22, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_72 as StdString with uid="WMPAYGEVJZ",Visible=.t., Left=13, Top=173,;
    Alignment=1, Width=59, Height=18,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oBox_1_27 as StdBox with uid="XKFADLCDCH",left=9, top=29, width=429,height=1

  add object oBox_1_31 as StdBox with uid="NMYQAVJPOJ",left=463, top=208, width=334,height=1

  add object oBox_1_53 as StdBox with uid="PXBWCLDVXD",left=463, top=29, width=334,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_kvd','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
