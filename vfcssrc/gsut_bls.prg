* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bls                                                        *
*              Lancia report                                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-09-20                                                      *
* Last revis.: 2011-10-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bls",oParentObject)
return(i_retval)

define class tgsut_bls as StdBatch
  * --- Local variables
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia report da bottone di stampa ma senza il  "Tipo di stampa" (hide)  -   Si veda ad es. in GSUT_KFG
    if NOT EMPTY(THIS.oParentObject.w_OQRY)
      do case
        case upper(THIS.oParentObject.class)="TGSUT_KFG"
          L_CODCLA = this.oParentObject.w_SELCODCLA
          L_DESCLA = this.oParentObject.w_DESCLA
          L_TIPOALL = this.oParentObject.w_TIPOALL
          L_DESTIPO = this.oParentObject.w_DESTIPO
          L_CLASALL = this.oParentObject.w_CLASALL
          L_DESCLAAL = this.oParentObject.w_DESCLAAL
          L_SELNOMEORIG = this.oParentObject.w_SELNOMEORIG
          L_SELNOMEFILE = this.oParentObject.w_SELNOMEFILE
        case upper(THIS.oParentObject.class)="TGSAL_KCR"
          L_REGINI = this.oParentObject.w_REGINI
          L_REGFIN = this.oParentObject.w_REGFIN
          L_CONTRO = this.oParentObject.w_CONTRO
          L_DESCTR = this.oParentObject.w_DESCTR
          L_TOTRICA = this.oParentObject.w_TOTRICA
          L_TOTRICA_SL = this.oParentObject.w_TOTRICA_SL
          L_TOTRICA_SP = this.oParentObject.w_TOTRICA_SP
          L_TOTRICA_DS = this.oParentObject.w_TOTRICA_DS
          L_TOTRICA_DP = this.oParentObject.w_TOTRICA_DP
          L_TOTCOST = this.oParentObject.w_TOTCOST
          L_TOTCOST_SL = this.oParentObject.w_TOTCOST_SL
          L_TOTCOST_SP = this.oParentObject.w_TOTCOST_SP
          L_TOTCOST_DS = this.oParentObject.w_TOTCOST_DS
          L_TOTCOST_DP = this.oParentObject.w_TOTCOST_DP
          L_TOTDIFF = this.oParentObject.w_TOTDIFF
          L_TOTDIFF_SL = this.oParentObject.w_TOTDIFF_SL
          L_TOTDIFF_SP = this.oParentObject.w_TOTDIFF_SP
          L_TOTDIFF_DS = this.oParentObject.w_TOTDIFF_DS
          L_TOTDIFF_DP = this.oParentObject.w_TOTDIFF_DP
          L_TOTSCAD = this.oParentObject.w_TOTSCAD
          L_TOTSCAD_SL = this.oParentObject.w_TOTSCAD_SL
          L_TOTSCAD_SP = this.oParentObject.w_TOTSCAD_SP
          L_TOTSCAD_DS = this.oParentObject.w_TOTSCAD_DS
          L_TOTSCAD_DP = this.oParentObject.w_TOTSCAD_DP
          L_VIS_TOTSCAD=this.oParentObject.w_VIS_TOTSCAD
           
 L_ESEPRE=this.oParentObject.w_ESEPRE 
 L_CODESE=this.oParentObject.w_CODESE 
 L_CODESES=this.oParentObject.w_CODESES 
 L_REGINIS= this.oParentObject.w_REGINIS 
 L_REGFINS = this.oParentObject.w_REGFINS
      endcase
      vx_exec(""+alltrim(THIS.oParentObject.w_OQRY)+", "+alltrim(THIS.oParentObject.w_orep)+"",this.oParentObject)
    else
      ah_ErrorMsg("Impossibile eseguire la stampa: � necessario caricare l'output utente relativo al programma "+THIS.oParentObject.cPrg,,"")
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
