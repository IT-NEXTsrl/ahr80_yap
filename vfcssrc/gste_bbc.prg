* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bbc                                                        *
*              Controlli castelletto banche                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_5]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-05-05                                                      *
* Last revis.: 2005-05-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bbc",oParentObject)
return(i_retval)

define class tgste_bbc as StdBatch
  * --- Local variables
  w_MESS = space(200)
  * --- WorkFile variables
  BAN_CAST_idx=0
  COC_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo alla replace end del detail BAN_CAST (Castelletto banche)
    this.w_MESS = " "
    * --- Select from gste_bbc
    do vq_exec with 'gste_bbc',this,'_Curs_gste_bbc','',.f.,.t.
    if used('_Curs_gste_bbc')
      select _Curs_gste_bbc
      locate for 1=1
      do while not(eof())
      this.w_MESS = ah_Msgformat("Codice banca incongruente")
      EXIT
        select _Curs_gste_bbc
        continue
      enddo
      use
    endif
    if NOT EMPTY(this.w_MESS)
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='BAN_CAST'
    this.cWorkTables[2]='COC_MAST'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_gste_bbc')
      use in _Curs_gste_bbc
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
