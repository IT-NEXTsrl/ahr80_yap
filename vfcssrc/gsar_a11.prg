* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_a11                                                        *
*              Parametri fattura CBI                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [39] [VRS_1]                                                    *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-24                                                      *
* Last revis.: 2012-01-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsar_a11")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsar_a11")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsar_a11")
  return

* --- Class definition
define class tgsar_a11 as StdPCForm
  Width  = 615
  Height = 193
  Top    = 93
  Left   = 22
  cComment = "Parametri fattura CBI"
  cPrg = "gsar_a11"
  HelpContextID=125208727
  add object cnt as tcgsar_a11
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsar_a11 as PCContext
  w_COCODAZI = space(5)
  w_RAGAZI = space(40)
  w_UTCC = 0
  w_COSTRCBI = space(10)
  w_RBANRIF = space(10)
  w_EBANRIF = space(10)
  w_COBANPRO = space(15)
  w_COTIPBAC = space(1)
  w_COPATCBI = space(50)
  w_COFILCBI = space(254)
  w_COALLCBI = space(254)
  w_UTCV = 0
  w_UTDC = space(8)
  w_UTDV = space(8)
  w_OBTEST = space(8)
  w_DATOBSO = space(8)
  w_DESSTR = space(40)
  w_FLGCBI = space(1)
  w_RDESBAN = space(35)
  w_EDESBAN = space(35)
  w_OBTEST = space(10)
  w_LPATH = space(1)
  proc Save(oFrom)
    this.w_COCODAZI = oFrom.w_COCODAZI
    this.w_RAGAZI = oFrom.w_RAGAZI
    this.w_UTCC = oFrom.w_UTCC
    this.w_COSTRCBI = oFrom.w_COSTRCBI
    this.w_RBANRIF = oFrom.w_RBANRIF
    this.w_EBANRIF = oFrom.w_EBANRIF
    this.w_COBANPRO = oFrom.w_COBANPRO
    this.w_COTIPBAC = oFrom.w_COTIPBAC
    this.w_COPATCBI = oFrom.w_COPATCBI
    this.w_COFILCBI = oFrom.w_COFILCBI
    this.w_COALLCBI = oFrom.w_COALLCBI
    this.w_UTCV = oFrom.w_UTCV
    this.w_UTDC = oFrom.w_UTDC
    this.w_UTDV = oFrom.w_UTDV
    this.w_OBTEST = oFrom.w_OBTEST
    this.w_DATOBSO = oFrom.w_DATOBSO
    this.w_DESSTR = oFrom.w_DESSTR
    this.w_FLGCBI = oFrom.w_FLGCBI
    this.w_RDESBAN = oFrom.w_RDESBAN
    this.w_EDESBAN = oFrom.w_EDESBAN
    this.w_OBTEST = oFrom.w_OBTEST
    this.w_LPATH = oFrom.w_LPATH
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_COCODAZI = this.w_COCODAZI
    oTo.w_RAGAZI = this.w_RAGAZI
    oTo.w_UTCC = this.w_UTCC
    oTo.w_COSTRCBI = this.w_COSTRCBI
    oTo.w_RBANRIF = this.w_RBANRIF
    oTo.w_EBANRIF = this.w_EBANRIF
    oTo.w_COBANPRO = this.w_COBANPRO
    oTo.w_COTIPBAC = this.w_COTIPBAC
    oTo.w_COPATCBI = this.w_COPATCBI
    oTo.w_COFILCBI = this.w_COFILCBI
    oTo.w_COALLCBI = this.w_COALLCBI
    oTo.w_UTCV = this.w_UTCV
    oTo.w_UTDC = this.w_UTDC
    oTo.w_UTDV = this.w_UTDV
    oTo.w_OBTEST = this.w_OBTEST
    oTo.w_DATOBSO = this.w_DATOBSO
    oTo.w_DESSTR = this.w_DESSTR
    oTo.w_FLGCBI = this.w_FLGCBI
    oTo.w_RDESBAN = this.w_RDESBAN
    oTo.w_EDESBAN = this.w_EDESBAN
    oTo.w_OBTEST = this.w_OBTEST
    oTo.w_LPATH = this.w_LPATH
    PCContext::Load(oTo)
enddefine

define class tcgsar_a11 as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 615
  Height = 193
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-01-02"
  HelpContextID=125208727
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=22

  * --- Constant Properties
  CONTROPA_IDX = 0
  AZIENDA_IDX = 0
  VASTRUTT_IDX = 0
  COC_MAST_IDX = 0
  cFile = "CONTROPA"
  cKeySelect = "COCODAZI"
  cKeyWhere  = "COCODAZI=this.w_COCODAZI"
  cKeyWhereODBC = '"COCODAZI="+cp_ToStrODBC(this.w_COCODAZI)';

  cKeyWhereODBCqualified = '"CONTROPA.COCODAZI="+cp_ToStrODBC(this.w_COCODAZI)';

  cPrg = "gsar_a11"
  cComment = "Parametri fattura CBI"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_COCODAZI = space(5)
  w_RAGAZI = space(40)
  w_UTCC = 0
  w_COSTRCBI = space(10)
  w_RBANRIF = space(10)
  w_EBANRIF = space(10)
  w_COBANPRO = space(15)
  w_COTIPBAC = space(1)
  w_COPATCBI = space(50)
  o_COPATCBI = space(50)
  w_COFILCBI = space(254)
  w_COALLCBI = space(254)
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_DESSTR = space(40)
  w_FLGCBI = space(1)
  w_RDESBAN = space(35)
  w_EDESBAN = space(35)
  w_OBTEST = space(10)
  w_LPATH = .F.
  * --- Area Manuale = Declare Variables
  * --- gsar_a11
  * --- Disabilita il Caricamento e la Cancellazione sulla Toolbar
  proc SetCPToolBar()
          doDefault()
          oCpToolBar.b3.enabled=.f.
          oCpToolBar.b5.enabled=.f.
  endproc
  * ---- Disattiva i metodi Load e Delete (posso solo variare)
  proc ecpLoad()
      * ----
  endproc
  proc ecpDelete()
      * ----
  endproc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_a11Pag1","gsar_a11",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Fattura CBI")
      .Pages(1).HelpContextID = 247003994
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCOSTRCBI_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='VASTRUTT'
    this.cWorkTables[3]='COC_MAST'
    this.cWorkTables[4]='CONTROPA'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CONTROPA_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CONTROPA_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsar_a11'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_1_joined
    link_1_1_joined=.f.
    local link_1_4_joined
    link_1_4_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- gsar_a11
    this.w_cocodazi=i_codazi
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from CONTROPA where COCODAZI=KeySet.COCODAZI
    *
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CONTROPA')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CONTROPA.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CONTROPA '
      link_1_1_joined=this.AddJoinedLink_1_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_4_joined=this.AddJoinedLink_1_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'COCODAZI',this.w_COCODAZI  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_RAGAZI = space(40)
        .w_RBANRIF = space(10)
        .w_EBANRIF = space(10)
        .w_OBTEST = i_datsys
        .w_DATOBSO = ctod("  /  /  ")
        .w_DESSTR = space(40)
        .w_FLGCBI = space(1)
        .w_RDESBAN = space(35)
        .w_EDESBAN = space(35)
        .w_LPATH = .f.
        .w_COCODAZI = NVL(COCODAZI,space(5))
          if link_1_1_joined
            this.w_COCODAZI = NVL(AZCODAZI101,NVL(this.w_COCODAZI,space(5)))
            this.w_RAGAZI = NVL(AZRAGAZI101,space(40))
          else
          .link_1_1('Load')
          endif
        .w_UTCC = NVL(UTCC,0)
        .w_COSTRCBI = NVL(COSTRCBI,space(10))
          if link_1_4_joined
            this.w_COSTRCBI = NVL(STCODICE104,NVL(this.w_COSTRCBI,space(10)))
            this.w_DESSTR = NVL(STDESCRI104,space(40))
            this.w_FLGCBI = NVL(STFLGCBI104,space(1))
          else
          .link_1_4('Load')
          endif
          .link_1_5('Load')
          .link_1_6('Load')
        .w_COBANPRO = NVL(COBANPRO,space(15))
        .w_COTIPBAC = NVL(COTIPBAC,space(1))
        .w_COPATCBI = NVL(COPATCBI,space(50))
        .w_COFILCBI = NVL(COFILCBI,space(254))
        .w_COALLCBI = NVL(COALLCBI,space(254))
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
        .w_OBTEST = i_DATSYS
        cp_LoadRecExtFlds(this,'CONTROPA')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsar_a11
    if this.w_cocodazi<>i_codazi and not empty(this.w_cocodazi)
     this.blankrec()
     this.w_cocodazi=""
     ah_ErrorMsg("Manutenzione consentita alla sola azienda corrente (%1)",,'',i_CODAZI)
    endif
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_COCODAZI = space(5)
      .w_RAGAZI = space(40)
      .w_UTCC = 0
      .w_COSTRCBI = space(10)
      .w_RBANRIF = space(10)
      .w_EBANRIF = space(10)
      .w_COBANPRO = space(15)
      .w_COTIPBAC = space(1)
      .w_COPATCBI = space(50)
      .w_COFILCBI = space(254)
      .w_COALLCBI = space(254)
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_DESSTR = space(40)
      .w_FLGCBI = space(1)
      .w_RDESBAN = space(35)
      .w_EDESBAN = space(35)
      .w_OBTEST = space(10)
      .w_LPATH = .f.
      if .cFunction<>"Filter"
        .w_COCODAZI = i_codazi
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_COCODAZI))
          .link_1_1('Full')
          endif
        .DoRTCalc(2,4,.f.)
          if not(empty(.w_COSTRCBI))
          .link_1_4('Full')
          endif
        .DoRTCalc(5,5,.f.)
          if not(empty(.w_RBANRIF))
          .link_1_5('Full')
          endif
        .DoRTCalc(6,6,.f.)
          if not(empty(.w_EBANRIF))
          .link_1_6('Full')
          endif
        .w_COBANPRO = IIF(g_APPLICATION="ADHOC REVOLUTION",.w_RBANRIF,.w_EBANRIF)
        .w_COTIPBAC = 'C'
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
          .DoRTCalc(9,14,.f.)
        .w_OBTEST = i_datsys
          .DoRTCalc(16,20,.f.)
        .w_OBTEST = i_DATSYS
      endif
    endwith
    cp_BlankRecExtFlds(this,'CONTROPA')
    this.DoRTCalc(22,22,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oCOSTRCBI_1_4.enabled = i_bVal
      .Page1.oPag.oRBANRIF_1_5.enabled = i_bVal
      .Page1.oPag.oEBANRIF_1_6.enabled = i_bVal
      .Page1.oPag.oCOTIPBAC_1_8.enabled = i_bVal
      .Page1.oPag.oCOPATCBI_1_9.enabled = i_bVal
      .Page1.oPag.oCOFILCBI_1_10.enabled = i_bVal
      .Page1.oPag.oCOALLCBI_1_11.enabled = i_bVal
      .Page1.oPag.oBtn_1_21.enabled = .Page1.oPag.oBtn_1_21.mCond()
    endwith
    cp_SetEnabledExtFlds(this,'CONTROPA',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCODAZI,"COCODAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COSTRCBI,"COSTRCBI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COBANPRO,"COBANPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COTIPBAC,"COTIPBAC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COPATCBI,"COPATCBI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COFILCBI,"COFILCBI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COALLCBI,"COALLCBI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CONTROPA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.CONTROPA_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CONTROPA
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CONTROPA')
        i_extval=cp_InsertValODBCExtFlds(this,'CONTROPA')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(COCODAZI,UTCC,COSTRCBI,COBANPRO,COTIPBAC"+;
                  ",COPATCBI,COFILCBI,COALLCBI,UTCV,UTDC"+;
                  ",UTDV "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_COCODAZI)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBCNull(this.w_COSTRCBI)+;
                  ","+cp_ToStrODBC(this.w_COBANPRO)+;
                  ","+cp_ToStrODBC(this.w_COTIPBAC)+;
                  ","+cp_ToStrODBC(this.w_COPATCBI)+;
                  ","+cp_ToStrODBC(this.w_COFILCBI)+;
                  ","+cp_ToStrODBC(this.w_COALLCBI)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CONTROPA')
        i_extval=cp_InsertValVFPExtFlds(this,'CONTROPA')
        cp_CheckDeletedKey(i_cTable,0,'COCODAZI',this.w_COCODAZI)
        INSERT INTO (i_cTable);
              (COCODAZI,UTCC,COSTRCBI,COBANPRO,COTIPBAC,COPATCBI,COFILCBI,COALLCBI,UTCV,UTDC,UTDV  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_COCODAZI;
                  ,this.w_UTCC;
                  ,this.w_COSTRCBI;
                  ,this.w_COBANPRO;
                  ,this.w_COTIPBAC;
                  ,this.w_COPATCBI;
                  ,this.w_COFILCBI;
                  ,this.w_COALLCBI;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.CONTROPA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.CONTROPA_IDX,i_nConn)
      *
      * update CONTROPA
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'CONTROPA')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",COSTRCBI="+cp_ToStrODBCNull(this.w_COSTRCBI)+;
             ",COBANPRO="+cp_ToStrODBC(this.w_COBANPRO)+;
             ",COTIPBAC="+cp_ToStrODBC(this.w_COTIPBAC)+;
             ",COPATCBI="+cp_ToStrODBC(this.w_COPATCBI)+;
             ",COFILCBI="+cp_ToStrODBC(this.w_COFILCBI)+;
             ",COALLCBI="+cp_ToStrODBC(this.w_COALLCBI)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'CONTROPA')
        i_cWhere = cp_PKFox(i_cTable  ,'COCODAZI',this.w_COCODAZI  )
        UPDATE (i_cTable) SET;
              UTCC=this.w_UTCC;
             ,COSTRCBI=this.w_COSTRCBI;
             ,COBANPRO=this.w_COBANPRO;
             ,COTIPBAC=this.w_COTIPBAC;
             ,COPATCBI=this.w_COPATCBI;
             ,COFILCBI=this.w_COFILCBI;
             ,COALLCBI=this.w_COALLCBI;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CONTROPA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.CONTROPA_IDX,i_nConn)
      *
      * delete CONTROPA
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'COCODAZI',this.w_COCODAZI  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,6,.t.)
            .w_COBANPRO = IIF(g_APPLICATION="ADHOC REVOLUTION",.w_RBANRIF,.w_EBANRIF)
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
        .DoRTCalc(8,20,.t.)
            .w_OBTEST = i_DATSYS
        if .o_COPATCBI<>.w_COPATCBI
          .Calculate_FORBCAQGLL()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(22,22,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
    endwith
  return

  proc Calculate_DYEAIUKFHP()
    with this
          * --- Assegnamento banca
          .w_RBANRIF = IIF(!Isahe(),.w_COBANPRO,'')
          .link_1_5('Full')
          .w_EBANRIF = IIF(Isahe(),.w_COBANPRO,'')
          .link_1_6('Full')
    endwith
  endproc
  proc Calculate_FORBCAQGLL()
    with this
          * --- Controllo path generazione CBI
          .w_COPATCBI = IIF(right(alltrim(.w_COPATCBI),1)='\' or empty(.w_COPATCBI),.w_COPATCBI,alltrim(.w_COPATCBI)+iif(len(alltrim(.w_COPATCBI))=1,':\','\' ) )
          .w_LPATH = chknfile(.w_COPATCBI,'F')
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oRBANRIF_1_5.visible=!this.oPgFrm.Page1.oPag.oRBANRIF_1_5.mHide()
    this.oPgFrm.Page1.oPag.oEBANRIF_1_6.visible=!this.oPgFrm.Page1.oPag.oEBANRIF_1_6.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_22.visible=!this.oPgFrm.Page1.oPag.oStr_1_22.mHide()
    this.oPgFrm.Page1.oPag.oRDESBAN_1_27.visible=!this.oPgFrm.Page1.oPag.oRDESBAN_1_27.mHide()
    this.oPgFrm.Page1.oPag.oEDESBAN_1_28.visible=!this.oPgFrm.Page1.oPag.oEDESBAN_1_28.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_15.Event(cEvent)
        if lower(cEvent)==lower("Load")
          .Calculate_DYEAIUKFHP()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=COCODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_COCODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_COCODAZI)
            select AZCODAZI,AZRAGAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_RAGAZI = NVL(_Link_.AZRAGAZI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_COCODAZI = space(5)
      endif
      this.w_RAGAZI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.AZIENDA_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_1.AZCODAZI as AZCODAZI101"+ ",link_1_1.AZRAGAZI as AZRAGAZI101"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_1 on CONTROPA.COCODAZI=link_1_1.AZCODAZI"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_1"
          i_cKey=i_cKey+'+" and CONTROPA.COCODAZI=link_1_1.AZCODAZI(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COSTRCBI
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VASTRUTT_IDX,3]
    i_lTable = "VASTRUTT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2], .t., this.VASTRUTT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COSTRCBI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'VASTRUTT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" STCODICE like "+cp_ToStrODBC(trim(this.w_COSTRCBI)+"%");

          i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI,STFLGCBI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by STCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'STCODICE',trim(this.w_COSTRCBI))
          select STCODICE,STDESCRI,STFLGCBI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by STCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COSTRCBI)==trim(_Link_.STCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" STDESCRI like "+cp_ToStrODBC(trim(this.w_COSTRCBI)+"%");

            i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI,STFLGCBI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" STDESCRI like "+cp_ToStr(trim(this.w_COSTRCBI)+"%");

            select STCODICE,STDESCRI,STFLGCBI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COSTRCBI) and !this.bDontReportError
            deferred_cp_zoom('VASTRUTT','*','STCODICE',cp_AbsName(oSource.parent,'oCOSTRCBI_1_4'),i_cWhere,'',"Strutture CBI",'gsar_k11.VASTRUTT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI,STFLGCBI";
                     +" from "+i_cTable+" "+i_lTable+" where STCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'STCODICE',oSource.xKey(1))
            select STCODICE,STDESCRI,STFLGCBI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COSTRCBI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI,STFLGCBI";
                   +" from "+i_cTable+" "+i_lTable+" where STCODICE="+cp_ToStrODBC(this.w_COSTRCBI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'STCODICE',this.w_COSTRCBI)
            select STCODICE,STDESCRI,STFLGCBI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COSTRCBI = NVL(_Link_.STCODICE,space(10))
      this.w_DESSTR = NVL(_Link_.STDESCRI,space(40))
      this.w_FLGCBI = NVL(_Link_.STFLGCBI,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_COSTRCBI = space(10)
      endif
      this.w_DESSTR = space(40)
      this.w_FLGCBI = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLGCBI='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Attenzione struttura non di tipo CBI")
        endif
        this.w_COSTRCBI = space(10)
        this.w_DESSTR = space(40)
        this.w_FLGCBI = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])+'\'+cp_ToStr(_Link_.STCODICE,1)
      cp_ShowWarn(i_cKey,this.VASTRUTT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COSTRCBI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VASTRUTT_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_4.STCODICE as STCODICE104"+ ",link_1_4.STDESCRI as STDESCRI104"+ ",link_1_4.STFLGCBI as STFLGCBI104"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_4 on CONTROPA.COSTRCBI=link_1_4.STCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_4"
          i_cKey=i_cKey+'+" and CONTROPA.COSTRCBI=link_1_4.STCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=RBANRIF
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RBANRIF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_RBANRIF)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_RBANRIF))
          select BACODBAN,BADESCRI,BADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RBANRIF)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RBANRIF) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oRBANRIF_1_5'),i_cWhere,'GSTE_ACB',"Conti banche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESCRI,BADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RBANRIF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_RBANRIF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_RBANRIF)
            select BACODBAN,BADESCRI,BADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RBANRIF = NVL(_Link_.BACODBAN,space(10))
      this.w_RDESBAN = NVL(_Link_.BADESCRI,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.BADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_RBANRIF = space(10)
      endif
      this.w_RDESBAN = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RBANRIF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=EBANRIF
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EBANRIF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACC',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BANUMCOR like "+cp_ToStrODBC(trim(this.w_EBANRIF)+"%");

          i_ret=cp_SQL(i_nConn,"select BANUMCOR,BADESCRI,BADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BANUMCOR","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BANUMCOR',trim(this.w_EBANRIF))
          select BANUMCOR,BADESCRI,BADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BANUMCOR into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_EBANRIF)==trim(_Link_.BANUMCOR) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_EBANRIF) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BANUMCOR',cp_AbsName(oSource.parent,'oEBANRIF_1_6'),i_cWhere,'GSTE_ACC',"Conti di tesoreria",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BANUMCOR,BADESCRI,BADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where BANUMCOR="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BANUMCOR',oSource.xKey(1))
            select BANUMCOR,BADESCRI,BADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EBANRIF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BANUMCOR,BADESCRI,BADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where BANUMCOR="+cp_ToStrODBC(this.w_EBANRIF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BANUMCOR',this.w_EBANRIF)
            select BANUMCOR,BADESCRI,BADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EBANRIF = NVL(_Link_.BANUMCOR,space(10))
      this.w_EDESBAN = NVL(_Link_.BADESCRI,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.BADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_EBANRIF = space(10)
      endif
      this.w_EDESBAN = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_EBANRIF = space(10)
        this.w_EDESBAN = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BANUMCOR,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EBANRIF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCOSTRCBI_1_4.value==this.w_COSTRCBI)
      this.oPgFrm.Page1.oPag.oCOSTRCBI_1_4.value=this.w_COSTRCBI
    endif
    if not(this.oPgFrm.Page1.oPag.oRBANRIF_1_5.value==this.w_RBANRIF)
      this.oPgFrm.Page1.oPag.oRBANRIF_1_5.value=this.w_RBANRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oEBANRIF_1_6.value==this.w_EBANRIF)
      this.oPgFrm.Page1.oPag.oEBANRIF_1_6.value=this.w_EBANRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oCOTIPBAC_1_8.RadioValue()==this.w_COTIPBAC)
      this.oPgFrm.Page1.oPag.oCOTIPBAC_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOPATCBI_1_9.value==this.w_COPATCBI)
      this.oPgFrm.Page1.oPag.oCOPATCBI_1_9.value=this.w_COPATCBI
    endif
    if not(this.oPgFrm.Page1.oPag.oCOFILCBI_1_10.value==this.w_COFILCBI)
      this.oPgFrm.Page1.oPag.oCOFILCBI_1_10.value=this.w_COFILCBI
    endif
    if not(this.oPgFrm.Page1.oPag.oCOALLCBI_1_11.value==this.w_COALLCBI)
      this.oPgFrm.Page1.oPag.oCOALLCBI_1_11.value=this.w_COALLCBI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESSTR_1_18.value==this.w_DESSTR)
      this.oPgFrm.Page1.oPag.oDESSTR_1_18.value=this.w_DESSTR
    endif
    if not(this.oPgFrm.Page1.oPag.oRDESBAN_1_27.value==this.w_RDESBAN)
      this.oPgFrm.Page1.oPag.oRDESBAN_1_27.value=this.w_RDESBAN
    endif
    if not(this.oPgFrm.Page1.oPag.oEDESBAN_1_28.value==this.w_EDESBAN)
      this.oPgFrm.Page1.oPag.oEDESBAN_1_28.value=this.w_EDESBAN
    endif
    cp_SetControlsValueExtFlds(this,'CONTROPA')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_FLGCBI='S')  and not(empty(.w_COSTRCBI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOSTRCBI_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione struttura non di tipo CBI")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(!Isahe())  and not(empty(.w_EBANRIF))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEBANRIF_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_COPATCBI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOPATCBI_1_9.SetFocus()
            i_bnoObbl = !empty(.w_COPATCBI)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsar_a11
      if i_bRes
         IF not(empty(.w_COPATCBI) or directory(alltrim(.w_COPATCBI)))
            i_bRes = .f.
          	i_bnoChk = .f.
            i_cErrorMsg = Ah_MsgFormat("La cartella impostata per il path per generazione file CBI non esiste")
         ENDIF
      ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_COPATCBI = this.w_COPATCBI
    return

enddefine

* --- Define pages as container
define class tgsar_a11Pag1 as StdContainer
  Width  = 611
  height = 193
  stdWidth  = 611
  stdheight = 193
  resizeXpos=448
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCOSTRCBI_1_4 as StdField with uid="KSBVZCKFCZ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_COSTRCBI", cQueryName = "COSTRCBI",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione struttura non di tipo CBI",;
    ToolTipText = "Codice struttura CBI",;
    HelpContextID = 1045393,;
   bGlobalFont=.t.,;
    Height=21, Width=133, Left=156, Top=17, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="VASTRUTT", oKey_1_1="STCODICE", oKey_1_2="this.w_COSTRCBI"

  func oCOSTRCBI_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOSTRCBI_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOSTRCBI_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VASTRUTT','*','STCODICE',cp_AbsName(this.parent,'oCOSTRCBI_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Strutture CBI",'gsar_k11.VASTRUTT_VZM',this.parent.oContained
  endproc

  add object oRBANRIF_1_5 as StdField with uid="COOMTDDOKD",rtseq=5,rtrep=.f.,;
    cFormVar = "w_RBANRIF", cQueryName = "RBANRIF",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 169287658,;
   bGlobalFont=.t.,;
    Height=21, Width=133, Left=156, Top=47, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_RBANRIF"

  func oRBANRIF_1_5.mHide()
    with this.Parent.oContained
      return (Isahe())
    endwith
  endfunc

  func oRBANRIF_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oRBANRIF_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRBANRIF_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oRBANRIF_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"Conti banche",'',this.parent.oContained
  endproc
  proc oRBANRIF_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_RBANRIF
     i_obj.ecpSave()
  endproc

  add object oEBANRIF_1_6 as StdField with uid="QBOIJOXSLZ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_EBANRIF", cQueryName = "EBANRIF",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 169287866,;
   bGlobalFont=.t.,;
    Height=21, Width=133, Left=156, Top=48, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACC", oKey_1_1="BANUMCOR", oKey_1_2="this.w_EBANRIF"

  func oEBANRIF_1_6.mHide()
    with this.Parent.oContained
      return (!Isahe())
    endwith
  endfunc

  func oEBANRIF_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oEBANRIF_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oEBANRIF_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BANUMCOR',cp_AbsName(this.parent,'oEBANRIF_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACC',"Conti di tesoreria",'',this.parent.oContained
  endproc
  proc oEBANRIF_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BANUMCOR=this.parent.oContained.w_EBANRIF
     i_obj.ecpSave()
  endproc


  add object oCOTIPBAC_1_8 as StdCombo with uid="JFQRIUWQXJ",rtseq=8,rtrep=.f.,left=156,top=76,width=135,height=21;
    , ToolTipText = "Crea cartella per codice/descrizione banca nel percorso del file CBI";
    , HelpContextID = 20636567;
    , cFormVar="w_COTIPBAC",RowSource=""+"Codice banca,"+"Descrizione,"+"Nessuno", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCOTIPBAC_1_8.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'D',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oCOTIPBAC_1_8.GetRadio()
    this.Parent.oContained.w_COTIPBAC = this.RadioValue()
    return .t.
  endfunc

  func oCOTIPBAC_1_8.SetRadio()
    this.Parent.oContained.w_COTIPBAC=trim(this.Parent.oContained.w_COTIPBAC)
    this.value = ;
      iif(this.Parent.oContained.w_COTIPBAC=='C',1,;
      iif(this.Parent.oContained.w_COTIPBAC=='D',2,;
      iif(this.Parent.oContained.w_COTIPBAC=='N',3,;
      0)))
  endfunc

  add object oCOPATCBI_1_9 as StdField with uid="ELNRKBISSA",rtseq=9,rtrep=.f.,;
    cFormVar = "w_COPATCBI", cQueryName = "COPATCBI",;
    bObbl = .t. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Path per generazione file CBI",;
    HelpContextID = 205713,;
   bGlobalFont=.t.,;
    Height=21, Width=429, Left=155, Top=104, InputMask=replicate('X',50)

  add object oCOFILCBI_1_10 as StdField with uid="RLJBFGBVXL",rtseq=10,rtrep=.f.,;
    cFormVar = "w_COFILCBI", cQueryName = "COFILCBI",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Nome file per generazione CBI",;
    HelpContextID = 8110993,;
   bGlobalFont=.t.,;
    Height=21, Width=427, Left=155, Top=131, InputMask=replicate('X',254)

  add object oCOALLCBI_1_11 as StdField with uid="UZTCBDTBJB",rtseq=11,rtrep=.f.,;
    cFormVar = "w_COALLCBI", cQueryName = "COALLCBI",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Nome allegato per generazione CBI",;
    HelpContextID = 7934865,;
   bGlobalFont=.t.,;
    Height=21, Width=426, Left=155, Top=160, InputMask=replicate('X',254)


  add object oObj_1_15 as cp_runprogram with uid="WUGFWDJQQA",left=624, top=96, width=245,height=24,;
    caption='GSAR_BAM(modifica)',;
   bGlobalFont=.t.,;
    prg='GSAR_BAM("modifica")',;
    cEvent = "Record Updated",;
    nPag=1;
    , HelpContextID = 55017562

  add object oDESSTR_1_18 as StdField with uid="YNDTULZADW",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESSTR", cQueryName = "DESSTR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 15793610,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=295, Top=17, InputMask=replicate('X',40)


  add object oBtn_1_21 as StdButton with uid="AVVNWRYYOI",left=588, top=106, width=19,height=18,;
    caption="...", nPag=1;
    , HelpContextID = 125409750;
  , bGlobalFont=.t.

    proc oBtn_1_21.Click()
      with this.Parent.oContained
        .w_COPATCBI=left(cp_getdir(IIF(EMPTY(.w_COPATCBI),sys(5)+sys(2003),.w_COPATCBI),"Percorso di destinazione")+space(200),200)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oRDESBAN_1_27 as StdField with uid="ZWITLNGPEV",rtseq=19,rtrep=.f.,;
    cFormVar = "w_RDESBAN", cQueryName = "RDESBAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 216932886,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=295, Top=47, InputMask=replicate('X',35)

  func oRDESBAN_1_27.mHide()
    with this.Parent.oContained
      return (g_APPLICATION<>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oEDESBAN_1_28 as StdField with uid="JZODFOEEAK",rtseq=20,rtrep=.f.,;
    cFormVar = "w_EDESBAN", cQueryName = "EDESBAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 216932678,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=295, Top=47, InputMask=replicate('X',35)

  func oEDESBAN_1_28.mHide()
    with this.Parent.oContained
      return (g_APPLICATION = "ADHOC REVOLUTION")
    endwith
  endfunc

  add object oStr_1_19 as StdString with uid="MVPGWDBYAZ",Visible=.t., Left=7, Top=20,;
    Alignment=1, Width=147, Height=18,;
    Caption="Codice struttura CBI:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="CFWKLFGVQW",Visible=.t., Left=7, Top=49,;
    Alignment=1, Width=147, Height=18,;
    Caption="Codice banca proponente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="LDCBIROCJZ",Visible=.t., Left=7, Top=106,;
    Alignment=1, Width=147, Height=18,;
    Caption="File CBI:"  ;
  , bGlobalFont=.t.

  func oStr_1_22.mHide()
    with this.Parent.oContained
      return (g_FAEL<>'S')
    endwith
  endfunc

  add object oStr_1_23 as StdString with uid="PFDAMWKYWP",Visible=.t., Left=7, Top=76,;
    Alignment=1, Width=147, Height=18,;
    Caption="Sottocartella banche:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="FRZVXSCOEU",Visible=.t., Left=7, Top=135,;
    Alignment=1, Width=147, Height=18,;
    Caption="Nome file :"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="VGUYJFEBBO",Visible=.t., Left=7, Top=162,;
    Alignment=1, Width=147, Height=18,;
    Caption="Nome allegato :"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_a11','CONTROPA','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".COCODAZI=CONTROPA.COCODAZI";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
