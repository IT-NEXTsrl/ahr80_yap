* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bpv                                                        *
*              STAMPA PROSPETTO DEI VERSAMENTI                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_27]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-26                                                      *
* Last revis.: 2009-06-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bpv",oParentObject)
return(i_retval)

define class tgscg_bpv as StdBatch
  * --- Local variables
  w_APPO = space(3)
  w_APPO1 = 0
  * --- WorkFile variables
  VALUTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Dichirazione Variabili
    if EMPTY(this.oParentObject.w_ANNOINI) OR EMPTY(this.oParentObject.w_PERIOINI)
      ah_ErrorMsg("Inserire l'anno/periodo iniziale",,"")
      i_retcode = 'stop'
      return
    endif
    if EMPTY(this.oParentObject.w_ANNOFIN) OR EMPTY(this.oParentObject.w_PERIOFIN)
      ah_ErrorMsg("Inserire l'anno/periodo finale",,"")
      i_retcode = 'stop'
      return
    endif
    * --- Legge Decimali Euro e Lire
    this.w_APPO = g_CODLIR
    this.w_APPO1 = 0
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VADECTOT"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_APPO);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VADECTOT;
        from (i_cTable) where;
            VACODVAL = this.w_APPO;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_APPO1 = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    L_DECLIR=this.w_APPO1
    this.w_APPO = g_CODEUR
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VADECTOT"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_APPO);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VADECTOT;
        from (i_cTable) where;
            VACODVAL = this.w_APPO;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_APPO1 = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    L_DECEUR=this.w_APPO1
    VQ_EXEC("QUERY\GSCG_SPV.VQR",this,"__TMP__")
    L_CODAZI=this.oParentObject.w_CODAZI
    l_datverini=this.oParentObject.w_datverini
    l_datverfin=this.oParentObject.w_datverfin
    l_annoini=this.oParentObject.w_annoini
    l_annofin=this.oParentObject.w_annofin
    l_perioini=this.oParentObject.w_perioini
    l_periofin=this.oParentObject.w_periofin
    l_DESCRATT=IIF(g_ATTIVI="S", "<Tutte>", this.oParentObject.w_DESCRATT)
    CP_CHPRN("QUERY\GSCG_SPV.FRX", " ", this)
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='VALUTE'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
