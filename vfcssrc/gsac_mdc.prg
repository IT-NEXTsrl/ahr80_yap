* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsac_mdc                                                        *
*              Documenti collegati                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_42]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-06                                                      *
* Last revis.: 2011-03-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsac_mdc")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsac_mdc")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsac_mdc")
  return

* --- Class definition
define class tgsac_mdc as StdPCForm
  Width  = 706
  Height = 174
  Top    = 26
  Left   = 118
  cComment = "Documenti collegati"
  cPrg = "gsac_mdc"
  HelpContextID=80373609
  add object cnt as tcgsac_mdc
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsac_mdc as PCContext
  w_DCCODICE = space(5)
  w_DCCOLLEG = space(5)
  w_DESDOC = space(35)
  w_DCFLINTE = space(1)
  w_DCFLEVAS = space(1)
  w_CONCOL = space(5)
  w_MAGCOL = space(5)
  w_FISCOL = space(1)
  w_FLVEAC = space(1)
  w_DCFLACCO = space(1)
  w_FLANAL = space(1)
  w_FLELAN = space(1)
  w_FLEVAS = space(1)
  w_CATDOC = space(2)
  w_DCFLGROR = space(1)
  w_DCFLRIPS = space(1)
  proc Save(i_oFrom)
    this.w_DCCODICE = i_oFrom.w_DCCODICE
    this.w_DCCOLLEG = i_oFrom.w_DCCOLLEG
    this.w_DESDOC = i_oFrom.w_DESDOC
    this.w_DCFLINTE = i_oFrom.w_DCFLINTE
    this.w_DCFLEVAS = i_oFrom.w_DCFLEVAS
    this.w_CONCOL = i_oFrom.w_CONCOL
    this.w_MAGCOL = i_oFrom.w_MAGCOL
    this.w_FISCOL = i_oFrom.w_FISCOL
    this.w_FLVEAC = i_oFrom.w_FLVEAC
    this.w_DCFLACCO = i_oFrom.w_DCFLACCO
    this.w_FLANAL = i_oFrom.w_FLANAL
    this.w_FLELAN = i_oFrom.w_FLELAN
    this.w_FLEVAS = i_oFrom.w_FLEVAS
    this.w_CATDOC = i_oFrom.w_CATDOC
    this.w_DCFLGROR = i_oFrom.w_DCFLGROR
    this.w_DCFLRIPS = i_oFrom.w_DCFLRIPS
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_DCCODICE = this.w_DCCODICE
    i_oTo.w_DCCOLLEG = this.w_DCCOLLEG
    i_oTo.w_DESDOC = this.w_DESDOC
    i_oTo.w_DCFLINTE = this.w_DCFLINTE
    i_oTo.w_DCFLEVAS = this.w_DCFLEVAS
    i_oTo.w_CONCOL = this.w_CONCOL
    i_oTo.w_MAGCOL = this.w_MAGCOL
    i_oTo.w_FISCOL = this.w_FISCOL
    i_oTo.w_FLVEAC = this.w_FLVEAC
    i_oTo.w_DCFLACCO = this.w_DCFLACCO
    i_oTo.w_FLANAL = this.w_FLANAL
    i_oTo.w_FLELAN = this.w_FLELAN
    i_oTo.w_FLEVAS = this.w_FLEVAS
    i_oTo.w_CATDOC = this.w_CATDOC
    i_oTo.w_DCFLGROR = this.w_DCFLGROR
    i_oTo.w_DCFLRIPS = this.w_DCFLRIPS
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsac_mdc as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 706
  Height = 174
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-03-02"
  HelpContextID=80373609
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=16

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  DOC_COLL_IDX = 0
  TIP_DOCU_IDX = 0
  CAM_AGAZ_IDX = 0
  cFile = "DOC_COLL"
  cKeySelect = "DCCODICE"
  cKeyWhere  = "DCCODICE=this.w_DCCODICE"
  cKeyDetail  = "DCCODICE=this.w_DCCODICE and DCCOLLEG=this.w_DCCOLLEG"
  cKeyWhereODBC = '"DCCODICE="+cp_ToStrODBC(this.w_DCCODICE)';

  cKeyDetailWhereODBC = '"DCCODICE="+cp_ToStrODBC(this.w_DCCODICE)';
      +'+" and DCCOLLEG="+cp_ToStrODBC(this.w_DCCOLLEG)';

  cKeyWhereODBCqualified = '"DOC_COLL.DCCODICE="+cp_ToStrODBC(this.w_DCCODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gsac_mdc"
  cComment = "Documenti collegati"
  i_nRowNum = 0
  i_nRowPerPage = 8
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DCCODICE = space(5)
  w_DCCOLLEG = space(5)
  w_DESDOC = space(35)
  w_DCFLINTE = space(1)
  w_DCFLEVAS = space(1)
  o_DCFLEVAS = space(1)
  w_CONCOL = space(5)
  w_MAGCOL = space(5)
  w_FISCOL = space(1)
  w_FLVEAC = space(1)
  w_DCFLACCO = space(1)
  w_FLANAL = space(1)
  w_FLELAN = space(1)
  w_FLEVAS = space(1)
  w_CATDOC = space(2)
  w_DCFLGROR = space(1)
  w_DCFLRIPS = space(1)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsac_mdcPag1","gsac_mdc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='TIP_DOCU'
    this.cWorkTables[2]='CAM_AGAZ'
    this.cWorkTables[3]='DOC_COLL'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DOC_COLL_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DOC_COLL_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsac_mdc'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_1_joined
    link_2_1_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from DOC_COLL where DCCODICE=KeySet.DCCODICE
    *                            and DCCOLLEG=KeySet.DCCOLLEG
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.DOC_COLL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DOC_COLL_IDX,2],this.bLoadRecFilter,this.DOC_COLL_IDX,"gsac_mdc")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DOC_COLL')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DOC_COLL.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DOC_COLL '
      link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DCCODICE',this.w_DCCODICE  )
      select * from (i_cTable) DOC_COLL where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_FLVEAC = space(1)
        .w_DCCODICE = NVL(DCCODICE,space(5))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'DOC_COLL')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_DESDOC = space(35)
          .w_CONCOL = space(5)
          .w_MAGCOL = space(5)
          .w_FISCOL = space(1)
          .w_FLANAL = space(1)
          .w_FLELAN = space(1)
          .w_CATDOC = space(2)
          .w_DCCOLLEG = NVL(DCCOLLEG,space(5))
          if link_2_1_joined
            this.w_DCCOLLEG = NVL(TDTIPDOC201,NVL(this.w_DCCOLLEG,space(5)))
            this.w_DESDOC = NVL(TDDESDOC201,space(35))
            this.w_CONCOL = NVL(TDCAUCON201,space(5))
            this.w_MAGCOL = NVL(TDCAUMAG201,space(5))
            this.w_FLVEAC = NVL(TDFLVEAC201,space(1))
            this.w_FLANAL = NVL(TDFLANAL201,space(1))
            this.w_FLELAN = NVL(TDFLELAN201,space(1))
            this.w_CATDOC = NVL(TDCATDOC201,space(2))
          else
          .link_2_1('Load')
          endif
          .w_DCFLINTE = NVL(DCFLINTE,space(1))
          .w_DCFLEVAS = NVL(DCFLEVAS,space(1))
          .link_2_6('Load')
          .w_DCFLACCO = NVL(DCFLACCO,space(1))
        .w_FLEVAS = .w_DCFLEVAS
          .w_DCFLGROR = NVL(DCFLGROR,space(1))
          .w_DCFLRIPS = NVL(DCFLRIPS,space(1))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace DCCOLLEG with .w_DCCOLLEG
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_DCCODICE=space(5)
      .w_DCCOLLEG=space(5)
      .w_DESDOC=space(35)
      .w_DCFLINTE=space(1)
      .w_DCFLEVAS=space(1)
      .w_CONCOL=space(5)
      .w_MAGCOL=space(5)
      .w_FISCOL=space(1)
      .w_FLVEAC=space(1)
      .w_DCFLACCO=space(1)
      .w_FLANAL=space(1)
      .w_FLELAN=space(1)
      .w_FLEVAS=space(1)
      .w_CATDOC=space(2)
      .w_DCFLGROR=space(1)
      .w_DCFLRIPS=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        if not(empty(.w_DCCOLLEG))
         .link_2_1('Full')
        endif
        .DoRTCalc(3,7,.f.)
        if not(empty(.w_MAGCOL))
         .link_2_6('Full')
        endif
        .DoRTCalc(8,9,.f.)
        .w_DCFLACCO = ' '
        .DoRTCalc(11,12,.f.)
        .w_FLEVAS = .w_DCFLEVAS
        .DoRTCalc(14,14,.f.)
        .w_DCFLGROR = IIF(.w_DCFLEVAS=' ',' ',.w_DCFLGROR)
        .w_DCFLRIPS = 'N'
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'DOC_COLL')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'DOC_COLL',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DOC_COLL_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DCCODICE,"DCCODICE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_DCCOLLEG C(5);
      ,t_DESDOC C(35);
      ,t_DCFLINTE N(3);
      ,t_DCFLEVAS N(3);
      ,t_DCFLACCO N(3);
      ,t_DCFLGROR N(3);
      ,t_DCFLRIPS N(3);
      ,DCCOLLEG C(5);
      ,t_CONCOL C(5);
      ,t_MAGCOL C(5);
      ,t_FISCOL C(1);
      ,t_FLANAL C(1);
      ,t_FLELAN C(1);
      ,t_FLEVAS C(1);
      ,t_CATDOC C(2);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsac_mdcbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDCCOLLEG_2_1.controlsource=this.cTrsName+'.t_DCCOLLEG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESDOC_2_2.controlsource=this.cTrsName+'.t_DESDOC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDCFLINTE_2_3.controlsource=this.cTrsName+'.t_DCFLINTE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDCFLEVAS_2_4.controlsource=this.cTrsName+'.t_DCFLEVAS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDCFLACCO_2_9.controlsource=this.cTrsName+'.t_DCFLACCO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDCFLGROR_2_14.controlsource=this.cTrsName+'.t_DCFLGROR'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDCFLRIPS_2_15.controlsource=this.cTrsName+'.t_DCFLRIPS'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(69)
    this.AddVLine(332)
    this.AddVLine(404)
    this.AddVLine(477)
    this.AddVLine(550)
    this.AddVLine(623)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDCCOLLEG_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DOC_COLL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DOC_COLL_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DOC_COLL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DOC_COLL_IDX,2])
      *
      * insert into DOC_COLL
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DOC_COLL')
        i_extval=cp_InsertValODBCExtFlds(this,'DOC_COLL')
        i_cFldBody=" "+;
                  "(DCCODICE,DCCOLLEG,DCFLINTE,DCFLEVAS,DCFLACCO"+;
                  ",DCFLGROR,DCFLRIPS,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_DCCODICE)+","+cp_ToStrODBCNull(this.w_DCCOLLEG)+","+cp_ToStrODBC(this.w_DCFLINTE)+","+cp_ToStrODBC(this.w_DCFLEVAS)+","+cp_ToStrODBC(this.w_DCFLACCO)+;
             ","+cp_ToStrODBC(this.w_DCFLGROR)+","+cp_ToStrODBC(this.w_DCFLRIPS)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DOC_COLL')
        i_extval=cp_InsertValVFPExtFlds(this,'DOC_COLL')
        cp_CheckDeletedKey(i_cTable,0,'DCCODICE',this.w_DCCODICE,'DCCOLLEG',this.w_DCCOLLEG)
        INSERT INTO (i_cTable) (;
                   DCCODICE;
                  ,DCCOLLEG;
                  ,DCFLINTE;
                  ,DCFLEVAS;
                  ,DCFLACCO;
                  ,DCFLGROR;
                  ,DCFLRIPS;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_DCCODICE;
                  ,this.w_DCCOLLEG;
                  ,this.w_DCFLINTE;
                  ,this.w_DCFLEVAS;
                  ,this.w_DCFLACCO;
                  ,this.w_DCFLGROR;
                  ,this.w_DCFLRIPS;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.DOC_COLL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DOC_COLL_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (t_DCCOLLEG<>space(5)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'DOC_COLL')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and DCCOLLEG="+cp_ToStrODBC(&i_TN.->DCCOLLEG)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'DOC_COLL')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and DCCOLLEG=&i_TN.->DCCOLLEG;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_DCCOLLEG<>space(5)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and DCCOLLEG="+cp_ToStrODBC(&i_TN.->DCCOLLEG)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and DCCOLLEG=&i_TN.->DCCOLLEG;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace DCCOLLEG with this.w_DCCOLLEG
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update DOC_COLL
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'DOC_COLL')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " DCFLINTE="+cp_ToStrODBC(this.w_DCFLINTE)+;
                     ",DCFLEVAS="+cp_ToStrODBC(this.w_DCFLEVAS)+;
                     ",DCFLACCO="+cp_ToStrODBC(this.w_DCFLACCO)+;
                     ",DCFLGROR="+cp_ToStrODBC(this.w_DCFLGROR)+;
                     ",DCFLRIPS="+cp_ToStrODBC(this.w_DCFLRIPS)+;
                     ",DCCOLLEG="+cp_ToStrODBC(this.w_DCCOLLEG)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and DCCOLLEG="+cp_ToStrODBC(DCCOLLEG)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'DOC_COLL')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      DCFLINTE=this.w_DCFLINTE;
                     ,DCFLEVAS=this.w_DCFLEVAS;
                     ,DCFLACCO=this.w_DCFLACCO;
                     ,DCFLGROR=this.w_DCFLGROR;
                     ,DCFLRIPS=this.w_DCFLRIPS;
                     ,DCCOLLEG=this.w_DCCOLLEG;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and DCCOLLEG=&i_TN.->DCCOLLEG;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DOC_COLL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DOC_COLL_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_DCCOLLEG<>space(5)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete DOC_COLL
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and DCCOLLEG="+cp_ToStrODBC(&i_TN.->DCCOLLEG)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and DCCOLLEG=&i_TN.->DCCOLLEG;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_DCCOLLEG<>space(5)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DOC_COLL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DOC_COLL_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,6,.t.)
          .link_2_6('Full')
        .DoRTCalc(8,9,.t.)
        if .o_DCFLEVAS<>.w_DCFLEVAS
          .w_DCFLACCO = ' '
        endif
        .DoRTCalc(11,12,.t.)
          .w_FLEVAS = .w_DCFLEVAS
        .DoRTCalc(14,14,.t.)
        if .o_DCFLEVAS<>.w_DCFLEVAS
          .w_DCFLGROR = IIF(.w_DCFLEVAS=' ',' ',.w_DCFLGROR)
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(16,16,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_CONCOL with this.w_CONCOL
      replace t_MAGCOL with this.w_MAGCOL
      replace t_FISCOL with this.w_FISCOL
      replace t_FLANAL with this.w_FLANAL
      replace t_FLELAN with this.w_FLELAN
      replace t_FLEVAS with this.w_FLEVAS
      replace t_CATDOC with this.w_CATDOC
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDCFLACCO_2_9.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDCFLACCO_2_9.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDCFLGROR_2_14.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDCFLGROR_2_14.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DCCOLLEG
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DCCOLLEG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_DCCOLLEG)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCAUCON,TDCAUMAG,TDFLVEAC,TDFLANAL,TDFLELAN,TDCATDOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_DCCOLLEG))
          select TDTIPDOC,TDDESDOC,TDCAUCON,TDCAUMAG,TDFLVEAC,TDFLANAL,TDFLELAN,TDCATDOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DCCOLLEG)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DCCOLLEG) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oDCCOLLEG_2_1'),i_cWhere,'',"Documenti collegati",'GSVE_MDC.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCAUCON,TDCAUMAG,TDFLVEAC,TDFLANAL,TDFLELAN,TDCATDOC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDCAUCON,TDCAUMAG,TDFLVEAC,TDFLANAL,TDFLELAN,TDCATDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DCCOLLEG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCAUCON,TDCAUMAG,TDFLVEAC,TDFLANAL,TDFLELAN,TDCATDOC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_DCCOLLEG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_DCCOLLEG)
            select TDTIPDOC,TDDESDOC,TDCAUCON,TDCAUMAG,TDFLVEAC,TDFLANAL,TDFLELAN,TDCATDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DCCOLLEG = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESDOC = NVL(_Link_.TDDESDOC,space(35))
      this.w_CONCOL = NVL(_Link_.TDCAUCON,space(5))
      this.w_MAGCOL = NVL(_Link_.TDCAUMAG,space(5))
      this.w_FLVEAC = NVL(_Link_.TDFLVEAC,space(1))
      this.w_FLANAL = NVL(_Link_.TDFLANAL,space(1))
      this.w_FLELAN = NVL(_Link_.TDFLELAN,space(1))
      this.w_CATDOC = NVL(_Link_.TDCATDOC,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_DCCOLLEG = space(5)
      endif
      this.w_DESDOC = space(35)
      this.w_CONCOL = space(5)
      this.w_MAGCOL = space(5)
      this.w_FLVEAC = space(1)
      this.w_FLANAL = space(1)
      this.w_FLELAN = space(1)
      this.w_CATDOC = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DCCOLLEG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 8 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIP_DOCU_IDX,3] and i_nFlds+8<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.TDTIPDOC as TDTIPDOC201"+ ",link_2_1.TDDESDOC as TDDESDOC201"+ ",link_2_1.TDCAUCON as TDCAUCON201"+ ",link_2_1.TDCAUMAG as TDCAUMAG201"+ ",link_2_1.TDFLVEAC as TDFLVEAC201"+ ",link_2_1.TDFLANAL as TDFLANAL201"+ ",link_2_1.TDFLELAN as TDFLELAN201"+ ",link_2_1.TDCATDOC as TDCATDOC201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on DOC_COLL.DCCOLLEG=link_2_1.TDTIPDOC"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and DOC_COLL.DCCOLLEG=link_2_1.TDTIPDOC(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MAGCOL
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MAGCOL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MAGCOL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMFLELGM";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_MAGCOL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_MAGCOL)
            select CMCODICE,CMFLELGM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MAGCOL = NVL(_Link_.CMCODICE,space(5))
      this.w_FISCOL = NVL(_Link_.CMFLELGM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MAGCOL = space(5)
      endif
      this.w_FISCOL = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MAGCOL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDCCOLLEG_2_1.value==this.w_DCCOLLEG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDCCOLLEG_2_1.value=this.w_DCCOLLEG
      replace t_DCCOLLEG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDCCOLLEG_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESDOC_2_2.value==this.w_DESDOC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESDOC_2_2.value=this.w_DESDOC
      replace t_DESDOC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESDOC_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDCFLINTE_2_3.RadioValue()==this.w_DCFLINTE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDCFLINTE_2_3.SetRadio()
      replace t_DCFLINTE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDCFLINTE_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDCFLEVAS_2_4.RadioValue()==this.w_DCFLEVAS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDCFLEVAS_2_4.SetRadio()
      replace t_DCFLEVAS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDCFLEVAS_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDCFLACCO_2_9.RadioValue()==this.w_DCFLACCO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDCFLACCO_2_9.SetRadio()
      replace t_DCFLACCO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDCFLACCO_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDCFLGROR_2_14.RadioValue()==this.w_DCFLGROR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDCFLGROR_2_14.SetRadio()
      replace t_DCFLGROR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDCFLGROR_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDCFLRIPS_2_15.RadioValue()==this.w_DCFLRIPS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDCFLRIPS_2_15.SetRadio()
      replace t_DCFLRIPS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDCFLRIPS_2_15.value
    endif
    cp_SetControlsValueExtFlds(this,'DOC_COLL')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .w_DCCOLLEG<>space(5)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DCFLEVAS = this.w_DCFLEVAS
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_DCCOLLEG<>space(5))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_DCCOLLEG=space(5)
      .w_DESDOC=space(35)
      .w_DCFLINTE=space(1)
      .w_DCFLEVAS=space(1)
      .w_CONCOL=space(5)
      .w_MAGCOL=space(5)
      .w_FISCOL=space(1)
      .w_DCFLACCO=space(1)
      .w_FLANAL=space(1)
      .w_FLELAN=space(1)
      .w_FLEVAS=space(1)
      .w_CATDOC=space(2)
      .w_DCFLGROR=space(1)
      .w_DCFLRIPS=space(1)
      .DoRTCalc(1,2,.f.)
      if not(empty(.w_DCCOLLEG))
        .link_2_1('Full')
      endif
      .DoRTCalc(3,7,.f.)
      if not(empty(.w_MAGCOL))
        .link_2_6('Full')
      endif
      .DoRTCalc(8,9,.f.)
        .w_DCFLACCO = ' '
      .DoRTCalc(11,12,.f.)
        .w_FLEVAS = .w_DCFLEVAS
      .DoRTCalc(14,14,.f.)
        .w_DCFLGROR = IIF(.w_DCFLEVAS=' ',' ',.w_DCFLGROR)
        .w_DCFLRIPS = 'N'
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_DCCOLLEG = t_DCCOLLEG
    this.w_DESDOC = t_DESDOC
    this.w_DCFLINTE = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDCFLINTE_2_3.RadioValue(.t.)
    this.w_DCFLEVAS = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDCFLEVAS_2_4.RadioValue(.t.)
    this.w_CONCOL = t_CONCOL
    this.w_MAGCOL = t_MAGCOL
    this.w_FISCOL = t_FISCOL
    this.w_DCFLACCO = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDCFLACCO_2_9.RadioValue(.t.)
    this.w_FLANAL = t_FLANAL
    this.w_FLELAN = t_FLELAN
    this.w_FLEVAS = t_FLEVAS
    this.w_CATDOC = t_CATDOC
    this.w_DCFLGROR = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDCFLGROR_2_14.RadioValue(.t.)
    this.w_DCFLRIPS = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDCFLRIPS_2_15.RadioValue(.t.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_DCCOLLEG with this.w_DCCOLLEG
    replace t_DESDOC with this.w_DESDOC
    replace t_DCFLINTE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDCFLINTE_2_3.ToRadio()
    replace t_DCFLEVAS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDCFLEVAS_2_4.ToRadio()
    replace t_CONCOL with this.w_CONCOL
    replace t_MAGCOL with this.w_MAGCOL
    replace t_FISCOL with this.w_FISCOL
    replace t_DCFLACCO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDCFLACCO_2_9.ToRadio()
    replace t_FLANAL with this.w_FLANAL
    replace t_FLELAN with this.w_FLELAN
    replace t_FLEVAS with this.w_FLEVAS
    replace t_CATDOC with this.w_CATDOC
    replace t_DCFLGROR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDCFLGROR_2_14.ToRadio()
    replace t_DCFLRIPS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDCFLRIPS_2_15.ToRadio()
    if i_srv='A'
      replace DCCOLLEG with this.w_DCCOLLEG
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsac_mdcPag1 as StdContainer
  Width  = 702
  height = 174
  stdWidth  = 702
  stdheight = 174
  resizeXpos=258
  resizeYpos=135
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=10, top=0, width=690,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=7,Field1="DCCOLLEG",Label1="Codice",Field2="DESDOC",Label2="Descrizione",Field3="DCFLINTE",Label3="Intestatario",Field4="DCFLEVAS",Label4="Evasione",Field5="DCFLACCO",Label5="Fatt.anticipo",Field6="DCFLGROR",Label6="Raggruppa",Field7="DCFLRIPS",Label7="Ricalcola",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 235717242

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=0,top=19,;
    width=686+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*8*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=1,top=20,width=685+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*8*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='TIP_DOCU|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='TIP_DOCU'
        oDropInto=this.oBodyCol.oRow.oDCCOLLEG_2_1
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsac_mdcBodyRow as CPBodyRowCnt
  Width=676
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oDCCOLLEG_2_1 as StdTrsField with uid="ZJYKTOWLAT",rtseq=2,rtrep=.t.,;
    cFormVar="w_DCCOLLEG",value=space(5),isprimarykey=.t.,;
    ToolTipText = "Codice del documento collegato",;
    HelpContextID = 206114941,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=58, Left=-2, Top=0, cSayPict=["!!!!!"], cGetPict=["!!!!!"], InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_DCCOLLEG"

  func oDCCOLLEG_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oDCCOLLEG_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oDCCOLLEG_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oDCCOLLEG_2_1.readonly and this.parent.oDCCOLLEG_2_1.isprimarykey)
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oDCCOLLEG_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Documenti collegati",'GSVE_MDC.TIP_DOCU_VZM',this.parent.oContained
   endif
  endproc

  add object oDESDOC_2_2 as StdTrsField with uid="VAEMBPLRZZ",rtseq=3,rtrep=.t.,;
    cFormVar="w_DESDOC",value=space(35),enabled=.f.,;
    HelpContextID = 210824650,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=261, Left=58, Top=0, InputMask=replicate('X',35)

  add object oDCFLINTE_2_3 as StdTrsCheck with uid="GBKRQNXGUM",rtrep=.t.,;
    cFormVar="w_DCFLINTE",  caption="No",;
    ToolTipText = "Se attivo: seleziona i documenti di qualsiasi intestatario",;
    HelpContextID = 236339323,;
    Left=321, Top=0, Width=69,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , autosize=.f.;
   , bGlobalFont=.t.


  func oDCFLINTE_2_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DCFLINTE,&i_cF..t_DCFLINTE),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oDCFLINTE_2_3.GetRadio()
    this.Parent.oContained.w_DCFLINTE = this.RadioValue()
    return .t.
  endfunc

  func oDCFLINTE_2_3.ToRadio()
    this.Parent.oContained.w_DCFLINTE=trim(this.Parent.oContained.w_DCFLINTE)
    return(;
      iif(this.Parent.oContained.w_DCFLINTE=='S',1,;
      0))
  endfunc

  func oDCFLINTE_2_3.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oDCFLEVAS_2_4 as StdTrsCheck with uid="ZLDWYRFAPI",rtrep=.t.,;
    cFormVar="w_DCFLEVAS",  caption="No",;
    ToolTipText = "Se attivo: non esegue l'evasione dei dati importati sui documenti di origine",;
    HelpContextID = 97927305,;
    Left=394, Top=0, Width=69,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , autosize=.f.;
   , bGlobalFont=.t.


  func oDCFLEVAS_2_4.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DCFLEVAS,&i_cF..t_DCFLEVAS),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oDCFLEVAS_2_4.GetRadio()
    this.Parent.oContained.w_DCFLEVAS = this.RadioValue()
    return .t.
  endfunc

  func oDCFLEVAS_2_4.ToRadio()
    this.Parent.oContained.w_DCFLEVAS=trim(this.Parent.oContained.w_DCFLEVAS)
    return(;
      iif(this.Parent.oContained.w_DCFLEVAS=='S',1,;
      0))
  endfunc

  func oDCFLEVAS_2_4.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oDCFLACCO_2_9 as StdTrsCheck with uid="GPDOKQXGYO",rtrep=.t.,;
    cFormVar="w_DCFLACCO",  caption="Si",;
    ToolTipText = "Se attivo: le righe docum.di origine saranno considerate come acconto fattura",;
    HelpContextID = 43401349,;
    Left=467, Top=0, Width=69,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , autosize=.f.;
   , bGlobalFont=.t.


  func oDCFLACCO_2_9.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DCFLACCO,&i_cF..t_DCFLACCO),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oDCFLACCO_2_9.GetRadio()
    this.Parent.oContained.w_DCFLACCO = this.RadioValue()
    return .t.
  endfunc

  func oDCFLACCO_2_9.ToRadio()
    this.Parent.oContained.w_DCFLACCO=trim(this.Parent.oContained.w_DCFLACCO)
    return(;
      iif(this.Parent.oContained.w_DCFLACCO=='S',1,;
      0))
  endfunc

  func oDCFLACCO_2_9.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oDCFLACCO_2_9.mCond()
    with this.Parent.oContained
      return (.w_DCFLEVAS=' ')
    endwith
  endfunc

  add object oDCFLGROR_2_14 as StdTrsCheck with uid="PMSGEMKWXP",rtrep=.t.,;
    cFormVar="w_DCFLGROR",  caption="Si",;
    ToolTipText = "Se attivo: raggruppa le righe congruenti di questa tipologia di documenti di origine",;
    HelpContextID = 235519864,;
    Left=540, Top=0, Width=69,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , autosize=.f.;
   , bGlobalFont=.t.


  func oDCFLGROR_2_14.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DCFLGROR,&i_cF..t_DCFLGROR),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oDCFLGROR_2_14.GetRadio()
    this.Parent.oContained.w_DCFLGROR = this.RadioValue()
    return .t.
  endfunc

  func oDCFLGROR_2_14.ToRadio()
    this.Parent.oContained.w_DCFLGROR=trim(this.Parent.oContained.w_DCFLGROR)
    return(;
      iif(this.Parent.oContained.w_DCFLGROR=='S',1,;
      0))
  endfunc

  func oDCFLGROR_2_14.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oDCFLGROR_2_14.mCond()
    with this.Parent.oContained
      return (.w_DCFLEVAS='S')
    endwith
  endfunc

  add object oDCFLRIPS_2_15 as StdTrsCheck with uid="TZGPRGQZOV",rtrep=.t.,;
    cFormVar="w_DCFLRIPS",  caption="No",;
    ToolTipText = "Se attivo: i prezzi/sconti delle le righe docum.di origine non saranno ricalcolate",;
    HelpContextID = 106545015,;
    Left=613, Top=0, Width=58,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , autosize=.f.;
   , bGlobalFont=.t.


  func oDCFLRIPS_2_15.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DCFLRIPS,&i_cF..t_DCFLRIPS),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oDCFLRIPS_2_15.GetRadio()
    this.Parent.oContained.w_DCFLRIPS = this.RadioValue()
    return .t.
  endfunc

  func oDCFLRIPS_2_15.ToRadio()
    this.Parent.oContained.w_DCFLRIPS=trim(this.Parent.oContained.w_DCFLRIPS)
    return(;
      iif(this.Parent.oContained.w_DCFLRIPS=='S',1,;
      0))
  endfunc

  func oDCFLRIPS_2_15.SetRadio()
    this.value=this.ToRadio()
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oDCCOLLEG_2_1.When()
    return(.t.)
  proc oDCCOLLEG_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oDCCOLLEG_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=7
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsac_mdc','DOC_COLL','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DCCODICE=DOC_COLL.DCCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
