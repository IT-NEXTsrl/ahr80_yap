* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_atp                                                        *
*              Tabella provvigioni                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_37]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-15                                                      *
* Last revis.: 2011-03-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsve_atp"))

* --- Class definition
define class tgsve_atp as StdForm
  Top    = 8
  Left   = 10

  * --- Standard Properties
  Width  = 580
  Height = 287+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-03-01"
  HelpContextID=175492503
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Constant Properties
  PRO_AGEN_IDX = 0
  GRUPRO_IDX = 0
  GRUPRO_IDX = 0
  GRUPRO_IDX = 0
  TAB_PROV_IDX = 0
  cFile = "PRO_AGEN"
  cKeySelect = "PVCATAGE,PVCHIAVE"
  cKeyWhere  = "PVCATAGE=this.w_PVCATAGE and PVCHIAVE=this.w_PVCHIAVE"
  cKeyWhereODBC = '"PVCATAGE="+cp_ToStrODBC(this.w_PVCATAGE)';
      +'+" and PVCHIAVE="+cp_ToStrODBC(this.w_PVCHIAVE)';

  cKeyWhereODBCqualified = '"PRO_AGEN.PVCATAGE="+cp_ToStrODBC(this.w_PVCATAGE)';
      +'+" and PRO_AGEN.PVCHIAVE="+cp_ToStrODBC(this.w_PVCHIAVE)';

  cPrg = "gsve_atp"
  cComment = "Tabella provvigioni"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PVCATAGE = space(5)
  w_DESAGE = space(35)
  w_TIPAGE = space(1)
  w_PVCATCLI = space(5)
  w_DESCLI = space(35)
  w_TIPCLI = space(1)
  w_PVCATART = space(5)
  w_DESART = space(35)
  w_TIPART = space(1)
  w_PVCHIAVE = space(10)

  * --- Children pointers
  GSVE_MTP = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'PRO_AGEN','gsve_atp')
    stdPageFrame::Init()
    *set procedure to GSVE_MTP additive
    with this
      .Pages(1).addobject("oPag","tgsve_atpPag1","gsve_atp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Provvigioni")
      .Pages(1).HelpContextID = 190462543
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPVCATAGE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSVE_MTP
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='GRUPRO'
    this.cWorkTables[2]='GRUPRO'
    this.cWorkTables[3]='GRUPRO'
    this.cWorkTables[4]='TAB_PROV'
    this.cWorkTables[5]='PRO_AGEN'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PRO_AGEN_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PRO_AGEN_IDX,3]
  return

  function CreateChildren()
    this.GSVE_MTP = CREATEOBJECT('stdDynamicChild',this,'GSVE_MTP',this.oPgFrm.Page1.oPag.oLinkPC_1_11)
    this.GSVE_MTP.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSVE_MTP)
      this.GSVE_MTP.DestroyChildrenChain()
      this.GSVE_MTP=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_11')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSVE_MTP.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSVE_MTP.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSVE_MTP.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSVE_MTP.SetKey(;
            .w_PVCATAGE,"TPCATAGE";
            ,.w_PVCHIAVE,"TPCHIAVE";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSVE_MTP.ChangeRow(this.cRowID+'      1',1;
             ,.w_PVCATAGE,"TPCATAGE";
             ,.w_PVCHIAVE,"TPCHIAVE";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSVE_MTP)
        i_f=.GSVE_MTP.BuildFilter()
        if !(i_f==.GSVE_MTP.cQueryFilter)
          i_fnidx=.GSVE_MTP.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSVE_MTP.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSVE_MTP.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSVE_MTP.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSVE_MTP.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_PVCATAGE = NVL(PVCATAGE,space(5))
      .w_PVCHIAVE = NVL(PVCHIAVE,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_1_joined
    link_1_1_joined=.f.
    local link_1_4_joined
    link_1_4_joined=.f.
    local link_1_7_joined
    link_1_7_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from PRO_AGEN where PVCATAGE=KeySet.PVCATAGE
    *                            and PVCHIAVE=KeySet.PVCHIAVE
    *
    i_nConn = i_TableProp[this.PRO_AGEN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PRO_AGEN_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PRO_AGEN')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PRO_AGEN.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PRO_AGEN '
      link_1_1_joined=this.AddJoinedLink_1_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_4_joined=this.AddJoinedLink_1_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_7_joined=this.AddJoinedLink_1_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PVCATAGE',this.w_PVCATAGE  ,'PVCHIAVE',this.w_PVCHIAVE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESAGE = space(35)
        .w_TIPAGE = space(1)
        .w_DESCLI = space(35)
        .w_TIPCLI = space(1)
        .w_DESART = space(35)
        .w_TIPART = space(1)
        .w_PVCATAGE = NVL(PVCATAGE,space(5))
          if link_1_1_joined
            this.w_PVCATAGE = NVL(GPCODICE101,NVL(this.w_PVCATAGE,space(5)))
            this.w_DESAGE = NVL(GPDESCRI101,space(35))
            this.w_TIPAGE = NVL(GPTIPPRO101,space(1))
          else
          .link_1_1('Load')
          endif
        .w_PVCATCLI = NVL(PVCATCLI,space(5))
          if link_1_4_joined
            this.w_PVCATCLI = NVL(GPCODICE104,NVL(this.w_PVCATCLI,space(5)))
            this.w_DESCLI = NVL(GPDESCRI104,space(35))
            this.w_TIPCLI = NVL(GPTIPPRO104,space(1))
          else
          .link_1_4('Load')
          endif
        .w_PVCATART = NVL(PVCATART,space(5))
          if link_1_7_joined
            this.w_PVCATART = NVL(GPCODICE107,NVL(this.w_PVCATART,space(5)))
            this.w_DESART = NVL(GPDESCRI107,space(35))
            this.w_TIPART = NVL(GPTIPPRO107,space(1))
          else
          .link_1_7('Load')
          endif
        .w_PVCHIAVE = NVL(PVCHIAVE,space(10))
        cp_LoadRecExtFlds(this,'PRO_AGEN')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PVCATAGE = space(5)
      .w_DESAGE = space(35)
      .w_TIPAGE = space(1)
      .w_PVCATCLI = space(5)
      .w_DESCLI = space(35)
      .w_TIPCLI = space(1)
      .w_PVCATART = space(5)
      .w_DESART = space(35)
      .w_TIPART = space(1)
      .w_PVCHIAVE = space(10)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_PVCATAGE))
          .link_1_1('Full')
          endif
        .DoRTCalc(2,4,.f.)
          if not(empty(.w_PVCATCLI))
          .link_1_4('Full')
          endif
        .DoRTCalc(5,7,.f.)
          if not(empty(.w_PVCATART))
          .link_1_7('Full')
          endif
          .DoRTCalc(8,9,.f.)
        .w_PVCHIAVE = IIF(EMPTY(.w_PVCATCLI), '#####', .w_PVCATCLI)+IIF(EMPTY(.w_PVCATART), '#####', .w_PVCATART)
      endif
    endwith
    cp_BlankRecExtFlds(this,'PRO_AGEN')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oPVCATAGE_1_1.enabled = i_bVal
      .Page1.oPag.oPVCATCLI_1_4.enabled = i_bVal
      .Page1.oPag.oPVCATART_1_7.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oPVCATAGE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oPVCATAGE_1_1.enabled = .t.
      endif
    endwith
    this.GSVE_MTP.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'PRO_AGEN',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSVE_MTP.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PRO_AGEN_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PVCATAGE,"PVCATAGE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PVCATCLI,"PVCATCLI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PVCATART,"PVCATART",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PVCHIAVE,"PVCHIAVE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PRO_AGEN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PRO_AGEN_IDX,2])
    i_lTable = "PRO_AGEN"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.PRO_AGEN_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSVE_STP with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PRO_AGEN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PRO_AGEN_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.PRO_AGEN_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into PRO_AGEN
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PRO_AGEN')
        i_extval=cp_InsertValODBCExtFlds(this,'PRO_AGEN')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(PVCATAGE,PVCATCLI,PVCATART,PVCHIAVE "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_PVCATAGE)+;
                  ","+cp_ToStrODBCNull(this.w_PVCATCLI)+;
                  ","+cp_ToStrODBCNull(this.w_PVCATART)+;
                  ","+cp_ToStrODBC(this.w_PVCHIAVE)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PRO_AGEN')
        i_extval=cp_InsertValVFPExtFlds(this,'PRO_AGEN')
        cp_CheckDeletedKey(i_cTable,0,'PVCATAGE',this.w_PVCATAGE,'PVCHIAVE',this.w_PVCHIAVE)
        INSERT INTO (i_cTable);
              (PVCATAGE,PVCATCLI,PVCATART,PVCHIAVE  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_PVCATAGE;
                  ,this.w_PVCATCLI;
                  ,this.w_PVCATART;
                  ,this.w_PVCHIAVE;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.PRO_AGEN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PRO_AGEN_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.PRO_AGEN_IDX,i_nConn)
      *
      * update PRO_AGEN
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'PRO_AGEN')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " PVCATCLI="+cp_ToStrODBCNull(this.w_PVCATCLI)+;
             ",PVCATART="+cp_ToStrODBCNull(this.w_PVCATART)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'PRO_AGEN')
        i_cWhere = cp_PKFox(i_cTable  ,'PVCATAGE',this.w_PVCATAGE  ,'PVCHIAVE',this.w_PVCHIAVE  )
        UPDATE (i_cTable) SET;
              PVCATCLI=this.w_PVCATCLI;
             ,PVCATART=this.w_PVCATART;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSVE_MTP : Saving
      this.GSVE_MTP.ChangeRow(this.cRowID+'      1',0;
             ,this.w_PVCATAGE,"TPCATAGE";
             ,this.w_PVCHIAVE,"TPCHIAVE";
             )
      this.GSVE_MTP.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSVE_MTP : Deleting
    this.GSVE_MTP.ChangeRow(this.cRowID+'      1',0;
           ,this.w_PVCATAGE,"TPCATAGE";
           ,this.w_PVCHIAVE,"TPCHIAVE";
           )
    this.GSVE_MTP.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PRO_AGEN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PRO_AGEN_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.PRO_AGEN_IDX,i_nConn)
      *
      * delete PRO_AGEN
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'PVCATAGE',this.w_PVCATAGE  ,'PVCHIAVE',this.w_PVCHIAVE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PRO_AGEN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PRO_AGEN_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,9,.t.)
            .w_PVCHIAVE = IIF(EMPTY(.w_PVCATCLI), '#####', .w_PVCATCLI)+IIF(EMPTY(.w_PVCATART), '#####', .w_PVCATART)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPVCATCLI_1_4.enabled = this.oPgFrm.Page1.oPag.oPVCATCLI_1_4.mCond()
    this.oPgFrm.Page1.oPag.oPVCATART_1_7.enabled = this.oPgFrm.Page1.oPag.oPVCATART_1_7.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PVCATAGE
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUPRO_IDX,3]
    i_lTable = "GRUPRO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2], .t., this.GRUPRO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PVCATAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGP',True,'GRUPRO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GPCODICE like "+cp_ToStrODBC(trim(this.w_PVCATAGE)+"%");

          i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPTIPPRO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GPCODICE',trim(this.w_PVCATAGE))
          select GPCODICE,GPDESCRI,GPTIPPRO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PVCATAGE)==trim(_Link_.GPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PVCATAGE) and !this.bDontReportError
            deferred_cp_zoom('GRUPRO','*','GPCODICE',cp_AbsName(oSource.parent,'oPVCATAGE_1_1'),i_cWhere,'GSAR_AGP',"Categorie provvigioni",'GSVE0STP.GRUPRO_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPTIPPRO";
                     +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',oSource.xKey(1))
            select GPCODICE,GPDESCRI,GPTIPPRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PVCATAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPTIPPRO";
                   +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(this.w_PVCATAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',this.w_PVCATAGE)
            select GPCODICE,GPDESCRI,GPTIPPRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PVCATAGE = NVL(_Link_.GPCODICE,space(5))
      this.w_DESAGE = NVL(_Link_.GPDESCRI,space(35))
      this.w_TIPAGE = NVL(_Link_.GPTIPPRO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PVCATAGE = space(5)
      endif
      this.w_DESAGE = space(35)
      this.w_TIPAGE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPAGE='G'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PVCATAGE = space(5)
        this.w_DESAGE = space(35)
        this.w_TIPAGE = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2])+'\'+cp_ToStr(_Link_.GPCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUPRO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PVCATAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.GRUPRO_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_1.GPCODICE as GPCODICE101"+ ",link_1_1.GPDESCRI as GPDESCRI101"+ ",link_1_1.GPTIPPRO as GPTIPPRO101"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_1 on PRO_AGEN.PVCATAGE=link_1_1.GPCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_1"
          i_cKey=i_cKey+'+" and PRO_AGEN.PVCATAGE=link_1_1.GPCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PVCATCLI
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUPRO_IDX,3]
    i_lTable = "GRUPRO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2], .t., this.GRUPRO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PVCATCLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGP',True,'GRUPRO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GPCODICE like "+cp_ToStrODBC(trim(this.w_PVCATCLI)+"%");

          i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPTIPPRO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GPCODICE',trim(this.w_PVCATCLI))
          select GPCODICE,GPDESCRI,GPTIPPRO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PVCATCLI)==trim(_Link_.GPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PVCATCLI) and !this.bDontReportError
            deferred_cp_zoom('GRUPRO','*','GPCODICE',cp_AbsName(oSource.parent,'oPVCATCLI_1_4'),i_cWhere,'GSAR_AGP',"Categorie provvigioni",'GSVE1STP.GRUPRO_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPTIPPRO";
                     +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',oSource.xKey(1))
            select GPCODICE,GPDESCRI,GPTIPPRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PVCATCLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPTIPPRO";
                   +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(this.w_PVCATCLI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',this.w_PVCATCLI)
            select GPCODICE,GPDESCRI,GPTIPPRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PVCATCLI = NVL(_Link_.GPCODICE,space(5))
      this.w_DESCLI = NVL(_Link_.GPDESCRI,space(35))
      this.w_TIPCLI = NVL(_Link_.GPTIPPRO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PVCATCLI = space(5)
      endif
      this.w_DESCLI = space(35)
      this.w_TIPCLI = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPCLI='C' OR EMPTY(.w_PVCATCLI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PVCATCLI = space(5)
        this.w_DESCLI = space(35)
        this.w_TIPCLI = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2])+'\'+cp_ToStr(_Link_.GPCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUPRO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PVCATCLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.GRUPRO_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_4.GPCODICE as GPCODICE104"+ ",link_1_4.GPDESCRI as GPDESCRI104"+ ",link_1_4.GPTIPPRO as GPTIPPRO104"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_4 on PRO_AGEN.PVCATCLI=link_1_4.GPCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_4"
          i_cKey=i_cKey+'+" and PRO_AGEN.PVCATCLI=link_1_4.GPCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PVCATART
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUPRO_IDX,3]
    i_lTable = "GRUPRO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2], .t., this.GRUPRO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PVCATART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGP',True,'GRUPRO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GPCODICE like "+cp_ToStrODBC(trim(this.w_PVCATART)+"%");

          i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPTIPPRO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GPCODICE',trim(this.w_PVCATART))
          select GPCODICE,GPDESCRI,GPTIPPRO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PVCATART)==trim(_Link_.GPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PVCATART) and !this.bDontReportError
            deferred_cp_zoom('GRUPRO','*','GPCODICE',cp_AbsName(oSource.parent,'oPVCATART_1_7'),i_cWhere,'GSAR_AGP',"Categorie provvigioni",'GSVE2STP.GRUPRO_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPTIPPRO";
                     +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',oSource.xKey(1))
            select GPCODICE,GPDESCRI,GPTIPPRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PVCATART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPTIPPRO";
                   +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(this.w_PVCATART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',this.w_PVCATART)
            select GPCODICE,GPDESCRI,GPTIPPRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PVCATART = NVL(_Link_.GPCODICE,space(5))
      this.w_DESART = NVL(_Link_.GPDESCRI,space(35))
      this.w_TIPART = NVL(_Link_.GPTIPPRO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PVCATART = space(5)
      endif
      this.w_DESART = space(35)
      this.w_TIPART = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPART='A' OR EMPTY(.w_PVCATART)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PVCATART = space(5)
        this.w_DESART = space(35)
        this.w_TIPART = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2])+'\'+cp_ToStr(_Link_.GPCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUPRO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PVCATART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.GRUPRO_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_7.GPCODICE as GPCODICE107"+ ",link_1_7.GPDESCRI as GPDESCRI107"+ ",link_1_7.GPTIPPRO as GPTIPPRO107"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_7 on PRO_AGEN.PVCATART=link_1_7.GPCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_7"
          i_cKey=i_cKey+'+" and PRO_AGEN.PVCATART=link_1_7.GPCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPVCATAGE_1_1.value==this.w_PVCATAGE)
      this.oPgFrm.Page1.oPag.oPVCATAGE_1_1.value=this.w_PVCATAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESAGE_1_2.value==this.w_DESAGE)
      this.oPgFrm.Page1.oPag.oDESAGE_1_2.value=this.w_DESAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oPVCATCLI_1_4.value==this.w_PVCATCLI)
      this.oPgFrm.Page1.oPag.oPVCATCLI_1_4.value=this.w_PVCATCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLI_1_5.value==this.w_DESCLI)
      this.oPgFrm.Page1.oPag.oDESCLI_1_5.value=this.w_DESCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oPVCATART_1_7.value==this.w_PVCATART)
      this.oPgFrm.Page1.oPag.oPVCATART_1_7.value=this.w_PVCATART
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_8.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_8.value=this.w_DESART
    endif
    cp_SetControlsValueExtFlds(this,'PRO_AGEN')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_PVCATAGE)) or not(.w_TIPAGE='G'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPVCATAGE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_PVCATAGE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TIPCLI='C' OR EMPTY(.w_PVCATCLI))  and (.cFunction <> 'Edit')  and not(empty(.w_PVCATCLI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPVCATCLI_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TIPART='A' OR EMPTY(.w_PVCATART))  and (.cFunction <> 'Edit')  and not(empty(.w_PVCATART))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPVCATART_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .GSVE_MTP.CheckForm()
      if i_bres
        i_bres=  .GSVE_MTP.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    * --- GSVE_MTP : Depends On
    this.GSVE_MTP.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsve_atpPag1 as StdContainer
  Width  = 576
  height = 287
  stdWidth  = 576
  stdheight = 287
  resizeXpos=570
  resizeYpos=192
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPVCATAGE_1_1 as StdField with uid="EFWFSBLHDR",rtseq=1,rtrep=.f.,;
    cFormVar = "w_PVCATAGE", cQueryName = "PVCATAGE",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Categoria provvigioni legata all'agente",;
    HelpContextID = 16472379,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=107, Top=34, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUPRO", cZoomOnZoom="GSAR_AGP", oKey_1_1="GPCODICE", oKey_1_2="this.w_PVCATAGE"

  func oPVCATAGE_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oPVCATAGE_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPVCATAGE_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUPRO','*','GPCODICE',cp_AbsName(this.parent,'oPVCATAGE_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGP',"Categorie provvigioni",'GSVE0STP.GRUPRO_VZM',this.parent.oContained
  endproc
  proc oPVCATAGE_1_1.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GPCODICE=this.parent.oContained.w_PVCATAGE
     i_obj.ecpSave()
  endproc

  add object oDESAGE_1_2 as StdField with uid="WATWWYPXPN",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DESAGE", cQueryName = "DESAGE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 198424778,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=173, Top=34, InputMask=replicate('X',35)

  add object oPVCATCLI_1_4 as StdField with uid="PLIYRFWDCW",rtseq=4,rtrep=.f.,;
    cFormVar = "w_PVCATCLI", cQueryName = "PVCATCLI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Categoria provvigioni legata al cliente (vuoto = cliente generico)",;
    HelpContextID = 218408641,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=107, Top=61, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUPRO", cZoomOnZoom="GSAR_AGP", oKey_1_1="GPCODICE", oKey_1_2="this.w_PVCATCLI"

  func oPVCATCLI_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction <> 'Edit')
    endwith
   endif
  endfunc

  func oPVCATCLI_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oPVCATCLI_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPVCATCLI_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUPRO','*','GPCODICE',cp_AbsName(this.parent,'oPVCATCLI_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGP',"Categorie provvigioni",'GSVE1STP.GRUPRO_VZM',this.parent.oContained
  endproc
  proc oPVCATCLI_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GPCODICE=this.parent.oContained.w_PVCATCLI
     i_obj.ecpSave()
  endproc

  add object oDESCLI_1_5 as StdField with uid="KQCMYBFXPL",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESCLI", cQueryName = "DESCLI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 125941962,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=173, Top=61, InputMask=replicate('X',35)

  add object oPVCATART_1_7 as StdField with uid="KDXQQTSYXK",rtseq=7,rtrep=.f.,;
    cFormVar = "w_PVCATART", cQueryName = "PVCATART",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Categoria provvigioni legata all'articolo (vuoto = articolo generico)",;
    HelpContextID = 251963062,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=107, Top=88, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUPRO", cZoomOnZoom="GSAR_AGP", oKey_1_1="GPCODICE", oKey_1_2="this.w_PVCATART"

  func oPVCATART_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction <> 'Edit')
    endwith
   endif
  endfunc

  func oPVCATART_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oPVCATART_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPVCATART_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUPRO','*','GPCODICE',cp_AbsName(this.parent,'oPVCATART_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGP',"Categorie provvigioni",'GSVE2STP.GRUPRO_VZM',this.parent.oContained
  endproc
  proc oPVCATART_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GPCODICE=this.parent.oContained.w_PVCATART
     i_obj.ecpSave()
  endproc

  add object oDESART_1_8 as StdField with uid="UNNKHUSYKU",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 203667658,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=173, Top=88, InputMask=replicate('X',35)


  add object oLinkPC_1_11 as stdDynamicChildContainer with uid="GOVCEQRADZ",left=101, top=125, width=461, height=152, bOnScreen=.t.;


  add object oStr_1_12 as StdString with uid="HWURJBUPTY",Visible=.t., Left=6, Top=7,;
    Alignment=0, Width=377, Height=15,;
    Caption="Categorie provvigioni"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_13 as StdString with uid="OIUUNPNYOI",Visible=.t., Left=41, Top=34,;
    Alignment=1, Width=60, Height=15,;
    Caption="Agente:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_14 as StdString with uid="GVBHGCUNAH",Visible=.t., Left=41, Top=61,;
    Alignment=1, Width=60, Height=15,;
    Caption="Cliente:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_15 as StdString with uid="WUDMUFVIXH",Visible=.t., Left=41, Top=88,;
    Alignment=1, Width=60, Height=15,;
    Caption="Articolo:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_17 as StdString with uid="NBFTUFGMHD",Visible=.t., Left=10, Top=128,;
    Alignment=0, Width=87, Height=15,;
    Caption="% Provvigioni"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_16 as StdBox with uid="OGAFPABMMM",left=2, top=24, width=435,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_atp','PRO_AGEN','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PVCATAGE=PRO_AGEN.PVCATAGE";
  +" and "+i_cAliasName2+".PVCHIAVE=PRO_AGEN.PVCHIAVE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
