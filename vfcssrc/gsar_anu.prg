* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_anu                                                        *
*              Codifica automatica archivi                                     *
*                                                                              *
*      Author: ZUCCHETTI SPA -CS                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-03-21                                                      *
* Last revis.: 2010-05-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_anu"))

* --- Class definition
define class tgsar_anu as StdForm
  Top    = 1
  Left   = 3

  * --- Standard Properties
  Width  = 547
  Height = 537+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-05-10"
  HelpContextID=193558377
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=11

  * --- Constant Properties
  NUMAUT_M_IDX = 0
  AZIENDA_IDX = 0
  cFile = "NUMAUT_M"
  cKeySelect = "NACODAZI,NATIPGES,NATIPGES"
  cKeyWhere  = "NACODAZI=this.w_NACODAZI and NATIPGES=this.w_NATIPGES and NATIPGES=this.w_NATIPGES"
  cKeyWhereODBC = '"NACODAZI="+cp_ToStrODBC(this.w_NACODAZI)';
      +'+" and NATIPGES="+cp_ToStrODBC(this.w_NATIPGES)';
      +'+" and NATIPGES="+cp_ToStrODBC(this.w_NATIPGES)';

  cKeyWhereODBCqualified = '"NUMAUT_M.NACODAZI="+cp_ToStrODBC(this.w_NACODAZI)';
      +'+" and NUMAUT_M.NATIPGES="+cp_ToStrODBC(this.w_NATIPGES)';
      +'+" and NUMAUT_M.NATIPGES="+cp_ToStrODBC(this.w_NATIPGES)';

  cPrg = "gsar_anu"
  cComment = "Codifica automatica archivi"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_NACODAZI = space(5)
  w_NAACTIVE = space(1)
  w_NANOMPRO = space(40)
  w_AZRAGAZI = space(40)
  w_NATIPGES = space(2)
  o_NATIPGES = space(2)
  w_NATIPGES = space(2)
  w_NOMETAB = space(20)
  o_NOMETAB = space(20)
  w_EXPRESULT = space(0)
  w_SELEZI = space(1)
  o_SELEZI = space(1)
  w_PROGUPD = space(1)
  w_CHIAVEARC = space(15)

  * --- Children pointers
  GSAR_MNV = .NULL.
  GSAR_MNU = .NULL.
  w_ZOOMAUT = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=4, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'NUMAUT_M','gsar_anu')
    stdPageFrame::Init()
    *set procedure to GSAR_MNV additive
    *set procedure to GSAR_MNU additive
    with this
      .Pages(1).addobject("oPag","tgsar_anuPag1","gsar_anu",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Parametri codifica")
      .Pages(1).HelpContextID = 6606225
      .Pages(2).addobject("oPag","tgsar_anuPag2","gsar_anu",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Verifica codice")
      .Pages(2).HelpContextID = 239233207
      .Pages(3).addobject("oPag","tgsar_anuPag3","gsar_anu",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Aggiorna autonumber")
      .Pages(3).HelpContextID = 213277964
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oNACODAZI_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSAR_MNV
    *release procedure GSAR_MNU
    * --- Area Manuale = Init Page Frame
    * --- gsar_anu
    this.parent.opgfrm.page3.enabled=.f.
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
    proc Init()
      this.w_ZOOMAUT = this.oPgFrm.Pages(3).oPag.ZOOMAUT
      DoDefault()
    proc Destroy()
      this.w_ZOOMAUT = .NULL.
      DoDefault()

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='NUMAUT_M'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.NUMAUT_M_IDX,5],7]
    this.nPostItConn=i_TableProp[this.NUMAUT_M_IDX,3]
  return

  function CreateChildren()
    this.GSAR_MNV = CREATEOBJECT('stdDynamicChild',this,'GSAR_MNV',this.oPgFrm.Page1.oPag.oLinkPC_1_5)
    this.GSAR_MNV.createrealchild()
    this.GSAR_MNU = CREATEOBJECT('stdDynamicChild',this,'GSAR_MNU',this.oPgFrm.Page1.oPag.oLinkPC_1_6)
    this.GSAR_MNU.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSAR_MNV)
      this.GSAR_MNV.DestroyChildrenChain()
      this.GSAR_MNV=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_5')
    if !ISNULL(this.GSAR_MNU)
      this.GSAR_MNU.DestroyChildrenChain()
      this.GSAR_MNU=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_6')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSAR_MNV.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSAR_MNU.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSAR_MNV.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSAR_MNU.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSAR_MNV.NewDocument()
    this.GSAR_MNU.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSAR_MNV.SetKey(;
            .w_NACODAZI,"NACODAZI";
            ,.w_NATIPGES,"NATIPGES";
            )
      this.GSAR_MNU.SetKey(;
            .w_NACODAZI,"NACODAZI";
            ,.w_NATIPGES,"NATIPGES";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSAR_MNV.ChangeRow(this.cRowID+'      1',1;
             ,.w_NACODAZI,"NACODAZI";
             ,.w_NATIPGES,"NATIPGES";
             )
      .GSAR_MNU.ChangeRow(this.cRowID+'      1',1;
             ,.w_NACODAZI,"NACODAZI";
             ,.w_NATIPGES,"NATIPGES";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSAR_MNV)
        i_f=.GSAR_MNV.BuildFilter()
        if !(i_f==.GSAR_MNV.cQueryFilter)
          i_fnidx=.GSAR_MNV.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAR_MNV.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAR_MNV.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAR_MNV.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAR_MNV.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSAR_MNU)
        i_f=.GSAR_MNU.BuildFilter()
        if !(i_f==.GSAR_MNU.cQueryFilter)
          i_fnidx=.GSAR_MNU.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAR_MNU.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAR_MNU.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAR_MNU.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAR_MNU.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_NACODAZI = NVL(NACODAZI,space(5))
      .w_NATIPGES = NVL(NATIPGES,space(2))
      .w_NATIPGES = NVL(NATIPGES,space(2))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_1_joined
    link_1_1_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from NUMAUT_M where NACODAZI=KeySet.NACODAZI
    *                            and NATIPGES=KeySet.NATIPGES
    *                            and NATIPGES=KeySet.NATIPGES
    *
    i_nConn = i_TableProp[this.NUMAUT_M_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.NUMAUT_M_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('NUMAUT_M')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "NUMAUT_M.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' NUMAUT_M '
      link_1_1_joined=this.AddJoinedLink_1_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'NACODAZI',this.w_NACODAZI  ,'NATIPGES',this.w_NATIPGES  ,'NATIPGES',this.w_NATIPGES  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_AZRAGAZI = space(40)
        .w_EXPRESULT = space(0)
        .w_SELEZI = 'S'
        .w_PROGUPD = 'N'
        .w_NACODAZI = NVL(NACODAZI,space(5))
          if link_1_1_joined
            this.w_NACODAZI = NVL(AZCODAZI101,NVL(this.w_NACODAZI,space(5)))
            this.w_AZRAGAZI = NVL(AZRAGAZI101,space(40))
          else
          .link_1_1('Load')
          endif
        .w_NAACTIVE = NVL(NAACTIVE,space(1))
        .w_NANOMPRO = NVL(NANOMPRO,space(40))
        .w_NATIPGES = NVL(NATIPGES,space(2))
        .w_NATIPGES = NVL(NATIPGES,space(2))
        .w_NOMETAB = iif(.w_NATIPGES$'CG-CL-FO','CONTI',iif(.w_NATIPGES='MG','MASTRI',iif(.w_NATIPGES='CA','VOC_COST',iif(.w_NATIPGES='MA','MASTVOCI',iif(.w_NATIPGES$'AR-AS','ART_ICOL','AGENTI')))))
        .oPgFrm.Page3.oPag.ZOOMAUT.Calculate()
        .w_CHIAVEARC = .w_ZOOMAUT.GetVar('CHIAVE')
        cp_LoadRecExtFlds(this,'NUMAUT_M')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_NACODAZI = space(5)
      .w_NAACTIVE = space(1)
      .w_NANOMPRO = space(40)
      .w_AZRAGAZI = space(40)
      .w_NATIPGES = space(2)
      .w_NATIPGES = space(2)
      .w_NOMETAB = space(20)
      .w_EXPRESULT = space(0)
      .w_SELEZI = space(1)
      .w_PROGUPD = space(1)
      .w_CHIAVEARC = space(15)
      if .cFunction<>"Filter"
        .w_NACODAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_NACODAZI))
          .link_1_1('Full')
          endif
        .w_NAACTIVE = 'N'
          .DoRTCalc(3,4,.f.)
        .w_NATIPGES = IIF(.cFunction='Load', 'CG', .w_NATIPGES)
        .w_NATIPGES = IIF(.cFunction='Load', 'CG', .w_NATIPGES)
        .w_NOMETAB = iif(.w_NATIPGES$'CG-CL-FO','CONTI',iif(.w_NATIPGES='MG','MASTRI',iif(.w_NATIPGES='CA','VOC_COST',iif(.w_NATIPGES='MA','MASTVOCI',iif(.w_NATIPGES$'AR-AS','ART_ICOL','AGENTI')))))
        .oPgFrm.Page3.oPag.ZOOMAUT.Calculate()
          .DoRTCalc(8,8,.f.)
        .w_SELEZI = 'S'
        .w_PROGUPD = 'N'
        .w_CHIAVEARC = .w_ZOOMAUT.GetVar('CHIAVE')
      endif
    endwith
    cp_BlankRecExtFlds(this,'NUMAUT_M')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oNACODAZI_1_1.enabled = !i_bVal
      .Page1.oPag.oNAACTIVE_1_2.enabled = i_bVal
      .Page1.oPag.oNANOMPRO_1_3.enabled = i_bVal
      .Page1.oPag.oNATIPGES_1_10.enabled = i_bVal
      .Page1.oPag.oNATIPGES_1_11.enabled = i_bVal
      .Page3.oPag.oSELEZI_3_2.enabled_(i_bVal)
      .Page1.oPag.oBtn_1_4.enabled = i_bVal
      .Page1.oPag.oBtn_1_14.enabled = i_bVal
      .Page1.oPag.oBtn_1_15.enabled = .Page1.oPag.oBtn_1_15.mCond()
      .Page3.oPag.oBtn_3_3.enabled = i_bVal
      .Page3.oPag.oBtn_3_6.enabled = i_bVal
      .Page3.oPag.oBtn_3_8.enabled = i_bVal
      .Page3.oPag.ZOOMAUT.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oNATIPGES_1_10.enabled = .f.
        .Page1.oPag.oNATIPGES_1_11.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oNATIPGES_1_10.enabled = .t.
        .Page1.oPag.oNATIPGES_1_11.enabled = .t.
      endif
    endwith
    this.GSAR_MNV.SetStatus(i_cOp)
    this.GSAR_MNU.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'NUMAUT_M',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSAR_MNV.SetChildrenStatus(i_cOp)
  *  this.GSAR_MNU.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.NUMAUT_M_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NACODAZI,"NACODAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NAACTIVE,"NAACTIVE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NANOMPRO,"NANOMPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NATIPGES,"NATIPGES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NATIPGES,"NATIPGES",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.NUMAUT_M_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.NUMAUT_M_IDX,2])
    i_lTable = "NUMAUT_M"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.NUMAUT_M_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.NUMAUT_M_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.NUMAUT_M_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.NUMAUT_M_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into NUMAUT_M
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'NUMAUT_M')
        i_extval=cp_InsertValODBCExtFlds(this,'NUMAUT_M')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(NACODAZI,NAACTIVE,NANOMPRO,NATIPGES "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_NACODAZI)+;
                  ","+cp_ToStrODBC(this.w_NAACTIVE)+;
                  ","+cp_ToStrODBC(this.w_NANOMPRO)+;
                  ","+cp_ToStrODBC(this.w_NATIPGES)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'NUMAUT_M')
        i_extval=cp_InsertValVFPExtFlds(this,'NUMAUT_M')
        cp_CheckDeletedKey(i_cTable,0,'NACODAZI',this.w_NACODAZI,'NATIPGES',this.w_NATIPGES,'NATIPGES',this.w_NATIPGES)
        INSERT INTO (i_cTable);
              (NACODAZI,NAACTIVE,NANOMPRO,NATIPGES  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_NACODAZI;
                  ,this.w_NAACTIVE;
                  ,this.w_NANOMPRO;
                  ,this.w_NATIPGES;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.NUMAUT_M_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.NUMAUT_M_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.NUMAUT_M_IDX,i_nConn)
      *
      * update NUMAUT_M
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'NUMAUT_M')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " NAACTIVE="+cp_ToStrODBC(this.w_NAACTIVE)+;
             ",NANOMPRO="+cp_ToStrODBC(this.w_NANOMPRO)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'NUMAUT_M')
        i_cWhere = cp_PKFox(i_cTable  ,'NACODAZI',this.w_NACODAZI  ,'NATIPGES',this.w_NATIPGES  ,'NATIPGES',this.w_NATIPGES  )
        UPDATE (i_cTable) SET;
              NAACTIVE=this.w_NAACTIVE;
             ,NANOMPRO=this.w_NANOMPRO;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSAR_MNV : Saving
      this.GSAR_MNV.ChangeRow(this.cRowID+'      1',0;
             ,this.w_NACODAZI,"NACODAZI";
             ,this.w_NATIPGES,"NATIPGES";
             )
      this.GSAR_MNV.mReplace()
      * --- GSAR_MNU : Saving
      this.GSAR_MNU.ChangeRow(this.cRowID+'      1',0;
             ,this.w_NACODAZI,"NACODAZI";
             ,this.w_NATIPGES,"NATIPGES";
             )
      this.GSAR_MNU.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSAR_MNV : Deleting
    this.GSAR_MNV.ChangeRow(this.cRowID+'      1',0;
           ,this.w_NACODAZI,"NACODAZI";
           ,this.w_NATIPGES,"NATIPGES";
           )
    this.GSAR_MNV.mDelete()
    * --- GSAR_MNU : Deleting
    this.GSAR_MNU.ChangeRow(this.cRowID+'      1',0;
           ,this.w_NACODAZI,"NACODAZI";
           ,this.w_NATIPGES,"NATIPGES";
           )
    this.GSAR_MNU.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.NUMAUT_M_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.NUMAUT_M_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.NUMAUT_M_IDX,i_nConn)
      *
      * delete NUMAUT_M
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'NACODAZI',this.w_NACODAZI  ,'NATIPGES',this.w_NATIPGES  ,'NATIPGES',this.w_NATIPGES  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.NUMAUT_M_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.NUMAUT_M_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,6,.t.)
        if .o_NATIPGES<>.w_NATIPGES
            .w_NOMETAB = iif(.w_NATIPGES$'CG-CL-FO','CONTI',iif(.w_NATIPGES='MG','MASTRI',iif(.w_NATIPGES='CA','VOC_COST',iif(.w_NATIPGES='MA','MASTVOCI',iif(.w_NATIPGES$'AR-AS','ART_ICOL','AGENTI')))))
        endif
        .oPgFrm.Page3.oPag.ZOOMAUT.Calculate()
        if .o_SELEZI<>.w_SELEZI
          .Calculate_JROJMCSSXL()
        endif
        .DoRTCalc(8,10,.t.)
            .w_CHIAVEARC = .w_ZOOMAUT.GetVar('CHIAVE')
        if .o_NOMETAB<>.w_NOMETAB
          .Calculate_CJOOKSBOAO()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page3.oPag.ZOOMAUT.Calculate()
    endwith
  return

  proc Calculate_VXXRKYGQJV()
    with this
          * --- Seleziona file
          gsar_bnu(this;
              ,'F';
             )
    endwith
  endproc
  proc Calculate_JROJMCSSXL()
    with this
          * --- W_selezi changed
          GSAR_BNU(this;
              ,'S';
             )
    endwith
  endproc
  proc Calculate_RACZSKZHAR()
    with this
          * --- Uscita
          GSAR_BNU(this;
              ,'X';
             )
    endwith
  endproc
  proc Calculate_RJTBMKMMLG()
    with this
          * --- ActivatePage 1
          .oPgFrm.page3.enabled = .F.
    endwith
  endproc
  proc Calculate_CJOOKSBOAO()
    with this
          * --- Aggiorno il campo tabella del figlio
          .GSAR_MNU.w_TABELLA = .w_NOMETAB
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oNATIPGES_1_10.enabled = this.oPgFrm.Page1.oPag.oNATIPGES_1_10.mCond()
    this.oPgFrm.Page1.oPag.oNATIPGES_1_11.enabled = this.oPgFrm.Page1.oPag.oNATIPGES_1_11.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_6.enabled = this.oPgFrm.Page3.oPag.oBtn_3_6.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oNATIPGES_1_10.visible=!this.oPgFrm.Page1.oPag.oNATIPGES_1_10.mHide()
    this.oPgFrm.Page1.oPag.oNATIPGES_1_11.visible=!this.oPgFrm.Page1.oPag.oNATIPGES_1_11.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_19.visible=!this.oPgFrm.Page1.oPag.oStr_1_19.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_21.visible=!this.oPgFrm.Page1.oPag.oStr_1_21.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_22.visible=!this.oPgFrm.Page1.oPag.oStr_1_22.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("SelezionaFile")
          .Calculate_VXXRKYGQJV()
          bRefresh=.t.
        endif
      .oPgFrm.Page3.oPag.ZOOMAUT.Event(cEvent)
        if lower(cEvent)==lower("Done")
          .Calculate_RACZSKZHAR()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("ActivatePage 1")
          .Calculate_RJTBMKMMLG()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=NACODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NACODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NACODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_NACODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_NACODAZI)
            select AZCODAZI,AZRAGAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NACODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_AZRAGAZI = NVL(_Link_.AZRAGAZI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_NACODAZI = space(5)
      endif
      this.w_AZRAGAZI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NACODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.AZIENDA_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_1.AZCODAZI as AZCODAZI101"+ ",link_1_1.AZRAGAZI as AZRAGAZI101"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_1 on NUMAUT_M.NACODAZI=link_1_1.AZCODAZI"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_1"
          i_cKey=i_cKey+'+" and NUMAUT_M.NACODAZI=link_1_1.AZCODAZI(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oNACODAZI_1_1.value==this.w_NACODAZI)
      this.oPgFrm.Page1.oPag.oNACODAZI_1_1.value=this.w_NACODAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oNAACTIVE_1_2.RadioValue()==this.w_NAACTIVE)
      this.oPgFrm.Page1.oPag.oNAACTIVE_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNANOMPRO_1_3.value==this.w_NANOMPRO)
      this.oPgFrm.Page1.oPag.oNANOMPRO_1_3.value=this.w_NANOMPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oAZRAGAZI_1_8.value==this.w_AZRAGAZI)
      this.oPgFrm.Page1.oPag.oAZRAGAZI_1_8.value=this.w_AZRAGAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oNATIPGES_1_10.RadioValue()==this.w_NATIPGES)
      this.oPgFrm.Page1.oPag.oNATIPGES_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNATIPGES_1_11.RadioValue()==this.w_NATIPGES)
      this.oPgFrm.Page1.oPag.oNATIPGES_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oEXPRESULT_2_1.value==this.w_EXPRESULT)
      this.oPgFrm.Page2.oPag.oEXPRESULT_2_1.value=this.w_EXPRESULT
    endif
    if not(this.oPgFrm.Page3.oPag.oSELEZI_3_2.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page3.oPag.oSELEZI_3_2.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'NUMAUT_M')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      *i_bRes = i_bRes .and. .GSAR_MNV.CheckForm()
      if i_bres
        i_bres=  .GSAR_MNV.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      *i_bRes = i_bRes .and. .GSAR_MNU.CheckForm()
      if i_bres
        i_bres=  .GSAR_MNU.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsar_anu
      * Lancio la verifica del codice alla check form
      
      if GSAR_BNU(this,'K')=-1
        ah_ErrorMsg("Il codice risultante supera la lunghezza massima ammessa dal campo.",48)
        i_bRes=.f.
      else
        if GSAR_BNU(this,'K')>0
          if ! ah_YesNo("Attenzione: non � stato possibile verificare il codice.%0Premere il bottone di verifica oppure controllare la regolarit� delle espressioni ed i valori immessi.%0Si desidera salvare comunque le modifiche?")
            i_bRes=.f.
          endif
        endif
      endif
      * Verifico in caso di espressione autonumber che sia possibile effettuare il controllo
      returnval=GSAR_BNU(this,'G')
      if returnval=1
        mess=ah_msgformat("Attenzione: non � possibile effettuare la verifica degli autonumber in presenza di espressioni contenenti funzioni Fox.")
        if ! ah_YesNo("%1%0Si desidera caricare comunque la codifica?","",mess)
          i_bRes=.f.
        endif
      else
        * Verifico in caso di espressione autonumber che venga effettuata la verifica e l'aggiornamento
        returnval=GSAR_BNU(this,'N')
        if returnval>0
          if returnval=1
             mess=ah_msgformat("Attenzione: � stata effettuata la verifica dei progressivi ma non l'aggiornamento degli stessi.")
          endif
          if returnval=2
             mess=ah_msgformat("Attenzione: non � stata effettuata la verifica dei progressivi.")
          endif
          if ! ah_YesNo("%1%0Si desidera caricare comunque la codifica?","",mess)
            i_bRes=.f.
          endif
        endif
      endif
      if i_bRes
          if GSAR_BNU(this,'L') > 0
             ah_ErrorMsg("Attenzione: la lunghezza delle espressioni deve essere maggiore di 0",48)
             i_bRes=.f.
          endif
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_NATIPGES = this.w_NATIPGES
    this.o_NOMETAB = this.w_NOMETAB
    this.o_SELEZI = this.w_SELEZI
    * --- GSAR_MNV : Depends On
    this.GSAR_MNV.SaveDependsOn()
    * --- GSAR_MNU : Depends On
    this.GSAR_MNU.SaveDependsOn()
    return

  func CanView()
    local i_res
    i_res=this.w_NACODAZI = i_CODAZI
    if !i_res
      this.BlankRec()
      this.SetControlsValue()
      cp_ErrorMsg('MSG_CANNOT_VIEW')
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsar_anuPag1 as StdContainer
  Width  = 543
  height = 537
  stdWidth  = 543
  stdheight = 537
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oNACODAZI_1_1 as StdField with uid="FXGJUHQZRE",rtseq=1,rtrep=.f.,;
    cFormVar = "w_NACODAZI", cQueryName = "NACODAZI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Azienda per cui � valida la codifica",;
    HelpContextID = 100008161,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=135, Top=9, InputMask=replicate('X',5), cLinkFile="AZIENDA", oKey_1_1="AZCODAZI", oKey_1_2="this.w_NACODAZI"

  func oNACODAZI_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oNAACTIVE_1_2 as StdCheck with uid="XVGHWJICIX",rtseq=2,rtrep=.f.,left=438, top=34, caption="Attivo",;
    ToolTipText = "Se attivo il codice dell'anagrafica verr� assegnato in automatico",;
    HelpContextID = 50192155,;
    cFormVar="w_NAACTIVE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oNAACTIVE_1_2.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oNAACTIVE_1_2.GetRadio()
    this.Parent.oContained.w_NAACTIVE = this.RadioValue()
    return .t.
  endfunc

  func oNAACTIVE_1_2.SetRadio()
    this.Parent.oContained.w_NAACTIVE=trim(this.Parent.oContained.w_NAACTIVE)
    this.value = ;
      iif(this.Parent.oContained.w_NAACTIVE=='S',1,;
      0)
  endfunc

  add object oNANOMPRO_1_3 as StdField with uid="NDSBGDLZTT",rtseq=3,rtrep=.f.,;
    cFormVar = "w_NANOMPRO", cQueryName = "NANOMPRO",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Nome procedura da eseguire per calcolare il codice",;
    HelpContextID = 161132325,;
   bGlobalFont=.t.,;
    Height=21, Width=325, Left=135, Top=57, InputMask=replicate('X',40)


  add object oBtn_1_4 as StdButton with uid="BEDORAZNCG",left=463, top=56, width=24,height=23,;
    caption="...", nPag=1;
    , ToolTipText = "Selezione file da eseguire";
    , HelpContextID = 193357354;
  , bGlobalFont=.t.

    proc oBtn_1_4.Click()
      with this.Parent.oContained
        .NotifyEvent('SelezionaFile')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oLinkPC_1_5 as stdDynamicChildContainer with uid="VLDLUOMVWQ",left=12, top=82, width=512, height=225, bOnScreen=.t.;



  add object oLinkPC_1_6 as stdDynamicChildContainer with uid="CHLJHMLBAH",left=11, top=313, width=518, height=171, bOnScreen=.t.;


  add object oAZRAGAZI_1_8 as StdField with uid="IGACJSXZTV",rtseq=4,rtrep=.f.,;
    cFormVar = "w_AZRAGAZI", cQueryName = "AZRAGAZI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 97712305,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=194, Top=9, InputMask=replicate('X',40)


  add object oNATIPGES_1_10 as StdCombo with uid="IRMEDYYTPE",rtseq=5,rtrep=.f.,left=135,top=34,width=239,height=21;
    , ToolTipText = "Tabella per cui � valida la codifica";
    , HelpContextID = 12914473;
    , cFormVar="w_NATIPGES",RowSource=""+"Conti contabilit� generale,"+"Mastri contabilit� generale,"+"Voci contabilit� analitica,"+"Clienti,"+"Fornitori,"+"Agenti,"+"Articoli,"+"Servizi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oNATIPGES_1_10.RadioValue()
    return(iif(this.value =1,'CG',;
    iif(this.value =2,'MG',;
    iif(this.value =3,'CA',;
    iif(this.value =4,'CL',;
    iif(this.value =5,'FO',;
    iif(this.value =6,'AG',;
    iif(this.value =7,'AR',;
    iif(this.value =8,'AS',;
    space(2))))))))))
  endfunc
  func oNATIPGES_1_10.GetRadio()
    this.Parent.oContained.w_NATIPGES = this.RadioValue()
    return .t.
  endfunc

  func oNATIPGES_1_10.SetRadio()
    this.Parent.oContained.w_NATIPGES=trim(this.Parent.oContained.w_NATIPGES)
    this.value = ;
      iif(this.Parent.oContained.w_NATIPGES=='CG',1,;
      iif(this.Parent.oContained.w_NATIPGES=='MG',2,;
      iif(this.Parent.oContained.w_NATIPGES=='CA',3,;
      iif(this.Parent.oContained.w_NATIPGES=='CL',4,;
      iif(this.Parent.oContained.w_NATIPGES=='FO',5,;
      iif(this.Parent.oContained.w_NATIPGES=='AG',6,;
      iif(this.Parent.oContained.w_NATIPGES=='AR',7,;
      iif(this.Parent.oContained.w_NATIPGES=='AS',8,;
      0))))))))
  endfunc

  func oNATIPGES_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
   endif
  endfunc

  func oNATIPGES_1_10.mHide()
    with this.Parent.oContained
      return (not isahr())
    endwith
  endfunc


  add object oNATIPGES_1_11 as StdCombo with uid="REJCYZFYQA",rtseq=6,rtrep=.f.,left=135,top=34,width=239,height=21;
    , ToolTipText = "Tabella per cui � valida la codifica";
    , HelpContextID = 12914473;
    , cFormVar="w_NATIPGES",RowSource=""+"Conti contabilit� generale,"+"Mastri contabilit� generale,"+"Voci contabilit� analitica,"+"Mastri contabilit� analitica,"+"Clienti,"+"Fornitori,"+"Agenti,"+"Articoli,"+"Servizi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oNATIPGES_1_11.RadioValue()
    return(iif(this.value =1,'CG',;
    iif(this.value =2,'MG',;
    iif(this.value =3,'CA',;
    iif(this.value =4,'MA',;
    iif(this.value =5,'CL',;
    iif(this.value =6,'FO',;
    iif(this.value =7,'AG',;
    iif(this.value =8,'AR',;
    iif(this.value =9,'AS',;
    space(2)))))))))))
  endfunc
  func oNATIPGES_1_11.GetRadio()
    this.Parent.oContained.w_NATIPGES = this.RadioValue()
    return .t.
  endfunc

  func oNATIPGES_1_11.SetRadio()
    this.Parent.oContained.w_NATIPGES=trim(this.Parent.oContained.w_NATIPGES)
    this.value = ;
      iif(this.Parent.oContained.w_NATIPGES=='CG',1,;
      iif(this.Parent.oContained.w_NATIPGES=='MG',2,;
      iif(this.Parent.oContained.w_NATIPGES=='CA',3,;
      iif(this.Parent.oContained.w_NATIPGES=='MA',4,;
      iif(this.Parent.oContained.w_NATIPGES=='CL',5,;
      iif(this.Parent.oContained.w_NATIPGES=='FO',6,;
      iif(this.Parent.oContained.w_NATIPGES=='AG',7,;
      iif(this.Parent.oContained.w_NATIPGES=='AR',8,;
      iif(this.Parent.oContained.w_NATIPGES=='AS',9,;
      0)))))))))
  endfunc

  func oNATIPGES_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
   endif
  endfunc

  func oNATIPGES_1_11.mHide()
    with this.Parent.oContained
      return (not isahe())
    endwith
  endfunc


  add object oBtn_1_14 as StdButton with uid="YFHPHWDAME",left=437, top=490, width=48,height=45,;
    CpPicture="bmp\check.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per verificare la composizione del codice";
    , HelpContextID = 165822998;
    , Caption='\<Verifica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      with this.Parent.oContained
        GSAR_BNU(this.Parent.oContained,"C")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_15 as StdButton with uid="UWOTKOJTRV",left=491, top=490, width=48,height=45,;
    CpPicture="bmp\GENERA.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare gli autonumber da generare";
    , HelpContextID = 165822998;
    , Caption='\<Autonum.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      with this.Parent.oContained
        GSAR_BNU(this.Parent.oContained,"A")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_7 as StdString with uid="UCKBOGAMIV",Visible=.t., Left=33, Top=34,;
    Alignment=1, Width=98, Height=19,;
    Caption="Tipo tabella:"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_9 as StdString with uid="MPBFMHWRPM",Visible=.t., Left=21, Top=60,;
    Alignment=1, Width=110, Height=18,;
    Caption="Nome procedura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="AKPWOXGJFZ",Visible=.t., Left=40, Top=490,;
    Alignment=0, Width=135, Height=17,;
    Caption="Campo per verifica"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_18 as StdString with uid="HJJLLKIJQZ",Visible=.t., Left=40, Top=509,;
    Alignment=0, Width=142, Height=17,;
    Caption="Variabile per verifica"    , ForeColor=RGB(0,0,255);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_19 as StdString with uid="DECFZFFLJD",Visible=.t., Left=1, Top=551,;
    Alignment=0, Width=838, Height=21,;
    Caption="I controlli sull'aggiornamento autonumber e sulla validit� delle espressioni � gestito nel manual block 'Check Form'"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_19.mHide()
    with this.Parent.oContained
      return (.t.)
    endwith
  endfunc

  add object oStr_1_21 as StdString with uid="BXYYXTURBG",Visible=.t., Left=61, Top=9,;
    Alignment=1, Width=70, Height=19,;
    Caption="Azienda:"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_21.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_22 as StdString with uid="CMNOGKBUCS",Visible=.t., Left=61, Top=9,;
    Alignment=1, Width=70, Height=19,;
    Caption="Studio:"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_22.mHide()
    with this.Parent.oContained
      return (NOT IsAlt())
    endwith
  endfunc
enddefine
define class tgsar_anuPag2 as StdContainer
  Width  = 543
  height = 537
  stdWidth  = 543
  stdheight = 537
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oEXPRESULT_2_1 as StdMemo with uid="SDLVEPCQSQ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_EXPRESULT", cQueryName = "EXPRESULT",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 203287250,;
   bGlobalFont=.t.,;
    Height=517, Width=532, Left=5, Top=12
enddefine
define class tgsar_anuPag3 as StdContainer
  Width  = 543
  height = 537
  stdWidth  = 543
  stdheight = 537
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZOOMAUT as cp_szoombox with uid="KRDMEPCCSS",left=5, top=14, width=534,height=457,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="CONTI",cZoomFile="GSAR_BNU",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.f.,bQueryOnLoad=.t.,cMenuFile="",cZoomOnZoom="",;
    cEvent = "Load",;
    nPag=3;
    , HelpContextID = 252874726

  add object oSELEZI_3_2 as StdRadio with uid="RDFUYVXOXW",rtseq=9,rtrep=.f.,left=15, top=478, width=129,height=32;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=3;
  , bGlobalFont=.t.

    proc oSELEZI_3_2.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 56660774
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 56660774
      this.Buttons(2).Top=15
      this.SetAll("Width",127)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_3_2.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_3_2.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_3_2.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc


  add object oBtn_3_3 as StdButton with uid="FTBYGUKXVD",left=490, top=489, width=48,height=45,;
    CpPicture="bmp\GENERA.bmp", caption="", nPag=3;
    , ToolTipText = "Premere per aggiornare i progressivi";
    , HelpContextID = 165822998;
    , Caption='\<Genera';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_3.Click()
      with this.Parent.oContained
        GSAR_BNU(this.Parent.oContained,"U")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_3_6 as StdButton with uid="LCPAQFVWVH",left=439, top=489, width=48,height=45,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per aprire il codice progressivo";
    , HelpContextID = 165822998;
    , Caption='\<Apri';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_6.Click()
      with this.Parent.oContained
        GSAR_BNU(this.Parent.oContained,"O")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_6.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!EMPTY(.w_CHIAVEARC))
      endwith
    endif
  endfunc


  add object oBtn_3_8 as StdButton with uid="EHFVNFRHES",left=388, top=489, width=48,height=45,;
    CpPicture="BMP\APPLICA.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per effettuare aggiornamento autonumber";
    , HelpContextID = 165822998;
    , Caption='\<Aggiorna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_8.Click()
      with this.Parent.oContained
        GSAR_BNU(this.Parent.oContained,"A")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_anu','NUMAUT_M','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".NACODAZI=NUMAUT_M.NACODAZI";
  +" and "+i_cAliasName2+".NATIPGES=NUMAUT_M.NATIPGES";
  +" and "+i_cAliasName2+".NATIPGES=NUMAUT_M.NATIPGES";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
