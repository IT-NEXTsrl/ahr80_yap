* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bfd                                                        *
*              Generazione fatture differite                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_845]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-10-13                                                      *
* Last revis.: 2018-06-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bfd",oParentObject,m.pOper)
return(i_retval)

define class tgsve_bfd as StdBatch
  * --- Local variables
  pOper = space(1)
  w_TIPOPE = space(1)
  w_APPO = space(10)
  w_SERIAL = space(10)
  w_NUDOC = 0
  w_ROWNUM = 0
  w_OKDOC = 0
  w_CODART = space(20)
  w_DESART = space(40)
  w_CODICE1 = space(20)
  w_DESSUP = space(0)
  w_CODCON = space(15)
  w_TIPCON = space(1)
  w_VALSIM = space(5)
  w_RETBFA = .f.
  w_COMMDEFA = space(15)
  w_SALCOM = space(1)
  w_CODCOM = space(15)
  w_LAST = 0
  w_FIRST = 0
  w_RAGG = .f.
  w_COPYSPED = space(1)
  w_ORDERBY = space(254)
  w_DATAGGPO = space(254)
  w_DATAGGAN = space(254)
  w_DACAM_01 = space(30)
  w_DACAM_02 = space(30)
  w_DACAM_03 = space(30)
  w_DACAM_04 = space(30)
  w_DACAM_05 = space(30)
  w_DACAM_06 = space(30)
  w_DAPRI_01 = 0
  w_DAPRI_02 = 0
  w_DAPRI_03 = 0
  w_DAPRI_04 = 0
  w_DAPRI_05 = 0
  w_DAPRI_06 = 0
  w_DAORD_01 = space(1)
  w_DAORD_02 = space(1)
  w_DAORD_03 = space(1)
  w_DAORD_04 = space(1)
  w_DAORD_05 = space(1)
  w_DAORD_06 = space(1)
  w_IDXFLDAN = 0
  w_IDXFLDAT = 0
  w_PRIATTFL = 0
  w_DO_ORDER = .f.
  w_oERRORLOG = .NULL.
  w_DBRK = space(10)
  w_OBRK = space(10)
  w_NEWDES = space(10)
  w_INIPAG = .f.
  w_FIRSTROW = .f.
  w_ROTT_T = space(1)
  w_ROTT_I = space(1)
  w_ROTT_B = space(1)
  w_ROTTURA = space(11)
  w_IMPPAG = space(1)
  w_FLSILI = space(1)
  w_TDRIPCON = space(1)
  w_OK_LET = .f.
  w_NWARN = 0
  w_TIPOPEDIC = space(1)
  w_IMPDIC = 0
  w_IMPUTI = 0
  w_DDIMPUTI = 0
  w_MVCODCLI = space(15)
  w_CAMBIO = .f.
  w_OLDRIFDIC = space(15)
  w_IVAAGE = space(5)
  w_OLDAGG1 = space(15)
  w_ROTTAGG1 = 0
  w_OLDROTTAGG1 = 0
  w_OLDAGG2 = space(15)
  w_ROTTAGG2 = 0
  w_OLDROTTAGG2 = 0
  w_OLDAGG3 = space(15)
  w_ROTTAGG3 = 0
  w_OLDROTTAGG3 = 0
  w_OLDAGG4 = space(15)
  w_ROTTAGG4 = 0
  w_OLDROTTAGG4 = 0
  w_OLDAGG5 = ctod("  /  /  ")
  w_ROTTAGG5 = 0
  w_OLDROTTAGG5 = 0
  w_OLDAGG6 = ctod("  /  /  ")
  w_ROTTAGG6 = 0
  w_OLDROTTAGG6 = 0
  w_FULLAGG_01 = space(15)
  w_FULLAGG_02 = space(15)
  w_FULLAGG_03 = space(15)
  w_FULLAGG_04 = space(15)
  w_FULLAGG_05 = ctod("  /  /  ")
  w_FULLAGG_06 = ctod("  /  /  ")
  w_PASCONTO = 0
  w_PASPEINC = 0
  w_OLDSPEINC = 0
  w_OLDSCOPAG = 0
  w_ANPAGFOR = space(5)
  w_PAVALINC = space(3)
  w_PAVALIN2 = space(3)
  w_PASPEIN2 = 0
  w_OLDCODPAG = space(5)
  w_CAMBIAPAG = .f.
  w_INDIVE = 0
  w_MVSERNOCA = space(10)
  w_ORCLADOC = space(2)
  w_OK_LETD = .f.
  w_DDOC = ctod("  /  /  ")
  w_NDOC = 0
  w_ADOC = space(10)
  w_WARNPAG = .f.
  w_LTIPOPE = space(1)
  w_LDATDOC = ctod("  /  /  ")
  w_DATAREG = ctod("  /  /  ")
  w_LCODORN = space(15)
  w_LTIPOPE = space(10)
  w_LCODIVA = space(5)
  w_OPREZZO = 0
  w_TESTESPLACC = space(3)
  w_TDFLAPCA = space(1)
  w_OFLAPCA = space(0)
  w_RIFCAC = 0
  w_CACONT = space(5)
  w_DCFLRIPS = space(1)
  w_CALPRZ = 0
  w_CATART = space(5)
  w_GRUMER = space(5)
  w_CATCLI = space(5)
  w_LCATCOM = space(3)
  w_SCOLIS = space(1)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_UNMIS3 = space(3)
  w_OPERAT = space(1)
  w_OPERA3 = space(1)
  w_IVALIS = space(1)
  w_APPOLIS = space(10)
  w_MOLTIP = 0
  w_MOLTI3 = 0
  w_INDIVA = 0
  w_LIPREZZO = 0
  w_IVACON = space(1)
  w_PREZUM = space(1)
  w_QTAUM2 = 0
  w_QTAUM3 = 0
  w_CLUNIMIS = space(3)
  w_QTALIS = 0
  w_LISCON = 0
  w_TDORDAPE = space(1)
  w_MVCODATT = space(15)
  w_NEXTFAT = ctod("  /  /  ")
  w_DATEVA = ctod("  /  /  ")
  w_FLEVOA = .f.
  w_MD__FREQ = space(1)
  w_APNSRI = space(1)
  w_APVSRI = space(1)
  w_APRIDE = space(1)
  w_FLEVAORD = .f.
  w_CPROWNUM = 0
  w_MVFLSCAF = space(1)
  w_MVCONTRO = space(15)
  w_MVCLADOC = space(2)
  w_MVSPEINC = 0
  w_CPROWORD = 0
  w_MVCONTRA = space(15)
  w_MVSERRIF = space(10)
  w_MVIVAINC = space(5)
  w_MVNUMRIF = 0
  w_MVCONIND = space(15)
  w_MVROWRIF = 0
  w_MVFLRINC = space(1)
  w_MVTIPRIG = space(1)
  w_MVCODLIS = space(5)
  w_MVPERPRO = 0
  w_MVSPEIMB = 0
  w_IVACAU = space(5)
  w_MVFLFOCA = space(1)
  w_FLFOCA = space(1)
  w_MVIVACAU = space(5)
  w_MVCAUIMB = 0
  w_CAUIMB = 0
  w_MVCODICE = space(20)
  w_MVQTAMOV = 0
  w_MVIMPPRO = 0
  w_MVIVAIMB = space(5)
  w_FLDDESOLD = space(5)
  w_TIPFAT = space(1)
  w_MVCODART = space(20)
  w_MVQTAUM1 = 0
  w_MVACCONT = 0
  w_MVFLRIMB = space(1)
  w_MVDESART = space(40)
  w_MVPREZZO = 0
  w_MVSCOCL1 = 0
  w_MVSPETRA = 0
  w_MVCODCEN = space(15)
  w_MVVOCCEN = space(15)
  w_MVCODCOM = space(15)
  w_MVDESSUP = space(10)
  w_MVSCONT1 = 0
  w_MVSCOCL2 = 0
  w_MVIVATRA = space(5)
  w_MVUNIMIS = space(3)
  w_MVSCONT2 = 0
  w_MVSCOPAG = 0
  w_MVFLRTRA = space(1)
  w_MVCATCON = space(5)
  w_MVSCONT3 = 0
  w_MVCAUMAG = space(5)
  w_MVSPEBOL = 0
  w_MVINICOM = ctod("  /  /  ")
  w_MVCODCLA = space(3)
  w_MVSCONT4 = 0
  w_MVMOLSUP = 0
  w_MVIVABOL = space(5)
  w_MVFINCOM = ctod("  /  /  ")
  w_MVCODCON = space(15)
  w_MVFLOMAG = space(1)
  w_MVNUMCOL = 0
  w_MVDATDIV = ctod("  /  /  ")
  w_MVFLSCOR = space(1)
  w_MVCODIVE = space(5)
  w_MVCODIVA = space(5)
  w_MVDATDOC = ctod("  /  /  ")
  w_MVDATPLA = ctod("  /  /  ")
  w_MVCODDES = space(5)
  w_MVCODAGE = space(5)
  w_MVCODPAG = space(5)
  w_MVSCONTI = 0
  w_MVVALNAZ = space(3)
  w_MVTIPORN = space(1)
  w_MVTCONTR = space(15)
  w_MVCODBAN = space(10)
  w_MVNUMCOR = space(25)
  w_MVIMPARR = 0
  w_MVCAOVAL = 0
  w_MVCODORN = space(15)
  w_MVTCOLIS = space(5)
  w_MVCODVAL = space(3)
  w_COCODDVAL = space(3)
  w_MVAIMPS1 = 0
  w_MVVALRIG = 0
  w_MVASPEST = space(30)
  w_MVACIVA1 = space(5)
  w_MVPESNET = 0
  w_MVAIMPS2 = 0
  w_MVIMPACC = 0
  w_MVQTACOL = 0
  w_MVACIVA2 = space(5)
  w_MVFLTRAS = space(1)
  w_MVAIMPS3 = 0
  w_MVIMPSCO = 0
  w_MVFLELGM = space(1)
  w_MVACIVA3 = space(5)
  w_MVNOMENC = space(8)
  w_MVAIMPS4 = 0
  w_MVVALMAG = 0
  w_MVIMPEVA = 0
  w_MVACIVA4 = space(5)
  w_MVUMSUPP = space(3)
  w_MVAIMPS5 = 0
  w_MVIMPNAZ = 0
  w_MVFLERIF = space(1)
  w_MVACIVA5 = space(5)
  w_MVAIMPN1 = 0
  w_MVAIMPS6 = 0
  w_MVAFLOM1 = space(1)
  w_MVFLARIF = space(1)
  w_MVACIVA6 = space(5)
  w_MVAIMPN2 = 0
  w_MVTOTENA = 0
  w_MVAFLOM2 = space(1)
  w_MVQTALOR = 0
  w_MVFLVEAC = space(1)
  w_MVEMERIC = space(1)
  w_MVAIMPN3 = 0
  w_MVAFLOM3 = space(1)
  w_MVCONCON = space(1)
  w_MVRIFFAD = space(10)
  w_MVFLSALD = space(1)
  w_MVACCPRE = 0
  w_MVAIMPN4 = 0
  w_MVAFLOM4 = space(1)
  w_MVCODPOR = space(1)
  w_MVQTAPES = 0
  w_MVTINCOM = ctod("  /  /  ")
  w_MVAIMPN5 = 0
  w_MVAFLOM5 = space(1)
  w_MVCODVET = space(5)
  w_MVCODVE2 = space(5)
  w_MVCODVE3 = space(5)
  w_MVCODBA2 = space(15)
  w_MVTFICOM = ctod("  /  /  ")
  w_MVAIMPN6 = 0
  w_MVAFLOM6 = space(1)
  w_MVCODSPE = space(3)
  w_MVRIFDIC = space(15)
  w_MVRIFODL = space(10)
  w_MVKEYSAL = space(20)
  w_MVTOTRIT = 0
  w_MVFLFOSC = space(1)
  w_MVNAZPRO = space(3)
  w_MVPROORD = space(2)
  w_MVDATEVA = ctod("  /  /  ")
  w_MVRIFFAD = space(10)
  w_MVCODAG2 = space(5)
  w_MVIMPCOM = 0
  w_APPART = space(10)
  w_DECTOT = 0
  w_CODECTOT = 0
  w_DECUNI = 0
  w_PERIVA = 0
  w_PEIINC = 0
  w_SPEINC = 0
  w_BOLESE = 0
  w_BOLIVA = space(1)
  w_BOLINC = space(1)
  w_SPEIMB = 0
  w_BOLSUP = 0
  w_MESE1 = 0
  w_PEIIMB = 0
  w_SPETRA = 0
  w_BOLCAM = 0
  w_MESE2 = 0
  w_BOLIMB = space(1)
  w_SPEBOL = 0
  w_BOLARR = 0
  w_GIORN1 = 0
  w_PEITRA = 0
  w_ACCONT = 0
  w_BOLMIN = 0
  w_GIORN2 = 0
  w_BOLTRA = space(1)
  w_IVAINC = space(5)
  w_TOTMERCE = 0
  w_GIOFIS = 0
  w_BOLBOL = space(1)
  w_IVAIMB = space(5)
  w_TOTALE = 0
  w_CLBOLFAT = space(1)
  w_BOLIVE = space(1)
  w_IVATRA = space(5)
  w_TOTIMPON = 0
  w_RSNUMRAT = 0
  w_ACQINT = space(1)
  w_MFLELGM = space(1)
  w_IVABOL = space(5)
  w_TOTIMPOS = 0
  w_RSDATRAT = ctod("  /  /  ")
  w_RSFLSOSP = space(1)
  w_MFLAVAL = space(1)
  w_PERIVE = 0
  w_CAOVAL = 0
  w_TOTFATTU = 0
  w_RSMODPAG = space(10)
  w_RSDESRIG = space(50)
  w_MCAUCOL = space(5)
  w_ACCPRE = 0
  w_IMPARR = 0
  w_CAONAZ = 0
  w_RSIMPRAT = 0
  w_TIPOIN = space(5)
  w_CODESE = space(4)
  w_ANTIPFAT = space(1)
  w_APPO1 = 0
  w_APPO2 = 0
  w_DETNUM = 0
  w_SERORI = space(10)
  w_NUMINI = 0
  w_CLIFOR = space(15)
  w_QTAP = 0
  w_ORDNUM = 0
  w_ROWORI = 0
  w_NUMFIN = 0
  w_QTAR = 0
  w_FLFOBO = space(1)
  w_DATAIN = ctod("  /  /  ")
  w_SPEIN2 = 0
  w_CODNAZ = space(3)
  w_DATAFI = ctod("  /  /  ")
  w_VALINC = space(3)
  w_NURIG = 0
  w_CALRATE = .f.
  w_CLADOC = space(2)
  w_VALIN2 = space(3)
  w_ERRDOC = .f.
  w_RICTOT = .f.
  w_PN = 0
  w_UM1 = space(3)
  w_PL = 0
  w_UM2 = space(3)
  w_PC = 0
  w_UM3 = space(3)
  w_CC = 0
  w_PN1 = 0
  w_PN2 = 0
  w_PL1 = 0
  w_PL2 = 0
  w_PC1 = 0
  w_PC2 = 0
  w_CC1 = 0
  w_CC2 = 0
  w_NCOL = 0
  w_RIFMES = space(100)
  w_TIPDOC = space(5)
  w_DESRIF = space(18)
  w_ARTDES = space(20)
  w_MESS = space(30)
  w_TIPCONTO = space(1)
  w_CODCONTO = space(15)
  w_OBDATDOC = ctod("  /  /  ")
  w_DATOBSO = ctod("  /  /  ")
  w_DATORIG = ctod("  /  /  ")
  w_MVDESDOC = space(50)
  w_OKINS = .f.
  w_PROANN = space(4)
  w_PROALF = space(10)
  w_PROPRD = space(2)
  w_PRODOC = 0
  w_RSBANNOS = space(15)
  w_RSBANAPP = space(10)
  w_RSCONCOR = space(25)
  w_RSFLPROV = space(1)
  w_MVFLVABD = space(1)
  w_CARMEM = 0
  w_MVTIPPRO = space(2)
  w_MVTIPPR2 = space(2)
  w_MVPROCAP = 0
  w_MVIMPCAP = 0
  w_MVFLNOAN = space(1)
  w_MVCODCES = space(20)
  w_DATCOM = ctod("  /  /  ")
  w_DECCOM = 0
  w_FLSERA = space(1)
  w_COCODVAL = space(3)
  w_RIFDIC = space(15)
  w_DOQTAEVA = 0
  w_DOIMPEVA = 0
  w_DLQTAEVA = 0
  w_DLIMPEVA = 0
  w_DOFLEVAS = space(1)
  w_O_IVAIMB = space(5)
  w_O_IVABOL = space(5)
  w_O_IVATRA = space(5)
  w_O_SPETRA = 0
  w_O_SPEIMB = 0
  w_O_SPEBOL = 0
  w_MVFLSCOM = space(1)
  w_CODLIN = space(3)
  w_MODRIF = space(5)
  w_BOLCAU = space(1)
  w_REVCAU = space(1)
  w_MVCODRES = space(5)
  w_NWARN = 0
  w_FLESIG = space(1)
  w_ERRID1 = .f.
  w_MVAGG_01 = space(15)
  w_MVAGG_02 = space(15)
  w_MVAGG_03 = space(15)
  w_MVAGG_04 = space(15)
  w_MVAGG_05 = ctod("  /  /  ")
  w_MVAGG_06 = ctod("  /  /  ")
  w_FLGRITE = space(1)
  w_LOOPR = 0
  w_IMPFIN = 0
  w_FLSFIN = 0
  w_LNREG = 0
  w_LDREG = ctod("  /  /  ")
  w_LSERIAL = space(10)
  w_DATREV = ctod("  /  /  ")
  w_TOTUTI = 0
  w_ERRDIC = .f.
  w_DICODIVA = space(5)
  w_DINUMDOC = 0
  w_DISERDOC = space(3)
  w_DI__ANNO = space(4)
  w_DIDATLET = ctod("  /  /  ")
  w_DIFLGCLO = space(1)
  w_MVACIVA = space(5)
  w_LOOP = .f.
  w_PERIND = 0
  w_MESS_BFA = space(254)
  w_MESS_SEQ = space(254)
  w_AZPLADOC = space(1)
  w_NUMRIGA = 0
  w_RIFDIC = space(10)
  w_DIDICCOL_FILTRO = space(10)
  w_LFLELAN = space(1)
  w_MAXROW = 0
  w_MVCACONT = space(5)
  w_MVRIFCAC = 0
  w_LROWORD = 0
  w_QTAIMP = 0
  w_QTAIM1 = 0
  w_MVCODMAG = space(5)
  w_MVCODMAT = space(5)
  w_MVFLIMPE = space(1)
  w_MVFLORDI = space(1)
  w_MVFLRISE = space(1)
  w_MVF2IMPE = space(1)
  w_MVF2ORDI = space(1)
  w_MVF2RISE = space(1)
  w_FLIMPE = space(1)
  w_FLORDI = space(1)
  w_MVQTASAL = 0
  w_PRZEVA = 0
  w_PPCALPRO = space(2)
  w_OLDCALPRO = space(2)
  w_PPCALSCO = space(1)
  w_PPLISRIF = space(5)
  w_MASSGEN = space(1)
  w_SERPRO = space(10)
  w_CODAZI = space(5)
  w_FRROWFAT = 0
  w_FRNUMFAT = 0
  w_FRSERDDT = space(10)
  w_FRROWDDT = 0
  w_FRNUMDDT = 0
  w_FRQTAIMP = 0
  w_FRQTAIM1 = 0
  w_RECACC = .f.
  w_MAXACC = 0
  w_SERACC = space(10)
  w_SERAC1 = space(10)
  w_NUMACC = 0
  w_NUMAC1 = 0
  w_CLADOC = space(2)
  w_TOTAFACC = 0
  w_SPEACC = 0
  w_SPEACC_IVA = 0
  w_STAMPA1 = space(30)
  w_QUERY = space(50)
  w_REPORT = space(50)
  w_TESTO = space(1)
  w_TIPCON1 = space(1)
  w_CODCON1 = space(15)
  w_DTOBS1 = ctod("  /  /  ")
  w_codic = space(15)
  w_nmdes = space(40)
  w_ind = space(40)
  w_cap = space(8)
  w_loca = space(30)
  w_prov = space(2)
  w_rif = space(2)
  w_TOTRATEBFA = 0
  w_TOTRATEORI = 0
  w_TOTDOC = 0
  w_MINRATA = 0
  w_ERRRAT = .f.
  w_FLINSE = .f.
  w_UPDA = .f.
  w_PROGRIGA = 0
  w_LOOP = .f.
  w_NUMDOC = 0
  w_MULSER = space(10)
  w_MULROW = 0
  w_MULRIF = 0
  w_ORNUMDOC = 0
  w_ORALFDOC = space(10)
  w_ORDATDOC = ctod("  /  /  ")
  w_ORTIPDOC = space(5)
  w_ORDESRIF = space(18)
  w_RIFCLI = space(0)
  w_NRIF = 0
  w_ARIF = space(10)
  w_DRIF = ctod("  /  /  ")
  w_APRIFCLI = space(0)
  w_APNRIF = 0
  w_APARIF = space(10)
  w_APDRIF = ctod("  /  /  ")
  w_MEMO = space(0)
  w_ANFLAPCA = space(1)
  w_COFRAZ = space(1)
  w_UNIMIS = space(3)
  w_KCODICE = space(20)
  w_KUNIMIS = space(3)
  w_KQTAMOV = 0
  w_KPREZZO = 0
  w_KCODIVA = space(3)
  w_CONTRIB = space(5)
  w_RIFCACESP = 0
  w_ATTPOS = 0
  w_SRCSERRIF = space(10)
  w_FATHERRIF = space(10)
  w_POSAPP = 0
  * --- WorkFile variables
  ART_ICOL_idx=0
  CAM_AGAZ_idx=0
  CAN_TIER_idx=0
  CONTI_idx=0
  DES_DIVE_idx=0
  DET_DIFF_idx=0
  DET_PIAS_idx=0
  DIC_INTE_idx=0
  DOC_COLL_idx=0
  DOC_DETT_idx=0
  DOC_MAST_idx=0
  DOC_RATE_idx=0
  KEY_ARTI_idx=0
  PAG_AMEN_idx=0
  PAR_PROV_idx=0
  RAG_FATT_idx=0
  SALDIART_idx=0
  LISTINI_idx=0
  TIP_DOCU_idx=0
  TMPVEND1_idx=0
  TMPVEND2_idx=0
  VALUTE_idx=0
  VOCIIVA_idx=0
  UNIMIS_idx=0
  MODCLDAT_idx=0
  AZIENDA_idx=0
  DATI_AGG_idx=0
  PAR_PROD_idx=0
  SALDICOM_idx=0
  DICDINTE_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Parametro Esegue: I = Non Spunta Documenti; D = Spunta Documenti
    * --- Generazione Automatica Documenti Fatture Differite (da GSVE_AFD)
    * --- Parametro Provenienza: I,D = Fatture Differite 
    * --- Se .T. trovate righe descrittive riferite a Documenti di Clienti con Fatturazione per Ordine, al termine, dovranno essere comunque evase
    this.w_FLEVAORD = .F.
    this.w_MVDATDIV = this.oParentObject.w_DATDIV
    this.w_MVDATDOC = this.oParentObject.w_DATDOC
    this.w_MVVALNAZ = g_PERVAL
    this.w_CAONAZ = GETCAM(this.w_MVVALNAZ, this.w_MVDATDOC, 0)
    this.w_MVFLVEAC = "V"
    this.w_MVEMERIC = this.w_MVFLVEAC
    this.w_FLFOBO = " "
    this.w_MVCLADOC = this.oParentObject.w_MVCLADOC
    * --- Parametri di Selezione Stampa
    this.w_TIPOIN = this.oParentObject.w_MVTIPDOC
    this.w_NUMINI = 999999999999999
    this.w_NUMFIN = 0
    this.w_DATAIN = i_FINDAT
    this.w_DATAFI = i_INIDAT
    this.w_CODESE = this.oParentObject.w_MVCODESE
    this.w_CLIFOR = ""
    this.w_ACQINT = "N"
    * --- Legge l'ultimo progressivo (per verifiche progressivi)
    this.w_PROANN = this.oParentObject.w_MVANNDOC
    this.w_PROALF = this.oParentObject.w_MVALFDOC
    this.w_PROPRD = this.oParentObject.w_MVPRD
    this.w_PRODOC = 0
    i_Conn=i_TableProp[this.DOC_MAST_IDX, 3]
    cp_AskTableProg(this, i_Conn, "PRDOC", "i_CODAZI,w_PROANN,w_PROPRD,w_PROALF,w_PRODOC")
    * --- Rate Scadenze
    DIMENSION DR[1000, 9]
    * --- Legge Informazioni di Riga di Default
    this.w_MVCAUMAG = this.oParentObject.w_MVTCAMAG
    this.w_MCAUCOL = SPACE(5)
    this.w_MFLELGM = " "
    this.w_MFLAVAL = " "
    this.w_MVRIFFAD = SPACE(10)
    * --- Se FLPACK='D' in Fattura Differita Notifica Selezionati Documenti Fattura da Maschera
    this.w_TIPOPE = IIF(this.pOper="I" AND this.oParentObject.w_FLPACK="D", "D", this.pOper)
    * --- Fatture Differite
    this.w_MVRIFFAD = this.oParentObject.w_PSSERIAL
    this.oParentObject.w_MVRIFPIA = SPACE(10)
    * --- Read from CAM_AGAZ
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CMFLELGM,CMFLAVAL"+;
        " from "+i_cTable+" CAM_AGAZ where ";
            +"CMCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVTCAMAG);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CMFLELGM,CMFLAVAL;
        from (i_cTable) where;
            CMCODICE = this.oParentObject.w_MVTCAMAG;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MFLELGM = NVL(cp_ToDate(_read_.CMFLELGM),cp_NullValue(_read_.CMFLELGM))
      this.w_MFLAVAL = NVL(cp_ToDate(_read_.CMFLAVAL),cp_NullValue(_read_.CMFLAVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Mi assicuro che la variabile sia space di 15 (per evitare problemi con Oracle)
    this.w_COMMDEFA = NVL(g_PPCODCOM, SPACE(15))
    if this.w_TIPOPE<>"D"
      * --- Elimina Cursore di Appoggio se non deve essere gestito
      USE IN SELECT("GeneApp")
    endif
    * --- Elenco dei Numeri e Date Documenti Inseriti
    *     La tabella TMPVEND2 creata a seguito di questa elaborazione � utilizzata 
    *     a pag9 per determinare se il numero documento che si sta inserendo � gia 
    *     esistente.
    *     Questa funzionalit� mi occorre per utilizzare la fatt. differita per tappare
    *     eventuali buchi nella numerazione...
    * --- Create temporary table TMPVEND1
    i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('QUERY\GSVE_BF2',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPVEND1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- TMPVEND2 contiene l'intervallo di progressivi in cui si vogliono inserire le fatture
    *     Cio� dal progressivo pi� grande con la prIma data pi� piccola a DATDOC al progressivo
    *     pi� piccolo con proma data pi� grande a datdoc
    * --- ES:
    *     MVNUMDOC = 1                 DATDOC = 02/01/05
    *     DOC_MAST                                          TMPVEND2
    *     NUMDOC            DATADOC                 NUMDOC                DATADOC             FLAGFL
    *         9                       04/01/05                      2                              01/01/05                 F
    *         6                       03/01/05                      3                              02/01/05
    *         5                       02/01/05                      5                              02/01/05
    *         3                       02/01/05                      6                              03/01/05                 L
    *         2                       01/01/05
    *         1                       01/01/05
    * --- Create temporary table TMPVEND2
    i_nIdx=cp_AddTableDef('TMPVEND2') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('QUERY\GSVE2BFD',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPVEND2_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- La tabella temporanea contiene un record fittizzio usato per impostare la lunghezza del campo NUMDOC  a 15 (la delete cancella il record fittizzio)
    * --- Delete from TMPVEND2
    i_nConn=i_TableProp[this.TMPVEND2_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND2_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"FLAGFL = "+cp_ToStrODBC("X");
             )
    else
      delete from (i_cTable) where;
            FLAGFL = "X";

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Drop temporary table TMPVEND1
    i_nIdx=cp_GetTableDefIdx('TMPVEND1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPVEND1')
    endif
    * --- Controllo multiutenza nelle raggruppate per Articolo
    this.w_RAGG = .F.
    * --- Leggo il primo numero dell'intervallo
    * --- Read from TMPVEND2
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.TMPVEND2_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND2_idx,2],.t.,this.TMPVEND2_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "NUMDOC"+;
        " from "+i_cTable+" TMPVEND2 where ";
            +"FLAGFL = "+cp_ToStrODBC("F");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        NUMDOC;
        from (i_cTable) where;
            FLAGFL = "F";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_FIRST = NVL(cp_ToDate(_read_.NUMDOC),cp_NullValue(_read_.NUMDOC))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if i_rows <> 1
      * --- Non esiste nessun record marcato come FIRST, quindi seleziono il primo
      *     record come FIRST 
      * --- Select from TMPVEND2
      i_nConn=i_TableProp[this.TMPVEND2_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND2_idx,2],.t.,this.TMPVEND2_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMPVEND2 ";
            +" where FLAGFL<>'L'";
            +" order by DATDOC, NUMDOC";
             ,"_Curs_TMPVEND2")
      else
        select * from (i_cTable);
         where FLAGFL<>"L";
         order by DATDOC, NUMDOC;
          into cursor _Curs_TMPVEND2
      endif
      if used('_Curs_TMPVEND2')
        select _Curs_TMPVEND2
        locate for 1=1
        do while not(eof())
        * --- Write into TMPVEND2
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMPVEND2_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND2_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND2_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"FLAGFL ="+cp_NullLink(cp_ToStrODBC("F"),'TMPVEND2','FLAGFL');
              +i_ccchkf ;
          +" where ";
              +"NUMDOC = "+cp_ToStrODBC(NUMDOC);
                 )
        else
          update (i_cTable) set;
              FLAGFL = "F";
              &i_ccchkf. ;
           where;
              NUMDOC = NUMDOC;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.w_FIRST = NUMDOC
        EXIT
          select _Curs_TMPVEND2
          continue
        enddo
        use
      endif
    endif
    * --- Leggo l'ultimo numero dell'intervallo
    * --- Read from TMPVEND2
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.TMPVEND2_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND2_idx,2],.t.,this.TMPVEND2_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "NUMDOC"+;
        " from "+i_cTable+" TMPVEND2 where ";
            +"FLAGFL = "+cp_ToStrODBC("L");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        NUMDOC;
        from (i_cTable) where;
            FLAGFL = "L";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_LAST = NVL(cp_ToDate(_read_.NUMDOC),cp_NullValue(_read_.NUMDOC))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if i_rows <> 1
      * --- Non esiste nessun record marcato come LAST, quindi inserisco come limite
      *     superiore dell'intervallo 999999, non ho limite superiore
      * --- Insert into TMPVEND2
      i_nConn=i_TableProp[this.TMPVEND2_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND2_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPVEND2_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"NUMDOC"+",DATDOC"+",FLAGFL"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(999999999999999),'TMPVEND2','NUMDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVDATDOC),'TMPVEND2','DATDOC');
        +","+cp_NullLink(cp_ToStrODBC("L"),'TMPVEND2','FLAGFL');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'NUMDOC',999999999999999,'DATDOC',this.w_MVDATDOC,'FLAGFL',"L")
        insert into (i_cTable) (NUMDOC,DATDOC,FLAGFL &i_ccchkf. );
           values (;
             999999999999999;
             ,this.w_MVDATDOC;
             ,"L";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      this.w_LAST = 999999999999999
    endif
    if this.w_LAST <> 999999999999999 AND this.oParentObject.w_MVNUMDOC <= this.w_LAST
      * --- Trovato un documento con lo stesso numero iniziale o inferiore
      this.w_APPO = "Esistono documenti con numero fattura uguale o superiore al primo impostato; proseguo?%0(Il numero documento verr� calcolato in base al primo numero disponibile successivo)"
      if NOT ah_YesNo(this.w_APPO)
        i_retcode = 'stop'
        return
      endif
      * --- Inizia a Contare dal Primo Inserito
      this.oParentObject.w_MVNUMDOC = this.w_FIRST
    else
      * --- Decremento di 1 affinche l'elaborazione parta dal numero che ho specificato 
      *     sulla maschera
      this.oParentObject.w_MVNUMDOC = this.oParentObject.w_MVNUMDOC - 1
    endif
    * --- Controllo se devo considerare Spedizione e Condizione di consegna come campi
    *     di rottura.
    *     Solo se Intra e contabilit� generale presenti, se ho l'Obbligatoriet� delle cessioni o Acquisti e cessini
    *     e se genero una fattura intra
    if g_INTR="S" And g_COGE="S" And this.oParentObject.w_DTOBBL$"C-E" And this.oParentObject.w_CCTIPDOC = "FE"
      this.w_COPYSPED = "S"
    else
      this.w_COPYSPED = "N"
    endif
    * --- Carica i Documenti da Generare
    this.w_ORDERBY = ""
    this.w_DATAGGPO = ""
    this.w_DATAGGAN = ""
    this.w_ORDERBY = this.w_ORDERBY + "TDORDAPE, ORDCLI, DATORD, ALFORD, NUMORD, TDFLAPCA, FLSERI, CODIVE,CODAGE, CODAG2, CODPAG, CODBAN, "
    this.w_ORDERBY = this.w_ORDERBY + "NUMCOR, CODBA2, CODVAL,CAOORD,SCOCL1,SCOCL2,SCOPAG,FLSCOR,FLRAGG,FLRAG1,FLDDES,CONORD,SPEORD,TESTCAU, FLRIMB, FLTRAS, "
    this.w_ORDERBY = this.w_ORDERBY + "MVFLRINC,  MVRIFDIC, MVFLSCOM, MVIVACAU, MVFLFOSC, MVFLVABD, IVAINC, IVATRA, IVAIMB, IVABOL, "
    this.w_ORDERBY = this.w_ORDERBY + "@@DATIAGGPOSTORDER@@  MVDATDOC, MVALFDOC, MVNUMDOC, SERIAL, ROWORD"
    * --- Read from DATI_AGG
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DATI_AGG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DATI_AGG_idx,2],.t.,this.DATI_AGG_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "DACAM_01,DACAM_02,DACAM_03,DACAM_04,DACAM_05,DACAM_06,DAPRI_01,DAPRI_02,DAPRI_03,DAPRI_04,DAPRI_05,DAPRI_06,DAORD_01,DAORD_02,DAORD_03,DAORD_04,DAORD_05,DAORD_06"+;
        " from "+i_cTable+" DATI_AGG where ";
            +"DASERIAL = "+cp_ToStrODBC("0000000001");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        DACAM_01,DACAM_02,DACAM_03,DACAM_04,DACAM_05,DACAM_06,DAPRI_01,DAPRI_02,DAPRI_03,DAPRI_04,DAPRI_05,DAPRI_06,DAORD_01,DAORD_02,DAORD_03,DAORD_04,DAORD_05,DAORD_06;
        from (i_cTable) where;
            DASERIAL = "0000000001";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DACAM_01 = NVL(cp_ToDate(_read_.DACAM_01),cp_NullValue(_read_.DACAM_01))
      this.w_DACAM_02 = NVL(cp_ToDate(_read_.DACAM_02),cp_NullValue(_read_.DACAM_02))
      this.w_DACAM_03 = NVL(cp_ToDate(_read_.DACAM_03),cp_NullValue(_read_.DACAM_03))
      this.w_DACAM_04 = NVL(cp_ToDate(_read_.DACAM_04),cp_NullValue(_read_.DACAM_04))
      this.w_DACAM_05 = NVL(cp_ToDate(_read_.DACAM_05),cp_NullValue(_read_.DACAM_05))
      this.w_DACAM_06 = NVL(cp_ToDate(_read_.DACAM_06),cp_NullValue(_read_.DACAM_06))
      this.w_DAPRI_01 = NVL(cp_ToDate(_read_.DAPRI_01),cp_NullValue(_read_.DAPRI_01))
      this.w_DAPRI_02 = NVL(cp_ToDate(_read_.DAPRI_02),cp_NullValue(_read_.DAPRI_02))
      this.w_DAPRI_03 = NVL(cp_ToDate(_read_.DAPRI_03),cp_NullValue(_read_.DAPRI_03))
      this.w_DAPRI_04 = NVL(cp_ToDate(_read_.DAPRI_04),cp_NullValue(_read_.DAPRI_04))
      this.w_DAPRI_05 = NVL(cp_ToDate(_read_.DAPRI_05),cp_NullValue(_read_.DAPRI_05))
      this.w_DAPRI_06 = NVL(cp_ToDate(_read_.DAPRI_06),cp_NullValue(_read_.DAPRI_06))
      this.w_DAORD_01 = NVL(cp_ToDate(_read_.DAORD_01),cp_NullValue(_read_.DAORD_01))
      this.w_DAORD_02 = NVL(cp_ToDate(_read_.DAORD_02),cp_NullValue(_read_.DAORD_02))
      this.w_DAORD_03 = NVL(cp_ToDate(_read_.DAORD_03),cp_NullValue(_read_.DAORD_03))
      this.w_DAORD_04 = NVL(cp_ToDate(_read_.DAORD_04),cp_NullValue(_read_.DAORD_04))
      this.w_DAORD_05 = NVL(cp_ToDate(_read_.DAORD_05),cp_NullValue(_read_.DAORD_05))
      this.w_DAORD_06 = NVL(cp_ToDate(_read_.DAORD_06),cp_NullValue(_read_.DAORD_06))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    LOCAL TestMacro
    this.w_IDXFLDAN = 1
    this.w_DO_ORDER = "P" $ ALLTRIM(this.w_DAORD_01)+ALLTRIM(this.w_DAORD_02)+ALLTRIM(this.w_DAORD_03)+ALLTRIM(this.w_DAORD_04)+ALLTRIM(this.w_DAORD_05)+ALLTRIM(this.w_DAORD_06)
    this.w_DO_ORDER = this.w_DO_ORDER OR "D" $ ALLTRIM(this.w_DAORD_01)+ALLTRIM(this.w_DAORD_02)+ALLTRIM(this.w_DAORD_03)+ALLTRIM(this.w_DAORD_04)+ALLTRIM(this.w_DAORD_05)+ALLTRIM(this.w_DAORD_06)
    this.w_DO_ORDER = this.w_DO_ORDER OR "S" $ ALLTRIM(this.w_DAORD_01)+ALLTRIM(this.w_DAORD_02)+ALLTRIM(this.w_DAORD_03)+ALLTRIM(this.w_DAORD_04)+ALLTRIM(this.w_DAORD_05)+ALLTRIM(this.w_DAORD_06)
    this.w_DO_ORDER = this.w_DO_ORDER OR "F" $ ALLTRIM(this.w_DAORD_01)+ALLTRIM(this.w_DAORD_02)+ALLTRIM(this.w_DAORD_03)+ALLTRIM(this.w_DAORD_04)+ALLTRIM(this.w_DAORD_05)+ALLTRIM(this.w_DAORD_06)
    if this.w_DO_ORDER
      private L_MACRO
      do while this.w_IDXFLDAN<=6
        * --- Ricerco il campo con priorit� pi� alta
        this.w_IDXFLDAT = 1
        L_MACRO = "this.w_DAPRI_"+RIGHT("00"+ALLTRIM(STR(this.w_IDXFLDAT)), 2)
        this.w_PRIATTFL = &L_MACRO
        do while this.w_PRIATTFL <> this.w_IDXFLDAN
          this.w_IDXFLDAT = this.w_IDXFLDAT + 1
          L_MACRO = "this.w_DAPRI_"+RIGHT("00"+ALLTRIM(STR(this.w_IDXFLDAT)), 2)
          this.w_PRIATTFL = &L_MACRO
        enddo
        L_MACRO = "this.oParentobject.w_TDFLIA"+RIGHT("00"+ALLTRIM(STR(this.w_IDXFLDAT)), 2)
        TestMacro=&L_MACRO
        if TestMacro= "S"
          L_MACRO = "this.w_DAORD_"+RIGHT("00"+ALLTRIM(STR(this.w_IDXFLDAT)), 2)
          * --- Se il campo aggiuntivo non deve essere ignorato per gli ordinamenti lo metto nelle
          *     variabili dedicate per farlo anticipare o seguire gli ordinamenti standard
          do case
            case &L_MACRO = "P"
              this.w_DATAGGAN = this.w_DATAGGAN + "MVAGG_"+RIGHT("00"+ALLTRIM(STR(this.w_IDXFLDAT)), 2)+", "
            case &L_MACRO = "D"
              this.w_DATAGGAN = this.w_DATAGGAN + "MVAGG_"+RIGHT("00"+ALLTRIM(STR(this.w_IDXFLDAT)), 2)+" DESC, "
            case &L_MACRO = "S"
              this.w_DATAGGPO = this.w_DATAGGPO + "MVAGG_"+RIGHT("00"+ALLTRIM(STR(this.w_IDXFLDAT)), 2)+", "
            case &L_MACRO = "F"
              this.w_DATAGGPO = this.w_DATAGGPO + "MVAGG_"+RIGHT("00"+ALLTRIM(STR(this.w_IDXFLDAT)), 2)+" DESC, "
          endcase
        endif
        this.w_IDXFLDAN = this.w_IDXFLDAN + 1
      enddo
      release L_MACRO
    endif
    this.w_ORDERBY = this.w_DATAGGAN + this.w_ORDERBY
    this.w_ORDERBY = STRTRAN(this.w_ORDERBY, "@@DATIAGGPOSTORDER@@", this.w_DATAGGPO, 1, 1, 2) 
    * --- Carica i Documenti da Generare
    ah_Msg("Ricerca documenti da generare...")
    * --- GSVE_KGF e GSVE1KGF in union per documenti senza e con Per conto di...
    vq_exec("query\GSVE_KGF.VQR",this,"GENEFATT")
    this.w_ORDERBY = ""
    * --- Geneapp contiene:
    *     Se apro la maschera dei dettagli:
    *       -  Le righe deselezionate singolarmente dall'utente e per via del filtro su FLVALO con il CPROWNUM della singola riga
    *       -  I seriali con 9999 come CPROWNUM dei documenti deselezionati dal master o deselezionati per FLVALO su Nessuno
    *     
    *     Se non apro la maschera, Geneapp esiste solo se indico FLVALO a N (Documenti non valorizzati: Nessuno)  
    *          In questo caso GeneApp contiene i seriali dei documenti che hanno almeno una riga a 0 senza considerare le descrittive.
    *          Nel caso invece un documento abbia tutte le righe non valorizzate la query GSVE1KFD messa in IN nella GSVE_KGF e GSVE1KGF funziona come filtro
    *          Genefatt in questo caso non contiene gi� le righe che poi estrarra la query GSVE_KGD
    if USED("GENEFATT")
      * --- A questo Punto Sfronda tutti quelli Selezionati Tramite Spunta
      if USED("GeneApp")
        ah_Msg("Eliminazione dei documenti da non generare...")
        SELECT GeneApp
        GO TOP
        SCAN FOR NOT EMPTY(NVL(MVSERIAL," "))
        this.w_SERIAL = MVSERIAL
        this.w_ROWNUM = NVL(CPROWNUM, 0)
        if this.w_ROWNUM = 9999
          * --- Elimina Tutti i Documenti
          DELETE FROM GENEFATT WHERE SERIAL = this.w_SERIAL
        else
          * --- Elimina la Singola Riga
          DELETE FROM GENEFATT WHERE SERIAL = this.w_SERIAL AND CPROWNUM = this.w_ROWNUM
        endif
        SELECT GeneApp
        ENDSCAN
        SELECT GeneApp
        USE
      endif
      this.w_ARTDES = SPACE(20)
      if NOT EMPTY(g_ARTDES)
        * --- Read from KEY_ARTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CACODART"+;
            " from "+i_cTable+" KEY_ARTI where ";
                +"CACODICE = "+cp_ToStrODBC(g_ARTDES);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CACODART;
            from (i_cTable) where;
                CACODICE = g_ARTDES;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_ARTDES = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      * --- Inizio Aggiornamento vero e proprio
      this.w_NUDOC = 0
      this.w_OKDOC = 0
      if RECCOUNT("GENEFATT") > 0
        ah_Msg("Inizio fase di generazione...")
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_APPO = "Operazione completata%0N. %1 documenti generati%0su %2 documenti da generare"
        ah_ErrorMsg(this.w_APPO,"i","", ALLTRIM(STR(this.w_OKDOC)), ALLTRIM(STR(this.w_NUDOC)) )
        * --- LOG Errori
        if this.w_ErrDoc
          this.w_oERRORLOG.PrintLog(this.oParentObject,"Messaggi di errore")     
        else
          this.w_oERRORLOG.PrintLog(this.oParentObject,"Messaggi di warning")     
        endif
        if this.w_OKDOC<>0 AND this.oParentObject.w_STADOC="S"
          * --- Lancia il Report Fatture Differite
          this.Page_6()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      else
        this.w_APPO = "Per l'intervallo selezionato non esistono documenti da generare"
        ah_ErrorMsg(this.w_APPO)
      endif
      * --- Chiude i Cursori lasciati Aperti
      USE IN SELECT("GeneFatt")
    endif
    * --- Elimina Cursore di Appoggio se esiste ancora
    USE IN SELECT("GeneApp")
    USE IN SELECT("EvaDesc")
    USE IN SELECT("TestAcc")
    USE IN SELECT("TestAcc1")
    USE IN SELECT("AppRagg")
    USE IN SELECT("FattAcc")
    USE IN SELECT("__TMP__")
    * --- Drop temporary table TMPVEND2
    i_nIdx=cp_GetTableDefIdx('TMPVEND2')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPVEND2')
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Genera i Documenti di Fattura Differita
    * --- Stanzia l'oggetto per la gestione delle messaggistiche di errore
    this.w_oERRORLOG=createobject("AH_ErrorLog")
    * --- Cursore Evasione Righe descrittive (solo per Fatturazione x Ordine)
    *     Contiene fattura per fattura eventuali righe descrittive del DDT
    *     non importate (con SERRIF vuoto)
    CREATE CURSOR EvaDesc (SERIAL C(10), ROWNUM N(4) , NUMRIF N(4) )
    * --- Testa i Cambi Documento di Destinazione e Documento Origine
    this.w_DBRK = "##zz##"
    this.w_OBRK = "##zz##"
    this.w_INIPAG = .T.
    this.w_DETNUM = 0
    this.w_ORDNUM = 0
    this.w_OKINS = .T.
    this.w_FIRSTROW = .T.
    * --- Variabili utilizzate per eseguire rottura nei casi previsti sulle spese accessorie e codici Iva delle spese
    *     Sul primo record sempre 'N'
    this.w_ROTT_T = "N"
    this.w_ROTT_I = "N"
    this.w_ROTT_B = "N"
    this.w_OLDROTTAGG1 = 0
    this.w_OLDROTTAGG2 = 0
    this.w_OLDROTTAGG3 = 0
    this.w_OLDROTTAGG4 = 0
    this.w_OLDROTTAGG5 = 0
    this.w_OLDROTTAGG6 = 0
    * --- Assegno un valore random che poi verr� modificato solo se devo fare rottura
    *     per i codici Iva Spese
    this.w_ROTTURA = Sys(2015)
    this.w_NWARN = 0
    * --- Read from TIP_DOCU
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "TDFLIMPA,TDFLSILI,TDFLAPCA,TDRIPCON"+;
        " from "+i_cTable+" TIP_DOCU where ";
            +"TDTIPDOC = "+cp_ToStrODBC(this.oParentObject.w_MVTIPDOC);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        TDFLIMPA,TDFLSILI,TDFLAPCA,TDRIPCON;
        from (i_cTable) where;
            TDTIPDOC = this.oParentObject.w_MVTIPDOC;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_IMPPAG = NVL(cp_ToDate(_read_.TDFLIMPA),cp_NullValue(_read_.TDFLIMPA))
      this.w_FLSILI = NVL(cp_ToDate(_read_.TDFLSILI),cp_NullValue(_read_.TDFLSILI))
      this.w_TDFLAPCA = NVL(cp_ToDate(_read_.TDFLAPCA),cp_NullValue(_read_.TDFLAPCA))
      this.w_TDRIPCON = NVL(cp_ToDate(_read_.TDRIPCON),cp_NullValue(_read_.TDRIPCON))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    DECLARE ARRDIC (14,1)
    SELECT DISTINCT CODCLI, MVRIFDIC FROM GENEFATT INTO CURSOR LETTINT WHERE MVCLADOC <>"DT"
    SELECT LETTINT 
 GO TOP 
 SCAN
    * --- Lettura lettera di intento valida
    * --- Azzero l'Array che verr� riempito dalla Funzione
    ARRDIC(1)=0
    this.w_MVRIFDIC = NVL(MVRIFDIC, SPACE(15))
    this.w_OLDRIFDIC = NVL(MVRIFDIC, SPACE(15))
    this.w_MVCODCLI = NVL(CODCLI, SPACE(15))
    this.w_OK_LET = .F.
    if NOT EMPTY(this.w_MVRIFDIC)
      * --- Testo se la lettera d'intento � ancora valida (passando il seriale della lettera associata al documento d'origine)
      this.w_OK_LET = CAL_LETT(this.oParentObject.w_DATDOC,"C",this.w_MVCODCLI, @ArrDic, this.w_MVRIFDIC)
    endif
    if this.w_OK_LET
      * --- Se la lettera del doc. d'origine � ancora valida la lascio
    else
      * --- Se la lettera del doc. d'origine non � pi� valida cerco se ce ne � una valida alla data fattura
      this.w_OK_LET = CAL_LETT(this.oParentObject.w_DATDOC,"C",this.w_MVCODCLI, @ArrDic)
    endif
    if this.w_OK_LET
      * --- Trovata
      this.w_MVRIFDIC = Arrdic(11)
      this.w_IVAAGE = Arrdic(8)
    else
      this.w_MVRIFDIC = space(15)
    endif
    if this.w_MVRIFDIC<>this.w_OLDRIFDIC
      this.w_CAMBIO = .T.
      SELECT GENEFATT
      * --- Effettuo la replace solo se effettivamente il campo mvrifdic � stato cambiato
      if NOT EMPTY(this.w_MVRIFDIC)
        REPLACE MVRIFDIC WITH this.w_MVRIFDIC, CODIVE WITH this.w_IVAAGE FOR CODCLI=this.w_MVCODCLI AND NVL(MVRIFDIC,SPACE(15)) =this.w_OLDRIFDIC AND MVCLADOC<>"DT"
      else
        REPLACE MVRIFDIC WITH this.w_MVRIFDIC, CODIVE WITH space(5), IVAINC WITH g_COIINC, IVAIMB WITH g_COIIMB, IVATRA WITH g_COITRA FOR CODCLI=this.w_MVCODCLI AND NVL(MVRIFDIC,SPACE(15)) =this.w_OLDRIFDIC AND MVCLADOC<>"DT"
      endif
    endif
    SELECT LETTINT
    ENDSCAN
    USE IN SELECT("Lettint")
    if this.w_CAMBIO
      * --- Riordino il cursore solo se effettivamente il campo mvrifdic � stato cambiato
      SELECT * from GENEFATT into cursor GENEFATT order by 1 , 123 , 96 , 106 , 105 , 17 , 2 , 3 , 4 , 5 , 6 , 98 , 7 , 8 , 97 , 9 , 10 , 11 , 12 , 13 , 14 , 15 , 16 , 118 , 119 , 117 , 18 , 19 , 68 , 61 , 112 , 116 , 64 desc, 65 , 101 , 102 , 103 , 104 , 51 , 50 , 49 , 20 , 21 
    endif
    SELECT GENEFATT
    GO TOP
    SCAN FOR NOT EMPTY(NVL(SERIAL," ")) AND NOT EMPTY(NVL(CODCLI," ")) AND NOT DELETED()
    if NOT EMPTY(NVL(MVCODCLA,"")) AND NVL(MVSPEIMB,0)=0 AND NVL(MVSPETRA,0)=0 AND NVL(MVFLFOSC," ")<>"S" AND NVL(MVFLSCAF," ")<>"S"
      * --- Verifica se riga da non importare
      if MVCODCLA $ this.oParentObject.w_ESCL1+"|"+this.oParentObject.w_ESCL2+"|"+this.oParentObject.w_ESCL3+"|"+this.oParentObject.w_ESCL4+"|"+this.oParentObject.w_ESCL5
        LOOP
      endif
    endif
    if NVL(ANTIPFAT," ")$"CE" AND NVL(MVTIPRIG, " ")="D" AND EMPTY(NVL(MVSERRIF, SPACE(10))) AND !NVL(TDORDAPE, " ")="S"
      * --- Verifica se riga da non importare.
      *     
      *     Se la riga descrittiva non � attribuibile a nessun ordine (MVSERRIF vuoto) 
      *     non posso importarla all'interno della fattura, sar� cmq evasa
      *     in pag1
      this.w_FLEVAORD = .T.
      Insert Into EvaDesc (SERIAL, ROWNUM, NUMRIF ) Values ( Genefatt.SERIAL , Genefatt.CPROWNUM , -20 )
      LOOP
    endif
    SELECT GENEFATT
    * --- Se non � la prima riga esaminata
    if Not this.w_FIRSTROW And this.w_OBRK <> Nvl(GENEFATT.SERIAL,"@@@")
      this.w_ROTT_T = "S"
      this.w_ROTT_I = "S"
      this.w_ROTT_B = "S"
      * --- Controllo rottura Iva spese Trasporto
      do case
        case NVL(IVATRA, SPACE(5)) = this.w_O_IVATRA 
          this.w_ROTT_T = "N"
        case NVL(IVATRA, SPACE(5)) <> this.w_O_IVATRA And (Empty(this.w_O_SPETRA) Or Empty(Nvl(MVSPETRA,0)) )
          this.w_ROTT_T = "N"
      endcase
      * --- Controllo Rottura Iva spese Bolli
      do case
        case NVL(IVABOL, SPACE(5)) = this.w_O_IVABOL
          this.w_ROTT_B = "N"
        case NVL(IVABOL, SPACE(5)) <> this.w_O_IVABOL And (Empty(this.w_O_SPEBOL) Or Empty(Nvl(MVSPEBOL,0)) )
          this.w_ROTT_B = "N"
      endcase
      * --- Controllo Rottura Iva Spese Imballo
      do case
        case NVL(IVAIMB, SPACE(5)) = this.w_O_IVAIMB
          this.w_ROTT_I = "N"
        case NVL(IVAIMB, SPACE(5)) <> this.w_O_IVAIMB And (Empty(this.w_O_SPEIMB) Or Empty(Nvl(MVSPEIMB,0)) )
          this.w_ROTT_I = "N"
      endcase
      * --- Nel caso devo fare rottura riassegno un nuovo valore
      if this.w_ROTT_T = "S" Or this.w_ROTT_I = "S" Or this.w_ROTT_B = "S"
        this.w_ROTTURA = Sys(2015)
      endif
    endif
    * --- Ordinamento:
    *     Intestatario DDT/ Per Conto Di
    *     Flag ordine aperto
    *     Data (+)
    *     Alfa (+)
    *     Numero (+)
    *     TDFLAPCA = flag applica contributi accessori
    *     FLSERI, vale MVSERIAL se il documento non pu� essere spezzato, MVSERRIF se fatturazione per Ordine, MVSERIAL se ordine aperto altrimenti 'XXXXXXXXXX'
    *     Codice IVA in testata 
    *     Agente 
    *     Capo Area 
    *     Pagamento 
    *     Banca 
    *     Numero Conto Corrente 
    *     Ns. Banca 
    *     Valuta 
    *     Cambio (#)
    *     Primo Sconto di piede 
    *     Secondo Sconto di piede 
    *     Sconto Pagamento 
    *     Flag Scorporo 
    *     Livello di raggruppamento righe (@)
    *     Sede di destinazione, se cliente con fatturazione per sede o sede+Ordine, altrimenti valorizzato con 'DDDDD'
    *     Condizione di Consegna - Se fattura INTRA
    *     Tipo di Spedizione  - Se fattura INTRA
    *     Test Cauzione Imballi ('N' se no cauzioni, 'S' se cauzioni<>0) (�)
    *     Flag ripartizione imballo / trasporti 
    *     Dichiarazione d'intento 
    *     Calcola sconto su omaggi 
    *     Codice Iva per Cauzioni 
    *     Forza Sconti 
    *     Flag beni deperibili 
    *     Codice IVA incassi 
    *     Codice IVA trasporti 
    *     Codice IVA imballi 
    *     Codice IVA bolli 
    *     Data (!)
    *     Alfa (!)
    *     Numero (!)
    *     MVSERIAL (&)
    *     CPROWORD (&)
    *     Dati aggiuntivi:
    *     a) precedono tutti gli altri campi dell'ordinamento
    *     b) succedono tutti gli altri campi tranne Data Alfa Numero Mvserial Cproword
    *     c) non sono considerati nell'ordinamento a sec. delle impostazioni negli archivi aziendali
    *     
    *     (+) Valorizzati se intestatario non ha fattura per singolo DDT
    *     (&) Campi fuori dalla rottura
    *     (!) Al termine di tutti gli ordinamenti ri ordino per data alfa e numero, per gestire i casi in cui ho DDT con le stesse caratteristiche da raggruppare
    *     (�)I DDT CON CAUZIONI DA IMBALLO POSSONO ESSERE RAGGRUPPATI SOLO SE HANNO UN IMPORTO<>0 ALTRIMENTI NO!
    *     SE HANNO UN IMPORTO<>0 NON VANNO SPEZZATI...
    *     (#) Se attivo il check in azienda cambio di origine
    *     (@) Tre campi che contengono ognuno o il valore di riga o il valore di testata a seconda se 
    *     rispettivamente le spese imballo, incasso o gli sconti di piede sono diversi da zero. 
    *     In pratica se ho delle spese il documento non � spezzato ma eventualmente pu� essere raggruppato 
    *     con un altro documento/ righe di altro documento con medesimo livello di raggruppamento (da verificare meglio)
    *     
    this.w_NEWDES = IIF(NVL(GENEFATT.ANTIPFAT, "N")="D" OR NVL(GENEFATT.ANTIPFAT, "N")="E",NVL(GENEFATT.MVCODCON, SPACE(15)),NVL(GENEFATT.CODCLI, SPACE(15)))+NVL(GENEFATT.TDORDAPE, " ")
    if g_COAC="S"
      * --- Appplicazione contributi accessori fa rottura
      this.w_NEWDES = this.w_NEWDES + NVL(GENEFATT.TDFLAPCA ,SPACE(1))
    endif
    this.w_NEWDES = this.w_NEWDES + NVL(GENEFATT.FLSERI,"XXXXXXXXXX") +NVL(GENEFATT.CODIVE,SPACE(5))
    this.w_NEWDES = this.w_NEWDES+NVL(GENEFATT.CODAGE,SPACE(5))+NVL(GENEFATT.CODAG2,SPACE(5))+NVL(GENEFATT.CODPAG,SPACE(5))
    this.w_NEWDES = this.w_NEWDES+NVL(GENEFATT.CODBAN,SPACE(10))+NVL(GENEFATT.NUMCOR,SPACE(25))+NVL(GENEFATT.CODBA2,SPACE(15))
    this.w_NEWDES = this.w_NEWDES+NVL(GENEFATT.CODVAL,SPACE(3))+IIF(this.oParentObject.w_FLCAMB="S",Str(Nvl(GENEFATT.CAOORD,0),12,7),"")
    this.w_NEWDES = this.w_NEWDES+STR(NVL(GENEFATT.SCOCL1,0),6,2)+STR(NVL(GENEFATT.SCOCL2,0),6,2)+STR(NVL(GENEFATT.SCOPAG,0),6,2)
    this.w_NEWDES = this.w_NEWDES+NVL(GENEFATT.FLSCOR,"N")+NVL(GENEFATT.FLRAGG," ")+NVL(GENEFATT.FLRAG1," ")+NVL(GENEFATT.FLRAG2," ")+NVL(GENEFATT.FLDDES,"DDDDD")
    * --- Se Attivo modulo intra e contabilit� generale, e genero una fattura di tipo intra
    *     e nei dati azienda � impostata l'obbligatoriet� su cessioni o Acquisti e cessioni
    *     devo considerare come campi di rottura la Condizione di consegna e il tipo Spedizione
    this.w_NEWDES = this.w_NEWDES+Nvl(GENEFATT.CONORD," ") + Nvl(GENEFATT.SPEORD,(Space(3)))
    this.w_NEWDES = this.w_NEWDES+Nvl(GENEFATT.TESTCAU,"N")+NVL(GENEFATT.FLRIMB," ")+NVL(GENEFATT.FLTRAS," ")+NVL(GENEFATT.MVFLRINC," ")
    this.w_NEWDES = this.w_NEWDES+this.w_ROTTURA + NVL(GENEFATT.MVRIFDIC, SPACE(15)) + NVL(GENEFATT.MVFLSCOM, "N") + NVL(GENEFATT.MVIVACAU,Space(5))
    this.w_NEWDES = this.w_NEWDES+NVL(GENEFATT.MVFLFOSC, " ")+IIF(this.oParentObject.w_PSFLVEBD="D", NVL(GENEFATT.MVFLVABD, " "), this.oParentObject.w_PSFLVEBD)
    this.w_NEWDES = this.w_NEWDES+NVL(GENEFATT.IVAINC, Space(5))+NVL(GENEFATT.IVATRA, Space(5))+NVL(GENEFATT.IVAIMB, Space(5))+NVL(GENEFATT.IVABOL, Space(5))
    this.w_NEWDES = this.w_NEWDES+NVL(GENEFATT.IVAINC, Space(5))+NVL(GENEFATT.IVATRA, Space(5))+NVL(GENEFATT.IVAIMB, Space(5))+NVL(GENEFATT.IVABOL, Space(5))
    if this.oParentObject.w_TDFLIA01<>"S" OR this.oParentObject.w_TDFLRA01="N" OR (this.oParentObject.w_TDFLRA01="S" AND (this.w_OLDAGG1=nvl(GENEFATT.MVAGG_01,"") OR ( EMPTY(this.w_OLDAGG1) and (empty(this.w_FULLAGG_01) or this.w_FULLAGG_01= nvl(GENEFATT.MVAGG_01,"")) ) OR EMPTY(nvl(GENEFATT.MVAGG_01,"")) ) ) OR (this.oParentObject.w_TDFLRA01="A" AND this.w_OLDAGG1=nvl(GENEFATT.MVAGG_01,""))
      * --- rottagg1 =' N' No rottura uguaglianza o blank se rottura ma non tassativa
      this.w_ROTTAGG1 = this.w_OLDROTTAGG1
    else
      this.w_ROTTAGG1 = 1-this.w_OLDROTTAGG1
    endif
    if this.oParentObject.w_TDFLIA02<>"S" OR this.oParentObject.w_TDFLRA02="N" OR (this.oParentObject.w_TDFLRA02="S" AND (this.w_OLDAGG2=nvl(GENEFATT.MVAGG_02,"") OR ( EMPTY(this.w_OLDAGG2) and (empty(this.w_FULLAGG_02) or this.w_FULLAGG_02= nvl(GENEFATT.MVAGG_02,"")) ) OR EMPTY(nvl(GENEFATT.MVAGG_02,"")) ) ) OR (this.oParentObject.w_TDFLRA02="A" AND this.w_OLDAGG2=nvl(GENEFATT.MVAGG_02,""))
      * --- rottagg2 =' N' No rottura uguaglianza o blank se rottura ma non tassativa
      this.w_ROTTAGG2 = this.w_OLDROTTAGG2
    else
      this.w_ROTTAGG2 = 1-this.w_OLDROTTAGG2
    endif
    if this.oParentObject.w_TDFLIA03<>"S" OR this.oParentObject.w_TDFLRA03="N" OR (this.oParentObject.w_TDFLRA03="S" AND (this.w_OLDAGG3=nvl(GENEFATT.MVAGG_03,"") OR ( EMPTY(this.w_OLDAGG3) and (empty(this.w_FULLAGG_03) or this.w_FULLAGG_03= nvl(GENEFATT.MVAGG_03,"")) ) OR EMPTY(nvl(GENEFATT.MVAGG_03,"")) ) ) OR (this.oParentObject.w_TDFLRA03="A" AND this.w_OLDAGG3=nvl(GENEFATT.MVAGG_03,""))
      * --- rottagg3 =' N' No rottura uguaglianza o blank se rottura ma non tassativa
      this.w_ROTTAGG3 = this.w_OLDROTTAGG3
    else
      this.w_ROTTAGG3 = 1-this.w_OLDROTTAGG3
    endif
    if this.oParentObject.w_TDFLIA04<>"S" OR this.oParentObject.w_TDFLRA04="N" OR (this.oParentObject.w_TDFLRA04="S" AND (this.w_OLDAGG4=nvl(GENEFATT.MVAGG_04,"") OR ( EMPTY(this.w_OLDAGG4) and (empty(this.w_FULLAGG_04) or this.w_FULLAGG_04= nvl(GENEFATT.MVAGG_04,"")) ) OR EMPTY(nvl(GENEFATT.MVAGG_04,"")) ) ) OR (this.oParentObject.w_TDFLRA04="A" AND this.w_OLDAGG4=nvl(GENEFATT.MVAGG_04,""))
      * --- rottagg3 =' N' No rottura uguaglianza o blank se rottura ma non tassativa
      this.w_ROTTAGG4 = this.w_OLDROTTAGG4
    else
      this.w_ROTTAGG4 = 1-this.w_OLDROTTAGG4
    endif
    if this.oParentObject.w_TDFLIA05<>"S" OR this.oParentObject.w_TDFLRA05="N" OR (this.oParentObject.w_TDFLRA05="S" AND (this.w_OLDAGG5=nvl(GENEFATT.MVAGG_05,{}) OR ( EMPTY(this.w_OLDAGG5) and (empty(this.w_FULLAGG_05) or this.w_FULLAGG_05= nvl(GENEFATT.MVAGG_05,{})) ) OR EMPTY(nvl(GENEFATT.MVAGG_05,{})) ) ) OR (this.oParentObject.w_TDFLRA05="A" AND this.w_OLDAGG5=nvl(GENEFATT.MVAGG_05,{}))
      * --- rottagg3 =' N' No rottura uguaglianza o blank se rottura ma non tassativa
      this.w_ROTTAGG5 = this.w_OLDROTTAGG5
    else
      this.w_ROTTAGG5 = 1-this.w_OLDROTTAGG5
    endif
    if this.oParentObject.w_TDFLIA06<>"S" OR this.oParentObject.w_TDFLRA06="N" OR (this.oParentObject.w_TDFLRA06="S" AND (this.w_OLDAGG6=nvl(GENEFATT.MVAGG_06,{}) OR ( EMPTY(this.w_OLDAGG6) and (empty(this.w_FULLAGG_06) or this.w_FULLAGG_06= nvl(GENEFATT.MVAGG_06,{})) ) OR EMPTY(nvl(GENEFATT.MVAGG_06,{})) ) ) OR (this.oParentObject.w_TDFLRA06="A" AND this.w_OLDAGG6=nvl(GENEFATT.MVAGG_06,{}))
      * --- rottagg3 =' N' No rottura uguaglianza o blank se rottura ma non tassativa
      this.w_ROTTAGG6 = this.w_OLDROTTAGG6
    else
      this.w_ROTTAGG6 = 1-this.w_OLDROTTAGG6
    endif
    this.w_NEWDES = this.w_NEWDES+STR(this.w_ROTTAGG1) +STR(this.w_ROTTAGG2) +STR(this.w_ROTTAGG3) +STR(this.w_ROTTAGG4) +STR(this.w_ROTTAGG5) +STR(this.w_ROTTAGG6)
    * --- Evita possibili situazioni Case Sensitive
    this.w_NEWDES = UPPER(this.w_NEWDES)
    * --- Testa Campo di Rottura
    if this.w_DBRK<>this.w_NEWDES
      this.w_CALRATE = .F.
      this.w_FULLAGG_01 = ""
      this.w_FULLAGG_02 = ""
      this.w_FULLAGG_03 = ""
      this.w_FULLAGG_04 = ""
      this.w_FULLAGG_05 = cp_chartodate("  -  -  ")
      this.w_FULLAGG_06 = cp_chartodate("  -  -  ")
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      SELECT GENEFATT
      this.w_WARNPAG = .F.
      * --- Inizializza i dati di Testata del Nuovo Documento
      this.w_MVCODCON = NVL(CODCLI, SPACE(15))
      this.w_MVCODAGE = NVL(CODAGE, SPACE(5))
      this.w_MVCODAG2 = NVL(CODAG2, SPACE(5))
      this.w_MVCODPAG = NVL(CODPAG, SPACE(5))
      this.w_ANTIPFAT = NVL(ANTIPFAT," ")
      this.w_INIPAG = .T.
      this.w_FIRSTROW = .F.
      this.w_MVCODBAN = NVL(CODBAN, SPACE(10))
      this.w_MVNUMCOR = NVL(NUMCOR, SPACE(25))
      this.w_MVCODBA2 = NVL(CODBA2, SPACE(15))
      this.w_MVCODVAL = NVL(CODVAL, SPACE(5))
      this.w_MVSCOCL1 = NVL(SCOCL1,0)
      this.w_MVSCOCL2 = NVL(SCOCL2,0)
      this.w_MVFLFOSC = NVL(MVFLFOSC, SPACE(1))
      this.w_MVFLVABD = IIF(this.oParentObject.w_PSFLVEBD="D", NVL(MVFLVABD, SPACE(1)), this.oParentObject.w_PSFLVEBD)
      this.w_MVFLSCAF = NVL(MVFLSCAF, SPACE(1))
      this.w_MVFLSCOR = NVL(FLSCOR, "N")
      this.w_MVFLSCOM = Nvl(MVFLSCOM, "N")
      this.w_MVSERRIF = NVL(SERIAL, SPACE(10))
      this.w_MVTCONTR = NVL(MVTCONTR, SPACE(15))
      this.w_MVTCOLIS = NVL(MVTCOLIS, SPACE(5))
      this.w_MVCAOVAL = this.w_CAONAZ
      * --- w_CAOVAL=Cambio documento di Origine
      this.w_CAOVAL = Nvl(CAOVAL,0)
      this.w_MVCODDES = SPACE(5)
      this.w_MVTIPORN = this.oParentObject.w_MVTIPCON
      this.w_MVCODORN = SPACE(15)
      this.w_MVASPEST = SPACE(30)
      this.w_MVDESDOC = SPACE(50)
      if not empty(NVL(MVAGG_01,"") ) and this.w_ROTTAGG1=1
        * --- Primo campo valorizzato
        this.w_FULLAGG_01 = NVL(MVAGG_01,"")
      endif
      if not empty(NVL(MVAGG_02,"") ) and this.w_ROTTAGG2=1
        * --- Primo campo valorizzato
        this.w_FULLAGG_02 = NVL(MVAGG_02,"")
      endif
      if not empty(NVL(MVAGG_03,"") ) and this.w_ROTTAGG3=1
        * --- Primo campo valorizzato
        this.w_FULLAGG_03 = NVL(MVAGG_03,"")
      endif
      if not empty(NVL(MVAGG_04,"") ) and this.w_ROTTAGG4=1
        * --- Primo campo valorizzato
        this.w_FULLAGG_04 = NVL(MVAGG_04,"")
      endif
      if not empty(NVL(MVAGG_05,cp_chartodate("  -  -  ")) ) and this.w_ROTTAGG5=1
        * --- Primo campo valorizzato
        this.w_FULLAGG_05 = NVL(MVAGG_05,cp_chartodate("  -  -  "))
      endif
      if not empty(NVL(MVAGG_06,cp_chartodate("  -  -  ")) ) and this.w_ROTTAGG6=1
        * --- Primo campo valorizzato
        this.w_FULLAGG_06 = NVL(MVAGG_06,cp_chartodate("  -  -  "))
      endif
      this.w_MVAGG_01 = NVL(MVAGG_01,"")
      this.w_MVAGG_02 = NVL(MVAGG_02,"")
      this.w_MVAGG_03 = NVL(MVAGG_03,"")
      this.w_MVAGG_04 = NVL(MVAGG_04,"")
      this.w_MVAGG_05 = NVL(MVAGG_05,cp_chartodate("  -  -  "))
      this.w_MVAGG_06 = NVL(MVAGG_06,cp_chartodate("  -  -  "))
      * --- In ogni caso se la fattura differita genera gli elenchi intra devo riportare i dati obbligatori
      if NVL(this.w_ANTIPFAT," ")="S" Or this.w_COPYSPED="S"
        this.w_MVCONCON = NVL(MVCONCON, " ")
        this.w_MVCODSPE = NVL(MVCODSPE, SPACE(3))
      else
        this.w_MVCONCON = " "
        this.w_MVCODSPE = SPACE(3)
      endif
      if NVL(this.w_ANTIPFAT," ")="S"
        this.w_MVCODPOR = NVL(MVCODPOR, " ")
        this.w_MVCODVET = NVL(MVCODVET, SPACE(5))
        this.w_MVCODVE2 = NVL(MVCODVE2, SPACE(5))
        this.w_MVCODVE3 = NVL(MVCODVE3, SPACE(5))
        this.w_MVCODDES = NVL(MVCODDES, SPACE(5))
        this.w_MVDESDOC = NVL(MVDESDOC,SPACE(50))
      else
        this.w_MVCODPOR = " "
        this.w_MVCODVET = SPACE(5)
        this.w_MVCODVE2 = SPACE(5)
        this.w_MVCODVE3 = SPACE(5)
        this.w_MVCODDES = IIF(NVL(FLDDES, SPACE(5))="DDDDD", SPACE(5), NVL(FLDDES, SPACE(5)))
      endif
      if CONORN = "B"
        * --- ConOrn pu� essere ='A' o ='B'
        *     A = Fatturazione su intestatario
        *     B = Fatturazione su Per Conto di
        *     
        *     Nel caso B il codice destinazione deve essere svuotato poich� quella presente 
        *     nel documento di origine � la destinazione dell'intestatario e non del per conto di
        this.w_MVCODDES = SPACE(5)
      endif
      * --- Pagamenti
      this.w_MVDATDIV = this.oParentObject.w_DATDIV
      if this.w_MVFLSCAF="S" OR EMPTY(this.oParentObject.w_PSPAGFOR)
        if this.w_MVFLSCAF="S"
          this.w_MVDATDIV = EVL(NVL(MVDATDIV, " "), this.w_MVDATDIV )
        endif
        this.w_MVCODPAG = NVL(CODPAG, SPACE(5))
        this.w_MVSCOPAG = NVL(SCOPAG,0)
        this.w_CAMBIAPAG = .F.
      else
        this.w_CAMBIAPAG = .T.
        this.w_OLDCODPAG = this.w_MVCODPAG
        this.w_OLDSCOPAG = NVL(SCOPAG,0)
        this.w_ANPAGFOR = NVL(ANPAGFOR, SPACE(5) )
        this.w_MVCODPAG = IIF(NOT EMPTY(NVL(this.w_ANPAGFOR,SPACE(5))),this.w_ANPAGFOR, this.oParentObject.w_PSPAGFOR)
        * --- Read from PAG_AMEN
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAG_AMEN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAG_AMEN_idx,2],.t.,this.PAG_AMEN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PASCONTO"+;
            " from "+i_cTable+" PAG_AMEN where ";
                +"PACODICE = "+cp_ToStrODBC(this.w_MVCODPAG);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PASCONTO;
            from (i_cTable) where;
                PACODICE = this.w_MVCODPAG;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PASCONTO = NVL(cp_ToDate(_read_.PASCONTO),cp_NullValue(_read_.PASCONTO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_MVSCOPAG = this.w_PASCONTO
      endif
      this.w_MVTINCOM = {}
      this.w_MVTFICOM = {}
      this.w_MVCODIVE = NVL(CODIVE, SPACE(5))
      * --- Variabile letta per GSAR_BFA
      if NOT EMPTY(this.w_MVCODIVE)
        * --- Read from VOCIIVA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VOCIIVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "IVBOLIVA,IVPERIVA,IVPERIND"+;
            " from "+i_cTable+" VOCIIVA where ";
                +"IVCODIVA = "+cp_ToStrODBC(this.w_MVCODIVE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            IVBOLIVA,IVPERIVA,IVPERIND;
            from (i_cTable) where;
                IVCODIVA = this.w_MVCODIVE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_BOLIVE = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
          this.w_PERIVE = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
          this.w_INDIVE = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      else
        this.w_PERIVE = 0
        this.w_INDIVE = 0
        this.w_BOLIVE = " "
      endif
      this.w_MVRIFDIC = NVL(MVRIFDIC, SPACE(10))
      if this.w_MVCODVAL<>this.w_MVVALNAZ
        * --- Codice valuta diversa dalla valuta di conto
        if this.oParentObject.w_FLCAMB="S"
          * --- Nei dati Aziend � attivo il Check: Cambio di Origine
          *     Verr� utilizzato quindi il cambio del documento di Origine 
          *     che  in questo caso � anche campo di rottura
          this.w_MVCAOVAL = this.w_CAOVAL
        else
          * --- Cambio del Giorno
          this.w_MVCAOVAL = GETCAM(this.w_MVCODVAL, this.w_MVDATDOC, 0)
        endif
      endif
      * --- Legge Dati Associati alla Valuta
      this.w_DECTOT = 0
      this.w_DECUNI = 0
      this.w_BOLESE = 0
      this.w_BOLSUP = 0
      this.w_BOLCAM = 0
      this.w_BOLARR = 0
      this.w_BOLMIN = 0
      * --- Read from VALUTE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VALUTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "VADECTOT,VABOLESE,VABOLSUP,VABOLCAM,VABOLARR,VABOLMIM,VADECUNI"+;
          " from "+i_cTable+" VALUTE where ";
              +"VACODVAL = "+cp_ToStrODBC(this.w_MVCODVAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          VADECTOT,VABOLESE,VABOLSUP,VABOLCAM,VABOLARR,VABOLMIM,VADECUNI;
          from (i_cTable) where;
              VACODVAL = this.w_MVCODVAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
        this.w_BOLESE = NVL(cp_ToDate(_read_.VABOLESE),cp_NullValue(_read_.VABOLESE))
        this.w_BOLSUP = NVL(cp_ToDate(_read_.VABOLSUP),cp_NullValue(_read_.VABOLSUP))
        this.w_BOLCAM = NVL(cp_ToDate(_read_.VABOLCAM),cp_NullValue(_read_.VABOLCAM))
        this.w_BOLARR = NVL(cp_ToDate(_read_.VABOLARR),cp_NullValue(_read_.VABOLARR))
        this.w_BOLMIN = NVL(cp_ToDate(_read_.VABOLMIM),cp_NullValue(_read_.VABOLMIM))
        this.w_DECUNI = NVL(cp_ToDate(_read_.VADECUNI),cp_NullValue(_read_.VADECUNI))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Gestione contributi accessori
      *     (gia gestito a livello di query ribadito per tracciabilit� intervento)
      if g_COAC="S"
        this.w_OFLAPCA = NVL(GENEFATT.TDFLAPCA ,SPACE(1))
      else
        this.w_OFLAPCA = "N"
      endif
    endif
    * --- Cambia Documento di Origine o Destinazione
    if this.w_OBRK<>SERIAL OR this.w_DBRK<>this.w_NEWDES
      if this.w_OBRK<>NVL(SERIAL, SPACE(10))
        * --- Se Nuovo Documento di Origine Riporta sul Tmp di Appoggio le Spese...
        this.w_MVSERRIF = NVL(SERIAL, SPACE(10))
        if NOT EMPTY(this.w_MVSERRIF)
          * --- memorizza l'ultimo seriale del doc origine senza considerare la riga del contributo accessorio che non ha Mvserrif valorizzato
          this.w_MVSERNOCA = NVL(SERIAL, SPACE(10))
        endif
        this.w_IVAINC = NVL(IVAINC, SPACE(5) )
        this.w_IVAIMB = NVL(IVAIMB, SPACE(5) )
        this.w_IVATRA = NVL(IVATRA, SPACE(5) )
        this.w_SPEINC = 0
        this.w_SPEIMB = 0
        this.w_SPETRA = 0
        this.w_SPEBOL = 0
        this.w_IVABOL = SPACE(5)
        this.w_ACCONT = 0
        this.w_VALINC = SPACE(3)
        this.w_VALIN2 = SPACE(3)
        this.w_SPEIN2 = 0
        this.w_CLADOC = SPACE(2)
        this.w_IMPARR = 0
        this.w_ACCPRE = 0
        this.w_MVSCONTI = this.w_MVSCONTI + IIF(this.w_MVFLFOSC="S", MVSCONTI, 0)
        this.w_DESRIF = SPACE(18)
        this.w_RIFCLI = SPACE(5)
        this.w_NRIF = 0
        this.w_ARIF = Space(10)
        this.w_DRIF = cp_CharToDate("  -  -  ")
        this.w_CAUIMB = 0
        this.w_IVACAU = SPACE(5)
        this.w_FLFOCA = " "
        this.w_TIPOPEDIC = ""
        * --- Read from DOC_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MV__NOTE,MVACCONT,MVACCPRE,MVALFEST,MVCLADOC,MVDATDOC,MVDATEST,MVFLRIMB,MVFLRINC,MVFLRTRA,MVIMPARR,MVIVABOL,MVNUMEST,MVRIFDIC,MVSPEBOL,MVSPEIMB,MVSPEINC,MVSPETRA,MVTIPDOC,MVNUMDOC,MVALFDOC,MVCAUIMB,MVIVACAU,MVFLFOCA"+;
            " from "+i_cTable+" DOC_MAST where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERRIF);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MV__NOTE,MVACCONT,MVACCPRE,MVALFEST,MVCLADOC,MVDATDOC,MVDATEST,MVFLRIMB,MVFLRINC,MVFLRTRA,MVIMPARR,MVIVABOL,MVNUMEST,MVRIFDIC,MVSPEBOL,MVSPEIMB,MVSPEINC,MVSPETRA,MVTIPDOC,MVNUMDOC,MVALFDOC,MVCAUIMB,MVIVACAU,MVFLFOCA;
            from (i_cTable) where;
                MVSERIAL = this.w_MVSERRIF;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_RIFCLI = NVL(cp_ToDate(_read_.MV__NOTE),cp_NullValue(_read_.MV__NOTE))
          this.w_ACCONT = NVL(cp_ToDate(_read_.MVACCONT),cp_NullValue(_read_.MVACCONT))
          this.w_ACCPRE = NVL(cp_ToDate(_read_.MVACCPRE),cp_NullValue(_read_.MVACCPRE))
          this.w_ARIF = NVL(cp_ToDate(_read_.MVALFEST),cp_NullValue(_read_.MVALFEST))
          this.w_CLADOC = NVL(cp_ToDate(_read_.MVCLADOC),cp_NullValue(_read_.MVCLADOC))
          this.w_DDOC = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
          this.w_DRIF = NVL(cp_ToDate(_read_.MVDATEST),cp_NullValue(_read_.MVDATEST))
          this.w_MVFLRIMB = NVL(cp_ToDate(_read_.MVFLRIMB),cp_NullValue(_read_.MVFLRIMB))
          this.w_MVFLRINC = NVL(cp_ToDate(_read_.MVFLRINC),cp_NullValue(_read_.MVFLRINC))
          this.w_MVFLRTRA = NVL(cp_ToDate(_read_.MVFLRTRA),cp_NullValue(_read_.MVFLRTRA))
          this.w_IMPARR = NVL(cp_ToDate(_read_.MVIMPARR),cp_NullValue(_read_.MVIMPARR))
          this.w_IVABOL = NVL(cp_ToDate(_read_.MVIVABOL),cp_NullValue(_read_.MVIVABOL))
          this.w_NRIF = NVL(cp_ToDate(_read_.MVNUMEST),cp_NullValue(_read_.MVNUMEST))
          this.w_RIFDIC = NVL(cp_ToDate(_read_.MVRIFDIC),cp_NullValue(_read_.MVRIFDIC))
          this.w_SPEBOL = NVL(cp_ToDate(_read_.MVSPEBOL),cp_NullValue(_read_.MVSPEBOL))
          this.w_SPEIMB = NVL(cp_ToDate(_read_.MVSPEIMB),cp_NullValue(_read_.MVSPEIMB))
          this.w_SPEINC = NVL(cp_ToDate(_read_.MVSPEINC),cp_NullValue(_read_.MVSPEINC))
          this.w_SPETRA = NVL(cp_ToDate(_read_.MVSPETRA),cp_NullValue(_read_.MVSPETRA))
          this.w_TIPDOC = NVL(cp_ToDate(_read_.MVTIPDOC),cp_NullValue(_read_.MVTIPDOC))
          this.w_NDOC = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
          this.w_ADOC = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
          this.w_CAUIMB = NVL(cp_ToDate(_read_.MVCAUIMB),cp_NullValue(_read_.MVCAUIMB))
          this.w_IVACAU = NVL(cp_ToDate(_read_.MVIVACAU),cp_NullValue(_read_.MVIVACAU))
          this.w_FLFOCA = NVL(cp_ToDate(_read_.MVFLFOCA),cp_NullValue(_read_.MVFLFOCA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from TIP_DOCU
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TDDESRIF"+;
            " from "+i_cTable+" TIP_DOCU where ";
                +"TDTIPDOC = "+cp_ToStrODBC(this.w_TIPDOC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TDDESRIF;
            from (i_cTable) where;
                TDTIPDOC = this.w_TIPDOC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DESRIF = NVL(cp_ToDate(_read_.TDDESRIF),cp_NullValue(_read_.TDDESRIF))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      if this.w_DBRK<>this.w_NEWDES
        if this.w_ANTIPFAT$"CE" 
          if NOT EMPTY(NVL(MVSERRIF, SPACE(10))) AND NOT EMPTY(g_ARTDES)
            this.w_SERORI = NVL(MVSERRIF, SPACE(10))
            * --- Leggo informazioni x creare riga descrittiva..
            this.w_RIFCLI = ""
            this.w_APNRIF = this.w_NRIF
            this.w_APARIF = this.w_ARIF
            this.w_APDRIF = this.w_DRIF
            * --- Read from DOC_MAST
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.DOC_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "MVNUMDOC,MVALFDOC,MVDATDOC,MVTIPDOC,MVNUMEST,MVALFEST,MVDATEST,MV__NOTE,MVCLADOC"+;
                " from "+i_cTable+" DOC_MAST where ";
                    +"MVSERIAL = "+cp_ToStrODBC(this.w_SERORI);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                MVNUMDOC,MVALFDOC,MVDATDOC,MVTIPDOC,MVNUMEST,MVALFEST,MVDATEST,MV__NOTE,MVCLADOC;
                from (i_cTable) where;
                    MVSERIAL = this.w_SERORI;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_ORNUMDOC = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
              this.w_ORALFDOC = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
              this.w_ORDATDOC = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
              this.w_ORTIPDOC = NVL(cp_ToDate(_read_.MVTIPDOC),cp_NullValue(_read_.MVTIPDOC))
              this.w_NRIF = NVL(cp_ToDate(_read_.MVNUMEST),cp_NullValue(_read_.MVNUMEST))
              this.w_ARIF = NVL(cp_ToDate(_read_.MVALFEST),cp_NullValue(_read_.MVALFEST))
              this.w_DRIF = NVL(cp_ToDate(_read_.MVDATEST),cp_NullValue(_read_.MVDATEST))
              this.w_RIFCLI = NVL(cp_ToDate(_read_.MV__NOTE),cp_NullValue(_read_.MV__NOTE))
              this.w_ORCLADOC = NVL(cp_ToDate(_read_.MVCLADOC),cp_NullValue(_read_.MVCLADOC))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Read from TIP_DOCU
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "TDDESRIF,TDMODRIF"+;
                " from "+i_cTable+" TIP_DOCU where ";
                    +"TDTIPDOC = "+cp_ToStrODBC(this.w_ORTIPDOC);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                TDDESRIF,TDMODRIF;
                from (i_cTable) where;
                    TDTIPDOC = this.w_ORTIPDOC;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_ORDESRIF = NVL(cp_ToDate(_read_.TDDESRIF),cp_NullValue(_read_.TDDESRIF))
              this.w_MODRIF = NVL(cp_ToDate(_read_.TDMODRIF),cp_NullValue(_read_.TDMODRIF))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_APNSRI = this.oParentObject.w_FLNSRI
            this.w_APVSRI = this.oParentObject.w_FLVSRI
            this.w_APRIDE = this.oParentObject.w_FLRIDE
            if this.w_ORCLADOC<>"OR"
              * --- legge i check riferimenti dalla causale ddt solo se non sono ordini
              * --- Read from TIP_DOCU
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "TDFLNSRI,TDFLVSRI,TDFLRIDE"+;
                  " from "+i_cTable+" TIP_DOCU where ";
                      +"TDTIPDOC = "+cp_ToStrODBC(this.w_TIPDOC);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  TDFLNSRI,TDFLVSRI,TDFLRIDE;
                  from (i_cTable) where;
                      TDTIPDOC = this.w_TIPDOC;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.oParentObject.w_FLNSRI = NVL(cp_ToDate(_read_.TDFLNSRI),cp_NullValue(_read_.TDFLNSRI))
                this.oParentObject.w_FLVSRI = NVL(cp_ToDate(_read_.TDFLVSRI),cp_NullValue(_read_.TDFLVSRI))
                this.oParentObject.w_FLRIDE = NVL(cp_ToDate(_read_.TDFLRIDE),cp_NullValue(_read_.TDFLRIDE))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            this.Page_11()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_RIFCLI = ""
            this.w_NRIF = this.w_APNRIF
            this.w_ARIF = this.w_APARIF
            this.w_DRIF = this.w_APDRIF
            this.oParentObject.w_FLVSRI = this.w_APVSRI
            this.oParentObject.w_FLNSRI = this.w_APNSRI
            this.oParentObject.w_FLRIDE = this.w_APRIDE
            Select GeneFatt
          endif
          if this.w_OBRK=NVL(SERIAL, SPACE(10))
            * --- Il documento importato ha al suo interno 2 o pi� ordini
            this.w_MVIVABOL = nvl(IVABOL,SPACE(5))
          endif
        endif
        this.w_RIFMES = SPACE(100)
      endif
      if this.w_OBRK<>NVL(SERIAL, SPACE(10))
        * --- lettere d'intento relative a ddt
        if empty(this.w_RIFDIC) and this.w_CLADOC="DT"
          this.w_OK_LETD = CAL_LETT(this.w_DDOC,this.oParentObject.w_MVTIPCON,this.w_MVCODCON, @ArrDic, SPACE(10), this.w_MVCODDES)
          if this.w_OK_LETD and empty(this.w_RIFDIC)
            * --- Alla data del documento di Origine c'era una lettera di intento valida ma non era stata caricata
            *     Probabilmente � stata caricata successivamente al caricamento del documento
            this.w_oERRORLOG.AddMsgLogPartNoTrans(space(5), "%0Warning:%0")     
            this.w_oERRORLOG.AddMsgLog("Alla data del documento %1 n. %2 del %3 esiste una lettera di intento valida%0Controllare se � stata inserita successivamente o se � stata eliminata manualmente dallo stesso%0", this.w_TIPDOC, Alltrim(STR(this.w_NDOC,15))+ IIF(Not Empty(this.w_ADOC),"/"+Alltrim(this.w_ADOC),"") , DTOC(this.w_DDOC))     
            this.w_NWARN = this.w_NWARN + 1
          endif
        endif
        if not empty(this.w_MVRIFDIC)
          * --- Read from DIC_INTE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DIC_INTE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2],.t.,this.DIC_INTE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "DITIPOPE"+;
              " from "+i_cTable+" DIC_INTE where ";
                  +"DISERIAL = "+cp_ToStrODBC(this.w_MVRIFDIC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              DITIPOPE;
              from (i_cTable) where;
                  DISERIAL = this.w_MVRIFDIC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_TIPOPEDIC = NVL(cp_ToDate(_read_.DITIPOPE),cp_NullValue(_read_.DITIPOPE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if NOT EMPTY(this.w_MVCODIVE)
          * --- Read from VOCIIVA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VOCIIVA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "IVBOLIVA,IVPERIVA,IVPERIND"+;
              " from "+i_cTable+" VOCIIVA where ";
                  +"IVCODIVA = "+cp_ToStrODBC(this.w_MVCODIVE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              IVBOLIVA,IVPERIVA,IVPERIND;
              from (i_cTable) where;
                  IVCODIVA = this.w_MVCODIVE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_BOLIVE = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
            this.w_PERIVE = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
            this.w_INDIVE = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        else
          this.w_PERIVE = 0
          this.w_BOLIVE = " "
          this.w_INDIVE = 0
        endif
        if this.w_MVFLRINC="S"
          this.w_MVSPEINC = this.w_MVSPEINC + this.w_SPEINC
          if !(this.w_MVFLSCAF="S" OR EMPTY(this.oParentObject.w_PSPAGFOR) )
            this.w_OLDSPEINC = this.w_MVSPEINC
            this.w_MVSPEINC = CALSPEINC( "V", this.w_MVCODPAG, this.w_MVCODVAL, this.oParentObject.w_MVTIPCON, this.w_MVCODCON, this.oParentObject.w_FLSPIN )
          endif
        else
          if this.w_INIPAG
            * --- Ricalcola le Spese di Incasso
            this.w_MVSPEINC = CALSPEINC( "V", this.w_MVCODPAG, this.w_MVCODVAL, this.oParentObject.w_MVTIPCON, this.w_MVCODCON, this.oParentObject.w_FLSPIN )
            if this.w_CAMBIAPAG
              * --- simulo il ricalcolo con il vecchio pagamento
              this.w_OLDSPEINC = CALSPEINC( "V", this.w_OLDCODPAG, this.w_MVCODVAL, this.oParentObject.w_MVTIPCON, this.w_MVCODCON, this.oParentObject.w_FLSPIN )
            endif
          endif
        endif
        if !(this.w_MVFLSCAF="S" OR EMPTY(this.oParentObject.w_PSPAGFOR) )
          * --- verifica se mutano le condizioni di incasso e sconto pagamento con il nuovo pagamento
          if NVL(this.w_OLDSCOPAG,0)<>NVL(this.w_MVSCOPAG, 0) OR ( NVL(this.w_MVSPEINC,0) <> NVL(this.w_OLDSPEINC, 0) ) 
            this.w_WARNPAG = .T.
          endif
        endif
        this.w_INIPAG = .F.
        this.w_MVSPEIMB = this.w_MVSPEIMB + this.w_SPEIMB
        this.w_MVSPETRA = this.w_MVSPETRA + this.w_SPETRA
        this.w_MVSPEBOL = this.w_MVSPEBOL + this.w_SPEBOL
        this.w_MVIMPARR = this.w_MVIMPARR + this.w_IMPARR
        this.w_MVCAUIMB = this.w_MVCAUIMB + this.w_CAUIMB
        * --- Memorizzo le spese del documento per controllare al successivo se devo fre rottura
        this.w_O_SPETRA = this.w_MVSPETRA
        this.w_O_SPEIMB = this.w_MVSPEIMB
        this.w_O_SPEBOL = this.w_MVSPEBOL
        * --- L' Acconto Precedente non non si accumula dagli Ordini
        this.w_MVACCPRE = this.w_MVACCPRE + IIF(this.w_CLADOC="OR", 0, (this.w_ACCONT+this.w_ACCPRE))
        * --- Nel caso in cui ho raggruppato i documenti e non ho codice iva o spese in quello che sto esaminando
        *     devo riportare il codice Iva del Documento precedente il quale probabilmente conteneva anche le spese
        this.w_MVIVAINC = IIF(EMPTY(this.w_IVAINC), this.w_MVIVAINC, this.w_IVAINC)
        this.w_MVIVAIMB = IIF(EMPTY(this.w_IVAIMB), IIF(Not Empty(this.w_O_IVAIMB) And this.w_ROTT_I="N" and this.w_DBRK=this.w_NEWDES,this.w_O_IVAIMB,this.w_MVIVAIMB), this.w_IVAIMB)
        this.w_MVIVATRA = IIF(EMPTY(this.w_IVATRA), IIF(Not Empty(this.w_O_IVATRA) And this.w_ROTT_T="N" and this.w_DBRK=this.w_NEWDES,this.w_O_IVATRA,this.w_MVIVATRA), this.w_IVATRA)
        this.w_MVIVABOL = IIF(EMPTY(this.w_IVABOL),IIF(this.w_SPEBOL=0,this.w_MVIVABOL,IIF(EMPTY(this.w_MVIVABOL),IIF(Not Empty(this.w_O_IVABOL) And this.w_ROTT_B="N" and this.w_DBRK=this.w_NEWDES,this.w_O_IVABOL,g_COIBOL),this.w_MVIVABOL)),this.w_IVABOL)
        * --- Iva cauzioni non pu� essere vuota sul documento di origine ed � utilizzata nell'ordinamento e raggruppamento
        this.w_MVIVACAU = this.w_IVACAU
        if Not Empty(this.w_MVIVACAU)
          * --- Rileggo i valori dell'Iva per cauzioni imballi
          * --- Read from VOCIIVA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VOCIIVA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "IVBOLIVA,IVREVCHA"+;
              " from "+i_cTable+" VOCIIVA where ";
                  +"IVCODIVA = "+cp_ToStrODBC(this.w_MVIVACAU);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              IVBOLIVA,IVREVCHA;
              from (i_cTable) where;
                  IVCODIVA = this.w_MVIVACAU;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_BOLCAU = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
            this.w_REVCAU = NVL(cp_ToDate(_read_.IVREVCHA),cp_NullValue(_read_.IVREVCHA))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        * --- Flag forza cauzione: se tra i documenti di origine ne esiste uno con il flag attivo
        *     lo attivo anche sul documento di destinazione
        this.w_MVFLFOCA = IIF(this.w_FLFOCA="S","S",this.w_MVFLFOCA)
        * --- Memorizzo i valori dei codici Iva spese per controllare alla riga successiva
        *     se dovr� eseguire rottura 
        this.w_O_IVAIMB = this.w_MVIVAIMB
        this.w_O_IVABOL = this.w_MVIVABOL
        this.w_O_IVATRA = this.w_MVIVATRA
        this.w_MVTOTENA = 0
        this.w_MVTOTRIT = 0
        this.w_OBRK = NVL(SERIAL, SPACE(10))
      endif
      * --- Se accorpo dei documenti devo necessariamente ricalcolare le rate
      if this.w_OBRK<>NVL(SERIAL, SPACE(10)) OR this.w_DBRK=this.w_NEWDES
        this.w_CALRATE = .T.
      endif
      if empty(this.w_MVCODPAG)
        if nvl(FLSERI,SPACE(10))="XXXXXXXXXX"
          * --- Riazzero variabili dei riferimento Msg errore
          if this.w_FLDDESOLD<>nvl(FLDDES,SPACE(5))
            this.w_RIFMES = SPACE(100)
          endif
          * --- Memorizzo informazioni per  messaggio di errore
          this.w_RIFMES = this.W_RIFMES+CHR(13) + ALLTRIM(STR(NVL(MVNUMDOC,0),15))+IIF(EMPTY(NVL(MVALFDOC,Space(10))),"", "/"+Alltrim(MVALFDOC))
          this.w_RIFMES = ALLTRIM(this.w_RIFMES) + "  " + ah_Msgformat("del.: %1", DTOC(CP_TODATE(MVDATDOC)) )
        else
          * --- Memorizzo informazioni per  messaggio di errore
          this.w_RIFMES = SPACE(100)
          this.w_RIFMES = ALLTRIM(STR(NVL(MVNUMDOC,0),15))+IIF(EMPTY(NVL(MVALFDOC,Space(10))),"", "/"+Alltrim(MVALFDOC))
          this.w_RIFMES = ALLTRIM(this.w_RIFMES) + "  " + ah_Msgformat("del.: %1", DTOC(CP_TODATE(MVDATDOC)) )
        endif
        this.w_FLDDESOLD = nvl(FLDDES,SPACE(5))
      endif
      if NOT EMPTY(g_ARTDES)
        this.w_ORNUMDOC = Nvl( GeneFatt.MVNUMDOC , 0 )
        this.w_ORALFDOC = Nvl( GeneFatt.MVALFDOC , Space(10) )
        this.w_ORDATDOC = Nvl( GeneFatt.MVDATDOC , cp_CharToDate("  -  -    ") )
        this.w_ORTIPDOC = this.w_TIPDOC
        this.w_ORDESRIF = this.w_DESRIF
        this.w_RIFCLI = Nvl( GeneFatt.MV__NOTE , "" )
        * --- Read from TIP_DOCU
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TDMODRIF"+;
            " from "+i_cTable+" TIP_DOCU where ";
                +"TDTIPDOC = "+cp_ToStrODBC(this.w_ORTIPDOC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TDMODRIF;
            from (i_cTable) where;
                TDTIPDOC = this.w_ORTIPDOC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MODRIF = NVL(cp_ToDate(_read_.TDMODRIF),cp_NullValue(_read_.TDMODRIF))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.Page_11()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    this.w_DBRK = this.w_NEWDES
    this.w_OLDAGG1 = NVL(MVAGG_01,"")
    this.w_OLDROTTAGG1 = this.w_ROTTAGG1
    this.w_OLDAGG2 = NVL(MVAGG_02,"")
    this.w_OLDROTTAGG2 = this.w_ROTTAGG2
    this.w_OLDAGG3 = NVL(MVAGG_03,"")
    this.w_OLDROTTAGG3 = this.w_ROTTAGG3
    this.w_OLDAGG4 = NVL(MVAGG_04,"")
    this.w_OLDROTTAGG4 = this.w_ROTTAGG4
    this.w_OLDAGG5 = NVL(MVAGG_05,cp_chartodate("  -  -  ") )
    this.w_OLDROTTAGG5 = this.w_ROTTAGG5
    this.w_OLDAGG6 = NVL(MVAGG_06,cp_chartodate("  -  -  ") )
    this.w_OLDROTTAGG6 = this.w_ROTTAGG6
    * --- Scrive nuova Riga sul Temporaneo di Appoggio
    this.w_MVTIPRIG = NVL(MVTIPRIG," ")
    this.w_MVCODICE = NVL(MVCODICE, SPACE(20))
    this.w_MVCODART = NVL(MVCODART, SPACE(20))
    this.w_MVDESART = NVL(MVDESART, SPACE(40))
    this.w_MVDESSUP = NVL(MVDESSUP, SPACE(10))
    this.w_MVUNIMIS = NVL(MVUNIMIS, SPACE(3))
    this.w_MVCATCON = NVL(MVCATCON, SPACE(5))
    this.w_MVCODCLA = NVL(MVCODCLA, SPACE(3))
    this.w_MVCONTRO = NVL(MVCONTRO, SPACE(15))
    this.w_MVCONTRA = NVL(MVCONTRA, SPACE(15))
    this.w_MVCODLIS = NVL(MVCODLIS, SPACE(5))
    this.w_MVQTAMOV = NVL(MVQTAMOV, 0)
    this.w_MVQTAUM1 = NVL(MVQTAUM1, 0)
    this.w_MVPREZZO = NVL(MVPREZZO, 0)
    this.w_MVSCONT1 = NVL(MVSCONT1, 0)
    this.w_MVSCONT2 = NVL(MVSCONT2, 0)
    this.w_MVSCONT3 = NVL(MVSCONT3, 0)
    this.w_MVSCONT4 = NVL(MVSCONT4, 0)
    this.w_MVFLOMAG = NVL(MVFLOMAG, SPACE(1))
    this.w_MVCODIVA = NVL(MVCODIVA, SPACE(5))
    this.w_LDATDOC = Nvl( GeneFatt.MVDATDOC , cp_CharToDate("  -  -    ") )
    if this.oParentObject.w_FLRICIVA AND ( EMPTY(this.oParentObject.w_RIIVDTIN) OR this.w_LDATDOC>= this.oParentObject.w_RIIVDTIN) AND ( EMPTY(this.oParentObject.w_RIIVDTFI) OR this.w_LDATDOC<= this.oParentObject.w_RIIVDTFI) AND ( EMPTY(this.oParentObject.w_RIIVLSIV) OR this.w_MVCODIVA+","$this.oParentObject.w_RIIVLSIV)
      this.w_LCODORN = NVL(GeneFatt.MVCODORN, SPACE(15))
      this.w_LTIPOPE = NVL(GeneFatt.MVTIPOPE, SPACE(10))
      this.w_DATAREG = Nvl( GeneFatt.MVDATREG , cp_CharToDate("  -  -    ") )
      this.w_LCODIVA = CALCODIV(this.w_MVCODART, this.oParentObject.w_MVTIPCON, iif(!empty(this.w_LCODORN),this.w_LCODORN,this.w_MVCODCON), this.w_LTIPOPE, this.w_DATAREG)
      if NOT EMPTY(NVL(this.w_LCODIVA," "))
        this.w_MVCODIVA = this.w_LCODIVA
        * --- Read from VOCIIVA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VOCIIVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "IVPERIVA,IVBOLIVA"+;
            " from "+i_cTable+" VOCIIVA where ";
                +"IVCODIVA = "+cp_ToStrODBC(this.w_MVCODIVA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            IVPERIVA,IVBOLIVA;
            from (i_cTable) where;
                IVCODIVA = this.w_MVCODIVA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PERIVA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
          this.w_BOLIVA = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
    else
      this.w_PERIVA = NVL(IVPERIVA, 0)
      this.w_BOLIVA = NVL(IVBOLIVA, " ")
    endif
    if this.oParentObject.w_FLRICIVA AND ( EMPTY(this.oParentObject.w_RIIVDTIN) OR this.w_LDATDOC>= this.oParentObject.w_RIIVDTIN) AND ( EMPTY(this.oParentObject.w_RIIVDTFI) OR this.w_LDATDOC<= this.oParentObject.w_RIIVDTFI) AND ( EMPTY(this.oParentObject.w_RIIVLSIV) OR this.w_MVIVATRA+","$this.oParentObject.w_RIIVLSIV)
      this.w_MVIVATRA = g_COITRA
    endif
    if this.oParentObject.w_FLRICIVA AND ( EMPTY(this.oParentObject.w_RIIVDTIN) OR this.w_LDATDOC>= this.oParentObject.w_RIIVDTIN) AND ( EMPTY(this.oParentObject.w_RIIVDTFI) OR this.w_LDATDOC<= this.oParentObject.w_RIIVDTFI) AND ( EMPTY(this.oParentObject.w_RIIVLSIV) OR this.w_MVIVAIMB+","$this.oParentObject.w_RIIVLSIV)
      this.w_MVIVAIMB = g_COIIMB
    endif
    if this.oParentObject.w_FLRICIVA AND ( EMPTY(this.oParentObject.w_RIIVDTIN) OR this.w_LDATDOC>= this.oParentObject.w_RIIVDTIN) AND ( EMPTY(this.oParentObject.w_RIIVDTFI) OR this.w_LDATDOC<= this.oParentObject.w_RIIVDTFI) AND ( EMPTY(this.oParentObject.w_RIIVLSIV) OR this.w_MVIVABOL+","$this.oParentObject.w_RIIVLSIV)
      this.w_MVIVABOL = g_COIBOL
    endif
    if this.oParentObject.w_FLRICIVA AND ( EMPTY(this.oParentObject.w_RIIVDTIN) OR this.w_LDATDOC>= this.oParentObject.w_RIIVDTIN) AND ( EMPTY(this.oParentObject.w_RIIVDTFI) OR this.w_LDATDOC<= this.oParentObject.w_RIIVDTFI) AND ( EMPTY(this.oParentObject.w_RIIVLSIV) OR this.w_MVIVAINC+","$this.oParentObject.w_RIIVLSIV)
      this.w_MVIVAINC = g_COIINC
    endif
    this.w_OPREZZO = NVL(MVPREZZO, 0)
    * --- Gestione contributi accessori (g_COAC)
    *     La categoria la mantengo sempre...
    this.w_CACONT = Nvl( GeneFatt.MVCACONT , Space(5))
    * --- Contributi accessori
    this.w_RIFCAC = NVL(GENEfatt.MVRIFCAC ,0)
    if g_COAC="S"
      * --- Per la routine di esplosione sono sufficienti gli ultimi tre caratteri...
      this.w_TESTESPLACC = this.w_TDFLAPCA+NVL(GENEFATT.ANFLAPCA ,SPACE(1))+NVL(GENEFATT.ARFLAPCA ,SPACE(1))
      this.w_OFLAPCA = NVL(GENEFATT.TDFLAPCA ,SPACE(1))
    else
      this.w_OFLAPCA = "N"
      this.w_TESTESPLACC = "NNN"
    endif
    if this.w_MVTIPRIG<>"D" AND this.w_MVQTAMOV>0 And this.oParentObject.w_PRZDES="S" AND EMPTY( this.w_CACONT )
      * --- Leggo se la causale deve eseguire il ricalcolo prezzi
      * --- Read from DOC_COLL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DOC_COLL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_COLL_idx,2],.t.,this.DOC_COLL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "DCFLRIPS"+;
          " from "+i_cTable+" DOC_COLL where ";
              +"DCCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVTIPDOC);
              +" and DCCOLLEG = "+cp_ToStrODBC(this.w_ORTIPDOC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          DCFLRIPS;
          from (i_cTable) where;
              DCCODICE = this.oParentObject.w_MVTIPDOC;
              and DCCOLLEG = this.w_ORTIPDOC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DCFLRIPS = NVL(cp_ToDate(_read_.DCFLRIPS),cp_NullValue(_read_.DCFLRIPS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_DCFLRIPS <> "S"
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARCATSCM,ARGRUMER,ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP,ARPREZUM"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_MVCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARCATSCM,ARGRUMER,ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP,ARPREZUM;
            from (i_cTable) where;
                ARCODART = this.w_MVCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CATART = NVL(cp_ToDate(_read_.ARCATSCM),cp_NullValue(_read_.ARCATSCM))
          this.w_GRUMER = NVL(cp_ToDate(_read_.ARGRUMER),cp_NullValue(_read_.ARGRUMER))
          this.w_UNMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
          this.w_UNMIS2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
          this.w_OPERAT = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
          this.w_MOLTIP = NVL(cp_ToDate(_read_.ARMOLTIP),cp_NullValue(_read_.ARMOLTIP))
          this.w_PREZUM = NVL(cp_ToDate(_read_.ARPREZUM),cp_NullValue(_read_.ARPREZUM))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from KEY_ARTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CAUNIMIS,CAOPERAT,CAMOLTIP"+;
            " from "+i_cTable+" KEY_ARTI where ";
                +"CACODICE = "+cp_ToStrODBC(this.w_MVCODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CAUNIMIS,CAOPERAT,CAMOLTIP;
            from (i_cTable) where;
                CACODICE = this.w_MVCODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_UNMIS3 = NVL(cp_ToDate(_read_.CAUNIMIS),cp_NullValue(_read_.CAUNIMIS))
          this.w_OPERA3 = NVL(cp_ToDate(_read_.CAOPERAT),cp_NullValue(_read_.CAOPERAT))
          this.w_MOLTI3 = NVL(cp_ToDate(_read_.CAMOLTIP),cp_NullValue(_read_.CAMOLTIP))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANCATSCM,ANCATCOM"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_MVTIPCON);
                +" and ANCODICE = "+cp_ToStrODBC(this.w_MVCODCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANCATSCM,ANCATCOM;
            from (i_cTable) where;
                ANTIPCON = this.oParentObject.w_MVTIPCON;
                and ANCODICE = this.w_MVCODCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CATCLI = NVL(cp_ToDate(_read_.ANCATSCM),cp_NullValue(_read_.ANCATSCM))
          this.w_LCATCOM = NVL(cp_ToDate(_read_.ANCATCOM),cp_NullValue(_read_.ANCATCOM))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from LISTINI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.LISTINI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LISTINI_idx,2],.t.,this.LISTINI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "LSIVALIS,LSFLSCON"+;
            " from "+i_cTable+" LISTINI where ";
                +"LSCODLIS = "+cp_ToStrODBC(this.w_MVTCOLIS);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            LSIVALIS,LSFLSCON;
            from (i_cTable) where;
                LSCODLIS = this.w_MVTCOLIS;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_IVALIS = NVL(cp_ToDate(_read_.LSIVALIS),cp_NullValue(_read_.LSIVALIS))
          this.w_SCOLIS = NVL(cp_ToDate(_read_.LSFLSCON),cp_NullValue(_read_.LSFLSCON))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from VOCIIVA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VOCIIVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "IVPERIND"+;
            " from "+i_cTable+" VOCIIVA where ";
                +"IVCODIVA = "+cp_ToStrODBC(this.w_MVCODIVA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            IVPERIND;
            from (i_cTable) where;
                IVCODIVA = this.w_MVCODIVA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_INDIVA = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Calcola Prezzo Da Listini/Contratti
        DECLARE ARRCALC (16,1)
        * --- Azzero l'Array che verr� riempito dalla Funzione
        ARRCALC(1)=0
        * --- Il contratto � in questo caso ricalcolato dalla funzione di calcolo prezzi
        this.w_MVCONTRA = Space(15)
        this.w_QTAUM2 = CALQTA(this.w_MVQTAUM1,this.w_UNMIS2, this.w_UNMIS2,IIF(this.w_OPERAT="/","*","/"), this.w_MOLTIP, "", "", "", , this.w_UNMIS3, IIF(this.w_OPERA3="/","*","/"), this.w_MOLTI3)
        this.w_QTAUM3 = CALQTA(this.w_MVQTAUM1,this.w_UNMIS3, Space(3),IIF(this.w_OPERAT="/","*","/"), this.w_MOLTIP, "", "", "", , this.w_UNMIS3, IIF(this.w_OPERA3="/","*","/"), this.w_MOLTI3)
        DIMENSION pArrUm[9]
        pArrUm [1] = this.w_PREZUM 
 pArrUm [2] = this.w_MVUNIMIS 
 pArrUm [3] = this.w_MVQTAMOV 
 pArrUm [4] = this.w_UNMIS1 
 pArrUm [5] = this.w_MVQTAUM1 
 pArrUm [6] = this.w_UNMIS2 
 pArrUm [7] = this.w_QTAUM2 
 pArrUm [8] = this.w_UNMIS3 
 pArrUm[9] = this.w_QTAUM3
        * --- Lancio la funzione per il calcolo dei prezzi da listino contratto e ricerca sconti.
        *     Il secondo passaggio del parametro MVCODVAL sostituisce il passaggio del codice
        *     della valuta dell'ultimo costodi acquisto.
        this.w_CALPRZ = CalPrzli( this.w_MVCONTRA , this.oParentObject.w_MVTIPCON , this.w_MVCODLIS , this.w_MVCODART , this.w_GRUMER , this.w_MVQTAUM1 , this.w_MVCODVAL , this.w_MVCAOVAL , this.oParentObject.w_MVDATREG , this.w_CATCLI , this.w_CATART, this.w_MVCODVAL, this.w_MVCODCON, this.w_LCATCOM, this.w_MVFLSCOR, this.w_SCOLIS,0,"V",@ARRCALC, this.oParentObject.w_PRZVAC, this.w_MVFLVEAC, "N", @pArrUm )
        this.w_LIPREZZO = ARRCALC(5)
        this.w_CLUNIMIS = ARRCALC(16)
        if this.w_MVUNIMIS<>this.w_CLUNIMIS AND NOT EMPTY(this.w_CLUNIMIS)
          this.w_LISCON = ARRCALC(7)
          this.w_QTALIS = IIF(this.w_CLUNIMIS=this.w_UNMIS1,this.w_MVQTAUM1, IIF(this.w_CLUNIMIS=this.w_UNMIS2, this.w_QTAUM2, this.w_QTAUM3))
          this.w_MVPREZZO = cp_Round(CALMMPZ(this.w_LIPREZZO, this.w_MVQTAMOV, this.w_QTALIS, IIF(this.w_LISCON=2, this.w_IVALIS, "N"), IIF(this.w_MVFLSCOR="S", 0, this.w_PERIVA), this.w_DECUNI),this.w_DECUNI)
        endif
        this.w_MVSCONT1 = ARRCALC(1)
        this.w_MVSCONT2 = ARRCALC(2)
        this.w_MVSCONT3 = ARRCALC(3)
        this.w_MVSCONT4 = ARRCALC(4)
        this.w_MVCONTRA = ARRCALC(9)
        this.w_IVACON = ARRCALC(12)
        this.w_APPOLIS = (IIF(Empty(this.w_UNMIS1),Space(3),this.w_UNMIS1))+(IIF(Empty(this.w_UNMIS2),Space(3),this.w_UNMIS2))+(IIF(Empty(this.w_UNMIS3),Space(3),this.w_UNMIS3))+this.w_MVUNIMIS+this.w_OPERAT+this.w_OPERA3+this.w_IVALIS+"P"+ALLTRIM(STR(this.w_DECUNI))
        if EMPTY(this.w_CLUNIMIS)
          this.w_MVPREZZO = CALMMLIS(this.w_LIPREZZO,this.w_APPOLIS,this.w_MOLTIP,this.w_MOLTI3, IIF(this.w_MVFLSCOR="S", 0, this.w_PERIVA))
        else
          if this.w_MVUNIMIS=this.w_CLUNIMIS
            * --- mi serve solo calcolare l'iva pertanto inganno il ricalcolo delle quantit� mettendo i moltiplicatori a 1
            this.w_MVPREZZO = CALMMLIS(this.w_LIPREZZO,this.w_APPOLIS,1,1, IIF(this.w_MVFLSCOR="S", 0, this.w_PERIVA))
          endif
        endif
        this.w_MVVALRIG = CAVALRIG(this.w_MVPREZZO,this.w_MVQTAMOV, this.w_MVSCONT1,this.w_MVSCONT2,this.w_MVSCONT3,this.w_MVSCONT4,this.w_DECTOT)
        this.w_MVVALMAG = CAVALMAG(this.w_MVFLSCOR, this.w_MVVALRIG, this.w_MVIMPSCO, 0, this.w_PERIVA, this.w_DECTOT, this.w_MVCODIVE, this.w_PERIVE )
        this.w_MVIMPNAZ = CAIMPNAZ(this.w_MVFLVEAC, this.w_MVVALMAG, this.w_MVCAOVAL, this.w_CAONAZ, this.w_MVDATDOC, this.w_MVVALNAZ, this.w_MVCODVAL, this.w_MVCODIVE, this.w_PERIVE, this.w_INDIVE, this.w_PERIVA, this.w_INDIVA )
        SELECT GENEFATT
      endif
    endif
    this.w_MVVALRIG = NVL(MVVALRIG, 0)
    this.w_MVSERRIF = NVL(SERIAL, SPACE(10))
    this.w_MVROWRIF = NVL(CPROWNUM, 0)
    this.w_MVPESNET = NVL(MVPESNET, 0)
    this.w_MVFLTRAS = NVL(MVFLTRAS, " ")
    this.w_MVNOMENC = NVL(MVNOMENC, SPACE(8))
    this.w_MVUMSUPP = NVL(MVUMSUPP, SPACE(3))
    this.w_MVMOLSUP = NVL(MVMOLSUP, 0)
    this.w_MVNAZPRO = NVL(MVNAZPRO,"   ")
    this.w_MVPROORD = NVL(MVPROORD,"  ")
    this.w_MVNUMCOL = NVL(MVNUMCOL, 0)
    this.w_MVCONIND = NVL(MVCONIND, SPACE(15))
    this.w_SERORI = NVL(MVSERRIF, SPACE(10))
    this.w_ROWORI = NVL(MVROWRIF, 0)
    this.w_FLSERA = NVL(ARTIPSER, SPACE(1))
    this.w_TDORDAPE = NVL(TDORDAPE, " ")
    * --- Se � attivo il flag genera provvigioni sulla fattura differita e ho attivo il flag
    *     'Provvigioni in Generazione documenti' nei parametri provvigioni
    *     svuoto i valori delle provvigioni in modo che mi vengano ricalcolate dal GSVE_BPP
    if g_PROGEN= "S"
      this.w_MVTIPPRO = IIF(NVL(MVTIPPRO,SPACE(2))$"FO-ES", NVL(MVTIPPRO,SPACE(2)),IIF(g_CALPRO="DI","DC",IIF(g_CALPRO="GD","ST","CT")))
      this.w_MVTIPPR2 = IIF(NVL(MVTIPPR2,SPACE(2))$"FO-ES", NVL(MVTIPPR2,SPACE(2)),IIF(g_CALPRO="DI","DC",IIF(g_CALPRO="GD","ST","CT")))
      this.w_MVPERPRO = IIF(NVL(MVTIPPRO,SPACE(2))$"FO-ES", NVL(MVPERPRO, 0), 0)
      this.w_MVIMPPRO = IIF(NVL(MVTIPPRO,SPACE(2))$"FO-ES", NVL(MVIMPPRO, 0), 0)
      this.w_MVPROCAP = IIF(NVL(MVTIPPR2,SPACE(2))$"FO-ES", NVL(MVPROCAP, 0), 0)
      this.w_MVIMPCAP = IIF(NVL(MVTIPPR2,SPACE(2))$"FO-ES", NVL(MVIMPCAP, 0), 0)
    else
      * --- Altrimenti riporto quelli del DDT
      this.w_MVTIPPRO = NVL(MVTIPPRO,SPACE(2))
      this.w_MVTIPPR2 = NVL(MVTIPPR2,SPACE(2))
      this.w_MVPERPRO = NVL(MVPERPRO, 0)
      this.w_MVIMPPRO = NVL(MVIMPPRO, 0)
      this.w_MVPROCAP = NVL(MVPROCAP, 0)
      this.w_MVIMPCAP = NVL(MVIMPCAP, 0)
    endif
    this.w_MVCODCES = NVL(MVCODCES,SPACE(20))
    this.w_MVIMPCOM = NVL(MVIMPCOM, 0)
    this.w_DOQTAEVA = Nvl(MVQTAEVA,0)
    this.w_DOIMPEVA = Nvl(MVIMPEVA,0)
    this.w_MVFLNOAN = NVL(MVFLNOAN, "N")
    * --- Analitica Documento 
    if g_PERCCR="S" AND this.oParentObject.w_FLANAL="S" AND this.w_MVFLNOAN="N"
      this.w_MVCODCEN = NVL(MVCODCEN, SPACE(15))
    else
      this.w_MVCODCEN = SPACE(15)
    endif
    * --- Gestione Commessa
    if this.w_MVTIPRIG<>"D"
      if ((g_COMM="S" AND this.oParentObject.w_FLGCOM="S") OR (g_PERCCR="S" AND this.oParentObject.w_FLANAL="S" AND this.w_MVFLNOAN="N")) 
        this.w_MVVOCCEN = NVL(MVVOCCEN, SPACE(15))
      else
        this.w_MVVOCCEN = SPACE(15)
      endif
      if ((g_COMM="S" AND this.oParentObject.w_FLGCOM="S") OR (g_PERCAN="S" AND this.oParentObject.w_FLANAL="S" AND this.w_MVFLNOAN="N")) 
        this.w_MVCODCOM = NVL(MVCODCOM, SPACE(15))
      else
        this.w_MVCODCOM = SPACE(15)
      endif
    else
      this.w_MVVOCCEN = SPACE(15)
      this.w_MVCODCOM = SPACE(15)
    endif
    this.w_MVCODATT = IIF(g_COMM="S" AND this.oParentObject.w_FLGCOM="S",NVL(MVCODATT, SPACE(15)),Space(15))
    this.w_MVCODRES = NVL(MVCODRES, SPACE(5))
    this.w_MVIMPEVA = IIF(this.w_MVTIPRIG="F", NVL(MVIMPEVA, 0), 0)
    this.w_MVINICOM = CP_TODATE(MVINICOM)
    this.w_MVFINCOM = CP_TODATE(MVFINCOM)
    * --- Flag evasione ordine apero
    this.w_FLEVOA = .F.
    if NVL(TDORDAPE, " ") = "S"
      * --- Ordine Aperto non deve evadere il documento di origine ma aggiornare la data 
      *     di prossima fatturazione
      this.w_DATEVA = NVL(MVDATEVA, CTOD(""))
      * --- Calcolo data prossima fatturazione
      this.w_NEXTFAT = MAX(NextTime(NVL(MVMC_PER,"   "), this.w_DATEVA, .f., .t.), NVL(MVDATOAI, CTOD("")))
      this.w_MVFLARIF = " "
      this.w_MVFLERIF = " "
      this.w_FLEVOA = .T.
      * --- Se frequenza unica svuoto la data prossima fatturazione
      * --- Read from MODCLDAT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MODCLDAT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MODCLDAT_idx,2],.t.,this.MODCLDAT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MD__FREQ"+;
          " from "+i_cTable+" MODCLDAT where ";
              +"MDCODICE = "+cp_ToStrODBC(NVL(MVMC_PER, " "));
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MD__FREQ;
          from (i_cTable) where;
              MDCODICE = NVL(MVMC_PER, " ");
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MD__FREQ = NVL(cp_ToDate(_read_.MD__FREQ),cp_NullValue(_read_.MD__FREQ))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if (this.w_NEXTFAT> NVL(MVDATOAF, CTOD("")) And not Empty(NVL(MVDATOAF, CTOD("")))) Or (this.w_MD__FREQ="U")
        * --- Sbianco la data prevista evasione
        this.w_NEXTFAT = CTOD("")
      endif
    else
      * --- Per consentire l'aggiornamento del valore fiscale nel documento di origine 
      * --- in caso di modifica del documento generato � necessario :
      this.w_MVFLARIF = "+"
      this.w_MVFLERIF = "S"
      this.w_NEXTFAT = CTOD("")
      this.w_DATEVA = CTOD("")
    endif
    if this.w_MVTIPRIG="F" AND this.w_MVIMPEVA<>0
      * --- Aggiusta le Valori stornando l'evaso
      this.w_MVPREZZO = this.w_MVPREZZO - this.w_MVIMPEVA
    endif
    * --- Ricalcolo comunque MVVALRIG per Generazione Fatture differite da DDT parzialmente Evasi
    this.w_MVVALRIG = CAVALRIG(this.w_MVPREZZO,this.w_MVQTAMOV, this.w_MVSCONT1,this.w_MVSCONT2,this.w_MVSCONT3,this.w_MVSCONT4,this.w_DECTOT)
    * --- Totalizzatori
    this.w_TOTALE = this.w_TOTALE + this.w_MVVALRIG
    this.w_TOTMERCE = this.w_TOTMERCE + IIF(this.w_MVFLOMAG="X" AND this.w_FLSERA<>"S", this.w_MVVALRIG, 0)
    this.w_APPO = Alltrim(STR(NVL(MVNUMDOC,0),15,0))+" "+Alltrim(NVL(MVALFDOC,Space(10)))+" " + ah_Msgformat("Del.: %1", DTOC(CP_TODATE(MVDATDOC)) )
     
 INSERT INTO GeneApp ; 
 (t_MVTIPRIG, t_MVCODICE, t_MVCODART, ; 
 t_MVDESART, t_MVDESSUP, t_MVUNIMIS, t_MVCATCON, ; 
 t_MVCONTRO, t_MVCODCLA, t_MVCONTRA, t_MVCODLIS, ; 
 t_MVQTAMOV, t_MVQTAUM1, t_MVPREZZO, ; 
 t_MVSCONT1, t_MVSCONT2, t_MVSCONT3, t_MVSCONT4, ; 
 t_MVFLOMAG, t_MVCODIVA, t_PERIVA, t_BOLIVA, t_MVVALRIG, t_MVPERPRO, ; 
 t_MVIMPPRO, t_MVSERRIF, t_MVROWRIF, ; 
 t_MVPESNET, t_MVFLTRAS, t_MVNOMENC, t_MVUMSUPP, ; 
 t_MVMOLSUP, t_MVNUMCOL, t_MVCONIND, ; 
 t_MVNAZPRO, t_MVPROORD, t_MVCODCEN, t_MVVOCCEN, ; 
 t_MVCODCOM, t_MVINICOM, t_MVFINCOM,t_MVFLARIF, t_MVIMPEVA, ; 
 t_MESS, t_MVSERORI, t_MVNUMORI, t_MVFLERIF, t_FLSERA,t_MVTIPPRO, ; 
 t_MVCODCES, t_MVIMPCOM, t_MVIMPAC2, t_DOQTAEVA, t_DOIMPEVA, t_MVTIPPR2, t_MVPROCAP, t_MVIMPCAP,t_MVFLNOAN,t_ORAPDATEVA,t_ORAPDATGEN, t_TDORDAPE, t_MVCODRES, t_TESTESPLACC, t_MVRIFCAC, t_MVCACONT, t_FATHERRIF, t_MVFLORCO,t_MVCODATT) ; 
 VALUES ( this.w_MVTIPRIG, this.w_MVCODICE, this.w_MVCODART, ; 
 this.w_MVDESART, this.w_MVDESSUP, this.w_MVUNIMIS, this.w_MVCATCON, ; 
 this.w_MVCONTRO, this.w_MVCODCLA, this.w_MVCONTRA, this.w_MVCODLIS, ; 
 this.w_MVQTAMOV, this.w_MVQTAUM1, this.w_MVPREZZO, ; 
 this.w_MVSCONT1, this.w_MVSCONT2, this.w_MVSCONT3, this.w_MVSCONT4, ; 
 this.w_MVFLOMAG, this.w_MVCODIVA, this.w_PERIVA, this.w_BOLIVA, this.w_MVVALRIG, this.w_MVPERPRO, ; 
 this.w_MVIMPPRO, this.w_MVSERRIF, this.w_MVROWRIF, ; 
 this.w_MVPESNET, this.w_MVFLTRAS, this.w_MVNOMENC, this.w_MVUMSUPP, ; 
 this.w_MVMOLSUP, this.w_MVNUMCOL, this.w_MVCONIND, ; 
 this.w_MVNAZPRO, this.w_MVPROORD, this.w_MVCODCEN, this.w_MVVOCCEN, ; 
 this.w_MVCODCOM, this.w_MVINICOM, this.w_MVFINCOM, this.w_MVFLARIF, this.w_MVIMPEVA, this.w_APPO, ; 
 this.w_SERORI, this.w_ROWORI, this.w_MVFLERIF, this.w_FLSERA, this.w_MVTIPPRO, ; 
 this.w_MVCODCES, this.w_MVIMPCOM, 0, this.w_DOQTAEVA, this.w_DOIMPEVA, this.w_MVTIPPR2, this.w_MVPROCAP, this.w_MVIMPCAP,this.w_MVFLNOAN, this.w_NEXTFAT , this.w_DATEVA, this.w_TDORDAPE, this.w_MVCODRES, this.w_TESTESPLACC, this.w_RIFCAC, this.w_CACONT , this.w_MVSERRIF, " ",this.w_MVCODATT)
    ENDSCAN
    * --- Testa l'Ultima Uscita
    this.Page_4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Variabili Globali e Locali
    * --- Dati aggiuntivi
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se non e' il Primo Ingresso, Scrive il Nuovo Documento di Destinazione
    if this.w_DBRK <> "##zz##" AND RECCOUNT("GeneApp") > 0
      * --- Aggiorna le Spese Accessorie e gli Sconti Finali
      this.w_GIORN1 = 0
      this.w_BOLINC = " "
      this.w_PEIINC = 0
      this.w_GIORN2 = 0
      this.w_BOLIMB = " "
      this.w_PEIIMB = 0
      this.w_MESE1 = 0
      this.w_BOLTRA = " "
      this.w_PEITRA = 0
      this.w_MESE2 = 0
      this.w_BOLBOL = " "
      this.w_CLBOLFAT = " "
      this.w_GIOFIS = 0
      this.w_MVQTACOL = 0
      this.w_CODNAZ = g_CODNAZ
      this.w_MVQTAPES = 0
      this.w_MVQTALOR = 0
      this.w_MVFLSALD = " "
      this.w_FLESIG = ""
      this.w_ERRID1 = .F.
      this.w_FLGRITE = " "
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANGIOSC1,ANGIOSC2,AN1MESCL,AN2MESCL,ANBOLFAT,ANGIOFIS,ANNAZION,ANFLAPCA,ANFLESIG,ANFLRITE"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_MVTIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_MVCODCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANGIOSC1,ANGIOSC2,AN1MESCL,AN2MESCL,ANBOLFAT,ANGIOFIS,ANNAZION,ANFLAPCA,ANFLESIG,ANFLRITE;
          from (i_cTable) where;
              ANTIPCON = this.oParentObject.w_MVTIPCON;
              and ANCODICE = this.w_MVCODCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_GIORN1 = NVL(cp_ToDate(_read_.ANGIOSC1),cp_NullValue(_read_.ANGIOSC1))
        this.w_GIORN2 = NVL(cp_ToDate(_read_.ANGIOSC2),cp_NullValue(_read_.ANGIOSC2))
        this.w_MESE1 = NVL(cp_ToDate(_read_.AN1MESCL),cp_NullValue(_read_.AN1MESCL))
        this.w_MESE2 = NVL(cp_ToDate(_read_.AN2MESCL),cp_NullValue(_read_.AN2MESCL))
        this.w_CLBOLFAT = NVL(cp_ToDate(_read_.ANBOLFAT),cp_NullValue(_read_.ANBOLFAT))
        this.w_GIOFIS = NVL(cp_ToDate(_read_.ANGIOFIS),cp_NullValue(_read_.ANGIOFIS))
        this.w_CODNAZ = NVL(cp_ToDate(_read_.ANNAZION),cp_NullValue(_read_.ANNAZION))
        this.w_ANFLAPCA = NVL(cp_ToDate(_read_.ANFLAPCA),cp_NullValue(_read_.ANFLAPCA))
        this.w_FLESIG = NVL(cp_ToDate(_read_.ANFLESIG),cp_NullValue(_read_.ANFLESIG))
        this.w_FLGRITE = NVL(cp_ToDate(_read_.ANFLRITE),cp_NullValue(_read_.ANFLRITE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      do case
        case NVL(this.w_FLESIG,"")="S" AND NVL(this.oParentObject.w_FLIVDF,"")=" "
          * --- Cliente di tipo soggetto pubblico e causale contabile senza esigibilit� differita
          this.w_ERRID1 = .T.
      endcase
      * --- IVA e Bolli su Spese Accessorie
      if NOT EMPTY(this.w_MVIVAINC)
        * --- Read from VOCIIVA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VOCIIVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "IVPERIVA,IVBOLIVA"+;
            " from "+i_cTable+" VOCIIVA where ";
                +"IVCODIVA = "+cp_ToStrODBC(this.w_MVIVAINC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            IVPERIVA,IVBOLIVA;
            from (i_cTable) where;
                IVCODIVA = this.w_MVIVAINC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PEIINC = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
          this.w_BOLINC = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      if NOT EMPTY(this.w_MVIVAIMB)
        * --- Read from VOCIIVA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VOCIIVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "IVPERIVA,IVBOLIVA"+;
            " from "+i_cTable+" VOCIIVA where ";
                +"IVCODIVA = "+cp_ToStrODBC(this.w_MVIVAIMB);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            IVPERIVA,IVBOLIVA;
            from (i_cTable) where;
                IVCODIVA = this.w_MVIVAIMB;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PEIIMB = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
          this.w_BOLIMB = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      if NOT EMPTY(this.w_MVIVATRA)
        * --- Read from VOCIIVA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VOCIIVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "IVPERIVA,IVBOLIVA"+;
            " from "+i_cTable+" VOCIIVA where ";
                +"IVCODIVA = "+cp_ToStrODBC(this.w_MVIVATRA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            IVPERIVA,IVBOLIVA;
            from (i_cTable) where;
                IVCODIVA = this.w_MVIVATRA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PEITRA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
          this.w_BOLTRA = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      if NOT EMPTY(this.w_MVIVABOL)
        * --- Read from VOCIIVA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VOCIIVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "IVBOLIVA"+;
            " from "+i_cTable+" VOCIIVA where ";
                +"IVCODIVA = "+cp_ToStrODBC(this.w_MVIVABOL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            IVBOLIVA;
            from (i_cTable) where;
                IVCODIVA = this.w_MVIVABOL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_BOLBOL = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      USE IN SELECT("AppRagg")
      this.Page_5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if g_COAC="S" And this.w_ANFLAPCA="S" And this.w_TDFLAPCA="S" 
        this.Page_12()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Prima di lanciare il GSAR_BFA azzero l'array delle rate che altrimenti rimane valorizzato 
      *     in base al documento esaminato precedentemente
      this.w_LOOPR = 1
      do while this.w_LOOPR <= alen(DR,1)
        DR[ this.w_LOOPR , 1] = cp_CharToDate("  -  -  ")
        DR[ this.w_LOOPR , 2] = 0
        DR[ this.w_LOOPR , 3] = "  "
        DR[ this.w_LOOPR , 4] = "  "
        DR[ this.w_LOOPR , 5] = "  "
        DR[ this.w_LOOPR , 6] = "  "
        DR[ this.w_LOOPR , 7] = " "
        DR[ this.w_LOOPR , 8] = " "
        DR[ this.w_LOOPR , 9] = " "
        this.w_LOOPR = this.w_LOOPR + 1
      enddo
      * --- Calcoli Finali
      this.w_RETBFA = .F.
      GSAR_BFA(this,"D")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Calcola MVSERIAL, MVNUMREG, MVNUMDOC
      this.oParentObject.w_MVSERIAL = SPACE(10)
      this.oParentObject.w_MVNUMREG = 0
      this.oParentObject.w_MVNUMEST = 0
      this.oParentObject.w_MVANNDOC = STR(YEAR(this.w_MVDATDOC), 4, 0)
      this.w_CPROWNUM = 0
      this.w_CPROWORD = 0
      this.w_MAXROW = 0
      this.w_MVNUMRIF = -20
      this.w_NUDOC = this.w_NUDOC + 1
      this.w_NURIG = 0
      * --- Controllo importo minimo fatturabile
      this.w_IMPFIN = 0
      this.w_FLSFIN = " "
      if this.oParentObject.w_PSMINIMP<>0 And Not Empty(this.oParentObject.w_PSMINVAL) And this.oParentObject.w_PSMINVAL=this.w_MVCODVAL
        if Abs(this.w_TOTFATTU-this.w_MVACCPRE)<=this.oParentObject.w_PSMINIMP
          * --- Nel caso l'importo che risulterebbe da fatturare sia inferiore o uguale a quello minimo 
          *     indicato sulla causale, imposto il totale fatturato come sconto finanziario e azzero le rate calcolate
          *     dal GSAR_BFA
          this.w_IMPFIN = cp_Round(this.w_TOTFATTU-this.w_MVACCPRE, this.w_DECTOT)
          this.w_FLSFIN = "S"
          this.w_MVFLSCAF = " "
          * --- Svuoto array delle rate
          DR =""
        endif
      endif
      if this.w_OKINS
        this.oParentObject.w_MVNUMDOC = this.oParentObject.w_MVNUMDOC + 1
        * --- Pagina 9 testa MVNUNMDOC e cerca il primo progressivo libero, se trova
        *     un progessivo pi� piccolo di PRODOC pone a .T. w_OKINS, se non trova
        *     progressivi liberi per riepire gli eventuali buchi pone a .F. w_OKINS
        this.Page_9()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      if this.w_OKINS=.F.
        * --- MVNUMDOC deve essere azzerato per evitare che i documenti successivi vengano 'Forzati'
        *     Azzero MVNUMDOC sia quando ho raggiunto l'ultimo progressivo sia quando viene 
        *     specificato un progressivo pi� grande di PRODOC
        this.oParentObject.w_MVNUMDOC = 0
      endif
      * --- Try
      local bErr_0540A018
      bErr_0540A018=bTrsErr
      this.Try_0540A018()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
      endif
      bTrsErr=bTrsErr or bErr_0540A018
      * --- End
    endif
    * --- Azzera il Temporaneo di Appoggio
     
 CREATE CURSOR GeneApp ; 
 (CPROWNUM N(4,0), t_MVTIPRIG C(1), t_MVCODICE C(20), t_MVCODART C(20), ; 
 t_MVDESART C(40), t_MVDESSUP M(10), t_MVUNIMIS C(3), t_MVCATCON C(5), ; 
 t_MVCONTRO C(15), t_MVCODCLA C(3), t_MVCONTRA C(15), t_MVCODLIS C(5), t_MVCONIND C(15), ; 
 t_MVQTAMOV N(12,3), t_MVQTAUM1 N(12,3), t_MVPREZZO N(18,5), ; 
 t_MVSCONT1 N(6,2), t_MVSCONT2 N(6,2), t_MVSCONT3 N(6,2), t_MVSCONT4 N(6,2), ; 
 t_MVFLOMAG C(1), t_MVCODIVA C(5), t_PERIVA N(5,2), t_BOLIVA C(1), t_MVVALRIG N(18,4), t_MVPERPRO N(5,2), ; 
 t_MVIMPPRO N(18,4), t_MVSERRIF C(10), t_MVROWRIF N(4,0), ; 
 t_MVPESNET N(9,3), t_MVFLTRAS C(1), t_MVNOMENC C(8), t_MVUMSUPP C(3), ; 
 t_MVMOLSUP N(8,3), t_MVNUMCOL N(5,0), ; 
 t_MVIMPACC N(18,4), t_MVIMPSCO N(18,4), t_MVVALMAG N(18,4), t_MVIMPNAZ N(18,4), ; 
 t_MVNAZPRO C(3), t_MVPROORD C(2), t_MVCODCEN C(15), t_MVVOCCEN C(15), ; 
 t_MVCODCOM C(15), t_MVINICOM D(8), t_MVFINCOM D(8), t_MVFLARIF C(1), t_MVIMPEVA N(18,5), t_MESS C(30),; 
 t_MVSERORI C(10), t_MVNUMORI N(4,0), t_MVFLERIF C(1), t_FLSERA C(1), ; 
 t_MVTIPPRO C(2),t_MVCODCES C(15), t_MVIMPCOM N(18,4), t_MVIMPAC2 N(18,4), t_DOQTAEVA N(12,5) , ; 
 t_DOIMPEVA N(18,5), t_MVTIPPR2 C(2), t_MVPROCAP N(5,2), t_MVIMPCAP N(18,4),t_MVFLNOAN C(1) , t_ORAPDATEVA D(8) , t_ORAPDATGEN D(8), t_TDORDAPE C(1), t_MVCODRES C(5), t_TESTESPLACC C(3) , t_MVRIFCAC N(4), t_MVCACONT C(5) , t_FATHERRIF C(10), t_CPROWORD N(5), t_MVFLORCO C(1),t_MVCODATT C(15))
    * --- Reinizializza le Variabili di Lavoro
    this.w_TOTMERCE = 0
    this.w_MVIVAIMB = SPACE(5)
    this.w_MVACIVA1 = SPACE(5)
    this.w_MVAIMPS1 = 0
    this.w_TOTALE = 0
    this.w_MVIVAINC = SPACE(5)
    this.w_MVACIVA2 = SPACE(5)
    this.w_MVAIMPS2 = 0
    this.w_MVSPEINC = 0
    this.w_MVSCONTI = 0
    this.w_MVIVATRA = SPACE(5)
    this.w_MVACIVA3 = SPACE(5)
    this.w_MVAIMPS3 = 0
    this.w_MVSPEIMB = 0
    this.w_MVIVABOL = SPACE(5)
    this.w_MVACIVA4 = SPACE(5)
    this.w_MVAIMPS4 = 0
    this.w_MVSPETRA = 0
    this.w_MVFLRINC = " "
    this.w_MVACIVA5 = SPACE(5)
    this.w_MVAIMPS5 = 0
    this.w_MVIVACAU = Space(5)
    this.w_BOLCAU = " "
    this.w_REVCAU = " "
    this.w_MVFLFOCA = " "
    this.w_MVCAUIMB = 0
    this.w_MVSPEBOL = 0
    this.w_MVFLRIMB = " "
    this.w_MVACIVA6 = SPACE(5)
    this.w_MVAIMPS6 = 0
    this.w_MVACCONT = 0
    this.w_MVFLRTRA = " "
    this.w_MVAFLOM1 = SPACE(1)
    this.w_MVAIMPN1 = 0
    this.w_TOTIMPON = 0
    this.w_MVAFLOM2 = SPACE(1)
    this.w_MVAIMPN2 = 0
    this.w_TOTIMPOS = 0
    this.w_MVAFLOM3 = SPACE(1)
    this.w_MVAIMPN3 = 0
    this.w_TOTFATTU = 0
    this.w_MVAFLOM4 = SPACE(1)
    this.w_MVAIMPN4 = 0
    this.w_MVIMPARR = 0
    this.w_MVAFLOM5 = SPACE(1)
    this.w_MVAIMPN5 = 0
    this.w_MVACCPRE = 0
    this.w_MVAFLOM6 = SPACE(1)
    this.w_MVAIMPN6 = 0
    this.w_MVFLSCAF = " "
  endproc
  proc Try_0540A018()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_Conn=i_TableProp[this.DOC_MAST_IDX, 3]
    cp_NextTableProg(this.oParentObject, i_Conn, "SEDOC", "i_codazi,w_MVSERIAL")
    cp_NextTableProg(this.oParentObject, i_Conn, "PRDOC", "i_codazi,w_MVANNDOC,w_MVPRD,w_MVALFDOC,w_MVNUMDOC")
    this.oParentObject.w_MVNUMREG = this.oParentObject.w_MVNUMDOC
    * --- Verifica se esiste gia' un documento con stesso numero
    this.w_LNREG = 0
    this.w_LDREG = cp_CharToDate("  -  -  ")
    this.w_LSERIAL = SPACE(10)
    * --- Read from DOC_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MVNUMREG,MVDATREG,MVSERIAL"+;
        " from "+i_cTable+" DOC_MAST where ";
            +"MVANNDOC = "+cp_ToStrODBC(this.oParentObject.w_MVANNDOC);
            +" and MVPRD = "+cp_ToStrODBC(this.oParentObject.w_MVPRD);
            +" and MVALFDOC = "+cp_ToStrODBC(this.oParentObject.w_MVALFDOC);
            +" and MVNUMDOC = "+cp_ToStrODBC(this.oParentObject.w_MVNUMDOC);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MVNUMREG,MVDATREG,MVSERIAL;
        from (i_cTable) where;
            MVANNDOC = this.oParentObject.w_MVANNDOC;
            and MVPRD = this.oParentObject.w_MVPRD;
            and MVALFDOC = this.oParentObject.w_MVALFDOC;
            and MVNUMDOC = this.oParentObject.w_MVNUMDOC;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_LNREG = NVL(cp_ToDate(_read_.MVNUMREG),cp_NullValue(_read_.MVNUMREG))
      this.w_LDREG = NVL(cp_ToDate(_read_.MVDATREG),cp_NullValue(_read_.MVDATREG))
      this.w_LSERIAL = NVL(cp_ToDate(_read_.MVSERIAL),cp_NullValue(_read_.MVSERIAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_ERRDOC = .F.
    this.w_ERRDIC = .F.
    if Not Empty( this.w_MVRIFDIC )
      this.w_IMPUTI = 0
      * --- Read from DIC_INTE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DIC_INTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2],.t.,this.DIC_INTE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "DIDATREV,DIIMPDIC,DIIMPUTI,DICODIVA,DINUMDOC,DISERDOC,DI__ANNO,DIDATLET,DIFLGCLO"+;
          " from "+i_cTable+" DIC_INTE where ";
              +"DISERIAL = "+cp_ToStrODBC(this.w_MVRIFDIC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          DIDATREV,DIIMPDIC,DIIMPUTI,DICODIVA,DINUMDOC,DISERDOC,DI__ANNO,DIDATLET,DIFLGCLO;
          from (i_cTable) where;
              DISERIAL = this.w_MVRIFDIC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DATREV = NVL(cp_ToDate(_read_.DIDATREV),cp_NullValue(_read_.DIDATREV))
        this.w_IMPDIC = NVL(cp_ToDate(_read_.DIIMPDIC),cp_NullValue(_read_.DIIMPDIC))
        this.w_TOTUTI = NVL(cp_ToDate(_read_.DIIMPUTI),cp_NullValue(_read_.DIIMPUTI))
        this.w_DICODIVA = NVL(cp_ToDate(_read_.DICODIVA),cp_NullValue(_read_.DICODIVA))
        this.w_DINUMDOC = NVL(cp_ToDate(_read_.DINUMDOC),cp_NullValue(_read_.DINUMDOC))
        this.w_DISERDOC = NVL(cp_ToDate(_read_.DISERDOC),cp_NullValue(_read_.DISERDOC))
        this.w_DI__ANNO = NVL(cp_ToDate(_read_.DI__ANNO),cp_NullValue(_read_.DI__ANNO))
        this.w_DIDATLET = NVL(cp_ToDate(_read_.DIDATLET),cp_NullValue(_read_.DIDATLET))
        this.w_DIFLGCLO = NVL(cp_ToDate(_read_.DIFLGCLO),cp_NullValue(_read_.DIFLGCLO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- converto il totale imponibile in valuta di conto
      if this.w_FLSILI="S" and this.w_TIPOPEDIC<>"D" 
        if this.w_DICODIVA=this.w_MVACIVA1 And this.w_MVAFLOM1="X"
          this.w_IMPUTI = this.w_MVAIMPN1
        endif
        if this.w_DICODIVA=this.w_MVACIVA2 And this.w_MVAFLOM2="X"
          this.w_IMPUTI = this.w_IMPUTI + this.w_MVAIMPN2
        endif
        if this.w_DICODIVA=this.w_MVACIVA3 And this.w_MVAFLOM3="X"
          this.w_IMPUTI = this.w_IMPUTI + this.w_MVAIMPN3
        endif
        if this.w_DICODIVA=this.w_MVACIVA4 And this.w_MVAFLOM4="X"
          this.w_IMPUTI = this.w_IMPUTI + this.w_MVAIMPN4
        endif
        if this.w_DICODIVA=this.w_MVACIVA5 And this.w_MVAFLOM5="X"
          this.w_IMPUTI = this.w_IMPUTI + this.w_MVAIMPN5
        endif
        if this.w_DICODIVA=this.w_MVACIVA6 And this.w_MVAFLOM6="X"
          this.w_IMPUTI = this.w_IMPUTI + this.w_MVAIMPN6
        endif
        this.w_DDIMPUTI = this.w_IMPUTI
        if g_PERVAL<>this.w_MVCODVAL
          this.w_DDIMPUTI = VAL2MON(this.w_DDIMPUTI, this.w_MVCAOVAL, g_CAOVAL, this.w_MVDATDOC, g_PERVAL,g_PERPVL)
        endif
        this.w_TOTUTI = this.w_DDIMPUTI+this.w_TOTUTI
        if this.w_TOTUTI>this.w_IMPDIC 
          this.w_ERRDIC = .T.
        endif
      endif
      this.w_ERRDOC = not Empty(this.w_DATREV) And this.w_MVDATDOC>this.w_DATREV
    else
      this.w_ERRDOC = .F.
      this.w_ERRDIC = .F.
    endif
    this.w_PERIND = 0
    if this.oParentObject.w_CCTIPREG="V"
      this.w_LOOP = 1
      do while this.w_LOOP<=6
         
 y=ALLTRIM(STR(this.w_LOOP))
        this.w_MVACIVA = this.w_MVACIVA&y
        if Not Empty(this.w_MVACIVA)
          * --- Read from VOCIIVA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VOCIIVA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "IVPERIND"+;
              " from "+i_cTable+" VOCIIVA where ";
                  +"IVCODIVA = "+cp_ToStrODBC(this.w_MVACIVA);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              IVPERIND;
              from (i_cTable) where;
                  IVCODIVA = this.w_MVACIVA;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_PERIND = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_PERIND<>0
            * --- Esco
            this.w_LOOP = 6
          endif
        endif
        this.w_LOOP = this.w_LOOP + 1
      enddo
    endif
    if not empty(this.w_LSERIAL) or empty(this.w_MVCODPAG) OR this.w_MVCAOVAL=0 Or this.w_PERIND<>0 Or this.w_ERRDOC OR this.w_ERRDIC or this.w_RETBFA
      this.w_oERRORLOG.AddMsgLogPartNoTrans(space(5), "%0Bloccante:%0")     
      do case
        case not empty(this.w_LSERIAL)
          * --- Legge Estremi documento di origine
          this.w_oERRORLOG.AddMsgLog("Impossibile generare doc.n.: %1 del %2 - documento gi� presente in registrazione %3 del %4", Alltrim(STR(this.oParentObject.w_MVNUMDOC,15,0))+" "+Alltrim(this.oParentObject.w_MVALFDOC), DTOC(this.w_MVDATDOC), Alltrim(STR(this.w_LNREG,15,0)), DTOC(this.w_LDREG) )     
        case empty(this.w_MVCODPAG) 
          if this.w_TIPFAT<>"S" 
            this.w_oERRORLOG.AddMsgLog("Impossibile generare fattura differita associata ai doc.n.: %1 poich� privi del codice pagamento", alltrim(this.w_RIFMES))     
          else
            this.w_oERRORLOG.AddMsgLog("Impossibile generare fattura differita associata al doc.n.: %1. Documento senza codice pagamento", alltrim(this.w_RIFMES))     
          endif
        case this.w_MVCAOVAL=0
          * --- Read from VALUTE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VALUTE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "VASIMVAL"+;
              " from "+i_cTable+" VALUTE where ";
                  +"VACODVAL = "+cp_ToStrODBC(this.w_MVCODVAL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              VASIMVAL;
              from (i_cTable) where;
                  VACODVAL = this.w_MVCODVAL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_VALSIM = NVL(cp_ToDate(_read_.VASIMVAL),cp_NullValue(_read_.VASIMVAL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_oERRORLOG.AddMsgLog("Impossibile generare doc.n.: %1 del %2 - Cambio valuta %3 del documento di destinazione non presente nell'archivio cambi", Alltrim(STR(this.oParentObject.w_MVNUMDOC,15,0))+" "+Alltrim(this.oParentObject.w_MVALFDOC), DTOC(this.w_MVDATDOC), ALLTRIM(this.w_VALSIM) )     
        case this.w_PERIND<>0
          this.w_oERRORLOG.AddMsgLog("Impossibile generare doc.n.: %1 del %2 - riga castelletto %3: codice IVA %4 di tipo indetraibile", Alltrim(STR(this.oParentObject.w_MVNUMDOC,15,0))+" "+Alltrim(this.oParentObject.w_MVALFDOC), DTOC(this.w_MVDATDOC), y,ALLTRIM(this.w_MVACIVA))     
        case this.w_ERRDOC
          * --- Testo se la dichiarazione di intento ha attivo il flag "Chiusura dichiarazione"
          if this.w_DIFLGCLO="S"
            this.w_oERRORLOG.AddMsgLog("Impossibile generare fattura - lettera di intento chiusa il %1 per il cliente %2", ALLTRIM(Dtoc( this.w_DATREV )), this.w_MVCODCON )     
          else
            this.w_oERRORLOG.AddMsgLog("Impossibile generare fattura - lettera di intento revocata il %1 per il cliente %2", ALLTRIM(Dtoc( this.w_DATREV )), this.w_MVCODCON )     
          endif
        case this.w_ERRDIC
          * --- Read from VALUTE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VALUTE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "VASIMVAL"+;
              " from "+i_cTable+" VALUTE where ";
                  +"VACODVAL = "+cp_ToStrODBC(this.w_MVCODVAL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              VASIMVAL;
              from (i_cTable) where;
                  VACODVAL = this.w_MVCODVAL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_VALSIM = NVL(cp_ToDate(_read_.VASIMVAL),cp_NullValue(_read_.VASIMVAL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_oERRORLOG.AddMsgLog("Impossibile generare fattura differita n.: %1 del %2", Alltrim(STR(this.oParentObject.w_MVNUMDOC,15,0))+" "+Alltrim(this.oParentObject.w_MVALFDOC), DTOC(this.w_MVDATDOC))     
          this.w_oERRORLOG.AddMsgLog("Dichiarazione di intento n.: %1 %2 / %3 del %4 per il cliente %5 con importo utilizzato superiore al consentito.",alltrim(str(this.w_DINUMDOC,6)),alltrim(this.w_DISERDOC),alltrim(this.w_DI__ANNO),DTOC(this.w_DIDATLET),alltrim(this.w_MVCODCON))     
          this.w_oERRORLOG.AddMsgLog("Importo utilizzato: %1 %2%0Importo dichiarazione: %3 %2", alltrim(str(this.w_TOTUTI,18,4)), this.w_VALSIM, alltrim(str(this.w_IMPDIC,18,4)))     
        case this.w_RETBFA
          this.w_oERRORLOG.AddMsgLog("Impossibile generare fattura differita n.: %1 del %2", Alltrim(STR(this.oParentObject.w_MVNUMDOC,15,0))+" "+Alltrim(this.oParentObject.w_MVALFDOC), DTOC(this.w_MVDATDOC))     
          this.w_oERRORLOG.AddMsgLog(ALLTRIM(this.w_MESS_BFA))     
      endcase
      this.w_ERRDOC = .T.
      this.oParentObject.w_MVNUMDOC = this.oParentObject.w_MVNUMDOC - 1
      * --- Documento non Accettato (no Disponibilita')
      * --- Raise
      i_Error="Errore"
      return
    else
      * --- Documento fuori sequenza
      * --- Verifico se � possibile inserire il documento, controllo se sto inserendo
      *     un documento fuori sequenza
      this.w_MESS_SEQ = CHKNUMD(this.w_MVDATDOC, this.oParentObject.w_MVNUMDOC, this.oParentObject.w_MVALFDOC, this.oParentObject.w_MVANNDOC, this.oParentObject.w_MVPRD, this.oParentObject.w_MVSERIAL, "D")
      if NOT EMPTY(this.w_MESS_SEQ)
        this.w_oERRORLOG.AddMsgLog("Attenzione generato doc.n.: %1 del %2 - %3", Alltrim(STR(this.oParentObject.w_MVNUMDOC,15,0))+" "+Alltrim(this.oParentObject.w_MVALFDOC), DTOC(this.w_MVDATDOC), this.w_MESS_SEQ)     
        this.w_NWARN = this.w_NWARN + 1
      endif
      if this.w_TIPOPEDIC="O"
        this.w_oERRORLOG.AddMsgLogPartNoTrans(space(5), "%0Warning:%0")     
        this.w_oERRORLOG.AddMsgLog("Fattura n %1 del %2, verificare dichiarazione d'intento (operazione specifica)", Alltrim(STR(this.oParentObject.w_MVNUMDOC,15,0))+" "+Alltrim(this.oParentObject.w_MVALFDOC), DTOC(this.w_MVDATDOC))     
        this.w_NWARN = this.w_NWARN + 1
      endif
      if this.w_ERRID1
        this.w_oERRORLOG.AddMsgLogPartNoTrans(space(5), "%0Warning:%0")     
        this.w_oERRORLOG.AddMsgLog("Fattura differita n.: %1 del %2", Alltrim(STR(this.oParentObject.w_MVNUMDOC,15,0))+" "+Alltrim(this.oParentObject.w_MVALFDOC), DTOC(this.w_MVDATDOC))     
        this.w_oERRORLOG.AddMsgLog("cliente di tipo soggetto pubblico e causale contabile senza esigibilit� differita%0Cliente: %1%0Causale contabile: %2", alltrim(this.w_MVCODCON), alltrim(this.oParentObject.w_MVCAUCON))     
        this.w_NWARN = this.w_NWARN + 1
      endif
      if this.w_WARNPAG
        this.w_oERRORLOG.AddMsgLogPartNoTrans(space(5), "%0Warning:%0")     
        this.w_oERRORLOG.AddMsgLog("Fattura n %1 del %2, Impostato pagamento forzato, verificare le condizioni applicate", Alltrim(STR(this.oParentObject.w_MVNUMDOC,15,0))+" "+Alltrim(this.oParentObject.w_MVALFDOC), DTOC(this.w_MVDATDOC))     
        this.w_NWARN = this.w_NWARN + 1
      endif
      * --- Scrive la Testata
      * --- Read from AZIENDA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AZIENDA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AZPLADOC"+;
          " from "+i_cTable+" AZIENDA where ";
              +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AZPLADOC;
          from (i_cTable) where;
              AZCODAZI = i_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_AZPLADOC = NVL(cp_ToDate(_read_.AZPLADOC),cp_NullValue(_read_.AZPLADOC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_MVDATPLA = IIF(this.w_AZPLADOC="S",this.w_MVDATDOC,this.oParentObject.w_MVDATREG)
      * --- Insert into DOC_MAST
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOC_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"MVSERIAL"+",MVCODESE"+",MVCODUTE"+",MVNUMREG"+",MVDATREG"+",MVTIPDOC"+",MVCLADOC"+",MVFLVEAC"+",MVFLACCO"+",MVFLINTE"+",MVFLPROV"+",MVPRD"+",MVNUMDOC"+",MVALFDOC"+",MVDATDOC"+",MVDATCIV"+",MVTCAMAG"+",MVTFICOM"+",MVTINCOM"+",MVRIFPIA"+",MVTCOLIS"+",MVTFRAGG"+",MVCAUCON"+",MVTIPCON"+",MVCODCON"+",MVCODAGE"+",MVCODDES"+",MVGENPRO"+",MVFLCONT"+",MVFLGIOM"+",MVGENEFF"+",MVANNDOC"+",MVFLSCOR"+",MVRIFDIC"+",MVCODSPE"+",MVCODVET"+",MVCODPOR"+",MVCONCON"+",MVASPEST"+",MVTIPORN"+",MVCODORN"+",MVTCONTR"+",MVCODAG2"+",MVCODVE2"+",MVCODVE3"+",MVDATPLA"+",MVEMERIC"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVSERIAL),'DOC_MAST','MVSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVCODESE),'DOC_MAST','MVCODESE');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVCODUTE),'DOC_MAST','MVCODUTE');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVNUMREG),'DOC_MAST','MVNUMREG');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVDATREG),'DOC_MAST','MVDATREG');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVTIPDOC),'DOC_MAST','MVTIPDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVCLADOC),'DOC_MAST','MVCLADOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLVEAC),'DOC_MAST','MVFLVEAC');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVFLACCO),'DOC_MAST','MVFLACCO');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVFLINTE),'DOC_MAST','MVFLINTE');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVFLPROV),'DOC_MAST','MVFLPROV');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVPRD),'DOC_MAST','MVPRD');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVNUMDOC),'DOC_MAST','MVNUMDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVALFDOC),'DOC_MAST','MVALFDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVDATDOC),'DOC_MAST','MVDATDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVDATCIV),'DOC_MAST','MVDATCIV');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVTCAMAG),'DOC_MAST','MVTCAMAG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVTFICOM),'DOC_MAST','MVTFICOM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVTINCOM),'DOC_MAST','MVTINCOM');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVRIFPIA),'DOC_MAST','MVRIFPIA');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVTCOLIS),'DOC_MAST','MVTCOLIS');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVTFRAGG),'DOC_MAST','MVTFRAGG');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVCAUCON),'DOC_MAST','MVCAUCON');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVTIPCON),'DOC_MAST','MVTIPCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODCON),'DOC_MAST','MVCODCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODAGE),'DOC_MAST','MVCODAGE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODDES),'DOC_MAST','MVCODDES');
        +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVGENPRO');
        +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVFLCONT');
        +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVFLGIOM');
        +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVGENEFF');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVANNDOC),'DOC_MAST','MVANNDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLSCOR),'DOC_MAST','MVFLSCOR');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVRIFDIC),'DOC_MAST','MVRIFDIC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODSPE),'DOC_MAST','MVCODSPE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODVET),'DOC_MAST','MVCODVET');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODPOR),'DOC_MAST','MVCODPOR');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVCONCON),'DOC_MAST','MVCONCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVASPEST),'DOC_MAST','MVASPEST');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVTIPORN),'DOC_MAST','MVTIPORN');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODORN),'DOC_MAST','MVCODORN');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVTCONTR),'DOC_MAST','MVTCONTR');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODAG2),'DOC_MAST','MVCODAG2');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODVE2),'DOC_MAST','MVCODVE2');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODVE3),'DOC_MAST','MVCODVE3');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVDATPLA),'DOC_MAST','MVDATPLA');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVEMERIC),'DOC_MAST','MVEMERIC');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'MVSERIAL',this.oParentObject.w_MVSERIAL,'MVCODESE',this.oParentObject.w_MVCODESE,'MVCODUTE',this.oParentObject.w_MVCODUTE,'MVNUMREG',this.oParentObject.w_MVNUMREG,'MVDATREG',this.oParentObject.w_MVDATREG,'MVTIPDOC',this.oParentObject.w_MVTIPDOC,'MVCLADOC',this.w_MVCLADOC,'MVFLVEAC',this.w_MVFLVEAC,'MVFLACCO',this.oParentObject.w_MVFLACCO,'MVFLINTE',this.oParentObject.w_MVFLINTE,'MVFLPROV',this.oParentObject.w_MVFLPROV,'MVPRD',this.oParentObject.w_MVPRD)
        insert into (i_cTable) (MVSERIAL,MVCODESE,MVCODUTE,MVNUMREG,MVDATREG,MVTIPDOC,MVCLADOC,MVFLVEAC,MVFLACCO,MVFLINTE,MVFLPROV,MVPRD,MVNUMDOC,MVALFDOC,MVDATDOC,MVDATCIV,MVTCAMAG,MVTFICOM,MVTINCOM,MVRIFPIA,MVTCOLIS,MVTFRAGG,MVCAUCON,MVTIPCON,MVCODCON,MVCODAGE,MVCODDES,MVGENPRO,MVFLCONT,MVFLGIOM,MVGENEFF,MVANNDOC,MVFLSCOR,MVRIFDIC,MVCODSPE,MVCODVET,MVCODPOR,MVCONCON,MVASPEST,MVTIPORN,MVCODORN,MVTCONTR,MVCODAG2,MVCODVE2,MVCODVE3,MVDATPLA,MVEMERIC &i_ccchkf. );
           values (;
             this.oParentObject.w_MVSERIAL;
             ,this.oParentObject.w_MVCODESE;
             ,this.oParentObject.w_MVCODUTE;
             ,this.oParentObject.w_MVNUMREG;
             ,this.oParentObject.w_MVDATREG;
             ,this.oParentObject.w_MVTIPDOC;
             ,this.w_MVCLADOC;
             ,this.w_MVFLVEAC;
             ,this.oParentObject.w_MVFLACCO;
             ,this.oParentObject.w_MVFLINTE;
             ,this.oParentObject.w_MVFLPROV;
             ,this.oParentObject.w_MVPRD;
             ,this.oParentObject.w_MVNUMDOC;
             ,this.oParentObject.w_MVALFDOC;
             ,this.w_MVDATDOC;
             ,this.oParentObject.w_MVDATCIV;
             ,this.oParentObject.w_MVTCAMAG;
             ,this.w_MVTFICOM;
             ,this.w_MVTINCOM;
             ,this.oParentObject.w_MVRIFPIA;
             ,this.w_MVTCOLIS;
             ,this.oParentObject.w_MVTFRAGG;
             ,this.oParentObject.w_MVCAUCON;
             ,this.oParentObject.w_MVTIPCON;
             ,this.w_MVCODCON;
             ,this.w_MVCODAGE;
             ,this.w_MVCODDES;
             ," ";
             ," ";
             ," ";
             ," ";
             ,this.oParentObject.w_MVANNDOC;
             ,this.w_MVFLSCOR;
             ,this.w_MVRIFDIC;
             ,this.w_MVCODSPE;
             ,this.w_MVCODVET;
             ,this.w_MVCODPOR;
             ,this.w_MVCONCON;
             ,this.w_MVASPEST;
             ,this.w_MVTIPORN;
             ,this.w_MVCODORN;
             ,this.w_MVTCONTR;
             ,this.w_MVCODAG2;
             ,this.w_MVCODVE2;
             ,this.w_MVCODVE3;
             ,this.w_MVDATPLA;
             ,this.w_MVEMERIC;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Occorrono 3 frasi per problema lunghezza stringa INSERT
      * --- Write into DOC_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVCODPAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODPAG),'DOC_MAST','MVCODPAG');
        +",MVCODIVE ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODIVE),'DOC_MAST','MVCODIVE');
        +",MVSCONTI ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCONTI),'DOC_MAST','MVSCONTI');
        +",MVSPEINC ="+cp_NullLink(cp_ToStrODBC(this.w_MVSPEINC),'DOC_MAST','MVSPEINC');
        +",MVIVAINC ="+cp_NullLink(cp_ToStrODBC(this.w_MVIVAINC),'DOC_MAST','MVIVAINC');
        +",MVFLRINC ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLRINC),'DOC_MAST','MVFLRINC');
        +",MVSPETRA ="+cp_NullLink(cp_ToStrODBC(this.w_MVSPETRA),'DOC_MAST','MVSPETRA');
        +",MVIVATRA ="+cp_NullLink(cp_ToStrODBC(this.w_MVIVATRA),'DOC_MAST','MVIVATRA');
        +",MVFLRTRA ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLRTRA),'DOC_MAST','MVFLRTRA');
        +",MVSPEIMB ="+cp_NullLink(cp_ToStrODBC(this.w_MVSPEIMB),'DOC_MAST','MVSPEIMB');
        +",MVIVAIMB ="+cp_NullLink(cp_ToStrODBC(this.w_MVIVAIMB),'DOC_MAST','MVIVAIMB');
        +",MVFLRIMB ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLRIMB),'DOC_MAST','MVFLRIMB');
        +",MVSPEBOL ="+cp_NullLink(cp_ToStrODBC(this.w_MVSPEBOL),'DOC_MAST','MVSPEBOL');
        +",MVDATDIV ="+cp_NullLink(cp_ToStrODBC(this.w_MVDATDIV),'DOC_MAST','MVDATDIV');
        +",MVCODBAN ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODBAN),'DOC_MAST','MVCODBAN');
        +",MVVALNAZ ="+cp_NullLink(cp_ToStrODBC(this.w_MVVALNAZ),'DOC_MAST','MVVALNAZ');
        +",MVCODVAL ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODVAL),'DOC_MAST','MVCODVAL');
        +",MVCAOVAL ="+cp_NullLink(cp_ToStrODBC(this.w_MVCAOVAL),'DOC_MAST','MVCAOVAL');
        +",MVSCOCL1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCOCL1),'DOC_MAST','MVSCOCL1');
        +",MVSCOCL2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCOCL2),'DOC_MAST','MVSCOCL2');
        +",MVSCOPAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCOPAG),'DOC_MAST','MVSCOPAG');
        +",MVALFEST ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVALFEST),'DOC_MAST','MVALFEST');
        +",MVPRP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVPRP),'DOC_MAST','MVPRP');
        +",MVANNPRO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVANNPRO),'DOC_MAST','MVANNPRO');
        +",MVNUMEST ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVNUMEST),'DOC_MAST','MVNUMEST');
        +",MVFLFOSC ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLFOSC),'DOC_MAST','MVFLFOSC');
        +",MVNOTAGG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVNOTAGG),'DOC_MAST','MVNOTAGG');
        +",MVFLCAPA ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVFLCAPA');
        +",MVNUMCOR ="+cp_NullLink(cp_ToStrODBC(this.w_MVNUMCOR),'DOC_MAST','MVNUMCOR');
            +i_ccchkf ;
        +" where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL);
               )
      else
        update (i_cTable) set;
            MVCODPAG = this.w_MVCODPAG;
            ,MVCODIVE = this.w_MVCODIVE;
            ,MVSCONTI = this.w_MVSCONTI;
            ,MVSPEINC = this.w_MVSPEINC;
            ,MVIVAINC = this.w_MVIVAINC;
            ,MVFLRINC = this.w_MVFLRINC;
            ,MVSPETRA = this.w_MVSPETRA;
            ,MVIVATRA = this.w_MVIVATRA;
            ,MVFLRTRA = this.w_MVFLRTRA;
            ,MVSPEIMB = this.w_MVSPEIMB;
            ,MVIVAIMB = this.w_MVIVAIMB;
            ,MVFLRIMB = this.w_MVFLRIMB;
            ,MVSPEBOL = this.w_MVSPEBOL;
            ,MVDATDIV = this.w_MVDATDIV;
            ,MVCODBAN = this.w_MVCODBAN;
            ,MVVALNAZ = this.w_MVVALNAZ;
            ,MVCODVAL = this.w_MVCODVAL;
            ,MVCAOVAL = this.w_MVCAOVAL;
            ,MVSCOCL1 = this.w_MVSCOCL1;
            ,MVSCOCL2 = this.w_MVSCOCL2;
            ,MVSCOPAG = this.w_MVSCOPAG;
            ,MVALFEST = this.oParentObject.w_MVALFEST;
            ,MVPRP = this.oParentObject.w_MVPRP;
            ,MVANNPRO = this.oParentObject.w_MVANNPRO;
            ,MVNUMEST = this.oParentObject.w_MVNUMEST;
            ,MVFLFOSC = this.w_MVFLFOSC;
            ,MVNOTAGG = this.oParentObject.w_MVNOTAGG;
            ,MVFLCAPA = " ";
            ,MVNUMCOR = this.w_MVNUMCOR;
            &i_ccchkf. ;
         where;
            MVSERIAL = this.oParentObject.w_MVSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Write into DOC_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVIMPARR ="+cp_NullLink(cp_ToStrODBC(this.w_MVIMPARR),'DOC_MAST','MVIMPARR');
        +",MVIVABOL ="+cp_NullLink(cp_ToStrODBC(this.w_MVIVABOL),'DOC_MAST','MVIVABOL');
        +",MVACCONT ="+cp_NullLink(cp_ToStrODBC(this.w_MVACCONT),'DOC_MAST','MVACCONT');
        +",UTCC ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'DOC_MAST','UTCC');
        +",UTCV ="+cp_NullLink(cp_ToStrODBC(0),'DOC_MAST','UTCV');
        +",UTDC ="+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'DOC_MAST','UTDC');
        +",UTDV ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'DOC_MAST','UTDV');
        +",MVACIVA1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA1),'DOC_MAST','MVACIVA1');
        +",MVACIVA2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA2),'DOC_MAST','MVACIVA2');
        +",MVACIVA3 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA3),'DOC_MAST','MVACIVA3');
        +",MVACIVA4 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA4),'DOC_MAST','MVACIVA4');
        +",MVACIVA5 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA5),'DOC_MAST','MVACIVA5');
        +",MVACIVA6 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA6),'DOC_MAST','MVACIVA6');
        +",MVAIMPN1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN1),'DOC_MAST','MVAIMPN1');
        +",MVAIMPN2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN2),'DOC_MAST','MVAIMPN2');
        +",MVAIMPN3 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN3),'DOC_MAST','MVAIMPN3');
        +",MVAIMPN4 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN4),'DOC_MAST','MVAIMPN4');
        +",MVAIMPN5 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN5),'DOC_MAST','MVAIMPN5');
        +",MVAIMPN6 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN6),'DOC_MAST','MVAIMPN6');
        +",MVAIMPS1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS1),'DOC_MAST','MVAIMPS1');
        +",MVAIMPS2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS2),'DOC_MAST','MVAIMPS2');
        +",MVAIMPS3 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS3),'DOC_MAST','MVAIMPS3');
        +",MVAIMPS4 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS4),'DOC_MAST','MVAIMPS4');
        +",MVAIMPS5 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS5),'DOC_MAST','MVAIMPS5');
        +",MVAIMPS6 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS6),'DOC_MAST','MVAIMPS6');
        +",MVAFLOM1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM1),'DOC_MAST','MVAFLOM1');
        +",MVAFLOM2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM2),'DOC_MAST','MVAFLOM2');
        +",MVAFLOM3 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM3),'DOC_MAST','MVAFLOM3');
        +",MVAFLOM4 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM4),'DOC_MAST','MVAFLOM4');
        +",MVAFLOM5 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM5),'DOC_MAST','MVAFLOM5');
        +",MVAFLOM6 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM6),'DOC_MAST','MVAFLOM6');
        +",MVACCPRE ="+cp_NullLink(cp_ToStrODBC(this.w_MVACCPRE),'DOC_MAST','MVACCPRE');
        +",MVCODBA2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODBA2),'DOC_MAST','MVCODBA2');
        +",MVRIFFAD ="+cp_NullLink(cp_ToStrODBC(this.w_MVRIFFAD),'DOC_MAST','MVRIFFAD');
        +",MVFLSCAF ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLSCAF),'DOC_MAST','MVFLSCAF');
        +",MVRIFODL ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'DOC_MAST','MVRIFODL');
        +",MVDESDOC ="+cp_NullLink(cp_ToStrODBC(this.w_MVDESDOC),'DOC_MAST','MVDESDOC');
        +",MVFLVABD ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLVABD),'DOC_MAST','MVFLVABD');
        +",MVFLSCOM ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLSCOM),'DOC_MAST','MVFLSCOM');
        +",MVIVACAU ="+cp_NullLink(cp_ToStrODBC(this.w_MVIVACAU),'DOC_MAST','MVIVACAU');
        +",MVCAUIMB ="+cp_NullLink(cp_ToStrODBC(this.w_MVCAUIMB),'DOC_MAST','MVCAUIMB');
        +",MVFLFOCA ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLFOCA),'DOC_MAST','MVFLFOCA');
        +",MVIMPFIN ="+cp_NullLink(cp_ToStrODBC(this.w_IMPFIN),'DOC_MAST','MVIMPFIN');
        +",MVFLSFIN ="+cp_NullLink(cp_ToStrODBC(this.w_FLSFIN),'DOC_MAST','MVFLSFIN');
        +",MVAGG_01 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAGG_01),'DOC_MAST','MVAGG_01');
        +",MVAGG_02 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAGG_02),'DOC_MAST','MVAGG_02');
        +",MVAGG_03 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAGG_03),'DOC_MAST','MVAGG_03');
        +",MVAGG_04 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAGG_04),'DOC_MAST','MVAGG_04');
        +",MVAGG_05 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAGG_05),'DOC_MAST','MVAGG_05');
        +",MVAGG_06 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAGG_06),'DOC_MAST','MVAGG_06');
            +i_ccchkf ;
        +" where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL);
               )
      else
        update (i_cTable) set;
            MVIMPARR = this.w_MVIMPARR;
            ,MVIVABOL = this.w_MVIVABOL;
            ,MVACCONT = this.w_MVACCONT;
            ,UTCC = i_CODUTE;
            ,UTCV = 0;
            ,UTDC = SetInfoDate( g_CALUTD );
            ,UTDV = cp_CharToDate("  -  -    ");
            ,MVACIVA1 = this.w_MVACIVA1;
            ,MVACIVA2 = this.w_MVACIVA2;
            ,MVACIVA3 = this.w_MVACIVA3;
            ,MVACIVA4 = this.w_MVACIVA4;
            ,MVACIVA5 = this.w_MVACIVA5;
            ,MVACIVA6 = this.w_MVACIVA6;
            ,MVAIMPN1 = this.w_MVAIMPN1;
            ,MVAIMPN2 = this.w_MVAIMPN2;
            ,MVAIMPN3 = this.w_MVAIMPN3;
            ,MVAIMPN4 = this.w_MVAIMPN4;
            ,MVAIMPN5 = this.w_MVAIMPN5;
            ,MVAIMPN6 = this.w_MVAIMPN6;
            ,MVAIMPS1 = this.w_MVAIMPS1;
            ,MVAIMPS2 = this.w_MVAIMPS2;
            ,MVAIMPS3 = this.w_MVAIMPS3;
            ,MVAIMPS4 = this.w_MVAIMPS4;
            ,MVAIMPS5 = this.w_MVAIMPS5;
            ,MVAIMPS6 = this.w_MVAIMPS6;
            ,MVAFLOM1 = this.w_MVAFLOM1;
            ,MVAFLOM2 = this.w_MVAFLOM2;
            ,MVAFLOM3 = this.w_MVAFLOM3;
            ,MVAFLOM4 = this.w_MVAFLOM4;
            ,MVAFLOM5 = this.w_MVAFLOM5;
            ,MVAFLOM6 = this.w_MVAFLOM6;
            ,MVACCPRE = this.w_MVACCPRE;
            ,MVCODBA2 = this.w_MVCODBA2;
            ,MVRIFFAD = this.w_MVRIFFAD;
            ,MVFLSCAF = this.w_MVFLSCAF;
            ,MVRIFODL = SPACE(10);
            ,MVDESDOC = this.w_MVDESDOC;
            ,MVFLVABD = this.w_MVFLVABD;
            ,MVFLSCOM = this.w_MVFLSCOM;
            ,MVIVACAU = this.w_MVIVACAU;
            ,MVCAUIMB = this.w_MVCAUIMB;
            ,MVFLFOCA = this.w_MVFLFOCA;
            ,MVIMPFIN = this.w_IMPFIN;
            ,MVFLSFIN = this.w_FLSFIN;
            ,MVAGG_01 = this.w_MVAGG_01;
            ,MVAGG_02 = this.w_MVAGG_02;
            ,MVAGG_03 = this.w_MVAGG_03;
            ,MVAGG_04 = this.w_MVAGG_04;
            ,MVAGG_05 = this.w_MVAGG_05;
            ,MVAGG_06 = this.w_MVAGG_06;
            &i_ccchkf. ;
         where;
            MVSERIAL = this.oParentObject.w_MVSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      if this.w_FLSILI="S" and this.w_TIPOPEDIC<>"D" And Not Empty(this.w_MVRIFDIC)
        this.w_NUMRIGA = 0
        this.w_RIFDIC = this.w_MVRIFDIC
        this.w_DIDICCOL_FILTRO = Space(10)
        * --- Inserisco un nuovo record nella tabella "Dettaglio dichiarazioni di intento"
        *     per tenere traccia dell'operazione. Devo risalire all'ultima riga inserita nella tabella
        * --- Select from DICDINTE9
        do vq_exec with 'DICDINTE9',this,'_Curs_DICDINTE9','',.f.,.t.
        if used('_Curs_DICDINTE9')
          select _Curs_DICDINTE9
          locate for 1=1
          do while not(eof())
          this.w_NUMRIGA = IIF(_Curs_DICDINTE9.ORDINA="A",_Curs_DICDINTE9.CPROWNUM,this.w_NUMRIGA)
            select _Curs_DICDINTE9
            continue
          enddo
          use
        endif
        this.w_NUMRIGA = this.w_NUMRIGA+1
        * --- Insert into DICDINTE
        i_nConn=i_TableProp[this.DICDINTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DICDINTE_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DICDINTE_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"DDSERIAL"+",CPROWNUM"+",CPROWORD"+",DDDOCSER"+",DDIMPDIC"+",DDTIPINS"+",DDDATOPE"+",DDTIPAGG"+",DDCODUTE"+",DDVISMAN"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_MVRIFDIC),'DICDINTE','DDSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA),'DICDINTE','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA*10),'DICDINTE','CPROWORD');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVSERIAL),'DICDINTE','DDDOCSER');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DDIMPUTI),'DICDINTE','DDIMPDIC');
          +","+cp_NullLink(cp_ToStrODBC("D"),'DICDINTE','DDTIPINS');
          +","+cp_NullLink(cp_ToStrODBC(i_datsys),'DICDINTE','DDDATOPE');
          +","+cp_NullLink(cp_ToStrODBC("I"),'DICDINTE','DDTIPAGG');
          +","+cp_NullLink(cp_ToStrODBC(i_codute),'DICDINTE','DDCODUTE');
          +","+cp_NullLink(cp_ToStrODBC("N"),'DICDINTE','DDVISMAN');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'DDSERIAL',this.w_MVRIFDIC,'CPROWNUM',this.w_NUMRIGA,'CPROWORD',this.w_NUMRIGA*10,'DDDOCSER',this.oParentObject.w_MVSERIAL,'DDIMPDIC',this.w_DDIMPUTI,'DDTIPINS',"D",'DDDATOPE',i_datsys,'DDTIPAGG',"I",'DDCODUTE',i_codute,'DDVISMAN',"N")
          insert into (i_cTable) (DDSERIAL,CPROWNUM,CPROWORD,DDDOCSER,DDIMPDIC,DDTIPINS,DDDATOPE,DDTIPAGG,DDCODUTE,DDVISMAN &i_ccchkf. );
             values (;
               this.w_MVRIFDIC;
               ,this.w_NUMRIGA;
               ,this.w_NUMRIGA*10;
               ,this.oParentObject.w_MVSERIAL;
               ,this.w_DDIMPUTI;
               ,"D";
               ,i_datsys;
               ,"I";
               ,i_codute;
               ,"N";
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        * --- Write into DIC_INTE
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DIC_INTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DIC_INTE_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"DIIMPUTI ="+cp_NullLink(cp_ToStrODBC(this.w_TOTUTI),'DIC_INTE','DIIMPUTI');
              +i_ccchkf ;
          +" where ";
              +"DISERIAL = "+cp_ToStrODBC(this.w_MVRIFDIC);
                 )
        else
          update (i_cTable) set;
              DIIMPUTI = this.w_TOTUTI;
              &i_ccchkf. ;
           where;
              DISERIAL = this.w_MVRIFDIC;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      * --- Cicla sulle Righe Documento
      ah_Msg("Generazione documento: %1",.T.,.F.,.F., ALLTRIM(STR(this.oParentObject.w_MVNUMDOC,15)))
      SELECT GeneApp
      GO TOP
      SCAN FOR t_MVTIPRIG<>" " AND NOT EMPTY(t_MVCODICE)
      * --- Scrive il Dettaglio
      this.w_MVTIPRIG = t_MVTIPRIG
      this.w_MVFLOMAG = t_MVFLOMAG
      this.w_MVCODICE = t_MVCODICE
      this.w_MVCODIVA = t_MVCODIVA
      this.w_MVCODART = t_MVCODART
      this.w_MVVALRIG = t_MVVALRIG
      this.w_MVDESART = t_MVDESART
      this.w_MVIMPACC = t_MVIMPACC
      this.w_MVDESSUP = t_MVDESSUP
      this.w_MVIMPSCO = t_MVIMPSCO
      this.w_MVUNIMIS = t_MVUNIMIS
      this.w_MVVALMAG = t_MVVALMAG
      this.w_MVCATCON = t_MVCATCON
      this.w_MVIMPNAZ = t_MVIMPNAZ
      this.w_MVCONTRO = t_MVCONTRO
      this.w_MVCODCLA = t_MVCODCLA
      this.w_MVPERPRO = t_MVPERPRO
      this.w_MVIMPPRO = t_MVIMPPRO
      this.w_MVCONTRA = t_MVCONTRA
      this.w_MVPESNET = t_MVPESNET
      this.w_MVNOMENC = t_MVNOMENC
      this.w_MVFLTRAS = t_MVFLTRAS
      this.w_MVNAZPRO = t_MVNAZPRO
      this.w_MVCODLIS = t_MVCODLIS
      this.w_MVPROORD = t_MVPROORD
      this.w_MVUMSUPP = t_MVUMSUPP
      this.w_MVQTAMOV = t_MVQTAMOV
      this.w_MVMOLSUP = t_MVMOLSUP
      this.w_MVQTAUM1 = t_MVQTAUM1
      this.w_MVNUMCOL = t_MVNUMCOL
      this.w_MVPREZZO = t_MVPREZZO
      this.w_MVSCONT1 = t_MVSCONT1
      this.w_MVSERRIF = t_MVSERRIF
      this.w_MVSCONT2 = t_MVSCONT2
      this.w_MVROWRIF = t_MVROWRIF
      this.w_MVSCONT3 = t_MVSCONT3
      this.w_MVIMPEVA = t_MVIMPEVA
      this.w_MVSCONT4 = t_MVSCONT4
      this.w_MVFLERIF = t_MVFLERIF
      this.w_MVCONIND = t_MVCONIND
      this.w_MVFLARIF = t_MVFLARIF
      this.w_MVDATEVA = IIF(this.oParentObject.w_FLDTPR="S", this.w_MVDATDOC, cp_CharToDate("  -  -  "))
      this.w_MVCODCEN = t_MVCODCEN
      this.w_MVVOCCEN = t_MVVOCCEN
      this.w_MVCODCOM = t_MVCODCOM
      this.w_MVCODATT = t_MVCODATT
      this.w_FLSERA = t_FLSERA
      this.w_MVTIPPRO = t_MVTIPPRO
      this.w_MVTIPPR2 = t_MVTIPPR2
      this.w_MVPROCAP = t_MVPROCAP
      this.w_MVIMPCAP = t_MVIMPCAP
      this.w_MVCODCES = t_MVCODCES
      this.w_MVIMPCOM = t_MVIMPCOM
      this.w_DOQTAEVA = t_DOQTAEVA
      this.w_DOIMPEVA = t_DOIMPEVA
      this.w_MVFLNOAN = t_MVFLNOAN
      this.w_MVINICOM = t_MVINICOM
      this.w_MVFINCOM = t_MVFINCOM
      this.w_NEXTFAT = NVL(t_ORAPDATEVA, CTOD(""))
      this.w_DATEVA = NVL(t_ORAPDATGEN, CTOD(""))
      this.w_TDORDAPE = NVL(t_TDORDAPE, " ")
      this.w_MVCODRES = t_MVCODRES
      * --- Se riga che esclude analitica non deve essere considerato movimento di analitica
      this.w_LFLELAN = IIF(this.w_MVFLNOAN="N",this.oParentObject.w_FLELAN," ")
      * --- Importo di Commessa
      if g_COMM="S" AND NOT EMPTY(this.w_MVCODCOM)
        * --- Legge Valuta di Commessa
        * --- Read from CAN_TIER
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAN_TIER_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CNCODVAL"+;
            " from "+i_cTable+" CAN_TIER where ";
                +"CNCODCAN = "+cp_ToStrODBC(this.w_MVCODCOM);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CNCODVAL;
            from (i_cTable) where;
                CNCODCAN = this.w_MVCODCOM;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_COCODVAL = NVL(cp_ToDate(_read_.CNCODVAL),cp_NullValue(_read_.CNCODVAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from VALUTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VALUTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VADECTOT"+;
            " from "+i_cTable+" VALUTE where ";
                +"VACODVAL = "+cp_ToStrODBC(this.w_COCODVAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VADECTOT;
            from (i_cTable) where;
                VACODVAL = this.w_COCODVAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DECCOM = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_MVIMPCOM = CAIMPCOM( IIF(Empty(this.w_MVCODCOM),"S", " "), this.w_MVCODVAL, this.w_COCODVAL, this.w_MVIMPNAZ, this.w_MVCAOVAL, this.w_MVDATDOC, this.w_DECCOM )
      endif
      * --- Contatore Righe inserite
      this.w_NURIG = this.w_NURIG+1
      this.w_CPROWNUM = this.w_CPROWNUM + 1
      * --- Riaprto sempre dal CPROWORD pi� alto (contributi accessori)
      this.w_MAXROW = Max( this.w_CPROWORD , this.w_MAXROW )
      this.w_CPROWORD = this.w_MAXROW + 10
      this.w_MVCACONT = NVL(t_MVCACONT, Space(5) )
      this.w_MVRIFCAC = Nvl( t_MVRIFCAC , 0 )
      if this.w_MVRIFCAC>0
        * --- Se contributo accessorio devo recuperare l'attuale riferimento, mi 
        *     baso su MVSERRIF e MVROWRIF, ricerco quindi il CPROWNUM
        *     della riga padre (sempre prima per costruzione)
        *     Se RIFCAC>0 potrebbe essere un contributo esploso sul documento di
        *     origine
        if Not Empty( Nvl(geneapp.t_FATHERRIF,"") )
          this.w_FATHERRIF = geneapp.t_FATHERRIF
        else
          this.w_FATHERRIF = this.w_MVSERRIF
        endif
        * --- Read from DOC_DETT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CPROWNUM,CPROWORD"+;
            " from "+i_cTable+" DOC_DETT where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL);
                +" and MVSERRIF = "+cp_ToStrODBC(this.w_FATHERRIF);
                +" and MVROWRIF = "+cp_ToStrODBC(this.w_MVRIFCAC);
                +" and MVNUMRIF = "+cp_ToStrODBC(this.w_MVNUMRIF);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CPROWNUM,CPROWORD;
            from (i_cTable) where;
                MVSERIAL = this.oParentObject.w_MVSERIAL;
                and MVSERRIF = this.w_FATHERRIF;
                and MVROWRIF = this.w_MVRIFCAC;
                and MVNUMRIF = this.w_MVNUMRIF;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MVRIFCAC = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
          this.w_LROWORD = NVL(cp_ToDate(_read_.CPROWORD),cp_NullValue(_read_.CPROWORD))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if Not Empty( Nvl( GeneApp.t_CPROWORD , 0 ) ) and this.w_LROWORD<>0
          this.w_CPROWORD = this.w_LROWORD + Nvl( GeneApp.t_CPROWORD , 0 )
        endif
      endif
      * --- Riferimento tipo Riga -90=Raggruppata , altrimenti -20
      this.w_MVNUMRIF = IIF(this.oParentObject.w_FLRAGG="S" AND this.w_MVTIPRIG $ "RM", -90, -20)
      * --- MVFLARIF vale '-' per le Fatture di Acconto; ' ' per le righe raggruppate (non deve stornare nulla) altrimenti '+'
      this.w_MVFLARIF = IIF(this.w_MVNUMRIF = -90, " ", this.w_MVFLARIF)
      * --- Se la riga � descrittiva non devo memorizzare quantit� importata per evitare 
      *     storno negativo  in fase di cancellazione documenti.
      this.w_QTAIMP = IIF(this.w_MVTIPRIG="D",0,this.w_MVQTAMOV)
      this.w_QTAIM1 = IIF(this.w_MVTIPRIG="D",0,this.w_MVQTAUM1)
      * --- Insert into DOC_DETT
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOC_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"MVTIPATT"+",MVNUMRIF"+",CPROWNUM"+",CPROWORD"+",MVCATCON"+",MVCAUMAG"+",MVCODART"+",MVCODCEN"+",MVCODCLA"+",MVCODCOM"+",MVCODICE"+",MVCODIVA"+",MVCODLIS"+",MVCONIND"+",MVCONTRA"+",MVCONTRO"+",MVDESART"+",MVDESSUP"+",MVFINCOM"+",MVFLARIF"+",MVFLERIF"+",MVFLOMAG"+",MVFLTRAS"+",MVIMPACC"+",MVIMPCOM"+",MVIMPNAZ"+",MVIMPPRO"+",MVIMPSCO"+",MVINICOM"+",MVMOLSUP"+",MVNAZPRO"+",MVNOMENC"+",MVNUMCOL"+",MVPERPRO"+",MVPESNET"+",MVPREZZO"+",MVPROORD"+",MVQTAMOV"+",MVQTASAL"+",MVQTAUM1"+",MVROWRIF"+",MVSCONT1"+",MVSCONT2"+",MVSCONT3"+",MVSCONT4"+",MVSERIAL"+",MVSERRIF"+",MVFLRAGG"+",MVTIPRIG"+",MVUNIMIS"+",MVVALMAG"+",MVVALRIG"+",MVVOCCEN"+",MVFLLOTT"+",MVF2LOTT"+",MVQTAIMP"+",MVQTAIM1"+",MVTIPPRO"+",MVCODCES"+",MVFLELAN"+",MV_SEGNO"+",MVTIPPR2"+",MVPROCAP"+",MVIMPCAP"+",MVFLNOAN"+",MVCODRES"+",MVRIFCAC"+",MVCACONT"+",MVCODATT"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("A"),'DOC_DETT','MVTIPATT');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVNUMRIF),'DOC_DETT','MVNUMRIF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'DOC_DETT','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'DOC_DETT','CPROWORD');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVCATCON),'DOC_DETT','MVCATCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVCAUMAG),'DOC_DETT','MVCAUMAG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODART),'DOC_DETT','MVCODART');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODCEN),'DOC_DETT','MVCODCEN');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODCLA),'DOC_DETT','MVCODCLA');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODCOM),'DOC_DETT','MVCODCOM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODICE),'DOC_DETT','MVCODICE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODIVA),'DOC_DETT','MVCODIVA');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODLIS),'DOC_DETT','MVCODLIS');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVCONIND),'DOC_DETT','MVCONIND');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVCONTRA),'DOC_DETT','MVCONTRA');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVCONTRO),'DOC_DETT','MVCONTRO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVDESART),'DOC_DETT','MVDESART');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVDESSUP),'DOC_DETT','MVDESSUP');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVFINCOM),'DOC_DETT','MVFINCOM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLARIF),'DOC_DETT','MVFLARIF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLERIF),'DOC_DETT','MVFLERIF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLOMAG),'DOC_DETT','MVFLOMAG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLTRAS),'DOC_DETT','MVFLTRAS');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVIMPACC),'DOC_DETT','MVIMPACC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVIMPCOM),'DOC_DETT','MVIMPCOM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVIMPNAZ),'DOC_DETT','MVIMPNAZ');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVIMPPRO),'DOC_DETT','MVIMPPRO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVIMPSCO),'DOC_DETT','MVIMPSCO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVINICOM),'DOC_DETT','MVINICOM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVMOLSUP),'DOC_DETT','MVMOLSUP');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVNAZPRO),'DOC_DETT','MVNAZPRO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVNOMENC),'DOC_DETT','MVNOMENC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVNUMCOL),'DOC_DETT','MVNUMCOL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVPERPRO),'DOC_DETT','MVPERPRO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVPESNET),'DOC_DETT','MVPESNET');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVPREZZO),'DOC_DETT','MVPREZZO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVPROORD),'DOC_DETT','MVPROORD');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAMOV),'DOC_DETT','MVQTAMOV');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAUM1),'DOC_DETT','MVQTASAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAUM1),'DOC_DETT','MVQTAUM1');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVROWRIF),'DOC_DETT','MVROWRIF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT1),'DOC_DETT','MVSCONT1');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT2),'DOC_DETT','MVSCONT2');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT3),'DOC_DETT','MVSCONT3');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT4),'DOC_DETT','MVSCONT4');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVSERIAL),'DOC_DETT','MVSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVSERRIF),'DOC_DETT','MVSERRIF');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVTFRAGG),'DOC_DETT','MVFLRAGG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVTIPRIG),'DOC_DETT','MVTIPRIG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVUNIMIS),'DOC_DETT','MVUNIMIS');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVVALMAG),'DOC_DETT','MVVALMAG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVVALRIG),'DOC_DETT','MVVALRIG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVVOCCEN),'DOC_DETT','MVVOCCEN');
        +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLLOTT');
        +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVF2LOTT');
        +","+cp_NullLink(cp_ToStrODBC(this.w_QTAIMP),'DOC_DETT','MVQTAIMP');
        +","+cp_NullLink(cp_ToStrODBC(this.w_QTAIM1),'DOC_DETT','MVQTAIM1');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVTIPPRO),'DOC_DETT','MVTIPPRO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODCES),'DOC_DETT','MVCODCES');
        +","+cp_NullLink(cp_ToStrODBC(this.w_LFLELAN),'DOC_DETT','MVFLELAN');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MV_SEGNO),'DOC_DETT','MV_SEGNO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVTIPPR2),'DOC_DETT','MVTIPPR2');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVPROCAP),'DOC_DETT','MVPROCAP');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVIMPCAP),'DOC_DETT','MVIMPCAP');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLNOAN),'DOC_DETT','MVFLNOAN');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODRES),'DOC_DETT','MVCODRES');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVRIFCAC),'DOC_DETT','MVRIFCAC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVCACONT ),'DOC_DETT','MVCACONT');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODATT),'DOC_DETT','MVCODATT');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'MVTIPATT',"A",'MVNUMRIF',this.w_MVNUMRIF,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'MVCATCON',this.w_MVCATCON,'MVCAUMAG',this.w_MVCAUMAG,'MVCODART',this.w_MVCODART,'MVCODCEN',this.w_MVCODCEN,'MVCODCLA',this.w_MVCODCLA,'MVCODCOM',this.w_MVCODCOM,'MVCODICE',this.w_MVCODICE,'MVCODIVA',this.w_MVCODIVA)
        insert into (i_cTable) (MVTIPATT,MVNUMRIF,CPROWNUM,CPROWORD,MVCATCON,MVCAUMAG,MVCODART,MVCODCEN,MVCODCLA,MVCODCOM,MVCODICE,MVCODIVA,MVCODLIS,MVCONIND,MVCONTRA,MVCONTRO,MVDESART,MVDESSUP,MVFINCOM,MVFLARIF,MVFLERIF,MVFLOMAG,MVFLTRAS,MVIMPACC,MVIMPCOM,MVIMPNAZ,MVIMPPRO,MVIMPSCO,MVINICOM,MVMOLSUP,MVNAZPRO,MVNOMENC,MVNUMCOL,MVPERPRO,MVPESNET,MVPREZZO,MVPROORD,MVQTAMOV,MVQTASAL,MVQTAUM1,MVROWRIF,MVSCONT1,MVSCONT2,MVSCONT3,MVSCONT4,MVSERIAL,MVSERRIF,MVFLRAGG,MVTIPRIG,MVUNIMIS,MVVALMAG,MVVALRIG,MVVOCCEN,MVFLLOTT,MVF2LOTT,MVQTAIMP,MVQTAIM1,MVTIPPRO,MVCODCES,MVFLELAN,MV_SEGNO,MVTIPPR2,MVPROCAP,MVIMPCAP,MVFLNOAN,MVCODRES,MVRIFCAC,MVCACONT,MVCODATT &i_ccchkf. );
           values (;
             "A";
             ,this.w_MVNUMRIF;
             ,this.w_CPROWNUM;
             ,this.w_CPROWORD;
             ,this.w_MVCATCON;
             ,this.w_MVCAUMAG;
             ,this.w_MVCODART;
             ,this.w_MVCODCEN;
             ,this.w_MVCODCLA;
             ,this.w_MVCODCOM;
             ,this.w_MVCODICE;
             ,this.w_MVCODIVA;
             ,this.w_MVCODLIS;
             ,this.w_MVCONIND;
             ,this.w_MVCONTRA;
             ,this.w_MVCONTRO;
             ,this.w_MVDESART;
             ,this.w_MVDESSUP;
             ,this.w_MVFINCOM;
             ,this.w_MVFLARIF;
             ,this.w_MVFLERIF;
             ,this.w_MVFLOMAG;
             ,this.w_MVFLTRAS;
             ,this.w_MVIMPACC;
             ,this.w_MVIMPCOM;
             ,this.w_MVIMPNAZ;
             ,this.w_MVIMPPRO;
             ,this.w_MVIMPSCO;
             ,this.w_MVINICOM;
             ,this.w_MVMOLSUP;
             ,this.w_MVNAZPRO;
             ,this.w_MVNOMENC;
             ,this.w_MVNUMCOL;
             ,this.w_MVPERPRO;
             ,this.w_MVPESNET;
             ,this.w_MVPREZZO;
             ,this.w_MVPROORD;
             ,this.w_MVQTAMOV;
             ,this.w_MVQTAUM1;
             ,this.w_MVQTAUM1;
             ,this.w_MVROWRIF;
             ,this.w_MVSCONT1;
             ,this.w_MVSCONT2;
             ,this.w_MVSCONT3;
             ,this.w_MVSCONT4;
             ,this.oParentObject.w_MVSERIAL;
             ,this.w_MVSERRIF;
             ,this.oParentObject.w_MVTFRAGG;
             ,this.w_MVTIPRIG;
             ,this.w_MVUNIMIS;
             ,this.w_MVVALMAG;
             ,this.w_MVVALRIG;
             ,this.w_MVVOCCEN;
             ," ";
             ," ";
             ,this.w_QTAIMP;
             ,this.w_QTAIM1;
             ,this.w_MVTIPPRO;
             ,this.w_MVCODCES;
             ,this.w_LFLELAN;
             ,this.oParentObject.w_MV_SEGNO;
             ,this.w_MVTIPPR2;
             ,this.w_MVPROCAP;
             ,this.w_MVIMPCAP;
             ,this.w_MVFLNOAN;
             ,this.w_MVCODRES;
             ,this.w_MVRIFCAC;
             ,this.w_MVCACONT ;
             ,this.w_MVCODATT;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      if this.w_MVNUMRIF=-90 AND USED("AppRagg")
        * --- Memorizza il Riferimento Riga della Fattura Differita
        SELECT GeneApp
        this.w_APPO2 = PROGRIGA
        UPDATE AppRagg SET RIFROW=this.w_CPROWNUM WHERE RIGRIF=this.w_APPO2
      endif
      if this.w_MVNUMRIF=-20
        * --- controllo Multi Utenza solo se fatturazione Normale. Non Se Raggruppata.
        *     In questo caso viene gestito in Pi� in Basso. Prima della scrittura in Rag_Fatt
        this.w_MULSER = this.w_MVSERRIF
        this.w_MULROW = this.w_MVROWRIF
        this.w_MULRIF = this.w_MVNUMRIF
        * --- Controllo Evasione Multiutenza
        this.Page_10()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Scrive Data Generazione sul Documento Origine
      do case
        case this.w_MVFLARIF="-"
          if NOT EMPTY(this.w_MVSERRIF)
            this.w_APPO = SPACE(3)
            * --- Read from DOC_MAST
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.DOC_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "MVCODVAL"+;
                " from "+i_cTable+" DOC_MAST where ";
                    +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERRIF);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                MVCODVAL;
                from (i_cTable) where;
                    MVSERIAL = this.w_MVSERRIF;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_APPO = NVL(cp_ToDate(_read_.MVCODVAL),cp_NullValue(_read_.MVCODVAL))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Se FAttura di Acconto in altra Valuta converte l'importo evaso alla valuta di del Doc. di Origine
            if this.w_MVCODVAL<>this.w_APPO AND NOT EMPTY(this.w_APPO)
              this.w_APPO1 = 0
              this.w_APPO2 = 0
              this.w_APPO1 = GETCAM(this.w_APPO, this.w_MVDATDOC)
              this.w_APPO2 = GETVALUT(this.w_APPO, "VADECUNI")
              * --- Converte in Euro
              this.w_MVPREZZO = VAL2MON(this.w_MVPREZZO, this.w_APPO1,1, this.w_MVDATDOC, this.w_APPO2)
            endif
            * --- Aggiorno l'evasione sulla riga della Fattura di Acconto
            * --- Write into DOC_DETT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.DOC_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLERIF),'DOC_DETT','MVFLEVAS');
              +",MVIMPEVA =MVIMPEVA- "+cp_ToStrODBC(this.w_MVPREZZO);
              +",MVEFFEVA ="+cp_NullLink(cp_ToStrODBC(this.w_MVDATDOC),'DOC_DETT','MVEFFEVA');
              +",MVQTAEVA ="+cp_NullLink(cp_ToStrODBC(this.w_MVQTAMOV),'DOC_DETT','MVQTAEVA');
              +",MVQTAEV1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVQTAUM1),'DOC_DETT','MVQTAEV1');
                  +i_ccchkf ;
              +" where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERRIF);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_MVROWRIF);
                  +" and MVNUMRIF = "+cp_ToStrODBC(this.w_MVNUMRIF);
                     )
            else
              update (i_cTable) set;
                  MVFLEVAS = this.w_MVFLERIF;
                  ,MVIMPEVA = MVIMPEVA - this.w_MVPREZZO;
                  ,MVEFFEVA = this.w_MVDATDOC;
                  ,MVQTAEVA = this.w_MVQTAMOV;
                  ,MVQTAEV1 = this.w_MVQTAUM1;
                  &i_ccchkf. ;
               where;
                  MVSERIAL = this.w_MVSERRIF;
                  and CPROWNUM = this.w_MVROWRIF;
                  and MVNUMRIF = this.w_MVNUMRIF;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        case this.w_MVFLARIF="+" AND this.w_MVNUMRIF=-20
          * --- Storno saldi
          * --- Read from DOC_DETT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DOC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MVCODMAG,MVFLRISE,MVFLORDI,MVFLIMPE,MVCODMAT,MVF2RISE,MVF2ORDI,MVF2IMPE,MVQTASAL,MVKEYSAL,MVCODART,MVCODCOM"+;
              " from "+i_cTable+" DOC_DETT where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERRIF);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_MVROWRIF);
                  +" and MVNUMRIF = "+cp_ToStrODBC(this.w_MVNUMRIF);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MVCODMAG,MVFLRISE,MVFLORDI,MVFLIMPE,MVCODMAT,MVF2RISE,MVF2ORDI,MVF2IMPE,MVQTASAL,MVKEYSAL,MVCODART,MVCODCOM;
              from (i_cTable) where;
                  MVSERIAL = this.w_MVSERRIF;
                  and CPROWNUM = this.w_MVROWRIF;
                  and MVNUMRIF = this.w_MVNUMRIF;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_MVCODMAG = NVL(cp_ToDate(_read_.MVCODMAG),cp_NullValue(_read_.MVCODMAG))
            this.w_MVFLRISE = NVL(cp_ToDate(_read_.MVFLRISE),cp_NullValue(_read_.MVFLRISE))
            this.w_MVFLORDI = NVL(cp_ToDate(_read_.MVFLORDI),cp_NullValue(_read_.MVFLORDI))
            this.w_MVFLIMPE = NVL(cp_ToDate(_read_.MVFLIMPE),cp_NullValue(_read_.MVFLIMPE))
            this.w_MVCODMAT = NVL(cp_ToDate(_read_.MVCODMAT),cp_NullValue(_read_.MVCODMAT))
            this.w_MVF2RISE = NVL(cp_ToDate(_read_.MVF2RISE),cp_NullValue(_read_.MVF2RISE))
            this.w_MVF2ORDI = NVL(cp_ToDate(_read_.MVF2ORDI),cp_NullValue(_read_.MVF2ORDI))
            this.w_MVF2IMPE = NVL(cp_ToDate(_read_.MVF2IMPE),cp_NullValue(_read_.MVF2IMPE))
            this.w_MVQTASAL = NVL(cp_ToDate(_read_.MVQTASAL),cp_NullValue(_read_.MVQTASAL))
            this.w_MVKEYSAL = NVL(cp_ToDate(_read_.MVKEYSAL),cp_NullValue(_read_.MVKEYSAL))
            this.w_CODART = NVL(cp_ToDate(_read_.MVCODART),cp_NullValue(_read_.MVCODART))
            this.w_CODCOM = NVL(cp_ToDate(_read_.MVCODCOM),cp_NullValue(_read_.MVCODCOM))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if Not Empty(this.w_MVFLRISE) Or Not Empty(this.w_MVF2RISE)
            * --- Documento non Accettato (movimenta riservato)
            * --- Documento non Accettato (movimenta riservato)
            this.w_oERRORLOG.AddMsgLogPartNoTrans(space(5), "%0Bloccante:%0")     
            this.w_oERRORLOG.AddMsgLog("Impossibile generare doc.: %1 n.: %2 del %3%0Movimenta il riservato", this.w_TIPDOC, Alltrim(STR(this.w_NDOC,15))+ IIF(Not Empty(this.w_ADOC),"/"+Alltrim(this.w_ADOC),""), DTOC(this.w_DDOC))     
            * --- Raise
            i_Error="Errore"
            return
          endif
          if NOT EMPTY(this.w_MVFLORDI+this.w_MVFLIMPE+this.w_MVF2ORDI+this.w_MVF2IMPE)
            * --- Aggiorno saldi MVCODMAG
            this.w_FLIMPE = ICASE(this.w_MVFLIMPE="+","-", this.w_MVFLIMPE="-","+"," ")
            this.w_FLORDI = ICASE(this.w_MVFLORDI="+","-", this.w_MVFLORDI="-","+"," ")
            * --- Write into SALDIART
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDIART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_FLORDI,'SLQTOPER','this.w_MVQTASAL',this.w_MVQTASAL,'update',i_nConn)
              i_cOp2=cp_SetTrsOp(this.w_FLIMPE,'SLQTIPER','this.w_MVQTASAL',this.w_MVQTASAL,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
              +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
                  +i_ccchkf ;
              +" where ";
                  +"SLCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
                  +" and SLCODMAG = "+cp_ToStrODBC(this.w_MVCODMAG);
                     )
            else
              update (i_cTable) set;
                  SLQTOPER = &i_cOp1.;
                  ,SLQTIPER = &i_cOp2.;
                  &i_ccchkf. ;
               where;
                  SLCODICE = this.w_MVKEYSAL;
                  and SLCODMAG = this.w_MVCODMAG;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            * --- Read from ART_ICOL
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ART_ICOL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ARSALCOM"+;
                " from "+i_cTable+" ART_ICOL where ";
                    +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ARSALCOM;
                from (i_cTable) where;
                    ARCODART = this.w_CODART;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if this.w_SALCOM="S"
              if empty(nvl(this.w_CODCOM,""))
                this.w_COMMAPPO = this.w_COMMDEFA
              else
                this.w_COMMAPPO = this.w_CODCOM
              endif
              * --- Write into SALDICOM
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDICOM_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                i_cOp1=cp_SetTrsOp(this.w_FLORDI,'SCQTOPER','this.w_MVQTASAL',this.w_MVQTASAL,'update',i_nConn)
                i_cOp2=cp_SetTrsOp(this.w_FLIMPE,'SCQTIPER','this.w_MVQTASAL',this.w_MVQTASAL,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
                +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
                    +i_ccchkf ;
                +" where ";
                    +"SCCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
                    +" and SCCODMAG = "+cp_ToStrODBC(this.w_MVCODMAG);
                    +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                       )
              else
                update (i_cTable) set;
                    SCQTOPER = &i_cOp1.;
                    ,SCQTIPER = &i_cOp2.;
                    &i_ccchkf. ;
                 where;
                    SCCODICE = this.w_MVKEYSAL;
                    and SCCODMAG = this.w_MVCODMAG;
                    and SCCODCAN = this.w_COMMAPPO;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore Aggiornamento Saldi Commessa'
                return
              endif
            endif
            * --- Aggiorno saldi MVCODMAT
            this.w_FLIMPE = ICASE(this.w_MVF2IMPE="+","-", this.w_MVF2IMPE="-","+"," ")
            this.w_FLORDI = ICASE(this.w_MVF2ORDI="+","-", this.w_MVF2ORDI="-","+"," ")
            * --- Write into SALDIART
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDIART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_FLORDI,'SLQTOPER','this.w_MVQTASAL',this.w_MVQTASAL,'update',i_nConn)
              i_cOp2=cp_SetTrsOp(this.w_FLIMPE,'SLQTIPER','this.w_MVQTASAL',this.w_MVQTASAL,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
              +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
                  +i_ccchkf ;
              +" where ";
                  +"SLCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
                  +" and SLCODMAG = "+cp_ToStrODBC(this.w_MVCODMAT);
                     )
            else
              update (i_cTable) set;
                  SLQTOPER = &i_cOp1.;
                  ,SLQTIPER = &i_cOp2.;
                  &i_ccchkf. ;
               where;
                  SLCODICE = this.w_MVKEYSAL;
                  and SLCODMAG = this.w_MVCODMAT;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            if this.w_SALCOM="S"
              * --- Write into SALDICOM
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDICOM_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                i_cOp1=cp_SetTrsOp(this.w_FLORDI,'SCQTOPER','this.w_MVQTASAL',this.w_MVQTASAL,'update',i_nConn)
                i_cOp2=cp_SetTrsOp(this.w_FLIMPE,'SCQTIPER','this.w_MVQTASAL',this.w_MVQTASAL,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
                +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
                    +i_ccchkf ;
                +" where ";
                    +"SCCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
                    +" and SCCODMAG = "+cp_ToStrODBC(this.w_MVCODMAT);
                    +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                       )
              else
                update (i_cTable) set;
                    SCQTOPER = &i_cOp1.;
                    ,SCQTIPER = &i_cOp2.;
                    &i_ccchkf. ;
                 where;
                    SCCODICE = this.w_MVKEYSAL;
                    and SCCODMAG = this.w_MVCODMAT;
                    and SCCODCAN = this.w_COMMAPPO;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore Aggiornamento Saldi Commessa'
                return
              endif
            endif
            this.w_MVQTASAL = 0
          endif
          * --- Scrive Data Generazione sul Documento Origine
          * --- Tenere presente che La Fattura Differita non Aggiorna le Evasioni (Qta o Valore)
          * --- anche perche' non e' piu' possibile evaderla dai Documenti (Filtro su MVDATGEN)
          * --- Se riga forfettaria aggiorno importo evaso altrimenti sommo 0
          this.w_PRZEVA = IIF( this.w_MVTIPRIG="F" , this.w_MVPREZZO , 0 ) 
          * --- Write into DOC_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DOC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVDATGEN ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVDATREG),'DOC_DETT','MVDATGEN');
            +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_DETT','MVFLEVAS');
            +",MVQTAEVA =MVQTAEVA+ "+cp_ToStrODBC(this.w_MVQTAMOV);
            +",MVQTAEV1 =MVQTAEV1+ "+cp_ToStrODBC(this.w_MVQTAUM1);
            +",MVIMPEVA =MVIMPEVA+ "+cp_ToStrODBC(this.w_PRZEVA);
            +",MVQTASAL ="+cp_NullLink(cp_ToStrODBC(this.w_MVQTASAL),'DOC_DETT','MVQTASAL');
                +i_ccchkf ;
            +" where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERRIF);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_MVROWRIF);
                +" and MVNUMRIF = "+cp_ToStrODBC(this.w_MVNUMRIF);
                   )
          else
            update (i_cTable) set;
                MVDATGEN = this.oParentObject.w_MVDATREG;
                ,MVFLEVAS = "S";
                ,MVQTAEVA = MVQTAEVA + this.w_MVQTAMOV;
                ,MVQTAEV1 = MVQTAEV1 + this.w_MVQTAUM1;
                ,MVIMPEVA = MVIMPEVA + this.w_PRZEVA;
                ,MVQTASAL = this.w_MVQTASAL;
                &i_ccchkf. ;
             where;
                MVSERIAL = this.w_MVSERRIF;
                and CPROWNUM = this.w_MVROWRIF;
                and MVNUMRIF = this.w_MVNUMRIF;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        otherwise
          * --- Ordine aperto, aggiorno data prevista evasione
          if this.w_MVNUMRIF=-20 AND this.w_TDORDAPE="S"
            * --- Write into DOC_DETT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.DOC_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MVDATEVA ="+cp_NullLink(cp_ToStrODBC(this.w_NEXTFAT),'DOC_DETT','MVDATEVA');
              +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(IIF(EMPTY(this.w_NEXTFAT), "S", " ")),'DOC_DETT','MVFLEVAS');
              +",MVDATGEN ="+cp_NullLink(cp_ToStrODBC(this.w_DATEVA),'DOC_DETT','MVDATGEN');
                  +i_ccchkf ;
              +" where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERRIF);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_MVROWRIF);
                  +" and MVNUMRIF = "+cp_ToStrODBC(this.w_MVNUMRIF);
                     )
            else
              update (i_cTable) set;
                  MVDATEVA = this.w_NEXTFAT;
                  ,MVFLEVAS = IIF(EMPTY(this.w_NEXTFAT), "S", " ");
                  ,MVDATGEN = this.w_DATEVA;
                  &i_ccchkf. ;
               where;
                  MVSERIAL = this.w_MVSERRIF;
                  and CPROWNUM = this.w_MVROWRIF;
                  and MVNUMRIF = this.w_MVNUMRIF;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
      endcase
      * --- Rivalorizzo il DDT
      if this.oParentObject.w_FLRAGG <>"S" AND this.oParentObject.w_PRZDES="S"
        RivalDoc(this.oParentObject.w_MVSERIAL, this.w_CPROWNUM, this.w_MVSERRIF, this.w_MVROWRIF, this.w_MVNUMRIF)
      endif
      * --- Warning di riga
      if this.w_MVTIPRIG<>"D"
        if Empty(this.w_MVCODCEN) AND g_PERCCR="S" AND this.oParentObject.w_FLANAL="S" AND this.w_MVFLNOAN="N"
          this.w_oERRORLOG.AddMsgLogPartNoTrans(space(5), "%0Warning:%0")     
          this.w_oERRORLOG.AddMsgLog("Attenzione riga  %1 del documento n. %2 del %3 senza centro di costo", Alltrim(str(this.w_CPROWORD)), Alltrim(STR(this.oParentObject.w_MVNUMDOC,15,0))+" "+Alltrim(this.oParentObject.w_MVALFDOC), DTOC(this.w_MVDATDOC))     
          this.w_NWARN = this.w_NWARN + 1
        endif
        if Empty(this.w_MVVOCCEN) AND ((g_COMM="S" AND this.oParentObject.w_FLGCOM="S") OR (g_PERCCR="S" AND this.oParentObject.w_FLANAL="S" AND this.w_MVFLNOAN="N")) 
          this.w_oERRORLOG.AddMsgLogPartNoTrans(space(5), "%0Warning:%0")     
          this.w_oERRORLOG.AddMsgLog("Attenzione riga  %1 del documento n. %2 del %3 senza voce di costo", Alltrim(str(this.w_CPROWORD)), Alltrim(STR(this.oParentObject.w_MVNUMDOC,15,0))+" "+Alltrim(this.oParentObject.w_MVALFDOC), DTOC(this.w_MVDATDOC))     
          this.w_NWARN = this.w_NWARN + 1
        endif
        if Empty(this.w_MVCODCOM) AND ((g_COMM="S" AND this.oParentObject.w_FLGCOM="S") OR (g_PERCAN="S" AND this.oParentObject.w_FLANAL="S" AND this.w_MVFLNOAN="N")) AND this.w_MVTIPRIG<>"D"
          this.w_oERRORLOG.AddMsgLogPartNoTrans(space(5), "%0Warning:%0")     
          this.w_oERRORLOG.AddMsgLog("Attenzione riga  %1 del documento n. %2 del %3 senza commessa", Alltrim(str(this.w_CPROWORD)), Alltrim(STR(this.oParentObject.w_MVNUMDOC,15,0))+" "+Alltrim(this.oParentObject.w_MVALFDOC), DTOC(this.w_MVDATDOC))     
          this.w_NWARN = this.w_NWARN + 1
        endif
        if Empty(this.w_MVCODATT) AND g_COMM="S" AND this.oParentObject.w_FLGCOM="S"
          this.w_oERRORLOG.AddMsgLogPartNoTrans(space(5), "%0Warning:%0")     
          this.w_oERRORLOG.AddMsgLog("Attenzione riga  %1 del documento n. %2 del %3 senza attivit�", Alltrim(str(this.w_CPROWORD)), Alltrim(STR(this.oParentObject.w_MVNUMDOC,15,0))+" "+Alltrim(this.oParentObject.w_MVALFDOC), DTOC(this.w_MVDATDOC))     
          this.w_NWARN = this.w_NWARN + 1
        endif
      endif
      SELECT GeneApp
      ENDSCAN
      if this.w_NURIG=0
        * --- Fattura senza righe...
        this.w_oERRORLOG.AddMsgLogPartNoTrans(space(5), "%0Bloccante:%0")     
        this.w_oERRORLOG.AddMsgLog("Impossibile generare fattura senza righe.")     
        * --- Raise
        i_Error="Errore"
        return
      else
        this.Page_7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_ERRRAT
          this.w_oERRORLOG.AddMsgLogPartNoTrans(space(5), "%0Warning:%0")     
          this.w_oERRORLOG.AddMsgLog("Fattura differita n.: %1 del %2", Alltrim(STR(this.oParentObject.w_MVNUMDOC,15,0))+" "+Alltrim(this.oParentObject.w_MVALFDOC), DTOC(this.w_MVDATDOC))     
          this.w_oERRORLOG.AddMsgLog("Importi rate scadenze incongruenti con il totale documento%0Allineare manualmente gli importi scadenze con il totale documento")     
          this.w_NWARN = this.w_NWARN + 1
        endif
      endif
      if this.w_RICTOT
        * --- Ricalcola totali perch� alcune righe non sono state portate sul documento
        * --- w_Rictot � stato attivato se non � selezionato
        *     il flag 'Ripartisce contributi accessori' della causale documento.
        do GSAR_BRD with this, this.oParentObject.w_MVSERIAL,.F.,iif(this.w_MVFLSCAF="S" and this.oParentObject.w_PRZDES<>"S", .T.,.F.),.F.,.F.,.T.
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Select from DOC_DETT
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select CPROWNUM, MVSERRIF, MVROWRIF, MVNUMRIF  from "+i_cTable+" DOC_DETT ";
              +" where MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL)+"";
               ,"_Curs_DOC_DETT")
        else
          select CPROWNUM, MVSERRIF, MVROWRIF, MVNUMRIF from (i_cTable);
           where MVSERIAL = this.oParentObject.w_MVSERIAL;
            into cursor _Curs_DOC_DETT
        endif
        if used('_Curs_DOC_DETT')
          select _Curs_DOC_DETT
          locate for 1=1
          do while not(eof())
          * --- Rivalorizza i DDT
          this.w_CPROWNUM = CPROWNUM
          this.w_MVSERRIF = MVSERRIF
          this.w_MVROWRIF = MVROWRIF
          this.w_MVNUMRIF = MVNUMRIF
          RivalDoc(this.oParentObject.w_MVSERIAL, this.w_CPROWNUM, this.w_MVSERRIF, this.w_MVROWRIF, this.w_MVNUMRIF)
            select _Curs_DOC_DETT
            continue
          enddo
          use
        endif
        this.w_RICTOT = .F.
      endif
      if g_PROGEN= "S"
        this.w_CODAZI = i_CODAZI
        this.w_SERPRO = this.oParentObject.w_MVSERIAL
        this.w_MASSGEN = "S"
        * --- Considero sempre come se precedentemente avesi avuto disattivo
        *     In questo modo mi aggiorna il documento generato nel modo corretto
        this.w_OLDCALPRO = "DI"
        * --- Read from PAR_PROV
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_PROV_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROV_idx,2],.t.,this.PAR_PROV_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PPCALPRO,PPCALSCO,PPLISRIF"+;
            " from "+i_cTable+" PAR_PROV where ";
                +"PPCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PPCALPRO,PPCALSCO,PPLISRIF;
            from (i_cTable) where;
                PPCODAZI = this.w_CODAZI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PPCALPRO = NVL(cp_ToDate(_read_.PPCALPRO),cp_NullValue(_read_.PPCALPRO))
          this.w_PPCALSCO = NVL(cp_ToDate(_read_.PPCALSCO),cp_NullValue(_read_.PPCALSCO))
          this.w_PPLISRIF = NVL(cp_ToDate(_read_.PPLISRIF),cp_NullValue(_read_.PPLISRIF))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        do GSVE_BPP with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Fatturazione per Ordine/Ordine+Destinazione ; 
      *     Marco con data generazione e flag evaso le righe del DDT descrittive 
      *     non importate da ordine
      if this.w_FLEVAORD
         
 Select EvaDesc 
 Go Top
        do while Not Eof( "EvaDesc" )
          * --- Chiudo la riga descrittiva e marco l'operazione...
          GSVE_BGE(this,"CHIUDE", this.w_MVRIFFAD , NVL(EvaDesc.SERIAL, SPACE(10)) , NVL(EvaDesc.ROWNUM, 0), NVL(EvaDesc.NUMRIF, 0) , this.oParentObject.w_MVDATREG )
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
           
 Select EvaDesc 
 Skip
        enddo
        * --- Una volta inseriti i dati zappo il transitorio...
         
 Select EvaDesc 
 Zap
        * --- Ricomincio dal prossimo...
        this.w_FLEVAORD = .F.
      endif
      if USED("AppRagg")
        if this.oParentObject.w_FLRAGG="S"
          * --- Aggiorna Riferimenti per le Fatture ragruppate
          SELECT AppRagg
          GO TOP
          SCAN FOR t_MVTIPRIG $ "RM" AND NOT EMPTY(t_MVCODICE)
          this.w_RAGG = .T.
          this.w_FRROWFAT = NVL(RIFROW, 0)
          this.w_FRNUMFAT = -90
          this.w_FRSERDDT = t_MVSERRIF
          this.w_FRROWDDT = t_MVROWRIF
          this.w_FRNUMDDT = -20
          this.w_FRQTAIMP = Nvl(t_MVQTAMOV, 0)
          this.w_FRQTAIM1 = Nvl(t_MVQTAUM1, 0)
          this.w_DOQTAEVA = t_DOQTAEVA
          this.w_DOIMPEVA = t_DOIMPEVA
          this.w_MULSER = this.w_FRSERDDT
          this.w_MULROW = this.w_FRROWDDT
          this.w_MULRIF = this.w_FRNUMDDT
          this.w_DATEVA = NVL(t_ORAPDATGEN, CTOD(""))
          this.w_NEXTFAT = t_ORAPDATEVA
          * --- Controllo Evasione Multiutenza
          this.Page_10()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Insert into RAG_FATT
          i_nConn=i_TableProp[this.RAG_FATT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.RAG_FATT_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.RAG_FATT_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"FRSERFAT"+",FRROWFAT"+",FRNUMFAT"+",FRSERDDT"+",FRROWDDT"+",FRNUMDDT"+",FRQTAIMP"+",FRQTAIM1"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVSERIAL),'RAG_FATT','FRSERFAT');
            +","+cp_NullLink(cp_ToStrODBC(this.w_FRROWFAT),'RAG_FATT','FRROWFAT');
            +","+cp_NullLink(cp_ToStrODBC(this.w_FRNUMFAT),'RAG_FATT','FRNUMFAT');
            +","+cp_NullLink(cp_ToStrODBC(this.w_FRSERDDT),'RAG_FATT','FRSERDDT');
            +","+cp_NullLink(cp_ToStrODBC(this.w_FRROWDDT),'RAG_FATT','FRROWDDT');
            +","+cp_NullLink(cp_ToStrODBC(this.w_FRNUMDDT),'RAG_FATT','FRNUMDDT');
            +","+cp_NullLink(cp_ToStrODBC(this.w_FRQTAIMP),'RAG_FATT','FRQTAIMP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_FRQTAIM1),'RAG_FATT','FRQTAIM1');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'FRSERFAT',this.oParentObject.w_MVSERIAL,'FRROWFAT',this.w_FRROWFAT,'FRNUMFAT',this.w_FRNUMFAT,'FRSERDDT',this.w_FRSERDDT,'FRROWDDT',this.w_FRROWDDT,'FRNUMDDT',this.w_FRNUMDDT,'FRQTAIMP',this.w_FRQTAIMP,'FRQTAIM1',this.w_FRQTAIM1)
            insert into (i_cTable) (FRSERFAT,FRROWFAT,FRNUMFAT,FRSERDDT,FRROWDDT,FRNUMDDT,FRQTAIMP,FRQTAIM1 &i_ccchkf. );
               values (;
                 this.oParentObject.w_MVSERIAL;
                 ,this.w_FRROWFAT;
                 ,this.w_FRNUMFAT;
                 ,this.w_FRSERDDT;
                 ,this.w_FRROWDDT;
                 ,this.w_FRNUMDDT;
                 ,this.w_FRQTAIMP;
                 ,this.w_FRQTAIM1;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error='Impossibile Aggiornare Tabella RAG_FATT'
            return
          endif
          * --- Aggiorno righe documenti di Origine
          * --- Aggiorno solo se non ordine aperto
          if NVL(t_TDORDAPE, " ")<>"S"
            * --- Aggiorno saldi
            * --- Read from DOC_DETT
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.DOC_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "MVCODMAG,MVFLRISE,MVFLORDI,MVFLIMPE,MVCODMAT,MVF2RISE,MVF2ORDI,MVF2IMPE,MVQTASAL,MVKEYSAL,MVCODART,MVCODCOM"+;
                " from "+i_cTable+" DOC_DETT where ";
                    +"MVSERIAL = "+cp_ToStrODBC(this.w_FRSERDDT);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.w_FRROWDDT);
                    +" and MVNUMRIF = "+cp_ToStrODBC(this.w_FRNUMDDT);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                MVCODMAG,MVFLRISE,MVFLORDI,MVFLIMPE,MVCODMAT,MVF2RISE,MVF2ORDI,MVF2IMPE,MVQTASAL,MVKEYSAL,MVCODART,MVCODCOM;
                from (i_cTable) where;
                    MVSERIAL = this.w_FRSERDDT;
                    and CPROWNUM = this.w_FRROWDDT;
                    and MVNUMRIF = this.w_FRNUMDDT;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_MVCODMAG = NVL(cp_ToDate(_read_.MVCODMAG),cp_NullValue(_read_.MVCODMAG))
              this.w_MVFLRISE = NVL(cp_ToDate(_read_.MVFLRISE),cp_NullValue(_read_.MVFLRISE))
              this.w_MVFLORDI = NVL(cp_ToDate(_read_.MVFLORDI),cp_NullValue(_read_.MVFLORDI))
              this.w_MVFLIMPE = NVL(cp_ToDate(_read_.MVFLIMPE),cp_NullValue(_read_.MVFLIMPE))
              this.w_MVCODMAT = NVL(cp_ToDate(_read_.MVCODMAT),cp_NullValue(_read_.MVCODMAT))
              this.w_MVF2RISE = NVL(cp_ToDate(_read_.MVF2RISE),cp_NullValue(_read_.MVF2RISE))
              this.w_MVF2ORDI = NVL(cp_ToDate(_read_.MVF2ORDI),cp_NullValue(_read_.MVF2ORDI))
              this.w_MVF2IMPE = NVL(cp_ToDate(_read_.MVF2IMPE),cp_NullValue(_read_.MVF2IMPE))
              this.w_MVQTASAL = NVL(cp_ToDate(_read_.MVQTASAL),cp_NullValue(_read_.MVQTASAL))
              this.w_MVKEYSAL = NVL(cp_ToDate(_read_.MVKEYSAL),cp_NullValue(_read_.MVKEYSAL))
              this.w_CODART = NVL(cp_ToDate(_read_.MVCODART),cp_NullValue(_read_.MVCODART))
              this.w_CODCOM = NVL(cp_ToDate(_read_.MVCODCOM),cp_NullValue(_read_.MVCODCOM))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if Not Empty(this.w_MVFLRISE) Or Not Empty(this.w_MVF2RISE)
              * --- Documento non Accettato (movimenta riservato)
              this.w_oERRORLOG.AddMsgLogPartNoTrans(space(5), "%0Bloccante:%0")     
              this.w_oERRORLOG.AddMsgLog("Impossibile generare doc.: %1 n.: %2 del %3%0Movimenta il riservato", this.w_TIPDOC, Alltrim(STR(this.w_NDOC,15))+ IIF(Not Empty(this.w_ADOC),"/"+Alltrim(this.w_ADOC),""), DTOC(this.w_DDOC))     
              * --- Raise
              i_Error="Errore"
              return
            endif
            if NOT EMPTY(this.w_MVFLORDI+this.w_MVFLIMPE+this.w_MVF2ORDI+this.w_MVF2IMPE)
              * --- Aggiorno saldi MVCODMAG
              this.w_FLIMPE = ICASE(this.w_MVFLIMPE="+","-", this.w_MVFLIMPE="-","+"," ")
              this.w_FLORDI = ICASE(this.w_MVFLORDI="+","-", this.w_MVFLORDI="-","+"," ")
              * --- Write into SALDIART
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDIART_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
                i_cOp1=cp_SetTrsOp(this.w_FLORDI,'SLQTOPER','this.w_MVQTASAL',this.w_MVQTASAL,'update',i_nConn)
                i_cOp2=cp_SetTrsOp(this.w_FLIMPE,'SLQTIPER','this.w_MVQTASAL',this.w_MVQTASAL,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
                +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
                    +i_ccchkf ;
                +" where ";
                    +"SLCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
                    +" and SLCODMAG = "+cp_ToStrODBC(this.w_MVCODMAG);
                       )
              else
                update (i_cTable) set;
                    SLQTOPER = &i_cOp1.;
                    ,SLQTIPER = &i_cOp2.;
                    &i_ccchkf. ;
                 where;
                    SLCODICE = this.w_MVKEYSAL;
                    and SLCODMAG = this.w_MVCODMAG;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              * --- Read from ART_ICOL
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.ART_ICOL_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ARSALCOM"+;
                  " from "+i_cTable+" ART_ICOL where ";
                      +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ARSALCOM;
                  from (i_cTable) where;
                      ARCODART = this.w_CODART;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if this.w_SALCOM="S"
                if empty(nvl(this.w_CODCOM,""))
                  this.w_COMMAPPO = this.w_COMMDEFA
                else
                  this.w_COMMAPPO = this.w_CODCOM
                endif
                * --- Write into SALDICOM
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.SALDICOM_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                  i_cOp1=cp_SetTrsOp(this.w_FLORDI,'SCQTOPER','this.w_MVQTASAL',this.w_MVQTASAL,'update',i_nConn)
                  i_cOp2=cp_SetTrsOp(this.w_FLIMPE,'SCQTIPER','this.w_MVQTASAL',this.w_MVQTASAL,'update',i_nConn)
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
                  +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
                      +i_ccchkf ;
                  +" where ";
                      +"SCCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
                      +" and SCCODMAG = "+cp_ToStrODBC(this.w_MVCODMAG);
                      +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                         )
                else
                  update (i_cTable) set;
                      SCQTOPER = &i_cOp1.;
                      ,SCQTIPER = &i_cOp2.;
                      &i_ccchkf. ;
                   where;
                      SCCODICE = this.w_MVKEYSAL;
                      and SCCODMAG = this.w_MVCODMAG;
                      and SCCODCAN = this.w_COMMAPPO;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error='Errore Aggiornamento Saldi Commessa'
                  return
                endif
              endif
              * --- Aggiorno saldi MVCODMAT
              this.w_FLIMPE = ICASE(this.w_MVF2IMPE="+","-", this.w_MVF2IMPE="-","+"," ")
              this.w_FLORDI = ICASE(this.w_MVF2ORDI="+","-", this.w_MVF2ORDI="-","+"," ")
              * --- Write into SALDIART
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDIART_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
                i_cOp1=cp_SetTrsOp(this.w_FLORDI,'SLQTOPER','this.w_MVQTASAL',this.w_MVQTASAL,'update',i_nConn)
                i_cOp2=cp_SetTrsOp(this.w_FLIMPE,'SLQTIPER','this.w_MVQTASAL',this.w_MVQTASAL,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
                +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
                    +i_ccchkf ;
                +" where ";
                    +"SLCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
                    +" and SLCODMAG = "+cp_ToStrODBC(this.w_MVCODMAT);
                       )
              else
                update (i_cTable) set;
                    SLQTOPER = &i_cOp1.;
                    ,SLQTIPER = &i_cOp2.;
                    &i_ccchkf. ;
                 where;
                    SLCODICE = this.w_MVKEYSAL;
                    and SLCODMAG = this.w_MVCODMAT;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              if this.w_SALCOM="S"
                * --- Write into SALDICOM
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.SALDICOM_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                  i_cOp1=cp_SetTrsOp(this.w_FLORDI,'SCQTOPER','this.w_MVQTASAL',this.w_MVQTASAL,'update',i_nConn)
                  i_cOp2=cp_SetTrsOp(this.w_FLIMPE,'SCQTIPER','this.w_MVQTASAL',this.w_MVQTASAL,'update',i_nConn)
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
                  +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
                      +i_ccchkf ;
                  +" where ";
                      +"SCCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
                      +" and SCCODMAG = "+cp_ToStrODBC(this.w_MVCODMAT);
                      +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                         )
                else
                  update (i_cTable) set;
                      SCQTOPER = &i_cOp1.;
                      ,SCQTIPER = &i_cOp2.;
                      &i_ccchkf. ;
                   where;
                      SCCODICE = this.w_MVKEYSAL;
                      and SCCODMAG = this.w_MVCODMAT;
                      and SCCODCAN = this.w_COMMAPPO;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error='Errore Aggiornamento Saldi Commessa'
                  return
                endif
              endif
              this.w_MVQTASAL = 0
            endif
            * --- Write into DOC_DETT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.DOC_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
              declare i_aIndex[1]
              i_cQueryTable=cp_getTempTableName(i_nConn)
              i_aIndex(1)="MVSERIAL,CPROWNUM,MVNUMRIF"
              do vq_exec with 'gsve0bfd',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)	
              i_cDB=cp_GetDatabaseType(i_nConn)
              do case
              case i_cDB="SQLServer"
                i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                      +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MVDATGEN ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVDATREG),'DOC_DETT','MVDATGEN');
              +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_DETT','MVFLEVAS');
                  +",MVQTAEVA = _t2.MVQTAMOV";
                  +",MVQTAEV1 = _t2.MVQTAUM1";
                  +",MVIMPEVA = _t2.MVPREZZO";
              +",MVQTASAL ="+cp_NullLink(cp_ToStrODBC(this.w_MVQTASAL),'DOC_DETT','MVQTASAL');
                  +i_ccchkf;
                  +" from "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
              case i_cDB="MySQL"
                i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                      +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 set ";
              +"DOC_DETT.MVDATGEN ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVDATREG),'DOC_DETT','MVDATGEN');
              +",DOC_DETT.MVFLEVAS ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_DETT','MVFLEVAS');
                  +",DOC_DETT.MVQTAEVA = _t2.MVQTAMOV";
                  +",DOC_DETT.MVQTAEV1 = _t2.MVQTAUM1";
                  +",DOC_DETT.MVIMPEVA = _t2.MVPREZZO";
              +",DOC_DETT.MVQTASAL ="+cp_NullLink(cp_ToStrODBC(this.w_MVQTASAL),'DOC_DETT','MVQTASAL');
                  +Iif(Empty(i_ccchkf),"",",DOC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                  +" where "+i_cWhere)
              case i_cDB="Oracle"
                i_cWhere="DOC_DETT.MVSERIAL = t2.MVSERIAL";
                      +" and "+"DOC_DETT.CPROWNUM = t2.CPROWNUM";
                      +" and "+"DOC_DETT.MVNUMRIF = t2.MVNUMRIF";
                
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set (";
                  +"MVDATGEN,";
                  +"MVFLEVAS,";
                  +"MVQTAEVA,";
                  +"MVQTAEV1,";
                  +"MVIMPEVA,";
                  +"MVQTASAL";
                  +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                  +cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVDATREG),'DOC_DETT','MVDATGEN')+",";
                  +cp_NullLink(cp_ToStrODBC("S"),'DOC_DETT','MVFLEVAS')+",";
                  +"t2.MVQTAMOV,";
                  +"t2.MVQTAUM1,";
                  +"t2.MVPREZZO,";
                  +cp_NullLink(cp_ToStrODBC(this.w_MVQTASAL),'DOC_DETT','MVQTASAL')+"";
                  +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                  +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
              case i_cDB="PostgreSQL"
                i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                      +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set ";
              +"MVDATGEN ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVDATREG),'DOC_DETT','MVDATGEN');
              +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_DETT','MVFLEVAS');
                  +",MVQTAEVA = _t2.MVQTAMOV";
                  +",MVQTAEV1 = _t2.MVQTAUM1";
                  +",MVIMPEVA = _t2.MVPREZZO";
              +",MVQTASAL ="+cp_NullLink(cp_ToStrODBC(this.w_MVQTASAL),'DOC_DETT','MVQTASAL');
                  +i_ccchkf;
                  +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
              otherwise
                i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
                      +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                      +" and "+i_cTable+".MVNUMRIF = "+i_cQueryTable+".MVNUMRIF";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MVDATGEN ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVDATREG),'DOC_DETT','MVDATGEN');
              +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_DETT','MVFLEVAS');
                  +",MVQTAEVA = (select MVQTAMOV from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVQTAEV1 = (select MVQTAUM1 from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVIMPEVA = (select MVPREZZO from "+i_cQueryTable+" where "+i_cWhere+")";
              +",MVQTASAL ="+cp_NullLink(cp_ToStrODBC(this.w_MVQTASAL),'DOC_DETT','MVQTASAL');
                  +i_ccchkf;
                  +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
              endcase
              cp_DropTempTable(i_nConn,i_cQueryTable)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          else
            * --- Ordine aperto, aggiorno data fatturazione
            * --- Write into DOC_DETT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.DOC_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MVDATEVA ="+cp_NullLink(cp_ToStrODBC(this.w_NEXTFAT),'DOC_DETT','MVDATEVA');
              +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(IIF(EMPTY(this.w_NEXTFAT), "S", " ")),'DOC_DETT','MVFLEVAS');
              +",MVDATGEN ="+cp_NullLink(cp_ToStrODBC(this.w_DATEVA),'DOC_DETT','MVDATGEN');
                  +i_ccchkf ;
              +" where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.w_FRSERDDT);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_FRROWDDT);
                  +" and MVNUMRIF = "+cp_ToStrODBC(this.w_FRNUMDDT);
                     )
            else
              update (i_cTable) set;
                  MVDATEVA = this.w_NEXTFAT;
                  ,MVFLEVAS = IIF(EMPTY(this.w_NEXTFAT), "S", " ");
                  ,MVDATGEN = this.w_DATEVA;
                  &i_ccchkf. ;
               where;
                  MVSERIAL = this.w_FRSERDDT;
                  and CPROWNUM = this.w_FRROWDDT;
                  and MVNUMRIF = this.w_FRNUMDDT;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          SELECT AppRagg
          ENDSCAN
          this.w_OKDOC = this.w_OKDOC + 1
        endif
        SELECT AppRagg
        USE
      endif
      * --- commit
      cp_EndTrs(.t.)
    endif
    return


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Importa Eventuali Fatture di Acconto
     
 SELECT t_MVSERORI AS SERRIF, t_MVNUMORI AS NUMRIF, SUM(t_MVVALRIG) AS TOTIMP, 999-999 AS PERCEN ; 
 FROM GeneApp WHERE NOT EMPTY(t_MVSERORI) AND t_MVNUMORI<>0 ; 
 INTO CURSOR TestAcc GROUP BY SERRIF, NUMRIF
    SELECT TestAcc
    if RECCOUNT()>0
      * --- Se Trovati records Passa a Cercare le fatture di Acconto
      Cur = WrCursor ("TestAcc")
      ah_Msg("Ricerca fatture di acconto da generare...")
      vq_exec("query\GSVE_QAC.VQR",this,"FattAcc")
      SELECT FattAcc
      if RECCOUNT()>0
        * --- Effettuo una Ricerca Recursiva su TestAcc
        this.w_RECACC = .T.
        do while this.w_RECACC
          this.w_RECACC = .F.
          SELECT TestAcc
          GO TOP
          SCAN FOR NUMRIF<>0
          * --- Il Ciclo va avanti fino a quando tutti i NUMRIF sono stati Azzerati
          * --- Ovvero se sono stati trovati Ordini (SERRIF valorizzato) o no (SERRIF vuoto o record cancellato)
          this.w_RECACC = .T.
          this.w_SERACC = SERRIF
          this.w_NUMACC = NUMRIF
          this.w_CLADOC = "  "
          this.w_MAXACC = 0
          * --- Read from DOC_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DOC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MVCLADOC,MVMAXACC"+;
              " from "+i_cTable+" DOC_MAST where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.w_SERACC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MVCLADOC,MVMAXACC;
              from (i_cTable) where;
                  MVSERIAL = this.w_SERACC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CLADOC = NVL(cp_ToDate(_read_.MVCLADOC),cp_NullValue(_read_.MVCLADOC))
            this.w_MAXACC = NVL(cp_ToDate(_read_.MVMAXACC),cp_NullValue(_read_.MVMAXACC))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          SELECT TestAcc
          if i_Rows<>0
            if this.w_CLADOC="OR"
              * --- Se si tratta di Ordine aggiorna in tutti i records lo stesso SERRIF
              * --- Azzera NUMRIF per non ricercarlo nei successivi LOOP
              GO TOP
              UPDATE TestAcc SET NUMRIF=0, PERCEN=IIF(this.w_MAXACC=0,100,this.w_MAXACC) WHERE SERRIF=this.w_SERACC
              EXIT
            else
              * --- C'e' un documento ma non e' un Ordine, allora leggo la Riga di origine
              this.w_SERAC1 = SPACE(10)
              this.w_NUMAC1 = 0
              * --- Read from DOC_DETT
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.DOC_DETT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "MVSERRIF,MVROWRIF"+;
                  " from "+i_cTable+" DOC_DETT where ";
                      +"MVSERIAL = "+cp_ToStrODBC(this.w_SERACC);
                      +" and CPROWNUM = "+cp_ToStrODBC(this.w_NUMACC);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  MVSERRIF,MVROWRIF;
                  from (i_cTable) where;
                      MVSERIAL = this.w_SERACC;
                      and CPROWNUM = this.w_NUMACC;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_SERAC1 = NVL(cp_ToDate(_read_.MVSERRIF),cp_NullValue(_read_.MVSERRIF))
                this.w_NUMAC1 = NVL(cp_ToDate(_read_.MVROWRIF),cp_NullValue(_read_.MVROWRIF))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              SELECT TestAcc
              * --- Quindi Sostituisco i Riferimenti che utilizzero' al successivo LOOP (tranne se Vuoti)
              * --- Uso la REPLACE xbase per non spostare il Puntatore
              REPLACE SERRIF WITH this.w_SERAC1, NUMRIF WITH this.w_NUMAC1
            endif
          else
            * --- Il Documento di Origine non esiste elimina tutti i Records che puntano allo stesso
            GO TOP
            DELETE FROM TestAcc WHERE SERRIF=this.w_SERACC
            EXIT
          endif
          ENDSCAN
        enddo
        * --- Alla fine Raggruppo i record Superstiti per SERRIF (Serial Ordine)
         
 SELECT SERRIF, SUM(cp_ROUND((TOTIMP*PERCEN)/100, this.w_DECUNI)) AS TOTACC FROM TestAcc ; 
 WHERE NOT EMPTY(SERRIF) AND NUMRIF=0 AND NOT DELETED() ; 
 INTO CURSOR TestAcc1 GROUP BY SERRIF
         
 Select TestAcc 
 Use
        USE
        * --- A questo punto abbiamo tutti gli Ordini di origine delle Righe Fatturate
        *     Con il totale Importi Evadibili, diminuiti della percentuale Max. di Acconto da Evadere
        SELECT TestAcc1
        GO TOP
        SCAN FOR TOTACC>0
        * --- Ciclo sul Cursore e verifico se esiste un riferimeno dell'ordine sulle righe Fatture di Acconto
        this.w_SERACC = SERRIF
        this.w_TOTAFACC = cp_ROUND(NVL(TOTACC, 0), this.w_DECUNI)
        SELECT FattAcc
        GO TOP
        SCAN FOR MVRIFORD=this.w_SERACC AND this.w_TOTAFACC>0
        this.w_ORNUMDOC = NVL(MVNUMDOC, 0)
        this.w_ORALFDOC = NVL(MVALFDOC, Space(10))
        this.w_ORDATDOC = CP_TODATE(MVDATDOC)
        this.w_ORTIPDOC = NVL(MVTIPDOC, SPACE(5))
        this.w_ORDESRIF = " "
        this.w_NRIF = NVL(MVNUMEST, 0)
        this.w_ARIF = NVL(MVALFEST, Space(10))
        this.w_DRIF = CP_TODATE(MVDATEST)
        * --- Nelle fatture non � possibile impostare il riferimento descrittivo...
        this.w_RIFCLI = ""
        if NOT EMPTY(this.w_ORTIPDOC)
          * --- Read from TIP_DOCU
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TDDESRIF,TDMODRIF"+;
              " from "+i_cTable+" TIP_DOCU where ";
                  +"TDTIPDOC = "+cp_ToStrODBC(this.w_ORTIPDOC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TDDESRIF,TDMODRIF;
              from (i_cTable) where;
                  TDTIPDOC = this.w_ORTIPDOC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_ORDESRIF = NVL(cp_ToDate(_read_.TDDESRIF),cp_NullValue(_read_.TDDESRIF))
            this.w_MODRIF = NVL(cp_ToDate(_read_.TDMODRIF),cp_NullValue(_read_.TDMODRIF))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        this.Page_11()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        SELECT FattAcc
        * --- Flag Riga Evasa (su Fattura di Anticipo) (ricalcola sempre le Rate)
        this.w_CALRATE = .T.
        this.w_MVPREZZO = this.w_TOTAFACC
        this.w_MVFLERIF = IIF(this.w_MVPREZZO<MVPREZZO, " ", "S")
        * --- L'importo da Riportare su Riga non puo' essere maggiore dell'Importo ancora da Evadere
        this.w_MVPREZZO = IIF(this.w_MVPREZZO>MVPREZZO, MVPREZZO, this.w_MVPREZZO) * -1
        * --- Storna l'importo ancora Evadibile per le successive eventuali Fatture di Acconto
        this.w_TOTAFACC = this.w_TOTAFACC + this.w_MVPREZZO
        this.w_MVSERRIF = MVSERIAL
        this.w_MVROWRIF = CPROWNUM
        this.w_MVCODICE = NVL(MVCODICE," ")
        this.w_MVCODART = NVL(MVCODART," ")
        this.w_MVDESART = NVL(MVDESART, " ")
        this.w_MVDESSUP = NVL(MVDESSUP, " ")
        this.w_MVUNIMIS = NVL(MVUNIMIS, " ")
        this.w_MVCATCON = NVL(MVCATCON, " ")
        this.w_MVCONTRO = NVL(MVCONTRO, " ")
        this.w_MVCODCLA = NVL(MVCODCLA, " ")
        this.w_MVCONTRA = NVL(MVCONTRA, " ")
        this.w_MVCODLIS = NVL(MVCODLIS, " ")
        this.w_MVSCONT1 = NVL(MVSCONT1, 0)
        this.w_MVSCONT2 = NVL(MVSCONT2, 0)
        this.w_MVSCONT3 = NVL(MVSCONT3, 0)
        this.w_MVSCONT4 = NVL(MVSCONT4, 0)
        this.w_MVFLOMAG = NVL(MVFLOMAG, "X")
        this.w_MVCODIVA = NVL(MVCODIVA," ")
        this.w_PERIVA = NVL(IVPERIVA, 0)
        this.w_BOLIVA = NVL(IVBOLIVA, " ")
        this.w_MVCONIND = NVL(MVCONIND," ")
        this.w_FLSERA = NVL(ARTIPSER, " ")
        this.w_MVCODCES = NVL(MVCODCES,SPACE(20))
        this.w_MVVALRIG = CAVALRIG(this.w_MVPREZZO,1, this.w_MVSCONT1,this.w_MVSCONT2,this.w_MVSCONT3,this.w_MVSCONT4,this.w_DECTOT)
        this.w_TOTALE = this.w_TOTALE + this.w_MVVALRIG
        this.w_TOTMERCE = this.w_TOTMERCE + IIF(this.w_MVFLOMAG="X" AND this.w_FLSERA<>"S", this.w_MVVALRIG, 0)
        * --- Se � attivo il flag genera provvigioni sulla fattura differita e ho attivo il flag
        *     'Provvigioni in Generazione documenti' nei parametri provvigioni
        *     svuoto i valori delle provvigioni in modo che mi vengano ricalcolate dal GSVE_BPP
        if g_PROGEN= "S"
          this.w_MVTIPPRO = IIF(NVL(MVTIPPRO,SPACE(2))$"FO-ES", NVL(MVTIPPRO,SPACE(2)),IIF(g_CALPRO="DI","DC",IIF(g_CALPRO="GD","ST","CT")))
          this.w_MVTIPPR2 = IIF(NVL(MVTIPPR2,SPACE(2))$"FO-ES", NVL(MVTIPPR2,SPACE(2)),IIF(g_CALPRO="DI","DC",IIF(g_CALPRO="GD","ST","CT")))
          this.w_MVPERPRO = IIF(NVL(MVTIPPRO,SPACE(2))$"FO-ES", NVL(MVPERPRO, 0), 0)
          this.w_MVIMPPRO = IIF(NVL(MVTIPPRO,SPACE(2))$"FO-ES", NVL(MVIMPPRO, 0), 0)
          this.w_MVPROCAP = IIF(NVL(MVTIPPRO,SPACE(2))$"FO-ES", NVL(MVPROCAP, 0), 0)
          this.w_MVIMPCAP = IIF(NVL(MVTIPPRO,SPACE(2))$"FO-ES", NVL(MVIMPCAP, 0), 0)
        else
          * --- Altrimenti riporto quelli del DDT
          this.w_MVTIPPRO = NVL(MVTIPPRO,SPACE(2))
          this.w_MVTIPPR2 = NVL(MVTIPPR2,SPACE(2))
          this.w_MVPERPRO = NVL(MVPERPRO,0)
          this.w_MVIMPPRO = NVL(MVIMPPRO,0)
          this.w_MVPROCAP = NVL(MVPROCAP,0)
          this.w_MVIMPCAP = NVL(MVIMPCAP,0)
        endif
        this.w_MVIMPCOM = NVL(MVIMPCOM,0)
        this.w_DOQTAEVA = Nvl(MVQTAEVA,0)
        this.w_DOIMPEVA = Nvl(MVIMPEVA,0)
        this.w_MVFLNOAN = NVL(MVFLNOAN, "N")
         
 INSERT INTO GeneApp ; 
 (t_MVTIPRIG, t_MVCODICE, t_MVCODART, ; 
 t_MVDESART, t_MVDESSUP, t_MVUNIMIS, t_MVCATCON, ; 
 t_MVCONTRO, t_MVCODCLA, t_MVCONTRA, t_MVCODLIS, ; 
 t_MVQTAMOV, t_MVQTAUM1, t_MVPREZZO, ; 
 t_MVSCONT1, t_MVSCONT2, t_MVSCONT3, t_MVSCONT4, ; 
 t_MVFLOMAG, t_MVCODIVA, t_PERIVA, t_BOLIVA, t_MVVALRIG, t_MVPERPRO, ; 
 t_MVIMPPRO, t_MVSERRIF, t_MVROWRIF, ; 
 t_MVPESNET, t_MVFLTRAS, t_MVNOMENC, t_MVUMSUPP, ; 
 t_MVMOLSUP, t_MVNUMCOL, t_MVCONIND, ; 
 t_MVNAZPRO, t_MVPROORD, t_MVCODCOM, t_MVINICOM, t_MVFINCOM,t_MVFLARIF, t_MVIMPEVA, t_MESS, ; 
 t_MVSERORI, t_MVNUMORI, t_MVFLERIF, t_FLSERA,t_MVTIPPRO, t_MVCODCES, ; 
 t_MVIMPCOM, t_MVIMPAC2, t_DOQTAEVA, t_DOIMPEVA, t_MVTIPPR2, t_MVPROCAP, t_MVIMPCAP,t_MVFLNOAN, t_MVCODRES, t_MVFLORCO) ; 
 VALUES ; 
 ("F", this.w_MVCODICE, this.w_MVCODART, ; 
 this.w_MVDESART, this.w_MVDESSUP, this.w_MVUNIMIS, this.w_MVCATCON, ; 
 this.w_MVCONTRO, this.w_MVCODCLA, this.w_MVCONTRA, this.w_MVCODLIS, ; 
 1, 1, this.w_MVPREZZO, ; 
 this.w_MVSCONT1, this.w_MVSCONT2, this.w_MVSCONT3, this.w_MVSCONT4, ; 
 this.w_MVFLOMAG, this.w_MVCODIVA, this.w_PERIVA, this.w_BOLIVA, this.w_MVVALRIG, this.w_MVPERPRO, ; 
 this.w_MVIMPPRO, this.w_MVSERRIF, this.w_MVROWRIF, ; 
 0, " ", " ", " ", ; 
 0, 0, this.w_MVCONIND, ; 
 " ", " ", " ", cp_CharToDate("  -  -  "), cp_CharToDate("  -  -  "), "-", 0, " ",; 
 " ", 0, this.w_MVFLERIF, this.w_FLSERA, this.w_MVTIPPRO, this.w_MVCODCES, ; 
 this.w_MVIMPCOM, 0, this.w_DOQTAEVA, this.w_DOIMPEVA, this.w_MVTIPPR2, this.w_MVPROCAP, this.w_MVIMPCAP,this.w_MVFLNOAN, this.w_MVCODRES," ")
        SELECT FattAcc
        ENDSCAN
        SELECT TestAcc1
        ENDSCAN
        SELECT TestAcc1
        USE
      endif
      SELECT FattAcc
      USE
    endif
    USE IN SELECT("TestAcc")
    * --- Raggruppa le righe per Articolo
    if this.oParentObject.w_FLRAGG="S"
      this.Page_8()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.w_MVFLFOSC <> "S"
      this.w_MVSCONTI = Calsco(this.w_TOTMERCE, this.w_MVSCOCL1, this.w_MVSCOCL2, this.w_MVSCOPAG, this.w_DECTOT)
    endif
    * --- Ripartisce Spese Accessorie e Sconti
    * --- Totale Spese da Ripartire
    *     Nel caso di Scorporo piede fattura e codice Iva spese presente, calcolo in w_SPEACC_IVA 
    *     la somma delle spese scorporate della loro Iva.
    this.w_SPEACC_IVA = 0
    this.w_SPEACC = 0
    if Not Empty(this.w_MVIVAINC) 
      if this.w_MVFLSCOR="S"
        this.w_SPEACC_IVA = IIF(this.w_MVFLRINC="S", Calnet( this.w_MVSPEINC, this.w_PEIINC, this.w_DECTOT, Space(5), 0 ) , 0)
      else
        this.w_SPEACC_IVA = IIF(this.w_MVFLRINC="S", this.w_MVSPEINC, 0)
      endif
    else
      this.w_SPEACC = IIF(this.w_MVFLRINC="S", this.w_MVSPEINC, 0)
    endif
    if Not Empty(this.w_MVIVAIMB) 
      if this.w_MVFLSCOR="S"
        this.w_SPEACC_IVA = this.w_SPEACC_IVA + IIF(this.w_MVFLRIMB="S", Calnet( this.w_MVSPEIMB, this.w_PEIIMB, this.w_DECTOT, Space(5), 0 ) , 0)
      else
        this.w_SPEACC_IVA = this.w_SPEACC_IVA + IIF(this.w_MVFLRIMB="S", this.w_MVSPEIMB, 0)
      endif
    else
      this.w_SPEACC = this.w_SPEACC + IIF(this.w_MVFLRIMB="S", this.w_MVSPEIMB, 0)
    endif
    if Not Empty(this.w_MVIVATRA) 
      if this.w_MVFLSCOR="S"
        this.w_SPEACC_IVA = this.w_SPEACC_IVA + IIF(this.w_MVFLRTRA="S", Calnet( this.w_MVSPETRA, this.w_PEITRA, this.w_DECTOT, Space(5), 0 ) , 0)
      else
        this.w_SPEACC_IVA = this.w_SPEACC_IVA + IIF(this.w_MVFLRTRA="S", this.w_MVSPETRA, 0)
      endif
    else
      this.w_SPEACC = this.w_SPEACC + IIF(this.w_MVFLRTRA="S", this.w_MVSPETRA, 0)
    endif
    this.w_DATCOM = IIF(Empty(this.w_MVDATDOC),this.oParentObject.w_MVDATREG, this.w_MVDATDOC)
    * --- La ripartizione delle spese viene fatta solo se non � attivo
    *     il flag 'Ripartisce contributi accessori' della causale documento.
    *     Se il flag � attivo la ripartizione sar� chiamata da GSAR_BRD nel caso sia stata attivata
    *     la variabile w_RICTOT
    *     w_RICTOT � attivo anche nel caso in cui debbano essere calcolate le ritenute passive
    if this.w_TDRIPCON = "S" OR (this.w_FLGRITE = "S" and this.oParentObject.w_GESRIT="S" AND g_RITE="S")
      this.w_RICTOT = .T.
    else
      GSAR_BRS(this,"B", "GeneApp", this.w_MVFLVEAC, this.w_MVFLSCOR, this.w_SPEACC, this.w_MVSCONTI, this.w_TOTMERCE, this.w_MVCODIVE, this.w_MVCAOVAL, this.w_DATCOM, this.w_CAONAZ, this.w_MVVALNAZ, this.w_MVCODVAL, this.w_SPEACC_IVA, , this.w_TDRIPCON)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa Fatture Differite
    this.oParentObject.w_MVSERIAL = ""
    this.oParentObject.w_CODDES = ""
    this.oParentObject.w_CATCOM = ""
    this.oParentObject.w_CODZON = ""
    this.oParentObject.w_CODAGE = ""
    this.oParentObject.w_CODPAG = ""
    GSVE_BRD(This.oParentObject, "F")
  endproc


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Cicla Sul Vettore delle Rate (Calcolato in GSAR_BFA)
    * --- Se il Documento di Origine Ha attivo il Check "Scadenze Confermate" e la Fattura Differita non e' originata da pi� documenti
    * --- Riprende le Scadenze del Documento di origine
    this.w_TOTRATEBFA = 0
    this.w_TOTRATEORI = 0
    this.w_TOTDOC = 0
    this.w_MINRATA = 0
    this.w_ERRRAT = .F.
    this.w_TOTRATEBFA = DR[alen(DR,1), 2]
    if this.w_MVFLSCAF="S" AND this.w_CALRATE=.F.
      l_i = 1
      * --- Select from DOC_RATE
      i_nConn=i_TableProp[this.DOC_RATE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_RATE_idx,2],.t.,this.DOC_RATE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" DOC_RATE ";
            +" where RSSERIAL="+cp_ToStrODBC(this.w_MVSERNOCA)+"";
             ,"_Curs_DOC_RATE")
      else
        select * from (i_cTable);
         where RSSERIAL=this.w_MVSERNOCA;
          into cursor _Curs_DOC_RATE
      endif
      if used('_Curs_DOC_RATE')
        select _Curs_DOC_RATE
        locate for 1=1
        do while not(eof())
        DR[l_i, 1] = _Curs_DOC_RATE.RSDATRAT
        DR[l_i, 2] = _Curs_DOC_RATE.RSIMPRAT
        DR[l_i, 3] = _Curs_DOC_RATE.RSMODPAG
        DR[l_i, 4] = NVL(_Curs_DOC_RATE.RSDESRIG, " ")
        DR[l_i, 5] = NVL(_Curs_DOC_RATE.RSBANNOS,SPACE(15))
        DR[l_i, 6] = NVL(_Curs_DOC_RATE.RSBANAPP,SPACE(10))
        DR[l_i, 7] = NVL(_Curs_DOC_RATE.RSFLSOSP," ")
        DR[l_i, 8] = NVL(_Curs_DOC_RATE.RSCONCOR," ")
        DR[l_i, 9] = NVL(_Curs_DOC_RATE.RSFLPROV," ")
        l_i = l_i + 1
        this.w_TOTRATEORI = this.w_TOTRATEORI + NVL(_Curs_DOC_RATE.RSIMPRAT,0)
          select _Curs_DOC_RATE
          continue
        enddo
        use
      endif
      FOR l_x=l_i TO (alen(DR,1)-1)
      DR[l_x, 1] = cp_CharToDate("  -  -    ")
      DR[l_x, 2] = 0
      DR[l_x, 3] = ""
      NEXT
      if this.w_TOTRATEBFA<>this.w_TOTRATEORI
        this.w_ERRRAT = .T.
      endif
    endif
    FOR l_i=1 TO (alen(DR,1)-1)
    if NOT EMPTY(DR[l_i, 1]) AND NOT EMPTY(DR[l_i, 2])
      this.w_RSNUMRAT = l_i
      this.w_RSDATRAT = DR[l_i, 1]
      this.w_RSIMPRAT = DR[l_i, 2]
      this.w_RSMODPAG = DR[l_i, 3]
      this.w_RSDESRIG = DR[l_i, 4]
      this.w_RSBANNOS = DR[l_i, 5]
      this.w_RSBANAPP = DR[l_i, 6]
      this.w_RSFLSOSP = DR[l_i, 7]
      this.w_RSCONCOR = DR[l_i, 8]
      this.w_RSFLPROV = DR[l_i, 9]
      * --- Scrive Doc_Rate
      * --- Insert into DOC_RATE
      i_nConn=i_TableProp[this.DOC_RATE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_RATE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOC_RATE_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"RSSERIAL"+",RSNUMRAT"+",RSDATRAT"+",RSIMPRAT"+",RSMODPAG"+",RSFLSOSP"+",RSDESRIG"+",RSBANNOS"+",RSBANAPP"+",RSCONCOR"+",RSFLPROV"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVSERIAL),'DOC_RATE','RSSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_RSNUMRAT),'DOC_RATE','RSNUMRAT');
        +","+cp_NullLink(cp_ToStrODBC(this.w_RSDATRAT),'DOC_RATE','RSDATRAT');
        +","+cp_NullLink(cp_ToStrODBC(this.w_RSIMPRAT),'DOC_RATE','RSIMPRAT');
        +","+cp_NullLink(cp_ToStrODBC(this.w_RSMODPAG),'DOC_RATE','RSMODPAG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_RSFLSOSP),'DOC_RATE','RSFLSOSP');
        +","+cp_NullLink(cp_ToStrODBC(this.w_RSDESRIG),'DOC_RATE','RSDESRIG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_RSBANNOS),'DOC_RATE','RSBANNOS');
        +","+cp_NullLink(cp_ToStrODBC(this.w_RSBANAPP),'DOC_RATE','RSBANAPP');
        +","+cp_NullLink(cp_ToStrODBC(this.w_RSCONCOR),'DOC_RATE','RSCONCOR');
        +","+cp_NullLink(cp_ToStrODBC(this.w_RSFLPROV),'DOC_RATE','RSFLPROV');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'RSSERIAL',this.oParentObject.w_MVSERIAL,'RSNUMRAT',this.w_RSNUMRAT,'RSDATRAT',this.w_RSDATRAT,'RSIMPRAT',this.w_RSIMPRAT,'RSMODPAG',this.w_RSMODPAG,'RSFLSOSP',this.w_RSFLSOSP,'RSDESRIG',this.w_RSDESRIG,'RSBANNOS',this.w_RSBANNOS,'RSBANAPP',this.w_RSBANAPP,'RSCONCOR',this.w_RSCONCOR,'RSFLPROV',this.w_RSFLPROV)
        insert into (i_cTable) (RSSERIAL,RSNUMRAT,RSDATRAT,RSIMPRAT,RSMODPAG,RSFLSOSP,RSDESRIG,RSBANNOS,RSBANAPP,RSCONCOR,RSFLPROV &i_ccchkf. );
           values (;
             this.oParentObject.w_MVSERIAL;
             ,this.w_RSNUMRAT;
             ,this.w_RSDATRAT;
             ,this.w_RSIMPRAT;
             ,this.w_RSMODPAG;
             ,this.w_RSFLSOSP;
             ,this.w_RSDESRIG;
             ,this.w_RSBANNOS;
             ,this.w_RSBANAPP;
             ,this.w_RSCONCOR;
             ,this.w_RSFLPROV;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      this.w_TOTDOC = this.w_TOTDOC + this.w_RSIMPRAT
      this.w_MINRATA = MIN(this.w_MINRATA, this.w_RSIMPRAT)
    endif
    NEXT
    if this.w_TOTDOC<0
      * --- Documento non Accettato
      this.w_oERRORLOG.AddMsgLogPartNoTrans(space(5), "%0Bloccante:%0")     
      this.w_oERRORLOG.AddMsgLog("Impossibile generare doc.n.: %1 del %2 - importo totale negativo", Alltrim(STR(this.oParentObject.w_MVNUMDOC,15,0))+" "+Alltrim(this.oParentObject.w_MVALFDOC), DTOC(this.w_MVDATDOC))     
      this.w_ERRDOC = .T.
      this.oParentObject.w_MVNUMDOC = this.oParentObject.w_MVNUMDOC - 1
      * --- Raise
      i_Error="Errore"
      return
    endif
    if this.w_MINRATA<0
      * --- Documento non Accettato
      this.w_oERRORLOG.AddMsgLogPartNoTrans(space(5), "%0Warning:%0")     
      this.w_oERRORLOG.AddMsgLog("Verificare doc.n.: %1 del %2 - rate con importo negativo", Alltrim(STR(this.oParentObject.w_MVNUMDOC,15,0))+" "+Alltrim(this.oParentObject.w_MVALFDOC), DTOC(this.w_MVDATDOC))     
      this.w_NWARN = this.w_NWARN + 1
    endif
    if this.oParentObject.w_FLRAGG<>"S"
      this.w_OKDOC = this.w_OKDOC + 1
    endif
    * --- Imposta Limiti per stampa Differite
    this.w_NUMINI = MIN(this.oParentObject.w_MVNUMDOC, this.w_NUMINI)
    this.w_NUMFIN = MAX(this.oParentObject.w_MVNUMDOC, this.w_NUMFIN)
    this.w_DATAIN = MIN(this.w_MVDATDOC, this.w_DATAIN)
    this.w_DATAFI = MAX(this.w_MVDATDOC, this.w_DATAFI)
    this.w_DETNUM = this.w_DETNUM + 1
    this.w_ORDNUM = this.w_ORDNUM + 10
    * --- Insert into DET_DIFF
    i_nConn=i_TableProp[this.DET_DIFF_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DET_DIFF_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DET_DIFF_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"DPSERIAL"+",CPROWNUM"+",CPROWORD"+",DPSERDOC"+",DPNUMDOC"+",DPALFDOC"+",DPDATDOC"+",DPTIPCLF"+",DPCODCLF"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MVRIFFAD),'DET_DIFF','DPSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DETNUM),'DET_DIFF','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ORDNUM),'DET_DIFF','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVSERIAL),'DET_DIFF','DPSERDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVNUMDOC),'DET_DIFF','DPNUMDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVALFDOC),'DET_DIFF','DPALFDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVDATDOC),'DET_DIFF','DPDATDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVTIPCON),'DET_DIFF','DPTIPCLF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODCON),'DET_DIFF','DPCODCLF');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'DPSERIAL',this.w_MVRIFFAD,'CPROWNUM',this.w_DETNUM,'CPROWORD',this.w_ORDNUM,'DPSERDOC',this.oParentObject.w_MVSERIAL,'DPNUMDOC',this.oParentObject.w_MVNUMDOC,'DPALFDOC',this.oParentObject.w_MVALFDOC,'DPDATDOC',this.w_MVDATDOC,'DPTIPCLF',this.oParentObject.w_MVTIPCON,'DPCODCLF',this.w_MVCODCON)
      insert into (i_cTable) (DPSERIAL,CPROWNUM,CPROWORD,DPSERDOC,DPNUMDOC,DPALFDOC,DPDATDOC,DPTIPCLF,DPCODCLF &i_ccchkf. );
         values (;
           this.w_MVRIFFAD;
           ,this.w_DETNUM;
           ,this.w_ORDNUM;
           ,this.oParentObject.w_MVSERIAL;
           ,this.oParentObject.w_MVNUMDOC;
           ,this.oParentObject.w_MVALFDOC;
           ,this.w_MVDATDOC;
           ,this.oParentObject.w_MVTIPCON;
           ,this.w_MVCODCON;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
  endproc


  procedure Page_8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Raggruppa per Articolo
    * --- Salva il Contenuto in Cursore appoggio
    Select *, 9999*0 AS RIGRIF, 9999*0 AS RIFROW FROM GeneApp INTO CURSOR AppRagg NoFilter ReadWrite
    * --- Quindi lo Ricostruisce raggruppando tutti gi Articoli Congruenti.
    *     Come primo passo recupero le righe descrittive...
     Select AppRagg.*, 0*9999999 As PROGRIGA; 
 From AppRagg Where t_MVTIPRIG="D" Into Cursor GeneApp NoFilter ReadWrite
    this.w_TOTMERCE = 0
    this.w_PROGRIGA = 1
    this.w_UPDA = .F.
     
 Select AppRagg 
 Go Top
    SCAN FOR NOT EMPTY(t_MVTIPRIG) And t_MVTIPRIG<>"D"
    this.w_FLINSE = .T.
    this.w_MVTIPRIG = t_MVTIPRIG
    this.w_MVCODICE = t_MVCODICE
    this.w_MVCODART = t_MVCODART
    this.w_MVDESART = t_MVDESART
    this.w_MVDESSUP = t_MVDESSUP
    this.w_MVUNIMIS = t_MVUNIMIS
    this.w_MVCATCON = t_MVCATCON
    this.w_MVCONTRO = t_MVCONTRO
    this.w_MVCODCLA = t_MVCODCLA
    this.w_MVCONTRA = t_MVCONTRA
    this.w_MVCODLIS = t_MVCODLIS
    this.w_MVCONIND = t_MVCONIND
    this.w_MVQTAMOV = t_MVQTAMOV
    this.w_MVQTAUM1 = t_MVQTAUM1
    this.w_MVPREZZO = t_MVPREZZO
    this.w_MVSCONT1 = t_MVSCONT1
    this.w_MVSCONT2 = t_MVSCONT2
    this.w_MVSCONT3 = t_MVSCONT3
    this.w_MVSCONT4 = t_MVSCONT4
    this.w_MVFLOMAG = t_MVFLOMAG
    this.w_MVCODIVA = t_MVCODIVA
    this.w_PERIVA = t_PERIVA
    this.w_BOLIVA = t_BOLIVA
    this.w_MVVALRIG = t_MVVALRIG
    this.w_MVPERPRO = t_MVPERPRO
    this.w_MVIMPPRO = t_MVIMPPRO
    this.w_MVSERRIF = t_MVSERRIF
    this.w_MVROWRIF = t_MVROWRIF
    this.w_MVPESNET = t_MVPESNET
    this.w_MVFLTRAS = t_MVFLTRAS
    this.w_MVNOMENC = t_MVNOMENC
    this.w_MVUMSUPP = t_MVUMSUPP
    this.w_MVMOLSUP = t_MVMOLSUP
    this.w_MVNUMCOL = t_MVNUMCOL
    this.w_MVIMPACC = 0
    this.w_MVIMPSCO = 0
    this.w_MVVALMAG = 0
    this.w_MVIMPNAZ = 0
    this.w_MVNAZPRO = t_MVNAZPRO
    this.w_MVPROORD = t_MVPROORD
    this.w_MVCODCEN = t_MVCODCEN
    this.w_MVVOCCEN = t_MVVOCCEN
    this.w_MVCODCOM = t_MVCODCOM
    this.w_MVCODATT = t_MVCODATT
    this.w_MVINICOM = t_MVINICOM
    this.w_MVFINCOM = t_MVFINCOM
    this.w_MVFLARIF = t_MVFLARIF
    this.w_MVIMPEVA = t_MVIMPEVA
    this.w_MVFLERIF = t_MVFLERIF
    this.w_FLSERA = t_FLSERA
    this.w_MESS = t_MESS
    this.w_SERORI = t_MVSERORI
    this.w_ROWORI = t_MVNUMORI
    this.w_MVTIPPRO = t_MVTIPPRO
    this.w_MVTIPPR2 = t_MVTIPPR2
    this.w_MVPROCAP = t_MVPROCAP
    this.w_MVIMPCAP = t_MVIMPCAP
    this.w_MVCODCES = t_MVCODCES
    this.w_MVIMPCOM = 0
    this.w_DOQTAEVA = t_DOQTAEVA
    this.w_DOIMPEVA = t_DOIMPEVA
    this.w_MVFLNOAN = t_MVFLNOAN
    this.w_TDORDAPE = t_TDORDAPE
    this.w_DATEVA = t_ORAPDATGEN
    this.w_NEXTFAT = t_ORAPDATEVA
    this.w_MVCODRES = t_MVCODRES
    this.w_TESTESPLACC = t_TESTESPLACC
    * --- Nel caso di raggruppamento eventuali riferimenti aicontributi accessori
    *     vanno persi
    this.w_CACONT = Nvl(t_MVCACONT ,Space(5))
    this.w_RIFCAC = 0 
    if this.w_MVTIPRIG $ "RM"
       
 Select GeneApp 
 Go Top
      if this.w_TDORDAPE = "S"
        * --- Ordine aperto inserisco sempre una nuova riga
        LOCATE FOR 1=0
      else
         
 LOCATE FOR t_MVTIPRIG= this.w_MVTIPRIG AND t_MVCODICE= this.w_MVCODICE AND t_MVCODART= this.w_MVCODART AND ; 
 t_MVDESART= this.w_MVDESART AND t_MVUNIMIS= this.w_MVUNIMIS AND t_MVCATCON= this.w_MVCATCON AND t_MVCONTRO= this.w_MVCONTRO AND ; 
 t_MVCODCLA= this.w_MVCODCLA AND t_MVCONIND= this.w_MVCONIND AND t_MVPREZZO= this.w_MVPREZZO AND ; 
 t_MVSCONT1= this.w_MVSCONT1 AND t_MVSCONT2= this.w_MVSCONT2 AND t_MVSCONT3= this.w_MVSCONT3 AND t_MVSCONT4= this.w_MVSCONT4 AND ; 
 t_MVFLOMAG= this.w_MVFLOMAG AND t_MVCODIVA= this.w_MVCODIVA AND t_MVPERPRO= this.w_MVPERPRO AND ; 
 t_MVCODCEN= this.w_MVCODCEN AND t_MVVOCCEN= this.w_MVVOCCEN AND t_MVCODCOM= this.w_MVCODCOM AND t_MVTIPPRO= this.w_MVTIPPRO ; 
 AND t_MVCODCES= this.w_MVCODCES AND t_MVTIPPR2= this.w_MVTIPPR2 AND t_MVPROCAP= this.w_MVPROCAP And Nvl(t_MVCACONT,Space(5))=this.w_CACONT; 
 AND Nvl(t_MVCODATT,Space(15))= this.w_MVCODATT
      endif
      if Found()
        * --- Congruita' dati INTRA
        if this.oParentObject.w_CCTIPDOC $ "FE-NE"
          this.w_FLINSE = t_MVFLTRAS<>this.w_MVFLTRAS OR t_MVNOMENC<>this.w_MVNOMENC OR t_MVUMSUPP<>this.w_MVUMSUPP OR t_MVMOLSUP<>this.w_MVMOLSUP OR t_MVNAZPRO<>this.w_MVNAZPRO OR t_MVPROORD<>this.w_MVPROORD
        else
          this.w_FLINSE = .F.
        endif
      endif
    endif
    if this.w_FLINSE
      * --- Inserisce nuova Riga
       
 INSERT INTO GeneApp ; 
 (t_MVTIPRIG, t_MVCODICE, t_MVCODART, ; 
 t_MVDESART, t_MVDESSUP, t_MVUNIMIS, t_MVCATCON, ; 
 t_MVCONTRO, t_MVCODCLA, t_MVCONTRA, t_MVCODLIS, ; 
 t_MVQTAMOV, t_MVQTAUM1, t_MVPREZZO, ; 
 t_MVSCONT1, t_MVSCONT2, t_MVSCONT3, t_MVSCONT4, ; 
 t_MVFLOMAG, t_MVCODIVA, t_PERIVA, t_BOLIVA, t_MVVALRIG, t_MVPERPRO, ; 
 t_MVIMPPRO, t_MVSERRIF, t_MVROWRIF, ; 
 t_MVPESNET, t_MVFLTRAS, t_MVNOMENC, t_MVUMSUPP, ; 
 t_MVMOLSUP, t_MVNUMCOL, t_MVCONIND, ; 
 t_MVNAZPRO, t_MVPROORD, t_MVCODCEN, t_MVVOCCEN, ; 
 t_MVCODCOM, t_MVINICOM, t_MVFINCOM, ; 
 t_MVIMPACC, t_MVIMPSCO, t_MVVALMAG, t_MVIMPNAZ,t_MVFLARIF, t_MVIMPEVA, ; 
 t_MESS, t_MVSERORI, t_MVNUMORI, t_MVFLERIF, t_FLSERA,t_MVTIPPRO, t_MVCODCES, t_MVIMPCOM,; 
 t_MVTIPPR2, t_MVPROCAP, t_MVIMPCAP,t_MVFLNOAN, PROGRIGA, t_ORAPDATEVA,t_ORAPDATGEN, t_TDORDAPE, t_MVCODRES, t_TESTESPLACC, t_MVRIFCAC, t_MVCACONT, t_MVFLORCO,t_MVCODATT,t_DOQTAEVA, t_DOIMPEVA) ; 
 VALUES ( this.w_MVTIPRIG, this.w_MVCODICE, this.w_MVCODART, ; 
 this.w_MVDESART, this.w_MVDESSUP, this.w_MVUNIMIS, this.w_MVCATCON, ; 
 this.w_MVCONTRO, this.w_MVCODCLA, this.w_MVCONTRA, this.w_MVCODLIS, ; 
 this.w_MVQTAMOV, this.w_MVQTAUM1, this.w_MVPREZZO, ; 
 this.w_MVSCONT1, this.w_MVSCONT2, this.w_MVSCONT3, this.w_MVSCONT4, ; 
 this.w_MVFLOMAG, this.w_MVCODIVA, this.w_PERIVA, this.w_BOLIVA, this.w_MVVALRIG, this.w_MVPERPRO, ; 
 this.w_MVIMPPRO, this.w_MVSERRIF, this.w_MVROWRIF, ; 
 this.w_MVPESNET, this.w_MVFLTRAS, this.w_MVNOMENC, this.w_MVUMSUPP, ; 
 this.w_MVMOLSUP, this.w_MVNUMCOL, this.w_MVCONIND, ; 
 this.w_MVNAZPRO, this.w_MVPROORD, this.w_MVCODCEN, this.w_MVVOCCEN, ; 
 this.w_MVCODCOM, this.w_MVINICOM, this.w_MVFINCOM, ; 
 this.w_MVIMPACC, this.w_MVIMPSCO, this.w_MVVALMAG, this.w_MVIMPNAZ, this.w_MVFLARIF, this.w_MVIMPEVA, ; 
 this.w_MESS, this.w_SERORI, this.w_ROWORI, this.w_MVFLERIF, this.w_FLSERA, this.w_MVTIPPRO, this.w_MVCODCES, this.w_MVIMPCOM,; 
 this.w_MVTIPPR2, this.w_MVPROCAP, this.w_MVIMPCAP, this.w_MVFLNOAN, this.w_PROGRIGA, this.w_NEXTFAT , this.w_DATEVA, this.w_TDORDAPE, this.w_MVCODRES, this.w_TESTESPLACC, this.w_RIFCAC, this.w_CACONT," ",this.w_MVCODATT, this.w_DOQTAEVA, this.w_DOIMPEVA)
      this.w_PROGRIGA = this.w_PROGRIGA + 1
    else
      * --- Aggiunge alla Riga
      REPLACE t_MVQTAMOV WITH (t_MVQTAMOV + this.w_MVQTAMOV), t_MVQTAUM1 WITH (t_MVQTAUM1 + this.w_MVQTAUM1)
      REPLACE t_MVIMPPRO WITH (t_MVIMPPRO + this.w_MVIMPPRO), t_MVIMPCAP WITH (t_MVIMPCAP + this.w_MVIMPCAP), t_MVNUMCOL WITH (t_MVNUMCOL + this.w_MVNUMCOL)
      * --- Memorizzo la parte di MVVALRIG aggiunta alla riga (x calcolare al termine w_TOTMERCE)
      this.w_MVVALRIG = CAVALRIG(t_MVPREZZO,t_MVQTAMOV, t_MVSCONT1, t_MVSCONT2, t_MVSCONT3, t_MVSCONT4,this.w_DECTOT) - t_MVVALRIG
      REPLACE t_MVVALRIG WITH t_MVVALRIG + this.w_MVVALRIG
      this.w_UPDA = .T.
    endif
    if this.w_MVTIPRIG $ "RM"
      * --- Segna Riferimento Riga del Documento che Verra' generato
      this.w_APPO1 = GeneApp.PROGRIGA
       
 SELECT AppRagg 
 REPLACE RIGRIF WITH this.w_APPO1
    endif
    this.w_TOTMERCE = this.w_TOTMERCE + IIF(this.w_MVFLOMAG="X" AND this.w_FLSERA<>"S", this.w_MVVALRIG, 0)
    SELECT AppRagg
    ENDSCAN
    if this.w_UPDA
      * --- Riordino il cursore per avere gli articoli ordinati in ordine alfabetico e poi rendo il cursore Scrivibile
      *     Se attivo il modulo contributi accessori riporto come ultimi i contributi accessori
      SELECT *, iif( g_COAC="S" And Not Empty( t_MVCACONT ) , "Z" , t_MVTIPRIG) As OrdTipRig From GeneApp Into Cursor GeneApp Order By OrdTipRig , t_MVCODICE NoFilter Readwrite
    endif
  endproc


  procedure Page_9
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica se il Numero Documento e' Inseribile
    this.w_LOOP = .T.
    * --- Ciclo finch� non trovo il primo numero libero
    do while this.w_LOOP
      * --- Controllo se w_MVNUMDOC � gi� presente in TMPVEND2
      * --- Read from TMPVEND2
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.TMPVEND2_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND2_idx,2],.t.,this.TMPVEND2_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "NUMDOC"+;
          " from "+i_cTable+" TMPVEND2 where ";
              +"NUMDOC = "+cp_ToStrODBC(this.oParentObject.w_MVNUMDOC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          NUMDOC;
          from (i_cTable) where;
              NUMDOC = this.oParentObject.w_MVNUMDOC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_NUMDOC = NVL(cp_ToDate(_read_.NUMDOC),cp_NullValue(_read_.NUMDOC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if i_rows = 1
        * --- Trovato un documento con lo stesso numero provo il numero successivo
        this.oParentObject.w_MVNUMDOC = this.w_NUMDOC + 1
      else
        * --- w_MVNUMDOC non � presente quindi ho trovato il primo numero libero
        this.w_LOOP = .F.
      endif
    enddo
    if this.oParentObject.w_MVNUMDOC = this.w_LAST + 1 AND this.oParentObject.w_MVNUMDOC <= this.w_PRODOC
      * --- Non ho pi� buchi liberi il documento non pu� essere inserito
      this.w_OKINS = .F.
    else
      * --- Il numero che si vuole inserire e' maggiore di tutti, di qui in avanti assegno in base alla Tabella Progressivi
      this.w_OKINS = IIF(this.oParentObject.w_MVNUMDOC<=this.w_PRODOC, .T., .F.)
    endif
  endproc


  procedure Page_10
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo Multiutenza: per impedire di evadere lo stesso documento 2 volte
    * --- Read from DOC_DETT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MVQTAEVA,MVIMPEVA,MVFLEVAS"+;
        " from "+i_cTable+" DOC_DETT where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_MULSER);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_MULROW);
            +" and MVNUMRIF = "+cp_ToStrODBC(this.w_MULRIF);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MVQTAEVA,MVIMPEVA,MVFLEVAS;
        from (i_cTable) where;
            MVSERIAL = this.w_MULSER;
            and CPROWNUM = this.w_MULROW;
            and MVNUMRIF = this.w_MULRIF;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DLQTAEVA = NVL(cp_ToDate(_read_.MVQTAEVA),cp_NullValue(_read_.MVQTAEVA))
      this.w_DLIMPEVA = NVL(cp_ToDate(_read_.MVIMPEVA),cp_NullValue(_read_.MVIMPEVA))
      this.w_DOFLEVAS = NVL(cp_ToDate(_read_.MVFLEVAS),cp_NullValue(_read_.MVFLEVAS))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Variabili DO... Valori dei documenti (DDT) da generare derivanti dalla query.
    *     Variabili DL.... Valori dei documenti (DDT) da generare al momento della Evasione
    *     Se Diversi significa che sono gi� stati Evasi
    if this.w_DOQTAEVA <> this.w_DLQTAEVA Or this.w_DOIMPEVA <> this.w_DLIMPEVA Or this.w_DOFLEVAS = "S"
      * --- Blocco Multi Utenza
      * --- Read from DOC_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MVDATDOC,MVTIPDOC,MVNUMDOC,MVALFDOC"+;
          " from "+i_cTable+" DOC_MAST where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_MULSER);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MVDATDOC,MVTIPDOC,MVNUMDOC,MVALFDOC;
          from (i_cTable) where;
              MVSERIAL = this.w_MULSER;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DDOC = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
        this.w_TIPDOC = NVL(cp_ToDate(_read_.MVTIPDOC),cp_NullValue(_read_.MVTIPDOC))
        this.w_NDOC = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
        this.w_ADOC = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_oERRORLOG.AddMsgLogPartNoTrans(space(5), "%0Bloccante:%0")     
      this.w_oERRORLOG.AddMsgLog("Impossibile generare doc.: %1 n.: %2 del %3%0Gi� evaso da un altro utente", this.w_TIPDOC, Alltrim(STR(this.w_NDOC,15))+ IIF(Not Empty(this.w_ADOC),"/"+Alltrim(this.w_ADOC),""), DTOC(this.w_DDOC))     
      this.w_ERRDOC = .T.
      if this.w_RAGG
        * --- Nel caso di fatturazione raggruppata per articolo, cancello i riferimenti
        *     ai DDT da generare e quelli delle Fatture generate dal cursore di appoggio.
        *     Altrimenti potrebbe generarle al giro successivo
        Delete from AppRagg where t_MVSERRIF = this.w_FRSERDDT
      endif
      * --- Documento non Accettato (Gi� Evaso da Generazione Simultanea)
      * --- Raise
      i_Error="Errore"
      return
    endif
  endproc


  procedure Page_11
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se Fatturazione per Ordine genero la Riga descrittiva del Riferimento all'Ordine
    * --- Read from CONTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ANCODLIN"+;
        " from "+i_cTable+" CONTI where ";
            +"ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_MVTIPCON);
            +" and ANCODICE = "+cp_ToStrODBC(this.w_MVCODCON);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ANCODLIN;
        from (i_cTable) where;
            ANTIPCON = this.oParentObject.w_MVTIPCON;
            and ANCODICE = this.w_MVCODCON;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODLIN = NVL(cp_ToDate(_read_.ANCODLIN),cp_NullValue(_read_.ANCODLIN))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.oParentObject.w_FLNSRI="S"
       
 DIMENSION ARPARAM[11,2] 
 ARPARAM[1,1]="NUMDOC" 
 ARPARAM[1,2]=ALLTRIM(STR(this.w_ORNUMDOC,15)) 
 ARPARAM[2,1]="ALFDOC" 
 ARPARAM[2,2]=Alltrim(this.w_ORALFDOC) 
 ARPARAM[3,1]="DATDOC" 
 ARPARAM[3,2]=DTOC(CP_TODATE(this.w_ORDATDOC)) 
 ARPARAM[4,1]="ALFPRO" 
 ARPARAM[4,2]=Alltrim(this.w_ARIF) 
 ARPARAM[5,1]="DATPRO" 
 ARPARAM[5,2]=DTOC(this.w_DRIF) 
 ARPARAM[6,1]="CODCON" 
 ARPARAM[6,2]=ALLTRIM(this.w_MVCODCON) 
 ARPARAM[7,1]="ELEDOC" 
 ARPARAM[7,2]="" 
 ARPARAM[8,1]="CODAGE" 
 ARPARAM[8,2]=ALLTRIM(this.w_MVCODAGE) 
 ARPARAM[9,1]="NUMPRO" 
 ARPARAM[9,2]= ALLTRIM(STR(this.w_NRIF,15,0)) 
 ARPARAM[10,1]="NUMPAR" 
 ARPARAM[10,2]="" 
 ARPARAM[11,1]="DATSCA" 
 ARPARAM[11,2]=""
       
 DIMENSION ARPARAM2[2] 
 ARPARAM2[1]=ALLTRIM(this.w_ORTIPDOC) 
 ARPARAM2[2]=ALLTRIM(STR(this.w_ORNUMDOC,15))+IIF(EMPTY(this.w_ORALFDOC),"", "/"+Alltrim(this.w_ORALFDOC))+SPACE(1)+DTOC(CP_TODATE(this.w_ORDATDOC))
      * --- Scrive nuova Riga Descrittiva sul Temporaneo di Appoggio
      this.w_MVDESART = CALRIFDES("nsd",this.w_CODLIN, this.w_MODRIF, @ARPARAM, this.w_ORDESRIF, @ARPARAM2)
      * --- sbianco la var perch� potrebbe essere sporca degli assegnamenti precedenti
      this.w_DESSUP = ""
      if len(alltrim(this.w_MVDESART)) >40
        * --- Se la lunghezza del Riferimento descrittivo � maggiore di 30 caratteri,
        *     inserisco nella descrizione articolo MVDESART i caratteri fino all'ultima occorrenza 
        *     dello spazio nei primi 30 caratteri.
        *     Il resto viene inserito nella descrizione supplementare.
        this.w_DESSUP = SUBSTR(this.w_MVDESART,(RATC(" ",LEFT(this.w_MVDESART,40))+1))
        this.w_MVDESART = LEFT(this.w_MVDESART, RATC(" ", LEFT(this.w_MVDESART,40)))
      endif
       
 INSERT INTO GeneApp (t_MVTIPRIG, t_MVCODICE, t_MVCODART, t_MVDESART, t_MVDESSUP, t_MVSERRIF, t_MVCODCLA, t_MESS) ; 
 VALUES ("D", g_ARTDES, this.w_ARTDES, this.w_MVDESART, this.w_DESSUP, Space(10), this.oParentObject.w_TPNSRI, "+++")
    endif
    * --- Vostro Riferimento
    if this.oParentObject.w_FLVSRI="S" AND this.w_NRIF<>0
       
 DIMENSION ARPARAM[11,2] 
 ARPARAM[1,1]="NUMDOC" 
 ARPARAM[1,2]="" 
 ARPARAM[2,1]="ALFDOC" 
 ARPARAM[2,2]="" 
 ARPARAM[3,1]="DATDOC" 
 ARPARAM[3,2]="" 
 ARPARAM[4,1]="ALFPRO" 
 ARPARAM[4,2]=Alltrim(this.w_ARIF) 
 ARPARAM[5,1]="DATPRO" 
 ARPARAM[5,2]=DTOC(this.w_DRIF) 
 ARPARAM[6,1]="CODCON" 
 ARPARAM[6,2]=ALLTRIM(this.w_MVCODCON) 
 ARPARAM[7,1]="ELEDOC" 
 ARPARAM[7,2]="" 
 ARPARAM[8,1]="CODAGE" 
 ARPARAM[8,2]=ALLTRIM(this.w_MVCODAGE) 
 ARPARAM[9,1]="NUMPRO" 
 ARPARAM[9,2]= ALLTRIM(STR(this.w_NRIF,15)) 
 ARPARAM[10,1]="NUMPAR" 
 ARPARAM[10,2]="" 
 ARPARAM[11,1]="DATSCA" 
 ARPARAM[11,2]=""
       
 DIMENSION ARPARAM2[2] 
 ARPARAM2[1]="N" 
 ARPARAM2[2]=ALLTRIM(STR(this.w_NRIF,15))+IIF(EMPTY(this.w_ARIF),"", "/"+Alltrim(this.w_ARIF)) +SPACE(1)+DTOC(this.w_DRIF) 
      * --- Scrive nuova Riga Descrittiva sul Temporaneo di Appoggio
      this.w_MVDESART = CALRIFDES("vsd",this.w_CODLIN, this.w_MODRIF, @ARPARAM,"", @ARPARAM2)
      * --- sbianco la var perch� potrebbe essere sporca degli assegnamenti precedenti
      this.w_DESSUP = ""
      if len(alltrim(this.w_MVDESART)) >40
        * --- Se la lunghezza del Riferimento descrittivo � maggiore di 30 caratteri,
        *     inserisco nella descrizione articolo MVDESART i caratteri fino all'ultima occorrenza 
        *     dello spazio nei primi 30 caratteri.
        *     Il resto viene inserito nella descrizione supplementare.
        this.w_DESSUP = SUBSTR(this.w_MVDESART,(RATC(" ",LEFT(this.w_MVDESART,40))+1))
        this.w_MVDESART = LEFT(this.w_MVDESART, RATC(" ", LEFT(this.w_MVDESART,40)))
      endif
       
 INSERT INTO GeneApp (t_MVTIPRIG, t_MVCODICE, t_MVCODART, t_MVDESART, t_MVDESSUP, t_MVSERRIF, t_MVCODCLA, t_MESS) ; 
 VALUES ("D", g_ARTDES, this.w_ARTDES, this.w_MVDESART, this.w_DESSUP,Space(10), this.oParentObject.w_TPVSRI, "+++")
    endif
    * --- Rif. Descrittivo
    if this.oParentObject.w_FLRIDE="S"
       
 DIMENSION ARPARAM[11,2] 
 ARPARAM[1,1]="NUMDOC" 
 ARPARAM[1,2]=ALLTRIM(STR(this.w_ORNUMDOC,15)) 
 ARPARAM[2,1]="ALFDOC" 
 ARPARAM[2,2]=Alltrim(this.w_ORALFDOC) 
 ARPARAM[3,1]="DATDOC" 
 ARPARAM[3,2]=DTOC(CP_TODATE(this.w_ORDATDOC)) 
 ARPARAM[4,1]="ALFPRO" 
 ARPARAM[4,2]=Alltrim(this.w_ARIF) 
 ARPARAM[5,1]="DATPRO" 
 ARPARAM[5,2]=DTOC(this.w_DRIF) 
 ARPARAM[6,1]="CODCON" 
 ARPARAM[6,2]=ALLTRIM(this.w_MVCODCON) 
 ARPARAM[7,1]="ELEDOC" 
 ARPARAM[7,2]="" 
 ARPARAM[8,1]="CODAGE" 
 ARPARAM[8,2]=ALLTRIM(this.w_MVCODAGE) 
 ARPARAM[9,1]="NUMPRO" 
 ARPARAM[9,2]= ALLTRIM(STR(this.w_NRIF,15,0)) 
 ARPARAM[10,1]="NUMPAR" 
 ARPARAM[10,2]="" 
 ARPARAM[11,1]="DATSCA" 
 ARPARAM[11,2]=""
      this.w_MEMO = ""
      this.w_MEMO = this.w_RIFCLI+" "
      this.w_MEMO = IIF(NOT EMPTY(this.w_MEMO), "R.( "+ALLTRIM(this.w_ORTIPDOC) + ") "+ this.w_MEMO, CALRIFDES("dsd",this.w_CODLIN, this.w_MODRIF, @ARPARAM, this.w_ORDESRIF,.F.) )
      if len(alltrim(this.w_MEMO)) >40
        * --- Se la lunghezza del Riferimento descrittivo � maggiore di 30 caratteri,
        *     inserisco nella descrizione articolo MVDESART i caratteri fino all'ultima occorrenza 
        *     dello spazio nei primi 30 caratteri.
        *     Il resto viene inserito nella descrizione supplementare.
        this.w_RIFCLI = LEFT(this.w_MEMO, RATC(" ", LEFT(this.w_MEMO,40)))
        this.w_MVDESSUP = SUBSTR(this.w_MEMO,(RATC(" ",LEFT(this.w_MEMO,40))+1))
      else
        * --- Se il riferimento � pi� corto di 30 caratteri, inserisco tutto nella descrizione
        this.w_RIFCLI = ALLTRIM(this.w_MEMO)
        this.w_MVDESSUP = SPACE(10)
      endif
      if NOT EMPTY(ALLTRIM(this.w_RIFCLI))
        this.w_MVDESART = ALLTRIM(this.w_RIFCLI)
         
 INSERT INTO GeneApp (t_MVTIPRIG, t_MVCODICE, t_MVCODART, t_MVDESART, t_MVDESSUP, t_MVSERRIF, t_MVCODCLA, t_MESS) ; 
 VALUES ("D", g_ARTDES, this.w_ARTDES, this.w_MVDESART, this.w_MVDESSUP, SPACE(10), this.oParentObject.w_TPRDES, "+++")
      endif
    endif
  endproc


  procedure Page_12
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Dato il cursore del dettaglio esplode riga per riga i contributi di riga e
    *     sul cursore determina il dettaglio per l'esplosione a peso
    * --- Se la causale di origine non gestice i contributi accessori esplodo
    *     i contributi di riga
    if this.w_OFLAPCA="N"
      Select GeneApp 
 Go Top
      do while Not Eof( "GeneApp" )
        * --- L'ultimo carattere contiene il check contributi accessori dell'articolo
        if Right(Nvl(GeneApp.t_TESTESPLACC ,"NNN"),1)="S"
          this.w_RIFCACESP = GeneApp.t_MVROWRIF
          * --- Verifico se la riga ha all'interno del temporaneo una riga di attributi che
          *     la riferisce, per cui ricerco una riga che ha il medesimo MVSERRIF
          *     e il  MVRIFCAC uguale a MVROWRIF 
          this.w_SRCSERRIF = GeneApp.t_MVSERRIF
          this.w_ATTPOS = Recno("GeneApp") 
          Locate For t_MVSERRIF=this.w_SRCSERRIF And t_MVRIFCAC=this.w_RIFCACESP
          if Not Found()
            Select GeneApp 
 Go this.w_ATTPOS
            * --- Come riferimento passo il numero di riga dell'articolo che provoca
            *     l'esplosione
            this.w_UNIMIS = GeneApp.t_MVUNIMIS
            * --- Read from UNIMIS
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.UNIMIS_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "UMFLFRAZ"+;
                " from "+i_cTable+" UNIMIS where ";
                    +"UMCODICE = "+cp_ToStrODBC(this.w_UNIMIS);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                UMFLFRAZ;
                from (i_cTable) where;
                    UMCODICE = this.w_UNIMIS;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_COFRAZ = NVL(cp_ToDate(_read_.UMFLFRAZ),cp_NullValue(_read_.UMFLFRAZ))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            GSAR_BEG(this,GeneApp.t_MVCODART, this.oParentObject.w_MVTIPCON, this.w_MVCODCON , this.w_MVDATDOC, GeneApp.t_MVCODIVA,GeneApp.t_MVUNIMIS ,GeneApp.t_MVQTAMOV, GeneApp.t_MVPREZZO, this.w_MVCODVAL, IIF(this.w_MVFLSCOR="S" ,GeneApp.t_PERIVA ,0), this.w_COFRAZ, "ACC", "CURESPL")
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if Used( "CURESPL")
              this.w_FATHERRIF = geneapp.t_MVSERRIF
              this.Page_13()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          else
            Select GeneApp 
 Go this.w_ATTPOS
          endif
        endif
        Select GeneApp 
 Skip
      enddo
    endif
    * --- In contributi a peso li esplodo sempre
    *     
    *     Alla routine devo passare un cursore che contiene il dettaglio
    *     del documento appena caricato
    this.w_RIFCACESP = -1
    GSAR_BGA(this,"GEN", This , 0 , 0 , 10 , "I")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.Page_13()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Page_13
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if Used("CurEspl")
      Select CurEspl 
 Go top
      this.w_POSAPP = RecNo("GeneApp")+IIf( Eof( "GeneApp" ) ,-1 ,0 )
      this.w_CPROWORD = 1
      do while (.not. eof("CurEspl"))
        * --- RIcostruisco tutti i dati di riga da quanto restituito dal cursore di esplosione..
        this.w_KCODICE = CURESPL.CODICE
        this.w_KUNIMIS = CURESPL.UNIMIS
        this.w_KQTAMOV = CURESPL.QTAMOV
        this.w_KPREZZO = CURESPL.PREZZO
        this.w_KCODIVA = CURESPL.CODIVA
        this.w_CONTRIB = CURESPL.CATCON
        * --- Read from KEY_ARTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CA__TIPO,CACODART,CADESSUP,CADESART"+;
            " from "+i_cTable+" KEY_ARTI where ";
                +"CACODICE = "+cp_ToStrODBC(this.w_KCODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CA__TIPO,CACODART,CADESSUP,CADESART;
            from (i_cTable) where;
                CACODICE = this.w_KCODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MVTIPRIG = NVL(cp_ToDate(_read_.CA__TIPO),cp_NullValue(_read_.CA__TIPO))
          this.w_MVCODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
          this.w_MVDESSUP = NVL(cp_ToDate(_read_.CADESSUP),cp_NullValue(_read_.CADESSUP))
          this.w_MVDESART = NVL(cp_ToDate(_read_.CADESART),cp_NullValue(_read_.CADESART))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARNOMENC,ARUMSUPP,ARMOLSUP,ARPESNET,ARCATCON,ARCODCLA"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_MVCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARNOMENC,ARUMSUPP,ARMOLSUP,ARPESNET,ARCATCON,ARCODCLA;
            from (i_cTable) where;
                ARCODART = this.w_MVCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MVNOMENC = NVL(cp_ToDate(_read_.ARNOMENC),cp_NullValue(_read_.ARNOMENC))
          this.w_MVUMSUPP = NVL(cp_ToDate(_read_.ARUMSUPP),cp_NullValue(_read_.ARUMSUPP))
          this.w_MVMOLSUP = NVL(cp_ToDate(_read_.ARMOLSUP),cp_NullValue(_read_.ARMOLSUP))
          this.w_MVPESNET = NVL(cp_ToDate(_read_.ARPESNET),cp_NullValue(_read_.ARPESNET))
          this.w_MVCATCON = NVL(cp_ToDate(_read_.ARCATCON),cp_NullValue(_read_.ARCATCON))
          this.w_MVCODCLA = NVL(cp_ToDate(_read_.ARCODCLA),cp_NullValue(_read_.ARCODCLA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from VOCIIVA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VOCIIVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "IVPERIND,IVPERIVA,IVBOLIVA"+;
            " from "+i_cTable+" VOCIIVA where ";
                +"IVCODIVA = "+cp_ToStrODBC(this.w_KCODIVA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            IVPERIND,IVPERIVA,IVBOLIVA;
            from (i_cTable) where;
                IVCODIVA = this.w_KCODIVA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_INDIVA = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
          this.w_PERIVA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
          this.w_BOLIVA = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_MVVALRIG = CAVALRIG(this.w_KPREZZO,this.w_KQTAMOV, 0,0,0,0,this.w_DECTOT)
        this.w_MVVALMAG = CAVALMAG(this.w_MVFLSCOR, this.w_MVVALRIG, 0, 0, this.w_PERIVA, this.w_DECTOT, this.w_MVCODIVE, this.w_PERIVE )
        this.w_MVIMPNAZ = CAIMPNAZ(this.w_MVFLVEAC, this.w_MVVALMAG, this.w_MVCAOVAL, this.w_CAONAZ, this.w_MVDATDOC, this.w_MVVALNAZ, this.w_MVCODVAL, this.w_MVCODIVE, this.w_PERIVE, this.w_INDIVE, this.w_PERIVA, this.w_INDIVA )
        this.w_MVFLTRAS = " "
        this.w_MVCODCEN = Space(15)
        this.w_MVVOCCEN = Space(15)
        this.w_MVCODCOM = Space(15)
        this.w_MVCODATT = Space(15)
        this.w_APPO = ah_Msgformat("Contributo accessorio generato in automatico")
        this.w_MVTIPPRO = IIF(g_CALPRO="DI","DC",IIF(g_CALPRO="GD","ST","CT"))
        this.w_MVIMPCOM = 0
        this.w_MVTIPPR2 = IIF(g_CALPRO="DI","DC",IIF(g_CALPRO="GD","ST","CT"))
         
 INSERT INTO GeneApp ; 
 (t_MVTIPRIG, t_MVCODICE, t_MVCODART, ; 
 t_MVDESART, t_MVDESSUP, t_MVUNIMIS, t_MVCATCON, ; 
 t_MVCONTRO, t_MVCODCLA, t_MVCONTRA, t_MVCODLIS, ; 
 t_MVQTAMOV, t_MVQTAUM1, t_MVPREZZO, ; 
 t_MVSCONT1, t_MVSCONT2, t_MVSCONT3, t_MVSCONT4, ; 
 t_MVFLOMAG, t_MVCODIVA, t_PERIVA, t_BOLIVA, t_MVVALRIG, t_MVVALMAG, t_MVIMPNAZ, t_MVPERPRO, ; 
 t_MVIMPPRO, t_MVSERRIF, t_MVROWRIF, ; 
 t_MVPESNET, t_MVFLTRAS, t_MVNOMENC, t_MVUMSUPP, ; 
 t_MVMOLSUP, t_MVNUMCOL, t_MVCONIND, ; 
 t_MVNAZPRO, t_MVPROORD, t_MVCODCEN, t_MVVOCCEN, ; 
 t_MVCODCOM, t_MVINICOM, t_MVFINCOM,t_MVFLARIF, t_MVIMPEVA, ; 
 t_MESS, t_MVSERORI, t_MVNUMORI, t_MVFLERIF, t_FLSERA,t_MVTIPPRO, ; 
 t_MVCODCES, t_MVIMPCOM, t_MVIMPAC2, t_DOQTAEVA, t_DOIMPEVA, t_MVTIPPR2, t_MVPROCAP, t_MVIMPCAP,t_MVFLNOAN,t_ORAPDATEVA,t_ORAPDATGEN, t_TDORDAPE, t_TESTESPLACC, t_MVRIFCAC, t_MVCACONT, t_FATHERRIF, t_CPROWORD, t_MVFLORCO,t_MVCODATT) ; 
 VALUES ( this.w_MVTIPRIG, this.w_KCODICE, this.w_MVCODART, ; 
 this.w_MVDESART, this.w_MVDESSUP, this.w_KUNIMIS, this.w_MVCATCON, ; 
 Space(15), this.w_MVCODCLA, Space(15), Space(5), ; 
 this.w_KQTAMOV, this.w_KQTAMOV , this.w_KPREZZO, ; 
 0, 0, 0, 0, ; 
 "X", this.w_KCODIVA, this.w_PERIVA, this.w_BOLIVA, this.w_MVVALRIG, this.w_MVVALMAG, this.w_MVIMPNAZ, 0, ; 
 0, Space(10), 0, ; 
 this.w_MVPESNET, this.w_MVFLTRAS, this.w_MVNOMENC, this.w_MVUMSUPP, ; 
 this.w_MVMOLSUP, 0, Space(15), ; 
 Space(3), Space(2), this.w_MVCODCEN, this.w_MVVOCCEN, ; 
 this.w_MVCODCOM, this.w_MVTINCOM, this.w_MVTFICOM, " ", 0, this.w_APPO, ; 
 Space(10), 0, " ", "S", this.w_MVTIPPRO, ; 
 Space(20), this.w_MVIMPCOM, 0, 0, 0, this.w_MVTIPPR2, 0, 0," ", Cp_CharToDate("  -  -    ") , Cp_CharToDate("  -  -    "), this.w_TDORDAPE, "NNN",this.w_RIFCACESP, this.w_CONTRIB, this.w_FATHERRIF, this.w_CPROWORD, " " ,this.w_MVCODATT)
        this.w_CPROWORD = this.w_CPROWORD + 1
        Select CurEspl
        if Not Eof("CurEspl")
          Skip
        endif
      enddo
      Select CurEspl 
 Use
      Select "GeneApp" 
 Go ( this.w_POSAPP )
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,30)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='CAM_AGAZ'
    this.cWorkTables[3]='CAN_TIER'
    this.cWorkTables[4]='CONTI'
    this.cWorkTables[5]='DES_DIVE'
    this.cWorkTables[6]='DET_DIFF'
    this.cWorkTables[7]='DET_PIAS'
    this.cWorkTables[8]='DIC_INTE'
    this.cWorkTables[9]='DOC_COLL'
    this.cWorkTables[10]='DOC_DETT'
    this.cWorkTables[11]='DOC_MAST'
    this.cWorkTables[12]='DOC_RATE'
    this.cWorkTables[13]='KEY_ARTI'
    this.cWorkTables[14]='PAG_AMEN'
    this.cWorkTables[15]='PAR_PROV'
    this.cWorkTables[16]='RAG_FATT'
    this.cWorkTables[17]='SALDIART'
    this.cWorkTables[18]='LISTINI'
    this.cWorkTables[19]='TIP_DOCU'
    this.cWorkTables[20]='*TMPVEND1'
    this.cWorkTables[21]='*TMPVEND2'
    this.cWorkTables[22]='VALUTE'
    this.cWorkTables[23]='VOCIIVA'
    this.cWorkTables[24]='UNIMIS'
    this.cWorkTables[25]='MODCLDAT'
    this.cWorkTables[26]='AZIENDA'
    this.cWorkTables[27]='DATI_AGG'
    this.cWorkTables[28]='PAR_PROD'
    this.cWorkTables[29]='SALDICOM'
    this.cWorkTables[30]='DICDINTE'
    return(this.OpenAllTables(30))

  proc CloseCursors()
    if used('_Curs_TMPVEND2')
      use in _Curs_TMPVEND2
    endif
    if used('_Curs_DICDINTE9')
      use in _Curs_DICDINTE9
    endif
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    if used('_Curs_DOC_RATE')
      use in _Curs_DOC_RATE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
