* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bez                                                        *
*              Apri dati elnchi clienti / fornitori                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-01-28                                                      *
* Last revis.: 2007-09-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pSERIAL,pRIGA,pRIFPNT
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bez",oParentObject,m.pSERIAL,m.pRIGA,m.pRIFPNT)
return(i_retval)

define class tgscg_bez as StdBatch
  * --- Local variables
  pSERIAL = space(10)
  pRIGA = 0
  pRIFPNT = space(10)
  w_MASK = .NULL.
  w_DESERIAL = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.pRIGA=1 and empty(nvl(this.pRIFPNT,""))
      this.w_DESERIAL = this.pSERIAL
      do GSCG_KEL with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      * --- definisco l'oggetto come appartenente alla classe della Prima Nota
      this.w_MASK = GSCG_MEL()
      * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
      if !(this.w_MASK.bSec1)
        i_retcode = 'stop'
        return
      endif
      this.w_MASK.w_DESERIAL = this.pSERIAL
      * --- creo il curosre delle solo chiavi
      this.w_MASK.QueryKeySet("DESERIAL='"+this.pSERIAL+ "'","")     
      * --- mi metto in interrogazione
      this.w_MASK.LoadRecWarn()     
      this.w_MASK.EcpEdit()     
    endif
  endproc


  proc Init(oParentObject,pSERIAL,pRIGA,pRIFPNT)
    this.pSERIAL=pSERIAL
    this.pRIGA=pRIGA
    this.pRIFPNT=pRIFPNT
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pSERIAL,pRIGA,pRIFPNT"
endproc
