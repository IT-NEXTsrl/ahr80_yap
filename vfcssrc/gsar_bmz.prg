* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bmz                                                        *
*              Routine per lanciare maschera con zoom e riposizionarsi         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-01-04                                                      *
* Last revis.: 2012-01-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_ObjCTRL,w_NAMEMASK
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bmz",oParentObject,m.w_ObjCTRL,m.w_NAMEMASK)
return(i_retval)

define class tgsar_bmz as StdBatch
  * --- Local variables
  w_ObjCTRL = .NULL.
  w_NAMEMASK = space(50)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Routine per lanciare maschera con zoom e riposizionarsi
    do (this.w_NAMEMASK) with this.oParentObject
    this.w_ObjCTRL.SetFocus()     
  endproc


  proc Init(oParentObject,w_ObjCTRL,w_NAMEMASK)
    this.w_ObjCTRL=w_ObjCTRL
    this.w_NAMEMASK=w_NAMEMASK
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_ObjCTRL,w_NAMEMASK"
endproc
