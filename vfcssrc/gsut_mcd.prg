* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_mcd                                                        *
*              Classi documentali                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [101] [VRS_108]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-08-20                                                      *
* Last revis.: 2017-06-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_mcd"))

* --- Class definition
define class tgsut_mcd as StdTrsForm
  Top    = 1
  Left   = 9

  * --- Standard Properties
  Width  = 789
  Height = 550+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2017-06-30"
  HelpContextID=97076073
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=81

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  PROMCLAS_IDX = 0
  PRODCLAS_IDX = 0
  CLA_ALLE_IDX = 0
  TIP_ALLE_IDX = 0
  PARA_EDS_IDX = 0
  STATISOS_IDX = 0
  CLASMSOS_IDX = 0
  CLASDSOS_IDX = 0
  PAR_ALTE_IDX = 0
  cFile = "PROMCLAS"
  cFileDetail = "PRODCLAS"
  cKeySelect = "CDCODCLA"
  cKeyWhere  = "CDCODCLA=this.w_CDCODCLA"
  cKeyDetail  = "CDCODCLA=this.w_CDCODCLA"
  cKeyWhereODBC = '"CDCODCLA="+cp_ToStrODBC(this.w_CDCODCLA)';

  cKeyDetailWhereODBC = '"CDCODCLA="+cp_ToStrODBC(this.w_CDCODCLA)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"PRODCLAS.CDCODCLA="+cp_ToStrODBC(this.w_CDCODCLA)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'PRODCLAS.CPROWORD '
  cPrg = "gsut_mcd"
  cComment = "Classi documentali"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 7
  icon = "movi.ico"
  cAutoZoom = 'GSUT_MCD'
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- gsut_mcd
  i_nRowPerPage = iif(g_DMIP='S' OR g_DOCM<>'S', 16, this.i_nRowPerPage)
  * --- Fine Area Manuale

  * --- Local Variables
  w_HIDEDM = .F.
  w_ISALT = .F.
  w_CDCODCLA = space(15)
  w_CDDESCLA = space(50)
  w_READAZI = space(10)
  w_CDCLAPRA = space(1)
  o_CDCLAPRA = space(1)
  w_CDMOTARC = space(1)
  w_CDCLAAWE = space(15)
  w_CDTIPARC = space(1)
  w_CDMODALL = space(1)
  o_CDMODALL = space(1)
  w_CDMODALL = space(1)
  w_CDRIFDEF = space(1)
  o_CDRIFDEF = space(1)
  w_CDRIFTAB = space(20)
  o_CDRIFTAB = space(20)
  w_CDFOLDER = space(1)
  w_CDGETDES = space(1)
  w_CDTIPRAG = space(1)
  w_CDCAMRAG = space(50)
  w_CDTIPALL = space(5)
  o_CDTIPALL = space(5)
  w_CDCOMTIP = space(1)
  w_CDCLAALL = space(5)
  o_CDCLAALL = space(5)
  w_CDCOMCLA = space(1)
  w_CDPATSTD = space(75)
  o_CDPATSTD = space(75)
  w_GETPATSTD = space(75)
  w_DESCRALL = space(35)
  w_DESCCLA = space(35)
  w_CODAZI = space(5)
  w_XXSERVER = space(20)
  w_XX__PORT = 0
  w_XX__USER = space(20)
  w_XXPASSWD = space(20)
  w_XXARCHIVE = space(20)
  w_CDSERVER = space(20)
  w_CD__PORT = 0
  w_CD__USER = space(20)
  w_CDPASSWD = space(20)
  w_X1PASSWD = space(20)
  w_CDARCHIVE = space(20)
  w_CDFLCTRL = space(1)
  w_CDFIRDIG = space(1)
  o_CDFIRDIG = space(1)
  w_CDCONSOS = space(1)
  o_CDCONSOS = space(1)
  w_CDCLASOS = space(10)
  o_CDCLASOS = space(10)
  w_CDAVVSOS = 0
  w_CDALIASF = space(15)
  o_CDALIASF = space(15)
  w_CDFLGDES = space(1)
  w_CDPUBWEB = space(1)
  w_CDNOMFIL = space(200)
  w_CDERASEF = space(1)
  w_CDTIPOBC = space(1)
  o_CDTIPOBC = space(1)
  w_CDREPOBC = space(254)
  w_GETREPOBC = space(254)
  w_OKREPOBC = .F.
  w_RESCHK = 0
  w_COKEYSOS = space(10)
  w_CDKEYUPD = space(1)
  w_OLDCDMODALL = space(1)
  w_CDCLAPRA = space(1)
  w_ATIPARC = space(1)
  w_CPROWORD = 0
  w_CDCODATT = space(15)
  o_CDCODATT = space(15)
  w_CDDESATT = space(40)
  w_CDTIPATT = space(1)
  o_CDTIPATT = space(1)
  w_CDCAMCUR = space(0)
  w_CDCHKOBB = space(1)
  o_CDCHKOBB = space(1)
  w_CDTABKEY = space(15)
  w_CDATTINF = space(15)
  o_CDATTINF = space(15)
  w_CDVLPRED = space(50)
  w_CDCREARA = space(1)
  w_CDNATAWE = space(15)
  w_CDFLGSOS = space(2)
  w_CDATTPRI = space(1)
  w_CDATTSOS = space(10)
  w_CODCLASOS = space(10)
  w_KEYTYPE = space(8)
  w_CDRIFTEM = space(1)
  w_ChkCtrlCDATTINF = space(10)
  w_UTCC = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_UTCV = 0
  w_CDCLAINF = space(15)
  w_CDOKSYNC = space(1)

  * --- Children pointers
  GSUT_MPE = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsut_mcd
  * Definizioni variabili per il controllo sulla modifica dei parametri
  * di definizione del path 'base' di archiviazione allegati.
  oldpath=' '
  oldmod=' '
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'PROMCLAS','gsut_mcd')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_mcdPag1","gsut_mcd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Classe documentale")
      .Pages(1).HelpContextID = 73047141
      .Pages(2).addobject("oPag","tgsut_mcdPag2")
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Autorizzazioni")
      .Pages(2).HelpContextID = 186877584
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCDCODCLA_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- gsut_mcd
    if g_DMIP='S' OR g_DOCM<>'S'
      this.Pages(1).oPag.resizeYpos=this.Pages(1).oPag.resizeYpos - 181
      this.Pages(1).oPag.oBody3D.height=int(fontmetric(1,"Arial",9,"")*16*1.3000000000000000)-2
      this.Pages(1).oPag.oBody.height=int(fontmetric(1,"Arial",9,"")*16*1.3000000000000000)-4
      for jjj=1 to this.PageCount+1
            * --- definisce la pagina in cui cercare
            if jjj<=this.PageCount and vartype(this.Pages(jjj).oPag)='O'
              jj=this.Pages(jjj).oPag
              * --- ora trova i controls
              for each ii in jj.Controls
                if ii.top>=90 and ii.top<478
                  ii.top=ii.top - 181
                endif
              next
            endif
      next
      local oCDNOMFIL
      oCDNOMFIL=this.Parent.GetCtrl("w_CDNOMFIL")
      oCDNOMFIL.ToolTipText="Espressione per la determinazione del nome file web (es.: dtos (mvdatdoc) +mvalfdoc+...) utilizzato da print system"
      oCDNOMFIL=.null.
      local oCDPUBWEB
      oCDPUBWEB=this.Parent.GetCtrl("w_CDPUBWEB")
      oCDPUBWEB.ToolTipText=""
      oCDPUBWEB=.null.
      if g_DOCM<>'S'
        if g_DMIP='S'
          this.parent.cComment=cp_Translate("Classi librerie allegati Infinity D.M.S.")
        else
          this.parent.cComment=cp_Translate("Classi librerie allegati Infinity D.M.S. stand alone")
        endif
      endif  
    endif
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[9]
    this.cWorkTables[1]='CLA_ALLE'
    this.cWorkTables[2]='TIP_ALLE'
    this.cWorkTables[3]='PARA_EDS'
    this.cWorkTables[4]='STATISOS'
    this.cWorkTables[5]='CLASMSOS'
    this.cWorkTables[6]='CLASDSOS'
    this.cWorkTables[7]='PAR_ALTE'
    this.cWorkTables[8]='PROMCLAS'
    this.cWorkTables[9]='PRODCLAS'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(9))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PROMCLAS_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PROMCLAS_IDX,3]
  return

  function CreateChildren()
    this.GSUT_MPE = CREATEOBJECT('stdDynamicChild',this,'GSUT_MPE',this.oPgFrm.Page2.oPag.oLinkPC_4_1)
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSUT_MPE)
      this.GSUT_MPE.DestroyChildrenChain()
      this.GSUT_MPE=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_4_1')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSUT_MPE.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSUT_MPE.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSUT_MPE.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .GSUT_MPE.ChangeRow(this.cRowID+'      1',1;
             ,.w_CDCODCLA,"PECODCLA";
             )
    endwith
    select (i_cOldSel)
    return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_CDCODCLA = NVL(CDCODCLA,space(15))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_19_joined
    link_1_19_joined=.f.
    local link_1_21_joined
    link_1_21_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from PROMCLAS where CDCODCLA=KeySet.CDCODCLA
    *
    i_nConn = i_TableProp[this.PROMCLAS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2],this.bLoadRecFilter,this.PROMCLAS_IDX,"gsut_mcd")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PROMCLAS')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PROMCLAS.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"PRODCLAS.","PROMCLAS.")
      i_cTable = i_cTable+' PROMCLAS '
      link_1_19_joined=this.AddJoinedLink_1_19(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_21_joined=this.AddJoinedLink_1_21(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CDCODCLA',this.w_CDCODCLA  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_GETPATSTD = space(75)
        .w_DESCRALL = space(35)
        .w_DESCCLA = space(35)
        .w_CODAZI = i_CODAZI
        .w_XXSERVER = space(20)
        .w_XX__PORT = 0
        .w_XX__USER = space(20)
        .w_XXPASSWD = space(20)
        .w_XXARCHIVE = space(20)
        .w_GETREPOBC = space(254)
        .w_OKREPOBC = .f.
        .w_RESCHK = 0
        .w_COKEYSOS = space(10)
        .w_OLDCDMODALL = space(1)
        .w_ATIPARC = space(1)
        .w_ChkCtrlCDATTINF = space(10)
        .w_HIDEDM = g_DMIP='S' OR g_DOCM<>'S'
        .w_ISALT = IsAlt()
        .w_CDCODCLA = NVL(CDCODCLA,space(15))
        .w_CDDESCLA = NVL(CDDESCLA,space(50))
        .w_READAZI = i_codazi
          .link_1_6('Load')
        .w_CDCLAPRA = NVL(CDCLAPRA,space(1))
        .w_CDMOTARC = NVL(CDMOTARC,space(1))
        .w_CDCLAAWE = NVL(CDCLAAWE,space(15))
        .w_CDTIPARC = NVL(CDTIPARC,space(1))
        .w_CDMODALL = NVL(CDMODALL,space(1))
        .w_CDMODALL = NVL(CDMODALL,space(1))
        .w_CDRIFDEF = NVL(CDRIFDEF,space(1))
        .w_CDRIFTAB = NVL(CDRIFTAB,space(20))
        .w_CDFOLDER = NVL(CDFOLDER,space(1))
        .w_CDGETDES = NVL(CDGETDES,space(1))
        .w_CDTIPRAG = NVL(CDTIPRAG,space(1))
        .w_CDCAMRAG = NVL(CDCAMRAG,space(50))
        .w_CDTIPALL = NVL(CDTIPALL,space(5))
          if link_1_19_joined
            this.w_CDTIPALL = NVL(TACODICE119,NVL(this.w_CDTIPALL,space(5)))
            this.w_DESCRALL = NVL(TADESCRI119,space(35))
            this.w_CDCOMTIP = NVL(TACOPTIP119,space(1))
          else
          .link_1_19('Load')
          endif
        .w_CDCOMTIP = NVL(CDCOMTIP,space(1))
        .w_CDCLAALL = NVL(CDCLAALL,space(5))
          if link_1_21_joined
            this.w_CDCLAALL = NVL(TACODCLA121,NVL(this.w_CDCLAALL,space(5)))
            this.w_DESCCLA = NVL(TACLADES121,space(35))
            this.w_CDCOMCLA = NVL(TACOPCLA121,space(1))
          else
          .link_1_21('Load')
          endif
        .w_CDCOMCLA = NVL(CDCOMCLA,space(1))
        .w_CDPATSTD = NVL(CDPATSTD,space(75))
          .link_1_40('Load')
        .w_CDSERVER = NVL(CDSERVER,space(20))
        .w_CD__PORT = NVL(CD__PORT,0)
        .w_CD__USER = NVL(CD__USER,space(20))
        .w_CDPASSWD = NVL(CDPASSWD,space(20))
        .w_X1PASSWD = iif(.w_CDMODALL='E', CifraCnf( ALLTRIM(.w_XXPASSWD) , 'D' ), space(20))
        .w_CDARCHIVE = NVL(CDARCHIVE,space(20))
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate('*')
        .w_CDFLCTRL = NVL(CDFLCTRL,space(1))
        .w_CDFIRDIG = NVL(CDFIRDIG,space(1))
        .w_CDCONSOS = NVL(CDCONSOS,space(1))
        .w_CDCLASOS = NVL(CDCLASOS,space(10))
          * evitabile
          *.link_1_64('Load')
        .w_CDAVVSOS = NVL(CDAVVSOS,0)
        .w_CDALIASF = NVL(CDALIASF,space(15))
        .w_CDFLGDES = NVL(CDFLGDES,space(1))
        .w_CDPUBWEB = NVL(CDPUBWEB,space(1))
        .w_CDNOMFIL = NVL(CDNOMFIL,space(200))
        .w_CDERASEF = NVL(CDERASEF,space(1))
        .oPgFrm.Page1.oPag.oObj_1_72.Calculate()
        .w_CDTIPOBC = NVL(CDTIPOBC,space(1))
        .w_CDREPOBC = NVL(CDREPOBC,space(254))
        .w_CDKEYUPD = NVL(CDKEYUPD,space(1))
        .w_CDCLAPRA = NVL(CDCLAPRA,space(1))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_UTCC = NVL(UTCC,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_UTCV = NVL(UTCV,0)
        .w_CDCLAINF = NVL(CDCLAINF,space(15))
        .w_CDOKSYNC = NVL(CDOKSYNC,space(1))
        cp_LoadRecExtFlds(this,'PROMCLAS')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from PRODCLAS where CDCODCLA=KeySet.CDCODCLA
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.PRODCLAS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PRODCLAS_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('PRODCLAS')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "PRODCLAS.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" PRODCLAS"
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'CDCODCLA',this.w_CDCODCLA  )
        select * from (i_cTable) PRODCLAS where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
        .w_CODCLASOS = .w_CDCLASOS
          .w_KEYTYPE = space(8)
          .w_CPROWNUM = CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_CDCODATT = NVL(CDCODATT,space(15))
          .w_CDDESATT = NVL(CDDESATT,space(40))
          .w_CDTIPATT = NVL(CDTIPATT,space(1))
          .w_CDCAMCUR = NVL(CDCAMCUR,space(0))
          .w_CDCHKOBB = NVL(CDCHKOBB,space(1))
          .w_CDTABKEY = NVL(CDTABKEY,space(15))
          .w_CDATTINF = NVL(CDATTINF,space(15))
          .w_CDVLPRED = NVL(CDVLPRED,space(50))
          .w_CDCREARA = NVL(CDCREARA,space(1))
          .w_CDNATAWE = NVL(CDNATAWE,space(15))
          .w_CDFLGSOS = NVL(CDFLGSOS,space(2))
          .w_CDATTPRI = NVL(CDATTPRI,space(1))
          .w_CDATTSOS = NVL(CDATTSOS,space(10))
          .link_2_15('Load')
          .w_CDRIFTEM = NVL(CDRIFTEM,space(1))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .w_HIDEDM = g_DMIP='S' OR g_DOCM<>'S'
        .w_ISALT = IsAlt()
        .w_READAZI = i_codazi
        .w_X1PASSWD = iif(.w_CDMODALL='E', CifraCnf( ALLTRIM(.w_XXPASSWD) , 'D' ), space(20))
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate('*')
        .oPgFrm.Page1.oPag.oObj_1_72.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_1_110.enabled = .oPgFrm.Page1.oPag.oBtn_1_110.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsut_mcd
    this.oldpath = this.w_CDPATSTD
    this.oldmod = this.w_CDMODALL
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_HIDEDM=.f.
      .w_ISALT=.f.
      .w_CDCODCLA=space(15)
      .w_CDDESCLA=space(50)
      .w_READAZI=space(10)
      .w_CDCLAPRA=space(1)
      .w_CDMOTARC=space(1)
      .w_CDCLAAWE=space(15)
      .w_CDTIPARC=space(1)
      .w_CDMODALL=space(1)
      .w_CDMODALL=space(1)
      .w_CDRIFDEF=space(1)
      .w_CDRIFTAB=space(20)
      .w_CDFOLDER=space(1)
      .w_CDGETDES=space(1)
      .w_CDTIPRAG=space(1)
      .w_CDCAMRAG=space(50)
      .w_CDTIPALL=space(5)
      .w_CDCOMTIP=space(1)
      .w_CDCLAALL=space(5)
      .w_CDCOMCLA=space(1)
      .w_CDPATSTD=space(75)
      .w_GETPATSTD=space(75)
      .w_DESCRALL=space(35)
      .w_DESCCLA=space(35)
      .w_CODAZI=space(5)
      .w_XXSERVER=space(20)
      .w_XX__PORT=0
      .w_XX__USER=space(20)
      .w_XXPASSWD=space(20)
      .w_XXARCHIVE=space(20)
      .w_CDSERVER=space(20)
      .w_CD__PORT=0
      .w_CD__USER=space(20)
      .w_CDPASSWD=space(20)
      .w_X1PASSWD=space(20)
      .w_CDARCHIVE=space(20)
      .w_CDFLCTRL=space(1)
      .w_CDFIRDIG=space(1)
      .w_CDCONSOS=space(1)
      .w_CDCLASOS=space(10)
      .w_CDAVVSOS=0
      .w_CDALIASF=space(15)
      .w_CDFLGDES=space(1)
      .w_CDPUBWEB=space(1)
      .w_CDNOMFIL=space(200)
      .w_CDERASEF=space(1)
      .w_CDTIPOBC=space(1)
      .w_CDREPOBC=space(254)
      .w_GETREPOBC=space(254)
      .w_OKREPOBC=.f.
      .w_RESCHK=0
      .w_COKEYSOS=space(10)
      .w_CDKEYUPD=space(1)
      .w_OLDCDMODALL=space(1)
      .w_CDCLAPRA=space(1)
      .w_ATIPARC=space(1)
      .w_CPROWORD=10
      .w_CDCODATT=space(15)
      .w_CDDESATT=space(40)
      .w_CDTIPATT=space(1)
      .w_CDCAMCUR=space(0)
      .w_CDCHKOBB=space(1)
      .w_CDTABKEY=space(15)
      .w_CDATTINF=space(15)
      .w_CDVLPRED=space(50)
      .w_CDCREARA=space(1)
      .w_CDNATAWE=space(15)
      .w_CDFLGSOS=space(2)
      .w_CDATTPRI=space(1)
      .w_CDATTSOS=space(10)
      .w_CODCLASOS=space(10)
      .w_KEYTYPE=space(8)
      .w_CDRIFTEM=space(1)
      .w_ChkCtrlCDATTINF=space(10)
      .w_UTCC=0
      .w_UTDC=ctot("")
      .w_UTDV=ctot("")
      .w_UTCV=0
      .w_CDCLAINF=space(15)
      .w_CDOKSYNC=space(1)
      if .cFunction<>"Filter"
        .w_HIDEDM = g_DMIP='S' OR g_DOCM<>'S'
        .w_ISALT = IsAlt()
        .DoRTCalc(3,4,.f.)
        .w_READAZI = i_codazi
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_READAZI))
         .link_1_6('Full')
        endif
        .w_CDCLAPRA = 'N'
        .w_CDMOTARC = 'G'
        .w_CDCLAAWE = .w_CDCODCLA
        .w_CDTIPARC = IIF (.w_ISALT, 'M', 'A')
        .w_CDMODALL = iif(g_DMIP='S', 'I', iif(g_DOCM='S', 'F', 'S'))
        .DoRTCalc(11,11,.f.)
        .w_CDRIFDEF = IIF(.w_ISALT, 'S', 'N')
        .w_CDRIFTAB = IIF(.w_CDRIFDEF='N', space(20), .w_CDRIFTAB)
        .w_CDFOLDER = iif(.w_CDMODALL#'F','N',.w_CDFOLDER)
        .w_CDGETDES = 'N'
        .w_CDTIPRAG = 'N'
        .DoRTCalc(17,18,.f.)
        if not(empty(.w_CDTIPALL))
         .link_1_19('Full')
        endif
        .w_CDCOMTIP = iif(empty(.w_CDTIPALL) or .w_CDCLAPRA='S','N',.w_CDCOMTIP)
        .w_CDCLAALL = space(5)
        .DoRTCalc(20,20,.f.)
        if not(empty(.w_CDCLAALL))
         .link_1_21('Full')
        endif
        .w_CDCOMCLA = iif(empty(.w_CDCLAALL) or .w_CDCLAPRA='S','N',.w_CDCOMCLA)
        .DoRTCalc(22,25,.f.)
        .w_CODAZI = i_CODAZI
        .DoRTCalc(26,26,.f.)
        if not(empty(.w_CODAZI))
         .link_1_40('Full')
        endif
        .DoRTCalc(27,31,.f.)
        .w_CDSERVER = iif(.w_CDMODALL='E', .w_XXSERVER, space(20))
        .w_CD__PORT = iif(.w_CDMODALL='E', .w_XX__PORT, 0)
        .w_CD__USER = iif(.w_CDMODALL='E', .w_XX__USER, space(20))
        .w_CDPASSWD = iif(.w_CDMODALL='E', .w_XXPASSWD, space(20))
        .w_X1PASSWD = iif(.w_CDMODALL='E', CifraCnf( ALLTRIM(.w_XXPASSWD) , 'D' ), space(20))
        .w_CDARCHIVE = iif(.w_CDMODALL='E', .w_XXARCHIVE, space(20))
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate('*')
        .w_CDFLCTRL = iif(.w_CDMODALL='S','N', 'S')
        .w_CDFIRDIG = icase(.w_CDMODALL = 'E','N',.w_CDCONSOS='S','S','N')
        .w_CDCONSOS = 'N'
        .w_CDCLASOS = iif(.w_CDCONSOS='S',.w_CDCLASOS,space(10))
        .DoRTCalc(41,41,.f.)
        if not(empty(.w_CDCLASOS))
         .link_1_64('Full')
        endif
        .w_CDAVVSOS = iif(.w_CDCONSOS='S',.w_CDAVVSOS,0)
        .w_CDALIASF = CHRTRAN(.w_CDALIASF,[^\/:*?"<>|&.' ],'')
        .w_CDFLGDES = iif(.w_CDRIFTAB=='CAN_TIER',.w_CDFLGDES,'N')
        .w_CDPUBWEB = iif(.w_CDMODALL $ 'IS', 'S', 'N')
        .w_CDNOMFIL = space(200)
        .w_CDERASEF = iif(.w_CDMODALL='I','S','N')
        .oPgFrm.Page1.oPag.oObj_1_72.Calculate()
        .w_CDTIPOBC = "Q"
        .w_CDREPOBC = ICASE(.w_CDTIPOBC='E', 'QUERY\GSUT_BCV.FRX', .w_CDTIPOBC='F', 'QUERY\GSUT1BCV.FRX', .w_CDTIPOBC='Q', 'QUERY\GSUT2BCV.FRX', 'QUERY\GSUT3BCV.FRX')
        .DoRTCalc(50,51,.f.)
        .w_RESCHK = 0
        .DoRTCalc(53,55,.f.)
        .w_CDCLAPRA = iif(INLIST(.w_CDMODALL,'F','L') AND NOT EMPTY(.w_CDCLAPRA), .w_CDCLAPRA, 'N')
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(57,62,.f.)
        .w_CDCHKOBB = 'N'
        .DoRTCalc(64,68,.f.)
        .w_CDFLGSOS = iif(.w_CDCONSOS='S',.w_CDFLGSOS,'NN')
        .w_CDATTPRI = iif(.w_CDCHKOBB='S',.w_CDATTPRI,'N')
        .w_CDATTSOS = iif(.w_CDCONSOS='S' and not empty(.w_CDCLASOS),.w_CDATTSOS,space(15))
        .DoRTCalc(71,71,.f.)
        if not(empty(.w_CDATTSOS))
         .link_2_15('Full')
        endif
        .w_CODCLASOS = .w_CDCLASOS
        .DoRTCalc(73,73,.f.)
        .w_CDRIFTEM = iif(.w_CDFIRDIG='S' and .w_CDTIPATT='D',.w_CDRIFTEM,'N')
      endif
    endwith
    cp_BlankRecExtFlds(this,'PROMCLAS')
    this.DoRTCalc(75,81,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_1_110.enabled = this.oPgFrm.Page1.oPag.oBtn_1_110.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCDCODCLA_1_4.enabled = i_bVal
      .Page1.oPag.oCDDESCLA_1_5.enabled = i_bVal
      .Page1.oPag.oCDCLAPRA_1_7.enabled = i_bVal
      .Page1.oPag.oCDTIPARC_1_10.enabled = i_bVal
      .Page1.oPag.oCDMODALL_1_11.enabled = i_bVal
      .Page1.oPag.oCDMODALL_1_12.enabled = i_bVal
      .Page1.oPag.oCDRIFDEF_1_13.enabled = i_bVal
      .Page1.oPag.oCDRIFTAB_1_14.enabled = i_bVal
      .Page1.oPag.oCDFOLDER_1_15.enabled = i_bVal
      .Page1.oPag.oCDGETDES_1_16.enabled = i_bVal
      .Page1.oPag.oCDTIPRAG_1_17.enabled = i_bVal
      .Page1.oPag.oCDCAMRAG_1_18.enabled = i_bVal
      .Page1.oPag.oCDTIPALL_1_19.enabled = i_bVal
      .Page1.oPag.oCDCOMTIP_1_20.enabled = i_bVal
      .Page1.oPag.oCDCLAALL_1_21.enabled = i_bVal
      .Page1.oPag.oCDCOMCLA_1_22.enabled = i_bVal
      .Page1.oPag.oCDPATSTD_1_23.enabled = i_bVal
      .Page1.oPag.oCDSERVER_1_46.enabled = i_bVal
      .Page1.oPag.oCD__PORT_1_47.enabled = i_bVal
      .Page1.oPag.oCD__USER_1_48.enabled = i_bVal
      .Page1.oPag.oX1PASSWD_1_50.enabled = i_bVal
      .Page1.oPag.oCDARCHIVE_1_55.enabled = i_bVal
      .Page1.oPag.oCDFLCTRL_1_60.enabled = i_bVal
      .Page1.oPag.oCDFIRDIG_1_61.enabled = i_bVal
      .Page1.oPag.oCDCONSOS_1_62.enabled = i_bVal
      .Page1.oPag.oCDCLASOS_1_64.enabled = i_bVal
      .Page1.oPag.oCDAVVSOS_1_65.enabled = i_bVal
      .Page1.oPag.oCDALIASF_1_66.enabled = i_bVal
      .Page1.oPag.oCDFLGDES_1_67.enabled = i_bVal
      .Page1.oPag.oCDPUBWEB_1_69.enabled = i_bVal
      .Page1.oPag.oCDNOMFIL_1_70.enabled = i_bVal
      .Page1.oPag.oCDERASEF_1_71.enabled = i_bVal
      .Page1.oPag.oCDTIPOBC_1_73.enabled = i_bVal
      .Page1.oPag.oCDREPOBC_1_74.enabled = i_bVal
      .Page1.oPag.oCDCLAPRA_1_95.enabled = i_bVal
      .Page1.oPag.oCDVLPRED_2_10.enabled = i_bVal
      .Page1.oPag.oCDCREARA_2_11.enabled = i_bVal
      .Page1.oPag.oCDATTPRI_2_14.enabled = i_bVal
      .Page1.oPag.oBtn_1_24.enabled = i_bVal
      .Page1.oPag.oBtn_1_75.enabled = i_bVal
      .Page1.oPag.oBtn_1_86.enabled = i_bVal
      .Page1.oPag.oBtn_1_110.enabled = .Page1.oPag.oBtn_1_110.mCond()
      .Page1.oPag.oObj_1_57.enabled = i_bVal
      .Page1.oPag.oObj_1_72.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oCDCODCLA_1_4.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oCDCODCLA_1_4.enabled = .t.
      endif
    endwith
    this.GSUT_MPE.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'PROMCLAS',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSUT_MPE.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PROMCLAS_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDCODCLA,"CDCODCLA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDDESCLA,"CDDESCLA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDCLAPRA,"CDCLAPRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDMOTARC,"CDMOTARC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDCLAAWE,"CDCLAAWE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDTIPARC,"CDTIPARC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDMODALL,"CDMODALL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDMODALL,"CDMODALL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDRIFDEF,"CDRIFDEF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDRIFTAB,"CDRIFTAB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDFOLDER,"CDFOLDER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDGETDES,"CDGETDES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDTIPRAG,"CDTIPRAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDCAMRAG,"CDCAMRAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDTIPALL,"CDTIPALL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDCOMTIP,"CDCOMTIP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDCLAALL,"CDCLAALL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDCOMCLA,"CDCOMCLA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDPATSTD,"CDPATSTD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDSERVER,"CDSERVER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CD__PORT,"CD__PORT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CD__USER,"CD__USER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDPASSWD,"CDPASSWD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDARCHIVE,"CDARCHIVE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDFLCTRL,"CDFLCTRL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDFIRDIG,"CDFIRDIG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDCONSOS,"CDCONSOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDCLASOS,"CDCLASOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDAVVSOS,"CDAVVSOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDALIASF,"CDALIASF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDFLGDES,"CDFLGDES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDPUBWEB,"CDPUBWEB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDNOMFIL,"CDNOMFIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDERASEF,"CDERASEF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDTIPOBC,"CDTIPOBC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDREPOBC,"CDREPOBC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDKEYUPD,"CDKEYUPD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDCLAPRA,"CDCLAPRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDCLAINF,"CDCLAINF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDOKSYNC,"CDOKSYNC",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PROMCLAS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])
    i_lTable = "PROMCLAS"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.PROMCLAS_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSDM_SCD with this
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_CDCODATT C(15);
      ,t_CDDESATT C(40);
      ,t_CDTIPATT N(3);
      ,t_CDCAMCUR M(10);
      ,t_CDCHKOBB N(3);
      ,t_CDTABKEY C(15);
      ,t_CDATTINF C(15);
      ,t_CDVLPRED C(50);
      ,t_CDCREARA N(3);
      ,t_CDFLGSOS N(3);
      ,t_CDATTPRI N(3);
      ,t_CDATTSOS C(10);
      ,t_CDRIFTEM N(3);
      ,CPROWNUM N(10);
      ,t_CDNATAWE C(15);
      ,t_CODCLASOS C(10);
      ,t_KEYTYPE C(8);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsut_mcdbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCDCODATT_2_2.controlsource=this.cTrsName+'.t_CDCODATT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCDDESATT_2_4.controlsource=this.cTrsName+'.t_CDDESATT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCDTIPATT_2_5.controlsource=this.cTrsName+'.t_CDTIPATT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCDCAMCUR_2_6.controlsource=this.cTrsName+'.t_CDCAMCUR'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCDCHKOBB_2_7.controlsource=this.cTrsName+'.t_CDCHKOBB'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCDTABKEY_2_8.controlsource=this.cTrsName+'.t_CDTABKEY'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCDATTINF_2_9.controlsource=this.cTrsName+'.t_CDATTINF'
    this.oPgFRm.Page1.oPag.oCDVLPRED_2_10.controlsource=this.cTrsName+'.t_CDVLPRED'
    this.oPgFRm.Page1.oPag.oCDCREARA_2_11.controlsource=this.cTrsName+'.t_CDCREARA'
    this.oPgFRm.Page1.oPag.oCDFLGSOS_2_13.controlsource=this.cTrsName+'.t_CDFLGSOS'
    this.oPgFRm.Page1.oPag.oCDATTPRI_2_14.controlsource=this.cTrsName+'.t_CDATTPRI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCDATTSOS_2_15.controlsource=this.cTrsName+'.t_CDATTSOS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCDRIFTEM_2_18.controlsource=this.cTrsName+'.t_CDRIFTEM'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(45)
    this.AddVLine(139)
    this.AddVLine(245)
    this.AddVLine(319)
    this.AddVLine(493)
    this.AddVLine(537)
    this.AddVLine(567)
    this.AddVLine(666)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PROMCLAS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into PROMCLAS
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PROMCLAS')
        i_extval=cp_InsertValODBCExtFlds(this,'PROMCLAS')
        local i_cFld
        i_cFld=" "+;
                  "(CDCODCLA,CDDESCLA,CDCLAPRA,CDMOTARC,CDCLAAWE"+;
                  ",CDTIPARC,CDMODALL,CDRIFDEF,CDRIFTAB,CDFOLDER"+;
                  ",CDGETDES,CDTIPRAG,CDCAMRAG,CDTIPALL,CDCOMTIP"+;
                  ",CDCLAALL,CDCOMCLA,CDPATSTD,CDSERVER,CD__PORT"+;
                  ",CD__USER,CDPASSWD,CDARCHIVE,CDFLCTRL,CDFIRDIG"+;
                  ",CDCONSOS,CDCLASOS,CDAVVSOS,CDALIASF,CDFLGDES"+;
                  ",CDPUBWEB,CDNOMFIL,CDERASEF,CDTIPOBC,CDREPOBC"+;
                  ",CDKEYUPD,UTCC,UTDC,UTDV,UTCV"+;
                  ",CDCLAINF,CDOKSYNC"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_CDCODCLA)+;
                    ","+cp_ToStrODBC(this.w_CDDESCLA)+;
                    ","+cp_ToStrODBC(this.w_CDCLAPRA)+;
                    ","+cp_ToStrODBC(this.w_CDMOTARC)+;
                    ","+cp_ToStrODBC(this.w_CDCLAAWE)+;
                    ","+cp_ToStrODBC(this.w_CDTIPARC)+;
                    ","+cp_ToStrODBC(this.w_CDMODALL)+;
                    ","+cp_ToStrODBC(this.w_CDRIFDEF)+;
                    ","+cp_ToStrODBC(this.w_CDRIFTAB)+;
                    ","+cp_ToStrODBC(this.w_CDFOLDER)+;
                    ","+cp_ToStrODBC(this.w_CDGETDES)+;
                    ","+cp_ToStrODBC(this.w_CDTIPRAG)+;
                    ","+cp_ToStrODBC(this.w_CDCAMRAG)+;
                    ","+cp_ToStrODBCNull(this.w_CDTIPALL)+;
                    ","+cp_ToStrODBC(this.w_CDCOMTIP)+;
                    ","+cp_ToStrODBCNull(this.w_CDCLAALL)+;
                    ","+cp_ToStrODBC(this.w_CDCOMCLA)+;
                    ","+cp_ToStrODBC(this.w_CDPATSTD)+;
                    ","+cp_ToStrODBC(this.w_CDSERVER)+;
                    ","+cp_ToStrODBC(this.w_CD__PORT)+;
                    ","+cp_ToStrODBC(this.w_CD__USER)+;
                    ","+cp_ToStrODBC(this.w_CDPASSWD)+;
                    ","+cp_ToStrODBC(this.w_CDARCHIVE)+;
                    ","+cp_ToStrODBC(this.w_CDFLCTRL)+;
                    ","+cp_ToStrODBC(this.w_CDFIRDIG)+;
                    ","+cp_ToStrODBC(this.w_CDCONSOS)+;
                    ","+cp_ToStrODBCNull(this.w_CDCLASOS)+;
                    ","+cp_ToStrODBC(this.w_CDAVVSOS)+;
                    ","+cp_ToStrODBC(this.w_CDALIASF)+;
                    ","+cp_ToStrODBC(this.w_CDFLGDES)+;
                    ","+cp_ToStrODBC(this.w_CDPUBWEB)+;
                    ","+cp_ToStrODBC(this.w_CDNOMFIL)+;
                    ","+cp_ToStrODBC(this.w_CDERASEF)+;
                    ","+cp_ToStrODBC(this.w_CDTIPOBC)+;
                    ","+cp_ToStrODBC(this.w_CDREPOBC)+;
                    ","+cp_ToStrODBC(this.w_CDKEYUPD)+;
                    ","+cp_ToStrODBC(this.w_UTCC)+;
                    ","+cp_ToStrODBC(this.w_UTDC)+;
                    ","+cp_ToStrODBC(this.w_UTDV)+;
                    ","+cp_ToStrODBC(this.w_UTCV)+;
                    ","+cp_ToStrODBC(this.w_CDCLAINF)+;
                    ","+cp_ToStrODBC(this.w_CDOKSYNC)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PROMCLAS')
        i_extval=cp_InsertValVFPExtFlds(this,'PROMCLAS')
        cp_CheckDeletedKey(i_cTable,0,'CDCODCLA',this.w_CDCODCLA)
        INSERT INTO (i_cTable);
              (CDCODCLA,CDDESCLA,CDCLAPRA,CDMOTARC,CDCLAAWE,CDTIPARC,CDMODALL,CDRIFDEF,CDRIFTAB,CDFOLDER,CDGETDES,CDTIPRAG,CDCAMRAG,CDTIPALL,CDCOMTIP,CDCLAALL,CDCOMCLA,CDPATSTD,CDSERVER,CD__PORT,CD__USER,CDPASSWD,CDARCHIVE,CDFLCTRL,CDFIRDIG,CDCONSOS,CDCLASOS,CDAVVSOS,CDALIASF,CDFLGDES,CDPUBWEB,CDNOMFIL,CDERASEF,CDTIPOBC,CDREPOBC,CDKEYUPD,UTCC,UTDC,UTDV,UTCV,CDCLAINF,CDOKSYNC &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_CDCODCLA;
                  ,this.w_CDDESCLA;
                  ,this.w_CDCLAPRA;
                  ,this.w_CDMOTARC;
                  ,this.w_CDCLAAWE;
                  ,this.w_CDTIPARC;
                  ,this.w_CDMODALL;
                  ,this.w_CDRIFDEF;
                  ,this.w_CDRIFTAB;
                  ,this.w_CDFOLDER;
                  ,this.w_CDGETDES;
                  ,this.w_CDTIPRAG;
                  ,this.w_CDCAMRAG;
                  ,this.w_CDTIPALL;
                  ,this.w_CDCOMTIP;
                  ,this.w_CDCLAALL;
                  ,this.w_CDCOMCLA;
                  ,this.w_CDPATSTD;
                  ,this.w_CDSERVER;
                  ,this.w_CD__PORT;
                  ,this.w_CD__USER;
                  ,this.w_CDPASSWD;
                  ,this.w_CDARCHIVE;
                  ,this.w_CDFLCTRL;
                  ,this.w_CDFIRDIG;
                  ,this.w_CDCONSOS;
                  ,this.w_CDCLASOS;
                  ,this.w_CDAVVSOS;
                  ,this.w_CDALIASF;
                  ,this.w_CDFLGDES;
                  ,this.w_CDPUBWEB;
                  ,this.w_CDNOMFIL;
                  ,this.w_CDERASEF;
                  ,this.w_CDTIPOBC;
                  ,this.w_CDREPOBC;
                  ,this.w_CDKEYUPD;
                  ,this.w_UTCC;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_UTCV;
                  ,this.w_CDCLAINF;
                  ,this.w_CDOKSYNC;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PRODCLAS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PRODCLAS_IDX,2])
      *
      * insert into PRODCLAS
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(CDCODCLA,CPROWORD,CDCODATT,CDDESATT,CDTIPATT"+;
                  ",CDCAMCUR,CDCHKOBB,CDTABKEY,CDATTINF,CDVLPRED"+;
                  ",CDCREARA,CDNATAWE,CDFLGSOS,CDATTPRI,CDATTSOS"+;
                  ",CDRIFTEM,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_CDCODCLA)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_CDCODATT)+","+cp_ToStrODBC(this.w_CDDESATT)+","+cp_ToStrODBC(this.w_CDTIPATT)+;
             ","+cp_ToStrODBC(this.w_CDCAMCUR)+","+cp_ToStrODBC(this.w_CDCHKOBB)+","+cp_ToStrODBC(this.w_CDTABKEY)+","+cp_ToStrODBC(this.w_CDATTINF)+","+cp_ToStrODBC(this.w_CDVLPRED)+;
             ","+cp_ToStrODBC(this.w_CDCREARA)+","+cp_ToStrODBC(this.w_CDNATAWE)+","+cp_ToStrODBC(this.w_CDFLGSOS)+","+cp_ToStrODBC(this.w_CDATTPRI)+","+cp_ToStrODBCNull(this.w_CDATTSOS)+;
             ","+cp_ToStrODBC(this.w_CDRIFTEM)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'CDCODCLA',this.w_CDCODCLA)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_CDCODCLA,this.w_CPROWORD,this.w_CDCODATT,this.w_CDDESATT,this.w_CDTIPATT"+;
                ",this.w_CDCAMCUR,this.w_CDCHKOBB,this.w_CDTABKEY,this.w_CDATTINF,this.w_CDVLPRED"+;
                ",this.w_CDCREARA,this.w_CDNATAWE,this.w_CDFLGSOS,this.w_CDATTPRI,this.w_CDATTSOS"+;
                ",this.w_CDRIFTEM,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.PROMCLAS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update PROMCLAS
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'PROMCLAS')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " CDDESCLA="+cp_ToStrODBC(this.w_CDDESCLA)+;
             ",CDCLAPRA="+cp_ToStrODBC(this.w_CDCLAPRA)+;
             ",CDMOTARC="+cp_ToStrODBC(this.w_CDMOTARC)+;
             ",CDCLAAWE="+cp_ToStrODBC(this.w_CDCLAAWE)+;
             ",CDTIPARC="+cp_ToStrODBC(this.w_CDTIPARC)+;
             ",CDMODALL="+cp_ToStrODBC(this.w_CDMODALL)+;
             ",CDRIFDEF="+cp_ToStrODBC(this.w_CDRIFDEF)+;
             ",CDRIFTAB="+cp_ToStrODBC(this.w_CDRIFTAB)+;
             ",CDFOLDER="+cp_ToStrODBC(this.w_CDFOLDER)+;
             ",CDGETDES="+cp_ToStrODBC(this.w_CDGETDES)+;
             ",CDTIPRAG="+cp_ToStrODBC(this.w_CDTIPRAG)+;
             ",CDCAMRAG="+cp_ToStrODBC(this.w_CDCAMRAG)+;
             ",CDTIPALL="+cp_ToStrODBCNull(this.w_CDTIPALL)+;
             ",CDCOMTIP="+cp_ToStrODBC(this.w_CDCOMTIP)+;
             ",CDCLAALL="+cp_ToStrODBCNull(this.w_CDCLAALL)+;
             ",CDCOMCLA="+cp_ToStrODBC(this.w_CDCOMCLA)+;
             ",CDPATSTD="+cp_ToStrODBC(this.w_CDPATSTD)+;
             ",CDSERVER="+cp_ToStrODBC(this.w_CDSERVER)+;
             ",CD__PORT="+cp_ToStrODBC(this.w_CD__PORT)+;
             ",CD__USER="+cp_ToStrODBC(this.w_CD__USER)+;
             ",CDPASSWD="+cp_ToStrODBC(this.w_CDPASSWD)+;
             ",CDARCHIVE="+cp_ToStrODBC(this.w_CDARCHIVE)+;
             ",CDFLCTRL="+cp_ToStrODBC(this.w_CDFLCTRL)+;
             ",CDFIRDIG="+cp_ToStrODBC(this.w_CDFIRDIG)+;
             ",CDCONSOS="+cp_ToStrODBC(this.w_CDCONSOS)+;
             ",CDCLASOS="+cp_ToStrODBCNull(this.w_CDCLASOS)+;
             ",CDAVVSOS="+cp_ToStrODBC(this.w_CDAVVSOS)+;
             ",CDALIASF="+cp_ToStrODBC(this.w_CDALIASF)+;
             ",CDFLGDES="+cp_ToStrODBC(this.w_CDFLGDES)+;
             ",CDPUBWEB="+cp_ToStrODBC(this.w_CDPUBWEB)+;
             ",CDNOMFIL="+cp_ToStrODBC(this.w_CDNOMFIL)+;
             ",CDERASEF="+cp_ToStrODBC(this.w_CDERASEF)+;
             ",CDTIPOBC="+cp_ToStrODBC(this.w_CDTIPOBC)+;
             ",CDREPOBC="+cp_ToStrODBC(this.w_CDREPOBC)+;
             ",CDKEYUPD="+cp_ToStrODBC(this.w_CDKEYUPD)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",CDCLAINF="+cp_ToStrODBC(this.w_CDCLAINF)+;
             ",CDOKSYNC="+cp_ToStrODBC(this.w_CDOKSYNC)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'PROMCLAS')
          i_cWhere = cp_PKFox(i_cTable  ,'CDCODCLA',this.w_CDCODCLA  )
          UPDATE (i_cTable) SET;
              CDDESCLA=this.w_CDDESCLA;
             ,CDCLAPRA=this.w_CDCLAPRA;
             ,CDMOTARC=this.w_CDMOTARC;
             ,CDCLAAWE=this.w_CDCLAAWE;
             ,CDTIPARC=this.w_CDTIPARC;
             ,CDMODALL=this.w_CDMODALL;
             ,CDRIFDEF=this.w_CDRIFDEF;
             ,CDRIFTAB=this.w_CDRIFTAB;
             ,CDFOLDER=this.w_CDFOLDER;
             ,CDGETDES=this.w_CDGETDES;
             ,CDTIPRAG=this.w_CDTIPRAG;
             ,CDCAMRAG=this.w_CDCAMRAG;
             ,CDTIPALL=this.w_CDTIPALL;
             ,CDCOMTIP=this.w_CDCOMTIP;
             ,CDCLAALL=this.w_CDCLAALL;
             ,CDCOMCLA=this.w_CDCOMCLA;
             ,CDPATSTD=this.w_CDPATSTD;
             ,CDSERVER=this.w_CDSERVER;
             ,CD__PORT=this.w_CD__PORT;
             ,CD__USER=this.w_CD__USER;
             ,CDPASSWD=this.w_CDPASSWD;
             ,CDARCHIVE=this.w_CDARCHIVE;
             ,CDFLCTRL=this.w_CDFLCTRL;
             ,CDFIRDIG=this.w_CDFIRDIG;
             ,CDCONSOS=this.w_CDCONSOS;
             ,CDCLASOS=this.w_CDCLASOS;
             ,CDAVVSOS=this.w_CDAVVSOS;
             ,CDALIASF=this.w_CDALIASF;
             ,CDFLGDES=this.w_CDFLGDES;
             ,CDPUBWEB=this.w_CDPUBWEB;
             ,CDNOMFIL=this.w_CDNOMFIL;
             ,CDERASEF=this.w_CDERASEF;
             ,CDTIPOBC=this.w_CDTIPOBC;
             ,CDREPOBC=this.w_CDREPOBC;
             ,CDKEYUPD=this.w_CDKEYUPD;
             ,UTCC=this.w_UTCC;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,UTCV=this.w_UTCV;
             ,CDCLAINF=this.w_CDCLAINF;
             ,CDOKSYNC=this.w_CDOKSYNC;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_CDDESATT)) and not empty(t_CDCODATT)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.PRODCLAS_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.PRODCLAS_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from PRODCLAS
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update PRODCLAS
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",CDCODATT="+cp_ToStrODBC(this.w_CDCODATT)+;
                     ",CDDESATT="+cp_ToStrODBC(this.w_CDDESATT)+;
                     ",CDTIPATT="+cp_ToStrODBC(this.w_CDTIPATT)+;
                     ",CDCAMCUR="+cp_ToStrODBC(this.w_CDCAMCUR)+;
                     ",CDCHKOBB="+cp_ToStrODBC(this.w_CDCHKOBB)+;
                     ",CDTABKEY="+cp_ToStrODBC(this.w_CDTABKEY)+;
                     ",CDATTINF="+cp_ToStrODBC(this.w_CDATTINF)+;
                     ",CDVLPRED="+cp_ToStrODBC(this.w_CDVLPRED)+;
                     ",CDCREARA="+cp_ToStrODBC(this.w_CDCREARA)+;
                     ",CDNATAWE="+cp_ToStrODBC(this.w_CDNATAWE)+;
                     ",CDFLGSOS="+cp_ToStrODBC(this.w_CDFLGSOS)+;
                     ",CDATTPRI="+cp_ToStrODBC(this.w_CDATTPRI)+;
                     ",CDATTSOS="+cp_ToStrODBCNull(this.w_CDATTSOS)+;
                     ",CDRIFTEM="+cp_ToStrODBC(this.w_CDRIFTEM)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,CDCODATT=this.w_CDCODATT;
                     ,CDDESATT=this.w_CDDESATT;
                     ,CDTIPATT=this.w_CDTIPATT;
                     ,CDCAMCUR=this.w_CDCAMCUR;
                     ,CDCHKOBB=this.w_CDCHKOBB;
                     ,CDTABKEY=this.w_CDTABKEY;
                     ,CDATTINF=this.w_CDATTINF;
                     ,CDVLPRED=this.w_CDVLPRED;
                     ,CDCREARA=this.w_CDCREARA;
                     ,CDNATAWE=this.w_CDNATAWE;
                     ,CDFLGSOS=this.w_CDFLGSOS;
                     ,CDATTPRI=this.w_CDATTPRI;
                     ,CDATTSOS=this.w_CDATTSOS;
                     ,CDRIFTEM=this.w_CDRIFTEM;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask header and footer children to save themselves
      * --- GSUT_MPE : Saving
      this.GSUT_MPE.ChangeRow(this.cRowID+'      1',0;
             ,this.w_CDCODCLA,"PECODCLA";
             )
      this.GSUT_MPE.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- gsut_mcd
    If !Isalt() and not(bTrsErr) and g_DOCM='S'
       this.WorkFromTrs()
       This.Notifyevent('ControlliFinali')
    Endif
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSUT_MPE : Deleting
    this.GSUT_MPE.ChangeRow(this.cRowID+'      1',0;
           ,this.w_CDCODCLA,"PECODCLA";
           )
    this.GSUT_MPE.mDelete()
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_CDDESATT)) and not empty(t_CDCODATT)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.PRODCLAS_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.PRODCLAS_IDX,2])
        *
        * delete PRODCLAS
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.PROMCLAS_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])
        *
        * delete PROMCLAS
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_CDDESATT)) and not empty(t_CDCODATT)) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PROMCLAS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])
    if i_bUpd
      with this
          .w_HIDEDM = g_DMIP='S' OR g_DOCM<>'S'
          .w_ISALT = IsAlt()
        .DoRTCalc(3,4,.t.)
          .w_READAZI = i_codazi
          .link_1_6('Full')
        .DoRTCalc(6,7,.t.)
          .w_CDCLAAWE = .w_CDCODCLA
        .DoRTCalc(9,12,.t.)
        if .o_CDRIFDEF<>.w_CDRIFDEF
          .w_CDRIFTAB = IIF(.w_CDRIFDEF='N', space(20), .w_CDRIFTAB)
        endif
        if .o_CDMODALL<>.w_CDMODALL
          .w_CDFOLDER = iif(.w_CDMODALL#'F','N',.w_CDFOLDER)
        endif
        .DoRTCalc(15,18,.t.)
        if .o_CDTIPALL<>.w_CDTIPALL.or. .o_CDMODALL<>.w_CDMODALL
          .w_CDCOMTIP = iif(empty(.w_CDTIPALL) or .w_CDCLAPRA='S','N',.w_CDCOMTIP)
        endif
        if .o_CDTIPALL<>.w_CDTIPALL
          .w_CDCLAALL = space(5)
          .link_1_21('Full')
        endif
        if .o_CDCLAALL<>.w_CDCLAALL.or. .o_CDMODALL<>.w_CDMODALL.or. .o_CDCLAPRA<>.w_CDCLAPRA
          .w_CDCOMCLA = iif(empty(.w_CDCLAALL) or .w_CDCLAPRA='S','N',.w_CDCOMCLA)
        endif
        if .o_CDRIFDEF<>.w_CDRIFDEF
          .Calculate_AXYYFVFWZV()
        endif
        .DoRTCalc(22,25,.t.)
          .link_1_40('Full')
        .DoRTCalc(27,31,.t.)
        if .o_CDMODALL<>.w_CDMODALL
          .w_CDSERVER = iif(.w_CDMODALL='E', .w_XXSERVER, space(20))
        endif
        if .o_CDMODALL<>.w_CDMODALL
          .w_CD__PORT = iif(.w_CDMODALL='E', .w_XX__PORT, 0)
        endif
        if .o_CDMODALL<>.w_CDMODALL
          .w_CD__USER = iif(.w_CDMODALL='E', .w_XX__USER, space(20))
        endif
        if .o_CDMODALL<>.w_CDMODALL
          .w_CDPASSWD = iif(.w_CDMODALL='E', .w_XXPASSWD, space(20))
        endif
        if .o_CDMODALL<>.w_CDMODALL
          .w_X1PASSWD = iif(.w_CDMODALL='E', CifraCnf( ALLTRIM(.w_XXPASSWD) , 'D' ), space(20))
        endif
        if .o_CDMODALL<>.w_CDMODALL
          .w_CDARCHIVE = iif(.w_CDMODALL='E', .w_XXARCHIVE, space(20))
        endif
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate('*')
        if .o_CDMODALL<>.w_CDMODALL
          .w_CDFLCTRL = iif(.w_CDMODALL='S','N', 'S')
        endif
        if .o_CDCONSOS<>.w_CDCONSOS
          .w_CDFIRDIG = icase(.w_CDMODALL = 'E','N',.w_CDCONSOS='S','S','N')
        endif
        .DoRTCalc(40,40,.t.)
        if .o_CDCONSOS<>.w_CDCONSOS
          .w_CDCLASOS = iif(.w_CDCONSOS='S',.w_CDCLASOS,space(10))
          .link_1_64('Full')
        endif
        if .o_CDCONSOS<>.w_CDCONSOS
          .w_CDAVVSOS = iif(.w_CDCONSOS='S',.w_CDAVVSOS,0)
        endif
        if .o_CDALIASF<>.w_CDALIASF
          .w_CDALIASF = CHRTRAN(.w_CDALIASF,[^\/:*?"<>|&.' ],'')
        endif
        if .o_CDRIFTAB<>.w_CDRIFTAB
          .w_CDFLGDES = iif(.w_CDRIFTAB=='CAN_TIER',.w_CDFLGDES,'N')
        endif
        if .o_CDMODALL<>.w_CDMODALL
          .w_CDPUBWEB = iif(.w_CDMODALL $ 'IS', 'S', 'N')
        endif
        .DoRTCalc(46,46,.t.)
        if .o_CDMODALL<>.w_CDMODALL
          .w_CDERASEF = iif(.w_CDMODALL='I','S','N')
        endif
        .oPgFrm.Page1.oPag.oObj_1_72.Calculate()
        .DoRTCalc(48,48,.t.)
        if .o_CDTIPOBC<>.w_CDTIPOBC
          .w_CDREPOBC = ICASE(.w_CDTIPOBC='E', 'QUERY\GSUT_BCV.FRX', .w_CDTIPOBC='F', 'QUERY\GSUT1BCV.FRX', .w_CDTIPOBC='Q', 'QUERY\GSUT2BCV.FRX', 'QUERY\GSUT3BCV.FRX')
        endif
        if .o_CDCONSOS<>.w_CDCONSOS
          .Calculate_EEAFAMQCGS()
        endif
        if .o_CDCLASOS<>.w_CDCLASOS
          .Calculate_PPPFNVUTAR()
        endif
        if .o_CDCLAPRA<>.w_CDCLAPRA
          .Calculate_HUEDFVAJPU()
        endif
        .DoRTCalc(50,55,.t.)
        if .o_CDMODALL<>.w_CDMODALL
          .w_CDCLAPRA = iif(INLIST(.w_CDMODALL,'F','L') AND NOT EMPTY(.w_CDCLAPRA), .w_CDCLAPRA, 'N')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        if .o_CDCODATT<>.w_CDCODATT
          .Calculate_MXSYGIWEWL()
        endif
        .DoRTCalc(57,68,.t.)
        if .o_CDCONSOS<>.w_CDCONSOS
          .w_CDFLGSOS = iif(.w_CDCONSOS='S',.w_CDFLGSOS,'NN')
        endif
        if .o_CDCHKOBB<>.w_CDCHKOBB
          .w_CDATTPRI = iif(.w_CDCHKOBB='S',.w_CDATTPRI,'N')
        endif
        if .o_CDCONSOS<>.w_CDCONSOS.or. .o_CDCLASOS<>.w_CDCLASOS
          .w_CDATTSOS = iif(.w_CDCONSOS='S' and not empty(.w_CDCLASOS),.w_CDATTSOS,space(15))
          .link_2_15('Full')
        endif
        .DoRTCalc(72,73,.t.)
        if .o_CDFIRDIG<>.w_CDFIRDIG.or. .o_CDTIPATT<>.w_CDTIPATT
          .w_CDRIFTEM = iif(.w_CDFIRDIG='S' and .w_CDTIPATT='D',.w_CDRIFTEM,'N')
        endif
        if .o_CDATTINF<>.w_CDATTINF
          .Calculate_IVPCSXRYTE()
        endif
        if .o_CDPATSTD<>.w_CDPATSTD
          .Calculate_RQVOBXBJZH()
        endif
        if .o_CDTIPOBC<>.w_CDTIPOBC
          .Calculate_DDCWKXAXCL()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(75,81,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_CDNATAWE with this.w_CDNATAWE
      replace t_CODCLASOS with this.w_CODCLASOS
      replace t_KEYTYPE with this.w_KEYTYPE
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate('*')
        .oPgFrm.Page1.oPag.oObj_1_72.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_ZIGJGWTMXZ()
    with this
          * --- GETCDPATSTD
          .w_GETPATSTD = cp_getdir(IIF(EMPTY(.w_CDPATSTD),sys(5)+sys(2003),.w_CDPATSTD),"Percorso di archiviazione")
          .w_CDPATSTD = EVL(.w_GETPATSTD, .w_CDPATSTD)
    endwith
  endproc
  proc Calculate_AXYYFVFWZV()
    with this
          * --- Ricalcolo cdfolder, cdgetdes
          .w_CDFOLDER = IIF(.w_CDRIFDEF='N',' ',.w_CDFOLDER)
          .w_CDGETDES = IIF(.w_CDRIFDEF='N',' ',.w_CDGETDES)
    endwith
  endproc
  proc Calculate_YSIDYAOZHI()
    with this
          * --- Cifra password
          .w_CDPASSWD = CifraCnf( ALLTRIM(.w_X1PASSWD) , 'C' )
    endwith
  endproc
  proc Calculate_EYFXYUGQKX()
    with this
          * --- Decifra password
          .w_X1PASSWD = CifraCnf( ALLTRIM(.w_CDPASSWD) , 'C' )
    endwith
  endproc
  proc Calculate_GNLMCWROQV()
    with this
          * --- getcdrepobc
          .w_GETREPOBC = GETFILE("frx")
          .w_OKREPOBC = ChkfileFRX(.w_GETREPOBC)
          .w_CDREPOBC = IIF(.w_OKREPOBC, .w_GETREPOBC, .w_CDREPOBC)
    endwith
  endproc
  proc Calculate_MECHOCOWAR()
    with this
          * --- Controlli su attributi SOStitutiva
          GSDM_BVC(this;
              ,'C';
             )
    endwith
  endproc
  proc Calculate_EEAFAMQCGS()
    with this
          * --- Inizializzazione colonne per gestione attributi SOStitutiva
          GSDM_BVC(this;
              ,'I';
             )
    endwith
  endproc
  proc Calculate_PPPFNVUTAR()
    with this
          * --- Gestione bottone inserimento attributi SOStitutiva di SIstema
          .w_CDKEYUPD = 4556
    endwith
  endproc
  proc Calculate_TQUZMTIBSA()
    with this
          * --- salvataggio nel caso di cambio stato con Infinity DMS
          .w_OLDCDMODALL = .w_CDMODALL
          .w_CDMODALL = iif(g_DMIP='S' AND .w_CDMODALL<>'S', 'I', .w_CDMODALL)
    endwith
  endproc
  proc Calculate_INLIYTIJMT()
    with this
          * --- salvataggio nel caso di cambio stato con Infinity DMS
          .w_CDMODALL = iif(g_DMIP='S' AND .w_CDMODALL<>'S', 'I', .w_CDMODALL)
          .bUpdated = .w_CDMODALL<>.w_OLDCDMODALL
          .bHeaderUpdated = .w_CDMODALL<>.w_OLDCDMODALL
    endwith
  endproc
  proc Calculate_JIJBJYMSLK()
    with this
          * --- Controlli  per AlterEgo
          GSDM_BVC(this;
              ,'A';
             )
    endwith
  endproc
  proc Calculate_HUEDFVAJPU()
    with this
          * --- Gestione flag completa path per classe 'Pratica'
          .w_CDCOMTIP = iif(.w_CDCLAPRA='S','N',.w_CDCOMTIP)
          .w_CDCOMCLA = iif(.w_CDCLAPRA='S','N',.w_CDCOMCLA)
    endwith
  endproc
  proc Calculate_DNMDULINZX()
    with this
          * --- Controlli flag predefinita
          GSDM_BVC(this;
              ,'K';
             )
    endwith
  endproc
  proc Calculate_MXSYGIWEWL()
    with this
          * --- caratteri speciali in CDCODATT
          .w_CDCODATT = chkstrch(.w_CDCODATT)
    endwith
  endproc
  proc Calculate_IVPCSXRYTE()
    with this
          * --- Controllo per mancata valorizzazione CDATTINF se riepito da zoom
          .w_ChkCtrlCDATTINF = this.SetUpdateRow()
    endwith
  endproc
  proc Calculate_RQVOBXBJZH()
    with this
          * --- Controllo su cambio path di archiviazione
          conferma(this;
              ,"WARN";
             )
          .o_CDPATSTD = .w_CDPATSTD
    endwith
  endproc
  proc Calculate_HLWVWNQXQZ()
    with this
          * --- Aggiorno path di archiviazione
          conferma(this;
              ,"SAVE";
             )
    endwith
  endproc
  proc Calculate_DDCWKXAXCL()
    with this
          * --- Report di default
          .w_CDREPOBC = ICASE(.w_CDTIPOBC='E', 'QUERY\GSUT_BCV.FRX', .w_CDTIPOBC='F', 'QUERY\GSUT1BCV.FRX', .w_CDTIPOBC='Q', 'QUERY\GSUT2BCV.FRX', 'QUERY\GSUT3BCV.FRX')
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCDMODALL_1_11.enabled = this.oPgFrm.Page1.oPag.oCDMODALL_1_11.mCond()
    this.oPgFrm.Page1.oPag.oCDRIFDEF_1_13.enabled = this.oPgFrm.Page1.oPag.oCDRIFDEF_1_13.mCond()
    this.oPgFrm.Page1.oPag.oCDRIFTAB_1_14.enabled = this.oPgFrm.Page1.oPag.oCDRIFTAB_1_14.mCond()
    this.oPgFrm.Page1.oPag.oCDFOLDER_1_15.enabled = this.oPgFrm.Page1.oPag.oCDFOLDER_1_15.mCond()
    this.oPgFrm.Page1.oPag.oCDGETDES_1_16.enabled = this.oPgFrm.Page1.oPag.oCDGETDES_1_16.mCond()
    this.oPgFrm.Page1.oPag.oCDTIPRAG_1_17.enabled = this.oPgFrm.Page1.oPag.oCDTIPRAG_1_17.mCond()
    this.oPgFrm.Page1.oPag.oCDCAMRAG_1_18.enabled = this.oPgFrm.Page1.oPag.oCDCAMRAG_1_18.mCond()
    this.oPgFrm.Page1.oPag.oCDCOMTIP_1_20.enabled = this.oPgFrm.Page1.oPag.oCDCOMTIP_1_20.mCond()
    this.oPgFrm.Page1.oPag.oCDCOMCLA_1_22.enabled = this.oPgFrm.Page1.oPag.oCDCOMCLA_1_22.mCond()
    this.oPgFrm.Page1.oPag.oCDPATSTD_1_23.enabled = this.oPgFrm.Page1.oPag.oCDPATSTD_1_23.mCond()
    this.oPgFrm.Page1.oPag.oCDCONSOS_1_62.enabled = this.oPgFrm.Page1.oPag.oCDCONSOS_1_62.mCond()
    this.oPgFrm.Page1.oPag.oCDAVVSOS_1_65.enabled = this.oPgFrm.Page1.oPag.oCDAVVSOS_1_65.mCond()
    this.oPgFrm.Page1.oPag.oCDPUBWEB_1_69.enabled = this.oPgFrm.Page1.oPag.oCDPUBWEB_1_69.mCond()
    this.oPgFrm.Page1.oPag.oCDERASEF_1_71.enabled = this.oPgFrm.Page1.oPag.oCDERASEF_1_71.mCond()
    this.oPgFrm.Page1.oPag.oCDCLAPRA_1_95.enabled = this.oPgFrm.Page1.oPag.oCDCLAPRA_1_95.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_86.enabled = this.oPgFrm.Page1.oPag.oBtn_1_86.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oCDATTPRI_2_14.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oCDATTPRI_2_14.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCDATTSOS_2_15.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCDATTSOS_2_15.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCDRIFTEM_2_18.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCDRIFTEM_2_18.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Pages(2).enabled=not(this.w_CDMODALL='S')
    this.oPgFrm.Page1.oPag.oCDCLAPRA_1_7.visible=!this.oPgFrm.Page1.oPag.oCDCLAPRA_1_7.mHide()
    this.oPgFrm.Page1.oPag.oCDTIPARC_1_10.visible=!this.oPgFrm.Page1.oPag.oCDTIPARC_1_10.mHide()
    this.oPgFrm.Page1.oPag.oCDMODALL_1_11.visible=!this.oPgFrm.Page1.oPag.oCDMODALL_1_11.mHide()
    this.oPgFrm.Page1.oPag.oCDMODALL_1_12.visible=!this.oPgFrm.Page1.oPag.oCDMODALL_1_12.mHide()
    this.oPgFrm.Page1.oPag.oCDFOLDER_1_15.visible=!this.oPgFrm.Page1.oPag.oCDFOLDER_1_15.mHide()
    this.oPgFrm.Page1.oPag.oCDGETDES_1_16.visible=!this.oPgFrm.Page1.oPag.oCDGETDES_1_16.mHide()
    this.oPgFrm.Page1.oPag.oCDTIPRAG_1_17.visible=!this.oPgFrm.Page1.oPag.oCDTIPRAG_1_17.mHide()
    this.oPgFrm.Page1.oPag.oCDCAMRAG_1_18.visible=!this.oPgFrm.Page1.oPag.oCDCAMRAG_1_18.mHide()
    this.oPgFrm.Page1.oPag.oCDTIPALL_1_19.visible=!this.oPgFrm.Page1.oPag.oCDTIPALL_1_19.mHide()
    this.oPgFrm.Page1.oPag.oCDCOMTIP_1_20.visible=!this.oPgFrm.Page1.oPag.oCDCOMTIP_1_20.mHide()
    this.oPgFrm.Page1.oPag.oCDCLAALL_1_21.visible=!this.oPgFrm.Page1.oPag.oCDCLAALL_1_21.mHide()
    this.oPgFrm.Page1.oPag.oCDCOMCLA_1_22.visible=!this.oPgFrm.Page1.oPag.oCDCOMCLA_1_22.mHide()
    this.oPgFrm.Page1.oPag.oCDPATSTD_1_23.visible=!this.oPgFrm.Page1.oPag.oCDPATSTD_1_23.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_24.visible=!this.oPgFrm.Page1.oPag.oBtn_1_24.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_28.visible=!this.oPgFrm.Page1.oPag.oStr_1_28.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_29.visible=!this.oPgFrm.Page1.oPag.oStr_1_29.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_30.visible=!this.oPgFrm.Page1.oPag.oStr_1_30.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_32.visible=!this.oPgFrm.Page1.oPag.oStr_1_32.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_36.visible=!this.oPgFrm.Page1.oPag.oStr_1_36.mHide()
    this.oPgFrm.Page1.oPag.oDESCRALL_1_37.visible=!this.oPgFrm.Page1.oPag.oDESCRALL_1_37.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_38.visible=!this.oPgFrm.Page1.oPag.oStr_1_38.mHide()
    this.oPgFrm.Page1.oPag.oDESCCLA_1_39.visible=!this.oPgFrm.Page1.oPag.oDESCCLA_1_39.mHide()
    this.oPgFrm.Page1.oPag.oCDSERVER_1_46.visible=!this.oPgFrm.Page1.oPag.oCDSERVER_1_46.mHide()
    this.oPgFrm.Page1.oPag.oCD__PORT_1_47.visible=!this.oPgFrm.Page1.oPag.oCD__PORT_1_47.mHide()
    this.oPgFrm.Page1.oPag.oCD__USER_1_48.visible=!this.oPgFrm.Page1.oPag.oCD__USER_1_48.mHide()
    this.oPgFrm.Page1.oPag.oX1PASSWD_1_50.visible=!this.oPgFrm.Page1.oPag.oX1PASSWD_1_50.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_51.visible=!this.oPgFrm.Page1.oPag.oStr_1_51.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_52.visible=!this.oPgFrm.Page1.oPag.oStr_1_52.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_53.visible=!this.oPgFrm.Page1.oPag.oStr_1_53.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_54.visible=!this.oPgFrm.Page1.oPag.oStr_1_54.mHide()
    this.oPgFrm.Page1.oPag.oCDARCHIVE_1_55.visible=!this.oPgFrm.Page1.oPag.oCDARCHIVE_1_55.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_56.visible=!this.oPgFrm.Page1.oPag.oStr_1_56.mHide()
    this.oPgFrm.Page1.oPag.oCDFLCTRL_1_60.visible=!this.oPgFrm.Page1.oPag.oCDFLCTRL_1_60.mHide()
    this.oPgFrm.Page1.oPag.oCDFIRDIG_1_61.visible=!this.oPgFrm.Page1.oPag.oCDFIRDIG_1_61.mHide()
    this.oPgFrm.Page1.oPag.oCDCONSOS_1_62.visible=!this.oPgFrm.Page1.oPag.oCDCONSOS_1_62.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_63.visible=!this.oPgFrm.Page1.oPag.oStr_1_63.mHide()
    this.oPgFrm.Page1.oPag.oCDCLASOS_1_64.visible=!this.oPgFrm.Page1.oPag.oCDCLASOS_1_64.mHide()
    this.oPgFrm.Page1.oPag.oCDAVVSOS_1_65.visible=!this.oPgFrm.Page1.oPag.oCDAVVSOS_1_65.mHide()
    this.oPgFrm.Page1.oPag.oCDALIASF_1_66.visible=!this.oPgFrm.Page1.oPag.oCDALIASF_1_66.mHide()
    this.oPgFrm.Page1.oPag.oCDFLGDES_1_67.visible=!this.oPgFrm.Page1.oPag.oCDFLGDES_1_67.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_68.visible=!this.oPgFrm.Page1.oPag.oStr_1_68.mHide()
    this.oPgFrm.Page1.oPag.oCDPUBWEB_1_69.visible=!this.oPgFrm.Page1.oPag.oCDPUBWEB_1_69.mHide()
    this.oPgFrm.Page1.oPag.oCDNOMFIL_1_70.visible=!this.oPgFrm.Page1.oPag.oCDNOMFIL_1_70.mHide()
    this.oPgFrm.Page1.oPag.oCDERASEF_1_71.visible=!this.oPgFrm.Page1.oPag.oCDERASEF_1_71.mHide()
    this.oPgFrm.Page1.oPag.oCDTIPOBC_1_73.visible=!this.oPgFrm.Page1.oPag.oCDTIPOBC_1_73.mHide()
    this.oPgFrm.Page1.oPag.oCDREPOBC_1_74.visible=!this.oPgFrm.Page1.oPag.oCDREPOBC_1_74.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_75.visible=!this.oPgFrm.Page1.oPag.oBtn_1_75.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_79.visible=!this.oPgFrm.Page1.oPag.oStr_1_79.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_80.visible=!this.oPgFrm.Page1.oPag.oStr_1_80.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_86.visible=!this.oPgFrm.Page1.oPag.oBtn_1_86.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_88.visible=!this.oPgFrm.Page1.oPag.oStr_1_88.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_93.visible=!this.oPgFrm.Page1.oPag.oStr_1_93.mHide()
    this.oPgFrm.Page1.oPag.oCDCLAPRA_1_95.visible=!this.oPgFrm.Page1.oPag.oCDCLAPRA_1_95.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_99.visible=!this.oPgFrm.Page1.oPag.oStr_1_99.mHide()
    this.oPgFrm.Page1.oPag.oCDCLAINF_1_107.visible=!this.oPgFrm.Page1.oPag.oCDCLAINF_1_107.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_108.visible=!this.oPgFrm.Page1.oPag.oStr_1_108.mHide()
    this.oPgFrm.Page1.oPag.oCDOKSYNC_1_109.visible=!this.oPgFrm.Page1.oPag.oCDOKSYNC_1_109.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_110.visible=!this.oPgFrm.Page1.oPag.oBtn_1_110.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDATTINF_2_9.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDATTINF_2_9.mHide()
    this.oPgFrm.Page1.oPag.oCDCREARA_2_11.visible=!this.oPgFrm.Page1.oPag.oCDCREARA_2_11.mHide()
    this.oPgFrm.Page1.oPag.oCDFLGSOS_2_13.visible=!this.oPgFrm.Page1.oPag.oCDFLGSOS_2_13.mHide()
    this.oPgFrm.Page1.oPag.oCDATTPRI_2_14.visible=!this.oPgFrm.Page1.oPag.oCDATTPRI_2_14.mHide()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDATTSOS_2_15.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDATTSOS_2_15.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("GETPATSTD")
          .Calculate_ZIGJGWTMXZ()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_57.Event(cEvent)
        if lower(cEvent)==lower("Insert start") or lower(cEvent)==lower("Update start")
          .Calculate_YSIDYAOZHI()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Load")
          .Calculate_EYFXYUGQKX()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_72.Event(cEvent)
        if lower(cEvent)==lower("getcdrepobc")
          .Calculate_GNLMCWROQV()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("CheckAttSos")
          .Calculate_MECHOCOWAR()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Load") or lower(cEvent)==lower("Update start")
          .Calculate_TQUZMTIBSA()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Edit Started")
          .Calculate_INLIYTIJMT()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("CheckAet")
          .Calculate_JIJBJYMSLK()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("ControlliFinali")
          .Calculate_DNMDULINZX()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
        if lower(cEvent)==lower("Record Updated")
          .Calculate_HLWVWNQXQZ()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsut_mcd
    If (Cevent='w_CDCLAPRA Changed' or Cevent='w_CDMODALL Changed' ) and inlist(This.w_CDMODALL,'E','I') and This.w_CDCLAPRA='S'
      do case 
        case This.w_CDMODALL = 'E'
          Ah_errormsg("Errore: modalit� di archiviazione di tipo EDS, la classe non pu� essere predefinita")
        case This.w_CDMODALL = 'I'
           Ah_errormsg("Errore: modalit� di archiviazione di tipo Infinity D.M.S., la classe non pu� essere predefinita")
      endcase
      This.w_CDCLAPRA='N'
    Endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=READAZI
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_ALTE_IDX,3]
    i_lTable = "PAR_ALTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2], .t., this.PAR_ALTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PATIPARC";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_READAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_READAZI)
            select PACODAZI,PATIPARC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READAZI = NVL(_Link_.PACODAZI,space(10))
      this.w_ATIPARC = NVL(_Link_.PATIPARC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_READAZI = space(10)
      endif
      this.w_ATIPARC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_ALTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CDTIPALL
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_ALLE_IDX,3]
    i_lTable = "TIP_ALLE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_ALLE_IDX,2], .t., this.TIP_ALLE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_ALLE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CDTIPALL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSUT_MTA',True,'TIP_ALLE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TACODICE like "+cp_ToStrODBC(trim(this.w_CDTIPALL)+"%");

          i_ret=cp_SQL(i_nConn,"select TACODICE,TADESCRI,TACOPTIP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TACODICE',trim(this.w_CDTIPALL))
          select TACODICE,TADESCRI,TACOPTIP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CDTIPALL)==trim(_Link_.TACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CDTIPALL) and !this.bDontReportError
            deferred_cp_zoom('TIP_ALLE','*','TACODICE',cp_AbsName(oSource.parent,'oCDTIPALL_1_19'),i_cWhere,'GSUT_MTA',"Tipologie allegato",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TADESCRI,TACOPTIP";
                     +" from "+i_cTable+" "+i_lTable+" where TACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODICE',oSource.xKey(1))
            select TACODICE,TADESCRI,TACOPTIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CDTIPALL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TADESCRI,TACOPTIP";
                   +" from "+i_cTable+" "+i_lTable+" where TACODICE="+cp_ToStrODBC(this.w_CDTIPALL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODICE',this.w_CDTIPALL)
            select TACODICE,TADESCRI,TACOPTIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CDTIPALL = NVL(_Link_.TACODICE,space(5))
      this.w_DESCRALL = NVL(_Link_.TADESCRI,space(35))
      this.w_CDCOMTIP = NVL(_Link_.TACOPTIP,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CDTIPALL = space(5)
      endif
      this.w_DESCRALL = space(35)
      this.w_CDCOMTIP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_ALLE_IDX,2])+'\'+cp_ToStr(_Link_.TACODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_ALLE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CDTIPALL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_19(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIP_ALLE_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIP_ALLE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_19.TACODICE as TACODICE119"+ ",link_1_19.TADESCRI as TADESCRI119"+ ",link_1_19.TACOPTIP as TACOPTIP119"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_19 on PROMCLAS.CDTIPALL=link_1_19.TACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_19"
          i_cKey=i_cKey+'+" and PROMCLAS.CDTIPALL=link_1_19.TACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CDCLAALL
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_ALLE_IDX,3]
    i_lTable = "CLA_ALLE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_ALLE_IDX,2], .t., this.CLA_ALLE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_ALLE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CDCLAALL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSUT_MTA',True,'CLA_ALLE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TACODCLA like "+cp_ToStrODBC(trim(this.w_CDCLAALL)+"%");
                   +" and TACODICE="+cp_ToStrODBC(this.w_CDTIPALL);

          i_ret=cp_SQL(i_nConn,"select TACODICE,TACODCLA,TACLADES,TACOPCLA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TACODICE,TACODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TACODICE',this.w_CDTIPALL;
                     ,'TACODCLA',trim(this.w_CDCLAALL))
          select TACODICE,TACODCLA,TACLADES,TACOPCLA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TACODICE,TACODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CDCLAALL)==trim(_Link_.TACODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CDCLAALL) and !this.bDontReportError
            deferred_cp_zoom('CLA_ALLE','*','TACODICE,TACODCLA',cp_AbsName(oSource.parent,'oCDCLAALL_1_21'),i_cWhere,'GSUT_MTA',"Classi allegato",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CDTIPALL<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TACODCLA,TACLADES,TACOPCLA";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TACODICE,TACODCLA,TACLADES,TACOPCLA;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TACODCLA,TACLADES,TACOPCLA";
                     +" from "+i_cTable+" "+i_lTable+" where TACODCLA="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TACODICE="+cp_ToStrODBC(this.w_CDTIPALL);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODICE',oSource.xKey(1);
                       ,'TACODCLA',oSource.xKey(2))
            select TACODICE,TACODCLA,TACLADES,TACOPCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CDCLAALL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TACODCLA,TACLADES,TACOPCLA";
                   +" from "+i_cTable+" "+i_lTable+" where TACODCLA="+cp_ToStrODBC(this.w_CDCLAALL);
                   +" and TACODICE="+cp_ToStrODBC(this.w_CDTIPALL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODICE',this.w_CDTIPALL;
                       ,'TACODCLA',this.w_CDCLAALL)
            select TACODICE,TACODCLA,TACLADES,TACOPCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CDCLAALL = NVL(_Link_.TACODCLA,space(5))
      this.w_DESCCLA = NVL(_Link_.TACLADES,space(35))
      this.w_CDCOMCLA = NVL(_Link_.TACOPCLA,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CDCLAALL = space(5)
      endif
      this.w_DESCCLA = space(35)
      this.w_CDCOMCLA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_ALLE_IDX,2])+'\'+cp_ToStr(_Link_.TACODICE,1)+'\'+cp_ToStr(_Link_.TACODCLA,1)
      cp_ShowWarn(i_cKey,this.CLA_ALLE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CDCLAALL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_21(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CLA_ALLE_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CLA_ALLE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_21.TACODCLA as TACODCLA121"+ ",link_1_21.TACLADES as TACLADES121"+ ",link_1_21.TACOPCLA as TACOPCLA121"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_21 on PROMCLAS.CDCLAALL=link_1_21.TACODCLA"+" and PROMCLAS.CDTIPALL=link_1_21.TACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_21"
          i_cKey=i_cKey+'+" and PROMCLAS.CDCLAALL=link_1_21.TACODCLA(+)"'+'+" and PROMCLAS.CDTIPALL=link_1_21.TACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CODAZI
  func Link_1_40(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PARA_EDS_IDX,3]
    i_lTable = "PARA_EDS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PARA_EDS_IDX,2], .t., this.PARA_EDS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PARA_EDS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDCODAZI,CDSERVER,CD__PORT,CD__USER,CDPASSWD,CDARCHIVE";
                   +" from "+i_cTable+" "+i_lTable+" where CDCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDCODAZI',this.w_CODAZI)
            select CDCODAZI,CDSERVER,CD__PORT,CD__USER,CDPASSWD,CDARCHIVE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.CDCODAZI,space(5))
      this.w_XXSERVER = NVL(_Link_.CDSERVER,space(20))
      this.w_XX__PORT = NVL(_Link_.CD__PORT,0)
      this.w_XX__USER = NVL(_Link_.CD__USER,space(20))
      this.w_XXPASSWD = NVL(_Link_.CDPASSWD,space(20))
      this.w_XXARCHIVE = NVL(_Link_.CDARCHIVE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_XXSERVER = space(20)
      this.w_XX__PORT = 0
      this.w_XX__USER = space(20)
      this.w_XXPASSWD = space(20)
      this.w_XXARCHIVE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PARA_EDS_IDX,2])+'\'+cp_ToStr(_Link_.CDCODAZI,1)
      cp_ShowWarn(i_cKey,this.PARA_EDS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CDCLASOS
  func Link_1_64(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLASMSOS_IDX,3]
    i_lTable = "CLASMSOS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLASMSOS_IDX,2], .t., this.CLASMSOS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLASMSOS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CDCLASOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSDM_KVS',True,'CLASMSOS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SZCODCLA like "+cp_ToStrODBC(trim(this.w_CDCLASOS)+"%");

          i_ret=cp_SQL(i_nConn,"select SZCODCLA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SZCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SZCODCLA',trim(this.w_CDCLASOS))
          select SZCODCLA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SZCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CDCLASOS)==trim(_Link_.SZCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CDCLASOS) and !this.bDontReportError
            deferred_cp_zoom('CLASMSOS','*','SZCODCLA',cp_AbsName(oSource.parent,'oCDCLASOS_1_64'),i_cWhere,'GSDM_KVS',"Elenco classi SOS",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SZCODCLA";
                     +" from "+i_cTable+" "+i_lTable+" where SZCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SZCODCLA',oSource.xKey(1))
            select SZCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CDCLASOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SZCODCLA";
                   +" from "+i_cTable+" "+i_lTable+" where SZCODCLA="+cp_ToStrODBC(this.w_CDCLASOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SZCODCLA',this.w_CDCLASOS)
            select SZCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CDCLASOS = NVL(_Link_.SZCODCLA,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_CDCLASOS = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLASMSOS_IDX,2])+'\'+cp_ToStr(_Link_.SZCODCLA,1)
      cp_ShowWarn(i_cKey,this.CLASMSOS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CDCLASOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CDATTSOS
  func Link_2_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLASDSOS_IDX,3]
    i_lTable = "CLASDSOS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLASDSOS_IDX,2], .t., this.CLASDSOS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLASDSOS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CDATTSOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CLASDSOS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SZCODKEY like "+cp_ToStrODBC(trim(this.w_CDATTSOS)+"%");
                   +" and SZCODCLA="+cp_ToStrODBC(this.w_CODCLASOS);

          i_ret=cp_SQL(i_nConn,"select SZCODCLA,SZCODKEY,SZTIPINT,SZTIPKEY";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SZCODCLA,SZCODKEY","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SZCODCLA',this.w_CODCLASOS;
                     ,'SZCODKEY',trim(this.w_CDATTSOS))
          select SZCODCLA,SZCODKEY,SZTIPINT,SZTIPKEY;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SZCODCLA,SZCODKEY into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CDATTSOS)==trim(_Link_.SZCODKEY) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CDATTSOS) and !this.bDontReportError
            deferred_cp_zoom('CLASDSOS','*','SZCODCLA,SZCODKEY',cp_AbsName(oSource.parent,'oCDATTSOS_2_15'),i_cWhere,'',"Elenco chiavi classe SOS",'GSUT_MCD.CLASDSOS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODCLASOS<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SZCODCLA,SZCODKEY,SZTIPINT,SZTIPKEY";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select SZCODCLA,SZCODKEY,SZTIPINT,SZTIPKEY;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SZCODCLA,SZCODKEY,SZTIPINT,SZTIPKEY";
                     +" from "+i_cTable+" "+i_lTable+" where SZCODKEY="+cp_ToStrODBC(oSource.xKey(2));
                     +" and SZCODCLA="+cp_ToStrODBC(this.w_CODCLASOS);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SZCODCLA',oSource.xKey(1);
                       ,'SZCODKEY',oSource.xKey(2))
            select SZCODCLA,SZCODKEY,SZTIPINT,SZTIPKEY;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CDATTSOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SZCODCLA,SZCODKEY,SZTIPINT,SZTIPKEY";
                   +" from "+i_cTable+" "+i_lTable+" where SZCODKEY="+cp_ToStrODBC(this.w_CDATTSOS);
                   +" and SZCODCLA="+cp_ToStrODBC(this.w_CODCLASOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SZCODCLA',this.w_CODCLASOS;
                       ,'SZCODKEY',this.w_CDATTSOS)
            select SZCODCLA,SZCODKEY,SZTIPINT,SZTIPKEY;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CDATTSOS = NVL(_Link_.SZCODKEY,space(10))
      this.w_CDFLGSOS = NVL(_Link_.SZTIPINT,space(2))
      this.w_KEYTYPE = NVL(_Link_.SZTIPKEY,space(8))
    else
      if i_cCtrl<>'Load'
        this.w_CDATTSOS = space(10)
      endif
      this.w_CDFLGSOS = space(2)
      this.w_KEYTYPE = space(8)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLASDSOS_IDX,2])+'\'+cp_ToStr(_Link_.SZCODCLA,1)+'\'+cp_ToStr(_Link_.SZCODKEY,1)
      cp_ShowWarn(i_cKey,this.CLASDSOS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CDATTSOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oCDCODCLA_1_4.value==this.w_CDCODCLA)
      this.oPgFrm.Page1.oPag.oCDCODCLA_1_4.value=this.w_CDCODCLA
    endif
    if not(this.oPgFrm.Page1.oPag.oCDDESCLA_1_5.value==this.w_CDDESCLA)
      this.oPgFrm.Page1.oPag.oCDDESCLA_1_5.value=this.w_CDDESCLA
    endif
    if not(this.oPgFrm.Page1.oPag.oCDCLAPRA_1_7.RadioValue()==this.w_CDCLAPRA)
      this.oPgFrm.Page1.oPag.oCDCLAPRA_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCDTIPARC_1_10.RadioValue()==this.w_CDTIPARC)
      this.oPgFrm.Page1.oPag.oCDTIPARC_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCDMODALL_1_11.RadioValue()==this.w_CDMODALL)
      this.oPgFrm.Page1.oPag.oCDMODALL_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCDMODALL_1_12.RadioValue()==this.w_CDMODALL)
      this.oPgFrm.Page1.oPag.oCDMODALL_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCDRIFDEF_1_13.RadioValue()==this.w_CDRIFDEF)
      this.oPgFrm.Page1.oPag.oCDRIFDEF_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCDRIFTAB_1_14.value==this.w_CDRIFTAB)
      this.oPgFrm.Page1.oPag.oCDRIFTAB_1_14.value=this.w_CDRIFTAB
    endif
    if not(this.oPgFrm.Page1.oPag.oCDFOLDER_1_15.RadioValue()==this.w_CDFOLDER)
      this.oPgFrm.Page1.oPag.oCDFOLDER_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCDGETDES_1_16.RadioValue()==this.w_CDGETDES)
      this.oPgFrm.Page1.oPag.oCDGETDES_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCDTIPRAG_1_17.RadioValue()==this.w_CDTIPRAG)
      this.oPgFrm.Page1.oPag.oCDTIPRAG_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCDCAMRAG_1_18.value==this.w_CDCAMRAG)
      this.oPgFrm.Page1.oPag.oCDCAMRAG_1_18.value=this.w_CDCAMRAG
    endif
    if not(this.oPgFrm.Page1.oPag.oCDTIPALL_1_19.value==this.w_CDTIPALL)
      this.oPgFrm.Page1.oPag.oCDTIPALL_1_19.value=this.w_CDTIPALL
    endif
    if not(this.oPgFrm.Page1.oPag.oCDCOMTIP_1_20.RadioValue()==this.w_CDCOMTIP)
      this.oPgFrm.Page1.oPag.oCDCOMTIP_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCDCLAALL_1_21.value==this.w_CDCLAALL)
      this.oPgFrm.Page1.oPag.oCDCLAALL_1_21.value=this.w_CDCLAALL
    endif
    if not(this.oPgFrm.Page1.oPag.oCDCOMCLA_1_22.RadioValue()==this.w_CDCOMCLA)
      this.oPgFrm.Page1.oPag.oCDCOMCLA_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCDPATSTD_1_23.value==this.w_CDPATSTD)
      this.oPgFrm.Page1.oPag.oCDPATSTD_1_23.value=this.w_CDPATSTD
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRALL_1_37.value==this.w_DESCRALL)
      this.oPgFrm.Page1.oPag.oDESCRALL_1_37.value=this.w_DESCRALL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCCLA_1_39.value==this.w_DESCCLA)
      this.oPgFrm.Page1.oPag.oDESCCLA_1_39.value=this.w_DESCCLA
    endif
    if not(this.oPgFrm.Page1.oPag.oCDSERVER_1_46.value==this.w_CDSERVER)
      this.oPgFrm.Page1.oPag.oCDSERVER_1_46.value=this.w_CDSERVER
    endif
    if not(this.oPgFrm.Page1.oPag.oCD__PORT_1_47.value==this.w_CD__PORT)
      this.oPgFrm.Page1.oPag.oCD__PORT_1_47.value=this.w_CD__PORT
    endif
    if not(this.oPgFrm.Page1.oPag.oCD__USER_1_48.value==this.w_CD__USER)
      this.oPgFrm.Page1.oPag.oCD__USER_1_48.value=this.w_CD__USER
    endif
    if not(this.oPgFrm.Page1.oPag.oX1PASSWD_1_50.value==this.w_X1PASSWD)
      this.oPgFrm.Page1.oPag.oX1PASSWD_1_50.value=this.w_X1PASSWD
    endif
    if not(this.oPgFrm.Page1.oPag.oCDARCHIVE_1_55.value==this.w_CDARCHIVE)
      this.oPgFrm.Page1.oPag.oCDARCHIVE_1_55.value=this.w_CDARCHIVE
    endif
    if not(this.oPgFrm.Page1.oPag.oCDFLCTRL_1_60.RadioValue()==this.w_CDFLCTRL)
      this.oPgFrm.Page1.oPag.oCDFLCTRL_1_60.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCDFIRDIG_1_61.RadioValue()==this.w_CDFIRDIG)
      this.oPgFrm.Page1.oPag.oCDFIRDIG_1_61.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCDCONSOS_1_62.RadioValue()==this.w_CDCONSOS)
      this.oPgFrm.Page1.oPag.oCDCONSOS_1_62.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCDCLASOS_1_64.value==this.w_CDCLASOS)
      this.oPgFrm.Page1.oPag.oCDCLASOS_1_64.value=this.w_CDCLASOS
    endif
    if not(this.oPgFrm.Page1.oPag.oCDAVVSOS_1_65.value==this.w_CDAVVSOS)
      this.oPgFrm.Page1.oPag.oCDAVVSOS_1_65.value=this.w_CDAVVSOS
    endif
    if not(this.oPgFrm.Page1.oPag.oCDALIASF_1_66.value==this.w_CDALIASF)
      this.oPgFrm.Page1.oPag.oCDALIASF_1_66.value=this.w_CDALIASF
    endif
    if not(this.oPgFrm.Page1.oPag.oCDFLGDES_1_67.RadioValue()==this.w_CDFLGDES)
      this.oPgFrm.Page1.oPag.oCDFLGDES_1_67.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCDPUBWEB_1_69.RadioValue()==this.w_CDPUBWEB)
      this.oPgFrm.Page1.oPag.oCDPUBWEB_1_69.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCDNOMFIL_1_70.value==this.w_CDNOMFIL)
      this.oPgFrm.Page1.oPag.oCDNOMFIL_1_70.value=this.w_CDNOMFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oCDERASEF_1_71.RadioValue()==this.w_CDERASEF)
      this.oPgFrm.Page1.oPag.oCDERASEF_1_71.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCDTIPOBC_1_73.RadioValue()==this.w_CDTIPOBC)
      this.oPgFrm.Page1.oPag.oCDTIPOBC_1_73.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCDREPOBC_1_74.value==this.w_CDREPOBC)
      this.oPgFrm.Page1.oPag.oCDREPOBC_1_74.value=this.w_CDREPOBC
    endif
    if not(this.oPgFrm.Page1.oPag.oCDCLAPRA_1_95.RadioValue()==this.w_CDCLAPRA)
      this.oPgFrm.Page1.oPag.oCDCLAPRA_1_95.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCDVLPRED_2_10.value==this.w_CDVLPRED)
      this.oPgFrm.Page1.oPag.oCDVLPRED_2_10.value=this.w_CDVLPRED
      replace t_CDVLPRED with this.oPgFrm.Page1.oPag.oCDVLPRED_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCDCREARA_2_11.RadioValue()==this.w_CDCREARA)
      this.oPgFrm.Page1.oPag.oCDCREARA_2_11.SetRadio()
      replace t_CDCREARA with this.oPgFrm.Page1.oPag.oCDCREARA_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCDFLGSOS_2_13.RadioValue()==this.w_CDFLGSOS)
      this.oPgFrm.Page1.oPag.oCDFLGSOS_2_13.SetRadio()
      replace t_CDFLGSOS with this.oPgFrm.Page1.oPag.oCDFLGSOS_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCDATTPRI_2_14.RadioValue()==this.w_CDATTPRI)
      this.oPgFrm.Page1.oPag.oCDATTPRI_2_14.SetRadio()
      replace t_CDATTPRI with this.oPgFrm.Page1.oPag.oCDATTPRI_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCDCLAINF_1_107.value==this.w_CDCLAINF)
      this.oPgFrm.Page1.oPag.oCDCLAINF_1_107.value=this.w_CDCLAINF
    endif
    if not(this.oPgFrm.Page1.oPag.oCDOKSYNC_1_109.RadioValue()==this.w_CDOKSYNC)
      this.oPgFrm.Page1.oPag.oCDOKSYNC_1_109.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDCODATT_2_2.value==this.w_CDCODATT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDCODATT_2_2.value=this.w_CDCODATT
      replace t_CDCODATT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDCODATT_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDDESATT_2_4.value==this.w_CDDESATT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDDESATT_2_4.value=this.w_CDDESATT
      replace t_CDDESATT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDDESATT_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDTIPATT_2_5.RadioValue()==this.w_CDTIPATT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDTIPATT_2_5.SetRadio()
      replace t_CDTIPATT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDTIPATT_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDCAMCUR_2_6.value==this.w_CDCAMCUR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDCAMCUR_2_6.value=this.w_CDCAMCUR
      replace t_CDCAMCUR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDCAMCUR_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDCHKOBB_2_7.RadioValue()==this.w_CDCHKOBB)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDCHKOBB_2_7.SetRadio()
      replace t_CDCHKOBB with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDCHKOBB_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDTABKEY_2_8.value==this.w_CDTABKEY)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDTABKEY_2_8.value=this.w_CDTABKEY
      replace t_CDTABKEY with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDTABKEY_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDATTINF_2_9.value==this.w_CDATTINF)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDATTINF_2_9.value=this.w_CDATTINF
      replace t_CDATTINF with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDATTINF_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDATTSOS_2_15.value==this.w_CDATTSOS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDATTSOS_2_15.value=this.w_CDATTSOS
      replace t_CDATTSOS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDATTSOS_2_15.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDRIFTEM_2_18.RadioValue()==this.w_CDRIFTEM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDRIFTEM_2_18.SetRadio()
      replace t_CDRIFTEM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDRIFTEM_2_18.value
    endif
    cp_SetControlsValueExtFlds(this,'PROMCLAS')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CDCODCLA))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCDCODCLA_1_4.SetFocus()
            i_bnoObbl = !empty(.w_CDCODCLA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CDMODALL) or not(inlist(.w_CDMODALL,'F','L','S') OR (.w_CDMODALL='E' AND g_AEDS='S') OR (.w_CDMODALL='I' AND g_CPIN='S')))  and not(g_DMIP='S')  and (g_DOCM='S')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCDMODALL_1_11.SetFocus()
            i_bnoObbl = !empty(.w_CDMODALL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CDMODALL))  and not(g_DMIP<>'S')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCDMODALL_1_12.SetFocus()
            i_bnoObbl = !empty(.w_CDMODALL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CDPATSTD))  and not(.w_CDMODALL<>'F' OR .w_HIDEDM)  and (.w_CDMODALL='F')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCDPATSTD_1_23.SetFocus()
            i_bnoObbl = !empty(.w_CDPATSTD)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CDSERVER))  and not(.w_CDMODALL<>'E' or g_AEDS<>'S' OR .w_HIDEDM)
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCDSERVER_1_46.SetFocus()
            i_bnoObbl = !empty(.w_CDSERVER)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CD__PORT))  and not(.w_CDMODALL<>'E' or g_AEDS<>'S' OR .w_HIDEDM)
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCD__PORT_1_47.SetFocus()
            i_bnoObbl = !empty(.w_CD__PORT)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CDARCHIVE))  and not(.w_CDMODALL<>'E' or g_AEDS<>'S' OR .w_HIDEDM)
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCDARCHIVE_1_55.SetFocus()
            i_bnoObbl = !empty(.w_CDARCHIVE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.w_CDCONSOS='S' and .w_CDFIRDIG='S') or .w_CDCONSOS#'S')  and not(.w_ISALT or .w_HIDEDM or .w_CDMODALL $ 'ISE' )
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCDFIRDIG_1_61.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non � consentito deselezionare il check firma digitale con attivo il check conservazione sostitutiva. Riattivarlo!")
          case   not(empty(.w_CDREPOBC) or JUSTEXT(.w_CDREPOBC)="FRX" and FILE(.w_CDREPOBC))  and not(not .w_CDMODALL $ 'IS')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCDREPOBC_1_74.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Percorso file non valido oppure estensione diversa da report")
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      *i_bRes = i_bRes .and. .GSUT_MPE.CheckForm()
      if i_bres
        i_bres=  .GSUT_MPE.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsut_mcd
      *Controlli eseguiti solo la classe ha attributi SOStitutiva, su univocit� e
      *completezza degli attributi inseriti.
      if (! empty(.w_CDCLASOS) and g_DOCM ='S') or .w_CDFIRDIG = 'S'
        .w_RESCHK=0
        this.notifyEvent('CheckAttSos')
        do case
          case ! empty(.w_CDCLASOS)
            if .w_RESCHK<>0			
              do case
                 case .w_RESCHK = 1
                    Ah_ErrorMsg('Sono presenti attributi SOStitutiva duplicati',64,'')
                    i_bRes=.f.
                 case .w_RESCHK = 2
                    Ah_ErrorMsg('Attenzione:%0Non sono stati inseriti tutti gli attributi della classe SOStitutiva usata, ad esempio: %1%0Gli indici associati a questa classe verranno scartati in fase di spedizione.',48,'',this.w_COKEYSOS)            
                 case .w_RESCHK = 3
                    Ah_ErrorMsg('Attenzione:%0Sono presenti attributi SOStitutiva duplicati%0Non sono stati inseriti tutti gli attributi della classe SOStitutiva usata, ad esempio: %1%0Gli indici associati a questa classe verranno scartati in fase di spedizione.',48,'',this.w_COKEYSOS)               
                    i_bRes=.f.
                 case .w_RESCHK = 4
                    Ah_ErrorMsg('Sono stati selezionati pi� campi per il riferimento temporale da utilizzare nella firma digitale',64,'')
                    i_bRes=.f.
                 case .w_RESCHK > 4  
                    Ah_ErrorMsg('- Sono presenti attributi SOStitutiva duplicati%0- Non sono stati inseriti tutti gli attributi della classe SOStitutiva usata, ad esempio :%1%0Gli indici associati a questa classe verranno scartati in fase di spedizione.',64,'',this.w_COKEYSOS)
                    i_bRes=.f.
              endcase
          	endif
          case empty(.w_CDCLASOS)  
            if .w_RESCHK = 4
              Ah_ErrorMsg('Sono stati selezionati pi� campi per il riferimento temporale da utilizzare nella firma digitale',64,'')
              i_bRes=.f.
            endif
        endcase  
      endif
      if .w_CDCLAPRA='S' and Empty(.w_CDRIFTAB) and i_bRes
          Ah_ErrorMsg('Attenzione classe predefinita, indicare tabella di riferimento',64,'')
          i_bRes=.f.
      endif
      *Controlli per AET, su check pratica e attributo primario 
      if i_bRes 
        .w_RESCHK=0
        if IsAlt() 
         this.notifyEvent('CheckAet') 
        endif
        if .w_RESCHK<>0			
              do case
                 case .w_RESCHK = 1
                    Ah_ErrorMsg('Sono stati attivati pi� attributi primari, deve essercene solo uno',64,'')
                    i_bRes=.f.
                 case .w_RESCHK = 2
                    Ah_ErrorMsg("Non � stato attivato alcun attributo primario o manca l'attributo relativo alla descrizione, deve essercene uno completo di descrizione",64,'')
                    i_bRes=.f.   
                 case .w_RESCHK = 3
                    Ah_ErrorMsg('Attributo primario non associato al campo CNCODCAN, selezione non ammessa',64,'')
                    i_bRes=.f.  
                 case .w_RESCHK = 4
                    Ah_ErrorMsg('Attributo primario privo di tabella, selezione non ammessa',64,'')
                    i_bRes=.f.     
                 case .w_RESCHK >= 5
                     Ah_ErrorMsg('Sono presenti altre classi con attivo il check classe pratiche, selezione non ammessa',64,'')
                    i_bRes=.f. 
              endcase  
        endif         
      endif
      
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (.cTrsName);
       where not(deleted()) and (not(Empty(t_CDDESATT)) and not empty(t_CDCODATT));
        into cursor __chk__
    if not(1<=cnt)
      do cp_ErrorMsg with cp_MsgFormat(MSG_NEEDED_AT_LEAST__ROWS,"1"),"","",.F.
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(EMPTY(.w_CDVLPRED) or IIF(.w_CDTIPATT='C', IIF(LEFT(.w_CDVLPRED,1) <>"'" and LEFT(.w_CDVLPRED,1) <>'"', .F., .T.), .T.)) and (not(Empty(.w_CDDESATT)) and not empty(.w_CDCODATT))
          .oNewFocus=.oPgFrm.Page1.oPag.oCDVLPRED_2_10
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Per gli attributi di tipo carattere, il valore predefinito deve essere tra apici o doppi apici")
      endcase
      if not(Empty(.w_CDDESATT)) and not empty(.w_CDCODATT)
        * --- Area Manuale = Check Row
        * --- gsut_mcd
        if this.w_CDFLGSOS='LI' and empty(this.w_CDCAMCUR) and empty(this.w_CDVLPRED) && Libera
           Ah_errormsg("Inserire in corrispondenza di una chiave SOS 'Libera', o un campo o un valore di default",16,'')
           i_bres=.f.
           i_bnoChk=.f.
        endif
        if inlist(this.w_CDFLGSOS,'HS','NM') and ! (empty(this.w_CDCAMCUR) and empty(this.w_CDVLPRED)) && Hash - Nome file
           Ah_errormsg("In corrispondenza di una chiave SOS 'Hash' o 'Nome file', non devono essere inseriti n� un campo n� un valore di default",16,'')
           i_bres=.f.
           i_bnoChk=.f.
        endif
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CDCLAPRA = this.w_CDCLAPRA
    this.o_CDMODALL = this.w_CDMODALL
    this.o_CDRIFDEF = this.w_CDRIFDEF
    this.o_CDRIFTAB = this.w_CDRIFTAB
    this.o_CDTIPALL = this.w_CDTIPALL
    this.o_CDCLAALL = this.w_CDCLAALL
    this.o_CDPATSTD = this.w_CDPATSTD
    this.o_CDFIRDIG = this.w_CDFIRDIG
    this.o_CDCONSOS = this.w_CDCONSOS
    this.o_CDCLASOS = this.w_CDCLASOS
    this.o_CDALIASF = this.w_CDALIASF
    this.o_CDTIPOBC = this.w_CDTIPOBC
    this.o_CDCODATT = this.w_CDCODATT
    this.o_CDTIPATT = this.w_CDTIPATT
    this.o_CDCHKOBB = this.w_CDCHKOBB
    this.o_CDATTINF = this.w_CDATTINF
    * --- GSUT_MPE : Depends On
    this.GSUT_MPE.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_CDDESATT)) and not empty(t_CDCODATT))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_CDCODATT=space(15)
      .w_CDDESATT=space(40)
      .w_CDTIPATT=space(1)
      .w_CDCAMCUR=space(0)
      .w_CDCHKOBB=space(1)
      .w_CDTABKEY=space(15)
      .w_CDATTINF=space(15)
      .w_CDVLPRED=space(50)
      .w_CDCREARA=space(1)
      .w_CDNATAWE=space(15)
      .w_CDFLGSOS=space(2)
      .w_CDATTPRI=space(1)
      .w_CDATTSOS=space(10)
      .w_CODCLASOS=space(10)
      .w_KEYTYPE=space(8)
      .w_CDRIFTEM=space(1)
      .DoRTCalc(1,62,.f.)
        .w_CDCHKOBB = 'N'
      .DoRTCalc(64,68,.f.)
        .w_CDFLGSOS = iif(.w_CDCONSOS='S',.w_CDFLGSOS,'NN')
        .w_CDATTPRI = iif(.w_CDCHKOBB='S',.w_CDATTPRI,'N')
        .w_CDATTSOS = iif(.w_CDCONSOS='S' and not empty(.w_CDCLASOS),.w_CDATTSOS,space(15))
      .DoRTCalc(71,71,.f.)
      if not(empty(.w_CDATTSOS))
        .link_2_15('Full')
      endif
        .w_CODCLASOS = .w_CDCLASOS
      .DoRTCalc(73,73,.f.)
        .w_CDRIFTEM = iif(.w_CDFIRDIG='S' and .w_CDTIPATT='D',.w_CDRIFTEM,'N')
    endwith
    this.DoRTCalc(75,81,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_CDCODATT = t_CDCODATT
    this.w_CDDESATT = t_CDDESATT
    this.w_CDTIPATT = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDTIPATT_2_5.RadioValue(.t.)
    this.w_CDCAMCUR = t_CDCAMCUR
    this.w_CDCHKOBB = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDCHKOBB_2_7.RadioValue(.t.)
    this.w_CDTABKEY = t_CDTABKEY
    this.w_CDATTINF = t_CDATTINF
    this.w_CDVLPRED = t_CDVLPRED
    this.w_CDCREARA = this.oPgFrm.Page1.oPag.oCDCREARA_2_11.RadioValue(.t.)
    this.w_CDNATAWE = t_CDNATAWE
    this.w_CDFLGSOS = this.oPgFrm.Page1.oPag.oCDFLGSOS_2_13.RadioValue(.t.)
    this.w_CDATTPRI = this.oPgFrm.Page1.oPag.oCDATTPRI_2_14.RadioValue(.t.)
    this.w_CDATTSOS = t_CDATTSOS
    this.w_CODCLASOS = t_CODCLASOS
    this.w_KEYTYPE = t_KEYTYPE
    this.w_CDRIFTEM = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDRIFTEM_2_18.RadioValue(.t.)
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_CDCODATT with this.w_CDCODATT
    replace t_CDDESATT with this.w_CDDESATT
    replace t_CDTIPATT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDTIPATT_2_5.ToRadio()
    replace t_CDCAMCUR with this.w_CDCAMCUR
    replace t_CDCHKOBB with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDCHKOBB_2_7.ToRadio()
    replace t_CDTABKEY with this.w_CDTABKEY
    replace t_CDATTINF with this.w_CDATTINF
    replace t_CDVLPRED with this.w_CDVLPRED
    replace t_CDCREARA with this.oPgFrm.Page1.oPag.oCDCREARA_2_11.ToRadio()
    replace t_CDNATAWE with this.w_CDNATAWE
    replace t_CDFLGSOS with this.oPgFrm.Page1.oPag.oCDFLGSOS_2_13.ToRadio()
    replace t_CDATTPRI with this.oPgFrm.Page1.oPag.oCDATTPRI_2_14.ToRadio()
    replace t_CDATTSOS with this.w_CDATTSOS
    replace t_CODCLASOS with this.w_CODCLASOS
    replace t_KEYTYPE with this.w_KEYTYPE
    replace t_CDRIFTEM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDRIFTEM_2_18.ToRadio()
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
  func CanEdit()
    local i_res
    i_res=g_REVI<>"S" OR this.w_CDOKSYNC<>"S"
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("La modifica della classe documentale deve essere eseguita su Infinity"))
    endif
    return(i_res)
  func CanAdd()
    local i_res
    i_res=g_REVI<>"S"
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Il caricamento delle classi documentali deve essere eseguito su Infinity"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsut_mcdPag1 as StdContainer
  Width  = 785
  height = 550
  stdWidth  = 785
  stdheight = 550
  resizeXpos=339
  resizeYpos=395
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCDCODCLA_1_4 as StdField with uid="OTVSTSMEPW",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CDCODCLA", cQueryName = "CDCODCLA",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 238406297,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=108, Left=153, Top=12, InputMask=replicate('X',15)

  add object oCDDESCLA_1_5 as StdField with uid="KXZYWKLJGH",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CDDESCLA", cQueryName = "CDDESCLA",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 223328921,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=265, Top=12, InputMask=replicate('X',50)

  add object oCDCLAPRA_1_7 as StdCheck with uid="SPOFHGXSEB",rtseq=6,rtrep=.f.,left=689, top=12, caption="Pratica",;
    ToolTipText = "Se attivo, la classe � il riferimento per l'archivio pratiche",;
    HelpContextID = 244790631,;
    cFormVar="w_CDCLAPRA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCDCLAPRA_1_7.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CDCLAPRA,&i_cF..t_CDCLAPRA),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oCDCLAPRA_1_7.GetRadio()
    this.Parent.oContained.w_CDCLAPRA = this.RadioValue()
    return .t.
  endfunc

  func oCDCLAPRA_1_7.ToRadio()
    this.Parent.oContained.w_CDCLAPRA=trim(this.Parent.oContained.w_CDCLAPRA)
    return(;
      iif(this.Parent.oContained.w_CDCLAPRA=='S',1,;
      0))
  endfunc

  func oCDCLAPRA_1_7.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCDCLAPRA_1_7.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (! .w_ISALT)
    endwith
    endif
  endfunc


  add object oCDTIPARC_1_10 as StdCombo with uid="NUGEISEJHG",rtseq=9,rtrep=.f.,left=153,top=40,width=165,height=21;
    , ToolTipText = "Tipo archiviazione: manuale o automatica";
    , HelpContextID = 8734057;
    , cFormVar="w_CDTIPARC",RowSource=""+"Manuale,"+"Automatica", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCDTIPARC_1_10.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CDTIPARC,&i_cF..t_CDTIPARC),this.value)
    return(iif(xVal =1,'M',;
    iif(xVal =2,'A',;
    space(1))))
  endfunc
  func oCDTIPARC_1_10.GetRadio()
    this.Parent.oContained.w_CDTIPARC = this.RadioValue()
    return .t.
  endfunc

  func oCDTIPARC_1_10.ToRadio()
    this.Parent.oContained.w_CDTIPARC=trim(this.Parent.oContained.w_CDTIPARC)
    return(;
      iif(this.Parent.oContained.w_CDTIPARC=='M',1,;
      iif(this.Parent.oContained.w_CDTIPARC=='A',2,;
      0)))
  endfunc

  func oCDTIPARC_1_10.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCDTIPARC_1_10.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_DOCM<>'S')
    endwith
    endif
  endfunc


  add object oCDMODALL_1_11 as StdCombo with uid="ZLEVCIMQZX",rtseq=10,rtrep=.f.,left=408,top=40,width=162,height=21;
    , ToolTipText = "Tipo archiviazione: copia file, collegamento, extended document server, Infinity D.M.S. o Infinity D.M.S. stand alone";
    , HelpContextID = 3484302;
    , cFormVar="w_CDMODALL",RowSource=""+"Collegamento,"+"Copia file,"+"Extended document server,"+"Infinity D.M.S.,"+"Infinity D.M.S. stand alone", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oCDMODALL_1_11.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CDMODALL,&i_cF..t_CDMODALL),this.value)
    return(iif(xVal =1,'L',;
    iif(xVal =2,'F',;
    iif(xVal =3,'E',;
    iif(xVal =4,'I',;
    iif(xVal =5,'S',;
    space(1)))))))
  endfunc
  func oCDMODALL_1_11.GetRadio()
    this.Parent.oContained.w_CDMODALL = this.RadioValue()
    return .t.
  endfunc

  func oCDMODALL_1_11.ToRadio()
    this.Parent.oContained.w_CDMODALL=trim(this.Parent.oContained.w_CDMODALL)
    return(;
      iif(this.Parent.oContained.w_CDMODALL=='L',1,;
      iif(this.Parent.oContained.w_CDMODALL=='F',2,;
      iif(this.Parent.oContained.w_CDMODALL=='E',3,;
      iif(this.Parent.oContained.w_CDMODALL=='I',4,;
      iif(this.Parent.oContained.w_CDMODALL=='S',5,;
      0))))))
  endfunc

  func oCDMODALL_1_11.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCDMODALL_1_11.mCond()
    with this.Parent.oContained
      return (g_DOCM='S')
    endwith
  endfunc

  func oCDMODALL_1_11.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_DMIP='S')
    endwith
    endif
  endfunc

  func oCDMODALL_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (inlist(.w_CDMODALL,'F','L','S') OR (.w_CDMODALL='E' AND g_AEDS='S') OR (.w_CDMODALL='I' AND g_CPIN='S'))
    endwith
    return bRes
  endfunc


  add object oCDMODALL_1_12 as StdCombo with uid="POUKVHPTMO",rtseq=11,rtrep=.f.,left=408,top=40,width=162,height=21;
    , ToolTipText = "Tipo archiviazione: Infinity D.M.S. o Infinity D.M.S. stand alone";
    , HelpContextID = 3484302;
    , cFormVar="w_CDMODALL",RowSource=""+"Infinity D.M.S.,"+"Infinity D.M.S. stand alone", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oCDMODALL_1_12.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CDMODALL,&i_cF..t_CDMODALL),this.value)
    return(iif(xVal =1,'I',;
    iif(xVal =2,'S',;
    space(1))))
  endfunc
  func oCDMODALL_1_12.GetRadio()
    this.Parent.oContained.w_CDMODALL = this.RadioValue()
    return .t.
  endfunc

  func oCDMODALL_1_12.ToRadio()
    this.Parent.oContained.w_CDMODALL=trim(this.Parent.oContained.w_CDMODALL)
    return(;
      iif(this.Parent.oContained.w_CDMODALL=='I',1,;
      iif(this.Parent.oContained.w_CDMODALL=='S',2,;
      0)))
  endfunc

  func oCDMODALL_1_12.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCDMODALL_1_12.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_DMIP<>'S')
    endwith
    endif
  endfunc


  add object oCDRIFDEF_1_13 as StdCombo with uid="DUUHJPUDNE",rtseq=12,rtrep=.f.,left=153,top=67,width=165,height=21;
    , ToolTipText = "Indicare se la classe � di riferimento per una tabella per l'archiviazione rapida";
    , HelpContextID = 48571756;
    , cFormVar="w_CDRIFDEF",RowSource=""+"Non presente,"+"Selezionabile,"+"Default,"+"Standard", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCDRIFDEF_1_13.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CDRIFDEF,&i_cF..t_CDRIFDEF),this.value)
    return(iif(xVal =1,'N',;
    iif(xVal =2,'A',;
    iif(xVal =3,'D',;
    iif(xVal =4,'S',;
    space(1))))))
  endfunc
  func oCDRIFDEF_1_13.GetRadio()
    this.Parent.oContained.w_CDRIFDEF = this.RadioValue()
    return .t.
  endfunc

  func oCDRIFDEF_1_13.ToRadio()
    this.Parent.oContained.w_CDRIFDEF=trim(this.Parent.oContained.w_CDRIFDEF)
    return(;
      iif(this.Parent.oContained.w_CDRIFDEF=='N',1,;
      iif(this.Parent.oContained.w_CDRIFDEF=='A',2,;
      iif(this.Parent.oContained.w_CDRIFDEF=='D',3,;
      iif(this.Parent.oContained.w_CDRIFDEF=='S',4,;
      0)))))
  endfunc

  func oCDRIFDEF_1_13.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCDRIFDEF_1_13.mCond()
    with this.Parent.oContained
      return (!.w_ISALT)
    endwith
  endfunc

  add object oCDRIFTAB_1_14 as StdField with uid="WDIHKDTNJF",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CDRIFTAB", cQueryName = "CDRIFTAB",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Tabella di riferimento per classe (x archiviazione rapida)",;
    HelpContextID = 48571752,;
   bGlobalFont=.t.,;
    Height=20, Width=162, Left=408, Top=67, InputMask=replicate('X',20)

  func oCDRIFTAB_1_14.mCond()
    with this.Parent.oContained
      return (.w_CDRIFDEF<>'N')
    endwith
  endfunc

  add object oCDFOLDER_1_15 as StdCheck with uid="LXVFEOAWVF",rtseq=14,rtrep=.f.,left=575, top=43, caption="Cartella contenitore",;
    ToolTipText = "Se attivo, in caso di archiviazione rapida, apre una cartella vuota come raccoglitore di documenti da archiviare",;
    HelpContextID = 55207288,;
    cFormVar="w_CDFOLDER", bObbl = .f. , nPag = 1;
    , TABSTOP=.F.;
   , bGlobalFont=.t.


  func oCDFOLDER_1_15.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CDFOLDER,&i_cF..t_CDFOLDER),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oCDFOLDER_1_15.GetRadio()
    this.Parent.oContained.w_CDFOLDER = this.RadioValue()
    return .t.
  endfunc

  func oCDFOLDER_1_15.ToRadio()
    this.Parent.oContained.w_CDFOLDER=trim(this.Parent.oContained.w_CDFOLDER)
    return(;
      iif(this.Parent.oContained.w_CDFOLDER=='S',1,;
      0))
  endfunc

  func oCDFOLDER_1_15.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCDFOLDER_1_15.mCond()
    with this.Parent.oContained
      return (.w_CDRIFDEF<>'N' and .w_CDMODALL='F' AND !.w_ISALT)
    endwith
  endfunc

  func oCDFOLDER_1_15.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_HIDEDM)
    endwith
    endif
  endfunc

  add object oCDGETDES_1_16 as StdCheck with uid="KBWUAFREJM",rtseq=15,rtrep=.f.,left=575, top=67, caption="Commento archiviazione",;
    ToolTipText = "Se attivo, in caso di archiviazione rapida, richiede commento ai documenti da archiviare",;
    HelpContextID = 62944633,;
    cFormVar="w_CDGETDES", bObbl = .f. , nPag = 1;
    , TABSTOP=.F.;
   , bGlobalFont=.t.


  func oCDGETDES_1_16.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CDGETDES,&i_cF..t_CDGETDES),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oCDGETDES_1_16.GetRadio()
    this.Parent.oContained.w_CDGETDES = this.RadioValue()
    return .t.
  endfunc

  func oCDGETDES_1_16.ToRadio()
    this.Parent.oContained.w_CDGETDES=trim(this.Parent.oContained.w_CDGETDES)
    return(;
      iif(this.Parent.oContained.w_CDGETDES=='S',1,;
      0))
  endfunc

  func oCDGETDES_1_16.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCDGETDES_1_16.mCond()
    with this.Parent.oContained
      return (.w_CDRIFDEF<>'N' AND !.w_ISALT AND .w_CDMODALL<>'S')
    endwith
  endfunc

  func oCDGETDES_1_16.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_HIDEDM)
    endwith
    endif
  endfunc


  add object oCDTIPRAG_1_17 as StdCombo with uid="UHPXUZGYAZ",rtseq=16,rtrep=.f.,left=153,top=94,width=165,height=21;
    , ToolTipText = "Tipo di raggruppamento";
    , HelpContextID = 25511277;
    , cFormVar="w_CDTIPRAG",RowSource=""+"Settimana,"+"Mese,"+"Trimestre,"+"Quadrimestre,"+"Semestre,"+"Anno,"+"Nessuno", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCDTIPRAG_1_17.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CDTIPRAG,&i_cF..t_CDTIPRAG),this.value)
    return(iif(xVal =1,'W',;
    iif(xVal =2,'M',;
    iif(xVal =3,'T',;
    iif(xVal =4,'Q',;
    iif(xVal =5,'S',;
    iif(xVal =6,'A',;
    iif(xVal =7,'N',;
    space(1)))))))))
  endfunc
  func oCDTIPRAG_1_17.GetRadio()
    this.Parent.oContained.w_CDTIPRAG = this.RadioValue()
    return .t.
  endfunc

  func oCDTIPRAG_1_17.ToRadio()
    this.Parent.oContained.w_CDTIPRAG=trim(this.Parent.oContained.w_CDTIPRAG)
    return(;
      iif(this.Parent.oContained.w_CDTIPRAG=='W',1,;
      iif(this.Parent.oContained.w_CDTIPRAG=='M',2,;
      iif(this.Parent.oContained.w_CDTIPRAG=='T',3,;
      iif(this.Parent.oContained.w_CDTIPRAG=='Q',4,;
      iif(this.Parent.oContained.w_CDTIPRAG=='S',5,;
      iif(this.Parent.oContained.w_CDTIPRAG=='A',6,;
      iif(this.Parent.oContained.w_CDTIPRAG=='N',7,;
      0))))))))
  endfunc

  func oCDTIPRAG_1_17.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCDTIPRAG_1_17.mCond()
    with this.Parent.oContained
      return (.w_CDMODALL='F')
    endwith
  endfunc

  func oCDTIPRAG_1_17.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CDMODALL<>'F')
    endwith
    endif
  endfunc

  add object oCDCAMRAG_1_18 as StdField with uid="UGPCADCXLR",rtseq=17,rtrep=.f.,;
    cFormVar = "w_CDCAMRAG", cQueryName = "CDCAMRAG",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Campo data di raggruppamento (vuoto = data del giorno)",;
    HelpContextID = 21771629,;
   bGlobalFont=.t.,;
    Height=21, Width=330, Left=408, Top=94, InputMask=replicate('X',50)

  func oCDCAMRAG_1_18.mCond()
    with this.Parent.oContained
      return (.w_CDTIPRAG<>'N' and .w_CDMODALL='F')
    endwith
  endfunc

  func oCDCAMRAG_1_18.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CDMODALL<>'F')
    endwith
    endif
  endfunc

  add object oCDTIPALL_1_19 as StdField with uid="MEBDQMKGMI",rtseq=18,rtrep=.f.,;
    cFormVar = "w_CDTIPALL", cQueryName = "CDTIPALL",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 259701390,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=153, Top=121, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_ALLE", cZoomOnZoom="GSUT_MTA", oKey_1_1="TACODICE", oKey_1_2="this.w_CDTIPALL"

  func oCDTIPALL_1_19.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_HIDEDM)
    endwith
    endif
  endfunc

  func oCDTIPALL_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
      if .not. empty(.w_CDCLAALL)
        bRes2=.link_1_21('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCDTIPALL_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCDTIPALL_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_ALLE','*','TACODICE',cp_AbsName(this.parent,'oCDTIPALL_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSUT_MTA',"Tipologie allegato",'',this.parent.oContained
  endproc
  proc oCDTIPALL_1_19.mZoomOnZoom
    local i_obj
    i_obj=GSUT_MTA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TACODICE=this.parent.oContained.w_CDTIPALL
    i_obj.ecpSave()
  endproc

  add object oCDCOMTIP_1_20 as StdCheck with uid="RVXLDLQFYI",rtseq=19,rtrep=.f.,left=503, top=120, caption="Completa path file",;
    ToolTipText = "Se attivo completa il path di archiviazione file con il codice tipologia allegato",;
    HelpContextID = 212191882,;
    cFormVar="w_CDCOMTIP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCDCOMTIP_1_20.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CDCOMTIP,&i_cF..t_CDCOMTIP),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oCDCOMTIP_1_20.GetRadio()
    this.Parent.oContained.w_CDCOMTIP = this.RadioValue()
    return .t.
  endfunc

  func oCDCOMTIP_1_20.ToRadio()
    this.Parent.oContained.w_CDCOMTIP=trim(this.Parent.oContained.w_CDCOMTIP)
    return(;
      iif(this.Parent.oContained.w_CDCOMTIP=='S',1,;
      0))
  endfunc

  func oCDCOMTIP_1_20.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCDCOMTIP_1_20.mCond()
    with this.Parent.oContained
      return (.w_CDMODALL='F' and ! empty(.w_CDTIPALL)  and .w_CDCLAPRA#'S')
    endwith
  endfunc

  func oCDCOMTIP_1_20.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_HIDEDM)
    endwith
    endif
  endfunc

  add object oCDCLAALL_1_21 as StdField with uid="ETBUCWLTOD",rtseq=20,rtrep=.f.,;
    cFormVar = "w_CDCLAALL", cQueryName = "CDCLAALL",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 6867598,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=153, Top=148, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CLA_ALLE", cZoomOnZoom="GSUT_MTA", oKey_1_1="TACODICE", oKey_1_2="this.w_CDTIPALL", oKey_2_1="TACODCLA", oKey_2_2="this.w_CDCLAALL"

  func oCDCLAALL_1_21.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_HIDEDM)
    endwith
    endif
  endfunc

  func oCDCLAALL_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oCDCLAALL_1_21.ecpDrop(oSource)
    this.Parent.oContained.link_1_21('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCDCLAALL_1_21.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CLA_ALLE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TACODICE="+cp_ToStrODBC(this.Parent.oContained.w_CDTIPALL)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TACODICE="+cp_ToStr(this.Parent.oContained.w_CDTIPALL)
    endif
    do cp_zoom with 'CLA_ALLE','*','TACODICE,TACODCLA',cp_AbsName(this.parent,'oCDCLAALL_1_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSUT_MTA',"Classi allegato",'',this.parent.oContained
  endproc
  proc oCDCLAALL_1_21.mZoomOnZoom
    local i_obj
    i_obj=GSUT_MTA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TACODICE=w_CDTIPALL
     i_obj.w_TACODCLA=this.parent.oContained.w_CDCLAALL
    i_obj.ecpSave()
  endproc

  add object oCDCOMCLA_1_22 as StdCheck with uid="EWNJAJHNOJ",rtseq=21,rtrep=.f.,left=503, top=148, caption="Completa path file",;
    ToolTipText = "Se attivo completa il path di archiviazione file con il codice classe allegato",;
    HelpContextID = 228969113,;
    cFormVar="w_CDCOMCLA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCDCOMCLA_1_22.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CDCOMCLA,&i_cF..t_CDCOMCLA),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oCDCOMCLA_1_22.GetRadio()
    this.Parent.oContained.w_CDCOMCLA = this.RadioValue()
    return .t.
  endfunc

  func oCDCOMCLA_1_22.ToRadio()
    this.Parent.oContained.w_CDCOMCLA=trim(this.Parent.oContained.w_CDCOMCLA)
    return(;
      iif(this.Parent.oContained.w_CDCOMCLA=='S',1,;
      0))
  endfunc

  func oCDCOMCLA_1_22.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCDCOMCLA_1_22.mCond()
    with this.Parent.oContained
      return (.w_CDMODALL='F' and ! empty(.w_CDCLAALL) and .w_CDCLAPRA#'S')
    endwith
  endfunc

  func oCDCOMCLA_1_22.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_HIDEDM)
    endwith
    endif
  endfunc

  add object oCDPATSTD_1_23 as StdField with uid="RLMPBQFAQZ",rtseq=22,rtrep=.f.,;
    cFormVar = "w_CDPATSTD", cQueryName = "CDPATSTD",;
    bObbl = .t. , nPag = 1, value=space(75), bMultilanguage =  .f.,;
    ToolTipText = "Percorso standard di archiviazione",;
    HelpContextID = 45942122,;
   bGlobalFont=.t.,;
    Height=21, Width=585, Left=153, Top=199, InputMask=replicate('X',75)

  func oCDPATSTD_1_23.mCond()
    with this.Parent.oContained
      return (.w_CDMODALL='F')
    endwith
  endfunc

  func oCDPATSTD_1_23.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CDMODALL<>'F' OR .w_HIDEDM)
    endwith
    endif
  endfunc

  proc oCDPATSTD_1_23.mAfter
    with this.Parent.oContained
      .w_CDPATSTD=addbs(.w_CDPATSTD)
    endwith
  endproc


  add object oBtn_1_24 as StdButton with uid="DJSDBFCPVI",left=743, top=199, width=20,height=22,;
    caption="...", nPag=1;
    , HelpContextID = 96875050;
  , bGlobalFont=.t.

    proc oBtn_1_24.Click()
      with this.Parent.oContained
        .NotifyEvent("GETPATSTD")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_24.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CDMODALL<>'F' OR .w_HIDEDM)
    endwith
   endif
  endfunc

  add object oDESCRALL_1_37 as StdField with uid="NMLQZKBPVP",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESCRALL", cQueryName = "DESCRALL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 258001278,;
   bGlobalFont=.t.,;
    Height=21, Width=272, Left=216, Top=121, InputMask=replicate('X',35)

  func oDESCRALL_1_37.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_HIDEDM)
    endwith
    endif
  endfunc

  add object oDESCCLA_1_39 as StdField with uid="UNVMALFABC",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESCCLA", cQueryName = "DESCCLA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 179254838,;
   bGlobalFont=.t.,;
    Height=21, Width=272, Left=216, Top=148, InputMask=replicate('X',35)

  func oDESCCLA_1_39.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_HIDEDM)
    endwith
    endif
  endfunc

  add object oCDSERVER_1_46 as StdField with uid="NKSNTOCZXU",rtseq=32,rtrep=.f.,;
    cFormVar = "w_CDSERVER", cQueryName = "CDSERVER",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Server E.D.S.",;
    HelpContextID = 94451064,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=153, Top=174, InputMask=replicate('X',20)

  func oCDSERVER_1_46.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CDMODALL<>'E' or g_AEDS<>'S' OR .w_HIDEDM)
    endwith
    endif
  endfunc

  add object oCD__PORT_1_47 as StdField with uid="JXOWCOZSXN",rtseq=33,rtrep=.f.,;
    cFormVar = "w_CD__PORT", cQueryName = "CD__PORT",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Porta server E.D.S.",;
    HelpContextID = 245101946,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=501, Top=174, cSayPict='"999999"', cGetPict='"999999"'

  func oCD__PORT_1_47.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CDMODALL<>'E' or g_AEDS<>'S' OR .w_HIDEDM)
    endwith
    endif
  endfunc

  add object oCD__USER_1_48 as StdField with uid="PKLGKASPHP",rtseq=34,rtrep=.f.,;
    cFormVar = "w_CD__USER", cQueryName = "CD__USER",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Utente E.D.S.",;
    HelpContextID = 49018232,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=153, Top=199, InputMask=replicate('X',20)

  func oCD__USER_1_48.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CDMODALL<>'E' or g_AEDS<>'S' OR .w_HIDEDM)
    endwith
    endif
  endfunc

  add object oX1PASSWD_1_50 as StdField with uid="PXJXDPGZHU",rtseq=36,rtrep=.f.,;
    cFormVar = "w_X1PASSWD", cQueryName = "X1PASSWD",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Password utente E.D.S.",;
    HelpContextID = 44889018,;
   bGlobalFont=.t.,;
    Height=20, Width=162, Left=408, Top=200, InputMask=replicate('X',20)

  func oX1PASSWD_1_50.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CDMODALL<>'E' or g_AEDS<>'S' OR .w_HIDEDM)
    endwith
    endif
  endfunc

  add object oCDARCHIVE_1_55 as StdField with uid="GOMFFFNYVB",rtseq=37,rtrep=.f.,;
    cFormVar = "w_CDARCHIVE", cQueryName = "CDARCHIVE",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Archivio E.D.S.",;
    HelpContextID = 155379252,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=153, Top=224, InputMask=replicate('X',20)

  func oCDARCHIVE_1_55.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CDMODALL<>'E' or g_AEDS<>'S' OR .w_HIDEDM)
    endwith
    endif
  endfunc


  add object oObj_1_57 as cp_setobjprop with uid="AUWPJKIPCT",left=565, top=605, width=99,height=22,;
    caption='Pwd',;
   bGlobalFont=.t.,;
    cObj="w_X1PASSWD",cProp="passwordchar",;
    nPag=1;
    , HelpContextID = 96634634

  add object oCDFLCTRL_1_60 as StdCheck with uid="EPBYBMENAI",rtseq=38,rtrep=.f.,left=330, top=224, caption="Controlla esistenza dei file archiviati",;
    ToolTipText = "Se attivo, nella Ricerca documenti archiviati verr� eseguito il controllo di esistenza dei files associati alla presente classe documentale.",;
    HelpContextID = 45573490,;
    cFormVar="w_CDFLCTRL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCDFLCTRL_1_60.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CDFLCTRL,&i_cF..t_CDFLCTRL),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oCDFLCTRL_1_60.GetRadio()
    this.Parent.oContained.w_CDFLCTRL = this.RadioValue()
    return .t.
  endfunc

  func oCDFLCTRL_1_60.ToRadio()
    this.Parent.oContained.w_CDFLCTRL=trim(this.Parent.oContained.w_CDFLCTRL)
    return(;
      iif(this.Parent.oContained.w_CDFLCTRL=='S',1,;
      0))
  endfunc

  func oCDFLCTRL_1_60.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCDFLCTRL_1_60.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_HIDEDM OR .w_CDMODALL='S')
    endwith
    endif
  endfunc

  add object oCDFIRDIG_1_61 as StdCheck with uid="WVSDKFLAXN",rtseq=39,rtrep=.f.,left=154, top=246, caption="Firma digitale",;
    ToolTipText = "Flag abilitazione firma digitale",;
    HelpContextID = 207329939,;
    cFormVar="w_CDFIRDIG", bObbl = .f. , nPag = 1;
    ,sErrorMsg = "Non � consentito deselezionare il check firma digitale con attivo il check conservazione sostitutiva. Riattivarlo!";
   , bGlobalFont=.t.


  func oCDFIRDIG_1_61.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CDFIRDIG,&i_cF..t_CDFIRDIG),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oCDFIRDIG_1_61.GetRadio()
    this.Parent.oContained.w_CDFIRDIG = this.RadioValue()
    return .t.
  endfunc

  func oCDFIRDIG_1_61.ToRadio()
    this.Parent.oContained.w_CDFIRDIG=trim(this.Parent.oContained.w_CDFIRDIG)
    return(;
      iif(this.Parent.oContained.w_CDFIRDIG=='S',1,;
      0))
  endfunc

  func oCDFIRDIG_1_61.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCDFIRDIG_1_61.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ISALT or .w_HIDEDM or .w_CDMODALL $ 'ISE' )
    endwith
    endif
  endfunc

  func oCDFIRDIG_1_61.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_CDCONSOS='S' and .w_CDFIRDIG='S') or .w_CDCONSOS#'S')
    endwith
    return bRes
  endfunc

  add object oCDCONSOS_1_62 as StdCheck with uid="HXHNUEXTSO",rtseq=40,rtrep=.f.,left=270, top=246, caption="Conservazione sostitutiva",;
    ToolTipText = "Se attivo indica che la classe appartiene al SOS",;
    HelpContextID = 227920519,;
    cFormVar="w_CDCONSOS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCDCONSOS_1_62.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CDCONSOS,&i_cF..t_CDCONSOS),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oCDCONSOS_1_62.GetRadio()
    this.Parent.oContained.w_CDCONSOS = this.RadioValue()
    return .t.
  endfunc

  func oCDCONSOS_1_62.ToRadio()
    this.Parent.oContained.w_CDCONSOS=trim(this.Parent.oContained.w_CDCONSOS)
    return(;
      iif(this.Parent.oContained.w_CDCONSOS=='S',1,;
      0))
  endfunc

  func oCDCONSOS_1_62.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCDCONSOS_1_62.mCond()
    with this.Parent.oContained
      return (g_DOCM='S')
    endwith
  endfunc

  func oCDCONSOS_1_62.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ISALT or .w_HIDEDM or .w_CDMODALL $ 'IS')
    endwith
    endif
  endfunc

  add object oCDCLASOS_1_64 as StdField with uid="YGKYDITQXE",rtseq=41,rtrep=.f.,;
    cFormVar = "w_CDCLASOS", cQueryName = "CDCLASOS",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice della classe documentale SOS",;
    HelpContextID = 241748615,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=531, Top=247, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CLASMSOS", cZoomOnZoom="GSDM_KVS", oKey_1_1="SZCODCLA", oKey_1_2="this.w_CDCLASOS"

  func oCDCLASOS_1_64.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CDCONSOS#'S' or .w_HIDEDM)
    endwith
    endif
  endfunc

  func oCDCLASOS_1_64.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_64('Part',this)
    endwith
    return bRes
  endfunc

  proc oCDCLASOS_1_64.ecpDrop(oSource)
    this.Parent.oContained.link_1_64('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCDCLASOS_1_64.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLASMSOS','*','SZCODCLA',cp_AbsName(this.parent,'oCDCLASOS_1_64'),iif(empty(i_cWhere),.f.,i_cWhere),'GSDM_KVS',"Elenco classi SOS",'',this.parent.oContained
  endproc
  proc oCDCLASOS_1_64.mZoomOnZoom
    local i_obj
    i_obj=GSDM_KVS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_SZCODCLA=this.parent.oContained.w_CDCLASOS
    i_obj.ecpSave()
  endproc

  add object oCDAVVSOS_1_65 as StdField with uid="UALETBSAEH",rtseq=42,rtrep=.f.,;
    cFormVar = "w_CDAVVSOS", cQueryName = "CDAVVSOS",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorni validit� documento per invio a SOS",;
    HelpContextID = 219081351,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=715, Top=247, cSayPict="'999'", cGetPict="'999'"

  func oCDAVVSOS_1_65.mCond()
    with this.Parent.oContained
      return (.w_CDCONSOS='S')
    endwith
  endfunc

  func oCDAVVSOS_1_65.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CDCONSOS#'S' or .w_HIDEDM)
    endwith
    endif
  endfunc

  add object oCDALIASF_1_66 as StdField with uid="QZELYSPMJW",rtseq=43,rtrep=.f.,;
    cFormVar = "w_CDALIASF", cQueryName = "CDALIASF",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Stringa di caratteri da utilizzare nella costruzione del nome del file archiviato (solo in modalit� copia)",;
    HelpContextID = 1512812,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=154, Top=273, cSayPict="repl('!',15)", cGetPict="repl('!',15)", InputMask=replicate('X',15)

  func oCDALIASF_1_66.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (! .w_ISALT OR .w_HIDEDM)
    endwith
    endif
  endfunc


  add object oCDFLGDES_1_67 as StdCombo with uid="IXRNNUUUNQ",value=5,rtseq=44,rtrep=.f.,left=531,top=273,width=162,height=22;
    , ToolTipText = "Se attivo, nella costruzione del nome del file archiviato, si utilizza il criterio scelto (Solo in modalit� copia)";
    , HelpContextID = 49767801;
    , cFormVar="w_CDFLGDES",RowSource=""+"Descrizione pratica in nome file,"+"Des. pratica in nome file + cod. utente,"+"Codice pratica,"+"Codice pratica + cod. utente,"+"Nessuno,"+"Cod. utente", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCDFLGDES_1_67.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CDFLGDES,&i_cF..t_CDFLGDES),this.value)
    return(iif(xVal =1,'S',;
    iif(xVal =2,'U',;
    iif(xVal =3,'C',;
    iif(xVal =4,'T',;
    iif(xVal =5,' ',;
    iif(xVal =6,'K',;
    space(1))))))))
  endfunc
  func oCDFLGDES_1_67.GetRadio()
    this.Parent.oContained.w_CDFLGDES = this.RadioValue()
    return .t.
  endfunc

  func oCDFLGDES_1_67.ToRadio()
    this.Parent.oContained.w_CDFLGDES=trim(this.Parent.oContained.w_CDFLGDES)
    return(;
      iif(this.Parent.oContained.w_CDFLGDES=='S',1,;
      iif(this.Parent.oContained.w_CDFLGDES=='U',2,;
      iif(this.Parent.oContained.w_CDFLGDES=='C',3,;
      iif(this.Parent.oContained.w_CDFLGDES=='T',4,;
      iif(this.Parent.oContained.w_CDFLGDES=='',5,;
      iif(this.Parent.oContained.w_CDFLGDES=='K',6,;
      0)))))))
  endfunc

  func oCDFLGDES_1_67.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCDFLGDES_1_67.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CDRIFTAB # 'CAN_TIER'  or ! .w_ISALT OR .w_HIDEDM)
    endwith
    endif
  endfunc

  add object oCDPUBWEB_1_69 as StdCheck with uid="RWQSQMBOSF",rtseq=45,rtrep=.f.,left=283, top=299, caption="Pubblica su Web",;
    ToolTipText = "Se attivo nell'associazione manuale da file l'indice creato potr� essere pubblicato su web",;
    HelpContextID = 95487336,;
    cFormVar="w_CDPUBWEB", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCDPUBWEB_1_69.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CDPUBWEB,&i_cF..t_CDPUBWEB),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oCDPUBWEB_1_69.GetRadio()
    this.Parent.oContained.w_CDPUBWEB = this.RadioValue()
    return .t.
  endfunc

  func oCDPUBWEB_1_69.ToRadio()
    this.Parent.oContained.w_CDPUBWEB=trim(this.Parent.oContained.w_CDPUBWEB)
    return(;
      iif(this.Parent.oContained.w_CDPUBWEB=='S',1,;
      0))
  endfunc

  func oCDPUBWEB_1_69.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCDPUBWEB_1_69.mCond()
    with this.Parent.oContained
      return (!.w_CDMODALL$'IS')
    endwith
  endfunc

  func oCDPUBWEB_1_69.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_CPIN<>'S' AND g_DMIP<>'S' AND .w_CDMODALL<>'S')
    endwith
    endif
  endfunc

  add object oCDNOMFIL_1_70 as StdField with uid="WWRITYAJJQ",rtseq=46,rtrep=.f.,;
    cFormVar = "w_CDNOMFIL", cQueryName = "CDNOMFIL",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Espressione per la determinazione del nome file web (es.: dtos (mvdatdoc) +mvalfdoc+...) utilizzato da print system e da processo documentale",;
    HelpContextID = 178592398,;
   bGlobalFont=.t.,;
    Height=21, Width=460, Left=154, Top=323, InputMask=replicate('X',200)

  func oCDNOMFIL_1_70.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CDPUBWEB<>'S')
    endwith
    endif
  endfunc

  add object oCDERASEF_1_71 as StdCheck with uid="FVRXNBLLQH",rtseq=47,rtrep=.f.,left=619, top=322, caption="Cancella file",;
    ToolTipText = "Se attivo: cancella il file dopo la pubblicazione",;
    HelpContextID = 27088236,;
    cFormVar="w_CDERASEF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCDERASEF_1_71.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CDERASEF,&i_cF..t_CDERASEF),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oCDERASEF_1_71.GetRadio()
    this.Parent.oContained.w_CDERASEF = this.RadioValue()
    return .t.
  endfunc

  func oCDERASEF_1_71.ToRadio()
    this.Parent.oContained.w_CDERASEF=trim(this.Parent.oContained.w_CDERASEF)
    return(;
      iif(this.Parent.oContained.w_CDERASEF=='S',1,;
      0))
  endfunc

  func oCDERASEF_1_71.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCDERASEF_1_71.mCond()
    with this.Parent.oContained
      return (.w_CDMODALL<>'I')
    endwith
  endfunc

  func oCDERASEF_1_71.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_CPIN<>'S' or .w_HIDEDM OR .w_CDMODALL='S' OR .w_CDPUBWEB<>'S')
    endwith
    endif
  endfunc


  add object oObj_1_72 as cp_runprogram with uid="WCIZPCBCWB",left=-4, top=591, width=229,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSDM_BAT('X')",;
    cEvent = "w_CDCLASOS Changed",;
    nPag=1;
    , HelpContextID = 80921574


  add object oCDTIPOBC_1_73 as StdCombo with uid="RYSMHSWWDV",rtseq=48,rtrep=.f.,left=154,top=346,width=150,height=22;
    , ToolTipText = "Tipologia di report per rappresentazione codice esterno per acquisizione file da Infinity";
    , HelpContextID = 243615081;
    , cFormVar="w_CDTIPOBC",RowSource=""+"Etichetta barcode,"+"Etichetta QR code,"+"Frontespizio barcode,"+"Frontespizio QR code", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCDTIPOBC_1_73.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CDTIPOBC,&i_cF..t_CDTIPOBC),this.value)
    return(iif(xVal =1,'E',;
    iif(xVal =2,'Q',;
    iif(xVal =3,'F',;
    iif(xVal =4,'R',;
    space(1))))))
  endfunc
  func oCDTIPOBC_1_73.GetRadio()
    this.Parent.oContained.w_CDTIPOBC = this.RadioValue()
    return .t.
  endfunc

  func oCDTIPOBC_1_73.ToRadio()
    this.Parent.oContained.w_CDTIPOBC=trim(this.Parent.oContained.w_CDTIPOBC)
    return(;
      iif(this.Parent.oContained.w_CDTIPOBC=='E',1,;
      iif(this.Parent.oContained.w_CDTIPOBC=='Q',2,;
      iif(this.Parent.oContained.w_CDTIPOBC=='F',3,;
      iif(this.Parent.oContained.w_CDTIPOBC=='R',4,;
      0)))))
  endfunc

  func oCDTIPOBC_1_73.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCDTIPOBC_1_73.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not .w_CDMODALL $ 'IS')
    endwith
    endif
  endfunc

  add object oCDREPOBC_1_74 as StdField with uid="KKWGMXFTAK",rtseq=49,rtrep=.f.,;
    cFormVar = "w_CDREPOBC", cQueryName = "CDREPOBC",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    sErrorMsg = "Percorso file non valido oppure estensione diversa da report",;
    ToolTipText = "Report per la creazione del codice esterno (proposto in base al tipo selezionato)",;
    HelpContextID = 243344745,;
   bGlobalFont=.t.,;
    Height=21, Width=398, Left=310, Top=346, InputMask=replicate('X',254)

  func oCDREPOBC_1_74.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not .w_CDMODALL $ 'IS')
    endwith
    endif
  endfunc

  func oCDREPOBC_1_74.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_CDREPOBC) or JUSTEXT(.w_CDREPOBC)="FRX" and FILE(.w_CDREPOBC))
    endwith
    return bRes
  endfunc


  add object oBtn_1_75 as StdButton with uid="MXPTSVOWWH",left=712, top=349, width=19,height=18,;
    caption="...", nPag=1;
    , HelpContextID = 96875050;
  , bGlobalFont=.t.

    proc oBtn_1_75.Click()
      with this.Parent.oContained
        .NotifyEvent("getcdrepobc")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_75.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not .w_CDMODALL $ 'IS')
    endwith
   endif
  endfunc


  add object oBtn_1_86 as StdButton with uid="UHLKEQDFSQ",left=735, top=321, width=48,height=45,;
    CpPicture="caratter.ico", caption="", nPag=1;
    , ToolTipText = "Inserisce le chiavi della classe SOS di tipologia diversa da Libero";
    , HelpContextID = 96690054;
    , tabstop=.f., Caption='\<Ins. key';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_86.Click()
      with this.Parent.oContained
        GSDM_BAT(this.Parent.oContained,"S")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_86.mCond()
    with this.Parent.oContained
      return (.w_CDKEYUPD = 'N' and ! empty(.w_CDCLASOS))
    endwith
  endfunc

  func oBtn_1_86.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CDCONSOS#'S' or .w_HIDEDM)
    endwith
   endif
  endfunc

  add object oCDCLAPRA_1_95 as StdCheck with uid="QBWHNRJPDE",rtseq=56,rtrep=.f.,left=689, top=12, caption="Predefinita",;
    ToolTipText = "Se attivo, la classe � il riferimento per l'invia a",;
    HelpContextID = 244790631,;
    cFormVar="w_CDCLAPRA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCDCLAPRA_1_95.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CDCLAPRA,&i_cF..t_CDCLAPRA),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oCDCLAPRA_1_95.GetRadio()
    this.Parent.oContained.w_CDCLAPRA = this.RadioValue()
    return .t.
  endfunc

  func oCDCLAPRA_1_95.ToRadio()
    this.Parent.oContained.w_CDCLAPRA=trim(this.Parent.oContained.w_CDCLAPRA)
    return(;
      iif(this.Parent.oContained.w_CDCLAPRA=='S',1,;
      0))
  endfunc

  func oCDCLAPRA_1_95.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCDCLAPRA_1_95.mCond()
    with this.Parent.oContained
      return (INLIST(.w_CDMODALL,'F','L'))
    endwith
  endfunc

  func oCDCLAPRA_1_95.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ISALT or .w_HIDEDM)
    endwith
    endif
  endfunc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=5, top=372, width=778,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=10,Field1="CPROWORD",Label1="Seq.",Field2="CDCODATT",Label2="Attributo",Field3="CDDESATT",Label3="Descrizione",Field4="CDTIPATT",Label4="Tipo",Field5="CDCAMCUR",Label5="Nome campo o espressione",Field6="CDCHKOBB",Label6="Obbl",Field7="CDRIFTEM",Label7="F.D.",Field8="CDTABKEY",Label8="Tabella",Field9="CDATTSOS",Label9="iif(w_HIDEDM or w_CDMODALL$'IS','',Ah_MsgFormat('Attributo SOS'))",Field10="CDATTINF",Label10="iif(w_HIDEDM or w_CDMODALL$'IS',Ah_MsgFormat('Attributo Infinity'),'')",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 219014778

  add object oCDCLAINF_1_107 as StdField with uid="QHJGACKLFZ",rtseq=80,rtrep=.f.,;
    cFormVar = "w_CDCLAINF", cQueryName = "CDCLAINF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 141085332,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=154, Top=298, InputMask=replicate('X',15)

  func oCDCLAINF_1_107.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not .w_CDMODALL $ 'IS')
    endwith
    endif
  endfunc

  add object oCDOKSYNC_1_109 as StdCheck with uid="DSWZHQEBME",rtseq=81,rtrep=.f.,left=687, top=300, caption="Classe sincr.", enabled=.f.,;
    ToolTipText = "Classe sincronizzata con Infinity",;
    HelpContextID = 122227351,;
    cFormVar="w_CDOKSYNC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCDOKSYNC_1_109.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CDOKSYNC,&i_cF..t_CDOKSYNC),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oCDOKSYNC_1_109.GetRadio()
    this.Parent.oContained.w_CDOKSYNC = this.RadioValue()
    return .t.
  endfunc

  func oCDOKSYNC_1_109.ToRadio()
    this.Parent.oContained.w_CDOKSYNC=trim(this.Parent.oContained.w_CDOKSYNC)
    return(;
      iif(this.Parent.oContained.w_CDOKSYNC=='S',1,;
      0))
  endfunc

  func oCDOKSYNC_1_109.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCDOKSYNC_1_109.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_REVI<>'S')
    endwith
    endif
  endfunc


  add object oBtn_1_110 as StdButton with uid="JXJMDZCZCY",left=729, top=7, width=48,height=45,;
    CpPicture="bmp/fixinf.bmp", caption="", nPag=1,tabstop=.f.;
    , ToolTipText = "Apre la classe documentale su Infinity";
    , HelpContextID = 176590847;
    , Caption='\<Infinity';
  , bGlobalFont=.t.

    proc oBtn_1_110.Click()
      with this.Parent.oContained
        =InfinityLink("CLASSEDOCUMENTALE", LRTrim(.w_CDCLAINF))
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_110.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_REVI<>"S" or Empty(.w_CDCLAINF) or .w_CDOKSYNC<>'S')
    endwith
   endif
  endfunc

  add object oStr_1_27 as StdString with uid="UGJTJSLWQN",Visible=.t., Left=2, Top=12,;
    Alignment=1, Width=148, Height=18,;
    Caption="Classe documentale:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_28 as StdString with uid="IZFUYXUQFH",Visible=.t., Left=2, Top=202,;
    Alignment=1, Width=148, Height=18,;
    Caption="Percorso archiviazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_28.mHide()
    with this.Parent.oContained
      return (.w_CDMODALL<>'F' OR .w_HIDEDM)
    endwith
  endfunc

  add object oStr_1_29 as StdString with uid="EKQIMEYHBI",Visible=.t., Left=6, Top=94,;
    Alignment=1, Width=144, Height=18,;
    Caption="Tipo raggruppamento:"  ;
  , bGlobalFont=.t.

  func oStr_1_29.mHide()
    with this.Parent.oContained
      return (.w_CDMODALL<>'F')
    endwith
  endfunc

  add object oStr_1_30 as StdString with uid="TXXHVCLBNU",Visible=.t., Left=339, Top=94,;
    Alignment=1, Width=67, Height=18,;
    Caption="Su campo:"  ;
  , bGlobalFont=.t.

  func oStr_1_30.mHide()
    with this.Parent.oContained
      return (.w_CDMODALL<>'F')
    endwith
  endfunc

  add object oStr_1_31 as StdString with uid="QNKCHOEUBP",Visible=.t., Left=6, Top=67,;
    Alignment=1, Width=144, Height=18,;
    Caption="Archiviazione rapida:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="EBAMCHPEXS",Visible=.t., Left=6, Top=40,;
    Alignment=1, Width=144, Height=18,;
    Caption="Tipo archiviazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_32.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S')
    endwith
  endfunc

  add object oStr_1_33 as StdString with uid="UMYKNFSBCW",Visible=.t., Left=325, Top=67,;
    Alignment=1, Width=81, Height=18,;
    Caption="Tabella:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="VKFXTPEPKB",Visible=.t., Left=321, Top=40,;
    Alignment=1, Width=85, Height=18,;
    Caption="Modalit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="VGAGEJKARI",Visible=.t., Left=16, Top=124,;
    Alignment=1, Width=134, Height=18,;
    Caption="Tipologia allegato:"  ;
  , bGlobalFont=.t.

  func oStr_1_36.mHide()
    with this.Parent.oContained
      return (.w_HIDEDM)
    endwith
  endfunc

  add object oStr_1_38 as StdString with uid="TWBRHGVCYP",Visible=.t., Left=16, Top=150,;
    Alignment=1, Width=134, Height=18,;
    Caption="Classe allegato:"  ;
  , bGlobalFont=.t.

  func oStr_1_38.mHide()
    with this.Parent.oContained
      return (.w_HIDEDM)
    endwith
  endfunc

  add object oStr_1_51 as StdString with uid="TVWPGFDUJC",Visible=.t., Left=81, Top=176,;
    Alignment=1, Width=66, Height=18,;
    Caption="Server:"  ;
  , bGlobalFont=.t.

  func oStr_1_51.mHide()
    with this.Parent.oContained
      return (.w_CDMODALL<>'E' or g_AEDS<>'S' OR .w_HIDEDM)
    endwith
  endfunc

  add object oStr_1_52 as StdString with uid="NDTMPBVYKF",Visible=.t., Left=434, Top=176,;
    Alignment=1, Width=63, Height=18,;
    Caption="Porta:"  ;
  , bGlobalFont=.t.

  func oStr_1_52.mHide()
    with this.Parent.oContained
      return (.w_CDMODALL<>'E' or g_AEDS<>'S' OR .w_HIDEDM)
    endwith
  endfunc

  add object oStr_1_53 as StdString with uid="BYJVZYMXMF",Visible=.t., Left=97, Top=202,;
    Alignment=1, Width=53, Height=18,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  func oStr_1_53.mHide()
    with this.Parent.oContained
      return (.w_CDMODALL<>'E' or g_AEDS<>'S' OR .w_HIDEDM)
    endwith
  endfunc

  add object oStr_1_54 as StdString with uid="TWWILMOJAY",Visible=.t., Left=329, Top=201,;
    Alignment=1, Width=77, Height=18,;
    Caption="Password:"  ;
  , bGlobalFont=.t.

  func oStr_1_54.mHide()
    with this.Parent.oContained
      return (.w_CDMODALL<>'E' or g_AEDS<>'S' OR .w_HIDEDM)
    endwith
  endfunc

  add object oStr_1_56 as StdString with uid="LYKNCDUNMI",Visible=.t., Left=91, Top=226,;
    Alignment=1, Width=59, Height=18,;
    Caption="Archivio:"  ;
  , bGlobalFont=.t.

  func oStr_1_56.mHide()
    with this.Parent.oContained
      return (.w_CDMODALL<>'E' or g_AEDS<>'S' OR .w_HIDEDM)
    endwith
  endfunc

  add object oStr_1_63 as StdString with uid="EKBVUNTFNY",Visible=.t., Left=445, Top=249,;
    Alignment=1, Width=83, Height=18,;
    Caption="Classe SOS:"  ;
  , bGlobalFont=.t.

  func oStr_1_63.mHide()
    with this.Parent.oContained
      return (.w_CDCONSOS#'S' or .w_HIDEDM)
    endwith
  endfunc

  add object oStr_1_68 as StdString with uid="JAAXQJFUIZ",Visible=.t., Left=631, Top=249,;
    Alignment=1, Width=83, Height=18,;
    Caption="Giorni avviso:"  ;
  , bGlobalFont=.t.

  func oStr_1_68.mHide()
    with this.Parent.oContained
      return (.w_CDCONSOS#'S' or .w_HIDEDM)
    endwith
  endfunc

  add object oStr_1_79 as StdString with uid="DTQKIUTUIT",Visible=.t., Left=13, Top=350,;
    Alignment=1, Width=136, Height=18,;
    Caption="Report codice esterno:"  ;
  , bGlobalFont=.t.

  func oStr_1_79.mHide()
    with this.Parent.oContained
      return (not .w_CDMODALL $ 'IS')
    endwith
  endfunc

  add object oStr_1_80 as StdString with uid="FZPYFREPSP",Visible=.t., Left=596, Top=527,;
    Alignment=1, Width=69, Height=18,;
    Caption="Tipo SOS:"  ;
  , bGlobalFont=.t.

  func oStr_1_80.mHide()
    with this.Parent.oContained
      return (.w_ISALT OR .w_CDMODALL $ 'IS')
    endwith
  endfunc

  add object oStr_1_88 as StdString with uid="XHVFALXOJJ",Visible=.t., Left=14, Top=325,;
    Alignment=1, Width=135, Height=18,;
    Caption="Nome file web:"  ;
  , bGlobalFont=.t.

  func oStr_1_88.mHide()
    with this.Parent.oContained
      return (.w_CDPUBWEB<>'S')
    endwith
  endfunc

  add object oStr_1_93 as StdString with uid="YDERCRITIT",Visible=.t., Left=60, Top=275,;
    Alignment=1, Width=90, Height=18,;
    Caption="Alias tabella:"  ;
  , bGlobalFont=.t.

  func oStr_1_93.mHide()
    with this.Parent.oContained
      return (! .w_ISALT OR .w_HIDEDM)
    endwith
  endfunc

  add object oStr_1_99 as StdString with uid="XGBHFPTLZA",Visible=.t., Left=393, Top=275,;
    Alignment=1, Width=135, Height=18,;
    Caption="Inserisce nel nome file:"  ;
  , bGlobalFont=.t.

  func oStr_1_99.mHide()
    with this.Parent.oContained
      return (.w_CDRIFTAB # 'CAN_TIER'  or ! .w_ISALT)
    endwith
  endfunc

  add object oStr_1_100 as StdString with uid="BANILYKCCQ",Visible=.t., Left=1, Top=527,;
    Alignment=1, Width=106, Height=18,;
    Caption="Valore predefinito:"  ;
  , bGlobalFont=.t.

  add object oStr_1_108 as StdString with uid="GTIHSZPWSD",Visible=.t., Left=14, Top=302,;
    Alignment=1, Width=135, Height=18,;
    Caption="Codice classe Infinity:"  ;
  , bGlobalFont=.t.

  func oStr_1_108.mHide()
    with this.Parent.oContained
      return (not .w_CDMODALL $ 'IS')
    endwith
  endfunc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-5,top=389,;
    width=774+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*7*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-4,top=390,width=773+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*7*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='CLASDSOS|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oCDVLPRED_2_10.Refresh()
      this.Parent.oCDCREARA_2_11.Refresh()
      this.Parent.oCDFLGSOS_2_13.Refresh()
      this.Parent.oCDATTPRI_2_14.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='CLASDSOS'
        oDropInto=this.oBodyCol.oRow.oCDATTSOS_2_15
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oCDVLPRED_2_10 as StdTrsField with uid="ZSRCOYLJCQ",rtseq=66,rtrep=.t.,;
    cFormVar="w_CDVLPRED",value=space(50),;
    HelpContextID = 25716074,;
    cTotal="", bFixedPos=.t., cQueryName = "CDVLPRED",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Per gli attributi di tipo carattere, il valore predefinito deve essere tra apici o doppi apici",;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=112, Top=527, InputMask=replicate('X',50)

  func oCDVLPRED_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_CDVLPRED) or IIF(.w_CDTIPATT='C', IIF(LEFT(.w_CDVLPRED,1) <>"'" and LEFT(.w_CDVLPRED,1) <>'"', .F., .T.), .T.))
    endwith
    return bRes
  endfunc

  add object oCDCREARA_2_11 as StdTrsCheck with uid="FQBSGYSWIH",rtrep=.t.,;
    cFormVar="w_CDCREARA",  caption="Creazione contestuale anagrafiche",;
    ToolTipText = "se attivo, inserisce i dati necessari nel xml per creare le anagrafiche su infinity (attualmente gestito solo per tabella CONTI)",;
    HelpContextID = 266155367,;
    Left=305, Top=529,;
    cTotal="", cQueryName = "CDCREARA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oCDCREARA_2_11.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CDCREARA,&i_cF..t_CDCREARA),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oCDCREARA_2_11.GetRadio()
    this.Parent.oContained.w_CDCREARA = this.RadioValue()
    return .t.
  endfunc

  func oCDCREARA_2_11.ToRadio()
    this.Parent.oContained.w_CDCREARA=trim(this.Parent.oContained.w_CDCREARA)
    return(;
      iif(this.Parent.oContained.w_CDCREARA=='S',1,;
      0))
  endfunc

  func oCDCREARA_2_11.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCDCREARA_2_11.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_CDTABKEY) or not inlist(alltrim(.w_CDTABKEY), "CONTI") OR .w_CDMODALL<>'S')
    endwith
    endif
  endfunc

  add object oCDFLGSOS_2_13 as StdTrsCombo with uid="EVMRZDGMGW",rtrep=.t.,;
    cFormVar="w_CDFLGSOS", RowSource=""+"Non gestito,"+"Libero,"+"Hash,"+"Nome file,"+"Cod. spedizione,"+"Sistema" , enabled=.f.,;
    HelpContextID = 235444871,;
    Height=25, Width=97, Left=669, Top=528,;
    cTotal="", cQueryName = "CDFLGSOS",;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oCDFLGSOS_2_13.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CDFLGSOS,&i_cF..t_CDFLGSOS),this.value)
    return(iif(xVal =1,'NN',;
    iif(xVal =2,'LI',;
    iif(xVal =3,'HS',;
    iif(xVal =4,'NM',;
    iif(xVal =5,'CS',;
    iif(xVal =6,'SI',;
    space(2))))))))
  endfunc
  func oCDFLGSOS_2_13.GetRadio()
    this.Parent.oContained.w_CDFLGSOS = this.RadioValue()
    return .t.
  endfunc

  func oCDFLGSOS_2_13.ToRadio()
    this.Parent.oContained.w_CDFLGSOS=trim(this.Parent.oContained.w_CDFLGSOS)
    return(;
      iif(this.Parent.oContained.w_CDFLGSOS=='NN',1,;
      iif(this.Parent.oContained.w_CDFLGSOS=='LI',2,;
      iif(this.Parent.oContained.w_CDFLGSOS=='HS',3,;
      iif(this.Parent.oContained.w_CDFLGSOS=='NM',4,;
      iif(this.Parent.oContained.w_CDFLGSOS=='CS',5,;
      iif(this.Parent.oContained.w_CDFLGSOS=='SI',6,;
      0)))))))
  endfunc

  func oCDFLGSOS_2_13.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCDFLGSOS_2_13.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ISALT OR .w_CDMODALL $ 'IS')
    endwith
    endif
  endfunc

  add object oCDATTPRI_2_14 as StdTrsCheck with uid="MADESJLCYU",rtrep=.t.,;
    cFormVar="w_CDATTPRI",  caption="Attr. primario",;
    ToolTipText = "Se attivo, indica l'attributo primario",;
    HelpContextID = 265229679,;
    Left=669, Top=529,;
    cTotal="", cQueryName = "CDATTPRI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oCDATTPRI_2_14.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CDATTPRI,&i_cF..t_CDATTPRI),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oCDATTPRI_2_14.GetRadio()
    this.Parent.oContained.w_CDATTPRI = this.RadioValue()
    return .t.
  endfunc

  func oCDATTPRI_2_14.ToRadio()
    this.Parent.oContained.w_CDATTPRI=trim(this.Parent.oContained.w_CDATTPRI)
    return(;
      iif(this.Parent.oContained.w_CDATTPRI=='S',1,;
      0))
  endfunc

  func oCDATTPRI_2_14.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCDATTPRI_2_14.mCond()
    with this.Parent.oContained
      return (.w_CDCHKOBB = 'S')
    endwith
  endfunc

  func oCDATTPRI_2_14.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_ISALT)
    endwith
    endif
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

  define class tgsut_mcdPag2 as StdContainer
    Width  = 785
    height = 550
    stdWidth  = 785
    stdheight = 550
  resizeXpos=306
  resizeYpos=186
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_4_1 as stdDynamicChildContainer with uid="FDAZLVKLOB",left=7, top=8, width=718, height=420, bOnScreen=.t.;

  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsut_mpe",lower(this.oContained.GSUT_MPE.class))=0
        this.oContained.GSUT_MPE.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine

* --- Defining Body row
define class tgsut_mcdBodyRow as CPBodyRowCnt
  Width=764
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="MRRIHBXRRG",rtseq=58,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 251343210,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=39, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  add object oCDCODATT_2_2 as StdTrsField with uid="GTKMTSQINS",rtseq=59,rtrep=.t.,;
    cFormVar="w_CDCODATT",value=space(15),;
    HelpContextID = 264910202,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=91, Left=40, Top=0, cSayPict=[repl("!",15)], cGetPict=[repl("!",15)], InputMask=replicate('X',15)

  add object oCDDESATT_2_4 as StdTrsField with uid="IQXGXDIYKI",rtseq=60,rtrep=.t.,;
    cFormVar="w_CDDESATT",value=space(40),;
    HelpContextID = 11552122,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=102, Left=134, Top=0, InputMask=replicate('X',40)

  add object oCDTIPATT_2_5 as StdTrsCombo with uid="GDYMOBKLYF",rtrep=.t.,;
    cFormVar="w_CDTIPATT", RowSource=""+"Numerico,"+"Carattere,"+"Data" , ;
    HelpContextID = 8734074,;
    Height=22, Width=71, Left=240, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oCDTIPATT_2_5.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CDTIPATT,&i_cF..t_CDTIPATT),this.value)
    return(iif(xVal =1,'N',;
    iif(xVal =2,'C',;
    iif(xVal =3,'D',;
    space(1)))))
  endfunc
  func oCDTIPATT_2_5.GetRadio()
    this.Parent.oContained.w_CDTIPATT = this.RadioValue()
    return .t.
  endfunc

  func oCDTIPATT_2_5.ToRadio()
    this.Parent.oContained.w_CDTIPATT=trim(this.Parent.oContained.w_CDTIPATT)
    return(;
      iif(this.Parent.oContained.w_CDTIPATT=='N',1,;
      iif(this.Parent.oContained.w_CDTIPATT=='C',2,;
      iif(this.Parent.oContained.w_CDTIPATT=='D',3,;
      0))))
  endfunc

  func oCDTIPATT_2_5.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oCDCAMCUR_2_6 as StdTrsMemo with uid="GPCJGRMLMQ",rtseq=62,rtrep=.t.,;
    cFormVar="w_CDCAMCUR",value=space(0),;
    ToolTipText = "Campo cursore x identificare la chiave nella tabella di riferimento",;
    HelpContextID = 38548856,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=171, Left=313, Top=0

  add object oCDCHKOBB_2_7 as StdTrsCombo with uid="DPTGBKUMXI",rtrep=.t.,;
    cFormVar="w_CDCHKOBB", RowSource=""+"Si,"+"No" , ;
    HelpContextID = 238237032,;
    Height=22, Width=40, Left=488, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oCDCHKOBB_2_7.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CDCHKOBB,&i_cF..t_CDCHKOBB),this.value)
    return(iif(xVal =1,'S',;
    iif(xVal =2,'N',;
    'N')))
  endfunc
  func oCDCHKOBB_2_7.GetRadio()
    this.Parent.oContained.w_CDCHKOBB = this.RadioValue()
    return .t.
  endfunc

  func oCDCHKOBB_2_7.ToRadio()
    this.Parent.oContained.w_CDCHKOBB=trim(this.Parent.oContained.w_CDCHKOBB)
    return(;
      iif(this.Parent.oContained.w_CDCHKOBB=='S',1,;
      iif(this.Parent.oContained.w_CDCHKOBB=='N',2,;
      0)))
  endfunc

  func oCDCHKOBB_2_7.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oCDTABKEY_2_8 as StdTrsField with uid="NWDIHZZLIV",rtseq=64,rtrep=.t.,;
    cFormVar="w_CDTABKEY",value=space(15),;
    HelpContextID = 161301887,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=94, Left=563, Top=0, InputMask=replicate('X',15)

  add object oCDATTINF_2_9 as StdTrsField with uid="EWITWFQVDH",rtseq=65,rtrep=.t.,;
    cFormVar="w_CDATTINF",value=space(15),;
    ToolTipText = "Codice attributo classe Infinity D.M.S.",;
    HelpContextID = 120646292,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=97, Left=662, Top=0, InputMask=replicate('X',15), bHasZoom = .t. 

  func oCDATTINF_2_9.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not(.w_HIDEDM or .w_CDMODALL$'IS'))
    endwith
    endif
  endfunc

  proc oCDATTINF_2_9.mZoom
    vx_exec("CDATTINF.VZM",this.Parent.oContained)
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oCDATTSOS_2_15 as StdTrsField with uid="CDPIMOTMCX",rtseq=71,rtrep=.t.,;
    cFormVar="w_CDATTSOS",value=space(10),;
    ToolTipText = "Codice chiave classe SOS",;
    HelpContextID = 221309575,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=97, Left=662, Top=0, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CLASDSOS", oKey_1_1="SZCODCLA", oKey_1_2="this.w_CODCLASOS", oKey_2_1="SZCODKEY", oKey_2_2="this.w_CDATTSOS"

  func oCDATTSOS_2_15.mCond()
    with this.Parent.oContained
      return (.w_CDCONSOS='S')
    endwith
  endfunc

  func oCDATTSOS_2_15.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_HIDEDM or .w_CDMODALL$'IS')
    endwith
    endif
  endfunc

  func oCDATTSOS_2_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oCDATTSOS_2_15.ecpDrop(oSource)
    this.Parent.oContained.link_2_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCDATTSOS_2_15.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CLASDSOS_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SZCODCLA="+cp_ToStrODBC(this.Parent.oContained.w_CODCLASOS)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SZCODCLA="+cp_ToStr(this.Parent.oContained.w_CODCLASOS)
    endif
    do cp_zoom with 'CLASDSOS','*','SZCODCLA,SZCODKEY',cp_AbsName(this.parent,'oCDATTSOS_2_15'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco chiavi classe SOS",'GSUT_MCD.CLASDSOS_VZM',this.parent.oContained
  endproc

  add object oCDRIFTEM_2_18 as StdTrsCheck with uid="UGQVAGLKEB",rtrep=.t.,;
    cFormVar="w_CDRIFTEM",  caption="",;
    HelpContextID = 48571763,;
    Left=533, Top=0, Width=24,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , AutoSize=.F.;
   , bGlobalFont=.t.


  func oCDRIFTEM_2_18.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CDRIFTEM,&i_cF..t_CDRIFTEM),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oCDRIFTEM_2_18.GetRadio()
    this.Parent.oContained.w_CDRIFTEM = this.RadioValue()
    return .t.
  endfunc

  func oCDRIFTEM_2_18.ToRadio()
    this.Parent.oContained.w_CDRIFTEM=trim(this.Parent.oContained.w_CDRIFTEM)
    return(;
      iif(this.Parent.oContained.w_CDRIFTEM=='S',1,;
      0))
  endfunc

  func oCDRIFTEM_2_18.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCDRIFTEM_2_18.mCond()
    with this.Parent.oContained
      return (.w_CDFIRDIG='S' and .w_CDTIPATT='D')
    endwith
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=6
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_mcd','PROMCLAS','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CDCODCLA=PROMCLAS.CDCODCLA";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut_mcd
Procedure CONFERMA(obj,cparam)
  Local risposta
  If Empty(cparam)
    ah_msg("Paramentro non valido")
  Else
    If obj.cfunction='Edit' and obj.w_CDPATSTD<>obj.oldpath and ((obj.w_CDMODALL = 'F' and !Isalt()) or (Isalt() and obj.w_ATIPARC<>'F'))
      risposta = ah_yesno("La modifica del path standard comporta l'aggiornamento del path assoluto degli indici.%0Confermi l'operazione di aggiornamento?",48)
      If risposta
        If cparam="SAVE"
        * Record salvato, aggiorno l'indice
          Do GSUT_BKK With obj
        Endif
      Else
        If cparam="WARN"
        * ripristino i valori originari
          obj.w_CDPATSTD = obj.oldpath
          obj.w_CDMODALL = obj.oldmod
        Endif
      Endif
    Endif
  Endif
Endproc

Function ChkfileFRX(PathFile as char) as Boolean
LOCAL Chkfile as Boolean
    IF NOT empty(PathFile) 
        Chkfile = JUSTEXT(PathFile)="FRX" and FILE(PathFile)
        IF NOT Chkfile
            ah_errormsg("Percorso file non valido oppure estensione diversa da report")
        ENDIF
    ENDIF
RETURN Chkfile
ENDFUNC
* --- Fine Area Manuale
