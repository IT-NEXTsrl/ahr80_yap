* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscf_bim                                                        *
*              Invio messaggi da controllo flussi                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-02-08                                                      *
* Last revis.: 2012-06-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscf_bim",oParentObject)
return(i_retval)

define class tgscf_bim as StdBatch
  * --- Local variables
  w_OACODUTE = 0
  w_OASERIAL = space(10)
  w_OASTAMP = space(10)
  w_OA__MAIL = space(1)
  w_OAPOSTIN = space(1)
  w_MSG = space(10)
  w_TIPOPE = space(1)
  w_TLB = space(10)
  w_ROWSTAMP = space(10)
  w_RECVAL = space(250)
  w_RECNO = 0
  w_UTEMAIL = space(250)
  w_REBUSOBJ = space(20)
  w_TBLBTN = space(20)
  w_INDKEY = 0
  w_PKFLDS = space(254)
  w_PK = space(254)
  w_PKPOS = 0
  w_PKIDX = 0
  w_BOTTONE = space(250)
  w_OBJMAIL = space(250)
  w_REPSTBTN = space(1)
  w_REKEYSEL = space(60)
  w_CPROWNUM = 0
  w_OAOBJMSG = space(250)
  w_OATXTMSG = space(0)
  w_OAPIEMSG = space(0)
  w_MYLANGUAGE = space(3)
  w_UTELANGUAGE = space(3)
  w_FLDNAME = space(50)
  w_OACCADDR = space(0)
  w_RECCNADR = space(0)
  w_RECKEY = .f.
  w_IDXFLD = 0
  w_FLDTYPE = space(1)
  w_Ret = .f.
  w_CntDest = 0
  w_StrIndiri = space(10)
  w_TO = space(5)
  w_POS = 0
  * --- WorkFile variables
  LOGCNTUT_idx=0
  LOGCNTFL_idx=0
  OPAGGREG_idx=0
  REF_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Invio notifica da controllo flussi (da GSCF_KIM)
    if this.oParentObject.w_POSTIN="S" OR this.oParentObject.w_EMAIL="S"
      local array arrkey(5), arrval(5), arrbtn(5)
      this.w_MYLANGUAGE = i_cLanguage
      * --- Select from gscf_bim
      do vq_exec with 'gscf_bim',this,'_Curs_gscf_bim','',.f.,.t.
      if used('_Curs_gscf_bim')
        select _Curs_gscf_bim
        locate for 1=1
        do while not(eof())
        this.w_OACODUTE = _Curs_gscf_bim.OACODUTE
        this.w_OASERIAL = _Curs_gscf_bim.OASERIAL
        this.w_OASTAMP = _Curs_gscf_bim.OASTAMP
        this.w_OA__MAIL = _Curs_gscf_bim.OA__MAIL
        this.w_OAPOSTIN = _Curs_gscf_bim.OAPOSTIN
        this.w_REKEYSEL = _Curs_gscf_bim.REKEYSEL
        this.w_UTEMAIL = _Curs_gscf_bim.UTEMAIL
        this.w_UTELANGUAGE = nvl(_Curs_gscf_bim.language,"   ")
        if empty(this.w_UTELANGUAGE)
          i_cLanguage = this.w_MYLANGUAGE
        else
          * --- Cambio la variabile di sistema della lingua dell'utente
          i_cLanguage = this.w_UTELANGUAGE
          g_bNoTranslateDefault = "N"
        endif
        this.w_CPROWNUM = 0
        this.w_OAOBJMSG = ""
        this.w_OATXTMSG = ""
        this.w_OAPIEMSG = ""
        this.w_OACCADDR = ""
        * --- Read from OPAGGREG
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.OPAGGREG_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OPAGGREG_idx,2],.t.,this.OPAGGREG_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CPROWNUM,OAOBJMSG,OATXTMSG,OAPIEMSG,OACCADDR"+;
            " from "+i_cTable+" OPAGGREG where ";
                +"OASERIAL = "+cp_ToStrODBC(this.w_OASERIAL);
                +" and OACODUTE = "+cp_ToStrODBC(this.w_OACODUTE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CPROWNUM,OAOBJMSG,OATXTMSG,OAPIEMSG,OACCADDR;
            from (i_cTable) where;
                OASERIAL = this.w_OASERIAL;
                and OACODUTE = this.w_OACODUTE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CPROWNUM = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
          this.w_OAOBJMSG = NVL(cp_ToDate(_read_.OAOBJMSG),cp_NullValue(_read_.OAOBJMSG))
          this.w_OATXTMSG = NVL(cp_ToDate(_read_.OATXTMSG),cp_NullValue(_read_.OATXTMSG))
          this.w_OAPIEMSG = NVL(cp_ToDate(_read_.OAPIEMSG),cp_NullValue(_read_.OAPIEMSG))
          this.w_OACCADDR = NVL(cp_ToDate(_read_.OACCADDR),cp_NullValue(_read_.OACCADDR))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_CPROWNUM=0
          * --- Cerco per gruppo
          * --- Select from gscf_bim4
          do vq_exec with 'gscf_bim4',this,'_Curs_gscf_bim4','',.f.,.t.
          if used('_Curs_gscf_bim4')
            select _Curs_gscf_bim4
            locate for 1=1
            do while not(eof())
            this.w_OAOBJMSG = nvl(_Curs_gscf_bim4.OAOBJMSG,"")
            this.w_OATXTMSG = nvl(_Curs_gscf_bim4.OATXTMSG ,"")
            this.w_OAPIEMSG = nvl(_Curs_gscf_bim4.OAPIEMSG ,"")
            this.w_OACCADDR = nvl(_Curs_gscf_bim4.OACCADDR ,"")
              select _Curs_gscf_bim4
              continue
            enddo
            use
          endif
        endif
        this.w_REBUSOBJ = upper(alltrim(_Curs_gscf_bim.REBUSOBJ))
        this.w_REPSTBTN = nvl(_Curs_gscf_bim.REPSTBTN,"N")
        vq_exec("query\gscf_bim2", this, "mylog")
        Select mylog
        this.w_MSG = ah_msgformat("Regola: %1 <%2>%0", this.w_OASERIAL, alltrim(_Curs_gscf_bim.redescri))
        this.w_OBJMAIL = iif(empty(this.w_OAOBJMSG), ah_msgformat("Controllo Flussi  ") + this.w_MSG, this.w_OAOBJMSG)
        this.w_MSG = this.w_MSG + ah_msgformat("Utente: %1%0", alltrim(mylog.name))
        this.w_MSG = this.w_MSG + ah_msgformat("Azienda: %1 - %2", alltrim(i_CODAZI), g_RAGAZI)
        if not empty(g_CODBUN)
          this.w_MSG = this.w_MSG + ah_msgformat("%0Business unit: %1 - %2", alltrim(g_CODBUN), LookTab("BUSIUNIT", "BUDESCRI", "BUCODAZI", i_CODAZI, "BUCODICE", g_CODBUN))
        endif
        this.w_MSG = this.w_MSG + ah_msgformat("%0Data e ora: %1 - %2%0", alltrim(dtoc(mylog.LODATAOP)), mylog.LOTIMEOP)
        this.w_MSG = this.w_MSG + ah_msgformat("Tabella: %1%0", cp_translate(alltrim(iif(empty(mylog.tbcomment), mylog.re_table, mylog.tbcomment))))
        this.w_TIPOPE = "$"
        this.w_TLB = re_table
        this.w_ROWSTAMP = lostmrig
        this.w_TBLBTN = re_table
        this.w_INDKEY = 5
        do while this.w_INDKEY>0
          arrkey(this.w_INDKEY)=""
          arrval(this.w_INDKEY)=""
          arrbtn(this.w_INDKEY)=""
          this.w_INDKEY = this.w_INDKEY-1
        enddo
        * --- CREO IL MESSAGGIO
        scan
        this.w_RECVAL = alltrim(iif(empty(lonewval), lofldval, lonewval))
        if this.w_TLB<>re_table
          this.w_MSG = this.w_MSG + ah_msgformat("%0Tabella: %1%0", cp_translate(alltrim(iif(empty(tbcomment), re_table, tbcomment))))
          this.w_TLB = re_table
        endif
        if this.w_ROWSTAMP<>lostmrig
          this.w_MSG = this.w_MSG + ah_msgformat("%0")
          this.w_ROWSTAMP = lostmrig
        endif
        this.w_RECKEY = (lotipope="K" or lokeyrec="S" and lotipope<>"J" ) and this.w_TBLBTN=re_table
        if this.w_RECKEY
          this.w_INDKEY = this.w_INDKEY+1
          if this.w_INDKEY<=5
            arrkey(this.w_INDKEY) = alltrim(refldnam)
            arrval(this.w_INDKEY) = this.w_RECVAL
            this.w_IDXFLD = i_dcx.GETFieldidx(this.w_TBLBTN, alltrim(refldnam))
            if this.w_IDXFLD>0
              * --- Verifico il tipo del dato
              this.w_FLDTYPE = i_dcx.getfieldtype(this.w_TBLBTN, this.w_IDXFLD)
              do case
                case this.w_FLDTYPE="N"
                  arrval(this.w_INDKEY) = VAL(arrval(this.w_INDKEY))
                case this.w_FLDTYPE="T"
                  arrval(this.w_INDKEY) = CTOT(arrval(this.w_INDKEY))
                case this.w_FLDTYPE="D"
                  arrval(this.w_INDKEY) = CTOD(arrval(this.w_INDKEY))
                case this.w_FLDTYPE="C" AND EMPTY(arrval(this.w_INDKEY))
                  arrval(this.w_INDKEY) = "@<VUOTO>"
              endcase
            endif
          endif
        endif
        this.w_FLDNAME = cp_translate(alltrim(iif(empty(flcommen), refldnam, flcommen)))
        do case
          case lotipope="K"
            if this.oParentObject.w_SHOWKEY="S"
              this.w_MSG = this.w_MSG + ah_msgformat("Chiave di ricerca: %1  =  %2%0", this.w_FLDNAME, this.w_RECVAL)
            else
              * --- cerco l'identificativo
              this.w_RECNO = recno()
              locate for lostmrig=this.w_ROWSTAMP and lotipope="J"
              go this.w_RECNO
              if not found()
                this.w_MSG = this.w_MSG + ah_msgformat("Identificativo: %1  =  %2%0", this.w_FLDNAME, this.w_RECVAL)
              endif
            endif
          case lotipope="J"
            this.w_MSG = this.w_MSG + ah_msgformat("Identificativo: %1  =  %2%0", this.w_FLDNAME, this.w_RECVAL)
          case lotipope="D"
            if this.w_TIPOPE<>lotipope
              this.w_MSG = this.w_MSG + ah_msgformat("Dati eliminati:%0")
            endif
            this.w_MSG = this.w_MSG + ah_msgformat("Campo: %1 - Valore: %2%0", this.w_FLDNAME, alltrim(lofldval))
            if this.w_RECKEY
              * --- record eliminato, tolgo il bottone <visualizza>
              this.w_REPSTBTN = "N"
            endif
          case lotipope="I"
            if lokeyrec="S"
              this.w_MSG = this.w_MSG + ah_msgformat("Chiave di ricerca: %1  =  %2%0", this.w_FLDNAME, this.w_RECVAL)
            endif
            if this.w_TIPOPE<>lotipope
              this.w_MSG = this.w_MSG + ah_msgformat("Dati inseriti:%0")
            endif
            this.w_MSG = this.w_MSG + ah_msgformat("Campo: %1 - Valore: %2%0", this.w_FLDNAME, alltrim(lonewval))
          case lotipope="R"
            if this.w_TIPOPE<>lotipope
              this.w_MSG = this.w_MSG + ah_msgformat("Dati modificati:%0")
            endif
            this.w_MSG = this.w_MSG + ah_msgformat("Campo: %1 - Vecchio valore: %2 - Nuovo valore: %3%0", this.w_FLDNAME, alltrim(lofldval), alltrim(lonewval))
        endcase
        this.w_TIPOPE = lotipope
        endscan
        if not empty(this.w_OATXTMSG)
          this.w_MSG = ah_msgformat("%1%0", alltrim(this.w_OATXTMSG)) + this.w_MSG
        endif
        if not empty(this.w_OAPIEMSG)
          this.w_MSG = this.w_MSG + ah_msgformat("%0%1", alltrim(this.w_OAPIEMSG))
        endif
        if this.w_OAPOSTIN="S" and this.oParentObject.w_POSTIN="S"
          * --- INVIA POSTIN
          if this.w_INDKEY>0
            if EMPTY(NVL(this.w_REKEYSEL,""))
              this.w_PKFLDS = alltrim(i_dcx.getidxdef(this.w_TBLBTN,1))
            else
              this.w_PKFLDS = ALLTRIM(this.w_REKEYSEL)
            endif
            if "," $ this.w_PKFLDS
              * --- Metto i campi chiave in sequenza
              if this.w_INDKEY = OCCURS(",", this.w_PKFLDS)+1
                this.w_INDKEY = 0
                do while not empty(this.w_PKFLDS)
                  this.w_INDKEY = this.w_INDKEY+1
                  this.w_PKPOS = AT(",", this.w_PKFLDS)
                  if this.w_PKPOS=0
                    this.w_PK = this.w_PKFLDS
                    this.w_PKFLDS = ""
                  else
                    this.w_PK = LEFT(this.w_PKFLDS, this.w_PKPOS-1)
                    this.w_PKFLDS = SUBSTR(this.w_PKFLDS, this.w_PKPOS+1)
                  endif
                  this.w_PKIDX = ASCAN(arrkey, this.w_PK)
                  if this.w_PKIDX=0
                    this.w_PKFLDS = ""
                    this.w_INDKEY = 0
                  else
                    arrbtn(this.w_INDKEY) = arrval(this.w_PKIDX)
                  endif
                enddo
              else
                this.w_INDKEY = 0
              endif
            else
              * --- PK composta da un solo campo
              if this.w_INDKEY=1
                arrbtn(1) = arrval(1)
              else
                this.w_INDKEY = 0
              endif
            endif
          endif
          if EMPTY(this.w_REBUSOBJ) or this.w_REPSTBTN<>"S" or this.w_INDKEY=0
            GSUT_BIP(this,this.w_OACODUTE, this.w_MSG)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_BOTTONE = ""
          else
            if "," $ this.w_REBUSOBJ
              * --- gestione con paramentro
              this.w_REBUSOBJ = left(this.w_REBUSOBJ, at(",",this.w_REBUSOBJ)-1) + "('" + substr(this.w_REBUSOBJ, at(",",this.w_REBUSOBJ)+1)+ "')"
            endif
            this.w_BOTTONE = gsut_bmb(this.w_REBUSOBJ, "Visualizza", i_CODAZI, arrbtn(1), arrbtn(2), arrbtn(3), arrbtn(4), arrbtn(5))
            GSUT_BIP(this,this.w_OACODUTE, this.w_MSG,, this.w_BOTTONE)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          * --- Write into LOGCNTUT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.LOGCNTUT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.LOGCNTUT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.LOGCNTUT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"OAPOSTIN ="+cp_NullLink(cp_ToStrODBC("I"),'LOGCNTUT','OAPOSTIN');
                +i_ccchkf ;
            +" where ";
                +"OASERIAL = "+cp_ToStrODBC(this.w_OASERIAL);
                +" and OASTAMP = "+cp_ToStrODBC(this.w_OASTAMP);
                +" and OACODUTE = "+cp_ToStrODBC(this.w_OACODUTE);
                   )
          else
            update (i_cTable) set;
                OAPOSTIN = "I";
                &i_ccchkf. ;
             where;
                OASERIAL = this.w_OASERIAL;
                and OASTAMP = this.w_OASTAMP;
                and OACODUTE = this.w_OACODUTE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        if this.w_OA__MAIL="S" and this.oParentObject.w_EMAIL="S"
          if empty(nvl(this.w_UTEMAIL,""))
            * --- errore: Indirizzo e-mail mancante 
          else
            * --- Read from REF_MAST
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.REF_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.REF_MAST_idx,2],.t.,this.REF_MAST_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "RECCNADR"+;
                " from "+i_cTable+" REF_MAST where ";
                    +"RESERIAL = "+cp_ToStrODBC(this.w_OASERIAL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                RECCNADR;
                from (i_cTable) where;
                    RESERIAL = this.w_OASERIAL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_RECCNADR = NVL(cp_ToDate(_read_.RECCNADR),cp_NullValue(_read_.RECCNADR))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if empty(this.w_RECCNADR) and empty(this.w_OACCADDR)
              this.w_Ret = EMAIL( "" ,"N",,this.w_UTEMAIL, this.w_OBJMAIL, this.w_MSG)
            else
              PRIVATE ListInd
              Dimension ListInd(1,2)
              ListInd(1,1)=alltrim(this.w_UTEMAIL)
              ListInd(1,2)="A"
              this.w_CntDest = 2
              if not empty(this.w_OACCADDR)
                this.w_StrIndiri = this.w_OACCADDR
                this.w_TO = "CC"
                this.Pag2()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
              if not empty(this.w_RECCNADR)
                this.w_StrIndiri = this.w_RECCNADR
                this.w_TO = "CCN"
                this.Pag2()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
              this.w_Ret = EMAIL( "" ,"N",,@ListInd, this.w_OBJMAIL, this.w_MSG)
              release ListInd
            endif
            * --- Write into LOGCNTUT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.LOGCNTUT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.LOGCNTUT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.LOGCNTUT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"OA__MAIL ="+cp_NullLink(cp_ToStrODBC("I"),'LOGCNTUT','OA__MAIL');
                  +i_ccchkf ;
              +" where ";
                  +"OASERIAL = "+cp_ToStrODBC(this.w_OASERIAL);
                  +" and OASTAMP = "+cp_ToStrODBC(this.w_OASTAMP);
                  +" and OACODUTE = "+cp_ToStrODBC(this.w_OACODUTE);
                     )
            else
              update (i_cTable) set;
                  OA__MAIL = "I";
                  &i_ccchkf. ;
               where;
                  OASERIAL = this.w_OASERIAL;
                  and OASTAMP = this.w_OASTAMP;
                  and OACODUTE = this.w_OACODUTE;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        endif
          select _Curs_gscf_bim
          continue
        enddo
        use
      endif
      i_cLanguage = this.w_MYLANGUAGE
    endif
    * --- Select from GSCF_BIM3
    do vq_exec with 'GSCF_BIM3',this,'_Curs_GSCF_BIM3','',.f.,.t.
    if used('_Curs_GSCF_BIM3')
      select _Curs_GSCF_BIM3
      locate for 1=1
      do while not(eof())
      * --- Elimino dal log i record "scaduti"
      this.w_OASERIAL = _Curs_gscf_bim3.LOSERIAL
      this.w_OASTAMP = _Curs_gscf_bim3.LOSTAMP
      * --- Delete from LOGCNTUT
      i_nConn=i_TableProp[this.LOGCNTUT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.LOGCNTUT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"OASERIAL = "+cp_ToStrODBC(this.w_OASERIAL);
              +" and OASTAMP = "+cp_ToStrODBC(this.w_OASTAMP);
               )
      else
        delete from (i_cTable) where;
              OASERIAL = this.w_OASERIAL;
              and OASTAMP = this.w_OASTAMP;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Delete from LOGCNTFL
      i_nConn=i_TableProp[this.LOGCNTFL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.LOGCNTFL_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"LOSERIAL = "+cp_ToStrODBC(this.w_OASERIAL);
              +" and LOSTAMP = "+cp_ToStrODBC(this.w_OASTAMP);
               )
      else
        delete from (i_cTable) where;
              LOSERIAL = this.w_OASERIAL;
              and LOSTAMP = this.w_OASTAMP;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
        select _Curs_GSCF_BIM3
        continue
      enddo
      use
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- riporto i destinatari in un array
    this.w_StrIndiri = ALLTRIM(this.w_StrIndiri)
    this.w_POS = AT(";",this.w_StrIndiri)
    do while this.w_POS>0 OR LEN(this.w_StrIndiri)>0
      * --- Procediamo a riempire l'array
      Dimension ListInd(this.w_CntDest,2)
      ListInd(this.w_CntDest,2)=this.w_TO
      if this.w_POS>0
        * --- Nella stringa analizzata c'� il carattere '; '
        ListInd(this.w_CntDest,1)= SUBSTR(this.w_StrIndiri,1,this.w_POS-1)
        this.w_StrIndiri = SUBSTR(this.w_StrIndiri,this.w_POS+1)
      else
        * --- Nessun ';' presente
        ListInd(this.w_CntDest,1)=this.w_StrIndiri
        this.w_StrIndiri = ""
      endif
      this.w_POS = AT(";",this.w_StrIndiri)
      this.w_CntDest = this.w_CntDest + 1
    enddo
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='LOGCNTUT'
    this.cWorkTables[2]='LOGCNTFL'
    this.cWorkTables[3]='OPAGGREG'
    this.cWorkTables[4]='REF_MAST'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_gscf_bim')
      use in _Curs_gscf_bim
    endif
    if used('_Curs_gscf_bim4')
      use in _Curs_gscf_bim4
    endif
    if used('_Curs_GSCF_BIM3')
      use in _Curs_GSCF_BIM3
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
