* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscp_bsc                                                        *
*              Stampe di controllo per CPZ                                     *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-12-15                                                      *
* Last revis.: 2005-12-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscp_bsc",oParentObject,m.pAzione)
return(i_retval)

define class tgscp_bsc as StdBatch
  * --- Local variables
  w_CTRLATT = .NULL.
  w_INDPRE = 0
  pAzione = space(0)
  w_CTRL = .NULL.
  w_CTRLSEL = space(250)
  w_ACTION = space(3)
  w_INDICE = 0
  w_POSVET = 0
  w_LISTA = space(30)
  w_TXTVER = space(254)
  * --- WorkFile variables
  TMPDOCUM_idx=0
  AZIENDA_idx=0
  TMPRIPA1_idx=0
  TMPRIPA2_idx=0
  DOC_DETT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- pAzione:     E = Elabora stampa
    *                         S = Seleziona/ Deseleziona flag
    *                         I   = Inizializzo controlli maschera
    do case
      case this.pAzione="E"
        * --- Creo la tabella temporanea vuota
        * --- Create temporary table TMPDOCUM
        i_nIdx=cp_AddTableDef('TMPDOCUM') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('gscp_bsc',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPDOCUM_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- Create temporary table TMPRIPA1
        i_nIdx=cp_AddTableDef('TMPRIPA1') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        declare indexes_TPMZQEXEKB[1]
        indexes_TPMZQEXEKB[1]='LICODLIS'
        vq_exec('gscplbsc_ex2',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_TPMZQEXEKB,.f.)
        this.TMPRIPA1_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- Create temporary table TMPRIPA2
        i_nIdx=cp_AddTableDef('TMPRIPA2') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        declare indexes_JRMYIFWGIV[1]
        indexes_JRMYIFWGIV[1]='CONUMERO'
        vq_exec('gscplbsc_ex3',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_JRMYIFWGIV,.f.)
        this.TMPRIPA2_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        if this.oParentObject.w_FLCHKAGE="S"
          this.w_ACTION = "ON"
          this.w_CTRLSEL = "w_FLCHKAGE"
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          ah_msg("Verifica agenti...",.t.,.t.)
          * --- Insert into TMPDOCUM
          i_nConn=i_TableProp[this.TMPDOCUM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPDOCUM_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gscp1age",this.TMPDOCUM_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          this.w_ACTION = "OFF"
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if this.oParentObject.w_FLCHKART="S"
          this.w_ACTION = "ON"
          this.w_CTRLSEL = "w_FLCHKART"
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          ah_msg("Verifica articoli...",.t.,.t.)
          if UPPER(g_APPLICATION)="AD HOC ENTERPRISE"
            * --- Insert into TMPDOCUM
            i_nConn=i_TableProp[this.TMPDOCUM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPDOCUM_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gscpabsc",this.TMPDOCUM_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
          * --- Insert into TMPDOCUM
          i_nConn=i_TableProp[this.TMPDOCUM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPDOCUM_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gscpabsc1",this.TMPDOCUM_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          * --- Insert into TMPDOCUM
          i_nConn=i_TableProp[this.TMPDOCUM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPDOCUM_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gscpabsc2",this.TMPDOCUM_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          this.w_ACTION = "OFF"
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if this.oParentObject.w_FLCHKLIS="S"
          this.w_ACTION = "ON"
          this.w_CTRLSEL = "w_FLCHKLIS"
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          ah_msg("Verifica listini e contratti...",.t.,.t.)
          * --- Insert into TMPDOCUM
          i_nConn=i_TableProp[this.TMPDOCUM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPDOCUM_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gscplbsc",this.TMPDOCUM_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          * --- Insert into TMPDOCUM
          i_nConn=i_TableProp[this.TMPDOCUM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPDOCUM_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gscplbsc1",this.TMPDOCUM_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          * --- Insert into TMPDOCUM
          i_nConn=i_TableProp[this.TMPDOCUM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPDOCUM_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gscplbsc2",this.TMPDOCUM_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          this.w_ACTION = "OFF"
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if this.oParentObject.w_FLCHKORD="S"
          this.w_ACTION = "ON"
          this.w_CTRLSEL = "w_FLCHKORD"
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          ah_msg("Verifica ordini...",.t.,.t.)
          * --- Insert into TMPDOCUM
          i_nConn=i_TableProp[this.TMPDOCUM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPDOCUM_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gscp_ord",this.TMPDOCUM_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          * --- Insert into TMPDOCUM
          i_nConn=i_TableProp[this.TMPDOCUM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPDOCUM_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gscp1ord",this.TMPDOCUM_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          * --- Insert into TMPDOCUM
          i_nConn=i_TableProp[this.TMPDOCUM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPDOCUM_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gscp2ord",this.TMPDOCUM_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          * --- Insert into TMPDOCUM
          i_nConn=i_TableProp[this.TMPDOCUM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPDOCUM_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gscp3ord",this.TMPDOCUM_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          if UPPER(g_APPLICATION)="AD HOC ENTERPRISE"
            * --- Insert into TMPDOCUM
            i_nConn=i_TableProp[this.TMPDOCUM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPDOCUM_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gscp4ord",this.TMPDOCUM_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
          * --- Insert into TMPDOCUM
          i_nConn=i_TableProp[this.TMPDOCUM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPDOCUM_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gscp5ord",this.TMPDOCUM_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          this.w_ACTION = "OFF"
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if this.oParentObject.w_FLCHKDOC="S"
          this.w_ACTION = "ON"
          this.w_CTRLSEL = "w_FLCHKDOC"
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          ah_msg("Verifica documenti...",.t.,.t.)
          * --- Insert into TMPDOCUM
          i_nConn=i_TableProp[this.TMPDOCUM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPDOCUM_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gscp1doc",this.TMPDOCUM_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          * --- Insert into TMPDOCUM
          i_nConn=i_TableProp[this.TMPDOCUM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPDOCUM_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gscp2doc",this.TMPDOCUM_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          this.w_ACTION = "OFF"
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if this.oParentObject.w_FLCHKPRO="S"
          this.w_ACTION = "ON"
          this.w_CTRLSEL = "w_FLCHKPRO"
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          ah_msg("Verifica provvigioni...",.t.,.t.)
          * --- Insert into TMPDOCUM
          i_nConn=i_TableProp[this.TMPDOCUM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPDOCUM_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gscp_pro",this.TMPDOCUM_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          * --- Insert into TMPDOCUM
          i_nConn=i_TableProp[this.TMPDOCUM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPDOCUM_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gscp3pro",this.TMPDOCUM_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          this.w_ACTION = "OFF"
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- Lancio la stampa dei dati estratti ===============================================================================================
        ah_msg("Estrazione e ordinamento dati...",.t.,.t.)
        vq_exec("QUERY\GSCP_SSC.VQR", this, "__TMP__")
        WAIT CLEAR
        CP_CHPRN("QUERY\GSCP_SSC")
        * --- Elimino la tabella temporanea non pi� necessaria
        * --- Drop temporary table TMPDOCUM
        i_nIdx=cp_GetTableDefIdx('TMPDOCUM')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPDOCUM')
        endif
        * --- Drop temporary table TMPRIPA1
        i_nIdx=cp_GetTableDefIdx('TMPRIPA1')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPRIPA1')
        endif
        * --- Drop temporary table TMPRIPA2
        i_nIdx=cp_GetTableDefIdx('TMPRIPA2')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPRIPA2')
        endif
      case this.pAzione="S"
        if this.oParentObject.w_FLSELDES="S"
          this.oParentObject.w_FLCHKAGE = "S"
          this.oParentObject.w_FLCHKART = "S"
          this.oParentObject.w_FLCHKDOC = "S"
          this.oParentObject.w_FLCHKLIS = "S"
          this.oParentObject.w_FLCHKORD = "S"
          this.oParentObject.w_FLCHKPRO = "S"
        else
          this.oParentObject.w_FLCHKAGE = "N"
          this.oParentObject.w_FLCHKART = "N"
          this.oParentObject.w_FLCHKDOC = "N"
          this.oParentObject.w_FLCHKLIS = "N"
          this.oParentObject.w_FLCHKORD = "N"
          this.oParentObject.w_FLCHKPRO = "N"
        endif
      case this.pAzione="I"
        this.oParentObject.w_VERIFICHE = ""
        this.w_INDPRE = -1
        this.w_INDICE = 1
        * --- Verifiche per agenti
        this.w_TXTVER = "Agenti"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_TXTVER = "Per agenti con riferimento fornitore non pubblicato"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Verifiche per Articoli
        this.w_INDICE = this.w_INDICE+1
        this.w_TXTVER = "Articoli"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if g_APPLICATION="AHE"
          this.w_TXTVER = "Articoli gestiti a varianti"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        this.w_TXTVER = "Articolo descrittivo non pubblicato"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_TXTVER = "Articolo privo di listini e contratti associati"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- verifiche per listini e contratti
        this.w_INDICE = this.w_INDICE+1
        this.w_TXTVER = "Listini e contratti"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_TXTVER = "Listini o contratti riferiti esclusivamente ad articoli non pubblicati"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_TXTVER = "Contratti riferiti a clienti non pubblicati"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- verifiche per ordini
        this.w_INDICE = this.w_INDICE+1
        this.w_TXTVER = "Ordini"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_TXTVER = "Intestati a clienti non pubblicati"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_TXTVER = "Associati ad agenti non pubblicati"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_TXTVER = "Con listini o contratti con pubblicati"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_TXTVER = "Con magazzini o magazzini collegati non pubblicati"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_TXTVER = "Con articoli non pubblicati"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Verifiche per documenti
        this.w_INDICE = this.w_INDICE+1
        this.w_TXTVER = "Documenti"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_TXTVER = "Intestati a clienti o fornitori non pubblicati"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_TXTVER = "Riferiti ad agenti non pubblicati"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Verifiche per provvigioni
        this.w_INDICE = this.w_INDICE+1
        this.w_TXTVER = "Provvigioni"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_TXTVER = "Legate a documenti non pubblicati"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_TXTVER = "Legate ad agenti non pubblicati"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Imposto le propriet� della casella di testo
        this.w_CTRL = This.oParentObject.GETCTRL("w_VERIFICHE")
        this.w_CTRL.borderstyle = IIF(UPPER(g_APPLICATION)="ADHOC REVOLUTION",1,0)
        this.w_CTRL = .NULL.
        * --- Visualizzo le modifiche apportate
        This.oParentObject.SetControlsValue()
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_CTRLATT = this.oparentobject.getctrl(ALLTRIM(this.w_CTRLSEL))
    if this.w_ACTION="ON"
      this.w_CTRLATT.backcolor = RGB(255,255,0)
      this.w_CTRLATT.backstyle = 1
    else
      this.w_CTRLATT.backstyle = 0
    endif
    DOEVENT
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_LISTA = "ABCDEFGHILMNOPQRSTUVZ"
    if this.w_INDPRE<>this.w_INDICE
      this.w_POSVET = 0
      this.w_INDPRE = this.w_INDICE
      this.oParentObject.w_VERIFICHE = this.oParentObject.w_VERIFICHE+CHR(9)+ALLTRIM(STR(this.w_INDICE))+") "+ah_msgformat(ALLTRIM(this.w_TXTVER))+CHR(13)
    else
      this.w_POSVET = this.w_POSVET+1
      this.oParentObject.w_VERIFICHE = this.oParentObject.w_VERIFICHE+CHR(9)+CHR(9)+SUBSTR(this.w_LISTA,this.w_POSVET,1)+") "+ah_msgformat(ALLTRIM(this.w_TXTVER))+CHR(13)
    endif
  endproc


  proc Init(oParentObject,pAzione)
    this.pAzione=pAzione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='*TMPDOCUM'
    this.cWorkTables[2]='AZIENDA'
    this.cWorkTables[3]='*TMPRIPA1'
    this.cWorkTables[4]='*TMPRIPA2'
    this.cWorkTables[5]='DOC_DETT'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione"
endproc
