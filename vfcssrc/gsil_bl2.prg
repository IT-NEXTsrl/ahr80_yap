* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsil_bl2                                                        *
*              Legge categorie listini                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-12-05                                                      *
* Last revis.: 2000-12-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsil_bl2",oParentObject)
return(i_retval)

define class tgsil_bl2 as StdBatch
  * --- Local variables
  w_CNT = 0
  * --- WorkFile variables
  CATELIST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Legge Categoria del listino e carica i Listini da Importare (da GSIL_KIL)
    this.oParentObject.w_FLLIS1 = " "
    this.oParentObject.w_FLIVA1 = " "
    this.oParentObject.w_FLVAL1 = "   "
    this.oParentObject.w_CODLIS1 = SPACE(5)
    this.oParentObject.w_STRLIS1 = SPACE(5)
    this.oParentObject.w_FLLIS2 = " "
    this.oParentObject.w_FLIVA2 = " "
    this.oParentObject.w_FLVAL2 = "   "
    this.oParentObject.w_CODLIS2 = SPACE(5)
    this.oParentObject.w_STRLIS2 = SPACE(5)
    this.oParentObject.w_FLLIS3 = " "
    this.oParentObject.w_FLIVA3 = " "
    this.oParentObject.w_FLVAL3 = "   "
    this.oParentObject.w_CODLIS3 = SPACE(5)
    this.oParentObject.w_STRLIS3 = SPACE(5)
    this.oParentObject.w_FLLIS4 = " "
    this.oParentObject.w_FLIVA4 = " "
    this.oParentObject.w_FLVAL4 = "   "
    this.oParentObject.w_CODLIS4 = SPACE(5)
    this.oParentObject.w_STRLIS4 = SPACE(5)
    this.oParentObject.w_FLLIS5 = " "
    this.oParentObject.w_FLIVA5 = " "
    this.oParentObject.w_FLVAL5 = "   "
    this.oParentObject.w_CODLIS5 = SPACE(5)
    this.oParentObject.w_STRLIS5 = SPACE(5)
    this.w_CNT = 0
    this.oParentObject.w_DESLIS1 = SPACE(40)
    this.oParentObject.w_DESLIS2 = SPACE(40)
    this.oParentObject.w_DESLIS3 = SPACE(40)
    this.oParentObject.w_DESLIS4 = SPACE(40)
    this.oParentObject.w_DESLIS5 = SPACE(40)
    if NOT EMPTY(this.oParentObject.w_CATLIS)
      * --- Select from CATELIST
      i_nConn=i_TableProp[this.CATELIST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CATELIST_idx,2],.t.,this.CATELIST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" CATELIST ";
            +" where CTCODCAT="+cp_ToStrODBC(this.oParentObject.w_CATLIS)+"";
            +" order by CTPOSINI";
             ,"_Curs_CATELIST")
      else
        select * from (i_cTable);
         where CTCODCAT=this.oParentObject.w_CATLIS;
         order by CTPOSINI;
          into cursor _Curs_CATELIST
      endif
      if used('_Curs_CATELIST')
        select _Curs_CATELIST
        locate for 1=1
        do while not(eof())
        if NVL(_Curs_CATELIST.CTFLLIST, " ")="S" 
          this.w_CNT = this.w_CNT + 1
          if this.w_CNT<6
            KK = ALLTRIM(STR(this.w_CNT))
            this.oParentObject.w_FLLIS&KK = "S"
            this.oParentObject.w_FLIVA&KK = NVL(_Curs_CATELIST.CTFLGIVA, "N")
            this.oParentObject.w_FLVAL&KK = NVL(_Curs_CATELIST.CTCODISO, "   ")
            this.oParentObject.w_STRLIS&KK = ALLTRIM(NVL(_Curs_CATELIST.CTDESLIS,""))
          endif
        endif
          select _Curs_CATELIST
          continue
        enddo
        use
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CATELIST'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_CATELIST')
      use in _Curs_CATELIST
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
