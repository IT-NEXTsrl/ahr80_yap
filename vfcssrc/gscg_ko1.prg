* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_ko1                                                        *
*              Operazioni superiori a 3000 euro                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-07-25                                                      *
* Last revis.: 2011-08-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_ko1",oParentObject))

* --- Class definition
define class tgscg_ko1 as StdForm
  Top    = 2
  Left   = 3

  * --- Standard Properties
  Width  = 700
  Height = 441
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-08-22"
  HelpContextID=166339945
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=13

  * --- Constant Properties
  _IDX = 0
  PNT_MAST_IDX = 0
  CONTI_IDX = 0
  cPrg = "gscg_ko1"
  cComment = "Operazioni superiori a 3000 euro"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_PNSERIAL = space(10)
  o_PNSERIAL = space(10)
  w_PNSERIAL1 = space(10)
  w_PNTIPDOC = space(2)
  w_PNTIPREG = space(1)
  w_PNCODVAL = space(3)
  w_PNDATDOC = ctod('  /  /  ')
  w_PNDATREG = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_PNTIPCLF = space(1)
  w_PNCODCLF = space(15)
  w_ANOPETRE = space(1)
  w_ANFLSOAL = space(1)
  w_DESCLF = space(40)

  * --- Children pointers
  GSCG_AOS = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    *set procedure to GSCG_AOS additive
    with this
      .Pages(1).addobject("oPag","tgscg_ko1Pag1","gscg_ko1",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    *release procedure GSCG_AOS
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='PNT_MAST'
    this.cWorkTables[2]='CONTI'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return

  function CreateChildren()
    this.GSCG_AOS = CREATEOBJECT('stdDynamicChild',this,'GSCG_AOS',this.oPgFrm.Page1.oPag.oLinkPC_1_14)
    this.GSCG_AOS.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSCG_AOS)
      this.GSCG_AOS.DestroyChildrenChain()
      this.GSCG_AOS=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_14')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSCG_AOS.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSCG_AOS.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSCG_AOS.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSCG_AOS.SetKey(;
            .w_PNSERIAL,"OSSERIAL";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSCG_AOS.ChangeRow(this.cRowID+'      1',1;
             ,.w_PNSERIAL,"OSSERIAL";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSCG_AOS)
        i_f=.GSCG_AOS.BuildFilter()
        if !(i_f==.GSCG_AOS.cQueryFilter)
          i_fnidx=.GSCG_AOS.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCG_AOS.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCG_AOS.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCG_AOS.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCG_AOS.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.Save_GSCG_AOS(.f.)
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    if  this.GSCG_AOS.IsAChildUpdated()
      return cp_YesNo(MSG_DISCARD_CHANGES_QP)
    endif
    return .t.
  endfunc
  proc Save_GSCG_AOS(i_ask)
    if this.GSCG_AOS.IsAChildUpdated() and (!i_ask or cp_YesNo(MSG_SAVE_CHANGES_QP+' (LinkPC)'))
      cp_BeginTrs()
      this.GSCG_AOS.mReplace(.t.)
      cp_EndTrs()
    endif
  endproc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PNSERIAL=space(10)
      .w_PNSERIAL1=space(10)
      .w_PNTIPDOC=space(2)
      .w_PNTIPREG=space(1)
      .w_PNCODVAL=space(3)
      .w_PNDATDOC=ctod("  /  /  ")
      .w_PNDATREG=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_PNTIPCLF=space(1)
      .w_PNCODCLF=space(15)
      .w_ANOPETRE=space(1)
      .w_ANFLSOAL=space(1)
      .w_DESCLF=space(40)
      .w_PNSERIAL=oParentObject.w_PNSERIAL
      .w_PNSERIAL1=oParentObject.w_PNSERIAL1
        .DoRTCalc(1,2,.f.)
        if not(empty(.w_PNSERIAL1))
          .link_1_2('Full')
        endif
          .DoRTCalc(3,7,.f.)
        .w_OBTEST = iif(empty(.w_PNDATDOC), .w_PNDATREG, .w_PNDATDOC)
        .DoRTCalc(9,10,.f.)
        if not(empty(.w_PNCODCLF))
          .link_1_10('Full')
        endif
      .GSCG_AOS.NewDocument()
      .GSCG_AOS.ChangeRow('1',1,.w_PNSERIAL,"OSSERIAL")
      if not(.GSCG_AOS.bLoaded)
        .GSCG_AOS.SetKey(.w_PNSERIAL,"OSSERIAL")
      endif
    endwith
    this.DoRTCalc(11,13,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    this.GSCG_AOS.SetStatus(i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSCG_AOS.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_PNSERIAL=.w_PNSERIAL
      .oParentObject.w_PNSERIAL1=.w_PNSERIAL1
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
          .link_1_2('Full')
        .DoRTCalc(3,7,.t.)
            .w_OBTEST = iif(empty(.w_PNDATDOC), .w_PNDATREG, .w_PNDATDOC)
        .DoRTCalc(9,9,.t.)
          .link_1_10('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .w_PNSERIAL<>.o_PNSERIAL
          .Save_GSCG_AOS(.t.)
          .GSCG_AOS.NewDocument()
          .GSCG_AOS.ChangeRow('1',1,.w_PNSERIAL,"OSSERIAL")
          if not(.GSCG_AOS.bLoaded)
            .GSCG_AOS.SetKey(.w_PNSERIAL,"OSSERIAL")
          endif
        endif
      endwith
      this.DoRTCalc(11,13,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- gscg_ko1
    if CEVENT ="Edit Aborted"
    THIS.OPARENTOBJECT.w_SALVATO=.F.
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PNSERIAL1
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PNT_MAST_IDX,3]
    i_lTable = "PNT_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PNT_MAST_IDX,2], .t., this.PNT_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PNT_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PNSERIAL1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PNSERIAL1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PNSERIAL,PNTIPDOC,PNTIPREG,PNCODVAL,PNDATDOC,PNDATREG,PNTIPCLF,PNCODCLF";
                   +" from "+i_cTable+" "+i_lTable+" where PNSERIAL="+cp_ToStrODBC(this.w_PNSERIAL1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PNSERIAL',this.w_PNSERIAL1)
            select PNSERIAL,PNTIPDOC,PNTIPREG,PNCODVAL,PNDATDOC,PNDATREG,PNTIPCLF,PNCODCLF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PNSERIAL1 = NVL(_Link_.PNSERIAL,space(10))
      this.w_PNTIPDOC = NVL(_Link_.PNTIPDOC,space(2))
      this.w_PNTIPREG = NVL(_Link_.PNTIPREG,space(1))
      this.w_PNCODVAL = NVL(_Link_.PNCODVAL,space(3))
      this.w_PNDATDOC = NVL(cp_ToDate(_Link_.PNDATDOC),ctod("  /  /  "))
      this.w_PNDATREG = NVL(cp_ToDate(_Link_.PNDATREG),ctod("  /  /  "))
      this.w_PNTIPCLF = NVL(_Link_.PNTIPCLF,space(1))
      this.w_PNCODCLF = NVL(_Link_.PNCODCLF,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_PNSERIAL1 = space(10)
      endif
      this.w_PNTIPDOC = space(2)
      this.w_PNTIPREG = space(1)
      this.w_PNCODVAL = space(3)
      this.w_PNDATDOC = ctod("  /  /  ")
      this.w_PNDATREG = ctod("  /  /  ")
      this.w_PNTIPCLF = space(1)
      this.w_PNCODCLF = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PNT_MAST_IDX,2])+'\'+cp_ToStr(_Link_.PNSERIAL,1)
      cp_ShowWarn(i_cKey,this.PNT_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PNSERIAL1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PNCODCLF
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PNCODCLF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PNCODCLF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANOPETRE,ANFLSOAL,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PNCODCLF);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PNTIPCLF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_PNTIPCLF;
                       ,'ANCODICE',this.w_PNCODCLF)
            select ANTIPCON,ANCODICE,ANOPETRE,ANFLSOAL,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PNCODCLF = NVL(_Link_.ANCODICE,space(15))
      this.w_ANOPETRE = NVL(_Link_.ANOPETRE,space(1))
      this.w_ANFLSOAL = NVL(_Link_.ANFLSOAL,space(1))
      this.w_DESCLF = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PNCODCLF = space(15)
      endif
      this.w_ANOPETRE = space(1)
      this.w_ANFLSOAL = space(1)
      this.w_DESCLF = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PNCODCLF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      *i_bRes = i_bRes .and. .GSCG_AOS.CheckForm()
      if i_bres
        i_bres=  .GSCG_AOS.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PNSERIAL = this.w_PNSERIAL
    * --- GSCG_AOS : Depends On
    this.GSCG_AOS.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgscg_ko1Pag1 as StdContainer
  Width  = 696
  height = 441
  stdWidth  = 696
  stdheight = 441
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_1_14 as stdDynamicChildContainer with uid="XDEOVTADWY",left=4, top=1, width=687, height=388, bOnScreen=.t.;



  add object oBtn_1_15 as StdButton with uid="ZMIZZNTUDJ",left=590, top=392, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per salvare le impostazioni";
    , HelpContextID = 111184874;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_16 as StdButton with uid="JWIBHYOYEX",left=641, top=392, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 159022522;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_ko1','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
