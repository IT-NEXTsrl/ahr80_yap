* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bsd                                                        *
*              Stampa del documento                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_326]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-10                                                      *
* Last revis.: 2017-01-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bsd",oParentObject,m.pTipo)
return(i_retval)

define class tgsve_bsd as StdBatch
  * --- Local variables
  w_oMultiReport = .NULL.
  pTipo = 0
  w_RIFPRA = space(1)
  w_RIFOGG = space(1)
  w_RIFVAL = space(1)
  w_RIFDAT = space(1)
  w_RIFSUP = space(1)
  w_RIFRIGHE = space(1)
  w_RIFDATDIR = space(1)
  w_RIFDATONO = space(1)
  w_RIFDATTEM = space(1)
  w_RIFDATSPA = space(1)
  w_RIFQTA = space(1)
  w_RIFRESTIT = space(1)
  w_RIFPARTI = space(1)
  w_RIFNOIMPO = space(1)
  w_RIFCOEDIF = space(1)
  w_RIFIMP = space(1)
  w_RIFGU = space(1)
  w_RIFMINMAX = space(1)
  w_RIFSCOFIN = space(1)
  w_RIFDATGEN = space(1)
  w_RIFRAGFAS = space(1)
  w_PRTIPCAU = space(1)
  w_MVSERRIF = space(10)
  w_OK = .f.
  w_TIPO = space(1)
  w_NUMINI = 0
  w_NUMFIN = 0
  w_LINGUA = space(3)
  w_codiva = space(5)
  w_APPO = space(10)
  w_PRGSTA = space(8)
  w_contiva = 0
  w_NUMER = 0
  w_CODICE = space(15)
  w_DATAIN = ctod("  /  /  ")
  w_TRADLIN = space(3)
  w_SERDOC = space(10)
  w_contrate = 0
  w_FC = space(1)
  w_MESS = space(1)
  w_DATAFI = ctod("  /  /  ")
  w_TIPDOC = space(5)
  w_CAMPO = space(10)
  w_CATCOM = space(3)
  w_CLIFOR = space(15)
  w_CODCON = space(15)
  w_DATA = ctod("  /  /  ")
  w_TIPOIN = space(5)
  w_BIANCO = .f.
  w_CODCLA = space(3)
  w_NOTE = space(0)
  w_SERIE1 = space(10)
  w_SERIE2 = space(10)
  w_CODAGE = space(5)
  w_CATEGO = space(2)
  w_CODZON = space(3)
  w_CODVET = space(5)
  w_CATCOM = space(3)
  w_CODPAG = space(5)
  w_NOSBAN = space(5)
  w_CODESE = space(4)
  w_DT = space(35)
  w_OLDMAGA = space(5)
  w_CODSTA = space(3)
  w_OLDCAUSA = space(5)
  w_COND = .f.
  w_CODCAU = space(5)
  w_CAUMAG = space(5)
  w_CODART = space(20)
  w_DESART = space(40)
  w_TIPCON = space(1)
  w_CODMAG = space(5)
  w_CODICE1 = space(20)
  w_DESSUP = space(0)
  w_TRPRGST = 0
  w_OKMAG = .f.
  w_PRIMAG = space(5)
  w_KEYOBS = ctod("  /  /  ")
  w_TIPCON1 = space(1)
  w_CODCON1 = space(15)
  w_DTOBS0 = ctod("  /  /  ")
  w_DTOBS1 = ctod("  /  /  ")
  w_codic = space(15)
  w_nmdes = space(40)
  w_ind = space(40)
  w_cap = space(8)
  w_loca = space(30)
  w_prov = space(2)
  w_nazi = space(3)
  w_rif = space(2)
  w_DATORIG = ctod("  /  /  ")
  w_CODDES = space(5)
  w_PRIMAT = space(5)
  w_DESART1 = space(40)
  w_DESSUP1 = space(0)
  w_MVCODICE = space(20)
  w_DATREG = ctod("  /  /  ")
  w_CARMEM = 0
  w_OLDSUBJECT = space(254)
  w_OLDSUBJECTFAX = space(254)
  w_SERIAL = space(10)
  w_PADRE = .NULL.
  w_NOVERSED = .f.
  w_LOTDIFR = space(1)
  w_LOTDIFT = space(1)
  w_NUMRIF = 0
  w_DATMAT = ctod("  /  /  ")
  w_OGG = .NULL.
  w_OKLOT = .f.
  w_OLD_STOPFK = .f.
  w_OKCHI = .f.
  w_OLD_STOPFK = .f.
  w_LNumErr = 0
  TmpC = space(100)
  w_NAZION = space(3)
  w_CODGRU = space(10)
  w_FLCODO = space(1)
  w_MODSCHED = space(0)
  w_OBJECT = .NULL.
  w_TIPO_ATT = space(20)
  w_NUM_GG = 0
  w_DES_ATT = space(254)
  w_ASCLADOC = space(15)
  w_ASBARIMM = space(1)
  w_ASBAUTOM = space(1)
  w_ASNOREPS = space(1)
  w_RICDESCRI = space(1)
  w_MODALLEG = space(1)
  w_CATCH = space(250)
  w_Operazione = space(1)
  w_RETVAL = 0
  w_NomeFile = space(10)
  w_NomeRep = space(10)
  w_INCLASSEDOC = space(15)
  w_INARCHIVIO = space(20)
  w_ARCHIVIO = space(20)
  w_CHIAVE = space(20)
  w_DESBRE = space(100)
  w_DESLUN = space(0)
  w_TIPOALLE = space(5)
  w_CLASSEALL = space(5)
  w_QUEMOD = space(60)
  w_CDPUBWEB = space(1)
  w_CDRIFDEF = space(1)
  w_CDRIFTAB = space(20)
  w_GRPDEF = space(5)
  w_OLD_STOPFK = .f.
  w_SETLINKED = .f.
  * --- WorkFile variables
  DES_DIVE_idx=0
  KEY_ARTI_idx=0
  OUT_PUTS_idx=0
  TIP_DOCU_idx=0
  TRADDOCU_idx=0
  DOC_MAST_idx=0
  DOC_DETT_idx=0
  OUTPUTMP_idx=0
  AZIENDA_idx=0
  CONTI_idx=0
  STR_INTE_idx=0
  PAR_ALTE_idx=0
  PAR_AGEN_idx=0
  CAUMATTI_idx=0
  ASSCAUDO_idx=0
  PROMCLAS_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue la Stampa del Documento (da GSVE_MDV, GSOR_MDV, GSAC_MDV)
    * --- Parametri passati alla query per compatibilit� con ristampa doc.
    this.w_PADRE = This.oParentObject
    * --- Leggo lingua e gruppo default
    if Not empty(this.oParentObject.w_MVCODCON)
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANCODLIN,ANGRPDEF,ANNAZION,ANCODGRU"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_MVTIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVCODCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANCODLIN,ANGRPDEF,ANNAZION,ANCODGRU;
          from (i_cTable) where;
              ANTIPCON = this.oParentObject.w_MVTIPCON;
              and ANCODICE = this.oParentObject.w_MVCODCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_LINGUA = NVL(cp_ToDate(_read_.ANCODLIN),cp_NullValue(_read_.ANCODLIN))
        this.w_GRPDEF = NVL(cp_ToDate(_read_.ANGRPDEF),cp_NullValue(_read_.ANGRPDEF))
        this.w_NAZION = NVL(cp_ToDate(_read_.ANNAZION),cp_NullValue(_read_.ANNAZION))
        this.w_CODGRU = NVL(cp_ToDate(_read_.ANCODGRU),cp_NullValue(_read_.ANCODGRU))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    * --- Se ho impostato nelle causali documenti Imputazione Lotti alla Conferma e ho il modulo 
    *     Magazzino funzioni avanzate attivo, lancio la maschera dell'aggiornamento differito.
    if NOT EMPTY(this.oParentObject.w_MVSERIAL) AND this.oParentObject.w_MVFLPROV="N" AND this.pTipo<>0
      * --- Se e' Attivo il Modulo magazzino Produzione e il Documento e' Confermato 
      if g_EACD="S" AND this.oParentObject.w_FLARCO="S" AND (NOT EMPTY(this.oParentObject.w_CAUPFI) OR NOT EMPTY(this.oParentObject.w_CAUCOD))
        * --- e la causale prevede la gestione Articoli Composti, genera documenti Articoli Composti
        this.w_PADRE.NotifyEvent("EsplodeDistinta")     
      endif
    endif
    if g_MADV="S"
      if Not Empty(this.oParentObject.w_CAUCOD)
        * --- Read from TIP_DOCU
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TDLOTDIF"+;
            " from "+i_cTable+" TIP_DOCU where ";
                +"TDTIPDOC = "+cp_ToStrODBC(this.oParentObject.w_CAUCOD);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TDLOTDIF;
            from (i_cTable) where;
                TDTIPDOC = this.oParentObject.w_CAUCOD;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_LOTDIFR = NVL(cp_ToDate(_read_.TDLOTDIF),cp_NullValue(_read_.TDLOTDIF))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      if Not Empty(this.oParentObject.w_CAUPFI)
        * --- Read from TIP_DOCU
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TDLOTDIF"+;
            " from "+i_cTable+" TIP_DOCU where ";
                +"TDTIPDOC = "+cp_ToStrODBC(this.oParentObject.w_CAUPFI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TDLOTDIF;
            from (i_cTable) where;
                TDTIPDOC = this.oParentObject.w_CAUPFI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_LOTDIFT = NVL(cp_ToDate(_read_.TDLOTDIF),cp_NullValue(_read_.TDLOTDIF))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      if (this.oParentObject.w_LOTDIF="C" OR this.w_LOTDIFR $ "C-I" OR this.w_LOTDIFT $ "C-I") 
        this.w_DATMAT = this.oParentObject.w_MVDATREG
        this.w_NUMRIF = -20
        this.w_SERIAL = this.oParentObject.w_MVSERIAL
        this.w_OKLOT = .F.
        * --- Verifico se il documento � gi� corretto
        * --- Select from GSMD0SZM
        do vq_exec with 'GSMD0SZM',this,'_Curs_GSMD0SZM','',.f.,.t.
        if used('_Curs_GSMD0SZM')
          select _Curs_GSMD0SZM
          locate for 1=1
          do while not(eof())
          this.w_OKLOT = .T.
            select _Curs_GSMD0SZM
            continue
          enddo
          use
        endif
        if this.w_OKLOT
          if type("i_stopFK")="U"
            public i_stopFK
          endif
           
 Public g_DATSEL 
 g_DATSEL = this.oParentObject.w_MVDATDOC
          this.w_OLD_STOPFK = i_stopFK
          i_stopFK=.f.
          this.w_OGG = GSMD_SZM( this.oParentObject.w_MVSERIAL )
          i_stopFK=this.w_OLD_STOPFK
          this.w_OGG = .NULL.
        endif
      endif
    endif
    if Isalt() and this.oParentObject.w_MVCLADOC="FA" and this.pTipo>0
      * --- Select from query\GSPR_KZP
      do vq_exec with 'query\GSPR_KZP',this,'_Curs_query_GSPR_KZP','',.f.,.t.
      if used('_Curs_query_GSPR_KZP')
        select _Curs_query_GSPR_KZP
        locate for 1=1
        do while not(eof())
        this.w_OKCHI = .T.
        exit
          select _Curs_query_GSPR_KZP
          continue
        enddo
        use
      endif
      if this.oParentObject.w_FLGCHI="S" AND this.w_OKCHI AND Ah_yesno("Si vuole procedere alla chiusura delle pratiche del documento?")
        if type("i_stopFK")="U"
          public i_stopFK
        endif
        this.w_OLD_STOPFK = i_stopFK
        i_stopFK=.f.
        this.w_OGG = GSPR_KZP(this.oParentObject.w_MVSERIAL)
        i_stopFK=this.w_OLD_STOPFK
        this.w_OGG = .null.
      endif
    endif
    if g_VEFA="S" 
      * --- Creazione FIle EDI
    endif
    if USED("Matrerro") and g_COLA="S"
      this.w_LNumErr = reccount("Matrerro")
      if this.w_LNumErr>0
        this.TmpC = "Si sono verificati errori (%1) durante l'elaborazione%0Desideri la stampa dell'elenco degli errori?"
        if ah_YesNo(this.TmpC,"",alltrim(str(this.w_LNumErr,5,0)) )
          SELECT * FROM MATRERRO INTO CURSOR __TMP__
          CP_CHPRN("..\COLA\EXE\QUERY\GSCO_SML.FRX", " ", this)
        endif
        do GSMD_SZM with "          "
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      SELECT Matrerro
      USE
    endif
    if g_AGEN="S" AND this.pTipo=2 and Vartype(this.w_PADRE.w_OGGATT)="O"
      * --- Eseguo abbinamento documenti\attivit�
      GSAG_BAB(this,"L")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_PADRE.w_OGGATT = .Null.
    endif
    if NOT ((this.oParentObject.w_FLNSTA="S" AND this.pTipo<>0) OR (EMPTY(this.oParentObject.w_MVSERIAL) AND this.pTipo=0))
      * --- Documento Abilitato per le Stampe o selezionato
      * --- I vettori vanno definiti fuori dalla funzione
       
 dimension iva(6,2) 
 dimension doc_rate(999,3) 
 dimension doc_imba(20,7) 
 dimension doc_rite(6,5) 
 iva="" 
 doc_rate="" 
 doc_rite="" 
 doc_imba=""
      dimension aCONT_ACC(6,3)
      aCONT_ACC=""
      * --- Cambio della Valuta in LIRE utilizzato in talune stampe
      l_cambio = GETCAM(g_PERVAL, I_DATSYS)
      * --- Passo al report Flag per incasso Corrispettivi
      L_FLINCA=this.oParentObject.w_FLINCA
      * --- SETTO VARIABILI PER FAX E EMAIL
      i_DEST = ALLTRIM(iif(empty(this.oParentObject.w_MVCODDES), this.oParentObject.w_DESCLF, this.oParentObject.w_NOMDES))
      i_FAXNO = this.oParentObject.w_TELFAX
      i_EMAIL = this.oParentObject.w_EMAIL
      i_EMAIL_PEC = this.oParentObject.w_EMPEC
      * --- Verifico i contatti associati al cliente o fornitore per inserire gli indirizzi email
      *     Solo per Fatture, Ordini e DDT
      if this.oParentObject.w_MVCLADOC $ "FA-DT-OR-NC"
        this.w_NOVERSED = IIF(EMPTY(NVL(this.oParentObject.w_MVCODDES," ")),EMPTY(this.oParentObject.w_DDCODDES),EMPTY(this.oParentObject.w_MVCODDES))
        GSAR_BRM(this,"E_M",this.oParentObject.w_MVTIPCON,this.oParentObject.w_MVCODCON,IIF(this.oParentObject.w_MVCLADOC $ "DT-OR","NO","FA"),"V",IIF(EMPTY(NVL(this.oParentObject.w_MVCODDES," ")),this.oParentObject.w_DDCODDES,this.oParentObject.w_MVCODDES),this.w_NOVERSED and this.oParentObject.w_MVCLADOC $ "DT-OR")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Valorizzo le variabili globali per il Fax (per la selezione del numero di telefono)
        i_CLIFORDES = this.oParentObject.w_MVTIPCON 
 i_CODDES = this.oParentObject.w_MVCODCON 
 i_TIPDES = IIF(this.oParentObject.w_MVCLADOC $ "DT-OR","CO","FA") 
 i_ORIGINE="N"
      endif
      i_WEDEST = this.oParentObject.w_MVTIPCON+this.oParentObject.w_MVCODCON
      i_WEALLENAME = this.oParentObject.w_CATDOC+this.oParentObject.w_MVFLVEAC+"_"+dtos(this.oParentObject.w_MVDATDOC)+"_"+Alltrim(this.oParentObject.w_MVALFDOC)+IIF(EMPTY(this.oParentObject.w_MVALFDOC),"","_")+alltrim(STR(this.oParentObject.w_MVNUMDOC,15,0))
      do case
        case this.oParentObject.w_CATDOC="DI"
          i_WEALLETITLE = ah_Msgformat("Doc.interno n. %1 del %2",alltrim(STR(this.oParentObject.w_MVNUMDOC,15,0))+IIF(EMPTY(this.oParentObject.w_MVALFDOC),"","/")+alltrim(this.oParentObject.w_MVALFDOC), dtoc(this.oParentObject.w_MVDATDOC) )
        case this.oParentObject.w_CATDOC="OR"
          i_WEALLETITLE = ah_Msgformat("Ordine n. %1 del %2",alltrim(STR(this.oParentObject.w_MVNUMDOC,15,0))+IIF(EMPTY(this.oParentObject.w_MVALFDOC),"","/")+alltrim(this.oParentObject.w_MVALFDOC), dtoc(this.oParentObject.w_MVDATDOC) )
        case this.oParentObject.w_CATDOC="DT"
          i_WEALLETITLE = ah_Msgformat("Doc.trasporto n. %1 del %2",alltrim(STR(this.oParentObject.w_MVNUMDOC,15,0))+IIF(EMPTY(this.oParentObject.w_MVALFDOC),"","/")+alltrim(this.oParentObject.w_MVALFDOC), dtoc(this.oParentObject.w_MVDATDOC) ) 
        case this.oParentObject.w_CATDOC="FA"
          i_WEALLETITLE = ah_Msgformat("Fattura n. %1 del %2",alltrim(STR(this.oParentObject.w_MVNUMDOC,15,0))+IIF(EMPTY(this.oParentObject.w_MVALFDOC),"","/")+alltrim(this.oParentObject.w_MVALFDOC), dtoc(this.oParentObject.w_MVDATDOC) )
        case this.oParentObject.w_CATDOC="NC"
          i_WEALLETITLE = ah_Msgformat("Nota di credito n. %1 del %2",alltrim(STR(this.oParentObject.w_MVNUMDOC,15,0))+IIF(EMPTY(this.oParentObject.w_MVALFDOC),"","/")+alltrim(this.oParentObject.w_MVALFDOC), dtoc(this.oParentObject.w_MVDATDOC) )
        case this.oParentObject.w_CATDOC="OP"
          i_WEALLETITLE = ah_Msgformat("Ordine previsionale n. %1 del %2",alltrim(STR(this.oParentObject.w_MVNUMDOC,15,0))+IIF(EMPTY(this.oParentObject.w_MVALFDOC),"","/")+alltrim(this.oParentObject.w_MVALFDOC), dtoc(this.oParentObject.w_MVDATDOC) )
        case this.oParentObject.w_CATDOC="RF"
          i_WEALLETITLE = ah_Msgformat("Corrispettivo n. %1 del %2",alltrim(STR(this.oParentObject.w_MVNUMDOC,15,0))+IIF(EMPTY(this.oParentObject.w_MVALFDOC),"","/")+alltrim(this.oParentObject.w_MVALFDOC), dtoc(this.oParentObject.w_MVDATDOC) )
        otherwise
          i_WEALLETITLE = ah_Msgformat("Documento n. %1 del %2",alltrim(STR(this.oParentObject.w_MVNUMDOC,15,0))+IIF(EMPTY(this.oParentObject.w_MVALFDOC),"","/")+alltrim(this.oParentObject.w_MVALFDOC), dtoc(this.oParentObject.w_MVDATDOC) )
      endcase
      this.w_OLDSUBJECT = i_EMAILSUBJECT
      this.w_OLDSUBJECTFAX = i_FAXSUBJECT
      i_EMAILSUBJECT=i_WEALLETITLE
      i_FAXSUBJECT = i_EMAILSUBJECT
      * --- Se attivo il modulo IZCP valorizza le relative variabili pubbliche 
      if g_IZCP$"SA"
        g_ZCPTINTEST = this.oParentObject.w_MVTIPCON
        g_ZCPCINTEST = this.oParentObject.w_MVCODCON
        g_ZCPALLENAME = alltrim(i_WEALLENAME)+".PDF"
        g_ZCPALLETITLE = i_WEALLETITLE
        g_ZCPOBTEST = this.oParentObject.w_MVDATDOC
        g_ZCPFILETYPE = "DOCUM"
        g_ZCPFILEPKEY = IIF(EMPTY(this.oParentObject.w_MVSERWEB),"G"+this.oParentObject.w_MVSERIAL,this.oParentObject.w_MVSERWEB)
        g_ZCPLOCATION = this.oParentObject.w_MVTIPCON+ALLTRIM(this.oParentObject.w_MVCODCON)+ALLTRIM(NVL(this.oParentObject.w_MVCODDES," "))
        g_ZCPYEAR = NVL(ALLTRIM(STR(YEAR(this.oParentObject.w_MVDATDOC))), " ")
        g_ZCPMONTH = RIGHT("00"+ALLTRIM(NVL(ALLTRIM(STR(MONTH(this.oParentObject.w_MVDATDOC))), " ")),2)
        g_ZCPDOCTYPE = this.oParentObject.w_MVTIPDOC
      endif
      ah_Msg("Stampa in corso...")
      * --- Controllo i documenti di trasporto
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Resetta Fax/Email
    i_FAXNO = ""
    i_EMAIL = ""
    i_EMAIL_PEC = ""
    i_DEST = " "
    i_WEDEST = " "
    i_WEALLENAME = " "
    i_WEALLETITLE = " "
    i_EMAILSUBJECT = this.w_OLDSUBJECT
    i_FAXSUBJECT = this.w_OLDSUBJECTFAX
    g_ZCPTINTEST = " "
    g_ZCPCINTEST = " "
    g_ZCPALLENAME = " "
    g_ZCPALLETITLE = " "
    g_ZCPFILETYPE=" "
    g_ZCPFILEPKEY= " "
    g_ZCPLOCATION = " "
    g_ZCPYEAR = " "
    g_ZCPMONTH = " "
    g_ZCPDOCTYPE = " "
    i_CLIFORDES =" "
    i_CODDES =" "
    i_TIPDES =" "
    i_ORIGINE=" "
    if IsAlt() and this.pTipo=2
      * --- Solo se Alterego e solo se � stato inserito un nuovo documento
      * --- Variabile Richiesta conferma documento
      this.w_FLCODO = " "
      this.w_OK = .T.
      * --- Lettura dal campo della tabella Parametri generali (PAR_ALTE)
      * --- Read from PAR_ALTE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_ALTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_ALTE_idx,2],.t.,this.PAR_ALTE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PAFLCODO"+;
          " from "+i_cTable+" PAR_ALTE where ";
              +"PACODAZI = "+cp_ToStrODBC(i_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PAFLCODO;
          from (i_cTable) where;
              PACODAZI = i_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_FLCODO = NVL(cp_ToDate(_read_.PAFLCODO),cp_NullValue(_read_.PAFLCODO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_FLCODO="S"
        * --- Richiesta conferma documento
        this.w_OK = ah_YesNo("Vuoi confermare il documento stampato?")
        if NOT this.w_OK
          if VARTYPE(g_SCHEDULER)<>"C" Or g_SCHEDULER<>"S"
            public g_SCHEDULER
            g_SCHEDULER="S"
            this.w_MODSCHED = "S"
          endif
          * --- Deve modificare cancellare l'attivit�
          * --- Instanzio l'anagrafica
          this.w_OBJECT = GSVE_MDV(this.oParentObject.w_MVCLADOC)
          * --- Controllo se ha passato il test di accesso
          if !(this.w_OBJECT.bSec1)
            i_retcode = 'stop'
            return
          endif
          * --- Cerco il record selezionato
          this.w_OBJECT.ECPFILTER()     
          this.w_OBJECT.w_MVSERIAL = this.oParentObject.w_MVSERIAL
          this.w_OBJECT.ECPSAVE()     
          * --- Cancello
          this.w_OBJECT.ECPDELETE()     
          this.w_OBJECT.ECPQUIT()     
          if this.w_MODSCHED="S"
            g_SCHEDULER=" "
          endif
        endif
      endif
      * --- Lettura dalla tabella Parametri attivit�
      * --- Read from PAR_AGEN
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_AGEN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_AGEN_idx,2],.t.,this.PAR_AGEN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PATIPCTR,PANGGINC"+;
          " from "+i_cTable+" PAR_AGEN where ";
              +"PACODAZI = "+cp_ToStrODBC(i_codazi);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PATIPCTR,PANGGINC;
          from (i_cTable) where;
              PACODAZI = i_codazi;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_TIPO_ATT = NVL(cp_ToDate(_read_.PATIPCTR),cp_NullValue(_read_.PATIPCTR))
        this.w_NUM_GG = NVL(cp_ToDate(_read_.PANGGINC),cp_NullValue(_read_.PANGGINC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_PRTIPCAU = This.oparentobject.w_PRTIPCAU
      * --- Se non siamo sotto schedulatore, se � stato confermato il doc. stampato, se inserito il tipo attivit� nei Parametri attivit�, se siamo in presenza di una proforma/proforma di acconto
      if (VARTYPE(g_SCHEDULER)<>"C" OR g_SCHEDULER<>"S" ) AND this.w_OK AND NOT EMPTY(this.w_TIPO_ATT) AND this.w_PRTIPCAU$"SP"
        if ah_YesNo("Vuoi caricare l'attivit� per il controllo dell'incasso?")
          * --- Lettura dell'oggetto attivit� dalla tabella Tipi attivit�
          * --- Read from CAUMATTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAUMATTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAUMATTI_idx,2],.t.,this.CAUMATTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CADESCRI"+;
              " from "+i_cTable+" CAUMATTI where ";
                  +"CACODICE = "+cp_ToStrODBC(this.w_TIPO_ATT);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CADESCRI;
              from (i_cTable) where;
                  CACODICE = this.w_TIPO_ATT;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DES_ATT = NVL(cp_ToDate(_read_.CADESCRI),cp_NullValue(_read_.CADESCRI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
    endif
    if g_AGFA="S" AND NOT ISALT() AND (this.pTipo=1 OR this.pTipo=2)
      * --- Creazione evento da documento
      GSFA_BGE(this,this.oParentObject.w_MVSERIAL)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.oParentObject.cFunction="Load"
      * --- Verifico se devo generare il file xml per acquisizione tramite barcode
      * --- Read from ASSCAUDO
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ASSCAUDO_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ASSCAUDO_idx,2],.t.,this.ASSCAUDO_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ASCLADOC,ASBARIMM,ASBAUTOM,ASNOREPS"+;
          " from "+i_cTable+" ASSCAUDO where ";
              +"AS__TIPO = "+cp_ToStrODBC("D");
              +" and ASCODCAU = "+cp_ToStrODBC(this.oParentObject.w_MVTIPDOC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ASCLADOC,ASBARIMM,ASBAUTOM,ASNOREPS;
          from (i_cTable) where;
              AS__TIPO = "D";
              and ASCODCAU = this.oParentObject.w_MVTIPDOC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ASCLADOC = NVL(cp_ToDate(_read_.ASCLADOC),cp_NullValue(_read_.ASCLADOC))
        this.w_ASBARIMM = NVL(cp_ToDate(_read_.ASBARIMM),cp_NullValue(_read_.ASBARIMM))
        this.w_ASBAUTOM = NVL(cp_ToDate(_read_.ASBAUTOM),cp_NullValue(_read_.ASBAUTOM))
        this.w_ASNOREPS = NVL(cp_ToDate(_read_.ASNOREPS),cp_NullValue(_read_.ASNOREPS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_ASBARIMM="S" AND NOT EMPTY(this.w_ASCLADOC)
        if SUBSTR( CHKPECLA( this.w_ASCLADOC, i_CODUTE), 1, 1 ) = "S"
          * --- Read from PROMCLAS
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PROMCLAS_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CDGETDES,CDMODALL,CDPUBWEB,CDRIFDEF,CDRIFTAB"+;
              " from "+i_cTable+" PROMCLAS where ";
                  +"CDCODCLA = "+cp_ToStrODBC(this.w_ASCLADOC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CDGETDES,CDMODALL,CDPUBWEB,CDRIFDEF,CDRIFTAB;
              from (i_cTable) where;
                  CDCODCLA = this.w_ASCLADOC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_RICDESCRI = NVL(cp_ToDate(_read_.CDGETDES),cp_NullValue(_read_.CDGETDES))
            this.w_MODALLEG = NVL(cp_ToDate(_read_.CDMODALL),cp_NullValue(_read_.CDMODALL))
            this.w_CDPUBWEB = NVL(cp_ToDate(_read_.CDPUBWEB),cp_NullValue(_read_.CDPUBWEB))
            this.w_CDRIFDEF = NVL(cp_ToDate(_read_.CDRIFDEF),cp_NullValue(_read_.CDRIFDEF))
            this.w_CDRIFTAB = NVL(cp_ToDate(_read_.CDRIFTAB),cp_NullValue(_read_.CDRIFTAB))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_CDRIFTAB = upper(alltrim(this.w_CDRIFTAB))
          do case
            case not this.w_MODALLEG $ "IS"
              ah_ErrorMsg("Generazione barcode: modalit� classe documentale non valida")
            case this.w_CDRIFDEF="N"
              ah_ErrorMsg("Generazione barcode: valore su <archiviazione rapida> non valido")
            case this.w_CDRIFTAB<>"DOC_MAST"
              ah_ErrorMsg("Generazione barcode: tabella di riferimento non valida")
            otherwise
              if this.w_MODALLEG="I"
                * --- creo pdf vuoto per CPIN e DMIP (su IDMSsa non ci deve essere)
                this.w_CATCH = FORCEEXT(ADDBS(tempadhoc())+SUBSTR(SYS(2015),2),"PDF")
                copy file "query\gsut_bcv.pdf" to (this.w_CATCH)
              endif
              this.w_NomeFile = this.w_CATCH
              this.w_NomeRep = "GSUT_BCV.PDF"
              this.w_INCLASSEDOC = this.w_ASCLADOC
              this.w_INARCHIVIO = "X"
              this.w_ARCHIVIO = this.w_CDRIFTAB
              this.w_CHIAVE = this.oParentObject.w_MVSERIAL
              this.w_Operazione = "B"
              i_nConn=i_TableProp[this.DOC_MAST_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
              * --- Questo cursore sarebbe aggiornato dalla LoadRec. ma adesso sono in caricamento (lo usa la GSUT_BBA)
              this.w_RETVAL = cp_SQL(i_nConn,"select * from "+i_cTable+" where MVSERIAL="+cp_ToStrODBC(this.w_CHIAVE), this.oParentObject.cCursor)
              if this.w_RETVAL<>-1
                this.w_RETVAL = GSUT_BPI(this,4)
              else
                ah_ErrorMsg("Errore generazione barcode:%0%1",,, message())
              endif
          endcase
        else
          ah_ErrorMsg("Generazione barcode: non si possiedono le autorizzazioni per la classe documentale indicata")
        endif
      endif
    endif
    this.bUpdateParentObject=.F.
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if g_DISB="S" 
      this.w_SERDOC = this.oParentObject.w_MVSERIAL
      this.w_DATREG = this.oParentObject.w_MVDATREG
      this.w_NUMINI = this.oParentObject.w_MVNUMDOC
      this.w_NUMFIN = this.oParentObject.w_MVNUMDOC
      this.w_SERIE1 = this.oParentObject.w_MVALFDOC
      this.w_SERIE2 = this.oParentObject.w_MVALFDOC
      this.w_CLIFOR = this.oParentObject.w_MVCODCON
      this.w_DATAFI = this.oParentObject.w_MVDATDOC
      this.w_DATAIN = this.oParentObject.w_MVDATDOC
    endif
    * --- Creo tabella vuota, altrimenti GSAR_BSD va in errore
    if Isalt()
       
 Public g_NOTMSG 
 g_NOTMSG=this.oParentObject.w_MV__NOTE
      this.w_RIFPRA = This.oparentobject.w_RIFPRA
      this.w_RIFOGG = This.oparentobject.w_RIFOGG
      this.w_RIFVAL = This.oparentobject.w_RIFVAL
      this.w_RIFDAT = This.oparentobject.w_RIFDAT
      this.w_RIFSUP = This.oparentobject.w_RIFSUP
      this.w_RIFRIGHE = This.oparentobject.w_RIFRIGHE
      this.w_RIFDATDIR = This.oparentobject.w_RIFDATDIR
      this.w_RIFDATONO = This.oparentobject.w_RIFDATONO
      this.w_RIFDATTEM = This.oparentobject.w_RIFDATTEM
      this.w_RIFDATSPA = This.oparentobject.w_RIFDATSPA
      this.w_RIFQTA = This.oparentobject.w_RIFQTA
      this.w_RIFRESTIT = This.oparentobject.w_RIFRESTIT
      this.w_RIFPARTI = This.oparentobject.w_RIFPARTI
      this.w_RIFNOIMPO = This.oparentobject.w_RIFNOIMPO
      this.w_RIFCOEDIF = This.oparentobject.w_RIFCOEDIF
      this.w_RIFIMP = This.oparentobject.w_RIFIMP
      this.w_RIFGU = This.oparentobject.w_RIFGU
      this.w_RIFMINMAX = This.oparentobject.w_RIFMINMAX
      this.w_RIFSCOFIN = This.oparentobject.w_RIFSCOFIN
      this.w_RIFDATGEN = This.oparentobject.w_RIFDATGEN
      this.w_RIFRAGFAS = This.oparentobject.w_RIFRAGFAS
      this.w_PRTIPCAU = This.oparentobject.w_PRTIPCAU
    endif
    * --- Create temporary table OUTPUTMP
    i_nIdx=cp_AddTableDef('OUTPUTMP') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.AZIENDA_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          +" where 1=0";
          )
    this.OUTPUTMP_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    this.w_oMultiReport=createobject("MultiReport")
    if NOT GSAR_BSD(this,this.w_oMultiReport, This, this.oParentObject.w_MVTIPDOC, this.w_LINGUA, this.w_GRPDEF, "D",.F.,.T.)
      * --- Drop temporary table OUTPUTMP
      i_nIdx=cp_GetTableDefIdx('OUTPUTMP')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('OUTPUTMP')
      endif
      i_retcode = 'stop'
      return
    endif
    * --- Drop temporary table OUTPUTMP
    i_nIdx=cp_GetTableDefIdx('OUTPUTMP')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('OUTPUTMP')
    endif
    if !(this.oParentObject.cFunction <> "Query" And this.w_oMultiReport.IsEmpty())
      * --- L'HasEvent  � lanciata all'interno della Cp_DOaction che mette a .t.
      *     i_stopFk per impedire successive pressioni di tasto. Per poter
      *     gestire Esc e F10 sulla maschera la valorizzo a .F.
      if type("i_stopFK")="U"
        public i_stopFK
      endif
      this.w_OLD_STOPFK = i_stopFK
      i_stopFK=.f.
      CP_CHPRN(this.w_oMultiReport, , this)
      i_stopFK=this.w_OLD_STOPFK
    endif
     
 Release g_NOTMSG
    this.w_oMultiReport = .null.
    WAIT CLEAR
    * --- Chiusura cursore Righe Evase senza Importazione
    if used(this.oParentObject.w_Cur_Ev_Row)
      select ( this.oParentObject.w_Cur_Ev_Row )
      use
    endif
    * --- Chiusura cursore Righe Raggruppate in Importazione
    if used(this.oParentObject.w_Cur_Rag_Row)
      select ( this.oParentObject.w_Cur_Rag_Row )
      use
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Apertura attivit� per il controllo dell'incasso di una proforma
    * --- Instanzio l'anagrafica
    this.w_OBJECT = GSAG_AAT()
    * --- Controllo se ha passato il test di accesso
    if !(this.w_OBJECT.bSec1)
      i_retcode = 'stop'
      return
    endif
    this.w_OBJECT.ECPLOAD()     
    this.w_SETLINKED = SetValueLinked("M", this.w_OBJECT, "w_ATCAUATT", this.w_TIPO_ATT)
    this.w_SETLINKED = SetValueLinked("M", this.w_OBJECT, "w_ATOGGETT", alltrim(this.w_DES_ATT)+" Proforma n. "+alltrim(str(this.oParentObject.w_MVNUMDOC))+IIF(NOT EMPTY(this.oParentObject.w_MVALFDOC), "/"+alltrim(this.oParentObject.w_MVALFDOC), "")+" del "+DTOC(this.oParentObject.w_MVDATDOC))
    this.w_SETLINKED = SetValueLinked("M", this.w_OBJECT, "w_DATINI", DATE()+this.w_NUM_GG)
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pTipo)
    this.pTipo=pTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,16)]
    this.cWorkTables[1]='DES_DIVE'
    this.cWorkTables[2]='KEY_ARTI'
    this.cWorkTables[3]='OUT_PUTS'
    this.cWorkTables[4]='TIP_DOCU'
    this.cWorkTables[5]='TRADDOCU'
    this.cWorkTables[6]='DOC_MAST'
    this.cWorkTables[7]='DOC_DETT'
    this.cWorkTables[8]='*OUTPUTMP'
    this.cWorkTables[9]='AZIENDA'
    this.cWorkTables[10]='CONTI'
    this.cWorkTables[11]='STR_INTE'
    this.cWorkTables[12]='PAR_ALTE'
    this.cWorkTables[13]='PAR_AGEN'
    this.cWorkTables[14]='CAUMATTI'
    this.cWorkTables[15]='ASSCAUDO'
    this.cWorkTables[16]='PROMCLAS'
    return(this.OpenAllTables(16))

  proc CloseCursors()
    if used('_Curs_GSMD0SZM')
      use in _Curs_GSMD0SZM
    endif
    if used('_Curs_query_GSPR_KZP')
      use in _Curs_query_GSPR_KZP
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo"
endproc
