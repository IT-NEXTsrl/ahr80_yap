* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bnp                                                        *
*              Batch note provvigioni                                          *
*                                                                              *
*      Author: Tam Software                                                    *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_203]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-06-05                                                      *
* Last revis.: 2013-03-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bnp",oParentObject)
return(i_retval)

define class tgsve_bnp as StdBatch
  * --- Local variables
  w_CAOVAL = 0
  w_CODAZI = space(5)
  w_PERPREAG = 0
  w_TIPO = space(1)
  w_TOTPROV = 0
  w_PLURIMAX = 0
  w_NUMNOTA = 0
  w_CODIVA = space(5)
  w_ANNO = 0
  w_FLESCL = space(1)
  w_MAXIMPON = 0
  w_PERIVA = 0
  w_PLNUMREG = 0
  w_AGENTE = space(5)
  w_MONOMAX = 0
  w_VALNUM = space(3)
  w_TEMP = 0
  w_AGENTE = space(5)
  w_IMPONI = 0
  w_TOTIMPON = 0
  w_CODVAL = space(3)
  w_TOTALE = 0
  w_IMPOSTA = 0
  w_Dectot = 0
  w_IMPONI1 = 0
  w_NETTOCOR = 0
  w_IVA = 0
  w_ENASARCO = 0
  w_ENASARCOAS = 0
  w_RITENUTA = 0
  w_IMPRITE = 0
  w_NOTAPROV = 0
  w_VALUNOTA = space(3)
  w_TOTMAXIM = 0
  w_TOTPRO1 = 0
  w_PERPREAZ = 0
  w_TOTPERC = 0
  w_MAXCONTR = 0
  w_CONPRE = 0
  w_TOTCONPRE = 0
  w_MAXNES = 0
  w_MAXESC = 0
  w_MASSIMALE = space(1)
  w_SOKAGE = 0
  w_AGFLAZIE = space(1)
  * --- WorkFile variables
  PAR_PROV_idx=0
  VOCIIVA_idx=0
  PRO_NUME_idx=0
  PRO_LIQU_idx=0
  AGENTI_idx=0
  VALUTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato dalla stampa note provvigioni (GSVE_SNP)
    * --- Controllo la presenza degli agenti..
    if Empty( this.oParentObject.w_AGEINI ) Or Empty ( this.oParentObject.w_AGEFIN )
      ah_ErrorMsg("Inserire agente iniziale e/o agente finale")
      i_retcode = 'stop'
      return
    endif
    * --- Leggo il codice Azienda
    this.w_CODAZI = i_CODAZI
    if this.oParentObject.w_DATA1>this.oParentObject.w_DATA2
      ah_ErrorMsg("Intervallo di date errato")
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_CODFOR="R" AND this.oParentObject.w_STAMPA="F"
      ah_ErrorMsg("Agente iniziale incongruente con il tipo di stampa")
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_CODFOR1="R" AND this.oParentObject.w_STAMPA="F"
      ah_ErrorMsg("Agente finale incongruente con il tipo di stampa")
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_CODFOR="F" AND this.oParentObject.w_STAMPA="R"
      ah_ErrorMsg("Agente iniziale incongruente con il tipo di stampa")
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_CODFOR1="F" AND this.oParentObject.w_STAMPA="R"
      ah_ErrorMsg("Agente finale incongruente al tipo di stampa")
      i_retcode = 'stop'
      return
    endif
    * --- Se la variabile w_stampa assume il valore 'F' lancio la stampa Fattura
    *     se assume il valore 'R' lancio la stampa Ricevuta
    *     infine se assume il valore 'N' lancio la stampa Nota Provvigioni
    do case
      case this.oParentObject.w_STAMPA="F"
        * --- Eseguo la query di estrazione dei dati.
        vq_exec("query\GSVE_PLF.VQR",this,"TEMP")
        SELECT TEMP 
        * --- Verifico se all'interno del cursore TEMP sono presenti provvigioni con il cambio
        *     non valorizzato. 
        *     Se esistono provvigioni senza cambio lancio una stampa di errore.
        Select Plnumreg,Pl__Anno,Pldatreg,Plcodage from Temp into cursor Errore where Nvl(plcaoval,0)=0
        if Reccount ("Errore")>0
          if ah_YesNo("Stampo situazione messaggi di errore?")
            Select * from Errore into cursor __TMP__
            CP_CHPRN("QUERY\GSVE_ERR.FRX", " ", this)
          endif
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          i_retcode = 'stop'
          return
        else
          * --- Raggruppo per Plnumnot,Plalfnot,Pldatnot,Plcodval,Plcaoval
          *     Effettuo il raggruppamento per il cambio nel caso di valuta ExtraEmu
          Select Min(Agdesage) as Agdesage, Min(Agindage) as Agindage, Min(Agcapage) as Agcapage, Min(Agcitage) as Agcitage,; 
 Min(Agproage) as Agproage, Min(Agfisage) as Agfisage, Min(Anpariva) as Anpariva, Min(Agcodfor) as Agcodfor,; 
 Sum(Pltotpro) as Pltotpro,Min(Plcodage) as Plcodage,Min(Agritirp) as Agritirp,Min(Agperimp) as Agperimp,; 
 Min(Agflazie) as Agflazie, Plnumnot,Plalfnot,Pldatnot,; 
 Min(Pldatini) as Pldatini,Max(Pldatfin) as Pldatfin,Min(Pl__Anno) as Pl__Anno,Min(Imponi) as Imponi,; 
 Min(Flescl) as Flescl,Plcodval , Min(Ancodval) as Ancodval,Min(Vadectot) as Vadectot,; 
 Min(Plvalnot) as Plvalnot,Min (Plimpnot) as Plimpnot ,Plcaoval,Min(Afflintr) as Afflintr,Min(Ancodiva) as Ancodiva; 
 From TEMP group by Plnumnot,Plalfnot,Pldatnot,Plcodval,Plcaoval ; 
 order by Plnumnot,Plalfnot,Pldatnot,Plcodval into cursor Temp
        endif
      case this.oParentObject.w_STAMPA="R"
        vq_exec("query\GSVE_PLR.VQR",this,"TEMP")
        * --- Verifico se all'interno del cursore TEMP sono presenti provvigioni con il cambio
        *     non valorizzato. Se sono presenti blocco l'elaborazione stampando l'elenco
        *     delle registrazioni.
        Select Plnumreg,Pl__Anno,Pldatreg,Plcodage from Temp into cursor Errore where Nvl(plcaoval,0)=0
        if Reccount ("Errore")>0
          if ah_YesNo("Stampo situazione messaggi di errore?")
            Select * from Errore into cursor __TMP__
            CP_CHPRN("QUERY\GSVE_ERR.FRX", " ", this)
          endif
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          i_retcode = 'stop'
          return
        else
          * --- Raggruppo per Agente, Valuta e Cambio.
          *     Effettuo il raggruppamento per il cambio nel caso di valuta ExtraEmu
          Select Min(Agdesage) as Agdesage, Min(Agindage) as Agindage, Min(Agcapage) as Agcapage, Min(Agcitage) as Agcitage,; 
 Min(Agproage) as Agproage, Min(Agfisage) as Agfisage, Min(Anpariva) as Anpariva, Min(Agcodfor) as Agcodfor,; 
 Sum(Pltotpro) as Pltotpro,Plcodage,Min(Agritirp) as Agritirp,Min(Agperimp) as Agperimp,; 
 Min(Agflazie) as Agflazie, Min(Plnumnot) as Plnumnot,Min(Plalfnot) as Plalfnot,Min(Pldatnot) as Pldatnot,; 
 Min(Pldatini) as Pldatini,Max(Pldatfin) as Pldatfin,Min(Pl__Anno) as Pl__Anno,Min(Imponi) as Imponi,; 
 Min(Flescl) as Flescl,Plcodval , Min(Ancodval) as Ancodval,Min(Vadectot) as Vadectot,; 
 Min(Iva) as Iva, Min(Enasarco) as Enasarco, Min(ENASARCOAS) as ENASARCOAS, Min(Ritenuta) as Ritenuta, Min(Nettocor) as Nettocor,Plcaoval,; 
 Min(Afflintr) as Afflintr,Min(Ancodiva) as Ancodiva; 
 From TEMP group by Plcodage,Plcodval,Plcaoval order by Plcodage,Plcodval into cursor Temp
        endif
      case this.oParentObject.w_STAMPA=" "
        * --- Filtro sul tipo della nota provvigioni (definitiva o meno)
        this.w_TIPO = IIF( this.oParentObject.w_SCELTA ="R","S","N")
        L_NUMERO=this.oParentObject.w_NUMERO
        L_SERIE= this.oParentObject.w_ALFDOC
        * --- Controllo se sto effettuando una stampa in 'Definitiva' oppure una 'Ristampa'
        if this.w_TIPO="N"
          * --- Stampa in 'Definitiva' oppure Simulata.
          vq_exec("query\GSVE_SNP.VQR",this,"TEMP")
          * --- Verifico se all'interno del cursore TEMP sono presenti provvigioni con il cambio
          *     non valorizzato.
          Select Plnumreg,Pl__Anno,Pldatreg,Plcodage from Temp into cursor Errore where Nvl(plcaoval,0)=0
          if Reccount ("Errore")>0
            if ah_YesNo("Stampo situazione messaggi di errore?")
              Select * from Errore into cursor __TMP__
              CP_CHPRN("QUERY\GSVE_ERR.FRX", " ", this)
            endif
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            i_retcode = 'stop'
            return
          else
            * --- Raggruppo per Agente, Valuta e Cambio.
            *     Effettuo il raggruppamento per il cambio nel caso di valuta ExtraEmu
            Select Min(Agdesage) as Agdesage, Min(Agindage) as Agindage, Min(Agcapage) as Agcapage, Min(Agcitage) as Agcitage,; 
 Min(Agproage) as Agproage, Min(Agfisage) as Agfisage, Min(Anpariva) as Anpariva, Min(Agcodfor) as Agcodfor,; 
 Sum(Pltotpro) as Pltotpro,Plcodage,Min(Agritirp) as Agritirp,Min(Agperimp) as Agperimp,; 
 Min(Agflazie) as Agflazie, Min(Plnumnot) as Plnumnot,Min(Plalfnot) as Plalfnot,Min(Pldatnot) as Pldatnot,; 
 Min(Pldatini) as Pldatini,Max(Pldatfin) as Pldatfin,Min(Pl__Anno) as Pl__Anno,Min(Imponi) as Imponi,; 
 Min(Flescl) as Flescl,Plcodval , Min(Ancodval) as Ancodval,Min(Vadectot) as Vadectot,; 
 Min(Iva) as Iva, Min(Enasarco) as Enasarco, Min(ENASARCOAS) as ENASARCOAS, Min(Ritenuta) as Ritenuta, Min(Nettocor) as Nettocor,Plcaoval,; 
 Min(Afflintr) as Afflintr,Min(Ancodiva) as Ancodiva; 
 From TEMP group by Plcodage,Plcodval,Plcaoval order by Plcodage,Plcodval into cursor Temp
          endif
        else
          * --- Ristampa Note Provvigioni.
          vq_exec("query\GSVE1SNP.VQR",this,"TEMP")
          * --- Verifico se all'interno del cursore TEMP sono presenti provvigioni con il cambio
          *     non valorizzato.
          Select Plnumreg,Pl__Anno,Pldatreg,Plcodage from Temp into cursor Errore where Nvl(plcaoval,0)=0
          if Reccount ("Errore")>0
            if ah_YesNo("Stampo situazione messaggi di errore?")
              Select * from Errore into cursor __TMP__
              CP_CHPRN("QUERY\GSVE_ERR.FRX", " ", this)
            endif
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            i_retcode = 'stop'
            return
          else
            * --- Raggruppo per Plnumnot,Plalfnot,Pldatnot,Plcodval,Plcaoval
            *     Effettuo il raggruppamento per il cambio nel caso di valuta ExtraEmu
            Select Min(Agdesage) as Agdesage, Min(Agindage) as Agindage, Min(Agcapage) as Agcapage, Min(Agcitage) as Agcitage,; 
 Min(Agproage) as Agproage, Min(Agfisage) as Agfisage, Min(Anpariva) as Anpariva, Min(Agcodfor) as Agcodfor,; 
 Sum(Pltotpro) as Pltotpro,Min(Plcodage) as Plcodage,Min(Agritirp) as Agritirp,Min(Agperimp) as Agperimp,; 
 Min(Agflazie) as Agflazie, Plnumnot,Plalfnot,Pldatnot,; 
 Min(Pldatini) as Pldatini,Max(Pldatfin) as Pldatfin,Min(Pl__Anno) as Pl__Anno,Min(Imponi) as Imponi,; 
 Min(Flescl) as Flescl,Plcodval , Min(Ancodval) as Ancodval,Min(Vadectot) as Vadectot,; 
 Min(Plvalnot) as Plvalnot,Min(Plimpnot) as Plimpnot,Plcaoval,Min(Afflintr) as Afflintr,Min(Ancodiva) as Ancodiva; 
 From TEMP group by Plnumnot,Plalfnot,Pldatnot,Plcodval,Plcaoval ; 
 order by Plnumnot,Plalfnot,Pldatnot,Plcodval,Plcaoval into cursor Temp
          endif
        endif
    endcase
    * --- Effettuo due tipi di raggruppamento diversi in base al tipo di stampa scelta.Se Ristampa
    *     effettuo un raggruppamento per Numero Alfa Data e data mentre nell'altro caso raggruppo
    *     per codice agente.
    do case
      case this.oParentObject.w_Stampa="R" Or (this.oParentObject.w_Stampa=" " And this.w_Tipo="N")
        Select TEMP
        =WrCursor("TEMP")
        Go Top
        Scan
        this.w_CODVAL = NVL(TEMP.ANCODVAL,g_perval)
        this.w_TOTPROV = Temp.Pltotpro
        this.w_CAOVAL = 0
        * --- Read from VALUTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VALUTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VADECTOT,VACAOVAL"+;
            " from "+i_cTable+" VALUTE where ";
                +"VACODVAL = "+cp_ToStrODBC(this.w_CODVAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VADECTOT,VACAOVAL;
            from (i_cTable) where;
                VACODVAL = this.w_CODVAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
          this.w_CAOVAL = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        Select TEMP
        * --- Verifico se la valuta del fornitore collegato all'agente � diversa da quella
        * --- presente nelle provvigioni liquidate.
        if this.w_CODVAL<>TEMP.PLCODVAL
          * --- Valuta ExtraEmu
          if this.w_CAOVAL=0
            * --- Se Extra Euro legge l'ultimo Cambio
            this.w_CAOVAL = GETCAM(this.w_CODVAL, this.oParentObject.w_DATNOTA, 0)
          endif
          * --- Calcolo il totale della provvigione.Utilizzo la funzione VAL2MON
          this.w_TOTPROV = VAL2MON(Temp.Pltotpro,Temp.Plcaoval,this.w_Caoval,this.oParentObject.w_Datnota,this.w_Codval)
          Select TEMP
        endif
        this.w_TOTPROV = cp_ROUND(this.w_Totprov,this.w_Dectot)
        Replace VADECTOT with this.w_DECTOT
        Replace ANCODVAL with this.w_CODVAL
        Replace PLTOTPRO with this.w_TOTPROV
        Endscan
        * --- Il risultato del cursore TEMP lo passo nel nuovo cursore Agente
        Select Min(Agdesage) as Agdesage, Min(Agindage) as Agindage, Min(Agcapage) as Agcapage, Min(Agcitage) as Agcitage,; 
 Min(Agproage) as Agproage, Min(Agfisage) as Agfisage, Min(Anpariva) as Anpariva, Min(Agcodfor) as Agcodfor,; 
 Sum(Pltotpro) as Pltotpro,Plcodage,Min(Agritirp) as Agritirp,Min(Agperimp) as Agperimp,; 
 Min(Agflazie) as Agflazie, Min(Plnumnot) as Plnumnot,Min(Plalfnot) as Plalfnot,Min(Pldatnot) as Pldatnot,; 
 Min(Pldatini) as Pldatini,Max(Pldatfin) as Pldatfin,Min(Pl__Anno) as Pl__Anno,Min(Imponi) as Imponi,; 
 Min(Flescl) as Flescl,Min(Plcodval) as Plcodval,Ancodval,Min(Vadectot) as Vadectot,; 
 Min(Iva) as Iva, Min(Enasarco) as Enasarco, Min(ENASARCOAS) as ENASARCOAS, Min(Ritenuta) as Ritenuta, Min(Nettocor) as Nettocor,Min(Afflintr) as Afflintr,Min(Ancodiva) as Ancodiva, space(1) as massimale; 
 From TEMP group by Plcodage,Ancodval order by Plcodage,Ancodval into cursor Agente
      case this.oParentObject.w_Stampa="F" or (this.oParentObject.w_Stampa=" " and this.w_Tipo="S")
        Select TEMP
        =WrCursor("TEMP")
        Go Top
        Scan
        this.w_CODVAL = TEMP.PLVALNOT
        this.w_Dectot = TEMP.VADECTOT
        this.w_TOTPROV = Temp.Pltotpro
        this.w_CAOVAL = 0
        * --- Read from VALUTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VALUTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VACAOVAL"+;
            " from "+i_cTable+" VALUTE where ";
                +"VACODVAL = "+cp_ToStrODBC(this.w_CODVAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VACAOVAL;
            from (i_cTable) where;
                VACODVAL = this.w_CODVAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CAOVAL = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        Select TEMP
        * --- Converte alla Valuta dell'Agente
        if this.w_CAOVAL=0
          * --- Se Extra Euro legge l'ultimo Cambio
          this.w_CAOVAL = GETCAM(this.w_CODVAL, this.oParentObject.w_DATNOTA, 0)
        endif
        * --- Verifico se la valuta del fornitore collegato all'agente � diversa da quella
        * --- presente nelle provvigioni liquidate.
        if this.w_CODVAL<>TEMP.PLCODVAL
          this.w_TOTPROV = VAL2MON(Temp.Pltotpro,Temp.Plcaoval,this.w_Caoval,this.oParentObject.w_Datnota,this.w_Codval)
          Select TEMP
        endif
        this.w_TOTPROV = cp_ROUND(this.w_Totprov,this.w_Dectot)
        Replace PLTOTPRO with this.w_TOTPROV
        Endscan
        * --- Verifico se lancio la ristampa note provvigioni oppure la stampa Fattura
        if this.oParentObject.w_Stampa="F"
          * --- Nel caso di stampa Fattura deve effettuare due ragruppamenti:
          *     1 - Plnumnot,Plalfnot,Pldatnot
          *     2 - Plcodage,Plvalnot
          * --- 1 -Raggruppamento
          Select Min(Agdesage) as Agdesage, Min(Agindage) as Agindage, Min(Agcapage) as Agcapage, Min(Agcitage) as Agcitage,; 
 Min(Agproage) as Agproage, Min(Agfisage) as Agfisage, Min(Anpariva) as Anpariva, Min(Agcodfor) as Agcodfor,; 
 Sum(Pltotpro) as Pltotpro,Min(Plcodage) as Plcodage,Min(Agritirp) as Agritirp,Min(Agperimp) as Agperimp,; 
 Min(Agflazie) as Agflazie, Plnumnot,Plalfnot,Pldatnot,; 
 Min(Pldatini) as Pldatini,Max(Pldatfin) as Pldatfin,Min(Pl__Anno) as Pl__Anno,Sum(Imponi) as Imponi,; 
 Min(Flescl) as Flescl,Plcodval , Min(Ancodval) as Ancodval,Min(Vadectot) as Vadectot,; 
 Min(Plvalnot) as Plvalnot,Min(Plimpnot) as Plimpnot,Min(Afflintr) as Afflintr,Min(Ancodiva) as Ancodiva; 
 From TEMP group by Plnumnot,Plalfnot,Pldatnot,Plcodval ; 
 order by Plnumnot,Plalfnot,Pldatnot,Plcodval into cursor Appoggio
          * --- 2 -Raggruppamento
          Select Min(Agdesage) as Agdesage, Min(Agindage) as Agindage, Min(Agcapage) as Agcapage, Min(Agcitage) as Agcitage,; 
 Min(Agproage) as Agproage, Min(Agfisage) as Agfisage, Min(Anpariva) as Anpariva, Min(Agcodfor) as Agcodfor,; 
 Sum(Pltotpro) as Pltotpro,Min(Plcodage) as Plcodage,Min(Agritirp) as Agritirp,Min(Agperimp) as Agperimp,; 
 Min(Agflazie) as Agflazie,Min(Pldatini) as Pldatini,Max(Pldatfin) as Pldatfin,; 
 Min(Pl__Anno) as Pl__Anno,Sum(Imponi) as Imponi,Min(Flescl) as Flescl,Min(Plcodval) as Plcodval,; 
 Min(Ancodval) as Ancodval,Min(Vadectot) as Vadectot, Plvalnot ,Sum (Plimpnot) as Plimpnot,Min(Afflintr) as Afflintr,Min(Ancodiva) as Ancodiva," " as massimale; 
 From Appoggio group by Plcodage,Plvalnot order by Plcodage,Plvalnot into cursor Agente
        else
          Select Min(Agdesage) as Agdesage, Min(Agindage) as Agindage, Min(Agcapage) as Agcapage, Min(Agcitage) as Agcitage,; 
 Min(Agproage) as Agproage, Min(Agfisage) as Agfisage, Min(Anpariva) as Anpariva, Min(Agcodfor) as Agcodfor,; 
 Sum(Pltotpro) as Pltotpro,Min(Plcodage) as Plcodage,Min(Agritirp) as Agritirp,Min(Agperimp) as Agperimp,; 
 Min(Agflazie) as Agflazie, Plnumnot, Plalfnot, Pldatnot,; 
 Min(Pldatini) as Pldatini,Max(Pldatfin) as Pldatfin,Min(Pl__Anno) as Pl__Anno,Sum(Imponi) as Imponi,; 
 Min(Flescl) as Flescl,Min(Plcodval) as Plcodval,Min(Ancodval) as Ancodval,Min(Vadectot) as Vadectot,; 
 Min(Plvalnot) as Plvalnot,Min(Plimpnot) as Plimpnot,Min(Afflintr) as Afflintr,Min(Ancodiva) as Ancodiva, " " as massimale; 
 From TEMP group by Plnumnot,Plalfnot,Pldatnot order by Plnumnot,Plalfnot,Pldatnot into cursor Agente
        endif
    endcase
    * --- Leggo la percentuale da applicare ed il codice IVA
    * --- Read from PAR_PROV
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_PROV_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROV_idx,2],.t.,this.PAR_PROV_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PPPREAGE,PPCODIVA,PPPREAZI,PPMAXESC,PPMAXNES,PPSOKAGE"+;
        " from "+i_cTable+" PAR_PROV where ";
            +"PPCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PPPREAGE,PPCODIVA,PPPREAZI,PPMAXESC,PPMAXNES,PPSOKAGE;
        from (i_cTable) where;
            PPCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PERPREAG = NVL(cp_ToDate(_read_.PPPREAGE),cp_NullValue(_read_.PPPREAGE))
      this.w_CODIVA = NVL(cp_ToDate(_read_.PPCODIVA),cp_NullValue(_read_.PPCODIVA))
      this.w_PERPREAZ = NVL(cp_ToDate(_read_.PPPREAZI),cp_NullValue(_read_.PPPREAZI))
      this.w_MAXESC = NVL(cp_ToDate(_read_.PPMAXESC),cp_NullValue(_read_.PPMAXESC))
      this.w_MAXNES = NVL(cp_ToDate(_read_.PPMAXNES),cp_NullValue(_read_.PPMAXNES))
      this.w_SOKAGE = NVL(cp_ToDate(_read_.PPSOKAGE),cp_NullValue(_read_.PPSOKAGE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from VOCIIVA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VOCIIVA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "IVPERIVA"+;
        " from "+i_cTable+" VOCIIVA where ";
            +"IVCODIVA = "+cp_ToStrODBC(this.w_CODIVA);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        IVPERIVA;
        from (i_cTable) where;
            IVCODIVA = this.w_CODIVA;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PERIVA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Controllo che tipo di stampa ho selezionato.Se scelgo Ristampa non devo effettuare
    * --- nessun controllo sul massimo imponibile.
    if  this.oParentObject.w_Stampa="R" Or (this.oParentObject.w_Stampa=" " And this.w_Tipo="N")
      Select Agente
      =WrCursor("Agente")
      Go Top
      this.w_AGENTE = "XXZYY"
      Scan for this.w_Agente<>TEMP.PLCODAGE
      this.w_AGENTE = Agente.PLCODAGE
      Select Agente
      this.w_TOTPROV = Agente.PLTOTPRO
      this.w_FLESCL = Agente.FLESCL
      * --- Controllo se l'agente � plurimandatario oppure monomandatario
      if this.w_FLESCL="M"
        this.w_MONOMAX = this.w_MAXESC
      else
        this.w_MONOMAX = this.w_MAXNES
      endif
      this.w_TOTPERC = this.w_PERPREAG + this.w_PERPREAZ
      this.w_MAXCONTR = INT((this.w_MONOMAX * this.w_TOTPERC)/100)
      * --- Dopo aver letto il massimo imponibile calcolo il massimo contributo che un agente
      *     pu� avere come contributo enasarco
      this.w_TOTIMPON = 0
      this.w_ANNO = VAL(NVL(Agente.PL__ANNO,0))
      this.w_CODVAL = Agente.Ancodval
      this.w_IVA = 0
      this.w_ENASARCO = 0
      this.w_ENASARCOAS = 0
      this.w_RITENUTA = 0
      this.w_IMPRITE = 0
      this.w_NETTOCOR = 0
      * --- Calcolo il cambio per convertire gli importi nella valuta dell'agente
      this.w_CAOVAL = 0
      * --- Read from VALUTE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VALUTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "VACAOVAL,VADECTOT"+;
          " from "+i_cTable+" VALUTE where ";
              +"VACODVAL = "+cp_ToStrODBC(this.w_CODVAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          VACAOVAL,VADECTOT;
          from (i_cTable) where;
              VACODVAL = this.w_CODVAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CAOVAL = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
        this.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_CAOVAL=0
        * --- Se Extra Euro legge l'ultimo Cambio
        this.w_CAOVAL = GETCAM(this.w_CODVAL, this.oParentObject.w_DATNOTA, 0)
      endif
      vq_exec("query\GSVE1MVT.VQR",this,"TEMP1")
      Select TEMP1
      Go Top
      Scan
      this.w_IMPONI = TEMP1.VTIMPENA
      this.w_CONPRE = TEMP1.VTCONPRE
      if this.w_CODVAL<>Temp1.Vtcodval
        this.w_IMPONI = VAL2MON(Temp1.Vtimpena,Temp1.Vtcaoval,this.w_Caoval,this.oParentObject.w_Datnota,this.w_Codval)
        this.w_CONPRE = VAL2MON(Temp1.Vtconpre,Temp1.Vtcaoval,this.w_Caoval,this.oParentObject.w_Datnota,this.w_Codval)
        Select TEMP1
      endif
      this.w_IMPONI = cp_ROUND(this.w_Imponi,this.w_Dectot)
      * --- Se il record deriva dalla tabella manutenzione provvigione liquidate devo applicare
      *     le percentuali  specificate nella tabella provvigioni
      this.w_CONPRE = cp_ROUND(this.w_CONPRE * (this.w_TOTPERC) / 100, this.w_DECTOT)
      * --- Sommo l'importo presente nei versamenti trimestrali con le note provvigioni gi� stampate
      * --- in definitiva ma ancora da versare.
      this.w_TOTIMPON = this.w_TOTIMPON + this.w_IMPONI
      this.w_TOTCONPRE = this.w_TOTCONPRE+this.w_CONPRE
      endscan
      this.w_TOTALE = this.w_TOTIMPON+this.w_TOTPROV
      Select Agente
      this.w_MAXIMPON = VAL2MON(this.w_MONOMAX,g_Caoval,this.w_Caoval,this.oParentObject.w_Datnota,this.w_Codval)
      this.w_MAXIMPON = cp_ROUND(this.w_MAXIMPON,this.w_Dectot)
      this.w_AGFLAZIE = NVL(AGFLAZIE," ")
      * --- Controllo se ho superato il massimo imponibile del fondo ENASARCO
      if this.w_TOTALE>=this.w_MAXIMPON AND this.w_AGFLAZIE<>"S"
        this.w_TOTMAXIM = (this.w_MAXIMPON-this.w_TOTIMPON)
        this.w_TOTMAXIM = IIF(this.w_TOTMAXIM<0,0,this.w_TOTMAXIM)
        Replace IMPONI with this.w_TOTMAXIM
      else
        Replace IMPONI with Agente.PLTOTPRO
      endif
      * --- Calcolo anche l 'IVA, il contributo Enasarco a carico dell'agente, le ritenute d'acconto, ed il netto corrisposto.
      *     Effettuo  questi calcoli all'interno del batch perch� mi serve il valore netto corrisposto della provvigione.
      *     Questo campo deve essere inserito nella tabella Provvigioni liquidate.
      *     Se il fornitore � soggetto INTRA non devo calcolare l'IVA.La stessa cosa
      *     devo effettuarla nel caso in cui il fornitore di riferimento ha specificato un
      *     codice IVA non imponibile.
      if Nvl(Agente.Afflintr,"")="S" or Not Empty(Nvl(Agente.Ancodiva,""))
        this.w_IVA = 0
      else
        this.w_IVA = ((Agente.Pltotpro*this.w_Periva)/100)
        this.w_IVA = IVAROUND(this.w_IVA,Vadectot,1,Ancodval)
      endif
      this.w_RITENUTA = cp_ROUND(((pltotpro*agperimp)/100),vadectot)
      this.w_ENASARCO = IIF(AGFLAZIE="I",cp_ROUND(((Imponi*this.w_Perpreag)/100),Vadectot),0)
      this.w_ENASARCOAS = IIF(AGFLAZIE="S",cp_ROUND(((Imponi*this.w_Sokage)/100),Vadectot),0)
      this.w_IMPRITE = cp_ROUND(((this.w_ritenuta*agritirp)/100),vadectot)
      this.w_NETTOCOR = (Pltotpro+this.w_IVA)-(this.w_ENASARCO+ this.w_ENASARCOAS+this.w_IMPRITE)
      Replace IVA with this.w_IVA
      Replace Enasarco with this.w_Enasarco
      Replace ENASARCOAS with this.w_ENASARCOAS
      Replace Ritenuta with this.w_IMPRITE
      Replace Nettocor with this.w_Nettocor
      Replace Massimale with this.w_Massimale
      Endscan
    endif
    L_SCELTA=this.oParentObject.w_SCELTA
    L_PERIVA=this.w_PERIVA
    L_DATA1=this.oParentObject.w_DATA1
    L_AGEINI=this.oParentObject.w_AGEINI
    L_AGEFIN=this.oParentObject.w_AGEFIN
    L_DATA2=this.oParentObject.w_DATA2
    L_ALFDOC=this.oParentObject.w_ALFDOC
    L_VALUTA=this.oParentObject.w_VALUTA
    L_DATNOTA=this.oParentObject.w_DATNOTA
    L_CODIVA=this.w_CODIVA
    L_PERPREAG=this.w_PERPREAG
    L_SOKAGE=this.w_SOKAGE
    * --- Passo tutto al cursore di stampa __TMP__
    Select * From Agente into cursor __TMP__
    * --- Chiudo i cursori
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Lancio i vari report di stampa.
    do case
      case this.oParentObject.w_STAMPA="F"
        CP_CHPRN("QUERY\GSVE_PLF.FRX", " ", this)
      case this.oParentObject.w_STAMPA="R"
        CP_CHPRN("QUERY\GSVE_PLR.FRX", " ", this)
      case this.oParentObject.w_STAMPA=" "
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
    if this.oParentObject.w_AGGPROS="S"
      * --- Stampa Prospetto Allegato
      vx_exec(""+alltrim("QUERY\GSVE12SPL.VQR")+", "+alltrim("query\gsve1spl.frx" )+"",this)
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorno il progressivo solo se stampa definitiva
    if this.oParentObject.w_scelta="D"
      * --- Lancio il report di stampa in definitiva
      select __TMP__
      GO TOP
      SCAN
      * --- Try
      local bErr_0380E4D8
      bErr_0380E4D8=bTrsErr
      this.Try_0380E4D8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
      endif
      bTrsErr=bTrsErr or bErr_0380E4D8
      * --- End
      ENDSCAN
    endif
    if this.w_TIPO="N"
      * --- Stampa in 'Definitiva' oppure Simulata.
      CP_CHPRN("QUERY\GSVE_SNP.FRX", " ", this)
    else
      * --- Ristampa Note Provvigioni.
      CP_CHPRN("QUERY\GSVE1SNP.FRX", " ", this)
    endif
  endproc
  proc Try_0380E4D8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.oParentObject.w_NUMERO = 0
    * --- Utilizzo l'archivio degli AutoNumber, non associato a nessun archivio
    i_Conn=i_TableProp[this.PRO_LIQU_IDX, 3]
    cp_NextTableProg(this.oParentObject, i_Conn, "PRLIN", "i_codazi,w_ANNDOC,w_ALFDOC,w_NUMERO")
    * --- Aggiorno le provvigioni liquidate
    select __TMP__
    this.w_AGENTE = NVL(__TMP__.PLCODAGE,SPACE(5))
    this.w_Dectot = NVL(__TMP__.Vadectot,0)
    this.w_IMPOSTA = cp_ROUND(NVL(__TMP__.Imponi,0),this.w_Dectot)
    this.w_NOTAPROV = cp_ROUND(NVL(__TMP__.Nettocor,0),this.w_Dectot)
    this.w_VALUNOTA = __TMP__.ANCODVAL
    this.w_VALNUM = L_Valuta
    * --- Select from gsve_sn2
    do vq_exec with 'gsve_sn2',this,'_Curs_gsve_sn2','',.f.,.t.
    if used('_Curs_gsve_sn2')
      select _Curs_gsve_sn2
      locate for 1=1
      do while not(eof())
      * --- Recupero le note provvigioni associate all'agente
      * --- Write into PRO_LIQU
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PRO_LIQU_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PRO_LIQU_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PRO_LIQU_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PLNUMNOT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NUMERO),'PRO_LIQU','PLNUMNOT');
        +",PLALFNOT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ALFDOC),'PRO_LIQU','PLALFNOT');
        +",PLDATNOT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATNOTA),'PRO_LIQU','PLDATNOT');
        +",PLCONTRI ="+cp_NullLink(cp_ToStrODBC(this.w_IMPOSTA),'PRO_LIQU','PLCONTRI');
        +",PLIMPNOT ="+cp_NullLink(cp_ToStrODBC(this.w_NOTAPROV),'PRO_LIQU','PLIMPNOT');
        +",PLVALNOT ="+cp_NullLink(cp_ToStrODBC(this.w_VALUNOTA),'PRO_LIQU','PLVALNOT');
            +i_ccchkf ;
        +" where ";
            +"PLNUMREG = "+cp_ToStrODBC(_Curs_gsve_sn2.PLNUMREG);
            +" and PL__ANNO = "+cp_ToStrODBC(_Curs_gsve_sn2.PL__ANNO);
               )
      else
        update (i_cTable) set;
            PLNUMNOT = this.oParentObject.w_NUMERO;
            ,PLALFNOT = this.oParentObject.w_ALFDOC;
            ,PLDATNOT = this.oParentObject.w_DATNOTA;
            ,PLCONTRI = this.w_IMPOSTA;
            ,PLIMPNOT = this.w_NOTAPROV;
            ,PLVALNOT = this.w_VALUNOTA;
            &i_ccchkf. ;
         where;
            PLNUMREG = _Curs_gsve_sn2.PLNUMREG;
            and PL__ANNO = _Curs_gsve_sn2.PL__ANNO;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_gsve_sn2
        continue
      enddo
      use
    endif
    this.oParentObject.w_NUMERO = this.oParentObject.w_NUMERO+1
    select __TMP__
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiusura Cursori
    if used("TEMP")
      select TEMP
      use
    endif
    if used("TEMP1")
      select TEMP1
      use
    endif
    if used("Agente")
      select Agente
      use
    endif
    if used("Errore")
      select Errore
      use
    endif
    if used("Appoggio")
      select Appoggio
      use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='PAR_PROV'
    this.cWorkTables[2]='VOCIIVA'
    this.cWorkTables[3]='PRO_NUME'
    this.cWorkTables[4]='PRO_LIQU'
    this.cWorkTables[5]='AGENTI'
    this.cWorkTables[6]='VALUTE'
    return(this.OpenAllTables(6))

  proc CloseCursors()
    if used('_Curs_gsve_sn2')
      use in _Curs_gsve_sn2
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
