* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_ssc                                                        *
*              Stampa sovraccolli                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_118]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-14                                                      *
* Last revis.: 2011-09-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsve_ssc",oParentObject))

* --- Class definition
define class tgsve_ssc as StdForm
  Top    = 10
  Left   = 49

  * --- Standard Properties
  Width  = 537
  Height = 278
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-09-02"
  HelpContextID=90845801
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=27

  * --- Constant Properties
  _IDX = 0
  TIP_DOCU_IDX = 0
  CONTI_IDX = 0
  cPrg = "gsve_ssc"
  cComment = "Stampa sovraccolli"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_VENACQ = space(1)
  w_CATDO3 = space(2)
  o_CATDO3 = space(2)
  w_CATDO1 = space(2)
  o_CATDO1 = space(2)
  w_CATDO2 = space(2)
  o_CATDO2 = space(2)
  w_CATDO2 = space(2)
  w_CATEGO = space(2)
  w_TIPO = space(1)
  o_TIPO = space(1)
  w_TIPODOC = space(5)
  w_DATINI = ctod('  /  /  ')
  o_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_CLIFOR = space(1)
  w_NUMINI = 0
  w_ALFINI = space(10)
  w_NUMFIN = 0
  w_ALFFIN = space(10)
  w_CLIENTE = space(15)
  w_CLIENTE = space(15)
  w_NUMCOP = 0
  w_DESCLI = space(40)
  w_DESCRI = space(35)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_FLVEAC = space(1)
  w_ONUME = 0
  w_TIPSTA = 0
  w_FLPACK = space(1)
  w_TIPODOC = space(5)
  * --- Area Manuale = Declare Variables
  * --- gsve_ssc
  proc SetCPToolBar()
          doDefault()
          oCpToolBar.b4.enabled=.f.
  endproc
  
  * --- In questa A.M. viene inizializzata la Variabilei Globale del Form che Identifica
  * --- La Categoria Documento in base al Valore del Parametro.
  * --- tipo Documento
  pvenacq = ''
  
  * --- Per gestione sicurezza procedure con parametri
  func getSecuritycode()
    local l_CICLO
    l_CICLO=this.oParentObject
    return(lower('GSVE_SSC'+IIF(Type('l_CICLO')='C',','+l_CICLO,'')))
  EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_sscPag1","gsve_ssc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCATDO3_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='TIP_DOCU'
    this.cWorkTables[2]='CONTI'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSVE_BSO with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_VENACQ=space(1)
      .w_CATDO3=space(2)
      .w_CATDO1=space(2)
      .w_CATDO2=space(2)
      .w_CATDO2=space(2)
      .w_CATEGO=space(2)
      .w_TIPO=space(1)
      .w_TIPODOC=space(5)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_CLIFOR=space(1)
      .w_NUMINI=0
      .w_ALFINI=space(10)
      .w_NUMFIN=0
      .w_ALFFIN=space(10)
      .w_CLIENTE=space(15)
      .w_CLIENTE=space(15)
      .w_NUMCOP=0
      .w_DESCLI=space(40)
      .w_DESCRI=space(35)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_FLVEAC=space(1)
      .w_ONUME=0
      .w_TIPSTA=0
      .w_FLPACK=space(1)
      .w_TIPODOC=space(5)
        .w_VENACQ = this.oParentObject
        .w_CATDO3 = 'XX'
        .w_CATDO1 = 'XX'
        .w_CATDO2 = 'XX'
        .w_CATDO2 = 'XX'
        .w_CATEGO = IIF(.w_VENACQ='V',IIF(g_ACQU='S', IIF(.w_CATDO1='XX', '  ', .w_CATDO1), IIF(.w_CATDO2='IF', 'DI', IIF(.w_CATDO2='DF', 'DT', IIF(.w_CATDO2='XX', '  ', .w_CATDO2)))), IIF(.w_CATDO3='XX', '  ', .w_CATDO3))
        .w_TIPO = IIF(.w_VENACQ='V','C','F')
        .w_TIPODOC = SPACE(5)
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_TIPODOC))
          .link_1_8('Full')
        endif
        .w_DATINI = G_INIESE
        .w_DATFIN = G_FINESE
          .DoRTCalc(11,11,.f.)
        .w_NUMINI = 1
        .w_ALFINI = ''
        .w_NUMFIN = 999999999999999
        .w_ALFFIN = ''
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_CLIENTE))
          .link_1_16('Full')
        endif
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_CLIENTE))
          .link_1_17('Full')
        endif
        .w_NUMCOP = 1
          .DoRTCalc(19,20,.f.)
        .w_OBTEST = IIF(EMPTY(.w_DATINI),i_INIDAT,.w_DATINI)
          .DoRTCalc(22,24,.f.)
        .w_TIPSTA = .w_ONUME
      .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
          .DoRTCalc(26,26,.f.)
        .w_TIPODOC = SPACE(5)
        .DoRTCalc(27,27,.f.)
        if not(empty(.w_TIPODOC))
          .link_1_42('Full')
        endif
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_36.enabled = this.oPgFrm.Page1.oPag.oBtn_1_36.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_37.enabled = this.oPgFrm.Page1.oPag.oBtn_1_37.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
            .w_CATEGO = IIF(.w_VENACQ='V',IIF(g_ACQU='S', IIF(.w_CATDO1='XX', '  ', .w_CATDO1), IIF(.w_CATDO2='IF', 'DI', IIF(.w_CATDO2='DF', 'DT', IIF(.w_CATDO2='XX', '  ', .w_CATDO2)))), IIF(.w_CATDO3='XX', '  ', .w_CATDO3))
        .DoRTCalc(7,7,.t.)
        if .o_CATDO1<>.w_CATDO1.or. .o_CATDO2<>.w_CATDO2.or. .o_CATDO3<>.w_CATDO3
            .w_TIPODOC = SPACE(5)
          .link_1_8('Full')
        endif
        .DoRTCalc(9,15,.t.)
        if .o_TIPO<>.w_TIPO
          .link_1_16('Full')
        endif
        if .o_TIPO<>.w_TIPO
          .link_1_17('Full')
        endif
        .DoRTCalc(18,20,.t.)
        if .o_DATINI<>.w_DATINI
            .w_OBTEST = IIF(EMPTY(.w_DATINI),i_INIDAT,.w_DATINI)
        endif
        .DoRTCalc(22,24,.t.)
            .w_TIPSTA = .w_ONUME
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
        .DoRTCalc(26,26,.t.)
        if .o_CATDO1<>.w_CATDO1.or. .o_CATDO2<>.w_CATDO2.or. .o_CATDO3<>.w_CATDO3
            .w_TIPODOC = SPACE(5)
          .link_1_42('Full')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCATDO1_1_3.enabled = this.oPgFrm.Page1.oPag.oCATDO1_1_3.mCond()
    this.oPgFrm.Page1.oPag.oCATDO2_1_4.enabled = this.oPgFrm.Page1.oPag.oCATDO2_1_4.mCond()
    this.oPgFrm.Page1.oPag.oCATDO2_1_5.enabled = this.oPgFrm.Page1.oPag.oCATDO2_1_5.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_36.enabled = this.oPgFrm.Page1.oPag.oBtn_1_36.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCATDO3_1_2.visible=!this.oPgFrm.Page1.oPag.oCATDO3_1_2.mHide()
    this.oPgFrm.Page1.oPag.oCATDO1_1_3.visible=!this.oPgFrm.Page1.oPag.oCATDO1_1_3.mHide()
    this.oPgFrm.Page1.oPag.oCATDO2_1_4.visible=!this.oPgFrm.Page1.oPag.oCATDO2_1_4.mHide()
    this.oPgFrm.Page1.oPag.oCATDO2_1_5.visible=!this.oPgFrm.Page1.oPag.oCATDO2_1_5.mHide()
    this.oPgFrm.Page1.oPag.oTIPODOC_1_8.visible=!this.oPgFrm.Page1.oPag.oTIPODOC_1_8.mHide()
    this.oPgFrm.Page1.oPag.oCLIENTE_1_16.visible=!this.oPgFrm.Page1.oPag.oCLIENTE_1_16.mHide()
    this.oPgFrm.Page1.oPag.oCLIENTE_1_17.visible=!this.oPgFrm.Page1.oPag.oCLIENTE_1_17.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_23.visible=!this.oPgFrm.Page1.oPag.oStr_1_23.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_40.visible=!this.oPgFrm.Page1.oPag.oStr_1_40.mHide()
    this.oPgFrm.Page1.oPag.oTIPODOC_1_42.visible=!this.oPgFrm.Page1.oPag.oTIPODOC_1_42.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_35.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=TIPODOC
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPODOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_TIPODOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE,TDFLVEAC,TDFLPACK";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_TIPODOC))
          select TDTIPDOC,TDDESDOC,TDFLINTE,TDFLVEAC,TDFLPACK;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPODOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TDDESDOC like "+cp_ToStrODBC(trim(this.w_TIPODOC)+"%");

            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE,TDFLVEAC,TDFLPACK";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TDDESDOC like "+cp_ToStr(trim(this.w_TIPODOC)+"%");

            select TDTIPDOC,TDDESDOC,TDFLINTE,TDFLVEAC,TDFLPACK;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_TIPODOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oTIPODOC_1_8'),i_cWhere,'GSVE_ATD',"Causali documenti",'GSVE_SSC.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE,TDFLVEAC,TDFLPACK";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLINTE,TDFLVEAC,TDFLPACK;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPODOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE,TDFLVEAC,TDFLPACK";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_TIPODOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_TIPODOC)
            select TDTIPDOC,TDDESDOC,TDFLINTE,TDFLVEAC,TDFLPACK;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPODOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESCRI = NVL(_Link_.TDDESDOC,space(35))
      this.w_CLIFOR = NVL(_Link_.TDFLINTE,space(1))
      this.w_FLVEAC = NVL(_Link_.TDFLVEAC,space(1))
      this.w_FLPACK = NVL(_Link_.TDFLPACK,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TIPODOC = space(5)
      endif
      this.w_DESCRI = space(35)
      this.w_CLIFOR = space(1)
      this.w_FLVEAC = space(1)
      this.w_FLPACK = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_TIPODOC) OR (.w_FLVEAC=.w_VENACQ AND .w_FLPACK='S')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Tipo documento inesistente, di tipo non corretto o con flag Packing List non attivo")
        endif
        this.w_TIPODOC = space(5)
        this.w_DESCRI = space(35)
        this.w_CLIFOR = space(1)
        this.w_FLVEAC = space(1)
        this.w_FLPACK = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPODOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLIENTE
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLIENTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CLIENTE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPO;
                     ,'ANCODICE',trim(this.w_CLIENTE))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLIENTE)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CLIENTE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CLIENTE)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPO);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CLIENTE) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCLIENTE_1_16'),i_cWhere,'GSAR_BZC',"Elenco clienti",'GSVE_SCL.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice intestatario inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLIENTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CLIENTE);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPO;
                       ,'ANCODICE',this.w_CLIENTE)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLIENTE = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLI = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CLIENTE = space(15)
      endif
      this.w_DESCLI = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_TIPO='C'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice intestatario inesistente oppure obsoleto")
        endif
        this.w_CLIENTE = space(15)
        this.w_DESCLI = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLIENTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLIENTE
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLIENTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFR',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CLIENTE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPO;
                     ,'ANCODICE',trim(this.w_CLIENTE))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLIENTE)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CLIENTE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CLIENTE)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPO);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CLIENTE) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCLIENTE_1_17'),i_cWhere,'GSAR_AFR',"Elenco fornitori",'GSVE_SFR.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice intestatario inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLIENTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CLIENTE);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPO;
                       ,'ANCODICE',this.w_CLIENTE)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLIENTE = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLI = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CLIENTE = space(15)
      endif
      this.w_DESCLI = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_TIPO='F'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice intestatario inesistente oppure obsoleto")
        endif
        this.w_CLIENTE = space(15)
        this.w_DESCLI = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLIENTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPODOC
  func Link_1_42(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPODOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAC_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_TIPODOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE,TDFLVEAC,TDFLPACK";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_TIPODOC))
          select TDTIPDOC,TDDESDOC,TDFLINTE,TDFLVEAC,TDFLPACK;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPODOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TDDESDOC like "+cp_ToStrODBC(trim(this.w_TIPODOC)+"%");

            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE,TDFLVEAC,TDFLPACK";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TDDESDOC like "+cp_ToStr(trim(this.w_TIPODOC)+"%");

            select TDTIPDOC,TDDESDOC,TDFLINTE,TDFLVEAC,TDFLPACK;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_TIPODOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oTIPODOC_1_42'),i_cWhere,'GSAC_ATD',"Causali documenti",'GSVE_SSC.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE,TDFLVEAC,TDFLPACK";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLINTE,TDFLVEAC,TDFLPACK;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPODOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE,TDFLVEAC,TDFLPACK";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_TIPODOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_TIPODOC)
            select TDTIPDOC,TDDESDOC,TDFLINTE,TDFLVEAC,TDFLPACK;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPODOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESCRI = NVL(_Link_.TDDESDOC,space(35))
      this.w_CLIFOR = NVL(_Link_.TDFLINTE,space(1))
      this.w_FLVEAC = NVL(_Link_.TDFLVEAC,space(1))
      this.w_FLPACK = NVL(_Link_.TDFLPACK,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TIPODOC = space(5)
      endif
      this.w_DESCRI = space(35)
      this.w_CLIFOR = space(1)
      this.w_FLVEAC = space(1)
      this.w_FLPACK = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_TIPODOC) OR (.w_FLVEAC=.w_VENACQ AND .w_FLPACK='S')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Tipo documento inesistente, di tipo non corretto o con flag Packing List non attivo")
        endif
        this.w_TIPODOC = space(5)
        this.w_DESCRI = space(35)
        this.w_CLIFOR = space(1)
        this.w_FLVEAC = space(1)
        this.w_FLPACK = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPODOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCATDO3_1_2.RadioValue()==this.w_CATDO3)
      this.oPgFrm.Page1.oPag.oCATDO3_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCATDO1_1_3.RadioValue()==this.w_CATDO1)
      this.oPgFrm.Page1.oPag.oCATDO1_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCATDO2_1_4.RadioValue()==this.w_CATDO2)
      this.oPgFrm.Page1.oPag.oCATDO2_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCATDO2_1_5.RadioValue()==this.w_CATDO2)
      this.oPgFrm.Page1.oPag.oCATDO2_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPODOC_1_8.value==this.w_TIPODOC)
      this.oPgFrm.Page1.oPag.oTIPODOC_1_8.value=this.w_TIPODOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_9.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_9.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_10.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_10.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMINI_1_12.value==this.w_NUMINI)
      this.oPgFrm.Page1.oPag.oNUMINI_1_12.value=this.w_NUMINI
    endif
    if not(this.oPgFrm.Page1.oPag.oALFINI_1_13.value==this.w_ALFINI)
      this.oPgFrm.Page1.oPag.oALFINI_1_13.value=this.w_ALFINI
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMFIN_1_14.value==this.w_NUMFIN)
      this.oPgFrm.Page1.oPag.oNUMFIN_1_14.value=this.w_NUMFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oALFFIN_1_15.value==this.w_ALFFIN)
      this.oPgFrm.Page1.oPag.oALFFIN_1_15.value=this.w_ALFFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCLIENTE_1_16.value==this.w_CLIENTE)
      this.oPgFrm.Page1.oPag.oCLIENTE_1_16.value=this.w_CLIENTE
    endif
    if not(this.oPgFrm.Page1.oPag.oCLIENTE_1_17.value==this.w_CLIENTE)
      this.oPgFrm.Page1.oPag.oCLIENTE_1_17.value=this.w_CLIENTE
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMCOP_1_18.value==this.w_NUMCOP)
      this.oPgFrm.Page1.oPag.oNUMCOP_1_18.value=this.w_NUMCOP
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLI_1_24.value==this.w_DESCLI)
      this.oPgFrm.Page1.oPag.oDESCLI_1_24.value=this.w_DESCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_27.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_27.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPODOC_1_42.value==this.w_TIPODOC)
      this.oPgFrm.Page1.oPag.oTIPODOC_1_42.value=this.w_TIPODOC
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(EMPTY(.w_TIPODOC) OR (.w_FLVEAC=.w_VENACQ AND .w_FLPACK='S'))  and not(.w_VENACQ='A')  and not(empty(.w_TIPODOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTIPODOC_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Tipo documento inesistente, di tipo non corretto o con flag Packing List non attivo")
          case   ((empty(.w_DATINI)) or not((empty(.w_DATFIN)) OR (.w_DATFIN>=.w_DATINI)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_9.SetFocus()
            i_bnoObbl = !empty(.w_DATINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data finale � minore della data iniziale")
          case   ((empty(.w_DATFIN)) or not((.w_DATFIN>=.w_DATINI) OR  ( empty(.w_DATINI))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_10.SetFocus()
            i_bnoObbl = !empty(.w_DATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data finale � minore della data iniziale")
          case   not((empty(.w_NUMFIN)) OR  (.w_NUMINI<=.w_NUMFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMINI_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero documento iniziale � pi� grande del numero finale")
          case   not((empty(.w_ALFFIN)) OR  (.w_ALFINI<=.w_ALFFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oALFINI_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � pi� grande della serie finale")
          case   not((.w_NUMFIN>=.w_NUMINI) or (empty(.w_NUMFIN)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMFIN_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero documento iniziale � pi� grande del numero finale")
          case   not((.w_ALFFIN>=.w_ALFINI) or (empty(.w_ALFINI)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oALFFIN_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � maggire della serie finale")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_TIPO='C')  and not(.w_TIPO='F')  and not(empty(.w_CLIENTE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCLIENTE_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice intestatario inesistente oppure obsoleto")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_TIPO='F')  and not(.w_TIPO='C')  and not(empty(.w_CLIENTE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCLIENTE_1_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice intestatario inesistente oppure obsoleto")
          case   not(.w_NUMCOP>0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMCOP_1_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un numero copie maggiore di zero")
          case   not(EMPTY(.w_TIPODOC) OR (.w_FLVEAC=.w_VENACQ AND .w_FLPACK='S'))  and not(.w_VENACQ='V')  and not(empty(.w_TIPODOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTIPODOC_1_42.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Tipo documento inesistente, di tipo non corretto o con flag Packing List non attivo")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CATDO3 = this.w_CATDO3
    this.o_CATDO1 = this.w_CATDO1
    this.o_CATDO2 = this.w_CATDO2
    this.o_TIPO = this.w_TIPO
    this.o_DATINI = this.w_DATINI
    return

enddefine

* --- Define pages as container
define class tgsve_sscPag1 as StdContainer
  Width  = 533
  height = 278
  stdWidth  = 533
  stdheight = 278
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oCATDO3_1_2 as StdCombo with uid="AZFEXNVYDI",rtseq=2,rtrep=.f.,left=106,top=15,width=130,height=21;
    , ToolTipText = "Categoria di appartenenza del documento selezionata";
    , HelpContextID = 221293786;
    , cFormVar="w_CATDO3",RowSource=""+"Tutti i documenti,"+"Documenti interni,"+"Doc. di trasporto,"+"Fatture,"+"Note di credito", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCATDO3_1_2.RadioValue()
    return(iif(this.value =1,'XX',;
    iif(this.value =2,'DI',;
    iif(this.value =3,'DT',;
    iif(this.value =4,'FA',;
    iif(this.value =5,'NC',;
    space(2)))))))
  endfunc
  func oCATDO3_1_2.GetRadio()
    this.Parent.oContained.w_CATDO3 = this.RadioValue()
    return .t.
  endfunc

  func oCATDO3_1_2.SetRadio()
    this.Parent.oContained.w_CATDO3=trim(this.Parent.oContained.w_CATDO3)
    this.value = ;
      iif(this.Parent.oContained.w_CATDO3=='XX',1,;
      iif(this.Parent.oContained.w_CATDO3=='DI',2,;
      iif(this.Parent.oContained.w_CATDO3=='DT',3,;
      iif(this.Parent.oContained.w_CATDO3=='FA',4,;
      iif(this.Parent.oContained.w_CATDO3=='NC',5,;
      0)))))
  endfunc

  func oCATDO3_1_2.mHide()
    with this.Parent.oContained
      return (.w_VENACQ='V')
    endwith
  endfunc


  add object oCATDO1_1_3 as StdCombo with uid="QDJUHAEQBR",rtseq=3,rtrep=.f.,left=106,top=15,width=130,height=21;
    , ToolTipText = "Categoria documento di selezione";
    , HelpContextID = 254848218;
    , cFormVar="w_CATDO1",RowSource=""+"Tutti i documenti,"+"Documenti interni,"+"Doc. di trasporto,"+"Fatture,"+"Note di credito,"+"Ricevute fiscali", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCATDO1_1_3.RadioValue()
    return(iif(this.value =1,'XX',;
    iif(this.value =2,'DI',;
    iif(this.value =3,'DT',;
    iif(this.value =4,'FA',;
    iif(this.value =5,'NC',;
    iif(this.value =6,'RF',;
    space(2))))))))
  endfunc
  func oCATDO1_1_3.GetRadio()
    this.Parent.oContained.w_CATDO1 = this.RadioValue()
    return .t.
  endfunc

  func oCATDO1_1_3.SetRadio()
    this.Parent.oContained.w_CATDO1=trim(this.Parent.oContained.w_CATDO1)
    this.value = ;
      iif(this.Parent.oContained.w_CATDO1=='XX',1,;
      iif(this.Parent.oContained.w_CATDO1=='DI',2,;
      iif(this.Parent.oContained.w_CATDO1=='DT',3,;
      iif(this.Parent.oContained.w_CATDO1=='FA',4,;
      iif(this.Parent.oContained.w_CATDO1=='NC',5,;
      iif(this.Parent.oContained.w_CATDO1=='RF',6,;
      0))))))
  endfunc

  func oCATDO1_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_ACQU='S')
    endwith
   endif
  endfunc

  func oCATDO1_1_3.mHide()
    with this.Parent.oContained
      return (g_ACQU<>'S' AND .w_VENACQ='V' OR .w_VENACQ='A')
    endwith
  endfunc


  add object oCATDO2_1_4 as StdCombo with uid="KLSHGSXUVJ",rtseq=4,rtrep=.f.,left=106,top=15,width=130,height=21;
    , ToolTipText = "Categoria documento di selezione";
    , HelpContextID = 238071002;
    , cFormVar="w_CATDO2",RowSource=""+"Tutti i documenti,"+"Documenti interni,"+"Fatture,"+"Note di credito", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCATDO2_1_4.RadioValue()
    return(iif(this.value =1,'XX',;
    iif(this.value =2,'DI',;
    iif(this.value =3,'FA',;
    iif(this.value =4,'NC',;
    space(2))))))
  endfunc
  func oCATDO2_1_4.GetRadio()
    this.Parent.oContained.w_CATDO2 = this.RadioValue()
    return .t.
  endfunc

  func oCATDO2_1_4.SetRadio()
    this.Parent.oContained.w_CATDO2=trim(this.Parent.oContained.w_CATDO2)
    this.value = ;
      iif(this.Parent.oContained.w_CATDO2=='XX',1,;
      iif(this.Parent.oContained.w_CATDO2=='DI',2,;
      iif(this.Parent.oContained.w_CATDO2=='FA',3,;
      iif(this.Parent.oContained.w_CATDO2=='NC',4,;
      0))))
  endfunc

  func oCATDO2_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAlt())
    endwith
   endif
  endfunc

  func oCATDO2_1_4.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc


  add object oCATDO2_1_5 as StdCombo with uid="UPJNZIKQML",rtseq=5,rtrep=.f.,left=106,top=15,width=130,height=21;
    , ToolTipText = "Categoria documento di selezione";
    , HelpContextID = 238071002;
    , cFormVar="w_CATDO2",RowSource=""+"Tutti i documenti,"+"Documenti interni,"+"Doc. di trasporto,"+"Fatture,"+"Note di credito,"+"Ricevute fiscali,"+"DDT a fornitore,"+"Carichi a fornitore", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCATDO2_1_5.RadioValue()
    return(iif(this.value =1,'XX',;
    iif(this.value =2,'DI',;
    iif(this.value =3,'DT',;
    iif(this.value =4,'FA',;
    iif(this.value =5,'NC',;
    iif(this.value =6,'RF',;
    iif(this.value =7,'DF',;
    iif(this.value =8,'IF',;
    space(2))))))))))
  endfunc
  func oCATDO2_1_5.GetRadio()
    this.Parent.oContained.w_CATDO2 = this.RadioValue()
    return .t.
  endfunc

  func oCATDO2_1_5.SetRadio()
    this.Parent.oContained.w_CATDO2=trim(this.Parent.oContained.w_CATDO2)
    this.value = ;
      iif(this.Parent.oContained.w_CATDO2=='XX',1,;
      iif(this.Parent.oContained.w_CATDO2=='DI',2,;
      iif(this.Parent.oContained.w_CATDO2=='DT',3,;
      iif(this.Parent.oContained.w_CATDO2=='FA',4,;
      iif(this.Parent.oContained.w_CATDO2=='NC',5,;
      iif(this.Parent.oContained.w_CATDO2=='RF',6,;
      iif(this.Parent.oContained.w_CATDO2=='DF',7,;
      iif(this.Parent.oContained.w_CATDO2=='IF',8,;
      0))))))))
  endfunc

  func oCATDO2_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!(g_ACQU='S' AND .w_VENACQ='V' OR .w_VENACQ='A' OR ISALT()))
    endwith
   endif
  endfunc

  func oCATDO2_1_5.mHide()
    with this.Parent.oContained
      return (g_ACQU='S' AND .w_VENACQ='V' OR .w_VENACQ='A' OR ISALT())
    endwith
  endfunc

  add object oTIPODOC_1_8 as StdField with uid="QVHWJTDCVW",rtseq=8,rtrep=.f.,;
    cFormVar = "w_TIPODOC", cQueryName = "TIPODOC",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Tipo documento inesistente, di tipo non corretto o con flag Packing List non attivo",;
    ToolTipText = "Tipo documento selezionato",;
    HelpContextID = 237640758,;
   bGlobalFont=.t.,;
    Height=21, Width=60, Left=104, Top=45, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_TIPODOC"

  func oTIPODOC_1_8.mHide()
    with this.Parent.oContained
      return (.w_VENACQ='A')
    endwith
  endfunc

  func oTIPODOC_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIPODOC_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIPODOC_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oTIPODOC_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Causali documenti",'GSVE_SSC.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oTIPODOC_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_TIPODOC
     i_obj.ecpSave()
  endproc

  add object oDATINI_1_9 as StdField with uid="JWHNCRKDRD",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data finale � minore della data iniziale",;
    ToolTipText = "Data documento di inizio selezione",;
    HelpContextID = 121351370,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=104, Top=78

  func oDATINI_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_DATFIN)) OR (.w_DATFIN>=.w_DATINI))
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_10 as StdField with uid="OWEPFQDEHL",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data finale � minore della data iniziale",;
    ToolTipText = "Data documento di fine selezione",;
    HelpContextID = 42904778,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=104, Top=104

  func oDATFIN_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATFIN>=.w_DATINI) OR  ( empty(.w_DATINI)))
    endwith
    return bRes
  endfunc

  add object oNUMINI_1_12 as StdField with uid="BXBQYPGVTU",rtseq=12,rtrep=.f.,;
    cFormVar = "w_NUMINI", cQueryName = "NUMINI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero documento iniziale � pi� grande del numero finale",;
    ToolTipText = "Numero documento di inizio selezione",;
    HelpContextID = 121374762,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=312, Top=78, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oNUMINI_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_NUMFIN)) OR  (.w_NUMINI<=.w_NUMFIN))
    endwith
    return bRes
  endfunc

  add object oALFINI_1_13 as StdField with uid="HVPVSEHHGU",rtseq=13,rtrep=.f.,;
    cFormVar = "w_ALFINI", cQueryName = "ALFINI",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � pi� grande della serie finale",;
    ToolTipText = "Serie documento di inizio selezione",;
    HelpContextID = 121405946,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=442, Top=78, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  func oALFINI_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_ALFFIN)) OR  (.w_ALFINI<=.w_ALFFIN))
    endwith
    return bRes
  endfunc

  add object oNUMFIN_1_14 as StdField with uid="PROBAWFVAK",rtseq=14,rtrep=.f.,;
    cFormVar = "w_NUMFIN", cQueryName = "NUMFIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero documento iniziale � pi� grande del numero finale",;
    ToolTipText = "Numero documento di fine selezione",;
    HelpContextID = 42928170,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=312, Top=104, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oNUMFIN_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_NUMFIN>=.w_NUMINI) or (empty(.w_NUMFIN)))
    endwith
    return bRes
  endfunc

  add object oALFFIN_1_15 as StdField with uid="LHPNDMBVHJ",rtseq=15,rtrep=.f.,;
    cFormVar = "w_ALFFIN", cQueryName = "ALFFIN",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � maggire della serie finale",;
    ToolTipText = "Serie documento di fine selezione",;
    HelpContextID = 42959354,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=442, Top=104, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  func oALFFIN_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_ALFFIN>=.w_ALFINI) or (empty(.w_ALFINI)))
    endwith
    return bRes
  endfunc

  add object oCLIENTE_1_16 as StdField with uid="DNOSNHKUKW",rtseq=16,rtrep=.f.,;
    cFormVar = "w_CLIENTE", cQueryName = "CLIENTE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice intestatario inesistente oppure obsoleto",;
    ToolTipText = "Codice cliente di selezione",;
    HelpContextID = 62893606,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=104, Top=136, cSayPict="p_CLF", cGetPict="p_CLF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPO", oKey_2_1="ANCODICE", oKey_2_2="this.w_CLIENTE"

  func oCLIENTE_1_16.mHide()
    with this.Parent.oContained
      return (.w_TIPO='F')
    endwith
  endfunc

  func oCLIENTE_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLIENTE_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLIENTE_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPO)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCLIENTE_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco clienti",'GSVE_SCL.CONTI_VZM',this.parent.oContained
  endproc
  proc oCLIENTE_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPO
     i_obj.w_ANCODICE=this.parent.oContained.w_CLIENTE
     i_obj.ecpSave()
  endproc

  add object oCLIENTE_1_17 as StdField with uid="NBBYYNYWGJ",rtseq=17,rtrep=.f.,;
    cFormVar = "w_CLIENTE", cQueryName = "CLIENTE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice intestatario inesistente oppure obsoleto",;
    ToolTipText = "Codice fornitore di selezione",;
    HelpContextID = 62893606,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=104, Top=136, cSayPict="p_CLF", cGetPict="p_CLF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_AFR", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPO", oKey_2_1="ANCODICE", oKey_2_2="this.w_CLIENTE"

  func oCLIENTE_1_17.mHide()
    with this.Parent.oContained
      return (.w_TIPO='C')
    endwith
  endfunc

  func oCLIENTE_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLIENTE_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLIENTE_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPO)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCLIENTE_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFR',"Elenco fornitori",'GSVE_SFR.CONTI_VZM',this.parent.oContained
  endproc
  proc oCLIENTE_1_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPO
     i_obj.w_ANCODICE=this.parent.oContained.w_CLIENTE
     i_obj.ecpSave()
  endproc

  add object oNUMCOP_1_18 as StdField with uid="AVKJYHFZEC",rtseq=18,rtrep=.f.,;
    cFormVar = "w_NUMCOP", cQueryName = "NUMCOP",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un numero copie maggiore di zero",;
    ToolTipText = "Numero di copie da stampare per ogni etichetta",;
    HelpContextID = 3278890,;
   bGlobalFont=.t.,;
    Height=21, Width=67, Left=104, Top=166, cSayPict='"99999"', cGetPict='"99999"'

  func oNUMCOP_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_NUMCOP>0)
    endwith
    return bRes
  endfunc

  add object oDESCLI_1_24 as StdField with uid="KLMLYMSVHU",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESCLI", cQueryName = "DESCLI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 123844810,;
   bGlobalFont=.t.,;
    Height=21, Width=287, Left=238, Top=136, InputMask=replicate('X',40)

  add object oDESCRI_1_27 as StdField with uid="RLYFINUUXX",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 117553354,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=168, Top=45, InputMask=replicate('X',35)


  add object oObj_1_35 as cp_outputCombo with uid="ZLERXIVPWT",left=104, top=194, width=421,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 87151846


  add object oBtn_1_36 as StdButton with uid="ZNREPGHWCX",left=426, top=222, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare";
    , HelpContextID = 50943782;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_36.Click()
      with this.Parent.oContained
        do GSVE_BSO with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_36.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OREP))
      endwith
    endif
  endfunc


  add object oBtn_1_37 as StdButton with uid="XYGIHPUFHG",left=477, top=222, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 83528378;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_37.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oTIPODOC_1_42 as StdField with uid="UJQUQPGWQT",rtseq=27,rtrep=.f.,;
    cFormVar = "w_TIPODOC", cQueryName = "TIPODOC",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Tipo documento inesistente, di tipo non corretto o con flag Packing List non attivo",;
    ToolTipText = "Tipo documento selezionato",;
    HelpContextID = 237640758,;
   bGlobalFont=.t.,;
    Height=21, Width=60, Left=104, Top=45, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSAC_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_TIPODOC"

  func oTIPODOC_1_42.mHide()
    with this.Parent.oContained
      return (.w_VENACQ='V')
    endwith
  endfunc

  func oTIPODOC_1_42.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_42('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIPODOC_1_42.ecpDrop(oSource)
    this.Parent.oContained.link_1_42('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIPODOC_1_42.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oTIPODOC_1_42'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAC_ATD',"Causali documenti",'GSVE_SSC.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oTIPODOC_1_42.mZoomOnZoom
    local i_obj
    i_obj=GSAC_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_TIPODOC
     i_obj.ecpSave()
  endproc

  add object oStr_1_19 as StdString with uid="NXSDBFWLJV",Visible=.t., Left=10, Top=78,;
    Alignment=1, Width=92, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="LJKHLIFCHP",Visible=.t., Left=10, Top=104,;
    Alignment=1, Width=92, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="NCIHTWZTBT",Visible=.t., Left=207, Top=78,;
    Alignment=1, Width=102, Height=15,;
    Caption="Da numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="MQFLLNLPCM",Visible=.t., Left=207, Top=104,;
    Alignment=1, Width=102, Height=15,;
    Caption="A numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="MWDWEHTJAR",Visible=.t., Left=10, Top=136,;
    Alignment=1, Width=92, Height=15,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  func oStr_1_23.mHide()
    with this.Parent.oContained
      return (.w_TIPO='F')
    endwith
  endfunc

  add object oStr_1_25 as StdString with uid="HVWOGBOYOF",Visible=.t., Left=10, Top=194,;
    Alignment=1, Width=92, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="XSZYPLRAJY",Visible=.t., Left=-1, Top=45,;
    Alignment=1, Width=103, Height=15,;
    Caption="Tipo documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="LVVBINNLET",Visible=.t., Left=431, Top=79,;
    Alignment=2, Width=12, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="PKVCFMTCZS",Visible=.t., Left=431, Top=105,;
    Alignment=2, Width=12, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="RPAQEFCQPW",Visible=.t., Left=12, Top=166,;
    Alignment=1, Width=90, Height=18,;
    Caption="Numero copie:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="FCAOACONWI",Visible=.t., Left=10, Top=136,;
    Alignment=1, Width=92, Height=18,;
    Caption="Fornitore:"  ;
  , bGlobalFont=.t.

  func oStr_1_40.mHide()
    with this.Parent.oContained
      return (.w_TIPO='C')
    endwith
  endfunc

  add object oStr_1_41 as StdString with uid="OOAUBIWOYB",Visible=.t., Left=11, Top=15,;
    Alignment=1, Width=92, Height=15,;
    Caption="Categoria:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_ssc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
