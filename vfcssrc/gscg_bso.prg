* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bso                                                        *
*              Stampa saldi contabili                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_51]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-12                                                      *
* Last revis.: 2014-02-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bso",oParentObject,m.pTipo)
return(i_retval)

define class tgscg_bso as StdBatch
  * --- Local variables
  pTipo = 0
  w_ESEPREC = space(4)
  w_VALESPR = space(3)
  w_CAMBIO = 0
  w_CAOVAL = 0
  w_DECIMAL = 0
  w_SIMBOLO = space(5)
  w_PAGINE = 0
  * --- WorkFile variables
  ESERCIZI_idx=0
  VALUTE_idx=0
  AZIENDA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa Saldi Contabili (da GSCG_SSO)
    * --- Parametro Utilizzato per differenziare la Stampa Crediti Clienti dalla Stampa Saldi
    * --- La PROCEDURA stampa nella divisa di conto dell'esercizio selezionato
    * --- Se seleziono Riporto saldi- va a leggere i saldi (in linea e fuori linea) nell'esercizio precedente, se i saldi iniziali
    * --- del conto sono zero e il conto NON � economico ( ne COSTO ne RICAVO sezione di Bilancio C R)
    * --- Quste 2 variabili contengono i tassi fissi della divisa esercizio scelto (CAOVAL)  e della divisa dell'esercizio prec (CAMBIO)
    this.w_CAMBIO = 1
    this.w_CAOVAL = 1
    * --- Ordine di Stampa 3 =Descrizione ; 1 =Codice
    if this.pTipo=1
      if this.oParentObject.w_ORDINE="D"
        Ordine=" 3"
      else
        Ordine=" 1"
      endif
    endif
    * --- Leggo il numero di Decimali della Valuta dell'esercizio Scelto
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VASIMVAL"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.oParentObject.w_VALESE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VASIMVAL;
        from (i_cTable) where;
            VACODVAL = this.oParentObject.w_VALESE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_SIMBOLO = NVL(cp_ToDate(_read_.VASIMVAL),cp_NullValue(_read_.VASIMVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_CAOVAL = GETCAM(this.oParentObject.w_VALESE, I_DATSYS)
    this.w_DECIMAL = GETVALUT(this.oParentObject.w_VALESE, "VADECTOT")
    * --- Se Riporto Saldi Es. Precedente calcolo l'esercizo precedente
    if this.oParentObject.w_RIPSALDI="S"
      * --- Solo nel caso di stampa saldi Cli/For; nel caso di stampa crediti/debiti la variabile w_RIPSALDI � sempre ='N'
      this.w_ESEPREC = CALCESPR(i_codazi,this.oParentObject.w_DATAINI,.t.)
      if empty(this.w_ESEPREC)
        ah_ErrorMsg("Non esiste un esercizio precedente a %1",,"", this.oParentObject.w_ESE)
        i_retcode = 'stop'
        return
      endif
      * --- Leggo la valuta dell'esercizio precedente
      * --- Read from ESERCIZI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ESERCIZI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ESVALNAZ"+;
          " from "+i_cTable+" ESERCIZI where ";
              +"ESCODAZI = "+cp_ToStrODBC(i_CODAZI);
              +" and ESCODESE = "+cp_ToStrODBC(this.w_ESEPREC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ESVALNAZ;
          from (i_cTable) where;
              ESCODAZI = i_CODAZI;
              and ESCODESE = this.w_ESEPREC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_VALESPR = NVL(cp_ToDate(_read_.ESVALNAZ),cp_NullValue(_read_.ESVALNAZ))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Se i due Esercizi  hanno valute diverse vado a leggere il Tasso fisso della Valuta dell'esercizio prec.
      if this.oParentObject.w_VALESE<>this.w_VALESPR
        * --- Read from VALUTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VALUTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VASIMVAL"+;
            " from "+i_cTable+" VALUTE where ";
                +"VACODVAL = "+cp_ToStrODBC(this.w_VALESPR);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VASIMVAL;
            from (i_cTable) where;
                VACODVAL = this.w_VALESPR;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SIMBOLO = NVL(cp_ToDate(_read_.VASIMVAL),cp_NullValue(_read_.VASIMVAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_CAMBIO = GETCAM(this.w_VALESPR,this.oParentObject.w_DATAINI)
      endif
      * --- La Query mi recupera anche i saldi dell'esercizio precedente
      vq_exec("query\gscg_bso.vqr",this,"__tmp__")
      * --- Vado a calcolare per ognuno la Sezione di Bilancio
      SELECT MIN(SLCODICE) AS SLCODICE, MIN(SLTIPCON) AS SLTIPCON, MIN(ANDESCRI) AS ANDESCRI, ;
      SUM(SLDARPRO) AS SLDARPRO, SUM(SLAVEPRO) AS SLAVEPRO, SUM(SLDARPER) AS SLDARPER, ;
      SUM(SLAVEPER) AS SLAVEPER, SUM(SLDARINI) AS SLDARINI, SUM(SLAVEINI) AS SLAVEINI, ;
      SUM(SLDARFIN) AS SLDARFIN, SUM(SLAVEFIN) AS SLAVEFIN, SUM(SLDARPRO1) AS SLDARPRO1, ;
      SUM(SLAVEPRO1) AS SLAVEPRO1, SUM(SLDARPER1) AS SLDARPER1, SUM(SLAVEPER1) AS SLAVEPER1, ;
      SUM(SLDARFIN1) AS SLDARFIN1, SUM(SLAVEFIN1) AS SLAVEFIN1, MIN(ANCONSUP) AS ANCONSUP, ;
      CALCSEZ(ANCONSUP) as SEZ_BIL, ;
      SUM((SLDARPRO+SLDARPER+SLDARFIN)-(SLAVEPRO+SLAVEPER+SLAVEFIN))as SAL_ORA,;
      SUM((SLDARPRO1+SLDARPER1+SLDARFIN1)-(SLAVEPRO1+SLAVEPER1+SLAVEFIN1)) as SAL_INI;
      from __tmp__ GROUP BY SLCODICE INTO CURSOR __tmp__ ORDER BY &Ordine
      SELECT * from __tmp__ WHERE SAL_ORA <>0 OR SAL_INI <>0 OR this.oParentObject.w_SALDZERO="S" INTO CURSOR __tmp__
      * --- Variabili da passare al report
      L_ECODICE=this.oParentObject.w_ECODICE
      L_BCODICE=this.oParentObject.w_BCODICE
      L_ESE=this.oParentObject.w_ESE
      L_EMASTRO=this.oParentObject.w_EMASTRO
      L_BMASTRO=this.oParentObject.w_BMASTRO
      L_DECIMI=(this.w_DECIMAL+2)*20
      L_DEC=this.w_DECIMAL
      L_RIPSALDI=this.oParentObject.w_RIPSALDI
      L_SALDZERO=this.oParentObject.w_SALDZERO
      * --- Campi per i Cambi
      L_cambio=this.w_CAMBIO
      L_VALESPR=this.w_VALESPR
      L_CAOVAL=this.w_CAOVAL
      L_VALUTA=this.oParentObject.w_VALESE
      if reccount("__tmp__") = 0 
        ah_ErrorMsg("Per la selezione effettuata non esistono dati da elaborare",,"")
        i_retcode = 'stop'
        return
      else
        * --- Lancia la Stampa
        if this.oParentObject.w_STAMPA="S"
          * --- Sintetica
          CP_CHPRN("QUERY\GSCG_BSO.FRX", " ", this)
        else
          * --- Dettagliata
          CP_CHPRN("QUERY\GSCG1BSO.FRX", " ", this)
        endif
      endif
    else
      * --- In questo caso i saldi Iniziali vengono completamente ignorati dalla Stampa
      * --- Variabili da passare al report
      L_ECODICE=this.oParentObject.w_ECODICE
      L_BCODICE=this.oParentObject.w_BCODICE
      L_ESE=this.oParentObject.w_ESE
      L_EMASTRO=this.oParentObject.w_EMASTRO
      L_BMASTRO=this.oParentObject.w_BMASTRO
      L_DECIMI=(this.w_DECIMAL+2)*20
      L_VALUTA=this.w_SIMBOLO
      L_RIPSALDI=this.oParentObject.w_RIPSALDI
      if this.pTipo=1
        L_SALDZERO=this.oParentObject.w_SALDZERO
      endif
      if this.pTipo=0
        * --- PER NUMERAZIONE PAGINE IN TESTATA
        * --- Read from AZIENDA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.AZIENDA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "AZINDAZI,AZLOCAZI,AZCAPAZI,AZPROAZI,AZPIVAZI,AZCOFAZI"+;
            " from "+i_cTable+" AZIENDA where ";
                +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            AZINDAZI,AZLOCAZI,AZCAPAZI,AZPROAZI,AZPIVAZI,AZCOFAZI;
            from (i_cTable) where;
                AZCODAZI = i_CODAZI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          w_INDAZI = NVL(cp_ToDate(_read_.AZINDAZI),cp_NullValue(_read_.AZINDAZI))
          w_LOCAZI = NVL(cp_ToDate(_read_.AZLOCAZI),cp_NullValue(_read_.AZLOCAZI))
          w_CAPAZI = NVL(cp_ToDate(_read_.AZCAPAZI),cp_NullValue(_read_.AZCAPAZI))
          w_PROAZI = NVL(cp_ToDate(_read_.AZPROAZI),cp_NullValue(_read_.AZPROAZI))
          w_PIVAZI = NVL(cp_ToDate(_read_.AZPIVAZI),cp_NullValue(_read_.AZPIVAZI))
          w_COFAZI = NVL(cp_ToDate(_read_.AZCOFAZI),cp_NullValue(_read_.AZCOFAZI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        L_PAGINE=0
        L_PRPALI=this.oParentObject.w_PRPALI
        L_INTLIN=this.oParentObject.w_INTLIN
        L_PREFLI=this.oParentObject.w_PREFLI
        L_INDAZI=w_INDAZI
        L_LOCAZI=w_LOCAZI
        L_CAPAZI=w_CAPAZI
        L_PROAZI=w_PROAZI
        L_COFAZI=w_COFAZI
        L_PIVAZI=w_PIVAZI
      endif
      * --- Lancia la Stampa
      vq_exec("query\gscg6bso.vqr",this,"__tmp__")
      if this.pTipo=1
        SELECT * FROM __tmp__ INTO CURSOR __tmp__ ORDER BY &Ordine
      else
        SELECT * FROM __tmp__ INTO CURSOR __tmp__ 
      endif
      if this.pTipo=1
        SELECT * from __tmp__ WHERE (SLDARPRO+SLDARPER+SLDARFIN)-(SLAVEPRO+SLAVEPER+SLAVEFIN) <>0 OR this.oParentObject.w_SALDZERO="S" INTO CURSOR __tmp__
      else
        SELECT * from __tmp__ WHERE (SLDARPRO+SLDARPER+SLDARFIN)-(SLAVEPRO+SLAVEPER+SLAVEFIN) <>0 INTO CURSOR __tmp__
      endif
      if reccount("__tmp__") = 0
        ah_ErrorMsg("Per la selezione effettuata non esistono dati da elaborare",,"")
        i_retcode = 'stop'
        return
      else
        if this.oParentObject.w_STAMPA="S"
          if this.pTipo=1
            * --- Sintetica
            CP_CHPRN("QUERY\GSCG6BSO.FRX", " ", this)
          else
            * --- Lancio la Stampa Crediti Clienti
            CP_CHPRN("QUERY\GSCG2BSO.FRX", " ", this)
            this.w_PAGINE = L_PAGINE+this.oParentObject.w_PRPALI
            * --- Aggiorno il progressivo pagine solo se Attivo il check intestazione
            if this.oParentObject.w_INTLIN="S"
              if ah_YesNo("Aggiorno il progressivo di pagina nei dati azienda?")
                * --- Try
                local bErr_03F06350
                bErr_03F06350=bTrsErr
                this.Try_03F06350()
                * --- Catch
                if !empty(i_Error)
                  i_ErrMsg=i_Error
                  i_Error=''
                  * --- accept error
                  bTrsErr=.f.
                  ah_ErrorMsg("Impossibile aggiornare ultima pagina di stampa; verificare tabella dati azienda",,"")
                endif
                bTrsErr=bTrsErr or bErr_03F06350
                * --- End
                this.oParentObject.w_PRPALI = this.w_PAGINE
              endif
            endif
          endif
        else
          * --- Dettagliata
          CP_CHPRN("QUERY\GSCG3BSO.FRX", " ", this)
        endif
      endif
    endif
    if used("UsrTmp")
      select UsrTmp
      use
    endif
    if used("Mastri")
      select Mastri
      use
    endif
    if used("Primo")
      select Primo
      use
    endif
  endproc
  proc Try_03F06350()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into AZIENDA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AZPRPALI ="+cp_NullLink(cp_ToStrODBC(this.w_PAGINE),'AZIENDA','AZPRPALI');
          +i_ccchkf ;
      +" where ";
          +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
             )
    else
      update (i_cTable) set;
          AZPRPALI = this.w_PAGINE;
          &i_ccchkf. ;
       where;
          AZCODAZI = i_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  proc Init(oParentObject,pTipo)
    this.pTipo=pTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='AZIENDA'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo"
endproc
