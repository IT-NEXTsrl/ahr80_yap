* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_acc                                                        *
*              Codici catastali Sdi-Basilea2                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_10]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-04-29                                                      *
* Last revis.: 2009-10-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_acc"))

* --- Class definition
define class tgsar_acc as StdForm
  Top    = 37
  Left   = 88

  * --- Standard Properties
  Width  = 471
  Height = 135+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-10-13"
  HelpContextID=109672297
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Constant Properties
  COD_CATA_IDX = 0
  cFile = "COD_CATA"
  cKeySelect = "CCCODICE"
  cKeyWhere  = "CCCODICE=this.w_CCCODICE"
  cKeyWhereODBC = '"CCCODICE="+cp_ToStrODBC(this.w_CCCODICE)';

  cKeyWhereODBCqualified = '"COD_CATA.CCCODICE="+cp_ToStrODBC(this.w_CCCODICE)';

  cPrg = "gsar_acc"
  cComment = "Codici catastali Sdi-Basilea2"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CCCODICE = space(4)
  w_CCREGION = space(3)
  w_CCAREAGE = space(2)
  w_CC____PR = space(2)
  w_CCLOCALI = space(40)
  w_CC___CAP = space(9)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'COD_CATA','gsar_acc')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_accPag1","gsar_acc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Codice catastale")
      .Pages(1).HelpContextID = 231720702
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCCCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='COD_CATA'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.COD_CATA_IDX,5],7]
    this.nPostItConn=i_TableProp[this.COD_CATA_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_CCCODICE = NVL(CCCODICE,space(4))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from COD_CATA where CCCODICE=KeySet.CCCODICE
    *
    i_nConn = i_TableProp[this.COD_CATA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.COD_CATA_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('COD_CATA')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "COD_CATA.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' COD_CATA '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CCCODICE',this.w_CCCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CCCODICE = NVL(CCCODICE,space(4))
        .w_CCREGION = NVL(CCREGION,space(3))
        .w_CCAREAGE = NVL(CCAREAGE,space(2))
        .w_CC____PR = NVL(CC____PR,space(2))
        .w_CCLOCALI = NVL(CCLOCALI,space(40))
        .w_CC___CAP = NVL(CC___CAP,space(9))
        cp_LoadRecExtFlds(this,'COD_CATA')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CCCODICE = space(4)
      .w_CCREGION = space(3)
      .w_CCAREAGE = space(2)
      .w_CC____PR = space(2)
      .w_CCLOCALI = space(40)
      .w_CC___CAP = space(9)
      if .cFunction<>"Filter"
      endif
    endwith
    cp_BlankRecExtFlds(this,'COD_CATA')
    this.DoRTCalc(1,6,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCCCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oCCREGION_1_2.enabled = i_bVal
      .Page1.oPag.oCCAREAGE_1_3.enabled = i_bVal
      .Page1.oPag.oCC____PR_1_4.enabled = i_bVal
      .Page1.oPag.oCCLOCALI_1_6.enabled = i_bVal
      .Page1.oPag.oCC___CAP_1_8.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oCCCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oCCCODICE_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'COD_CATA',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.COD_CATA_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCCODICE,"CCCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCREGION,"CCREGION",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCAREAGE,"CCAREAGE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CC____PR,"CC____PR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCLOCALI,"CCLOCALI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CC___CAP,"CC___CAP",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.COD_CATA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.COD_CATA_IDX,2])
    i_lTable = "COD_CATA"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.COD_CATA_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.COD_CATA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.COD_CATA_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.COD_CATA_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into COD_CATA
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'COD_CATA')
        i_extval=cp_InsertValODBCExtFlds(this,'COD_CATA')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(CCCODICE,CCREGION,CCAREAGE,CC____PR,CCLOCALI"+;
                  ",CC___CAP "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_CCCODICE)+;
                  ","+cp_ToStrODBC(this.w_CCREGION)+;
                  ","+cp_ToStrODBC(this.w_CCAREAGE)+;
                  ","+cp_ToStrODBC(this.w_CC____PR)+;
                  ","+cp_ToStrODBC(this.w_CCLOCALI)+;
                  ","+cp_ToStrODBC(this.w_CC___CAP)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'COD_CATA')
        i_extval=cp_InsertValVFPExtFlds(this,'COD_CATA')
        cp_CheckDeletedKey(i_cTable,0,'CCCODICE',this.w_CCCODICE)
        INSERT INTO (i_cTable);
              (CCCODICE,CCREGION,CCAREAGE,CC____PR,CCLOCALI,CC___CAP  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_CCCODICE;
                  ,this.w_CCREGION;
                  ,this.w_CCAREAGE;
                  ,this.w_CC____PR;
                  ,this.w_CCLOCALI;
                  ,this.w_CC___CAP;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.COD_CATA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.COD_CATA_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.COD_CATA_IDX,i_nConn)
      *
      * update COD_CATA
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'COD_CATA')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " CCREGION="+cp_ToStrODBC(this.w_CCREGION)+;
             ",CCAREAGE="+cp_ToStrODBC(this.w_CCAREAGE)+;
             ",CC____PR="+cp_ToStrODBC(this.w_CC____PR)+;
             ",CCLOCALI="+cp_ToStrODBC(this.w_CCLOCALI)+;
             ",CC___CAP="+cp_ToStrODBC(this.w_CC___CAP)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'COD_CATA')
        i_cWhere = cp_PKFox(i_cTable  ,'CCCODICE',this.w_CCCODICE  )
        UPDATE (i_cTable) SET;
              CCREGION=this.w_CCREGION;
             ,CCAREAGE=this.w_CCAREAGE;
             ,CC____PR=this.w_CC____PR;
             ,CCLOCALI=this.w_CCLOCALI;
             ,CC___CAP=this.w_CC___CAP;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.COD_CATA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.COD_CATA_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.COD_CATA_IDX,i_nConn)
      *
      * delete COD_CATA
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'CCCODICE',this.w_CCCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.COD_CATA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.COD_CATA_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,6,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCCCODICE_1_1.value==this.w_CCCODICE)
      this.oPgFrm.Page1.oPag.oCCCODICE_1_1.value=this.w_CCCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oCCREGION_1_2.RadioValue()==this.w_CCREGION)
      this.oPgFrm.Page1.oPag.oCCREGION_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCAREAGE_1_3.value==this.w_CCAREAGE)
      this.oPgFrm.Page1.oPag.oCCAREAGE_1_3.value=this.w_CCAREAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oCC____PR_1_4.value==this.w_CC____PR)
      this.oPgFrm.Page1.oPag.oCC____PR_1_4.value=this.w_CC____PR
    endif
    if not(this.oPgFrm.Page1.oPag.oCCLOCALI_1_6.value==this.w_CCLOCALI)
      this.oPgFrm.Page1.oPag.oCCLOCALI_1_6.value=this.w_CCLOCALI
    endif
    if not(this.oPgFrm.Page1.oPag.oCC___CAP_1_8.value==this.w_CC___CAP)
      this.oPgFrm.Page1.oPag.oCC___CAP_1_8.value=this.w_CC___CAP
    endif
    cp_SetControlsValueExtFlds(this,'COD_CATA')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_accPag1 as StdContainer
  Width  = 467
  height = 135
  stdWidth  = 467
  stdheight = 135
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCCCODICE_1_1 as StdField with uid="UZMPMRRPLL",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CCCODICE", cQueryName = "CCCODICE",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Codice catastale",;
    HelpContextID = 118095979,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=55, Left=75, Top=14, InputMask=replicate('X',4)


  add object oCCREGION_1_2 as StdCombo with uid="AMZZDCACZO",rtseq=2,rtrep=.f.,left=75,top=42,width=164,height=21;
    , ToolTipText = "Regione";
    , HelpContextID = 147787660;
    , cFormVar="w_CCREGION",RowSource=""+"Abruzzo,"+"Basilicata,"+"Calabria,"+"Campania,"+"Emilia Romagna,"+"Friuli Venezia Giulia,"+"Lazio,"+"Liguria,"+"Lombardia,"+"Marche,"+"Molise,"+"Piemonte,"+"Puglia,"+"Sardegna,"+"Sicilia,"+"Toscana,"+"Trentino Alto Adige,"+"Umbria,"+"Valle d'Aosta,"+"Veneto", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCCREGION_1_2.RadioValue()
    return(iif(this.value =1,'ABR',;
    iif(this.value =2,'BAS',;
    iif(this.value =3,'CAL',;
    iif(this.value =4,'CAM',;
    iif(this.value =5,'EMI',;
    iif(this.value =6,'FRI',;
    iif(this.value =7,'LAZ',;
    iif(this.value =8,'LIG',;
    iif(this.value =9,'LOM',;
    iif(this.value =10,'MAR',;
    iif(this.value =11,'MOL',;
    iif(this.value =12,'PIE',;
    iif(this.value =13,'PUG',;
    iif(this.value =14,'SAR',;
    iif(this.value =15,'SIC',;
    iif(this.value =16,'TOS',;
    iif(this.value =17,'TRE',;
    iif(this.value =18,'UMB',;
    iif(this.value =19,'AOS',;
    iif(this.value =20,'VEN',;
    space(3))))))))))))))))))))))
  endfunc
  func oCCREGION_1_2.GetRadio()
    this.Parent.oContained.w_CCREGION = this.RadioValue()
    return .t.
  endfunc

  func oCCREGION_1_2.SetRadio()
    this.Parent.oContained.w_CCREGION=trim(this.Parent.oContained.w_CCREGION)
    this.value = ;
      iif(this.Parent.oContained.w_CCREGION=='ABR',1,;
      iif(this.Parent.oContained.w_CCREGION=='BAS',2,;
      iif(this.Parent.oContained.w_CCREGION=='CAL',3,;
      iif(this.Parent.oContained.w_CCREGION=='CAM',4,;
      iif(this.Parent.oContained.w_CCREGION=='EMI',5,;
      iif(this.Parent.oContained.w_CCREGION=='FRI',6,;
      iif(this.Parent.oContained.w_CCREGION=='LAZ',7,;
      iif(this.Parent.oContained.w_CCREGION=='LIG',8,;
      iif(this.Parent.oContained.w_CCREGION=='LOM',9,;
      iif(this.Parent.oContained.w_CCREGION=='MAR',10,;
      iif(this.Parent.oContained.w_CCREGION=='MOL',11,;
      iif(this.Parent.oContained.w_CCREGION=='PIE',12,;
      iif(this.Parent.oContained.w_CCREGION=='PUG',13,;
      iif(this.Parent.oContained.w_CCREGION=='SAR',14,;
      iif(this.Parent.oContained.w_CCREGION=='SIC',15,;
      iif(this.Parent.oContained.w_CCREGION=='TOS',16,;
      iif(this.Parent.oContained.w_CCREGION=='TRE',17,;
      iif(this.Parent.oContained.w_CCREGION=='UMB',18,;
      iif(this.Parent.oContained.w_CCREGION=='AOS',19,;
      iif(this.Parent.oContained.w_CCREGION=='VEN',20,;
      0))))))))))))))))))))
  endfunc

  add object oCCAREAGE_1_3 as StdField with uid="PIBEIXYFHO",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CCAREAGE", cQueryName = "CCAREAGE",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Area geografica",;
    HelpContextID = 253550699,;
   bGlobalFont=.t.,;
    Height=21, Width=33, Left=298, Top=42, InputMask=replicate('X',2)

  add object oCC____PR_1_4 as StdField with uid="LSHZAYFFOG",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CC____PR", cQueryName = "CC____PR",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia",;
    HelpContextID = 20201352,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=420, Top=42, InputMask=replicate('X',2)

  add object oCCLOCALI_1_6 as StdField with uid="ETTPJOSATP",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CCLOCALI", cQueryName = "CCLOCALI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Localit�",;
    HelpContextID = 17133457,;
   bGlobalFont=.t.,;
    Height=21, Width=386, Left=75, Top=70, InputMask=replicate('X',40)

  add object oCC___CAP_1_8 as StdField with uid="PIUQYGNIKT",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CC___CAP", cQueryName = "CC___CAP",;
    bObbl = .f. , nPag = 1, value=space(9), bMultilanguage =  .f.,;
    ToolTipText = "CAP",;
    HelpContextID = 221527946,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=75, Top=97, InputMask=replicate('X',9)

  add object oStr_1_5 as StdString with uid="EROMSOQCAY",Visible=.t., Left=16, Top=14,;
    Alignment=1, Width=55, Height=18,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_7 as StdString with uid="MURWYRCGVU",Visible=.t., Left=11, Top=70,;
    Alignment=1, Width=60, Height=18,;
    Caption="Localit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="RKKGFZVLSH",Visible=.t., Left=5, Top=97,;
    Alignment=1, Width=66, Height=18,;
    Caption="CAP:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="WUHRUHFKUG",Visible=.t., Left=348, Top=42,;
    Alignment=1, Width=70, Height=18,;
    Caption="Provincia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="PFWDKUFYWC",Visible=.t., Left=7, Top=42,;
    Alignment=1, Width=64, Height=18,;
    Caption="Regione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="EODLUMUXZA",Visible=.t., Left=249, Top=42,;
    Alignment=1, Width=44, Height=18,;
    Caption="Area:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_acc','COD_CATA','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CCCODICE=COD_CATA.CCCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
