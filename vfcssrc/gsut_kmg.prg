* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kmg                                                        *
*              MyGadget Bar                                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-01-24                                                      *
* Last revis.: 2016-03-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_kmg",oParentObject))

* --- Class definition
define class tgsut_kmg as StdForm
  Top    = 6
  Left   = 13

  * --- Standard Properties
  Width  = 455
  Height = 630
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-03-21"
  HelpContextID=199836521
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=12

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsut_kmg"
  cComment = "MyGadget Bar"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_MG_START = space(1)
  w_MGPIANOV = space(1)
  w_MGCHKCOL = space(1)
  w_MGBCKCOL = 0
  w_MGEFTRAN = space(1)
  w_MG_TIMER = space(1)
  w_MGSALCNF = space(1)
  w_MG_PINMG = space(1)
  w_RET = space(1)
  w_MGHIDREF = space(1)
  w_MGDTHEME = space(1)
  w_MGDISRAP = space(1)
  * --- Area Manuale = Declare Variables
  * --- gsut_kmg
    Left = -1000
    bShowOption = .F.
    bShowBar = .F.
    ShowWindow = 2
    nOldProc = 0
    bChkPositionForm = .F.
    nHwndClient = 0
    bPin = .F.
    bModifyMode = .F.
    bLibraryMode = .F.
    bStopResize=.F.
    showTips=.F.
    
    bApplyFormDecorator =.f.
    
    nAlphaColor = RGB(255,255,255)
    
    *--- Form di sistema
    bSysForm = .T.
    
    Dimension aPoints[6,2]
    nWidth = This.Width
    
    bOnTopStore = .F.
    
    oToolTipTimer = .null. && oggetto per la gestione dei tooltip dell'interfaccia
    Add Object oBalloon AS cp_balloontip && balloon per le notifiche sulla barra
    
    PROCEDURE Declare()
  	    DECLARE INTEGER CreateRectRgn IN WIN32API;
  	        INTEGER nLeftRect, INTEGER nTopRect,;
  	        INTEGER nRightRect, INTEGER nBottomRect
          
  	    DECLARE INTEGER CombineRgn IN WIN32API; 
  	        INTEGER hrgnDest, INTEGER hrgnSrc1,;
  	        INTEGER hrgnSrc2,;
  	        INTEGER fnCombineMode
  	        
  	    DECLARE SetWindowRgn IN WIN32API;
  	        INTEGER hWindow, INTEGER hRgn, SHORT bRedraw
  	        
  	    DECLARE INTEGER DeleteObject IN WIN32API;
  	        INTEGER hObject
        DECLARE INTEGER SetCapture IN WIN32API;
            INTEGER hWnd  
        DECLARE INTEGER ReleaseCapture IN WIN32API    
    EndProc
    
    Proc SetRegionOn(bShow, bOption)
      #DEFINE RGN_OR 2
     
      Local hRgnBase, hRgnOpt, hRgnRight
      Local x1,y1,x2,y2
      Local oShape
      
      oShape = This.oPgFrm.Page1.oPag.oBox_1_3
      oShape.Visible=.F.
      
      This.bShowBar = m.bShow Or m.bOption
      *--- Form base 
      If This.bShowBar Or This.bPin
        oShape = This.oPgFrm.Page1.oPag.oBox_1_2
      Else
        oShape = This.oPgFrm.Page1.oPag.oBox_1_1
      EndIf
      x1=m.oShape.Left
      y1=m.oShape.Top
      x2=m.x1+m.oShape.Width
      y2=m.y1+m.oShape.Height
      hRgnBase = CreateRectRgn(m.x1, m.y1, m.x2, m.y2)
      
      *--- Riposiziono il balloon
      This.oBalloon.ctlHide(2)
      This.oBalloon.ctlVisible = .F.
      This.oBalloon.ctlLeft = m.x1-2
      This.oBalloon.ctlTop = This.oPgFrm.Page1.oPag.oBtn_1_6.Top+(This.oPgFrm.Page1.oPag.oBtn_1_6.Height*0.5)
     
      *--- Option
      This.bShowOption = m.bOption
      If This.bShowOption
        oShape = This.oPgFrm.Page1.oPag.oBox_1_3
        x1=m.oShape.Left
        y1=m.oShape.Top
        x2=m.x1+m.oShape.Width
        y2=m.y1+m.oShape.Height
        hRgnOpt = CreateRectRgn(m.x1, m.y1, m.x2, m.y2)   
        = CombineRgn(m.hRgnBase, m.hRgnBase, m.hRgnOpt, RGN_OR)
        = DeleteObject(m.hRgnOpt)  
        oShape.Visible=.t.
      EndIf
      *--- Tooltip Fox sulle opzioni
      This.ShowTips = This.bShowOption
      
      = SetWindowRgn(This.hwnd, m.hRgnBase, 1)
      = DeleteObject(m.hRgnBase)
     
    EndProc
    
    Proc PntInRgn(nX, nY)   
      Local oShp, bInside
      
      m.bInside = .T.
     
      *--- Form base 
      If This.bShowBar Or This.bPin
        oShp = This.oPgFrm.Page1.oPag.oBox_1_2
      Else
        oShp = This.oPgFrm.Page1.oPag.oBox_1_1
      EndIf
      
      m.bInside = m.nX>=m.oShp.Left And m.nX<=(m.oShp.Left+m.oShp.Width) And m.nY>=m.oShp.Top And m.nY<=(m.oShp.Top+m.oShp.Height)
      
      *--- Se non sono sulla spalla verifico se sono nella form delle opzioni
      If !m.bInside And This.bShowOption
        oShp = This.oPgFrm.Page1.oPag.oBox_1_3
        m.bInside = m.nX>=m.oShp.Left And m.nX<=(m.oShp.Left+m.oShp.Width) And m.nY>=m.oShp.Top And m.nY<=(m.oShp.Top+m.oShp.Height)
        *--- Tooltip Fox SOLO sulle opzioni
        This.ShowTips = m.bInside
      Else
        This.ShowTips = .F.
      Endif
      
      Return m.bInside   
    EndProc
    
    Proc OnStore()
      This.bLibraryMode = !This.bLibraryMode
      This.bModifyMode = .T.  &&Sempre in modifica
      Local lbl
      lbl = This.GetCtrl("M O D I F I C A")
      lbl.Visible = This.bModifyMode
      lbl=.null.    
      Obj = This.oPgFrm.Page1.oPag.oBtn_1_11
      Obj.CpPicture = "gd_save.bmp"
      Obj = .null.      
      This.oParentObject.ModifyMode(This.bModifyMode , .F. , This.bLibraryMode)
      *--- Se necessario metto l'interfaccia dietro tutte le form
      If This.bLibraryMode And This.oParentObject.bOnTop
        This.bOnTopStore = .T.
        This.oParentObject.SetView(.F., This.oParentObject.bFullScreen, .F.)
      Endif
      *--- Ripristino l'interfaccia on Top se necessario
      If !This.bLibraryMode And This.bOnTopStore
        This.bOnTopStore = .F.
        This.oParentObject.SetView(.F., This.oParentObject.bFullScreen, .T.)
      Endif
    EndProc
    
    Proc OnOption()
       This.bShowOption = !This.bShowOption
       This.SetRegionOn(.T., This.bShowOption)
    EndProc
  
    Proc OnHide()
       This.SetRegionOn()
       This.ShowTips = .F.
    EndProc
  
    Proc OnShow()
       This.SetRegionOn(.T., .F.)
       This.ShowTips = .F.
    EndProc
  
   	Procedure Eventhandler(HWnd, Msg, wParam, Lparam)
  		With This
        If m.HWnd = .nHwndClient
         SetCapture(m.HWnd)
         If MCOL(.name, 3)<>-1 And MROW(.name,3)<>-1 And .PntInRgn(Get_X_LParam(m.Lparam),Get_Y_LParam(m.Lparam))
           *--- Dentro la maschera
           If !.bShowBar
             *--- Forzo il resize
             .Height = .Height + 1
             .Height = .Height - 1
             *--- Apro la barra
             .SetRegionOn(.T., .bShowOption)
           EndIf
         Else
           *--- Fuori dalla maschera
           .SetRegionOn()
           ReleaseCapture()
         EndIf
        Else
          *lcBuffer = Sys(2600, m.lParam, 24)
          Sys(2600, m.lParam+16, 4, BinToC(.nWidth,'4RS'))
        EndIf
      	Return CallWindowProc(.nOldProc, m.HWnd, m.Msg, m.wParam, m.Lparam)
  		EndWith
  	EndProc
    
    Procedure Pin()  
      This.bPin = !This.bPin
      Obj = This.oPgFrm.Page1.oPag.oBtn_1_12
      Obj.CpPicture = IIF(This.bPin, "gd_pin.bmp", "gd_unpin.bmp")
      Obj = .null.
      This.oParentObject.SetView(!This.oParentObject.Visible, This.oParentObject.bFullScreen, This.oParentObject.bOnTop)
      This.w_MG_PINMG = Iif(This.bPin, 'S', '')
    EndProc
    
    Procedure OnModify(bNoSave)
    	This.bModifyMode = !This.bModifyMode
      If !This.bModifyMode
        This.bLibraryMode = .F.
      EndIf
      Local lbl
      lbl = This.GetCtrl("M O D I F I C A")
      lbl.Visible = This.bModifyMode
      lbl=.null. 
      Obj = This.oPgFrm.Page1.oPag.oBtn_1_11
      Obj.CpPicture = IIF(This.bModifyMode, "gd_save.bmp", "gd_pencil.bmp")
      Obj = .null.    
  		This.oParentObject.ModifyMode(This.bModifyMode, m.bNoSave, This.bLibraryMode)
      *--- Ripristino l'interfaccia on Top se necessario
      If !This.bLibraryMode And This.bOnTopStore
        This.bOnTopStore = .F.
        This.oParentObject.SetView(.F., This.oParentObject.bFullScreen, .T.)
      Endif
    EndProc
    
    Procedure ExitModify(bNoSave)
    	This.bModifyMode = .F.
      This.bLibraryMode = .F.
      Local lbl
      lbl = This.GetCtrl("M O D I F I C A")
      lbl.Visible = .F.
      lbl=.null.  
      Obj = This.oPgFrm.Page1.oPag.oBtn_1_11
      Obj.CpPicture = "gd_pencil.bmp"
      Obj = .null.     
  		This.oParentObject.ModifyMode(This.bModifyMode, m.bNoSave, This.bLibraryMode)
    EndProc
    
    Procedure Front_Back(Obj)
      This.oParentObject.SetView(.F., This.oParentObject.bFullScreen, !This.oParentObject.bOntop)
      Obj.CpPicture = IIF(This.oParentObject.bOntop, "gd_back.bmp", "gd_front.bmp")
      Obj.ToolTiptext = IIF(This.oParentObject.bOntop, cp_Translate("Porta in secondo piano"), cp_Translate("Porta in primo piano"))
      Obj = .null.    
    EndProc
    
    Proc Resize()
      If !This.bStopResize
        DoDefault()
        This.SetRegionOn(This.bShowBar, This.bShowOption)
      Endif
    EndProc
    
    Proc GetColor(pColor)
      local l_NewColor, oCtrl
      if Type("pColor")='N'
         l_NewColor = m.pColor
      Else
         l_NewColor = GetColor(This.w_MGBCKCOL)
      EndIf
      If m.l_NewColor<>-1
        This.w_MGBCKCOL = m.l_NewColor
        oCtrl = this.oPgFrm.Page1.oPag.oMGBCKCOL_1_18
        oCtrl.DisabledBackColor = m.l_NewColor
        oCtrl.DisabledForeColor = m.l_NewColor
        oCtrl = .NULL.
      EndIf
    EndProc
    
    Proc SaveSetting()
      gsut_bgg(This, "SaveSetting")
    EndProc
    
    Proc RestoreSetting()
      gsut_bgg(This, "RestoreSetting")
    EndProc
    
    *--- Scroll event
    Proc ScrollUpClick()
      This.oParentObject.MouseWheel(1)
    EndProc
  
    Proc ScrollDownClick()
      This.oParentObject.MouseWheel(-1)
    EndProc
  
  
    Proc Activate()
  		*--- Ridefinita per non essere eseguita
    EndProc
  	Proc Deactivate()
  		*--- Ridefinita per non essere eseguita
    EndProc    
    
    Proc SetStatus()
      *--- Ridefinita per non essere eseguita
    EndProc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kmgPag1","gsut_kmg",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMG_START_1_15
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsut_kmg
    #Define WM_MOUSEMOVE 0x0200
    #Define GWL_WNDPROC	-4  
    #Define GW_CHILD 5
    #Define WM_WINDOWPOSCHANGING 0x0046
    With This
      .Declare()
    
      #Define GWL_EXSTYLE -20
      #Define GWL_STYLE -16
      nExStyle = GetWindowLong(.hWnd, GWL_EXSTYLE) 
    	SetWindowLong(.hWnd, GWL_EXSTYLE, BITAND(nExStyle, BITNOT(0x00040000))) 
    	nStyle = GetWindowLong(.hWnd, GWL_STYLE) 
    	SetWindowLong(.hWnd, GWL_STYLE, BITOR(nStyle, 0x40000000)) 
    	SetParent(.hWnd, _Screen.hWnd)
      
      .bStopResize=.T.
      .Titlebar = 0
      .BorderStyle = 0
      
      .nHwndClient = GetWindow(.hWnd, GW_CHILD)
      .nOldProc = GetWindowLong(.nHwndClient, GWL_WNDPROC)  
      BindEvent(.nHwndClient, WM_MOUSEMOVE, This, "Eventhandler")
      BindEvent(.Hwnd, WM_WINDOWPOSCHANGING, This, "Eventhandler")
      
      *--- Layout
      Local lbl
      lbl = .GetCtrl("My Gadget")
      lbl.Rotation=270
      lbl = .GetCtrl("M O D I F I C A")
      lbl.Rotation=270
      lbl.Visible = .F.
      *--- Scroll
      lbl = .GetCtrl("<<")
      lbl.Rotation=270
      lbl.Visible = .F.
      BindEvent(lbl, "Click", This, "ScrollUpClick")
      lbl = .GetCtrl(">>")
      lbl.Rotation=270
      lbl.Visible = .F.  
      BindEvent(lbl, "Click", This, "ScrollDownClick")
      lbl=.null.
    
    
      .aPoints[1,1] = 0
      .aPoints[1,2] = 0
      .aPoints[2,1] = 100
      .aPoints[2,2] = 0
      .aPoints[3,1] = 100
      .aPoints[3,2] = 27
      .aPoints[4,1] = 84.8
      .aPoints[4,2] = 27
      .aPoints[5,1] = 84.8
      .aPoints[5,2] = 100
      .aPoints[6,1] = 0
      .aPoints[6,2] = 100 
      
      .oPgFrm.Page1.oPag.oBox_1_3.Polypoints = "ThisForm.aPoints"
      
      local nBackColor
      *nBackColor = .oParentObject.BackColor
      nBackColor = i_ThemesManager.GetProp(109)
      
      .oPgFrm.Page1.oPag.oBox_1_4.BorderColor=RGBAlphaBlending(m.nBackColor, .nAlphaColor, 0.4)
      .oPgFrm.Page1.oPag.oBox_1_4.BackStyle= 0
      .oPgFrm.Page1.oPag.oBox_1_4.SpecialEffect= 1
      
      .oPgFrm.Page1.oPag.oBox_1_5.BorderColor=RGBAlphaBlending(m.nBackColor, .nAlphaColor, 0.4)
      .oPgFrm.Page1.oPag.oBox_1_5.BackStyle= 0
      .oPgFrm.Page1.oPag.oBox_1_5.SpecialEffect= 1
      
      .ImgBackground.Visible=.F.
      *.BackColor = RGBAlphaBlending(.oParentObject.BackColor, RGB(255,255,255), 0.1)
      .BackColor = i_ThemesManager.GetProp(109)
      
      .oPgfrm.Page1.oPag.Backstyle=0
      .oPgfrm.Page1.Backstyle=0
      
      Local Obj
      For Each Obj In .oPgFrm.Page1.oPag.Controls
        If Lower(m.Obj.Class)="gadgetbutton"
          Obj.ChangeTheme()
        EndIf
      Next
      Obj = .null.
      
      .oPgFrm.Page1.oPag.SetAll("Forecolor", RGB(255,255,255))
      .oPgFrm.Page1.oPag.SetAll("Backcolor", This.BackColor, 'StdCombo')
      .oPgFrm.Page1.oPag.SetAll("Bordercolor", RGB(230,230,230), 'StdCombo')
      .oPgFrm.Page1.oPag.SetAll("Backcolor", This.BackColor, 'StdTableCombo')
      .oPgFrm.Page1.oPag.SetAll("Bordercolor", RGB(230,230,230), 'StdTableCombo')
      .oPgFrm.Page1.oPag.SetAll("SpecialEffect", 1, 'StdTableCombo')
      .SetRegionOn(.bShowBar, .bShowOption)
      
       .oPgFrm.Page1.oPag.oBox_1_1.Visible = .f.
      .oPgFrm.Page1.oPag.oBox_1_2.Visible = .f.
        
      .oPgFrm.Page1.oPag.oBox_1_3.Visible = .f.
      .oPgFrm.Page1.oPag.oBox_1_3.BorderColor = RGB(255,255,255)
      .oPgFrm.Page1.oPag.oBox_1_3.BackStyle = 0
      .oPgFrm.Page1.oPag.oBox_1_3.SpecialEffect = 1  
      
      .bStopResize=.F.
      
      *--- Mi salvo la larghezza iniziale
      .nWidth = .Width
      .Height = SYSMETRIC(2)
      
      *--- Impostazioni balloon
      .oBalloon.ctlHideDelay = 5000
    EndWith
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_MG_START=space(1)
      .w_MGPIANOV=space(1)
      .w_MGCHKCOL=space(1)
      .w_MGBCKCOL=0
      .w_MGEFTRAN=space(1)
      .w_MG_TIMER=space(1)
      .w_MGSALCNF=space(1)
      .w_MG_PINMG=space(1)
      .w_RET=space(1)
      .w_MGHIDREF=space(1)
      .w_MGDTHEME=space(1)
      .w_MGDISRAP=space(1)
    endwith
    this.DoRTCalc(1,12,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,12,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_GMKDLIENRT()
    with this
          * --- Chiusa GadgetManager
          .w_RET = IIF(!ISNULL(ThisForm.oParentObject), ThisForm.oParentObject._Release(),'')
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsut_kmg
    If Lower(cEvent)='w_mg_pinmg changed'
        This.Pin()
    EndIf
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Done")
          .Calculate_GMKDLIENRT()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMG_START_1_15.RadioValue()==this.w_MG_START)
      this.oPgFrm.Page1.oPag.oMG_START_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMGPIANOV_1_16.RadioValue()==this.w_MGPIANOV)
      this.oPgFrm.Page1.oPag.oMGPIANOV_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMGCHKCOL_1_17.RadioValue()==this.w_MGCHKCOL)
      this.oPgFrm.Page1.oPag.oMGCHKCOL_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMGBCKCOL_1_18.value==this.w_MGBCKCOL)
      this.oPgFrm.Page1.oPag.oMGBCKCOL_1_18.value=this.w_MGBCKCOL
    endif
    if not(this.oPgFrm.Page1.oPag.oMGEFTRAN_1_19.RadioValue()==this.w_MGEFTRAN)
      this.oPgFrm.Page1.oPag.oMGEFTRAN_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMG_TIMER_1_20.RadioValue()==this.w_MG_TIMER)
      this.oPgFrm.Page1.oPag.oMG_TIMER_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMGSALCNF_1_22.RadioValue()==this.w_MGSALCNF)
      this.oPgFrm.Page1.oPag.oMGSALCNF_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMG_PINMG_1_23.RadioValue()==this.w_MG_PINMG)
      this.oPgFrm.Page1.oPag.oMG_PINMG_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMGHIDREF_1_34.RadioValue()==this.w_MGHIDREF)
      this.oPgFrm.Page1.oPag.oMGHIDREF_1_34.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMGDTHEME_1_35.RadioValue()==this.w_MGDTHEME)
      this.oPgFrm.Page1.oPag.oMGDTHEME_1_35.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMGDISRAP_1_38.RadioValue()==this.w_MGDISRAP)
      this.oPgFrm.Page1.oPag.oMGDISRAP_1_38.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsut_kmgPag1 as StdContainer
  Width  = 452
  height = 630
  stdWidth  = 452
  stdheight = 630
  resizeYpos=624
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_6 as GadgetButton with uid="ZHROAHYXSG",left=365, top=49, width=53,height=51,;
    CpPicture="gd_front.bmp", caption="", nPag=1;
    , ToolTipText = "Porta in primo piano";
    , HelpContextID = 199552938;
  , bGlobalFont=.t.

    proc oBtn_1_6.Click()
      with this.Parent.oContained
        ThisForm.Front_Back(This)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_6.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!ThisForm.bLibraryMode)
      endwith
    endif
  endfunc


  add object oBtn_1_7 as GadgetButton with uid="PNBGNSYSDB",left=365, top=119, width=53,height=51,;
    CpPicture="gd_fullscreen.bmp", caption="", nPag=1;
    , ToolTipText = "Schermo intero";
    , HelpContextID = 192285098;
  , bGlobalFont=.t.

    proc oBtn_1_7.Click()
      with this.Parent.oContained
        ThisForm.oParentObject.SetView(.F., .T., ThisForm.oParentObject.bOntop)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_8 as GadgetButton with uid="ATUWDRJPUA",left=365, top=174, width=53,height=51,;
    CpPicture="gd_side.bmp", caption="", nPag=1;
    , ToolTipText = "Schermo parziale";
    , HelpContextID = 192779482;
  , bGlobalFont=.t.

    proc oBtn_1_8.Click()
      with this.Parent.oContained
        ThisForm.oParentObject.SetView(.F., .F., ThisForm.oParentObject.bOntop)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_9 as GadgetButton with uid="ECTVCLYZGQ",left=365, top=229, width=53,height=51,;
    CpPicture="gd_hide.bmp", caption="", nPag=1;
    , ToolTipText = "Nascondi";
    , HelpContextID = 192779658;
  , bGlobalFont=.t.

    proc oBtn_1_9.Click()
      with this.Parent.oContained
        ThisForm.oParentObject.SetView(.T.,ThisForm.oParentObject.bFullScreen, ThisForm.oParentObject.bOntop)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_10 as GadgetButton with uid="YCITVWPHUI",left=365, top=293, width=53,height=51,;
    CpPicture="gd_add.bmp", caption="", nPag=1;
    , ToolTipText = "Aggiungi gadget";
    , HelpContextID = 85973466;
  , bGlobalFont=.t.

    proc oBtn_1_10.Click()
      with this.Parent.oContained
        ThisForm.OnStore()
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_11 as GadgetButton with uid="LCJAHRROYK",left=365, top=359, width=53,height=51,;
    CpPicture="gd_pencil.bmp", caption="", nPag=1;
    , ToolTipText = "Modifica gadget";
    , HelpContextID = 65433798;
  , bGlobalFont=.t.

    proc oBtn_1_11.Click()
      with this.Parent.oContained
        ThisForm.OnModify()
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_12 as GadgetButton with uid="NXSRSOXOLH",left=363, top=4, width=17,height=18,;
    CpPicture="gd_unpin.bmp", caption="", nPag=1;
    , ToolTipText = "Pin";
    , HelpContextID = 199357706;
    , ImgSize = 16;
  , bGlobalFont=.t.

    proc oBtn_1_12.Click()
      with this.Parent.oContained
        ThisForm.Pin()
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_13 as GadgetButton with uid="IGYBSEBAEK",left=365, top=414, width=53,height=51,;
    CpPicture="gd_setting.bmp", caption="", nPag=1;
    , ToolTipText = "Impostazioni";
    , HelpContextID = 158822886;
  , bGlobalFont=.t.

    proc oBtn_1_13.Click()
      with this.Parent.oContained
        ThisForm.OnOption()
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_14 as GadgetButton with uid="XHOOBKRLGS",left=432, top=4, width=16,height=18,;
    CpPicture="gd_esc.bmp", caption="", nPag=1;
    , ToolTipText = "Chiudi";
    , HelpContextID = 199835018;
    , ImgSize = 16;
  , bGlobalFont=.t.

    proc oBtn_1_14.Click()
      with this.Parent.oContained
        ThisForm.oParentObject._Release()
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oMG_START_1_15 as StdCheck with uid="BCTGDPIBMU",rtseq=1,rtrep=.f.,left=163, top=432, caption="Apri all'avvio",;
    ToolTipText = "Se attivo, apre automaticamente la MyGadget all'avvio di Ad Hoc",;
    HelpContextID = 179304730,;
    cFormVar="w_MG_START", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMG_START_1_15.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oMG_START_1_15.GetRadio()
    this.Parent.oContained.w_MG_START = this.RadioValue()
    return .t.
  endfunc

  func oMG_START_1_15.SetRadio()
    this.Parent.oContained.w_MG_START=trim(this.Parent.oContained.w_MG_START)
    this.value = ;
      iif(this.Parent.oContained.w_MG_START=='S',1,;
      0)
  endfunc

  add object oMGPIANOV_1_16 as StdCheck with uid="WVBPJQIXVS",rtseq=2,rtrep=.f.,left=163, top=454, caption="In primo piano",;
    ToolTipText = "Se attivo, apre la MyGadget sempre in primo piano",;
    HelpContextID = 108333340,;
    cFormVar="w_MGPIANOV", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMGPIANOV_1_16.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oMGPIANOV_1_16.GetRadio()
    this.Parent.oContained.w_MGPIANOV = this.RadioValue()
    return .t.
  endfunc

  func oMGPIANOV_1_16.SetRadio()
    this.Parent.oContained.w_MGPIANOV=trim(this.Parent.oContained.w_MGPIANOV)
    this.value = ;
      iif(this.Parent.oContained.w_MGPIANOV=='S',1,;
      0)
  endfunc

  add object oMGCHKCOL_1_17 as StdRadio with uid="LNTRRMXMHX",rtseq=3,rtrep=.f.,left=27, top=437, width=118,height=48;
    , ToolTipText = "Se selezionato, all'apertura MyGadget sar� visualizzato a schermo intero";
    , cFormVar="w_MGCHKCOL", ButtonCount=3, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oMGCHKCOL_1_17.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Schermo intero"
      this.Buttons(1).HelpContextID = 202586386
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Schermo parziale"
      this.Buttons(2).HelpContextID = 202586386
      this.Buttons(2).Top=15
      this.Buttons(3).Caption="Nascosto"
      this.Buttons(3).HelpContextID = 202586386
      this.Buttons(3).Top=30
      this.SetAll("Width",116)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Se selezionato, all'apertura MyGadget sar� visualizzato a schermo intero")
      StdRadio::init()
    endproc

  func oMGCHKCOL_1_17.RadioValue()
    return(iif(this.value =1,"N",;
    iif(this.value =2,"S",;
    iif(this.value =3,"X",;
    space(1)))))
  endfunc
  func oMGCHKCOL_1_17.GetRadio()
    this.Parent.oContained.w_MGCHKCOL = this.RadioValue()
    return .t.
  endfunc

  func oMGCHKCOL_1_17.SetRadio()
    this.Parent.oContained.w_MGCHKCOL=trim(this.Parent.oContained.w_MGCHKCOL)
    this.value = ;
      iif(this.Parent.oContained.w_MGCHKCOL=="N",1,;
      iif(this.Parent.oContained.w_MGCHKCOL=="S",2,;
      iif(this.Parent.oContained.w_MGCHKCOL=="X",3,;
      0)))
  endfunc

  add object oMGBCKCOL_1_18 as StdField with uid="ZKBKGWRDIT",rtseq=4,rtrep=.f.,;
    cFormVar = "w_MGBCKCOL", cQueryName = "MGBCKCOL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Colore di sfondo della MyGadget",;
    HelpContextID = 202254610,;
   bGlobalFont=.t.,;
    Height=21, Width=109, Left=17, Top=504, bNoDisabledBackColor=.T.


  add object oMGEFTRAN_1_19 as StdCombo with uid="DZRHTRVORI",value=1,rtseq=5,rtrep=.f.,left=17,top=545,width=129,height=22;
    , bNoBackColor = .T.;
    , ToolTipText = "Animazione con cui verr� nascosta o mostrata la MyGadget";
    , HelpContextID = 195123476;
    , cFormVar="w_MGEFTRAN",RowSource=""+"Nessuno,"+"Linear,"+"BounceEaseOut,"+"SineEaseOut,"+"ElasticEaseOut,"+"QuarticEaseInOut,"+"ExponentialEaseInOut", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMGEFTRAN_1_19.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'L',;
    iif(this.value =3,'B',;
    iif(this.value =4,'S',;
    iif(this.value =5,'E',;
    iif(this.value =6,'Q',;
    iif(this.value =7,'X',;
    space(1)))))))))
  endfunc
  func oMGEFTRAN_1_19.GetRadio()
    this.Parent.oContained.w_MGEFTRAN = this.RadioValue()
    return .t.
  endfunc

  func oMGEFTRAN_1_19.SetRadio()
    this.Parent.oContained.w_MGEFTRAN=trim(this.Parent.oContained.w_MGEFTRAN)
    this.value = ;
      iif(this.Parent.oContained.w_MGEFTRAN=='',1,;
      iif(this.Parent.oContained.w_MGEFTRAN=='L',2,;
      iif(this.Parent.oContained.w_MGEFTRAN=='B',3,;
      iif(this.Parent.oContained.w_MGEFTRAN=='S',4,;
      iif(this.Parent.oContained.w_MGEFTRAN=='E',5,;
      iif(this.Parent.oContained.w_MGEFTRAN=='Q',6,;
      iif(this.Parent.oContained.w_MGEFTRAN=='X',7,;
      0)))))))
  endfunc

  add object oMG_TIMER_1_20 as StdCheck with uid="LIOPEWNVIQ",rtseq=6,rtrep=.f.,left=163, top=476, caption="Aggiornamento automatico",;
    ToolTipText = "Se attivo, verr� effettuato l'aggiornamento automatico dei gadget",;
    HelpContextID = 167708392,;
    cFormVar="w_MG_TIMER", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMG_TIMER_1_20.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oMG_TIMER_1_20.GetRadio()
    this.Parent.oContained.w_MG_TIMER = this.RadioValue()
    return .t.
  endfunc

  func oMG_TIMER_1_20.SetRadio()
    this.Parent.oContained.w_MG_TIMER=trim(this.Parent.oContained.w_MG_TIMER)
    this.value = ;
      iif(this.Parent.oContained.w_MG_TIMER=='S',1,;
      0)
  endfunc

  add object oMGSALCNF_1_22 as StdCheck with uid="NBRKQLSLIS",rtseq=7,rtrep=.f.,left=163, top=543, caption="Conserva attributi utente",;
    ToolTipText = "Se attivo conserva gli attributi utente se il gadget viene rimosso",;
    HelpContextID = 203241740,;
    cFormVar="w_MGSALCNF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMGSALCNF_1_22.RadioValue()
    return(iif(this.value =1,"S",;
    space(1)))
  endfunc
  func oMGSALCNF_1_22.GetRadio()
    this.Parent.oContained.w_MGSALCNF = this.RadioValue()
    return .t.
  endfunc

  func oMGSALCNF_1_22.SetRadio()
    this.Parent.oContained.w_MGSALCNF=trim(this.Parent.oContained.w_MGSALCNF)
    this.value = ;
      iif(this.Parent.oContained.w_MGSALCNF=="S",1,;
      0)
  endfunc

  add object oMG_PINMG_1_23 as StdCheck with uid="FCAVRMPELW",rtseq=8,rtrep=.f.,left=163, top=520, caption="Blocca My Gadget bar",;
    ToolTipText = "Se attivo la MyGadget Bar rester� sempre aperta",;
    HelpContextID = 117242125,;
    cFormVar="w_MG_PINMG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMG_PINMG_1_23.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oMG_PINMG_1_23.GetRadio()
    this.Parent.oContained.w_MG_PINMG = this.RadioValue()
    return .t.
  endfunc

  func oMG_PINMG_1_23.SetRadio()
    this.Parent.oContained.w_MG_PINMG=trim(this.Parent.oContained.w_MG_PINMG)
    this.value = ;
      iif(this.Parent.oContained.w_MG_PINMG=='S',1,;
      0)
  endfunc


  add object oBtn_1_27 as GadgetButton with uid="UVSXGUNUMH",left=128, top=505, width=17,height=18,;
    CpPicture="gd_color.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per selezionare un colore";
    , HelpContextID = 199635498;
    , ImgSize = 16;
  , bGlobalFont=.t.

    proc oBtn_1_27.Click()
      with this.Parent.oContained
        ThisForm.GetColor()
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_28 as StdButton with uid="MZZAQQBGLM",left=310, top=591, width=20,height=21,;
    CpPicture="save.bmp", caption="", nPag=1;
    , ToolTipText = "Salva impostazioni";
    , HelpContextID = 199835050;
    , ImgSize = 16;
  , bGlobalFont=.t.

    proc oBtn_1_28.Click()
      with this.Parent.oContained
        ThisForm.SaveSetting()
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_29 as StdButton with uid="ZSMFNRPDHK",left=332, top=591, width=20,height=21,;
    CpPicture="opz.bmp", caption="", nPag=1;
    , ToolTipText = "Ripristina impostazioni di default";
    , HelpContextID = 199835338;
    , ImgSize = 16;
  , bGlobalFont=.t.

    proc oBtn_1_29.Click()
      with this.Parent.oContained
        ThisForm.RestoreSetting()
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_30 as GadgetButton with uid="KLZMOWWYZT",left=403, top=4, width=17,height=18,;
    CpPicture="gd_refresh.bmp", caption="", nPag=1;
    , ToolTipText = "Aggiorna gadget";
    , HelpContextID = 199835114;
    , ImgSize = 16;
  , bGlobalFont=.t.

    proc oBtn_1_30.Click()
      with this.Parent.oContained
        ThisForm.oParentObject.NotifyEvent("GadgetOnDemand", !ThisForm.oParentObject.bUpdateHiddenGadget)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oMGHIDREF_1_34 as StdCheck with uid="ZNXEOFKVLS",rtseq=10,rtrep=.f.,left=163, top=498, caption="Aggiorna gadget non visibili",;
    ToolTipText = "Se attivo, vengono aggiornati tutti i gadget in uso, altrimenti solo i gadget visibili nell'area corrente",;
    HelpContextID = 89880308,;
    cFormVar="w_MGHIDREF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMGHIDREF_1_34.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oMGHIDREF_1_34.GetRadio()
    this.Parent.oContained.w_MGHIDREF = this.RadioValue()
    return .t.
  endfunc

  func oMGHIDREF_1_34.SetRadio()
    this.Parent.oContained.w_MGHIDREF=trim(this.Parent.oContained.w_MGHIDREF)
    this.value = ;
      iif(this.Parent.oContained.w_MGHIDREF=='S',1,;
      0)
  endfunc


  add object oMGDTHEME_1_35 as StdTableCombo with uid="YTZKOSMTCC",rtseq=11,rtrep=.f.,left=17,top=589,width=129,height=22;
    , bNoBackColor = .T.,cDescEmptyElement = "Nessuno";
    , ToolTipText = "Tema che verr� applicato ai gadget aggiunti da library";
    , HelpContextID = 34649845;
    , cFormVar="w_MGDTHEME",tablefilter="", bObbl = .f. , nPag = 1;
    , cTable='query/gsut_mgc.vqr',cKey='GCCODICE',cValue='GCDESCRI',cOrderBy='',xDefault=space(1);
  , bGlobalFont=.t.


  add object oMGDISRAP_1_38 as StdCheck with uid="CWIJKWJQOO",rtseq=12,rtrep=.f.,left=163, top=565, caption="Disposizione rapida",;
    ToolTipText = "Se attivo la disposizione dei gadget non viene interpolata (no animazione)",;
    HelpContextID = 194267414,;
    cFormVar="w_MGDISRAP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMGDISRAP_1_38.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oMGDISRAP_1_38.GetRadio()
    this.Parent.oContained.w_MGDISRAP = this.RadioValue()
    return .t.
  endfunc

  func oMGDISRAP_1_38.SetRadio()
    this.Parent.oContained.w_MGDISRAP=trim(this.Parent.oContained.w_MGDISRAP)
    this.value = ;
      iif(this.Parent.oContained.w_MGDISRAP=='S',1,;
      0)
  endfunc

  add object oStr_1_21 as StdString with uid="NFGNONRKWM",Visible=.t., Left=21, Top=489,;
    Alignment=0, Width=50, Height=18,;
    Caption="Sfondo"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="XSUASFESRX",Visible=.t., Left=433, Top=29,;
    Alignment=0, Width=15, Height=70,;
    Caption="My Gadget"  ;
    , FontName = "Tahoma", FontSize = 10, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_25 as StdString with uid="XQFGJBBNAL",Visible=.t., Left=21, Top=416,;
    Alignment=0, Width=70, Height=18,;
    Caption="Impostazioni"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="HMKVVUYHCC",Visible=.t., Left=21, Top=530,;
    Alignment=0, Width=98, Height=18,;
    Caption="Animazione"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="KBKXLQUYEV",Visible=.t., Left=21, Top=574,;
    Alignment=0, Width=98, Height=18,;
    Caption="Tema di default"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="HBKPUOHKLT",Visible=.t., Left=433, Top=137,;
    Alignment=0, Width=15, Height=249,;
    Caption="M O D I F I C A"  ;
    , FontName = "Tahoma", FontSize = 10, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_39 as StdString with uid="UBCLYOTZXW",Visible=.t., Left=433, Top=570,;
    Alignment=0, Width=17, Height=21,;
    Caption="<<"    , cRef='w_LBLOPEN';
  ;
    , FontName = "Open Sans", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_40 as StdString with uid="AJZAQSORDJ",Visible=.t., Left=433, Top=597,;
    Alignment=0, Width=17, Height=21,;
    Caption=">>"    , cRef='w_LBLCLOSE';
  ;
    , FontName = "Open Sans", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_1 as StdBox with uid="NWUCQCQOOQ",left=428, top=0, width=24,height=630

  add object oBox_1_2 as StdBox with uid="JULSOGRZYT",left=358, top=0, width=93,height=630

  add object oBox_1_3 as StdBox with uid="SIKEDTQLFH",left=7, top=411, width=414,height=210

  add object oBox_1_4 as StdBox with uid="ZSZLBMFFYV",left=430, top=7, width=1,height=475

  add object oBox_1_5 as StdBox with uid="OVKZPALIML",left=14, top=431, width=122,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kmg','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
