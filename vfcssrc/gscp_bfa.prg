* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscp_bfa                                                        *
*              Allega formati                                                  *
*                                                                              *
*      Author: ZUCCHETTI S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_64]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-05-20                                                      *
* Last revis.: 2009-04-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pSTEP
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscp_bfa",oParentObject,m.pSTEP)
return(i_retval)

define class tgscp_bfa as StdBatch
  * --- Local variables
  pSTEP = 0
  w_CODAZI = space(5)
  w_ROWNUM = 0
  w_TMPN = 0
  w_DETIPDES = space(1)
  w_DECODGRU = 0
  w_DECODROL = 0
  w_DETIPCON = space(1)
  w_DECODCON = space(15)
  w_DECODAGE = space(5)
  w_DEVALDES = space(50)
  w_DE__READ = space(1)
  w_DE_WRITE = space(1)
  w_DEDELETE = space(1)
  w_RETVAL = 0
  w_TIPO = space(1)
  w_FILEXML = space(254)
  w_NHF = 0
  w_TMPC = space(254)
  w_TMPN1 = 0
  w_CODAGE = space(5)
  w_CODCON = space(15)
  * --- WorkFile variables
  ZPARAMGEST_idx=0
  ZDESTIN_idx=0
  CONTROPA_idx=0
  CONTI_idx=0
  AGENTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- LA SEGUENTE ROUTINE VIENE CHIAMATA DA GSUT_BPX (Corporate Portal attivo)
    * --- Inizializzo la variabile di ritorno, se vale -1 significa che si � verificato un errore e l'operazione
    *     sar� sospesa.
    this.w_RETVAL = 0
    do case
      case this.pSTEP=1
        * --- Predetermina nome file ...
        this.oParentObject.w_ZCPFNAME = alltrim(i_CODAZI)+"_"+alltrim(str(i_CODUTE,5,0))+"_"+SYS(2015)
        this.oParentObject.w_ZCPFTITLE = IIF(NOT EMPTY(g_ZCPALLENAME), g_ZCPALLENAME, AllTrim(this.oParentObject.w_DescReport)+".PDF")
        this.oParentObject.w_ZCPFDESCRI = IIF(NOT EMPTY(g_ZCPALLETITLE), g_ZCPALLETITLE, this.oParentObject.w_DescReport)
        this.oParentObject.w_ZCPFITYPE = IIF(NOT EMPTY(g_ZCPFILETYPE), g_ZCPFILETYPE, "GENER")
        this.oParentObject.w_ZCPFIPKEY = IIF(NOT EMPTY(g_ZCPFILEPKEY), g_ZCPFILEPKEY, "No Owner")
        * --- Inizializzo i valori delle variabili presenti in "GSUT2KXP"
        this.oParentObject.w_TIPDES1 = IIF(VarType(g_ZCPTINTEST)="C" and g_ZCPTINTEST$"CFGA", g_ZCPTINTEST, "C")
        this.oParentObject.w_TIPCON1 = IIF(VarType(g_ZCPTINTEST)="C" and g_ZCPTINTEST$"CF", g_ZCPTINTEST, "C")
        this.oParentObject.w_CODCON1 = IIF(VarType(g_ZCPTINTEST)="C" and g_ZCPTINTEST$"CF", g_ZCPCINTEST, " ")
        this.oParentObject.w_CODAGE1 = IIF(VarType(g_ZCPTINTEST)="C" and g_ZCPTINTEST$"A", g_ZCPCINTEST, " ")
        this.oParentObject.w_CODGRU1 = IIF(VarType(g_ZCPTINTEST)="C" and g_ZCPTINTEST$"G", g_ZCPCINTEST, 0)
        * --- Valorizzo il tipo per i rimanenti destinatari
        this.w_TMPN = 2
        do while this.w_TMPN <= 10
          nIndice = alltrim(str(this.w_TMPN,2,0))
          this.oParentObject.w_TIPDES&nIndice = "X"
          this.w_TMPN = this.w_TMPN + 1
        enddo
        * --- Predetermina Folder ...
        this.oParentObject.w_CODAZI = i_CODAZI
        * --- Read from ZPARAMGEST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ZPARAMGEST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ZPARAMGEST_idx,2],.t.,this.ZPARAMGEST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PACOMFOL,PACMNFOL"+;
            " from "+i_cTable+" ZPARAMGEST where ";
                +"PACODICE = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PACOMFOL,PACMNFOL;
            from (i_cTable) where;
                PACODICE = this.oParentObject.w_CODAZI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_FOLDCOMP = NVL(cp_ToDate(_read_.PACOMFOL),cp_NullValue(_read_.PACOMFOL))
          this.oParentObject.w_FOLDCOMU = NVL(cp_ToDate(_read_.PACMNFOL),cp_NullValue(_read_.PACMNFOL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.oParentObject.w_ZCPCFOLDER = IIF(this.oParentObject.w_TIPDES1 $ "CFA", this.oParentObject.w_FOLDCOMP, this.oParentObject.w_FOLDCOMU)
        * --- Lancia Maschera GSUT2KXP (Invio documenti)
        this.oParentObject.w_RISERVATO = "S"
        this.oParentObject.w_MaskRejected  = .T.
        * --- Chiama la maschera utilizzando come ambiente quello presente nell'oggetto GSUT_BXP
        do GSUT2KXP with this.oParentObject
        if this.oParentObject.w_MaskRejected
          ah_ErrorMsg("Operazione sospesa come richiesto","i","")
          this.w_RETVAL = -1
        else
          * --- Se, tra i "destinatari" � stato selezionato un agente, deve verificare se quell'agente � legato ad un fornitore, in tal caso deve 
          *     sostituire l'agente con il fornitore, altrimenti CPZ da errore (a CPZ mandiamo il fornitore e, come figlio, l'agente).
          if "A" $ this.oParentObject.w_TIPDES1+this.oParentObject.w_TIPDES2+this.oParentObject.w_TIPDES3+this.oParentObject.w_TIPDES4+this.oParentObject.w_TIPDES5+this.oParentObject.w_TIPDES6+this.oParentObject.w_TIPDES7+this.oParentObject.w_TIPDES8+this.oParentObject.w_TIPDES9+this.oParentObject.w_TIPDES10
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
      case this.pSTEP=2
        * --- SECONDA CHIAMATA - Creazione nome file ed estensione
        if this.oParentObject.w_FDFFile
          this.oParentObject.w_NomeFile = this.oParentObject.w_NomeRep+".FDF"
        else
          this.oParentObject.w_NomeFile = this.oParentObject.w_ZCPFNAME+"."+this.oParentObject.w_EXT
        endif
      case this.pSTEP=3
        * --- TERZA CHIAMATA - Ricerca path di destinazione ed eventuale creazione della cartella
        * --- Legge cartella dove depositare il file .PDF
        this.oParentObject.w_CODAZI = i_CODAZI
        * --- Read from CONTROPA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTROPA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2],.t.,this.CONTROPA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "COPATHDO"+;
            " from "+i_cTable+" CONTROPA where ";
                +"COCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            COPATHDO;
            from (i_cTable) where;
                COCODAZI = this.oParentObject.w_CODAZI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_DIRLAV = NVL(cp_ToDate(_read_.COPATHDO),cp_NullValue(_read_.COPATHDO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Elimina eventuale \ finale, poi la rimette ...
        this.oParentObject.w_DIRLAV = alltrim(this.oParentObject.w_DIRLAV)
        this.oParentObject.w_DIRLAV = IIF(Right(this.oParentObject.w_DIRLAV,1)="\", left(this.oParentObject.w_DIRLAV, Len(this.oParentObject.w_DIRLAV)-1), this.oParentObject.w_DIRLAV)
        * --- Se non c'� crea la cartella ...
        if Not(this.AH_ExistFolder(this.oParentObject.w_DIRLAV))
          if Not(this.AH_CreateFolder(this.oParentObject.w_DIRLAV))
            this.w_RETVAL = -1
          endif
        endif
      case this.pSTEP=4
        * --- QUARTA CHIAMATA - Inserimento dati relativi all'elaborazione in essere
        this.w_NHF = -1
        * --- Inserisce record testata ...
        this.oParentObject.w_DVPATHFI = this.oParentObject.w_NOMEFILE
        this.oParentObject.w_DVDESELA = LEFT(this.oParentObject.w_ZCPFDESCRI,100)
        this.oParentObject.w_DV__NOTE = this.oParentObject.w_ZCPFDESCRI
        this.oParentObject.w_DVDATCRE = i_DATSYS
        this.oParentObject.w_DV_PARAM = "NAME="+alltrim(this.oParentObject.w_ZCPFTITLE)+"."+this.oParentObject.w_EXT+"; "
        this.oParentObject.w_DV_PARAM = "NAME="+alltrim(this.oParentObject.w_ZCPFTITLE)+"; "
        if g_ICPZSTANDALONE or g_IZCP="A"
          this.w_FILEXML = LRTrim(this.oParentObject.w_NOMEFILE)+".xml"
          this.w_NHF = FCreate(this.w_FILEXML)
          if this.w_NHF>=0
            this.w_TmPC = '<?xml version="1.0" encoding="iso-8859-1"?>'
            this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
            this.w_TmPC = '<Document xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">'
            this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
            this.w_TmPC = "  <Name>" + this.ConvertXML(LRTrim(this.oParentObject.w_ZCPFTITLE)) + "</Name>"
            this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
            if this.oParentObject.w_ZCPTFOLDER="C"
              this.w_TmPC = "  <FolderCompany>%" + this.oParentObject.w_TIPDES1+IIF(this.oParentObject.w_TIPDES1="A",alltrim(this.oParentObject.w_CODAGE1),alltrim(this.oParentObject.w_CODCON1))+"%"+alltrim(this.oParentObject.w_ZCPPFOLDER) + "</FolderCompany>"
              this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
            else
              this.w_TmPC = "  <Folder>" + alltrim(this.oParentObject.w_ZCPPFOLDER) + "</Folder>"
              this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
            endif
            this.w_TmPC = "  <Description>" + this.ConvertXML(LRTrim(this.oParentObject.w_DVDESELA)) + "</Description>"
            this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
            this.w_TmPC = "  <Notes>" + this.ConvertXML(LRTrim(this.oParentObject.w_DV__NOTE)) + "</Notes>"
            this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
            this.w_TmPC = "  <AttachmentType>" + this.oParentObject.w_ZCPFITYPE + "</AttachmentType>"
            this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
            this.w_TmPC = "  <AttachmentOwnerCode>" + this.oParentObject.w_ZCPFIPKEY + "</AttachmentOwnerCode>"
            this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
            this.w_TmPC = "  <RemoveFileAfterUpload>S</RemoveFileAfterUpload>"
            this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
            this.w_TmPC = "  <Reserved>" + this.oParentObject.w_RISERVATO + "</Reserved>"
            this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
          endif
        else
          if this.oParentObject.w_ZCPTFOLDER="C"
            this.oParentObject.w_DV_PARAM = this.oParentObject.w_DV_PARAM+"FOLDERCOMPANY=%"+this.oParentObject.w_TIPDES1+IIF(this.oParentObject.w_TIPDES1="A",alltrim(this.oParentObject.w_CODAGE1),alltrim(this.oParentObject.w_CODCON1))+"%"+alltrim(this.oParentObject.w_ZCPPFOLDER)+"; "
          else
            this.oParentObject.w_DV_PARAM = this.oParentObject.w_DV_PARAM+"FOLDER="+alltrim(this.oParentObject.w_ZCPPFOLDER)+"; "
          endif
          this.oParentObject.w_DVCODELA = ZCPINDIV("TRA",this.oParentObject.w_DVDESELA,this.oParentObject.w_DVDATCRE,"S",this.oParentObject.w_DVPATHFI,this.oParentObject.w_DV_PARAM,this.oParentObject.w_DV__NOTE,this.oParentObject.w_RISERVATO,this.oParentObject.w_ZCPFITYPE,this.oParentObject.w_ZCPFIPKEY)
        endif
        * --- Inserisce Destinatari ...
        if this.oParentObject.w_RISERVATO="S"
          * --- Se stand-alone apre il tag Securities
          if (g_ICPZSTANDALONE or g_IZCP="A") and this.w_NHF>=0
            this.w_TmPC = "  <Securities>"
            this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
          endif
          * --- Go
          this.w_ROWNUM = 1
          this.w_TMPN = 1
          do while this.w_TMPN<=10
            * --- Check n-esimo destinatario ...
            nIndice = alltrim(str(this.w_TMPN,2,0))
            if this.oParentObject.w_TIPDES&nIndice <>"X"
              * --- Go
              this.w_DETIPDES = this.oParentObject.w_TIPDES&nIndice
              this.w_DECODGRU = this.oParentObject.w_CODGRU&nIndice
              this.w_DETIPCON = IIF(this.w_DETIPDES$"CF", this.w_DETIPDES, " ")
              this.w_DECODCON = IIF(this.w_DETIPDES$"CF", this.oParentObject.w_CODCON&nIndice, " ")
              this.w_DECODAGE = IIF(this.w_DETIPDES="A", this.oParentObject.w_CODAGE&nIndice, " ")
              this.w_DECODROL = this.oParentObject.w_CODROL&nIndice
              this.w_DEVALDES = IIF(this.w_DETIPDES$"CF", this.w_DECODCON, IIF(this.w_DETIPDES="A", this.w_DECODAGE, IIF(this.w_DETIPDES="G", str(this.w_DECODGRU,6,0), " ")))
              this.w_DE__READ = this.oParentObject.w_READ&nIndice
              this.w_DE_WRITE = this.oParentObject.w_WRITE&nIndice
              this.w_DEDELETE = this.oParentObject.w_DELETE&nIndice
              if this.w_NHF>=0 and (g_ICPZSTANDALONE or g_IZCP="A")
                * --- Controlla se l'intestatario � "pieno"  ...
                this.w_TmPC = IIF(this.w_DETIPDES$"CF", this.w_DECODCON, IIF(this.w_DETIPDES="A", this.w_DECODAGE, IIF(this.w_DETIPDES="G", str(this.w_DECODGRU,6,0), " ")))
                if Not(Empty(this.w_TMPC))
                  * --- Se stand-alone apre il tag Security
                  this.w_TmPC = "    <Security>"
                  this.w_TMPN1 = FPUTS(this.w_NHF, this.w_TMPC)
                  this.w_TmPC = IIF(this.w_DETIPDES="C","CUSTOMER", IIF(this.w_DETIPDES="F", "SUPPLIER", IIF(this.w_DETIPDES="A","SALESAGENT",IIF(this.w_DETIPDES="G","GROUP"," "))))
                  this.w_TmPC = space(6)+"<EntityType>" + this.w_Tmpc + "</EntityType>"
                  this.w_TMPN1 = FPUTS(this.w_NHF, this.w_TMPC)
                  this.w_TmPC = IIF(this.w_DETIPDES$"CF", this.w_DETIPDES+this.w_DECODCON, IIF(this.w_DETIPDES="A", "A"+this.w_DECODAGE, IIF(this.w_DETIPDES="G", str(this.w_DECODGRU,6,0), " ")))
                  this.w_TmPC = space(6)+"<EntityCode>" + LRTrim(this.w_Tmpc) + "</EntityCode>"
                  this.w_TMPN1 = FPUTS(this.w_NHF, this.w_TMPC)
                  this.w_TmPC = space(6)+"<Read>" + this.w_DE__READ + "</Read>"
                  this.w_TMPN1 = FPUTS(this.w_NHF, this.w_TMPC)
                  this.w_TmPC = space(6)+"<Write>" + this.w_DE_WRITE + "</Write>"
                  this.w_TMPN1 = FPUTS(this.w_NHF, this.w_TMPC)
                  this.w_TmPC = space(6)+"<Delete>" + this.w_DEDELETE + "</Delete>"
                  this.w_TMPN1 = FPUTS(this.w_NHF, this.w_TMPC)
                  * --- Se stand-alone chiude il tag Security
                  this.w_TmPC = "    </Security>"
                  this.w_TMPN1 = FPUTS(this.w_NHF, this.w_TMPC)
                endif
              else
                * --- Insert into ZDESTIN
                i_nConn=i_TableProp[this.ZDESTIN_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.ZDESTIN_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_ccchkf=''
                i_ccchkv=''
                this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ZDESTIN_idx,i_nConn)
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                              " ("+"DECODELA"+",CPROWNUM"+",DETIPDES"+",DECODGRU"+",DETIPCON"+",DECODCON"+",DECODAGE"+",DECODROL"+",DEVALDES"+",DE__READ"+",DE_WRITE"+",DEDELETE"+i_ccchkf+") values ("+;
                  cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DVCODELA),'ZDESTIN','DECODELA');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'ZDESTIN','CPROWNUM');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_DETIPDES),'ZDESTIN','DETIPDES');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_DECODGRU),'ZDESTIN','DECODGRU');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_DETIPCON),'ZDESTIN','DETIPCON');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_DECODCON),'ZDESTIN','DECODCON');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_DECODAGE),'ZDESTIN','DECODAGE');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_DECODROL),'ZDESTIN','DECODROL');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_DEVALDES),'ZDESTIN','DEVALDES');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_DE__READ),'ZDESTIN','DE__READ');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_DE_WRITE),'ZDESTIN','DE_WRITE');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_DEDELETE),'ZDESTIN','DEDELETE');
                       +i_ccchkv+")")
                else
                  cp_CheckDeletedKey(i_cTable,0,'DECODELA',this.oParentObject.w_DVCODELA,'CPROWNUM',this.w_ROWNUM,'DETIPDES',this.w_DETIPDES,'DECODGRU',this.w_DECODGRU,'DETIPCON',this.w_DETIPCON,'DECODCON',this.w_DECODCON,'DECODAGE',this.w_DECODAGE,'DECODROL',this.w_DECODROL,'DEVALDES',this.w_DEVALDES,'DE__READ',this.w_DE__READ,'DE_WRITE',this.w_DE_WRITE,'DEDELETE',this.w_DEDELETE)
                  insert into (i_cTable) (DECODELA,CPROWNUM,DETIPDES,DECODGRU,DETIPCON,DECODCON,DECODAGE,DECODROL,DEVALDES,DE__READ,DE_WRITE,DEDELETE &i_ccchkf. );
                     values (;
                       this.oParentObject.w_DVCODELA;
                       ,this.w_ROWNUM;
                       ,this.w_DETIPDES;
                       ,this.w_DECODGRU;
                       ,this.w_DETIPCON;
                       ,this.w_DECODCON;
                       ,this.w_DECODAGE;
                       ,this.w_DECODROL;
                       ,this.w_DEVALDES;
                       ,this.w_DE__READ;
                       ,this.w_DE_WRITE;
                       ,this.w_DEDELETE;
                       &i_ccchkv. )
                  i_Rows=iif(bTrsErr,0,1)
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if i_Rows<0 or bTrsErr
                  * --- Error: insert not accepted
                  i_Error=MSG_INSERT_ERROR
                  return
                endif
              endif
              this.w_ROWNUM = this.w_ROWNUM + 1
            endif
            * --- Aggiorna contatore
            this.w_TMPN = this.w_TMPN+1
          enddo
          * --- Se stand-alone chiude il tag Securities
          if (g_ICPZSTANDALONE or g_IZCP="A") and this.w_NHF>=0
            this.w_TmPC = "  </Securities>"
            this.w_TMPN1 = FPUTS(this.w_NHF, this.w_TMPC)
          endif
        endif
        * --- Se stand-alone chiude il tag Securities
        if (g_ICPZSTANDALONE or g_IZCP="A") and this.w_NHF>=0
          this.w_TmPC = "</Document>"
          this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
          this.w_TMPN = FClose(this.w_NHF)
        endif
        * --- Messaggio Finale
        ah_ErrorMsg("Preparazione file per invio a Corporate Portal Zucchetti completata con successo","i","")
    endcase
    * --- --
    i_retcode = 'stop'
    i_retval = this.w_RETVAL
    return
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Trattamento agenti legati a fornitori
    if this.oParentObject.w_TIPDES1="A" and Not(Empty(this.oParentObject.w_CODAGE1))
      * --- Legge presenza fornitore su anagrafica agente
      this.w_CODAGE = this.oParentObject.w_CODAGE1
      * --- Read from AGENTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AGENTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2],.t.,this.AGENTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AGCODFOR"+;
          " from "+i_cTable+" AGENTI where ";
              +"AGCODAGE = "+cp_ToStrODBC(this.w_CODAGE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AGCODFOR;
          from (i_cTable) where;
              AGCODAGE = this.w_CODAGE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODCON = NVL(cp_ToDate(_read_.AGCODFOR),cp_NullValue(_read_.AGCODFOR))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if Not(Empty(this.w_CODCON))
        * --- Sostituisce l'agente con il fornitore ....
        this.oParentObject.w_TIPDES1 = "F"
        this.oParentObject.w_TIPCON1 = "F"
        this.oParentObject.w_CODCON1 = this.w_CODCON
      endif
    endif
    * --- --
    if this.oParentObject.w_TIPDES2="A" and Not(Empty(this.oParentObject.w_CODAGE2))
      * --- Legge presenza fornitore su anagrafica agente
      this.w_CODAGE = this.oParentObject.w_CODAGE2
      * --- Read from AGENTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AGENTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2],.t.,this.AGENTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AGCODFOR"+;
          " from "+i_cTable+" AGENTI where ";
              +"AGCODAGE = "+cp_ToStrODBC(this.w_CODAGE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AGCODFOR;
          from (i_cTable) where;
              AGCODAGE = this.w_CODAGE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODCON = NVL(cp_ToDate(_read_.AGCODFOR),cp_NullValue(_read_.AGCODFOR))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if Not(Empty(this.w_CODCON))
        * --- Sostituisce l'agente con il fornitore ....
        this.oParentObject.w_TIPDES2 = "F"
        this.oParentObject.w_TIPCON2 = "F"
        this.oParentObject.w_CODCON2 = this.w_CODCON
      endif
    endif
    * --- --
    if this.oParentObject.w_TIPDES3="A" and Not(Empty(this.oParentObject.w_CODAGE3))
      * --- Legge presenza fornitore su anagrafica agente
      this.w_CODAGE = this.oParentObject.w_CODAGE3
      * --- Read from AGENTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AGENTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2],.t.,this.AGENTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AGCODFOR"+;
          " from "+i_cTable+" AGENTI where ";
              +"AGCODAGE = "+cp_ToStrODBC(this.w_CODAGE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AGCODFOR;
          from (i_cTable) where;
              AGCODAGE = this.w_CODAGE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODCON = NVL(cp_ToDate(_read_.AGCODFOR),cp_NullValue(_read_.AGCODFOR))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if Not(Empty(this.w_CODCON))
        * --- Sostituisce l'agente con il fornitore ....
        this.oParentObject.w_TIPDES3 = "F"
        this.oParentObject.w_TIPCON3 = "F"
        this.oParentObject.w_CODCON3 = this.w_CODCON
      endif
    endif
    * --- --
    if this.oParentObject.w_TIPDES4="A" and Not(Empty(this.oParentObject.w_CODAGE4))
      * --- Legge presenza fornitore su anagrafica agente
      this.w_CODAGE = this.oParentObject.w_CODAGE4
      * --- Read from AGENTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AGENTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2],.t.,this.AGENTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AGCODFOR"+;
          " from "+i_cTable+" AGENTI where ";
              +"AGCODAGE = "+cp_ToStrODBC(this.w_CODAGE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AGCODFOR;
          from (i_cTable) where;
              AGCODAGE = this.w_CODAGE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODCON = NVL(cp_ToDate(_read_.AGCODFOR),cp_NullValue(_read_.AGCODFOR))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if Not(Empty(this.w_CODCON))
        * --- Sostituisce l'agente con il fornitore ....
        this.oParentObject.w_TIPDES4 = "F"
        this.oParentObject.w_TIPCON4 = "F"
        this.oParentObject.w_CODCON4 = this.w_CODCON
      endif
    endif
    * --- --
    if this.oParentObject.w_TIPDES5="A" and Not(Empty(this.oParentObject.w_CODAGE5))
      * --- Legge presenza fornitore su anagrafica agente
      this.w_CODAGE = this.oParentObject.w_CODAGE5
      * --- Read from AGENTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AGENTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2],.t.,this.AGENTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AGCODFOR"+;
          " from "+i_cTable+" AGENTI where ";
              +"AGCODAGE = "+cp_ToStrODBC(this.w_CODAGE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AGCODFOR;
          from (i_cTable) where;
              AGCODAGE = this.w_CODAGE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODCON = NVL(cp_ToDate(_read_.AGCODFOR),cp_NullValue(_read_.AGCODFOR))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if Not(Empty(this.w_CODCON))
        * --- Sostituisce l'agente con il fornitore ....
        this.oParentObject.w_TIPDES5 = "F"
        this.oParentObject.w_TIPCON5 = "F"
        this.oParentObject.w_CODCON5 = this.w_CODCON
      endif
    endif
    * --- --
    if this.oParentObject.w_TIPDES6="A" and Not(Empty(this.oParentObject.w_CODAGE6))
      * --- Legge presenza fornitore su anagrafica agente
      this.w_CODAGE = this.oParentObject.w_CODAGE6
      * --- Read from AGENTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AGENTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2],.t.,this.AGENTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AGCODFOR"+;
          " from "+i_cTable+" AGENTI where ";
              +"AGCODAGE = "+cp_ToStrODBC(this.w_CODAGE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AGCODFOR;
          from (i_cTable) where;
              AGCODAGE = this.w_CODAGE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODCON = NVL(cp_ToDate(_read_.AGCODFOR),cp_NullValue(_read_.AGCODFOR))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if Not(Empty(this.w_CODCON))
        * --- Sostituisce l'agente con il fornitore ....
        this.oParentObject.w_TIPDES6 = "F"
        this.oParentObject.w_TIPCON6 = "F"
        this.oParentObject.w_CODCON6 = this.w_CODCON
      endif
    endif
    * --- --
    if this.oParentObject.w_TIPDES7="A" and Not(Empty(this.oParentObject.w_CODAGE7))
      * --- Legge presenza fornitore su anagrafica agente
      this.w_CODAGE = this.oParentObject.w_CODAGE7
      * --- Read from AGENTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AGENTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2],.t.,this.AGENTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AGCODFOR"+;
          " from "+i_cTable+" AGENTI where ";
              +"AGCODAGE = "+cp_ToStrODBC(this.w_CODAGE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AGCODFOR;
          from (i_cTable) where;
              AGCODAGE = this.w_CODAGE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODCON = NVL(cp_ToDate(_read_.AGCODFOR),cp_NullValue(_read_.AGCODFOR))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if Not(Empty(this.w_CODCON))
        * --- Sostituisce l'agente con il fornitore ....
        this.oParentObject.w_TIPDES7 = "F"
        this.oParentObject.w_TIPCON7 = "F"
        this.oParentObject.w_CODCON7 = this.w_CODCON
      endif
    endif
    * --- --
    if this.oParentObject.w_TIPDES8="A" and Not(Empty(this.oParentObject.w_CODAGE8))
      * --- Legge presenza fornitore su anagrafica agente
      this.w_CODAGE = this.oParentObject.w_CODAGE8
      * --- Read from AGENTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AGENTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2],.t.,this.AGENTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AGCODFOR"+;
          " from "+i_cTable+" AGENTI where ";
              +"AGCODAGE = "+cp_ToStrODBC(this.w_CODAGE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AGCODFOR;
          from (i_cTable) where;
              AGCODAGE = this.w_CODAGE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODCON = NVL(cp_ToDate(_read_.AGCODFOR),cp_NullValue(_read_.AGCODFOR))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if Not(Empty(this.w_CODCON))
        * --- Sostituisce l'agente con il fornitore ....
        this.oParentObject.w_TIPDES8 = "F"
        this.oParentObject.w_TIPCON8 = "F"
        this.oParentObject.w_CODCON8 = this.w_CODCON
      endif
    endif
    * --- --
    if this.oParentObject.w_TIPDES9="A" and Not(Empty(this.oParentObject.w_CODAGE9))
      * --- Legge presenza fornitore su anagrafica agente
      this.w_CODAGE = this.oParentObject.w_CODAGE9
      * --- Read from AGENTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AGENTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2],.t.,this.AGENTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AGCODFOR"+;
          " from "+i_cTable+" AGENTI where ";
              +"AGCODAGE = "+cp_ToStrODBC(this.w_CODAGE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AGCODFOR;
          from (i_cTable) where;
              AGCODAGE = this.w_CODAGE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODCON = NVL(cp_ToDate(_read_.AGCODFOR),cp_NullValue(_read_.AGCODFOR))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if Not(Empty(this.w_CODCON))
        * --- Sostituisce l'agente con il fornitore ....
        this.oParentObject.w_TIPDES9 = "F"
        this.oParentObject.w_TIPCON9 = "F"
        this.oParentObject.w_CODCON9 = this.w_CODCON
      endif
    endif
    * --- --
    if this.oParentObject.w_TIPDES10="A" and Not(Empty(this.oParentObject.w_CODAGE10))
      * --- Legge presenza fornitore su anagrafica agente
      this.w_CODAGE = this.oParentObject.w_CODAGE10
      * --- Read from AGENTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AGENTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2],.t.,this.AGENTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AGCODFOR"+;
          " from "+i_cTable+" AGENTI where ";
              +"AGCODAGE = "+cp_ToStrODBC(this.w_CODAGE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AGCODFOR;
          from (i_cTable) where;
              AGCODAGE = this.w_CODAGE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODCON = NVL(cp_ToDate(_read_.AGCODFOR),cp_NullValue(_read_.AGCODFOR))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if Not(Empty(this.w_CODCON))
        * --- Sostituisce l'agente con il fornitore ....
        this.oParentObject.w_TIPDES10 = "F"
        this.oParentObject.w_TIPCON10 = "F"
        this.oParentObject.w_CODCON10 = this.w_CODCON
      endif
    endif
  endproc


  proc Init(oParentObject,pSTEP)
    this.pSTEP=pSTEP
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='ZPARAMGEST'
    this.cWorkTables[2]='ZDESTIN'
    this.cWorkTables[3]='CONTROPA'
    this.cWorkTables[4]='CONTI'
    this.cWorkTables[5]='AGENTI'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- gscp_bfa
    * ===============================================================================
    * AH_ExistFolder() - Funzione che verifica l'esistenza di una cartella
    *
    Function AH_ExistFolder(pDirectory,pDirName)
    Private n_File, sTemp,a_Dir,n_Dir,sDirName,lDirectory,lDirName,i
    * check parametri
    if vartype(pDirName)<>'C'
       lDirectory=substr(pDirectory,1,rat('\',pDirectory))
       lDirName=substr(pDirectory,rat('\',pDirectory)+1)
    else
       lDirectory=pDirectory
       lDirName=pDirName
    endif
    *
    n_File=0
    n_Dir = adir(a_Dir,lDirectory+iif(right(lDirectory,1)='\','','\')+"*","D")
    for i=1 to n_Dir
       sDirName = upper(alltrim(a_Dir(i,1)))      && ADIR ritorna senza \ finale
       if !((sDirName == ".") .or. (sDirName == "..")) and sDirName==alltrim(upper(LDirName))
         n_File=n_File+1
       endif
    next
    Return (n_File>0)
    
    * ===============================================================================
    * AH_CreateFolder() - Crea la cartella passata come parametro
    *
    Function AH_CreateFolder(pDirectory)
    private lOldErr,lRet
    * On Error
    lOldErr=ON("Error")
    * Init Risultato
    lRet = True
    * Crea
    on error lRet=False
    *
    pDirectory=alltrim(pDirectory)
    pDirectory=iif(right(pDirectory,1)="\", left(pDirectory,len(pDirectory)-1), pDirectory)
    *
    MD (pDirectory)
    *
    if not(Empty(lOldErr))
      on error (lOldErr)
    else
      on error
    endif   
    * 
    if not(lRet)
      = Ah_ErrorMsg("Impossibile creare la cartella %1",'stop',,pDirectory)
    endif
    * Risultato
    Return (lRet)
    
    * ===============================================================================
    * ConvertXML() - Converte i caratteri speciali per file XML
    *
    function ConvertXML(sXml)
    * Sostituzione
    sXml = STRTRAN(sXml,"&","&amp;")
    sXml = STRTRAN(sXml,"'","&apos;")
    sXml = STRTRAN(sXml,">","&gt;")
    sXml = STRTRAN(sXml,"<","&lt;")
    sXml = STRTRAN(sXml,CHR(34),"&quot;")
    sXml = STRTRAN(sXml,CHR(1),"")
    sXml = STRTRAN(sXml,CHR(2),"")
    sXml = STRTRAN(sXml,CHR(3),"")
    sXml = STRTRAN(sXml,CHR(4),"")
    sXml = STRTRAN(sXml,CHR(5),"")
    sXml = STRTRAN(sXml,CHR(6),"")
    sXml = STRTRAN(sXml,CHR(7),"")
    sXml = STRTRAN(sXml,CHR(8),"")
    sXml = STRTRAN(sXml,CHR(11),"")
    sXml = STRTRAN(sXml,CHR(12),"")
    sXml = STRTRAN(sXml,CHR(14),"")
    sXml = STRTRAN(sXml,CHR(15),"")
    sXml = STRTRAN(sXml,CHR(16),"")
    sXml = STRTRAN(sXml,CHR(17),"")
    sXml = STRTRAN(sXml,CHR(18),"")
    sXml = STRTRAN(sXml,CHR(19),"")
    sXml = STRTRAN(sXml,CHR(20),"")
    sXml = STRTRAN(sXml,CHR(21),"")
    sXml = STRTRAN(sXml,CHR(22),"")
    sXml = STRTRAN(sXml,CHR(23),"")
    sXml = STRTRAN(sXml,CHR(24),"")
    sXml = STRTRAN(sXml,CHR(25),"")
    sXml = STRTRAN(sXml,CHR(26),"")
    sXml = STRTRAN(sXml,CHR(27),"")
    sXml = STRTRAN(sXml,CHR(28),"")
    sXml = STRTRAN(sXml,CHR(29),"")
    sXml = STRTRAN(sXml,CHR(30),"")
    sXml = STRTRAN(sXml,CHR(31),"")
    sXml = STRTRAN(sXml,CHR(127),"")
    sXml = STRTRAN(sXml,CHR(129),"")
    sXml = STRTRAN(sXml,CHR(141),"")
    sXml = STRTRAN(sXml,CHR(143),"")
    sXml = STRTRAN(sXml,CHR(144),"")
    sXml = STRTRAN(sXml,CHR(157),"")
    sXml = STRTRAN(sXml,CHR(13)+CHR(10),"{nbsp;}")
    * Fine
    return (sXml)
  endfunc
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pSTEP"
endproc
