* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_btd                                                        *
*              Check causali documenti                                         *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_15]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-12-22                                                      *
* Last revis.: 2007-09-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_btd",oParentObject,m.pTipo)
return(i_retval)

define class tgsar_btd as StdBatch
  * --- Local variables
  pTipo = space(1)
  w_GSAC_MDC = .NULL.
  w_MESS = space(250)
  * --- WorkFile variables
  DOC_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato alla Check Form delle anagrafiche causali documento
    *     pTipo:
    *     CA Check all'F10 (causali di acquisto/vendite)
    *     ED Edited start verifica se la causale � movimentata o meno
    do case
      case this.pTipo="CA"
        if Upper( This.OparentObject.Class )="TGSAC_ATD"
          this.w_GSAC_MDC = This.oParentObject.GSAC_MDC
        else
          this.w_GSAC_MDC = This.oParentObject.GSVE_MDC
        endif
        * --- Se w_TDFLMGPR='F' (Forzato) il magazzino principale e' obbligatorio
        if this.oParentObject.w_TDFLMGPR="F" and not empty(this.oParentObject.w_TDCAUMAG) and empty(this.oParentObject.w_TDCODMAG)
          this.oParentObject.w_RESCHK = 1
          ah_ErrorMsg("Codice magazzino principale non definito",,"")
          i_retcode = 'stop'
          return
        endif
        * --- Se w_TDFLMTPR='F' (Forzato) il magazzino collegato e' obbligatorio
        if this.oParentObject.w_TDFLMTPR="F" and not empty(this.oParentObject.w_CAUCOL) and empty(.w_TDCODMAT)
          this.oParentObject.w_RESCHK = 1
          ah_ErrorMsg("Codice magazzino collegato non definito",,"")
          i_retcode = 'stop'
          return
        endif
        * --- Scorro i documenti collegati per verificare...
        this.w_GSAC_MDC.MarkPos()     
         
 Select ( this.w_GSAC_MDC.cTrsName ) 
 Go Top
        do while Not Eof( this.w_GSAC_MDC.cTrsName )
          if Not Empty(Nvl(t_DCCOLLEG,"")) And Nvl(t_FLEVAS,"") <>"S" And Not Deleted() 
            if this.oParentObject.w_TDFLELAN ="S" AND NVL(t_FLELAN,"")="S"
              this.oParentObject.w_RESCHK = IIF(ah_YesNo("Sia la casuale principale %1 che la causale collegata %2 movimentano l'analitica.%0Confermi ugualmente?","", this.oParentObject.w_TDTIPDOC ,t_DCCOLLEG),0,1)
              if this.oParentObject.w_RESCHK<>0
                this.w_GSAC_MDC.RePos()     
                i_retcode = 'stop'
                return
              endif
            endif
            if this.oParentObject.w_FLELGM="S" AND NVL(t_FISCOL,"")="S"
              this.oParentObject.w_RESCHK = IIF(ah_YesNo("La causale collegata: %1 e il documento principale%0sono entrambi movimenti (di magazzino) fiscali%0%0Confermi ugualmente?","", t_DCCOLLEG),0,1)
              if this.oParentObject.w_RESCHK<>0
                this.w_GSAC_MDC.RePos()     
                i_retcode = 'stop'
                return
              endif
            endif
          endif
          if Not Eof( this.w_GSAC_MDC.cTrsName )
             
 Select ( this.w_GSAC_MDC.cTrsName ) 
 Skip
          endif
        enddo
        this.w_GSAC_MDC.RePos()     
        if this.oParentObject.w_TDASSCES="M" AND EMPTY(this.oParentObject.w_TDCAUCON)
          * --- Al checkform controlla Associazione Cespite (no movimento cespite se manca la causale contabile associata)
          this.oParentObject.w_RESCHK = 1
          ah_ErrorMsg("Il tipo associazione cespite non � congruente con le impostazioni correnti",,"")
          i_retcode = 'stop'
          return
        endif
        if IsAlt()
          GSAL_BCP(this,this.oParentObject.w_TDTIPDOC,this.oParentObject.w_TDCATDOC,this.oParentObject.w_TDDESDOC)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.pTipo="ED"
        * --- Se causale documentata disabilito il tipo...
        this.oParentObject.w_MOVIM = .f.
        * --- Select from DOC_MAST
        i_nConn=i_TableProp[this.DOC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" DOC_MAST ";
              +" where MVTIPDOC= "+cp_ToStrODBC(this.oParentObject.w_TDTIPDOC)+"";
               ,"_Curs_DOC_MAST")
        else
          select Count(*) As Conta from (i_cTable);
           where MVTIPDOC= this.oParentObject.w_TDTIPDOC;
            into cursor _Curs_DOC_MAST
        endif
        if used('_Curs_DOC_MAST')
          select _Curs_DOC_MAST
          locate for 1=1
          do while not(eof())
          this.oParentObject.w_MOVIM = Nvl ( _Curs_DOC_MAST.CONTA , 0 ) >0
            select _Curs_DOC_MAST
            continue
          enddo
          use
        endif
      case this.pTipo="AS"
        if this.oParentObject.w_TDASSCES="M" AND EMPTY(this.oParentObject.w_TDCAUCON)
          ah_ErrorMsg("Impostazione non congruente per mancanza della causale contabile",,"")
          this.oParentObject.w_TDASSCES = this.oParentObject.o_TDASSCES
        endif
    endcase
  endproc


  proc Init(oParentObject,pTipo)
    this.pTipo=pTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='DOC_MAST'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_DOC_MAST')
      use in _Curs_DOC_MAST
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo"
endproc
