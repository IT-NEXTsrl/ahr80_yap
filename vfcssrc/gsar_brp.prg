* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_brp                                                        *
*              Ripristina progressivi                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_24]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-03-29                                                      *
* Last revis.: 2011-07-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pFLVEAC,pANNDOC,pPRD,pNUMDOC,pALFDOC,pPRP,pNUMEST,pALFEST,pANNPRO,pCLADOC,pEMERIC
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_brp",oParentObject,m.pFLVEAC,m.pANNDOC,m.pPRD,m.pNUMDOC,m.pALFDOC,m.pPRP,m.pNUMEST,m.pALFEST,m.pANNPRO,m.pCLADOC,m.pEMERIC)
return(i_retval)

define class tgsar_brp as StdBatch
  * --- Local variables
  pFLVEAC = space(1)
  pANNDOC = space(4)
  pPRD = space(2)
  pNUMDOC = 0
  pALFDOC = space(10)
  pPRP = space(2)
  pNUMEST = 0
  pALFEST = space(10)
  pANNPRO = space(4)
  pCLADOC = space(2)
  pEMERIC = space(1)
  w_PRD = space(2)
  w_FLVEAC = space(1)
  w_ANNDOC = space(4)
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_ANNPRO = space(4)
  w_PRP = space(2)
  w_NUMEST = 0
  w_ALFEST = space(10)
  w_CLADOC = space(2)
  w_TESTNDOC = 0
  w_TESTPDOC = 0
  w_CHIAVE = space(100)
  w_OK_WARN = 0
  w_EMERIC = space(1)
  w_ULTIMINUMERI = space(10)
  * --- WorkFile variables
  DOC_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch per recupero Buchi numerazione Prograssivo nei documenti da GSVE_BMK
    *     Solo nel caso cancello il documento che contiene l'ultimo progressivo Utile
    this.w_PRD = this.pPRD
    this.w_FLVEAC = this.pFLVEAC
    this.w_ANNDOC = this.pANNDOC
    this.w_NUMDOC = this.pNUMDOC
    this.w_ALFDOC = this.pALFDOC
    this.w_ANNPRO = this.pANNPRO
    this.w_PRP = this.pPRP
    this.w_NUMEST = this.pNUMEST
    this.w_ALFEST = this.pALFEST
    this.w_CLADOC = this.pCLADOC
    this.w_EMERIC = IIF(VARTYPE(this.pEMERIC)="C" AND NOT EMPTY(this.pEMERIC), this.pEMERIC, this.w_FLVEAC)
    if (this.w_FLVEAC = "V" Or this.w_CLADOC="OR") Or (this.w_FLVEAC = "A" And this.w_PRD="DV" OR this.w_PRD="IV")
      this.w_TESTNDOC = this.w_NUMDOC
      i_Conn=i_TableProp[this.DOC_MAST_IDX, 3]
      cp_AskTableProg(this, i_Conn, "PRDOC", "i_codazi,w_ANNDOC,w_PRD,w_ALFDOC,w_NUMDOC")
      * --- Dopo aver ricalcolato il Numero documento, se il numero documento che sto cancellando �
      *     l'ultimo progressivo calcolato, riporto indietro anche il progressivo
      if this.w_TESTNDOC = this.w_NUMDOC - 1
        this.w_ULTIMINUMERI = SYS( 2015 )
        * --- Si estraggono i massimi numeri documento presenti in archivio
        VQ_EXEC( "QUERY\GSAR_BRP" , THIS , this.w_ULTIMINUMERI )
        if RECCOUNT( this.w_ULTIMINUMERI ) > 0
          SELECT( this.w_ULTIMINUMERI )
          GO TOP
          this.w_TESTNDOC = NVL( NUMDOC , 0 )
          * --- w_TESTNDOC � il massimo tra i numeri documento presenti in archivio
          USE
        else
          this.w_TESTNDOC = 0
        endif
        * --- Aggiorno il progressivo all'ultimo documento inserito
        this.w_CHIAVE = "prog\PRDOC\"+cp_ToStrODBC(alltrim(i_CODAZI))+"\"+cp_ToStrODBC(this.w_ANNDOC)+"\"+cp_ToStrODBC(this.w_PRD)+"\"+cp_ToStrODBC(this.w_ALFDOC)
        this.w_OK_WARN = sqlexec(i_Conn,"UPDATE CPWARN SET AUTONUM="+cp_ToStrODBC(this.w_TESTNDOC)+" WHERE TABLECODE="+cp_ToStrODBC(this.w_CHIAVE))
      endif
    endif
    if (this.w_FLVEAC = "A" OR this.w_EMERIC = "A") And this.w_ANNPRO<>"    "
      this.w_TESTPDOC = this.w_NUMEST
      i_Conn=i_TableProp[this.DOC_MAST_IDX, 3]
      cp_AskTableProg(this, i_Conn, "PRPRO", "i_codazi,w_ANNPRO,w_PRP,w_ALFEST,w_NUMEST")
      * --- Dopo aver ricalcolato il Numero documento, se il numero documento che sto cancellando �
      *     l'ultimo progressivo calcolato, riporto indietro anche il progressivo
      if this.w_TESTPDOC = this.w_NUMEST - 1
        this.w_ULTIMINUMERI = SYS( 2015 )
        * --- Si estraggono i massimi numeri protocollo presenti in archivio
        VQ_EXEC( "QUERY\GSAR2BRP" , THIS , this.w_ULTIMINUMERI )
        if RECCOUNT( this.w_ULTIMINUMERI ) > 0
          SELECT( this.w_ULTIMINUMERI )
          GO TOP
          this.w_TESTPDOC = NVL( NUMPRO , 0 )
          * --- w_TESTNDOC � il massimo tra i numeri protocollo presenti in archivio
          USE
        else
          this.w_TESTPDOC = 0
        endif
        * --- Aggiorno il progressivo al numero precedente
        this.w_CHIAVE = "prog\PRPRO\"+cp_ToStrODBC(alltrim(i_CODAZI))+"\"+cp_ToStrODBC(this.w_ANNPRO)+"\"+cp_ToStrODBC(this.w_PRP)+"\"+cp_ToStrODBC(this.w_ALFEST)
        this.w_OK_WARN = sqlexec(i_Conn,"UPDATE CPWARN SET AUTONUM="+cp_ToStrODBC(this.w_TESTPDOC)+" WHERE TABLECODE="+cp_ToStrODBC(this.w_CHIAVE))
      endif
    endif
  endproc


  proc Init(oParentObject,pFLVEAC,pANNDOC,pPRD,pNUMDOC,pALFDOC,pPRP,pNUMEST,pALFEST,pANNPRO,pCLADOC,pEMERIC)
    this.pFLVEAC=pFLVEAC
    this.pANNDOC=pANNDOC
    this.pPRD=pPRD
    this.pNUMDOC=pNUMDOC
    this.pALFDOC=pALFDOC
    this.pPRP=pPRP
    this.pNUMEST=pNUMEST
    this.pALFEST=pALFEST
    this.pANNPRO=pANNPRO
    this.pCLADOC=pCLADOC
    this.pEMERIC=pEMERIC
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='DOC_MAST'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pFLVEAC,pANNDOC,pPRD,pNUMDOC,pALFDOC,pPRP,pNUMEST,pALFEST,pANNPRO,pCLADOC,pEMERIC"
endproc
