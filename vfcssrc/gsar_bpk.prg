* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bpk                                                        *
*              Pagamenti controlli finali                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_2]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-11                                                      *
* Last revis.: 2015-03-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bpk",oParentObject)
return(i_retval)

define class tgsar_bpk as StdBatch
  * --- Local variables
  w_MESS = space(100)
  w_PADRE = .NULL.
  w_RIGA = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli Finali Pagamenti (da GSAR_APA)
    this.w_MESS = " "
    if NOT EMPTY(this.oParentObject.w_PACODICE)
      this.w_PADRE = this.oParentObject.Gsar_Mpa
      if this.w_PADRE.Numrow()=0
        this.w_MESS = Ah_MsgFormat("Dettaglio tipi pagamento non definito!")
      else
        this.w_PADRE.Exec_Select("TMP_PAG", "t_P2NUMRAT", "Val(t_P2NUMRAT)<>0 AND NOT EMPTY(t_P2MODPAG) and t_MPSPLPAY='S'", "", "", "")     
        if USED("TMP_PAG") and Reccount("TMP_PAG")>1
          this.w_MESS = Ah_MsgFormat("Esistono pi� tipi di pagamento con split payment attivo")
          Use in Select("TMP_PAG")
        endif
        if Empty(this.w_MESS)
          if USED("TMP_PAG") and Reccount("TMP_PAG")=1 And this.oParentObject.w_Paincass="S"
            this.w_MESS = Ah_MsgFormat("Il flag incassato non � attivabile se nel dettaglio%0 � presente una rata con tipologia pagamento split payment")
            Use in Select("TMP_PAG")
          endif
        endif
        if Empty(this.w_MESS)
          this.w_RIGA = this.w_PADRE.search("val(t_P2NUMRAT)<>0 AND NOT EMPTY(t_P2MODPAG) and t_MPSPLPAY='S' AND (t_P2FL_IVA=1 AND (t_P2FLNETT=1 OR t_P2FLSPES=1) or t_P2FL_IVA=0) And Not Deleted() ")
          if this.w_RIGA<>-1
            this.w_MESS = Ah_MsgFormat("Sulla tipologia pagamento split payment deve essere valorizzata solo l'IVA")
          endif
        endif
      endif
      if Not Empty(this.w_MESS)
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=this.w_MESS
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
