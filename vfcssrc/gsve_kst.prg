* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_kst                                                        *
*              Stampa schede di trasporto                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-09-29                                                      *
* Last revis.: 2010-01-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsve_kst",oParentObject))

* --- Class definition
define class tgsve_kst as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 790
  Height = 531+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-01-26"
  HelpContextID=99234409
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=38

  * --- Constant Properties
  _IDX = 0
  VETTORI_IDX = 0
  MAGAZZIN_IDX = 0
  GRUMERC_IDX = 0
  cPrg = "gsve_kst"
  cComment = "Stampa schede di trasporto"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_OBTEST = ctod('  /  /  ')
  w_MVCODVET = space(5)
  o_MVCODVET = space(5)
  w_DESVET = space(35)
  w_TDEMERIC = space(1)
  o_TDEMERIC = space(1)
  w_DTOBSO = ctod('  /  /  ')
  w_MVCODMAG = space(5)
  o_MVCODMAG = space(5)
  w_MGDESMAG = space(30)
  w_TDFLGETR = space(1)
  o_TDFLGETR = space(1)
  w_DTOBS2 = ctod('  /  /  ')
  w_MVDATINI = ctod('  /  /  ')
  o_MVDATINI = ctod('  /  /  ')
  w_MVDATFIN = ctod('  /  /  ')
  o_MVDATFIN = ctod('  /  /  ')
  w_MVFLRIEP = space(1)
  o_MVFLRIEP = space(1)
  w_MVFLFORZ = space(1)
  o_MVFLFORZ = space(1)
  w_MVMAGPAR = space(5)
  o_MVMAGPAR = space(5)
  w_MGDESPAR = space(30)
  w_MVGRUMER = space(5)
  w_GMDESCRI = space(35)
  w_MVTIPCOM = space(1)
  o_MVTIPCOM = space(1)
  w_MVRIFCOM = space(0)
  w_MVPIVCOM = space(12)
  w_MVTIPPRO = space(1)
  o_MVTIPPRO = space(1)
  w_MVRIFPRO = space(0)
  w_MVPIVPRO = space(12)
  w_MVTIPCAR = space(1)
  o_MVTIPCAR = space(1)
  w_MVRIFCAR = space(0)
  w_MVPIVCAR = space(12)
  w_MVEVEIST = space(0)
  w_CATDOC = space(2)
  w_MVCLADOC = space(2)
  w_FLVEAC = space(1)
  w_MVTIPELA = space(1)
  w_MVSERIAL = space(10)
  w_NUMDOCSE = 0
  o_NUMDOCSE = 0
  w_MVSTDDIC = space(50)
  w_MGGESCAR = space(1)
  w_MGRIFFOR = space(15)
  w_FLVARMOD = .F.
  w_MVCITPAR = space(30)
  w_CAUSALI = .NULL.
  w_DOCUMENTI = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_kstPag1","gsve_kst",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Filtri")
      .Pages(2).addobject("oPag","tgsve_kstPag2","gsve_kst",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezione documenti")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMVCODVET_1_5
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_CAUSALI = this.oPgFrm.Pages(1).oPag.CAUSALI
    this.w_DOCUMENTI = this.oPgFrm.Pages(2).oPag.DOCUMENTI
    DoDefault()
    proc Destroy()
      this.w_CAUSALI = .NULL.
      this.w_DOCUMENTI = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='VETTORI'
    this.cWorkTables[2]='MAGAZZIN'
    this.cWorkTables[3]='GRUMERC'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_OBTEST=ctod("  /  /  ")
      .w_MVCODVET=space(5)
      .w_DESVET=space(35)
      .w_TDEMERIC=space(1)
      .w_DTOBSO=ctod("  /  /  ")
      .w_MVCODMAG=space(5)
      .w_MGDESMAG=space(30)
      .w_TDFLGETR=space(1)
      .w_DTOBS2=ctod("  /  /  ")
      .w_MVDATINI=ctod("  /  /  ")
      .w_MVDATFIN=ctod("  /  /  ")
      .w_MVFLRIEP=space(1)
      .w_MVFLFORZ=space(1)
      .w_MVMAGPAR=space(5)
      .w_MGDESPAR=space(30)
      .w_MVGRUMER=space(5)
      .w_GMDESCRI=space(35)
      .w_MVTIPCOM=space(1)
      .w_MVRIFCOM=space(0)
      .w_MVPIVCOM=space(12)
      .w_MVTIPPRO=space(1)
      .w_MVRIFPRO=space(0)
      .w_MVPIVPRO=space(12)
      .w_MVTIPCAR=space(1)
      .w_MVRIFCAR=space(0)
      .w_MVPIVCAR=space(12)
      .w_MVEVEIST=space(0)
      .w_CATDOC=space(2)
      .w_MVCLADOC=space(2)
      .w_FLVEAC=space(1)
      .w_MVTIPELA=space(1)
      .w_MVSERIAL=space(10)
      .w_NUMDOCSE=0
      .w_MVSTDDIC=space(50)
      .w_MGGESCAR=space(1)
      .w_MGRIFFOR=space(15)
      .w_FLVARMOD=.f.
      .w_MVCITPAR=space(30)
      .oPgFrm.Page1.oPag.oObj_1_1.Calculate()
        .w_OBTEST = i_datsys
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_MVCODVET))
          .link_1_5('Full')
        endif
          .DoRTCalc(3,3,.f.)
        .w_TDEMERIC = 'V'
        .DoRTCalc(5,6,.f.)
        if not(empty(.w_MVCODMAG))
          .link_1_10('Full')
        endif
          .DoRTCalc(7,7,.f.)
        .w_TDFLGETR = 'N'
          .DoRTCalc(9,9,.f.)
        .w_MVDATINI = i_datsys
        .w_MVDATFIN = i_datsys
      .oPgFrm.Page1.oPag.CAUSALI.Calculate()
      .oPgFrm.Page2.oPag.DOCUMENTI.Calculate()
        .w_MVFLRIEP = IIF(.w_NUMDOCSE>1, .w_MVFLRIEP, 'N')
        .w_MVFLFORZ = IIF(.w_NUMDOCSE>1, .w_MVFLFORZ, 'N')
        .w_MVMAGPAR = SPACE(5)
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_MVMAGPAR))
          .link_2_6('Full')
        endif
          .DoRTCalc(15,15,.f.)
        .w_MVGRUMER = SPACE(5)
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_MVGRUMER))
          .link_2_9('Full')
        endif
          .DoRTCalc(17,17,.f.)
        .w_MVTIPCOM = 'M'
        .w_MVRIFCOM = ''
        .w_MVPIVCOM = ''
        .w_MVTIPPRO = 'M'
        .w_MVRIFPRO = ''
        .w_MVPIVPRO = ''
        .w_MVTIPCAR = 'M'
        .w_MVRIFCAR = ''
        .w_MVPIVCAR = ''
        .w_MVEVEIST = ''
        .w_CATDOC = 'DT'
        .w_MVCLADOC = 'DT'
        .w_FLVEAC = 'V'
        .w_MVTIPELA = IIF( .w_MVFLFORZ='S' OR .w_MVFLRIEP='S',  "M", "D")
          .DoRTCalc(32,32,.f.)
        .w_NUMDOCSE = 0
        .w_MVSTDDIC = "Vedasi documenti accompagnatori"
          .DoRTCalc(35,36,.f.)
        .w_FLVARMOD = .F.
        .w_MVCITPAR = SPACE(30)
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_38.enabled = this.oPgFrm.Page2.oPag.oBtn_2_38.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_39.enabled = this.oPgFrm.Page2.oPag.oBtn_2_39.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_33.enabled = this.oPgFrm.Page1.oPag.oBtn_1_33.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_34.enabled = this.oPgFrm.Page1.oPag.oBtn_1_34.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_40.enabled = this.oPgFrm.Page2.oPag.oBtn_2_40.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_41.enabled = this.oPgFrm.Page2.oPag.oBtn_2_41.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_42.enabled = this.oPgFrm.Page2.oPag.oBtn_2_42.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_1.Calculate()
        .oPgFrm.Page1.oPag.CAUSALI.Calculate()
        .oPgFrm.Page2.oPag.DOCUMENTI.Calculate()
        .DoRTCalc(1,11,.t.)
        if .o_NUMDOCSE<>.w_NUMDOCSE
            .w_MVFLRIEP = IIF(.w_NUMDOCSE>1, .w_MVFLRIEP, 'N')
        endif
        if .o_MVFLRIEP<>.w_MVFLRIEP.or. .o_NUMDOCSE<>.w_NUMDOCSE
            .w_MVFLFORZ = IIF(.w_NUMDOCSE>1, .w_MVFLFORZ, 'N')
        endif
        if .o_MVFLRIEP<>.w_MVFLRIEP.or. .o_MVFLFORZ<>.w_MVFLFORZ
            .w_MVMAGPAR = SPACE(5)
          .link_2_6('Full')
        endif
        .DoRTCalc(15,15,.t.)
        if .o_MVFLRIEP<>.w_MVFLRIEP.or. .o_MVFLFORZ<>.w_MVFLFORZ
            .w_MVGRUMER = SPACE(5)
          .link_2_9('Full')
        endif
        .DoRTCalc(17,17,.t.)
        if .o_MVFLRIEP<>.w_MVFLRIEP.or. .o_MVFLFORZ<>.w_MVFLFORZ
            .w_MVTIPCOM = 'M'
        endif
        if .o_MVFLRIEP<>.w_MVFLRIEP.or. .o_MVFLFORZ<>.w_MVFLFORZ.or. .o_MVTIPCOM<>.w_MVTIPCOM
            .w_MVRIFCOM = ''
        endif
        if .o_MVFLRIEP<>.w_MVFLRIEP.or. .o_MVFLFORZ<>.w_MVFLFORZ.or. .o_MVTIPCOM<>.w_MVTIPCOM
            .w_MVPIVCOM = ''
        endif
        if .o_MVFLRIEP<>.w_MVFLRIEP.or. .o_MVFLFORZ<>.w_MVFLFORZ
            .w_MVTIPPRO = 'M'
        endif
        if .o_MVFLRIEP<>.w_MVFLRIEP.or. .o_MVFLFORZ<>.w_MVFLFORZ.or. .o_MVTIPPRO<>.w_MVTIPPRO
            .w_MVRIFPRO = ''
        endif
        if .o_MVFLRIEP<>.w_MVFLRIEP.or. .o_MVFLFORZ<>.w_MVFLFORZ.or. .o_MVTIPPRO<>.w_MVTIPPRO
            .w_MVPIVPRO = ''
        endif
        if .o_MVFLRIEP<>.w_MVFLRIEP.or. .o_MVFLFORZ<>.w_MVFLFORZ.or. .o_MVMAGPAR<>.w_MVMAGPAR
            .w_MVTIPCAR = 'M'
        endif
        if .o_MVFLRIEP<>.w_MVFLRIEP.or. .o_MVFLFORZ<>.w_MVFLFORZ.or. .o_MVTIPCAR<>.w_MVTIPCAR
            .w_MVRIFCAR = ''
        endif
        if .o_MVFLRIEP<>.w_MVFLRIEP.or. .o_MVFLFORZ<>.w_MVFLFORZ.or. .o_MVTIPCAR<>.w_MVTIPCAR
            .w_MVPIVCAR = ''
        endif
        if .o_MVFLRIEP<>.w_MVFLRIEP.or. .o_MVFLFORZ<>.w_MVFLFORZ
            .w_MVEVEIST = ''
        endif
        .DoRTCalc(28,30,.t.)
            .w_MVTIPELA = IIF( .w_MVFLFORZ='S' OR .w_MVFLRIEP='S',  "M", "D")
        if .o_MVCODVET<>.w_MVCODVET.or. .o_MVCODMAG<>.w_MVCODMAG.or. .o_MVDATINI<>.w_MVDATINI.or. .o_MVDATFIN<>.w_MVDATFIN.or. .o_TDEMERIC<>.w_TDEMERIC
          .Calculate_LMRFPHCUXC()
        endif
        .DoRTCalc(32,37,.t.)
        if .o_MVFLRIEP<>.w_MVFLRIEP.or. .o_MVMAGPAR<>.w_MVMAGPAR
            .w_MVCITPAR = SPACE(30)
        endif
        if .o_TDEMERIC<>.w_TDEMERIC.or. .o_TDFLGETR<>.w_TDFLGETR
          .Calculate_NUCFCTOJQO()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_1.Calculate()
        .oPgFrm.Page1.oPag.CAUSALI.Calculate()
        .oPgFrm.Page2.oPag.DOCUMENTI.Calculate()
    endwith
  return

  proc Calculate_WJOYGWVCNV()
    with this
          * --- Creazione tabelle temporanee
          gsve_bst(this;
              ,'LOAD';
             )
    endwith
  endproc
  proc Calculate_GSWNOXOJTA()
    with this
          * --- Avvia ricerca
          gsve_bst(this;
              ,'SEARCHPAG';
             )
    endwith
  endproc
  proc Calculate_YVHGDKAWMZ()
    with this
          * --- Chiusura maschera
          gsve_bst(this;
              ,'CLOSE';
             )
    endwith
  endproc
  proc Calculate_CQIQADBWJO()
    with this
          * --- Inizializzazioni
          gsve_bst(this;
              ,'INIT';
             )
    endwith
  endproc
  proc Calculate_UFYRNVNWAN()
    with this
          * --- Selezione / Deselezione documenti
          gsve_bst(this;
              ,"CHKDOC";
             )
    endwith
  endproc
  proc Calculate_LMRFPHCUXC()
    with this
          * --- Aggiorna flag aggiornamento ricerca
          .w_FLVARMOD = .T.
    endwith
  endproc
  proc Calculate_NUCFCTOJQO()
    with this
          * --- Aggiorna causali
          gsve_bst(this;
              ,'AGGCAU';
             )
          gsve_bst(this;
              ,'SELEZ';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page2.oPag.oMVFLRIEP_2_3.enabled = this.oPgFrm.Page2.oPag.oMVFLRIEP_2_3.mCond()
    this.oPgFrm.Page2.oPag.oMVFLFORZ_2_4.enabled = this.oPgFrm.Page2.oPag.oMVFLFORZ_2_4.mCond()
    this.oPgFrm.Page2.oPag.oMVMAGPAR_2_6.enabled = this.oPgFrm.Page2.oPag.oMVMAGPAR_2_6.mCond()
    this.oPgFrm.Page2.oPag.oMVGRUMER_2_9.enabled = this.oPgFrm.Page2.oPag.oMVGRUMER_2_9.mCond()
    this.oPgFrm.Page2.oPag.oMVTIPCOM_2_12.enabled = this.oPgFrm.Page2.oPag.oMVTIPCOM_2_12.mCond()
    this.oPgFrm.Page2.oPag.oMVRIFCOM_2_13.enabled = this.oPgFrm.Page2.oPag.oMVRIFCOM_2_13.mCond()
    this.oPgFrm.Page2.oPag.oMVPIVCOM_2_15.enabled = this.oPgFrm.Page2.oPag.oMVPIVCOM_2_15.mCond()
    this.oPgFrm.Page2.oPag.oMVTIPPRO_2_18.enabled = this.oPgFrm.Page2.oPag.oMVTIPPRO_2_18.mCond()
    this.oPgFrm.Page2.oPag.oMVRIFPRO_2_19.enabled = this.oPgFrm.Page2.oPag.oMVRIFPRO_2_19.mCond()
    this.oPgFrm.Page2.oPag.oMVPIVPRO_2_21.enabled = this.oPgFrm.Page2.oPag.oMVPIVPRO_2_21.mCond()
    this.oPgFrm.Page2.oPag.oMVTIPCAR_2_24.enabled = this.oPgFrm.Page2.oPag.oMVTIPCAR_2_24.mCond()
    this.oPgFrm.Page2.oPag.oMVRIFCAR_2_25.enabled = this.oPgFrm.Page2.oPag.oMVRIFCAR_2_25.mCond()
    this.oPgFrm.Page2.oPag.oMVPIVCAR_2_27.enabled = this.oPgFrm.Page2.oPag.oMVPIVCAR_2_27.mCond()
    this.oPgFrm.Page2.oPag.oMVEVEIST_2_29.enabled = this.oPgFrm.Page2.oPag.oMVEVEIST_2_29.mCond()
    this.oPgFrm.Page2.oPag.oMVSTDDIC_2_34.enabled = this.oPgFrm.Page2.oPag.oMVSTDDIC_2_34.mCond()
    this.oPgFrm.Page2.oPag.oMVCITPAR_2_46.enabled = this.oPgFrm.Page2.oPag.oMVCITPAR_2_46.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_38.enabled = this.oPgFrm.Page2.oPag.oBtn_2_38.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Pages(2).enabled=not(EMPTY(this.w_MVCODVET))
    this.oPgFrm.Page1.oPag.oTDEMERIC_1_7.visible=!this.oPgFrm.Page1.oPag.oTDEMERIC_1_7.mHide()
    this.oPgFrm.Page1.oPag.oTDFLGETR_1_12.visible=!this.oPgFrm.Page1.oPag.oTDFLGETR_1_12.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_35.visible=!this.oPgFrm.Page1.oPag.oStr_1_35.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_1.Event(cEvent)
        if lower(cEvent)==lower("FormLoad")
          .Calculate_WJOYGWVCNV()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.CAUSALI.Event(cEvent)
      .oPgFrm.Page2.oPag.DOCUMENTI.Event(cEvent)
        if lower(cEvent)==lower("ActivatePage 2")
          .Calculate_GSWNOXOJTA()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Done")
          .Calculate_YVHGDKAWMZ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank")
          .Calculate_CQIQADBWJO()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_documenti row checked") or lower(cEvent)==lower("w_documenti row unchecked")
          .Calculate_UFYRNVNWAN()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_causali row checked") or lower(cEvent)==lower("w_causali row unchecked")
          .Calculate_LMRFPHCUXC()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MVCODVET
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VETTORI_IDX,3]
    i_lTable = "VETTORI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VETTORI_IDX,2], .t., this.VETTORI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VETTORI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCODVET) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVT',True,'VETTORI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VTCODVET like "+cp_ToStrODBC(trim(this.w_MVCODVET)+"%");

          i_ret=cp_SQL(i_nConn,"select VTCODVET,VTDESVET,VTDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VTCODVET","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VTCODVET',trim(this.w_MVCODVET))
          select VTCODVET,VTDESVET,VTDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VTCODVET into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCODVET)==trim(_Link_.VTCODVET) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVCODVET) and !this.bDontReportError
            deferred_cp_zoom('VETTORI','*','VTCODVET',cp_AbsName(oSource.parent,'oMVCODVET_1_5'),i_cWhere,'GSAR_AVT',"VETTORI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VTCODVET,VTDESVET,VTDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where VTCODVET="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VTCODVET',oSource.xKey(1))
            select VTCODVET,VTDESVET,VTDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCODVET)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VTCODVET,VTDESVET,VTDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where VTCODVET="+cp_ToStrODBC(this.w_MVCODVET);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VTCODVET',this.w_MVCODVET)
            select VTCODVET,VTDESVET,VTDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCODVET = NVL(_Link_.VTCODVET,space(5))
      this.w_DESVET = NVL(_Link_.VTDESVET,space(35))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.VTDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MVCODVET = space(5)
      endif
      this.w_DESVET = space(35)
      this.w_DTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        endif
        this.w_MVCODVET = space(5)
        this.w_DESVET = space(35)
        this.w_DTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VETTORI_IDX,2])+'\'+cp_ToStr(_Link_.VTCODVET,1)
      cp_ShowWarn(i_cKey,this.VETTORI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCODVET Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCODMAG
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_MVCODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_MVCODMAG))
          select MGCODMAG,MGDESMAG,MGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVCODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oMVCODMAG_1_10'),i_cWhere,'GSAR_AMA',"MAGAZZINI",'GSMA2AMA.MAGAZZIN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_MVCODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_MVCODMAG)
            select MGCODMAG,MGDESMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_MGDESMAG = NVL(_Link_.MGDESMAG,space(30))
      this.w_DTOBS2 = NVL(cp_ToDate(_Link_.MGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MVCODMAG = space(5)
      endif
      this.w_MGDESMAG = space(30)
      this.w_DTOBS2 = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DTOBS2) OR .w_DTOBS2>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice magazzino inesistente oppure obsoleto")
        endif
        this.w_MVCODMAG = space(5)
        this.w_MGDESMAG = space(30)
        this.w_DTOBS2 = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVMAGPAR
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVMAGPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_MVMAGPAR)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO,MGGESCAR,MGRIFFOR";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_MVMAGPAR))
          select MGCODMAG,MGDESMAG,MGDTOBSO,MGGESCAR,MGRIFFOR;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVMAGPAR)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVMAGPAR) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oMVMAGPAR_2_6'),i_cWhere,'GSAR_AMA',"MAGAZZINI",'GSMA2AMA.MAGAZZIN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO,MGGESCAR,MGRIFFOR";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGDTOBSO,MGGESCAR,MGRIFFOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVMAGPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO,MGGESCAR,MGRIFFOR";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_MVMAGPAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_MVMAGPAR)
            select MGCODMAG,MGDESMAG,MGDTOBSO,MGGESCAR,MGRIFFOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVMAGPAR = NVL(_Link_.MGCODMAG,space(5))
      this.w_MGDESPAR = NVL(_Link_.MGDESMAG,space(30))
      this.w_DTOBS2 = NVL(cp_ToDate(_Link_.MGDTOBSO),ctod("  /  /  "))
      this.w_MGGESCAR = NVL(_Link_.MGGESCAR,space(1))
      this.w_MGRIFFOR = NVL(_Link_.MGRIFFOR,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_MVMAGPAR = space(5)
      endif
      this.w_MGDESPAR = space(30)
      this.w_DTOBS2 = ctod("  /  /  ")
      this.w_MGGESCAR = space(1)
      this.w_MGRIFFOR = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DTOBS2) OR .w_DTOBS2>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice magazzino inesistente oppure obsoleto")
        endif
        this.w_MVMAGPAR = space(5)
        this.w_MGDESPAR = space(30)
        this.w_DTOBS2 = ctod("  /  /  ")
        this.w_MGGESCAR = space(1)
        this.w_MGRIFFOR = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVMAGPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVGRUMER
  func Link_2_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVGRUMER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGM',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_MVGRUMER)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_MVGRUMER))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVGRUMER)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVGRUMER) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oMVGRUMER_2_9'),i_cWhere,'GSAR_AGM',"GRUPPI MERCEOLOGICI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVGRUMER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_MVGRUMER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_MVGRUMER)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVGRUMER = NVL(_Link_.GMCODICE,space(5))
      this.w_GMDESCRI = NVL(_Link_.GMDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_MVGRUMER = space(5)
      endif
      this.w_GMDESCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVGRUMER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMVCODVET_1_5.value==this.w_MVCODVET)
      this.oPgFrm.Page1.oPag.oMVCODVET_1_5.value=this.w_MVCODVET
    endif
    if not(this.oPgFrm.Page1.oPag.oDESVET_1_6.value==this.w_DESVET)
      this.oPgFrm.Page1.oPag.oDESVET_1_6.value=this.w_DESVET
    endif
    if not(this.oPgFrm.Page1.oPag.oTDEMERIC_1_7.RadioValue()==this.w_TDEMERIC)
      this.oPgFrm.Page1.oPag.oTDEMERIC_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMVCODMAG_1_10.value==this.w_MVCODMAG)
      this.oPgFrm.Page1.oPag.oMVCODMAG_1_10.value=this.w_MVCODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oMGDESMAG_1_11.value==this.w_MGDESMAG)
      this.oPgFrm.Page1.oPag.oMGDESMAG_1_11.value=this.w_MGDESMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oTDFLGETR_1_12.RadioValue()==this.w_TDFLGETR)
      this.oPgFrm.Page1.oPag.oTDFLGETR_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMVDATINI_1_14.value==this.w_MVDATINI)
      this.oPgFrm.Page1.oPag.oMVDATINI_1_14.value=this.w_MVDATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oMVDATFIN_1_16.value==this.w_MVDATFIN)
      this.oPgFrm.Page1.oPag.oMVDATFIN_1_16.value=this.w_MVDATFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oMVFLRIEP_2_3.RadioValue()==this.w_MVFLRIEP)
      this.oPgFrm.Page2.oPag.oMVFLRIEP_2_3.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oMVFLFORZ_2_4.RadioValue()==this.w_MVFLFORZ)
      this.oPgFrm.Page2.oPag.oMVFLFORZ_2_4.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oMVMAGPAR_2_6.value==this.w_MVMAGPAR)
      this.oPgFrm.Page2.oPag.oMVMAGPAR_2_6.value=this.w_MVMAGPAR
    endif
    if not(this.oPgFrm.Page2.oPag.oMGDESPAR_2_7.value==this.w_MGDESPAR)
      this.oPgFrm.Page2.oPag.oMGDESPAR_2_7.value=this.w_MGDESPAR
    endif
    if not(this.oPgFrm.Page2.oPag.oMVGRUMER_2_9.value==this.w_MVGRUMER)
      this.oPgFrm.Page2.oPag.oMVGRUMER_2_9.value=this.w_MVGRUMER
    endif
    if not(this.oPgFrm.Page2.oPag.oGMDESCRI_2_10.value==this.w_GMDESCRI)
      this.oPgFrm.Page2.oPag.oGMDESCRI_2_10.value=this.w_GMDESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oMVTIPCOM_2_12.RadioValue()==this.w_MVTIPCOM)
      this.oPgFrm.Page2.oPag.oMVTIPCOM_2_12.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oMVRIFCOM_2_13.value==this.w_MVRIFCOM)
      this.oPgFrm.Page2.oPag.oMVRIFCOM_2_13.value=this.w_MVRIFCOM
    endif
    if not(this.oPgFrm.Page2.oPag.oMVPIVCOM_2_15.value==this.w_MVPIVCOM)
      this.oPgFrm.Page2.oPag.oMVPIVCOM_2_15.value=this.w_MVPIVCOM
    endif
    if not(this.oPgFrm.Page2.oPag.oMVTIPPRO_2_18.RadioValue()==this.w_MVTIPPRO)
      this.oPgFrm.Page2.oPag.oMVTIPPRO_2_18.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oMVRIFPRO_2_19.value==this.w_MVRIFPRO)
      this.oPgFrm.Page2.oPag.oMVRIFPRO_2_19.value=this.w_MVRIFPRO
    endif
    if not(this.oPgFrm.Page2.oPag.oMVPIVPRO_2_21.value==this.w_MVPIVPRO)
      this.oPgFrm.Page2.oPag.oMVPIVPRO_2_21.value=this.w_MVPIVPRO
    endif
    if not(this.oPgFrm.Page2.oPag.oMVTIPCAR_2_24.RadioValue()==this.w_MVTIPCAR)
      this.oPgFrm.Page2.oPag.oMVTIPCAR_2_24.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oMVRIFCAR_2_25.value==this.w_MVRIFCAR)
      this.oPgFrm.Page2.oPag.oMVRIFCAR_2_25.value=this.w_MVRIFCAR
    endif
    if not(this.oPgFrm.Page2.oPag.oMVPIVCAR_2_27.value==this.w_MVPIVCAR)
      this.oPgFrm.Page2.oPag.oMVPIVCAR_2_27.value=this.w_MVPIVCAR
    endif
    if not(this.oPgFrm.Page2.oPag.oMVEVEIST_2_29.value==this.w_MVEVEIST)
      this.oPgFrm.Page2.oPag.oMVEVEIST_2_29.value=this.w_MVEVEIST
    endif
    if not(this.oPgFrm.Page2.oPag.oMVSTDDIC_2_34.value==this.w_MVSTDDIC)
      this.oPgFrm.Page2.oPag.oMVSTDDIC_2_34.value=this.w_MVSTDDIC
    endif
    if not(this.oPgFrm.Page2.oPag.oMVCITPAR_2_46.value==this.w_MVCITPAR)
      this.oPgFrm.Page2.oPag.oMVCITPAR_2_46.value=this.w_MVCITPAR
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_MVCODVET)) or not(EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVCODVET_1_5.SetFocus()
            i_bnoObbl = !empty(.w_MVCODVET)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure obsoleto")
          case   not(EMPTY(.w_DTOBS2) OR .w_DTOBS2>.w_OBTEST)  and not(empty(.w_MVCODMAG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVCODMAG_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice magazzino inesistente oppure obsoleto")
          case   not(EMPTY(.w_DTOBS2) OR .w_DTOBS2>.w_OBTEST)  and (.w_MVFLRIEP='S' OR .w_MVFLFORZ='S')  and not(empty(.w_MVMAGPAR))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMVMAGPAR_2_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice magazzino inesistente oppure obsoleto")
          case   not(.w_MVTIPCAR<>'Z' OR (.w_MVTIPCAR='Z' AND !EMPTY(.w_MVMAGPAR) AND .w_MGGESCAR='S'))  and (.w_MVFLRIEP='S' OR .w_MVFLFORZ='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMVTIPCAR_2_24.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impossibile valorizzare con 'da magazzino impostato' se non � stato specificato il magazzino di partenza o se questo non ha il flag gestione caricatore attivo")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MVCODVET = this.w_MVCODVET
    this.o_TDEMERIC = this.w_TDEMERIC
    this.o_MVCODMAG = this.w_MVCODMAG
    this.o_TDFLGETR = this.w_TDFLGETR
    this.o_MVDATINI = this.w_MVDATINI
    this.o_MVDATFIN = this.w_MVDATFIN
    this.o_MVFLRIEP = this.w_MVFLRIEP
    this.o_MVFLFORZ = this.w_MVFLFORZ
    this.o_MVMAGPAR = this.w_MVMAGPAR
    this.o_MVTIPCOM = this.w_MVTIPCOM
    this.o_MVTIPPRO = this.w_MVTIPPRO
    this.o_MVTIPCAR = this.w_MVTIPCAR
    this.o_NUMDOCSE = this.w_NUMDOCSE
    return

enddefine

* --- Define pages as container
define class tgsve_kstPag1 as StdContainer
  Width  = 786
  height = 531
  stdWidth  = 786
  stdheight = 531
  resizeXpos=471
  resizeYpos=274
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_1_1 as cp_outputCombo with uid="ZLERXIVPWT",left=172, top=504, width=427,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 78763238

  add object oMVCODVET_1_5 as StdField with uid="EGUZJFCERH",rtseq=2,rtrep=.f.,;
    cFormVar = "w_MVCODVET", cQueryName = "MVCODVET",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
    ToolTipText = "Codice vettore",;
    HelpContextID = 78207258,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=155, Top=17, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VETTORI", cZoomOnZoom="GSAR_AVT", oKey_1_1="VTCODVET", oKey_1_2="this.w_MVCODVET"

  func oMVCODVET_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCODVET_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVCODVET_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VETTORI','*','VTCODVET',cp_AbsName(this.parent,'oMVCODVET_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVT',"VETTORI",'',this.parent.oContained
  endproc
  proc oMVCODVET_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VTCODVET=this.parent.oContained.w_MVCODVET
     i_obj.ecpSave()
  endproc

  add object oDESVET_1_6 as StdField with uid="EGJDTKWSPE",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DESVET", cQueryName = "DESVET",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 46221110,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=219, Top=17, InputMask=replicate('X',35)


  add object oTDEMERIC_1_7 as StdCombo with uid="HLUINHXUQR",rtseq=4,rtrep=.f.,left=590,top=17,width=179,height=21;
    , ToolTipText = "Filtro su documenti emessi/ricevuti";
    , HelpContextID = 256415879;
    , cFormVar="w_TDEMERIC",RowSource=""+"Emessi,"+"Ricevuti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTDEMERIC_1_7.RadioValue()
    return(iif(this.value =1,'V',;
    iif(this.value =2,'A',;
    space(1))))
  endfunc
  func oTDEMERIC_1_7.GetRadio()
    this.Parent.oContained.w_TDEMERIC = this.RadioValue()
    return .t.
  endfunc

  func oTDEMERIC_1_7.SetRadio()
    this.Parent.oContained.w_TDEMERIC=trim(this.Parent.oContained.w_TDEMERIC)
    this.value = ;
      iif(this.Parent.oContained.w_TDEMERIC=='V',1,;
      iif(this.Parent.oContained.w_TDEMERIC=='A',2,;
      0))
  endfunc

  func oTDEMERIC_1_7.mHide()
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)<>'AD HOC ENTERPRISE')
    endwith
  endfunc

  add object oMVCODMAG_1_10 as StdField with uid="JQMTBKIWES",rtseq=6,rtrep=.f.,;
    cFormVar = "w_MVCODMAG", cQueryName = "MVCODMAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice magazzino inesistente oppure obsoleto",;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 195647757,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=155, Top=46, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_MVCODMAG"

  func oMVCODMAG_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCODMAG_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVCODMAG_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oMVCODMAG_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"MAGAZZINI",'GSMA2AMA.MAGAZZIN_VZM',this.parent.oContained
  endproc
  proc oMVCODMAG_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_MVCODMAG
     i_obj.ecpSave()
  endproc

  add object oMGDESMAG_1_11 as StdField with uid="LTPYJNFPOR",rtseq=7,rtrep=.f.,;
    cFormVar = "w_MGDESMAG", cQueryName = "MGDESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 210721293,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=219, Top=46, InputMask=replicate('X',30)

  add object oTDFLGETR_1_12 as StdCheck with uid="AIIACETIEP",rtseq=8,rtrep=.f.,left=480, top=46, caption="Visualizza documenti ciclo passivo",;
    HelpContextID = 64386952,;
    cFormVar="w_TDFLGETR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTDFLGETR_1_12.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTDFLGETR_1_12.GetRadio()
    this.Parent.oContained.w_TDFLGETR = this.RadioValue()
    return .t.
  endfunc

  func oTDFLGETR_1_12.SetRadio()
    this.Parent.oContained.w_TDFLGETR=trim(this.Parent.oContained.w_TDFLGETR)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLGETR=='S',1,;
      0)
  endfunc

  func oTDFLGETR_1_12.mHide()
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)='AD HOC ENTERPRISE')
    endwith
  endfunc

  add object oMVDATINI_1_14 as StdField with uid="MBAMMFGKSJ",rtseq=10,rtrep=.f.,;
    cFormVar = "w_MVDATINI", cQueryName = "MVDATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data iniziale documenti da selezionare",;
    HelpContextID = 124032753,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=155, Top=75

  add object oMVDATFIN_1_16 as StdField with uid="ZGGWMCDQDO",rtseq=11,rtrep=.f.,;
    cFormVar = "w_MVDATFIN", cQueryName = "MVDATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data finale documenti da selezionare",;
    HelpContextID = 174364396,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=382, Top=75


  add object CAUSALI as cp_szoombox with uid="VRCPQZMKJC",left=32, top=128, width=445,height=324,;
    caption='CAUSALI',;
   bGlobalFont=.t.,;
    cMenuFile="",cZoomOnZoom="",cTable="TIP_DOCU",cZoomFile="GSVECKST",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.t.,;
    cEvent = "Blank,AggCau",;
    nPag=1;
    , HelpContextID = 92380378


  add object oBtn_1_30 as StdButton with uid="CGUPKJZTWS",left=676, top=482, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , HelpContextID = 77687830;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_30.Click()
      with this.Parent.oContained
        gsve_bst(this.Parent.oContained,"SEARCH")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_30.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!EMPTY(.w_MVCODVET))
      endwith
    endif
  endfunc


  add object oBtn_1_31 as StdButton with uid="EPHAICRQJJ",left=727, top=482, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 223485958;
    , Caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_31.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_32 as StdButton with uid="WOOVXXVTWQ",left=31, top=455, width=48,height=45,;
    CpPicture="BMP\Check.bmp", caption="", nPag=1;
    , HelpContextID = 263493414;
    , Caption='\<Sel. tutti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_32.Click()
      with this.Parent.oContained
        gsve_bst(this.Parent.oContained,"SELEZ")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_33 as StdButton with uid="NWXMVNCBXX",left=83, top=455, width=48,height=45,;
    CpPicture="BMP\UnCheck.bmp", caption="", nPag=1;
    , HelpContextID = 5037318;
    , Caption='\<Desel. tutti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_33.Click()
      with this.Parent.oContained
        gsve_bst(this.Parent.oContained,"DESELEZ")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_34 as StdButton with uid="ZPJFCAWGCO",left=134, top=455, width=48,height=45,;
    CpPicture="BMP\InvCheck.bmp", caption="", nPag=1;
    , HelpContextID = 256835906;
    , Caption='\<Inv. selez.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_34.Click()
      with this.Parent.oContained
        gsve_bst(this.Parent.oContained,"INVSELEZ")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_4 as StdString with uid="FYEUQEBXHO",Visible=.t., Left=8, Top=19,;
    Alignment=1, Width=142, Height=18,;
    Caption="Vettore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="UUVVPMPLVL",Visible=.t., Left=8, Top=48,;
    Alignment=1, Width=142, Height=18,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="SMVWVXDOJW",Visible=.t., Left=8, Top=77,;
    Alignment=1, Width=142, Height=18,;
    Caption="Da data documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="SLWDEEJYRC",Visible=.t., Left=235, Top=77,;
    Alignment=1, Width=142, Height=18,;
    Caption="A data documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="XNFYGEIEBX",Visible=.t., Left=8, Top=110,;
    Alignment=0, Width=245, Height=18,;
    Caption="Selezione causali documento da visualizzare"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="HVWOGBOYOF",Visible=.t., Left=25, Top=506,;
    Alignment=1, Width=142, Height=18,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="IBQXHBMDNW",Visible=.t., Left=484, Top=19,;
    Alignment=1, Width=102, Height=18,;
    Caption="Documenti:"  ;
  , bGlobalFont=.t.

  func oStr_1_35.mHide()
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)<>'AD HOC ENTERPRISE')
    endwith
  endfunc
enddefine
define class tgsve_kstPag2 as StdContainer
  Width  = 786
  height = 531
  stdWidth  = 786
  stdheight = 531
  resizeXpos=518
  resizeYpos=153
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object DOCUMENTI as cp_szoombox with uid="WYSJJHGBBA",left=13, top=22, width=769,height=208,;
    caption='DOCUMENTI',;
   bGlobalFont=.t.,;
    cTable="DOC_MAST",cZoomFile="GSVE_KST",bOptions=.f.,bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",;
    cEvent = "Ricerca",;
    nPag=2;
    , HelpContextID = 197175782

  add object oMVFLRIEP_2_3 as StdCheck with uid="EWPWDCXXHW",rtseq=12,rtrep=.f.,left=320, top=229, caption="Stampa riepilogativa",;
    ToolTipText = "Se attivo stampa una sola scheda per tutti i documenti selezionati",;
    HelpContextID = 143034646,;
    cFormVar="w_MVFLRIEP", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oMVFLRIEP_2_3.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oMVFLRIEP_2_3.GetRadio()
    this.Parent.oContained.w_MVFLRIEP = this.RadioValue()
    return .t.
  endfunc

  func oMVFLRIEP_2_3.SetRadio()
    this.Parent.oContained.w_MVFLRIEP=trim(this.Parent.oContained.w_MVFLRIEP)
    this.value = ;
      iif(this.Parent.oContained.w_MVFLRIEP=='S',1,;
      0)
  endfunc

  func oMVFLRIEP_2_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NUMDOCSE>1 AND .w_MVFLFORZ<>'S')
    endwith
   endif
  endfunc

  add object oMVFLFORZ_2_4 as StdCheck with uid="TSYGNHFKAS",rtseq=13,rtrep=.f.,left=550, top=229, caption="Forza dati",;
    ToolTipText = "Se attivo forza i dati impostati manualmente nella stampa",;
    HelpContextID = 231115040,;
    cFormVar="w_MVFLFORZ", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oMVFLFORZ_2_4.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oMVFLFORZ_2_4.GetRadio()
    this.Parent.oContained.w_MVFLFORZ = this.RadioValue()
    return .t.
  endfunc

  func oMVFLFORZ_2_4.SetRadio()
    this.Parent.oContained.w_MVFLFORZ=trim(this.Parent.oContained.w_MVFLFORZ)
    this.value = ;
      iif(this.Parent.oContained.w_MVFLFORZ=='S',1,;
      0)
  endfunc

  func oMVFLFORZ_2_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVFLRIEP<>'S' AND .w_NUMDOCSE>0)
    endwith
   endif
  endfunc

  add object oMVMAGPAR_2_6 as StdField with uid="PFZUGKTJHE",rtseq=14,rtrep=.f.,;
    cFormVar = "w_MVMAGPAR", cQueryName = "MVMAGPAR",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice magazzino inesistente oppure obsoleto",;
    ToolTipText = "Codice magazzino di partenza merce",;
    HelpContextID = 248248600,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=448, Top=249, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_MVMAGPAR"

  func oMVMAGPAR_2_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVFLRIEP='S' OR .w_MVFLFORZ='S')
    endwith
   endif
  endfunc

  func oMVMAGPAR_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVMAGPAR_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVMAGPAR_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oMVMAGPAR_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"MAGAZZINI",'GSMA2AMA.MAGAZZIN_VZM',this.parent.oContained
  endproc
  proc oMVMAGPAR_2_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_MVMAGPAR
     i_obj.ecpSave()
  endproc

  add object oMGDESPAR_2_7 as StdField with uid="JEBUOEYMPI",rtseq=15,rtrep=.f.,;
    cFormVar = "w_MGDESPAR", cQueryName = "MGDESPAR",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 261052952,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=515, Top=249, InputMask=replicate('X',30)

  add object oMVGRUMER_2_9 as StdField with uid="FMHTZNEUYC",rtseq=16,rtrep=.f.,;
    cFormVar = "w_MVGRUMER", cQueryName = "MVGRUMER",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo merceologico",;
    HelpContextID = 213686552,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=448, Top=273, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", cZoomOnZoom="GSAR_AGM", oKey_1_1="GMCODICE", oKey_1_2="this.w_MVGRUMER"

  func oMVGRUMER_2_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVFLRIEP='S' OR .w_MVFLFORZ='S')
    endwith
   endif
  endfunc

  func oMVGRUMER_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVGRUMER_2_9.ecpDrop(oSource)
    this.Parent.oContained.link_2_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVGRUMER_2_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oMVGRUMER_2_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGM',"GRUPPI MERCEOLOGICI",'',this.parent.oContained
  endproc
  proc oMVGRUMER_2_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GMCODICE=this.parent.oContained.w_MVGRUMER
     i_obj.ecpSave()
  endproc

  add object oGMDESCRI_2_10 as StdField with uid="UHRYMIKMPI",rtseq=17,rtrep=.f.,;
    cFormVar = "w_GMDESCRI", cQueryName = "GMDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 42950575,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=515, Top=273, InputMask=replicate('X',35)


  add object oMVTIPCOM_2_12 as StdCombo with uid="FLODMHKKYE",rtseq=18,rtrep=.f.,left=149,top=304,width=213,height=21;
    , HelpContextID = 228300525;
    , cFormVar="w_MVTIPCOM",RowSource=""+"Destinatario / Cessionario,"+"Mittente / Cedente,"+"Per conto di,"+"Altro", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oMVTIPCOM_2_12.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'M',;
    iif(this.value =3,'P',;
    iif(this.value =4,'A',;
    space(1))))))
  endfunc
  func oMVTIPCOM_2_12.GetRadio()
    this.Parent.oContained.w_MVTIPCOM = this.RadioValue()
    return .t.
  endfunc

  func oMVTIPCOM_2_12.SetRadio()
    this.Parent.oContained.w_MVTIPCOM=trim(this.Parent.oContained.w_MVTIPCOM)
    this.value = ;
      iif(this.Parent.oContained.w_MVTIPCOM=='D',1,;
      iif(this.Parent.oContained.w_MVTIPCOM=='M',2,;
      iif(this.Parent.oContained.w_MVTIPCOM=='P',3,;
      iif(this.Parent.oContained.w_MVTIPCOM=='A',4,;
      0))))
  endfunc

  func oMVTIPCOM_2_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVFLRIEP='S' OR .w_MVFLFORZ='S')
    endwith
   endif
  endfunc

  add object oMVRIFCOM_2_13 as StdMemo with uid="KZXRJOYPEM",rtseq=19,rtrep=.f.,;
    cFormVar = "w_MVRIFCOM", cQueryName = "MVRIFCOM",;
    bObbl = .f. , nPag = 2, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 238794477,;
   bGlobalFont=.t.,;
    Height=52, Width=246, Left=149, Top=326

  func oMVRIFCOM_2_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVTIPCOM='A' AND (.w_MVFLRIEP='S' OR .w_MVFLFORZ='S'))
    endwith
   endif
  endfunc

  add object oMVPIVCOM_2_15 as StdField with uid="NFLUQOAMKT",rtseq=20,rtrep=.f.,;
    cFormVar = "w_MVPIVCOM", cQueryName = "MVPIVCOM",;
    bObbl = .f. , nPag = 2, value=space(12), bMultilanguage =  .f.,;
    HelpContextID = 222025453,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=149, Top=381, InputMask=replicate('X',12)

  func oMVPIVCOM_2_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVTIPCOM='A' AND (.w_MVFLRIEP='S' OR .w_MVFLFORZ='S'))
    endwith
   endif
  endfunc


  add object oMVTIPPRO_2_18 as StdCombo with uid="ZEHCZGSSLY",rtseq=21,rtrep=.f.,left=536,top=304,width=213,height=21;
    , HelpContextID = 258238741;
    , cFormVar="w_MVTIPPRO",RowSource=""+"Destinatario / Cessionario,"+"Mittente / Cedente,"+"Per conto di,"+"Altro", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oMVTIPPRO_2_18.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'M',;
    iif(this.value =3,'P',;
    iif(this.value =4,'A',;
    space(1))))))
  endfunc
  func oMVTIPPRO_2_18.GetRadio()
    this.Parent.oContained.w_MVTIPPRO = this.RadioValue()
    return .t.
  endfunc

  func oMVTIPPRO_2_18.SetRadio()
    this.Parent.oContained.w_MVTIPPRO=trim(this.Parent.oContained.w_MVTIPPRO)
    this.value = ;
      iif(this.Parent.oContained.w_MVTIPPRO=='D',1,;
      iif(this.Parent.oContained.w_MVTIPPRO=='M',2,;
      iif(this.Parent.oContained.w_MVTIPPRO=='P',3,;
      iif(this.Parent.oContained.w_MVTIPPRO=='A',4,;
      0))))
  endfunc

  func oMVTIPPRO_2_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVFLRIEP='S' OR .w_MVFLFORZ='S')
    endwith
   endif
  endfunc

  add object oMVRIFPRO_2_19 as StdMemo with uid="CLJESRVQXN",rtseq=22,rtrep=.f.,;
    cFormVar = "w_MVRIFPRO", cQueryName = "MVRIFPRO",;
    bObbl = .f. , nPag = 2, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 247744789,;
   bGlobalFont=.t.,;
    Height=52, Width=246, Left=536, Top=326

  func oMVRIFPRO_2_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVTIPPRO='A' AND (.w_MVFLRIEP='S' OR .w_MVFLFORZ='S'))
    endwith
   endif
  endfunc

  add object oMVPIVPRO_2_21 as StdField with uid="PIWGAPHXNL",rtseq=23,rtrep=.f.,;
    cFormVar = "w_MVPIVPRO", cQueryName = "MVPIVPRO",;
    bObbl = .f. , nPag = 2, value=space(12), bMultilanguage =  .f.,;
    HelpContextID = 264513813,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=536, Top=381, InputMask=replicate('X',12)

  func oMVPIVPRO_2_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVTIPPRO='A' AND (.w_MVFLRIEP='S' OR .w_MVFLFORZ='S'))
    endwith
   endif
  endfunc


  add object oMVTIPCAR_2_24 as StdCombo with uid="BPLVKOENAC",rtseq=24,rtrep=.f.,left=149,top=406,width=213,height=21;
    , HelpContextID = 40134936;
    , cFormVar="w_MVTIPCAR",RowSource=""+"Destinatario / Cessionario,"+"Mittente / Cedente,"+"Per conto di,"+"Da magazzino indicato,"+"Altro", bObbl = .f. , nPag = 2;
    , sErrorMsg = "Impossibile valorizzare con 'da magazzino impostato' se non � stato specificato il magazzino di partenza o se questo non ha il flag gestione caricatore attivo";
  , bGlobalFont=.t.


  func oMVTIPCAR_2_24.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'M',;
    iif(this.value =3,'P',;
    iif(this.value =4,'Z',;
    iif(this.value =5,'A',;
    space(1)))))))
  endfunc
  func oMVTIPCAR_2_24.GetRadio()
    this.Parent.oContained.w_MVTIPCAR = this.RadioValue()
    return .t.
  endfunc

  func oMVTIPCAR_2_24.SetRadio()
    this.Parent.oContained.w_MVTIPCAR=trim(this.Parent.oContained.w_MVTIPCAR)
    this.value = ;
      iif(this.Parent.oContained.w_MVTIPCAR=='D',1,;
      iif(this.Parent.oContained.w_MVTIPCAR=='M',2,;
      iif(this.Parent.oContained.w_MVTIPCAR=='P',3,;
      iif(this.Parent.oContained.w_MVTIPCAR=='Z',4,;
      iif(this.Parent.oContained.w_MVTIPCAR=='A',5,;
      0)))))
  endfunc

  func oMVTIPCAR_2_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVFLRIEP='S' OR .w_MVFLFORZ='S')
    endwith
   endif
  endfunc

  func oMVTIPCAR_2_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MVTIPCAR<>'Z' OR (.w_MVTIPCAR='Z' AND !EMPTY(.w_MVMAGPAR) AND .w_MGGESCAR='S'))
    endwith
    return bRes
  endfunc

  add object oMVRIFCAR_2_25 as StdMemo with uid="EACKBFTWZY",rtseq=25,rtrep=.f.,;
    cFormVar = "w_MVRIFCAR", cQueryName = "MVRIFCAR",;
    bObbl = .f. , nPag = 2, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 29640984,;
   bGlobalFont=.t.,;
    Height=52, Width=246, Left=149, Top=428

  func oMVRIFCAR_2_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVTIPCAR='A' AND (.w_MVFLRIEP='S' OR .w_MVFLFORZ='S'))
    endwith
   endif
  endfunc

  add object oMVPIVCAR_2_27 as StdField with uid="LOQXFYMRYA",rtseq=26,rtrep=.f.,;
    cFormVar = "w_MVPIVCAR", cQueryName = "MVPIVCAR",;
    bObbl = .f. , nPag = 2, value=space(12), bMultilanguage =  .f.,;
    HelpContextID = 46410008,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=149, Top=483, InputMask=replicate('X',12)

  func oMVPIVCAR_2_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVTIPCAR='A' AND (.w_MVFLRIEP='S' OR .w_MVFLFORZ='S'))
    endwith
   endif
  endfunc

  add object oMVEVEIST_2_29 as StdMemo with uid="VCHACTNOWI",rtseq=27,rtrep=.f.,;
    cFormVar = "w_MVEVEIST", cQueryName = "MVEVEIST",;
    bObbl = .f. , nPag = 2, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Istruzioni da riportare nell'apposito spazio della scheda di trasporto",;
    HelpContextID = 130054426,;
   bGlobalFont=.t.,;
    Height=48, Width=246, Left=536, Top=406

  func oMVEVEIST_2_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVFLRIEP='S' OR .w_MVFLFORZ='S')
    endwith
   endif
  endfunc

  add object oMVSTDDIC_2_34 as StdField with uid="INKEOGEJMU",rtseq=34,rtrep=.f.,;
    cFormVar = "w_MVSTDDIC", cQueryName = "MVSTDDIC",;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Dicitura sostitutiva da riportare in caso di stampa riepilogativa",;
    HelpContextID = 223389431,;
   bGlobalFont=.t.,;
    Height=21, Width=377, Left=149, Top=509, InputMask=replicate('X',50)

  func oMVSTDDIC_2_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVFLRIEP='S')
    endwith
   endif
  endfunc


  add object oBtn_2_38 as StdButton with uid="XTJRDSLCCL",left=676, top=482, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=2;
    , HelpContextID = 42555174;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_38.Click()
      with this.Parent.oContained
        gsve_bst(this.Parent.oContained,"PRINT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_38.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!EMPTY(.w_MVCODVET) AND .w_NUMDOCSE > 0 AND !EMPTY(.w_MVTIPCAR))
      endwith
    endif
  endfunc


  add object oBtn_2_39 as StdButton with uid="RDXLTNCSFC",left=727, top=482, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , HelpContextID = 223485958;
    , Caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_39.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_40 as StdButton with uid="VHLQXRDIKX",left=14, top=230, width=48,height=45,;
    CpPicture="BMP\Check.bmp", caption="", nPag=2;
    , HelpContextID = 263493414;
    , Caption='\<Sel. tutti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_40.Click()
      with this.Parent.oContained
        gsve_bst(this.Parent.oContained,"SELDOC")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_41 as StdButton with uid="KAASRXRQQM",left=66, top=230, width=48,height=45,;
    CpPicture="BMP\UnCheck.bmp", caption="", nPag=2;
    , HelpContextID = 5037318;
    , Caption='\<Desel. tutti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_41.Click()
      with this.Parent.oContained
        gsve_bst(this.Parent.oContained,"DESELDOC")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_42 as StdButton with uid="OGHAEFHVDF",left=117, top=230, width=48,height=45,;
    CpPicture="BMP\InvCheck.bmp", caption="", nPag=2;
    , HelpContextID = 256835906;
    , Caption='\<Inv. selez.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_42.Click()
      with this.Parent.oContained
        gsve_bst(this.Parent.oContained,"INVSELDOC")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oMVCITPAR_2_46 as StdField with uid="MAMPSDVBQE",rtseq=38,rtrep=.f.,;
    cFormVar = "w_MVCITPAR", cQueryName = "MVCITPAR",;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Indica il luogo di compilazione che verr� indicato nella stampa della scheda di trasporto riepilogativa",;
    HelpContextID = 262363416,;
   bGlobalFont=.t.,;
    Height=21, Width=246, Left=536, Top=457, InputMask=replicate('X',30)

  func oMVCITPAR_2_46.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVFLRIEP='S' AND EMPTY(.w_MVMAGPAR))
    endwith
   endif
  endfunc

  add object oStr_2_1 as StdString with uid="DXZYOYPMMR",Visible=.t., Left=12, Top=6,;
    Alignment=0, Width=270, Height=18,;
    Caption="Documenti selezionabili"  ;
  , bGlobalFont=.t.

  add object oStr_2_5 as StdString with uid="LADQBOMLTC",Visible=.t., Left=224, Top=251,;
    Alignment=1, Width=219, Height=18,;
    Caption="Magazzino di partenza:"  ;
  , bGlobalFont=.t.

  add object oStr_2_8 as StdString with uid="VFUIVYJOLU",Visible=.t., Left=246, Top=275,;
    Alignment=1, Width=197, Height=18,;
    Caption="Categoria merceologica:"  ;
  , bGlobalFont=.t.

  add object oStr_2_11 as StdString with uid="QIGRBLJPQO",Visible=.t., Left=8, Top=305,;
    Alignment=1, Width=136, Height=18,;
    Caption="Committente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_14 as StdString with uid="RMBMYNFRLH",Visible=.t., Left=8, Top=328,;
    Alignment=1, Width=136, Height=18,;
    Caption="Riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_16 as StdString with uid="PYTNEUKVSE",Visible=.t., Left=8, Top=383,;
    Alignment=1, Width=136, Height=18,;
    Caption="Partita I.V.A.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_17 as StdString with uid="IMJJVLHANU",Visible=.t., Left=398, Top=305,;
    Alignment=1, Width=134, Height=18,;
    Caption="Proprietario:"  ;
  , bGlobalFont=.t.

  add object oStr_2_20 as StdString with uid="NPWNXHFHQJ",Visible=.t., Left=398, Top=328,;
    Alignment=1, Width=134, Height=18,;
    Caption="Riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_22 as StdString with uid="CJXDEOLADE",Visible=.t., Left=398, Top=383,;
    Alignment=1, Width=134, Height=18,;
    Caption="Partita I.V.A.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_23 as StdString with uid="BBMIXIWTCO",Visible=.t., Left=8, Top=407,;
    Alignment=1, Width=136, Height=18,;
    Caption="Caricatore:"  ;
  , bGlobalFont=.t.

  add object oStr_2_26 as StdString with uid="WFZSUAZLIH",Visible=.t., Left=8, Top=430,;
    Alignment=1, Width=136, Height=18,;
    Caption="Riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_28 as StdString with uid="YPRSNIYWOC",Visible=.t., Left=8, Top=485,;
    Alignment=1, Width=136, Height=18,;
    Caption="Partita I.V.A.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_30 as StdString with uid="QSVQSPOYVA",Visible=.t., Left=403, Top=407,;
    Alignment=1, Width=129, Height=18,;
    Caption="Eventuali istruzioni:"  ;
  , bGlobalFont=.t.

  add object oStr_2_35 as StdString with uid="HKWKJQXVNM",Visible=.t., Left=8, Top=511,;
    Alignment=1, Width=136, Height=18,;
    Caption="Dicitura sostitutiva:"  ;
  , bGlobalFont=.t.

  add object oStr_2_47 as StdString with uid="TEFPJXAITB",Visible=.t., Left=403, Top=460,;
    Alignment=1, Width=129, Height=18,;
    Caption="Luogo compilazione:"  ;
  , bGlobalFont=.t.

  add object oBox_2_43 as StdBox with uid="LPJZFAYYQW",left=5, top=298, width=394,height=108

  add object oBox_2_44 as StdBox with uid="QNZJAVBSBT",left=5, top=404, width=394,height=103

  add object oBox_2_45 as StdBox with uid="CEUNFRCDAC",left=397, top=298, width=388,height=108
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_kst','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
