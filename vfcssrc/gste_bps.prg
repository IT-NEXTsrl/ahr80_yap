* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bps                                                        *
*              Stampa partite/scadenze                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_288]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-02                                                      *
* Last revis.: 2014-06-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bps",oParentObject)
return(i_retval)

define class tgste_bps as StdBatch
  * --- Local variables
  w_SERIAL = space(10)
  w_DATSCA = ctod("  /  /  ")
  w_LNUMPAR = space(31)
  w_CPROWNUM = 0
  w_OLDSERIAL = space(10)
  w_OLDCPROWNUM = 0
  w_LIVELLO = space(254)
  w_OLDLIVELLO = space(254)
  w_FLGOK = space(1)
  w_DATVAL = ctod("  /  /  ")
  w_LOOP = .f.
  * --- WorkFile variables
  OUTPUTMP_idx=0
  TMP_PART_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elabora stampa Partite/Scadenze (da GSTE_SIS)
    this.w_DATVAL = iif(Not Empty(this.oParentObject.w_DATPAR),this.oParentObject.w_DATPAR,i_DATSYS)
    this.oParentObject.w_FLGSOS = IIF(this.oParentObject.w_FLSOSP $ "TN", " ", "S")
    this.oParentObject.w_FLGNSO = IIF(this.oParentObject.w_FLSOSP $ "TS", " ", "S")
    if this.oParentObject.w_FLSALD="R"
      * --- Solo Partite Aperte
      * --- Lancio GSTE_BPA per ottenere la tabella temporanea "TMP_PART_APE" 
      *     contenente le partite aperte filtrate con le principali selezioni.
      GSTE_BPA(this,this.oParentObject.w_SCAINI, this.oParentObject.w_SCAFIN, this.oParentObject.w_TIPCON,this.oParentObject.w_CODCON,this.oParentObject.w_ACODCO,this.oParentObject.w_VALUTA,this.oParentObject.w_NUMPAR,; 
 this.oParentObject.w_PAGRB,this.oParentObject.w_PAGBO,this.oParentObject.w_PAGRD,this.oParentObject.w_PAGRI,this.oParentObject.w_PAGMA,this.oParentObject.w_PAGCA,"GSCG_BSA","N", this.oParentObject.w_DATPAR )
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Settando w_FLGOK='N' eseguo solo i filtri applicabili 
      this.w_FLGOK = "N"
      * --- Creo il cursore temporaneo da passare a GSTE_BCP
      * --- La query deve contenere alcuni campi minimi indispensabili 
      *     nel batch GSTE_BCP:
      *     Tipcon: Tipologia Cliente \Fornitore
      *     Datsca: Data Scadenza
      *     Numpar: Numero Partita
      *     Codcon: Da codice Cliente\Fornitore
      *     Acodco: A codice Cliente\Fornitore
      *     Codval: Codice Valuta
      *     Flindi: Flag Cont. Indiretta Effetti
      *     Totimp: Importo della Partita
      *     Ptserrif: Seriale Partita di Origine
      *     Ptordrif: Provenienza Partita di Origine
      *     Ptnumrif:Numero di Riga Partita di Origine
      *     Ptflragg: Flag Partite Raggruppate
      *     Flcrsa:  Flag Tipologia Partita  C\S\A
      *     Tipdoc: Campo Fittizzio asseganto nella query
      *     Segno: Segno della Partita D\A
      *     Impave: Importo partita in Dare
      *     Impdar: Importo partita in Avere
      *     Tippag: Tipologia Pagamento
      *     Caoape: Cambio di Apertura
      *     Caoval: Cambio della Partita
      *     Ptserial: Seriale Partita
      *     Ptroword: Provenienza Partita
      *     Cprownum: Numero di riga Partita
      vq_exec("QUERY\GSTE2SIS.VQR", this, "PARTITE")
      * --- Eseguo il batch GSTE_BCP per elaborare le partite aperte
      GSTE_BCP(this,"A","PARTITE"," ")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Riverso il cursore temporaneo fox in una tabella temporanea lato server
      CURTOTAB("PARTITE", "TMP_PART")
      * --- Settando w_FLGOK='S' eseguo tutti i filtri
      this.w_FLGOK = "S"
      * --- Creo la tabella temporanea per l'output utente
      * --- Create temporary table OUTPUTMP
      i_nIdx=cp_AddTableDef('OUTPUTMP') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('QUERY\GSTE3SIS',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.OUTPUTMP_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    else
      * --- Tutte - Saldate - Non Saldate
      * --- Settando w_FLGOK='S' eseguo tutti i filtri
      this.w_FLGOK = "S"
      * --- Creo la tabella temporanea per l'output utente
      * --- Create temporary table OUTPUTMP
      i_nIdx=cp_AddTableDef('OUTPUTMP') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('QUERY\GSTE_SIS',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.OUTPUTMP_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    endif
    * --- Controllo se ho selezionato Dettaglio Partite
    if this.oParentObject.w_FLORIG="S"
      * --- Dettaglio partite
      * --- Preparo TMP_PART ad ospitare l'esplosione delle scadenze raggruppate
      * --- Drop temporary table TMP_PART
      i_nIdx=cp_GetTableDefIdx('TMP_PART')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMP_PART')
      endif
      * --- La query GSTERSIS serve per dare la giusta struttura alla tabella TMP_PART
      * --- Create temporary table TMP_PART
      i_nIdx=cp_AddTableDef('TMP_PART') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('QUERY\GSTERSIS',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMP_PART_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      * --- Seleziono tutte le scadenze raggruppate e per ciascuna recupero 
      *     le scadenze figlie
      * --- Select from OUTPUTMP
      i_nConn=i_TableProp[this.OUTPUTMP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OUTPUTMP_idx,2],.t.,this.OUTPUTMP_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select *  from "+i_cTable+" OUTPUTMP ";
            +" where FLRAGG='S'";
             ,"_Curs_OUTPUTMP")
      else
        select * from (i_cTable);
         where FLRAGG="S";
          into cursor _Curs_OUTPUTMP
      endif
      if used('_Curs_OUTPUTMP')
        select _Curs_OUTPUTMP
        locate for 1=1
        do while not(eof())
        this.w_SERIAL = _Curs_OUTPUTMP.PTSERIAL
        this.w_CPROWNUM = _Curs_OUTPUTMP.CPROWNUM
        this.w_LIVELLO = RIGHT("000"+ALLTRIM(STR(_Curs_OUTPUTMP.CPROWNUM)),3)
        this.w_DATSCA = _Curs_OUTPUTMP.DATSCA
        this.w_LNUMPAR = _Curs_OUTPUTMP.NUMPAR
        * --- Memorizzo queste informazioni per poter dopo eliminare la riga
        *     che sto per inserire
        this.w_OLDSERIAL = _Curs_OUTPUTMP.PTSERIAL
        this.w_OLDCPROWNUM = _Curs_OUTPUTMP.CPROWNUM
        this.w_OLDLIVELLO = RIGHT("000"+ALLTRIM(STR(_Curs_OUTPUTMP.CPROWNUM)),3)
        * --- Inserisco la scadenza corrente
        * --- Insert into TMP_PART
        i_nConn=i_TableProp[this.TMP_PART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMP_PART_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMP_PART_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"DATSCA"+",NUMPAR"+",TIPCON"+",CODCON"+",FLDAVE"+",TOTIMP"+",CODVAL"+",MODPAG"+",BANAPP"+",BANNOS"+",NUMDOC"+",ALFDOC"+",DATDOC"+",FLCRSA"+",PTSERIAL"+",PTROWORD"+",CPROWNUM"+",FLSOSP"+",NUMDIS"+",SIMVAL"+",DECTOT"+",FLRAGG"+",CAOVAL"+",CODAGE"+",TIPPAG"+",ORDINE"+",DATREG"+",DESSUP"+",NUMPRO"+",FLINDI"+",PTFLRAGG"+",BANFIL"+",PTSERRIF"+",PTORDRIF"+",PTNUMRIF"+",SEGNO"+",IMPDAR"+",IMPAVE"+",CAOAPE"+",TIPDOC"+",ANDESCRI"+",PTDATSCA"+",PTNUMPAR"+",PNCODCAU"+",FLGESPLO"+",LIVELLO"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(_Curs_OUTPUTMP.DATSCA),'TMP_PART','DATSCA');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_OUTPUTMP.NUMPAR),'TMP_PART','NUMPAR');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_OUTPUTMP.TIPCON),'TMP_PART','TIPCON');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_OUTPUTMP.CODCON),'TMP_PART','CODCON');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_OUTPUTMP.FLDAVE),'TMP_PART','FLDAVE');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_OUTPUTMP.TOTIMP),'TMP_PART','TOTIMP');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_OUTPUTMP.CODVAL),'TMP_PART','CODVAL');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_OUTPUTMP.MODPAG),'TMP_PART','MODPAG');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_OUTPUTMP.BANAPP),'TMP_PART','BANAPP');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_OUTPUTMP.BANNOS),'TMP_PART','BANNOS');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_OUTPUTMP.NUMDOC),'TMP_PART','NUMDOC');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_OUTPUTMP.ALFDOC),'TMP_PART','ALFDOC');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_OUTPUTMP.DATDOC),'TMP_PART','DATDOC');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_OUTPUTMP.FLCRSA),'TMP_PART','FLCRSA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'TMP_PART','PTSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_OUTPUTMP.PTROWORD),'TMP_PART','PTROWORD');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_OUTPUTMP.CPROWNUM),'TMP_PART','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_OUTPUTMP.FLSOSP),'TMP_PART','FLSOSP');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_OUTPUTMP.NUMDIS),'TMP_PART','NUMDIS');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_OUTPUTMP.SIMVAL),'TMP_PART','SIMVAL');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_OUTPUTMP.DECTOT),'TMP_PART','DECTOT');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_OUTPUTMP.FLRAGG),'TMP_PART','FLRAGG');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_OUTPUTMP.CAOVAL),'TMP_PART','CAOVAL');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_OUTPUTMP.CODAGE),'TMP_PART','CODAGE');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_OUTPUTMP.TIPPAG),'TMP_PART','TIPPAG');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_OUTPUTMP.ORDINE),'TMP_PART','ORDINE');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_OUTPUTMP.DATREG),'TMP_PART','DATREG');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_OUTPUTMP.DESSUP),'TMP_PART','DESSUP');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_OUTPUTMP.NUMPRO),'TMP_PART','NUMPRO');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_OUTPUTMP.FLINDI),'TMP_PART','FLINDI');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_OUTPUTMP.PTFLRAGG),'TMP_PART','PTFLRAGG');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_OUTPUTMP.BANFIL),'TMP_PART','BANFIL');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_OUTPUTMP.PTSERRIF),'TMP_PART','PTSERRIF');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_OUTPUTMP.PTORDRIF),'TMP_PART','PTORDRIF');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_OUTPUTMP.PTNUMRIF),'TMP_PART','PTNUMRIF');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_OUTPUTMP.SEGNO),'TMP_PART','SEGNO');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_OUTPUTMP.IMPDAR),'TMP_PART','IMPDAR');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_OUTPUTMP.IMPAVE),'TMP_PART','IMPAVE');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_OUTPUTMP.CAOAPE),'TMP_PART','CAOAPE');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_OUTPUTMP.TIPDOC),'TMP_PART','TIPDOC');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_OUTPUTMP.ANDESCRI),'TMP_PART','ANDESCRI');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_OUTPUTMP.PTDATSCA),'TMP_PART','PTDATSCA');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_OUTPUTMP.PTNUMPAR),'TMP_PART','PTNUMPAR');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_OUTPUTMP.PNCODCAU),'TMP_PART','PNCODCAU');
          +","+cp_NullLink(cp_ToStrODBC("N"),'TMP_PART','FLGESPLO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_LIVELLO),'TMP_PART','LIVELLO');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'DATSCA',_Curs_OUTPUTMP.DATSCA,'NUMPAR',_Curs_OUTPUTMP.NUMPAR,'TIPCON',_Curs_OUTPUTMP.TIPCON,'CODCON',_Curs_OUTPUTMP.CODCON,'FLDAVE',_Curs_OUTPUTMP.FLDAVE,'TOTIMP',_Curs_OUTPUTMP.TOTIMP,'CODVAL',_Curs_OUTPUTMP.CODVAL,'MODPAG',_Curs_OUTPUTMP.MODPAG,'BANAPP',_Curs_OUTPUTMP.BANAPP,'BANNOS',_Curs_OUTPUTMP.BANNOS,'NUMDOC',_Curs_OUTPUTMP.NUMDOC,'ALFDOC',_Curs_OUTPUTMP.ALFDOC)
          insert into (i_cTable) (DATSCA,NUMPAR,TIPCON,CODCON,FLDAVE,TOTIMP,CODVAL,MODPAG,BANAPP,BANNOS,NUMDOC,ALFDOC,DATDOC,FLCRSA,PTSERIAL,PTROWORD,CPROWNUM,FLSOSP,NUMDIS,SIMVAL,DECTOT,FLRAGG,CAOVAL,CODAGE,TIPPAG,ORDINE,DATREG,DESSUP,NUMPRO,FLINDI,PTFLRAGG,BANFIL,PTSERRIF,PTORDRIF,PTNUMRIF,SEGNO,IMPDAR,IMPAVE,CAOAPE,TIPDOC,ANDESCRI,PTDATSCA,PTNUMPAR,PNCODCAU,FLGESPLO,LIVELLO &i_ccchkf. );
             values (;
               _Curs_OUTPUTMP.DATSCA;
               ,_Curs_OUTPUTMP.NUMPAR;
               ,_Curs_OUTPUTMP.TIPCON;
               ,_Curs_OUTPUTMP.CODCON;
               ,_Curs_OUTPUTMP.FLDAVE;
               ,_Curs_OUTPUTMP.TOTIMP;
               ,_Curs_OUTPUTMP.CODVAL;
               ,_Curs_OUTPUTMP.MODPAG;
               ,_Curs_OUTPUTMP.BANAPP;
               ,_Curs_OUTPUTMP.BANNOS;
               ,_Curs_OUTPUTMP.NUMDOC;
               ,_Curs_OUTPUTMP.ALFDOC;
               ,_Curs_OUTPUTMP.DATDOC;
               ,_Curs_OUTPUTMP.FLCRSA;
               ,this.w_SERIAL;
               ,_Curs_OUTPUTMP.PTROWORD;
               ,_Curs_OUTPUTMP.CPROWNUM;
               ,_Curs_OUTPUTMP.FLSOSP;
               ,_Curs_OUTPUTMP.NUMDIS;
               ,_Curs_OUTPUTMP.SIMVAL;
               ,_Curs_OUTPUTMP.DECTOT;
               ,_Curs_OUTPUTMP.FLRAGG;
               ,_Curs_OUTPUTMP.CAOVAL;
               ,_Curs_OUTPUTMP.CODAGE;
               ,_Curs_OUTPUTMP.TIPPAG;
               ,_Curs_OUTPUTMP.ORDINE;
               ,_Curs_OUTPUTMP.DATREG;
               ,_Curs_OUTPUTMP.DESSUP;
               ,_Curs_OUTPUTMP.NUMPRO;
               ,_Curs_OUTPUTMP.FLINDI;
               ,_Curs_OUTPUTMP.PTFLRAGG;
               ,_Curs_OUTPUTMP.BANFIL;
               ,_Curs_OUTPUTMP.PTSERRIF;
               ,_Curs_OUTPUTMP.PTORDRIF;
               ,_Curs_OUTPUTMP.PTNUMRIF;
               ,_Curs_OUTPUTMP.SEGNO;
               ,_Curs_OUTPUTMP.IMPDAR;
               ,_Curs_OUTPUTMP.IMPAVE;
               ,_Curs_OUTPUTMP.CAOAPE;
               ,_Curs_OUTPUTMP.TIPDOC;
               ,_Curs_OUTPUTMP.ANDESCRI;
               ,_Curs_OUTPUTMP.PTDATSCA;
               ,_Curs_OUTPUTMP.PTNUMPAR;
               ,_Curs_OUTPUTMP.PNCODCAU;
               ,"N";
               ,this.w_LIVELLO;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        * --- Elaboro la tabella TMP_PART che conterr� tutte le scadenze figlie della 
        *     scadenza corrente
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Elimino la scadenza corrente dalla tabella TMP_PART poich� dopo devo inserire 
        *     TMP_PART in OUTPUTMP e quindi avrei delle righe ripetute.
        *     Come chiave per la cancellazione utilizzo anche LIVELLO perch� nel caso
        *     in cui stampi tutte le scadenze e ho una scadenza raggruppata (1) che ha come figlio
        *     un'altra scadenza raggruppata (2) eliminando la scadenza corrente (2) eliminerei
        *     anche la scadenza figlia di (1) 
        * --- Delete from TMP_PART
        i_nConn=i_TableProp[this.TMP_PART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMP_PART_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"PTSERIAL = "+cp_ToStrODBC(this.w_OLDSERIAL);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_OLDCPROWNUM);
                +" and LIVELLO = "+cp_ToStrODBC(this.w_OLDLIVELLO);
                 )
        else
          delete from (i_cTable) where;
                PTSERIAL = this.w_OLDSERIAL;
                and CPROWNUM = this.w_OLDCPROWNUM;
                and LIVELLO = this.w_OLDLIVELLO;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
          select _Curs_OUTPUTMP
          continue
        enddo
        use
      endif
      * --- Inserisco l'esplosione delle scadenze raggruppate nella tabella OUTPUTMP
      * --- Insert into OUTPUTMP
      i_nConn=i_TableProp[this.OUTPUTMP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OUTPUTMP_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"QUERY\GSTEUSIS",this.OUTPUTMP_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Write into OUTPUTMP
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.OUTPUTMP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OUTPUTMP_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="PTSERIAL,PTROWORD,CPROWNUM"
        do vq_exec with 'GSTELSIS',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.OUTPUTMP_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="OUTPUTMP.PTSERIAL = _t2.PTSERIAL";
                +" and "+"OUTPUTMP.PTROWORD = _t2.PTROWORD";
                +" and "+"OUTPUTMP.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"LIVELLO = _t2.LIVELLO";
            +i_ccchkf;
            +" from "+i_cTable+" OUTPUTMP, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="OUTPUTMP.PTSERIAL = _t2.PTSERIAL";
                +" and "+"OUTPUTMP.PTROWORD = _t2.PTROWORD";
                +" and "+"OUTPUTMP.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OUTPUTMP, "+i_cQueryTable+" _t2 set ";
            +"OUTPUTMP.LIVELLO = _t2.LIVELLO";
            +Iif(Empty(i_ccchkf),"",",OUTPUTMP.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="OUTPUTMP.PTSERIAL = t2.PTSERIAL";
                +" and "+"OUTPUTMP.PTROWORD = t2.PTROWORD";
                +" and "+"OUTPUTMP.CPROWNUM = t2.CPROWNUM";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OUTPUTMP set (";
            +"LIVELLO";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.LIVELLO";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="OUTPUTMP.PTSERIAL = _t2.PTSERIAL";
                +" and "+"OUTPUTMP.PTROWORD = _t2.PTROWORD";
                +" and "+"OUTPUTMP.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OUTPUTMP set ";
            +"LIVELLO = _t2.LIVELLO";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".PTSERIAL = "+i_cQueryTable+".PTSERIAL";
                +" and "+i_cTable+".PTROWORD = "+i_cQueryTable+".PTROWORD";
                +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"LIVELLO = (select LIVELLO from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Passo le variabili al report
    L_PARB=this.oParentObject.w_PAGRB
    L_SOSP=this.oParentObject.w_FLSOSP
    L_PAMA=this.oParentObject.w_PAGMA
    L_SALD=this.oParentObject.w_FLSALD
    L_PARI=this.oParentObject.w_PAGRI
    L_NUMPAR=this.oParentObject.w_NUMPAR
    L_PACA=this.oParentObject.w_PAGCA
    L_ADOINI=this.oParentObject.w_ADOINI
    L_PARD=this.oParentObject.w_PAGRD
    L_ADOFIN=this.oParentObject.w_ADOFIN
    L_PABO=this.oParentObject.w_PAGBO
    L_NDOINI=this.oParentObject.w_NDOINI
    L_SCAINI=this.oParentObject.w_SCAINI
    L_NDOFIN=this.oParentObject.w_NDOFIN
    L_SCAFIN=this.oParentObject.w_SCAFIN
    L_DDOINI=this.oParentObject.w_DDOINI
    L_BANNOS=this.oParentObject.w_BANNOS
    L_DDOFIN=this.oParentObject.w_DDOFIN
    L_VALUTA=this.oParentObject.w_VALUTA
    L_AGENTE=this.oParentObject.w_AGENTE
    L_BANAPP=this.oParentObject.w_BANAPP
    L_FORSEL=IIF(this.oParentObject.w_TIPCON="F", this.oParentObject.w_CODCON, " ")
    L_CONSEL=IIF(this.oParentObject.w_TIPCON="G", this.oParentObject.w_CODCON, " ")
    L_CLISEL=IIF(this.oParentObject.w_TIPCON="C", this.oParentObject.w_CODCON, " ")
    L_FORSEL1=IIF(this.oParentObject.w_TIPCON="F", this.oParentObject.w_ACODCO, " ")
    L_CONSEL1=IIF(this.oParentObject.w_TIPCON="G", this.oParentObject.w_ACODCO, " ")
    L_CLISEL1=IIF(this.oParentObject.w_TIPCON="C", this.oParentObject.w_ACODCO, " ")
    L_SCACLF=this.oParentObject.w_TIPCON
    L_REGINI=this.oParentObject.w_REGINI
    L_REGFIN=this.oParentObject.w_REGFIN
    L_NEWPAG=this.oParentObject.w_NEWPAG
    L_DATSTA=this.oParentObject.w_DATSTA
    L_DATVAL=this.w_DATVAL
    L_DESCON=this.oParentObject.w_DESCON
    L_ADESCON=this.oParentObject.w_ADESCON
    * --- Lancio la stampa
    vx_exec(""+alltrim(this.oParentObject.w_OQRY)+", "+alltrim(this.oParentObject.w_orep)+"",this)
    * --- Chiudo i cursori e tabelle temporanee
    * --- Drop temporary table TMP_PART_APE
    i_nIdx=cp_GetTableDefIdx('TMP_PART_APE')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_PART_APE')
    endif
    * --- Drop temporary table TMP_PART
    i_nIdx=cp_GetTableDefIdx('TMP_PART')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_PART')
    endif
    * --- Drop temporary table OUTPUTMP
    i_nIdx=cp_GetTableDefIdx('OUTPUTMP')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('OUTPUTMP')
    endif
    if USED("PARTITE")
      SELECT PARTITE
      USE
    endif
    if USED("__TMP__")
      SELECT __TMP__
      USE
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ricerca partite di origine per dettaglio partite raggruppate.
    this.w_LOOP = .T.
    do while this.w_LOOP
      * --- Assegno false a w_LOOP per controllare che ci sia almeno una scadenza non ancora esplosa
      this.w_LOOP = .F.
      * --- Seleziono le scadenze non ancora esplose FLGESPLO='N'
      * --- Select from TMP_PART
      i_nConn=i_TableProp[this.TMP_PART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMP_PART_idx,2],.t.,this.TMP_PART_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select *  from "+i_cTable+" TMP_PART ";
            +" where FLGESPLO='N' AND PTROWORD<>-2";
             ,"_Curs_TMP_PART")
      else
        select * from (i_cTable);
         where FLGESPLO="N" AND PTROWORD<>-2;
          into cursor _Curs_TMP_PART
      endif
      if used('_Curs_TMP_PART')
        select _Curs_TMP_PART
        locate for 1=1
        do while not(eof())
        this.w_SERIAL = _Curs_TMP_PART.PTSERIAL
        this.w_CPROWNUM = _Curs_TMP_PART.CPROWNUM
        * --- Il campo LIVELLO � utilizzato per ordinare le varie scadenze figlie
        this.w_LIVELLO = ALLTRIM(_Curs_TMP_PART.LIVELLO)
        * --- Inserisco in TMP_PART tutte le partite di origine della scadenza raggruppata che sto esaminando
        * --- Insert into TMP_PART
        i_nConn=i_TableProp[this.TMP_PART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMP_PART_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"QUERY\GSTESSIS",this.TMP_PART_idx)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        * --- Contrassegno la scadenza come esplosa
        * --- Write into TMP_PART
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMP_PART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMP_PART_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_PART_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"FLGESPLO ="+cp_NullLink(cp_ToStrODBC("S"),'TMP_PART','FLGESPLO');
              +i_ccchkf ;
          +" where ";
              +"PTSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
              +" and LIVELLO = "+cp_ToStrODBC(this.w_LIVELLO);
                 )
        else
          update (i_cTable) set;
              FLGESPLO = "S";
              &i_ccchkf. ;
           where;
              PTSERIAL = this.w_SERIAL;
              and CPROWNUM = this.w_CPROWNUM;
              and LIVELLO = this.w_LIVELLO;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Poich� sono entrato dentro alla select continuo il ciclo
        this.w_LOOP = .T.
          select _Curs_TMP_PART
          continue
        enddo
        use
      endif
    enddo
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='*OUTPUTMP'
    this.cWorkTables[2]='*TMP_PART'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_OUTPUTMP')
      use in _Curs_OUTPUTMP
    endif
    if used('_Curs_TMP_PART')
      use in _Curs_TMP_PART
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
