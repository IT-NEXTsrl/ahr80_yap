* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscp_apo                                                        *
*              Parametri                                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_117]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-03-28                                                      *
* Last revis.: 2008-05-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgscp_apo")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgscp_apo")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgscp_apo")
  return

* --- Class definition
define class tgscp_apo as StdPCForm
  Width  = 532
  Height = 185+35
  Top    = 2
  Left   = 15
  cComment = "Parametri"
  cPrg = "gscp_apo"
  HelpContextID=108423831
  add object cnt as tcgscp_apo
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgscp_apo as PCContext
  w_PACODICE = space(5)
  w_TDFLVEAC8 = space(1)
  w_TDCATDOC8 = space(2)
  w_TDFLINTE8 = space(1)
  w_PAFLPRO2 = space(1)
  w_PATIPDOC2 = space(5)
  w_LIVCFATT = space(1)
  w_PACODMAG2 = space(5)
  w_TDFLVEAC2 = space(1)
  w_TDCATDOC2 = space(2)
  w_TDFLINTE2 = space(1)
  w_TDDESDOC2 = space(35)
  w_MGDESMAG2 = space(30)
  w_PACENCOS2 = space(15)
  w_OBTEST = space(8)
  w_CCDESPIA2 = space(40)
  w_TDFLVEAC4 = space(1)
  w_TDCATDOC4 = space(2)
  w_TDFLINTE4 = space(1)
  w_TDFLVEAC6 = space(1)
  w_TDCATDOC6 = space(2)
  w_TDFLINTE6 = space(1)
  w_TDFLRIFI2 = space(1)
  w_TDFLRIFI4 = space(1)
  w_TDFLRIFI6 = space(1)
  w_TDFLRIFI8 = space(1)
  w_PAFLPRO4 = space(1)
  w_PATIPDOC4 = space(5)
  w_PACODMAG4 = space(5)
  w_TDDESDOC4 = space(35)
  w_MGDESMAG4 = space(30)
  w_PACENCOS4 = space(15)
  w_CCDESPIA4 = space(40)
  w_PAFLPRO6 = space(1)
  w_PATIPDOC6 = space(5)
  w_PACODMAG6 = space(5)
  w_TDDESDOC6 = space(35)
  w_MGDESMAG6 = space(30)
  w_PACENCOS6 = space(15)
  w_CCDESPIA6 = space(40)
  w_PATIPDOC8 = space(5)
  w_PACODMAG8 = space(5)
  w_TDDESDOC8 = space(35)
  w_MGDESMAG8 = space(30)
  w_PACENCOS8 = space(15)
  w_PAFLPRO8 = space(1)
  w_CCDESPIA8 = space(40)
  proc Save(oFrom)
    this.w_PACODICE = oFrom.w_PACODICE
    this.w_TDFLVEAC8 = oFrom.w_TDFLVEAC8
    this.w_TDCATDOC8 = oFrom.w_TDCATDOC8
    this.w_TDFLINTE8 = oFrom.w_TDFLINTE8
    this.w_PAFLPRO2 = oFrom.w_PAFLPRO2
    this.w_PATIPDOC2 = oFrom.w_PATIPDOC2
    this.w_LIVCFATT = oFrom.w_LIVCFATT
    this.w_PACODMAG2 = oFrom.w_PACODMAG2
    this.w_TDFLVEAC2 = oFrom.w_TDFLVEAC2
    this.w_TDCATDOC2 = oFrom.w_TDCATDOC2
    this.w_TDFLINTE2 = oFrom.w_TDFLINTE2
    this.w_TDDESDOC2 = oFrom.w_TDDESDOC2
    this.w_MGDESMAG2 = oFrom.w_MGDESMAG2
    this.w_PACENCOS2 = oFrom.w_PACENCOS2
    this.w_OBTEST = oFrom.w_OBTEST
    this.w_CCDESPIA2 = oFrom.w_CCDESPIA2
    this.w_TDFLVEAC4 = oFrom.w_TDFLVEAC4
    this.w_TDCATDOC4 = oFrom.w_TDCATDOC4
    this.w_TDFLINTE4 = oFrom.w_TDFLINTE4
    this.w_TDFLVEAC6 = oFrom.w_TDFLVEAC6
    this.w_TDCATDOC6 = oFrom.w_TDCATDOC6
    this.w_TDFLINTE6 = oFrom.w_TDFLINTE6
    this.w_TDFLRIFI2 = oFrom.w_TDFLRIFI2
    this.w_TDFLRIFI4 = oFrom.w_TDFLRIFI4
    this.w_TDFLRIFI6 = oFrom.w_TDFLRIFI6
    this.w_TDFLRIFI8 = oFrom.w_TDFLRIFI8
    this.w_PAFLPRO4 = oFrom.w_PAFLPRO4
    this.w_PATIPDOC4 = oFrom.w_PATIPDOC4
    this.w_PACODMAG4 = oFrom.w_PACODMAG4
    this.w_TDDESDOC4 = oFrom.w_TDDESDOC4
    this.w_MGDESMAG4 = oFrom.w_MGDESMAG4
    this.w_PACENCOS4 = oFrom.w_PACENCOS4
    this.w_CCDESPIA4 = oFrom.w_CCDESPIA4
    this.w_PAFLPRO6 = oFrom.w_PAFLPRO6
    this.w_PATIPDOC6 = oFrom.w_PATIPDOC6
    this.w_PACODMAG6 = oFrom.w_PACODMAG6
    this.w_TDDESDOC6 = oFrom.w_TDDESDOC6
    this.w_MGDESMAG6 = oFrom.w_MGDESMAG6
    this.w_PACENCOS6 = oFrom.w_PACENCOS6
    this.w_CCDESPIA6 = oFrom.w_CCDESPIA6
    this.w_PATIPDOC8 = oFrom.w_PATIPDOC8
    this.w_PACODMAG8 = oFrom.w_PACODMAG8
    this.w_TDDESDOC8 = oFrom.w_TDDESDOC8
    this.w_MGDESMAG8 = oFrom.w_MGDESMAG8
    this.w_PACENCOS8 = oFrom.w_PACENCOS8
    this.w_PAFLPRO8 = oFrom.w_PAFLPRO8
    this.w_CCDESPIA8 = oFrom.w_CCDESPIA8
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_PACODICE = this.w_PACODICE
    oTo.w_TDFLVEAC8 = this.w_TDFLVEAC8
    oTo.w_TDCATDOC8 = this.w_TDCATDOC8
    oTo.w_TDFLINTE8 = this.w_TDFLINTE8
    oTo.w_PAFLPRO2 = this.w_PAFLPRO2
    oTo.w_PATIPDOC2 = this.w_PATIPDOC2
    oTo.w_LIVCFATT = this.w_LIVCFATT
    oTo.w_PACODMAG2 = this.w_PACODMAG2
    oTo.w_TDFLVEAC2 = this.w_TDFLVEAC2
    oTo.w_TDCATDOC2 = this.w_TDCATDOC2
    oTo.w_TDFLINTE2 = this.w_TDFLINTE2
    oTo.w_TDDESDOC2 = this.w_TDDESDOC2
    oTo.w_MGDESMAG2 = this.w_MGDESMAG2
    oTo.w_PACENCOS2 = this.w_PACENCOS2
    oTo.w_OBTEST = this.w_OBTEST
    oTo.w_CCDESPIA2 = this.w_CCDESPIA2
    oTo.w_TDFLVEAC4 = this.w_TDFLVEAC4
    oTo.w_TDCATDOC4 = this.w_TDCATDOC4
    oTo.w_TDFLINTE4 = this.w_TDFLINTE4
    oTo.w_TDFLVEAC6 = this.w_TDFLVEAC6
    oTo.w_TDCATDOC6 = this.w_TDCATDOC6
    oTo.w_TDFLINTE6 = this.w_TDFLINTE6
    oTo.w_TDFLRIFI2 = this.w_TDFLRIFI2
    oTo.w_TDFLRIFI4 = this.w_TDFLRIFI4
    oTo.w_TDFLRIFI6 = this.w_TDFLRIFI6
    oTo.w_TDFLRIFI8 = this.w_TDFLRIFI8
    oTo.w_PAFLPRO4 = this.w_PAFLPRO4
    oTo.w_PATIPDOC4 = this.w_PATIPDOC4
    oTo.w_PACODMAG4 = this.w_PACODMAG4
    oTo.w_TDDESDOC4 = this.w_TDDESDOC4
    oTo.w_MGDESMAG4 = this.w_MGDESMAG4
    oTo.w_PACENCOS4 = this.w_PACENCOS4
    oTo.w_CCDESPIA4 = this.w_CCDESPIA4
    oTo.w_PAFLPRO6 = this.w_PAFLPRO6
    oTo.w_PATIPDOC6 = this.w_PATIPDOC6
    oTo.w_PACODMAG6 = this.w_PACODMAG6
    oTo.w_TDDESDOC6 = this.w_TDDESDOC6
    oTo.w_MGDESMAG6 = this.w_MGDESMAG6
    oTo.w_PACENCOS6 = this.w_PACENCOS6
    oTo.w_CCDESPIA6 = this.w_CCDESPIA6
    oTo.w_PATIPDOC8 = this.w_PATIPDOC8
    oTo.w_PACODMAG8 = this.w_PACODMAG8
    oTo.w_TDDESDOC8 = this.w_TDDESDOC8
    oTo.w_MGDESMAG8 = this.w_MGDESMAG8
    oTo.w_PACENCOS8 = this.w_PACENCOS8
    oTo.w_PAFLPRO8 = this.w_PAFLPRO8
    oTo.w_CCDESPIA8 = this.w_CCDESPIA8
    PCContext::Load(oTo)
enddefine

define class tcgscp_apo as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 532
  Height = 185+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-05-26"
  HelpContextID=108423831
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=47

  * --- Constant Properties
  ZPARAMORD_IDX = 0
  TIP_DOCU_IDX = 0
  MAGAZZIN_IDX = 0
  AZIENDA_IDX = 0
  CENCOST_IDX = 0
  cFile = "ZPARAMORD"
  cKeySelect = "PACODICE"
  cKeyWhere  = "PACODICE=this.w_PACODICE"
  cKeyWhereODBC = '"PACODICE="+cp_ToStrODBC(this.w_PACODICE)';

  cKeyWhereODBCqualified = '"ZPARAMORD.PACODICE="+cp_ToStrODBC(this.w_PACODICE)';

  cPrg = "gscp_apo"
  cComment = "Parametri"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PACODICE = space(5)
  w_TDFLVEAC8 = space(1)
  w_TDCATDOC8 = space(2)
  w_TDFLINTE8 = space(1)
  w_PAFLPRO2 = space(1)
  o_PAFLPRO2 = space(1)
  w_PATIPDOC2 = space(5)
  w_LIVCFATT = space(1)
  w_PACODMAG2 = space(5)
  w_TDFLVEAC2 = space(1)
  w_TDCATDOC2 = space(2)
  w_TDFLINTE2 = space(1)
  w_TDDESDOC2 = space(35)
  w_MGDESMAG2 = space(30)
  w_PACENCOS2 = space(15)
  w_OBTEST = ctod('  /  /  ')
  w_CCDESPIA2 = space(40)
  w_TDFLVEAC4 = space(1)
  w_TDCATDOC4 = space(2)
  w_TDFLINTE4 = space(1)
  w_TDFLVEAC6 = space(1)
  w_TDCATDOC6 = space(2)
  w_TDFLINTE6 = space(1)
  w_TDFLRIFI2 = space(1)
  w_TDFLRIFI4 = space(1)
  w_TDFLRIFI6 = space(1)
  w_TDFLRIFI8 = space(1)
  w_PAFLPRO4 = space(1)
  o_PAFLPRO4 = space(1)
  w_PATIPDOC4 = space(5)
  w_PACODMAG4 = space(5)
  w_TDDESDOC4 = space(35)
  w_MGDESMAG4 = space(30)
  w_PACENCOS4 = space(15)
  w_CCDESPIA4 = space(40)
  w_PAFLPRO6 = space(1)
  o_PAFLPRO6 = space(1)
  w_PATIPDOC6 = space(5)
  w_PACODMAG6 = space(5)
  w_TDDESDOC6 = space(35)
  w_MGDESMAG6 = space(30)
  w_PACENCOS6 = space(15)
  w_CCDESPIA6 = space(40)
  w_PATIPDOC8 = space(5)
  w_PACODMAG8 = space(5)
  w_TDDESDOC8 = space(35)
  w_MGDESMAG8 = space(30)
  w_PACENCOS8 = space(15)
  w_PAFLPRO8 = space(1)
  o_PAFLPRO8 = space(1)
  w_CCDESPIA8 = space(40)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=4, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscp_apoPag1","gscp_apo",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Liv. conf. ord. nero")
      .Pages(1).HelpContextID = 129235332
      .Pages(2).addobject("oPag","tgscp_apoPag2","gscp_apo",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Liv. conf. ord. rosso")
      .Pages(2).HelpContextID = 245896132
      .Pages(3).addobject("oPag","tgscp_apoPag3","gscp_apo",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Liv. conf. ord. giallo")
      .Pages(3).HelpContextID = 225438996
      .Pages(4).addobject("oPag","tgscp_apoPag4","gscp_apo",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Liv. conf. ord. verde")
      .Pages(4).HelpContextID = 234420740
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPAFLPRO2_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='TIP_DOCU'
    this.cWorkTables[2]='MAGAZZIN'
    this.cWorkTables[3]='AZIENDA'
    this.cWorkTables[4]='CENCOST'
    this.cWorkTables[5]='ZPARAMORD'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ZPARAMORD_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ZPARAMORD_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgscp_apo'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_4_joined
    link_1_4_joined=.f.
    local link_1_7_joined
    link_1_7_joined=.f.
    local link_1_14_joined
    link_1_14_joined=.f.
    local link_2_7_joined
    link_2_7_joined=.f.
    local link_2_9_joined
    link_2_9_joined=.f.
    local link_2_13_joined
    link_2_13_joined=.f.
    local link_3_7_joined
    link_3_7_joined=.f.
    local link_3_9_joined
    link_3_9_joined=.f.
    local link_3_13_joined
    link_3_13_joined=.f.
    local link_4_5_joined
    link_4_5_joined=.f.
    local link_4_7_joined
    link_4_7_joined=.f.
    local link_4_12_joined
    link_4_12_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from ZPARAMORD where PACODICE=KeySet.PACODICE
    *
    i_nConn = i_TableProp[this.ZPARAMORD_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ZPARAMORD_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ZPARAMORD')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ZPARAMORD.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ZPARAMORD '
      link_1_4_joined=this.AddJoinedLink_1_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_7_joined=this.AddJoinedLink_1_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_14_joined=this.AddJoinedLink_1_14(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_7_joined=this.AddJoinedLink_2_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_9_joined=this.AddJoinedLink_2_9(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_13_joined=this.AddJoinedLink_2_13(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_7_joined=this.AddJoinedLink_3_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_9_joined=this.AddJoinedLink_3_9(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_13_joined=this.AddJoinedLink_3_13(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_5_joined=this.AddJoinedLink_4_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_7_joined=this.AddJoinedLink_4_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_12_joined=this.AddJoinedLink_4_12(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PACODICE',this.w_PACODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TDFLVEAC8 = space(1)
        .w_TDCATDOC8 = space(2)
        .w_TDFLINTE8 = space(1)
        .w_LIVCFATT = space(1)
        .w_TDFLVEAC2 = space(1)
        .w_TDCATDOC2 = space(2)
        .w_TDFLINTE2 = space(1)
        .w_TDDESDOC2 = space(35)
        .w_MGDESMAG2 = space(30)
        .w_OBTEST = i_DATSYS
        .w_CCDESPIA2 = space(40)
        .w_TDFLVEAC4 = space(1)
        .w_TDCATDOC4 = space(2)
        .w_TDFLINTE4 = space(1)
        .w_TDFLVEAC6 = space(1)
        .w_TDCATDOC6 = space(2)
        .w_TDFLINTE6 = space(1)
        .w_TDFLRIFI2 = space(1)
        .w_TDFLRIFI4 = space(1)
        .w_TDFLRIFI6 = space(1)
        .w_TDFLRIFI8 = space(1)
        .w_TDDESDOC4 = space(35)
        .w_MGDESMAG4 = space(30)
        .w_CCDESPIA4 = space(40)
        .w_TDDESDOC6 = space(35)
        .w_MGDESMAG6 = space(30)
        .w_CCDESPIA6 = space(40)
        .w_TDDESDOC8 = space(35)
        .w_MGDESMAG8 = space(30)
        .w_CCDESPIA8 = space(40)
        .w_PACODICE = NVL(PACODICE,space(5))
        .w_PAFLPRO2 = NVL(PAFLPRO2,space(1))
        .w_PATIPDOC2 = NVL(PATIPDOC2,space(5))
          if link_1_4_joined
            this.w_PATIPDOC2 = NVL(TDTIPDOC104,NVL(this.w_PATIPDOC2,space(5)))
            this.w_TDDESDOC2 = NVL(TDDESDOC104,space(35))
            this.w_TDFLVEAC2 = NVL(TDFLVEAC104,space(1))
            this.w_TDCATDOC2 = NVL(TDCATDOC104,space(2))
            this.w_TDFLINTE2 = NVL(TDFLINTE104,space(1))
            this.w_TDFLRIFI2 = NVL(TDFLRIFI104,space(1))
          else
          .link_1_4('Load')
          endif
        .w_PACODMAG2 = NVL(PACODMAG2,space(5))
          if link_1_7_joined
            this.w_PACODMAG2 = NVL(MGCODMAG107,NVL(this.w_PACODMAG2,space(5)))
            this.w_MGDESMAG2 = NVL(MGDESMAG107,space(30))
          else
          .link_1_7('Load')
          endif
        .w_PACENCOS2 = NVL(PACENCOS2,space(15))
          if link_1_14_joined
            this.w_PACENCOS2 = NVL(CC_CONTO114,NVL(this.w_PACENCOS2,space(15)))
            this.w_CCDESPIA2 = NVL(CCDESPIA114,space(40))
          else
          .link_1_14('Load')
          endif
        .w_PAFLPRO4 = NVL(PAFLPRO4,space(1))
        .w_PATIPDOC4 = NVL(PATIPDOC4,space(5))
          if link_2_7_joined
            this.w_PATIPDOC4 = NVL(TDTIPDOC207,NVL(this.w_PATIPDOC4,space(5)))
            this.w_TDDESDOC4 = NVL(TDDESDOC207,space(35))
            this.w_TDFLVEAC4 = NVL(TDFLVEAC207,space(1))
            this.w_TDCATDOC4 = NVL(TDCATDOC207,space(2))
            this.w_TDFLINTE4 = NVL(TDFLINTE207,space(1))
            this.w_TDFLRIFI4 = NVL(TDFLRIFI207,space(1))
          else
          .link_2_7('Load')
          endif
        .w_PACODMAG4 = NVL(PACODMAG4,space(5))
          if link_2_9_joined
            this.w_PACODMAG4 = NVL(MGCODMAG209,NVL(this.w_PACODMAG4,space(5)))
            this.w_MGDESMAG4 = NVL(MGDESMAG209,space(30))
          else
          .link_2_9('Load')
          endif
        .w_PACENCOS4 = NVL(PACENCOS4,space(15))
          if link_2_13_joined
            this.w_PACENCOS4 = NVL(CC_CONTO213,NVL(this.w_PACENCOS4,space(15)))
            this.w_CCDESPIA4 = NVL(CCDESPIA213,space(40))
          else
          .link_2_13('Load')
          endif
        .w_PAFLPRO6 = NVL(PAFLPRO6,space(1))
        .w_PATIPDOC6 = NVL(PATIPDOC6,space(5))
          if link_3_7_joined
            this.w_PATIPDOC6 = NVL(TDTIPDOC307,NVL(this.w_PATIPDOC6,space(5)))
            this.w_TDDESDOC6 = NVL(TDDESDOC307,space(35))
            this.w_TDFLVEAC6 = NVL(TDFLVEAC307,space(1))
            this.w_TDCATDOC6 = NVL(TDCATDOC307,space(2))
            this.w_TDFLINTE6 = NVL(TDFLINTE307,space(1))
            this.w_TDFLRIFI6 = NVL(TDFLRIFI307,space(1))
          else
          .link_3_7('Load')
          endif
        .w_PACODMAG6 = NVL(PACODMAG6,space(5))
          if link_3_9_joined
            this.w_PACODMAG6 = NVL(MGCODMAG309,NVL(this.w_PACODMAG6,space(5)))
            this.w_MGDESMAG6 = NVL(MGDESMAG309,space(30))
          else
          .link_3_9('Load')
          endif
        .w_PACENCOS6 = NVL(PACENCOS6,space(15))
          if link_3_13_joined
            this.w_PACENCOS6 = NVL(CC_CONTO313,NVL(this.w_PACENCOS6,space(15)))
            this.w_CCDESPIA6 = NVL(CCDESPIA313,space(40))
          else
          .link_3_13('Load')
          endif
        .w_PATIPDOC8 = NVL(PATIPDOC8,space(5))
          if link_4_5_joined
            this.w_PATIPDOC8 = NVL(TDTIPDOC405,NVL(this.w_PATIPDOC8,space(5)))
            this.w_TDDESDOC8 = NVL(TDDESDOC405,space(35))
            this.w_TDFLVEAC8 = NVL(TDFLVEAC405,space(1))
            this.w_TDCATDOC8 = NVL(TDCATDOC405,space(2))
            this.w_TDFLINTE8 = NVL(TDFLINTE405,space(1))
            this.w_TDFLRIFI8 = NVL(TDFLRIFI405,space(1))
          else
          .link_4_5('Load')
          endif
        .w_PACODMAG8 = NVL(PACODMAG8,space(5))
          if link_4_7_joined
            this.w_PACODMAG8 = NVL(MGCODMAG407,NVL(this.w_PACODMAG8,space(5)))
            this.w_MGDESMAG8 = NVL(MGDESMAG407,space(30))
          else
          .link_4_7('Load')
          endif
        .w_PACENCOS8 = NVL(PACENCOS8,space(15))
          if link_4_12_joined
            this.w_PACENCOS8 = NVL(CC_CONTO412,NVL(this.w_PACENCOS8,space(15)))
            this.w_CCDESPIA8 = NVL(CCDESPIA412,space(40))
          else
          .link_4_12('Load')
          endif
        .w_PAFLPRO8 = NVL(PAFLPRO8,space(1))
        cp_LoadRecExtFlds(this,'ZPARAMORD')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_PACODICE = space(5)
      .w_TDFLVEAC8 = space(1)
      .w_TDCATDOC8 = space(2)
      .w_TDFLINTE8 = space(1)
      .w_PAFLPRO2 = space(1)
      .w_PATIPDOC2 = space(5)
      .w_LIVCFATT = space(1)
      .w_PACODMAG2 = space(5)
      .w_TDFLVEAC2 = space(1)
      .w_TDCATDOC2 = space(2)
      .w_TDFLINTE2 = space(1)
      .w_TDDESDOC2 = space(35)
      .w_MGDESMAG2 = space(30)
      .w_PACENCOS2 = space(15)
      .w_OBTEST = ctod("  /  /  ")
      .w_CCDESPIA2 = space(40)
      .w_TDFLVEAC4 = space(1)
      .w_TDCATDOC4 = space(2)
      .w_TDFLINTE4 = space(1)
      .w_TDFLVEAC6 = space(1)
      .w_TDCATDOC6 = space(2)
      .w_TDFLINTE6 = space(1)
      .w_TDFLRIFI2 = space(1)
      .w_TDFLRIFI4 = space(1)
      .w_TDFLRIFI6 = space(1)
      .w_TDFLRIFI8 = space(1)
      .w_PAFLPRO4 = space(1)
      .w_PATIPDOC4 = space(5)
      .w_PACODMAG4 = space(5)
      .w_TDDESDOC4 = space(35)
      .w_MGDESMAG4 = space(30)
      .w_PACENCOS4 = space(15)
      .w_CCDESPIA4 = space(40)
      .w_PAFLPRO6 = space(1)
      .w_PATIPDOC6 = space(5)
      .w_PACODMAG6 = space(5)
      .w_TDDESDOC6 = space(35)
      .w_MGDESMAG6 = space(30)
      .w_PACENCOS6 = space(15)
      .w_CCDESPIA6 = space(40)
      .w_PATIPDOC8 = space(5)
      .w_PACODMAG8 = space(5)
      .w_TDDESDOC8 = space(35)
      .w_MGDESMAG8 = space(30)
      .w_PACENCOS8 = space(15)
      .w_PAFLPRO8 = space(1)
      .w_CCDESPIA8 = space(40)
      if .cFunction<>"Filter"
          .DoRTCalc(1,4,.f.)
        .w_PAFLPRO2 = 'R'
        .w_PATIPDOC2 = IIF(.w_PAFLPRO2<>'R' AND .w_TDFLRIFI2='S',SPACE(5),IIF(.w_PAFLPRO2='R' AND .w_TDFLRIFI2='N',SPACE(5),.w_PATIPDOC2))
        .DoRTCalc(6,6,.f.)
          if not(empty(.w_PATIPDOC2))
          .link_1_4('Full')
          endif
        .DoRTCalc(7,8,.f.)
          if not(empty(.w_PACODMAG2))
          .link_1_7('Full')
          endif
        .DoRTCalc(9,14,.f.)
          if not(empty(.w_PACENCOS2))
          .link_1_14('Full')
          endif
        .w_OBTEST = i_DATSYS
          .DoRTCalc(16,26,.f.)
        .w_PAFLPRO4 = 'S'
        .w_PATIPDOC4 = IIF(.w_PAFLPRO4<>'R' AND .w_TDFLRIFI4='S',SPACE(5),IIF(.w_PAFLPRO4='R' AND .w_TDFLRIFI4='N',SPACE(5),.w_PATIPDOC4))
        .DoRTCalc(28,28,.f.)
          if not(empty(.w_PATIPDOC4))
          .link_2_7('Full')
          endif
        .DoRTCalc(29,29,.f.)
          if not(empty(.w_PACODMAG4))
          .link_2_9('Full')
          endif
        .DoRTCalc(30,32,.f.)
          if not(empty(.w_PACENCOS4))
          .link_2_13('Full')
          endif
          .DoRTCalc(33,33,.f.)
        .w_PAFLPRO6 = 'S'
        .w_PATIPDOC6 = IIF(.w_PAFLPRO6<>'R' AND .w_TDFLRIFI6='S',SPACE(5),IIF(.w_PAFLPRO6='R' AND .w_TDFLRIFI6='N',SPACE(5),.w_PATIPDOC6))
        .DoRTCalc(35,35,.f.)
          if not(empty(.w_PATIPDOC6))
          .link_3_7('Full')
          endif
        .DoRTCalc(36,36,.f.)
          if not(empty(.w_PACODMAG6))
          .link_3_9('Full')
          endif
        .DoRTCalc(37,39,.f.)
          if not(empty(.w_PACENCOS6))
          .link_3_13('Full')
          endif
          .DoRTCalc(40,40,.f.)
        .w_PATIPDOC8 = IIF(.w_PAFLPRO8<>'R' AND .w_TDFLRIFI8='S',SPACE(5),IIF(.w_PAFLPRO8='R' AND .w_TDFLRIFI8='N',SPACE(5),.w_PATIPDOC8))
        .DoRTCalc(41,41,.f.)
          if not(empty(.w_PATIPDOC8))
          .link_4_5('Full')
          endif
        .DoRTCalc(42,42,.f.)
          if not(empty(.w_PACODMAG8))
          .link_4_7('Full')
          endif
        .DoRTCalc(43,45,.f.)
          if not(empty(.w_PACENCOS8))
          .link_4_12('Full')
          endif
        .w_PAFLPRO8 = 'N'
      endif
    endwith
    cp_BlankRecExtFlds(this,'ZPARAMORD')
    this.DoRTCalc(47,47,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oPAFLPRO2_1_3.enabled = i_bVal
      .Page1.oPag.oPATIPDOC2_1_4.enabled = i_bVal
      .Page1.oPag.oPACODMAG2_1_7.enabled = i_bVal
      .Page1.oPag.oPACENCOS2_1_14.enabled = i_bVal
      .Page1.oPag.oCCDESPIA2_1_17.enabled = i_bVal
      .Page2.oPag.oPAFLPRO4_2_6.enabled = i_bVal
      .Page2.oPag.oPATIPDOC4_2_7.enabled = i_bVal
      .Page2.oPag.oPACODMAG4_2_9.enabled = i_bVal
      .Page2.oPag.oPACENCOS4_2_13.enabled = i_bVal
      .Page2.oPag.oCCDESPIA4_2_15.enabled = i_bVal
      .Page3.oPag.oPAFLPRO6_3_6.enabled = i_bVal
      .Page3.oPag.oPATIPDOC6_3_7.enabled = i_bVal
      .Page3.oPag.oPACODMAG6_3_9.enabled = i_bVal
      .Page3.oPag.oPACENCOS6_3_13.enabled = i_bVal
      .Page3.oPag.oCCDESPIA6_3_15.enabled = i_bVal
      .Page4.oPag.oPATIPDOC8_4_5.enabled = i_bVal
      .Page4.oPag.oPACODMAG8_4_7.enabled = i_bVal
      .Page4.oPag.oPACENCOS8_4_12.enabled = i_bVal
      .Page4.oPag.oPAFLPRO8_4_13.enabled = i_bVal
      .Page4.oPag.oCCDESPIA8_4_15.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'ZPARAMORD',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ZPARAMORD_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACODICE,"PACODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLPRO2,"PAFLPRO2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PATIPDOC2,"PATIPDOC2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACODMAG2,"PACODMAG2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACENCOS2,"PACENCOS2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLPRO4,"PAFLPRO4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PATIPDOC4,"PATIPDOC4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACODMAG4,"PACODMAG4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACENCOS4,"PACENCOS4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLPRO6,"PAFLPRO6",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PATIPDOC6,"PATIPDOC6",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACODMAG6,"PACODMAG6",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACENCOS6,"PACENCOS6",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PATIPDOC8,"PATIPDOC8",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACODMAG8,"PACODMAG8",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACENCOS8,"PACENCOS8",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLPRO8,"PAFLPRO8",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ZPARAMORD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ZPARAMORD_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.ZPARAMORD_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into ZPARAMORD
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ZPARAMORD')
        i_extval=cp_InsertValODBCExtFlds(this,'ZPARAMORD')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(PACODICE,PAFLPRO2,PATIPDOC2,PACODMAG2,PACENCOS2"+;
                  ",PAFLPRO4,PATIPDOC4,PACODMAG4,PACENCOS4,PAFLPRO6"+;
                  ",PATIPDOC6,PACODMAG6,PACENCOS6,PATIPDOC8,PACODMAG8"+;
                  ",PACENCOS8,PAFLPRO8 "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_PACODICE)+;
                  ","+cp_ToStrODBC(this.w_PAFLPRO2)+;
                  ","+cp_ToStrODBCNull(this.w_PATIPDOC2)+;
                  ","+cp_ToStrODBCNull(this.w_PACODMAG2)+;
                  ","+cp_ToStrODBCNull(this.w_PACENCOS2)+;
                  ","+cp_ToStrODBC(this.w_PAFLPRO4)+;
                  ","+cp_ToStrODBCNull(this.w_PATIPDOC4)+;
                  ","+cp_ToStrODBCNull(this.w_PACODMAG4)+;
                  ","+cp_ToStrODBCNull(this.w_PACENCOS4)+;
                  ","+cp_ToStrODBC(this.w_PAFLPRO6)+;
                  ","+cp_ToStrODBCNull(this.w_PATIPDOC6)+;
                  ","+cp_ToStrODBCNull(this.w_PACODMAG6)+;
                  ","+cp_ToStrODBCNull(this.w_PACENCOS6)+;
                  ","+cp_ToStrODBCNull(this.w_PATIPDOC8)+;
                  ","+cp_ToStrODBCNull(this.w_PACODMAG8)+;
                  ","+cp_ToStrODBCNull(this.w_PACENCOS8)+;
                  ","+cp_ToStrODBC(this.w_PAFLPRO8)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ZPARAMORD')
        i_extval=cp_InsertValVFPExtFlds(this,'ZPARAMORD')
        cp_CheckDeletedKey(i_cTable,0,'PACODICE',this.w_PACODICE)
        INSERT INTO (i_cTable);
              (PACODICE,PAFLPRO2,PATIPDOC2,PACODMAG2,PACENCOS2,PAFLPRO4,PATIPDOC4,PACODMAG4,PACENCOS4,PAFLPRO6,PATIPDOC6,PACODMAG6,PACENCOS6,PATIPDOC8,PACODMAG8,PACENCOS8,PAFLPRO8  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_PACODICE;
                  ,this.w_PAFLPRO2;
                  ,this.w_PATIPDOC2;
                  ,this.w_PACODMAG2;
                  ,this.w_PACENCOS2;
                  ,this.w_PAFLPRO4;
                  ,this.w_PATIPDOC4;
                  ,this.w_PACODMAG4;
                  ,this.w_PACENCOS4;
                  ,this.w_PAFLPRO6;
                  ,this.w_PATIPDOC6;
                  ,this.w_PACODMAG6;
                  ,this.w_PACENCOS6;
                  ,this.w_PATIPDOC8;
                  ,this.w_PACODMAG8;
                  ,this.w_PACENCOS8;
                  ,this.w_PAFLPRO8;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.ZPARAMORD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ZPARAMORD_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.ZPARAMORD_IDX,i_nConn)
      *
      * update ZPARAMORD
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'ZPARAMORD')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " PAFLPRO2="+cp_ToStrODBC(this.w_PAFLPRO2)+;
             ",PATIPDOC2="+cp_ToStrODBCNull(this.w_PATIPDOC2)+;
             ",PACODMAG2="+cp_ToStrODBCNull(this.w_PACODMAG2)+;
             ",PACENCOS2="+cp_ToStrODBCNull(this.w_PACENCOS2)+;
             ",PAFLPRO4="+cp_ToStrODBC(this.w_PAFLPRO4)+;
             ",PATIPDOC4="+cp_ToStrODBCNull(this.w_PATIPDOC4)+;
             ",PACODMAG4="+cp_ToStrODBCNull(this.w_PACODMAG4)+;
             ",PACENCOS4="+cp_ToStrODBCNull(this.w_PACENCOS4)+;
             ",PAFLPRO6="+cp_ToStrODBC(this.w_PAFLPRO6)+;
             ",PATIPDOC6="+cp_ToStrODBCNull(this.w_PATIPDOC6)+;
             ",PACODMAG6="+cp_ToStrODBCNull(this.w_PACODMAG6)+;
             ",PACENCOS6="+cp_ToStrODBCNull(this.w_PACENCOS6)+;
             ",PATIPDOC8="+cp_ToStrODBCNull(this.w_PATIPDOC8)+;
             ",PACODMAG8="+cp_ToStrODBCNull(this.w_PACODMAG8)+;
             ",PACENCOS8="+cp_ToStrODBCNull(this.w_PACENCOS8)+;
             ",PAFLPRO8="+cp_ToStrODBC(this.w_PAFLPRO8)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'ZPARAMORD')
        i_cWhere = cp_PKFox(i_cTable  ,'PACODICE',this.w_PACODICE  )
        UPDATE (i_cTable) SET;
              PAFLPRO2=this.w_PAFLPRO2;
             ,PATIPDOC2=this.w_PATIPDOC2;
             ,PACODMAG2=this.w_PACODMAG2;
             ,PACENCOS2=this.w_PACENCOS2;
             ,PAFLPRO4=this.w_PAFLPRO4;
             ,PATIPDOC4=this.w_PATIPDOC4;
             ,PACODMAG4=this.w_PACODMAG4;
             ,PACENCOS4=this.w_PACENCOS4;
             ,PAFLPRO6=this.w_PAFLPRO6;
             ,PATIPDOC6=this.w_PATIPDOC6;
             ,PACODMAG6=this.w_PACODMAG6;
             ,PACENCOS6=this.w_PACENCOS6;
             ,PATIPDOC8=this.w_PATIPDOC8;
             ,PACODMAG8=this.w_PACODMAG8;
             ,PACENCOS8=this.w_PACENCOS8;
             ,PAFLPRO8=this.w_PAFLPRO8;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ZPARAMORD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ZPARAMORD_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.ZPARAMORD_IDX,i_nConn)
      *
      * delete ZPARAMORD
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'PACODICE',this.w_PACODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ZPARAMORD_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ZPARAMORD_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
        if .o_PAFLPRO2<>.w_PAFLPRO2
            .w_PATIPDOC2 = IIF(.w_PAFLPRO2<>'R' AND .w_TDFLRIFI2='S',SPACE(5),IIF(.w_PAFLPRO2='R' AND .w_TDFLRIFI2='N',SPACE(5),.w_PATIPDOC2))
          .link_1_4('Full')
        endif
        .DoRTCalc(7,27,.t.)
        if .o_PAFLPRO4<>.w_PAFLPRO4
            .w_PATIPDOC4 = IIF(.w_PAFLPRO4<>'R' AND .w_TDFLRIFI4='S',SPACE(5),IIF(.w_PAFLPRO4='R' AND .w_TDFLRIFI4='N',SPACE(5),.w_PATIPDOC4))
          .link_2_7('Full')
        endif
        .DoRTCalc(29,34,.t.)
        if .o_PAFLPRO6<>.w_PAFLPRO6
            .w_PATIPDOC6 = IIF(.w_PAFLPRO6<>'R' AND .w_TDFLRIFI6='S',SPACE(5),IIF(.w_PAFLPRO6='R' AND .w_TDFLRIFI6='N',SPACE(5),.w_PATIPDOC6))
          .link_3_7('Full')
        endif
        .DoRTCalc(36,40,.t.)
        if .o_PAFLPRO8<>.w_PAFLPRO8
            .w_PATIPDOC8 = IIF(.w_PAFLPRO8<>'R' AND .w_TDFLRIFI8='S',SPACE(5),IIF(.w_PAFLPRO8='R' AND .w_TDFLRIFI8='N',SPACE(5),.w_PATIPDOC8))
          .link_4_5('Full')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(42,47,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oPACENCOS2_1_14.visible=!this.oPgFrm.Page1.oPag.oPACENCOS2_1_14.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_16.visible=!this.oPgFrm.Page1.oPag.oStr_1_16.mHide()
    this.oPgFrm.Page1.oPag.oCCDESPIA2_1_17.visible=!this.oPgFrm.Page1.oPag.oCCDESPIA2_1_17.mHide()
    this.oPgFrm.Page2.oPag.oPACENCOS4_2_13.visible=!this.oPgFrm.Page2.oPag.oPACENCOS4_2_13.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_14.visible=!this.oPgFrm.Page2.oPag.oStr_2_14.mHide()
    this.oPgFrm.Page2.oPag.oCCDESPIA4_2_15.visible=!this.oPgFrm.Page2.oPag.oCCDESPIA4_2_15.mHide()
    this.oPgFrm.Page3.oPag.oPACENCOS6_3_13.visible=!this.oPgFrm.Page3.oPag.oPACENCOS6_3_13.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_14.visible=!this.oPgFrm.Page3.oPag.oStr_3_14.mHide()
    this.oPgFrm.Page3.oPag.oCCDESPIA6_3_15.visible=!this.oPgFrm.Page3.oPag.oCCDESPIA6_3_15.mHide()
    this.oPgFrm.Page4.oPag.oPACENCOS8_4_12.visible=!this.oPgFrm.Page4.oPag.oPACENCOS8_4_12.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_14.visible=!this.oPgFrm.Page4.oPag.oStr_4_14.mHide()
    this.oPgFrm.Page4.oPag.oCCDESPIA8_4_15.visible=!this.oPgFrm.Page4.oPag.oCCDESPIA8_4_15.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gscp_apo
    if LEFT(cEvent,12)='ActivatePage'
       L_CTRLATT='this.w_PAFLPRO'+ALLTRIM(STR(VAL(RIGHT(cEvent,1))*2))
       this.w_LIVCFATT=&L_CTRLATT
    endif
    if LEFT(cEvent,9)='w_PAFLPRO'
       L_CTRLATT='this.'+LEFT(cEvent,10)
       this.w_LIVCFATT=&L_CTRLATT
    endif
    if cEvent='Load' or cEvent='Blank'
       L_CTRLATT='this.w_PAFLPRO2'
       this.w_LIVCFATT=&L_CTRLATT
    endif
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PATIPDOC2
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PATIPDOC2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_PATIPDOC2)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLINTE,TDFLRIFI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_PATIPDOC2))
          select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLINTE,TDFLRIFI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PATIPDOC2)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PATIPDOC2) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oPATIPDOC2_1_4'),i_cWhere,'',"Causali documento",'GSCP_KGO.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLINTE,TDFLRIFI";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLINTE,TDFLRIFI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PATIPDOC2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLINTE,TDFLRIFI";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_PATIPDOC2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_PATIPDOC2)
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLINTE,TDFLRIFI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PATIPDOC2 = NVL(_Link_.TDTIPDOC,space(5))
      this.w_TDDESDOC2 = NVL(_Link_.TDDESDOC,space(35))
      this.w_TDFLVEAC2 = NVL(_Link_.TDFLVEAC,space(1))
      this.w_TDCATDOC2 = NVL(_Link_.TDCATDOC,space(2))
      this.w_TDFLINTE2 = NVL(_Link_.TDFLINTE,space(1))
      this.w_TDFLRIFI2 = NVL(_Link_.TDFLRIFI,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PATIPDOC2 = space(5)
      endif
      this.w_TDDESDOC2 = space(35)
      this.w_TDFLVEAC2 = space(1)
      this.w_TDCATDOC2 = space(2)
      this.w_TDFLINTE2 = space(1)
      this.w_TDFLRIFI2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TDFLVEAC2='V' AND .w_TDCATDOC2 $ 'OR|DI' And .w_TDFLINTE2='C' AND ((.w_TDFLRIFI2='N' AND .w_PAFLPRO2<>'R') OR (.w_TDFLRIFI2='S' AND .w_PAFLPRO2='R'))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento incongruente")
        endif
        this.w_PATIPDOC2 = space(5)
        this.w_TDDESDOC2 = space(35)
        this.w_TDFLVEAC2 = space(1)
        this.w_TDCATDOC2 = space(2)
        this.w_TDFLINTE2 = space(1)
        this.w_TDFLRIFI2 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PATIPDOC2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIP_DOCU_IDX,3] and i_nFlds+6<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_4.TDTIPDOC as TDTIPDOC104"+ ",link_1_4.TDDESDOC as TDDESDOC104"+ ",link_1_4.TDFLVEAC as TDFLVEAC104"+ ",link_1_4.TDCATDOC as TDCATDOC104"+ ",link_1_4.TDFLINTE as TDFLINTE104"+ ",link_1_4.TDFLRIFI as TDFLRIFI104"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_4 on ZPARAMORD.PATIPDOC2=link_1_4.TDTIPDOC"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_4"
          i_cKey=i_cKey+'+" and ZPARAMORD.PATIPDOC2=link_1_4.TDTIPDOC(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PACODMAG2
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACODMAG2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_PACODMAG2)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_PACODMAG2))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PACODMAG2)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PACODMAG2) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oPACODMAG2_1_7'),i_cWhere,'',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACODMAG2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_PACODMAG2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_PACODMAG2)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACODMAG2 = NVL(_Link_.MGCODMAG,space(5))
      this.w_MGDESMAG2 = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_PACODMAG2 = space(5)
      endif
      this.w_MGDESMAG2 = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACODMAG2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_7.MGCODMAG as MGCODMAG107"+ ",link_1_7.MGDESMAG as MGDESMAG107"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_7 on ZPARAMORD.PACODMAG2=link_1_7.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_7"
          i_cKey=i_cKey+'+" and ZPARAMORD.PACODMAG2=link_1_7.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PACENCOS2
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACENCOS2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_PACENCOS2)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_PACENCOS2))
          select CC_CONTO,CCDESPIA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PACENCOS2)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PACENCOS2) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oPACENCOS2_1_14'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACENCOS2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_PACENCOS2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_PACENCOS2)
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACENCOS2 = NVL(_Link_.CC_CONTO,space(15))
      this.w_CCDESPIA2 = NVL(_Link_.CCDESPIA,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PACENCOS2 = space(15)
      endif
      this.w_CCDESPIA2 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACENCOS2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_14(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CENCOST_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_14.CC_CONTO as CC_CONTO114"+ ",link_1_14.CCDESPIA as CCDESPIA114"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_14 on ZPARAMORD.PACENCOS2=link_1_14.CC_CONTO"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_14"
          i_cKey=i_cKey+'+" and ZPARAMORD.PACENCOS2=link_1_14.CC_CONTO(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PATIPDOC4
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PATIPDOC4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_PATIPDOC4)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLINTE,TDFLRIFI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_PATIPDOC4))
          select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLINTE,TDFLRIFI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PATIPDOC4)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PATIPDOC4) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oPATIPDOC4_2_7'),i_cWhere,'',"Causali documento",'GSCP_KGO.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLINTE,TDFLRIFI";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLINTE,TDFLRIFI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PATIPDOC4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLINTE,TDFLRIFI";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_PATIPDOC4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_PATIPDOC4)
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLINTE,TDFLRIFI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PATIPDOC4 = NVL(_Link_.TDTIPDOC,space(5))
      this.w_TDDESDOC4 = NVL(_Link_.TDDESDOC,space(35))
      this.w_TDFLVEAC4 = NVL(_Link_.TDFLVEAC,space(1))
      this.w_TDCATDOC4 = NVL(_Link_.TDCATDOC,space(2))
      this.w_TDFLINTE4 = NVL(_Link_.TDFLINTE,space(1))
      this.w_TDFLRIFI4 = NVL(_Link_.TDFLRIFI,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PATIPDOC4 = space(5)
      endif
      this.w_TDDESDOC4 = space(35)
      this.w_TDFLVEAC4 = space(1)
      this.w_TDCATDOC4 = space(2)
      this.w_TDFLINTE4 = space(1)
      this.w_TDFLRIFI4 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TDFLVEAC4='V' AND .w_TDCATDOC4 $ 'OR|DI' And .w_TDFLINTE4='C' AND ((.w_TDFLRIFI4='N' AND .w_PAFLPRO4<>'R') OR (.w_TDFLRIFI4='S' AND .w_PAFLPRO4='R'))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento incongruente")
        endif
        this.w_PATIPDOC4 = space(5)
        this.w_TDDESDOC4 = space(35)
        this.w_TDFLVEAC4 = space(1)
        this.w_TDCATDOC4 = space(2)
        this.w_TDFLINTE4 = space(1)
        this.w_TDFLRIFI4 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PATIPDOC4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIP_DOCU_IDX,3] and i_nFlds+6<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_7.TDTIPDOC as TDTIPDOC207"+ ",link_2_7.TDDESDOC as TDDESDOC207"+ ",link_2_7.TDFLVEAC as TDFLVEAC207"+ ",link_2_7.TDCATDOC as TDCATDOC207"+ ",link_2_7.TDFLINTE as TDFLINTE207"+ ",link_2_7.TDFLRIFI as TDFLRIFI207"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_7 on ZPARAMORD.PATIPDOC4=link_2_7.TDTIPDOC"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_7"
          i_cKey=i_cKey+'+" and ZPARAMORD.PATIPDOC4=link_2_7.TDTIPDOC(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PACODMAG4
  func Link_2_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACODMAG4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_PACODMAG4)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_PACODMAG4))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PACODMAG4)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PACODMAG4) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oPACODMAG4_2_9'),i_cWhere,'',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACODMAG4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_PACODMAG4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_PACODMAG4)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACODMAG4 = NVL(_Link_.MGCODMAG,space(5))
      this.w_MGDESMAG4 = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_PACODMAG4 = space(5)
      endif
      this.w_MGDESMAG4 = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACODMAG4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_9(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_9.MGCODMAG as MGCODMAG209"+ ",link_2_9.MGDESMAG as MGDESMAG209"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_9 on ZPARAMORD.PACODMAG4=link_2_9.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_9"
          i_cKey=i_cKey+'+" and ZPARAMORD.PACODMAG4=link_2_9.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PACENCOS4
  func Link_2_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACENCOS4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_PACENCOS4)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_PACENCOS4))
          select CC_CONTO,CCDESPIA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PACENCOS4)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PACENCOS4) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oPACENCOS4_2_13'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACENCOS4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_PACENCOS4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_PACENCOS4)
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACENCOS4 = NVL(_Link_.CC_CONTO,space(15))
      this.w_CCDESPIA4 = NVL(_Link_.CCDESPIA,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PACENCOS4 = space(15)
      endif
      this.w_CCDESPIA4 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACENCOS4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_13(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CENCOST_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_13.CC_CONTO as CC_CONTO213"+ ",link_2_13.CCDESPIA as CCDESPIA213"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_13 on ZPARAMORD.PACENCOS4=link_2_13.CC_CONTO"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_13"
          i_cKey=i_cKey+'+" and ZPARAMORD.PACENCOS4=link_2_13.CC_CONTO(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PATIPDOC6
  func Link_3_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PATIPDOC6) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_PATIPDOC6)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLINTE,TDFLRIFI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_PATIPDOC6))
          select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLINTE,TDFLRIFI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PATIPDOC6)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PATIPDOC6) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oPATIPDOC6_3_7'),i_cWhere,'',"Causali documento",'GSCP_KGO.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLINTE,TDFLRIFI";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLINTE,TDFLRIFI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PATIPDOC6)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLINTE,TDFLRIFI";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_PATIPDOC6);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_PATIPDOC6)
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLINTE,TDFLRIFI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PATIPDOC6 = NVL(_Link_.TDTIPDOC,space(5))
      this.w_TDDESDOC6 = NVL(_Link_.TDDESDOC,space(35))
      this.w_TDFLVEAC6 = NVL(_Link_.TDFLVEAC,space(1))
      this.w_TDCATDOC6 = NVL(_Link_.TDCATDOC,space(2))
      this.w_TDFLINTE6 = NVL(_Link_.TDFLINTE,space(1))
      this.w_TDFLRIFI6 = NVL(_Link_.TDFLRIFI,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PATIPDOC6 = space(5)
      endif
      this.w_TDDESDOC6 = space(35)
      this.w_TDFLVEAC6 = space(1)
      this.w_TDCATDOC6 = space(2)
      this.w_TDFLINTE6 = space(1)
      this.w_TDFLRIFI6 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TDFLVEAC6='V' AND .w_TDCATDOC6 $ 'OR|DI' And .w_TDFLINTE6='C' AND ((.w_TDFLRIFI6='N' AND .w_PAFLPRO6<>'R') OR (.w_TDFLRIFI6='S' AND .w_PAFLPRO6='R'))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento incongruente")
        endif
        this.w_PATIPDOC6 = space(5)
        this.w_TDDESDOC6 = space(35)
        this.w_TDFLVEAC6 = space(1)
        this.w_TDCATDOC6 = space(2)
        this.w_TDFLINTE6 = space(1)
        this.w_TDFLRIFI6 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PATIPDOC6 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIP_DOCU_IDX,3] and i_nFlds+6<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_7.TDTIPDOC as TDTIPDOC307"+ ",link_3_7.TDDESDOC as TDDESDOC307"+ ",link_3_7.TDFLVEAC as TDFLVEAC307"+ ",link_3_7.TDCATDOC as TDCATDOC307"+ ",link_3_7.TDFLINTE as TDFLINTE307"+ ",link_3_7.TDFLRIFI as TDFLRIFI307"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_7 on ZPARAMORD.PATIPDOC6=link_3_7.TDTIPDOC"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_7"
          i_cKey=i_cKey+'+" and ZPARAMORD.PATIPDOC6=link_3_7.TDTIPDOC(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PACODMAG6
  func Link_3_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACODMAG6) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_PACODMAG6)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_PACODMAG6))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PACODMAG6)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PACODMAG6) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oPACODMAG6_3_9'),i_cWhere,'',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACODMAG6)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_PACODMAG6);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_PACODMAG6)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACODMAG6 = NVL(_Link_.MGCODMAG,space(5))
      this.w_MGDESMAG6 = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_PACODMAG6 = space(5)
      endif
      this.w_MGDESMAG6 = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACODMAG6 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_9(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_9.MGCODMAG as MGCODMAG309"+ ",link_3_9.MGDESMAG as MGDESMAG309"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_9 on ZPARAMORD.PACODMAG6=link_3_9.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_9"
          i_cKey=i_cKey+'+" and ZPARAMORD.PACODMAG6=link_3_9.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PACENCOS6
  func Link_3_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACENCOS6) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_PACENCOS6)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_PACENCOS6))
          select CC_CONTO,CCDESPIA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PACENCOS6)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PACENCOS6) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oPACENCOS6_3_13'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACENCOS6)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_PACENCOS6);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_PACENCOS6)
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACENCOS6 = NVL(_Link_.CC_CONTO,space(15))
      this.w_CCDESPIA6 = NVL(_Link_.CCDESPIA,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PACENCOS6 = space(15)
      endif
      this.w_CCDESPIA6 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACENCOS6 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_13(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CENCOST_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_13.CC_CONTO as CC_CONTO313"+ ",link_3_13.CCDESPIA as CCDESPIA313"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_13 on ZPARAMORD.PACENCOS6=link_3_13.CC_CONTO"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_13"
          i_cKey=i_cKey+'+" and ZPARAMORD.PACENCOS6=link_3_13.CC_CONTO(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PATIPDOC8
  func Link_4_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PATIPDOC8) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_PATIPDOC8)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLINTE,TDFLRIFI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_PATIPDOC8))
          select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLINTE,TDFLRIFI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PATIPDOC8)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PATIPDOC8) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oPATIPDOC8_4_5'),i_cWhere,'',"Causali documento",'GSCP_KGO.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLINTE,TDFLRIFI";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLINTE,TDFLRIFI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PATIPDOC8)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLINTE,TDFLRIFI";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_PATIPDOC8);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_PATIPDOC8)
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLINTE,TDFLRIFI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PATIPDOC8 = NVL(_Link_.TDTIPDOC,space(5))
      this.w_TDDESDOC8 = NVL(_Link_.TDDESDOC,space(35))
      this.w_TDFLVEAC8 = NVL(_Link_.TDFLVEAC,space(1))
      this.w_TDCATDOC8 = NVL(_Link_.TDCATDOC,space(2))
      this.w_TDFLINTE8 = NVL(_Link_.TDFLINTE,space(1))
      this.w_TDFLRIFI8 = NVL(_Link_.TDFLRIFI,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PATIPDOC8 = space(5)
      endif
      this.w_TDDESDOC8 = space(35)
      this.w_TDFLVEAC8 = space(1)
      this.w_TDCATDOC8 = space(2)
      this.w_TDFLINTE8 = space(1)
      this.w_TDFLRIFI8 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TDFLVEAC8='V' AND .w_TDCATDOC8 $ 'OR|DI' And .w_TDFLINTE8='C' AND ((.w_TDFLRIFI8='N' AND .w_PAFLPRO8<>'R') OR (.w_TDFLRIFI8='S' AND .w_PAFLPRO8='R'))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento incongruente")
        endif
        this.w_PATIPDOC8 = space(5)
        this.w_TDDESDOC8 = space(35)
        this.w_TDFLVEAC8 = space(1)
        this.w_TDCATDOC8 = space(2)
        this.w_TDFLINTE8 = space(1)
        this.w_TDFLRIFI8 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PATIPDOC8 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIP_DOCU_IDX,3] and i_nFlds+6<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_5.TDTIPDOC as TDTIPDOC405"+ ",link_4_5.TDDESDOC as TDDESDOC405"+ ",link_4_5.TDFLVEAC as TDFLVEAC405"+ ",link_4_5.TDCATDOC as TDCATDOC405"+ ",link_4_5.TDFLINTE as TDFLINTE405"+ ",link_4_5.TDFLRIFI as TDFLRIFI405"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_5 on ZPARAMORD.PATIPDOC8=link_4_5.TDTIPDOC"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_5"
          i_cKey=i_cKey+'+" and ZPARAMORD.PATIPDOC8=link_4_5.TDTIPDOC(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PACODMAG8
  func Link_4_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACODMAG8) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_PACODMAG8)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_PACODMAG8))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PACODMAG8)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PACODMAG8) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oPACODMAG8_4_7'),i_cWhere,'',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACODMAG8)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_PACODMAG8);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_PACODMAG8)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACODMAG8 = NVL(_Link_.MGCODMAG,space(5))
      this.w_MGDESMAG8 = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_PACODMAG8 = space(5)
      endif
      this.w_MGDESMAG8 = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACODMAG8 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_7.MGCODMAG as MGCODMAG407"+ ",link_4_7.MGDESMAG as MGDESMAG407"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_7 on ZPARAMORD.PACODMAG8=link_4_7.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_7"
          i_cKey=i_cKey+'+" and ZPARAMORD.PACODMAG8=link_4_7.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PACENCOS8
  func Link_4_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACENCOS8) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_PACENCOS8)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_PACENCOS8))
          select CC_CONTO,CCDESPIA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PACENCOS8)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PACENCOS8) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oPACENCOS8_4_12'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACENCOS8)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_PACENCOS8);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_PACENCOS8)
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACENCOS8 = NVL(_Link_.CC_CONTO,space(15))
      this.w_CCDESPIA8 = NVL(_Link_.CCDESPIA,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PACENCOS8 = space(15)
      endif
      this.w_CCDESPIA8 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACENCOS8 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_12(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CENCOST_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_12.CC_CONTO as CC_CONTO412"+ ",link_4_12.CCDESPIA as CCDESPIA412"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_12 on ZPARAMORD.PACENCOS8=link_4_12.CC_CONTO"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_12"
          i_cKey=i_cKey+'+" and ZPARAMORD.PACENCOS8=link_4_12.CC_CONTO(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPAFLPRO2_1_3.RadioValue()==this.w_PAFLPRO2)
      this.oPgFrm.Page1.oPag.oPAFLPRO2_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPATIPDOC2_1_4.value==this.w_PATIPDOC2)
      this.oPgFrm.Page1.oPag.oPATIPDOC2_1_4.value=this.w_PATIPDOC2
    endif
    if not(this.oPgFrm.Page1.oPag.oPACODMAG2_1_7.value==this.w_PACODMAG2)
      this.oPgFrm.Page1.oPag.oPACODMAG2_1_7.value=this.w_PACODMAG2
    endif
    if not(this.oPgFrm.Page1.oPag.oTDDESDOC2_1_12.value==this.w_TDDESDOC2)
      this.oPgFrm.Page1.oPag.oTDDESDOC2_1_12.value=this.w_TDDESDOC2
    endif
    if not(this.oPgFrm.Page1.oPag.oMGDESMAG2_1_13.value==this.w_MGDESMAG2)
      this.oPgFrm.Page1.oPag.oMGDESMAG2_1_13.value=this.w_MGDESMAG2
    endif
    if not(this.oPgFrm.Page1.oPag.oPACENCOS2_1_14.value==this.w_PACENCOS2)
      this.oPgFrm.Page1.oPag.oPACENCOS2_1_14.value=this.w_PACENCOS2
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDESPIA2_1_17.value==this.w_CCDESPIA2)
      this.oPgFrm.Page1.oPag.oCCDESPIA2_1_17.value=this.w_CCDESPIA2
    endif
    if not(this.oPgFrm.Page2.oPag.oPAFLPRO4_2_6.RadioValue()==this.w_PAFLPRO4)
      this.oPgFrm.Page2.oPag.oPAFLPRO4_2_6.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPATIPDOC4_2_7.value==this.w_PATIPDOC4)
      this.oPgFrm.Page2.oPag.oPATIPDOC4_2_7.value=this.w_PATIPDOC4
    endif
    if not(this.oPgFrm.Page2.oPag.oPACODMAG4_2_9.value==this.w_PACODMAG4)
      this.oPgFrm.Page2.oPag.oPACODMAG4_2_9.value=this.w_PACODMAG4
    endif
    if not(this.oPgFrm.Page2.oPag.oTDDESDOC4_2_11.value==this.w_TDDESDOC4)
      this.oPgFrm.Page2.oPag.oTDDESDOC4_2_11.value=this.w_TDDESDOC4
    endif
    if not(this.oPgFrm.Page2.oPag.oMGDESMAG4_2_12.value==this.w_MGDESMAG4)
      this.oPgFrm.Page2.oPag.oMGDESMAG4_2_12.value=this.w_MGDESMAG4
    endif
    if not(this.oPgFrm.Page2.oPag.oPACENCOS4_2_13.value==this.w_PACENCOS4)
      this.oPgFrm.Page2.oPag.oPACENCOS4_2_13.value=this.w_PACENCOS4
    endif
    if not(this.oPgFrm.Page2.oPag.oCCDESPIA4_2_15.value==this.w_CCDESPIA4)
      this.oPgFrm.Page2.oPag.oCCDESPIA4_2_15.value=this.w_CCDESPIA4
    endif
    if not(this.oPgFrm.Page3.oPag.oPAFLPRO6_3_6.RadioValue()==this.w_PAFLPRO6)
      this.oPgFrm.Page3.oPag.oPAFLPRO6_3_6.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oPATIPDOC6_3_7.value==this.w_PATIPDOC6)
      this.oPgFrm.Page3.oPag.oPATIPDOC6_3_7.value=this.w_PATIPDOC6
    endif
    if not(this.oPgFrm.Page3.oPag.oPACODMAG6_3_9.value==this.w_PACODMAG6)
      this.oPgFrm.Page3.oPag.oPACODMAG6_3_9.value=this.w_PACODMAG6
    endif
    if not(this.oPgFrm.Page3.oPag.oTDDESDOC6_3_11.value==this.w_TDDESDOC6)
      this.oPgFrm.Page3.oPag.oTDDESDOC6_3_11.value=this.w_TDDESDOC6
    endif
    if not(this.oPgFrm.Page3.oPag.oMGDESMAG6_3_12.value==this.w_MGDESMAG6)
      this.oPgFrm.Page3.oPag.oMGDESMAG6_3_12.value=this.w_MGDESMAG6
    endif
    if not(this.oPgFrm.Page3.oPag.oPACENCOS6_3_13.value==this.w_PACENCOS6)
      this.oPgFrm.Page3.oPag.oPACENCOS6_3_13.value=this.w_PACENCOS6
    endif
    if not(this.oPgFrm.Page3.oPag.oCCDESPIA6_3_15.value==this.w_CCDESPIA6)
      this.oPgFrm.Page3.oPag.oCCDESPIA6_3_15.value=this.w_CCDESPIA6
    endif
    if not(this.oPgFrm.Page4.oPag.oPATIPDOC8_4_5.value==this.w_PATIPDOC8)
      this.oPgFrm.Page4.oPag.oPATIPDOC8_4_5.value=this.w_PATIPDOC8
    endif
    if not(this.oPgFrm.Page4.oPag.oPACODMAG8_4_7.value==this.w_PACODMAG8)
      this.oPgFrm.Page4.oPag.oPACODMAG8_4_7.value=this.w_PACODMAG8
    endif
    if not(this.oPgFrm.Page4.oPag.oTDDESDOC8_4_9.value==this.w_TDDESDOC8)
      this.oPgFrm.Page4.oPag.oTDDESDOC8_4_9.value=this.w_TDDESDOC8
    endif
    if not(this.oPgFrm.Page4.oPag.oMGDESMAG8_4_10.value==this.w_MGDESMAG8)
      this.oPgFrm.Page4.oPag.oMGDESMAG8_4_10.value=this.w_MGDESMAG8
    endif
    if not(this.oPgFrm.Page4.oPag.oPACENCOS8_4_12.value==this.w_PACENCOS8)
      this.oPgFrm.Page4.oPag.oPACENCOS8_4_12.value=this.w_PACENCOS8
    endif
    if not(this.oPgFrm.Page4.oPag.oPAFLPRO8_4_13.RadioValue()==this.w_PAFLPRO8)
      this.oPgFrm.Page4.oPag.oPAFLPRO8_4_13.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oCCDESPIA8_4_15.value==this.w_CCDESPIA8)
      this.oPgFrm.Page4.oPag.oCCDESPIA8_4_15.value=this.w_CCDESPIA8
    endif
    cp_SetControlsValueExtFlds(this,'ZPARAMORD')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_PATIPDOC2)) or not(.w_TDFLVEAC2='V' AND .w_TDCATDOC2 $ 'OR|DI' And .w_TDFLINTE2='C' AND ((.w_TDFLRIFI2='N' AND .w_PAFLPRO2<>'R') OR (.w_TDFLRIFI2='S' AND .w_PAFLPRO2='R'))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPATIPDOC2_1_4.SetFocus()
            i_bnoObbl = !empty(.w_PATIPDOC2)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento incongruente")
          case   (empty(.w_PACODMAG2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPACODMAG2_1_7.SetFocus()
            i_bnoObbl = !empty(.w_PACODMAG2)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_PATIPDOC4)) or not(.w_TDFLVEAC4='V' AND .w_TDCATDOC4 $ 'OR|DI' And .w_TDFLINTE4='C' AND ((.w_TDFLRIFI4='N' AND .w_PAFLPRO4<>'R') OR (.w_TDFLRIFI4='S' AND .w_PAFLPRO4='R'))))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPATIPDOC4_2_7.SetFocus()
            i_bnoObbl = !empty(.w_PATIPDOC4)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento incongruente")
          case   (empty(.w_PACODMAG4))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPACODMAG4_2_9.SetFocus()
            i_bnoObbl = !empty(.w_PACODMAG4)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_PATIPDOC6)) or not(.w_TDFLVEAC6='V' AND .w_TDCATDOC6 $ 'OR|DI' And .w_TDFLINTE6='C' AND ((.w_TDFLRIFI6='N' AND .w_PAFLPRO6<>'R') OR (.w_TDFLRIFI6='S' AND .w_PAFLPRO6='R'))))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oPATIPDOC6_3_7.SetFocus()
            i_bnoObbl = !empty(.w_PATIPDOC6)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento incongruente")
          case   (empty(.w_PACODMAG6))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oPACODMAG6_3_9.SetFocus()
            i_bnoObbl = !empty(.w_PACODMAG6)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_PATIPDOC8)) or not(.w_TDFLVEAC8='V' AND .w_TDCATDOC8 $ 'OR|DI' And .w_TDFLINTE8='C' AND ((.w_TDFLRIFI8='N' AND .w_PAFLPRO8<>'R') OR (.w_TDFLRIFI8='S' AND .w_PAFLPRO8='R'))))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oPATIPDOC8_4_5.SetFocus()
            i_bnoObbl = !empty(.w_PATIPDOC8)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento incongruente")
          case   (empty(.w_PACODMAG8))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oPACODMAG8_4_7.SetFocus()
            i_bnoObbl = !empty(.w_PACODMAG8)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PAFLPRO2 = this.w_PAFLPRO2
    this.o_PAFLPRO4 = this.w_PAFLPRO4
    this.o_PAFLPRO6 = this.w_PAFLPRO6
    this.o_PAFLPRO8 = this.w_PAFLPRO8
    return

enddefine

* --- Define pages as container
define class tgscp_apoPag1 as StdContainer
  Width  = 528
  height = 185
  stdWidth  = 528
  stdheight = 185
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

    add object oBmp_1_2 as image with uid="FKJKHOZIOA",left=26, top=12, width=20,height=28,;
      Picture="bmp\black.ico"


  add object oPAFLPRO2_1_3 as StdCombo with uid="WNESRDKPYS",rtseq=5,rtrep=.f.,left=60,top=15,width=142,height=21;
    , HelpContextID = 37285592;
    , cFormVar="w_PAFLPRO2",RowSource=""+"Accetta,"+"Accetta in provvisorio,"+"Rifiuta ordine", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPAFLPRO2_1_3.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'R',;
    space(1)))))
  endfunc
  func oPAFLPRO2_1_3.GetRadio()
    this.Parent.oContained.w_PAFLPRO2 = this.RadioValue()
    return .t.
  endfunc

  func oPAFLPRO2_1_3.SetRadio()
    this.Parent.oContained.w_PAFLPRO2=trim(this.Parent.oContained.w_PAFLPRO2)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLPRO2=='N',1,;
      iif(this.Parent.oContained.w_PAFLPRO2=='S',2,;
      iif(this.Parent.oContained.w_PAFLPRO2=='R',3,;
      0)))
  endfunc

  add object oPATIPDOC2_1_4 as StdField with uid="ZWXDQYNLZQ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_PATIPDOC2", cQueryName = "PATIPDOC2",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento incongruente",;
    ToolTipText = "Causale documenti di vendita",;
    HelpContextID = 3869607,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=133, Top=51, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_PATIPDOC2"

  func oPATIPDOC2_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oPATIPDOC2_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPATIPDOC2_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oPATIPDOC2_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Causali documento",'GSCP_KGO.TIP_DOCU_VZM',this.parent.oContained
  endproc

  add object oPACODMAG2_1_7 as StdField with uid="JPXMEZURSS",rtseq=8,rtrep=.f.,;
    cFormVar = "w_PACODMAG2", cQueryName = "PACODMAG2",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 133569443,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=133, Top=84, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_PACODMAG2"

  func oPACODMAG2_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACODMAG2_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPACODMAG2_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oPACODMAG2_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Magazzini",'',this.parent.oContained
  endproc

  add object oTDDESDOC2_1_12 as StdField with uid="ZTWVQUWSOD",rtseq=12,rtrep=.f.,;
    cFormVar = "w_TDDESDOC2", cQueryName = "TDDESDOC2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 1050727,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=197, Top=51, InputMask=replicate('X',35)

  add object oMGDESMAG2_1_13 as StdField with uid="SJSYJUDWMJ",rtseq=13,rtrep=.f.,;
    cFormVar = "w_MGDESMAG2", cQueryName = "MGDESMAG2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 118490579,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=197, Top=84, InputMask=replicate('X',30)

  add object oPACENCOS2_1_14 as StdField with uid="XDWNFUWOQP",rtseq=14,rtrep=.f.,;
    cFormVar = "w_PACENCOS2", cQueryName = "PACENCOS2",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice centro di costo",;
    HelpContextID = 23075735,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=133, Top=117, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", oKey_1_1="CC_CONTO", oKey_1_2="this.w_PACENCOS2"

  func oPACENCOS2_1_14.mHide()
    with this.Parent.oContained
      return (g_PERCCM<>'S')
    endwith
  endfunc

  func oPACENCOS2_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACENCOS2_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPACENCOS2_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oPACENCOS2_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oCCDESPIA2_1_17 as StdField with uid="SNZPUIVKIT",rtseq=16,rtrep=.f.,;
    cFormVar = "w_CCDESPIA2", cQueryName = "CCDESPIA2",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 200275335,;
   bGlobalFont=.t.,;
    Height=21, Width=243, Left=254, Top=117, InputMask=replicate('X',40)

  func oCCDESPIA2_1_17.mHide()
    with this.Parent.oContained
      return (g_PERCCM<>'S')
    endwith
  endfunc

  add object oStr_1_6 as StdString with uid="KOASBANSIO",Visible=.t., Left=3, Top=54,;
    Alignment=1, Width=126, Height=18,;
    Caption="Causale documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="EDKIWZMDJY",Visible=.t., Left=27, Top=87,;
    Alignment=1, Width=102, Height=18,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="TKROZFEYRB",Visible=.t., Left=37, Top=119,;
    Alignment=1, Width=92, Height=18,;
    Caption="Centro di costo:"  ;
  , bGlobalFont=.t.

  func oStr_1_16.mHide()
    with this.Parent.oContained
      return (g_PERCCM<>'S')
    endwith
  endfunc
enddefine
define class tgscp_apoPag2 as StdContainer
  Width  = 528
  height = 185
  stdWidth  = 528
  stdheight = 185
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

    add object oBmp_2_5 as image with uid="IOGNWPLZZJ",left=26, top=12, width=20,height=28,;
      Picture="bmp\red.ico"


  add object oPAFLPRO4_2_6 as StdCombo with uid="VMATNHOBRR",rtseq=27,rtrep=.f.,left=60,top=15,width=142,height=21;
    , HelpContextID = 37285590;
    , cFormVar="w_PAFLPRO4",RowSource=""+"Accetta,"+"Accetta in provvisorio,"+"Rifiuta ordine", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oPAFLPRO4_2_6.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'R',;
    space(1)))))
  endfunc
  func oPAFLPRO4_2_6.GetRadio()
    this.Parent.oContained.w_PAFLPRO4 = this.RadioValue()
    return .t.
  endfunc

  func oPAFLPRO4_2_6.SetRadio()
    this.Parent.oContained.w_PAFLPRO4=trim(this.Parent.oContained.w_PAFLPRO4)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLPRO4=='N',1,;
      iif(this.Parent.oContained.w_PAFLPRO4=='S',2,;
      iif(this.Parent.oContained.w_PAFLPRO4=='R',3,;
      0)))
  endfunc

  add object oPATIPDOC4_2_7 as StdField with uid="KPGRQDFJHR",rtseq=28,rtrep=.f.,;
    cFormVar = "w_PATIPDOC4", cQueryName = "PATIPDOC4",;
    bObbl = .t. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento incongruente",;
    ToolTipText = "Causale documenti di vendita",;
    HelpContextID = 3869575,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=133, Top=51, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_PATIPDOC4"

  func oPATIPDOC4_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oPATIPDOC4_2_7.ecpDrop(oSource)
    this.Parent.oContained.link_2_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPATIPDOC4_2_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oPATIPDOC4_2_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Causali documento",'GSCP_KGO.TIP_DOCU_VZM',this.parent.oContained
  endproc

  add object oPACODMAG4_2_9 as StdField with uid="WMBGJMPVUY",rtseq=29,rtrep=.f.,;
    cFormVar = "w_PACODMAG4", cQueryName = "PACODMAG4",;
    bObbl = .t. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 133569411,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=133, Top=84, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_PACODMAG4"

  func oPACODMAG4_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACODMAG4_2_9.ecpDrop(oSource)
    this.Parent.oContained.link_2_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPACODMAG4_2_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oPACODMAG4_2_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Magazzini",'',this.parent.oContained
  endproc

  add object oTDDESDOC4_2_11 as StdField with uid="LDDPSSEVCN",rtseq=30,rtrep=.f.,;
    cFormVar = "w_TDDESDOC4", cQueryName = "TDDESDOC4",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 1050695,;
   bGlobalFont=.t.,;
    Height=21, Width=297, Left=197, Top=51, InputMask=replicate('X',35)

  add object oMGDESMAG4_2_12 as StdField with uid="WQFPVGAEEE",rtseq=31,rtrep=.f.,;
    cFormVar = "w_MGDESMAG4", cQueryName = "MGDESMAG4",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 118490547,;
   bGlobalFont=.t.,;
    Height=21, Width=297, Left=197, Top=84, InputMask=replicate('X',30)

  add object oPACENCOS4_2_13 as StdField with uid="YANDAVEZKQ",rtseq=32,rtrep=.f.,;
    cFormVar = "w_PACENCOS4", cQueryName = "PACENCOS4",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice centro di costo",;
    HelpContextID = 23075703,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=133, Top=117, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", oKey_1_1="CC_CONTO", oKey_1_2="this.w_PACENCOS4"

  func oPACENCOS4_2_13.mHide()
    with this.Parent.oContained
      return (g_PERCCM<>'S')
    endwith
  endfunc

  func oPACENCOS4_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACENCOS4_2_13.ecpDrop(oSource)
    this.Parent.oContained.link_2_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPACENCOS4_2_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oPACENCOS4_2_13'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oCCDESPIA4_2_15 as StdField with uid="LDRFPSJBJU",rtseq=33,rtrep=.f.,;
    cFormVar = "w_CCDESPIA4", cQueryName = "CCDESPIA4",;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 200275367,;
   bGlobalFont=.t.,;
    Height=21, Width=240, Left=254, Top=117, InputMask=replicate('X',40)

  func oCCDESPIA4_2_15.mHide()
    with this.Parent.oContained
      return (g_PERCCM<>'S')
    endwith
  endfunc

  add object oStr_2_8 as StdString with uid="KGSOFTRCJJ",Visible=.t., Left=3, Top=54,;
    Alignment=1, Width=126, Height=18,;
    Caption="Causale documento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_10 as StdString with uid="NAUTVHGIHC",Visible=.t., Left=27, Top=87,;
    Alignment=1, Width=102, Height=18,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_2_14 as StdString with uid="MURAVTPEJN",Visible=.t., Left=37, Top=119,;
    Alignment=1, Width=92, Height=18,;
    Caption="Centro di costo:"  ;
  , bGlobalFont=.t.

  func oStr_2_14.mHide()
    with this.Parent.oContained
      return (g_PERCCM<>'S')
    endwith
  endfunc
enddefine
define class tgscp_apoPag3 as StdContainer
  Width  = 528
  height = 185
  stdWidth  = 528
  stdheight = 185
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

    add object oBmp_3_5 as image with uid="LSAHQIBTZA",left=26, top=12, width=20,height=28,;
      Picture="bmp\yellow.ico"


  add object oPAFLPRO6_3_6 as StdCombo with uid="SXOAULDSGN",rtseq=34,rtrep=.f.,left=60,top=15,width=142,height=21;
    , HelpContextID = 37285588;
    , cFormVar="w_PAFLPRO6",RowSource=""+"Accetta,"+"Accetta in provvisorio,"+"Rifiuta ordine", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oPAFLPRO6_3_6.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'R',;
    space(1)))))
  endfunc
  func oPAFLPRO6_3_6.GetRadio()
    this.Parent.oContained.w_PAFLPRO6 = this.RadioValue()
    return .t.
  endfunc

  func oPAFLPRO6_3_6.SetRadio()
    this.Parent.oContained.w_PAFLPRO6=trim(this.Parent.oContained.w_PAFLPRO6)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLPRO6=='N',1,;
      iif(this.Parent.oContained.w_PAFLPRO6=='S',2,;
      iif(this.Parent.oContained.w_PAFLPRO6=='R',3,;
      0)))
  endfunc

  add object oPATIPDOC6_3_7 as StdField with uid="FHMMWCFNBK",rtseq=35,rtrep=.f.,;
    cFormVar = "w_PATIPDOC6", cQueryName = "PATIPDOC6",;
    bObbl = .t. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento incongruente",;
    ToolTipText = "Causale documenti di vendita",;
    HelpContextID = 3869543,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=133, Top=51, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_PATIPDOC6"

  func oPATIPDOC6_3_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oPATIPDOC6_3_7.ecpDrop(oSource)
    this.Parent.oContained.link_3_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPATIPDOC6_3_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oPATIPDOC6_3_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Causali documento",'GSCP_KGO.TIP_DOCU_VZM',this.parent.oContained
  endproc

  add object oPACODMAG6_3_9 as StdField with uid="VGKNBKYBGO",rtseq=36,rtrep=.f.,;
    cFormVar = "w_PACODMAG6", cQueryName = "PACODMAG6",;
    bObbl = .t. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 133569379,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=133, Top=84, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_PACODMAG6"

  func oPACODMAG6_3_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACODMAG6_3_9.ecpDrop(oSource)
    this.Parent.oContained.link_3_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPACODMAG6_3_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oPACODMAG6_3_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Magazzini",'',this.parent.oContained
  endproc

  add object oTDDESDOC6_3_11 as StdField with uid="XBECRDBXJU",rtseq=37,rtrep=.f.,;
    cFormVar = "w_TDDESDOC6", cQueryName = "TDDESDOC6",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 1050663,;
   bGlobalFont=.t.,;
    Height=21, Width=299, Left=197, Top=51, InputMask=replicate('X',35)

  add object oMGDESMAG6_3_12 as StdField with uid="NZRNXFYJZG",rtseq=38,rtrep=.f.,;
    cFormVar = "w_MGDESMAG6", cQueryName = "MGDESMAG6",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 118490515,;
   bGlobalFont=.t.,;
    Height=21, Width=299, Left=197, Top=84, InputMask=replicate('X',30)

  add object oPACENCOS6_3_13 as StdField with uid="FFARMBQUEM",rtseq=39,rtrep=.f.,;
    cFormVar = "w_PACENCOS6", cQueryName = "PACENCOS6",;
    bObbl = .f. , nPag = 3, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice centro di costo",;
    HelpContextID = 23075671,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=133, Top=117, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", oKey_1_1="CC_CONTO", oKey_1_2="this.w_PACENCOS6"

  func oPACENCOS6_3_13.mHide()
    with this.Parent.oContained
      return (g_PERCCM<>'S')
    endwith
  endfunc

  func oPACENCOS6_3_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACENCOS6_3_13.ecpDrop(oSource)
    this.Parent.oContained.link_3_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPACENCOS6_3_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oPACENCOS6_3_13'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oCCDESPIA6_3_15 as StdField with uid="UXNGJTALNV",rtseq=40,rtrep=.f.,;
    cFormVar = "w_CCDESPIA6", cQueryName = "CCDESPIA6",;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 200275399,;
   bGlobalFont=.t.,;
    Height=21, Width=240, Left=256, Top=117, InputMask=replicate('X',40)

  func oCCDESPIA6_3_15.mHide()
    with this.Parent.oContained
      return (g_PERCCM<>'S')
    endwith
  endfunc

  add object oStr_3_8 as StdString with uid="NZDJREGFIW",Visible=.t., Left=3, Top=54,;
    Alignment=1, Width=126, Height=18,;
    Caption="Causale documento:"  ;
  , bGlobalFont=.t.

  add object oStr_3_10 as StdString with uid="PITNSMXHVU",Visible=.t., Left=27, Top=87,;
    Alignment=1, Width=102, Height=18,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_3_14 as StdString with uid="KIALLTFKMT",Visible=.t., Left=37, Top=119,;
    Alignment=1, Width=92, Height=18,;
    Caption="Centro di costo:"  ;
  , bGlobalFont=.t.

  func oStr_3_14.mHide()
    with this.Parent.oContained
      return (g_PERCCM<>'S')
    endwith
  endfunc
enddefine
define class tgscp_apoPag4 as StdContainer
  Width  = 528
  height = 185
  stdWidth  = 528
  stdheight = 185
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

    add object oBmp_4_11 as image with uid="PZXQDOZVJO",left=26, top=12, width=20,height=28,;
      Picture="bmp\green.ico"

  add object oPATIPDOC8_4_5 as StdField with uid="TPSYWSQGCH",rtseq=41,rtrep=.f.,;
    cFormVar = "w_PATIPDOC8", cQueryName = "PATIPDOC8",;
    bObbl = .t. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento incongruente",;
    ToolTipText = "Causale documenti di vendita",;
    HelpContextID = 3869511,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=133, Top=51, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_PATIPDOC8"

  func oPATIPDOC8_4_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oPATIPDOC8_4_5.ecpDrop(oSource)
    this.Parent.oContained.link_4_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPATIPDOC8_4_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oPATIPDOC8_4_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Causali documento",'GSCP_KGO.TIP_DOCU_VZM',this.parent.oContained
  endproc

  add object oPACODMAG8_4_7 as StdField with uid="MXBWMGGFXT",rtseq=42,rtrep=.f.,;
    cFormVar = "w_PACODMAG8", cQueryName = "PACODMAG8",;
    bObbl = .t. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 133569347,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=133, Top=84, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_PACODMAG8"

  func oPACODMAG8_4_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACODMAG8_4_7.ecpDrop(oSource)
    this.Parent.oContained.link_4_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPACODMAG8_4_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oPACODMAG8_4_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Magazzini",'',this.parent.oContained
  endproc

  add object oTDDESDOC8_4_9 as StdField with uid="PAXZHBIKJS",rtseq=43,rtrep=.f.,;
    cFormVar = "w_TDDESDOC8", cQueryName = "TDDESDOC8",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 1050631,;
   bGlobalFont=.t.,;
    Height=21, Width=297, Left=197, Top=51, InputMask=replicate('X',35)

  add object oMGDESMAG8_4_10 as StdField with uid="OZBBJLJKQW",rtseq=44,rtrep=.f.,;
    cFormVar = "w_MGDESMAG8", cQueryName = "MGDESMAG8",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 118490483,;
   bGlobalFont=.t.,;
    Height=21, Width=297, Left=197, Top=84, InputMask=replicate('X',30)

  add object oPACENCOS8_4_12 as StdField with uid="IDGJMMQKPM",rtseq=45,rtrep=.f.,;
    cFormVar = "w_PACENCOS8", cQueryName = "PACENCOS8",;
    bObbl = .f. , nPag = 4, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice centro di costo",;
    HelpContextID = 23075639,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=133, Top=117, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", oKey_1_1="CC_CONTO", oKey_1_2="this.w_PACENCOS8"

  func oPACENCOS8_4_12.mHide()
    with this.Parent.oContained
      return (g_PERCCM<>'S')
    endwith
  endfunc

  func oPACENCOS8_4_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACENCOS8_4_12.ecpDrop(oSource)
    this.Parent.oContained.link_4_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPACENCOS8_4_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oPACENCOS8_4_12'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc


  add object oPAFLPRO8_4_13 as StdCombo with uid="FCBVDCVUJX",rtseq=46,rtrep=.f.,left=60,top=15,width=142,height=21;
    , HelpContextID = 37285586;
    , cFormVar="w_PAFLPRO8",RowSource=""+"Accetta,"+"Accetta in provvisorio,"+"Rifiuta ordine", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oPAFLPRO8_4_13.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'R',;
    space(1)))))
  endfunc
  func oPAFLPRO8_4_13.GetRadio()
    this.Parent.oContained.w_PAFLPRO8 = this.RadioValue()
    return .t.
  endfunc

  func oPAFLPRO8_4_13.SetRadio()
    this.Parent.oContained.w_PAFLPRO8=trim(this.Parent.oContained.w_PAFLPRO8)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLPRO8=='N',1,;
      iif(this.Parent.oContained.w_PAFLPRO8=='S',2,;
      iif(this.Parent.oContained.w_PAFLPRO8=='R',3,;
      0)))
  endfunc

  add object oCCDESPIA8_4_15 as StdField with uid="FNVWQNNRAZ",rtseq=47,rtrep=.f.,;
    cFormVar = "w_CCDESPIA8", cQueryName = "CCDESPIA8",;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 200275431,;
   bGlobalFont=.t.,;
    Height=21, Width=240, Left=254, Top=117, InputMask=replicate('X',40)

  func oCCDESPIA8_4_15.mHide()
    with this.Parent.oContained
      return (g_PERCCM<>'S')
    endwith
  endfunc

  add object oStr_4_6 as StdString with uid="TPXLVCNGLU",Visible=.t., Left=3, Top=54,;
    Alignment=1, Width=126, Height=18,;
    Caption="Causale documento:"  ;
  , bGlobalFont=.t.

  add object oStr_4_8 as StdString with uid="VRPOUWDZVP",Visible=.t., Left=27, Top=87,;
    Alignment=1, Width=102, Height=18,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_4_14 as StdString with uid="DILAEEBUIP",Visible=.t., Left=37, Top=119,;
    Alignment=1, Width=92, Height=18,;
    Caption="Centro di costo:"  ;
  , bGlobalFont=.t.

  func oStr_4_14.mHide()
    with this.Parent.oContained
      return (g_PERCCM<>'S')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscp_apo','ZPARAMORD','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PACODICE=ZPARAMORD.PACODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
