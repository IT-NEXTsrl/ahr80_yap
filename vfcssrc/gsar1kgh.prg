* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar1kgh                                                        *
*              Gestione path                                                   *
*                                                                              *
*      Author: Zucchetti                                                       *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_210]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-02-07                                                      *
* Last revis.: 2012-11-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar1kgh",oParentObject))

* --- Class definition
define class tgsar1kgh as StdForm
  Top    = -1
  Left   = 6

  * --- Standard Properties
  Width  = 799
  Height = 185
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-11-19"
  HelpContextID=35092329
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=16

  * --- Constant Properties
  _IDX = 0
  AZIENDA_IDX = 0
  CONTROPA_IDX = 0
  CAU_CONT_IDX = 0
  CONTI_IDX = 0
  cPrg = "gsar1kgh"
  cComment = "Gestione path"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_COCODAZI = space(5)
  w_RAGAZI = space(40)
  w_COPATHCP = space(254)
  w_HIDE_CP = .F.
  w_PATHXML = space(254)
  o_PATHXML = space(254)
  w_ADDAZI = space(1)
  o_ADDAZI = space(1)
  w_COPATHDO = space(254)
  w_PATHDOC = space(254)
  o_PATHDOC = space(254)
  w_COPATHSJ = space(254)
  w_PATHSJ = space(254)
  w_PATHAZ = space(10)
  w_CHKAZI = space(1)
  w_GETPAXML = space(254)
  w_GETPADOC = space(254)
  w_LPATH = .F.
  * --- Area Manuale = Declare Variables
  * --- gsar1kgh
      func HasCPEvents(i_cOp)
         if upper(i_cop)='ECPSAVE'
           This.NotifyEvent('Salva')
           return ( .F. )
         else
           return(.T.)
         endif
      endfunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar1kghPag1","gsar1kgh",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPATHXML_1_7
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='CONTROPA'
    this.cWorkTables[3]='CAU_CONT'
    this.cWorkTables[4]='CONTI'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_COCODAZI=space(5)
      .w_RAGAZI=space(40)
      .w_COPATHCP=space(254)
      .w_HIDE_CP=.f.
      .w_PATHXML=space(254)
      .w_ADDAZI=space(1)
      .w_COPATHDO=space(254)
      .w_PATHDOC=space(254)
      .w_COPATHSJ=space(254)
      .w_PATHSJ=space(254)
      .w_PATHAZ=space(10)
      .w_CHKAZI=space(1)
      .w_GETPAXML=space(254)
      .w_GETPADOC=space(254)
      .w_LPATH=.f.
        .w_CODAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODAZI))
          .link_1_1('Full')
        endif
        .w_COCODAZI = i_CODAZI
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_COCODAZI))
          .link_1_2('Full')
        endif
          .DoRTCalc(3,4,.f.)
        .w_HIDE_CP = NOT(g_IZCP='S' OR g_IZCP='A') OR g_CPIN='S' OR g_DMIP='S'
        .w_PATHXML = .w_COPATHCP
        .w_ADDAZI = IIF(g_CPIN#'S' AND g_DMIP#'S', 'N', .w_CHKAZI)
          .DoRTCalc(8,8,.f.)
        .w_PATHDOC = .w_COPATHDO
          .DoRTCalc(10,10,.f.)
        .w_PATHSJ = .w_COPATHSJ
      .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
        .w_PATHAZ = addbs(alltrim(i_CODAZI))
    endwith
    this.DoRTCalc(13,16,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
          .link_1_2('Full')
        .DoRTCalc(3,10,.t.)
            .w_PATHSJ = .w_COPATHSJ
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
        if .o_ADDAZI<>.w_ADDAZI.or. .o_PATHDOC<>.w_PATHDOC
          .Calculate_PTZHYAXGVK()
        endif
        if .o_PATHXML<>.w_PATHXML
          .Calculate_EQBXMGENAW()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(12,16,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
    endwith
  return

  proc Calculate_EAFSKIMOWN()
    with this
          * --- getpathxml
          .w_GETPAXML = cp_getdir(IIF(EMPTY(.w_PATHXML),sys(5)+sys(2003),.w_PATHXML),"Percorso di destinazione")
          .w_PATHXML = IIF(Empty(.w_GETPAXML), .w_PATHXML, .w_GETPAXML)
    endwith
  endproc
  proc Calculate_LITESGEVHV()
    with this
          * --- getpathdoc
          .o_PATHXML = .w_PATHXML
          .w_GETPADOC = cp_getdir(IIF(EMPTY(.w_PATHDOC),sys(5)+sys(2003),.w_PATHDOC),"Percorso di destinazione")
          .w_PATHDOC = IIF(Empty(.w_GETPADOC), .w_PATHDOC, .w_GETPADOC)
    endwith
  endproc
  proc Calculate_PTZHYAXGVK()
    with this
          * --- Aggiornamento path webfolder
          .w_PATHDOC = IIF(right(alltrim(.w_PATHDOC),1)='\' or empty(.w_PATHDOC),.w_PATHDOC,alltrim(.w_PATHDOC)+iif(len(alltrim(.w_PATHDOC))=1,':\','\' ) )
          .w_PATHDOC = icase(.w_ADDAZI='S' and not(.w_PATHAZ$.w_PATHDOC and substr(.w_PATHDOC, rat(.w_PATHAZ,.w_PATHDOC))=.w_PATHAZ), addbs(.w_PATHDOC)+.w_PATHAZ, .w_ADDAZI<>'S', addbs(strtran(.w_PATHDOC,.w_PATHAZ,'')), .w_PATHDOC)
          .w_LPATH = chknfile(.w_PATHDOC,'F')
    endwith
  endproc
  proc Calculate_EQBXMGENAW()
    with this
          * --- Aggiornamento pathdoccp per standalone
          .w_PATHXML = IIF(right(alltrim(.w_PATHXML),1)='\' or empty(.w_PATHXML),.w_PATHXML,alltrim(.w_PATHXML)+iif(len(alltrim(.w_PATHXML))=1,':\','\' ) )
          .w_LPATH = chknfile(.w_PATHXML,'F')
          .w_PATHDOC = IIF(g_IZCP='A' And g_CPIN<>'S',.w_PATHXML,.w_PATHDOC)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPATHDOC_1_13.enabled = this.oPgFrm.Page1.oPag.oPATHDOC_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oPATHXML_1_7.visible=!this.oPgFrm.Page1.oPag.oPATHXML_1_7.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_9.visible=!this.oPgFrm.Page1.oPag.oBtn_1_9.mHide()
    this.oPgFrm.Page1.oPag.oADDAZI_1_11.visible=!this.oPgFrm.Page1.oPag.oADDAZI_1_11.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_22.visible=!this.oPgFrm.Page1.oPag.oStr_1_22.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("getpathxml")
          .Calculate_EAFSKIMOWN()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("getpathdoc")
          .Calculate_LITESGEVHV()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_20.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_21.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI,AZRAGAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_RAGAZI = NVL(_Link_.AZRAGAZI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_RAGAZI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COCODAZI
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_lTable = "CONTROPA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2], .t., this.CONTROPA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COCODAZI,COPATHCP,COPATHDO,COPATHSJ,COCHKAZI";
                   +" from "+i_cTable+" "+i_lTable+" where COCODAZI="+cp_ToStrODBC(this.w_COCODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COCODAZI',this.w_COCODAZI)
            select COCODAZI,COPATHCP,COPATHDO,COPATHSJ,COCHKAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCODAZI = NVL(_Link_.COCODAZI,space(5))
      this.w_COPATHCP = NVL(_Link_.COPATHCP,space(254))
      this.w_COPATHDO = NVL(_Link_.COPATHDO,space(254))
      this.w_COPATHSJ = NVL(_Link_.COPATHSJ,space(254))
      this.w_CHKAZI = NVL(_Link_.COCHKAZI,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_COCODAZI = space(5)
      endif
      this.w_COPATHCP = space(254)
      this.w_COPATHDO = space(254)
      this.w_COPATHSJ = space(254)
      this.w_CHKAZI = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])+'\'+cp_ToStr(_Link_.COCODAZI,1)
      cp_ShowWarn(i_cKey,this.CONTROPA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCOCODAZI_1_2.value==this.w_COCODAZI)
      this.oPgFrm.Page1.oPag.oCOCODAZI_1_2.value=this.w_COCODAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oRAGAZI_1_4.value==this.w_RAGAZI)
      this.oPgFrm.Page1.oPag.oRAGAZI_1_4.value=this.w_RAGAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oPATHXML_1_7.value==this.w_PATHXML)
      this.oPgFrm.Page1.oPag.oPATHXML_1_7.value=this.w_PATHXML
    endif
    if not(this.oPgFrm.Page1.oPag.oADDAZI_1_11.RadioValue()==this.w_ADDAZI)
      this.oPgFrm.Page1.oPag.oADDAZI_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPATHDOC_1_13.value==this.w_PATHDOC)
      this.oPgFrm.Page1.oPag.oPATHDOC_1_13.value=this.w_PATHDOC
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(right(alltrim(.w_PATHXML),1)='\')  and not(.w_HIDE_CP)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPATHXML_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il path deve terminare con \")
          case   not(right(alltrim(.w_PATHDOC),1)='\')  and (g_IZCP='S' OR g_IZCP='A' OR g_CPIN='S' OR g_DMIP='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPATHDOC_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il path deve terminare con \")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PATHXML = this.w_PATHXML
    this.o_ADDAZI = this.w_ADDAZI
    this.o_PATHDOC = this.w_PATHDOC
    return

enddefine

* --- Define pages as container
define class tgsar1kghPag1 as StdContainer
  Width  = 795
  height = 185
  stdWidth  = 795
  stdheight = 185
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCOCODAZI_1_2 as StdField with uid="RHYBSWWJDM",rtseq=2,rtrep=.f.,;
    cFormVar = "w_COCODAZI", cQueryName = "COCODAZI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice azienda",;
    HelpContextID = 209974161,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=156, Top=12, InputMask=replicate('X',5), cLinkFile="CONTROPA", cZoomOnZoom="GSAR_AZI", oKey_1_1="COCODAZI", oKey_1_2="this.w_COCODAZI"

  func oCOCODAZI_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oRAGAZI_1_4 as StdField with uid="YZRGUVGBLS",rtseq=3,rtrep=.f.,;
    cFormVar = "w_RAGAZI", cQueryName = "RAGAZI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Ragione sociale",;
    HelpContextID = 214843158,;
   bGlobalFont=.t.,;
    Height=21, Width=303, Left=218, Top=12, InputMask=replicate('X',40)

  add object oPATHXML_1_7 as StdField with uid="JBAXQEESWO",rtseq=6,rtrep=.f.,;
    cFormVar = "w_PATHXML", cQueryName = "PATHXML",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    sErrorMsg = "Il path deve terminare con \",;
    ToolTipText = "Path file xml per Corporate Portal",;
    HelpContextID = 11931382,;
   bGlobalFont=.t.,;
    Height=21, Width=612, Left=156, Top=58, InputMask=replicate('X',254)

  func oPATHXML_1_7.mHide()
    with this.Parent.oContained
      return (.w_HIDE_CP)
    endwith
  endfunc

  func oPATHXML_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (right(alltrim(.w_PATHXML),1)='\')
    endwith
    return bRes
  endfunc


  add object oBtn_1_9 as StdButton with uid="XPNLNZJTZZ",left=770, top=60, width=19,height=18,;
    caption="...", nPag=1;
    , HelpContextID = 34891306;
  , bGlobalFont=.t.

    proc oBtn_1_9.Click()
      with this.Parent.oContained
        .NotifyEvent("getpathxml")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_HIDE_CP)
     endwith
    endif
  endfunc

  add object oADDAZI_1_11 as StdCheck with uid="NYQLNOYCZT",rtseq=7,rtrep=.f.,left=156, top=88, caption="Cod. Azienda",;
    ToolTipText = "Se attivo, viene aggiunto il codice  azienda al percorso inserito per il webfolder",;
    HelpContextID = 214831366,;
    cFormVar="w_ADDAZI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oADDAZI_1_11.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oADDAZI_1_11.GetRadio()
    this.Parent.oContained.w_ADDAZI = this.RadioValue()
    return .t.
  endfunc

  func oADDAZI_1_11.SetRadio()
    this.Parent.oContained.w_ADDAZI=trim(this.Parent.oContained.w_ADDAZI)
    this.value = ;
      iif(this.Parent.oContained.w_ADDAZI=='S',1,;
      0)
  endfunc

  func oADDAZI_1_11.mHide()
    with this.Parent.oContained
      return (g_CPIN#'S' AND g_DMIP#'S')
    endwith
  endfunc

  add object oPATHDOC_1_13 as StdField with uid="SZJHNZJXNQ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_PATHDOC", cQueryName = "PATHDOC",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    sErrorMsg = "Il path deve terminare con \",;
    ToolTipText = "Path generazione documenti da inviare a webfolder di Corporate Portal/Infinity",;
    HelpContextID = 24514294,;
   bGlobalFont=.t.,;
    Height=21, Width=612, Left=156, Top=110, InputMask=replicate('X',254)

  func oPATHDOC_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_IZCP='S' OR g_IZCP='A' OR g_CPIN='S' OR g_DMIP='S')
    endwith
   endif
  endfunc

  func oPATHDOC_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (right(alltrim(.w_PATHDOC),1)='\')
    endwith
    return bRes
  endfunc


  add object oBtn_1_16 as StdButton with uid="NFXQPYAEYW",left=770, top=110, width=19,height=18,;
    caption="...", nPag=1;
    , HelpContextID = 34891306;
  , bGlobalFont=.t.

    proc oBtn_1_16.Click()
      with this.Parent.oContained
        .NotifyEvent("getpathdoc")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_16.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (g_IZCP='S' OR g_IZCP='A' OR g_CPIN='S' OR g_DMIP='S')
      endwith
    endif
  endfunc


  add object oBtn_1_18 as StdButton with uid="ZCKZJJFBTQ",left=692, top=136, width=48,height=45,;
    CpPicture="BMP\OK.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per salvare le impostazioni";
    , HelpContextID = 122184807;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      with this.Parent.oContained
        GSAR_BGH(this.Parent.oContained,"OK")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_19 as StdButton with uid="FNJHZHCDIW",left=742, top=136, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 27774906;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_20 as cp_runprogram with uid="FNZKOKZWMN",left=32, top=193, width=122,height=18,;
    caption='GSAR_BGH',;
   bGlobalFont=.t.,;
    prg="GSAR_BGH('INIT')",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 164695890


  add object oObj_1_21 as cp_runprogram with uid="WMRSQHQDIR",left=32, top=214, width=122,height=18,;
    caption='GSAR_BGH',;
   bGlobalFont=.t.,;
    prg="GSAR_BGH('OK')",;
    cEvent = "Salva",;
    nPag=1;
    , HelpContextID = 164695890

  add object oStr_1_3 as StdString with uid="SUFQJTKCEG",Visible=.t., Left=96, Top=12,;
    Alignment=1, Width=58, Height=15,;
    Caption="Azienda:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="WTCZYAVLRT",Visible=.t., Left=8, Top=58,;
    Alignment=1, Width=146, Height=18,;
    Caption="Path file xml:"  ;
  , bGlobalFont=.t.

  func oStr_1_22.mHide()
    with this.Parent.oContained
      return (.w_HIDE_CP)
    endwith
  endfunc

  add object oStr_1_23 as StdString with uid="MBBIUXDJNO",Visible=.t., Left=8, Top=110,;
    Alignment=1, Width=146, Height=18,;
    Caption="Path documenti:"  ;
  , bGlobalFont=.t.

  add object oBox_1_8 as StdBox with uid="JWLOWSSHOD",left=15, top=46, width=764,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar1kgh','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
