* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kzm                                                        *
*              Seleziona destinatari E-mail                                    *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_14]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-04-07                                                      *
* Last revis.: 2014-11-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_kzm",oParentObject))

* --- Class definition
define class tgsut_kzm as StdForm
  Top    = 5
  Left   = 4

  * --- Standard Properties
  Width  = 706
  Height = 383
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-11-20"
  HelpContextID=18267287
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=11

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsut_kzm"
  cComment = "Seleziona destinatari E-mail"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_SMTP__TO = space(0)
  w_SMTP__CC = space(0)
  w_SMTP_CCN = space(0)
  w_SELEZI = space(1)
  w_IsFax = .F.
  w_ISFAXFLT = space(1)
  w_FLTYPEDE = space(1)
  w_UseOutlook = .F.
  w_ARTABKEY = space(50)
  w_SMTP__TO = space(0)
  w_PEC = space(1)
  w_Zoom = .NULL.
  w_ZoomFax = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kzmPag1","gsut_kzm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSMTP__TO_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_Zoom = this.oPgFrm.Pages(1).oPag.Zoom
    this.w_ZoomFax = this.oPgFrm.Pages(1).oPag.ZoomFax
    DoDefault()
    proc Destroy()
      this.w_Zoom = .NULL.
      this.w_ZoomFax = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_SMTP__TO=space(0)
      .w_SMTP__CC=space(0)
      .w_SMTP_CCN=space(0)
      .w_SELEZI=space(1)
      .w_IsFax=.f.
      .w_ISFAXFLT=space(1)
      .w_FLTYPEDE=space(1)
      .w_UseOutlook=.f.
      .w_ARTABKEY=space(50)
      .w_SMTP__TO=space(0)
      .w_PEC=space(1)
      .w_SMTP__TO=oParentObject.w_SMTP__TO
      .w_SMTP__CC=oParentObject.w_SMTP__CC
      .w_SMTP_CCN=oParentObject.w_SMTP_CCN
      .w_IsFax=oParentObject.w_IsFax
      .w_UseOutlook=oParentObject.w_UseOutlook
      .w_ARTABKEY=oParentObject.w_ARTABKEY
      .w_SMTP__TO=oParentObject.w_SMTP__TO
      .w_PEC=oParentObject.w_PEC
      .oPgFrm.Page1.oPag.oObj_1_2.Calculate()
      .oPgFrm.Page1.oPag.Zoom.Calculate()
          .DoRTCalc(1,3,.f.)
        .w_SELEZI = 'D'
          .DoRTCalc(5,5,.f.)
        .w_ISFAXFLT = IIF(!.w_ISFAX, "N", "S")
        .w_FLTYPEDE = IIF(EMPTY(.w_ARTABKEY), 'G',  'P')
      .oPgFrm.Page1.oPag.ZoomFax.Calculate()
    endwith
    this.DoRTCalc(8,11,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_SMTP__TO=.w_SMTP__TO
      .oParentObject.w_SMTP__CC=.w_SMTP__CC
      .oParentObject.w_SMTP_CCN=.w_SMTP_CCN
      .oParentObject.w_IsFax=.w_IsFax
      .oParentObject.w_UseOutlook=.w_UseOutlook
      .oParentObject.w_ARTABKEY=.w_ARTABKEY
      .oParentObject.w_SMTP__TO=.w_SMTP__TO
      .oParentObject.w_PEC=.w_PEC
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_2.Calculate()
        .oPgFrm.Page1.oPag.Zoom.Calculate()
        .DoRTCalc(1,5,.t.)
            .w_ISFAXFLT = IIF(!.w_ISFAX, "N", "S")
        .oPgFrm.Page1.oPag.ZoomFax.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(7,11,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_2.Calculate()
        .oPgFrm.Page1.oPag.Zoom.Calculate()
        .oPgFrm.Page1.oPag.ZoomFax.Calculate()
    endwith
  return

  proc Calculate_SATNIHJYAT()
    with this
          * --- Modifico query zoom e Caption
          .cComment = IIF(.w_IsFax, AH_MSGFORMAT("Seleziona destinatari fax"),IIF(i_bPEC,AH_MSGFORMAT("Seleziona destinatari e-mail PEC"),AH_MSGFORMAT("Seleziona destinatari e-mail")))
          .Caption = .cComment
          .w_ZOOM.ccpqueryname = IIF(.w_UseOutlook AND .w_FLTYPEDE<>'G', ICASE(.w_FLTYPEDE='P', "QUERY\GSUTPKIM",.w_FLTYPEDE='O', "QUERY\GSUTOKIM", IIF(g_OFFE='S' OR g_AGEN='S',"QUERY\GSUTTKIM1", "QUERY\GSUTTKIM") )  , IIF(g_OFFE='S' OR g_AGEN='S', "QUERY\GSUT_KIM2", "QUERY\GSUT_KIM") )
          .w_Zoom.visible = iif(.w_IsFax,.F.,.T.)
          .w_ZoomFax.visible = iif(!.w_IsFax,.F.,.T.)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oSMTP_CCN_1_8.enabled = this.oPgFrm.Page1.oPag.oSMTP_CCN_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oSMTP__TO_1_1.visible=!this.oPgFrm.Page1.oPag.oSMTP__TO_1_1.mHide()
    this.oPgFrm.Page1.oPag.oSMTP__CC_1_7.visible=!this.oPgFrm.Page1.oPag.oSMTP__CC_1_7.mHide()
    this.oPgFrm.Page1.oPag.oSMTP_CCN_1_8.visible=!this.oPgFrm.Page1.oPag.oSMTP_CCN_1_8.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_9.visible=!this.oPgFrm.Page1.oPag.oBtn_1_9.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_10.visible=!this.oPgFrm.Page1.oPag.oBtn_1_10.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_11.visible=!this.oPgFrm.Page1.oPag.oBtn_1_11.mHide()
    this.oPgFrm.Page1.oPag.oSELEZI_1_12.visible=!this.oPgFrm.Page1.oPag.oSELEZI_1_12.mHide()
    this.oPgFrm.Page1.oPag.oFLTYPEDE_1_15.visible=!this.oPgFrm.Page1.oPag.oFLTYPEDE_1_15.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_17.visible=!this.oPgFrm.Page1.oPag.oStr_1_17.mHide()
    this.oPgFrm.Page1.oPag.oSMTP__TO_1_20.visible=!this.oPgFrm.Page1.oPag.oSMTP__TO_1_20.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_21.visible=!this.oPgFrm.Page1.oPag.oBtn_1_21.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_2.Event(cEvent)
        if lower(cEvent)==lower("Init") or lower(cEvent)==lower("w_FLTYPEDE Changed")
          .Calculate_SATNIHJYAT()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.Zoom.Event(cEvent)
      .oPgFrm.Page1.oPag.ZoomFax.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsut_kzm
    If Cevent='w_FLTYPEDE Changed'
       This.Notifyevent('Elabora')
    Endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSMTP__TO_1_1.value==this.w_SMTP__TO)
      this.oPgFrm.Page1.oPag.oSMTP__TO_1_1.value=this.w_SMTP__TO
    endif
    if not(this.oPgFrm.Page1.oPag.oSMTP__CC_1_7.value==this.w_SMTP__CC)
      this.oPgFrm.Page1.oPag.oSMTP__CC_1_7.value=this.w_SMTP__CC
    endif
    if not(this.oPgFrm.Page1.oPag.oSMTP_CCN_1_8.value==this.w_SMTP_CCN)
      this.oPgFrm.Page1.oPag.oSMTP_CCN_1_8.value=this.w_SMTP_CCN
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_12.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLTYPEDE_1_15.RadioValue()==this.w_FLTYPEDE)
      this.oPgFrm.Page1.oPag.oFLTYPEDE_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSMTP__TO_1_20.value==this.w_SMTP__TO)
      this.oPgFrm.Page1.oPag.oSMTP__TO_1_20.value=this.w_SMTP__TO
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsut_kzmPag1 as StdContainer
  Width  = 702
  height = 383
  stdWidth  = 702
  stdheight = 383
  resizeXpos=438
  resizeYpos=211
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSMTP__TO_1_1 as StdMemo with uid="AZCXSPOHTN",rtseq=1,rtrep=.f.,;
    cFormVar = "w_SMTP__TO", cQueryName = "SMTP__TO",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 161722507,;
   bGlobalFont=.t.,;
    Height=24, Width=641, Left=55, Top=254

  func oSMTP__TO_1_1.mHide()
    with this.Parent.oContained
      return (.w_IsFax)
    endwith
  endfunc


  add object oObj_1_2 as cp_runprogram with uid="BNLBWFKVTM",left=11, top=403, width=238,height=18,;
    caption='GSUT_BIM',;
   bGlobalFont=.t.,;
    prg="GSUT_BIM('SEL')",;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 157312179


  add object Zoom as cp_szoombox with uid="KZYPGAMEOL",left=4, top=2, width=705,height=246,;
    caption='Object',;
   bGlobalFont=.t.,;
    bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cTable="CONTI",cZoomFile="GSUT_KIM",bOptions=.t.,cMenuFile="",cZoomOnZoom="GSAR_BZC",bNoZoomGridShape=.f.,bQueryOnDblClick=.t.,;
    cEvent = "Init,Elabora",;
    nPag=1;
    , HelpContextID = 72170522


  add object oBtn_1_5 as StdButton with uid="ZMIZZNTUDJ",left=595, top=334, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 18296038;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_6 as StdButton with uid="JWIBHYOYEX",left=649, top=334, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 25584710;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSMTP__CC_1_7 as StdMemo with uid="FLMQVRKHQO",rtseq=2,rtrep=.f.,;
    cFormVar = "w_SMTP__CC", cQueryName = "SMTP__CC",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 161722519,;
   bGlobalFont=.t.,;
    Height=24, Width=641, Left=55, Top=279

  func oSMTP__CC_1_7.mHide()
    with this.Parent.oContained
      return (.w_IsFax)
    endwith
  endfunc

  add object oSMTP_CCN_1_8 as StdMemo with uid="IGKOJAKQGT",rtseq=3,rtrep=.f.,;
    cFormVar = "w_SMTP_CCN", cQueryName = "SMTP_CCN",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 94613644,;
   bGlobalFont=.t.,;
    Height=24, Width=641, Left=55, Top=306

  func oSMTP_CCN_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not i_bPEC)
    endwith
   endif
  endfunc

  func oSMTP_CCN_1_8.mHide()
    with this.Parent.oContained
      return (.w_IsFax)
    endwith
  endfunc


  add object oBtn_1_9 as StdButton with uid="XFZTSGQDIZ",left=4, top=254, width=50,height=23,;
    caption="A", nPag=1;
    , ToolTipText = "Premere per inserire nella casella gli indirizzi E-mail selezionati";
    , HelpContextID = 18268422;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      with this.Parent.oContained
        GSUT_BIM(this.Parent.oContained,"TO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_IsFax)
     endwith
    endif
  endfunc


  add object oBtn_1_10 as StdButton with uid="TLJNPGAQLX",left=4, top=279, width=50,height=23,;
    caption="CC", nPag=1;
    , ToolTipText = "Premere per inserire nella casella gli indirizzi E-mail selezionati";
    , HelpContextID = 18285606;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      with this.Parent.oContained
        GSUT_BIM(this.Parent.oContained,"CC")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_10.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_IsFax)
     endwith
    endif
  endfunc


  add object oBtn_1_11 as StdButton with uid="FYRMYUNWVV",left=4, top=306, width=50,height=23,;
    caption="Ccn", nPag=1;
    , ToolTipText = "Premere per inserire nella casella gli indirizzi E-mail selezionati";
    , HelpContextID = 18744358;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      with this.Parent.oContained
        GSUT_BIM(this.Parent.oContained,"CN")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_11.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not i_bPEC)
      endwith
    endif
  endfunc

  func oBtn_1_11.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_IsFax)
     endwith
    endif
  endfunc

  add object oSELEZI_1_12 as StdRadio with uid="VNICEXLLTR",rtseq=4,rtrep=.f.,left=4, top=335, width=139,height=32;
    , tabstop=.f.;
    , ToolTipText = "Seleziona/deseleziona le righe del documento";
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_12.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 268384474
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 268384474
      this.Buttons(2).Top=15
      this.SetAll("Width",137)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona/deseleziona le righe del documento")
      StdRadio::init()
    endproc

  func oSELEZI_1_12.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_12.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_12.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc

  func oSELEZI_1_12.mHide()
    with this.Parent.oContained
      return (!isAlt() Or .w_IsFax)
    endwith
  endfunc


  add object oFLTYPEDE_1_15 as StdCombo with uid="IXKVSWUGFB",rtseq=7,rtrep=.f.,left=339,top=334,width=231,height=21;
    , HelpContextID = 76198501;
    , cFormVar="w_FLTYPEDE",RowSource=""+"della pratica,"+"di Outlook,"+"del gestionale,"+"del gestionale e di Outlook", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLTYPEDE_1_15.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'O',;
    iif(this.value =3,'G',;
    iif(this.value =4,'T',;
    space(1))))))
  endfunc
  func oFLTYPEDE_1_15.GetRadio()
    this.Parent.oContained.w_FLTYPEDE = this.RadioValue()
    return .t.
  endfunc

  func oFLTYPEDE_1_15.SetRadio()
    this.Parent.oContained.w_FLTYPEDE=trim(this.Parent.oContained.w_FLTYPEDE)
    this.value = ;
      iif(this.Parent.oContained.w_FLTYPEDE=='P',1,;
      iif(this.Parent.oContained.w_FLTYPEDE=='O',2,;
      iif(this.Parent.oContained.w_FLTYPEDE=='G',3,;
      iif(this.Parent.oContained.w_FLTYPEDE=='T',4,;
      0))))
  endfunc

  func oFLTYPEDE_1_15.mHide()
    with this.Parent.oContained
      return (!.w_UseOutlook or EMPTY(.w_ARTABKEY) or !isAlt() Or .w_IsFax)
    endwith
  endfunc


  add object ZoomFax as cp_szoombox with uid="ABPNQRSEVV",left=4, top=2, width=705,height=282,;
    caption='Object',;
   bGlobalFont=.t.,;
    bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cTable="CONTI",cZoomFile="GSUT1KIM",bOptions=.f.,cMenuFile="",cZoomOnZoom="",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 72170522

  add object oSMTP__TO_1_20 as StdMemo with uid="FAVNTMZAGW",rtseq=10,rtrep=.f.,;
    cFormVar = "w_SMTP__TO", cQueryName = "SMTP__TO",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 161722507,;
   bGlobalFont=.t.,;
    Height=24, Width=641, Left=55, Top=306

  func oSMTP__TO_1_20.mHide()
    with this.Parent.oContained
      return (!.w_IsFax)
    endwith
  endfunc


  add object oBtn_1_21 as StdButton with uid="QXGFVCEBOS",left=4, top=306, width=50,height=23,;
    caption="A", nPag=1;
    , ToolTipText = "Premere per inserire nella casella gli indirizzi E-mail selezionati";
    , HelpContextID = 18268422;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      with this.Parent.oContained
        GSUT_BIM(this.Parent.oContained,"TO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_21.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (!.w_IsFax)
     endwith
    endif
  endfunc

  add object oStr_1_17 as StdString with uid="GNTICLEFMD",Visible=.t., Left=147, Top=334,;
    Alignment=1, Width=186, Height=18,;
    Caption="Mostra destinatari:"  ;
  , bGlobalFont=.t.

  func oStr_1_17.mHide()
    with this.Parent.oContained
      return (!.w_UseOutlook or EMPTY(.w_ARTABKEY) or !IsAlt() Or .w_IsFax)
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kzm','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
