* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_sls                                                        *
*              Visualizza livello di servizio                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-14                                                      *
* Last revis.: 2011-09-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_sls",oParentObject))

* --- Class definition
define class tgsma_sls as StdForm
  Top    = 0
  Left   = 80

  * --- Standard Properties
  Width  = 558
  Height = 458+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-09-23"
  HelpContextID=208305001
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=20

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  ART_ICOL_IDX = 0
  FAM_ARTI_IDX = 0
  VALUTE_IDX = 0
  CATECOMM_IDX = 0
  CACOCLFO_IDX = 0
  cPrg = "gsma_sls"
  cComment = "Visualizza livello di servizio"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TIPOANA = space(1)
  w_TIPOANA = space(1)
  w_BASEANA = space(1)
  w_TIPOCONT = space(1)
  w_DATINI = ctod('  /  /  ')
  o_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_CLIEFOR = space(15)
  w_CLIEFOR1 = space(15)
  w_CATCOM = space(3)
  w_CATCOM1 = space(3)
  w_CATCON = space(5)
  w_CATCON1 = space(5)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_DESCCL = space(40)
  w_DESCCL1 = space(40)
  w_DESCAT = space(35)
  w_DESCAT1 = space(35)
  w_DESCON = space(35)
  w_DESCON1 = space(35)
  w_ZOOMLIV = .NULL.
  w_FORMULA = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_slsPag1","gsma_sls",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(2).addobject("oPag","tgsma_slsPag2","gsma_sls",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Visualizzazione")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPOANA_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOMLIV = this.oPgFrm.Pages(2).oPag.ZOOMLIV
    this.w_FORMULA = this.oPgFrm.Pages(1).oPag.FORMULA
    DoDefault()
    proc Destroy()
      this.w_ZOOMLIV = .NULL.
      this.w_FORMULA = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='FAM_ARTI'
    this.cWorkTables[4]='VALUTE'
    this.cWorkTables[5]='CATECOMM'
    this.cWorkTables[6]='CACOCLFO'
    return(this.OpenAllTables(6))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPOANA=space(1)
      .w_TIPOANA=space(1)
      .w_BASEANA=space(1)
      .w_TIPOCONT=space(1)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_CLIEFOR=space(15)
      .w_CLIEFOR1=space(15)
      .w_CATCOM=space(3)
      .w_CATCOM1=space(3)
      .w_CATCON=space(5)
      .w_CATCON1=space(5)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_DESCCL=space(40)
      .w_DESCCL1=space(40)
      .w_DESCAT=space(35)
      .w_DESCAT1=space(35)
      .w_DESCON=space(35)
      .w_DESCON1=space(35)
        .w_TIPOANA = 'R'
        .w_TIPOANA = 'R'
        .w_BASEANA = 'P'
        .w_TIPOCONT = 'C'
        .w_DATINI = g_iniese
        .w_DATFIN = i_datsys
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_CLIEFOR))
          .link_1_7('Full')
        endif
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_CLIEFOR1))
          .link_1_8('Full')
        endif
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_CATCOM))
          .link_1_9('Full')
        endif
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_CATCOM1))
          .link_1_10('Full')
        endif
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_CATCON))
          .link_1_11('Full')
        endif
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_CATCON1))
          .link_1_12('Full')
        endif
        .w_OBTEST = .w_DATINI
      .oPgFrm.Page2.oPag.ZOOMLIV.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
      .oPgFrm.Page1.oPag.FORMULA.Calculate(IIF(.w_TIPOANA='P', "bmp\PercIne.bmp", IIF(.w_TIPOANA='R', IIF(.w_BASEANA='P', "bmp\RitMed.bmp" , "bmp\RitMedR.bmp" ), "bmp\ScoMed.bmp")))
    endwith
    this.DoRTCalc(14,20,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_2.enabled = this.oPgFrm.Page2.oPag.oBtn_2_2.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_3.enabled = this.oPgFrm.Page2.oPag.oBtn_2_3.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,12,.t.)
        if .o_DATINI<>.w_DATINI
            .w_OBTEST = .w_DATINI
        endif
        .oPgFrm.Page2.oPag.ZOOMLIV.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
        .oPgFrm.Page1.oPag.FORMULA.Calculate(IIF(.w_TIPOANA='P', "bmp\PercIne.bmp", IIF(.w_TIPOANA='R', IIF(.w_BASEANA='P', "bmp\RitMed.bmp" , "bmp\RitMedR.bmp" ), "bmp\ScoMed.bmp")))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(14,20,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page2.oPag.ZOOMLIV.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
        .oPgFrm.Page1.oPag.FORMULA.Calculate(IIF(.w_TIPOANA='P', "bmp\PercIne.bmp", IIF(.w_TIPOANA='R', IIF(.w_BASEANA='P', "bmp\RitMed.bmp" , "bmp\RitMedR.bmp" ), "bmp\ScoMed.bmp")))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oTIPOANA_1_1.visible=!this.oPgFrm.Page1.oPag.oTIPOANA_1_1.mHide()
    this.oPgFrm.Page1.oPag.oTIPOANA_1_2.visible=!this.oPgFrm.Page1.oPag.oTIPOANA_1_2.mHide()
    this.oPgFrm.Page1.oPag.oBASEANA_1_3.visible=!this.oPgFrm.Page1.oPag.oBASEANA_1_3.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_35.visible=!this.oPgFrm.Page1.oPag.oStr_1_35.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page2.oPag.ZOOMLIV.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_32.Event(cEvent)
      .oPgFrm.Page1.oPag.FORMULA.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CLIEFOR
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLIEFOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CLIEFOR)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPOCONT);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPOCONT;
                     ,'ANCODICE',trim(this.w_CLIEFOR))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLIEFOR)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CLIEFOR) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCLIEFOR_1_7'),i_cWhere,'',"Elenco intestatari",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPOCONT<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice intestatario inesistente, obsoleto, oppure maggiore del codice finale")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPOCONT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLIEFOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CLIEFOR);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPOCONT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPOCONT;
                       ,'ANCODICE',this.w_CLIEFOR)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLIEFOR = NVL(_Link_.ANCODICE,space(15))
      this.W_DESCCL = NVL(_Link_.ANDESCRI,space(40))
      this.W_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CLIEFOR = space(15)
      endif
      this.W_DESCCL = space(40)
      this.W_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND (.w_CLIEFOR <= .w_CLIEFOR1 OR EMPTY(.w_CLIEFOR1))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice intestatario inesistente, obsoleto, oppure maggiore del codice finale")
        endif
        this.w_CLIEFOR = space(15)
        this.W_DESCCL = space(40)
        this.W_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLIEFOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLIEFOR1
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLIEFOR1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CLIEFOR1)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPOCONT);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPOCONT;
                     ,'ANCODICE',trim(this.w_CLIEFOR1))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLIEFOR1)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CLIEFOR1) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCLIEFOR1_1_8'),i_cWhere,'',"Elenco intestatari",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPOCONT<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice intestatario inesistente, obsoleto, oppure minore del codice iniziale")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPOCONT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLIEFOR1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CLIEFOR1);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPOCONT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPOCONT;
                       ,'ANCODICE',this.w_CLIEFOR1)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLIEFOR1 = NVL(_Link_.ANCODICE,space(15))
      this.W_DESCCL1 = NVL(_Link_.ANDESCRI,space(40))
      this.W_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CLIEFOR1 = space(15)
      endif
      this.W_DESCCL1 = space(40)
      this.W_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND (.w_CLIEFOR <= .w_CLIEFOR1)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice intestatario inesistente, obsoleto, oppure minore del codice iniziale")
        endif
        this.w_CLIEFOR1 = space(15)
        this.W_DESCCL1 = space(40)
        this.W_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLIEFOR1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATCOM
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATECOMM_IDX,3]
    i_lTable = "CATECOMM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2], .t., this.CATECOMM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACT',True,'CATECOMM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_CATCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_CATCOM))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATCOM)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATCOM) and !this.bDontReportError
            deferred_cp_zoom('CATECOMM','*','CTCODICE',cp_AbsName(oSource.parent,'oCATCOM_1_9'),i_cWhere,'GSAR_ACT',"Categorie commerciali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_CATCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_CATCOM)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATCOM = NVL(_Link_.CTCODICE,space(3))
      this.w_DESCAT = NVL(_Link_.CTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATCOM = space(3)
      endif
      this.w_DESCAT = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATCOM <= .w_CATCOM1 OR EMPTY(.w_CATCOM1)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Categorie commerciali incongruenti")
        endif
        this.w_CATCOM = space(3)
        this.w_DESCAT = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CATECOMM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATCOM1
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATECOMM_IDX,3]
    i_lTable = "CATECOMM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2], .t., this.CATECOMM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATCOM1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACT',True,'CATECOMM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_CATCOM1)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_CATCOM1))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATCOM1)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATCOM1) and !this.bDontReportError
            deferred_cp_zoom('CATECOMM','*','CTCODICE',cp_AbsName(oSource.parent,'oCATCOM1_1_10'),i_cWhere,'GSAR_ACT',"Categorie commerciali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATCOM1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_CATCOM1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_CATCOM1)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATCOM1 = NVL(_Link_.CTCODICE,space(3))
      this.w_DESCAT1 = NVL(_Link_.CTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATCOM1 = space(3)
      endif
      this.w_DESCAT1 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATCOM <= .w_CATCOM1
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Categorie commerciali incongruenti")
        endif
        this.w_CATCOM1 = space(3)
        this.w_DESCAT1 = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CATECOMM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATCOM1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATCON
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CACOCLFO_IDX,3]
    i_lTable = "CACOCLFO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2], .t., this.CACOCLFO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CACOCLFO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" C2CODICE like "+cp_ToStrODBC(trim(this.w_CATCON)+"%");

          i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by C2CODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'C2CODICE',trim(this.w_CATCON))
          select C2CODICE,C2DESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by C2CODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATCON)==trim(_Link_.C2CODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATCON) and !this.bDontReportError
            deferred_cp_zoom('CACOCLFO','*','C2CODICE',cp_AbsName(oSource.parent,'oCATCON_1_11'),i_cWhere,'',"Categorie contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',oSource.xKey(1))
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(this.w_CATCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',this.w_CATCON)
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATCON = NVL(_Link_.C2CODICE,space(5))
      this.w_DESCON = NVL(_Link_.C2DESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATCON = space(5)
      endif
      this.w_DESCON = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATCON <= .w_CATCON1 OR EMPTY(.w_CATCON1)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Categorie contabili incongruenti")
        endif
        this.w_CATCON = space(5)
        this.w_DESCON = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])+'\'+cp_ToStr(_Link_.C2CODICE,1)
      cp_ShowWarn(i_cKey,this.CACOCLFO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATCON1
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CACOCLFO_IDX,3]
    i_lTable = "CACOCLFO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2], .t., this.CACOCLFO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATCON1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CACOCLFO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" C2CODICE like "+cp_ToStrODBC(trim(this.w_CATCON1)+"%");

          i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by C2CODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'C2CODICE',trim(this.w_CATCON1))
          select C2CODICE,C2DESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by C2CODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATCON1)==trim(_Link_.C2CODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATCON1) and !this.bDontReportError
            deferred_cp_zoom('CACOCLFO','*','C2CODICE',cp_AbsName(oSource.parent,'oCATCON1_1_12'),i_cWhere,'',"Categorie contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',oSource.xKey(1))
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATCON1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(this.w_CATCON1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',this.w_CATCON1)
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATCON1 = NVL(_Link_.C2CODICE,space(5))
      this.w_DESCON1 = NVL(_Link_.C2DESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATCON1 = space(5)
      endif
      this.w_DESCON1 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATCON <= .w_CATCON1
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Categorie contabili incongruenti")
        endif
        this.w_CATCON1 = space(5)
        this.w_DESCON1 = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])+'\'+cp_ToStr(_Link_.C2CODICE,1)
      cp_ShowWarn(i_cKey,this.CACOCLFO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATCON1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPOANA_1_1.RadioValue()==this.w_TIPOANA)
      this.oPgFrm.Page1.oPag.oTIPOANA_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOANA_1_2.RadioValue()==this.w_TIPOANA)
      this.oPgFrm.Page1.oPag.oTIPOANA_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oBASEANA_1_3.RadioValue()==this.w_BASEANA)
      this.oPgFrm.Page1.oPag.oBASEANA_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOCONT_1_4.RadioValue()==this.w_TIPOCONT)
      this.oPgFrm.Page1.oPag.oTIPOCONT_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_5.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_5.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_6.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_6.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCLIEFOR_1_7.value==this.w_CLIEFOR)
      this.oPgFrm.Page1.oPag.oCLIEFOR_1_7.value=this.w_CLIEFOR
    endif
    if not(this.oPgFrm.Page1.oPag.oCLIEFOR1_1_8.value==this.w_CLIEFOR1)
      this.oPgFrm.Page1.oPag.oCLIEFOR1_1_8.value=this.w_CLIEFOR1
    endif
    if not(this.oPgFrm.Page1.oPag.oCATCOM_1_9.value==this.w_CATCOM)
      this.oPgFrm.Page1.oPag.oCATCOM_1_9.value=this.w_CATCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oCATCOM1_1_10.value==this.w_CATCOM1)
      this.oPgFrm.Page1.oPag.oCATCOM1_1_10.value=this.w_CATCOM1
    endif
    if not(this.oPgFrm.Page1.oPag.oCATCON_1_11.value==this.w_CATCON)
      this.oPgFrm.Page1.oPag.oCATCON_1_11.value=this.w_CATCON
    endif
    if not(this.oPgFrm.Page1.oPag.oCATCON1_1_12.value==this.w_CATCON1)
      this.oPgFrm.Page1.oPag.oCATCON1_1_12.value=this.w_CATCON1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCCL_1_17.value==this.w_DESCCL)
      this.oPgFrm.Page1.oPag.oDESCCL_1_17.value=this.w_DESCCL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCCL1_1_21.value==this.w_DESCCL1)
      this.oPgFrm.Page1.oPag.oDESCCL1_1_21.value=this.w_DESCCL1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAT_1_25.value==this.w_DESCAT)
      this.oPgFrm.Page1.oPag.oDESCAT_1_25.value=this.w_DESCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAT1_1_26.value==this.w_DESCAT1)
      this.oPgFrm.Page1.oPag.oDESCAT1_1_26.value=this.w_DESCAT1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_28.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_28.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON1_1_30.value==this.w_DESCON1)
      this.oPgFrm.Page1.oPag.oDESCON1_1_30.value=this.w_DESCON1
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_DATINI)) or not(.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_5.SetFocus()
            i_bnoObbl = !empty(.w_DATINI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_DATFIN)) or not(.w_DATINI<=.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_6.SetFocus()
            i_bnoObbl = !empty(.w_DATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND (.w_CLIEFOR <= .w_CLIEFOR1 OR EMPTY(.w_CLIEFOR1)))  and not(empty(.w_CLIEFOR))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCLIEFOR_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice intestatario inesistente, obsoleto, oppure maggiore del codice finale")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND (.w_CLIEFOR <= .w_CLIEFOR1))  and not(empty(.w_CLIEFOR1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCLIEFOR1_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice intestatario inesistente, obsoleto, oppure minore del codice iniziale")
          case   not(.w_CATCOM <= .w_CATCOM1 OR EMPTY(.w_CATCOM1))  and not(empty(.w_CATCOM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCATCOM_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Categorie commerciali incongruenti")
          case   not(.w_CATCOM <= .w_CATCOM1)  and not(empty(.w_CATCOM1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCATCOM1_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Categorie commerciali incongruenti")
          case   not(.w_CATCON <= .w_CATCON1 OR EMPTY(.w_CATCON1))  and not(empty(.w_CATCON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCATCON_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Categorie contabili incongruenti")
          case   not(.w_CATCON <= .w_CATCON1)  and not(empty(.w_CATCON1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCATCON1_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Categorie contabili incongruenti")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DATINI = this.w_DATINI
    return

enddefine

* --- Define pages as container
define class tgsma_slsPag1 as StdContainer
  Width  = 554
  height = 458
  stdWidth  = 554
  stdheight = 458
  resizeXpos=301
  resizeYpos=341
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPOANA_1_1 as StdCombo with uid="OXKENDJJRP",rtseq=1,rtrep=.f.,left=105,top=13,width=189,height=21;
    , ToolTipText = "Selezionare il tipo di analisi";
    , HelpContextID = 100258614;
    , cFormVar="w_TIPOANA",RowSource=""+"Ritardo medio di consegna,"+"Percentuale di evasione,"+"Scostamento medio contrattato", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOANA_1_1.RadioValue()
    return(iif(this.value =1,'R',;
    iif(this.value =2,'P',;
    iif(this.value =3,'S',;
    space(1)))))
  endfunc
  func oTIPOANA_1_1.GetRadio()
    this.Parent.oContained.w_TIPOANA = this.RadioValue()
    return .t.
  endfunc

  func oTIPOANA_1_1.SetRadio()
    this.Parent.oContained.w_TIPOANA=trim(this.Parent.oContained.w_TIPOANA)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOANA=='R',1,;
      iif(this.Parent.oContained.w_TIPOANA=='P',2,;
      iif(this.Parent.oContained.w_TIPOANA=='S',3,;
      0)))
  endfunc

  func oTIPOANA_1_1.mHide()
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)<>'AD HOC ENTERPRISE')
    endwith
  endfunc


  add object oTIPOANA_1_2 as StdCombo with uid="KJURDKYYTT",rtseq=2,rtrep=.f.,left=105,top=13,width=189,height=21;
    , ToolTipText = "Selezionare il tipo di analisi";
    , HelpContextID = 100258614;
    , cFormVar="w_TIPOANA",RowSource=""+"Ritardo medio di consegna,"+"Percentuale di inevaso", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOANA_1_2.RadioValue()
    return(iif(this.value =1,'R',;
    iif(this.value =2,'P',;
    space(1))))
  endfunc
  func oTIPOANA_1_2.GetRadio()
    this.Parent.oContained.w_TIPOANA = this.RadioValue()
    return .t.
  endfunc

  func oTIPOANA_1_2.SetRadio()
    this.Parent.oContained.w_TIPOANA=trim(this.Parent.oContained.w_TIPOANA)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOANA=='R',1,;
      iif(this.Parent.oContained.w_TIPOANA=='P',2,;
      0))
  endfunc

  func oTIPOANA_1_2.mHide()
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)='AD HOC ENTERPRISE')
    endwith
  endfunc


  add object oBASEANA_1_3 as StdCombo with uid="XFYHQQGASP",rtseq=3,rtrep=.f.,left=406,top=13,width=137,height=21;
    , ToolTipText = "Data su cui effettuare l'analisi";
    , HelpContextID = 99613206;
    , cFormVar="w_BASEANA",RowSource=""+"Data prev. evasione,"+"Data rich. evasione", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oBASEANA_1_3.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'R',;
    space(1))))
  endfunc
  func oBASEANA_1_3.GetRadio()
    this.Parent.oContained.w_BASEANA = this.RadioValue()
    return .t.
  endfunc

  func oBASEANA_1_3.SetRadio()
    this.Parent.oContained.w_BASEANA=trim(this.Parent.oContained.w_BASEANA)
    this.value = ;
      iif(this.Parent.oContained.w_BASEANA=='P',1,;
      iif(this.Parent.oContained.w_BASEANA=='R',2,;
      0))
  endfunc

  func oBASEANA_1_3.mHide()
    with this.Parent.oContained
      return (.w_TIPOANA<>'R' OR UPPER(g_APPLICATION)='ADHOC REVOLUTION')
    endwith
  endfunc


  add object oTIPOCONT_1_4 as StdCombo with uid="HPTNLESAYI",rtseq=4,rtrep=.f.,left=105,top=39,width=109,height=21;
    , ToolTipText = "Selezionare l'intestatario dell'analisi: per cliente o fornitore";
    , HelpContextID = 149302390;
    , cFormVar="w_TIPOCONT",RowSource=""+"Cliente,"+"Fornitore", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOCONT_1_4.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oTIPOCONT_1_4.GetRadio()
    this.Parent.oContained.w_TIPOCONT = this.RadioValue()
    return .t.
  endfunc

  func oTIPOCONT_1_4.SetRadio()
    this.Parent.oContained.w_TIPOCONT=trim(this.Parent.oContained.w_TIPOCONT)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOCONT=='C',1,;
      iif(this.Parent.oContained.w_TIPOCONT=='F',2,;
      0))
  endfunc

  func oTIPOCONT_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CLIEFOR)
        bRes2=.link_1_7('Full')
      endif
      if .not. empty(.w_CLIEFOR1)
        bRes2=.link_1_8('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oDATINI_1_5 as StdField with uid="GHCLLETEOX",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio selezione - filtra sulla data di prevista evasione dell'ordine",;
    HelpContextID = 238810570,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=318, Top=39

  func oDATINI_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_6 as StdField with uid="BBPCOCBGFG",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di fine selezione - filtra sulla data data di prevista evasione dell'ordine",;
    HelpContextID = 160363978,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=466, Top=39

  func oDATFIN_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN)
    endwith
    return bRes
  endfunc

  add object oCLIEFOR_1_7 as StdField with uid="NOVJLIUORK",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CLIEFOR", cQueryName = "CLIEFOR",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice intestatario inesistente, obsoleto, oppure maggiore del codice finale",;
    ToolTipText = "Codice intestatario da considerare",;
    HelpContextID = 121595174,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=105, Top=71, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPOCONT", oKey_2_1="ANCODICE", oKey_2_2="this.w_CLIEFOR"

  func oCLIEFOR_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLIEFOR_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLIEFOR_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPOCONT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPOCONT)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCLIEFOR_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco intestatari",'',this.parent.oContained
  endproc

  add object oCLIEFOR1_1_8 as StdField with uid="TXFPTOYJYK",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CLIEFOR1", cQueryName = "CLIEFOR1",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice intestatario inesistente, obsoleto, oppure minore del codice iniziale",;
    ToolTipText = "Codice intestatario da considerare",;
    HelpContextID = 121595223,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=105, Top=95, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPOCONT", oKey_2_1="ANCODICE", oKey_2_2="this.w_CLIEFOR1"

  func oCLIEFOR1_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLIEFOR1_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLIEFOR1_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPOCONT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPOCONT)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCLIEFOR1_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco intestatari",'',this.parent.oContained
  endproc

  add object oCATCOM_1_9 as StdField with uid="KOQVTBKGOL",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CATCOM", cQueryName = "CATCOM",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Categorie commerciali incongruenti",;
    ToolTipText = "Categoria commerciale di inizio selezione",;
    HelpContextID = 171046362,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=105, Top=125, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CATECOMM", cZoomOnZoom="GSAR_ACT", oKey_1_1="CTCODICE", oKey_1_2="this.w_CATCOM"

  func oCATCOM_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATCOM_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATCOM_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATECOMM','*','CTCODICE',cp_AbsName(this.parent,'oCATCOM_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACT',"Categorie commerciali",'',this.parent.oContained
  endproc
  proc oCATCOM_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_CATCOM
     i_obj.ecpSave()
  endproc

  add object oCATCOM1_1_10 as StdField with uid="JNDBABXDKJ",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CATCOM1", cQueryName = "CATCOM1",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Categorie commerciali incongruenti",;
    ToolTipText = "Categoria commerciale di fine selezione",;
    HelpContextID = 97389094,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=105, Top=149, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CATECOMM", cZoomOnZoom="GSAR_ACT", oKey_1_1="CTCODICE", oKey_1_2="this.w_CATCOM1"

  func oCATCOM1_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATCOM1_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATCOM1_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATECOMM','*','CTCODICE',cp_AbsName(this.parent,'oCATCOM1_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACT',"Categorie commerciali",'',this.parent.oContained
  endproc
  proc oCATCOM1_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_CATCOM1
     i_obj.ecpSave()
  endproc

  add object oCATCON_1_11 as StdField with uid="RYLJPYFLHF",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CATCON", cQueryName = "CATCON",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Categorie contabili incongruenti",;
    ToolTipText = "Categoria contabile di inizio selezione",;
    HelpContextID = 154269146,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=104, Top=181, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CACOCLFO", oKey_1_1="C2CODICE", oKey_1_2="this.w_CATCON"

  func oCATCON_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATCON_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATCON_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CACOCLFO','*','C2CODICE',cp_AbsName(this.parent,'oCATCON_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie contabili",'',this.parent.oContained
  endproc

  add object oCATCON1_1_12 as StdField with uid="QSICCJPXDA",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CATCON1", cQueryName = "CATCON1",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Categorie contabili incongruenti",;
    ToolTipText = "Categoria contabile di fine selezione",;
    HelpContextID = 114166310,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=104, Top=205, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CACOCLFO", oKey_1_1="C2CODICE", oKey_1_2="this.w_CATCON1"

  func oCATCON1_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATCON1_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATCON1_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CACOCLFO','*','C2CODICE',cp_AbsName(this.parent,'oCATCON1_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie contabili",'',this.parent.oContained
  endproc

  add object oDESCCL_1_17 as StdField with uid="TSHATGRORE",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DESCCL", cQueryName = "DESCCL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 200409546,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=249, Top=72, InputMask=replicate('X',40)


  add object oBtn_1_19 as StdButton with uid="PMNUKVFTEJ",left=497, top=413, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 151076374;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCCL1_1_21 as StdField with uid="XXUVEQVZQH",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DESCCL1", cQueryName = "DESCCL1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 68025910,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=249, Top=96, InputMask=replicate('X',40)

  add object oDESCAT_1_25 as StdField with uid="KNHKYIUTMP",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESCAT", cQueryName = "DESCAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 68288970,;
   bGlobalFont=.t.,;
    Height=21, Width=372, Left=166, Top=125, InputMask=replicate('X',35)

  add object oDESCAT1_1_26 as StdField with uid="SJBXZMIAQT",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DESCAT1", cQueryName = "DESCAT1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 200146486,;
   bGlobalFont=.t.,;
    Height=21, Width=372, Left=166, Top=149, InputMask=replicate('X',35)

  add object oDESCON_1_28 as StdField with uid="HIRLLOSBMQ",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 154272202,;
   bGlobalFont=.t.,;
    Height=21, Width=357, Left=181, Top=181, InputMask=replicate('X',35)

  add object oDESCON1_1_30 as StdField with uid="CRVYRYZZSU",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESCON1", cQueryName = "DESCON1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 114163254,;
   bGlobalFont=.t.,;
    Height=21, Width=357, Left=181, Top=205, InputMask=replicate('X',35)


  add object oObj_1_32 as cp_runprogram with uid="UCNNPCVMLI",left=-12, top=493, width=209,height=19,;
    caption='GSMA_BLL(E)',;
   bGlobalFont=.t.,;
    prg="GSMA_BLL('E')",;
    cEvent = "ActivatePage 2",;
    nPag=1;
    , HelpContextID = 70351822


  add object FORMULA as cp_showimage with uid="RCRPRZXZRK",left=11, top=251, width=516,height=154,;
    caption='FORMULA',;
   bGlobalFont=.t.,;
    default="bmp\ScoMed.bmp",backstyle=0,stretch=0,;
    cEvent = "w_TipoAna Changed",;
    nPag=1;
    , HelpContextID = 87554134

  add object oStr_1_15 as StdString with uid="CQOTTUQOCM",Visible=.t., Left=248, Top=39,;
    Alignment=1, Width=66, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="PGJXXHIMYD",Visible=.t., Left=399, Top=39,;
    Alignment=1, Width=63, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="MMUOKAYZDF",Visible=.t., Left=17, Top=70,;
    Alignment=1, Width=83, Height=18,;
    Caption="Da intestatario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="LFTQCAARIW",Visible=.t., Left=22, Top=39,;
    Alignment=1, Width=78, Height=18,;
    Caption="Analisi per:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="JDXZKUHYBK",Visible=.t., Left=17, Top=94,;
    Alignment=1, Width=83, Height=18,;
    Caption="A intestatario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="TRARVDNVMY",Visible=.t., Left=18, Top=130,;
    Alignment=1, Width=82, Height=18,;
    Caption="Da cat. comm.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="LMLSUFSXVA",Visible=.t., Left=8, Top=151,;
    Alignment=1, Width=92, Height=18,;
    Caption="A cat. comm.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="RHQMHJLQEW",Visible=.t., Left=3, Top=186,;
    Alignment=1, Width=96, Height=18,;
    Caption="Da cat. cont.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="HYXVTWGFCD",Visible=.t., Left=8, Top=210,;
    Alignment=1, Width=91, Height=18,;
    Caption="A cat. cont.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="UZBXYDJIZZ",Visible=.t., Left=23, Top=13,;
    Alignment=1, Width=77, Height=18,;
    Caption="Tipo analisi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="FXERKPFIKB",Visible=.t., Left=8, Top=232,;
    Alignment=0, Width=115, Height=18,;
    Caption="Formula utilizzata"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_35 as StdString with uid="WUIOQMSADR",Visible=.t., Left=296, Top=13,;
    Alignment=1, Width=104, Height=18,;
    Caption="Base analisi:"  ;
  , bGlobalFont=.t.

  func oStr_1_35.mHide()
    with this.Parent.oContained
      return (.w_TIPOANA<>'R' OR UPPER(g_APPLICATION)='ADHOC REVOLUTION')
    endwith
  endfunc
enddefine
define class tgsma_slsPag2 as StdContainer
  Width  = 554
  height = 458
  stdWidth  = 554
  stdheight = 458
  resizeXpos=269
  resizeYpos=213
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZOOMLIV as cp_zoombox with uid="TKFOGIDOBB",left=4, top=16, width=545,height=387,;
    caption='ZOOMLIV',;
   bGlobalFont=.t.,;
    cZoomFile="GSMA_SLS",bOptions=.t.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cTable="CONTI",;
    cEvent = "Interroga",;
    nPag=2;
    , HelpContextID = 27773334


  add object oBtn_2_2 as StdButton with uid="MABJXZDKUJ",left=501, top=411, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 151076374;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_2.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_3 as StdButton with uid="UCZYJPDVFF",left=445, top=411, width=48,height=45,;
    CpPicture="bmp\STAMPA.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per eseguire la stampa";
    , HelpContextID = 151076374;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_3.Click()
      with this.Parent.oContained
        GSMA_BLL(this.Parent.oContained,"P")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_sls','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
