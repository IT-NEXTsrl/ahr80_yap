* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_afa                                                        *
*              Famiglie articoli                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_62]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-13                                                      *
* Last revis.: 2012-03-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_afa"))

* --- Class definition
define class tgsar_afa as StdForm
  Top    = 62
  Left   = 137

  * --- Standard Properties
  Width  = 451
  Height = 87+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-03-12"
  HelpContextID=209094807
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  FAM_ARTI_IDX = 0
  cFile = "FAM_ARTI"
  cKeySelect = "FACODICE"
  cKeyWhere  = "FACODICE=this.w_FACODICE"
  cKeyWhereODBC = '"FACODICE="+cp_ToStrODBC(this.w_FACODICE)';

  cKeyWhereODBCqualified = '"FAM_ARTI.FACODICE="+cp_ToStrODBC(this.w_FACODICE)';

  cPrg = "gsar_afa"
  cComment = "Famiglie articoli"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_FACODICE = space(5)
  w_FADESCRI = space(35)
  w_VALATT = space(20)
  w_TABKEY = space(8)
  w_DELIMM = .F.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'FAM_ARTI','gsar_afa')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_afaPag1","gsar_afa",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Famiglia")
      .Pages(1).HelpContextID = 257343159
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFACODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='FAM_ARTI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.FAM_ARTI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.FAM_ARTI_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_FACODICE = NVL(FACODICE,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from FAM_ARTI where FACODICE=KeySet.FACODICE
    *
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('FAM_ARTI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "FAM_ARTI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' FAM_ARTI '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'FACODICE',this.w_FACODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_VALATT = .w_FACODICE
        .w_TABKEY = 'FAM_ARTI'
        .w_DELIMM = .f.
        .w_FACODICE = NVL(FACODICE,space(5))
        .w_FADESCRI = NVL( cp_TransLoadField('FADESCRI') ,space(35))
        cp_LoadRecExtFlds(this,'FAM_ARTI')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_FACODICE = space(5)
      .w_FADESCRI = space(35)
      .w_VALATT = space(20)
      .w_TABKEY = space(8)
      .w_DELIMM = .f.
      if .cFunction<>"Filter"
          .DoRTCalc(1,2,.f.)
        .w_VALATT = .w_FACODICE
        .w_TABKEY = 'FAM_ARTI'
        .w_DELIMM = .f.
      endif
    endwith
    cp_BlankRecExtFlds(this,'FAM_ARTI')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oFACODICE_1_1.enabled = i_bVal
      .Page1.oPag.oFADESCRI_1_2.enabled = i_bVal
      .Page1.oPag.oBtn_1_7.enabled = .Page1.oPag.oBtn_1_7.mCond()
      .Page1.oPag.oBtn_1_8.enabled = .Page1.oPag.oBtn_1_8.mCond()
      .Page1.oPag.oBtn_1_9.enabled = .Page1.oPag.oBtn_1_9.mCond()
      if i_cOp = "Edit"
        .Page1.oPag.oFACODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oFACODICE_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'FAM_ARTI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FACODICE,"FACODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FADESCRI,"FADESCRI",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    i_lTable = "FAM_ARTI"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.FAM_ARTI_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSAR_SFA with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.FAM_ARTI_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into FAM_ARTI
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'FAM_ARTI')
        i_extval=cp_InsertValODBCExtFlds(this,'FAM_ARTI')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(FACODICE,FADESCRI"+cp_TransInsFldName('FADESCRI')+" "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_FACODICE)+;
                  ","+cp_ToStrODBC(this.w_FADESCRI)+cp_TransInsFldValue(this.w_FADESCRI)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'FAM_ARTI')
        i_extval=cp_InsertValVFPExtFlds(this,'FAM_ARTI')
        cp_CheckDeletedKey(i_cTable,0,'FACODICE',this.w_FACODICE)
        INSERT INTO (i_cTable);
              (FACODICE,FADESCRI  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_FACODICE;
                  ,this.w_FADESCRI;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.FAM_ARTI_IDX,i_nConn)
      *
      * update FAM_ARTI
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'FAM_ARTI')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " FADESCRI="+cp_TransUpdFldName('FADESCRI',this.w_FADESCRI)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'FAM_ARTI')
        i_cWhere = cp_PKFox(i_cTable  ,'FACODICE',this.w_FACODICE  )
        UPDATE (i_cTable) SET;
              FADESCRI=this.w_FADESCRI;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.FAM_ARTI_IDX,i_nConn)
      *
      * delete FAM_ARTI
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'FACODICE',this.w_FACODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,5,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_CZGHMRBITS()
    with this
          * --- Cancellazione allegati collegati
          gsma_bae(this;
              ,'I';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_7.visible=!this.oPgFrm.Page1.oPag.oBtn_1_7.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_8.visible=!this.oPgFrm.Page1.oPag.oBtn_1_8.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_9.visible=!this.oPgFrm.Page1.oPag.oBtn_1_9.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Delete start")
          .Calculate_CZGHMRBITS()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFACODICE_1_1.value==this.w_FACODICE)
      this.oPgFrm.Page1.oPag.oFACODICE_1_1.value=this.w_FACODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oFADESCRI_1_2.value==this.w_FADESCRI)
      this.oPgFrm.Page1.oPag.oFADESCRI_1_2.value=this.w_FADESCRI
    endif
    cp_SetControlsValueExtFlds(this,'FAM_ARTI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_FACODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFACODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_FACODICE)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  func CanDelete()
    local i_res
    i_res=GSMA_BAE(this,'G')
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Operazione annullata"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsar_afaPag1 as StdContainer
  Width  = 447
  height = 87
  stdWidth  = 447
  stdheight = 87
  resizeXpos=274
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oFACODICE_1_1 as StdField with uid="ZBNTGPUBDY",rtseq=1,rtrep=.f.,;
    cFormVar = "w_FACODICE", cQueryName = "FACODICE",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice della famiglia articoli",;
    HelpContextID = 100008293,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=107, Top=11, InputMask=replicate('X',5)

  add object oFADESCRI_1_2 as StdField with uid="WWHUQZMFCN",rtseq=2,rtrep=.f.,;
    cFormVar = "w_FADESCRI", cQueryName = "FADESCRI",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .t.,;
    ToolTipText = "Descrizione della famiglia articoli",;
    HelpContextID = 185594209,;
   bGlobalFont=.t.,;
    Height=21, Width=276, Left=168, Top=11, InputMask=replicate('X',35)


  add object oBtn_1_7 as StdButton with uid="UHLKEQDFSQ",left=284, top=38, width=48,height=45,;
    CpPicture="bmp\cattura.bmp", caption="", nPag=1;
    , ToolTipText = "Cattura un allegato da unit� disco";
    , HelpContextID = 105000486;
    , tabstop=.f., Visible=IIF(g_FLARDO='S', .T., .F.), UID = 'CATTURA', disabledpicture = 'bmp\catturad.bmp',caption='\<Cattura';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      with this.Parent.oContained
        GSUT_BCV(this.Parent.oContained,"FAM_ARTI",.w_FACODICE,"GSAR_AFA","C")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_7.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.not. empty(.w_FACODICE) And .cFunction='Query')
      endwith
    endif
  endfunc

  func oBtn_1_7.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_FLARDO<>'S' or isalt())
     endwith
    endif
  endfunc


  add object oBtn_1_8 as StdButton with uid="YYRCTQLCDE",left=339, top=38, width=48,height=45,;
    CpPicture="BMP\visualicat.ico", caption="", nPag=1;
    , ToolTipText = "Visualizza allegati";
    , HelpContextID = 251891824;
    , Caption='\<Visualizza';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      with this.Parent.oContained
        GSUT_BCV(this.Parent.oContained,"FAM_ARTI",.w_FACODICE,"GSAR_AFA","V")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_8.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.not. empty(.w_FACODICE) And .cFunction='Query')
      endwith
    endif
  endfunc

  func oBtn_1_8.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_FLARDO<>'S' or isalt())
     endwith
    endif
  endfunc


  add object oBtn_1_9 as StdButton with uid="BJRFWTRDLX",left=394, top=38, width=48,height=45,;
    CpPicture="bmp\scanner.bmp", caption="", nPag=1;
    , ToolTipText = "Cattura un allegato da periferica di acquisizione immagini";
    , HelpContextID = 147521830;
    , tabstop=.f., Visible=IIF(g_FLARDO='S', .T., .F.), UID = 'SCANNER', disabledpicture = 'bmp\scannerd.bmp',caption='\<Scanner';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      with this.Parent.oContained
        GSUT_BCV(this.Parent.oContained,"FAM_ARTI",.w_FACODICE,"GSAR_AFA","S",MROW(),MCOL(),.T.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.not. empty(.w_FACODICE) And .cFunction='Query')
      endwith
    endif
  endfunc

  func oBtn_1_9.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_DOCM<>'S' or g_FLARDO<>'S' or isalt())
     endwith
    endif
  endfunc

  add object oStr_1_6 as StdString with uid="BGPQIUEGPT",Visible=.t., Left=4, Top=11,;
    Alignment=1, Width=102, Height=18,;
    Caption="Codice famiglia:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_afa','FAM_ARTI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".FACODICE=FAM_ARTI.FACODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
