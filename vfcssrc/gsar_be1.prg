* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_be1                                                        *
*              Verifica cancellazione codice ABI                               *
*                                                                              *
*      Author: Pollina Fabrizio                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][39]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-02-01                                                      *
* Last revis.: 2007-11-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCODABI,pCODCAB
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_be1",oParentObject,m.pCODABI,m.pCODCAB)
return(i_retval)

define class tgsar_be1 as StdBatch
  * --- Local variables
  pCODABI = space(5)
  pCODCAB = space(5)
  w_MESS = space(100)
  * --- WorkFile variables
  COD_CAB_idx=0
  BAN_CHE_idx=0
  MODVPAG_idx=0
  COC_MAST_idx=0
  DIS_BECV_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch lanciato da GSAR_ABI e GSAR_AFI
    if empty(nvl(this.pCODCAB,space(5)))
      * --- Read from COD_CAB
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.COD_CAB_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.COD_CAB_idx,2],.t.,this.COD_CAB_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "*"+;
          " from "+i_cTable+" COD_CAB where ";
              +"FICODABI = "+cp_ToStrODBC(this.pCODABI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          *;
          from (i_cTable) where;
              FICODABI = this.pCODABI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if i_Rows<>0
        this.w_MESS = Ah_MsgFormat("Codice ABI utilizzato nella tabella dei codici CAB, impossibile cancellare")
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=this.w_MESS
        i_retcode = 'stop'
        return
      endif
      * --- Read from BAN_CHE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.BAN_CHE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.BAN_CHE_idx,2],.t.,this.BAN_CHE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "*"+;
          " from "+i_cTable+" BAN_CHE where ";
              +"BACODABI = "+cp_ToStrODBC(this.pCODABI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          *;
          from (i_cTable) where;
              BACODABI = this.pCODABI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if i_Rows<>0
        this.w_MESS = Ah_MsgFormat("Codice ABI utilizzato nella tabella anagrafica BANCHE, impossibile cancellare")
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=this.w_MESS
        i_retcode = 'stop'
        return
      endif
      * --- Read from COC_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.COC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2],.t.,this.COC_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "*"+;
          " from "+i_cTable+" COC_MAST where ";
              +"BACODABI = "+cp_ToStrODBC(this.pCODABI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          *;
          from (i_cTable) where;
              BACODABI = this.pCODABI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if i_Rows<>0
        this.w_MESS = Ah_MsgFormat("Codice ABI utilizzato nella tabella conti di tesoreria, impossibile cancellare")
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=this.w_MESS
        i_retcode = 'stop'
        return
      endif
      * --- Read from MODVPAG
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MODVPAG_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MODVPAG_idx,2],.t.,this.MODVPAG_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "*"+;
          " from "+i_cTable+" MODVPAG where ";
              +"VFCODABI = "+cp_ToStrODBC(this.pCODABI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          *;
          from (i_cTable) where;
              VFCODABI = this.pCODABI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if i_Rows<>0
        this.w_MESS = Ah_MsgFormat("Codice CAB utilizzato nella tabella estremi versamento del modello F24, impossibile cancellare")
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=this.w_MESS
        i_retcode = 'stop'
        return
      endif
      if g_APPLICATION = "ad hoc ENTERPRISE" And g_REBA="S"
        * --- Read from DIS_BECV
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DIS_BECV_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DIS_BECV_idx,2],.t.,this.DIS_BECV_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "*"+;
            " from "+i_cTable+" DIS_BECV where ";
                +"DIBANEMI = "+cp_ToStrODBC(this.pCODABI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            *;
            from (i_cTable) where;
                DIBANEMI = this.pCODABI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_Rows<>0
          this.w_MESS = Ah_MsgFormat("Codice ABI utilizzato nella tabella informazioni CVS per bonifici esteri, impossibile cancellare")
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
        endif
      endif
    else
      * --- Read from BAN_CHE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.BAN_CHE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.BAN_CHE_idx,2],.t.,this.BAN_CHE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "*"+;
          " from "+i_cTable+" BAN_CHE where ";
              +"BACODABI = "+cp_ToStrODBC(this.pCODABI);
              +" and BACODCAB = "+cp_ToStrODBC(this.pCODCAB);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          *;
          from (i_cTable) where;
              BACODABI = this.pCODABI;
              and BACODCAB = this.pCODCAB;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if i_Rows<>0
        this.w_MESS = Ah_MsgFormat("Codice CAB utilizzato nella tabella anagrafica BANCHE, impossibile cancellare")
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=this.w_MESS
        i_retcode = 'stop'
        return
      endif
      * --- Read from COC_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.COC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2],.t.,this.COC_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "*"+;
          " from "+i_cTable+" COC_MAST where ";
              +"BACODABI = "+cp_ToStrODBC(this.pCODABI);
              +" and BACODCAB = "+cp_ToStrODBC(this.pCODCAB);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          *;
          from (i_cTable) where;
              BACODABI = this.pCODABI;
              and BACODCAB = this.pCODCAB;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if i_Rows<>0
        this.w_MESS = Ah_MsgFormat("Codice ABI utilizzato nella tabella conti di tesoreria, impossibile cancellare")
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=this.w_MESS
        i_retcode = 'stop'
        return
      endif
      * --- Read from MODVPAG
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MODVPAG_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MODVPAG_idx,2],.t.,this.MODVPAG_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "*"+;
          " from "+i_cTable+" MODVPAG where ";
              +"VFCODABI = "+cp_ToStrODBC(this.pCODABI);
              +" and VFCODCAB = "+cp_ToStrODBC(this.pCODCAB);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          *;
          from (i_cTable) where;
              VFCODABI = this.pCODABI;
              and VFCODCAB = this.pCODCAB;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if i_Rows<>0
        this.w_MESS = Ah_MsgFormat("Codice CAB utilizzato nella tabella estremi versamento del modello F24, impossibile cancellare")
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=this.w_MESS
        i_retcode = 'stop'
        return
      endif
      if g_TESO="S"
        * --- Read from MODVPAG
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.MODVPAG_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MODVPAG_idx,2],.t.,this.MODVPAG_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "*"+;
            " from "+i_cTable+" MODVPAG where ";
                +"VFCODTES = "+cp_ToStrODBC(this.pCODABI);
                +" and VFCABTES = "+cp_ToStrODBC(this.pCODCAB);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            *;
            from (i_cTable) where;
                VFCODTES = this.pCODABI;
                and VFCABTES = this.pCODCAB;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_Rows<>0
          this.w_MESS = Ah_MsgFormat("Codice CAB utilizzato nella tabella estremi versamento del modello F24 in codice banca/poste/concessionario, impossibile cancellare")
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
        endif
      else
        * --- Read from MODVPAG
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.MODVPAG_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MODVPAG_idx,2],.t.,this.MODVPAG_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "*"+;
            " from "+i_cTable+" MODVPAG where ";
                +"VFCODBAN = "+cp_ToStrODBC(this.pCODABI);
                +" and VFCABBAN = "+cp_ToStrODBC(this.pCODCAB);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            *;
            from (i_cTable) where;
                VFCODBAN = this.pCODABI;
                and VFCABBAN = this.pCODCAB;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_Rows<>0
          this.w_MESS = Ah_MsgFormat("Codice CAB utilizzato nella tabella estremi versamento del modello F24 in codice banca/poste/concessionario, impossibile cancellare")
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
        endif
      endif
    endif
  endproc


  proc Init(oParentObject,pCODABI,pCODCAB)
    this.pCODABI=pCODABI
    this.pCODCAB=pCODCAB
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='COD_CAB'
    this.cWorkTables[2]='BAN_CHE'
    this.cWorkTables[3]='MODVPAG'
    this.cWorkTables[4]='COC_MAST'
    this.cWorkTables[5]='DIS_BECV'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCODABI,pCODCAB"
endproc
