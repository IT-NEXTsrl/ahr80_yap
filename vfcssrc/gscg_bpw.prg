* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bpw                                                        *
*              Stampa verifica plafond                                         *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][147]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-06-30                                                      *
* Last revis.: 2015-09-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bpw",oParentObject,m.pOper)
return(i_retval)

define class tgscg_bpw as StdBatch
  * --- Local variables
  pOper = space(1)
  w_ANNPREC = space(4)
  w_MESEINIZIALE = 0
  w_ANNOINT = 0
  w_PLNODO = space(1)
  w_NRECORD = 0
  w_PLAFINI = 0
  w_INIPLA = 0
  w_TOTESPORT = 0
  w_ESPORT = 0
  w_UTPLAFMESPREC = 0
  w_DISPONIBILE = 0
  w_ESPORTAZIONI = 0
  w_IMPORTAZIONI = 0
  w_ANNPREC2 = space(4)
  w_MESEAGG = 0
  w_ANNOAGG = space(4)
  w_AGGPLADIS = .f.
  w_DATECOUNTER = ctod("  /  /  ")
  w_MOUNTHTOCHECK = 0
  w_YEARTOCHECK = 0
  w_AICODVAL = space(3)
  * --- WorkFile variables
  ATTIDETT_idx=0
  AZIENDA_idx=0
  ESERCIZI_idx=0
  TMPPNT_IVA_idx=0
  TMPPNT_MAST_idx=0
  DAT_IVAN_idx=0
  PLA_IVAN_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa verifica plafond (da GSCG_SVP)
    * --- Plafond disponibile all'inizio del periodo
    * --- Somma cessioni 12 mesi precedenti
    * --- Cessioni estere del 13-esimo mese
    * --- Utilizzo Plafond al termine del mese precedente (w_H mese prima)
    * --- Plafond disponibile
    * --- Leggo se escludere o meno i documenti passivi
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZPLNODO"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODATT);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZPLNODO;
        from (i_cTable) where;
            AZCODAZI = this.oParentObject.w_CODATT;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PLNODO = NVL(cp_ToDate(_read_.AZPLNODO),cp_NullValue(_read_.AZPLNODO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_ANNPREC = IIF ( this.oParentObject.w_MOBPLA="S" , alltrim(str(val(this.oParentObject.w_ANNO)-1)) ,"1900")
    this.w_AGGPLADIS = .F.
    do case
      case this.pOper="A"
        * --- Calcola i mesi da impostare sulla maschera di stampa
        this.oParentObject.w_MESEINI = 0
        * --- Verifico quale sia l'ultimo periodo stampato per l'anno impostato sulla 
        *     maschera
        * --- Select from PLA_IVAN
        i_nConn=i_TableProp[this.PLA_IVAN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PLA_IVAN_idx,2],.t.,this.PLA_IVAN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select Max ( DIPERIOD ) As Massimo  from "+i_cTable+" PLA_IVAN ";
              +" where DI__ANNO = "+cp_ToStrODBC(this.oParentObject.w_ANNO)+" And DICODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODATT)+"";
               ,"_Curs_PLA_IVAN")
        else
          select Max ( DIPERIOD ) As Massimo from (i_cTable);
           where DI__ANNO = this.oParentObject.w_ANNO And DICODAZI = this.oParentObject.w_CODATT;
            into cursor _Curs_PLA_IVAN
        endif
        if used('_Curs_PLA_IVAN')
          select _Curs_PLA_IVAN
          locate for 1=1
          do while not(eof())
          this.oParentObject.w_MESEINI = Nvl ( _Curs_PLA_IVAN.MASSIMO , 0 )
            select _Curs_PLA_IVAN
            continue
          enddo
          use
        endif
        * --- Nel caso non sia stato stampato in definitiva nessun periodo dell'anno
        *     filtrato vado a verificare quale sia l'ultimo periodo stampato per l'anno
        *     precedente
        if this.oParentObject.w_MESEINI=0
          * --- Select from PLA_IVAN
          i_nConn=i_TableProp[this.PLA_IVAN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PLA_IVAN_idx,2],.t.,this.PLA_IVAN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select Max ( DIPERIOD ) As Massimo  from "+i_cTable+" PLA_IVAN ";
                +" where DI__ANNO = "+cp_ToStrODBC(this.w_ANNPREC)+" And DICODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODATT)+"";
                 ,"_Curs_PLA_IVAN")
          else
            select Max ( DIPERIOD ) As Massimo from (i_cTable);
             where DI__ANNO = this.w_ANNPREC And DICODAZI = this.oParentObject.w_CODATT;
              into cursor _Curs_PLA_IVAN
          endif
          if used('_Curs_PLA_IVAN')
            select _Curs_PLA_IVAN
            locate for 1=1
            do while not(eof())
            this.oParentObject.w_MESESTAMPATOP = Nvl ( _Curs_PLA_IVAN.MASSIMO , 0 )
              select _Curs_PLA_IVAN
              continue
            enddo
            use
          endif
        else
          this.oParentObject.w_MESESTAMPATOP = 12
        endif
        this.oParentObject.w_MESESTAMPATO = this.oParentObject.w_MESEINI+1
        this.oParentObject.w_MESEINI = IIF(this.oParentObject.w_MESEINI=0,1,this.oParentObject.w_MESEINI)
        this.oParentObject.w_MESEFIN = this.oParentObject.w_MESEINI
        * --- Imposto la descrizione
        this.oParentObject.w_DESCRI = g_MESE[this.oParentObject.w_MESEINI]
        this.oParentObject.w_DESCRIF = g_MESE[this.oParentObject.w_MESEFIN]
      case this.pOper="S"
        * --- La variabile w_Meseiniziale mi serve per poter filtrare i movimenti di primanota
        *     e i documenti. Questa variabile serve per poter considerare anceh i periodi
        *     che non sono stati stampati in definitiva e non rientrano nei periodi filtrati 
        this.w_MESEINIZIALE = IIF(this.oParentObject.w_MESEINI<=this.oParentObject.w_MESESTAMPATO,this.oParentObject.w_MESEINI,this.oParentObject.w_MESESTAMPATO)
        * --- Questa stampa � lanciabile solo nel caso in cui la valuta plafond
        *     sia uguale alla valuta di conto
        if g_PERVAL<>this.oParentObject.w_VALPLA
          AH_ERRORMSG("Valuta stampa plafond differente dalla valuta di conto",48)
          i_retcode = 'stop'
          return
        endif
        if this.oParentObject.w_MOBPLA="S"
          * --- Read from DAT_IVAN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DAT_IVAN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DAT_IVAN_idx,2],.t.,this.DAT_IVAN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "IAVALPLA"+;
              " from "+i_cTable+" DAT_IVAN where ";
                  +"IACODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODATT);
                  +" and IA__ANNO = "+cp_ToStrODBC(this.w_ANNPREC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              IAVALPLA;
              from (i_cTable) where;
                  IACODAZI = this.oParentObject.w_CODATT;
                  and IA__ANNO = this.w_ANNPREC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_AICODVAL = NVL(cp_ToDate(_read_.IAVALPLA),cp_NullValue(_read_.IAVALPLA))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if g_PERVAL<>this.w_AICODVAL
            AH_ERRORMSG("Valuta stampa plafond differente dalla valuta di conto",48)
            i_retcode = 'stop'
            return
          endif
          * --- Nel caso di Plafond mobile verifico che almeno un periodo tra i 12
          *     precedenti il mese iniziale sia stato stampato in definitiva
          if this.oParentObject.w_MESESTAMPATOP<this.oParentObject.w_MESEINI
            AH_ERRORMSG("Nei dodici mesi precedenti non � stato stampato il Plafond in definitiva",48)
            i_retcode = 'stop'
            return
          endif
        endif
        ah_msg("Fase calcolo plafond...")
        * --- Se plafond fisso non leggo nessun dato dall'anno precedente
        this.w_ANNOINT = Val ( this.oParentObject.w_ANNO )
        * --- Inizio Elaborazione
        * --- Calcola gli Utilizzi Plafond
        ah_msg("Lettura documenti di utilizzo plafond")
        if this.w_PLNODO="S"
          * --- Se check Attivo eseguo solo l'ultima query della catena che preleva i dati dalla prima nota
          * --- Create temporary table TMPPNT_IVA
          i_nIdx=cp_AddTableDef('TMPPNT_IVA') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('QUERY\GSCG6SVP',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.TMPPNT_IVA_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        else
          * --- Create temporary table TMPPNT_IVA
          i_nIdx=cp_AddTableDef('TMPPNT_IVA') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('QUERY\GSCGDSVP',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.TMPPNT_IVA_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        endif
        * --- Nel caso di Plafond variabile devo inserire nel cursore TMPPNT_IVA 
        *     anche i record contenenti le esportazioni
        if this.oParentObject.w_MOBPLA="S"
          * --- Insert into TMPPNT_IVA
          i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"QUERY\GSCG8SVP",this.TMPPNT_IVA_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
        * --- Controllo se esiste almeno un record nella tabella temporanea TMPPNT_IVA
        * --- Select from TMPPNT_IVA
        i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2],.t.,this.TMPPNT_IVA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select count(*) AS NRECORD  from "+i_cTable+" TMPPNT_IVA ";
              +" where STAMPA='S'";
               ,"_Curs_TMPPNT_IVA")
        else
          select count(*) AS NRECORD from (i_cTable);
           where STAMPA="S";
            into cursor _Curs_TMPPNT_IVA
        endif
        if used('_Curs_TMPPNT_IVA')
          select _Curs_TMPPNT_IVA
          locate for 1=1
          do while not(eof())
          this.w_NRECORD = NRECORD
            select _Curs_TMPPNT_IVA
            continue
          enddo
          use
        endif
        if this.w_NRECORD>0
          if this.oParentObject.w_MOBPLA="S"
            * --- Inserisco i movimenti raggruppati per mese ed anno plafond. La query
            *     Gscg9svp considera i record della tabella "TmpPnt_Iva" mentre la query
            *     Gscg10svp legge i dati dalla tabella "Pla_fond"
            * --- Insert into TMPPNT_IVA
            i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"QUERY\GSCG9SVP",this.TMPPNT_IVA_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
            * --- Delete from TMPPNT_IVA
            i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"STA_MESPREC = "+cp_ToStrODBC("S");
                     )
            else
              delete from (i_cTable) where;
                    STA_MESPREC = "S";

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
            * --- Creo un nuovo cursore temporaneo "Tmppnt_mast" per poter calcolare
            *     il plafond disponibile dei periodi che devono essere stampati
            * --- Create temporary table TMPPNT_MAST
            i_nIdx=cp_AddTableDef('TMPPNT_MAST') && aggiunge la definizione nella lista delle tabelle
            i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
            vq_exec('GSCG11SVP',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
            this.TMPPNT_MAST_idx=i_nIdx
            i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
            this.w_DATECOUNTER = DATE( VAL(this.w_ANNPREC), this.oParentObject.w_MESEINI, 1 )
            * --- Devo verificare se dentro il cursore TMPPNT_MAST sia presenti tutti
            *     i 12 mesi precedenti al mese di inizio stampa, nel caso non fossero 
            *     presenti dei mesi devo inseririli
            do while this.w_DATECOUNTER < DATE( this.w_ANNOINT , this.oParentObject.w_MESEINI, 1)
              this.w_MOUNTHTOCHECK = MONTH( this.w_DATECOUNTER )
              this.w_YEARTOCHECK = YEAR( this.w_DATECOUNTER )
              * --- Read from TMPPNT_MAST
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.TMPPNT_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_MAST_idx,2],.t.,this.TMPPNT_MAST_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "*"+;
                  " from "+i_cTable+" TMPPNT_MAST where ";
                      +"ANNO = "+cp_ToStrODBC(this.w_YEARTOCHECK);
                      +" and MESE = "+cp_ToStrODBC(this.w_MOUNTHTOCHECK);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  *;
                  from (i_cTable) where;
                      ANNO = this.w_YEARTOCHECK;
                      and MESE = this.w_MOUNTHTOCHECK;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if i_Rows=0
                * --- Insert into TMPPNT_MAST
                i_nConn=i_TableProp[this.TMPPNT_MAST_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_MAST_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_ccchkf=''
                i_ccchkv=''
                this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPPNT_MAST_idx,i_nConn)
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                              " ("+"ANNO"+",MESE"+",IMPORTAZIONI"+",STA_MESPREC"+",ESPORTAZIONI"+",DISPONIBILE"+",STAMPA"+i_ccchkf+") values ("+;
                  cp_NullLink(cp_ToStrODBC(this.w_YEARTOCHECK),'TMPPNT_MAST','ANNO');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_MOUNTHTOCHECK),'TMPPNT_MAST','MESE');
                  +","+cp_NullLink(cp_ToStrODBC(0),'TMPPNT_MAST','IMPORTAZIONI');
                  +","+cp_NullLink(cp_ToStrODBC("N"),'TMPPNT_MAST','STA_MESPREC');
                  +","+cp_NullLink(cp_ToStrODBC(0),'TMPPNT_MAST','ESPORTAZIONI');
                  +","+cp_NullLink(cp_ToStrODBC(0),'TMPPNT_MAST','DISPONIBILE');
                  +","+cp_NullLink(cp_ToStrODBC("N"),'TMPPNT_MAST','STAMPA');
                       +i_ccchkv+")")
                else
                  cp_CheckDeletedKey(i_cTable,0,'ANNO',this.w_YEARTOCHECK,'MESE',this.w_MOUNTHTOCHECK,'IMPORTAZIONI',0,'STA_MESPREC',"N",'ESPORTAZIONI',0,'DISPONIBILE',0,'STAMPA',"N")
                  insert into (i_cTable) (ANNO,MESE,IMPORTAZIONI,STA_MESPREC,ESPORTAZIONI,DISPONIBILE,STAMPA &i_ccchkf. );
                     values (;
                       this.w_YEARTOCHECK;
                       ,this.w_MOUNTHTOCHECK;
                       ,0;
                       ,"N";
                       ,0;
                       ,0;
                       ,"N";
                       &i_ccchkv. )
                  i_Rows=iif(bTrsErr,0,1)
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if i_Rows<0 or bTrsErr
                  * --- Error: insert not accepted
                  i_Error=MSG_INSERT_ERROR
                  return
                endif
                * --- Insert into TMPPNT_IVA
                i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_ccchkf=''
                i_ccchkv=''
                this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPPNT_IVA_idx,i_nConn)
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                              " ("+"ANNO"+",MESE"+",IMPORTAZIONI"+",STA_MESPREC"+",ESPORTAZIONI"+",DISPONIBILE"+",STAMPA"+",ORDSTAMPA"+",NUMDOC"+",ALFDOC"+",INTRA"+",ORDINE"+",SERIALE"+",NUMRER"+i_ccchkf+") values ("+;
                  cp_NullLink(cp_ToStrODBC(this.w_YEARTOCHECK),'TMPPNT_IVA','ANNO');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_MOUNTHTOCHECK),'TMPPNT_IVA','MESE');
                  +","+cp_NullLink(cp_ToStrODBC(0),'TMPPNT_IVA','IMPORTAZIONI');
                  +","+cp_NullLink(cp_ToStrODBC("N"),'TMPPNT_IVA','STA_MESPREC');
                  +","+cp_NullLink(cp_ToStrODBC(0),'TMPPNT_IVA','ESPORTAZIONI');
                  +","+cp_NullLink(cp_ToStrODBC(0),'TMPPNT_IVA','DISPONIBILE');
                  +","+cp_NullLink(cp_ToStrODBC("N"),'TMPPNT_IVA','STAMPA');
                  +","+cp_NullLink(cp_ToStrODBC("A"),'TMPPNT_IVA','ORDSTAMPA');
                  +","+cp_NullLink(cp_ToStrODBC(0),'TMPPNT_IVA','NUMDOC');
                  +","+cp_NullLink(cp_ToStrODBC("  "),'TMPPNT_IVA','ALFDOC');
                  +","+cp_NullLink(cp_ToStrODBC(0),'TMPPNT_IVA','INTRA');
                  +","+cp_NullLink(cp_ToStrODBC("P"),'TMPPNT_IVA','ORDINE');
                  +","+cp_NullLink(cp_ToStrODBC(" "),'TMPPNT_IVA','SERIALE');
                  +","+cp_NullLink(cp_ToStrODBC(0),'TMPPNT_IVA','NUMRER');
                       +i_ccchkv+")")
                else
                  cp_CheckDeletedKey(i_cTable,0,'ANNO',this.w_YEARTOCHECK,'MESE',this.w_MOUNTHTOCHECK,'IMPORTAZIONI',0,'STA_MESPREC',"N",'ESPORTAZIONI',0,'DISPONIBILE',0,'STAMPA',"N",'ORDSTAMPA',"A",'NUMDOC',0,'ALFDOC',"  ",'INTRA',0,'ORDINE',"P")
                  insert into (i_cTable) (ANNO,MESE,IMPORTAZIONI,STA_MESPREC,ESPORTAZIONI,DISPONIBILE,STAMPA,ORDSTAMPA,NUMDOC,ALFDOC,INTRA,ORDINE,SERIALE,NUMRER &i_ccchkf. );
                     values (;
                       this.w_YEARTOCHECK;
                       ,this.w_MOUNTHTOCHECK;
                       ,0;
                       ,"N";
                       ,0;
                       ,0;
                       ,"N";
                       ,"A";
                       ,0;
                       ,"  ";
                       ,0;
                       ,"P";
                       ," ";
                       ,0;
                       &i_ccchkv. )
                  i_Rows=iif(bTrsErr,0,1)
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if i_Rows<0 or bTrsErr
                  * --- Error: insert not accepted
                  i_Error=MSG_INSERT_ERROR
                  return
                endif
              endif
              this.w_DATECOUNTER = GOMONTH( this.w_DATECOUNTER, 1)
            enddo
            * --- Select from TMPPNT_MAST
            i_nConn=i_TableProp[this.TMPPNT_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_MAST_idx,2],.t.,this.TMPPNT_MAST_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMPPNT_MAST ";
                  +" where STA_MESPREC='N'";
                  +" order by ANNO,MESE";
                   ,"_Curs_TMPPNT_MAST")
            else
              select * from (i_cTable);
               where STA_MESPREC="N";
               order by ANNO,MESE;
                into cursor _Curs_TMPPNT_MAST
            endif
            if used('_Curs_TMPPNT_MAST')
              select _Curs_TMPPNT_MAST
              locate for 1=1
              do while not(eof())
              this.w_MESEAGG = _Curs_TMPPNT_MAST.MESE
              this.w_ANNOAGG = _Curs_TMPPNT_MAST.ANNO
              this.w_ANNPREC = this.w_ANNOAGG-1
              * --- Per calcolare il plafond disponibile devo calcolare per i vari mesi non stampati in
              *     definitiva il totale delle esportazioni
              * --- Select from GSCG12SVP
              do vq_exec with 'GSCG12SVP',this,'_Curs_GSCG12SVP','',.f.,.t.
              if used('_Curs_GSCG12SVP')
                select _Curs_GSCG12SVP
                locate for 1=1
                do while not(eof())
                this.w_TOTESPORT = Nvl (_Curs_GSCG12SVP.TOTESPORT ,0 )
                  select _Curs_GSCG12SVP
                  continue
                enddo
                use
              endif
              * --- Leggo dal cursore temporareno TMPPNT_MAST il valore delle esportazioni
              *     degli utilizzi ed il plafond disponbile del mese precedente
              if this.w_MESEAGG=1
                * --- Read from TMPPNT_MAST
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.TMPPNT_MAST_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_MAST_idx,2],.t.,this.TMPPNT_MAST_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "IMPORTAZIONI,ESPORTAZIONI,DISPONIBILE"+;
                    " from "+i_cTable+" TMPPNT_MAST where ";
                        +"ANNO = "+cp_ToStrODBC(this.w_ANNPREC);
                        +" and MESE = "+cp_ToStrODBC(12);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    IMPORTAZIONI,ESPORTAZIONI,DISPONIBILE;
                    from (i_cTable) where;
                        ANNO = this.w_ANNPREC;
                        and MESE = 12;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_IMPORTAZIONI = NVL(cp_ToDate(_read_.IMPORTAZIONI),cp_NullValue(_read_.IMPORTAZIONI))
                  this.w_ESPORTAZIONI = NVL(cp_ToDate(_read_.ESPORTAZIONI),cp_NullValue(_read_.ESPORTAZIONI))
                  this.w_DISPONIBILE = NVL(cp_ToDate(_read_.DISPONIBILE),cp_NullValue(_read_.DISPONIBILE))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                * --- Cessioni estere del 13-esimo mese espungibili
                this.w_ANNPREC2 = this.w_ANNPREC-1
                * --- Read from PLA_IVAN
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.PLA_IVAN_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PLA_IVAN_idx,2],.t.,this.PLA_IVAN_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "DITOTESP"+;
                    " from "+i_cTable+" PLA_IVAN where ";
                        +"DICODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODATT);
                        +" and DI__ANNO = "+cp_ToStrODBC(this.w_ANNPREC2);
                        +" and DIPERIOD = "+cp_ToStrODBC(12);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    DITOTESP;
                    from (i_cTable) where;
                        DICODAZI = this.oParentObject.w_CODATT;
                        and DI__ANNO = this.w_ANNPREC2;
                        and DIPERIOD = 12;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_ESPORT = NVL(cp_ToDate(_read_.DITOTESP),cp_NullValue(_read_.DITOTESP))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                * --- Read from DAT_IVAN
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.DAT_IVAN_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.DAT_IVAN_idx,2],.t.,this.DAT_IVAN_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "IAVALPLA"+;
                    " from "+i_cTable+" DAT_IVAN where ";
                        +"IACODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODATT);
                        +" and IA__ANNO = "+cp_ToStrODBC(this.w_ANNPREC);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    IAVALPLA;
                    from (i_cTable) where;
                        IACODAZI = this.oParentObject.w_CODATT;
                        and IA__ANNO = this.w_ANNPREC;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_AICODVAL = NVL(cp_ToDate(_read_.IAVALPLA),cp_NullValue(_read_.IAVALPLA))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                if this.w_AICODVAL <> this.oParentObject.w_VALPLA
                  * --- convertiamo in valuta attuale il plafond residuo del mese precedente
                  this.w_ESPORT = VALCAM(this.w_ESPORT, this.w_AICODVAL, this.oParentObject.w_VALPLA,CP_CHARTODATE("01-01-" + this.oParentObject.w_ANNO), 0, this.oParentObject.w_DECTOT)
                endif
              else
                * --- Read from TMPPNT_MAST
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.TMPPNT_MAST_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_MAST_idx,2],.t.,this.TMPPNT_MAST_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "IMPORTAZIONI,ESPORTAZIONI,DISPONIBILE"+;
                    " from "+i_cTable+" TMPPNT_MAST where ";
                        +"ANNO = "+cp_ToStrODBC(this.w_ANNOAGG);
                        +" and MESE = "+cp_ToStrODBC(this.w_MESEAGG-1);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    IMPORTAZIONI,ESPORTAZIONI,DISPONIBILE;
                    from (i_cTable) where;
                        ANNO = this.w_ANNOAGG;
                        and MESE = this.w_MESEAGG-1;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_IMPORTAZIONI = NVL(cp_ToDate(_read_.IMPORTAZIONI),cp_NullValue(_read_.IMPORTAZIONI))
                  this.w_ESPORTAZIONI = NVL(cp_ToDate(_read_.ESPORTAZIONI),cp_NullValue(_read_.ESPORTAZIONI))
                  this.w_DISPONIBILE = NVL(cp_ToDate(_read_.DISPONIBILE),cp_NullValue(_read_.DISPONIBILE))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                * --- Cessioni estere del 13-esimo mese espungibili
                * --- Read from PLA_IVAN
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.PLA_IVAN_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PLA_IVAN_idx,2],.t.,this.PLA_IVAN_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "DITOTESP"+;
                    " from "+i_cTable+" PLA_IVAN where ";
                        +"DICODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODATT);
                        +" and DI__ANNO = "+cp_ToStrODBC(this.w_ANNPREC);
                        +" and DIPERIOD = "+cp_ToStrODBC(this.w_MESEAGG - 1);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    DITOTESP;
                    from (i_cTable) where;
                        DICODAZI = this.oParentObject.w_CODATT;
                        and DI__ANNO = this.w_ANNPREC;
                        and DIPERIOD = this.w_MESEAGG - 1;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_ESPORT = NVL(cp_ToDate(_read_.DITOTESP),cp_NullValue(_read_.DITOTESP))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
              endif
              * --- Calcolo la somma delle cessioni periodo precedente, dal totale tolgo l'ultima cessione ed aggiungo l'ex dodicesima ora tredicesima (w_C)
              * --- - plafond disponibile inizio mese + Utilizzi mese precedente
              this.w_UTPLAFMESPREC = this.w_TOTESPORT - this.w_ESPORTAZIONI + this.w_ESPORT - this.w_DISPONIBILE + this.w_IMPORTAZIONI
              * --- Plafond disponibile all'inizio del periodo w_A - (w_B - w_C)
              this.w_INIPLA = this.w_TOTESPORT - IIF( this.w_UTPLAFMESPREC - this.w_ESPORT >0 , this.w_UTPLAFMESPREC - this.w_ESPORT , 0 )
              * --- Aggiorno il plafond disponibile dei cursori temporanei "Tmppnt_mast" e 
              *     "Tmppnt_Iva"
              * --- Write into TMPPNT_MAST
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.TMPPNT_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_MAST_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_MAST_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"DISPONIBILE ="+cp_NullLink(cp_ToStrODBC(this.w_INIPLA),'TMPPNT_MAST','DISPONIBILE');
                    +i_ccchkf ;
                +" where ";
                    +"MESE = "+cp_ToStrODBC(this.w_MESEAGG);
                    +" and ANNO = "+cp_ToStrODBC(this.w_ANNOAGG);
                       )
              else
                update (i_cTable) set;
                    DISPONIBILE = this.w_INIPLA;
                    &i_ccchkf. ;
                 where;
                    MESE = this.w_MESEAGG;
                    and ANNO = this.w_ANNOAGG;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              * --- Write into TMPPNT_IVA
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_IVA_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"DISPONIBILE ="+cp_NullLink(cp_ToStrODBC(this.w_INIPLA),'TMPPNT_IVA','DISPONIBILE');
                    +i_ccchkf ;
                +" where ";
                    +"MESE = "+cp_ToStrODBC(this.w_MESEAGG);
                    +" and ANNO = "+cp_ToStrODBC(this.w_ANNOAGG);
                       )
              else
                update (i_cTable) set;
                    DISPONIBILE = this.w_INIPLA;
                    &i_ccchkf. ;
                 where;
                    MESE = this.w_MESEAGG;
                    and ANNO = this.w_ANNOAGG;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              if _Curs_TMPPNT_MAST.STAMPA="S" AND this.w_AGGPLADIS=.F.
                this.w_PLAFINI = this.w_INIPLA
                this.w_AGGPLADIS = .T.
              endif
                select _Curs_TMPPNT_MAST
                continue
              enddo
              use
            endif
            * --- Drop temporary table TMPPNT_MAST
            i_nIdx=cp_GetTableDefIdx('TMPPNT_MAST')
            if i_nIdx<>0
              cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
              cp_RemoveTableDef('TMPPNT_MAST')
            endif
          else
            * --- Se l'utente ha impostato come periodo di stampa il primo mese il plafond
            *     iniziale viene letto direttamente dalla tabella "Dati Annuali IVA"
            if this.oParentObject.w_MESEINI = 1
              * --- Leggo il dato impostato dall'utente nei Dati Iva
              * --- Read from DAT_IVAN
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.DAT_IVAN_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DAT_IVAN_idx,2],.t.,this.DAT_IVAN_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "IAPLAINI"+;
                  " from "+i_cTable+" DAT_IVAN where ";
                      +"IACODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODATT);
                      +" and IA__ANNO = "+cp_ToStrODBC(this.oParentObject.w_ANNO);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  IAPLAINI;
                  from (i_cTable) where;
                      IACODAZI = this.oParentObject.w_CODATT;
                      and IA__ANNO = this.oParentObject.w_ANNO;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_PLAFINI = NVL(cp_ToDate(_read_.IAPLAINI),cp_NullValue(_read_.IAPLAINI))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            else
              * --- Select from PLA_IVAN
              i_nConn=i_TableProp[this.PLA_IVAN_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PLA_IVAN_idx,2],.t.,this.PLA_IVAN_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select Sum ( DIPLAUTI ) As Somma  from "+i_cTable+" PLA_IVAN ";
                    +" where DICODAZI="+cp_ToStrODBC(this.oParentObject.w_CODATT)+" AND DI__ANNO="+cp_ToStrODBC(this.oParentObject.w_ANNO)+" AND DIPERIOD<"+cp_ToStrODBC(this.oParentObject.w_MESEINI)+"";
                     ,"_Curs_PLA_IVAN")
              else
                select Sum ( DIPLAUTI ) As Somma from (i_cTable);
                 where DICODAZI=this.oParentObject.w_CODATT AND DI__ANNO=this.oParentObject.w_ANNO AND DIPERIOD<this.oParentObject.w_MESEINI;
                  into cursor _Curs_PLA_IVAN
              endif
              if used('_Curs_PLA_IVAN')
                select _Curs_PLA_IVAN
                locate for 1=1
                do while not(eof())
                L_UTIPRE = Nvl( _Curs_PLA_IVAN.Somma , 0 ) 
                  select _Curs_PLA_IVAN
                  continue
                enddo
                use
              endif
              * --- Read from DAT_IVAN
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.DAT_IVAN_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DAT_IVAN_idx,2],.t.,this.DAT_IVAN_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "IAPLAINI"+;
                  " from "+i_cTable+" DAT_IVAN where ";
                      +"IACODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODATT);
                      +" and IA__ANNO = "+cp_ToStrODBC(this.oParentObject.w_ANNO);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  IAPLAINI;
                  from (i_cTable) where;
                      IACODAZI = this.oParentObject.w_CODATT;
                      and IA__ANNO = this.oParentObject.w_ANNO;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_INIPLA = NVL(cp_ToDate(_read_.IAPLAINI),cp_NullValue(_read_.IAPLAINI))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              * --- Se esistono movimenti da stampare, verifico che non siano presenti 
              *     dentro al cursore temporaneo TMPPNT_IVA record con il campo "Stampa"
              *     valorizzato a 'N'. Queste movimenti devono essere decrementati dal 
              *     plafond iniziale
              * --- Select from TMPPNT_IVA
              i_nConn=i_TableProp[this.TMPPNT_IVA_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_IVA_idx,2],.t.,this.TMPPNT_IVA_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select Sum ( INTRA + IMPORTAZIONI ) As Utilizzi  from "+i_cTable+" TMPPNT_IVA ";
                    +" where STAMPA='N'";
                     ,"_Curs_TMPPNT_IVA")
              else
                select Sum ( INTRA + IMPORTAZIONI ) As Utilizzi from (i_cTable);
                 where STAMPA="N";
                  into cursor _Curs_TMPPNT_IVA
              endif
              if used('_Curs_TMPPNT_IVA')
                select _Curs_TMPPNT_IVA
                locate for 1=1
                do while not(eof())
                L_UTIPRE = L_UTIPRE + Nvl( _Curs_TMPPNT_IVA.Utilizzi , 0 ) 
                  select _Curs_TMPPNT_IVA
                  continue
                enddo
                use
              endif
              this.w_PLAFINI = this.w_INIPLA - L_UTIPRE
              if this.w_PLAFINI<0
                AH_ERRORMSG("Attenzione: plafond in negativo",48)
              endif
            endif
          endif
          L_PLARES = this.w_PLAFINI 
 L_ANNO = this.oParentObject.w_ANNO 
 L_DESCRI = this.oParentObject.w_DESCRI 
 L_MESEINI = this.oParentObject.w_MESEINI 
 L_MESEFIN = this.oParentObject.w_MESEFIN 
 L_DESCRI = this.oParentObject.w_DESCRI 
 L_DESCRIF = this.oParentObject.w_DESCRIF 
 L_PLAMOB = this.oParentObject.w_MOBPLA 
 L_ATTIVITA = this.oParentObject.w_CODATT 
 L_DESATT = this.oParentObject.w_DESATT
          vq_exec(ALLTRIM(this.oParentObject.w_OQRY),this,"__TMP__")
          * --- Drop temporary table TMPPNT_IVA
          i_nIdx=cp_GetTableDefIdx('TMPPNT_IVA')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMPPNT_IVA')
          endif
          * --- Lancio il report
          CP_CHPRN( ALLTRIM(this.oParentObject.w_OREP),"",this.oParentObject )
        else
          AH_ERRORMSG("Non esistono movimenti da stampare",48)
          * --- Drop temporary table TMPPNT_IVA
          i_nIdx=cp_GetTableDefIdx('TMPPNT_IVA')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMPPNT_IVA')
          endif
          i_retcode = 'stop'
          return
        endif
    endcase
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='ATTIDETT'
    this.cWorkTables[2]='AZIENDA'
    this.cWorkTables[3]='ESERCIZI'
    this.cWorkTables[4]='*TMPPNT_IVA'
    this.cWorkTables[5]='*TMPPNT_MAST'
    this.cWorkTables[6]='DAT_IVAN'
    this.cWorkTables[7]='PLA_IVAN'
    return(this.OpenAllTables(7))

  proc CloseCursors()
    if used('_Curs_PLA_IVAN')
      use in _Curs_PLA_IVAN
    endif
    if used('_Curs_PLA_IVAN')
      use in _Curs_PLA_IVAN
    endif
    if used('_Curs_TMPPNT_IVA')
      use in _Curs_TMPPNT_IVA
    endif
    if used('_Curs_TMPPNT_MAST')
      use in _Curs_TMPPNT_MAST
    endif
    if used('_Curs_GSCG12SVP')
      use in _Curs_GSCG12SVP
    endif
    if used('_Curs_PLA_IVAN')
      use in _Curs_PLA_IVAN
    endif
    if used('_Curs_TMPPNT_IVA')
      use in _Curs_TMPPNT_IVA
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
