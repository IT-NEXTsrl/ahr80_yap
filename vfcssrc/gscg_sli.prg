* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_sli                                                        *
*              Stampa dichiarazioni                                            *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1997-07-18                                                      *
* Last revis.: 2015-02-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_sli",oParentObject))

* --- Class definition
define class tgscg_sli as StdForm
  Top    = 50
  Left   = 100

  * --- Standard Properties
  Width  = 520
  Height = 212
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-02-23"
  HelpContextID=60152471
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=21

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  cPrg = "gscg_sli"
  cComment = "Stampa dichiarazioni"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CLIFOR = space(1)
  o_CLIFOR = space(1)
  w_NUMERO1 = 0
  w_SERINI = space(3)
  w_NUMERO2 = 0
  w_SERFIN = space(3)
  w_DATA1 = ctod('  /  /  ')
  o_DATA1 = ctod('  /  /  ')
  w_DATA2 = ctod('  /  /  ')
  w_CODICE = space(15)
  w_CODICE1 = space(15)
  w_TIPOSTAM = space(1)
  o_TIPOSTAM = space(1)
  w_TIPIVA = space(1)
  w_DESCRI = space(30)
  w_DESCRI1 = space(30)
  w_STATO01 = space(1)
  w_STATO02 = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_CODAZI = space(5)
  w_EMAIL = space(254)
  w_TELFAX = space(18)
  w_EMPEC = space(254)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_sliPag1","gscg_sli",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCLIFOR_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='CONTI'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSCG_BLI(this,2)
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gscg_sli
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CLIFOR=space(1)
      .w_NUMERO1=0
      .w_SERINI=space(3)
      .w_NUMERO2=0
      .w_SERFIN=space(3)
      .w_DATA1=ctod("  /  /  ")
      .w_DATA2=ctod("  /  /  ")
      .w_CODICE=space(15)
      .w_CODICE1=space(15)
      .w_TIPOSTAM=space(1)
      .w_TIPIVA=space(1)
      .w_DESCRI=space(30)
      .w_DESCRI1=space(30)
      .w_STATO01=space(1)
      .w_STATO02=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_CODAZI=space(5)
      .w_EMAIL=space(254)
      .w_TELFAX=space(18)
      .w_EMPEC=space(254)
        .w_CLIFOR = 'E'
          .DoRTCalc(2,7,.f.)
        .w_CODICE = IIF(.w_CLIFOR$'DE',Space(15),.w_CODICE)
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_CODICE))
          .link_1_8('Full')
        endif
        .w_CODICE1 = IIF(.w_CLIFOR$'DE',Space(15),.w_CODICE1)
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_CODICE1))
          .link_1_9('Full')
        endif
        .w_TIPOSTAM = 'P'
        .w_TIPIVA = 'S'
          .DoRTCalc(12,13,.f.)
        .w_STATO01 = iif( .w_TipoStam $ "PD", "N", "S")
        .w_STATO02 = iif( .w_TipoStam $ "PD", " ", "R")
        .w_OBTEST = .w_DATA1
          .DoRTCalc(17,17,.f.)
        .w_CODAZI = i_CODAZI
    endwith
    this.DoRTCalc(19,21,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,7,.t.)
        if .o_CLIFOR<>.w_CLIFOR
            .w_CODICE = IIF(.w_CLIFOR$'DE',Space(15),.w_CODICE)
          .link_1_8('Full')
        endif
        if .o_CLIFOR<>.w_CLIFOR
            .w_CODICE1 = IIF(.w_CLIFOR$'DE',Space(15),.w_CODICE1)
          .link_1_9('Full')
        endif
        .DoRTCalc(10,13,.t.)
        if .o_TIPOSTAM<>.w_TIPOSTAM
            .w_STATO01 = iif( .w_TipoStam $ "PD", "N", "S")
        endif
        if .o_TIPOSTAM<>.w_TIPOSTAM
            .w_STATO02 = iif( .w_TipoStam $ "PD", " ", "R")
        endif
        if .o_DATA1<>.w_DATA1
            .w_OBTEST = .w_DATA1
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(17,21,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODICE_1_8.enabled = this.oPgFrm.Page1.oPag.oCODICE_1_8.mCond()
    this.oPgFrm.Page1.oPag.oCODICE1_1_9.enabled = this.oPgFrm.Page1.oPag.oCODICE1_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODICE
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODICE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CLIFOR);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,AN_EMAIL,AN_EMPEC,ANTELFAX";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_CLIFOR;
                     ,'ANCODICE',trim(this.w_CODICE))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,AN_EMAIL,AN_EMPEC,ANTELFAX;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODICE)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODICE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CLIFOR);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,AN_EMAIL,AN_EMPEC,ANTELFAX";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODICE)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_CLIFOR);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,AN_EMAIL,AN_EMPEC,ANTELFAX;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODICE) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODICE_1_8'),i_cWhere,'GSAR_BZC',"Fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CLIFOR<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,AN_EMAIL,AN_EMPEC,ANTELFAX";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,AN_EMAIL,AN_EMPEC,ANTELFAX;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Intervallo di codici errato oppure codice obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,AN_EMAIL,AN_EMPEC,ANTELFAX";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_CLIFOR);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,AN_EMAIL,AN_EMPEC,ANTELFAX;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,AN_EMAIL,AN_EMPEC,ANTELFAX";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODICE);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CLIFOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_CLIFOR;
                       ,'ANCODICE',this.w_CODICE)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,AN_EMAIL,AN_EMPEC,ANTELFAX;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODICE = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRI = NVL(_Link_.ANDESCRI,space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_EMAIL = NVL(_Link_.AN_EMAIL,space(254))
      this.w_EMPEC = NVL(_Link_.AN_EMPEC,space(254))
      this.w_TELFAX = NVL(_Link_.ANTELFAX,space(18))
    else
      if i_cCtrl<>'Load'
        this.w_CODICE = space(15)
      endif
      this.w_DESCRI = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_EMAIL = space(254)
      this.w_EMPEC = space(254)
      this.w_TELFAX = space(18)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(empty(.w_CODICE1) or .w_CODICE<=.w_CODICE1) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Intervallo di codici errato oppure codice obsoleto")
        endif
        this.w_CODICE = space(15)
        this.w_DESCRI = space(30)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_EMAIL = space(254)
        this.w_EMPEC = space(254)
        this.w_TELFAX = space(18)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODICE1
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODICE1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODICE1)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CLIFOR);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_CLIFOR;
                     ,'ANCODICE',trim(this.w_CODICE1))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODICE1)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODICE1)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CLIFOR);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODICE1)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_CLIFOR);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODICE1) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODICE1_1_9'),i_cWhere,'GSAR_BZC',"Fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CLIFOR<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Intervallo di codici errato oppure codice obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_CLIFOR);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODICE1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODICE1);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CLIFOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_CLIFOR;
                       ,'ANCODICE',this.w_CODICE1)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODICE1 = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRI1 = NVL(_Link_.ANDESCRI,space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODICE1 = space(15)
      endif
      this.w_DESCRI1 = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(empty(.w_CODICE) or .w_CODICE<=.w_CODICE1) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Intervallo di codici errato oppure codice obsoleto")
        endif
        this.w_CODICE1 = space(15)
        this.w_DESCRI1 = space(30)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODICE1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCLIFOR_1_1.RadioValue()==this.w_CLIFOR)
      this.oPgFrm.Page1.oPag.oCLIFOR_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMERO1_1_2.value==this.w_NUMERO1)
      this.oPgFrm.Page1.oPag.oNUMERO1_1_2.value=this.w_NUMERO1
    endif
    if not(this.oPgFrm.Page1.oPag.oSERINI_1_3.value==this.w_SERINI)
      this.oPgFrm.Page1.oPag.oSERINI_1_3.value=this.w_SERINI
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMERO2_1_4.value==this.w_NUMERO2)
      this.oPgFrm.Page1.oPag.oNUMERO2_1_4.value=this.w_NUMERO2
    endif
    if not(this.oPgFrm.Page1.oPag.oSERFIN_1_5.value==this.w_SERFIN)
      this.oPgFrm.Page1.oPag.oSERFIN_1_5.value=this.w_SERFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA1_1_6.value==this.w_DATA1)
      this.oPgFrm.Page1.oPag.oDATA1_1_6.value=this.w_DATA1
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA2_1_7.value==this.w_DATA2)
      this.oPgFrm.Page1.oPag.oDATA2_1_7.value=this.w_DATA2
    endif
    if not(this.oPgFrm.Page1.oPag.oCODICE_1_8.value==this.w_CODICE)
      this.oPgFrm.Page1.oPag.oCODICE_1_8.value=this.w_CODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oCODICE1_1_9.value==this.w_CODICE1)
      this.oPgFrm.Page1.oPag.oCODICE1_1_9.value=this.w_CODICE1
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOSTAM_1_10.RadioValue()==this.w_TIPOSTAM)
      this.oPgFrm.Page1.oPag.oTIPOSTAM_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPIVA_1_11.RadioValue()==this.w_TIPIVA)
      this.oPgFrm.Page1.oPag.oTIPIVA_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_19.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_19.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI1_1_20.value==this.w_DESCRI1)
      this.oPgFrm.Page1.oPag.oDESCRI1_1_20.value=this.w_DESCRI1
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_NUMERO1<=.w_NUMERO2 OR EMPTY(.w_NUMERO2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMERO1_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("L'intervallo di numeri � vuoto")
          case   not(.w_NUMERO1<=.w_NUMERO2 OR EMPTY(.w_NUMERO1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMERO2_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("L'intervallo di numeri � vuoto")
          case   not(empty(.w_DATA2) OR .w_DATA1<=.w_DATA2)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATA1_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore di quella finale")
          case   not(empty(.w_DATA1) OR .w_DATA1<=.w_DATA2)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATA2_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore di quella finale")
          case   not((empty(.w_CODICE1) or .w_CODICE<=.w_CODICE1) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and (.w_CLIFOR='F')  and not(empty(.w_CODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODICE_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Intervallo di codici errato oppure codice obsoleto")
          case   not((empty(.w_CODICE) or .w_CODICE<=.w_CODICE1) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and (.w_CLIFOR='F')  and not(empty(.w_CODICE1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODICE1_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Intervallo di codici errato oppure codice obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CLIFOR = this.w_CLIFOR
    this.o_DATA1 = this.w_DATA1
    this.o_TIPOSTAM = this.w_TIPOSTAM
    return

enddefine

* --- Define pages as container
define class tgscg_sliPag1 as StdContainer
  Width  = 516
  height = 212
  stdWidth  = 516
  stdheight = 212
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oCLIFOR_1_1 as StdCombo with uid="TLPINFZVAQ",rtseq=1,rtrep=.f.,left=400,top=13,width=108,height=21;
    , ToolTipText = "Selezionare tipo dichiarazione";
    , HelpContextID = 181451558;
    , cFormVar="w_CLIFOR",RowSource=""+"Fornitori,"+"Dogana,"+"Entrambi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCLIFOR_1_1.RadioValue()
    return(iif(this.value =1,'F',;
    iif(this.value =2,'D',;
    iif(this.value =3,'E',;
    space(1)))))
  endfunc
  func oCLIFOR_1_1.GetRadio()
    this.Parent.oContained.w_CLIFOR = this.RadioValue()
    return .t.
  endfunc

  func oCLIFOR_1_1.SetRadio()
    this.Parent.oContained.w_CLIFOR=trim(this.Parent.oContained.w_CLIFOR)
    this.value = ;
      iif(this.Parent.oContained.w_CLIFOR=='F',1,;
      iif(this.Parent.oContained.w_CLIFOR=='D',2,;
      iif(this.Parent.oContained.w_CLIFOR=='E',3,;
      0)))
  endfunc

  func oCLIFOR_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CODICE)
        bRes2=.link_1_8('Full')
      endif
      if .not. empty(.w_CODICE1)
        bRes2=.link_1_9('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oNUMERO1_1_2 as StdField with uid="FCYDSGVJQB",rtseq=2,rtrep=.f.,;
    cFormVar = "w_NUMERO1", cQueryName = "NUMERO1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "L'intervallo di numeri � vuoto",;
    ToolTipText = "Numero lettera d'intento di inizio selezione",;
    HelpContextID = 134218966,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=88, Top=13, cSayPict='"999999"', cGetPict='"999999"'

  func oNUMERO1_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_NUMERO1<=.w_NUMERO2 OR EMPTY(.w_NUMERO2))
    endwith
    return bRes
  endfunc

  add object oSERINI_1_3 as StdField with uid="EFKHLLLAPE",rtseq=3,rtrep=.f.,;
    cFormVar = "w_SERINI", cQueryName = "SERINI",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 29639974,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=170, Top=13, InputMask=replicate('X',3)

  add object oNUMERO2_1_4 as StdField with uid="TGTYFINBLY",rtseq=4,rtrep=.f.,;
    cFormVar = "w_NUMERO2", cQueryName = "NUMERO2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "L'intervallo di numeri � vuoto",;
    ToolTipText = "Numero lettera d'intento di fine selezione",;
    HelpContextID = 134218966,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=88, Top=45, cSayPict='"999999"', cGetPict='"999999"'

  func oNUMERO2_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_NUMERO1<=.w_NUMERO2 OR EMPTY(.w_NUMERO1))
    endwith
    return bRes
  endfunc

  add object oSERFIN_1_5 as StdField with uid="GFPWBOWKHW",rtseq=5,rtrep=.f.,;
    cFormVar = "w_SERFIN", cQueryName = "SERFIN",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 108086566,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=170, Top=45, InputMask=replicate('X',3)

  add object oDATA1_1_6 as StdField with uid="RANWBBMHQF",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DATA1", cQueryName = "DATA1",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore di quella finale",;
    ToolTipText = "Dara lettere d'intento di inizio selezione",;
    HelpContextID = 116154422,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=276, Top=13

  func oDATA1_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_DATA2) OR .w_DATA1<=.w_DATA2)
    endwith
    return bRes
  endfunc

  add object oDATA2_1_7 as StdField with uid="PSGKJCXJTH",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DATA2", cQueryName = "DATA2",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore di quella finale",;
    ToolTipText = "Data lettera d'intento di fine selezione",;
    HelpContextID = 117202998,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=276, Top=45

  func oDATA2_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_DATA1) OR .w_DATA1<=.w_DATA2)
    endwith
    return bRes
  endfunc

  add object oCODICE_1_8 as StdField with uid="FXWQXDDFWB",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CODICE", cQueryName = "CODICE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Intervallo di codici errato oppure codice obsoleto",;
    ToolTipText = "Fornitore di inizio selezione",;
    HelpContextID = 219377190,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=88, Top=77, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_CLIFOR", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODICE"

  func oCODICE_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CLIFOR='F')
    endwith
   endif
  endfunc

  func oCODICE_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODICE_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODICE_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_CLIFOR)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_CLIFOR)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODICE_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Fornitori",'',this.parent.oContained
  endproc
  proc oCODICE_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_CLIFOR
     i_obj.w_ANCODICE=this.parent.oContained.w_CODICE
     i_obj.ecpSave()
  endproc

  add object oCODICE1_1_9 as StdField with uid="NEHFRMUGSK",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CODICE1", cQueryName = "CODICE1",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Intervallo di codici errato oppure codice obsoleto",;
    ToolTipText = "Fornitore di fine selezione",;
    HelpContextID = 219377190,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=88, Top=109, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_CLIFOR", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODICE1"

  func oCODICE1_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CLIFOR='F')
    endwith
   endif
  endfunc

  func oCODICE1_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODICE1_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODICE1_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_CLIFOR)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_CLIFOR)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODICE1_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Fornitori",'',this.parent.oContained
  endproc
  proc oCODICE1_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_CLIFOR
     i_obj.w_ANCODICE=this.parent.oContained.w_CODICE1
     i_obj.ecpSave()
  endproc


  add object oTIPOSTAM_1_10 as StdCombo with uid="ZGGYGCZZRP",rtseq=10,rtrep=.f.,left=88,top=141,width=133,height=21;
    , ToolTipText = "Tipo di stampa selezionata";
    , HelpContextID = 219818371;
    , cFormVar="w_TIPOSTAM",RowSource=""+"Prova,"+"Definitiva,"+"Ristampa", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOSTAM_1_10.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'D',;
    iif(this.value =3,'R',;
    space(1)))))
  endfunc
  func oTIPOSTAM_1_10.GetRadio()
    this.Parent.oContained.w_TIPOSTAM = this.RadioValue()
    return .t.
  endfunc

  func oTIPOSTAM_1_10.SetRadio()
    this.Parent.oContained.w_TIPOSTAM=trim(this.Parent.oContained.w_TIPOSTAM)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOSTAM=='P',1,;
      iif(this.Parent.oContained.w_TIPOSTAM=='D',2,;
      iif(this.Parent.oContained.w_TIPOSTAM=='R',3,;
      0)))
  endfunc


  add object oTIPIVA_1_11 as StdCombo with uid="LAYJJYFVLP",rtseq=11,rtrep=.f.,left=88,top=173,width=133,height=21;
    , ToolTipText = "Applicazione IVA selezionata";
    , HelpContextID = 172239158;
    , cFormVar="w_TIPIVA",RowSource=""+"No applicazione IVA,"+"Con IVA agevolata", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPIVA_1_11.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oTIPIVA_1_11.GetRadio()
    this.Parent.oContained.w_TIPIVA = this.RadioValue()
    return .t.
  endfunc

  func oTIPIVA_1_11.SetRadio()
    this.Parent.oContained.w_TIPIVA=trim(this.Parent.oContained.w_TIPIVA)
    this.value = ;
      iif(this.Parent.oContained.w_TIPIVA=='S',1,;
      iif(this.Parent.oContained.w_TIPIVA=='N',2,;
      0))
  endfunc


  add object oBtn_1_14 as StdButton with uid="DQVCSDQVKB",left=360, top=162, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la stampa";
    , HelpContextID = 151098390;
    , caption='M\<odello';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      with this.Parent.oContained
        GSCG_BLI(this.Parent.oContained,9)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_14.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_TIPIVA='S')
      endwith
    endif
  endfunc


  add object oBtn_1_15 as StdButton with uid="SKCOCBYETW",left=411, top=162, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la stampa";
    , HelpContextID = 151098390;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      with this.Parent.oContained
        GSCG_BLI(this.Parent.oContained,2)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_16 as StdButton with uid="LPBGBWZOOZ",left=462, top=162, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 151098390;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCRI_1_19 as StdField with uid="YHLDSZHYJN",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 33444918,;
   bGlobalFont=.t.,;
    Height=21, Width=269, Left=223, Top=77, InputMask=replicate('X',30)

  add object oDESCRI1_1_20 as StdField with uid="EYLKLGFXUP",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESCRI1", cQueryName = "DESCRI1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 33444918,;
   bGlobalFont=.t.,;
    Height=21, Width=269, Left=223, Top=109, InputMask=replicate('X',30)

  add object oStr_1_12 as StdString with uid="VRRKIERCIL",Visible=.t., Left=207, Top=13,;
    Alignment=1, Width=66, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="CWEJJFNYEI",Visible=.t., Left=211, Top=45,;
    Alignment=1, Width=62, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="ATCMVPYSGD",Visible=.t., Left=2, Top=13,;
    Alignment=1, Width=83, Height=15,;
    Caption="Da numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="IXXJOBDKAP",Visible=.t., Left=10, Top=45,;
    Alignment=1, Width=75, Height=15,;
    Caption="A numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="WKTQTPBISR",Visible=.t., Left=51, Top=77,;
    Alignment=1, Width=34, Height=15,;
    Caption="Da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="AVJXZVDGTQ",Visible=.t., Left=62, Top=109,;
    Alignment=1, Width=23, Height=15,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="UGPYOVRVXA",Visible=.t., Left=5, Top=141,;
    Alignment=1, Width=80, Height=15,;
    Caption="Tipo stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="RAEGLMHBFN",Visible=.t., Left=30, Top=173,;
    Alignment=1, Width=55, Height=15,;
    Caption="Tipo IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="GGAILYTBLJ",Visible=.t., Left=159, Top=16,;
    Alignment=0, Width=10, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="DPPBNNOMRH",Visible=.t., Left=159, Top=49,;
    Alignment=0, Width=10, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="XYGGWYCGZT",Visible=.t., Left=358, Top=13,;
    Alignment=1, Width=39, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_sli','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
