* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_apa                                                        *
*              Parametri gestione studio                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-01-11                                                      *
* Last revis.: 2014-11-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsma_apa")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsma_apa")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsma_apa")
  return

* --- Class definition
define class tgsma_apa as StdPCForm
  Width  = 937
  Height = 393+35
  Top    = 192
  Left   = 13
  cComment = "Parametri gestione studio"
  cPrg = "gsma_apa"
  HelpContextID=160070505
  add object cnt as tcgsma_apa
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsma_apa as PCContext
  w_PACODAZI = space(5)
  w_TIPCON = space(1)
  w_PADATINI = space(8)
  w_PADATFIN = space(8)
  w_PAELCIVA = space(254)
  w_PA_IRPEF = space(5)
  w_PACASPRE = space(5)
  w_PASPEGEN = space(5)
  w_PARIFACC = space(1)
  w_PAFLGAQV = space(1)
  w_PAFLGZER = space(1)
  w_PAFLDESP = space(1)
  w_DESIRP = space(60)
  w_DESCPA = space(35)
  w_DESGEN = space(35)
  w_PA_INDPW = space(254)
  w_PAPREACC = space(254)
  w_DESPRE = space(40)
  w_OBTEST = space(8)
  w_TIPOENTE = space(1)
  w_PAPREFOR = space(20)
  w_DESFOR = space(40)
  w_PACODIVA = space(5)
  w_PAESCANA = space(1)
  w_PASONFOR = space(1)
  w_PAFLMIMA = space(1)
  w_PAGENPRE = space(1)
  w_PAFLGMSK = space(1)
  w_PAFLCODO = space(1)
  w_PAVISPRE = space(1)
  w_PAFILDAT = space(1)
  w_PAGGPREC = 0
  w_PAFLTOTS = space(1)
  w_PACAUPRE = space(5)
  w_PANOEDES = space(1)
  w_PAPRESPE = space(20)
  w_PAPREANT = space(20)
  w_DESSPE = space(40)
  w_DESANT = space(40)
  w_DATOBSO = space(8)
  w_OBTEST = space(8)
  w_PATIPARC = space(1)
  w_PACLASSE = space(15)
  w_PATIPSEN = space(1)
  w_PAORDFIL = space(1)
  w_PAHIDARC = space(1)
  w_PAHIDSCA = space(1)
  w_PAHIDOPE = space(1)
  w_PAVERDOC = space(1)
  w_PADATZIP = space(1)
  w_PAF23PRE = space(20)
  w_DESF23PRE = space(40)
  w_DESF23SPE = space(40)
  w_PAF23SPE = space(20)
  w_PAFLGDIK = space(1)
  w_CODART1 = space(20)
  w_CODART2 = space(20)
  w_FLSPAN1 = space(1)
  w_FLSPAN2 = space(1)
  w_Errore = space(1)
  w_DESIVA = space(35)
  w_PACODPUN = space(5)
  w_PATIPUDI = space(20)
  w_PAPWBUDI = space(20)
  w_PAPWBSCA = space(20)
  w_PAPWBRUO = space(5)
  w_DESUDI = space(254)
  w_PADATPOL = space(8)
  w_DESPWBUDI = space(254)
  w_DESPWBSCA = space(254)
  w_PRINDIRI = space(254)
  w_PRDESCRI = space(80)
  w_TIPPOLU = space(1)
  w_PADMCOMP = space(1)
  w_PANEWQRY = space(1)
  w_PAFLWDTP = space(1)
  w_PANOARCH = space(1)
  w_OLDGENPRE = space(1)
  w_PA_INDDE = space(254)
  w_PAESCNOM = space(1)
  w_PACAUANT = space(5)
  w_PACONANT = space(15)
  w_DESCON = space(60)
  w_PACONDEF = space(15)
  w_DEFANT = space(60)
  w_DESCAU = space(35)
  w_PACONSPE = space(15)
  w_DECSPE = space(60)
  w_PADEFSPE = space(15)
  w_DEFSPE = space(60)
  w_DESCLA = space(50)
  w_MODARC = space(1)
  w_CLAPRA = space(1)
  w_PAPATMOD = space(254)
  w_PAQUERY = space(254)
  w_QUERY = space(60)
  w_DIR_INST = space(50)
  w_CNSELDIR = space(254)
  w_PADATARC = space(1)
  w_PA_INVIO = space(1)
  proc Save(oFrom)
    this.w_PACODAZI = oFrom.w_PACODAZI
    this.w_TIPCON = oFrom.w_TIPCON
    this.w_PADATINI = oFrom.w_PADATINI
    this.w_PADATFIN = oFrom.w_PADATFIN
    this.w_PAELCIVA = oFrom.w_PAELCIVA
    this.w_PA_IRPEF = oFrom.w_PA_IRPEF
    this.w_PACASPRE = oFrom.w_PACASPRE
    this.w_PASPEGEN = oFrom.w_PASPEGEN
    this.w_PARIFACC = oFrom.w_PARIFACC
    this.w_PAFLGAQV = oFrom.w_PAFLGAQV
    this.w_PAFLGZER = oFrom.w_PAFLGZER
    this.w_PAFLDESP = oFrom.w_PAFLDESP
    this.w_DESIRP = oFrom.w_DESIRP
    this.w_DESCPA = oFrom.w_DESCPA
    this.w_DESGEN = oFrom.w_DESGEN
    this.w_PA_INDPW = oFrom.w_PA_INDPW
    this.w_PAPREACC = oFrom.w_PAPREACC
    this.w_DESPRE = oFrom.w_DESPRE
    this.w_OBTEST = oFrom.w_OBTEST
    this.w_TIPOENTE = oFrom.w_TIPOENTE
    this.w_PAPREFOR = oFrom.w_PAPREFOR
    this.w_DESFOR = oFrom.w_DESFOR
    this.w_PACODIVA = oFrom.w_PACODIVA
    this.w_PAESCANA = oFrom.w_PAESCANA
    this.w_PASONFOR = oFrom.w_PASONFOR
    this.w_PAFLMIMA = oFrom.w_PAFLMIMA
    this.w_PAGENPRE = oFrom.w_PAGENPRE
    this.w_PAFLGMSK = oFrom.w_PAFLGMSK
    this.w_PAFLCODO = oFrom.w_PAFLCODO
    this.w_PAVISPRE = oFrom.w_PAVISPRE
    this.w_PAFILDAT = oFrom.w_PAFILDAT
    this.w_PAGGPREC = oFrom.w_PAGGPREC
    this.w_PAFLTOTS = oFrom.w_PAFLTOTS
    this.w_PACAUPRE = oFrom.w_PACAUPRE
    this.w_PANOEDES = oFrom.w_PANOEDES
    this.w_PAPRESPE = oFrom.w_PAPRESPE
    this.w_PAPREANT = oFrom.w_PAPREANT
    this.w_DESSPE = oFrom.w_DESSPE
    this.w_DESANT = oFrom.w_DESANT
    this.w_DATOBSO = oFrom.w_DATOBSO
    this.w_OBTEST = oFrom.w_OBTEST
    this.w_PATIPARC = oFrom.w_PATIPARC
    this.w_PACLASSE = oFrom.w_PACLASSE
    this.w_PATIPSEN = oFrom.w_PATIPSEN
    this.w_PAORDFIL = oFrom.w_PAORDFIL
    this.w_PAHIDARC = oFrom.w_PAHIDARC
    this.w_PAHIDSCA = oFrom.w_PAHIDSCA
    this.w_PAHIDOPE = oFrom.w_PAHIDOPE
    this.w_PAVERDOC = oFrom.w_PAVERDOC
    this.w_PADATZIP = oFrom.w_PADATZIP
    this.w_PAF23PRE = oFrom.w_PAF23PRE
    this.w_DESF23PRE = oFrom.w_DESF23PRE
    this.w_DESF23SPE = oFrom.w_DESF23SPE
    this.w_PAF23SPE = oFrom.w_PAF23SPE
    this.w_PAFLGDIK = oFrom.w_PAFLGDIK
    this.w_CODART1 = oFrom.w_CODART1
    this.w_CODART2 = oFrom.w_CODART2
    this.w_FLSPAN1 = oFrom.w_FLSPAN1
    this.w_FLSPAN2 = oFrom.w_FLSPAN2
    this.w_Errore = oFrom.w_Errore
    this.w_DESIVA = oFrom.w_DESIVA
    this.w_PACODPUN = oFrom.w_PACODPUN
    this.w_PATIPUDI = oFrom.w_PATIPUDI
    this.w_PAPWBUDI = oFrom.w_PAPWBUDI
    this.w_PAPWBSCA = oFrom.w_PAPWBSCA
    this.w_PAPWBRUO = oFrom.w_PAPWBRUO
    this.w_DESUDI = oFrom.w_DESUDI
    this.w_PADATPOL = oFrom.w_PADATPOL
    this.w_DESPWBUDI = oFrom.w_DESPWBUDI
    this.w_DESPWBSCA = oFrom.w_DESPWBSCA
    this.w_PRINDIRI = oFrom.w_PRINDIRI
    this.w_PRDESCRI = oFrom.w_PRDESCRI
    this.w_TIPPOLU = oFrom.w_TIPPOLU
    this.w_PADMCOMP = oFrom.w_PADMCOMP
    this.w_PANEWQRY = oFrom.w_PANEWQRY
    this.w_PAFLWDTP = oFrom.w_PAFLWDTP
    this.w_PANOARCH = oFrom.w_PANOARCH
    this.w_OLDGENPRE = oFrom.w_OLDGENPRE
    this.w_PA_INDDE = oFrom.w_PA_INDDE
    this.w_PAESCNOM = oFrom.w_PAESCNOM
    this.w_PACAUANT = oFrom.w_PACAUANT
    this.w_PACONANT = oFrom.w_PACONANT
    this.w_DESCON = oFrom.w_DESCON
    this.w_PACONDEF = oFrom.w_PACONDEF
    this.w_DEFANT = oFrom.w_DEFANT
    this.w_DESCAU = oFrom.w_DESCAU
    this.w_PACONSPE = oFrom.w_PACONSPE
    this.w_DECSPE = oFrom.w_DECSPE
    this.w_PADEFSPE = oFrom.w_PADEFSPE
    this.w_DEFSPE = oFrom.w_DEFSPE
    this.w_DESCLA = oFrom.w_DESCLA
    this.w_MODARC = oFrom.w_MODARC
    this.w_CLAPRA = oFrom.w_CLAPRA
    this.w_PAPATMOD = oFrom.w_PAPATMOD
    this.w_PAQUERY = oFrom.w_PAQUERY
    this.w_QUERY = oFrom.w_QUERY
    this.w_DIR_INST = oFrom.w_DIR_INST
    this.w_CNSELDIR = oFrom.w_CNSELDIR
    this.w_PADATARC = oFrom.w_PADATARC
    this.w_PA_INVIO = oFrom.w_PA_INVIO
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_PACODAZI = this.w_PACODAZI
    oTo.w_TIPCON = this.w_TIPCON
    oTo.w_PADATINI = this.w_PADATINI
    oTo.w_PADATFIN = this.w_PADATFIN
    oTo.w_PAELCIVA = this.w_PAELCIVA
    oTo.w_PA_IRPEF = this.w_PA_IRPEF
    oTo.w_PACASPRE = this.w_PACASPRE
    oTo.w_PASPEGEN = this.w_PASPEGEN
    oTo.w_PARIFACC = this.w_PARIFACC
    oTo.w_PAFLGAQV = this.w_PAFLGAQV
    oTo.w_PAFLGZER = this.w_PAFLGZER
    oTo.w_PAFLDESP = this.w_PAFLDESP
    oTo.w_DESIRP = this.w_DESIRP
    oTo.w_DESCPA = this.w_DESCPA
    oTo.w_DESGEN = this.w_DESGEN
    oTo.w_PA_INDPW = this.w_PA_INDPW
    oTo.w_PAPREACC = this.w_PAPREACC
    oTo.w_DESPRE = this.w_DESPRE
    oTo.w_OBTEST = this.w_OBTEST
    oTo.w_TIPOENTE = this.w_TIPOENTE
    oTo.w_PAPREFOR = this.w_PAPREFOR
    oTo.w_DESFOR = this.w_DESFOR
    oTo.w_PACODIVA = this.w_PACODIVA
    oTo.w_PAESCANA = this.w_PAESCANA
    oTo.w_PASONFOR = this.w_PASONFOR
    oTo.w_PAFLMIMA = this.w_PAFLMIMA
    oTo.w_PAGENPRE = this.w_PAGENPRE
    oTo.w_PAFLGMSK = this.w_PAFLGMSK
    oTo.w_PAFLCODO = this.w_PAFLCODO
    oTo.w_PAVISPRE = this.w_PAVISPRE
    oTo.w_PAFILDAT = this.w_PAFILDAT
    oTo.w_PAGGPREC = this.w_PAGGPREC
    oTo.w_PAFLTOTS = this.w_PAFLTOTS
    oTo.w_PACAUPRE = this.w_PACAUPRE
    oTo.w_PANOEDES = this.w_PANOEDES
    oTo.w_PAPRESPE = this.w_PAPRESPE
    oTo.w_PAPREANT = this.w_PAPREANT
    oTo.w_DESSPE = this.w_DESSPE
    oTo.w_DESANT = this.w_DESANT
    oTo.w_DATOBSO = this.w_DATOBSO
    oTo.w_OBTEST = this.w_OBTEST
    oTo.w_PATIPARC = this.w_PATIPARC
    oTo.w_PACLASSE = this.w_PACLASSE
    oTo.w_PATIPSEN = this.w_PATIPSEN
    oTo.w_PAORDFIL = this.w_PAORDFIL
    oTo.w_PAHIDARC = this.w_PAHIDARC
    oTo.w_PAHIDSCA = this.w_PAHIDSCA
    oTo.w_PAHIDOPE = this.w_PAHIDOPE
    oTo.w_PAVERDOC = this.w_PAVERDOC
    oTo.w_PADATZIP = this.w_PADATZIP
    oTo.w_PAF23PRE = this.w_PAF23PRE
    oTo.w_DESF23PRE = this.w_DESF23PRE
    oTo.w_DESF23SPE = this.w_DESF23SPE
    oTo.w_PAF23SPE = this.w_PAF23SPE
    oTo.w_PAFLGDIK = this.w_PAFLGDIK
    oTo.w_CODART1 = this.w_CODART1
    oTo.w_CODART2 = this.w_CODART2
    oTo.w_FLSPAN1 = this.w_FLSPAN1
    oTo.w_FLSPAN2 = this.w_FLSPAN2
    oTo.w_Errore = this.w_Errore
    oTo.w_DESIVA = this.w_DESIVA
    oTo.w_PACODPUN = this.w_PACODPUN
    oTo.w_PATIPUDI = this.w_PATIPUDI
    oTo.w_PAPWBUDI = this.w_PAPWBUDI
    oTo.w_PAPWBSCA = this.w_PAPWBSCA
    oTo.w_PAPWBRUO = this.w_PAPWBRUO
    oTo.w_DESUDI = this.w_DESUDI
    oTo.w_PADATPOL = this.w_PADATPOL
    oTo.w_DESPWBUDI = this.w_DESPWBUDI
    oTo.w_DESPWBSCA = this.w_DESPWBSCA
    oTo.w_PRINDIRI = this.w_PRINDIRI
    oTo.w_PRDESCRI = this.w_PRDESCRI
    oTo.w_TIPPOLU = this.w_TIPPOLU
    oTo.w_PADMCOMP = this.w_PADMCOMP
    oTo.w_PANEWQRY = this.w_PANEWQRY
    oTo.w_PAFLWDTP = this.w_PAFLWDTP
    oTo.w_PANOARCH = this.w_PANOARCH
    oTo.w_OLDGENPRE = this.w_OLDGENPRE
    oTo.w_PA_INDDE = this.w_PA_INDDE
    oTo.w_PAESCNOM = this.w_PAESCNOM
    oTo.w_PACAUANT = this.w_PACAUANT
    oTo.w_PACONANT = this.w_PACONANT
    oTo.w_DESCON = this.w_DESCON
    oTo.w_PACONDEF = this.w_PACONDEF
    oTo.w_DEFANT = this.w_DEFANT
    oTo.w_DESCAU = this.w_DESCAU
    oTo.w_PACONSPE = this.w_PACONSPE
    oTo.w_DECSPE = this.w_DECSPE
    oTo.w_PADEFSPE = this.w_PADEFSPE
    oTo.w_DEFSPE = this.w_DEFSPE
    oTo.w_DESCLA = this.w_DESCLA
    oTo.w_MODARC = this.w_MODARC
    oTo.w_CLAPRA = this.w_CLAPRA
    oTo.w_PAPATMOD = this.w_PAPATMOD
    oTo.w_PAQUERY = this.w_PAQUERY
    oTo.w_QUERY = this.w_QUERY
    oTo.w_DIR_INST = this.w_DIR_INST
    oTo.w_CNSELDIR = this.w_CNSELDIR
    oTo.w_PADATARC = this.w_PADATARC
    oTo.w_PA_INVIO = this.w_PA_INVIO
    PCContext::Load(oTo)
enddefine

define class tcgsma_apa as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 937
  Height = 393+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-11-10"
  HelpContextID=160070505
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=100

  * --- Constant Properties
  PAR_ALTE_IDX = 0
  TRI_BUTI_IDX = 0
  VOCIIVA_IDX = 0
  KEY_ARTI_IDX = 0
  CONTI_IDX = 0
  CAU_CONT_IDX = 0
  PUN_ACCE_IDX = 0
  ART_ICOL_IDX = 0
  CAUMATTI_IDX = 0
  RUO_SOGI_IDX = 0
  TIP_DOCU_IDX = 0
  PROMCLAS_IDX = 0
  cFile = "PAR_ALTE"
  cKeySelect = "PACODAZI"
  cKeyWhere  = "PACODAZI=this.w_PACODAZI"
  cKeyWhereODBC = '"PACODAZI="+cp_ToStrODBC(this.w_PACODAZI)';

  cKeyWhereODBCqualified = '"PAR_ALTE.PACODAZI="+cp_ToStrODBC(this.w_PACODAZI)';

  cPrg = "gsma_apa"
  cComment = "Parametri gestione studio"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PACODAZI = space(5)
  w_TIPCON = space(1)
  w_PADATINI = ctod('  /  /  ')
  w_PADATFIN = ctod('  /  /  ')
  w_PAELCIVA = space(254)
  w_PA_IRPEF = space(5)
  w_PACASPRE = space(5)
  w_PASPEGEN = space(5)
  w_PARIFACC = space(1)
  w_PAFLGAQV = space(1)
  w_PAFLGZER = space(1)
  w_PAFLDESP = space(1)
  w_DESIRP = space(60)
  w_DESCPA = space(35)
  w_DESGEN = space(35)
  w_PA_INDPW = space(254)
  w_PAPREACC = space(254)
  o_PAPREACC = space(254)
  w_DESPRE = space(40)
  w_OBTEST = ctod('  /  /  ')
  w_TIPOENTE = space(1)
  w_PAPREFOR = space(20)
  o_PAPREFOR = space(20)
  w_DESFOR = space(40)
  w_PACODIVA = space(5)
  w_PAESCANA = space(1)
  w_PASONFOR = space(1)
  w_PAFLMIMA = space(1)
  w_PAGENPRE = space(1)
  o_PAGENPRE = space(1)
  w_PAFLGMSK = space(1)
  w_PAFLCODO = space(1)
  w_PAVISPRE = space(1)
  w_PAFILDAT = space(1)
  o_PAFILDAT = space(1)
  w_PAGGPREC = 0
  w_PAFLTOTS = space(1)
  w_PACAUPRE = space(5)
  w_PANOEDES = space(1)
  w_PAPRESPE = space(20)
  w_PAPREANT = space(20)
  w_DESSPE = space(40)
  w_DESANT = space(40)
  w_DATOBSO = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_PATIPARC = space(1)
  w_PACLASSE = space(15)
  o_PACLASSE = space(15)
  w_PATIPSEN = space(1)
  w_PAORDFIL = space(1)
  w_PAHIDARC = space(1)
  w_PAHIDSCA = space(1)
  w_PAHIDOPE = space(1)
  w_PAVERDOC = space(1)
  w_PADATZIP = space(1)
  w_PAF23PRE = space(20)
  w_DESF23PRE = space(40)
  w_DESF23SPE = space(40)
  w_PAF23SPE = space(20)
  w_PAFLGDIK = space(1)
  w_CODART1 = space(20)
  w_CODART2 = space(20)
  w_FLSPAN1 = space(1)
  w_FLSPAN2 = space(1)
  w_Errore = .F.
  w_DESIVA = space(35)
  w_PACODPUN = space(5)
  w_PATIPUDI = space(20)
  w_PAPWBUDI = space(20)
  w_PAPWBSCA = space(20)
  w_PAPWBRUO = space(5)
  w_DESUDI = space(254)
  w_PADATPOL = ctod('  /  /  ')
  w_DESPWBUDI = space(254)
  w_DESPWBSCA = space(254)
  w_PRINDIRI = space(254)
  w_PRDESCRI = space(80)
  w_TIPPOLU = space(1)
  w_PADMCOMP = space(1)
  w_PANEWQRY = space(1)
  w_PAFLWDTP = space(1)
  w_PANOARCH = space(1)
  w_OLDGENPRE = space(1)
  w_PA_INDDE = space(254)
  w_PAESCNOM = space(1)
  w_PACAUANT = space(5)
  w_PACONANT = space(15)
  w_DESCON = space(60)
  w_PACONDEF = space(15)
  w_DEFANT = space(60)
  w_DESCAU = space(35)
  w_PACONSPE = space(15)
  w_DECSPE = space(60)
  w_PADEFSPE = space(15)
  w_DEFSPE = space(60)
  w_DESCLA = space(50)
  w_MODARC = space(1)
  w_CLAPRA = space(1)
  w_PAPATMOD = space(254)
  w_PAQUERY = space(254)
  w_QUERY = space(60)
  o_QUERY = space(60)
  w_DIR_INST = space(50)
  w_CNSELDIR = space(254)
  w_PADATARC = space(1)
  w_PA_INVIO = space(1)

  * --- Children pointers
  gsal_mpa = .NULL.
  gsal_mca = .NULL.
  w_SelQue = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=4, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_apaPag1","gsma_apa",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Generale")
      .Pages(1).HelpContextID = 16658741
      .Pages(2).addobject("oPag","tgsma_apaPag2","gsma_apa",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Contabilizzazione")
      .Pages(2).HelpContextID = 267780128
      .Pages(3).addobject("oPag","tgsma_apaPag3","gsma_apa",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Document management")
      .Pages(3).HelpContextID = 263365529
      .Pages(4).addobject("oPag","tgsma_apaPag4","gsma_apa",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Pct")
      .Pages(4).HelpContextID = 159568650
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPADATINI_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
    proc Init()
      this.w_SelQue = this.oPgFrm.Pages(3).oPag.SelQue
      DoDefault()
    proc Destroy()
      this.w_SelQue = .NULL.
      DoDefault()

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[12]
    this.cWorkTables[1]='TRI_BUTI'
    this.cWorkTables[2]='VOCIIVA'
    this.cWorkTables[3]='KEY_ARTI'
    this.cWorkTables[4]='CONTI'
    this.cWorkTables[5]='CAU_CONT'
    this.cWorkTables[6]='PUN_ACCE'
    this.cWorkTables[7]='ART_ICOL'
    this.cWorkTables[8]='CAUMATTI'
    this.cWorkTables[9]='RUO_SOGI'
    this.cWorkTables[10]='TIP_DOCU'
    this.cWorkTables[11]='PROMCLAS'
    this.cWorkTables[12]='PAR_ALTE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(12))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PAR_ALTE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PAR_ALTE_IDX,3]
  return

  function CreateChildren()
    this.gsal_mpa = CREATEOBJECT('stdLazyChild',this,'gsal_mpa')
    this.gsal_mca = CREATEOBJECT('stdLazyChild',this,'gsal_mca')
    return

  procedure NewContext()
    return(createobject('tsgsma_apa'))

  function DestroyChildrenChain()
    this.oParentObject=.NULL.
    if !ISNULL(this.gsal_mpa)
      this.gsal_mpa.DestroyChildrenChain()
    endif
    if !ISNULL(this.gsal_mca)
      this.gsal_mca.DestroyChildrenChain()
    endif
    return

  function HideChildrenChain()
    *this.Hide()
    this.bOnScreen = .f.
    this.gsal_mpa.HideChildrenChain()
    this.gsal_mca.HideChildrenChain()
    return

  procedure DestroyChildren()
    if !ISNULL(this.gsal_mpa)
      this.gsal_mpa.DestroyChildrenChain()
      this.gsal_mpa=.NULL.
    endif
    if !ISNULL(this.gsal_mca)
      this.gsal_mca.DestroyChildrenChain()
      this.gsal_mca=.NULL.
    endif
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.gsal_mpa.IsAChildUpdated()
      i_bRes = i_bRes .or. this.gsal_mca.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.gsal_mpa.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.gsal_mca.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.gsal_mpa.NewDocument()
    this.gsal_mca.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.gsal_mpa.SetKey(;
            .w_PACODAZI,"PACODAZI";
            )
      this.gsal_mca.SetKey(;
            .w_PACODAZI,"PACODAZI";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .gsal_mpa.ChangeRow(this.cRowID+'      1',1;
             ,.w_PACODAZI,"PACODAZI";
             )
      .gsal_mca.ChangeRow(this.cRowID+'      1',1;
             ,.w_PACODAZI,"PACODAZI";
             )
    endwith
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_8_joined
    link_1_8_joined=.f.
    local link_1_9_joined
    link_1_9_joined=.f.
    local link_1_10_joined
    link_1_10_joined=.f.
    local link_1_23_joined
    link_1_23_joined=.f.
    local link_1_28_joined
    link_1_28_joined=.f.
    local link_1_30_joined
    link_1_30_joined=.f.
    local link_1_44_joined
    link_1_44_joined=.f.
    local link_1_45_joined
    link_1_45_joined=.f.
    local link_3_2_joined
    link_3_2_joined=.f.
    local link_1_52_joined
    link_1_52_joined=.f.
    local link_1_57_joined
    link_1_57_joined=.f.
    local link_4_1_joined
    link_4_1_joined=.f.
    local link_4_6_joined
    link_4_6_joined=.f.
    local link_4_7_joined
    link_4_7_joined=.f.
    local link_4_8_joined
    link_4_8_joined=.f.
    local link_2_1_joined
    link_2_1_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from PAR_ALTE where PACODAZI=KeySet.PACODAZI
    *
    i_nConn = i_TableProp[this.PAR_ALTE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PAR_ALTE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PAR_ALTE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PAR_ALTE '
      link_1_8_joined=this.AddJoinedLink_1_8(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_9_joined=this.AddJoinedLink_1_9(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_10_joined=this.AddJoinedLink_1_10(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_23_joined=this.AddJoinedLink_1_23(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_28_joined=this.AddJoinedLink_1_28(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_30_joined=this.AddJoinedLink_1_30(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_44_joined=this.AddJoinedLink_1_44(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_45_joined=this.AddJoinedLink_1_45(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_2_joined=this.AddJoinedLink_3_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_52_joined=this.AddJoinedLink_1_52(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_57_joined=this.AddJoinedLink_1_57(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_1_joined=this.AddJoinedLink_4_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_6_joined=this.AddJoinedLink_4_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_7_joined=this.AddJoinedLink_4_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_8_joined=this.AddJoinedLink_4_8(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PACODAZI',this.w_PACODAZI  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESIRP = space(60)
        .w_DESCPA = space(35)
        .w_DESGEN = space(35)
        .w_DESPRE = space(40)
        .w_OBTEST = i_datsys
        .w_TIPOENTE = ' '
        .w_DESFOR = space(40)
        .w_DESSPE = space(40)
        .w_DESANT = space(40)
        .w_DATOBSO = ctod("  /  /  ")
        .w_DESF23PRE = space(40)
        .w_DESF23SPE = space(40)
        .w_CODART1 = space(20)
        .w_CODART2 = space(20)
        .w_FLSPAN1 = space(1)
        .w_FLSPAN2 = space(1)
        .w_Errore = .f.
        .w_DESIVA = space(35)
        .w_DESUDI = space(254)
        .w_DESPWBUDI = space(254)
        .w_DESPWBSCA = space(254)
        .w_PRINDIRI = space(254)
        .w_PRDESCRI = space(80)
        .w_TIPPOLU = space(1)
        .w_OLDGENPRE = .w_PAGENPRE
        .w_DESCON = space(60)
        .w_DEFANT = space(60)
        .w_DESCAU = space(35)
        .w_DECSPE = space(60)
        .w_DEFSPE = space(60)
        .w_DESCLA = space(50)
        .w_MODARC = space(1)
        .w_CLAPRA = space(1)
        .w_QUERY = space(60)
        .w_DIR_INST = sys(5) + sys(2003) + "\"
        .w_CNSELDIR = space(254)
        .w_PACODAZI = NVL(PACODAZI,space(5))
        .w_TIPCON = 'G'
        .w_PADATINI = NVL(cp_ToDate(PADATINI),ctod("  /  /  "))
        .w_PADATFIN = NVL(cp_ToDate(PADATFIN),ctod("  /  /  "))
        .w_PAELCIVA = NVL(PAELCIVA,space(254))
        .w_PA_IRPEF = NVL(PA_IRPEF,space(5))
          if link_1_8_joined
            this.w_PA_IRPEF = NVL(TRCODTRI108,NVL(this.w_PA_IRPEF,space(5)))
            this.w_DESIRP = NVL(TRDESTRI108,space(60))
          else
          .link_1_8('Load')
          endif
        .w_PACASPRE = NVL(PACASPRE,space(5))
          if link_1_9_joined
            this.w_PACASPRE = NVL(IVCODIVA109,NVL(this.w_PACASPRE,space(5)))
            this.w_DESCPA = NVL(IVDESIVA109,space(35))
          else
          .link_1_9('Load')
          endif
        .w_PASPEGEN = NVL(PASPEGEN,space(5))
          if link_1_10_joined
            this.w_PASPEGEN = NVL(IVCODIVA110,NVL(this.w_PASPEGEN,space(5)))
            this.w_DESGEN = NVL(IVDESIVA110,space(35))
          else
          .link_1_10('Load')
          endif
        .w_PARIFACC = NVL(PARIFACC,space(1))
        .w_PAFLGAQV = NVL(PAFLGAQV,space(1))
        .w_PAFLGZER = NVL(PAFLGZER,space(1))
        .w_PAFLDESP = NVL(PAFLDESP,space(1))
        .w_PA_INDPW = NVL(PA_INDPW,space(254))
        .w_PAPREACC = NVL(PAPREACC,space(254))
          if link_1_23_joined
            this.w_PAPREACC = NVL(CACODICE123,NVL(this.w_PAPREACC,space(254)))
            this.w_DESPRE = NVL(CADESART123,space(40))
            this.w_CODART1 = NVL(CACODART123,space(20))
          else
          .link_1_23('Load')
          endif
        .w_PAPREFOR = NVL(PAPREFOR,space(20))
          if link_1_28_joined
            this.w_PAPREFOR = NVL(CACODICE128,NVL(this.w_PAPREFOR,space(20)))
            this.w_DESFOR = NVL(CADESART128,space(40))
            this.w_CODART2 = NVL(CACODART128,space(20))
          else
          .link_1_28('Load')
          endif
        .w_PACODIVA = NVL(PACODIVA,space(5))
          if link_1_30_joined
            this.w_PACODIVA = NVL(IVCODIVA130,NVL(this.w_PACODIVA,space(5)))
            this.w_DESIVA = NVL(IVDESIVA130,space(35))
            this.w_DATOBSO = NVL(cp_ToDate(IVDTOBSO130),ctod("  /  /  "))
          else
          .link_1_30('Load')
          endif
        .w_PAESCANA = NVL(PAESCANA,space(1))
        .w_PASONFOR = NVL(PASONFOR,space(1))
        .w_PAFLMIMA = NVL(PAFLMIMA,space(1))
        .w_PAGENPRE = NVL(PAGENPRE,space(1))
        .w_PAFLGMSK = NVL(PAFLGMSK,space(1))
        .w_PAFLCODO = NVL(PAFLCODO,space(1))
        .w_PAVISPRE = NVL(PAVISPRE,space(1))
        .w_PAFILDAT = NVL(PAFILDAT,space(1))
        .w_PAGGPREC = NVL(PAGGPREC,0)
        .w_PAFLTOTS = NVL(PAFLTOTS,space(1))
        .w_PACAUPRE = NVL(PACAUPRE,space(5))
          * evitabile
          *.link_1_41('Load')
        .w_PANOEDES = NVL(PANOEDES,space(1))
        .w_PAPRESPE = NVL(PAPRESPE,space(20))
          if link_1_44_joined
            this.w_PAPRESPE = NVL(CACODICE144,NVL(this.w_PAPRESPE,space(20)))
            this.w_DESSPE = NVL(CADESART144,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(CADTOBSO144),ctod("  /  /  "))
          else
          .link_1_44('Load')
          endif
        .w_PAPREANT = NVL(PAPREANT,space(20))
          if link_1_45_joined
            this.w_PAPREANT = NVL(CACODICE145,NVL(this.w_PAPREANT,space(20)))
            this.w_DESANT = NVL(CADESART145,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(CADTOBSO145),ctod("  /  /  "))
          else
          .link_1_45('Load')
          endif
        .w_OBTEST = i_datsys
        .w_PATIPARC = NVL(PATIPARC,space(1))
        .w_PACLASSE = NVL(PACLASSE,space(15))
          if link_3_2_joined
            this.w_PACLASSE = NVL(CDCODCLA302,NVL(this.w_PACLASSE,space(15)))
            this.w_DESCLA = NVL(CDDESCLA302,space(50))
            this.w_MODARC = NVL(CDMODALL302,space(1))
            this.w_CLAPRA = NVL(CDCLAPRA302,space(1))
          else
          .link_3_2('Load')
          endif
        .w_PATIPSEN = NVL(PATIPSEN,space(1))
        .w_PAORDFIL = NVL(PAORDFIL,space(1))
        .w_PAHIDARC = NVL(PAHIDARC,space(1))
        .w_PAHIDSCA = NVL(PAHIDSCA,space(1))
        .w_PAHIDOPE = NVL(PAHIDOPE,space(1))
        .w_PAVERDOC = NVL(PAVERDOC,space(1))
        .w_PADATZIP = NVL(PADATZIP,space(1))
        .w_PAF23PRE = NVL(PAF23PRE,space(20))
          if link_1_52_joined
            this.w_PAF23PRE = NVL(CACODICE152,NVL(this.w_PAF23PRE,space(20)))
            this.w_DESF23PRE = NVL(CADESART152,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(CADTOBSO152),ctod("  /  /  "))
          else
          .link_1_52('Load')
          endif
        .w_PAF23SPE = NVL(PAF23SPE,space(20))
          if link_1_57_joined
            this.w_PAF23SPE = NVL(CACODICE157,NVL(this.w_PAF23SPE,space(20)))
            this.w_DESF23SPE = NVL(CADESART157,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(CADTOBSO157),ctod("  /  /  "))
          else
          .link_1_57('Load')
          endif
        .w_PAFLGDIK = NVL(PAFLGDIK,space(1))
          .link_1_58('Load')
          .link_1_59('Load')
        .w_PACODPUN = NVL(PACODPUN,space(5))
          if link_4_1_joined
            this.w_PACODPUN = NVL(PRCODICE401,NVL(this.w_PACODPUN,space(5)))
            this.w_PRDESCRI = NVL(PRDESCRI401,space(80))
            this.w_PRINDIRI = NVL(PRINDIRI401,space(254))
          else
          .link_4_1('Load')
          endif
        .w_PATIPUDI = NVL(PATIPUDI,space(20))
          if link_4_6_joined
            this.w_PATIPUDI = NVL(CACODICE406,NVL(this.w_PATIPUDI,space(20)))
            this.w_DESUDI = NVL(CADESCRI406,space(254))
            this.w_TIPPOLU = NVL(CARAGGST406,space(1))
          else
          .link_4_6('Load')
          endif
        .w_PAPWBUDI = NVL(PAPWBUDI,space(20))
          if link_4_7_joined
            this.w_PAPWBUDI = NVL(CACODICE407,NVL(this.w_PAPWBUDI,space(20)))
            this.w_DESPWBUDI = NVL(CADESCRI407,space(254))
          else
          .link_4_7('Load')
          endif
        .w_PAPWBSCA = NVL(PAPWBSCA,space(20))
          if link_4_8_joined
            this.w_PAPWBSCA = NVL(CACODICE408,NVL(this.w_PAPWBSCA,space(20)))
            this.w_DESPWBSCA = NVL(CADESCRI408,space(254))
          else
          .link_4_8('Load')
          endif
        .w_PAPWBRUO = NVL(PAPWBRUO,space(5))
        .w_PADATPOL = NVL(cp_ToDate(PADATPOL),ctod("  /  /  "))
        .w_PADMCOMP = NVL(PADMCOMP,space(1))
        .w_PANEWQRY = NVL(PANEWQRY,space(1))
        .w_PAFLWDTP = NVL(PAFLWDTP,space(1))
        .w_PANOARCH = NVL(PANOARCH,space(1))
        .w_PA_INDDE = NVL(PA_INDDE,space(254))
        .w_PAESCNOM = NVL(PAESCNOM,space(1))
        .w_PACAUANT = NVL(PACAUANT,space(5))
          if link_2_1_joined
            this.w_PACAUANT = NVL(CCCODICE201,NVL(this.w_PACAUANT,space(5)))
            this.w_DESCAU = NVL(CCDESCRI201,space(35))
          else
          .link_2_1('Load')
          endif
        .w_PACONANT = NVL(PACONANT,space(15))
          .link_2_2('Load')
        .w_PACONDEF = NVL(PACONDEF,space(15))
          .link_2_5('Load')
        .w_PACONSPE = NVL(PACONSPE,space(15))
          .link_2_10('Load')
        .w_PADEFSPE = NVL(PADEFSPE,space(15))
          .link_2_13('Load')
        .w_PAPATMOD = NVL(PAPATMOD,space(254))
        .w_PAQUERY = NVL(PAQUERY,space(254))
        .oPgFrm.Page3.oPag.SelQue.Calculate()
        .w_PADATARC = NVL(PADATARC,space(1))
        .w_PA_INVIO = NVL(PA_INVIO,space(1))
        cp_LoadRecExtFlds(this,'PAR_ALTE')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_PACODAZI = space(5)
      .w_TIPCON = space(1)
      .w_PADATINI = ctod("  /  /  ")
      .w_PADATFIN = ctod("  /  /  ")
      .w_PAELCIVA = space(254)
      .w_PA_IRPEF = space(5)
      .w_PACASPRE = space(5)
      .w_PASPEGEN = space(5)
      .w_PARIFACC = space(1)
      .w_PAFLGAQV = space(1)
      .w_PAFLGZER = space(1)
      .w_PAFLDESP = space(1)
      .w_DESIRP = space(60)
      .w_DESCPA = space(35)
      .w_DESGEN = space(35)
      .w_PA_INDPW = space(254)
      .w_PAPREACC = space(254)
      .w_DESPRE = space(40)
      .w_OBTEST = ctod("  /  /  ")
      .w_TIPOENTE = space(1)
      .w_PAPREFOR = space(20)
      .w_DESFOR = space(40)
      .w_PACODIVA = space(5)
      .w_PAESCANA = space(1)
      .w_PASONFOR = space(1)
      .w_PAFLMIMA = space(1)
      .w_PAGENPRE = space(1)
      .w_PAFLGMSK = space(1)
      .w_PAFLCODO = space(1)
      .w_PAVISPRE = space(1)
      .w_PAFILDAT = space(1)
      .w_PAGGPREC = 0
      .w_PAFLTOTS = space(1)
      .w_PACAUPRE = space(5)
      .w_PANOEDES = space(1)
      .w_PAPRESPE = space(20)
      .w_PAPREANT = space(20)
      .w_DESSPE = space(40)
      .w_DESANT = space(40)
      .w_DATOBSO = ctod("  /  /  ")
      .w_OBTEST = ctod("  /  /  ")
      .w_PATIPARC = space(1)
      .w_PACLASSE = space(15)
      .w_PATIPSEN = space(1)
      .w_PAORDFIL = space(1)
      .w_PAHIDARC = space(1)
      .w_PAHIDSCA = space(1)
      .w_PAHIDOPE = space(1)
      .w_PAVERDOC = space(1)
      .w_PADATZIP = space(1)
      .w_PAF23PRE = space(20)
      .w_DESF23PRE = space(40)
      .w_DESF23SPE = space(40)
      .w_PAF23SPE = space(20)
      .w_PAFLGDIK = space(1)
      .w_CODART1 = space(20)
      .w_CODART2 = space(20)
      .w_FLSPAN1 = space(1)
      .w_FLSPAN2 = space(1)
      .w_Errore = .f.
      .w_DESIVA = space(35)
      .w_PACODPUN = space(5)
      .w_PATIPUDI = space(20)
      .w_PAPWBUDI = space(20)
      .w_PAPWBSCA = space(20)
      .w_PAPWBRUO = space(5)
      .w_DESUDI = space(254)
      .w_PADATPOL = ctod("  /  /  ")
      .w_DESPWBUDI = space(254)
      .w_DESPWBSCA = space(254)
      .w_PRINDIRI = space(254)
      .w_PRDESCRI = space(80)
      .w_TIPPOLU = space(1)
      .w_PADMCOMP = space(1)
      .w_PANEWQRY = space(1)
      .w_PAFLWDTP = space(1)
      .w_PANOARCH = space(1)
      .w_OLDGENPRE = space(1)
      .w_PA_INDDE = space(254)
      .w_PAESCNOM = space(1)
      .w_PACAUANT = space(5)
      .w_PACONANT = space(15)
      .w_DESCON = space(60)
      .w_PACONDEF = space(15)
      .w_DEFANT = space(60)
      .w_DESCAU = space(35)
      .w_PACONSPE = space(15)
      .w_DECSPE = space(60)
      .w_PADEFSPE = space(15)
      .w_DEFSPE = space(60)
      .w_DESCLA = space(50)
      .w_MODARC = space(1)
      .w_CLAPRA = space(1)
      .w_PAPATMOD = space(254)
      .w_PAQUERY = space(254)
      .w_QUERY = space(60)
      .w_DIR_INST = space(50)
      .w_CNSELDIR = space(254)
      .w_PADATARC = space(1)
      .w_PA_INVIO = space(1)
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_TIPCON = 'G'
        .DoRTCalc(3,6,.f.)
          if not(empty(.w_PA_IRPEF))
          .link_1_8('Full')
          endif
        .DoRTCalc(7,7,.f.)
          if not(empty(.w_PACASPRE))
          .link_1_9('Full')
          endif
        .DoRTCalc(8,8,.f.)
          if not(empty(.w_PASPEGEN))
          .link_1_10('Full')
          endif
          .DoRTCalc(9,9,.f.)
        .w_PAFLGAQV = 'N'
        .DoRTCalc(11,17,.f.)
          if not(empty(.w_PAPREACC))
          .link_1_23('Full')
          endif
          .DoRTCalc(18,18,.f.)
        .w_OBTEST = i_datsys
        .w_TIPOENTE = ' '
        .DoRTCalc(21,21,.f.)
          if not(empty(.w_PAPREFOR))
          .link_1_28('Full')
          endif
        .DoRTCalc(22,23,.f.)
          if not(empty(.w_PACODIVA))
          .link_1_30('Full')
          endif
        .w_PAESCANA = 'N'
        .w_PASONFOR = 'N'
          .DoRTCalc(26,31,.f.)
        .w_PAGGPREC = IIF(.w_PAFILDAT <> 'G', 0, .w_PAGGPREC)
        .DoRTCalc(33,34,.f.)
          if not(empty(.w_PACAUPRE))
          .link_1_41('Full')
          endif
        .DoRTCalc(35,36,.f.)
          if not(empty(.w_PAPRESPE))
          .link_1_44('Full')
          endif
        .DoRTCalc(37,37,.f.)
          if not(empty(.w_PAPREANT))
          .link_1_45('Full')
          endif
          .DoRTCalc(38,40,.f.)
        .w_OBTEST = i_datsys
        .w_PATIPARC = 'L'
        .DoRTCalc(43,43,.f.)
          if not(empty(.w_PACLASSE))
          .link_3_2('Full')
          endif
        .w_PATIPSEN = 'F'
        .w_PAORDFIL = 'F'
          .DoRTCalc(46,48,.f.)
        .w_PAVERDOC = 'S'
        .w_PADATZIP = 'S'
        .DoRTCalc(51,51,.f.)
          if not(empty(.w_PAF23PRE))
          .link_1_52('Full')
          endif
        .DoRTCalc(52,54,.f.)
          if not(empty(.w_PAF23SPE))
          .link_1_57('Full')
          endif
        .DoRTCalc(55,56,.f.)
          if not(empty(.w_CODART1))
          .link_1_58('Full')
          endif
        .DoRTCalc(57,57,.f.)
          if not(empty(.w_CODART2))
          .link_1_59('Full')
          endif
        .DoRTCalc(58,62,.f.)
          if not(empty(.w_PACODPUN))
          .link_4_1('Full')
          endif
        .DoRTCalc(63,63,.f.)
          if not(empty(.w_PATIPUDI))
          .link_4_6('Full')
          endif
        .DoRTCalc(64,64,.f.)
          if not(empty(.w_PAPWBUDI))
          .link_4_7('Full')
          endif
        .DoRTCalc(65,65,.f.)
          if not(empty(.w_PAPWBSCA))
          .link_4_8('Full')
          endif
          .DoRTCalc(66,76,.f.)
        .w_PANOARCH = 'N'
        .w_OLDGENPRE = .w_PAGENPRE
          .DoRTCalc(79,79,.f.)
        .w_PAESCNOM = 'N'
        .DoRTCalc(81,81,.f.)
          if not(empty(.w_PACAUANT))
          .link_2_1('Full')
          endif
        .DoRTCalc(82,82,.f.)
          if not(empty(.w_PACONANT))
          .link_2_2('Full')
          endif
        .DoRTCalc(83,84,.f.)
          if not(empty(.w_PACONDEF))
          .link_2_5('Full')
          endif
        .DoRTCalc(85,87,.f.)
          if not(empty(.w_PACONSPE))
          .link_2_10('Full')
          endif
        .DoRTCalc(88,89,.f.)
          if not(empty(.w_PADEFSPE))
          .link_2_13('Full')
          endif
          .DoRTCalc(90,94,.f.)
        .w_PAQUERY = strtran(.w_QUERY, .w_DIR_INST, "")
        .oPgFrm.Page3.oPag.SelQue.Calculate()
          .DoRTCalc(96,96,.f.)
        .w_DIR_INST = sys(5) + sys(2003) + "\"
          .DoRTCalc(98,99,.f.)
        .w_PA_INVIO = 'P'
      endif
    endwith
    cp_BlankRecExtFlds(this,'PAR_ALTE')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oPADATINI_1_3.enabled = i_bVal
      .Page1.oPag.oPADATFIN_1_5.enabled = i_bVal
      .Page1.oPag.oPAELCIVA_1_6.enabled = i_bVal
      .Page1.oPag.oPA_IRPEF_1_8.enabled = i_bVal
      .Page1.oPag.oPACASPRE_1_9.enabled = i_bVal
      .Page1.oPag.oPASPEGEN_1_10.enabled = i_bVal
      .Page1.oPag.oPARIFACC_1_11.enabled = i_bVal
      .Page1.oPag.oPAFLGAQV_1_12.enabled = i_bVal
      .Page1.oPag.oPAFLGZER_1_13.enabled = i_bVal
      .Page1.oPag.oPAFLDESP_1_14.enabled = i_bVal
      .Page1.oPag.oPAPREACC_1_23.enabled = i_bVal
      .Page1.oPag.oPAPREFOR_1_28.enabled = i_bVal
      .Page1.oPag.oPACODIVA_1_30.enabled = i_bVal
      .Page1.oPag.oPAESCANA_1_31.enabled = i_bVal
      .Page1.oPag.oPASONFOR_1_32.enabled = i_bVal
      .Page1.oPag.oPAFLMIMA_1_33.enabled = i_bVal
      .Page1.oPag.oPAGENPRE_1_34.enabled = i_bVal
      .Page1.oPag.oPAFLGMSK_1_35.enabled = i_bVal
      .Page1.oPag.oPAFLCODO_1_36.enabled = i_bVal
      .Page1.oPag.oPAVISPRE_1_37.enabled = i_bVal
      .Page1.oPag.oPAFILDAT_1_38.enabled = i_bVal
      .Page1.oPag.oPAGGPREC_1_39.enabled = i_bVal
      .Page1.oPag.oPAFLTOTS_1_40.enabled = i_bVal
      .Page1.oPag.oPACAUPRE_1_41.enabled = i_bVal
      .Page1.oPag.oPANOEDES_1_42.enabled = i_bVal
      .Page1.oPag.oPAPRESPE_1_44.enabled = i_bVal
      .Page1.oPag.oPAPREANT_1_45.enabled = i_bVal
      .Page3.oPag.oPATIPARC_3_1.enabled = i_bVal
      .Page3.oPag.oPACLASSE_3_2.enabled = i_bVal
      .Page3.oPag.oPATIPSEN_3_3.enabled = i_bVal
      .Page3.oPag.oPAORDFIL_3_4.enabled = i_bVal
      .Page3.oPag.oPAHIDARC_3_5.enabled = i_bVal
      .Page3.oPag.oPAHIDSCA_3_6.enabled = i_bVal
      .Page3.oPag.oPAHIDOPE_3_7.enabled = i_bVal
      .Page3.oPag.oPAVERDOC_3_10.enabled = i_bVal
      .Page3.oPag.oPADATZIP_3_11.enabled = i_bVal
      .Page1.oPag.oPAF23PRE_1_52.enabled = i_bVal
      .Page1.oPag.oPAF23SPE_1_57.enabled = i_bVal
      .Page3.oPag.oPAFLGDIK_3_12.enabled = i_bVal
      .Page4.oPag.oPACODPUN_4_1.enabled = i_bVal
      .Page4.oPag.oPATIPUDI_4_6.enabled = i_bVal
      .Page4.oPag.oPAPWBUDI_4_7.enabled = i_bVal
      .Page4.oPag.oPAPWBSCA_4_8.enabled = i_bVal
      .Page4.oPag.oPAPWBRUO_4_9.enabled = i_bVal
      .Page4.oPag.oPADATPOL_4_11.enabled = i_bVal
      .Page3.oPag.oPADMCOMP_3_13.enabled = i_bVal
      .Page3.oPag.oPANEWQRY_3_14.enabled = i_bVal
      .Page3.oPag.oPAFLWDTP_3_15.enabled = i_bVal
      .Page4.oPag.oPANOARCH_4_21.enabled = i_bVal
      .Page4.oPag.oPAESCNOM_4_23.enabled = i_bVal
      .Page2.oPag.oPACAUANT_2_1.enabled = i_bVal
      .Page2.oPag.oPACONANT_2_2.enabled = i_bVal
      .Page2.oPag.oPACONDEF_2_5.enabled = i_bVal
      .Page2.oPag.oPACONSPE_2_10.enabled = i_bVal
      .Page2.oPag.oPADEFSPE_2_13.enabled = i_bVal
      .Page3.oPag.oPADATARC_3_36.enabled = i_bVal
      .Page4.oPag.oPA_INVIO_4_24.enabled = i_bVal
      .Page3.oPag.oBtn_3_30.enabled = i_bVal
      .Page3.oPag.SelQue.enabled = i_bVal
    endwith
    this.gsal_mpa.SetStatus(i_cOp)
    this.gsal_mca.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'PAR_ALTE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.gsal_mpa.SetChildrenStatus(i_cOp)
  *  this.gsal_mca.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PAR_ALTE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACODAZI,"PACODAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PADATINI,"PADATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PADATFIN,"PADATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAELCIVA,"PAELCIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PA_IRPEF,"PA_IRPEF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACASPRE,"PACASPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PASPEGEN,"PASPEGEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PARIFACC,"PARIFACC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLGAQV,"PAFLGAQV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLGZER,"PAFLGZER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLDESP,"PAFLDESP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PA_INDPW,"PA_INDPW",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAPREACC,"PAPREACC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAPREFOR,"PAPREFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACODIVA,"PACODIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAESCANA,"PAESCANA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PASONFOR,"PASONFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLMIMA,"PAFLMIMA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAGENPRE,"PAGENPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLGMSK,"PAFLGMSK",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLCODO,"PAFLCODO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAVISPRE,"PAVISPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFILDAT,"PAFILDAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAGGPREC,"PAGGPREC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLTOTS,"PAFLTOTS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACAUPRE,"PACAUPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PANOEDES,"PANOEDES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAPRESPE,"PAPRESPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAPREANT,"PAPREANT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PATIPARC,"PATIPARC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACLASSE,"PACLASSE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PATIPSEN,"PATIPSEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAORDFIL,"PAORDFIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAHIDARC,"PAHIDARC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAHIDSCA,"PAHIDSCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAHIDOPE,"PAHIDOPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAVERDOC,"PAVERDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PADATZIP,"PADATZIP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAF23PRE,"PAF23PRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAF23SPE,"PAF23SPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLGDIK,"PAFLGDIK",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACODPUN,"PACODPUN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PATIPUDI,"PATIPUDI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAPWBUDI,"PAPWBUDI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAPWBSCA,"PAPWBSCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAPWBRUO,"PAPWBRUO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PADATPOL,"PADATPOL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PADMCOMP,"PADMCOMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PANEWQRY,"PANEWQRY",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLWDTP,"PAFLWDTP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PANOARCH,"PANOARCH",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PA_INDDE,"PA_INDDE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAESCNOM,"PAESCNOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACAUANT,"PACAUANT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACONANT,"PACONANT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACONDEF,"PACONDEF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACONSPE,"PACONSPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PADEFSPE,"PADEFSPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAPATMOD,"PAPATMOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAQUERY,"PAQUERY",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PADATARC,"PADATARC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PA_INVIO,"PA_INVIO",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PAR_ALTE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.PAR_ALTE_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into PAR_ALTE
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PAR_ALTE')
        i_extval=cp_InsertValODBCExtFlds(this,'PAR_ALTE')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(PACODAZI,PADATINI,PADATFIN,PAELCIVA,PA_IRPEF"+;
                  ",PACASPRE,PASPEGEN,PARIFACC,PAFLGAQV,PAFLGZER"+;
                  ",PAFLDESP,PA_INDPW,PAPREACC,PAPREFOR,PACODIVA"+;
                  ",PAESCANA,PASONFOR,PAFLMIMA,PAGENPRE,PAFLGMSK"+;
                  ",PAFLCODO,PAVISPRE,PAFILDAT,PAGGPREC,PAFLTOTS"+;
                  ",PACAUPRE,PANOEDES,PAPRESPE,PAPREANT,PATIPARC"+;
                  ",PACLASSE,PATIPSEN,PAORDFIL,PAHIDARC,PAHIDSCA"+;
                  ",PAHIDOPE,PAVERDOC,PADATZIP,PAF23PRE,PAF23SPE"+;
                  ",PAFLGDIK,PACODPUN,PATIPUDI,PAPWBUDI,PAPWBSCA"+;
                  ",PAPWBRUO,PADATPOL,PADMCOMP,PANEWQRY,PAFLWDTP"+;
                  ",PANOARCH,PA_INDDE,PAESCNOM,PACAUANT,PACONANT"+;
                  ",PACONDEF,PACONSPE,PADEFSPE,PAPATMOD,PAQUERY"+;
                  ",PADATARC,PA_INVIO "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_PACODAZI)+;
                  ","+cp_ToStrODBC(this.w_PADATINI)+;
                  ","+cp_ToStrODBC(this.w_PADATFIN)+;
                  ","+cp_ToStrODBC(this.w_PAELCIVA)+;
                  ","+cp_ToStrODBCNull(this.w_PA_IRPEF)+;
                  ","+cp_ToStrODBCNull(this.w_PACASPRE)+;
                  ","+cp_ToStrODBCNull(this.w_PASPEGEN)+;
                  ","+cp_ToStrODBC(this.w_PARIFACC)+;
                  ","+cp_ToStrODBC(this.w_PAFLGAQV)+;
                  ","+cp_ToStrODBC(this.w_PAFLGZER)+;
                  ","+cp_ToStrODBC(this.w_PAFLDESP)+;
                  ","+cp_ToStrODBC(this.w_PA_INDPW)+;
                  ","+cp_ToStrODBCNull(this.w_PAPREACC)+;
                  ","+cp_ToStrODBCNull(this.w_PAPREFOR)+;
                  ","+cp_ToStrODBCNull(this.w_PACODIVA)+;
                  ","+cp_ToStrODBC(this.w_PAESCANA)+;
                  ","+cp_ToStrODBC(this.w_PASONFOR)+;
                  ","+cp_ToStrODBC(this.w_PAFLMIMA)+;
                  ","+cp_ToStrODBC(this.w_PAGENPRE)+;
                  ","+cp_ToStrODBC(this.w_PAFLGMSK)+;
                  ","+cp_ToStrODBC(this.w_PAFLCODO)+;
                  ","+cp_ToStrODBC(this.w_PAVISPRE)+;
                  ","+cp_ToStrODBC(this.w_PAFILDAT)+;
                  ","+cp_ToStrODBC(this.w_PAGGPREC)+;
                  ","+cp_ToStrODBC(this.w_PAFLTOTS)+;
                  ","+cp_ToStrODBCNull(this.w_PACAUPRE)+;
                  ","+cp_ToStrODBC(this.w_PANOEDES)+;
                  ","+cp_ToStrODBCNull(this.w_PAPRESPE)+;
                  ","+cp_ToStrODBCNull(this.w_PAPREANT)+;
                  ","+cp_ToStrODBC(this.w_PATIPARC)+;
                  ","+cp_ToStrODBCNull(this.w_PACLASSE)+;
                  ","+cp_ToStrODBC(this.w_PATIPSEN)+;
                  ","+cp_ToStrODBC(this.w_PAORDFIL)+;
                  ","+cp_ToStrODBC(this.w_PAHIDARC)+;
                  ","+cp_ToStrODBC(this.w_PAHIDSCA)+;
                  ","+cp_ToStrODBC(this.w_PAHIDOPE)+;
                  ","+cp_ToStrODBC(this.w_PAVERDOC)+;
                  ","+cp_ToStrODBC(this.w_PADATZIP)+;
                  ","+cp_ToStrODBCNull(this.w_PAF23PRE)+;
                  ","+cp_ToStrODBCNull(this.w_PAF23SPE)+;
                  ","+cp_ToStrODBC(this.w_PAFLGDIK)+;
                  ","+cp_ToStrODBCNull(this.w_PACODPUN)+;
                  ","+cp_ToStrODBCNull(this.w_PATIPUDI)+;
                  ","+cp_ToStrODBCNull(this.w_PAPWBUDI)+;
                  ","+cp_ToStrODBCNull(this.w_PAPWBSCA)+;
                  ","+cp_ToStrODBC(this.w_PAPWBRUO)+;
                  ","+cp_ToStrODBC(this.w_PADATPOL)+;
                  ","+cp_ToStrODBC(this.w_PADMCOMP)+;
                  ","+cp_ToStrODBC(this.w_PANEWQRY)+;
                  ","+cp_ToStrODBC(this.w_PAFLWDTP)+;
                  ","+cp_ToStrODBC(this.w_PANOARCH)+;
                  ","+cp_ToStrODBC(this.w_PA_INDDE)+;
                  ","+cp_ToStrODBC(this.w_PAESCNOM)+;
                  ","+cp_ToStrODBCNull(this.w_PACAUANT)+;
                  ","+cp_ToStrODBCNull(this.w_PACONANT)+;
                  ","+cp_ToStrODBCNull(this.w_PACONDEF)+;
                  ","+cp_ToStrODBCNull(this.w_PACONSPE)+;
                  ","+cp_ToStrODBCNull(this.w_PADEFSPE)+;
                  ","+cp_ToStrODBC(this.w_PAPATMOD)+;
                  ","+cp_ToStrODBC(this.w_PAQUERY)+;
                  ","+cp_ToStrODBC(this.w_PADATARC)+;
                  ","+cp_ToStrODBC(this.w_PA_INVIO)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PAR_ALTE')
        i_extval=cp_InsertValVFPExtFlds(this,'PAR_ALTE')
        cp_CheckDeletedKey(i_cTable,0,'PACODAZI',this.w_PACODAZI)
        INSERT INTO (i_cTable);
              (PACODAZI,PADATINI,PADATFIN,PAELCIVA,PA_IRPEF,PACASPRE,PASPEGEN,PARIFACC,PAFLGAQV,PAFLGZER,PAFLDESP,PA_INDPW,PAPREACC,PAPREFOR,PACODIVA,PAESCANA,PASONFOR,PAFLMIMA,PAGENPRE,PAFLGMSK,PAFLCODO,PAVISPRE,PAFILDAT,PAGGPREC,PAFLTOTS,PACAUPRE,PANOEDES,PAPRESPE,PAPREANT,PATIPARC,PACLASSE,PATIPSEN,PAORDFIL,PAHIDARC,PAHIDSCA,PAHIDOPE,PAVERDOC,PADATZIP,PAF23PRE,PAF23SPE,PAFLGDIK,PACODPUN,PATIPUDI,PAPWBUDI,PAPWBSCA,PAPWBRUO,PADATPOL,PADMCOMP,PANEWQRY,PAFLWDTP,PANOARCH,PA_INDDE,PAESCNOM,PACAUANT,PACONANT,PACONDEF,PACONSPE,PADEFSPE,PAPATMOD,PAQUERY,PADATARC,PA_INVIO  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_PACODAZI;
                  ,this.w_PADATINI;
                  ,this.w_PADATFIN;
                  ,this.w_PAELCIVA;
                  ,this.w_PA_IRPEF;
                  ,this.w_PACASPRE;
                  ,this.w_PASPEGEN;
                  ,this.w_PARIFACC;
                  ,this.w_PAFLGAQV;
                  ,this.w_PAFLGZER;
                  ,this.w_PAFLDESP;
                  ,this.w_PA_INDPW;
                  ,this.w_PAPREACC;
                  ,this.w_PAPREFOR;
                  ,this.w_PACODIVA;
                  ,this.w_PAESCANA;
                  ,this.w_PASONFOR;
                  ,this.w_PAFLMIMA;
                  ,this.w_PAGENPRE;
                  ,this.w_PAFLGMSK;
                  ,this.w_PAFLCODO;
                  ,this.w_PAVISPRE;
                  ,this.w_PAFILDAT;
                  ,this.w_PAGGPREC;
                  ,this.w_PAFLTOTS;
                  ,this.w_PACAUPRE;
                  ,this.w_PANOEDES;
                  ,this.w_PAPRESPE;
                  ,this.w_PAPREANT;
                  ,this.w_PATIPARC;
                  ,this.w_PACLASSE;
                  ,this.w_PATIPSEN;
                  ,this.w_PAORDFIL;
                  ,this.w_PAHIDARC;
                  ,this.w_PAHIDSCA;
                  ,this.w_PAHIDOPE;
                  ,this.w_PAVERDOC;
                  ,this.w_PADATZIP;
                  ,this.w_PAF23PRE;
                  ,this.w_PAF23SPE;
                  ,this.w_PAFLGDIK;
                  ,this.w_PACODPUN;
                  ,this.w_PATIPUDI;
                  ,this.w_PAPWBUDI;
                  ,this.w_PAPWBSCA;
                  ,this.w_PAPWBRUO;
                  ,this.w_PADATPOL;
                  ,this.w_PADMCOMP;
                  ,this.w_PANEWQRY;
                  ,this.w_PAFLWDTP;
                  ,this.w_PANOARCH;
                  ,this.w_PA_INDDE;
                  ,this.w_PAESCNOM;
                  ,this.w_PACAUANT;
                  ,this.w_PACONANT;
                  ,this.w_PACONDEF;
                  ,this.w_PACONSPE;
                  ,this.w_PADEFSPE;
                  ,this.w_PAPATMOD;
                  ,this.w_PAQUERY;
                  ,this.w_PADATARC;
                  ,this.w_PA_INVIO;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.PAR_ALTE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.PAR_ALTE_IDX,i_nConn)
      *
      * update PAR_ALTE
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'PAR_ALTE')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " PADATINI="+cp_ToStrODBC(this.w_PADATINI)+;
             ",PADATFIN="+cp_ToStrODBC(this.w_PADATFIN)+;
             ",PAELCIVA="+cp_ToStrODBC(this.w_PAELCIVA)+;
             ",PA_IRPEF="+cp_ToStrODBCNull(this.w_PA_IRPEF)+;
             ",PACASPRE="+cp_ToStrODBCNull(this.w_PACASPRE)+;
             ",PASPEGEN="+cp_ToStrODBCNull(this.w_PASPEGEN)+;
             ",PARIFACC="+cp_ToStrODBC(this.w_PARIFACC)+;
             ",PAFLGAQV="+cp_ToStrODBC(this.w_PAFLGAQV)+;
             ",PAFLGZER="+cp_ToStrODBC(this.w_PAFLGZER)+;
             ",PAFLDESP="+cp_ToStrODBC(this.w_PAFLDESP)+;
             ",PA_INDPW="+cp_ToStrODBC(this.w_PA_INDPW)+;
             ",PAPREACC="+cp_ToStrODBCNull(this.w_PAPREACC)+;
             ",PAPREFOR="+cp_ToStrODBCNull(this.w_PAPREFOR)+;
             ",PACODIVA="+cp_ToStrODBCNull(this.w_PACODIVA)+;
             ",PAESCANA="+cp_ToStrODBC(this.w_PAESCANA)+;
             ",PASONFOR="+cp_ToStrODBC(this.w_PASONFOR)+;
             ",PAFLMIMA="+cp_ToStrODBC(this.w_PAFLMIMA)+;
             ",PAGENPRE="+cp_ToStrODBC(this.w_PAGENPRE)+;
             ",PAFLGMSK="+cp_ToStrODBC(this.w_PAFLGMSK)+;
             ",PAFLCODO="+cp_ToStrODBC(this.w_PAFLCODO)+;
             ",PAVISPRE="+cp_ToStrODBC(this.w_PAVISPRE)+;
             ",PAFILDAT="+cp_ToStrODBC(this.w_PAFILDAT)+;
             ",PAGGPREC="+cp_ToStrODBC(this.w_PAGGPREC)+;
             ",PAFLTOTS="+cp_ToStrODBC(this.w_PAFLTOTS)+;
             ",PACAUPRE="+cp_ToStrODBCNull(this.w_PACAUPRE)+;
             ",PANOEDES="+cp_ToStrODBC(this.w_PANOEDES)+;
             ",PAPRESPE="+cp_ToStrODBCNull(this.w_PAPRESPE)+;
             ",PAPREANT="+cp_ToStrODBCNull(this.w_PAPREANT)+;
             ",PATIPARC="+cp_ToStrODBC(this.w_PATIPARC)+;
             ",PACLASSE="+cp_ToStrODBCNull(this.w_PACLASSE)+;
             ",PATIPSEN="+cp_ToStrODBC(this.w_PATIPSEN)+;
             ",PAORDFIL="+cp_ToStrODBC(this.w_PAORDFIL)+;
             ",PAHIDARC="+cp_ToStrODBC(this.w_PAHIDARC)+;
             ",PAHIDSCA="+cp_ToStrODBC(this.w_PAHIDSCA)+;
             ",PAHIDOPE="+cp_ToStrODBC(this.w_PAHIDOPE)+;
             ",PAVERDOC="+cp_ToStrODBC(this.w_PAVERDOC)+;
             ",PADATZIP="+cp_ToStrODBC(this.w_PADATZIP)+;
             ",PAF23PRE="+cp_ToStrODBCNull(this.w_PAF23PRE)+;
             ",PAF23SPE="+cp_ToStrODBCNull(this.w_PAF23SPE)+;
             ",PAFLGDIK="+cp_ToStrODBC(this.w_PAFLGDIK)+;
             ",PACODPUN="+cp_ToStrODBCNull(this.w_PACODPUN)+;
             ",PATIPUDI="+cp_ToStrODBCNull(this.w_PATIPUDI)+;
             ",PAPWBUDI="+cp_ToStrODBCNull(this.w_PAPWBUDI)+;
             ",PAPWBSCA="+cp_ToStrODBCNull(this.w_PAPWBSCA)+;
             ",PAPWBRUO="+cp_ToStrODBC(this.w_PAPWBRUO)+;
             ",PADATPOL="+cp_ToStrODBC(this.w_PADATPOL)+;
             ",PADMCOMP="+cp_ToStrODBC(this.w_PADMCOMP)+;
             ",PANEWQRY="+cp_ToStrODBC(this.w_PANEWQRY)+;
             ",PAFLWDTP="+cp_ToStrODBC(this.w_PAFLWDTP)+;
             ",PANOARCH="+cp_ToStrODBC(this.w_PANOARCH)+;
             ",PA_INDDE="+cp_ToStrODBC(this.w_PA_INDDE)+;
             ",PAESCNOM="+cp_ToStrODBC(this.w_PAESCNOM)+;
             ",PACAUANT="+cp_ToStrODBCNull(this.w_PACAUANT)+;
             ",PACONANT="+cp_ToStrODBCNull(this.w_PACONANT)+;
             ",PACONDEF="+cp_ToStrODBCNull(this.w_PACONDEF)+;
             ",PACONSPE="+cp_ToStrODBCNull(this.w_PACONSPE)+;
             ",PADEFSPE="+cp_ToStrODBCNull(this.w_PADEFSPE)+;
             ",PAPATMOD="+cp_ToStrODBC(this.w_PAPATMOD)+;
             ",PAQUERY="+cp_ToStrODBC(this.w_PAQUERY)+;
             ",PADATARC="+cp_ToStrODBC(this.w_PADATARC)+;
             ",PA_INVIO="+cp_ToStrODBC(this.w_PA_INVIO)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'PAR_ALTE')
        i_cWhere = cp_PKFox(i_cTable  ,'PACODAZI',this.w_PACODAZI  )
        UPDATE (i_cTable) SET;
              PADATINI=this.w_PADATINI;
             ,PADATFIN=this.w_PADATFIN;
             ,PAELCIVA=this.w_PAELCIVA;
             ,PA_IRPEF=this.w_PA_IRPEF;
             ,PACASPRE=this.w_PACASPRE;
             ,PASPEGEN=this.w_PASPEGEN;
             ,PARIFACC=this.w_PARIFACC;
             ,PAFLGAQV=this.w_PAFLGAQV;
             ,PAFLGZER=this.w_PAFLGZER;
             ,PAFLDESP=this.w_PAFLDESP;
             ,PA_INDPW=this.w_PA_INDPW;
             ,PAPREACC=this.w_PAPREACC;
             ,PAPREFOR=this.w_PAPREFOR;
             ,PACODIVA=this.w_PACODIVA;
             ,PAESCANA=this.w_PAESCANA;
             ,PASONFOR=this.w_PASONFOR;
             ,PAFLMIMA=this.w_PAFLMIMA;
             ,PAGENPRE=this.w_PAGENPRE;
             ,PAFLGMSK=this.w_PAFLGMSK;
             ,PAFLCODO=this.w_PAFLCODO;
             ,PAVISPRE=this.w_PAVISPRE;
             ,PAFILDAT=this.w_PAFILDAT;
             ,PAGGPREC=this.w_PAGGPREC;
             ,PAFLTOTS=this.w_PAFLTOTS;
             ,PACAUPRE=this.w_PACAUPRE;
             ,PANOEDES=this.w_PANOEDES;
             ,PAPRESPE=this.w_PAPRESPE;
             ,PAPREANT=this.w_PAPREANT;
             ,PATIPARC=this.w_PATIPARC;
             ,PACLASSE=this.w_PACLASSE;
             ,PATIPSEN=this.w_PATIPSEN;
             ,PAORDFIL=this.w_PAORDFIL;
             ,PAHIDARC=this.w_PAHIDARC;
             ,PAHIDSCA=this.w_PAHIDSCA;
             ,PAHIDOPE=this.w_PAHIDOPE;
             ,PAVERDOC=this.w_PAVERDOC;
             ,PADATZIP=this.w_PADATZIP;
             ,PAF23PRE=this.w_PAF23PRE;
             ,PAF23SPE=this.w_PAF23SPE;
             ,PAFLGDIK=this.w_PAFLGDIK;
             ,PACODPUN=this.w_PACODPUN;
             ,PATIPUDI=this.w_PATIPUDI;
             ,PAPWBUDI=this.w_PAPWBUDI;
             ,PAPWBSCA=this.w_PAPWBSCA;
             ,PAPWBRUO=this.w_PAPWBRUO;
             ,PADATPOL=this.w_PADATPOL;
             ,PADMCOMP=this.w_PADMCOMP;
             ,PANEWQRY=this.w_PANEWQRY;
             ,PAFLWDTP=this.w_PAFLWDTP;
             ,PANOARCH=this.w_PANOARCH;
             ,PA_INDDE=this.w_PA_INDDE;
             ,PAESCNOM=this.w_PAESCNOM;
             ,PACAUANT=this.w_PACAUANT;
             ,PACONANT=this.w_PACONANT;
             ,PACONDEF=this.w_PACONDEF;
             ,PACONSPE=this.w_PACONSPE;
             ,PADEFSPE=this.w_PADEFSPE;
             ,PAPATMOD=this.w_PAPATMOD;
             ,PAQUERY=this.w_PAQUERY;
             ,PADATARC=this.w_PADATARC;
             ,PA_INVIO=this.w_PA_INVIO;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- gsal_mpa : Saving
      this.gsal_mpa.ChangeRow(this.cRowID+'      1',0;
             ,this.w_PACODAZI,"PACODAZI";
             )
      this.gsal_mpa.mReplace()
      * --- gsal_mca : Saving
      this.gsal_mca.ChangeRow(this.cRowID+'      1',0;
             ,this.w_PACODAZI,"PACODAZI";
             )
      this.gsal_mca.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    * --- gsal_mpa : Deleting
    this.gsal_mpa.ChangeRow(this.cRowID+'      1',0;
           ,this.w_PACODAZI,"PACODAZI";
           )
    this.gsal_mpa.mDelete()
    * --- gsal_mca : Deleting
    this.gsal_mca.ChangeRow(this.cRowID+'      1',0;
           ,this.w_PACODAZI,"PACODAZI";
           )
    this.gsal_mca.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PAR_ALTE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.PAR_ALTE_IDX,i_nConn)
      *
      * delete PAR_ALTE
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'PACODAZI',this.w_PACODAZI  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PAR_ALTE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
            .w_TIPCON = 'G'
        .DoRTCalc(3,31,.t.)
        if .o_PAFILDAT<>.w_PAFILDAT
            .w_PAGGPREC = IIF(.w_PAFILDAT <> 'G', 0, .w_PAGGPREC)
        endif
        .DoRTCalc(33,40,.t.)
            .w_OBTEST = i_datsys
        .DoRTCalc(42,55,.t.)
        if .o_PAPREACC<>.w_PAPREACC
          .link_1_58('Full')
        endif
        if .o_PAPREFOR<>.w_PAPREFOR
          .link_1_59('Full')
        endif
        if .o_PAPREACC<>.w_PAPREACC
          .Calculate_YLUWWBXQPS()
        endif
        if .o_PAPREFOR<>.w_PAPREFOR
          .Calculate_UTXJXNOEEX()
        endif
        if .o_PAGENPRE<>.w_PAGENPRE
          .Calculate_WUFKITXWVA()
        endif
        .DoRTCalc(58,94,.t.)
        if .o_QUERY<>.w_QUERY
            .w_PAQUERY = strtran(.w_QUERY, .w_DIR_INST, "")
        endif
        .oPgFrm.Page3.oPag.SelQue.Calculate()
        if .o_PACLASSE<>.w_PACLASSE
          .Calculate_XFTJWQLYKN()
        endif
        if .o_PACLASSE<>.w_PACLASSE
          .Calculate_SSCODGKEJW()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(96,100,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page3.oPag.SelQue.Calculate()
    endwith
  return

  proc Calculate_YLUWWBXQPS()
    with this
          * --- Controlla che la prestazione per acconto non abbia attivo il check di spese collegate
          .w_Errore = IIF(.w_FLSPAN1='S', Ah_ErrorMsg('Selezionare una prestazione che non abbia spesa e anticipazione collegate!'), .f.)
          .w_PAPREACC = IIF(.w_FLSPAN1='S','', .w_PAPREACC)
          .link_1_23('Full')
    endwith
  endproc
  proc Calculate_UTXJXNOEEX()
    with this
          * --- Controlla che la prestazione onorario a forfait non abbia attivo il check di spese collegate
          .w_Errore = IIF(.w_FLSPAN2='S', Ah_ErrorMsg('Selezionare una prestazione che non abbia spesa e anticipazione collegate!'), .f.)
          .w_PAPREFOR = IIF(.w_FLSPAN2='S','', .w_PAPREFOR)
          .link_1_28('Full')
    endwith
  endproc
  proc Calculate_WUFKITXWVA()
    with this
          * --- Azzera check Visualizza seriali prestazioni
          .w_PAVISPRE = IIF(.w_PAGENPRE='N', 'N', .w_PAVISPRE)
    endwith
  endproc
  proc Calculate_RLXSMAJPQK()
    with this
          * --- Associa cartella modelli
          .w_CNSELDIR = cp_GetDir(.w_PAPATMOD,ah_MsgFormat("Cartella modelli:"), ah_Msgformat("Selezionare cartella modelli pratica"))
          .w_PAPATMOD = IIF(!EMPTY(.w_CNSELDIR), strtran(.w_CNSELDIR, .w_DIR_INST, ''), .w_PAPATMOD)
    endwith
  endproc
  proc Calculate_XFTJWQLYKN()
    with this
          * --- Azzeramento campi
          .w_PAPATMOD = IIF(EMPTY(.w_PACLASSE), space(254), .w_PAPATMOD)
          .w_PAQUERY = IIF(EMPTY(.w_PACLASSE), space(254), .w_PAQUERY)
    endwith
  endproc
  proc Calculate_SSCODGKEJW()
    with this
          * --- Visualizza bottone
          VisBtn(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPAVISPRE_1_37.enabled = this.oPgFrm.Page1.oPag.oPAVISPRE_1_37.mCond()
    this.oPgFrm.Page1.oPag.oPANOEDES_1_42.enabled = this.oPgFrm.Page1.oPag.oPANOEDES_1_42.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oPAESCANA_1_31.visible=!this.oPgFrm.Page1.oPag.oPAESCANA_1_31.mHide()
    this.oPgFrm.Page1.oPag.oPAGGPREC_1_39.visible=!this.oPgFrm.Page1.oPag.oPAGGPREC_1_39.mHide()
    this.oPgFrm.Page1.oPag.oPAFLTOTS_1_40.visible=!this.oPgFrm.Page1.oPag.oPAFLTOTS_1_40.mHide()
    this.oPgFrm.Page1.oPag.oPACAUPRE_1_41.visible=!this.oPgFrm.Page1.oPag.oPACAUPRE_1_41.mHide()
    this.oPgFrm.Page4.oPag.oPATIPUDI_4_6.visible=!this.oPgFrm.Page4.oPag.oPATIPUDI_4_6.mHide()
    this.oPgFrm.Page4.oPag.oPAPWBUDI_4_7.visible=!this.oPgFrm.Page4.oPag.oPAPWBUDI_4_7.mHide()
    this.oPgFrm.Page4.oPag.oPAPWBSCA_4_8.visible=!this.oPgFrm.Page4.oPag.oPAPWBSCA_4_8.mHide()
    this.oPgFrm.Page4.oPag.oDESUDI_4_10.visible=!this.oPgFrm.Page4.oPag.oDESUDI_4_10.mHide()
    this.oPgFrm.Page4.oPag.oDESPWBUDI_4_12.visible=!this.oPgFrm.Page4.oPag.oDESPWBUDI_4_12.mHide()
    this.oPgFrm.Page4.oPag.oDESPWBSCA_4_13.visible=!this.oPgFrm.Page4.oPag.oDESPWBSCA_4_13.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_14.visible=!this.oPgFrm.Page4.oPag.oStr_4_14.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_15.visible=!this.oPgFrm.Page4.oPag.oStr_4_15.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_16.visible=!this.oPgFrm.Page4.oPag.oStr_4_16.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_70.visible=!this.oPgFrm.Page1.oPag.oStr_1_70.mHide()
    this.oPgFrm.Page2.oPag.oPACAUANT_2_1.visible=!this.oPgFrm.Page2.oPag.oPACAUANT_2_1.mHide()
    this.oPgFrm.Page2.oPag.oPACONANT_2_2.visible=!this.oPgFrm.Page2.oPag.oPACONANT_2_2.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_3.visible=!this.oPgFrm.Page2.oPag.oStr_2_3.mHide()
    this.oPgFrm.Page2.oPag.oDESCON_2_4.visible=!this.oPgFrm.Page2.oPag.oDESCON_2_4.mHide()
    this.oPgFrm.Page2.oPag.oPACONDEF_2_5.visible=!this.oPgFrm.Page2.oPag.oPACONDEF_2_5.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_6.visible=!this.oPgFrm.Page2.oPag.oStr_2_6.mHide()
    this.oPgFrm.Page2.oPag.oDEFANT_2_7.visible=!this.oPgFrm.Page2.oPag.oDEFANT_2_7.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_8.visible=!this.oPgFrm.Page2.oPag.oStr_2_8.mHide()
    this.oPgFrm.Page2.oPag.oDESCAU_2_9.visible=!this.oPgFrm.Page2.oPag.oDESCAU_2_9.mHide()
    this.oPgFrm.Page2.oPag.oPACONSPE_2_10.visible=!this.oPgFrm.Page2.oPag.oPACONSPE_2_10.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_11.visible=!this.oPgFrm.Page2.oPag.oStr_2_11.mHide()
    this.oPgFrm.Page2.oPag.oDECSPE_2_12.visible=!this.oPgFrm.Page2.oPag.oDECSPE_2_12.mHide()
    this.oPgFrm.Page2.oPag.oPADEFSPE_2_13.visible=!this.oPgFrm.Page2.oPag.oPADEFSPE_2_13.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_14.visible=!this.oPgFrm.Page2.oPag.oStr_2_14.mHide()
    this.oPgFrm.Page2.oPag.oDEFSPE_2_15.visible=!this.oPgFrm.Page2.oPag.oDEFSPE_2_15.mHide()
    this.oPgFrm.Page2.oPag.oLinkPC_2_16.visible=!this.oPgFrm.Page2.oPag.oLinkPC_2_16.mHide()
    this.oPgFrm.Page2.oPag.oLinkPC_2_17.visible=!this.oPgFrm.Page2.oPag.oLinkPC_2_17.mHide()
    this.oPgFrm.Page3.oPag.oPAPATMOD_3_21.visible=!this.oPgFrm.Page3.oPag.oPAPATMOD_3_21.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_22.visible=!this.oPgFrm.Page3.oPag.oStr_3_22.mHide()
    this.oPgFrm.Page3.oPag.oPAQUERY_3_23.visible=!this.oPgFrm.Page3.oPag.oPAQUERY_3_23.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_25.visible=!this.oPgFrm.Page3.oPag.oStr_3_25.mHide()
    this.oPgFrm.Page3.oPag.oBtn_3_30.visible=!this.oPgFrm.Page3.oPag.oBtn_3_30.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page3.oPag.SelQue.Event(cEvent)
        if lower(cEvent)==lower("SelDir")
          .Calculate_RLXSMAJPQK()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Load")
          .Calculate_SSCODGKEJW()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsma_apa
    IF CEVENT='w_PAGENPRE Changed'
        Local cursore
        * seguo controllo presenza prestazioni provvisorie
        cursore=Sys(2015)
       if  RECCOUNT(readtable('RAP_PRES','PRNUMPRA',' ',null,.f.,'1=1',0,cursore))>0
         Ah_errormsg('Attenzione esistono prestazioni provvisorie da confermare impossibile modificare')
         This.w_PAGENPRE=This.w_OLDGENPRE
       Else
         This.w_OLDGENPRE=This.w_PAGENPRE
       endif
       if used((cursore))
         Select ((cursore))
         Use
       Endif
    Endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PA_IRPEF
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TRI_BUTI_IDX,3]
    i_lTable = "TRI_BUTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2], .t., this.TRI_BUTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PA_IRPEF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATB',True,'TRI_BUTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODTRI like "+cp_ToStrODBC(trim(this.w_PA_IRPEF)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODTRI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODTRI',trim(this.w_PA_IRPEF))
          select TRCODTRI,TRDESTRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODTRI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PA_IRPEF)==trim(_Link_.TRCODTRI) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TRDESTRI like "+cp_ToStrODBC(trim(this.w_PA_IRPEF)+"%");

            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TRDESTRI like "+cp_ToStr(trim(this.w_PA_IRPEF)+"%");

            select TRCODTRI,TRDESTRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PA_IRPEF) and !this.bDontReportError
            deferred_cp_zoom('TRI_BUTI','*','TRCODTRI',cp_AbsName(oSource.parent,'oPA_IRPEF_1_8'),i_cWhere,'GSAR_ATB',"Elenco tributi",'GSRI1AMR.TRI_BUTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',oSource.xKey(1))
            select TRCODTRI,TRDESTRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PA_IRPEF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(this.w_PA_IRPEF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',this.w_PA_IRPEF)
            select TRCODTRI,TRDESTRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PA_IRPEF = NVL(_Link_.TRCODTRI,space(5))
      this.w_DESIRP = NVL(_Link_.TRDESTRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_PA_IRPEF = space(5)
      endif
      this.w_DESIRP = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])+'\'+cp_ToStr(_Link_.TRCODTRI,1)
      cp_ShowWarn(i_cKey,this.TRI_BUTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PA_IRPEF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_8(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TRI_BUTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_8.TRCODTRI as TRCODTRI108"+ ",link_1_8.TRDESTRI as TRDESTRI108"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_8 on PAR_ALTE.PA_IRPEF=link_1_8.TRCODTRI"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_8"
          i_cKey=i_cKey+'+" and PAR_ALTE.PA_IRPEF=link_1_8.TRCODTRI(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PACASPRE
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACASPRE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_PACASPRE)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_PACASPRE))
          select IVCODIVA,IVDESIVA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PACASPRE)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStrODBC(trim(this.w_PACASPRE)+"%");

            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStr(trim(this.w_PACASPRE)+"%");

            select IVCODIVA,IVDESIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PACASPRE) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oPACASPRE_1_9'),i_cWhere,'GSAR_AIV',"Voci IVA",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDESIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACASPRE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_PACASPRE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_PACASPRE)
            select IVCODIVA,IVDESIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACASPRE = NVL(_Link_.IVCODIVA,space(5))
      this.w_DESCPA = NVL(_Link_.IVDESIVA,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PACASPRE = space(5)
      endif
      this.w_DESCPA = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACASPRE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_9(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOCIIVA_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_9.IVCODIVA as IVCODIVA109"+ ",link_1_9.IVDESIVA as IVDESIVA109"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_9 on PAR_ALTE.PACASPRE=link_1_9.IVCODIVA"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_9"
          i_cKey=i_cKey+'+" and PAR_ALTE.PACASPRE=link_1_9.IVCODIVA(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PASPEGEN
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.vociiva_IDX,3]
    i_lTable = "vociiva"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.vociiva_IDX,2], .t., this.vociiva_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.vociiva_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PASPEGEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'vociiva')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_PASPEGEN)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_PASPEGEN))
          select IVCODIVA,IVDESIVA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PASPEGEN)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStrODBC(trim(this.w_PASPEGEN)+"%");

            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStr(trim(this.w_PASPEGEN)+"%");

            select IVCODIVA,IVDESIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PASPEGEN) and !this.bDontReportError
            deferred_cp_zoom('vociiva','*','IVCODIVA',cp_AbsName(oSource.parent,'oPASPEGEN_1_10'),i_cWhere,'GSAR_AIV',"Voci IVA",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDESIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PASPEGEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_PASPEGEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_PASPEGEN)
            select IVCODIVA,IVDESIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PASPEGEN = NVL(_Link_.IVCODIVA,space(5))
      this.w_DESGEN = NVL(_Link_.IVDESIVA,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PASPEGEN = space(5)
      endif
      this.w_DESGEN = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.vociiva_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.vociiva_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PASPEGEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_10(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.vociiva_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.vociiva_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_10.IVCODIVA as IVCODIVA110"+ ",link_1_10.IVDESIVA as IVDESIVA110"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_10 on PAR_ALTE.PASPEGEN=link_1_10.IVCODIVA"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_10"
          i_cKey=i_cKey+'+" and PAR_ALTE.PASPEGEN=link_1_10.IVCODIVA(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PAPREACC
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PAPREACC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_PAPREACC)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_PAPREACC))
          select CACODICE,CADESART,CACODART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PAPREACC)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_PAPREACC)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_PAPREACC)+"%");

            select CACODICE,CADESART,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PAPREACC) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oPAPREACC_1_23'),i_cWhere,'GSMA_BZA',"Prestazioni",'GSMA_APA.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PAPREACC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_PAPREACC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_PAPREACC)
            select CACODICE,CADESART,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PAPREACC = NVL(_Link_.CACODICE,space(254))
      this.w_DESPRE = NVL(_Link_.CADESART,space(40))
      this.w_CODART1 = NVL(_Link_.CACODART,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_PAPREACC = space(254)
      endif
      this.w_DESPRE = space(40)
      this.w_CODART1 = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PAPREACC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_23(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_23.CACODICE as CACODICE123"+ ",link_1_23.CADESART as CADESART123"+ ",link_1_23.CACODART as CACODART123"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_23 on PAR_ALTE.PAPREACC=link_1_23.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_23"
          i_cKey=i_cKey+'+" and PAR_ALTE.PAPREACC=link_1_23.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PAPREFOR
  func Link_1_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PAPREFOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_PAPREFOR)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_PAPREFOR))
          select CACODICE,CADESART,CACODART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PAPREFOR)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_PAPREFOR)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_PAPREFOR)+"%");

            select CACODICE,CADESART,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PAPREFOR) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oPAPREFOR_1_28'),i_cWhere,'GSMA_BZA',"Prestazioni",'GSMA_APA.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PAPREFOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_PAPREFOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_PAPREFOR)
            select CACODICE,CADESART,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PAPREFOR = NVL(_Link_.CACODICE,space(20))
      this.w_DESFOR = NVL(_Link_.CADESART,space(40))
      this.w_CODART2 = NVL(_Link_.CACODART,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_PAPREFOR = space(20)
      endif
      this.w_DESFOR = space(40)
      this.w_CODART2 = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PAPREFOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_28(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_28.CACODICE as CACODICE128"+ ",link_1_28.CADESART as CADESART128"+ ",link_1_28.CACODART as CACODART128"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_28 on PAR_ALTE.PAPREFOR=link_1_28.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_28"
          i_cKey=i_cKey+'+" and PAR_ALTE.PAPREFOR=link_1_28.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PACODIVA
  func Link_1_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACODIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_PACODIVA)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_PACODIVA))
          select IVCODIVA,IVDESIVA,IVDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PACODIVA)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStrODBC(trim(this.w_PACODIVA)+"%");

            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStr(trim(this.w_PACODIVA)+"%");

            select IVCODIVA,IVDESIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PACODIVA) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oPACODIVA_1_30'),i_cWhere,'GSAR_AIV',"Codici IVA",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDESIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACODIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_PACODIVA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_PACODIVA)
            select IVCODIVA,IVDESIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACODIVA = NVL(_Link_.IVCODIVA,space(5))
      this.w_DESIVA = NVL(_Link_.IVDESIVA,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.IVDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PACODIVA = space(5)
      endif
      this.w_DESIVA = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice IVA obsoleto oppure inesistente")
        endif
        this.w_PACODIVA = space(5)
        this.w_DESIVA = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACODIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_30(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOCIIVA_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_30.IVCODIVA as IVCODIVA130"+ ",link_1_30.IVDESIVA as IVDESIVA130"+ ",link_1_30.IVDTOBSO as IVDTOBSO130"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_30 on PAR_ALTE.PACODIVA=link_1_30.IVCODIVA"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_30"
          i_cKey=i_cKey+'+" and PAR_ALTE.PACODIVA=link_1_30.IVCODIVA(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PACAUPRE
  func Link_1_41(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACAUPRE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_PACAUPRE)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_PACAUPRE))
          select TDTIPDOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PACAUPRE)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PACAUPRE) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oPACAUPRE_1_41'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACAUPRE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_PACAUPRE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_PACAUPRE)
            select TDTIPDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACAUPRE = NVL(_Link_.TDTIPDOC,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_PACAUPRE = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACAUPRE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PAPRESPE
  func Link_1_44(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PAPRESPE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_PAPRESPE)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_PAPRESPE))
          select CACODICE,CADESART,CADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PAPRESPE)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_PAPRESPE)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_PAPRESPE)+"%");

            select CACODICE,CADESART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PAPRESPE) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oPAPRESPE_1_44'),i_cWhere,'GSMA_BZA',"Prestazioni",'Inspre.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PAPRESPE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_PAPRESPE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_PAPRESPE)
            select CACODICE,CADESART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PAPRESPE = NVL(_Link_.CACODICE,space(20))
      this.w_DESSPE = NVL(_Link_.CADESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PAPRESPE = space(20)
      endif
      this.w_DESSPE = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) and chkprest(.w_PAPRESPE,'S')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Prestazione inesistente od obsoleta o non di tipo spesa")
        endif
        this.w_PAPRESPE = space(20)
        this.w_DESSPE = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PAPRESPE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_44(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_44.CACODICE as CACODICE144"+ ",link_1_44.CADESART as CADESART144"+ ",link_1_44.CADTOBSO as CADTOBSO144"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_44 on PAR_ALTE.PAPRESPE=link_1_44.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_44"
          i_cKey=i_cKey+'+" and PAR_ALTE.PAPRESPE=link_1_44.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PAPREANT
  func Link_1_45(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PAPREANT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_PAPREANT)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_PAPREANT))
          select CACODICE,CADESART,CADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PAPREANT)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_PAPREANT)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_PAPREANT)+"%");

            select CACODICE,CADESART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PAPREANT) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oPAPREANT_1_45'),i_cWhere,'GSMA_BZA',"Prestazioni",'Inspre.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PAPREANT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_PAPREANT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_PAPREANT)
            select CACODICE,CADESART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PAPREANT = NVL(_Link_.CACODICE,space(20))
      this.w_DESANT = NVL(_Link_.CADESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PAPREANT = space(20)
      endif
      this.w_DESANT = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) and chkprest(.w_PAPREANT,'A')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Prestazione inesistente od obsoleta o non di tipo anticipazione")
        endif
        this.w_PAPREANT = space(20)
        this.w_DESANT = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PAPREANT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_45(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_45.CACODICE as CACODICE145"+ ",link_1_45.CADESART as CADESART145"+ ",link_1_45.CADTOBSO as CADTOBSO145"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_45 on PAR_ALTE.PAPREANT=link_1_45.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_45"
          i_cKey=i_cKey+'+" and PAR_ALTE.PAPREANT=link_1_45.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PACLASSE
  func Link_3_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PROMCLAS_IDX,3]
    i_lTable = "PROMCLAS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2], .t., this.PROMCLAS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACLASSE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsut_mcd',True,'PROMCLAS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CDCODCLA like "+cp_ToStrODBC(trim(this.w_PACLASSE)+"%");

          i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDMODALL,CDCLAPRA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CDCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CDCODCLA',trim(this.w_PACLASSE))
          select CDCODCLA,CDDESCLA,CDMODALL,CDCLAPRA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CDCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PACLASSE)==trim(_Link_.CDCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PACLASSE) and !this.bDontReportError
            deferred_cp_zoom('PROMCLAS','*','CDCODCLA',cp_AbsName(oSource.parent,'oPACLASSE_3_2'),i_cWhere,'gsut_mcd',"Classi documentali",'GSUT1AMD.PROMCLAS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDMODALL,CDCLAPRA";
                     +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDCODCLA',oSource.xKey(1))
            select CDCODCLA,CDDESCLA,CDMODALL,CDCLAPRA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACLASSE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDMODALL,CDCLAPRA";
                   +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(this.w_PACLASSE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDCODCLA',this.w_PACLASSE)
            select CDCODCLA,CDDESCLA,CDMODALL,CDCLAPRA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACLASSE = NVL(_Link_.CDCODCLA,space(15))
      this.w_DESCLA = NVL(_Link_.CDDESCLA,space(50))
      this.w_MODARC = NVL(_Link_.CDMODALL,space(1))
      this.w_CLAPRA = NVL(_Link_.CDCLAPRA,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PACLASSE = space(15)
      endif
      this.w_DESCLA = space(50)
      this.w_MODARC = space(1)
      this.w_CLAPRA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=( SUBSTR( CHKPECLA( .w_PACLASSE, i_CODUTE), 2, 1 ) = 'S' ) and .w_MODARC='F' AND .w_CLAPRA='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Classe documentale non valida")
        endif
        this.w_PACLASSE = space(15)
        this.w_DESCLA = space(50)
        this.w_MODARC = space(1)
        this.w_CLAPRA = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])+'\'+cp_ToStr(_Link_.CDCODCLA,1)
      cp_ShowWarn(i_cKey,this.PROMCLAS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACLASSE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PROMCLAS_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_2.CDCODCLA as CDCODCLA302"+ ",link_3_2.CDDESCLA as CDDESCLA302"+ ",link_3_2.CDMODALL as CDMODALL302"+ ",link_3_2.CDCLAPRA as CDCLAPRA302"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_2 on PAR_ALTE.PACLASSE=link_3_2.CDCODCLA"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_2"
          i_cKey=i_cKey+'+" and PAR_ALTE.PACLASSE=link_3_2.CDCODCLA(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PAF23PRE
  func Link_1_52(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PAF23PRE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_PAF23PRE)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_PAF23PRE))
          select CACODICE,CADESART,CADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PAF23PRE)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_PAF23PRE)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_PAF23PRE)+"%");

            select CACODICE,CADESART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PAF23PRE) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oPAF23PRE_1_52'),i_cWhere,'GSMA_BZA',"Prestazioni",'Inspre.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PAF23PRE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_PAF23PRE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_PAF23PRE)
            select CACODICE,CADESART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PAF23PRE = NVL(_Link_.CACODICE,space(20))
      this.w_DESF23PRE = NVL(_Link_.CADESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PAF23PRE = space(20)
      endif
      this.w_DESF23PRE = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) 
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Prestazione inesistente od obsoleta")
        endif
        this.w_PAF23PRE = space(20)
        this.w_DESF23PRE = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PAF23PRE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_52(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_52.CACODICE as CACODICE152"+ ",link_1_52.CADESART as CADESART152"+ ",link_1_52.CADTOBSO as CADTOBSO152"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_52 on PAR_ALTE.PAF23PRE=link_1_52.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_52"
          i_cKey=i_cKey+'+" and PAR_ALTE.PAF23PRE=link_1_52.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PAF23SPE
  func Link_1_57(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PAF23SPE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_PAF23SPE)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_PAF23SPE))
          select CACODICE,CADESART,CADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PAF23SPE)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_PAF23SPE)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_PAF23SPE)+"%");

            select CACODICE,CADESART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PAF23SPE) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oPAF23SPE_1_57'),i_cWhere,'GSMA_BZA',"Prestazioni",'Inspre.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PAF23SPE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_PAF23SPE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_PAF23SPE)
            select CACODICE,CADESART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PAF23SPE = NVL(_Link_.CACODICE,space(20))
      this.w_DESF23SPE = NVL(_Link_.CADESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PAF23SPE = space(20)
      endif
      this.w_DESF23SPE = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) and chkprest(.w_PAF23SPE,'A')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Prestazione inesistente od obsoleta o non di tipo anticipazione")
        endif
        this.w_PAF23SPE = space(20)
        this.w_DESF23SPE = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PAF23SPE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_57(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_57.CACODICE as CACODICE157"+ ",link_1_57.CADESART as CADESART157"+ ",link_1_57.CADTOBSO as CADTOBSO157"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_57 on PAR_ALTE.PAF23SPE=link_1_57.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_57"
          i_cKey=i_cKey+'+" and PAR_ALTE.PAF23SPE=link_1_57.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CODART1
  func Link_1_58(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODART1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODART1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARFLSPAN";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODART1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODART1)
            select ARCODART,ARFLSPAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODART1 = NVL(_Link_.ARCODART,space(20))
      this.w_FLSPAN1 = NVL(_Link_.ARFLSPAN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODART1 = space(20)
      endif
      this.w_FLSPAN1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODART1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODART2
  func Link_1_59(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODART2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODART2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARFLSPAN";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODART2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODART2)
            select ARCODART,ARFLSPAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODART2 = NVL(_Link_.ARCODART,space(20))
      this.w_FLSPAN2 = NVL(_Link_.ARFLSPAN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODART2 = space(20)
      endif
      this.w_FLSPAN2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODART2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PACODPUN
  func Link_4_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PUN_ACCE_IDX,3]
    i_lTable = "PUN_ACCE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PUN_ACCE_IDX,2], .t., this.PUN_ACCE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PUN_ACCE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACODPUN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'PUN_ACCE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PRCODICE like "+cp_ToStrODBC(trim(this.w_PACODPUN)+"%");

          i_ret=cp_SQL(i_nConn,"select PRCODICE,PRDESCRI,PRINDIRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PRCODICE',trim(this.w_PACODPUN))
          select PRCODICE,PRDESCRI,PRINDIRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PACODPUN)==trim(_Link_.PRCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" PRDESCRI like "+cp_ToStrODBC(trim(this.w_PACODPUN)+"%");

            i_ret=cp_SQL(i_nConn,"select PRCODICE,PRDESCRI,PRINDIRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" PRDESCRI like "+cp_ToStr(trim(this.w_PACODPUN)+"%");

            select PRCODICE,PRDESCRI,PRINDIRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PACODPUN) and !this.bDontReportError
            deferred_cp_zoom('PUN_ACCE','*','PRCODICE',cp_AbsName(oSource.parent,'oPACODPUN_4_1'),i_cWhere,'',"Punti di accesso",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PRCODICE,PRDESCRI,PRINDIRI";
                     +" from "+i_cTable+" "+i_lTable+" where PRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PRCODICE',oSource.xKey(1))
            select PRCODICE,PRDESCRI,PRINDIRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACODPUN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PRCODICE,PRDESCRI,PRINDIRI";
                   +" from "+i_cTable+" "+i_lTable+" where PRCODICE="+cp_ToStrODBC(this.w_PACODPUN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PRCODICE',this.w_PACODPUN)
            select PRCODICE,PRDESCRI,PRINDIRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACODPUN = NVL(_Link_.PRCODICE,space(5))
      this.w_PRDESCRI = NVL(_Link_.PRDESCRI,space(80))
      this.w_PRINDIRI = NVL(_Link_.PRINDIRI,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_PACODPUN = space(5)
      endif
      this.w_PRDESCRI = space(80)
      this.w_PRINDIRI = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PUN_ACCE_IDX,2])+'\'+cp_ToStr(_Link_.PRCODICE,1)
      cp_ShowWarn(i_cKey,this.PUN_ACCE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACODPUN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PUN_ACCE_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PUN_ACCE_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_1.PRCODICE as PRCODICE401"+ ",link_4_1.PRDESCRI as PRDESCRI401"+ ",link_4_1.PRINDIRI as PRINDIRI401"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_1 on PAR_ALTE.PACODPUN=link_4_1.PRCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_1"
          i_cKey=i_cKey+'+" and PAR_ALTE.PACODPUN=link_4_1.PRCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PATIPUDI
  func Link_4_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PATIPUDI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MCA',True,'CAUMATTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_PATIPUDI)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CARAGGST";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_PATIPUDI))
          select CACODICE,CADESCRI,CARAGGST;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PATIPUDI)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStrODBC(trim(this.w_PATIPUDI)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CARAGGST";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStr(trim(this.w_PATIPUDI)+"%");

            select CACODICE,CADESCRI,CARAGGST;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PATIPUDI) and !this.bDontReportError
            deferred_cp_zoom('CAUMATTI','*','CACODICE',cp_AbsName(oSource.parent,'oPATIPUDI_4_6'),i_cWhere,'GSAG_MCA',"Tipi attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CARAGGST";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESCRI,CARAGGST;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PATIPUDI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CARAGGST";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_PATIPUDI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_PATIPUDI)
            select CACODICE,CADESCRI,CARAGGST;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PATIPUDI = NVL(_Link_.CACODICE,space(20))
      this.w_DESUDI = NVL(_Link_.CADESCRI,space(254))
      this.w_TIPPOLU = NVL(_Link_.CARAGGST,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PATIPUDI = space(20)
      endif
      this.w_DESUDI = space(254)
      this.w_TIPPOLU = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PATIPUDI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAUMATTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_6.CACODICE as CACODICE406"+ ",link_4_6.CADESCRI as CADESCRI406"+ ",link_4_6.CARAGGST as CARAGGST406"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_6 on PAR_ALTE.PATIPUDI=link_4_6.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_6"
          i_cKey=i_cKey+'+" and PAR_ALTE.PATIPUDI=link_4_6.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PAPWBUDI
  func Link_4_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PAPWBUDI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MCA',True,'CAUMATTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_PAPWBUDI)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_PAPWBUDI))
          select CACODICE,CADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PAPWBUDI)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStrODBC(trim(this.w_PAPWBUDI)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStr(trim(this.w_PAPWBUDI)+"%");

            select CACODICE,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PAPWBUDI) and !this.bDontReportError
            deferred_cp_zoom('CAUMATTI','*','CACODICE',cp_AbsName(oSource.parent,'oPAPWBUDI_4_7'),i_cWhere,'GSAG_MCA',"Tipi attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PAPWBUDI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_PAPWBUDI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_PAPWBUDI)
            select CACODICE,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PAPWBUDI = NVL(_Link_.CACODICE,space(20))
      this.w_DESPWBUDI = NVL(_Link_.CADESCRI,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_PAPWBUDI = space(20)
      endif
      this.w_DESPWBUDI = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PAPWBUDI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAUMATTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_7.CACODICE as CACODICE407"+ ",link_4_7.CADESCRI as CADESCRI407"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_7 on PAR_ALTE.PAPWBUDI=link_4_7.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_7"
          i_cKey=i_cKey+'+" and PAR_ALTE.PAPWBUDI=link_4_7.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PAPWBSCA
  func Link_4_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PAPWBSCA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MCA',True,'CAUMATTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_PAPWBSCA)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_PAPWBSCA))
          select CACODICE,CADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PAPWBSCA)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStrODBC(trim(this.w_PAPWBSCA)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStr(trim(this.w_PAPWBSCA)+"%");

            select CACODICE,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PAPWBSCA) and !this.bDontReportError
            deferred_cp_zoom('CAUMATTI','*','CACODICE',cp_AbsName(oSource.parent,'oPAPWBSCA_4_8'),i_cWhere,'GSAG_MCA',"Tipi attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PAPWBSCA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_PAPWBSCA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_PAPWBSCA)
            select CACODICE,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PAPWBSCA = NVL(_Link_.CACODICE,space(20))
      this.w_DESPWBSCA = NVL(_Link_.CADESCRI,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_PAPWBSCA = space(20)
      endif
      this.w_DESPWBSCA = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PAPWBSCA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_8(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAUMATTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_8.CACODICE as CACODICE408"+ ",link_4_8.CADESCRI as CADESCRI408"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_8 on PAR_ALTE.PAPWBSCA=link_4_8.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_8"
          i_cKey=i_cKey+'+" and PAR_ALTE.PAPWBSCA=link_4_8.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PACAUANT
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACAUANT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gscg_acc',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_PACAUANT)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_PACAUANT))
          select CCCODICE,CCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PACAUANT)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_PACAUANT)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_PACAUANT)+"%");

            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PACAUANT) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oPACAUANT_2_1'),i_cWhere,'gscg_acc',"Causali contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACAUANT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_PACAUANT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_PACAUANT)
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACAUANT = NVL(_Link_.CCCODICE,space(5))
      this.w_DESCAU = NVL(_Link_.CCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PACAUANT = space(5)
      endif
      this.w_DESCAU = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACAUANT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.CCCODICE as CCCODICE201"+ ",link_2_1.CCDESCRI as CCDESCRI201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on PAR_ALTE.PACAUANT=link_2_1.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and PAR_ALTE.PACAUANT=link_2_1.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PACONANT
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACONANT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_api',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PACONANT)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_PACONANT))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PACONANT)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_PACONANT)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_PACONANT)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PACONANT) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPACONANT_2_2'),i_cWhere,'gsar_api',"Conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACONANT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PACONANT);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_PACONANT)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACONANT = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_PACONANT = space(15)
      endif
      this.w_DESCON = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACONANT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PACONDEF
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACONDEF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_api',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PACONDEF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_PACONDEF))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PACONDEF)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_PACONDEF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_PACONDEF)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PACONDEF) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPACONDEF_2_5'),i_cWhere,'gsar_api',"Conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACONDEF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PACONDEF);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_PACONDEF)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACONDEF = NVL(_Link_.ANCODICE,space(15))
      this.w_DEFANT = NVL(_Link_.ANDESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_PACONDEF = space(15)
      endif
      this.w_DEFANT = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACONDEF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PACONSPE
  func Link_2_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACONSPE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_api',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PACONSPE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_PACONSPE))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PACONSPE)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_PACONSPE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_PACONSPE)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PACONSPE) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPACONSPE_2_10'),i_cWhere,'gsar_api',"Conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACONSPE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PACONSPE);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_PACONSPE)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACONSPE = NVL(_Link_.ANCODICE,space(15))
      this.w_DECSPE = NVL(_Link_.ANDESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_PACONSPE = space(15)
      endif
      this.w_DECSPE = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACONSPE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PADEFSPE
  func Link_2_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PADEFSPE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_api',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PADEFSPE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_PADEFSPE))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PADEFSPE)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_PADEFSPE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_PADEFSPE)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PADEFSPE) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPADEFSPE_2_13'),i_cWhere,'gsar_api',"Conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PADEFSPE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PADEFSPE);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_PADEFSPE)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PADEFSPE = NVL(_Link_.ANCODICE,space(15))
      this.w_DEFSPE = NVL(_Link_.ANDESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_PADEFSPE = space(15)
      endif
      this.w_DEFSPE = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PADEFSPE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPADATINI_1_3.value==this.w_PADATINI)
      this.oPgFrm.Page1.oPag.oPADATINI_1_3.value=this.w_PADATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oPADATFIN_1_5.value==this.w_PADATFIN)
      this.oPgFrm.Page1.oPag.oPADATFIN_1_5.value=this.w_PADATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPAELCIVA_1_6.value==this.w_PAELCIVA)
      this.oPgFrm.Page1.oPag.oPAELCIVA_1_6.value=this.w_PAELCIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oPA_IRPEF_1_8.value==this.w_PA_IRPEF)
      this.oPgFrm.Page1.oPag.oPA_IRPEF_1_8.value=this.w_PA_IRPEF
    endif
    if not(this.oPgFrm.Page1.oPag.oPACASPRE_1_9.value==this.w_PACASPRE)
      this.oPgFrm.Page1.oPag.oPACASPRE_1_9.value=this.w_PACASPRE
    endif
    if not(this.oPgFrm.Page1.oPag.oPASPEGEN_1_10.value==this.w_PASPEGEN)
      this.oPgFrm.Page1.oPag.oPASPEGEN_1_10.value=this.w_PASPEGEN
    endif
    if not(this.oPgFrm.Page1.oPag.oPARIFACC_1_11.RadioValue()==this.w_PARIFACC)
      this.oPgFrm.Page1.oPag.oPARIFACC_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAFLGAQV_1_12.RadioValue()==this.w_PAFLGAQV)
      this.oPgFrm.Page1.oPag.oPAFLGAQV_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAFLGZER_1_13.RadioValue()==this.w_PAFLGZER)
      this.oPgFrm.Page1.oPag.oPAFLGZER_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAFLDESP_1_14.RadioValue()==this.w_PAFLDESP)
      this.oPgFrm.Page1.oPag.oPAFLDESP_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESIRP_1_18.value==this.w_DESIRP)
      this.oPgFrm.Page1.oPag.oDESIRP_1_18.value=this.w_DESIRP
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCPA_1_19.value==this.w_DESCPA)
      this.oPgFrm.Page1.oPag.oDESCPA_1_19.value=this.w_DESCPA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGEN_1_20.value==this.w_DESGEN)
      this.oPgFrm.Page1.oPag.oDESGEN_1_20.value=this.w_DESGEN
    endif
    if not(this.oPgFrm.Page1.oPag.oPAPREACC_1_23.value==this.w_PAPREACC)
      this.oPgFrm.Page1.oPag.oPAPREACC_1_23.value=this.w_PAPREACC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPRE_1_24.value==this.w_DESPRE)
      this.oPgFrm.Page1.oPag.oDESPRE_1_24.value=this.w_DESPRE
    endif
    if not(this.oPgFrm.Page1.oPag.oPAPREFOR_1_28.value==this.w_PAPREFOR)
      this.oPgFrm.Page1.oPag.oPAPREFOR_1_28.value=this.w_PAPREFOR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFOR_1_29.value==this.w_DESFOR)
      this.oPgFrm.Page1.oPag.oDESFOR_1_29.value=this.w_DESFOR
    endif
    if not(this.oPgFrm.Page1.oPag.oPACODIVA_1_30.value==this.w_PACODIVA)
      this.oPgFrm.Page1.oPag.oPACODIVA_1_30.value=this.w_PACODIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oPAESCANA_1_31.RadioValue()==this.w_PAESCANA)
      this.oPgFrm.Page1.oPag.oPAESCANA_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPASONFOR_1_32.RadioValue()==this.w_PASONFOR)
      this.oPgFrm.Page1.oPag.oPASONFOR_1_32.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAFLMIMA_1_33.RadioValue()==this.w_PAFLMIMA)
      this.oPgFrm.Page1.oPag.oPAFLMIMA_1_33.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGENPRE_1_34.RadioValue()==this.w_PAGENPRE)
      this.oPgFrm.Page1.oPag.oPAGENPRE_1_34.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAFLGMSK_1_35.RadioValue()==this.w_PAFLGMSK)
      this.oPgFrm.Page1.oPag.oPAFLGMSK_1_35.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAFLCODO_1_36.RadioValue()==this.w_PAFLCODO)
      this.oPgFrm.Page1.oPag.oPAFLCODO_1_36.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAVISPRE_1_37.RadioValue()==this.w_PAVISPRE)
      this.oPgFrm.Page1.oPag.oPAVISPRE_1_37.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAFILDAT_1_38.RadioValue()==this.w_PAFILDAT)
      this.oPgFrm.Page1.oPag.oPAFILDAT_1_38.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGGPREC_1_39.value==this.w_PAGGPREC)
      this.oPgFrm.Page1.oPag.oPAGGPREC_1_39.value=this.w_PAGGPREC
    endif
    if not(this.oPgFrm.Page1.oPag.oPAFLTOTS_1_40.RadioValue()==this.w_PAFLTOTS)
      this.oPgFrm.Page1.oPag.oPAFLTOTS_1_40.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPACAUPRE_1_41.value==this.w_PACAUPRE)
      this.oPgFrm.Page1.oPag.oPACAUPRE_1_41.value=this.w_PACAUPRE
    endif
    if not(this.oPgFrm.Page1.oPag.oPANOEDES_1_42.RadioValue()==this.w_PANOEDES)
      this.oPgFrm.Page1.oPag.oPANOEDES_1_42.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAPRESPE_1_44.value==this.w_PAPRESPE)
      this.oPgFrm.Page1.oPag.oPAPRESPE_1_44.value=this.w_PAPRESPE
    endif
    if not(this.oPgFrm.Page1.oPag.oPAPREANT_1_45.value==this.w_PAPREANT)
      this.oPgFrm.Page1.oPag.oPAPREANT_1_45.value=this.w_PAPREANT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESSPE_1_46.value==this.w_DESSPE)
      this.oPgFrm.Page1.oPag.oDESSPE_1_46.value=this.w_DESSPE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESANT_1_48.value==this.w_DESANT)
      this.oPgFrm.Page1.oPag.oDESANT_1_48.value=this.w_DESANT
    endif
    if not(this.oPgFrm.Page3.oPag.oPATIPARC_3_1.RadioValue()==this.w_PATIPARC)
      this.oPgFrm.Page3.oPag.oPATIPARC_3_1.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oPACLASSE_3_2.value==this.w_PACLASSE)
      this.oPgFrm.Page3.oPag.oPACLASSE_3_2.value=this.w_PACLASSE
    endif
    if not(this.oPgFrm.Page3.oPag.oPATIPSEN_3_3.RadioValue()==this.w_PATIPSEN)
      this.oPgFrm.Page3.oPag.oPATIPSEN_3_3.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oPAORDFIL_3_4.RadioValue()==this.w_PAORDFIL)
      this.oPgFrm.Page3.oPag.oPAORDFIL_3_4.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oPAHIDARC_3_5.RadioValue()==this.w_PAHIDARC)
      this.oPgFrm.Page3.oPag.oPAHIDARC_3_5.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oPAHIDSCA_3_6.RadioValue()==this.w_PAHIDSCA)
      this.oPgFrm.Page3.oPag.oPAHIDSCA_3_6.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oPAHIDOPE_3_7.RadioValue()==this.w_PAHIDOPE)
      this.oPgFrm.Page3.oPag.oPAHIDOPE_3_7.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oPAVERDOC_3_10.RadioValue()==this.w_PAVERDOC)
      this.oPgFrm.Page3.oPag.oPAVERDOC_3_10.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oPADATZIP_3_11.RadioValue()==this.w_PADATZIP)
      this.oPgFrm.Page3.oPag.oPADATZIP_3_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAF23PRE_1_52.value==this.w_PAF23PRE)
      this.oPgFrm.Page1.oPag.oPAF23PRE_1_52.value=this.w_PAF23PRE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESF23PRE_1_53.value==this.w_DESF23PRE)
      this.oPgFrm.Page1.oPag.oDESF23PRE_1_53.value=this.w_DESF23PRE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESF23SPE_1_54.value==this.w_DESF23SPE)
      this.oPgFrm.Page1.oPag.oDESF23SPE_1_54.value=this.w_DESF23SPE
    endif
    if not(this.oPgFrm.Page1.oPag.oPAF23SPE_1_57.value==this.w_PAF23SPE)
      this.oPgFrm.Page1.oPag.oPAF23SPE_1_57.value=this.w_PAF23SPE
    endif
    if not(this.oPgFrm.Page3.oPag.oPAFLGDIK_3_12.RadioValue()==this.w_PAFLGDIK)
      this.oPgFrm.Page3.oPag.oPAFLGDIK_3_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESIVA_1_67.value==this.w_DESIVA)
      this.oPgFrm.Page1.oPag.oDESIVA_1_67.value=this.w_DESIVA
    endif
    if not(this.oPgFrm.Page4.oPag.oPACODPUN_4_1.value==this.w_PACODPUN)
      this.oPgFrm.Page4.oPag.oPACODPUN_4_1.value=this.w_PACODPUN
    endif
    if not(this.oPgFrm.Page4.oPag.oPATIPUDI_4_6.value==this.w_PATIPUDI)
      this.oPgFrm.Page4.oPag.oPATIPUDI_4_6.value=this.w_PATIPUDI
    endif
    if not(this.oPgFrm.Page4.oPag.oPAPWBUDI_4_7.value==this.w_PAPWBUDI)
      this.oPgFrm.Page4.oPag.oPAPWBUDI_4_7.value=this.w_PAPWBUDI
    endif
    if not(this.oPgFrm.Page4.oPag.oPAPWBSCA_4_8.value==this.w_PAPWBSCA)
      this.oPgFrm.Page4.oPag.oPAPWBSCA_4_8.value=this.w_PAPWBSCA
    endif
    if not(this.oPgFrm.Page4.oPag.oPAPWBRUO_4_9.RadioValue()==this.w_PAPWBRUO)
      this.oPgFrm.Page4.oPag.oPAPWBRUO_4_9.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oDESUDI_4_10.value==this.w_DESUDI)
      this.oPgFrm.Page4.oPag.oDESUDI_4_10.value=this.w_DESUDI
    endif
    if not(this.oPgFrm.Page4.oPag.oPADATPOL_4_11.value==this.w_PADATPOL)
      this.oPgFrm.Page4.oPag.oPADATPOL_4_11.value=this.w_PADATPOL
    endif
    if not(this.oPgFrm.Page4.oPag.oDESPWBUDI_4_12.value==this.w_DESPWBUDI)
      this.oPgFrm.Page4.oPag.oDESPWBUDI_4_12.value=this.w_DESPWBUDI
    endif
    if not(this.oPgFrm.Page4.oPag.oDESPWBSCA_4_13.value==this.w_DESPWBSCA)
      this.oPgFrm.Page4.oPag.oDESPWBSCA_4_13.value=this.w_DESPWBSCA
    endif
    if not(this.oPgFrm.Page4.oPag.oPRINDIRI_4_18.value==this.w_PRINDIRI)
      this.oPgFrm.Page4.oPag.oPRINDIRI_4_18.value=this.w_PRINDIRI
    endif
    if not(this.oPgFrm.Page4.oPag.oPRDESCRI_4_19.value==this.w_PRDESCRI)
      this.oPgFrm.Page4.oPag.oPRDESCRI_4_19.value=this.w_PRDESCRI
    endif
    if not(this.oPgFrm.Page3.oPag.oPADMCOMP_3_13.RadioValue()==this.w_PADMCOMP)
      this.oPgFrm.Page3.oPag.oPADMCOMP_3_13.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oPANEWQRY_3_14.RadioValue()==this.w_PANEWQRY)
      this.oPgFrm.Page3.oPag.oPANEWQRY_3_14.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oPAFLWDTP_3_15.RadioValue()==this.w_PAFLWDTP)
      this.oPgFrm.Page3.oPag.oPAFLWDTP_3_15.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oPANOARCH_4_21.RadioValue()==this.w_PANOARCH)
      this.oPgFrm.Page4.oPag.oPANOARCH_4_21.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oPAESCNOM_4_23.RadioValue()==this.w_PAESCNOM)
      this.oPgFrm.Page4.oPag.oPAESCNOM_4_23.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPACAUANT_2_1.value==this.w_PACAUANT)
      this.oPgFrm.Page2.oPag.oPACAUANT_2_1.value=this.w_PACAUANT
    endif
    if not(this.oPgFrm.Page2.oPag.oPACONANT_2_2.value==this.w_PACONANT)
      this.oPgFrm.Page2.oPag.oPACONANT_2_2.value=this.w_PACONANT
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCON_2_4.value==this.w_DESCON)
      this.oPgFrm.Page2.oPag.oDESCON_2_4.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page2.oPag.oPACONDEF_2_5.value==this.w_PACONDEF)
      this.oPgFrm.Page2.oPag.oPACONDEF_2_5.value=this.w_PACONDEF
    endif
    if not(this.oPgFrm.Page2.oPag.oDEFANT_2_7.value==this.w_DEFANT)
      this.oPgFrm.Page2.oPag.oDEFANT_2_7.value=this.w_DEFANT
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAU_2_9.value==this.w_DESCAU)
      this.oPgFrm.Page2.oPag.oDESCAU_2_9.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page2.oPag.oPACONSPE_2_10.value==this.w_PACONSPE)
      this.oPgFrm.Page2.oPag.oPACONSPE_2_10.value=this.w_PACONSPE
    endif
    if not(this.oPgFrm.Page2.oPag.oDECSPE_2_12.value==this.w_DECSPE)
      this.oPgFrm.Page2.oPag.oDECSPE_2_12.value=this.w_DECSPE
    endif
    if not(this.oPgFrm.Page2.oPag.oPADEFSPE_2_13.value==this.w_PADEFSPE)
      this.oPgFrm.Page2.oPag.oPADEFSPE_2_13.value=this.w_PADEFSPE
    endif
    if not(this.oPgFrm.Page2.oPag.oDEFSPE_2_15.value==this.w_DEFSPE)
      this.oPgFrm.Page2.oPag.oDEFSPE_2_15.value=this.w_DEFSPE
    endif
    if not(this.oPgFrm.Page3.oPag.oDESCLA_3_18.value==this.w_DESCLA)
      this.oPgFrm.Page3.oPag.oDESCLA_3_18.value=this.w_DESCLA
    endif
    if not(this.oPgFrm.Page3.oPag.oPAPATMOD_3_21.value==this.w_PAPATMOD)
      this.oPgFrm.Page3.oPag.oPAPATMOD_3_21.value=this.w_PAPATMOD
    endif
    if not(this.oPgFrm.Page3.oPag.oPAQUERY_3_23.value==this.w_PAQUERY)
      this.oPgFrm.Page3.oPag.oPAQUERY_3_23.value=this.w_PAQUERY
    endif
    if not(this.oPgFrm.Page3.oPag.oPADATARC_3_36.RadioValue()==this.w_PADATARC)
      this.oPgFrm.Page3.oPag.oPADATARC_3_36.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oPA_INVIO_4_24.RadioValue()==this.w_PA_INVIO)
      this.oPgFrm.Page4.oPag.oPA_INVIO_4_24.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'PAR_ALTE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_PADATINI <= .w_PADATFIN)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPADATFIN_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data iniziale successiva a quella finale!")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_PACODIVA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPACODIVA_1_30.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice IVA obsoleto oppure inesistente")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) and chkprest(.w_PAPRESPE,'S'))  and not(empty(.w_PAPRESPE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPAPRESPE_1_44.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Prestazione inesistente od obsoleta o non di tipo spesa")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) and chkprest(.w_PAPREANT,'A'))  and not(empty(.w_PAPREANT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPAPREANT_1_45.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Prestazione inesistente od obsoleta o non di tipo anticipazione")
          case   not(.w_PATIPARC<>'E' or g_AEDS='S')
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oPATIPARC_3_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(( SUBSTR( CHKPECLA( .w_PACLASSE, i_CODUTE), 2, 1 ) = 'S' ) and .w_MODARC='F' AND .w_CLAPRA='S')  and not(empty(.w_PACLASSE))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oPACLASSE_3_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Classe documentale non valida")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) )  and not(empty(.w_PAF23PRE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPAF23PRE_1_52.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Prestazione inesistente od obsoleta")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) and chkprest(.w_PAF23SPE,'A'))  and not(empty(.w_PAF23SPE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPAF23SPE_1_57.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Prestazione inesistente od obsoleta o non di tipo anticipazione")
        endcase
      endif
      *i_bRes = i_bRes .and. .gsal_mpa.CheckForm()
      if i_bres
        i_bres=  .gsal_mpa.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      *i_bRes = i_bRes .and. .gsal_mca.CheckForm()
      if i_bres
        i_bres=  .gsal_mca.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PAPREACC = this.w_PAPREACC
    this.o_PAPREFOR = this.w_PAPREFOR
    this.o_PAGENPRE = this.w_PAGENPRE
    this.o_PAFILDAT = this.w_PAFILDAT
    this.o_PACLASSE = this.w_PACLASSE
    this.o_QUERY = this.w_QUERY
    * --- gsal_mpa : Depends On
    this.gsal_mpa.SaveDependsOn()
    * --- gsal_mca : Depends On
    this.gsal_mca.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsma_apaPag1 as StdContainer
  Width  = 934
  height = 393
  stdWidth  = 934
  stdheight = 393
  resizeXpos=600
  resizeYpos=127
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPADATINI_1_3 as StdField with uid="NJOEZHZXFC",rtseq=3,rtrep=.f.,;
    cFormVar = "w_PADATINI", cQueryName = "PADATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio validit� listino",;
    HelpContextID = 184874177,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=205, Top=5

  add object oPADATFIN_1_5 as StdField with uid="WKGEUKGUNY",rtseq=4,rtrep=.f.,;
    cFormVar = "w_PADATFIN", cQueryName = "PADATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data iniziale successiva a quella finale!",;
    ToolTipText = "Data fine validit� listino",;
    HelpContextID = 33229636,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=438, Top=5

  func oPADATFIN_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PADATINI <= .w_PADATFIN)
    endwith
    return bRes
  endfunc

  add object oPAELCIVA_1_6 as StdField with uid="WOBDXKBNKB",rtseq=5,rtrep=.f.,;
    cFormVar = "w_PAELCIVA", cQueryName = "PAELCIVA",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Inserire i codici iva non pi� in vigore che saranno utilizzati dalla procedura in fase di generazione delle parcelle. In caso di pi� codici iva inserli separati da virgola e congli spazi cha mancano fra la lunghezza del codice e i 5 caratteri del campo ",;
    HelpContextID = 66460471,;
   bGlobalFont=.t.,;
    Height=23, Width=195, Left=691, Top=5, InputMask=replicate('X',254)

  add object oPA_IRPEF_1_8 as StdField with uid="VRKDJNNFRJ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_PA_IRPEF", cQueryName = "PA_IRPEF",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice tributo IRPEF",;
    HelpContextID = 199539516,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=205, Top=30, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TRI_BUTI", cZoomOnZoom="GSAR_ATB", oKey_1_1="TRCODTRI", oKey_1_2="this.w_PA_IRPEF"

  func oPA_IRPEF_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oPA_IRPEF_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPA_IRPEF_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TRI_BUTI','*','TRCODTRI',cp_AbsName(this.parent,'oPA_IRPEF_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATB',"Elenco tributi",'GSRI1AMR.TRI_BUTI_VZM',this.parent.oContained
  endproc
  proc oPA_IRPEF_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODTRI=this.parent.oContained.w_PA_IRPEF
     i_obj.ecpSave()
  endproc

  add object oPACASPRE_1_9 as StdField with uid="JNHTBRIMNB",rtseq=7,rtrep=.f.,;
    cFormVar = "w_PACASPRE", cQueryName = "PACASPRE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice IVA cassa previdenza",;
    HelpContextID = 68486341,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=205, Top=55, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_PACASPRE"

  func oPACASPRE_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACASPRE_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPACASPRE_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oPACASPRE_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Voci IVA",'',this.parent.oContained
  endproc
  proc oPACASPRE_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_PACASPRE
     i_obj.ecpSave()
  endproc

  add object oPASPEGEN_1_10 as StdField with uid="KUMIPUWLSB",rtseq=8,rtrep=.f.,;
    cFormVar = "w_PASPEGEN", cQueryName = "PASPEGEN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice IVA spese generali",;
    HelpContextID = 35322692,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=205, Top=80, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="vociiva", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_PASPEGEN"

  func oPASPEGEN_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oPASPEGEN_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPASPEGEN_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'vociiva','*','IVCODIVA',cp_AbsName(this.parent,'oPASPEGEN_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Voci IVA",'',this.parent.oContained
  endproc
  proc oPASPEGEN_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_PASPEGEN
     i_obj.ecpSave()
  endproc

  add object oPARIFACC_1_11 as StdCheck with uid="TSWLEQXQCY",rtseq=9,rtrep=.f.,left=536, top=31, caption="Riferimento fatture di acconto",;
    ToolTipText = "Se attivo, nelle fatture/proforme/bozze verranno riportati i riferimenti alle fatture d'acconto detratte.",;
    HelpContextID = 64754887,;
    cFormVar="w_PARIFACC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPARIFACC_1_11.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPARIFACC_1_11.GetRadio()
    this.Parent.oContained.w_PARIFACC = this.RadioValue()
    return .t.
  endfunc

  func oPARIFACC_1_11.SetRadio()
    this.Parent.oContained.w_PARIFACC=trim(this.Parent.oContained.w_PARIFACC)
    this.value = ;
      iif(this.Parent.oContained.w_PARIFACC=='S',1,;
      0)
  endfunc

  add object oPAFLGAQV_1_12 as StdCheck with uid="TUHLPATABV",rtseq=10,rtrep=.f.,left=536, top=53, caption="Anticipazioni e spese A q.t� e valore nei documenti di acconto",;
    ToolTipText = "Se attivo, consente di inserire all'interno dei documenti di acconto anche le anticipazioni e le spese a quantit� e valore (Es:anticipazione per 10 marche da bollo da 8 euro anzich� anticipazione con il solo valore di 80 euro).",;
    HelpContextID = 63558836,;
    cFormVar="w_PAFLGAQV", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAFLGAQV_1_12.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPAFLGAQV_1_12.GetRadio()
    this.Parent.oContained.w_PAFLGAQV = this.RadioValue()
    return .t.
  endfunc

  func oPAFLGAQV_1_12.SetRadio()
    this.Parent.oContained.w_PAFLGAQV=trim(this.Parent.oContained.w_PAFLGAQV)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLGAQV=='S',1,;
      0)
  endfunc

  add object oPAFLGZER_1_13 as StdCheck with uid="PFQFUKAUIE",rtseq=11,rtrep=.f.,left=536, top=75, caption="Gestione prestazioni a valore con importo a zero",;
    ToolTipText = "Se attivo, inserisce nei documenti PRES anche le prestazioni a valore con importo a zero, considerandole durante il processo di evasione ne permette quindi l'inserimento in proforma e in fattura.",;
    HelpContextID = 87436104,;
    cFormVar="w_PAFLGZER", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAFLGZER_1_13.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPAFLGZER_1_13.GetRadio()
    this.Parent.oContained.w_PAFLGZER = this.RadioValue()
    return .t.
  endfunc

  func oPAFLGZER_1_13.SetRadio()
    this.Parent.oContained.w_PAFLGZER=trim(this.Parent.oContained.w_PAFLGZER)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLGZER=='S',1,;
      0)
  endfunc

  add object oPAFLDESP_1_14 as StdCheck with uid="FNAPSGPGNF",rtseq=12,rtrep=.f.,left=536, top=97, caption="Usa descrizione prestazione dei raggruppamenti",;
    ToolTipText = "Se attivo, viene utilizzata la descrizione della prestazione presente nei relativi raggruppamenti",;
    HelpContextID = 404294,;
    cFormVar="w_PAFLDESP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAFLDESP_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPAFLDESP_1_14.GetRadio()
    this.Parent.oContained.w_PAFLDESP = this.RadioValue()
    return .t.
  endfunc

  func oPAFLDESP_1_14.SetRadio()
    this.Parent.oContained.w_PAFLDESP=trim(this.Parent.oContained.w_PAFLDESP)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLDESP=='S',1,;
      0)
  endfunc

  add object oDESIRP_1_18 as StdField with uid="EIXFAMUXRD",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESIRP", cQueryName = "DESIRP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione codice tributo IRPEF",;
    HelpContextID = 68944330,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=256, Top=31, InputMask=replicate('X',60)

  add object oDESCPA_1_19 as StdField with uid="HTFAPVIGBU",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DESCPA", cQueryName = "DESCPA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione codice IVA cassa previdenza",;
    HelpContextID = 54657482,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=256, Top=55, InputMask=replicate('X',35)

  add object oDESGEN_1_20 as StdField with uid="ALGXXPRXZH",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DESGEN", cQueryName = "DESGEN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione codice IVA spese generali",;
    HelpContextID = 116261322,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=256, Top=80, InputMask=replicate('X',35)

  add object oPAPREACC_1_23 as StdField with uid="JNYNQLWBFX",rtseq=17,rtrep=.f.,;
    cFormVar = "w_PAPREACC", cQueryName = "PAPREACC",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    sErrorMsg = "Selezionare una prestazione che non abbia spesa e anticipazione collegate",;
    ToolTipText = "Prestazione per acconto diritti e onorari",;
    HelpContextID = 65221831,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=205, Top=128, InputMask=replicate('X',254), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_BZA", oKey_1_1="CACODICE", oKey_1_2="this.w_PAPREACC"

  func oPAPREACC_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oPAPREACC_1_23.ecpDrop(oSource)
    this.Parent.oContained.link_1_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPAPREACC_1_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oPAPREACC_1_23'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Prestazioni",'GSMA_APA.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oPAPREACC_1_23.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_PAPREACC
     i_obj.ecpSave()
  endproc

  add object oDESPRE_1_24 as StdField with uid="GIHHOJIHFW",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DESPRE", cQueryName = "DESPRE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione prestazione per acconto diritti e onorari",;
    HelpContextID = 253034954,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=363, Top=128, InputMask=replicate('X',40)

  add object oPAPREFOR_1_28 as StdField with uid="MAFMHAQEED",rtseq=21,rtrep=.f.,;
    cFormVar = "w_PAPREFOR", cQueryName = "PAPREFOR",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Selezionare una prestazione che non abbia spesa e anticipazione collegate",;
    ToolTipText = "Prestazione per onorario a forfait",;
    HelpContextID = 249771192,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=205, Top=151, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_BZA", oKey_1_1="CACODICE", oKey_1_2="this.w_PAPREFOR"

  func oPAPREFOR_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_28('Part',this)
    endwith
    return bRes
  endfunc

  proc oPAPREFOR_1_28.ecpDrop(oSource)
    this.Parent.oContained.link_1_28('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPAPREFOR_1_28.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oPAPREFOR_1_28'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Prestazioni",'GSMA_APA.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oPAPREFOR_1_28.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_PAPREFOR
     i_obj.ecpSave()
  endproc

  add object oDESFOR_1_29 as StdField with uid="HJFNHRJDES",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DESFOR", cQueryName = "DESFOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione prestazione per onorario a forfait",;
    HelpContextID = 38732234,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=363, Top=151, InputMask=replicate('X',40)

  add object oPACODIVA_1_30 as StdField with uid="PWKVQAKDSQ",rtseq=23,rtrep=.f.,;
    cFormVar = "w_PACODIVA", cQueryName = "PACODIVA",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice IVA obsoleto oppure inesistente",;
    ToolTipText = "Codice IVA per scorporo nelle fatture a forfait",;
    HelpContextID = 67697463,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=205, Top=174, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_PACODIVA"

  func oPACODIVA_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_30('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACODIVA_1_30.ecpDrop(oSource)
    this.Parent.oContained.link_1_30('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPACODIVA_1_30.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oPACODIVA_1_30'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Codici IVA",'',this.parent.oContained
  endproc
  proc oPACODIVA_1_30.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_PACODIVA
     i_obj.ecpSave()
  endproc

  add object oPAESCANA_1_31 as StdCheck with uid="OOMOYOLDYK",rtseq=24,rtrep=.f.,left=752, top=177, caption="Escludi analitica per le ant.",;
    ToolTipText = "Escludi analitica per le anticipazioni",;
    HelpContextID = 67298505,;
    cFormVar="w_PAESCANA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAESCANA_1_31.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPAESCANA_1_31.GetRadio()
    this.Parent.oContained.w_PAESCANA = this.RadioValue()
    return .t.
  endfunc

  func oPAESCANA_1_31.SetRadio()
    this.Parent.oContained.w_PAESCANA=trim(this.Parent.oContained.w_PAESCANA)
    this.value = ;
      iif(this.Parent.oContained.w_PAESCANA=='S',1,;
      0)
  endfunc

  func oPAESCANA_1_31.mHide()
    with this.Parent.oContained
      return (g_COAN <>'S')
    endwith
  endfunc

  add object oPASONFOR_1_32 as StdCheck with uid="VFLJPOMOOS",rtseq=25,rtrep=.f.,left=205, top=197, caption="Ricalcolo dei solo onorari per fatture a forfait",;
    ToolTipText = "Se attivo, nelle fatture a forfait, esegue il ricalcolo dei solo onorari",;
    HelpContextID = 240518328,;
    cFormVar="w_PASONFOR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPASONFOR_1_32.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPASONFOR_1_32.GetRadio()
    this.Parent.oContained.w_PASONFOR = this.RadioValue()
    return .t.
  endfunc

  func oPASONFOR_1_32.SetRadio()
    this.Parent.oContained.w_PASONFOR=trim(this.Parent.oContained.w_PASONFOR)
    this.value = ;
      iif(this.Parent.oContained.w_PASONFOR=='S',1,;
      0)
  endfunc

  add object oPAFLMIMA_1_33 as StdCheck with uid="USAMTWSIDS",rtseq=26,rtrep=.f.,left=468, top=197, caption="Controllo minimi e massimi in fatturazione",;
    ToolTipText = "Se attivo, al salvataggio di un nuovo documento, controlla che il valore delle prestazioni sia compreso tra i minimi e i massimi",;
    HelpContextID = 191485129,;
    cFormVar="w_PAFLMIMA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAFLMIMA_1_33.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPAFLMIMA_1_33.GetRadio()
    this.Parent.oContained.w_PAFLMIMA = this.RadioValue()
    return .t.
  endfunc

  func oPAFLMIMA_1_33.SetRadio()
    this.Parent.oContained.w_PAFLMIMA=trim(this.Parent.oContained.w_PAFLMIMA)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLMIMA=='S',1,;
      0)
  endfunc

  add object oPAGENPRE_1_34 as StdCheck with uid="FOGWWEZKXI",rtseq=27,rtrep=.f.,left=752, top=197, caption="Parcellazione semplificata",;
    ToolTipText = "Se attivo le prestazioni completate saranno registrate nell'anagrafica delle prestazioni, diversamente sar� generato un documento interno",;
    HelpContextID = 73450693,;
    cFormVar="w_PAGENPRE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAGENPRE_1_34.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPAGENPRE_1_34.GetRadio()
    this.Parent.oContained.w_PAGENPRE = this.RadioValue()
    return .t.
  endfunc

  func oPAGENPRE_1_34.SetRadio()
    this.Parent.oContained.w_PAGENPRE=trim(this.Parent.oContained.w_PAGENPRE)
    this.value = ;
      iif(this.Parent.oContained.w_PAGENPRE=='S',1,;
      0)
  endfunc

  add object oPAFLGMSK_1_35 as StdCheck with uid="BHSTICMJJT",rtseq=28,rtrep=.f.,left=205, top=216, caption="Maschera per righe spesa/ant. collegate ",;
    ToolTipText = "Se attivo, nelle attivit�, in inserimento provvisorio e in inserimento multiplo, apre la maschera per caricare la spesa e l'anticipazione collegata, diversamente crea le due righe collegate in modo silente",;
    HelpContextID = 137767745,;
    cFormVar="w_PAFLGMSK", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAFLGMSK_1_35.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPAFLGMSK_1_35.GetRadio()
    this.Parent.oContained.w_PAFLGMSK = this.RadioValue()
    return .t.
  endfunc

  func oPAFLGMSK_1_35.SetRadio()
    this.Parent.oContained.w_PAFLGMSK=trim(this.Parent.oContained.w_PAFLGMSK)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLGMSK=='S',1,;
      0)
  endfunc

  add object oPAFLCODO_1_36 as StdCheck with uid="FOGHUFNUKT",rtseq=29,rtrep=.f.,left=468, top=216, caption="Richiesta conferma documento stampato",;
    ToolTipText = "Se attivo, dopo la stampa verr� richiesto se si vuole confermare o eliminare il documento stampato",;
    HelpContextID = 167127877,;
    cFormVar="w_PAFLCODO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAFLCODO_1_36.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPAFLCODO_1_36.GetRadio()
    this.Parent.oContained.w_PAFLCODO = this.RadioValue()
    return .t.
  endfunc

  func oPAFLCODO_1_36.SetRadio()
    this.Parent.oContained.w_PAFLCODO=trim(this.Parent.oContained.w_PAFLCODO)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLCODO=='S',1,;
      0)
  endfunc

  add object oPAVISPRE_1_37 as StdCheck with uid="QGZTPDHQMN",rtseq=30,rtrep=.f.,left=752, top=216, caption="Visualizza seriali prestazioni",;
    ToolTipText = "Se attivo, al completamento delle prestazioni verr� visualizzato un elenco con il relativo seriale",;
    HelpContextID = 67884229,;
    cFormVar="w_PAVISPRE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAVISPRE_1_37.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPAVISPRE_1_37.GetRadio()
    this.Parent.oContained.w_PAVISPRE = this.RadioValue()
    return .t.
  endfunc

  func oPAVISPRE_1_37.SetRadio()
    this.Parent.oContained.w_PAVISPRE=trim(this.Parent.oContained.w_PAVISPRE)
    this.value = ;
      iif(this.Parent.oContained.w_PAVISPRE=='S',1,;
      0)
  endfunc

  func oPAVISPRE_1_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PAGENPRE = 'S')
    endwith
   endif
  endfunc


  add object oPAFILDAT_1_38 as StdCombo with uid="QRDDSYBLFI",rtseq=31,rtrep=.f.,left=205,top=248,width=153,height=21;
    , ToolTipText = "Permette di definire la data iniziale di selezione nelle ricerche di parcellazione";
    , HelpContextID = 8180918;
    , cFormVar="w_PAFILDAT",RowSource=""+"Data vuota,"+"1 Gen. anno precedente,"+"1 Gen. anno in corso,"+"Giorni indietro,"+"Data odierna", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPAFILDAT_1_38.RadioValue()
    return(iif(this.value =1,'V',;
    iif(this.value =2,'P',;
    iif(this.value =3,'I',;
    iif(this.value =4,'G',;
    iif(this.value =5,'O',;
    space(1)))))))
  endfunc
  func oPAFILDAT_1_38.GetRadio()
    this.Parent.oContained.w_PAFILDAT = this.RadioValue()
    return .t.
  endfunc

  func oPAFILDAT_1_38.SetRadio()
    this.Parent.oContained.w_PAFILDAT=trim(this.Parent.oContained.w_PAFILDAT)
    this.value = ;
      iif(this.Parent.oContained.w_PAFILDAT=='V',1,;
      iif(this.Parent.oContained.w_PAFILDAT=='P',2,;
      iif(this.Parent.oContained.w_PAFILDAT=='I',3,;
      iif(this.Parent.oContained.w_PAFILDAT=='G',4,;
      iif(this.Parent.oContained.w_PAFILDAT=='O',5,;
      0)))))
  endfunc

  add object oPAGGPREC_1_39 as StdField with uid="YORPZBPWEL",rtseq=32,rtrep=.f.,;
    cFormVar = "w_PAGGPREC", cQueryName = "PAGGPREC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero di giorni da sottrarre alla data di sistema",;
    HelpContextID = 230767417,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=361, Top=248, cSayPict="'999'", cGetPict="'999'"

  func oPAGGPREC_1_39.mHide()
    with this.Parent.oContained
      return (.w_PAFILDAT <> 'G')
    endwith
  endfunc

  add object oPAFLTOTS_1_40 as StdCheck with uid="PBVAPTMQTG",rtseq=33,rtrep=.f.,left=468, top=252, caption="Visualizza totale scadenze in elenco per cassa",;
    ToolTipText = "Se attivo, vengono visualizzati i campi relativi al Totale scadenze nell'Elenco costi e ricavi per cassa",;
    HelpContextID = 184953673,;
    cFormVar="w_PAFLTOTS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAFLTOTS_1_40.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPAFLTOTS_1_40.GetRadio()
    this.Parent.oContained.w_PAFLTOTS = this.RadioValue()
    return .t.
  endfunc

  func oPAFLTOTS_1_40.SetRadio()
    this.Parent.oContained.w_PAFLTOTS=trim(this.Parent.oContained.w_PAFLTOTS)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLTOTS=='S',1,;
      0)
  endfunc

  func oPAFLTOTS_1_40.mHide()
    with this.Parent.oContained
      return (g_COGE<>'S')
    endwith
  endfunc

  add object oPACAUPRE_1_41 as StdField with uid="HTNHTKRHWM",rtseq=34,rtrep=.f.,;
    cFormVar = "w_PACAUPRE", cQueryName = "PACAUPRE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale associata alle nuove prestazioni",;
    HelpContextID = 66389189,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=835, Top=242, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_PACAUPRE"

  func oPACAUPRE_1_41.mHide()
    with this.Parent.oContained
      return (.w_PAGENPRE<>'S')
    endwith
  endfunc

  func oPACAUPRE_1_41.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_41('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACAUPRE_1_41.ecpDrop(oSource)
    this.Parent.oContained.link_1_41('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPACAUPRE_1_41.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oPACAUPRE_1_41'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oPANOEDES_1_42 as StdCheck with uid="CYSVGWSQNB",rtseq=35,rtrep=.f.,left=468, top=234, caption="Non modificare descrizione prestazioni",;
    ToolTipText = "Se attivo rende non editabile descrizione delle prestazioni",;
    HelpContextID = 253340489,;
    cFormVar="w_PANOEDES", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPANOEDES_1_42.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPANOEDES_1_42.GetRadio()
    this.Parent.oContained.w_PANOEDES = this.RadioValue()
    return .t.
  endfunc

  func oPANOEDES_1_42.SetRadio()
    this.Parent.oContained.w_PANOEDES=trim(this.Parent.oContained.w_PANOEDES)
    this.value = ;
      iif(this.Parent.oContained.w_PANOEDES=='S',1,;
      0)
  endfunc

  func oPANOEDES_1_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (cp_IsAdministrator())
    endwith
   endif
  endfunc

  add object oPAPRESPE_1_44 as StdField with uid="HCWKCHJWPM",rtseq=36,rtrep=.f.,;
    cFormVar = "w_PAPRESPE", cQueryName = "PAPRESPE",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Prestazione inesistente od obsoleta o non di tipo spesa",;
    ToolTipText = "Codice predefinito per la voce di spesa da collegare alle tariffe che hanno il flag spesa e anticipazione collegate attivo",;
    HelpContextID = 31667397,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=205, Top=272, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_BZA", oKey_1_1="CACODICE", oKey_1_2="this.w_PAPRESPE"

  func oPAPRESPE_1_44.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_44('Part',this)
    endwith
    return bRes
  endfunc

  proc oPAPRESPE_1_44.ecpDrop(oSource)
    this.Parent.oContained.link_1_44('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPAPRESPE_1_44.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oPAPRESPE_1_44'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Prestazioni",'Inspre.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oPAPRESPE_1_44.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_PAPRESPE
     i_obj.ecpSave()
  endproc

  add object oPAPREANT_1_45 as StdField with uid="MFSGRSMTLE",rtseq=37,rtrep=.f.,;
    cFormVar = "w_PAPREANT", cQueryName = "PAPREANT",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Prestazione inesistente od obsoleta o non di tipo anticipazione",;
    ToolTipText = "Codice predefinito per la voce di anticipazione da collegare alle tariffe che hanno il flag Spesa e anticipazione collegate attivo",;
    HelpContextID = 65221814,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=205, Top=297, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_BZA", oKey_1_1="CACODICE", oKey_1_2="this.w_PAPREANT"

  func oPAPREANT_1_45.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_45('Part',this)
    endwith
    return bRes
  endfunc

  proc oPAPREANT_1_45.ecpDrop(oSource)
    this.Parent.oContained.link_1_45('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPAPREANT_1_45.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oPAPREANT_1_45'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Prestazioni",'Inspre.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oPAPREANT_1_45.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_PAPREANT
     i_obj.ecpSave()
  endproc

  add object oDESSPE_1_46 as StdField with uid="PRKXQIOYVV",rtseq=38,rtrep=.f.,;
    cFormVar = "w_DESSPE", cQueryName = "DESSPE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 254935498,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=361, Top=272, InputMask=replicate('X',40)

  add object oDESANT_1_48 as StdField with uid="JOTRIAYGLM",rtseq=39,rtrep=.f.,;
    cFormVar = "w_DESANT", cQueryName = "DESANT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 6554058,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=361, Top=297, InputMask=replicate('X',40)

  add object oPAF23PRE_1_52 as StdField with uid="GRDUWAVEBW",rtseq=51,rtrep=.f.,;
    cFormVar = "w_PAF23PRE", cQueryName = "PAF23PRE",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Prestazione inesistente od obsoleta",;
    ToolTipText = "Diritto/onorario relativo al versamento del contributo unificato utilizzato dalla procedura F23- modelli di pagamento",;
    HelpContextID = 103011525,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=205, Top=322, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_BZA", oKey_1_1="CACODICE", oKey_1_2="this.w_PAF23PRE"

  func oPAF23PRE_1_52.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_52('Part',this)
    endwith
    return bRes
  endfunc

  proc oPAF23PRE_1_52.ecpDrop(oSource)
    this.Parent.oContained.link_1_52('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPAF23PRE_1_52.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oPAF23PRE_1_52'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Prestazioni",'Inspre.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oPAF23PRE_1_52.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_PAF23PRE
     i_obj.ecpSave()
  endproc

  add object oDESF23PRE_1_53 as StdField with uid="CTYTHTPUID",rtseq=52,rtrep=.f.,;
    cFormVar = "w_DESF23PRE", cQueryName = "DESF23PRE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 52362536,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=361, Top=322, InputMask=replicate('X',40)

  add object oDESF23SPE_1_54 as StdField with uid="VPXBNJGCUA",rtseq=53,rtrep=.f.,;
    cFormVar = "w_DESF23SPE", cQueryName = "DESF23SPE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 52362538,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=361, Top=347, InputMask=replicate('X',40)

  add object oPAF23SPE_1_57 as StdField with uid="TONZZJJPEU",rtseq=54,rtrep=.f.,;
    cFormVar = "w_PAF23SPE", cQueryName = "PAF23SPE",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Prestazione inesistente od obsoleta o non di tipo anticipazione",;
    ToolTipText = "Anticipazione relativa all'importo del contributo unificato utilizzato dalla procedura F23- modelli di pagamento",;
    HelpContextID = 52679877,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=205, Top=347, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_BZA", oKey_1_1="CACODICE", oKey_1_2="this.w_PAF23SPE"

  func oPAF23SPE_1_57.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_57('Part',this)
    endwith
    return bRes
  endfunc

  proc oPAF23SPE_1_57.ecpDrop(oSource)
    this.Parent.oContained.link_1_57('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPAF23SPE_1_57.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oPAF23SPE_1_57'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Prestazioni",'Inspre.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oPAF23SPE_1_57.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_PAF23SPE
     i_obj.ecpSave()
  endproc

  add object oDESIVA_1_67 as StdField with uid="RCUFCSWUED",rtseq=61,rtrep=.f.,;
    cFormVar = "w_DESIVA", cQueryName = "DESIVA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 47972810,;
   bGlobalFont=.t.,;
    Height=21, Width=238, Left=259, Top=174, InputMask=replicate('X',35)

  add object oStr_1_4 as StdString with uid="GSVKAMMBAY",Visible=.t., Left=38, Top=6,;
    Alignment=1, Width=161, Height=18,;
    Caption="Inizio validit� listino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="DHSFKWFLFN",Visible=.t., Left=275, Top=6,;
    Alignment=1, Width=161, Height=18,;
    Caption="Fine validit� listino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="ACUERKETEM",Visible=.t., Left=38, Top=30,;
    Alignment=1, Width=161, Height=18,;
    Caption="Codice tributo IRPEF:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="SECHHJTYYD",Visible=.t., Left=38, Top=55,;
    Alignment=1, Width=161, Height=18,;
    Caption="Codice IVA cassa previdenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="HMYEZEBZFH",Visible=.t., Left=38, Top=80,;
    Alignment=1, Width=161, Height=18,;
    Caption="Codice IVA spese generali:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="ZYNGBYGZDE",Visible=.t., Left=38, Top=128,;
    Alignment=1, Width=161, Height=18,;
    Caption="Prestazione per acconto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="GLSNHZWALN",Visible=.t., Left=38, Top=151,;
    Alignment=1, Width=161, Height=18,;
    Caption="Prestazione onorario a forfait:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="THLGATPFTE",Visible=.t., Left=43, Top=250,;
    Alignment=1, Width=156, Height=18,;
    Caption="Data iniziale parcellazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="VUOMIBHHPU",Visible=.t., Left=43, Top=274,;
    Alignment=1, Width=156, Height=18,;
    Caption="Spesa da collegare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="UFMWTJAFGV",Visible=.t., Left=35, Top=299,;
    Alignment=1, Width=164, Height=18,;
    Caption="Anticipazione da collegare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="DQKHCNXYZV",Visible=.t., Left=68, Top=324,;
    Alignment=1, Width=131, Height=18,;
    Caption="Cont. unificato - Dir/ono:"  ;
  , bGlobalFont=.t.

  add object oStr_1_56 as StdString with uid="PNMCBFOWAT",Visible=.t., Left=25, Top=349,;
    Alignment=1, Width=174, Height=18,;
    Caption="Cont. unificato - Anticipazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_65 as StdString with uid="SIYSQONHMT",Visible=.t., Left=526, Top=8,;
    Alignment=1, Width=161, Height=18,;
    Caption="Codici IVA da sostituire:"  ;
  , bGlobalFont=.t.

  add object oStr_1_66 as StdString with uid="FGVURXSXKF",Visible=.t., Left=38, Top=174,;
    Alignment=1, Width=161, Height=18,;
    Caption="Codice iva a forfait:"  ;
  , bGlobalFont=.t.

  add object oStr_1_70 as StdString with uid="WPCGJLHZHK",Visible=.t., Left=739, Top=242,;
    Alignment=1, Width=94, Height=18,;
    Caption="Causale Prest.:"  ;
  , bGlobalFont=.t.

  func oStr_1_70.mHide()
    with this.Parent.oContained
      return (.w_PAGENPRE<>'S')
    endwith
  endfunc
enddefine
define class tgsma_apaPag2 as StdContainer
  Width  = 934
  height = 393
  stdWidth  = 934
  stdheight = 393
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPACAUANT_2_1 as StdField with uid="GRROVRNKGB",rtseq=81,rtrep=.f.,;
    cFormVar = "w_PACAUANT", cQueryName = "PACAUANT",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Inserire la causale contabile per contabilizzare le anticipazioni ",;
    HelpContextID = 49611958,;
   bGlobalFont=.t.,;
    Height=21, Width=68, Left=171, Top=29, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="gscg_acc", oKey_1_1="CCCODICE", oKey_1_2="this.w_PACAUANT"

  func oPACAUANT_2_1.mHide()
    with this.Parent.oContained
      return (g_COGE<>'S')
    endwith
  endfunc

  func oPACAUANT_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACAUANT_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPACAUANT_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oPACAUANT_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'gscg_acc',"Causali contabili",'',this.parent.oContained
  endproc
  proc oPACAUANT_2_1.mZoomOnZoom
    local i_obj
    i_obj=gscg_acc()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_PACAUANT
     i_obj.ecpSave()
  endproc

  add object oPACONANT_2_2 as StdField with uid="KVPEIUMBEA",rtseq=82,rtrep=.f.,;
    cFormVar = "w_PACONANT", cQueryName = "PACONANT",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Inserire il conto relativo alle anticipazioni (es: anticipazioni c/ terzi)",;
    HelpContextID = 56034486,;
   bGlobalFont=.t.,;
    Height=21, Width=127, Left=171, Top=55, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="gsar_api", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_PACONANT"

  func oPACONANT_2_2.mHide()
    with this.Parent.oContained
      return (g_COGE<>'S')
    endwith
  endfunc

  func oPACONANT_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACONANT_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPACONANT_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPACONANT_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_api',"Conti",'',this.parent.oContained
  endproc
  proc oPACONANT_2_2.mZoomOnZoom
    local i_obj
    i_obj=gsar_api()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_PACONANT
     i_obj.ecpSave()
  endproc

  add object oDESCON_2_4 as StdField with uid="WEPJRXUPXT",rtseq=83,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 106037706,;
   bGlobalFont=.t.,;
    Height=21, Width=442, Left=313, Top=55, InputMask=replicate('X',60)

  func oDESCON_2_4.mHide()
    with this.Parent.oContained
      return (g_COGE<>'S')
    endwith
  endfunc

  add object oPACONDEF_2_5 as StdField with uid="IQCVXUAZAR",rtseq=84,rtrep=.f.,;
    cFormVar = "w_PACONDEF", cQueryName = "PACONDEF",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Inserire il conto di contropartita delle anticipazioni (es: cassa)",;
    HelpContextID = 262732604,;
   bGlobalFont=.t.,;
    Height=21, Width=127, Left=171, Top=81, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="gsar_api", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_PACONDEF"

  func oPACONDEF_2_5.mHide()
    with this.Parent.oContained
      return (g_COGE<>'S')
    endwith
  endfunc

  func oPACONDEF_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACONDEF_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPACONDEF_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPACONDEF_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_api',"Conti",'',this.parent.oContained
  endproc
  proc oPACONDEF_2_5.mZoomOnZoom
    local i_obj
    i_obj=gsar_api()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_PACONDEF
     i_obj.ecpSave()
  endproc

  add object oDEFANT_2_7 as StdField with uid="DWSIRZRTKI",rtseq=85,rtrep=.f.,;
    cFormVar = "w_DEFANT", cQueryName = "DEFANT",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 6607306,;
   bGlobalFont=.t.,;
    Height=21, Width=442, Left=313, Top=81, InputMask=replicate('X',60)

  func oDEFANT_2_7.mHide()
    with this.Parent.oContained
      return (g_COGE<>'S')
    endwith
  endfunc

  add object oDESCAU_2_9 as StdField with uid="PKWBWELIXX",rtseq=86,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 3277258,;
   bGlobalFont=.t.,;
    Height=21, Width=442, Left=313, Top=29, InputMask=replicate('X',35)

  func oDESCAU_2_9.mHide()
    with this.Parent.oContained
      return (g_COGE<>'S')
    endwith
  endfunc

  add object oPACONSPE_2_10 as StdField with uid="EYIWBEUPIZ",rtseq=87,rtrep=.f.,;
    cFormVar = "w_PACONSPE", cQueryName = "PACONSPE",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Inserire il conto relativo alle spese (es: spese viaggio)",;
    HelpContextID = 22480069,;
   bGlobalFont=.t.,;
    Height=21, Width=127, Left=171, Top=107, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="gsar_api", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_PACONSPE"

  func oPACONSPE_2_10.mHide()
    with this.Parent.oContained
      return (g_COGE<>'S')
    endwith
  endfunc

  func oPACONSPE_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACONSPE_2_10.ecpDrop(oSource)
    this.Parent.oContained.link_2_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPACONSPE_2_10.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPACONSPE_2_10'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_api',"Conti",'',this.parent.oContained
  endproc
  proc oPACONSPE_2_10.mZoomOnZoom
    local i_obj
    i_obj=gsar_api()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_PACONSPE
     i_obj.ecpSave()
  endproc

  add object oDECSPE_2_12 as StdField with uid="ZCNCLZJYQN",rtseq=88,rtrep=.f.,;
    cFormVar = "w_DECSPE", cQueryName = "DECSPE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 255001034,;
   bGlobalFont=.t.,;
    Height=21, Width=442, Left=313, Top=107, InputMask=replicate('X',60)

  func oDECSPE_2_12.mHide()
    with this.Parent.oContained
      return (g_COGE<>'S')
    endwith
  endfunc

  add object oPADEFSPE_2_13 as StdField with uid="AQNCFOYJUV",rtseq=89,rtrep=.f.,;
    cFormVar = "w_PADEFSPE", cQueryName = "PADEFSPE",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Inserire il conto di contropartita delle spese (es: cassa)",;
    HelpContextID = 31519941,;
   bGlobalFont=.t.,;
    Height=21, Width=127, Left=171, Top=133, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="gsar_api", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_PADEFSPE"

  func oPADEFSPE_2_13.mHide()
    with this.Parent.oContained
      return (g_COGE<>'S')
    endwith
  endfunc

  func oPADEFSPE_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oPADEFSPE_2_13.ecpDrop(oSource)
    this.Parent.oContained.link_2_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPADEFSPE_2_13.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPADEFSPE_2_13'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_api',"Conti",'',this.parent.oContained
  endproc
  proc oPADEFSPE_2_13.mZoomOnZoom
    local i_obj
    i_obj=gsar_api()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_PADEFSPE
     i_obj.ecpSave()
  endproc

  add object oDEFSPE_2_15 as StdField with uid="UOHXUXPIKW",rtseq=90,rtrep=.f.,;
    cFormVar = "w_DEFSPE", cQueryName = "DEFSPE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 254988746,;
   bGlobalFont=.t.,;
    Height=21, Width=442, Left=313, Top=133, InputMask=replicate('X',60)

  func oDEFSPE_2_15.mHide()
    with this.Parent.oContained
      return (g_COGE<>'S')
    endwith
  endfunc


  add object oLinkPC_2_16 as StdButton with uid="WBABYITJLF",left=776, top=29, width=48,height=45,;
    CpPicture="bmp\dm_archivi.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per specificare i conti e le contropartite per la contabilizzazione delle anticipazioni e delle spese.";
    , HelpContextID = 201247345;
    , caption='\<Dettaglio';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_2_16.Click()
      this.Parent.oContained.gsal_mpa.LinkPCClick()
    endproc

  func oLinkPC_2_16.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (Empty(Alltrim(.w_PACONSPE+.w_PACONANT)) or Empty(.w_PACAUANT) or g_COGE<>'S')
     endwith
    endif
  endfunc


  add object oLinkPC_2_17 as StdButton with uid="XOCHMGHBNC",left=830, top=29, width=48,height=45,;
    CpPicture="bmp\dm_archivi.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per accedere all'elenco causali contabili escluse dalla stampa cronologico";
    , HelpContextID = 268122708;
    , caption='\<Cau.esc.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_2_17.Click()
      this.Parent.oContained.gsal_mca.LinkPCClick()
    endproc

  func oLinkPC_2_17.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_COGE<>'S')
     endwith
    endif
  endfunc

  add object oStr_2_3 as StdString with uid="HCZGVBPAOB",Visible=.t., Left=7, Top=57,;
    Alignment=1, Width=161, Height=18,;
    Caption="Conto anticipazione:"  ;
  , bGlobalFont=.t.

  func oStr_2_3.mHide()
    with this.Parent.oContained
      return (g_COGE<>'S')
    endwith
  endfunc

  add object oStr_2_6 as StdString with uid="FOTKOITUDJ",Visible=.t., Left=5, Top=83,;
    Alignment=1, Width=163, Height=18,;
    Caption="Controp. anticipazione:"  ;
  , bGlobalFont=.t.

  func oStr_2_6.mHide()
    with this.Parent.oContained
      return (g_COGE<>'S')
    endwith
  endfunc

  add object oStr_2_8 as StdString with uid="CQLBVTRJSN",Visible=.t., Left=9, Top=32,;
    Alignment=1, Width=159, Height=18,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  func oStr_2_8.mHide()
    with this.Parent.oContained
      return (g_COGE<>'S')
    endwith
  endfunc

  add object oStr_2_11 as StdString with uid="FCDCNEYGNU",Visible=.t., Left=7, Top=109,;
    Alignment=1, Width=161, Height=18,;
    Caption="Conto spese:"  ;
  , bGlobalFont=.t.

  func oStr_2_11.mHide()
    with this.Parent.oContained
      return (g_COGE<>'S')
    endwith
  endfunc

  add object oStr_2_14 as StdString with uid="UTZHJCNVUV",Visible=.t., Left=5, Top=135,;
    Alignment=1, Width=163, Height=18,;
    Caption="Controp. spese:"  ;
  , bGlobalFont=.t.

  func oStr_2_14.mHide()
    with this.Parent.oContained
      return (g_COGE<>'S')
    endwith
  endfunc
enddefine
define class tgsma_apaPag3 as StdContainer
  Width  = 934
  height = 393
  stdWidth  = 934
  stdheight = 393
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oPATIPARC_3_1 as StdCombo with uid="QJQQQSVIXO",rtseq=42,rtrep=.f.,left=166,top=51,width=212,height=22;
    , ToolTipText = "Scegliere copia file se si desidera archiviare un file utilizzando un numero progressivo e il nome della pratica,scegliere collegamento se si vuole archiviare un file con il nome del modello utilizzato";
    , HelpContextID = 54260935;
    , cFormVar="w_PATIPARC",RowSource=""+"Collegamento (Nome modello),"+"Copia (N. progressivo+nome pratica)", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oPATIPARC_3_1.RadioValue()
    return(iif(this.value =1,'L',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oPATIPARC_3_1.GetRadio()
    this.Parent.oContained.w_PATIPARC = this.RadioValue()
    return .t.
  endfunc

  func oPATIPARC_3_1.SetRadio()
    this.Parent.oContained.w_PATIPARC=trim(this.Parent.oContained.w_PATIPARC)
    this.value = ;
      iif(this.Parent.oContained.w_PATIPARC=='L',1,;
      iif(this.Parent.oContained.w_PATIPARC=='F',2,;
      0))
  endfunc

  func oPATIPARC_3_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PATIPARC<>'E' or g_AEDS='S')
    endwith
    return bRes
  endfunc

  add object oPACLASSE_3_2 as StdField with uid="WYEKFYPQWW",rtseq=43,rtrep=.f.,;
    cFormVar = "w_PACLASSE", cQueryName = "PACLASSE",;
    bObbl = .f. , nPag = 3, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Classe documentale non valida",;
    ToolTipText = "Classe documentale da proporre in fase di caricamento di un modello",;
    HelpContextID = 232127291,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=506, Top=51, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="PROMCLAS", cZoomOnZoom="gsut_mcd", oKey_1_1="CDCODCLA", oKey_1_2="this.w_PACLASSE"

  func oPACLASSE_3_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACLASSE_3_2.ecpDrop(oSource)
    this.Parent.oContained.link_3_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPACLASSE_3_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PROMCLAS','*','CDCODCLA',cp_AbsName(this.parent,'oPACLASSE_3_2'),iif(empty(i_cWhere),.f.,i_cWhere),'gsut_mcd',"Classi documentali",'GSUT1AMD.PROMCLAS_VZM',this.parent.oContained
  endproc
  proc oPACLASSE_3_2.mZoomOnZoom
    local i_obj
    i_obj=gsut_mcd()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CDCODCLA=this.parent.oContained.w_PACLASSE
     i_obj.ecpSave()
  endproc


  add object oPATIPSEN_3_3 as StdCombo with uid="VXBDWPDFWQ",rtseq=44,rtrep=.f.,left=166,top=83,width=212,height=22;
    , ToolTipText = "Modalit� di archiviazione predefinita da utilizzare per archiviare un documento tramite la funzione di invia a alteregotop tramite il tx sul file da archiviare";
    , HelpContextID = 247728964;
    , cFormVar="w_PATIPSEN",RowSource=""+"Collegamento,"+"Copia,"+"Sposta", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oPATIPSEN_3_3.RadioValue()
    return(iif(this.value =1,'L',;
    iif(this.value =2,'F',;
    iif(this.value =3,'S',;
    space(1)))))
  endfunc
  func oPATIPSEN_3_3.GetRadio()
    this.Parent.oContained.w_PATIPSEN = this.RadioValue()
    return .t.
  endfunc

  func oPATIPSEN_3_3.SetRadio()
    this.Parent.oContained.w_PATIPSEN=trim(this.Parent.oContained.w_PATIPSEN)
    this.value = ;
      iif(this.Parent.oContained.w_PATIPSEN=='L',1,;
      iif(this.Parent.oContained.w_PATIPSEN=='F',2,;
      iif(this.Parent.oContained.w_PATIPSEN=='S',3,;
      0)))
  endfunc


  add object oPAORDFIL_3_4 as StdCombo with uid="WUVFRRJPTL",rtseq=45,rtrep=.f.,left=166,top=115,width=212,height=22;
    , HelpContextID = 17611586;
    , cFormVar="w_PAORDFIL",RowSource=""+"Data archiviazione file,"+"Descrizione file", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oPAORDFIL_3_4.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oPAORDFIL_3_4.GetRadio()
    this.Parent.oContained.w_PAORDFIL = this.RadioValue()
    return .t.
  endfunc

  func oPAORDFIL_3_4.SetRadio()
    this.Parent.oContained.w_PAORDFIL=trim(this.Parent.oContained.w_PAORDFIL)
    this.value = ;
      iif(this.Parent.oContained.w_PAORDFIL=='D',1,;
      iif(this.Parent.oContained.w_PAORDFIL=='F',2,;
      0))
  endfunc

  add object oPAHIDARC_3_5 as StdCheck with uid="AZHWIYPMKO",rtseq=46,rtrep=.f.,left=167, top=149, caption="Nascondi bottone archivia",;
    ToolTipText = "Se attivo nasconde bottone archivia nella ricerca documenti",;
    HelpContextID = 66892999,;
    cFormVar="w_PAHIDARC", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oPAHIDARC_3_5.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPAHIDARC_3_5.GetRadio()
    this.Parent.oContained.w_PAHIDARC = this.RadioValue()
    return .t.
  endfunc

  func oPAHIDARC_3_5.SetRadio()
    this.Parent.oContained.w_PAHIDARC=trim(this.Parent.oContained.w_PAHIDARC)
    this.value = ;
      iif(this.Parent.oContained.w_PAHIDARC=='S',1,;
      0)
  endfunc

  add object oPAHIDSCA_3_6 as StdCheck with uid="KRESLVJKAC",rtseq=47,rtrep=.f.,left=167, top=172, caption="Nascondi bottone scanner",;
    ToolTipText = "Se attivo nasconde bottone archivia nella ricerca documenti",;
    HelpContextID = 235096887,;
    cFormVar="w_PAHIDSCA", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oPAHIDSCA_3_6.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPAHIDSCA_3_6.GetRadio()
    this.Parent.oContained.w_PAHIDSCA = this.RadioValue()
    return .t.
  endfunc

  func oPAHIDSCA_3_6.SetRadio()
    this.Parent.oContained.w_PAHIDSCA=trim(this.Parent.oContained.w_PAHIDSCA)
    this.value = ;
      iif(this.Parent.oContained.w_PAHIDSCA=='S',1,;
      0)
  endfunc

  add object oPAHIDOPE_3_7 as StdCheck with uid="DMCPXKGKCR",rtseq=48,rtrep=.f.,left=167, top=195, caption="Nascondi bottone apri file per nuovo documento",;
    ToolTipText = "Se attivo nasconde bottone di apertura file all'interno della maschera di nuovo documento",;
    HelpContextID = 100447429,;
    cFormVar="w_PAHIDOPE", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oPAHIDOPE_3_7.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPAHIDOPE_3_7.GetRadio()
    this.Parent.oContained.w_PAHIDOPE = this.RadioValue()
    return .t.
  endfunc

  func oPAHIDOPE_3_7.SetRadio()
    this.Parent.oContained.w_PAHIDOPE=trim(this.Parent.oContained.w_PAHIDOPE)
    this.value = ;
      iif(this.Parent.oContained.w_PAHIDOPE=='S',1,;
      0)
  endfunc

  add object oPAVERDOC_3_10 as StdCheck with uid="TOMTHFMVGS",rtseq=49,rtrep=.f.,left=167, top=218, caption="Copia di backup documenti importati",;
    ToolTipText = "Se attivo crea una copia del file prima di importarlo da archivio zip",;
    HelpContextID = 2086087,;
    cFormVar="w_PAVERDOC", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oPAVERDOC_3_10.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPAVERDOC_3_10.GetRadio()
    this.Parent.oContained.w_PAVERDOC = this.RadioValue()
    return .t.
  endfunc

  func oPAVERDOC_3_10.SetRadio()
    this.Parent.oContained.w_PAVERDOC=trim(this.Parent.oContained.w_PAVERDOC)
    this.value = ;
      iif(this.Parent.oContained.w_PAVERDOC=='S',1,;
      0)
  endfunc

  add object oPADATZIP_3_11 as StdCheck with uid="KLCTCSMUIL",rtseq=50,rtrep=.f.,left=167, top=241, caption="Applica criterio data creazione zip",;
    ToolTipText = "Se attivo crea una copia del file prima di importarlo da archivio zip",;
    HelpContextID = 100338502,;
    cFormVar="w_PADATZIP", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oPADATZIP_3_11.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPADATZIP_3_11.GetRadio()
    this.Parent.oContained.w_PADATZIP = this.RadioValue()
    return .t.
  endfunc

  func oPADATZIP_3_11.SetRadio()
    this.Parent.oContained.w_PADATZIP=trim(this.Parent.oContained.w_PADATZIP)
    this.value = ;
      iif(this.Parent.oContained.w_PADATZIP=='S',1,;
      0)
  endfunc

  add object oPAFLGDIK_3_12 as StdCheck with uid="YQXECXMUGL",rtseq=55,rtrep=.f.,left=167, top=264, caption="Applica infocamere Dike",;
    ToolTipText = "Se attivo applica infocamere Dike",;
    HelpContextID = 255208257,;
    cFormVar="w_PAFLGDIK", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oPAFLGDIK_3_12.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPAFLGDIK_3_12.GetRadio()
    this.Parent.oContained.w_PAFLGDIK = this.RadioValue()
    return .t.
  endfunc

  func oPAFLGDIK_3_12.SetRadio()
    this.Parent.oContained.w_PAFLGDIK=trim(this.Parent.oContained.w_PAFLGDIK)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLGDIK=='S',1,;
      0)
  endfunc

  add object oPADMCOMP_3_13 as StdCheck with uid="XBSGBWPRIS",rtseq=74,rtrep=.f.,left=167, top=287, caption="Completa le prestazioni caricate da archiviazione documenti e 'Invia a'",;
    ToolTipText = "Se attivo, le prestazioni caricate saranno immediatamente completate",;
    HelpContextID = 101250234,;
    cFormVar="w_PADMCOMP", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oPADMCOMP_3_13.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPADMCOMP_3_13.GetRadio()
    this.Parent.oContained.w_PADMCOMP = this.RadioValue()
    return .t.
  endfunc

  func oPADMCOMP_3_13.SetRadio()
    this.Parent.oContained.w_PADMCOMP=trim(this.Parent.oContained.w_PADMCOMP)
    this.value = ;
      iif(this.Parent.oContained.w_PADMCOMP=='S',1,;
      0)
  endfunc

  add object oPANEWQRY_3_14 as StdCheck with uid="STJYWBBCID",rtseq=75,rtrep=.f.,left=167, top=310, caption="Abilita query per ruolo soggetti",;
    ToolTipText = "Se attivo, vengono utilizzate le query che definiscono le parole chiave dei soggetti interni ed esterni con il codice del ruolo",;
    HelpContextID = 47207601,;
    cFormVar="w_PANEWQRY", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oPANEWQRY_3_14.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPANEWQRY_3_14.GetRadio()
    this.Parent.oContained.w_PANEWQRY = this.RadioValue()
    return .t.
  endfunc

  func oPANEWQRY_3_14.SetRadio()
    this.Parent.oContained.w_PANEWQRY=trim(this.Parent.oContained.w_PANEWQRY)
    this.value = ;
      iif(this.Parent.oContained.w_PANEWQRY=='S',1,;
      0)
  endfunc

  add object oPAFLWDTP_3_15 as StdCheck with uid="PFILGIGGMW",rtseq=76,rtrep=.f.,left=167, top=333, caption="Crea origine dati di Word nella cartella Temp",;
    ToolTipText = "Se attivo, il file DBF contenente l'origine dati di word per la compilazione di documenti sar� creato sempre nella cartella temp dell'utente (opzione consigliata per l'utilizzo in terminal server) ",;
    HelpContextID = 3550022,;
    cFormVar="w_PAFLWDTP", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oPAFLWDTP_3_15.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPAFLWDTP_3_15.GetRadio()
    this.Parent.oContained.w_PAFLWDTP = this.RadioValue()
    return .t.
  endfunc

  func oPAFLWDTP_3_15.SetRadio()
    this.Parent.oContained.w_PAFLWDTP=trim(this.Parent.oContained.w_PAFLWDTP)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLWDTP=='S',1,;
      0)
  endfunc

  add object oDESCLA_3_18 as StdField with uid="QAYXTXHQLF",rtseq=91,rtrep=.f.,;
    cFormVar = "w_DESCLA", cQueryName = "DESCLA",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione classe documentale",;
    HelpContextID = 58851786,;
   bGlobalFont=.t.,;
    Height=21, Width=279, Left=627, Top=51, InputMask=replicate('X',50)

  add object oPAPATMOD_3_21 as StdField with uid="OJGANSGHLZ",rtseq=94,rtrep=.f.,;
    cFormVar = "w_PAPATMOD", cQueryName = "PAPATMOD",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Selezionare la cartella per il salvataggio dei modelli",;
    HelpContextID = 117716166,;
   bGlobalFont=.t.,;
    Height=21, Width=400, Left=506, Top=83, InputMask=replicate('X',254)

  func oPAPATMOD_3_21.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_PACLASSE))
    endwith
  endfunc

  add object oPAQUERY_3_23 as StdField with uid="SZORNDXKDR",rtseq=95,rtrep=.f.,;
    cFormVar = "w_PAQUERY", cQueryName = "PAQUERY",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Query collegata al modello da proporre in fase di caricamento di un modello",;
    HelpContextID = 220191478,;
   bGlobalFont=.t.,;
    Height=21, Width=400, Left=506, Top=115, InputMask=replicate('X',254)

  func oPAQUERY_3_23.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_PACLASSE))
    endwith
  endfunc


  add object SelQue as cp_askfile with uid="ZPYLBBOQOY",left=910, top=115, width=22,height=22,;
    caption='...',;
   bGlobalFont=.t.,;
    var="w_QUERY",;
    nPag=3;
    , ToolTipText = "Premere per selezionare la query collegata";
    , HelpContextID = 159869482


  add object oBtn_3_30 as StdButton with uid="EGSLJKOSDU",left=910, top=84, width=22,height=22,;
    caption="...", nPag=3;
    , ToolTipText = "Premere per selezionare la cartella per il salvataggio dei modelli";
    , HelpContextID = 159869482;
  , bGlobalFont=.t.

    proc oBtn_3_30.Click()
      this.parent.oContained.NotifyEvent("SelDir")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_30.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_PACLASSE))
     endwith
    endif
  endfunc


  add object oPADATARC_3_36 as StdCombo with uid="TDQWPJWCFQ",rtseq=99,rtrep=.f.,left=166,top=360,width=212,height=22;
    , HelpContextID = 50656455;
    , cFormVar="w_PADATARC",RowSource=""+"Data archiviazione,"+"Data/ora invio mail", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oPADATARC_3_36.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'M',;
    space(1))))
  endfunc
  func oPADATARC_3_36.GetRadio()
    this.Parent.oContained.w_PADATARC = this.RadioValue()
    return .t.
  endfunc

  func oPADATARC_3_36.SetRadio()
    this.Parent.oContained.w_PADATARC=trim(this.Parent.oContained.w_PADATARC)
    this.value = ;
      iif(this.Parent.oContained.w_PADATARC=='A',1,;
      iif(this.Parent.oContained.w_PADATARC=='M',2,;
      0))
  endfunc

  add object oStr_3_8 as StdString with uid="QRXMCJVWMX",Visible=.t., Left=0, Top=54,;
    Alignment=1, Width=161, Height=18,;
    Caption="Modalit� di archiviazione:"  ;
  , bGlobalFont=.t.

  add object oStr_3_9 as StdString with uid="CSKZBRBVPL",Visible=.t., Left=1, Top=86,;
    Alignment=1, Width=160, Height=18,;
    Caption="Tipo archiviazione per invia a:"  ;
  , bGlobalFont=.t.

  add object oStr_3_16 as StdString with uid="TXVDLLNHXG",Visible=.t., Left=46, Top=116,;
    Alignment=1, Width=115, Height=18,;
    Caption="Ordinamento file per:"  ;
  , bGlobalFont=.t.

  add object oStr_3_17 as StdString with uid="NBGJRVYSBQ",Visible=.t., Left=385, Top=54,;
    Alignment=1, Width=118, Height=18,;
    Caption="Classe documentale:"  ;
  , bGlobalFont=.t.

  add object oStr_3_22 as StdString with uid="YBGFLCQTVZ",Visible=.t., Left=367, Top=86,;
    Alignment=1, Width=136, Height=18,;
    Caption="Percorso:"  ;
  , bGlobalFont=.t.

  func oStr_3_22.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_PACLASSE))
    endwith
  endfunc

  add object oStr_3_25 as StdString with uid="PERFLHHBRZ",Visible=.t., Left=367, Top=116,;
    Alignment=1, Width=136, Height=18,;
    Caption="Query collegata:"  ;
  , bGlobalFont=.t.

  func oStr_3_25.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_PACLASSE))
    endwith
  endfunc

  add object oStr_3_28 as StdString with uid="INVJFJQYDQ",Visible=.t., Left=411, Top=22,;
    Alignment=0, Width=159, Height=18,;
    Caption="Parametri per modelli pratica"  ;
  , bGlobalFont=.t.

  add object oStr_3_35 as StdString with uid="LYTKLYHNVQ",Visible=.t., Left=31, Top=360,;
    Alignment=1, Width=130, Height=18,;
    Caption="Data archiviazione mail:"  ;
  , bGlobalFont=.t.

  add object oBox_3_29 as StdBox with uid="VKFIFAMMTL",left=408, top=39, width=526,height=1
enddefine
define class tgsma_apaPag4 as StdContainer
  Width  = 934
  height = 393
  stdWidth  = 934
  stdheight = 393
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPACODPUN_4_1 as StdField with uid="BMZTRHPAZC",rtseq=62,rtrep=.f.,;
    cFormVar = "w_PACODPUN", cQueryName = "PACODPUN",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Punto di accesso predefinito per PolisWeb",;
    HelpContextID = 185137988,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=166, Top=60, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="PUN_ACCE", oKey_1_1="PRCODICE", oKey_1_2="this.w_PACODPUN"

  func oPACODPUN_4_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACODPUN_4_1.ecpDrop(oSource)
    this.Parent.oContained.link_4_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPACODPUN_4_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PUN_ACCE','*','PRCODICE',cp_AbsName(this.parent,'oPACODPUN_4_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Punti di accesso",'',this.parent.oContained
  endproc

  add object oPATIPUDI_4_6 as StdField with uid="XVUABJTVOI",rtseq=63,rtrep=.f.,;
    cFormVar = "w_PATIPUDI", cQueryName = "PATIPUDI",;
    bObbl = .f. , nPag = 4, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Tipo di attivit� che sar� utilizzato dalla procedura per la creazione automatica di un'attivit� partendo da un evento di Polisweb",;
    HelpContextID = 12847935,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=166, Top=112, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CAUMATTI", cZoomOnZoom="GSAG_MCA", oKey_1_1="CACODICE", oKey_1_2="this.w_PATIPUDI"

  func oPATIPUDI_4_6.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  func oPATIPUDI_4_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oPATIPUDI_4_6.ecpDrop(oSource)
    this.Parent.oContained.link_4_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPATIPUDI_4_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAUMATTI','*','CACODICE',cp_AbsName(this.parent,'oPATIPUDI_4_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MCA',"Tipi attivit�",'',this.parent.oContained
  endproc
  proc oPATIPUDI_4_6.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MCA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_PATIPUDI
     i_obj.ecpSave()
  endproc

  add object oPAPWBUDI_4_7 as StdField with uid="LDEBBAIJYR",rtseq=64,rtrep=.f.,;
    cFormVar = "w_PAPWBUDI", cQueryName = "PAPWBUDI",;
    bObbl = .f. , nPag = 4, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Tipo di attivit� che sar� utilizzato dalla procedura per la creazione automatica di un'attivit� partendo da un'udienza di Polisweb",;
    HelpContextID = 267504447,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=166, Top=138, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CAUMATTI", cZoomOnZoom="GSAG_MCA", oKey_1_1="CACODICE", oKey_1_2="this.w_PAPWBUDI"

  func oPAPWBUDI_4_7.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  func oPAPWBUDI_4_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oPAPWBUDI_4_7.ecpDrop(oSource)
    this.Parent.oContained.link_4_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPAPWBUDI_4_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAUMATTI','*','CACODICE',cp_AbsName(this.parent,'oPAPWBUDI_4_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MCA',"Tipi attivit�",'',this.parent.oContained
  endproc
  proc oPAPWBUDI_4_7.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MCA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_PAPWBUDI
     i_obj.ecpSave()
  endproc

  add object oPAPWBSCA_4_8 as StdField with uid="SOGOZGAKSF",rtseq=65,rtrep=.f.,;
    cFormVar = "w_PAPWBSCA", cQueryName = "PAPWBSCA",;
    bObbl = .f. , nPag = 4, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Tipo di attivit� che sar� utilizzato dalla procedura per la creazione automatica di un'attivit� partendo da una scadenza di Polisweb",;
    HelpContextID = 233950007,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=166, Top=164, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CAUMATTI", cZoomOnZoom="GSAG_MCA", oKey_1_1="CACODICE", oKey_1_2="this.w_PAPWBSCA"

  func oPAPWBSCA_4_8.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  func oPAPWBSCA_4_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oPAPWBSCA_4_8.ecpDrop(oSource)
    this.Parent.oContained.link_4_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPAPWBSCA_4_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAUMATTI','*','CACODICE',cp_AbsName(this.parent,'oPAPWBSCA_4_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MCA',"Tipi attivit�",'',this.parent.oContained
  endproc
  proc oPAPWBSCA_4_8.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MCA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_PAPWBSCA
     i_obj.ecpSave()
  endproc


  add object oPAPWBRUO_4_9 as StdTableCombo with uid="UYSSMSZZPA",rtseq=66,rtrep=.f.,left=166,top=190,width=181,height=22;
    , ToolTipText = "Definizione del ruolo all'interno delle pratiche che saranno inserite";
    , HelpContextID = 217172805;
    , cFormVar="w_PAPWBRUO",tablefilter="", bObbl = .f. , nPag = 4;
    , cTable='RUO_SOGI',cKey='RSCODRUO',cValue='RSDESRUO',cOrderBy='RSDESRUO',xDefault=space(5);
  , bGlobalFont=.t.


  add object oDESUDI_4_10 as StdField with uid="KVRFMKMDIC",rtseq=67,rtrep=.f.,;
    cFormVar = "w_DESUDI", cQueryName = "DESUDI",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 200278474,;
   bGlobalFont=.t.,;
    Height=21, Width=328, Left=355, Top=112, InputMask=replicate('X',254)

  func oDESUDI_4_10.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oPADATPOL_4_11 as StdField with uid="MLSMUJYKQK",rtseq=68,rtrep=.f.,;
    cFormVar = "w_PADATPOL", cQueryName = "PADATPOL",;
    bObbl = .f. , nPag = 4, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data ultima importazione eventi Polisweb",;
    HelpContextID = 67433662,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=607, Top=190

  add object oDESPWBUDI_4_12 as StdField with uid="WQAHQIBJZB",rtseq=69,rtrep=.f.,;
    cFormVar = "w_DESPWBUDI", cQueryName = "DESPWBUDI",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 238748426,;
   bGlobalFont=.t.,;
    Height=21, Width=328, Left=355, Top=138, InputMask=replicate('X',254)

  func oDESPWBUDI_4_12.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oDESPWBSCA_4_13 as StdField with uid="QFOYLECWCN",rtseq=70,rtrep=.f.,;
    cFormVar = "w_DESPWBSCA", cQueryName = "DESPWBSCA",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 29687159,;
   bGlobalFont=.t.,;
    Height=21, Width=328, Left=355, Top=164, InputMask=replicate('X',254)

  func oDESPWBSCA_4_13.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oPRINDIRI_4_18 as StdField with uid="HOYWEDEWFH",rtseq=71,rtrep=.f.,;
    cFormVar = "w_PRINDIRI", cQueryName = "PRINDIRI",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 200774593,;
   bGlobalFont=.t.,;
    Height=21, Width=517, Left=166, Top=86, InputMask=replicate('X',254)

  add object oPRDESCRI_4_19 as StdField with uid="GRLTFIRNOJ",rtseq=72,rtrep=.f.,;
    cFormVar = "w_PRDESCRI", cQueryName = "PRDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(80), bMultilanguage =  .f.,;
    HelpContextID = 17884097,;
   bGlobalFont=.t.,;
    Height=21, Width=440, Left=246, Top=60, InputMask=replicate('X',80)

  add object oPANOARCH_4_21 as StdCheck with uid="LXXYNJRULK",rtseq=77,rtrep=.f.,left=166, top=216, caption="Nascondi i fascicoli relativi a pratiche archiviate",;
    ToolTipText = "Se attivo, saranno nascosti i fascicoli relativi a pratiche archiviate",;
    HelpContextID = 215591742,;
    cFormVar="w_PANOARCH", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oPANOARCH_4_21.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oPANOARCH_4_21.GetRadio()
    this.Parent.oContained.w_PANOARCH = this.RadioValue()
    return .t.
  endfunc

  func oPANOARCH_4_21.SetRadio()
    this.Parent.oContained.w_PANOARCH=trim(this.Parent.oContained.w_PANOARCH)
    this.value = ;
      iif(this.Parent.oContained.w_PANOARCH=='S',1,;
      0)
  endfunc

  add object oPAESCNOM_4_23 as StdCheck with uid="TBXTORDYDT",rtseq=80,rtrep=.f.,left=166, top=243, caption="Non importare i nominativi del fascicolo Polisweb nella pratica",;
    ToolTipText = "Se attivo, i nominativi del fascicolo Polisweb non sono inseriti nella pratica",;
    HelpContextID = 117630141,;
    cFormVar="w_PAESCNOM", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oPAESCNOM_4_23.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oPAESCNOM_4_23.GetRadio()
    this.Parent.oContained.w_PAESCNOM = this.RadioValue()
    return .t.
  endfunc

  func oPAESCNOM_4_23.SetRadio()
    this.Parent.oContained.w_PAESCNOM=trim(this.Parent.oContained.w_PAESCNOM)
    this.value = ;
      iif(this.Parent.oContained.w_PAESCNOM=='S',1,;
      0)
  endfunc


  add object oPA_INVIO_4_24 as StdCombo with uid="NARHXVLLMG",rtseq=100,rtrep=.f.,left=166,top=277,width=181,height=22;
    , ToolTipText = "Modalit� per invio busta. Se modalit� utente standard, � necessario definire una modalit� standard nei servizi fax/email dell'utente, se modalit� pec, � necessario definirne i parametri nei servizi fax/email dell'utente intestatario della busta";
    , HelpContextID = 27573061;
    , cFormVar="w_PA_INVIO",RowSource=""+"Standard utente,"+"Servizio Pec", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oPA_INVIO_4_24.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'P',;
    space(1))))
  endfunc
  func oPA_INVIO_4_24.GetRadio()
    this.Parent.oContained.w_PA_INVIO = this.RadioValue()
    return .t.
  endfunc

  func oPA_INVIO_4_24.SetRadio()
    this.Parent.oContained.w_PA_INVIO=trim(this.Parent.oContained.w_PA_INVIO)
    this.value = ;
      iif(this.Parent.oContained.w_PA_INVIO=='M',1,;
      iif(this.Parent.oContained.w_PA_INVIO=='P',2,;
      0))
  endfunc

  add object oStr_4_2 as StdString with uid="UOJPFBYMEQ",Visible=.t., Left=48, Top=62,;
    Alignment=1, Width=112, Height=18,;
    Caption="Punto di accesso:"  ;
  , bGlobalFont=.t.

  add object oStr_4_3 as StdString with uid="YXQUVDPCUD",Visible=.t., Left=481, Top=190,;
    Alignment=1, Width=126, Height=18,;
    Caption="Data ultima importaz.:"  ;
  , bGlobalFont=.t.

  add object oStr_4_4 as StdString with uid="YRYJTFGVLI",Visible=.t., Left=54, Top=26,;
    Alignment=0, Width=133, Height=19,;
    Caption="Parametri per import"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_14 as StdString with uid="ZEQFCQECFG",Visible=.t., Left=87, Top=112,;
    Alignment=1, Width=73, Height=18,;
    Caption="Da eventi:"  ;
  , bGlobalFont=.t.

  func oStr_4_14.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oStr_4_15 as StdString with uid="JTINVXSDJS",Visible=.t., Left=87, Top=138,;
    Alignment=1, Width=73, Height=18,;
    Caption="Da udienza:"  ;
  , bGlobalFont=.t.

  func oStr_4_15.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oStr_4_16 as StdString with uid="BGNQUTAKWV",Visible=.t., Left=61, Top=164,;
    Alignment=1, Width=99, Height=18,;
    Caption="Da scadenza:"  ;
  , bGlobalFont=.t.

  func oStr_4_16.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oStr_4_17 as StdString with uid="UIVBERNCDQ",Visible=.t., Left=48, Top=190,;
    Alignment=1, Width=112, Height=18,;
    Caption="Ruolo risorsa:"  ;
  , bGlobalFont=.t.

  add object oStr_4_25 as StdString with uid="AWFELLZDJR",Visible=.t., Left=45, Top=281,;
    Alignment=1, Width=115, Height=18,;
    Caption="Modalit� invio busta telematica:"  ;
  , bGlobalFont=.t.

  add object oBox_4_5 as StdBox with uid="KHKWIADXRM",left=54, top=48, width=153,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_apa','PAR_ALTE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PACODAZI=PAR_ALTE.PACODAZI";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsma_apa
Proc VisBtn(pParent)
    pParent.w_SelQue.visible = not empty(pParent.w_PACLASSE)
EndProc
* --- Fine Area Manuale
