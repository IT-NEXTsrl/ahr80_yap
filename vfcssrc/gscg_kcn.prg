* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_kcn                                                        *
*              Contabilizzazione acconti contestuali                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_86]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-23                                                      *
* Last revis.: 2012-11-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_kcn",oParentObject))

* --- Class definition
define class tgscg_kcn as StdForm
  Top    = 7
  Left   = 7

  * --- Standard Properties
  Width  = 637
  Height = 415+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-11-07"
  HelpContextID=169204375
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=36

  * --- Constant Properties
  _IDX = 0
  CONTROPA_IDX = 0
  CONTI_IDX = 0
  CAU_CONT_IDX = 0
  VALUTE_IDX = 0
  cpusers_IDX = 0
  cPrg = "gscg_kcn"
  cComment = "Contabilizzazione acconti contestuali"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_TIPCON = space(1)
  w_FLVEAC = space(1)
  o_FLVEAC = space(1)
  w_CODAZI = space(5)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_TIPCLF = space(1)
  w_SAPAGA = space(5)
  w_TIPPAB = space(10)
  w_DATFIN = ctod('  /  /  ')
  o_DATFIN = ctod('  /  /  ')
  w_FLDATC = space(1)
  w_DATCON = ctod('  /  /  ')
  w_FLESER = space(1)
  w_CODCLF = space(15)
  w_DESCLF = space(40)
  w_CODVAL = space(3)
  o_CODVAL = space(3)
  w_DESVAL = space(35)
  w_CODUTE = 0
  w_DESUTE = space(35)
  w_DECTOT = 0
  o_DECTOT = 0
  w_CALCPICT = 0
  w_SELEZI = space(1)
  w_FLSELE = 0
  w_SERIALE = space(10)
  w_CACINC = space(5)
  w_COCINC = space(15)
  w_CACPAG = space(5)
  w_COCPAG = space(15)
  w_DECACI = space(35)
  w_DECACP = space(35)
  w_DECOCI = space(40)
  w_DECOCP = space(40)
  w_TR1 = space(1)
  w_TR2 = space(1)
  w_FP1 = space(1)
  w_FP2 = space(1)
  w_OBJ_LOG = space(10)
  w_CalcZoom = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_kcnPag1","gscg_kcn",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(2).addobject("oPag","tgscg_kcnPag2","gscg_kcn",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Contropartite")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFLVEAC_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gscg_kcn
    if VARTYPE(g_SCHEDULER)='C' AND g_SCHEDULER='S'
       this.parent.left=_screen.width+1
    endif
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_CalcZoom = this.oPgFrm.Pages(1).oPag.CalcZoom
    DoDefault()
    proc Destroy()
      this.w_CalcZoom = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='CONTROPA'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='CAU_CONT'
    this.cWorkTables[4]='VALUTE'
    this.cWorkTables[5]='cpusers'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSCG_BCN with this
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPCON=space(1)
      .w_FLVEAC=space(1)
      .w_CODAZI=space(5)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_TIPCLF=space(1)
      .w_SAPAGA=space(5)
      .w_TIPPAB=space(10)
      .w_DATFIN=ctod("  /  /  ")
      .w_FLDATC=space(1)
      .w_DATCON=ctod("  /  /  ")
      .w_FLESER=space(1)
      .w_CODCLF=space(15)
      .w_DESCLF=space(40)
      .w_CODVAL=space(3)
      .w_DESVAL=space(35)
      .w_CODUTE=0
      .w_DESUTE=space(35)
      .w_DECTOT=0
      .w_CALCPICT=0
      .w_SELEZI=space(1)
      .w_FLSELE=0
      .w_SERIALE=space(10)
      .w_CACINC=space(5)
      .w_COCINC=space(15)
      .w_CACPAG=space(5)
      .w_COCPAG=space(15)
      .w_DECACI=space(35)
      .w_DECACP=space(35)
      .w_DECOCI=space(40)
      .w_DECOCP=space(40)
      .w_TR1=space(1)
      .w_TR2=space(1)
      .w_FP1=space(1)
      .w_FP2=space(1)
      .w_OBJ_LOG=space(10)
        .w_TIPCON = 'G'
        .w_FLVEAC = 'V'
        .w_CODAZI = i_CODAZI
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODAZI))
          .link_1_3('Full')
        endif
        .w_OBTEST = .w_DATFIN
          .DoRTCalc(5,5,.f.)
        .w_TIPCLF = IIF(.w_FLVEAC='A', 'F', 'C')
          .DoRTCalc(7,7,.f.)
        .w_TIPPAB = SUBSTR(CHTIPPAG(.w_SAPAGA, 'RD'), 3)
        .w_DATFIN = i_datsys
        .w_FLDATC = IIF(IsAlt(),'D','U')
        .w_DATCON = .w_DATFIN
        .w_FLESER = 'O'
        .w_CODCLF = SPACE(15)
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_CODCLF))
          .link_1_13('Full')
        endif
          .DoRTCalc(14,14,.f.)
        .w_CODVAL = g_PERVAL
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_CODVAL))
          .link_1_15('Full')
        endif
        .DoRTCalc(16,17,.f.)
        if not(empty(.w_CODUTE))
          .link_1_17('Full')
        endif
          .DoRTCalc(18,18,.f.)
        .w_DECTOT = IIF(EMPTY(.w_CODVAL), 2, .w_DECTOT)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_SELEZI = 'D'
      .oPgFrm.Page1.oPag.CalcZoom.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .w_FLSELE = 0
        .w_SERIALE = .w_CalcZoom.getVar('MVSERIAL')
        .w_CACINC = .w_CACINC
        .DoRTCalc(24,24,.f.)
        if not(empty(.w_CACINC))
          .link_2_1('Full')
        endif
        .w_COCINC = .w_COCINC
        .DoRTCalc(25,25,.f.)
        if not(empty(.w_COCINC))
          .link_2_2('Full')
        endif
        .w_CACPAG = .w_CACPAG
        .DoRTCalc(26,26,.f.)
        if not(empty(.w_CACPAG))
          .link_2_3('Full')
        endif
        .w_COCPAG = .w_COCPAG
        .DoRTCalc(27,27,.f.)
        if not(empty(.w_COCPAG))
          .link_2_4('Full')
        endif
    endwith
    this.DoRTCalc(28,36,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_33.enabled = this.oPgFrm.Page1.oPag.oBtn_1_33.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- gscg_kcn
    If Type('This.oparentobject')='O'
      This.oparentobject.w_OBJ_LOG=this.w_OBJ_LOG
    endif
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_FLVEAC<>.w_FLVEAC
          .link_1_3('Full')
        endif
        if .o_DATFIN<>.w_DATFIN
            .w_OBTEST = .w_DATFIN
        endif
        .DoRTCalc(5,5,.t.)
            .w_TIPCLF = IIF(.w_FLVEAC='A', 'F', 'C')
        .DoRTCalc(7,12,.t.)
        if .o_FLVEAC<>.w_FLVEAC
            .w_CODCLF = SPACE(15)
          .link_1_13('Full')
        endif
        .DoRTCalc(14,18,.t.)
        if .o_CODVAL<>.w_CODVAL
            .w_DECTOT = IIF(EMPTY(.w_CODVAL), 2, .w_DECTOT)
        endif
        if .o_DECTOT<>.w_DECTOT
            .w_CALCPICT = DEFPIC(.w_DECTOT)
        endif
        .oPgFrm.Page1.oPag.CalcZoom.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .DoRTCalc(21,22,.t.)
            .w_SERIALE = .w_CalcZoom.getVar('MVSERIAL')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(24,36,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.CalcZoom.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oFLVEAC_1_2.enabled = this.oPgFrm.Page1.oPag.oFLVEAC_1_2.mCond()
    this.oPgFrm.Page1.oPag.oDATCON_1_11.enabled = this.oPgFrm.Page1.oPag.oDATCON_1_11.mCond()
    this.oPgFrm.Page2.oPag.oCACINC_2_1.enabled = this.oPgFrm.Page2.oPag.oCACINC_2_1.mCond()
    this.oPgFrm.Page2.oPag.oCOCINC_2_2.enabled = this.oPgFrm.Page2.oPag.oCOCINC_2_2.mCond()
    this.oPgFrm.Page2.oPag.oCACPAG_2_3.enabled = this.oPgFrm.Page2.oPag.oCACPAG_2_3.mCond()
    this.oPgFrm.Page2.oPag.oCOCPAG_2_4.enabled = this.oPgFrm.Page2.oPag.oCOCPAG_2_4.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_33.enabled = this.oPgFrm.Page1.oPag.oBtn_1_33.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oDATCON_1_11.visible=!this.oPgFrm.Page1.oPag.oDATCON_1_11.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_23.visible=!this.oPgFrm.Page1.oPag.oStr_1_23.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_24.visible=!this.oPgFrm.Page1.oPag.oStr_1_24.mHide()
    this.oPgFrm.Page2.oPag.oCACPAG_2_3.visible=!this.oPgFrm.Page2.oPag.oCACPAG_2_3.mHide()
    this.oPgFrm.Page2.oPag.oCOCPAG_2_4.visible=!this.oPgFrm.Page2.oPag.oCOCPAG_2_4.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_6.visible=!this.oPgFrm.Page2.oPag.oStr_2_6.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_8.visible=!this.oPgFrm.Page2.oPag.oStr_2_8.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_10.visible=!this.oPgFrm.Page2.oPag.oStr_2_10.mHide()
    this.oPgFrm.Page2.oPag.oDECACP_2_14.visible=!this.oPgFrm.Page2.oPag.oDECACP_2_14.mHide()
    this.oPgFrm.Page2.oPag.oDECOCP_2_16.visible=!this.oPgFrm.Page2.oPag.oDECOCP_2_16.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.CalcZoom.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_31.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_lTable = "CONTROPA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2], .t., this.CONTROPA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COCODAZI,COCACINC,COCOCINC,COCACPAG,COCOCPAG,COSAPAGA";
                   +" from "+i_cTable+" "+i_lTable+" where COCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COCODAZI',this.w_CODAZI)
            select COCODAZI,COCACINC,COCOCINC,COCACPAG,COCOCPAG,COSAPAGA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.COCODAZI,space(5))
      this.w_CACINC = NVL(_Link_.COCACINC,space(5))
      this.w_COCINC = NVL(_Link_.COCOCINC,space(15))
      this.w_CACPAG = NVL(_Link_.COCACPAG,space(5))
      this.w_COCPAG = NVL(_Link_.COCOCPAG,space(15))
      this.w_SAPAGA = NVL(_Link_.COSAPAGA,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_CACINC = space(5)
      this.w_COCINC = space(15)
      this.w_CACPAG = space(5)
      this.w_COCPAG = space(15)
      this.w_SAPAGA = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])+'\'+cp_ToStr(_Link_.COCODAZI,1)
      cp_ShowWarn(i_cKey,this.CONTROPA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCLF
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCLF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCLF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLF);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCLF;
                     ,'ANCODICE',trim(this.w_CODCLF))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCLF)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCLF) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCLF_1_13'),i_cWhere,'GSAR_BZC',"Clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCLF<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLF);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCLF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCLF);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCLF;
                       ,'ANCODICE',this.w_CODCLF)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCLF = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLF = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODCLF = space(15)
      endif
      this.w_DESCLF = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCLF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODVAL
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_CODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_CODVAL))
          select VACODVAL,VADESVAL,VADECTOT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oCODVAL_1_15'),i_cWhere,'GSAR_AVL',"Valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CODVAL)
            select VACODVAL,VADESVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DESVAL = NVL(_Link_.VADESVAL,space(35))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODVAL = space(3)
      endif
      this.w_DESVAL = space(35)
      this.w_DECTOT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODUTE
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpusers')
        if i_nConn<>0
          i_cWhere = " code="+cp_ToStrODBC(this.w_CODUTE);

          i_ret=cp_SQL(i_nConn,"select code,name";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'code',this.w_CODUTE)
          select code,name;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CODUTE) and !this.bDontReportError
            deferred_cp_zoom('cpusers','*','code',cp_AbsName(oSource.parent,'oCODUTE_1_17'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                     +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',oSource.xKey(1))
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_CODUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_CODUTE)
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODUTE = NVL(_Link_.code,0)
      this.w_DESUTE = NVL(_Link_.name,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODUTE = 0
      endif
      this.w_DESUTE = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CACINC
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CACINC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_CACINC)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO,CCTIPREG,CCFLPART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_CACINC))
          select CCCODICE,CCDESCRI,CCDTOBSO,CCTIPREG,CCFLPART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CACINC)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_CACINC)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO,CCTIPREG,CCFLPART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_CACINC)+"%");

            select CCCODICE,CCDESCRI,CCDTOBSO,CCTIPREG,CCFLPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CACINC) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCACINC_2_1'),i_cWhere,'GSCG_ACC',"Causali contabili",'CAUCOEFA.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO,CCTIPREG,CCFLPART";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCDTOBSO,CCTIPREG,CCFLPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CACINC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO,CCTIPREG,CCFLPART";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CACINC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CACINC)
            select CCCODICE,CCDESCRI,CCDTOBSO,CCTIPREG,CCFLPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CACINC = NVL(_Link_.CCCODICE,space(5))
      this.w_DECACI = NVL(_Link_.CCDESCRI,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
      this.w_TR1 = NVL(_Link_.CCTIPREG,space(1))
      this.w_FP1 = NVL(_Link_.CCFLPART,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CACINC = space(5)
      endif
      this.w_DECACI = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_TR1 = space(1)
      this.w_FP1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TR1='N' AND (empty(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO) AND (.w_FP1<>'N' OR g_PERPAR<>'S')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale incongruente o no partite o obsoleto o di tipo IVA")
        endif
        this.w_CACINC = space(5)
        this.w_DECACI = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_TR1 = space(1)
        this.w_FP1 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CACINC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COCINC
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCINC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_COCINC)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_COCINC))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCINC)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_COCINC)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_COCINC)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COCINC) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCOCINC_2_2'),i_cWhere,'GSAR_BZC',"Conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCINC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_COCINC);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_COCINC)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCINC = NVL(_Link_.ANCODICE,space(15))
      this.w_DECOCI = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCINC = space(15)
      endif
      this.w_DECOCI = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso o obsoleto")
        endif
        this.w_COCINC = space(15)
        this.w_DECOCI = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCINC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CACPAG
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CACPAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_CACPAG)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO,CCTIPREG,CCFLPART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_CACPAG))
          select CCCODICE,CCDESCRI,CCDTOBSO,CCTIPREG,CCFLPART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CACPAG)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_CACPAG)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO,CCTIPREG,CCFLPART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_CACPAG)+"%");

            select CCCODICE,CCDESCRI,CCDTOBSO,CCTIPREG,CCFLPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CACPAG) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCACPAG_2_3'),i_cWhere,'GSCG_ACC',"Causali contabili",'CAUCOEFA.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO,CCTIPREG,CCFLPART";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCDTOBSO,CCTIPREG,CCFLPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CACPAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO,CCTIPREG,CCFLPART";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CACPAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CACPAG)
            select CCCODICE,CCDESCRI,CCDTOBSO,CCTIPREG,CCFLPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CACPAG = NVL(_Link_.CCCODICE,space(5))
      this.w_DECACP = NVL(_Link_.CCDESCRI,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
      this.w_TR2 = NVL(_Link_.CCTIPREG,space(1))
      this.w_FP2 = NVL(_Link_.CCFLPART,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CACPAG = space(5)
      endif
      this.w_DECACP = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_TR2 = space(1)
      this.w_FP2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TR2='N' AND (empty(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO) AND (.w_FP2<>'N' OR g_PERPAR<>'S')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale incongruente o no partite o obsoleto o di tipo IVA")
        endif
        this.w_CACPAG = space(5)
        this.w_DECACP = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_TR2 = space(1)
        this.w_FP2 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CACPAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COCPAG
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCPAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_COCPAG)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_COCPAG))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCPAG)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_COCPAG)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_COCPAG)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COCPAG) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCOCPAG_2_4'),i_cWhere,'GSAR_BZC',"Conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCPAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_COCPAG);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_COCPAG)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCPAG = NVL(_Link_.ANCODICE,space(15))
      this.w_DECOCP = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCPAG = space(15)
      endif
      this.w_DECOCP = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso o obsoleto")
        endif
        this.w_COCPAG = space(15)
        this.w_DECOCP = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCPAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFLVEAC_1_2.RadioValue()==this.w_FLVEAC)
      this.oPgFrm.Page1.oPag.oFLVEAC_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_9.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_9.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDATC_1_10.RadioValue()==this.w_FLDATC)
      this.oPgFrm.Page1.oPag.oFLDATC_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATCON_1_11.value==this.w_DATCON)
      this.oPgFrm.Page1.oPag.oDATCON_1_11.value=this.w_DATCON
    endif
    if not(this.oPgFrm.Page1.oPag.oFLESER_1_12.RadioValue()==this.w_FLESER)
      this.oPgFrm.Page1.oPag.oFLESER_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCLF_1_13.value==this.w_CODCLF)
      this.oPgFrm.Page1.oPag.oCODCLF_1_13.value=this.w_CODCLF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLF_1_14.value==this.w_DESCLF)
      this.oPgFrm.Page1.oPag.oDESCLF_1_14.value=this.w_DESCLF
    endif
    if not(this.oPgFrm.Page1.oPag.oCODVAL_1_15.value==this.w_CODVAL)
      this.oPgFrm.Page1.oPag.oCODVAL_1_15.value=this.w_CODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESVAL_1_16.value==this.w_DESVAL)
      this.oPgFrm.Page1.oPag.oDESVAL_1_16.value=this.w_DESVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oCODUTE_1_17.value==this.w_CODUTE)
      this.oPgFrm.Page1.oPag.oCODUTE_1_17.value=this.w_CODUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUTE_1_18.value==this.w_DESUTE)
      this.oPgFrm.Page1.oPag.oDESUTE_1_18.value=this.w_DESUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_29.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCACINC_2_1.value==this.w_CACINC)
      this.oPgFrm.Page2.oPag.oCACINC_2_1.value=this.w_CACINC
    endif
    if not(this.oPgFrm.Page2.oPag.oCOCINC_2_2.value==this.w_COCINC)
      this.oPgFrm.Page2.oPag.oCOCINC_2_2.value=this.w_COCINC
    endif
    if not(this.oPgFrm.Page2.oPag.oCACPAG_2_3.value==this.w_CACPAG)
      this.oPgFrm.Page2.oPag.oCACPAG_2_3.value=this.w_CACPAG
    endif
    if not(this.oPgFrm.Page2.oPag.oCOCPAG_2_4.value==this.w_COCPAG)
      this.oPgFrm.Page2.oPag.oCOCPAG_2_4.value=this.w_COCPAG
    endif
    if not(this.oPgFrm.Page2.oPag.oDECACI_2_13.value==this.w_DECACI)
      this.oPgFrm.Page2.oPag.oDECACI_2_13.value=this.w_DECACI
    endif
    if not(this.oPgFrm.Page2.oPag.oDECACP_2_14.value==this.w_DECACP)
      this.oPgFrm.Page2.oPag.oDECACP_2_14.value=this.w_DECACP
    endif
    if not(this.oPgFrm.Page2.oPag.oDECOCI_2_15.value==this.w_DECOCI)
      this.oPgFrm.Page2.oPag.oDECOCI_2_15.value=this.w_DECOCI
    endif
    if not(this.oPgFrm.Page2.oPag.oDECOCP_2_16.value==this.w_DECOCP)
      this.oPgFrm.Page2.oPag.oDECOCP_2_16.value=this.w_DECOCP
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_DATFIN)) or not(.w_DATCON>=.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_9.SetFocus()
            i_bnoObbl = !empty(.w_DATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data documento maggiore della data di contabilizzazione")
          case   ((empty(.w_DATCON)) or not(.w_DATCON>=.w_DATFIN))  and not(.w_FLDATC<>'U')  and (.w_FLDATC='U')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATCON_1_11.SetFocus()
            i_bnoObbl = !empty(.w_DATCON)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data documento maggiore della data di contabilizzazione")
          case   ((empty(.w_CACINC)) or not(.w_TR1='N' AND (empty(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO) AND (.w_FP1<>'N' OR g_PERPAR<>'S')))  and (.w_FLVEAC='V')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCACINC_2_1.SetFocus()
            i_bnoObbl = !empty(.w_CACINC)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale incongruente o no partite o obsoleto o di tipo IVA")
          case   ((empty(.w_COCINC)) or not(empty(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO))  and (.w_FLVEAC='V')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCOCINC_2_2.SetFocus()
            i_bnoObbl = !empty(.w_COCINC)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore non ammesso o obsoleto")
          case   ((empty(.w_CACPAG)) or not(.w_TR2='N' AND (empty(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO) AND (.w_FP2<>'N' OR g_PERPAR<>'S')))  and not(IsAlt())  and (.w_FLVEAC='A')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCACPAG_2_3.SetFocus()
            i_bnoObbl = !empty(.w_CACPAG)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale incongruente o no partite o obsoleto o di tipo IVA")
          case   ((empty(.w_COCPAG)) or not(empty(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO))  and not(IsAlt())  and (.w_FLVEAC='A')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCOCPAG_2_4.SetFocus()
            i_bnoObbl = !empty(.w_COCPAG)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore non ammesso o obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_FLVEAC = this.w_FLVEAC
    this.o_DATFIN = this.w_DATFIN
    this.o_CODVAL = this.w_CODVAL
    this.o_DECTOT = this.w_DECTOT
    return

enddefine

* --- Define pages as container
define class tgscg_kcnPag1 as StdContainer
  Width  = 633
  height = 416
  stdWidth  = 633
  stdheight = 416
  resizeXpos=369
  resizeYpos=295
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oFLVEAC_1_2 as StdCombo with uid="PEMVMPJRHR",rtseq=2,rtrep=.f.,left=114,top=11,width=116,height=21;
    , ToolTipText = "Tipo di acconti da elaborare ricevuti (da ciclo attivo) o emessi (ciclo passivo)";
    , HelpContextID = 244282538;
    , cFormVar="w_FLVEAC",RowSource=""+"Ricevuti,"+"Emessi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLVEAC_1_2.RadioValue()
    return(iif(this.value =1,'V',;
    iif(this.value =2,'A',;
    space(1))))
  endfunc
  func oFLVEAC_1_2.GetRadio()
    this.Parent.oContained.w_FLVEAC = this.RadioValue()
    return .t.
  endfunc

  func oFLVEAC_1_2.SetRadio()
    this.Parent.oContained.w_FLVEAC=trim(this.Parent.oContained.w_FLVEAC)
    this.value = ;
      iif(this.Parent.oContained.w_FLVEAC=='V',1,;
      iif(this.Parent.oContained.w_FLVEAC=='A',2,;
      0))
  endfunc

  func oFLVEAC_1_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_ACQU='S')
    endwith
   endif
  endfunc

  add object oDATFIN_1_9 as StdField with uid="QDKUPUXTHD",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data documento maggiore della data di contabilizzazione",;
    ToolTipText = "Data di fine selezione dei documenti da contabilizzare",;
    HelpContextID = 51290058,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=547, Top=11

  func oDATFIN_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATCON>=.w_DATFIN)
    endwith
    return bRes
  endfunc


  add object oFLDATC_1_10 as StdCombo with uid="UHDMJZMGWU",rtseq=10,rtrep=.f.,left=114,top=42,width=116,height=21;
    , ToolTipText = "Data di registrazione: unica per tutti i documenti o uguale alla data documento";
    , HelpContextID = 224695466;
    , cFormVar="w_FLDATC",RowSource=""+"Data unica,"+"Data documento", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLDATC_1_10.RadioValue()
    return(iif(this.value =1,'U',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oFLDATC_1_10.GetRadio()
    this.Parent.oContained.w_FLDATC = this.RadioValue()
    return .t.
  endfunc

  func oFLDATC_1_10.SetRadio()
    this.Parent.oContained.w_FLDATC=trim(this.Parent.oContained.w_FLDATC)
    this.value = ;
      iif(this.Parent.oContained.w_FLDATC=='U',1,;
      iif(this.Parent.oContained.w_FLDATC=='D',2,;
      0))
  endfunc

  add object oDATCON_1_11 as StdField with uid="VKAKPNCKZW",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DATCON", cQueryName = "DATCON",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data documento maggiore della data di contabilizzazione",;
    ToolTipText = "Data di contabilizzazione dei documenti selezionati",;
    HelpContextID = 45195210,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=250, Top=42

  func oDATCON_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLDATC='U')
    endwith
   endif
  endfunc

  func oDATCON_1_11.mHide()
    with this.Parent.oContained
      return (.w_FLDATC<>'U')
    endwith
  endfunc

  func oDATCON_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATCON>=.w_DATFIN)
    endwith
    return bRes
  endfunc


  add object oFLESER_1_12 as StdCombo with uid="UEKJHMKBZI",rtseq=12,rtrep=.f.,left=388,top=42,width=157,height=21;
    , ToolTipText = "Applica esercizio in funzione della data di contabilizzazione o documento di origine";
    , HelpContextID = 256017578;
    , cFormVar="w_FLESER",RowSource=""+"Per data contabilizzazione,"+"Per documento di origine", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLESER_1_12.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'O',;
    space(1))))
  endfunc
  func oFLESER_1_12.GetRadio()
    this.Parent.oContained.w_FLESER = this.RadioValue()
    return .t.
  endfunc

  func oFLESER_1_12.SetRadio()
    this.Parent.oContained.w_FLESER=trim(this.Parent.oContained.w_FLESER)
    this.value = ;
      iif(this.Parent.oContained.w_FLESER=='D',1,;
      iif(this.Parent.oContained.w_FLESER=='O',2,;
      0))
  endfunc

  add object oCODCLF_1_13 as StdField with uid="JUZVYEKONC",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CODCLF", cQueryName = "CODCLF",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice cliente/fornitore di selezione (spazio = no selezione)",;
    HelpContextID = 182620634,;
   bGlobalFont=.t.,;
    Height=21, Width=133, Left=114, Top=73, cSayPict="p_CLF", cGetPict="p_CLF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCLF", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCLF"

  func oCODCLF_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCLF_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCLF_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCLF)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCLF)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCLF_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti/fornitori",'',this.parent.oContained
  endproc
  proc oCODCLF_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCLF
     i_obj.w_ANCODICE=this.parent.oContained.w_CODCLF
     i_obj.ecpSave()
  endproc

  add object oDESCLF_1_14 as StdField with uid="IWXXEXKERW",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DESCLF", cQueryName = "DESCLF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 182561738,;
   bGlobalFont=.t.,;
    Height=21, Width=291, Left=250, Top=73, InputMask=replicate('X',40)

  add object oCODVAL_1_15 as StdField with uid="ZHZKRSXIRB",rtseq=15,rtrep=.f.,;
    cFormVar = "w_CODVAL", cQueryName = "CODVAL",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice valuta di selezione documenti (spazio = no selezione)",;
    HelpContextID = 92246490,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=114, Top=101, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_CODVAL"

  func oCODVAL_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODVAL_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODVAL_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oCODVAL_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'',this.parent.oContained
  endproc
  proc oCODVAL_1_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_CODVAL
     i_obj.ecpSave()
  endproc

  add object oDESVAL_1_16 as StdField with uid="HMDBEHTNPX",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DESVAL", cQueryName = "DESVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 92187594,;
   bGlobalFont=.t.,;
    Height=21, Width=291, Left=250, Top=101, InputMask=replicate('X',35)

  add object oCODUTE_1_17 as StdField with uid="SUMUTKUGWD",rtseq=17,rtrep=.f.,;
    cFormVar = "w_CODUTE", cQueryName = "CODUTE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Eventuale utente di selezione (vuoto = no selezione)",;
    HelpContextID = 189829594,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=114, Top=129, cSayPict='"9999"', cGetPict='"9999"', bHasZoom = .t. , cLinkFile="cpusers", oKey_1_1="code", oKey_1_2="this.w_CODUTE"

  func oCODUTE_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODUTE_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODUTE_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'cpusers','*','code',cp_AbsName(this.parent,'oCODUTE_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oDESUTE_1_18 as StdField with uid="IDFXTJOSLZ",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DESUTE", cQueryName = "DESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 189770698,;
   bGlobalFont=.t.,;
    Height=21, Width=291, Left=250, Top=129, InputMask=replicate('X',35)


  add object oBtn_1_19 as StdButton with uid="KJMPMBPFXU",left=532, top=371, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Inizio contabilizzazione";
    , HelpContextID = 169233126;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_19.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_DATCON>=.w_DATFIN OR .w_FLDATC<>'U')
      endwith
    endif
  endfunc


  add object oBtn_1_20 as StdButton with uid="BUEPILAUOQ",left=583, top=371, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 176521798;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_26 as StdButton with uid="FFZTGIBZOZ",left=578, top=36, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per ricercare gli acconti da contabilizzare";
    , HelpContextID = 190744298;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_26.Click()
      with this.Parent.oContained
        .notifyevent('Calcola')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_26.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_DATCON>=.w_DATFIN OR .w_FLDATC<>'U')
      endwith
    endif
  endfunc

  add object oSELEZI_1_29 as StdRadio with uid="OLNKSHLUXV",rtseq=21,rtrep=.f.,left=7, top=372, width=133,height=32;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_29.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 117447386
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 117447386
      this.Buttons(2).Top=15
      this.SetAll("Width",131)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_1_29.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_29.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_29.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc


  add object CalcZoom as cp_szoombox with uid="URPFKUFOUW",left=3, top=154, width=628,height=213,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="DOC_MAST",cZoomFile="GSCG_KCN",bQueryOnLoad=.f.,bOptions=.f.,bAdvOptions=.f.,;
    cEvent = "Calcola",;
    nPag=1;
    , HelpContextID = 189668890


  add object oObj_1_31 as cp_runprogram with uid="PJWPVNSUAF",left=15, top=467, width=221,height=19,;
    caption='GSCG_B1N(SELE)',;
   bGlobalFont=.t.,;
    prg='GSCG_B1N("SELE")',;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 915404


  add object oBtn_1_33 as StdButton with uid="JZBFCQNAYU",left=145, top=371, width=48,height=45,;
    CpPicture="BMP\DOC1.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare il documento associato";
    , HelpContextID = 161380966;
    , tabstop=.f.,caption='\<Docum.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_33.Click()
      with this.Parent.oContained
        GSAR_BZM(this.Parent.oContained,.w_SERIALE, -20)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_33.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERIALE))
      endwith
    endif
  endfunc

  add object oStr_1_21 as StdString with uid="KSWFCYGMTY",Visible=.t., Left=255, Top=11,;
    Alignment=1, Width=290, Height=15,;
    Caption="Documenti emessi fino al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="JFNJJHQPTP",Visible=.t., Left=2, Top=11,;
    Alignment=1, Width=110, Height=15,;
    Caption="Tipo acconti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="ESTXFJJHFK",Visible=.t., Left=2, Top=73,;
    Alignment=1, Width=110, Height=15,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  func oStr_1_23.mHide()
    with this.Parent.oContained
      return (.w_TIPCLF<>'C')
    endwith
  endfunc

  add object oStr_1_24 as StdString with uid="XCSTFVKUCP",Visible=.t., Left=0, Top=72,;
    Alignment=1, Width=110, Height=15,;
    Caption="Fornitore:"  ;
  , bGlobalFont=.t.

  func oStr_1_24.mHide()
    with this.Parent.oContained
      return (.w_TIPCLF<>'F')
    endwith
  endfunc

  add object oStr_1_25 as StdString with uid="CNGCSJCJTJ",Visible=.t., Left=2, Top=42,;
    Alignment=1, Width=110, Height=15,;
    Caption="Contabilizzazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="ODDRWSFAQR",Visible=.t., Left=2, Top=101,;
    Alignment=1, Width=110, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="ZCFWNDVUQR",Visible=.t., Left=2, Top=129,;
    Alignment=1, Width=110, Height=15,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="GOZCMLKDHD",Visible=.t., Left=330, Top=43,;
    Alignment=1, Width=55, Height=18,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.
enddefine
define class tgscg_kcnPag2 as StdContainer
  Width  = 633
  height = 416
  stdWidth  = 633
  stdheight = 416
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCACINC_2_1 as StdField with uid="BBNQELFMPX",rtseq=24,rtrep=.f.,;
    cFormVar = "w_CACINC", cQueryName = "CACINC",;
    bObbl = .t. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale incongruente o no partite o obsoleto o di tipo IVA",;
    ToolTipText = "Causale contabile per la contabilizzazione degli acconti incassati",;
    HelpContextID = 230469594,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=109, Top=48, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_CACINC"

  func oCACINC_2_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLVEAC='V')
    endwith
   endif
  endfunc

  func oCACINC_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCACINC_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCACINC_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCACINC_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'CAUCOEFA.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oCACINC_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_CACINC
     i_obj.ecpSave()
  endproc

  add object oCOCINC_2_2 as StdField with uid="ZLPQGLTRSF",rtseq=25,rtrep=.f.,;
    cFormVar = "w_COCINC", cQueryName = "COCINC",;
    bObbl = .t. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso o obsoleto",;
    ToolTipText = "Contropartita contabile per la contabilizzazione degli acconti incassati",;
    HelpContextID = 230466010,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=109, Top=77, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_COCINC"

  func oCOCINC_2_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLVEAC='V')
    endwith
   endif
  endfunc

  func oCOCINC_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCINC_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCINC_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCOCINC_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'',this.parent.oContained
  endproc
  proc oCOCINC_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_COCINC
     i_obj.ecpSave()
  endproc

  add object oCACPAG_2_3 as StdField with uid="BAQIZCRSOS",rtseq=26,rtrep=.f.,;
    cFormVar = "w_CACPAG", cQueryName = "CACPAG",;
    bObbl = .t. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale incongruente o no partite o obsoleto o di tipo IVA",;
    ToolTipText = "Causale contabile per la contabilizzazione degli acconti pagati",;
    HelpContextID = 176533466,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=109, Top=157, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_CACPAG"

  func oCACPAG_2_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLVEAC='A')
    endwith
   endif
  endfunc

  func oCACPAG_2_3.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oCACPAG_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCACPAG_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCACPAG_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCACPAG_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'CAUCOEFA.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oCACPAG_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_CACPAG
     i_obj.ecpSave()
  endproc

  add object oCOCPAG_2_4 as StdField with uid="GMUCWUZJJT",rtseq=27,rtrep=.f.,;
    cFormVar = "w_COCPAG", cQueryName = "COCPAG",;
    bObbl = .t. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso o obsoleto",;
    ToolTipText = "Contropartita contabile per la contabilizzazione degli acconti pagati",;
    HelpContextID = 176529882,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=109, Top=187, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_COCPAG"

  func oCOCPAG_2_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLVEAC='A')
    endwith
   endif
  endfunc

  func oCOCPAG_2_4.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oCOCPAG_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCPAG_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCPAG_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCOCPAG_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'',this.parent.oContained
  endproc
  proc oCOCPAG_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_COCPAG
     i_obj.ecpSave()
  endproc

  add object oDECACI_2_13 as StdField with uid="ZYEPGWFDQE",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DECACI", cQueryName = "DECACI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 141863882,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=176, Top=48, InputMask=replicate('X',35)

  add object oDECACP_2_14 as StdField with uid="CQDRHMXRXO",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DECACP", cQueryName = "DECACP",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 24423370,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=176, Top=157, InputMask=replicate('X',35)

  func oDECACP_2_14.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oDECOCI_2_15 as StdField with uid="CGSRMYSTEG",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DECOCI", cQueryName = "DECOCI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 140946378,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=252, Top=77, InputMask=replicate('X',40)

  add object oDECOCP_2_16 as StdField with uid="JNFSCLDQHN",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DECOCP", cQueryName = "DECOCP",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 23505866,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=252, Top=187, InputMask=replicate('X',40)

  func oDECOCP_2_16.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_2_5 as StdString with uid="IDULFJVEGK",Visible=.t., Left=5, Top=17,;
    Alignment=0, Width=542, Height=15,;
    Caption="Acconti incassati"  ;
  , bGlobalFont=.t.

  add object oStr_2_6 as StdString with uid="ATILJNLRFN",Visible=.t., Left=5, Top=126,;
    Alignment=0, Width=542, Height=15,;
    Caption="Acconti pagati"  ;
  , bGlobalFont=.t.

  func oStr_2_6.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_2_7 as StdString with uid="WRNQNDMPBB",Visible=.t., Left=0, Top=48,;
    Alignment=1, Width=107, Height=15,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_8 as StdString with uid="JBFQOEPCSS",Visible=.t., Left=0, Top=157,;
    Alignment=1, Width=107, Height=15,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  func oStr_2_8.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_2_9 as StdString with uid="SLUTVLNYLS",Visible=.t., Left=0, Top=77,;
    Alignment=1, Width=107, Height=15,;
    Caption="Conto:"  ;
  , bGlobalFont=.t.

  add object oStr_2_10 as StdString with uid="CEBPHFMZFM",Visible=.t., Left=0, Top=187,;
    Alignment=1, Width=107, Height=15,;
    Caption="Conto:"  ;
  , bGlobalFont=.t.

  func oStr_2_10.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oBox_2_11 as StdBox with uid="JQAEMCCKTH",left=1, top=35, width=548,height=0

  add object oBox_2_12 as StdBox with uid="USDYTJDYDP",left=1, top=144, width=548,height=0
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_kcn','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
