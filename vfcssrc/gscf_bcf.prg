* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscf_bcf                                                        *
*              Gestione controllo flussi                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-10-08                                                      *
* Last revis.: 2013-09-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER,pFLDNAME,pTIPSEL
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscf_bcf",oParentObject,m.pOPER,m.pFLDNAME,m.pTIPSEL)
return(i_retval)

define class tgscf_bcf as StdBatch
  * --- Local variables
  w_REFLDNAM = space(15)
  w_FLDLIST = space(200)
  w_idx_tb = 0
  w_iconn_tb = 0
  w_PHTABLE = space(10)
  w_PKFILTER = space(10)
  w_PKVAL = .NULL.
  w_FLDVAL = .NULL.
  w_SQLCMD = space(200)
  w_REFLTEXP = space(10)
  w_STREXPR = space(10)
  w_j = 0
  w_char = space(1)
  w_LOREC_ID = 0
  w_LOKEYREC = space(1)
  w_RETFLTEX = space(250)
  w_RETDELEX = space(250)
  w_REREGSUC = space(1)
  w_RERIFREC = space(1)
  w_ONERROR = space(250)
  w_FLDNAME = space(40)
  w_CANCHK = .f.
  w_CANDEL = .f.
  w_TRSERR = space(10)
  w_retcode = space(10)
  w_EXPRES = space(250)
  w_RESUL = space(1)
  w_nOldArea = 0
  pOPER = space(2)
  pFLDNAME = space(30)
  pTIPSEL = space(1)
  w_PADRE = .NULL.
  w_FORM = .NULL.
  w_FLOWSTAMP = space(10)
  w_FlowNoRules = .f.
  w_ROWSTAMP = space(10)
  w_TABLENAME = space(50)
  w_DETAILNAME = space(50)
  w_TABLEDESCR = space(254)
  w_DETAILDESCR = space(254)
  w_TABLESELECTED = space(50)
  w_FLDFILTER = space(20)
  w_CHKUSROK = .f.
  w_RIFBUSOB = space(15)
  w_REFLENAB = space(1)
  w_OBJRULES = .NULL.
  w_POSTINBTN = space(1)
  w_cKEYSELECT = space(60)
  w_RESERIAL = space(10)
  w_REDESCRI = space(40)
  w_LOSTAMP = space(20)
  w_LOSERIAL = space(10)
  w_LOROWLOG = 0
  w_LOGTBNAM = space(10)
  w_LOFLDNAM = space(10)
  w_LOFLDVAL = space(10)
  w_LOFLDNAM1 = space(10)
  w_LOFLDVAL1 = space(10)
  w_LOFLDNAM2 = space(10)
  w_LOFLDVAL2 = space(10)
  w_LOFLDNAM3 = space(10)
  w_LOFLDVAL3 = space(10)
  w_LOFLDNAM4 = space(10)
  w_LOFLDVAL4 = space(10)
  w_LOFLDNAM5 = space(10)
  w_LOFLDVAL5 = space(10)
  w_LOFLDNAM6 = space(10)
  w_LOFLDVAL6 = space(10)
  w_LOFLDNAM7 = space(10)
  w_LOFLDVAL7 = space(10)
  w_LOFLDNAM8 = space(10)
  w_LOFLDVAL8 = space(10)
  w_EXISTS1 = 0
  w_EXISTS2 = 0
  w_EXISTS3 = 0
  w_EXISTS4 = 0
  w_EXISTS5 = 0
  w_EXISTS6 = 0
  w_EXISTS7 = 0
  w_EXISTS8 = 0
  w_IND = 0
  w_RIGASEL = space(1)
  w_TROVATA = .f.
  w_KEYLIST = space(254)
  w_FLNAME = space(50)
  w_TBNAME = space(50)
  w_POSVIR = 0
  w_NUMREC = 0
  w_SRC = space(30)
  w_KEYREC = 0
  w_TABMAST = space(20)
  w_VAR = space(0)
  w_NUMDEC = 0
  w_DEC = 0
  w_CTRS = space(10)
  * --- WorkFile variables
  REF_MAST_idx=0
  LOGCNTFL_idx=0
  REF_DETT_idx=0
  TMPSTAMP_idx=0
  LOGCNTUT_idx=0
  XDC_FIELDS_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- pOPER pu� assumere:
    *        RE = Avvio editor regole
    *        RE+ = Avvio editor regole (caricamento)
    *        VW = Visualizza i dati relativi della form
    *        DS = Delete start
    *        IS = Insert start
    *        US = Update start
    *        DR = Delete row start
    *        IR = Insert row start
    *        UR = Update row start
    *        ET = Scrivo nel log
    * --- pFLDNAME: imposta il filtro sul campo selezionato
    * --- pTIPSEL: tipo selezione
    *     T: tutto
    *     M: Master
    *     D: Detail
    *     R: solo riga selezionata
    this.w_PADRE = this.oParentObject
    this.w_FlowNoRules = .f.
    if PEMSTATUS( this.w_PADRE, "cFileDetail", 5 )
      * --- Master/Detail
      this.w_TABLENAME = ALLTRIM(this.w_PADRE.cFile)
      this.w_TABLEDESCR = ALLTRIM( i_dcx.gettabledescr( i_dcx.gettableidx( UPPER(this.w_TABLENAME) ) ) )
      this.w_DETAILNAME = ALLTRIM(this.w_PADRE.cFileDetail)
      this.w_DETAILDESCR = ALLTRIM( i_dcx.gettabledescr( i_dcx.gettableidx( UPPER(this.w_DETAILNAME) ) ) )
    else
      if PEMSTATUS( this.w_PADRE, "cFile", 5 )
        if empty(this.w_PADRE.cTrsName)
          * --- Master
          this.w_TABLENAME = ALLTRIM(this.w_PADRE.cFile)
          this.w_TABLEDESCR = ALLTRIM( i_dcx.gettabledescr( i_dcx.gettableidx( UPPER(this.w_TABLENAME) ) ) )
        else
          * --- Detail
          this.w_DETAILNAME = ALLTRIM(this.w_PADRE.cFile)
          this.w_DETAILDESCR = ALLTRIM( i_dcx.gettabledescr( i_dcx.gettableidx( UPPER(this.w_DETAILNAME) ) ) )
        endif
      endif
    endif
    if PEMSTATUS( this.w_PADRE, "cFlowStamp", 5 )
      this.w_FLOWSTAMP = this.w_PADRE.cFlowStamp
    else
      AH_MSG("FlowStamp mancante" )
      i_retcode = 'stop'
      return
    endif
    if PEMSTATUS( this.w_PADRE, "bFlowNoRules", 5 )
      this.w_FlowNoRules = this.w_PADRE.bFlowNoRules
    endif
    do case
      case upper(this.w_PADRE.BASECLASS)="FORM"
        this.w_RIFBUSOB = this.w_PADRE.getSecurityCode()
        this.w_POSTINBTN = "S"
        this.w_cKEYSELECT = this.w_PADRE.cKEYSELECT
      case upper(this.w_PADRE.BASECLASS)="CONTAINER" AND PEMSTATUS( this.w_PADRE, "cPRG", 5 )
        * --- sono dentro un child
        this.w_RIFBUSOB = this.w_PADRE.cPrg
        this.w_POSTINBTN = "N"
        * --- Eredito lo stamp dalla form
        if PEMSTATUS( this.w_PADRE, "cFlowStamp", 5 )
          this.w_FLOWSTAMP = space(10)
          this.w_FORM = this.w_PADRE.parent
          do while upper(this.w_FORM.BASECLASS)<>"FORM" and not isnull(this.w_FORM)
            this.w_FORM = this.w_FORM.parent
          enddo
          if PEMSTATUS( this.w_FORM, "cFlowStamp", 5 )
            if empty(this.w_FORM.cFile) and empty(this.w_FORM.cFlowstamp)
              this.w_FORM.cFlowStamp = sys(2015)
            endif
            this.w_PADRE.cFlowStamp = this.w_FORM.cFlowStamp
            this.w_FLOWSTAMP = this.w_PADRE.cFlowStamp
          endif
        endif
        if empty(this.w_DETAILNAME) and not empty(this.w_PADRE.cTRSNAME)
          * --- Nei Detail (PC) imposto in nome della tabella nel campo del Master
          this.w_DETAILNAME = this.w_TABLENAME
          this.w_DETAILDESCR = ALLTRIM( i_dcx.gettabledescr( i_dcx.gettableidx( UPPER(this.w_DETAILNAME) ) ) )
        endif
      otherwise
        i_retcode = 'stop'
        return
    endcase
    do case
      case this.pOPER=="RE" or this.pOPER=="RE+"
        this.w_OBJRULES = GSCF_MRE()
        if this.w_OBJRULES.bSec1
          * --- Verifico se esiste gi� il record per caricarlo en entrare in modifica, 
          *     altrimenti carico un nuovo record
          this.w_RESERIAL = SPACE(10)
          this.w_REFLENAB = "S"
          if this.pOPER=="RE"
            * --- Verifico se esiste almeno una regola
            this.w_RESERIAL = ""
            * --- Select from gscf_bcf
            do vq_exec with 'gscf_bcf',this,'_Curs_gscf_bcf','',.f.,.t.
            if used('_Curs_gscf_bcf')
              select _Curs_gscf_bcf
              locate for 1=1
              do while not(eof())
              this.w_RESERIAL = this.w_RESERIAL + "'" + ALLTRIM(_Curs_gscf_bcf.RESERIAL) + "',"
                select _Curs_gscf_bcf
                continue
              enddo
              use
            endif
          else
            this.pOPER = "RE"
          endif
          if EMPTY(this.w_RESERIAL)
            if this.w_OBJRULES.bSec2
              * --- Crea nuova regola
              this.w_OBJRULES.ecpLoad()     
              this.w_OBJRULES.w_REBUSOBJ = this.w_RIFBUSOB
              this.w_OBJRULES.o_REBUSOBJ = this.w_RIFBUSOB
              this.w_OBJRULES.w_REPSTBTN = this.w_POSTINBTN
              this.w_OBJRULES.w_REKEYSEL = this.w_cKEYSELECT
              SetValueLinked("MT", this.w_OBJRULES, "w_RETABMST", this.w_TABLENAME )
              SetValueLinked("MT", this.w_OBJRULES, "w_RETABDTL", this.w_DETAILNAME )
              this.w_OBJRULES.SetControlsValue()     
              this.w_OBJRULES.w_RE_TABLE = IIF(EMPTY(this.w_TABLENAME), this.w_OBJRULES.w_RETABDTL, this.w_OBJRULES.w_RETABMST)
              this.w_OBJRULES.mCalc(.t.)     
              this.w_OBJRULES.SetControlsValue()     
              * --- Carico le righe della chiave
              this.w_TBNAME = this.w_OBJRULES.w_RETABMST
              this.w_KEYLIST = IIF(empty(this.w_TBNAME), "", cp_KeyToSQL(i_dcx.getidxdef( this.w_TBNAME ,1)))
              if NOT EMPTY(this.w_KEYLIST)
                this.w_KEYLIST = ALLTRIM(this.w_KEYLIST) + ","
                this.Page_9()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
              this.w_TBNAME = this.w_OBJRULES.w_RETABDTL
              this.w_KEYLIST = IIF(empty(this.w_TBNAME), "", cp_KeyToSQL(i_dcx.getidxdef( this.w_TBNAME ,1)))
              if NOT EMPTY(this.w_KEYLIST)
                this.w_KEYLIST = ALLTRIM(this.w_KEYLIST) + ","
                this.Page_9()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
              this.w_OBJRULES.w_REKEYGES = "S"
              this.w_OBJRULES.mCalc(.t.)     
            else
              ah_ErrorMsg("Impossibile creare nuove regole del controllo flussi: %1",16,MSG_SECURITY_ADMIN,MSG_ACCESS_DENIED)
            endif
          else
            this.w_OBJRULES.ecpFilter()     
            if PEMSTATUS( this.w_OBJRULES, "cMyFilter", 5 )
              this.w_OBJRULES.cMyFilter = "RESERIAL IN (" + left(this.w_RESERIAL, len(this.w_RESERIAL)-1) + ")"
            else
              this.w_OBJRULES.w_REBUSOBJ = this.w_RIFBUSOB
              this.w_OBJRULES.w_RETABMST = this.w_TABLENAME
              this.w_OBJRULES.w_RETABDTL = this.w_DETAILNAME
            endif
            this.w_OBJRULES.ecpSave()     
          endif
        else
          ah_ErrorMsg("Impossibile accedere alla manutenzione delle regole del controllo flussi: %1",16,MSG_SECURITY_ADMIN,MSG_ACCESS_DENIED)
        endif
      case this.pOPER=="VW"
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pOPER=="DS"
        this.w_TABLESELECTED = this.w_TABLENAME
      case this.pOPER=="IS"
        this.w_TABLESELECTED = this.w_TABLENAME
      case this.pOPER=="US"
        this.w_TABLESELECTED = this.w_TABLENAME
      case this.pOPER=="DR"
        this.w_TABLESELECTED = this.w_DETAILNAME
      case this.pOPER=="IR"
        this.w_TABLESELECTED = this.w_DETAILNAME
      case this.pOPER=="UR"
        this.w_TABLESELECTED = this.w_DETAILNAME
      case this.pOPER=="ET"
        * --- Sono fuori transazione: record Inserted/Updated. scrivo nel log
        if USED("TMPLOG")
          if RECCOUNT("TMPLOG")>0
            * --- aggiungo un record e lo cancello, altrimenti (a volte) perde l'ultimo record
            select TMPLOG
            append blank
            delete
            go top
            * --- Se non rileva modifiche, cancello i record della PK dal log (tab. master)
            select LOSERIAL,SUM(iif(inlist(LOTIPOPE,"I","D","R"), 1, 0)) as LOTIPOPE from TMPLOG group by LOSERIAL into cursor zero
            select zero
            scan for lotipope=0
            this.w_LOSERIAL = LOSERIAL
            delete from TMPLOG where LOSERIAL=this.w_LOSERIAL
            endscan
            if RECCOUNT("TMPLOG")>1
              * --- Try
              local bErr_052A4F68
              bErr_052A4F68=bTrsErr
              this.Try_052A4F68()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- rollback
                bTrsErr=.t.
                cp_EndTrs(.t.)
                ah_errormsg("Errore aggiornamento log controllo flussi:%1%0",,,i_ErrMsg)
              endif
              bTrsErr=bTrsErr or bErr_052A4F68
              * --- End
            endif
          endif
        endif
        this.Pag10()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
    if not inlist(this.pOPER, "RE", "VW", "ET")
      * --- Analisi delle regole di log
      if this.w_FlowNoRules
        * --- Esce subito: non ci sono regole da applicare
      else
        * --- Analizza i dati del record
        if empty(this.w_FLOWSTAMP)
          this.w_FLOWSTAMP = sys(2015)
          ah_Msg("Controllo flussi non attivo per ridefinizione procedure salvataggio/cancellazione")
        endif
        vq_exec("GSCFRBCF.VQR", this, "_Rules_")
        if USED("_Rules_")
          if RECCOUNT("_Rules_")>0
            SELECT DISTINCT RESERIAL FROM _Rules_ ORDER BY repriori DESC INTO CURSOR _PKRULES_
            SELECT _PKRULES_
            GO TOP
            if RECCOUNT()>0
              SCAN
              this.w_RESERIAL = RESERIAL
              this.w_TROVATA = FALSE
              if right(this.pOPER,1)="R"
                * --- timestamp di riga
                this.w_ROWSTAMP = sys(2015)
              else
                this.w_ROWSTAMP = space(10)
              endif
              if this.pOPER=="DR"
                * --- creo il cursore con i dati dei campi relativi alla tabella master
                this.w_FLDLIST = ""
                * --- Read from REF_MAST
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.REF_MAST_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.REF_MAST_idx,2],.t.,this.REF_MAST_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "RETABMST"+;
                    " from "+i_cTable+" REF_MAST where ";
                        +"RESERIAL = "+cp_ToStrODBC(this.w_RESERIAL);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    RETABMST;
                    from (i_cTable) where;
                        RESERIAL = this.w_RESERIAL;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_TABMAST = NVL(cp_ToDate(_read_.RETABMST),cp_NullValue(_read_.RETABMST))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                if NOT EMPTY(this.w_TABMAST)
                  this.w_TABMAST = ALLTRIM(this.w_TABMAST)
                  * --- Select from REF_DETT
                  i_nConn=i_TableProp[this.REF_DETT_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.REF_DETT_idx,2],.t.,this.REF_DETT_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select REFLDNAM  from "+i_cTable+" REF_DETT ";
                        +" where RESERIAL="+cp_ToStrODBC(this.w_RESERIAL)+" and   RE_TABLE="+cp_ToStrODBC(this.w_TABMAST)+"";
                         ,"_Curs_REF_DETT")
                  else
                    select REFLDNAM from (i_cTable);
                     where RESERIAL=this.w_RESERIAL and   RE_TABLE=this.w_TABMAST;
                      into cursor _Curs_REF_DETT
                  endif
                  if used('_Curs_REF_DETT')
                    select _Curs_REF_DETT
                    locate for 1=1
                    do while not(eof())
                    this.w_REFLDNAM = alltrim(_Curs_REF_DETT.REFLDNAM)
                    if NOT this.w_REFLDNAM $ this.w_FLDLIST
                      this.w_FLDLIST = this.w_FLDLIST + this.w_REFLDNAM + ","
                    endif
                      select _Curs_REF_DETT
                      continue
                    enddo
                    use
                  endif
                  if not empty(this.w_FLDLIST)
                    this.w_FLDLIST = left(this.w_FLDLIST, len(this.w_FLDLIST)-1) 
                    this.w_idx_tb = cp_tablepropscan(this.w_TABMAST)
                    this.w_iconn_tb = i_TableProp[this.w_idx_tb,3]
                    this.w_PHTABLE = cp_SetAzi(i_TableProp[this.w_idx_tb,2])
                    this.w_SQLCMD = "select " + this.w_FLDLIST + " from " + this.w_PHTABLE + " where 1=0"
                    if cp_sqlexec(this.w_iconn_tb, this.w_SQLCMD, "oldvalmasts") = -1
                      * --- errore esecuzione query, siamo sotto transazione: fallisce la registrazione!
                      this.w_TRSERR = message()
                      this.Page_6()
                      if i_retcode='stop' or !empty(i_Error)
                        return
                      endif
                    else
                      select oldvalmasts
                      append blank
                      FOR gnCount = 1 TO FCOUNT( ) 
                      this.w_REFLDNAM = FIELD(gnCount)
                      if PEMSTATUS(this.w_PADRE, "w_"+this.w_REFLDNAM, 5)
                        this.w_PKVAL = EVAL("this.oParentObject.w_" + this.w_REFLDNAM)
                        cfldnam = this.w_REFLDNAM
                        replace &cfldnam with this.w_PKVAL
                      endif
                      NEXT
                      * --- aggiungo un record vuoto, altrimenti il cursore resta blank
                      append blank
                      select * from oldvalmasts into cursor oldvalmast readwrite
                      use in oldvalmasts
                    endif
                  endif
                endif
              endif
              * --- Se i flag inser., mod. e canc. sono tutti spenti, prendo il campo (potrebbe essere usato nell'expr di filtro)
              SELECT * FROM "_Rules_" WHERE RESERIAL=this.w_RESERIAL AND ; 
 (REKEYREC="S" OR RERIFREC="S" OR ( ( this.pOPER=="DS" OR this.pOPER=="DR") AND (REMONDEL="S" OR not empty(nvl(RETDELEX,"")))) ; 
 OR ( ( this.pOPER=="IS" OR this.pOPER=="IR") AND (REMONINS="S" OR REBLKINS="S" OR REOBBINS="S")) ; 
 OR ( ( this.pOPER=="US" OR this.pOPER=="UR") AND REMONMOD $ "SIO" ) ; 
 OR ( REMONDEL<>"S" AND REMONINS<>"S" AND REMONMOD<>"S") ) INTO CURSOR "_CurRule_"
              this.w_REDESCRI = REDESCRI
              this.Page_2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              SELECT _PKRULES_
              if this.w_TROVATA
                if this.w_REREGSUC<>"S"
                  * --- non applico altre regole: ESCO!
                  GO BOTTOM
                endif
              endif
              ENDSCAN
            endif
            USE IN _PKRules_
          else
            if right(this.pOPER,1)="R"
              * --- Non ci sono regole, disabilito la chiamata per le prossime righe
              this.w_PADRE.bFlowNoRules = .T.
            endif
          endif
        endif
        USE IN SELECT("_Rules_")
      endif
    endif
    this.bUpdateParentObject = .F.
  endproc
  proc Try_052A4F68()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_LOREC_ID = 0
    Select TMPLOG
    go top
    this.w_FLOWSTAMP = LOSTAMP
    scan
    this.w_LOREC_ID = this.w_LOREC_ID+1
    * --- Insert into LOGCNTFL
    i_nConn=i_TableProp[this.LOGCNTFL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LOGCNTFL_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LOGCNTFL_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LOSERIAL"+",LOSTAMP"+",LOCODUTE"+",LODATAOP"+",LOTIMEOP"+",LOREC_ID"+",LOSTMRIG"+",LOROWLOG"+",LOKEYREC"+",LOTIPOPE"+",LOFLDVAL"+",LONEWVAL"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(TMPLOG.LOSERIAL),'LOGCNTFL','LOSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(TMPLOG.LOSTAMP),'LOGCNTFL','LOSTAMP');
      +","+cp_NullLink(cp_ToStrODBC(i_codute),'LOGCNTFL','LOCODUTE');
      +","+cp_NullLink(cp_ToStrODBC(TMPLOG.LODATAOP),'LOGCNTFL','LODATAOP');
      +","+cp_NullLink(cp_ToStrODBC(TMPLOG.LOTIMEOP),'LOGCNTFL','LOTIMEOP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LOREC_ID),'LOGCNTFL','LOREC_ID');
      +","+cp_NullLink(cp_ToStrODBC(TMPLOG.LOSTMRIG),'LOGCNTFL','LOSTMRIG');
      +","+cp_NullLink(cp_ToStrODBC(TMPLOG.LOROWLOG),'LOGCNTFL','LOROWLOG');
      +","+cp_NullLink(cp_ToStrODBC(TMPLOG.LOKEYREC),'LOGCNTFL','LOKEYREC');
      +","+cp_NullLink(cp_ToStrODBC(TMPLOG.LOTIPOPE),'LOGCNTFL','LOTIPOPE');
      +","+cp_NullLink(cp_ToStrODBC(TMPLOG.LOFLDVAL),'LOGCNTFL','LOFLDVAL');
      +","+cp_NullLink(cp_ToStrODBC(TMPLOG.LONEWVAL),'LOGCNTFL','LONEWVAL');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LOSERIAL',TMPLOG.LOSERIAL,'LOSTAMP',TMPLOG.LOSTAMP,'LOCODUTE',i_codute,'LODATAOP',TMPLOG.LODATAOP,'LOTIMEOP',TMPLOG.LOTIMEOP,'LOREC_ID',this.w_LOREC_ID,'LOSTMRIG',TMPLOG.LOSTMRIG,'LOROWLOG',TMPLOG.LOROWLOG,'LOKEYREC',TMPLOG.LOKEYREC,'LOTIPOPE',TMPLOG.LOTIPOPE,'LOFLDVAL',TMPLOG.LOFLDVAL,'LONEWVAL',TMPLOG.LONEWVAL)
      insert into (i_cTable) (LOSERIAL,LOSTAMP,LOCODUTE,LODATAOP,LOTIMEOP,LOREC_ID,LOSTMRIG,LOROWLOG,LOKEYREC,LOTIPOPE,LOFLDVAL,LONEWVAL &i_ccchkf. );
         values (;
           TMPLOG.LOSERIAL;
           ,TMPLOG.LOSTAMP;
           ,i_codute;
           ,TMPLOG.LODATAOP;
           ,TMPLOG.LOTIMEOP;
           ,this.w_LOREC_ID;
           ,TMPLOG.LOSTMRIG;
           ,TMPLOG.LOROWLOG;
           ,TMPLOG.LOKEYREC;
           ,TMPLOG.LOTIPOPE;
           ,TMPLOG.LOFLDVAL;
           ,TMPLOG.LONEWVAL;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    endscan
    * --- Aggiungo i destinatari delle notifiche
    select zero
    scan for lotipope>0
    this.w_RESERIAL = LOSERIAL
    * --- Insert into LOGCNTUT
    i_nConn=i_TableProp[this.LOGCNTUT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LOGCNTUT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSCF4BCF",this.LOGCNTUT_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    endscan
    use in zero
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    private pExpr
    m.pExpr = space(1)
    private pRESERIAL
    m.pRESERIAL = space(10)
    private pREDESCRI
    m.pREDESCRI = space(1)
    * --- Elaborazione regola
    Local cur_name
    m.pExpr = ""
    m.pRESERIAL = this.w_RESERIAL
    m.pREDESCRI = this.w_REDESCRI
    this.w_FLDLIST = ""
    this.w_PKFILTER = ""
    cOnError = On("ERROR")
    oParent = this
    this.w_retcode = "go"
    on error gscf_ber(oParent, m.pEXPR, Message(), m.pRESERIAL, m.pREDESCRI, cOnError)
    if USED("_CurRule_")
      SELECT _CurRule_
      this.w_RETFLTEX = alltrim(RETFLTEX)
      if NOT EMPTY(this.w_RETFLTEX)
        this.w_EXPRES = this.w_RETFLTEX
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_RETFLTEX = this.w_EXPRES
      endif
      this.w_RETDELEX = alltrim(nvl(RETDELEX,""))
      if NOT EMPTY(this.w_RETDELEX)
        * --- condizione di cancellazione
        this.w_EXPRES = this.w_RETDELEX
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_RETDELEX = this.w_EXPRES
      endif
      this.w_REREGSUC = REREGSUC
      SCAN
      this.w_REFLDNAM = alltrim(REFLDNAM)
      if NOT this.w_REFLDNAM $ this.w_FLDLIST
        this.w_FLDLIST = this.w_FLDLIST + this.w_REFLDNAM + ","
      endif
      if REKEYREC="S"
        if PEMSTATUS(this.w_PADRE, "w_"+this.w_REFLDNAM, 5)
          if this.pOPER=="UR" and not empty(FIELD(this.w_REFLDNAM, this.w_PADRE.ctrsname))
            * --- Prendo il valore dal transitorio perch� la PK potrebbe essere variata (esempio: cambi giornalieri)
            this.w_PKVAL = this.w_PADRE.ctrsname + "." + this.w_REFLDNAM
            this.w_PKVAL = EVAL( this.w_PKVAL )
          else
            this.w_PKVAL = EVAL("this.oParentObject.w_" + this.w_REFLDNAM)
          endif
          this.w_PKFILTER = this.w_PKFILTER + this.w_REFLDNAM + "=" + cp_tostrodbc(this.w_PKVAL) + " AND "
        else
          * --- Valore della PK non trovato sul padre
        endif
      endif
      ENDSCAN
      if NOT EMPTY(this.w_FLDLIST) AND NOT EMPTY(this.w_PKFILTER)
        this.w_FLDLIST = left(this.w_FLDLIST, len(this.w_FLDLIST)-1) 
        this.w_idx_tb = cp_tablepropscan(this.w_TABLESELECTED)
        this.w_iconn_tb = i_TableProp[this.w_idx_tb,3]
        this.w_PHTABLE = cp_SetAzi(i_TableProp[this.w_idx_tb,2])
        if empty(this.w_PKFILTER)
          * --- errore: filtro su PK vuoto
        else
          this.w_PKFILTER = left(this.w_PKFILTER, len(this.w_PKFILTER)-4)
          this.w_SQLCMD = "select " + this.w_FLDLIST + " from " + this.w_PHTABLE + " where " + this.w_PKFILTER
          if cp_sqlexec(this.w_iconn_tb, this.w_SQLCMD, "oldvalues") = -1
            * --- errore esecuzione query, siamo sotto transazione: fallisce la registrazione!
            this.w_TRSERR = message()
            this.Page_6()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            this.w_KEYREC = reccount("oldvalues")
            if this.w_KEYREC>1
              * --- trovato pi� di un record, PK non valida
              this.w_TRSERR = "Chiave impostata sulla regola non univoca"
              this.Page_6()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            else
              * --- valuto l'espressione globale per vedere se la regola � applicabile 
              if (this.pOPER=="DR" and used("oldvalmast") or used("cf_curmast")) AND this.w_TABLESELECTED=this.w_DETAILNAME and not empty(this.w_TABLENAME)
                if used("cf_curmast")
                  * --- Cerco il cursore master della mia regola
                  Select cf_curmast
                  locate for regola=this.w_RESERIAL
                  if found()
                    cur_name = cursname
                    select * from (cur_name) into cursor oldvalmast
                  endif
                endif
                if used("oldvalmast")
                  * --- metto in join il cursore del master con quello del detail
                  oldmvflds=""
                  select oldvalues
                  AFIELDS(OLDV)
                  select oldvalmast
                  AFIELDS(OLDMV)
                  FOR IM=1 TO ALEN(OLDMV,1)
                  if ASCAN(OLDV, OLDMV[IM,1], 1, ALEN(OLDV,1), 1)=0
                    oldmvflds = oldmvflds+",t."+OLDMV[IM,1]
                  endif
                  NEXT
                  select oldvalues.* &oldmvflds into cursor _appo_ from oldvalues left outer join oldvalmast t on .T.
                  this.Page_7()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                  if used("_appo_")
                    select * from _appo_ into cursor oldvalues
                    use in _appo_
                  else
                    * --- Errore generico
                    this.w_TRSERR = Message()
                    this.Page_6()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                  endif
                endif
              endif
              select oldvalues
              if empty(this.w_RETFLTEX)
                this.w_CANCHK = TRUE
              else
                * --- valuto l'espressione globale
                this.w_CANCHK = this.ValutaExp(this.w_RETFLTEX)
                this.Page_7()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                if VARTYPE(this.w_CANCHK)<>"L"
                  * --- Espressione non booleana
                  this.w_TRSERR = ah_msgformat("Espressione di filtro non valida.")
                  this.Page_6()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                endif
              endif
              if this.w_CANCHK
                * --- controllo se i valori sono variati
                if NOT USED("TMPLOG")
                  VQ_EXEC("QUERY\GSCF6BCF", THIS, "TMPLOG")
                endif
                SELECT _CurRule_
                SCAN for not btrserr
                this.w_LOROWLOG = CPROWNUM
                this.w_REFLDNAM = alltrim(REFLDNAM)
                this.w_LOKEYREC = REKEYREC
                this.w_RERIFREC = RERIFREC
                if NOT EMPTY(this.w_REFLDNAM)
                  * --- condizione di filtro
                  this.w_REFLTEXP = alltrim(REFLTEXP)
                  if NOT EMPTY(this.w_REFLTEXP)
                    this.w_EXPRES = this.w_REFLTEXP
                    this.Page_5()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                    this.w_REFLTEXP = this.w_EXPRES
                  endif
                endif
                if PEMSTATUS(this.w_PADRE, "w_"+this.w_REFLDNAM, 5)
                  this.w_PKVAL = EVAL("this.oParentObject.w_" + this.w_REFLDNAM)
                  if VARTYPE(this.w_PKVAL)="C"
                    this.w_PKVAL = RTRIM(this.w_PKVAL)
                  endif
                  SELECT oldvalues
                  this.w_FLDVAL = EVAL(this.w_REFLDNAM)
                  if isnull(this.w_FLDVAL)
                    do case
                      case vartype(this.w_PKVAL)="C"
                        this.w_FLDVAL = ""
                      case vartype(this.w_PKVAL)="N"
                        this.w_FLDVAL = 0
                      case vartype(this.w_PKVAL)="D"
                        this.w_FLDVAL = CTOD("")
                      case vartype(this.w_PKVAL)="T"
                        this.w_FLDVAL = CTOT("")
                    endcase
                  else
                    if VARTYPE(this.w_FLDVAL)="C"
                      this.w_FLDVAL = RTRIM(this.w_FLDVAL)
                    endif
                  endif
                  * --- valuto l'espressione di filtro
                  if empty(this.w_REFLTEXP)
                    this.w_CANCHK = TRUE
                  else
                    * --- valuto l'espressione di filtro
                    this.w_CANCHK = this.ValutaExp(this.w_REFLTEXP)
                    this.Page_7()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                    if VARTYPE(this.w_CANCHK)<>"L"
                      * --- Espressione non booleana
                      this.w_TRSERR = ah_msgformat("Espressione di filtro non valida.")
                      this.Page_6()
                      if i_retcode='stop' or !empty(i_Error)
                        return
                      endif
                    endif
                  endif
                  if this.w_CANCHK
                    this.w_VAR = this.w_PKVAL
                    this.Page_3()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                    this.w_PKVAL = this.w_VAR
                    this.w_VAR = this.w_FLDVAL
                    this.Page_3()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                    this.w_FLDVAL = this.w_VAR
                    SELECT _CurRule_
                    this.w_FLDNAME = ALLTRIM(NVL(REFLDCOM,""))
                    do case
                      case ( this.pOPER=="DS" OR this.pOPER=="DR") AND (REMONDEL="S" OR NOT EMPTY(this.w_RETDELEX))
                        if NOT EMPTY(this.w_RETDELEX)
                          * --- VERIFICO SE POSSO CANCELLARE I DATI
                          this.w_CANDEL = this.ValutaExp(this.w_RETDELEX)
                          this.Page_7()
                          if i_retcode='stop' or !empty(i_Error)
                            return
                          endif
                          do case
                            case VARTYPE(this.w_CANDEL)<>"L"
                              * --- Espressione non booleana
                              this.w_TRSERR = ah_msgformat("Espressione di cancellazione non valida.")
                              this.Page_6()
                              if i_retcode='stop' or !empty(i_Error)
                                return
                              endif
                            case this.w_CANDEL
                              * --- Cancellazione bloccata
                              this.w_TRSERR = ah_msgformat("Cancellazione bloccata.")
                              this.Page_6()
                              if i_retcode='stop' or !empty(i_Error)
                                return
                              endif
                          endcase
                        endif
                        INSERT INTO TMPLOG (LOSERIAL,LOSTAMP,LODATAOP,LOTIMEOP,LOSTMRIG,; 
 LOROWLOG,LOKEYREC,LOTIPOPE,LOFLDVAL,LONEWVAL) VALUES ; 
 (this.w_RESERIAL, this.w_FLOWSTAMP, i_datsys, TIME(), this.w_ROWSTAMP, ; 
 this.w_LOROWLOG, this.w_LOKEYREC, "D", this.w_FLDVAL, "")
                        this.w_TROVATA = TRUE
                      case ( this.pOPER=="IS" OR this.pOPER=="IR") AND (REMONINS="S" OR REBLKINS="S" OR REOBBINS="S")
                        do case
                          case REBLKINS="S" AND NOT EMPTY(this.w_PKVAL)
                            * --- Inserimento del dato non ammesso
                            do case
                              case not empty(nvl(REMSGERR,""))
                                this.w_TRSERR = ALLTRIM(REMSGERR)
                              case not empty(nvl(REMSGERC,""))
                                this.w_TRSERR = ALLTRIM(REMSGERC)
                              otherwise
                                this.w_TRSERR = ah_msgformat("Valorizzazione del campo %1 (%2) non ammessa", alltrim(this.w_REFLDNAM), this.w_FLDNAME)
                            endcase
                            this.Page_6()
                            if i_retcode='stop' or !empty(i_Error)
                              return
                            endif
                          case REOBBINS="S" AND EMPTY(this.w_PKVAL)
                            * --- Dato obbligatorio
                            do case
                              case not empty(nvl(REMSGERR,""))
                                this.w_TRSERR = ALLTRIM(REMSGERR)
                              case not empty(nvl(REMSGERC,""))
                                this.w_TRSERR = ALLTRIM(REMSGERC)
                              otherwise
                                this.w_TRSERR = ah_msgformat("Campo %1 (%2): dato obbligatorio.", alltrim(this.w_REFLDNAM), this.w_FLDNAME)
                            endcase
                            this.Page_6()
                            if i_retcode='stop' or !empty(i_Error)
                              return
                            endif
                          case REMONINS="S"
                            INSERT INTO TMPLOG (LOSERIAL,LOSTAMP,LODATAOP,LOTIMEOP,LOSTMRIG,; 
 LOROWLOG,LOKEYREC,LOTIPOPE,LOFLDVAL,LONEWVAL) VALUES ; 
 (this.w_RESERIAL, this.w_FLOWSTAMP, i_datsys, TIME(), this.w_ROWSTAMP, ; 
 this.w_LOROWLOG, this.w_LOKEYREC, "I", "", this.w_PKVAL)
                            this.w_TROVATA = TRUE
                        endcase
                      case ( this.pOPER=="US" OR this.pOPER=="UR") AND REMONMOD $ "SI" and not this.w_PKVAL==this.w_FLDVAL
                        do case
                          case REMONMOD="I"
                            * --- Modifica del dato non ammessa
                            do case
                              case not empty(nvl(REMSGERR,""))
                                this.w_TRSERR = ALLTRIM(REMSGERR)
                              case not empty(nvl(REMSGERC,""))
                                this.w_TRSERR = ALLTRIM(REMSGERC)
                              otherwise
                                this.w_TRSERR = ah_msgformat("Modifica del campo %1 (%2) non ammessa", alltrim(this.w_REFLDNAM), this.w_FLDNAME)
                            endcase
                            this.Page_6()
                            if i_retcode='stop' or !empty(i_Error)
                              return
                            endif
                          case REMONMOD="S"
                            INSERT INTO TMPLOG (LOSERIAL,LOSTAMP,LODATAOP,LOTIMEOP,LOSTMRIG,; 
 LOROWLOG,LOKEYREC,LOTIPOPE,LOFLDVAL,LONEWVAL) VALUES ; 
 (this.w_RESERIAL, this.w_FLOWSTAMP, i_datsys, TIME(), this.w_ROWSTAMP, ; 
 this.w_LOROWLOG, this.w_LOKEYREC, "R", this.w_FLDVAL, this.w_PKVAL)
                            this.w_TROVATA = TRUE
                        endcase
                      case ( this.pOPER=="US" OR this.pOPER=="UR") AND REMONMOD = "O" and EMPTY(this.w_PKVAL)
                        * --- Dato obbligatorio
                        do case
                          case not empty(nvl(REMSGERR,""))
                            this.w_TRSERR = ALLTRIM(REMSGERR)
                          case not empty(nvl(REMSGERC,""))
                            this.w_TRSERR = ALLTRIM(REMSGERC)
                          otherwise
                            this.w_TRSERR = ah_msgformat("Campo %1 (%2): dato obbligatorio.", alltrim(this.w_REFLDNAM), this.w_FLDNAME)
                        endcase
                        this.Page_6()
                        if i_retcode='stop' or !empty(i_Error)
                          return
                        endif
                      case this.w_LOKEYREC="S" OR this.w_RERIFREC="S"
                        if this.w_LOKEYREC="S"
                          INSERT INTO TMPLOG (LOSERIAL,LOSTAMP,LODATAOP,LOTIMEOP,LOSTMRIG,; 
 LOROWLOG,LOKEYREC,LOTIPOPE,LOFLDVAL,LONEWVAL) VALUES ; 
 (this.w_RESERIAL, this.w_FLOWSTAMP, i_datsys, TIME(), this.w_ROWSTAMP, ; 
 this.w_LOROWLOG, this.w_LOKEYREC, "K", this.w_FLDVAL, this.w_PKVAL)
                        endif
                        if this.w_RERIFREC="S"
                          INSERT INTO TMPLOG (LOSERIAL,LOSTAMP,LODATAOP,LOTIMEOP,LOSTMRIG,; 
 LOROWLOG,LOKEYREC,LOTIPOPE,LOFLDVAL,LONEWVAL) VALUES ; 
 (this.w_RESERIAL, this.w_FLOWSTAMP, i_datsys, TIME(), this.w_ROWSTAMP, ; 
 this.w_LOROWLOG, this.w_LOKEYREC, "J", this.w_FLDVAL, this.w_PKVAL)
                        endif
                    endcase
                  endif
                else
                  * --- Variabile non trovata sul padre
                endif
                SELECT _CurRule_
                ENDSCAN
                if NOT(this.w_TROVATA or this.w_TABLESELECTED=this.w_TABLENAME and not empty(this.w_DETAILNAME))
                  * --- cancello le righe "senza modifiche"
                  DELETE FROM TMPLOG WHERE LOSERIAL=this.w_RESERIAL AND LOSTAMP=this.w_FLOWSTAMP AND LOSTMRIG=this.w_ROWSTAMP
                endif
              endif
              if this.w_TABLESELECTED=this.w_TABLENAME and not empty(this.w_DETAILNAME)
                if this.pOPER<>"D"
                  * --- Conservo i cursori dei campi di testata per le righe
                  if not used("cf_curmast")
                    create cursor cf_curmast (regola c(10), cursname C(10))
                  endif
                  cur_name = sys(2015)
                  Select cf_curmast
                  locate for regola=this.w_RESERIAL
                  if found()
                    cur_name=cursname
                  else
                    append blank
                    replace regola with this.w_RESERIAL, cursname with cur_name
                  endif
                  Select * from oldvalues into cursor &cur_name
                endif
              else
                use in select("oldvalmast")
              endif
              use in oldvalues
            endif
          endif
        endif
      endif
    endif
    USE IN SELECT("_CurRule_")
    on error &cOnError
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- conversione a stringa
    do case
      case vartype(this.w_VAR)="C"
        this.w_VAR = left(this.w_VAR,255)
      case vartype(this.w_VAR)="N"
        this.w_NUMDEC = 0
        if this.w_VAR <> INT(this.w_VAR)
          this.w_DEC = this.w_VAR - INT(this.w_VAR)
          do while this.w_DEC <> INT(this.w_DEC)
            this.w_DEC = 10*this.w_DEC
            this.w_NUMDEC = this.w_NUMDEC + 1
          enddo
        endif
        this.w_VAR = alltrim(str(this.w_VAR,28,this.w_NUMDEC))
      case vartype(this.w_VAR)="D"
        this.w_VAR = dtoc(this.w_VAR)
      case vartype(this.w_VAR)="T"
        this.w_VAR = ttoc(this.w_VAR)
        if right(this.w_VAR,8)="00:00:00"
          this.w_VAR = left(this.w_VAR, 10)
        endif
    endcase
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Visualizza i dati relativi della form
    if not empty(this.w_TABLENAME) or not empty(this.w_DETAILNAME)
      this.w_FLDFILTER = IIF(VARTYPE(this.pFLDNAME)="C", this.pFLDNAME, SPACE(20))
      * --- Try
      local bErr_053CE1E0
      bErr_053CE1E0=bTrsErr
      this.Try_053CE1E0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
      endif
      bTrsErr=bTrsErr or bErr_053CE1E0
      * --- End
    endif
  endproc
  proc Try_053CE1E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_RESERIAL = SPACE(10)
    this.w_LOSTAMP = SPACE(10)
    * --- Create temporary table TMPSTAMP
    i_nIdx=cp_AddTableDef('TMPSTAMP') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    cp_CreateTempTable(i_ServerConn[1,2],i_cTempTable,'*',' from TMPSTAMP_proto';
          )
    this.TMPSTAMP_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Select from gscf_bcf
    do vq_exec with 'gscf_bcf',this,'_Curs_gscf_bcf','',.f.,.t.
    if used('_Curs_gscf_bcf')
      select _Curs_gscf_bcf
      locate for 1=1
      do while not(eof())
      this.w_RESERIAL = ALLTRIM(_Curs_gscf_bcf.RESERIAL)
      this.w_LOSERIAL = this.w_RESERIAL
      this.w_LOGTBNAM = IIF(empty(this.w_TABLENAME), this.w_DETAILNAME, this.w_TABLENAME)
      this.w_RIGASEL = "N"
      do while NOT EMPTY(this.w_LOGTBNAM)
        this.w_LOFLDNAM = ""
        this.w_LOFLDVAL = ""
        this.w_LOFLDNAM1 = ""
        this.w_LOFLDVAL1 = ""
        this.w_LOFLDNAM2 = ""
        this.w_LOFLDVAL2 = ""
        this.w_LOFLDNAM3 = ""
        this.w_LOFLDVAL3 = ""
        this.w_LOFLDNAM4 = ""
        this.w_LOFLDVAL4 = ""
        this.w_LOFLDNAM5 = ""
        this.w_LOFLDVAL5 = ""
        this.w_LOFLDNAM6 = ""
        this.w_LOFLDVAL6 = ""
        this.w_LOFLDNAM7 = ""
        this.w_LOFLDVAL7 = ""
        this.w_LOFLDNAM8 = ""
        this.w_LOFLDVAL8 = ""
        this.w_EXISTS1 = 1
        this.w_EXISTS2 = 1
        this.w_EXISTS3 = 1
        this.w_EXISTS4 = 1
        this.w_EXISTS5 = 1
        this.w_EXISTS6 = 1
        this.w_EXISTS7 = 1
        this.w_EXISTS8 = 1
        this.w_IND = 0
        * --- Select from REF_DETT
        i_nConn=i_TableProp[this.REF_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.REF_DETT_idx,2],.t.,this.REF_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select REFLDNAM  from "+i_cTable+" REF_DETT ";
              +" where RESERIAL="+cp_ToStrODBC(this.w_RESERIAL)+" AND   RE_TABLE="+cp_ToStrODBC(this.w_LOGTBNAM)+" AND   REKEYREC='S'";
               ,"_Curs_REF_DETT")
        else
          select REFLDNAM from (i_cTable);
           where RESERIAL=this.w_RESERIAL AND   RE_TABLE=this.w_LOGTBNAM AND   REKEYREC="S";
            into cursor _Curs_REF_DETT
        endif
        if used('_Curs_REF_DETT')
          select _Curs_REF_DETT
          locate for 1=1
          do while not(eof())
          this.w_REFLDNAM = ALLTRIM(_Curs_REF_DETT.REFLDNAM)
          if PEMSTATUS( this.w_PADRE, "w_"+this.w_REFLDNAM, 5 )
            if this.w_RIGASEL="S"
              this.w_VAR = EVAL("THIS.w_PADRE.w_" + this.w_REFLDNAM)
            else
              * --- cerco solo i dati di testata, devo ignorare la PK dei detail
              this.w_CTRS = IIF(PEMSTATUS( this.w_PADRE, "ctrsname", 5), this.w_PADRE.ctrsname, "")
              if empty(this.w_CTRS)
                * --- anagrafica di tipo master
                this.w_VAR = EVAL("THIS.w_PADRE.w_" + this.w_REFLDNAM)
              else
                if empty(FIELD("t_"+this.w_REFLDNAM, this.w_CTRS)) and empty(FIELD(this.w_REFLDNAM, this.w_CTRS))
                  * --- campi fissi
                  this.w_VAR = EVAL("THIS.w_PADRE.w_" + this.w_REFLDNAM)
                else
                  * --- il dato � sul transitorio, lo ignoro!
                  this.w_VAR = ""
                endif
              endif
            endif
            this.Page_3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if not empty(this.w_VAR)
              this.w_PKVAL = this.w_VAR
              do case
                case this.w_IND=0
                  this.w_LOFLDNAM = this.w_REFLDNAM
                  this.w_LOFLDVAL = this.w_PKVAL
                case this.w_IND=1
                  this.w_LOFLDNAM1 = this.w_REFLDNAM
                  this.w_LOFLDVAL1 = this.w_PKVAL
                  this.w_EXISTS1 = 0
                case this.w_IND=2
                  this.w_LOFLDNAM2 = this.w_REFLDNAM
                  this.w_LOFLDVAL2 = this.w_PKVAL
                  this.w_EXISTS2 = 0
                case this.w_IND=3
                  this.w_LOFLDNAM3 = this.w_REFLDNAM
                  this.w_LOFLDVAL3 = this.w_PKVAL
                  this.w_EXISTS3 = 0
                case this.w_IND=4
                  this.w_LOFLDNAM4 = this.w_REFLDNAM
                  this.w_LOFLDVAL4 = this.w_PKVAL
                  this.w_EXISTS4 = 0
                case this.w_IND=5
                  this.w_LOFLDNAM5 = this.w_REFLDNAM
                  this.w_LOFLDVAL5 = this.w_PKVAL
                  this.w_EXISTS5 = 0
                case this.w_IND=6
                  this.w_LOFLDNAM6 = this.w_REFLDNAM
                  this.w_LOFLDVAL6 = this.w_PKVAL
                  this.w_EXISTS6 = 0
                case this.w_IND=7
                  this.w_LOFLDNAM7 = this.w_REFLDNAM
                  this.w_LOFLDVAL7 = this.w_PKVAL
                  this.w_EXISTS7 = 0
                case this.w_IND=8
                  this.w_LOFLDNAM8 = this.w_REFLDNAM
                  this.w_LOFLDVAL8 = this.w_PKVAL
                  this.w_EXISTS8 = 0
              endcase
              this.w_IND = this.w_IND+1
            endif
          endif
            select _Curs_REF_DETT
            continue
          enddo
          use
        endif
        * --- Insert into TMPSTAMP
        i_nConn=i_TableProp[this.TMPSTAMP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPSTAMP_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSCF_BVW",this.TMPSTAMP_idx)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        if EMPTY(this.w_DETAILNAME)
          this.w_LOGTBNAM = ""
        else
          if this.w_DETAILNAME=this.w_LOGTBNAM and this.w_RIGASEL="S"
            this.w_LOGTBNAM = ""
          else
            this.w_LOGTBNAM = this.w_DETAILNAME
            this.w_RIGASEL = "S"
          endif
        endif
      enddo
        select _Curs_gscf_bcf
        continue
      enddo
      use
    endif
    * --- Read from TMPSTAMP
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.TMPSTAMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPSTAMP_idx,2],.t.,this.TMPSTAMP_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "LOSTAMP"+;
        " from "+i_cTable+" TMPSTAMP where ";
            +"1 = "+cp_ToStrODBC(1);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        LOSTAMP;
        from (i_cTable) where;
            1 = 1;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_LOSTAMP = NVL(cp_ToDate(_read_.LOSTAMP),cp_NullValue(_read_.LOSTAMP))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if empty(this.w_RESERIAL) or empty(this.w_LOSTAMP)
      ah_ErrorMsg("Nessun dato da visualizzare dal controllo flussi")
    else
      do gscf_kvw with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Drop temporary table TMPSTAMP
    i_nIdx=cp_GetTableDefIdx('TMPSTAMP')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPSTAMP')
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Normalizzazione espressione
    if LOWER(this.w_EXPRES)="w_"
      this.w_EXPRES = "this.w_PADRE." + this.w_EXPRES 
    endif
    * --- Caratteri che possono precedere il w_
    this.w_j = 1
    this.w_STREXPR = " (/*-+=<>!,$#["
    do while this.w_j <= len(this.w_STREXPR)
      this.w_char = substr(this.w_STREXPR, this.w_j, 1)
      this.w_EXPRES = STRTRAN(this.w_EXPRES, this.w_char+"w_", this.w_char+"this.w_PADRE.w_")
      this.w_EXPRES = STRTRAN(this.w_EXPRES, this.w_char+"W_", this.w_char+"this.w_PADRE.w_")
      this.w_j = this.w_j+1
    enddo
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Abort transaction
    gscf_ber(this,"", this.w_TRSERR, this.w_RESERIAL, this.w_REDESCRI, cOnError)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.Pag10()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    i_retcode = 'stop'
    return
  endproc


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Check error
    if this.w_retcode="stop"
      this.Pag10()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      i_retcode = 'stop'
      return
    endif
  endproc


  procedure ValutaExp
    param pExpr
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valuta l'espressione
    if USED("oldvalues")
      this.w_nOldArea = select()
      select oldvalues
      this.w_RESUL = EVALUATE(m.pExpr)
      select (this.w_nOldArea)
    else
      this.w_RESUL = EVALUATE(m.pExpr)
    endif
    return(this.w_RESUL)
  endproc


  procedure Page_9
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carico le righe della chiave
    this.w_OBJRULES.MarkPos()     
    do while "," $ this.w_KEYLIST
      this.w_POSVIR = AT(",", this.w_KEYLIST)
      this.w_FLNAME = LEFT(this.w_KEYLIST, this.w_POSVIR-1)
      this.w_KEYLIST = SUBSTR(this.w_KEYLIST, this.w_POSVIR+1)
      * --- Cerco nelle righe se il campo non � gi� stato inserito
      this.w_SRC = "NVL(t_REFLDNAM, SPACE(30) ) = " + cp_ToStrODBC(this.w_FLNAME)
      this.w_SRC = this.w_SRC + " AND NVL(t_RE_TABLE, SPACE(30) ) = " + cp_ToStrODBC(this.w_TBNAME)
      this.w_SRC = this.w_SRC + " AND NOT DELETED()"
      this.w_NUMREC = this.w_OBJRULES.Search(this.w_SRC)
      if this.w_NUMREC<>-1
        this.w_OBJRULES.SetRow(this.w_NUMREC)     
      else
        this.w_OBJRULES.AddRow()     
        this.w_OBJRULES.w_RE_TABLE = this.w_TBNAME
        this.w_OBJRULES.w_MSTDET = IIF(this.w_TBNAME=this.w_OBJRULES.w_RETABDTL, "D", "M")
        this.w_OBJRULES.o_MSTDET = this.w_OBJRULES.w_MSTDET
        this.w_OBJRULES.w_REFLDNAM = this.w_FLNAME
        * --- Read from XDC_FIELDS
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.XDC_FIELDS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.XDC_FIELDS_idx,2],.t.,this.XDC_FIELDS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "FLCOMMEN"+;
            " from "+i_cTable+" XDC_FIELDS where ";
                +"TBNAME = "+cp_ToStrODBC(this.w_TBNAME);
                +" and FLNAME = "+cp_ToStrODBC(this.w_FLNAME);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            FLCOMMEN;
            from (i_cTable) where;
                TBNAME = this.w_TBNAME;
                and FLNAME = this.w_FLNAME;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_OBJRULES.w_REFLDCOM = NVL(cp_ToDate(_read_.FLCOMMEN),cp_NullValue(_read_.FLCOMMEN))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      this.w_OBJRULES.w_REKEYREC = "S"
      this.w_OBJRULES.w_RERIFREC = "N"
      this.w_OBJRULES.w_REMONINS = "S"
      this.w_OBJRULES.w_REMONMOD = "N"
      this.w_OBJRULES.w_REMONDEL = "S"
      this.w_OBJRULES.w_REBLKINS = "N"
      this.w_OBJRULES.w_REOBBINS = "N"
      this.w_OBJRULES.w_NPKM = IIF(ALLTRIM(this.w_TBNAME)==ALLTRIM(this.w_OBJRULES.w_RETABMST), 1, 0)
      this.w_OBJRULES.w_NPKD = IIF(ALLTRIM(this.w_TBNAME)==ALLTRIM(this.w_OBJRULES.w_RETABDTL), 1, 0)
      this.w_OBJRULES.mCalc(.t.)     
      this.w_OBJRULES.SaveRow()     
    enddo
    Select (this.w_OBJRULES.cTrsName)
    * --- TOTALIZZATORI
    SUM(T_NPKM) TO this.w_OBJRULES.w_TOTPKM
    SUM(T_NPKD) TO this.w_OBJRULES.w_TOTPKD
    this.w_OBJRULES.RePos()     
  endproc


  procedure Pag10
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiusura cursori di servizio
    use in select("oldvalmast")
    use in select("oldvalues")
    use in select("tmplog")
    if used("cf_curmast")
      Select cf_curmast
      scan
      use in select(cursname)
      endscan
      use in cf_curmast
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pOPER,pFLDNAME,pTIPSEL)
    this.pOPER=pOPER
    this.pFLDNAME=pFLDNAME
    this.pTIPSEL=pTIPSEL
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='REF_MAST'
    this.cWorkTables[2]='LOGCNTFL'
    this.cWorkTables[3]='REF_DETT'
    this.cWorkTables[4]='*TMPSTAMP'
    this.cWorkTables[5]='LOGCNTUT'
    this.cWorkTables[6]='XDC_FIELDS'
    return(this.OpenAllTables(6))

  proc CloseCursors()
    if used('_Curs_gscf_bcf')
      use in _Curs_gscf_bcf
    endif
    if used('_Curs_REF_DETT')
      use in _Curs_REF_DETT
    endif
    if used('_Curs_gscf_bcf')
      use in _Curs_gscf_bcf
    endif
    if used('_Curs_REF_DETT')
      use in _Curs_REF_DETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER,pFLDNAME,pTIPSEL"
endproc
