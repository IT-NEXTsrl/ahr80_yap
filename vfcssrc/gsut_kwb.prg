* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kwb                                                        *
*              Verifica assegnamento codici attributo                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-05-23                                                      *
* Last revis.: 2011-08-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_kwb",oParentObject))

* --- Class definition
define class tgsut_kwb as StdForm
  Top    = 2
  Left   = 3

  * --- Standard Properties
  Width  = 760
  Height = 469
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-08-26"
  HelpContextID=32064361
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsut_kwb"
  cComment = "Verifica assegnamento codici attributo"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CDATTOPE = space(10)
  w_CDOLDATTINF = space(15)
  o_CDOLDATTINF = space(15)
  w_CDATTINF = space(15)
  w_CDCODCLA = space(10)
  w_CDTABKEY = space(10)
  w_ZOOMATTR = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kwbPag1","gsut_kwb",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCDATTINF_1_8
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOMATTR = this.oPgFrm.Pages(1).oPag.ZOOMATTR
    DoDefault()
    proc Destroy()
      this.w_ZOOMATTR = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CDATTOPE=space(10)
      .w_CDOLDATTINF=space(15)
      .w_CDATTINF=space(15)
      .w_CDCODCLA=space(10)
      .w_CDTABKEY=space(10)
      .w_CDATTOPE=oParentObject.w_CDATTOPE
      .oPgFrm.Page1.oPag.ZOOMATTR.Calculate()
          .DoRTCalc(1,1,.f.)
        .w_CDOLDATTINF = .w_ZOOMATTR.getVar('CDATTINF')
        .w_CDATTINF = .w_CDOLDATTINF
        .w_CDCODCLA = .w_ZOOMATTR.getVar('CDCODCLA')
        .w_CDTABKEY = .w_ZOOMATTR.getVar('CDTABKEY')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_1.enabled = this.oPgFrm.Page1.oPag.oBtn_1_1.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_2.enabled = this.oPgFrm.Page1.oPag.oBtn_1_2.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_CDATTOPE=.w_CDATTOPE
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZOOMATTR.Calculate()
        .DoRTCalc(1,1,.t.)
            .w_CDOLDATTINF = .w_ZOOMATTR.getVar('CDATTINF')
        if .o_CDOLDATTINF<>.w_CDOLDATTINF
            .w_CDATTINF = .w_CDOLDATTINF
        endif
            .w_CDCODCLA = .w_ZOOMATTR.getVar('CDCODCLA')
            .w_CDTABKEY = .w_ZOOMATTR.getVar('CDTABKEY')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOMATTR.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOMATTR.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCDOLDATTINF_1_7.value==this.w_CDOLDATTINF)
      this.oPgFrm.Page1.oPag.oCDOLDATTINF_1_7.value=this.w_CDOLDATTINF
    endif
    if not(this.oPgFrm.Page1.oPag.oCDATTINF_1_8.value==this.w_CDATTINF)
      this.oPgFrm.Page1.oPag.oCDATTINF_1_8.value=this.w_CDATTINF
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CDOLDATTINF = this.w_CDOLDATTINF
    return

enddefine

* --- Define pages as container
define class tgsut_kwbPag1 as StdContainer
  Width  = 756
  height = 469
  stdWidth  = 756
  stdheight = 469
  resizeXpos=585
  resizeYpos=54
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_1 as StdButton with uid="ZMIZZNTUDJ",left=649, top=420, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per aggiornare le righe attributi selezionate";
    , HelpContextID = 32043802;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_1.Click()
      with this.Parent.oContained
        GSUT_BWB(this.Parent.oContained,.t.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_2 as StdButton with uid="JWIBHYOYEX",left=701, top=420, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 24746938;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_2.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZOOMATTR as cp_szoombox with uid="ELWFNUOYCH",left=5, top=24, width=744,height=393,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomOnZoom="",cZoomFile="gsut_kwb",bOptions=.t.,bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.t.,cMenuFile="",cTable="PRODCLAS",;
    cEvent = "Init,Requery",;
    nPag=1;
    , HelpContextID = 145933286

  add object oCDOLDATTINF_1_7 as StdField with uid="MQEHEKHGZY",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CDOLDATTINF", cQueryName = "CDOLDATTINF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo calcolato",;
    HelpContextID = 61646858,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=133, Top=426, InputMask=replicate('X',15)

  add object oCDATTINF_1_8 as StdField with uid="NMTXKRVPBH",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CDATTINF", cQueryName = "CDATTINF",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Nuovo codice attributo da assegnare",;
    HelpContextID = 55634580,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=431, Top=425, InputMask=replicate('X',15), bHasZoom = .t. 

  proc oCDATTINF_1_8.mZoom
    vx_exec("CDATTINF.VZM",this.Parent.oContained)
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc


  add object oBtn_1_9 as StdButton with uid="HJGSKITPKB",left=597, top=420, width=48,height=45,;
    CpPicture="bmp\visualiz.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per aprire la gestione";
    , HelpContextID = 209360997;
    , Caption='\<Classi doc.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      with this.Parent.oContained
        OpenGest("A","GSUT_MCD","CDCODCLA",.w_CDCODCLA)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_4 as StdString with uid="EPVMBTGNQP",Visible=.t., Left=13, Top=426,;
    Alignment=1, Width=115, Height=18,;
    Caption="Codice calcolato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="XIKCJHHPCI",Visible=.t., Left=289, Top=427,;
    Alignment=1, Width=136, Height=18,;
    Caption="Attributo da sostituire:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="QBGFQMSXUU",Visible=.t., Left=22, Top=4,;
    Alignment=0, Width=217, Height=17,;
    Caption="Attributi calcolati in automatico"    , ForeColor=RGB(0,192,0);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_12 as StdString with uid="IFDNUPEPOT",Visible=.t., Left=252, Top=4,;
    Alignment=0, Width=248, Height=17,;
    Caption="Attributi calcolati in questa elaborazione"    , ForeColor=RGB(192,0,0);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kwb','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
