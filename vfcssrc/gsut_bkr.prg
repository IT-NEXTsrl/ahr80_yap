* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bkr                                                        *
*              Query che recupera l'ultimo risultato del KPI                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-11-07                                                      *
* Last revis.: 2014-11-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_CODICE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bkr",oParentObject,m.w_CODICE)
return(i_retval)

define class tgsut_bkr as StdBatch
  * --- Local variables
  w_CODICE = space(10)
  w_FOUND = .f.
  w_RESXML = space(0)
  w_CURSOR = space(10)
  * --- WorkFile variables
  GSUT_BRK_idx=0
  ACTKEY_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_FOUND = .F.
    this.w_RESXML = .Null.
    * --- Create temporary table GSUT_BRK
    i_nIdx=cp_AddTableDef('GSUT_BRK') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.ACTKEY_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.ACTKEY_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"'' as xCode, 0 as xValue "," from "+i_cTable;
          +" where 1=0";
          )
    this.GSUT_BRK_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Se c'� la variabile globale uso quella
    *     (Utilizzata per la costruzione dei grafici modello)
    if Vartype(this.w_CODICE)<>"C" And Vartype(g_ParamKPI)=="C" And !Empty(g_ParamKPI)
      this.w_CODICE = g_ParamKPI
    endif
    if Vartype(this.w_CODICE)=="C" And !Empty(this.w_CODICE)
      * --- Select from query\gsut_bec
      do vq_exec with 'query\gsut_bec',this,'_Curs_query_gsut_bec','',.f.,.t.
      if used('_Curs_query_gsut_bec')
        select _Curs_query_gsut_bec
        locate for 1=1
        do while not(eof())
        this.w_FOUND = .T.
        this.w_RESXML = RKQRYXML
        * --- Esco subito perch� i risultati sono ordinati per data e ora:
        *     mi basta leggere il primo
        exit
          select _Curs_query_gsut_bec
          continue
        enddo
        use
      endif
      if this.w_FOUND And Vartype(this.w_RESXML)=="C" And !Empty(this.w_RESXML)
        * --- Un risultato � stato letto
        this.w_CURSOR = Sys(2015)
        XmlToCursor(this.w_RESXML,this.w_CURSOR,12) 
 CurToTab(this.w_CURSOR,"GSUT_BRK") 
 Use In Select(this.w_CURSOR)
      endif
    endif
  endproc


  proc Init(oParentObject,w_CODICE)
    this.w_CODICE=w_CODICE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='*GSUT_BRK'
    this.cWorkTables[2]='ACTKEY'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_query_gsut_bec')
      use in _Curs_query_gsut_bec
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_CODICE"
endproc
