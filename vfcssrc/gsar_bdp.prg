* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bdp                                                        *
*              Manutenzione badge                                              *
*                                                                              *
*      Author: Leopoldo Abbateggio                                             *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_20]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-07-27                                                      *
* Last revis.: 2010-06-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPARAM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bdp",oParentObject,m.pPARAM)
return(i_retval)

define class tgsar_bdp as StdBatch
  * --- Local variables
  pPARAM = space(10)
  w_CODDIPE = space(5)
  w_Messaggio = space(250)
  w_FLAG = space(2)
  w_OBJGEST = .NULL.
  w_CTRLGEST = .NULL.
  w_ANCODICE = space(15)
  w_NOCODICE = space(15)
  w_PACODAZI = space(5)
  * --- WorkFile variables
  DIPENDEN_idx=0
  CPAR_DEF_idx=0
  STRU_RIS_idx=0
  CONTI_idx=0
  OFF_NOMI_idx=0
  PAR_AGEN_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- >>>>>>>>>>>>> Da GSAR_ADP agli eventi Insert Start e Update Start
    * --- Lanciato dalla gestione dei dipendenti per evitare che dipendenti diversi abbiano lo stesso badge
    do case
      case this.pPARAM="BGD"
        if !empty(this.oParentObject.w_DPBADGE) and not bTrsErr
          * --- Read from DIPENDEN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DIPENDEN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2],.t.,this.DIPENDEN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "DPCODICE"+;
              " from "+i_cTable+" DIPENDEN where ";
                  +"DPBADGE = "+cp_ToStrODBC(this.oParentObject.w_DPBADGE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              DPCODICE;
              from (i_cTable) where;
                  DPBADGE = this.oParentObject.w_DPBADGE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CODDIPE = NVL(cp_ToDate(_read_.DPCODICE),cp_NullValue(_read_.DPCODICE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_Rows>0 and this.w_CODDIPE<>this.oParentObject.w_DPCODICE
            this.w_Messaggio = ah_MsgFormat("Il badge %1 � gi� stato assegnato%0al dipendente avente codice %2%0Salvataggio anagrafica non consentito", alltrim(this.oParentObject.w_DPBADGE), alltrim(this.w_CODDIPE))
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=this.w_Messaggio
          endif
        endif
        if !EMPTY(NVL(this.oParentObject.w_DPCODUTE,0)) AND !bTrsErr
          * --- Read from DIPENDEN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DIPENDEN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2],.t.,this.DIPENDEN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "DPCODICE"+;
              " from "+i_cTable+" DIPENDEN where ";
                  +"DPCODUTE = "+cp_ToStrODBC(this.oParentObject.w_DPCODUTE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              DPCODICE;
              from (i_cTable) where;
                  DPCODUTE = this.oParentObject.w_DPCODUTE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CODDIPE = NVL(cp_ToDate(_read_.DPCODICE),cp_NullValue(_read_.DPCODICE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_Rows>0 and this.w_CODDIPE<>this.oParentObject.w_DPCODICE And !Ah_YesNo("Il codice utente %1 � gi� stato assegnato alla persona %2%0Procedere comunque al salvataggio dell'anagrafica?", "",alltrim(str(this.oParentObject.w_DPCODUTE,4,0)), alltrim(this.w_CODDIPE))
            this.w_Messaggio = ah_MsgFormat("Salvataggio anagrafica sospeso come richiesto")
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=this.w_Messaggio
          endif
        endif
        if isAHE()
          this.oParentObject.NotifyEvent("AggLang")
        endif
      case this.pPARAM="COFR"
        * --- Al prossimo polling aggiorna i badge validi sul TRAX
        if not bTRSERR
          * --- Read from CPAR_DEF
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CPAR_DEF_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CPAR_DEF_idx,2],.t.,this.CPAR_DEF_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PDUPDTRX"+;
              " from "+i_cTable+" CPAR_DEF where ";
                  +"PDCHIAVE = "+cp_ToStrODBC("TAM");
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PDUPDTRX;
              from (i_cTable) where;
                  PDCHIAVE = "TAM";
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FLAG = NVL(cp_ToDate(_read_.PDUPDTRX),cp_NullValue(_read_.PDUPDTRX))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_FLAG = SUBSTR(NVL(this.w_FLAG,"NN"),1,1)+"B"
          * --- Write into CPAR_DEF
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.CPAR_DEF_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CPAR_DEF_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.CPAR_DEF_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PDUPDTRX ="+cp_NullLink(cp_ToStrODBC(this.w_FLAG),'CPAR_DEF','PDUPDTRX');
                +i_ccchkf ;
            +" where ";
                +"PDCHIAVE = "+cp_ToStrODBC("TAM");
                   )
          else
            update (i_cTable) set;
                PDUPDTRX = this.w_FLAG;
                &i_ccchkf. ;
             where;
                PDCHIAVE = "TAM";

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Impossibile scrivere sui parametri di commessa'
            return
          endif
        endif
      case this.pPARAM="COMM"
        * --- Al prossimo polling aggiorna le commesse valide sul TRAX
        if not bTRSERR
          * --- Read from CPAR_DEF
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CPAR_DEF_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CPAR_DEF_idx,2],.t.,this.CPAR_DEF_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PDUPDTRX"+;
              " from "+i_cTable+" CPAR_DEF where ";
                  +"PDCHIAVE = "+cp_ToStrODBC("TAM");
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PDUPDTRX;
              from (i_cTable) where;
                  PDCHIAVE = "TAM";
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FLAG = NVL(cp_ToDate(_read_.PDUPDTRX),cp_NullValue(_read_.PDUPDTRX))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_FLAG = "C"+SUBSTR(NVL(this.w_FLAG,"NN"),2,1)
          * --- Write into CPAR_DEF
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.CPAR_DEF_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CPAR_DEF_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.CPAR_DEF_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PDUPDTRX ="+cp_NullLink(cp_ToStrODBC(this.w_FLAG),'CPAR_DEF','PDUPDTRX');
                +i_ccchkf ;
            +" where ";
                +"PDCHIAVE = "+cp_ToStrODBC("TAM");
                   )
          else
            update (i_cTable) set;
                PDUPDTRX = this.w_FLAG;
                &i_ccchkf. ;
             where;
                PDCHIAVE = "TAM";

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Impossibile scrivere sui parametri di commessa'
            return
          endif
        endif
      case this.pPARAM=="AGGL"
        if IsAHE()
          GSAR_BRR(this.oParentObject , "N" )
        endif
      case this.pPARAM=="MANUTSTRAZ"
        if ((this.oParentObject.w_OLDFLVISI<>this.oParentObject.w_DPFLVISI) or (lower(this.oParentObject.cFunction)<>"edit" and this.oParentObject.bUpdated) OR this.oParentObject.GSAR_MSR.bUpdated OR this.oParentObject.w_CHKVARVI) AND (this.oParentObject.w_DPTIPRIS="G" and (ah_YesNo("E' necessario aggiornare la struttura aziendale ai fini di una corretta visualizzazione delle attivit� in agenda.%0 Vuoi aggiornare ora la struttura aziendale?")))
          this.w_OBJGEST = GSAR_KTW()
          if this.w_OBJGEST.bSec1
            this.w_CTRLGEST = this.w_OBJGEST.GetCtrl("\<Elabora")
            this.w_CTRLGEST.Click()     
            this.w_CTRLGEST = .NULL.
            this.w_OBJGEST.ecpQuit()     
            this.w_OBJGEST = .NULL.
          else
            ah_ErrorMsg("Impossibile accedere alla manutenzione della struttura aziendale")
          endif
        endif
      case this.pPARAM=="CANCGRUPPO"
        * --- Verifico se il codice della persona � utilizzato all'interno dell'Anagrafica clienti
        *     oppure nei debitori creditori diversi. Effettuo questa operazione solo se
        *     la variabile Dptipris vale 'P' (Persona)
        if this.oParentObject.w_DPTIPRIS="P"
          * --- Effettuo il controllo sulla tabella "Conti" solo se � stata attivata la gestione 
          *     del credito
          if Isahe() And g_GESCRE = "S" 
            * --- Read from CONTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CONTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ANCODICE"+;
                " from "+i_cTable+" CONTI where ";
                    +"ANGESCRE = "+cp_ToStrODBC(this.oParentObject.w_DPCODICE);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ANCODICE;
                from (i_cTable) where;
                    ANGESCRE = this.oParentObject.w_DPCODICE;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_ANCODICE = NVL(cp_ToDate(_read_.ANCODICE),cp_NullValue(_read_.ANCODICE))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if i_Rows>0
              this.w_Messaggio = ah_MsgFormat("La persona %1 � associata al cliente %2%0Eliminazione non consentita", alltrim(this.oParentObject.w_DPCODICE), alltrim(this.w_ANCODICE))
              * --- transaction error
              bTrsErr=.t.
              i_TrsMsg=this.w_Messaggio
              i_retcode = 'stop'
              return
            endif
          endif
          * --- Verifico se il codice persona che si intende cancellare � associato ai 
          *     debitori creditori diversi
          * --- Read from OFF_NOMI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "NOCODICE"+;
              " from "+i_cTable+" OFF_NOMI where ";
                  +"NOCOD_RU = "+cp_ToStrODBC(this.oParentObject.w_DPCODICE);
                  +" and NOFLGBEN = "+cp_ToStrODBC("B");
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              NOCODICE;
              from (i_cTable) where;
                  NOCOD_RU = this.oParentObject.w_DPCODICE;
                  and NOFLGBEN = "B";
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_NOCODICE = NVL(cp_ToDate(_read_.NOCODICE),cp_NullValue(_read_.NOCODICE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_Rows>0
            this.w_Messaggio = ah_MsgFormat("La persona %1 � associata al debitore creditore diverso %2%0Eliminazione non consentita", alltrim(this.oParentObject.w_DPCODICE), alltrim(this.w_NOCODICE))
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=this.w_Messaggio
            i_retcode = 'stop'
            return
          endif
        endif
        * --- Read from STRU_RIS
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.STRU_RIS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.STRU_RIS_idx,2],.t.,this.STRU_RIS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "SRCODICE"+;
            " from "+i_cTable+" STRU_RIS where ";
                +"SRRISCOL = "+cp_ToStrODBC(this.oParentObject.w_DPCODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            SRCODICE;
            from (i_cTable) where;
                SRRISCOL = this.oParentObject.w_DPCODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CODDIPE = NVL(cp_ToDate(_read_.SRCODICE),cp_NullValue(_read_.SRCODICE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_Rows>0 AND !EMPTY(NVL(this.w_CODDIPE, " "))
          do case
            case this.oParentObject.w_DPTIPRIS="G"
              this.w_Messaggio = ah_MsgFormat("Il gruppo %1 � presente nel gruppo %2%0Eliminazione gruppo non consentita", alltrim(this.oParentObject.w_DPCODICE), alltrim(this.w_CODDIPE))
            case this.oParentObject.w_DPTIPRIS="P"
              this.w_Messaggio = ah_MsgFormat("La persona %1 � presente nel gruppo %2%0Eliminazione persona non consentita", alltrim(this.oParentObject.w_DPCODICE), alltrim(this.w_CODDIPE))
            case this.oParentObject.w_DPTIPRIS="R"
              this.w_Messaggio = ah_MsgFormat("La risorsa %1 � presente nel gruppo %2%0Eliminazione risorsa non consentita", alltrim(this.oParentObject.w_DPCODICE), alltrim(this.w_CODDIPE))
          endcase
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_Messaggio
        endif
        * --- Verifico se il gruppo che si intende cancellare � utilizzato nei parametri agenda
        * --- Read from PAR_AGEN
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_AGEN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_AGEN_idx,2],.t.,this.PAR_AGEN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PACODAZI"+;
            " from "+i_cTable+" PAR_AGEN where ";
                +"PCGRUPAR = "+cp_ToStrODBC(this.oParentObject.w_DPCODICE);
                +" and PACODAZI = "+cp_ToStrODBC(i_codazi);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PACODAZI;
            from (i_cTable) where;
                PCGRUPAR = this.oParentObject.w_DPCODICE;
                and PACODAZI = i_codazi;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PACODAZI = NVL(cp_ToDate(_read_.PACODAZI),cp_NullValue(_read_.PACODAZI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_Rows>0
          this.w_Messaggio = ah_MsgFormat("Il gruppo %1 � presente nei parametri agenda%0Eliminazione gruppo non consentita", alltrim(this.oParentObject.w_DPCODICE))
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_Messaggio
          i_retcode = 'stop'
          return
        endif
    endcase
  endproc


  proc Init(oParentObject,pPARAM)
    this.pPARAM=pPARAM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='DIPENDEN'
    this.cWorkTables[2]='CPAR_DEF'
    this.cWorkTables[3]='STRU_RIS'
    this.cWorkTables[4]='CONTI'
    this.cWorkTables[5]='OFF_NOMI'
    this.cWorkTables[6]='PAR_AGEN'
    return(this.OpenAllTables(6))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPARAM"
endproc
