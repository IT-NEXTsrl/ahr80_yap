* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_mgz                                                        *
*              Gruppi di magazzino                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-12-19                                                      *
* Last revis.: 2012-10-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_mgz"))

* --- Class definition
define class tgsar_mgz as StdTrsForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 415
  Height = 287+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-10-05"
  HelpContextID=238454935
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  GRUMMAG_IDX = 0
  GRUDMAG_IDX = 0
  MAGAZZIN_IDX = 0
  cFile = "GRUMMAG"
  cFileDetail = "GRUDMAG"
  cKeySelect = "GRCODICE"
  cKeyWhere  = "GRCODICE=this.w_GRCODICE"
  cKeyDetail  = "GRCODICE=this.w_GRCODICE and GRCODMAG=this.w_GRCODMAG"
  cKeyWhereODBC = '"GRCODICE="+cp_ToStrODBC(this.w_GRCODICE)';

  cKeyDetailWhereODBC = '"GRCODICE="+cp_ToStrODBC(this.w_GRCODICE)';
      +'+" and GRCODMAG="+cp_ToStrODBC(this.w_GRCODMAG)';

  cKeyWhereODBCqualified = '"GRUDMAG.GRCODICE="+cp_ToStrODBC(this.w_GRCODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gsar_mgz"
  cComment = "Gruppi di magazzino"
  i_nRowNum = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  cAutoZoom = 'GSAR_MGZ'
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_GRCODICE = space(5)
  w_GRDESMAG = space(40)
  w_GRCODMAG = space(5)
  w_GRMGPRIN = space(1)
  w_DESMAG = space(40)
  w_OBTEST = space(10)
  w_TESTFORM = .F.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'GRUMMAG','gsar_mgz')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_mgzPag1","gsar_mgz",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Gruppo")
      .Pages(1).HelpContextID = 189903002
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oGRCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='MAGAZZIN'
    this.cWorkTables[2]='GRUMMAG'
    this.cWorkTables[3]='GRUDMAG'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.GRUMMAG_IDX,5],7]
    this.nPostItConn=i_TableProp[this.GRUMMAG_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_GRCODICE = NVL(GRCODICE,space(5))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_1_joined
    link_2_1_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from GRUMMAG where GRCODICE=KeySet.GRCODICE
    *
    i_nConn = i_TableProp[this.GRUMMAG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMMAG_IDX,2],this.bLoadRecFilter,this.GRUMMAG_IDX,"gsar_mgz")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('GRUMMAG')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "GRUMMAG.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"GRUDMAG.","GRUMMAG.")
      i_cTable = i_cTable+' GRUMMAG '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'GRCODICE',this.w_GRCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_OBTEST = i_datsys
        .w_TESTFORM = .f.
        .w_GRCODICE = NVL(GRCODICE,space(5))
        .w_GRDESMAG = NVL(GRDESMAG,space(40))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        cp_LoadRecExtFlds(this,'GRUMMAG')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from GRUDMAG where GRCODICE=KeySet.GRCODICE
      *                            and GRCODMAG=KeySet.GRCODMAG
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.GRUDMAG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GRUDMAG_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('GRUDMAG')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "GRUDMAG.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" GRUDMAG"
        link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'GRCODICE',this.w_GRCODICE  )
        select * from (i_cTable) GRUDMAG where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_DESMAG = space(40)
          .w_GRCODMAG = NVL(GRCODMAG,space(5))
          if link_2_1_joined
            this.w_GRCODMAG = NVL(MGCODMAG201,NVL(this.w_GRCODMAG,space(5)))
            this.w_DESMAG = NVL(MGDESMAG201,space(40))
          else
          .link_2_1('Load')
          endif
          .w_GRMGPRIN = NVL(GRMGPRIN,space(1))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace GRCODMAG with .w_GRCODMAG
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_GRCODICE=space(5)
      .w_GRDESMAG=space(40)
      .w_GRCODMAG=space(5)
      .w_GRMGPRIN=space(1)
      .w_DESMAG=space(40)
      .w_OBTEST=space(10)
      .w_TESTFORM=.f.
      if .cFunction<>"Filter"
        .DoRTCalc(1,3,.f.)
        if not(empty(.w_GRCODMAG))
         .link_2_1('Full')
        endif
        .w_GRMGPRIN = "N"
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(5,5,.f.)
        .w_OBTEST = i_datsys
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'GRUMMAG')
    this.DoRTCalc(7,7,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oGRCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oGRDESMAG_1_3.enabled = i_bVal
      .Page1.oPag.oObj_1_8.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oGRCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oGRCODICE_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'GRUMMAG',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.GRUMMAG_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GRCODICE,"GRCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GRDESMAG,"GRDESMAG",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.GRUMMAG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMMAG_IDX,2])
    i_lTable = "GRUMMAG"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.GRUMMAG_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_GRCODMAG C(5);
      ,t_GRMGPRIN N(3);
      ,t_DESMAG C(40);
      ,GRCODMAG C(5);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsar_mgzbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oGRCODMAG_2_1.controlsource=this.cTrsName+'.t_GRCODMAG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oGRMGPRIN_2_2.controlsource=this.cTrsName+'.t_GRMGPRIN'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESMAG_2_3.controlsource=this.cTrsName+'.t_DESMAG'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(69)
    this.AddVLine(368)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGRCODMAG_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.GRUMMAG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMMAG_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into GRUMMAG
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'GRUMMAG')
        i_extval=cp_InsertValODBCExtFlds(this,'GRUMMAG')
        local i_cFld
        i_cFld=" "+;
                  "(GRCODICE,GRDESMAG"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_GRCODICE)+;
                    ","+cp_ToStrODBC(this.w_GRDESMAG)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'GRUMMAG')
        i_extval=cp_InsertValVFPExtFlds(this,'GRUMMAG')
        cp_CheckDeletedKey(i_cTable,0,'GRCODICE',this.w_GRCODICE)
        INSERT INTO (i_cTable);
              (GRCODICE,GRDESMAG &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_GRCODICE;
                  ,this.w_GRDESMAG;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.GRUDMAG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GRUDMAG_IDX,2])
      *
      * insert into GRUDMAG
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(GRCODICE,GRCODMAG,GRMGPRIN,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_GRCODICE)+","+cp_ToStrODBCNull(this.w_GRCODMAG)+","+cp_ToStrODBC(this.w_GRMGPRIN)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,0,'GRCODICE',this.w_GRCODICE,'GRCODMAG',this.w_GRCODMAG)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_GRCODICE,this.w_GRCODMAG,this.w_GRMGPRIN,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.GRUMMAG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMMAG_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update GRUMMAG
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'GRUMMAG')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " GRDESMAG="+cp_ToStrODBC(this.w_GRDESMAG)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'GRUMMAG')
          i_cWhere = cp_PKFox(i_cTable  ,'GRCODICE',this.w_GRCODICE  )
          UPDATE (i_cTable) SET;
              GRDESMAG=this.w_GRDESMAG;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_GRCODMAG))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.GRUDMAG_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.GRUDMAG_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from GRUDMAG
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and GRCODMAG="+cp_ToStrODBC(&i_TN.->GRCODMAG)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and GRCODMAG=&i_TN.->GRCODMAG;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = 0
              replace GRCODMAG with this.w_GRCODMAG
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update GRUDMAG
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " GRMGPRIN="+cp_ToStrODBC(this.w_GRMGPRIN)+;
                     " ,GRCODMAG="+cp_ToStrODBC(this.w_GRCODMAG)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+;
                             " and GRCODMAG="+cp_ToStrODBC(GRCODMAG)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      GRMGPRIN=this.w_GRMGPRIN;
                     ,GRCODMAG=this.w_GRCODMAG;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere;
                                      and GRCODMAG=&i_TN.->GRCODMAG;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_GRCODMAG))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.GRUDMAG_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.GRUDMAG_IDX,2])
        *
        * delete GRUDMAG
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and GRCODMAG="+cp_ToStrODBC(&i_TN.->GRCODMAG)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and GRCODMAG=&i_TN.->GRCODMAG;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.GRUMMAG_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.GRUMMAG_IDX,2])
        *
        * delete GRUMMAG
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_GRCODMAG))) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.GRUMMAG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMMAG_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,7,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_8.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=GRCODMAG
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRCODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_GRCODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_GRCODMAG))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRCODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRCODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oGRCODMAG_2_1'),i_cWhere,'GSAR_AMA',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRCODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_GRCODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_GRCODMAG)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRCODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_GRCODMAG = space(5)
      endif
      this.w_DESMAG = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRCODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.MGCODMAG as MGCODMAG201"+ ",link_2_1.MGDESMAG as MGDESMAG201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on GRUDMAG.GRCODMAG=link_2_1.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and GRUDMAG.GRCODMAG=link_2_1.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oGRCODICE_1_1.value==this.w_GRCODICE)
      this.oPgFrm.Page1.oPag.oGRCODICE_1_1.value=this.w_GRCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oGRDESMAG_1_3.value==this.w_GRDESMAG)
      this.oPgFrm.Page1.oPag.oGRDESMAG_1_3.value=this.w_GRDESMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGRCODMAG_2_1.value==this.w_GRCODMAG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGRCODMAG_2_1.value=this.w_GRCODMAG
      replace t_GRCODMAG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGRCODMAG_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGRMGPRIN_2_2.RadioValue()==this.w_GRMGPRIN)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGRMGPRIN_2_2.SetRadio()
      replace t_GRMGPRIN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGRMGPRIN_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESMAG_2_3.value==this.w_DESMAG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESMAG_2_3.value=this.w_DESMAG
      replace t_DESMAG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESMAG_2_3.value
    endif
    cp_SetControlsValueExtFlds(this,'GRUMMAG')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsar_mgz
      * Prima esegue i controlli finali
      .w_TESTFORM=.T.
      if i_bRes
         .NotifyEvent('Controlli Finali')
         i_bRes = .w_TESTFORM
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (.cTrsName);
       where not(deleted()) and (not(Empty(t_GRCODMAG)));
        into cursor __chk__
    if not(1<=cnt)
      do cp_ErrorMsg with cp_MsgFormat(MSG_NEEDED_AT_LEAST__ROWS,"1"),"","",.F.
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_GRCODMAG))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_GRCODMAG)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_GRCODMAG=space(5)
      .w_GRMGPRIN=space(1)
      .w_DESMAG=space(40)
      .DoRTCalc(1,3,.f.)
      if not(empty(.w_GRCODMAG))
        .link_2_1('Full')
      endif
        .w_GRMGPRIN = "N"
    endwith
    this.DoRTCalc(5,7,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_GRCODMAG = t_GRCODMAG
    this.w_GRMGPRIN = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGRMGPRIN_2_2.RadioValue(.t.)
    this.w_DESMAG = t_DESMAG
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_GRCODMAG with this.w_GRCODMAG
    replace t_GRMGPRIN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGRMGPRIN_2_2.ToRadio()
    replace t_DESMAG with this.w_DESMAG
    if i_srv='A'
      replace GRCODMAG with this.w_GRCODMAG
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsar_mgzPag1 as StdContainer
  Width  = 411
  height = 287
  stdWidth  = 411
  stdheight = 287
  resizeXpos=259
  resizeYpos=195
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oGRCODICE_1_1 as StdField with uid="EEVLQTBRIV",rtseq=1,rtrep=.f.,;
    cFormVar = "w_GRCODICE", cQueryName = "GRCODICE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 70643797,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=113, Top=25, InputMask=replicate('X',5)

  add object oGRDESMAG_1_3 as StdField with uid="UVRYQPZOMP",rtseq=2,rtrep=.f.,;
    cFormVar = "w_GRDESMAG", cQueryName = "GRDESMAG",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 11542445,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=113, Top=48, InputMask=replicate('X',40)


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=3, top=78, width=403,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=3,Field1="GRCODMAG",Label1="Magazzino",Field2="DESMAG",Label2="Descrizione",Field3="GRMGPRIN",Label3="Principale",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 250760582


  add object oObj_1_8 as cp_runprogram with uid="OOFOBPVDBA",left=-3, top=307, width=182,height=19,;
    caption='GSAR_BGZ',;
   bGlobalFont=.t.,;
    prg="GSAR_BGZ('CONTROLLI')",;
    cEvent = "Controlli Finali",;
    nPag=1;
    , HelpContextID = 159584064

  add object oStr_1_2 as StdString with uid="ANODDRUBSM",Visible=.t., Left=28, Top=29,;
    Alignment=1, Width=84, Height=18,;
    Caption="Codice gruppo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="RHFMQXQVMS",Visible=.t., Left=2, Top=52,;
    Alignment=1, Width=110, Height=18,;
    Caption="Descrizione gruppo:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-5,top=96,;
    width=397+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-4,top=97,width=396+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='MAGAZZIN|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='MAGAZZIN'
        oDropInto=this.oBodyCol.oRow.oGRCODMAG_2_1
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsar_mgzBodyRow as CPBodyRowCnt
  Width=387
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oGRCODMAG_2_1 as StdTrsField with uid="FJSQVYRXPI",rtseq=3,rtrep=.t.,;
    cFormVar="w_GRCODMAG",value=space(5),isprimarykey=.t.,;
    HelpContextID = 264900525,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=62, Left=-2, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_GRCODMAG"

  func oGRCODMAG_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRCODMAG_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oGRCODMAG_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oGRCODMAG_2_1.readonly and this.parent.oGRCODMAG_2_1.isprimarykey)
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oGRCODMAG_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"",'',this.parent.oContained
   endif
  endproc
  proc oGRCODMAG_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_GRCODMAG
    i_obj.ecpSave()
  endproc

  add object oGRMGPRIN_2_2 as StdTrsCheck with uid="JNQSEVTGCL",rtrep=.t.,;
    cFormVar="w_GRMGPRIN",  caption="",;
    HelpContextID = 175984716,;
    Left=365, Top=0, Width=17,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oGRMGPRIN_2_2.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..GRMGPRIN,&i_cF..t_GRMGPRIN),this.value)
    return(iif(xVal =1,"S",;
    space(1)))
  endfunc
  func oGRMGPRIN_2_2.GetRadio()
    this.Parent.oContained.w_GRMGPRIN = this.RadioValue()
    return .t.
  endfunc

  func oGRMGPRIN_2_2.ToRadio()
    this.Parent.oContained.w_GRMGPRIN=trim(this.Parent.oContained.w_GRMGPRIN)
    return(;
      iif(this.Parent.oContained.w_GRMGPRIN=="S",1,;
      0))
  endfunc

  func oGRMGPRIN_2_2.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oDESMAG_2_3 as StdTrsField with uid="HHHGSSNOZF",rtseq=5,rtrep=.t.,;
    cFormVar="w_DESMAG",value=space(40),enabled=.f.,;
    HelpContextID = 161022518,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=293, Left=65, Top=0, InputMask=replicate('X',40)
  add object oLast as LastKeyMover
  * ---
  func oGRCODMAG_2_1.When()
    return(.t.)
  proc oGRCODMAG_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oGRCODMAG_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_mgz','GRUMMAG','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".GRCODICE=GRUMMAG.GRCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
