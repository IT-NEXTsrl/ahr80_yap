* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_beo                                                        *
*              Gestione operazione allegato                                    *
*                                                                              *
*      Author: Zucchetti S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-06-11                                                      *
* Last revis.: 2011-07-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_beo",oParentObject)
return(i_retval)

define class tgsut_beo as StdBatch
  * --- Local variables
  w_OBJ = .NULL.
  w_CURSORE = space(10)
  w_OPERAZ = space(1)
  * --- WorkFile variables
  UTE_NTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione operazione associata all'allegato, definita in 'Servizi Fax, mail ...'
    this.w_OBJ = this.oParentObject
    this.w_CURSORE = this.w_OBJ.w_ZoomGF.cCursor
    * --- Leggo l'operazione da eseguire
    * --- Read from UTE_NTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.UTE_NTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.UTE_NTI_idx,2],.t.,this.UTE_NTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "UTGESALL"+;
        " from "+i_cTable+" UTE_NTI where ";
            +"UTCODICE = "+cp_ToStrODBC(i_CODUTE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        UTGESALL;
        from (i_cTable) where;
            UTCODICE = i_CODUTE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_OPERAZ = NVL(cp_ToDate(_read_.UTGESALL),cp_NullValue(_read_.UTGESALL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_OPERAZ<>"N" and ! empty(this.oParentObject.w_ARCHIVIO)
      Select (this.w_CURSORE) 
 Go Top
      if reccount() = 1
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        i_retcode = 'stop'
        return
      endif
      Scan for IDALLPRI="S"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      exit
      EndScan
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valorizzo le variabili caller
    this.oParentObject.w_GFNOMEFILE = GFNOMEFILE
    this.oParentObject.w_GFPATHFILE = GFPATHFILE
    this.oParentObject.w_GFKEYINDIC = GFKEYINDIC
    this.oParentObject.w_GFORIGFILE = GFORIGFILE
    this.oParentObject.w_GFCLASDOCU = GFCLASDOCU
    this.oParentObject.w_GFIDMODALL = IDMODALL
    * --- Eseguo l'operazione richiesta
    do case
      case this.w_OPERAZ="V"
        do GSUT_BFG with this.w_OBJ,"V",this.oParentObject.w_Archivio,this.oParentObject.w_Chiave,GFPERIODO
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.w_OPERAZ="I"
        do GSUT_BFG with this.w_OBJ,"I",GFKEYINDIC,"",""
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.w_OPERAZ="M"
        do GSUT_BFG with this.w_OBJ,"M",this.oParentObject.w_Archivio,this.oParentObject.w_Chiave,GFPERIODO
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='UTE_NTI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
