* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_asi                                                        *
*              Sedi INAIL                                                      *
*                                                                              *
*      Author: Zucchetti TAM Srl & Zucchetti                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [116] [VRS_11]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-07-26                                                      *
* Last revis.: 2011-05-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_asi"))

* --- Class definition
define class tgscg_asi as StdForm
  Top    = 54
  Left   = 41

  * --- Standard Properties
  Width  = 598
  Height = 115+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-05-09"
  HelpContextID=158718615
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Constant Properties
  SE_INAIL_IDX = 0
  cFile = "SE_INAIL"
  cKeySelect = "SICODICE"
  cKeyWhere  = "SICODICE=this.w_SICODICE"
  cKeyWhereODBC = '"SICODICE="+cp_ToStrODBC(this.w_SICODICE)';

  cKeyWhereODBCqualified = '"SE_INAIL.SICODICE="+cp_ToStrODBC(this.w_SICODICE)';

  cPrg = "gscg_asi"
  cComment = "Sedi INAIL"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_SICODICE = space(5)
  w_SI__NOME = space(30)
  w_SI_INDIR = space(35)
  w_SI__CAP = space(8)
  w_SI_LOCAL = space(30)
  w_SI__PROV = space(2)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'SE_INAIL','gscg_asi')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_asiPag1","gscg_asi",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Sedi INAIL")
      .Pages(1).HelpContextID = 82171383
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSICODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='SE_INAIL'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.SE_INAIL_IDX,5],7]
    this.nPostItConn=i_TableProp[this.SE_INAIL_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_SICODICE = NVL(SICODICE,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from SE_INAIL where SICODICE=KeySet.SICODICE
    *
    i_nConn = i_TableProp[this.SE_INAIL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SE_INAIL_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('SE_INAIL')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "SE_INAIL.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' SE_INAIL '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'SICODICE',this.w_SICODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_SICODICE = NVL(SICODICE,space(5))
        .w_SI__NOME = NVL(SI__NOME,space(30))
        .w_SI_INDIR = NVL(SI_INDIR,space(35))
        .w_SI__CAP = NVL(SI__CAP,space(8))
        .w_SI_LOCAL = NVL(SI_LOCAL,space(30))
        .w_SI__PROV = NVL(SI__PROV,space(2))
        cp_LoadRecExtFlds(this,'SE_INAIL')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_SICODICE = space(5)
      .w_SI__NOME = space(30)
      .w_SI_INDIR = space(35)
      .w_SI__CAP = space(8)
      .w_SI_LOCAL = space(30)
      .w_SI__PROV = space(2)
      if .cFunction<>"Filter"
      endif
    endwith
    cp_BlankRecExtFlds(this,'SE_INAIL')
    this.DoRTCalc(1,6,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oSICODICE_1_1.enabled = i_bVal
      .Page1.oPag.oSI__NOME_1_3.enabled = i_bVal
      .Page1.oPag.oSI_INDIR_1_5.enabled = i_bVal
      .Page1.oPag.oSI__CAP_1_7.enabled = i_bVal
      .Page1.oPag.oSI_LOCAL_1_9.enabled = i_bVal
      .Page1.oPag.oSI__PROV_1_10.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oSICODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oSICODICE_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'SE_INAIL',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.SE_INAIL_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SICODICE,"SICODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SI__NOME,"SI__NOME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SI_INDIR,"SI_INDIR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SI__CAP,"SI__CAP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SI_LOCAL,"SI_LOCAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SI__PROV,"SI__PROV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.SE_INAIL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SE_INAIL_IDX,2])
    i_lTable = "SE_INAIL"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.SE_INAIL_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSCG_SSI with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.SE_INAIL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SE_INAIL_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.SE_INAIL_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into SE_INAIL
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'SE_INAIL')
        i_extval=cp_InsertValODBCExtFlds(this,'SE_INAIL')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(SICODICE,SI__NOME,SI_INDIR,SI__CAP,SI_LOCAL"+;
                  ",SI__PROV "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_SICODICE)+;
                  ","+cp_ToStrODBC(this.w_SI__NOME)+;
                  ","+cp_ToStrODBC(this.w_SI_INDIR)+;
                  ","+cp_ToStrODBC(this.w_SI__CAP)+;
                  ","+cp_ToStrODBC(this.w_SI_LOCAL)+;
                  ","+cp_ToStrODBC(this.w_SI__PROV)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'SE_INAIL')
        i_extval=cp_InsertValVFPExtFlds(this,'SE_INAIL')
        cp_CheckDeletedKey(i_cTable,0,'SICODICE',this.w_SICODICE)
        INSERT INTO (i_cTable);
              (SICODICE,SI__NOME,SI_INDIR,SI__CAP,SI_LOCAL,SI__PROV  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_SICODICE;
                  ,this.w_SI__NOME;
                  ,this.w_SI_INDIR;
                  ,this.w_SI__CAP;
                  ,this.w_SI_LOCAL;
                  ,this.w_SI__PROV;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.SE_INAIL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SE_INAIL_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.SE_INAIL_IDX,i_nConn)
      *
      * update SE_INAIL
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'SE_INAIL')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " SI__NOME="+cp_ToStrODBC(this.w_SI__NOME)+;
             ",SI_INDIR="+cp_ToStrODBC(this.w_SI_INDIR)+;
             ",SI__CAP="+cp_ToStrODBC(this.w_SI__CAP)+;
             ",SI_LOCAL="+cp_ToStrODBC(this.w_SI_LOCAL)+;
             ",SI__PROV="+cp_ToStrODBC(this.w_SI__PROV)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'SE_INAIL')
        i_cWhere = cp_PKFox(i_cTable  ,'SICODICE',this.w_SICODICE  )
        UPDATE (i_cTable) SET;
              SI__NOME=this.w_SI__NOME;
             ,SI_INDIR=this.w_SI_INDIR;
             ,SI__CAP=this.w_SI__CAP;
             ,SI_LOCAL=this.w_SI_LOCAL;
             ,SI__PROV=this.w_SI__PROV;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.SE_INAIL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SE_INAIL_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.SE_INAIL_IDX,i_nConn)
      *
      * delete SE_INAIL
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'SICODICE',this.w_SICODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.SE_INAIL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SE_INAIL_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,6,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSICODICE_1_1.value==this.w_SICODICE)
      this.oPgFrm.Page1.oPag.oSICODICE_1_1.value=this.w_SICODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oSI__NOME_1_3.value==this.w_SI__NOME)
      this.oPgFrm.Page1.oPag.oSI__NOME_1_3.value=this.w_SI__NOME
    endif
    if not(this.oPgFrm.Page1.oPag.oSI_INDIR_1_5.value==this.w_SI_INDIR)
      this.oPgFrm.Page1.oPag.oSI_INDIR_1_5.value=this.w_SI_INDIR
    endif
    if not(this.oPgFrm.Page1.oPag.oSI__CAP_1_7.value==this.w_SI__CAP)
      this.oPgFrm.Page1.oPag.oSI__CAP_1_7.value=this.w_SI__CAP
    endif
    if not(this.oPgFrm.Page1.oPag.oSI_LOCAL_1_9.value==this.w_SI_LOCAL)
      this.oPgFrm.Page1.oPag.oSI_LOCAL_1_9.value=this.w_SI_LOCAL
    endif
    if not(this.oPgFrm.Page1.oPag.oSI__PROV_1_10.value==this.w_SI__PROV)
      this.oPgFrm.Page1.oPag.oSI__PROV_1_10.value=this.w_SI__PROV
    endif
    cp_SetControlsValueExtFlds(this,'SE_INAIL')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgscg_asiPag1 as StdContainer
  Width  = 594
  height = 115
  stdWidth  = 594
  stdheight = 115
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSICODICE_1_1 as StdField with uid="JXXZRINJDM",rtseq=1,rtrep=.f.,;
    cFormVar = "w_SICODICE", cQueryName = "SICODICE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice sede",;
    HelpContextID = 150382229,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=48, Left=99, Top=21, InputMask=replicate('X',5)

  add object oSI__NOME_1_3 as StdField with uid="ZNLPRTTDUE",rtseq=2,rtrep=.f.,;
    cFormVar = "w_SI__NOME", cQueryName = "SI__NOME",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Nome della sede",;
    HelpContextID = 230365547,;
   bGlobalFont=.t.,;
    Height=21, Width=248, Left=230, Top=20, InputMask=replicate('X',30)

  add object oSI_INDIR_1_5 as StdField with uid="KLTHMOKEEB",rtseq=3,rtrep=.f.,;
    cFormVar = "w_SI_INDIR", cQueryName = "SI_INDIR",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo completo",;
    HelpContextID = 224061064,;
   bGlobalFont=.t.,;
    Height=21, Width=288, Left=99, Top=46, InputMask=replicate('X',35)

  add object oSI__CAP_1_7 as StdField with uid="VVKNAOKTAU",rtseq=4,rtrep=.f.,;
    cFormVar = "w_SI__CAP", cQueryName = "SI__CAP",;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "CAP",;
    HelpContextID = 252385574,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=99, Top=70, InputMask=replicate('X',8)

  add object oSI_LOCAL_1_9 as StdField with uid="BOGCQPLOPL",rtseq=5,rtrep=.f.,;
    cFormVar = "w_SI_LOCAL", cQueryName = "SI_LOCAL",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Localit� della sede",;
    HelpContextID = 28842354,;
   bGlobalFont=.t.,;
    Height=21, Width=248, Left=236, Top=70, InputMask=replicate('X',30)

  add object oSI__PROV_1_10 as StdField with uid="QZWWKMHGHU",rtseq=6,rtrep=.f.,;
    cFormVar = "w_SI__PROV", cQueryName = "SI__PROV",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia",;
    HelpContextID = 14358908,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=555, Top=70, InputMask=replicate('X',2)

  add object oStr_1_2 as StdString with uid="SSTBKDFVOO",Visible=.t., Left=1, Top=21,;
    Alignment=1, Width=96, Height=15,;
    Caption="Codice sede:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_4 as StdString with uid="ZIKCOYYRHT",Visible=.t., Left=150, Top=20,;
    Alignment=1, Width=77, Height=15,;
    Caption="Nome sede:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="KGBSTVXKKC",Visible=.t., Left=1, Top=46,;
    Alignment=1, Width=95, Height=15,;
    Caption="Indirizzo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="ABGVLPJTKU",Visible=.t., Left=3, Top=70,;
    Alignment=1, Width=93, Height=15,;
    Caption="CAP:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="EVILIIKYBI",Visible=.t., Left=173, Top=73,;
    Alignment=1, Width=60, Height=18,;
    Caption="Localit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="IIRXBWIMTL",Visible=.t., Left=489, Top=70,;
    Alignment=1, Width=64, Height=15,;
    Caption="Provincia:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_asi','SE_INAIL','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".SICODICE=SE_INAIL.SICODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
