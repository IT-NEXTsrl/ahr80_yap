* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bfe                                                        *
*              Verifica calendari festivitÓ predefiniti                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-04-20                                                      *
* Last revis.: 2012-09-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bfe",oParentObject)
return(i_retval)

define class tgsar_bfe as StdBatch
  * --- Local variables
  GSAR_MFE = .NULL.
  w_NumRighe = 0
  w_DATAFEST = ctod("  /  /  ")
  w_CODICE = space(3)
  w_DESCRI = space(35)
  w_TROVATO = .f.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica calendari festivitÓ predefiniti
    this.w_TROVATO = .F.
    if this.oParentObject.w_FEPREDEF="S"
      this.GSAR_MFE = this.oParentobject
      this.w_NumRighe = this.GSAR_MFE.NUmRow()
      if EMPTY(this.w_NumRighe)
        * --- Non sono presenti righe (festivitÓ)
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=ah_MsgFormat("impossibile impostare come predefinito il calendario: non sono state inserite festivitÓ.")
      else
        this.GSAR_MFE.MarkPos()     
        this.GSAR_MFE.FirstRow()     
        * --- Legge la data della festivitÓ sulla prima riga
        this.w_DATAFEST = this.GSAR_MFE.Get("FEDATFES")
        this.GSAR_MFE.RePos()     
        * --- Estrae le festivitÓ del medesimo anno incluse in un calendario predefinito
        * --- Select from QUERY\GSAR_BFE
        do vq_exec with 'QUERY\GSAR_BFE',this,'_Curs_QUERY_GSAR_BFE','',.f.,.t.
        if used('_Curs_QUERY_GSAR_BFE')
          select _Curs_QUERY_GSAR_BFE
          locate for 1=1
          do while not(eof())
          this.w_TROVATO = .T.
          this.w_CODICE = _Curs_QUERY_GSAR_BFE.FECODICE
          this.w_DESCRI = _Curs_QUERY_GSAR_BFE.FEDESCRI
            select _Curs_QUERY_GSAR_BFE
            continue
          enddo
          use
        endif
        if this.w_TROVATO
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=ah_MsgFormat("impossibile impostare come predefinito il calendario attuale. Il calendario %1 ( %2 ) giÓ impostato come predefinito.", ALLTRIM(this.w_CODICE), ALLTRIM(this.w_DESCRI) )
        endif
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  proc CloseCursors()
    if used('_Curs_QUERY_GSAR_BFE')
      use in _Curs_QUERY_GSAR_BFE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
