* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_blp                                                        *
*              Legge risposta dal client                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-11-19                                                      *
* Last revis.: 2014-11-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_CODUTE,w_Protcript,w_UFILOGG,w_UDATINV,w_UFILMIT
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_blp",oParentObject,m.w_CODUTE,m.w_Protcript,m.w_UFILOGG,m.w_UDATINV,m.w_UFILMIT)
return(i_retval)

define class tgsut_blp as StdBatch
  * --- Local variables
  w_CODUTE = space(5)
  w_Protcript = .f.
  w_UFILOGG = space(100)
  w_UDATINV = ctod("  /  /  ")
  w_UFILMIT = space(254)
  w_Indserver = space(100)
  w_Porta = 0
  w_Account = space(10)
  w_Pwd = space(10)
  w_MODALLEG = space(1)
  w_TIPOARCH = space(1)
  w_CLAALLE = space(5)
  w_TIPPATH = space(1)
  w_TIPCLA = space(1)
  w_TIPOALLE = space(5)
  w_PATHSTD = space(254)
  w_CDALIASF = space(15)
  w_CDFLGDES = space(1)
  w_PATIPARC = space(1)
  w_LICLADOC = space(15)
  w_IDTIPBST = space(1)
  w_ORAARCH = space(8)
  w_DATAARCH = ctod("  /  /  ")
  w_UTFILOGG = space(100)
  w_UTDATINV = ctot("")
  w_pathfile = space(100)
  w_READAZI = space(5)
  w_AR_TABLE = space(20)
  w_UTFILMIT = space(254)
  w_Account = space(10)
  w_Pwd = space(10)
  w_UM_EMAIL = space(100)
  w_zPop = .NULL.
  w_FILELOG = space(200)
  hLOG = 0
  w_STRINGA = space(100)
  w_nTotMsgs = 0
  w_nTotMessaggi = 0
  w_sUidsList = space(0)
  w_nMsgToProcess = 0
  w_indice = 0
  w_ind = 0
  w_CODUID = space(100)
  w_TOTMSG = 0
  w_DATINV = ctod("  /  /  ")
  w_NOELENCO = .f.
  w_TOTMSGARC = 0
  w_ORIG_UID = space(200)
  w_msgDim = 0
  w_msgHeader = .NULL.
  w_sFrom = space(100)
  w_sFromHead = space(100)
  w_sSubject = space(100)
  w_sDateSent = ctod("  /  /  ")
  w_sSubjectHead = space(100)
  w_sDateSentHead = ctod("  /  /  ")
  w_NICODPRA = space(10)
  w_NIPRADES = space(10)
  w_msgToProcess = .NULL.
  w_NOMFIL = space(254)
  w_nParts = 0
  w_msgPart = .NULL.
  w_NOMFILPART = space(254)
  w_PATHTEMP = space(254)
  w_COMPANYLIST = space(10)
  w_PATHATTPEC = space(254)
  w_OBJ_Notifica = .NULL.
  w_OBJ_From = .NULL.
  w_msgPart = .NULL.
  w_RESULT = space(100)
  w_NUMARR = 0
  w_NICODPRA = space(10)
  w_NIPRADES = space(10)
  w_NOMFILE = space(19)
  w_CODAZI = space(5)
  w_AUNOMFIL = space(100)
  w_TROVATA = .f.
  w_LENARR = 0
  w_NOTIFICA = space(2)
  w_TIPORIS = space(2)
  w_OGGETTO = space(100)
  w_NISERIAL = space(10)
  w_FILESITO = space(200)
  w_TOTNOT = 0
  w_INSERISCI = .f.
  w_TOTNOT = 0
  w_FILE_BBA = space(100)
  w_ESITO = 0
  w_DESESITO = space(0)
  w_IDSERIAL = space(20)
  w_IDTIPATT = space(1)
  * --- WorkFile variables
  ANA_CUID_idx=0
  CONTROPA_idx=0
  TMP_FILE_idx=0
  AZIENDA_idx=0
  UTEMAILS_idx=0
  PROMCLAS_idx=0
  PROMINDI_idx=0
  AUT_LPE_idx=0
  runtime_filters = 2

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_TOTMSG = 0
    this.w_TOTNOT = 0
    this.w_READAZI = i_codazi
    * --- Gestione degli errori necessaria per la creazione
    *      e l'utilizzo dell'oggetto. Gli errori verrebbero
    *       altrimenti visualizzati da FoxPro a video con la 
    *       finestra cancel/ignore/suspend
    *       Creazione oggetto per il collegamento pop3
    *       La libreria (DLL) � creata con Visual Studio 2010
    *       con target Microsoft .NET Framework 2.0
    *       Per poter essere utilizzata come oggetto COM dai
    *       programmi tipo Microsoft Visual FoxPro occorre registrarla
    *       con l'apposita utility RegAsm.exe
    *       Questa si trova nell'installazione di Microsoft .NET Framework
    *       Es. C:\Windows\Microsoft.NET\Framework64\v2.0.50727\RegAsm.exe
    *       Occorre avere i diritti di amministrazione
    *       Se la registrazione va a buon fine da linea di comando si legger�
    *       un messaggio di conferma, altrimenti un messaggio di errore
    *       Se lanciata da routine si pu� verificare il valore di ritorno
    *       che sar� 0 in caso di successo o di 100 se si � verificato un errore
    *       Es. Mancanza di diritti, file non trovato, ecc.
    *     zPop = NEWOBJECT("zOpenPop3")
    this.w_pathfile = TempAdhoc()
    this.w_AR_TABLE = "ISC_MAST"
    * --- Read from PROMCLAS
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PROMCLAS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CDCODCLA,CDMODALL,CDTIPARC,CDCLAALL,CDCOMTIP,CDCOMCLA,CDTIPALL,CDPATSTD,CDALIASF,CDFLGDES"+;
        " from "+i_cTable+" PROMCLAS where ";
            +"CDRIFTAB = "+cp_ToStrODBC(this.w_AR_TABLE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CDCODCLA,CDMODALL,CDTIPARC,CDCLAALL,CDCOMTIP,CDCOMCLA,CDTIPALL,CDPATSTD,CDALIASF,CDFLGDES;
        from (i_cTable) where;
            CDRIFTAB = this.w_AR_TABLE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_LICLADOC = NVL(cp_ToDate(_read_.CDCODCLA),cp_NullValue(_read_.CDCODCLA))
      this.w_MODALLEG = NVL(cp_ToDate(_read_.CDMODALL),cp_NullValue(_read_.CDMODALL))
      this.w_TIPOARCH = NVL(cp_ToDate(_read_.CDTIPARC),cp_NullValue(_read_.CDTIPARC))
      this.w_CLAALLE = NVL(cp_ToDate(_read_.CDCLAALL),cp_NullValue(_read_.CDCLAALL))
      this.w_TIPPATH = NVL(cp_ToDate(_read_.CDCOMTIP),cp_NullValue(_read_.CDCOMTIP))
      this.w_TIPCLA = NVL(cp_ToDate(_read_.CDCOMCLA),cp_NullValue(_read_.CDCOMCLA))
      this.w_TIPOALLE = NVL(cp_ToDate(_read_.CDTIPALL),cp_NullValue(_read_.CDTIPALL))
      this.w_PATHSTD = NVL(cp_ToDate(_read_.CDPATSTD),cp_NullValue(_read_.CDPATSTD))
      this.w_CDALIASF = NVL(cp_ToDate(_read_.CDALIASF),cp_NullValue(_read_.CDALIASF))
      this.w_CDFLGDES = NVL(cp_ToDate(_read_.CDFLGDES),cp_NullValue(_read_.CDFLGDES))
      this.w_PATIPARC = NVL(cp_ToDate(_read_.CDMODALL),cp_NullValue(_read_.CDMODALL))
      use
      if i_Rows=0
        cp_ErrorMsg("Attenzione, classe documentale per le buste non presente",'i','')
        i_retcode = 'stop'
        return
      endif
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Create temporary table TMP_FILE
    i_nIdx=cp_AddTableDef('TMP_FILE') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('GSUT_BLP',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMP_FILE_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    this.w_DATAARCH = i_DATSYS
    this.w_ORAARCH = ""
    * --- Select from UTEMAILS
    i_nConn=i_TableProp[this.UTEMAILS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.UTEMAILS_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" UTEMAILS ";
          +" where UMCODUTE="+cp_ToStrODBC(this.w_CODUTE)+" and (UM_INVIO='P' or UM_INVIO='S')";
           ,"_Curs_UTEMAILS")
    else
      select * from (i_cTable);
       where UMCODUTE=this.w_CODUTE and (UM_INVIO="P" or UM_INVIO="S");
        into cursor _Curs_UTEMAILS
    endif
    if used('_Curs_UTEMAILS')
      select _Curs_UTEMAILS
      locate for 1=1
      do while not(eof())
      this.w_UM_EMAIL = _Curs_UTEMAILS.UM_EMAIL
      if Empty(this.w_UDATINV)
        this.w_UTDATINV = Nvl(_Curs_UTEMAILS.UMDATINV,cp_CharToDateTime("  -  -       :  :  "))
      endif
      if Empty(this.w_UFILOGG)
        this.w_UTFILOGG = Nvl(_Curs_UTEMAILS.UMFILOGG," ")
      endif
      if Empty(this.w_UTFILMIT)
        this.w_UTFILMIT = Nvl(_Curs_UTEMAILS.UMFILMIT," ")
      endif
      * --- Read from AUT_LPE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AUT_LPE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AUT_LPE_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "LPPSW,LPLOGIN"+;
          " from "+i_cTable+" AUT_LPE where ";
              +"LPCODUTE = "+cp_ToStrODBC(this.w_CODUTE);
              +" and LP_EMAIL = "+cp_ToStrODBC(this.w_UM_EMAIL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          LPPSW,LPLOGIN;
          from (i_cTable) where;
              LPCODUTE = this.w_CODUTE;
              and LP_EMAIL = this.w_UM_EMAIL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PWD = NVL(cp_ToDate(_read_.LPPSW),cp_NullValue(_read_.LPPSW))
        this.w_ACCOUNT = NVL(cp_ToDate(_read_.LPLOGIN),cp_NullValue(_read_.LPLOGIN))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_Indserver = _Curs_UTEMAILS.UMSERVEI
      this.w_Porta = _Curs_UTEMAILS.UM_PORTI
      this.w_Pwd = cifracnf(this.w_Pwd,"D")
      if ! Directory(ADDBS(this.w_PATHFILE) + "Messaggi\")
         MD (ADDBS(this.w_PATHFILE) + "Messaggi\")
      endif
      * --- Path file inviato originariamente
      btrserr=.f.
       lOldErr=ON("Error")
      ON ERROR bErr=.T.
      this.w_zPop = NewObject("zOpenPop3")
      if Vartype(this.w_zPop)="O"
        * --- Oggetto libreria di collegamento creato correttamente
        *      Attivazione log errori
        *     Parametri
        *                     Percorso e nome file dove scrivere il log (Stringa) 
        *     	   Il percorso deve esistere, essere valido,raggiungibile e con diritti di scrittura
        *     	 Ritorna niente
        *     	 Il log degli errori pu� essere utile nel caso di errori 
        *     	 imprevisti in fase di runtime quindi � bene prevedere   
        *     	 una variabile globale da inserire eventualmente nel cnf 
        *     	 per attivare il log su file quando necessario.          
        *     	 Quando si utilizza la libreria occorre gestire gli      
        *     	 errori in quanto i metodi in caso di problemi escono    
        *     	 notificando un errore, il fox cambia la MESSAGE() ma    
        *     	 non cambia il numero ERROR() quindi occorre verificare  
        *     	 la variabile messa nella ON ERROR dopo ogli metodo.     
        *     	 se varia � possibile avere maggiori indicazioni dalla   
        *     	 MESSAGE() e decidere nel caso se proseguire con         
        *     	 l'elaborazione o fermarsi                               
        LnomFilLog=sys(2015)
        this.w_FILELOG = AddBs(tempadhoc())+forceext(LNomFilLog,"log")
        this.w_zPop.SetFileLog(AddBs(tempadhoc())+forceext(LNomFilLog+"_bis","log"))     
        this.hLOG = FCREATE(this.w_FILELOG,0)
        * --- Collegamento con il server di posta pop3
        *     Parametri
        *     	 Indirizzo del server (Stringa)
        *     	 Porta di collegamento (Numero)
        *     	Utilizzare protocollo criptato (.T. o .F.)
        *     	Ritorna
        *     	Niente
        this.w_zPop.Connect_2(Alltrim(this.w_Indserver),this.w_Porta,this.w_Protcript)     
        * --- In alternativa � possibile utilizzare Connect_3
        *     	Parametri
        *     	Indirizzo del server (Stringa)
        *     	Porta di collegamento (Numero)
        *     	Utilizzare protocollo criptato (.T. o .F.)
        *     	Tempo in millisecondi per il timeout di risposta (Default 60000)
        *     	Tempo in millisecondi per il timeout di invio (Default 60000)
        *                      Ritorna niente
        *     	==================================================
        *     	Es. zPop.Connect_3("pop.gmail.com",995,.T., 90000,60000)
        *     	==================================================
        if this.w_zPop.Connected()
          this.w_STRINGA =  "Inizio: "+Time()
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Autenticazione utente con il server
          *     		 Parametri
          *     		Username (Stringa)
          *     		Password (Stringa)
          *     		Ritorna Niente	
          this.w_zPop.Authenticate(Alltrim(this.w_account),Alltrim(this.w_pwd))     
          * --- Recupero il numero di messaggi presenti sul server
          *     		Parametri
          *     		Nessuno
          *     		Ritorna  valore intero (Numero di messaggi presenti sul server)
          this.w_nTotMsgs = this.w_zPop.GetMessageCount()
          this.w_nTotMessaggi = this.w_nTotMsgs
          * --- Recupero uID dei vari messaggi sul server
          *     		Parametri
          *     		Separatore di elementi (Stringa) (opzionale): carattere che separa gli uID, se non fornito viene utilizzato il | [pipe]
          *     		Ritorna stringa (Lista di uID separati dal carattere separatore)
          this.w_sUidsList = this.w_zPop.GetStringMessageUids()
          * --- Trasformo la stringa di uID in array per praticit�
          ALINES(aUids, this.w_sUidsList,4+1,"|")
          * --- La lista degli uID viene fornita in modo tale che il primo uID corrisponda
          *     		 al primo messaggio, il secondo uID al secondo messaggio e cos� via
          *     		 molto utile per verificare se un messaggio � gi� stato processato o meno
          *     		 a tal scopo si RACCOMANDA l'utilizzo di una sistema (Es. tabella su database)
          *     		 che MEMORIZZI gli uID PROCESSATI in modo da evitare di scaricare nuovamente
          *     		 messaggi di posta gi� scaricati e/o processati (potenzialmente molto pesanti)
          *     		
          *     		 Ipotizzando che tutti i messaggi tranne l'ultimo siano gi� stati processati
          *     		 verranno di seguito mostrati i metodi necessari per
          *     		 1) Verificare la dimensione del messaggio da scaricare
          *     		 2) Scaricare l'intestazione del messaggio (Utile per verificare ad esempio il mittente
          *     		    o altri dati presenti nell'header. I controlli potrebbero essere utili per evitare
          *     		    di scaricare messaggi molto grossi e non pertinenti allo scopo
          *     		    (Es. mittenti non validi o sconosciuti, privi di allegati o con allegati non utili)
          *     		 3) Scaricare il messaggio di posta integralmente e salvarlo come file EML
          *     		    Utile per un archiviazione sostitutiva se necessaria
          *     		    Il nome del file deve essere comprensivo di percorso e tutte le cartelle del
          *     		    percorso fornito devono esistere, essere raggiungibili e essere scrivibili
          *     		 4) Recuperare il numero di allegati presenti nel messaggio
          *     		 5) Recuperare la lista dei nome dei file allegati al messaggio
          *     		 6) Scaricare tutti gli allegati in una data directory
          *     		    la directory deve esistere, essere raggiungibile e essere scrivibile
          *     		 7) Scaricare solamente un allegato in una data directory
          *     		    la directory deve esistere, essere raggiungibile e essere scrivibile
          *     		 Se il messaggio processato � ti tipo pec � di fatto una notifica con all'interno
          *     		 alcuni allegati (ricevuta, certificato digitale, ecc), ma il messaggio originario
          *     		 inviato alla casella di posta � incluso come parte del messaggio principale
          *     		 con un determinato "MediaType" occorre quindi analizzare il messaggio caricato e
          *     		 e nel caso salvare tale parte (formato EML) ricaricarla e processarla
          *     		 come messaggio normale, quindi si procede con i seguenti punti:
          *     		 8) Caricamento di un file EML salvato nella libreria
          *     		 9) Analisi del mesaggio per scoprire eventuali parti di mail PEC
          *     		 10)Salvataggio del messaggio PEC, con successivo caricamento dello stesso
          *     		 11)Esempio di esecuzione di uno o pi� metodi gi� illustrati nei precedenti punti da 4 a 7
          this.w_indice = this.w_nTotMsgs
          this.w_ind = 0
          this.w_STRINGA =  "Totale messaggi scaricati: " +Alltrim(Str(this.w_INDICE))
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_NOELENCO = .t.
          Select * from (this.oParentObject.w_ZOOMUID.Ccursor) where XCHK=1 into cursor TMP_UID
          this.w_TOTMSGARC = Reccount("TMP_UID")
          if Reccount("TMP_UID") = 0
            Vq_exec("gsutublp.vqr",This,"TMP_UID") 
 Select * from TMP_UID into Array ARR_UID
            this.w_TOTMSGARC = Reccount("TMP_UID")
            Use inTMP_UID
            do while this.w_indice>0
              this.w_nMsgToProcess = this.w_indice
              this.w_CODUID = Left(aUids[this.w_INDICE],254)
              if ASCAN(ARR_UID,Alltrim(this.w_CODUID))=0
                this.w_ind = this.w_ind + 1
                AH_MSG( "Elaborazione messaggio...%1" , .T. ,,,ALLTRIM(STR(this.w_IND)))
                this.Page_2()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              else
                this.w_ORIG_UID = aUids[this.w_INDICE]
                this.w_STRINGA =  "Messaggio  con uid " + Alltrim(this.w_ORIG_UID) + " scartato: gi� presente in anagrafica messaggi"
                this.Pag3()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
              this.w_indice = this.w_indice-1
              if Not Empty(this.w_PATHTEMP) and Directory(this.w_PATHTEMP)
                DELETE file(Alltrim(Addbs(this.w_PATHTEMP))+"*.*")
              endif
            enddo
            Release ARR_UID
          else
            Select TMP_UID 
 Go top 
 Scan
            this.w_CODUID = AUCODUID
            this.w_nMsgToProcess = ASCAN(aUids,Alltrim(this.w_CODUID),1,0)
            this.w_ind = this.w_ind + 1
            AH_MSG( "Elaborazione messaggio...%1" , .T. ,,,ALLTRIM(STR(this.w_IND)))
            if this.w_nmsgToProcess>0
              this.Page_2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            if Not Empty(this.w_PATHTEMP) and Directory(this.w_PATHTEMP)
              DELETE file(Alltrim(Addbs(this.w_PATHTEMP))+"*.*")
            endif
            Endscan
          endif
          Use in TMP_UID
          this.w_STRINGA =  "Fine elaborazione: "+Time()
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
           
 aFilesName = .NULL. 
 aUids = .NULL.
          this.w_sUidsList = " "
          this.w_zPop = .Null.
        else
          cp_ErrorMsg("Non connesso",'i','')
        endif
      else
        cp_ErrorMsg("Errore creazione oggetto di collegamento",'i','')
      endif
      if btrserr
        btrserr=.f.
        cp_ErrorMsg(cp_msgformat("Errore nell'esecuzione: %1",Message()),'i','')
      endif
      if Vartype("g_VFPDELFOLDER")="U"
        PUBLIC g_VFPDELFOLDER
      endif
      OLDVFP= g_VFPDELFOLDER
      * --- Elimino file messaggi
       g_VFPDELFOLDER=.T
      if Not Empty(this.w_PATHFILE)
        =DELETEFOLDER(ADDBS(this.w_PATHFILE) + "Messaggi\"," ")
      endif
       g_VFPDELFOLDER=OLDVFP
      if Not Empty(this.w_PATHATTPEC) and Directory(this.w_PATHATTPEC)
        DELETE file(Alltrim(Addbs(this.w_PATHATTPEC))+"*.p7m")
      endif
      if Not Empty(this.w_PATHTEMP) and Directory(this.w_PATHTEMP)
        DELETE file(Alltrim(Addbs(this.w_PATHTEMP))+"*.*")
      endif
       on error &lOldErr
      FCLOSE(this.hLOG)
        select _Curs_UTEMAILS
        continue
      enddo
      use
    endif
    * --- Drop temporary table TMP_FILE
    i_nIdx=cp_GetTableDefIdx('TMP_FILE')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_FILE')
    endif
    cp_ErrorMsg(cp_msgformat("Elaborazione terminata%0letti %1 messaggi",Alltrim(str(this.w_TOTMSG)),Alltrim(str(this.w_TOTNOT))),'i','')
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica della dimensione del messaggio da scaricare [1]
    *     		  Parametri
    *     		   Messaggio da recuperare (intero)
    *     		  Ritorna
    *     		   Valore intero (Dimensione del messaggio in byte)
    this.w_STRINGA = " "
    this.w_msgDim = this.w_zPop.GetMessageSize(this.w_nMsgToProcess)
    LnomFil=" "
    this.w_NOMFILPART = " "
    * --- Recupero l'intestazione del messaggio per eventuali controlli [2]
    *     		  Parametri
    *     		   Messaggio da recuperare (intero)
    *     		  Ritorna
    *     		   Oggetto (Intestazione del messaggio)
    this.w_msgHeader = this.w_zPop.GetMessageHeaders(this.w_nMsgToProcess)
    * --- Alcune propriet� dell'header del messaggio
    *     		 nessuna � obbligatoria quindi alcune potrebbero essere vuote 
    this.w_sFrom = " "
    this.w_sSubject = " "
    this.w_sFromHead = this.w_msgHeader.From.MailAddress.toString()
    this.w_sSubjectHead = this.w_msgHeader.Subject
    this.w_sSubjectHead = this.w_msgHeader.Subject
    this.w_sDateSentHead = this.w_msgHeader.DateSent
    this.w_sDateSent = this.w_msgHeader.DateSent
    this.w_AUNOMFIL = " "
    this.w_NICODPRA = " "
    this.w_NIPRADES = " "
    * --- Eseguo filtro su messaggi  
    * --- Recupero il messaggio da processare (utile per tutti i passi successivi)
    *     		  Parametri
    *     		   Messaggio da recuperare (intero)
    *     		  Ritorna
    *     		   Oggetto (Messaggio)
    this.w_msgToProcess = this.w_zPop.GetMessage(this.w_nMsgToProcess)
    * --- Salvataggio del messaggio su file [3]
    *     		  Parametri
    *     		   Percorso e nome per il salvataggio del file (stringa)
    *     		  Ritorna
    *     		   Niente
    *     		  WARNING
    *     		   Se il file esiste gi� viene sovrascritto senza avvertimento
    LnomFil=sys(2015)
    this.w_NOMFIL = forceext(ADDBS(this.w_PATHFILE) + "Messaggi\"+Alltrim(LnomFil),".eml")
    this.w_msgToProcess.SaveToFile(this.w_NOMFIL)     
    * --- =======================================
    *     ===   RESET VARIABILI PER SICUREZZA       ====
    *     =======================================
    this.w_msgToProcess = .Null.
    this.w_msgHeader = .Null.
    this.w_nTotFileAttach = -1
    this.w_attachmentsFilesName = " "
    this.w_sUidsList = " "
    this.w_msgDim = -1
    * --- Caricamento di un messaggio salvato in precedenza in formato EML [8]
    *     		  Parametri
    *     		   Messaggio da caricare (Stringa)
    *     		  Ritorna
    *     		   Oggetto (messaggio)
    this.w_msgToProcess = this.w_zPop.LoadMessageFromPath(this.w_NOMFIL)
    * --- [4]
    this.w_nTotFileAttach = this.w_msgToProcess.GetAttachmentsNumber()
    * --- [7]
    *     		 I precedenti metodi restituiscono gli allegati dela PEC principale ma non il
    *     		 reale messaggio PEC inviatoci occorre quindi verificare se esiste un messaggio PEC
    *     		 Verifico le parti incluse nel messaggio con un determinato "MediaType" [9]
    *     		  Parametri
    *     		   Tipo di "MediaType" cercato (String) (opzionale) Se non passato verr� utilizzato "message/rfc822" 
    *     		  Ritorna
    *     		   Valore intero (Numero di parti presenti nel messaggio)
    this.w_nParts = this.w_msgToProcess.MediaTypeNumber()
    this.w_NOMFILPART = " "
    if this.w_nParts > 0
      * --- Recupero la parte di messaggio PEC [10]
      *     			 Parametri
      *     			  Parte da recuperare (Intero)
      *     			  Tipo di "MediaType" cercato (String) (opzionale) Se non passato verr� utilizzato "message/rfc822" 
      *     			 Ritorna
      *     			  Oggetto (MessagePart cercata)	
      this.w_msgPart = this.w_msgToProcess.GetMessagePartWithMediaType(1)
      * --- Salvo la parte di messaggio recuperata (Come punto [3] di un messaggio normale)
      this.w_NOMFILPART = ADDBS(this.w_PATHFILE) + "Messaggi\" + Alltrim(LnomFil) +"_Parte.eml"
      this.w_msgPart.SaveToFile(this.w_NOMFILPART)     
    endif
    * --- =======================================
    *     ===   RESET VARIABILI PER SICUREZZA       ====
    *     =======================================
    this.w_nParts = -1
    this.w_msgPart = .Null.
    this.w_msgToProcess = .Null.
    this.w_NICODPRA = " "
    this.w_INSERISCI = .t.
    if cp_fileexist(this.w_NOMFILPART)
      * --- [8]
      this.w_msgToProcess = this.w_zPop.LoadMessageFromPath(this.w_NOMFILPART)
      this.w_OBJ_Notifica = this.w_msgToProcess.ToMailMessage()
      this.w_OBJ_From = this.w_OBJ_Notifica.From
      * --- Determino oggetto effettivo
      this.w_sFrom = this.w_OBJ_From.Address
      this.w_sSubject = this.w_OBJ_Notifica.Subject
      this.w_OBJ_Notifica = .Null.
      this.w_OBJ_From = .Null.
      if (Alltrim(this.w_UTFILOGG) $ Alltrim(this.w_sSubject) OR Empty(this.w_UTFILOGG)) and (this.w_sDateSent>=this.w_UTDATINV OR Empty(this.w_UTDATINV)) and (Alltrim(this.w_UTFILMIT) $ Alltrim(this.w_sFrom) OR Empty(this.w_UTFILMIT))
        this.w_TOTMSG = this.w_TOTMSG + 1
        * --- [9]
        this.w_nParts = this.w_msgToProcess.MediaTypeNumber()
        this.w_PATHTEMP = ADDBS(this.w_PATHFILE) +"TEMP"
        if !Directory(this.w_PATHTEMP)
          * --- La creo
           MD (JUSTPATH(ADDBS(this.w_PATHTEMP)))
        endif
        if this.w_nParts > 0
          * --- [10]
          this.w_msgPart = this.w_msgToProcess.GetMessagePartWithMediaType(1)
          this.w_msgPart.SaveToFile(ADDBS(this.w_PATHFILE) + "Messaggi\"+Alltrim(this.w_CODUID)+"_ParteAllega.eml")     
          this.w_msgPart = .Null.
          this.w_msgToProcess = .Null.
          * --- [8]
          this.w_msgToProcess = this.w_zPop.LoadMessageFromPath(ADDBS(this.w_PATHFILE) + "Messaggi\"+Alltrim(this.w_CODUID)+"_ParteAllega.eml")
          * --- [6]
          this.w_msgToProcess.SaveAttachment(2,Alltrim(this.w_PATHTEMP))     
        endif
        this.w_msgToProcess.SaveAttachment(1,Alltrim(this.w_PATHTEMP))     
        ADIR(aFile,Addbs( Alltrim(this.w_PATHTEMP))+"*.*","")
        this.w_NICODPRA = " "
        if TYPE("afile")="C"
          this.w_NUMARR = 0
          this.w_TROVATA = .f.
          this.w_LENARR = Alen(afile,1)
          do while this.w_NUMARR <this.w_LENARR
            this.w_NUMARR = this.w_LENARR
            this.w_NICODPRA = " "
            this.w_NISERIAL = " "
            this.w_NIPRADES = " "
            this.w_NOMFILE = Alltrim(afile(this.w_NUMARR,1))
            this.w_OGGETTO = this.w_sSubject 
            this.Pag5()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_FILESITO = Addbs(Alltrim(this.w_PATHTEMP))+afile(this.w_NUMARR,1)
            if Not Empty(this.w_NICODPRA) and cp_fileexist(this.w_FILESITO)
              * --- Copio file nella directory ufficiale
              SetFileTimes(this.w_FILESITO,this.w_sDateSent)
              this.w_TOTNOT = this.w_TOTNOT + 1
              this.w_TROVATA = .t.
              this.w_STRINGA = "Messaggio "+ Alltrim(this.w_sSubject) + " con uid " + Alltrim(this.w_CODUID) +" esito: trovata pratica collegata al file " + Alltrim(this.w_NOMFILE)
              this.Pag3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            if this.w_TROVATA
              this.w_INSERISCI = .t.
              this.w_FILE_BBA = this.w_NOMFIL
              this.Pag4()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              exit
            endif
          enddo
          Release Afile
        else
          this.w_STRINGA =  "Messaggio "+ Alltrim(this.w_sSubject) + " con uid " + Alltrim(this.w_CODUID) +" scartato: nessun allegato rilevato"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_INSERISCI = .t.
        endif
      else
        this.w_INSERISCI = .t.
        this.Pag6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    else
      this.w_INSERISCI = .t.
      if (Alltrim(this.w_UTFILOGG) $ Alltrim(this.w_sSubjectHead) OR Empty(this.w_UTFILOGG)) and (this.w_sDateSentHead>=this.w_UTDATINV OR Empty(this.w_UTDATINV)) and (Alltrim(this.w_UTFILMIT) $ Alltrim(this.w_sFromHead) OR Empty(this.w_UTFILMIT))
        this.w_TOTMSG = this.w_TOTMSG + 1
        if "ACCETTAZIONE" $ Upper(this.w_sSubjectHead)
          * --- L'accettazione non ha come allegato la pec inviata
          this.w_OGGETTO = this.w_sSubjectHead
          this.w_NICODPRA = " "
          this.w_NISERIAL = " "
          this.w_NIPRADES = " "
          this.Pag5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_FILE_BBA = this.w_NOMFIL
          this.w_FILESITO = this.w_FILE_BBA
          if Not Empty(this.w_NICODPRA) and cp_fileexist(this.w_FILESITO)
            * --- Copio file nella directory ufficiale
            SetFileTimes(this.w_FILESITO,this.w_sDateSent)
            this.w_TOTNOT = this.w_TOTNOT + 1
            this.w_TROVATA = .t.
            this.w_STRINGA = "Messaggio "+ Alltrim(this.w_sSubject) + " con uid " + Alltrim(this.w_CODUID) +" esito: trovata pratica collegata al file " + Alltrim(this.w_NOMFILE)
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.Pag4()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        else
          this.w_STRINGA =  "Messaggio "+ Alltrim(this.w_sSubject) + " con uid " + Alltrim(this.w_CODUID) +" scartato: allegato incongruente"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      else
        this.Pag6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    Release Afile
    * --- =======================================
    *     ===   RESET VARIABILI PER SICUREZZA       ====
    *     =======================================
    if !btrserr and Not Empty(this.w_CODUID) AND this.w_INSERISCI
      * --- Try
      local bErr_041FA800
      bErr_041FA800=bTrsErr
      this.Try_041FA800()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- Write into ANA_CUID
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ANA_CUID_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ANA_CUID_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ANA_CUID_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"AUOGGETT ="+cp_NullLink(cp_ToStrODBC(this.w_sSubjectHead),'ANA_CUID','AUOGGETT');
          +",AUDATINV ="+cp_NullLink(cp_ToStrODBC(this.w_sDateSentHead),'ANA_CUID','AUDATINV');
          +",AU__FROM ="+cp_NullLink(cp_ToStrODBC(this.w_sFromHead),'ANA_CUID','AU__FROM');
          +",AUOGGET2 ="+cp_NullLink(cp_ToStrODBC(this.w_sSubject),'ANA_CUID','AUOGGET2');
          +",AU__FRO2 ="+cp_NullLink(cp_ToStrODBC(this.w_sFrom),'ANA_CUID','AU__FRO2');
          +",AUNOMFIL ="+cp_NullLink(cp_ToStrODBC(this.w_AUNOMFIL),'ANA_CUID','AUNOMFIL');
          +",AUDATIN2 ="+cp_NullLink(cp_ToStrODBC(this.w_sDateSent),'ANA_CUID','AUDATIN2');
          +",AUFILEML ="+cp_NullLink(cp_ToStrODBC(LnomFil),'ANA_CUID','AUFILEML');
          +",AUMOTIVO ="+cp_NullLink(cp_ToStrODBC(this.w_STRINGA),'ANA_CUID','AUMOTIVO');
          +",AUSERBUS ="+cp_NullLink(cp_ToStrODBC(this.w_NISERIAL),'ANA_CUID','AUSERBUS');
              +i_ccchkf ;
          +" where ";
              +"AUCODUID = "+cp_ToStrODBC(this.w_CODUID);
                 )
        else
          update (i_cTable) set;
              AUOGGETT = this.w_sSubjectHead;
              ,AUDATINV = this.w_sDateSentHead;
              ,AU__FROM = this.w_sFromHead;
              ,AUOGGET2 = this.w_sSubject;
              ,AU__FRO2 = this.w_sFrom;
              ,AUNOMFIL = this.w_AUNOMFIL;
              ,AUDATIN2 = this.w_sDateSent;
              ,AUFILEML = LnomFil;
              ,AUMOTIVO = this.w_STRINGA;
              ,AUSERBUS = this.w_NISERIAL;
              &i_ccchkf. ;
           where;
              AUCODUID = this.w_CODUID;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_041FA800
      * --- End
    endif
    this.w_CODUID = " "
    this.w_nParts = -1
    this.w_msgPart = .Null.
    this.w_msgToProcess = .Null.
    this.w_attachmentsFilesName = " "
    this.w_nTotFileAttach = -1
    this.w_msgHeader = .Null.
    this.w_msgDim = -1
  endproc
  proc Try_041FA800()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into ANA_CUID
    i_nConn=i_TableProp[this.ANA_CUID_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ANA_CUID_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ANA_CUID_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"AUCODUID"+",AUOGGETT"+",AUDATINV"+",AU__FROM"+",AUOGGET2"+",AU__FRO2"+",AUNOMFIL"+",AUDATIN2"+",AUFILEML"+",AUMOTIVO"+",AUSERBUS"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODUID),'ANA_CUID','AUCODUID');
      +","+cp_NullLink(cp_ToStrODBC(this.w_sSubjectHead),'ANA_CUID','AUOGGETT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_sDateSentHead),'ANA_CUID','AUDATINV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_sFromHead),'ANA_CUID','AU__FROM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_sSubject),'ANA_CUID','AUOGGET2');
      +","+cp_NullLink(cp_ToStrODBC(this.w_sFrom),'ANA_CUID','AU__FRO2');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AUNOMFIL),'ANA_CUID','AUNOMFIL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_sDateSent),'ANA_CUID','AUDATIN2');
      +","+cp_NullLink(cp_ToStrODBC(LnomFil),'ANA_CUID','AUFILEML');
      +","+cp_NullLink(cp_ToStrODBC(this.w_STRINGA),'ANA_CUID','AUMOTIVO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NISERIAL),'ANA_CUID','AUSERBUS');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'AUCODUID',this.w_CODUID,'AUOGGETT',this.w_sSubjectHead,'AUDATINV',this.w_sDateSentHead,'AU__FROM',this.w_sFromHead,'AUOGGET2',this.w_sSubject,'AU__FRO2',this.w_sFrom,'AUNOMFIL',this.w_AUNOMFIL,'AUDATIN2',this.w_sDateSent,'AUFILEML',LnomFil,'AUMOTIVO',this.w_STRINGA,'AUSERBUS',this.w_NISERIAL)
      insert into (i_cTable) (AUCODUID,AUOGGETT,AUDATINV,AU__FROM,AUOGGET2,AU__FRO2,AUNOMFIL,AUDATIN2,AUFILEML,AUMOTIVO,AUSERBUS &i_ccchkf. );
         values (;
           this.w_CODUID;
           ,this.w_sSubjectHead;
           ,this.w_sDateSentHead;
           ,this.w_sFromHead;
           ,this.w_sSubject;
           ,this.w_sFrom;
           ,this.w_AUNOMFIL;
           ,this.w_sDateSent;
           ,LnomFil;
           ,this.w_STRINGA;
           ,this.w_NISERIAL;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
    FWRITE(this.hLOG,CHR(13)+CHR(10),2)
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_ESITO = 0
    this.w_ESITO = Val(gsut_BIE(Null,this.w_FILESITO,"EsitoAtto/DatiEsito/MsgEsito/CodiceEsito"))
    do case
      case "ESITO" $ Upper(this.w_sSubjectHead) or (this.w_ESITO <> 0 and "ACCETTAZIONE" $ Upper(this.w_sSubjectHead))
        * --- Caso esito cancelliere arriva pc di accettazione con esito
        this.w_IDTIPATT = ICASE(this.w_ESITO=1,"P",this.w_ESITO=2,"D","N")
        if this.w_ESITO<0
          this.w_DESESITO = gsut_BIE(Null,this.w_FILESITO,"EsitoAtto/DatiEsito/MsgEsito/DescrizioneEsito")
          this.w_DESESITO = STRTRAN(this.w_DESESITO,"&plus&","+")
          this.w_DESESITO = STRTRAN(this.w_DESESITO,"&apos;","'")
          this.w_DESESITO = STRTRAN(this.w_DESESITO,"&quot",'"')
          this.w_DESESITO = STRTRAN(this.w_DESESITO,"&amp;","&")
          this.w_DESESITO = STRTRAN(this.w_DESESITO,"&lt; ","<")
          this.w_DESESITO = STRTRAN(this.w_DESESITO,"&gt;",">")
          if "ACCETTAZIONE" $ Upper(this.w_sSubjectHead)
            * --- Scartata dal cancelliere
            this.w_IDTIPATT = "S"
          endif
        endif
      case "ACCETTAZIONE" $ Upper(this.w_sSubjectHead)
        this.w_IDTIPATT = "A"
      case "CONSEGNA" $ Upper(this.w_sSubjectHead)
        this.w_IDTIPATT = "C"
    endcase
    Private L_ArrParam
    dimension L_ArrParam(30)
    L_ArrParam(1)="AR"
    L_ArrParam(2)=this.w_FILE_BBA
    L_ArrParam(3)=this.w_LICLADOC
    L_ArrParam(4)=i_CODUTE
    L_ArrParam(5)=This
    L_ArrParam(6)=this.w_AR_TABLE
    L_ArrParam(7)=this.w_NICODPRA
    L_ArrParam(8)=ALLTRIM(this.w_sSubjectHead)
    L_ArrParam(9)=" "
    L_ArrParam(10)=.T.
    L_ArrParam(11)=.T.
    L_ArrParam(12)=" "
    L_ArrParam(13)=" "
    L_ArrParam(14)=this
    L_ArrParam(15)=w_TIPALLE
    L_ArrParam(16)=this.w_CLAALLE
    L_ArrParam(17)=" "
    L_ArrParam(18)=" "
    L_ArrParam(19)="C"
    L_ArrParam(21)=.F.
    L_ArrParam(22)=this.w_PATHSTD
    L_ArrParam(25)=this.w_IDTIPBST
    L_ArrParam(26)=this.w_DATAARCH
    L_ArrParam(27)=this.w_ORAARCH
    L_SerIndex=" "
    GSUT_BBA(this,@L_ArrParam, @L_SerIndex)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if Not Empty(L_SERINDEX)
      this.w_IDSERIAL = Left(L_SerIndex,20)
      * --- C Consegnata
      *     A Accettata
      *     P Esito positivo
      *     N Esito negativo
      * --- Try
      local bErr_041DE708
      bErr_041DE708=bTrsErr
      this.Try_041DE708()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
      endif
      bTrsErr=bTrsErr or bErr_041DE708
      * --- End
    endif
  endproc
  proc Try_041DE708()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Write into PROMINDI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PROMINDI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"IDTIPATT ="+cp_NullLink(cp_ToStrODBC(this.w_IDTIPATT),'PROMINDI','IDTIPATT');
      +",ID_TESTO ="+cp_NullLink(cp_ToStrODBC(this.w_DESESITO),'PROMINDI','ID_TESTO');
          +i_ccchkf ;
      +" where ";
          +"IDSERIAL = "+cp_ToStrODBC(this.w_IDSERIAL);
             )
    else
      update (i_cTable) set;
          IDTIPATT = this.w_IDTIPATT;
          ,ID_TESTO = this.w_DESESITO;
          &i_ccchkf. ;
       where;
          IDSERIAL = this.w_IDSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Select from gsut0blp
    do vq_exec with 'gsut0blp',this,'_Curs_gsut0blp','',.f.,.t.
    if used('_Curs_gsut0blp')
      select _Curs_gsut0blp
      locate for 1=1
      do while not(eof())
      this.w_NICODPRA = _Curs_GSUT0BLP.NICODPRA
      this.w_NISERIAL = _Curs_GSUT0BLP.NISERIAL
      this.w_NIPRADES = _Curs_GSUT0BLP.NIPRADES
        select _Curs_gsut0blp
        continue
      enddo
      use
    endif
  endproc


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if Not EMpty(this.w_UTFILOGG)
      this.w_STRINGA =  "Messaggio "+ Alltrim(this.w_sSubject) + " con uid " + Alltrim(this.w_CODUID) +" scartato: filtri impostati "
      this.w_STRINGA = Alltrim(this.w_STRINGA) + " oggetto filtri " + Alltrim(this.w_UTFILOGG) + " oggetto msg " + alltrim(this.w_sSubject)
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if Not EMpty(this.w_UTFILMIT)
      this.w_STRINGA =  "Messaggio "+ Alltrim(this.w_sSubject) + " con uid " + Alltrim(this.w_CODUID) +" scartato: filtri impostati "
      this.w_STRINGA = Alltrim(this.w_STRINGA) + " mittente filtri " + Alltrim(this.w_UTFILMIT) + "mittente msg " + alltrim(this.w_sFrom)
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if Not EMpty(this.w_UTDATINV)
      this.w_STRINGA =  "Messaggio "+ Alltrim(this.w_sSubject) + " con uid " + Alltrim(this.w_CODUID) +" scartato: filtri impostati "
      this.w_STRINGA = Alltrim(this.w_STRINGA) + " data filtri " + Alltrim(ttoc(this.w_UTDATINV)) + " data msg " + Alltrim(ttoc(this.w_sDateSent))
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,w_CODUTE,w_Protcript,w_UFILOGG,w_UDATINV,w_UFILMIT)
    this.w_CODUTE=w_CODUTE
    this.w_Protcript=w_Protcript
    this.w_UFILOGG=w_UFILOGG
    this.w_UDATINV=w_UDATINV
    this.w_UFILMIT=w_UFILMIT
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,8)]
    this.cWorkTables[1]='ANA_CUID'
    this.cWorkTables[2]='CONTROPA'
    this.cWorkTables[3]='*TMP_FILE'
    this.cWorkTables[4]='AZIENDA'
    this.cWorkTables[5]='UTEMAILS'
    this.cWorkTables[6]='PROMCLAS'
    this.cWorkTables[7]='PROMINDI'
    this.cWorkTables[8]='AUT_LPE'
    return(this.OpenAllTables(8))

  proc CloseCursors()
    if used('_Curs_gsut0blp')
      use in _Curs_gsut0blp
    endif
    if used('_Curs_UTEMAILS')
      use in _Curs_UTEMAILS
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_CODUTE,w_Protcript,w_UFILOGG,w_UDATINV,w_UFILMIT"
endproc
