* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_adi                                                        *
*              Dichiarazioni di intento                                        *
*                                                                              *
*      Author: TAM SOFTWARE & CODELAB                                          *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-04-24                                                      *
* Last revis.: 2017-11-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_adi"))

* --- Class definition
define class tgscg_adi as StdForm
  Top    = 13
  Left   = 25

  * --- Standard Properties
  Width  = 774
  Height = 542+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2017-11-10"
  HelpContextID=92939625
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=83

  * --- Constant Properties
  DIC_INTE_IDX = 0
  CONTI_IDX = 0
  VOCIIVA_IDX = 0
  AZIENDA_IDX = 0
  VALUTE_IDX = 0
  TIPCODIV_IDX = 0
  GEDICDEM_IDX = 0
  cFile = "DIC_INTE"
  cKeySelect = "DISERIAL"
  cKeyWhere  = "DISERIAL=this.w_DISERIAL"
  cKeyWhereODBC = '"DISERIAL="+cp_ToStrODBC(this.w_DISERIAL)';

  cKeyWhereODBCqualified = '"DIC_INTE.DISERIAL="+cp_ToStrODBC(this.w_DISERIAL)';

  cPrg = "gscg_adi"
  cComment = "Dichiarazioni di intento"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_AZI = space(5)
  w_UFFICIO = space(10)
  w_DISERIAL = space(15)
  o_DISERIAL = space(15)
  w_DITIPCON = space(1)
  o_DITIPCON = space(1)
  w_DITIPIVA = space(1)
  o_DITIPIVA = space(1)
  w_DINUMDIC = 0
  w_DIALFDIC = space(2)
  w_DISERDIC = space(3)
  w_DI__ANNO = space(4)
  w_DIDATDIC = ctod('  /  /  ')
  o_DIDATDIC = ctod('  /  /  ')
  w_DIALFDOC = space(2)
  o_DIALFDOC = space(2)
  w_DINUMDOC = 0
  w_DISERDOC = space(3)
  w_DIDATLET = ctod('  /  /  ')
  w_VALESE = space(3)
  w_DICODICE = space(15)
  o_DICODICE = space(15)
  w_DESCON = space(40)
  w_DIDOGANA = space(30)
  w_DIUFFIVA = space(25)
  w_DIDATAPL = ctod('  /  /  ')
  w_AGGPRODOC = .F.
  w_DIUFIVFO = space(25)
  w_DIPROTEC = space(17)
  o_DIPROTEC = space(17)
  w_DIPRODOC = 0
  w_DICODIVA = space(5)
  w_DIARTESE = space(5)
  w_DIDESOPE = space(0)
  w_DESIVA = space(35)
  w_DITIPOPE = space(1)
  o_DITIPOPE = space(1)
  w_DITIPOPE = space(1)
  w_DIIMPDIC = 0
  o_DIIMPDIC = 0
  w_DIIMPUTI = 0
  o_DIIMPUTI = 0
  w_DIDATINI = ctod('  /  /  ')
  w_DIDATFIN = ctod('  /  /  ')
  w_DIDATREV = ctod('  /  /  ')
  w_ODATDIC = ctod('  /  /  ')
  w_PERIVA = 0
  w_DIFLSTAM = space(1)
  o_DIFLSTAM = space(1)
  w_SERIALE = space(10)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_DOCUNUME = 0
  w_HASEVCOP = space(20)
  w_HASEVENT = .F.
  w_DINUMPRO = space(3)
  w_DIAUTON1 = space(7)
  w_DIAUTON2 = space(7)
  w_DIDATINI = ctod('  /  /  ')
  w_DIDATFIN = ctod('  /  /  ')
  w_DIFLGDOG = space(1)
  w_ANFLSOAL = space(1)
  o_ANFLSOAL = space(1)
  w_EMAIL = space(254)
  w_EMPEC = space(254)
  w_TELFAX = space(18)
  w_NUMDIC = 0
  w_SERDIC = space(3)
  w_ANNO = space(4)
  w_DATDIC = ctod('  /  /  ')
  w_NUMDOC = 0
  w_SERDOC = space(3)
  w_DATLET = ctod('  /  /  ')
  w_ELABORA_ZOOM = space(10)
  w_DDDOCSER = space(10)
  w_DIDICCOL = space(15)
  o_DIDICCOL = space(15)
  w_NUMDIC_COLL = 0
  w_ANNDIC_COLL = space(4)
  w_DATDIC_COLL = ctod('  /  /  ')
  w_SERDIC_COLL = space(3)
  w_DIC_COLL_UTI = .F.
  w_DICSN = space(10)
  o_DICSN = space(10)
  w_IMPDIC = 0
  w_IMPUTI = 0
  w_DIIMPRES = 0
  w_IMPRES = 0
  w_DIKEYINT = space(10)
  w_DIFLGCLO = space(1)
  o_DIFLGCLO = space(1)
  w_NUMDOC_COLL = 0
  w_SERDOC_COLL = space(3)
  w_ZOOM_DETT = .F.
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_DISERIAL = this.W_DISERIAL
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_DI__ANNO = this.W_DI__ANNO
  op_DITIPCON = this.W_DITIPCON
  op_DISERDIC = this.W_DISERDIC
  op_DINUMDIC = this.W_DINUMDIC
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_DIAUTON2 = this.W_DIAUTON2
  op_DITIPCON = this.W_DITIPCON
  op_DITIPIVA = this.W_DITIPIVA
  op_DINUMDOC = this.W_DINUMDOC

  * --- Children pointers
  GSCG_MDU = .NULL.
  GSCG_MDF = .NULL.
  w_Elabora_Dic_Inte = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gscg_adi
  w_RESCHK=0
  
  * --- Gestiti i soli eventi Modifica, cancellazione, inserimento
  func ah_HasCPEvents(i_cOp)
  
    if this.cFunction='Query'
  	  	If Upper(i_cop)='ECPEDIT' Or Upper(i_cop)='ECPDELETE'
  	  		* esegue controllo se movimento bloccato se da problemi si pu� chiamare direttamente il Batch
  	  		This.w_HASEVCOP=i_cop
  	  		This.NotifyEvent('HasEvent')
  	  		Return ( This.w_HASEVENT )
  	  	Else
  	  		Return(.t.)
  	  	endif
  	Else
  	  	* --- Se l'evento non � modifica/cancellazione/inserimento prosegue
  	  	Return(.t.)
    Endif
  EndFunc
  
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'DIC_INTE','gscg_adi')
    stdPageFrame::Init()
    *set procedure to GSCG_MDU additive
    *set procedure to GSCG_MDF additive
    with this
      .Pages(1).addobject("oPag","tgscg_adiPag1","gscg_adi",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dichiarazioni di intento")
      .Pages(1).HelpContextID = 29022961
      .Pages(2).addobject("oPag","tgscg_adiPag2","gscg_adi",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Dettaglio importo utilizzato")
      .Pages(2).HelpContextID = 235447283
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSCG_MDU
    *release procedure GSCG_MDF
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
    proc Init()
      this.w_Elabora_Dic_Inte = this.oPgFrm.Pages(2).oPag.Elabora_Dic_Inte
      DoDefault()
    proc Destroy()
      this.w_Elabora_Dic_Inte = .NULL.
      DoDefault()

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='VOCIIVA'
    this.cWorkTables[3]='AZIENDA'
    this.cWorkTables[4]='VALUTE'
    this.cWorkTables[5]='TIPCODIV'
    this.cWorkTables[6]='GEDICDEM'
    this.cWorkTables[7]='DIC_INTE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(7))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DIC_INTE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DIC_INTE_IDX,3]
  return

  function CreateChildren()
    this.GSCG_MDU = CREATEOBJECT('stdDynamicChild',this,'GSCG_MDU',this.oPgFrm.Page1.oPag.oLinkPC_1_101)
    this.GSCG_MDU.createrealchild()
    this.GSCG_MDF = CREATEOBJECT('stdDynamicChild',this,'GSCG_MDF',this.oPgFrm.Page1.oPag.oLinkPC_1_117)
    this.GSCG_MDF.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSCG_MDU)
      this.GSCG_MDU.DestroyChildrenChain()
      this.GSCG_MDU=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_101')
    if !ISNULL(this.GSCG_MDF)
      this.GSCG_MDF.DestroyChildrenChain()
      this.GSCG_MDF=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_117')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSCG_MDU.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCG_MDF.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSCG_MDU.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCG_MDF.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSCG_MDU.NewDocument()
    this.GSCG_MDF.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSCG_MDU.SetKey(;
            .w_DISERIAL,"DDSERIAL";
            )
      this.GSCG_MDF.SetKey(;
            .w_DISERIAL,"DFSERIAL";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSCG_MDU.ChangeRow(this.cRowID+'      1',1;
             ,.w_DISERIAL,"DDSERIAL";
             )
      .GSCG_MDF.ChangeRow(this.cRowID+'      1',1;
             ,.w_DISERIAL,"DFSERIAL";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSCG_MDU)
        i_f=.GSCG_MDU.BuildFilter()
        if !(i_f==.GSCG_MDU.cQueryFilter)
          i_fnidx=.GSCG_MDU.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCG_MDU.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCG_MDU.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCG_MDU.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCG_MDU.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSCG_MDF)
        i_f=.GSCG_MDF.BuildFilter()
        if !(i_f==.GSCG_MDF.cQueryFilter)
          i_fnidx=.GSCG_MDF.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCG_MDF.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCG_MDF.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCG_MDF.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCG_MDF.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_DISERIAL = NVL(DISERIAL,space(15))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_16_joined
    link_1_16_joined=.f.
    local link_1_25_joined
    link_1_25_joined=.f.
    local link_1_93_joined
    link_1_93_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.Calculate_NBKBCTCOKW()
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from DIC_INTE where DISERIAL=KeySet.DISERIAL
    *
    i_nConn = i_TableProp[this.DIC_INTE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIC_INTE_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DIC_INTE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DIC_INTE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DIC_INTE '
      link_1_16_joined=this.AddJoinedLink_1_16(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_25_joined=this.AddJoinedLink_1_25(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_93_joined=this.AddJoinedLink_1_93(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DISERIAL',this.w_DISERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_AZI = i_codazi
        .w_UFFICIO = space(10)
        .w_DESCON = space(40)
        .w_AGGPRODOC = .t.
        .w_DESIVA = space(35)
        .w_SERIALE = space(10)
        .w_DATOBSO = ctod("  /  /  ")
        .w_DOCUNUME = 0
        .w_HASEVCOP = space(20)
        .w_HASEVENT = .f.
        .w_ANFLSOAL = space(1)
        .w_EMAIL = space(254)
        .w_EMPEC = space(254)
        .w_TELFAX = space(18)
        .w_ELABORA_ZOOM = space(10)
        .w_NUMDIC_COLL = 0
        .w_ANNDIC_COLL = space(4)
        .w_DATDIC_COLL = ctod("  /  /  ")
        .w_SERDIC_COLL = space(3)
        .w_DIC_COLL_UTI = .f.
        .w_DIIMPRES = (.w_DIIMPDIC-.w_DIIMPUTI)
        .w_DIKEYINT = Sys(2015)
        .w_NUMDOC_COLL = 0
        .w_SERDOC_COLL = space(3)
        .w_ZOOM_DETT = .f.
          .link_1_1('Load')
        .w_DISERIAL = NVL(DISERIAL,space(15))
        .op_DISERIAL = .w_DISERIAL
        .w_DITIPCON = NVL(DITIPCON,space(1))
        .op_DITIPCON = .w_DITIPCON
        .op_DITIPCON = .w_DITIPCON
        .w_DITIPIVA = NVL(DITIPIVA,space(1))
        .op_DITIPIVA = .w_DITIPIVA
        .w_DINUMDIC = NVL(DINUMDIC,0)
        .op_DINUMDIC = .w_DINUMDIC
        .w_DIALFDIC = NVL(DIALFDIC,space(2))
        .w_DISERDIC = NVL(DISERDIC,space(3))
        .op_DISERDIC = .w_DISERDIC
        .w_DI__ANNO = NVL(DI__ANNO,space(4))
        .op_DI__ANNO = .w_DI__ANNO
        .w_DIDATDIC = NVL(cp_ToDate(DIDATDIC),ctod("  /  /  "))
        .w_DIALFDOC = NVL(DIALFDOC,space(2))
        .w_DINUMDOC = NVL(DINUMDOC,0)
        .op_DINUMDOC = .w_DINUMDOC
        .w_DISERDOC = NVL(DISERDOC,space(3))
        .w_DIDATLET = NVL(cp_ToDate(DIDATLET),ctod("  /  /  "))
        .w_VALESE = CALVAP(.w_DIDATDIC)
          .link_1_15('Load')
        .w_DICODICE = NVL(DICODICE,space(15))
          if link_1_16_joined
            this.w_DICODICE = NVL(ANCODICE116,NVL(this.w_DICODICE,space(15)))
            this.w_DESCON = NVL(ANDESCRI116,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO116),ctod("  /  /  "))
            this.w_ANFLSOAL = NVL(ANFLSOAL116,space(1))
            this.w_EMAIL = NVL(AN_EMAIL116,space(254))
            this.w_EMPEC = NVL(AN_EMPEC116,space(254))
            this.w_TELFAX = NVL(ANTELFAX116,space(18))
          else
          .link_1_16('Load')
          endif
        .w_DIDOGANA = NVL(DIDOGANA,space(30))
        .w_DIUFFIVA = NVL(DIUFFIVA,space(25))
        .w_DIDATAPL = NVL(cp_ToDate(DIDATAPL),ctod("  /  /  "))
        .w_DIUFIVFO = NVL(DIUFIVFO,space(25))
        .w_DIPROTEC = NVL(DIPROTEC,space(17))
        .w_DIPRODOC = NVL(DIPRODOC,0)
        .w_DICODIVA = NVL(DICODIVA,space(5))
          if link_1_25_joined
            this.w_DICODIVA = NVL(IVCODIVA125,NVL(this.w_DICODIVA,space(5)))
            this.w_DESIVA = NVL(IVDESIVA125,space(35))
            this.w_PERIVA = NVL(IVPERIVA125,0)
            this.w_DATOBSO = NVL(cp_ToDate(IVDTOBSO125),ctod("  /  /  "))
          else
          .link_1_25('Load')
          endif
        .w_DIARTESE = NVL(DIARTESE,space(5))
        .w_DIDESOPE = NVL(DIDESOPE,space(0))
        .w_DITIPOPE = NVL(DITIPOPE,space(1))
        .w_DITIPOPE = NVL(DITIPOPE,space(1))
        .w_DIIMPDIC = NVL(DIIMPDIC,0)
        .w_DIIMPUTI = NVL(DIIMPUTI,0)
        .w_DIDATINI = NVL(cp_ToDate(DIDATINI),ctod("  /  /  "))
        .w_DIDATFIN = NVL(cp_ToDate(DIDATFIN),ctod("  /  /  "))
        .w_DIDATREV = NVL(cp_ToDate(DIDATREV),ctod("  /  /  "))
        .oPgFrm.Page1.oPag.oObj_1_48.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate()
        .w_ODATDIC = .w_DIDATDIC
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate(AH_Msgformat(IIF(.w_DITIPCON="C", "Cliente:",IIF(.w_DITIPCON="F","Fornitore:","")) ),'','')
        .w_PERIVA = iif(.w_DITIPIVA='S',0,100)
        .w_DIFLSTAM = NVL(DIFLSTAM,space(1))
        .w_OBTEST = .w_DIDATDIC
        .oPgFrm.Page1.oPag.oObj_1_67.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_70.Calculate()
        .w_DINUMPRO = NVL(DINUMPRO,space(3))
        .w_DIAUTON1 = NVL(DIAUTON1,space(7))
        .w_DIAUTON2 = NVL(DIAUTON2,space(7))
        .op_DIAUTON2 = .w_DIAUTON2
        .w_DIDATINI = NVL(cp_ToDate(DIDATINI),ctod("  /  /  "))
        .w_DIDATFIN = NVL(cp_ToDate(DIDATFIN),ctod("  /  /  "))
        .oPgFrm.Page1.oPag.oObj_1_84.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_86.Calculate()
        .w_DIFLGDOG = NVL(DIFLGDOG,space(1))
        .w_NUMDIC = .w_DINUMDIC
        .w_SERDIC = .w_DISERDIC
        .w_ANNO = .w_DI__ANNO
        .w_DATDIC = .w_DIDATDIC
        .w_NUMDOC = .w_DINUMDOC
        .w_SERDOC = .w_DISERDOC
        .w_DATLET = .w_DIDATLET
        .oPgFrm.Page2.oPag.Elabora_Dic_Inte.Calculate()
        .w_DDDOCSER = .w_Elabora_Dic_Inte.getVar('DDDOCSER')
        .w_DIDICCOL = NVL(DIDICCOL,space(15))
          if link_1_93_joined
            this.w_DIDICCOL = NVL(DISERIAL193,NVL(this.w_DIDICCOL,space(15)))
            this.w_NUMDOC_COLL = NVL(DINUMDOC193,0)
            this.w_SERDOC_COLL = NVL(DISERDOC193,space(3))
            this.w_ANNDIC_COLL = NVL(DI__ANNO193,space(4))
            this.w_DATDIC_COLL = NVL(cp_ToDate(DIDATLET193),ctod("  /  /  "))
            this.w_NUMDIC_COLL = NVL(DINUMDIC193,0)
            this.w_SERDIC_COLL = NVL(DISERDIC193,space(3))
          else
          .link_1_93('Load')
          endif
        .w_DICSN = IIF(Not Empty(.w_DIDICCOL),'S','N')
        .w_IMPDIC = .w_DIIMPDIC
        .w_IMPUTI = .w_DIIMPUTI
        .w_IMPRES = .w_DIIMPRES
        .w_DIFLGCLO = NVL(DIFLGCLO,space(1))
        .oPgFrm.Page2.oPag.oObj_2_29.Calculate()
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'DIC_INTE')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_58.enabled = this.oPgFrm.Page1.oPag.oBtn_1_58.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_92.enabled = this.oPgFrm.Page1.oPag.oBtn_1_92.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_105.enabled = this.oPgFrm.Page1.oPag.oBtn_1_105.mCond()
      this.oPgFrm.Page2.oPag.oBtn_2_31.enabled = this.oPgFrm.Page2.oPag.oBtn_2_31.mCond()
      this.oPgFrm.Page2.oPag.oBtn_2_32.enabled = this.oPgFrm.Page2.oPag.oBtn_2_32.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_118.enabled = this.oPgFrm.Page1.oPag.oBtn_1_118.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
    this.Calculate_RMHQSCTZMR()
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.Calculate_NBKBCTCOKW()
    this.ChildrenNewDocument()
    with this
      .w_AZI = space(5)
      .w_UFFICIO = space(10)
      .w_DISERIAL = space(15)
      .w_DITIPCON = space(1)
      .w_DITIPIVA = space(1)
      .w_DINUMDIC = 0
      .w_DIALFDIC = space(2)
      .w_DISERDIC = space(3)
      .w_DI__ANNO = space(4)
      .w_DIDATDIC = ctod("  /  /  ")
      .w_DIALFDOC = space(2)
      .w_DINUMDOC = 0
      .w_DISERDOC = space(3)
      .w_DIDATLET = ctod("  /  /  ")
      .w_VALESE = space(3)
      .w_DICODICE = space(15)
      .w_DESCON = space(40)
      .w_DIDOGANA = space(30)
      .w_DIUFFIVA = space(25)
      .w_DIDATAPL = ctod("  /  /  ")
      .w_AGGPRODOC = .f.
      .w_DIUFIVFO = space(25)
      .w_DIPROTEC = space(17)
      .w_DIPRODOC = 0
      .w_DICODIVA = space(5)
      .w_DIARTESE = space(5)
      .w_DIDESOPE = space(0)
      .w_DESIVA = space(35)
      .w_DITIPOPE = space(1)
      .w_DITIPOPE = space(1)
      .w_DIIMPDIC = 0
      .w_DIIMPUTI = 0
      .w_DIDATINI = ctod("  /  /  ")
      .w_DIDATFIN = ctod("  /  /  ")
      .w_DIDATREV = ctod("  /  /  ")
      .w_ODATDIC = ctod("  /  /  ")
      .w_PERIVA = 0
      .w_DIFLSTAM = space(1)
      .w_SERIALE = space(10)
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_DOCUNUME = 0
      .w_HASEVCOP = space(20)
      .w_HASEVENT = .f.
      .w_DINUMPRO = space(3)
      .w_DIAUTON1 = space(7)
      .w_DIAUTON2 = space(7)
      .w_DIDATINI = ctod("  /  /  ")
      .w_DIDATFIN = ctod("  /  /  ")
      .w_DIFLGDOG = space(1)
      .w_ANFLSOAL = space(1)
      .w_EMAIL = space(254)
      .w_EMPEC = space(254)
      .w_TELFAX = space(18)
      .w_NUMDIC = 0
      .w_SERDIC = space(3)
      .w_ANNO = space(4)
      .w_DATDIC = ctod("  /  /  ")
      .w_NUMDOC = 0
      .w_SERDOC = space(3)
      .w_DATLET = ctod("  /  /  ")
      .w_ELABORA_ZOOM = space(10)
      .w_DDDOCSER = space(10)
      .w_DIDICCOL = space(15)
      .w_NUMDIC_COLL = 0
      .w_ANNDIC_COLL = space(4)
      .w_DATDIC_COLL = ctod("  /  /  ")
      .w_SERDIC_COLL = space(3)
      .w_DIC_COLL_UTI = .f.
      .w_DICSN = space(10)
      .w_IMPDIC = 0
      .w_IMPUTI = 0
      .w_DIIMPRES = 0
      .w_IMPRES = 0
      .w_DIKEYINT = space(10)
      .w_DIFLGCLO = space(1)
      .w_NUMDOC_COLL = 0
      .w_SERDOC_COLL = space(3)
      .w_ZOOM_DETT = .f.
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      if .cFunction<>"Filter"
        .w_AZI = i_codazi
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_AZI))
          .link_1_1('Full')
          endif
          .DoRTCalc(2,3,.f.)
        .w_DITIPCON = 'F'
        .w_DITIPIVA = 'S'
          .DoRTCalc(6,8,.f.)
        .w_DI__ANNO = STR(YEAR(i_DATSYS),4,0)
        .w_DIDATDIC = i_datsys
          .DoRTCalc(11,11,.f.)
        .w_DINUMDOC = IIF(.w_DITIPCON='F',.w_DINUMDOC,0)
          .DoRTCalc(13,13,.f.)
        .w_DIDATLET = IIF(.w_DITIPCON='F',.w_DIDATDIC,cp_CharToDate('  -  -  '))
        .w_VALESE = CALVAP(.w_DIDATDIC)
        .DoRTCalc(15,15,.f.)
          if not(empty(.w_VALESE))
          .link_1_15('Full')
          endif
        .DoRTCalc(16,16,.f.)
          if not(empty(.w_DICODICE))
          .link_1_16('Full')
          endif
          .DoRTCalc(17,17,.f.)
        .w_DIDOGANA = IIF(.w_DITIPCON='C' OR NOT EMPTY(.w_DICODICE),'',.w_DIDOGANA)
        .w_DIUFFIVA = iif(.w_DITIPCON='F',.w_UFFICIO,'')
        .w_DIDATAPL = Cp_CharToDate('  -  -    ')
        .w_AGGPRODOC = .t.
          .DoRTCalc(22,22,.f.)
        .w_DIPROTEC = Space(17)
        .w_DIPRODOC = IIF(.w_AGGPRODOC,0,.w_DIPRODOC)
        .DoRTCalc(25,25,.f.)
          if not(empty(.w_DICODIVA))
          .link_1_25('Full')
          endif
        .w_DIARTESE = '8'+space(4)
          .DoRTCalc(27,28,.f.)
        .w_DITIPOPE = 'O'
        .w_DITIPOPE = 'O'
        .w_DIIMPDIC = iif(.w_DITIPOPE<>'I',0,.w_DIIMPDIC)
        .w_DIIMPUTI = iif(.w_DITIPOPE<>'I',0,.w_DIIMPUTI)
        .w_DIDATINI = cp_CharToDate("01-01-"+.w_DI__ANNO)
        .w_DIDATFIN = cp_CharToDate("31-12-"+.w_DI__ANNO)
        .oPgFrm.Page1.oPag.oObj_1_48.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate()
          .DoRTCalc(35,35,.f.)
        .w_ODATDIC = .w_DIDATDIC
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate(AH_Msgformat(IIF(.w_DITIPCON="C", "Cliente:",IIF(.w_DITIPCON="F","Fornitore:","")) ),'','')
        .w_PERIVA = iif(.w_DITIPIVA='S',0,100)
        .w_DIFLSTAM = 'N'
          .DoRTCalc(39,39,.f.)
        .w_OBTEST = .w_DIDATDIC
        .oPgFrm.Page1.oPag.oObj_1_67.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_70.Calculate()
          .DoRTCalc(41,44,.f.)
        .w_DINUMPRO = .w_DITIPIVA+.w_DIALFDOC
          .DoRTCalc(46,46,.f.)
        .w_DIAUTON2 = ALLTRIM(.w_DI__ANNO)+ALLTRIM(.w_DISERDOC)
        .w_DIDATINI = cp_CharToDate("01-01-"+.w_DI__ANNO)
        .w_DIDATFIN = cp_CharToDate("31-12-"+.w_DI__ANNO)
        .oPgFrm.Page1.oPag.oObj_1_84.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_86.Calculate()
        .w_DIFLGDOG = IIF(.w_DITIPCON='F' And Not Empty(.w_DICODICE) And .w_DIFLSTAM<>'R' AND .w_ANFLSOAL='S','S','N')
          .DoRTCalc(51,54,.f.)
        .w_NUMDIC = .w_DINUMDIC
        .w_SERDIC = .w_DISERDIC
        .w_ANNO = .w_DI__ANNO
        .w_DATDIC = .w_DIDATDIC
        .w_NUMDOC = .w_DINUMDOC
        .w_SERDOC = .w_DISERDOC
        .w_DATLET = .w_DIDATLET
        .oPgFrm.Page2.oPag.Elabora_Dic_Inte.Calculate()
          .DoRTCalc(62,62,.f.)
        .w_DDDOCSER = .w_Elabora_Dic_Inte.getVar('DDDOCSER')
        .DoRTCalc(64,64,.f.)
          if not(empty(.w_DIDICCOL))
          .link_1_93('Full')
          endif
          .DoRTCalc(65,69,.f.)
        .w_DICSN = IIF(Not Empty(.w_DIDICCOL),'S','N')
        .w_IMPDIC = .w_DIIMPDIC
        .w_IMPUTI = .w_DIIMPUTI
        .w_DIIMPRES = (.w_DIIMPDIC-.w_DIIMPUTI)
        .w_IMPRES = .w_DIIMPRES
        .w_DIKEYINT = Sys(2015)
        .w_DIFLGCLO = 'N'
        .oPgFrm.Page2.oPag.oObj_2_29.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'DIC_INTE')
    this.DoRTCalc(77,83,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_58.enabled = this.oPgFrm.Page1.oPag.oBtn_1_58.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_92.enabled = this.oPgFrm.Page1.oPag.oBtn_1_92.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_105.enabled = this.oPgFrm.Page1.oPag.oBtn_1_105.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_31.enabled = this.oPgFrm.Page2.oPag.oBtn_2_31.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_32.enabled = this.oPgFrm.Page2.oPag.oBtn_2_32.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_118.enabled = this.oPgFrm.Page1.oPag.oBtn_1_118.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DIC_INTE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIC_INTE_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SEDIN","i_codazi,w_DISERIAL")
      cp_AskTableProg(this,i_nConn,"PRDIN","i_codazi,w_DI__ANNO,w_DITIPCON,w_DISERDIC,w_DINUMDIC")
      if .w_Ditipcon='F'
        cp_AskTableProg(this,i_nConn,"PRDID","i_codazi,w_DIAUTON2,w_DITIPCON,w_DITIPIVA,w_DINUMDOC")
      endif
      .op_codazi = .w_codazi
      .op_DISERIAL = .w_DISERIAL
      .op_codazi = .w_codazi
      .op_DI__ANNO = .w_DI__ANNO
      .op_DITIPCON = .w_DITIPCON
      .op_DISERDIC = .w_DISERDIC
      .op_DINUMDIC = .w_DINUMDIC
      .op_codazi = .w_codazi
      .op_DIAUTON2 = .w_DIAUTON2
      .op_DITIPCON = .w_DITIPCON
      .op_DITIPIVA = .w_DITIPIVA
      .op_DINUMDOC = .w_DINUMDOC
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oDITIPCON_1_4.enabled = i_bVal
      .Page1.oPag.oDITIPIVA_1_5.enabled = i_bVal
      .Page1.oPag.oDINUMDIC_1_6.enabled = i_bVal
      .Page1.oPag.oDISERDIC_1_8.enabled = i_bVal
      .Page1.oPag.oDI__ANNO_1_9.enabled = i_bVal
      .Page1.oPag.oDIDATDIC_1_10.enabled = i_bVal
      .Page1.oPag.oDINUMDOC_1_12.enabled = i_bVal
      .Page1.oPag.oDISERDOC_1_13.enabled = i_bVal
      .Page1.oPag.oDIDATLET_1_14.enabled = i_bVal
      .Page1.oPag.oDICODICE_1_16.enabled = i_bVal
      .Page1.oPag.oDIDOGANA_1_18.enabled = i_bVal
      .Page1.oPag.oDIUFFIVA_1_19.enabled = i_bVal
      .Page1.oPag.oDIDATAPL_1_20.enabled = i_bVal
      .Page1.oPag.oDIUFIVFO_1_22.enabled = i_bVal
      .Page1.oPag.oDIPROTEC_1_23.enabled = i_bVal
      .Page1.oPag.oDIPRODOC_1_24.enabled = i_bVal
      .Page1.oPag.oDICODIVA_1_25.enabled = i_bVal
      .Page1.oPag.oDIARTESE_1_26.enabled = i_bVal
      .Page1.oPag.oDIDESOPE_1_27.enabled = i_bVal
      .Page1.oPag.oDITIPOPE_1_29.enabled_(i_bVal)
      .Page1.oPag.oDITIPOPE_1_30.enabled_(i_bVal)
      .Page1.oPag.oDIIMPDIC_1_31.enabled = i_bVal
      .Page1.oPag.oDIDATINI_1_33.enabled = i_bVal
      .Page1.oPag.oDIDATFIN_1_34.enabled = i_bVal
      .Page1.oPag.oDIDATREV_1_35.enabled = i_bVal
      .Page1.oPag.oDIDATINI_1_79.enabled = i_bVal
      .Page1.oPag.oDIDATFIN_1_80.enabled = i_bVal
      .Page1.oPag.oDIFLGDOG_1_87.enabled = i_bVal
      .Page1.oPag.oDICSN_1_103.enabled = i_bVal
      .Page1.oPag.oDIFLGCLO_1_110.enabled = i_bVal
      .Page1.oPag.oBtn_1_58.enabled = .Page1.oPag.oBtn_1_58.mCond()
      .Page1.oPag.oBtn_1_92.enabled = .Page1.oPag.oBtn_1_92.mCond()
      .Page1.oPag.oBtn_1_105.enabled = .Page1.oPag.oBtn_1_105.mCond()
      .Page1.oPag.oBtn_1_107.enabled = i_bVal
      .Page2.oPag.oBtn_2_31.enabled = .Page2.oPag.oBtn_2_31.mCond()
      .Page2.oPag.oBtn_2_32.enabled = .Page2.oPag.oBtn_2_32.mCond()
      .Page1.oPag.oBtn_1_116.enabled = i_bVal
      .Page1.oPag.oBtn_1_118.enabled = .Page1.oPag.oBtn_1_118.mCond()
      .Page1.oPag.oObj_1_48.enabled = i_bVal
      .Page1.oPag.oObj_1_49.enabled = i_bVal
      .Page1.oPag.oObj_1_67.enabled = i_bVal
      .Page1.oPag.oObj_1_70.enabled = i_bVal
      .Page1.oPag.oObj_1_84.enabled = i_bVal
      .Page1.oPag.oObj_1_86.enabled = i_bVal
      .Page2.oPag.Elabora_Dic_Inte.enabled = i_bVal
      .Page2.oPag.oObj_2_29.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oDINUMDIC_1_6.enabled = .t.
        .Page1.oPag.oDI__ANNO_1_9.enabled = .t.
        .Page1.oPag.oDICODICE_1_16.enabled = .t.
      endif
    endwith
    this.GSCG_MDU.SetStatus(i_cOp)
    this.GSCG_MDF.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'DIC_INTE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSCG_MDU.SetChildrenStatus(i_cOp)
  *  this.GSCG_MDF.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DIC_INTE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DISERIAL,"DISERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DITIPCON,"DITIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DITIPIVA,"DITIPIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DINUMDIC,"DINUMDIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIALFDIC,"DIALFDIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DISERDIC,"DISERDIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DI__ANNO,"DI__ANNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIDATDIC,"DIDATDIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIALFDOC,"DIALFDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DINUMDOC,"DINUMDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DISERDOC,"DISERDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIDATLET,"DIDATLET",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DICODICE,"DICODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIDOGANA,"DIDOGANA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIUFFIVA,"DIUFFIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIDATAPL,"DIDATAPL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIUFIVFO,"DIUFIVFO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIPROTEC,"DIPROTEC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIPRODOC,"DIPRODOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DICODIVA,"DICODIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIARTESE,"DIARTESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIDESOPE,"DIDESOPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DITIPOPE,"DITIPOPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DITIPOPE,"DITIPOPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIIMPDIC,"DIIMPDIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIIMPUTI,"DIIMPUTI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIDATINI,"DIDATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIDATFIN,"DIDATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIDATREV,"DIDATREV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIFLSTAM,"DIFLSTAM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DINUMPRO,"DINUMPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIAUTON1,"DIAUTON1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIAUTON2,"DIAUTON2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIDATINI,"DIDATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIDATFIN,"DIDATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIFLGDOG,"DIFLGDOG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIDICCOL,"DIDICCOL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIFLGCLO,"DIFLGCLO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DIC_INTE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIC_INTE_IDX,2])
    i_lTable = "DIC_INTE"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.DIC_INTE_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSCG_SLI with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DIC_INTE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIC_INTE_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.DIC_INTE_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SEDIN","i_codazi,w_DISERIAL")
          cp_NextTableProg(this,i_nConn,"PRDIN","i_codazi,w_DI__ANNO,w_DITIPCON,w_DISERDIC,w_DINUMDIC")
        if .w_Ditipcon='F'
          cp_NextTableProg(this,i_nConn,"PRDID","i_codazi,w_DIAUTON2,w_DITIPCON,w_DITIPIVA,w_DINUMDOC")
        endif
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into DIC_INTE
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DIC_INTE')
        i_extval=cp_InsertValODBCExtFlds(this,'DIC_INTE')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(DISERIAL,DITIPCON,DITIPIVA,DINUMDIC,DIALFDIC"+;
                  ",DISERDIC,DI__ANNO,DIDATDIC,DIALFDOC,DINUMDOC"+;
                  ",DISERDOC,DIDATLET,DICODICE,DIDOGANA,DIUFFIVA"+;
                  ",DIDATAPL,DIUFIVFO,DIPROTEC,DIPRODOC,DICODIVA"+;
                  ",DIARTESE,DIDESOPE,DITIPOPE,DIIMPDIC,DIIMPUTI"+;
                  ",DIDATINI,DIDATFIN,DIDATREV,DIFLSTAM,DINUMPRO"+;
                  ",DIAUTON1,DIAUTON2,DIFLGDOG,DIDICCOL,DIFLGCLO"+;
                  ",UTCC,UTCV,UTDC,UTDV "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_DISERIAL)+;
                  ","+cp_ToStrODBC(this.w_DITIPCON)+;
                  ","+cp_ToStrODBC(this.w_DITIPIVA)+;
                  ","+cp_ToStrODBC(this.w_DINUMDIC)+;
                  ","+cp_ToStrODBC(this.w_DIALFDIC)+;
                  ","+cp_ToStrODBC(this.w_DISERDIC)+;
                  ","+cp_ToStrODBC(this.w_DI__ANNO)+;
                  ","+cp_ToStrODBC(this.w_DIDATDIC)+;
                  ","+cp_ToStrODBC(this.w_DIALFDOC)+;
                  ","+cp_ToStrODBC(this.w_DINUMDOC)+;
                  ","+cp_ToStrODBC(this.w_DISERDOC)+;
                  ","+cp_ToStrODBC(this.w_DIDATLET)+;
                  ","+cp_ToStrODBCNull(this.w_DICODICE)+;
                  ","+cp_ToStrODBC(this.w_DIDOGANA)+;
                  ","+cp_ToStrODBC(this.w_DIUFFIVA)+;
                  ","+cp_ToStrODBC(this.w_DIDATAPL)+;
                  ","+cp_ToStrODBC(this.w_DIUFIVFO)+;
                  ","+cp_ToStrODBC(this.w_DIPROTEC)+;
                  ","+cp_ToStrODBC(this.w_DIPRODOC)+;
                  ","+cp_ToStrODBCNull(this.w_DICODIVA)+;
                  ","+cp_ToStrODBC(this.w_DIARTESE)+;
                  ","+cp_ToStrODBC(this.w_DIDESOPE)+;
                  ","+cp_ToStrODBC(this.w_DITIPOPE)+;
                  ","+cp_ToStrODBC(this.w_DIIMPDIC)+;
                  ","+cp_ToStrODBC(this.w_DIIMPUTI)+;
                  ","+cp_ToStrODBC(this.w_DIDATINI)+;
                  ","+cp_ToStrODBC(this.w_DIDATFIN)+;
                  ","+cp_ToStrODBC(this.w_DIDATREV)+;
                  ","+cp_ToStrODBC(this.w_DIFLSTAM)+;
                  ","+cp_ToStrODBC(this.w_DINUMPRO)+;
                  ","+cp_ToStrODBC(this.w_DIAUTON1)+;
                  ","+cp_ToStrODBC(this.w_DIAUTON2)+;
                  ","+cp_ToStrODBC(this.w_DIFLGDOG)+;
                  ","+cp_ToStrODBCNull(this.w_DIDICCOL)+;
                  ","+cp_ToStrODBC(this.w_DIFLGCLO)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DIC_INTE')
        i_extval=cp_InsertValVFPExtFlds(this,'DIC_INTE')
        cp_CheckDeletedKey(i_cTable,0,'DISERIAL',this.w_DISERIAL)
        INSERT INTO (i_cTable);
              (DISERIAL,DITIPCON,DITIPIVA,DINUMDIC,DIALFDIC,DISERDIC,DI__ANNO,DIDATDIC,DIALFDOC,DINUMDOC,DISERDOC,DIDATLET,DICODICE,DIDOGANA,DIUFFIVA,DIDATAPL,DIUFIVFO,DIPROTEC,DIPRODOC,DICODIVA,DIARTESE,DIDESOPE,DITIPOPE,DIIMPDIC,DIIMPUTI,DIDATINI,DIDATFIN,DIDATREV,DIFLSTAM,DINUMPRO,DIAUTON1,DIAUTON2,DIFLGDOG,DIDICCOL,DIFLGCLO,UTCC,UTCV,UTDC,UTDV  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_DISERIAL;
                  ,this.w_DITIPCON;
                  ,this.w_DITIPIVA;
                  ,this.w_DINUMDIC;
                  ,this.w_DIALFDIC;
                  ,this.w_DISERDIC;
                  ,this.w_DI__ANNO;
                  ,this.w_DIDATDIC;
                  ,this.w_DIALFDOC;
                  ,this.w_DINUMDOC;
                  ,this.w_DISERDOC;
                  ,this.w_DIDATLET;
                  ,this.w_DICODICE;
                  ,this.w_DIDOGANA;
                  ,this.w_DIUFFIVA;
                  ,this.w_DIDATAPL;
                  ,this.w_DIUFIVFO;
                  ,this.w_DIPROTEC;
                  ,this.w_DIPRODOC;
                  ,this.w_DICODIVA;
                  ,this.w_DIARTESE;
                  ,this.w_DIDESOPE;
                  ,this.w_DITIPOPE;
                  ,this.w_DIIMPDIC;
                  ,this.w_DIIMPUTI;
                  ,this.w_DIDATINI;
                  ,this.w_DIDATFIN;
                  ,this.w_DIDATREV;
                  ,this.w_DIFLSTAM;
                  ,this.w_DINUMPRO;
                  ,this.w_DIAUTON1;
                  ,this.w_DIAUTON2;
                  ,this.w_DIFLGDOG;
                  ,this.w_DIDICCOL;
                  ,this.w_DIFLGCLO;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.DIC_INTE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIC_INTE_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.DIC_INTE_IDX,i_nConn)
      *
      * update DIC_INTE
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'DIC_INTE')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " DITIPCON="+cp_ToStrODBC(this.w_DITIPCON)+;
             ",DITIPIVA="+cp_ToStrODBC(this.w_DITIPIVA)+;
             ",DINUMDIC="+cp_ToStrODBC(this.w_DINUMDIC)+;
             ",DIALFDIC="+cp_ToStrODBC(this.w_DIALFDIC)+;
             ",DISERDIC="+cp_ToStrODBC(this.w_DISERDIC)+;
             ",DI__ANNO="+cp_ToStrODBC(this.w_DI__ANNO)+;
             ",DIDATDIC="+cp_ToStrODBC(this.w_DIDATDIC)+;
             ",DIALFDOC="+cp_ToStrODBC(this.w_DIALFDOC)+;
             ",DINUMDOC="+cp_ToStrODBC(this.w_DINUMDOC)+;
             ",DISERDOC="+cp_ToStrODBC(this.w_DISERDOC)+;
             ",DIDATLET="+cp_ToStrODBC(this.w_DIDATLET)+;
             ",DICODICE="+cp_ToStrODBCNull(this.w_DICODICE)+;
             ",DIDOGANA="+cp_ToStrODBC(this.w_DIDOGANA)+;
             ",DIUFFIVA="+cp_ToStrODBC(this.w_DIUFFIVA)+;
             ",DIDATAPL="+cp_ToStrODBC(this.w_DIDATAPL)+;
             ",DIUFIVFO="+cp_ToStrODBC(this.w_DIUFIVFO)+;
             ",DIPROTEC="+cp_ToStrODBC(this.w_DIPROTEC)+;
             ",DIPRODOC="+cp_ToStrODBC(this.w_DIPRODOC)+;
             ",DICODIVA="+cp_ToStrODBCNull(this.w_DICODIVA)+;
             ",DIARTESE="+cp_ToStrODBC(this.w_DIARTESE)+;
             ",DIDESOPE="+cp_ToStrODBC(this.w_DIDESOPE)+;
             ",DITIPOPE="+cp_ToStrODBC(this.w_DITIPOPE)+;
             ",DIIMPDIC="+cp_ToStrODBC(this.w_DIIMPDIC)+;
             ",DIIMPUTI="+cp_ToStrODBC(this.w_DIIMPUTI)+;
             ",DIDATINI="+cp_ToStrODBC(this.w_DIDATINI)+;
             ",DIDATFIN="+cp_ToStrODBC(this.w_DIDATFIN)+;
             ",DIDATREV="+cp_ToStrODBC(this.w_DIDATREV)+;
             ",DIFLSTAM="+cp_ToStrODBC(this.w_DIFLSTAM)+;
             ",DINUMPRO="+cp_ToStrODBC(this.w_DINUMPRO)+;
             ",DIAUTON1="+cp_ToStrODBC(this.w_DIAUTON1)+;
             ",DIAUTON2="+cp_ToStrODBC(this.w_DIAUTON2)+;
             ",DIFLGDOG="+cp_ToStrODBC(this.w_DIFLGDOG)+;
             ",DIDICCOL="+cp_ToStrODBCNull(this.w_DIDICCOL)+;
             ",DIFLGCLO="+cp_ToStrODBC(this.w_DIFLGCLO)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'DIC_INTE')
        i_cWhere = cp_PKFox(i_cTable  ,'DISERIAL',this.w_DISERIAL  )
        UPDATE (i_cTable) SET;
              DITIPCON=this.w_DITIPCON;
             ,DITIPIVA=this.w_DITIPIVA;
             ,DINUMDIC=this.w_DINUMDIC;
             ,DIALFDIC=this.w_DIALFDIC;
             ,DISERDIC=this.w_DISERDIC;
             ,DI__ANNO=this.w_DI__ANNO;
             ,DIDATDIC=this.w_DIDATDIC;
             ,DIALFDOC=this.w_DIALFDOC;
             ,DINUMDOC=this.w_DINUMDOC;
             ,DISERDOC=this.w_DISERDOC;
             ,DIDATLET=this.w_DIDATLET;
             ,DICODICE=this.w_DICODICE;
             ,DIDOGANA=this.w_DIDOGANA;
             ,DIUFFIVA=this.w_DIUFFIVA;
             ,DIDATAPL=this.w_DIDATAPL;
             ,DIUFIVFO=this.w_DIUFIVFO;
             ,DIPROTEC=this.w_DIPROTEC;
             ,DIPRODOC=this.w_DIPRODOC;
             ,DICODIVA=this.w_DICODIVA;
             ,DIARTESE=this.w_DIARTESE;
             ,DIDESOPE=this.w_DIDESOPE;
             ,DITIPOPE=this.w_DITIPOPE;
             ,DIIMPDIC=this.w_DIIMPDIC;
             ,DIIMPUTI=this.w_DIIMPUTI;
             ,DIDATINI=this.w_DIDATINI;
             ,DIDATFIN=this.w_DIDATFIN;
             ,DIDATREV=this.w_DIDATREV;
             ,DIFLSTAM=this.w_DIFLSTAM;
             ,DINUMPRO=this.w_DINUMPRO;
             ,DIAUTON1=this.w_DIAUTON1;
             ,DIAUTON2=this.w_DIAUTON2;
             ,DIFLGDOG=this.w_DIFLGDOG;
             ,DIDICCOL=this.w_DIDICCOL;
             ,DIFLGCLO=this.w_DIFLGCLO;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSCG_MDU : Saving
      this.GSCG_MDU.ChangeRow(this.cRowID+'      1',0;
             ,this.w_DISERIAL,"DDSERIAL";
             )
      this.GSCG_MDU.mReplace()
      * --- GSCG_MDF : Saving
      this.GSCG_MDF.ChangeRow(this.cRowID+'      1',0;
             ,this.w_DISERIAL,"DFSERIAL";
             )
      this.GSCG_MDF.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSCG_MDU : Deleting
    this.GSCG_MDU.ChangeRow(this.cRowID+'      1',0;
           ,this.w_DISERIAL,"DDSERIAL";
           )
    this.GSCG_MDU.mDelete()
    * --- GSCG_MDF : Deleting
    this.GSCG_MDF.ChangeRow(this.cRowID+'      1',0;
           ,this.w_DISERIAL,"DFSERIAL";
           )
    this.GSCG_MDF.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DIC_INTE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIC_INTE_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.DIC_INTE_IDX,i_nConn)
      *
      * delete DIC_INTE
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'DISERIAL',this.w_DISERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DIC_INTE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIC_INTE_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,11,.t.)
        if .o_DITIPCON<>.w_DITIPCON.or. .o_DITIPIVA<>.w_DITIPIVA
            .w_DINUMDOC = IIF(.w_DITIPCON='F',.w_DINUMDOC,0)
        endif
        .DoRTCalc(13,13,.t.)
        if .o_DITIPCON<>.w_DITIPCON
            .w_DIDATLET = IIF(.w_DITIPCON='F',.w_DIDATDIC,cp_CharToDate('  -  -  '))
        endif
        if .o_DIDATDIC<>.w_DIDATDIC
            .w_VALESE = CALVAP(.w_DIDATDIC)
          .link_1_15('Full')
        endif
        .DoRTCalc(16,17,.t.)
        if .o_DICODICE<>.w_DICODICE
            .w_DIDOGANA = IIF(.w_DITIPCON='C' OR NOT EMPTY(.w_DICODICE),'',.w_DIDOGANA)
        endif
        if .o_DITIPCON<>.w_DITIPCON
            .w_DIUFFIVA = iif(.w_DITIPCON='F',.w_UFFICIO,'')
        endif
        if .o_DITIPCON<>.w_DITIPCON.or. .o_DITIPIVA<>.w_DITIPIVA
            .w_DIDATAPL = Cp_CharToDate('  -  -    ')
        endif
        .DoRTCalc(21,22,.t.)
        if .o_Ditipcon<>.w_Ditipcon.or. .o_Ditipiva<>.w_Ditipiva
            .w_DIPROTEC = Space(17)
        endif
        if .o_DIPROTEC<>.w_DIPROTEC
            .w_DIPRODOC = IIF(.w_AGGPRODOC,0,.w_DIPRODOC)
        endif
        .DoRTCalc(25,30,.t.)
        if .o_DITIPOPE<>.w_DITIPOPE
            .w_DIIMPDIC = iif(.w_DITIPOPE<>'I',0,.w_DIIMPDIC)
        endif
        if .o_DITIPOPE<>.w_DITIPOPE
            .w_DIIMPUTI = iif(.w_DITIPOPE<>'I',0,.w_DIIMPUTI)
        endif
        .oPgFrm.Page1.oPag.oObj_1_48.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate()
        .DoRTCalc(33,35,.t.)
        if .o_DISERIAL<>.w_DISERIAL
            .w_ODATDIC = .w_DIDATDIC
        endif
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate(AH_Msgformat(IIF(.w_DITIPCON="C", "Cliente:",IIF(.w_DITIPCON="F","Fornitore:","")) ),'','')
        if .o_DITIPIVA<>.w_DITIPIVA
            .w_PERIVA = iif(.w_DITIPIVA='S',0,100)
        endif
        .DoRTCalc(38,39,.t.)
        if .o_DIDATDIC<>.w_DIDATDIC
            .w_OBTEST = .w_DIDATDIC
        endif
        .oPgFrm.Page1.oPag.oObj_1_67.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_70.Calculate()
        .DoRTCalc(41,44,.t.)
        if .o_DITIPIVA<>.w_DITIPIVA.or. .o_DIALFDOC<>.w_DIALFDOC
            .w_DINUMPRO = .w_DITIPIVA+.w_DIALFDOC
        endif
        .DoRTCalc(46,46,.t.)
            .w_DIAUTON2 = ALLTRIM(.w_DI__ANNO)+ALLTRIM(.w_DISERDOC)
        if .o_DITIPOPE<>.w_DITIPOPE
          .Calculate_WQVMRKMECT()
        endif
        .oPgFrm.Page1.oPag.oObj_1_84.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_86.Calculate()
        .DoRTCalc(48,49,.t.)
        if .o_DITIPCON<>.w_DITIPCON.or. .o_DICODICE<>.w_DICODICE.or. .o_DIFLSTAM<>.w_DIFLSTAM.or. .o_ANFLSOAL<>.w_ANFLSOAL
            .w_DIFLGDOG = IIF(.w_DITIPCON='F' And Not Empty(.w_DICODICE) And .w_DIFLSTAM<>'R' AND .w_ANFLSOAL='S','S','N')
        endif
        .DoRTCalc(51,54,.t.)
            .w_NUMDIC = .w_DINUMDIC
            .w_SERDIC = .w_DISERDIC
            .w_ANNO = .w_DI__ANNO
            .w_DATDIC = .w_DIDATDIC
            .w_NUMDOC = .w_DINUMDOC
            .w_SERDOC = .w_DISERDOC
            .w_DATLET = .w_DIDATLET
        .oPgFrm.Page2.oPag.Elabora_Dic_Inte.Calculate()
        .DoRTCalc(62,62,.t.)
            .w_DDDOCSER = .w_Elabora_Dic_Inte.getVar('DDDOCSER')
          .link_1_93('Full')
        .DoRTCalc(65,69,.t.)
        if .o_DIDICCOL<>.w_DIDICCOL
            .w_DICSN = IIF(Not Empty(.w_DIDICCOL),'S','N')
        endif
        if .o_DICSN<>.w_DICSN
          .Calculate_QEQITFMKNA()
        endif
            .w_IMPDIC = .w_DIIMPDIC
            .w_IMPUTI = .w_DIIMPUTI
        if .o_DIIMPDIC<>.w_DIIMPDIC.or. .o_DIIMPUTI<>.w_DIIMPUTI
          .Calculate_ZKEPZKDUGG()
        endif
        .DoRTCalc(73,73,.t.)
            .w_IMPRES = .w_DIIMPRES
        if .o_DIFLGCLO<>.w_DIFLGCLO
          .Calculate_HKMFBCXYNA()
        endif
        .oPgFrm.Page2.oPag.oObj_2_29.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"SEDIN","i_codazi,w_DISERIAL")
          .op_DISERIAL = .w_DISERIAL
        endif
        if .op_codazi<>.w_codazi .or. .op_DI__ANNO<>.w_DI__ANNO .or. .op_DITIPCON<>.w_DITIPCON .or. .op_DISERDIC<>.w_DISERDIC
           cp_AskTableProg(this,i_nConn,"PRDIN","i_codazi,w_DI__ANNO,w_DITIPCON,w_DISERDIC,w_DINUMDIC")
          .op_DINUMDIC = .w_DINUMDIC
        endif
        if .op_codazi<>.w_codazi .or. .op_DIAUTON2<>.w_DIAUTON2 .or. .op_DITIPCON<>.w_DITIPCON .or. .op_DITIPIVA<>.w_DITIPIVA
          if .w_Ditipcon='F'
             cp_AskTableProg(this,i_nConn,"PRDID","i_codazi,w_DIAUTON2,w_DITIPCON,w_DITIPIVA,w_DINUMDOC")
          endif
          .op_DINUMDOC = .w_DINUMDOC
        endif
        .op_codazi = .w_codazi
        .op_codazi = .w_codazi
        .op_DI__ANNO = .w_DI__ANNO
        .op_DITIPCON = .w_DITIPCON
        .op_DISERDIC = .w_DISERDIC
        .op_codazi = .w_codazi
        .op_DIAUTON2 = .w_DIAUTON2
        .op_DITIPCON = .w_DITIPCON
        .op_DITIPIVA = .w_DITIPIVA
      endwith
      this.DoRTCalc(75,83,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_48.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate(AH_Msgformat(IIF(.w_DITIPCON="C", "Cliente:",IIF(.w_DITIPCON="F","Fornitore:","")) ),'','')
        .oPgFrm.Page1.oPag.oObj_1_67.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_70.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_84.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_86.Calculate()
        .oPgFrm.Page2.oPag.Elabora_Dic_Inte.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_29.Calculate()
    endwith
  return

  proc Calculate_WQVMRKMECT()
    with this
          * --- Aggiorna Importo al variare del tipo operazione
          .w_DIIMPDIC = IIF (.w_DITIPOPE<> 'I' ,  0 , .w_DIIMPDIC  )
          .w_DIDATINI = IIF ( .w_DITIPOPE='D' AND EMPTY (.w_DIDATINI), cp_CharToDate("01-01-"+.w_DI__ANNO)  ,.w_DIDATINI)
          .w_DIDATFIN = IIF ( .w_DITIPOPE='D' AND EMPTY (.w_DIDATFIN), cp_CharToDate("31-12-"+.w_DI__ANNO)  ,.w_DIDATFIN)
    endwith
  endproc
  proc Calculate_YBAXBFILPS()
    with this
          * --- Valorizzo dati zoom 
          .w_Elabora_Dic_Inte.cCpQueryName = IIF(.w_ZOOM_DETT,'QUERY\DICDINTE','QUERY\DICDINTE4')
          .w_ELABORA_ZOOM = .NotifyEvent('Elabora_Zoom')
    endwith
  endproc
  proc Calculate_NBKBCTCOKW()
    with this
          * --- Chiusura tabella DIC_TEMP
          Gscg_Bdn(this;
              ,'Done';
             )
    endwith
  endproc
  proc Calculate_RMHQSCTZMR()
    with this
          * --- Valorizzo tabella DIC_TEMP
          GSCG_BDN(this;
              ,'Zoom';
             )
    endwith
  endproc
  proc Calculate_QEQITFMKNA()
    with this
          * --- Aggiorno dichiarazione di intento collegata
          .w_DIDICCOL = IIF(.w_DICSN='S',.w_DIDICCOL,Space(10))
          .link_1_93('Full')
    endwith
  endproc
  proc Calculate_ZKEPZKDUGG()
    with this
          * --- Aggiorno la variabile importo residuo
          .w_DIIMPRES = (.w_DIIMPDIC-.w_DIIMPUTI)
    endwith
  endproc
  proc Calculate_HKMFBCXYNA()
    with this
          * --- Azzero data revoca
          .w_DIDATREV = {}
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDITIPCON_1_4.enabled = this.oPgFrm.Page1.oPag.oDITIPCON_1_4.mCond()
    this.oPgFrm.Page1.oPag.oDITIPIVA_1_5.enabled = this.oPgFrm.Page1.oPag.oDITIPIVA_1_5.mCond()
    this.oPgFrm.Page1.oPag.oDINUMDIC_1_6.enabled = this.oPgFrm.Page1.oPag.oDINUMDIC_1_6.mCond()
    this.oPgFrm.Page1.oPag.oDISERDIC_1_8.enabled = this.oPgFrm.Page1.oPag.oDISERDIC_1_8.mCond()
    this.oPgFrm.Page1.oPag.oDI__ANNO_1_9.enabled = this.oPgFrm.Page1.oPag.oDI__ANNO_1_9.mCond()
    this.oPgFrm.Page1.oPag.oDIDATDIC_1_10.enabled = this.oPgFrm.Page1.oPag.oDIDATDIC_1_10.mCond()
    this.oPgFrm.Page1.oPag.oDINUMDOC_1_12.enabled = this.oPgFrm.Page1.oPag.oDINUMDOC_1_12.mCond()
    this.oPgFrm.Page1.oPag.oDISERDOC_1_13.enabled = this.oPgFrm.Page1.oPag.oDISERDOC_1_13.mCond()
    this.oPgFrm.Page1.oPag.oDIDATLET_1_14.enabled = this.oPgFrm.Page1.oPag.oDIDATLET_1_14.mCond()
    this.oPgFrm.Page1.oPag.oDICODICE_1_16.enabled = this.oPgFrm.Page1.oPag.oDICODICE_1_16.mCond()
    this.oPgFrm.Page1.oPag.oDIDOGANA_1_18.enabled = this.oPgFrm.Page1.oPag.oDIDOGANA_1_18.mCond()
    this.oPgFrm.Page1.oPag.oDIUFFIVA_1_19.enabled = this.oPgFrm.Page1.oPag.oDIUFFIVA_1_19.mCond()
    this.oPgFrm.Page1.oPag.oDIDATAPL_1_20.enabled = this.oPgFrm.Page1.oPag.oDIDATAPL_1_20.mCond()
    this.oPgFrm.Page1.oPag.oDIUFIVFO_1_22.enabled = this.oPgFrm.Page1.oPag.oDIUFIVFO_1_22.mCond()
    this.oPgFrm.Page1.oPag.oDIPROTEC_1_23.enabled = this.oPgFrm.Page1.oPag.oDIPROTEC_1_23.mCond()
    this.oPgFrm.Page1.oPag.oDIPRODOC_1_24.enabled = this.oPgFrm.Page1.oPag.oDIPRODOC_1_24.mCond()
    this.oPgFrm.Page1.oPag.oDICODIVA_1_25.enabled = this.oPgFrm.Page1.oPag.oDICODIVA_1_25.mCond()
    this.oPgFrm.Page1.oPag.oDIARTESE_1_26.enabled = this.oPgFrm.Page1.oPag.oDIARTESE_1_26.mCond()
    this.oPgFrm.Page1.oPag.oDIDESOPE_1_27.enabled = this.oPgFrm.Page1.oPag.oDIDESOPE_1_27.mCond()
    this.oPgFrm.Page1.oPag.oDITIPOPE_1_29.enabled_(this.oPgFrm.Page1.oPag.oDITIPOPE_1_29.mCond())
    this.oPgFrm.Page1.oPag.oDITIPOPE_1_30.enabled_(this.oPgFrm.Page1.oPag.oDITIPOPE_1_30.mCond())
    this.oPgFrm.Page1.oPag.oDIIMPDIC_1_31.enabled = this.oPgFrm.Page1.oPag.oDIIMPDIC_1_31.mCond()
    this.oPgFrm.Page1.oPag.oDIDATINI_1_33.enabled = this.oPgFrm.Page1.oPag.oDIDATINI_1_33.mCond()
    this.oPgFrm.Page1.oPag.oDIDATFIN_1_34.enabled = this.oPgFrm.Page1.oPag.oDIDATFIN_1_34.mCond()
    this.oPgFrm.Page1.oPag.oDIDATINI_1_79.enabled = this.oPgFrm.Page1.oPag.oDIDATINI_1_79.mCond()
    this.oPgFrm.Page1.oPag.oDIDATFIN_1_80.enabled = this.oPgFrm.Page1.oPag.oDIDATFIN_1_80.mCond()
    this.oPgFrm.Page1.oPag.oDIFLGDOG_1_87.enabled = this.oPgFrm.Page1.oPag.oDIFLGDOG_1_87.mCond()
    this.oPgFrm.Page1.oPag.oDICSN_1_103.enabled = this.oPgFrm.Page1.oPag.oDICSN_1_103.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_58.enabled = this.oPgFrm.Page1.oPag.oBtn_1_58.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_92.enabled = this.oPgFrm.Page1.oPag.oBtn_1_92.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_107.enabled = this.oPgFrm.Page1.oPag.oBtn_1_107.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_31.enabled = this.oPgFrm.Page2.oPag.oBtn_2_31.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_32.enabled = this.oPgFrm.Page2.oPag.oBtn_2_32.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_116.enabled = this.oPgFrm.Page1.oPag.oBtn_1_116.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_118.enabled = this.oPgFrm.Page1.oPag.oBtn_1_118.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    oField= this.oPgFrm.Page1.oPag.oDIDATREV_1_35
    cCShape='TTxOb'+m.oField.Name
    If PemStatus(oField.Parent, cCShape, 5) 
      oField.Parent.&cCShape..visible= oField.visible And oField.enabled And oField.CondObbl()
    EndIf
    oField= this.oPgFrm.Page1.oPag.oDIDATINI_1_79
    cCShape='TTxOb'+m.oField.Name
    If PemStatus(oField.Parent, cCShape, 5) 
      oField.Parent.&cCShape..visible= oField.visible And oField.enabled And oField.CondObbl()
    EndIf
    oField= this.oPgFrm.Page1.oPag.oDIDATFIN_1_80
    cCShape='TTxOb'+m.oField.Name
    If PemStatus(oField.Parent, cCShape, 5) 
      oField.Parent.&cCShape..visible= oField.visible And oField.enabled And oField.CondObbl()
    EndIf
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Pages(2).enabled=not(this.w_Ditipope='D' Or this.w_Di__Anno<'2017' Or Empty(this.w_Dicodice))
    this.oPgFrm.Page1.oPag.oDIUFIVFO_1_22.visible=!this.oPgFrm.Page1.oPag.oDIUFIVFO_1_22.mHide()
    this.oPgFrm.Page1.oPag.oDITIPOPE_1_29.visible=!this.oPgFrm.Page1.oPag.oDITIPOPE_1_29.mHide()
    this.oPgFrm.Page1.oPag.oDITIPOPE_1_30.visible=!this.oPgFrm.Page1.oPag.oDITIPOPE_1_30.mHide()
    this.oPgFrm.Page1.oPag.oDIDATINI_1_33.visible=!this.oPgFrm.Page1.oPag.oDIDATINI_1_33.mHide()
    this.oPgFrm.Page1.oPag.oDIDATFIN_1_34.visible=!this.oPgFrm.Page1.oPag.oDIDATFIN_1_34.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_58.visible=!this.oPgFrm.Page1.oPag.oBtn_1_58.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_71.visible=!this.oPgFrm.Page1.oPag.oStr_1_71.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_77.visible=!this.oPgFrm.Page1.oPag.oStr_1_77.mHide()
    this.oPgFrm.Page1.oPag.oDIDATINI_1_79.visible=!this.oPgFrm.Page1.oPag.oDIDATINI_1_79.mHide()
    this.oPgFrm.Page1.oPag.oDIDATFIN_1_80.visible=!this.oPgFrm.Page1.oPag.oDIDATFIN_1_80.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_85.visible=!this.oPgFrm.Page1.oPag.oStr_1_85.mHide()
    this.oPgFrm.Page1.oPag.oDIFLGDOG_1_87.visible=!this.oPgFrm.Page1.oPag.oDIFLGDOG_1_87.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_92.visible=!this.oPgFrm.Page1.oPag.oBtn_1_92.mHide()
    this.oPgFrm.Page1.oPag.oNUMDIC_COLL_1_94.visible=!this.oPgFrm.Page1.oPag.oNUMDIC_COLL_1_94.mHide()
    this.oPgFrm.Page1.oPag.oANNDIC_COLL_1_95.visible=!this.oPgFrm.Page1.oPag.oANNDIC_COLL_1_95.mHide()
    this.oPgFrm.Page1.oPag.oDATDIC_COLL_1_96.visible=!this.oPgFrm.Page1.oPag.oDATDIC_COLL_1_96.mHide()
    this.oPgFrm.Page1.oPag.oSERDIC_COLL_1_97.visible=!this.oPgFrm.Page1.oPag.oSERDIC_COLL_1_97.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_98.visible=!this.oPgFrm.Page1.oPag.oStr_1_98.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_99.visible=!this.oPgFrm.Page1.oPag.oStr_1_99.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_100.visible=!this.oPgFrm.Page1.oPag.oStr_1_100.mHide()
    this.oPgFrm.Page1.oPag.oLinkPC_1_101.visible=!this.oPgFrm.Page1.oPag.oLinkPC_1_101.mHide()
    this.oPgFrm.Page1.oPag.oDICSN_1_103.visible=!this.oPgFrm.Page1.oPag.oDICSN_1_103.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_105.visible=!this.oPgFrm.Page1.oPag.oBtn_1_105.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_107.visible=!this.oPgFrm.Page1.oPag.oBtn_1_107.mHide()
    this.oPgFrm.Page1.oPag.oDIFLGCLO_1_110.visible=!this.oPgFrm.Page1.oPag.oDIFLGCLO_1_110.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_111.visible=!this.oPgFrm.Page1.oPag.oStr_1_111.mHide()
    this.oPgFrm.Page1.oPag.oNUMDOC_COLL_1_113.visible=!this.oPgFrm.Page1.oPag.oNUMDOC_COLL_1_113.mHide()
    this.oPgFrm.Page1.oPag.oSERDOC_COLL_1_114.visible=!this.oPgFrm.Page1.oPag.oSERDOC_COLL_1_114.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_31.visible=!this.oPgFrm.Page2.oPag.oBtn_2_31.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_32.visible=!this.oPgFrm.Page2.oPag.oBtn_2_32.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_116.visible=!this.oPgFrm.Page1.oPag.oBtn_1_116.mHide()
    this.oPgFrm.Page1.oPag.oLinkPC_1_117.visible=!this.oPgFrm.Page1.oPag.oLinkPC_1_117.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_48.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_49.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_52.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_67.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_70.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_84.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_86.Event(cEvent)
      .oPgFrm.Page2.oPag.Elabora_Dic_Inte.Event(cEvent)
        if lower(cEvent)==lower("ActivatePage 2") or lower(cEvent)==lower("Elaborazione")
          .Calculate_YBAXBFILPS()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Done") or lower(cEvent)==lower("Ricostruzione")
          .Calculate_NBKBCTCOKW()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Ricostruzione")
          .Calculate_RMHQSCTZMR()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Load")
          .Calculate_ZKEPZKDUGG()
          bRefresh=.t.
        endif
      .oPgFrm.Page2.oPag.oObj_2_29.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gscg_adi
    local obj
    IF cevent = 'w_DIDATINI Changed' 
       this.w_DIDATFIN = IIF ( this.w_DITIPOPE<> 'D' ,  IIF ( EMPTY (this.w_DIDATINI) , this.w_DIDATINI, IIF ( EMPTY ( this.w_DIDATFIN ) ,  MAX (this.w_DIDATINI , cp_CharToDate("31-12-"+this.w_DI__ANNO)) , this.w_DIDATFIN ))  ,this.w_DIDATFIN)
       obj=this.getctrl ("w_DIDATFIN")
       obj.value=this.w_DIDATFIN
       IF NOT CHKLINTE(this.w_DIDATINI,this.w_DIDATFIN,this.w_DIDATDIC, .F.)
         ah_ERRORMSG( "L'intervallo di date � incongruente oppure non sono comprese nel solito anno")
       ENDIF
    ENDIF
    
    
    IF cevent = 'w_DIDATFIN Changed' 
       this.w_DIDATINI = IIF ( this.w_DITIPOPE<> 'D' ,  IIF ( EMPTY (this.w_DIDATFIN) , this.w_DIDATFIN,IIF ( EMPTY ( this.w_DIDATINI ) ,  MIN (this.w_DIDATFIN , cp_CharToDate("01-01-"+this.w_DI__ANNO)), this.w_DIDATINI ))  ,this.w_DIDATINI)
          obj=this.getctrl ("w_DIDATINI")
          obj.value=this.w_DIDATINI
       IF NOT CHKLINTE(this.w_DIDATINI,this.w_DIDATFIN,this.w_DIDATDIC,.F.)
         ah_ERRORMSG( "L'intervallo di date � incongruente oppure non sono comprese nel solito anno")
       ENDIF 
    ENDIF
    
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=AZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZIVAAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_AZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_AZI)
            select AZCODAZI,AZIVAAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_UFFICIO = NVL(_Link_.AZIVAAZI,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_AZI = space(5)
      endif
      this.w_UFFICIO = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALESE
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALESE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALESE)
            select VACODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALESE = NVL(_Link_.VACODVAL,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_VALESE = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DICODICE
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DICODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_DICODICE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_DITIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANFLSOAL,AN_EMAIL,AN_EMPEC,ANTELFAX";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_DITIPCON;
                     ,'ANCODICE',trim(this.w_DICODICE))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANFLSOAL,AN_EMAIL,AN_EMPEC,ANTELFAX;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DICODICE)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_DICODICE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_DITIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANFLSOAL,AN_EMAIL,AN_EMPEC,ANTELFAX";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_DICODICE)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_DITIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANFLSOAL,AN_EMAIL,AN_EMPEC,ANTELFAX;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DICODICE) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oDICODICE_1_16'),i_cWhere,'GSAR_BZC',"Clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_DITIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANFLSOAL,AN_EMAIL,AN_EMPEC,ANTELFAX";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANFLSOAL,AN_EMAIL,AN_EMPEC,ANTELFAX;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANFLSOAL,AN_EMAIL,AN_EMPEC,ANTELFAX";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_DITIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANFLSOAL,AN_EMAIL,AN_EMPEC,ANTELFAX;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DICODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANFLSOAL,AN_EMAIL,AN_EMPEC,ANTELFAX";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_DICODICE);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_DITIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_DITIPCON;
                       ,'ANCODICE',this.w_DICODICE)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANFLSOAL,AN_EMAIL,AN_EMPEC,ANTELFAX;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DICODICE = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_ANFLSOAL = NVL(_Link_.ANFLSOAL,space(1))
      this.w_EMAIL = NVL(_Link_.AN_EMAIL,space(254))
      this.w_EMPEC = NVL(_Link_.AN_EMPEC,space(254))
      this.w_TELFAX = NVL(_Link_.ANTELFAX,space(18))
    else
      if i_cCtrl<>'Load'
        this.w_DICODICE = space(15)
      endif
      this.w_DESCON = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_ANFLSOAL = space(1)
      this.w_EMAIL = space(254)
      this.w_EMPEC = space(254)
      this.w_TELFAX = space(18)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        endif
        this.w_DICODICE = space(15)
        this.w_DESCON = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_ANFLSOAL = space(1)
        this.w_EMAIL = space(254)
        this.w_EMPEC = space(254)
        this.w_TELFAX = space(18)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DICODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_16(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 7 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+7<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_16.ANCODICE as ANCODICE116"+ ",link_1_16.ANDESCRI as ANDESCRI116"+ ",link_1_16.ANDTOBSO as ANDTOBSO116"+ ",link_1_16.ANFLSOAL as ANFLSOAL116"+ ",link_1_16.AN_EMAIL as AN_EMAIL116"+ ",link_1_16.AN_EMPEC as AN_EMPEC116"+ ",link_1_16.ANTELFAX as ANTELFAX116"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_16 on DIC_INTE.DICODICE=link_1_16.ANCODICE"+" and DIC_INTE.DITIPCON=link_1_16.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_16"
          i_cKey=i_cKey+'+" and DIC_INTE.DICODICE=link_1_16.ANCODICE(+)"'+'+" and DIC_INTE.DITIPCON=link_1_16.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DICODIVA
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DICODIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_DICODIVA)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVPERIVA,IVDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_DICODIVA))
          select IVCODIVA,IVDESIVA,IVPERIVA,IVDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DICODIVA)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStrODBC(trim(this.w_DICODIVA)+"%");

            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVPERIVA,IVDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStr(trim(this.w_DICODIVA)+"%");

            select IVCODIVA,IVDESIVA,IVPERIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DICODIVA) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oDICODIVA_1_25'),i_cWhere,'GSAR_AIV',"Codici IVA esenti",'GSCG0ADI.VOCIIVA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVPERIVA,IVDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDESIVA,IVPERIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DICODIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVPERIVA,IVDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_DICODIVA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_DICODIVA)
            select IVCODIVA,IVDESIVA,IVPERIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DICODIVA = NVL(_Link_.IVCODIVA,space(5))
      this.w_DESIVA = NVL(_Link_.IVDESIVA,space(35))
      this.w_PERIVA = NVL(_Link_.IVPERIVA,0)
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.IVDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DICODIVA = space(5)
      endif
      this.w_DESIVA = space(35)
      this.w_PERIVA = 0
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_PERIVA=0 AND .w_DITIPIVA='S') OR .w_DITIPIVA='N') and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice IVA inesistente oppure obsoleto")
        endif
        this.w_DICODIVA = space(5)
        this.w_DESIVA = space(35)
        this.w_PERIVA = 0
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DICODIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_25(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOCIIVA_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_25.IVCODIVA as IVCODIVA125"+ ",link_1_25.IVDESIVA as IVDESIVA125"+ ",link_1_25.IVPERIVA as IVPERIVA125"+ ",link_1_25.IVDTOBSO as IVDTOBSO125"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_25 on DIC_INTE.DICODIVA=link_1_25.IVCODIVA"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_25"
          i_cKey=i_cKey+'+" and DIC_INTE.DICODIVA=link_1_25.IVCODIVA(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DIDICCOL
  func Link_1_93(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIC_INTE_IDX,3]
    i_lTable = "DIC_INTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIC_INTE_IDX,2], .t., this.DIC_INTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIC_INTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DIDICCOL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DIDICCOL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DISERIAL,DINUMDOC,DISERDOC,DI__ANNO,DIDATLET,DINUMDIC,DISERDIC";
                   +" from "+i_cTable+" "+i_lTable+" where DISERIAL="+cp_ToStrODBC(this.w_DIDICCOL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DISERIAL',this.w_DIDICCOL)
            select DISERIAL,DINUMDOC,DISERDOC,DI__ANNO,DIDATLET,DINUMDIC,DISERDIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DIDICCOL = NVL(_Link_.DISERIAL,space(15))
      this.w_NUMDOC_COLL = NVL(_Link_.DINUMDOC,0)
      this.w_SERDOC_COLL = NVL(_Link_.DISERDOC,space(3))
      this.w_ANNDIC_COLL = NVL(_Link_.DI__ANNO,space(4))
      this.w_DATDIC_COLL = NVL(cp_ToDate(_Link_.DIDATLET),ctod("  /  /  "))
      this.w_NUMDIC_COLL = NVL(_Link_.DINUMDIC,0)
      this.w_SERDIC_COLL = NVL(_Link_.DISERDIC,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_DIDICCOL = space(15)
      endif
      this.w_NUMDOC_COLL = 0
      this.w_SERDOC_COLL = space(3)
      this.w_ANNDIC_COLL = space(4)
      this.w_DATDIC_COLL = ctod("  /  /  ")
      this.w_NUMDIC_COLL = 0
      this.w_SERDIC_COLL = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIC_INTE_IDX,2])+'\'+cp_ToStr(_Link_.DISERIAL,1)
      cp_ShowWarn(i_cKey,this.DIC_INTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DIDICCOL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_93(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 7 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.DIC_INTE_IDX,3] and i_nFlds+7<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.DIC_INTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_93.DISERIAL as DISERIAL193"+ ",link_1_93.DINUMDOC as DINUMDOC193"+ ",link_1_93.DISERDOC as DISERDOC193"+ ",link_1_93.DI__ANNO as DI__ANNO193"+ ",link_1_93.DIDATLET as DIDATLET193"+ ",link_1_93.DINUMDIC as DINUMDIC193"+ ",link_1_93.DISERDIC as DISERDIC193"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_93 on DIC_INTE.DIDICCOL=link_1_93.DISERIAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_93"
          i_cKey=i_cKey+'+" and DIC_INTE.DIDICCOL=link_1_93.DISERIAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDITIPCON_1_4.RadioValue()==this.w_DITIPCON)
      this.oPgFrm.Page1.oPag.oDITIPCON_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDITIPIVA_1_5.RadioValue()==this.w_DITIPIVA)
      this.oPgFrm.Page1.oPag.oDITIPIVA_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDINUMDIC_1_6.value==this.w_DINUMDIC)
      this.oPgFrm.Page1.oPag.oDINUMDIC_1_6.value=this.w_DINUMDIC
    endif
    if not(this.oPgFrm.Page1.oPag.oDISERDIC_1_8.value==this.w_DISERDIC)
      this.oPgFrm.Page1.oPag.oDISERDIC_1_8.value=this.w_DISERDIC
    endif
    if not(this.oPgFrm.Page1.oPag.oDI__ANNO_1_9.value==this.w_DI__ANNO)
      this.oPgFrm.Page1.oPag.oDI__ANNO_1_9.value=this.w_DI__ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oDIDATDIC_1_10.value==this.w_DIDATDIC)
      this.oPgFrm.Page1.oPag.oDIDATDIC_1_10.value=this.w_DIDATDIC
    endif
    if not(this.oPgFrm.Page1.oPag.oDINUMDOC_1_12.value==this.w_DINUMDOC)
      this.oPgFrm.Page1.oPag.oDINUMDOC_1_12.value=this.w_DINUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDISERDOC_1_13.value==this.w_DISERDOC)
      this.oPgFrm.Page1.oPag.oDISERDOC_1_13.value=this.w_DISERDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDIDATLET_1_14.value==this.w_DIDATLET)
      this.oPgFrm.Page1.oPag.oDIDATLET_1_14.value=this.w_DIDATLET
    endif
    if not(this.oPgFrm.Page1.oPag.oVALESE_1_15.value==this.w_VALESE)
      this.oPgFrm.Page1.oPag.oVALESE_1_15.value=this.w_VALESE
    endif
    if not(this.oPgFrm.Page1.oPag.oDICODICE_1_16.value==this.w_DICODICE)
      this.oPgFrm.Page1.oPag.oDICODICE_1_16.value=this.w_DICODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_17.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_17.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDIDOGANA_1_18.value==this.w_DIDOGANA)
      this.oPgFrm.Page1.oPag.oDIDOGANA_1_18.value=this.w_DIDOGANA
    endif
    if not(this.oPgFrm.Page1.oPag.oDIUFFIVA_1_19.value==this.w_DIUFFIVA)
      this.oPgFrm.Page1.oPag.oDIUFFIVA_1_19.value=this.w_DIUFFIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oDIDATAPL_1_20.value==this.w_DIDATAPL)
      this.oPgFrm.Page1.oPag.oDIDATAPL_1_20.value=this.w_DIDATAPL
    endif
    if not(this.oPgFrm.Page1.oPag.oDIUFIVFO_1_22.value==this.w_DIUFIVFO)
      this.oPgFrm.Page1.oPag.oDIUFIVFO_1_22.value=this.w_DIUFIVFO
    endif
    if not(this.oPgFrm.Page1.oPag.oDIPROTEC_1_23.value==this.w_DIPROTEC)
      this.oPgFrm.Page1.oPag.oDIPROTEC_1_23.value=this.w_DIPROTEC
    endif
    if not(this.oPgFrm.Page1.oPag.oDIPRODOC_1_24.value==this.w_DIPRODOC)
      this.oPgFrm.Page1.oPag.oDIPRODOC_1_24.value=this.w_DIPRODOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDICODIVA_1_25.value==this.w_DICODIVA)
      this.oPgFrm.Page1.oPag.oDICODIVA_1_25.value=this.w_DICODIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oDIARTESE_1_26.value==this.w_DIARTESE)
      this.oPgFrm.Page1.oPag.oDIARTESE_1_26.value=this.w_DIARTESE
    endif
    if not(this.oPgFrm.Page1.oPag.oDIDESOPE_1_27.value==this.w_DIDESOPE)
      this.oPgFrm.Page1.oPag.oDIDESOPE_1_27.value=this.w_DIDESOPE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESIVA_1_28.value==this.w_DESIVA)
      this.oPgFrm.Page1.oPag.oDESIVA_1_28.value=this.w_DESIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oDITIPOPE_1_29.RadioValue()==this.w_DITIPOPE)
      this.oPgFrm.Page1.oPag.oDITIPOPE_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDITIPOPE_1_30.RadioValue()==this.w_DITIPOPE)
      this.oPgFrm.Page1.oPag.oDITIPOPE_1_30.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDIIMPDIC_1_31.value==this.w_DIIMPDIC)
      this.oPgFrm.Page1.oPag.oDIIMPDIC_1_31.value=this.w_DIIMPDIC
    endif
    if not(this.oPgFrm.Page1.oPag.oDIIMPUTI_1_32.value==this.w_DIIMPUTI)
      this.oPgFrm.Page1.oPag.oDIIMPUTI_1_32.value=this.w_DIIMPUTI
    endif
    if not(this.oPgFrm.Page1.oPag.oDIDATINI_1_33.value==this.w_DIDATINI)
      this.oPgFrm.Page1.oPag.oDIDATINI_1_33.value=this.w_DIDATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDIDATFIN_1_34.value==this.w_DIDATFIN)
      this.oPgFrm.Page1.oPag.oDIDATFIN_1_34.value=this.w_DIDATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDIDATREV_1_35.value==this.w_DIDATREV)
      this.oPgFrm.Page1.oPag.oDIDATREV_1_35.value=this.w_DIDATREV
    endif
    if not(this.oPgFrm.Page1.oPag.oDIFLSTAM_1_56.RadioValue()==this.w_DIFLSTAM)
      this.oPgFrm.Page1.oPag.oDIFLSTAM_1_56.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDIDATINI_1_79.value==this.w_DIDATINI)
      this.oPgFrm.Page1.oPag.oDIDATINI_1_79.value=this.w_DIDATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDIDATFIN_1_80.value==this.w_DIDATFIN)
      this.oPgFrm.Page1.oPag.oDIDATFIN_1_80.value=this.w_DIDATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDIFLGDOG_1_87.RadioValue()==this.w_DIFLGDOG)
      this.oPgFrm.Page1.oPag.oDIFLGDOG_1_87.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNUMDIC_2_1.value==this.w_NUMDIC)
      this.oPgFrm.Page2.oPag.oNUMDIC_2_1.value=this.w_NUMDIC
    endif
    if not(this.oPgFrm.Page2.oPag.oSERDIC_2_2.value==this.w_SERDIC)
      this.oPgFrm.Page2.oPag.oSERDIC_2_2.value=this.w_SERDIC
    endif
    if not(this.oPgFrm.Page2.oPag.oANNO_2_3.value==this.w_ANNO)
      this.oPgFrm.Page2.oPag.oANNO_2_3.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page2.oPag.oDATDIC_2_11.value==this.w_DATDIC)
      this.oPgFrm.Page2.oPag.oDATDIC_2_11.value=this.w_DATDIC
    endif
    if not(this.oPgFrm.Page2.oPag.oNUMDOC_2_12.value==this.w_NUMDOC)
      this.oPgFrm.Page2.oPag.oNUMDOC_2_12.value=this.w_NUMDOC
    endif
    if not(this.oPgFrm.Page2.oPag.oSERDOC_2_13.value==this.w_SERDOC)
      this.oPgFrm.Page2.oPag.oSERDOC_2_13.value=this.w_SERDOC
    endif
    if not(this.oPgFrm.Page2.oPag.oDATLET_2_14.value==this.w_DATLET)
      this.oPgFrm.Page2.oPag.oDATLET_2_14.value=this.w_DATLET
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMDIC_COLL_1_94.value==this.w_NUMDIC_COLL)
      this.oPgFrm.Page1.oPag.oNUMDIC_COLL_1_94.value=this.w_NUMDIC_COLL
    endif
    if not(this.oPgFrm.Page1.oPag.oANNDIC_COLL_1_95.value==this.w_ANNDIC_COLL)
      this.oPgFrm.Page1.oPag.oANNDIC_COLL_1_95.value=this.w_ANNDIC_COLL
    endif
    if not(this.oPgFrm.Page1.oPag.oDATDIC_COLL_1_96.value==this.w_DATDIC_COLL)
      this.oPgFrm.Page1.oPag.oDATDIC_COLL_1_96.value=this.w_DATDIC_COLL
    endif
    if not(this.oPgFrm.Page1.oPag.oSERDIC_COLL_1_97.value==this.w_SERDIC_COLL)
      this.oPgFrm.Page1.oPag.oSERDIC_COLL_1_97.value=this.w_SERDIC_COLL
    endif
    if not(this.oPgFrm.Page1.oPag.oDICSN_1_103.RadioValue()==this.w_DICSN)
      this.oPgFrm.Page1.oPag.oDICSN_1_103.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oIMPDIC_2_23.value==this.w_IMPDIC)
      this.oPgFrm.Page2.oPag.oIMPDIC_2_23.value=this.w_IMPDIC
    endif
    if not(this.oPgFrm.Page2.oPag.oIMPUTI_2_24.value==this.w_IMPUTI)
      this.oPgFrm.Page2.oPag.oIMPUTI_2_24.value=this.w_IMPUTI
    endif
    if not(this.oPgFrm.Page1.oPag.oDIIMPRES_1_108.value==this.w_DIIMPRES)
      this.oPgFrm.Page1.oPag.oDIIMPRES_1_108.value=this.w_DIIMPRES
    endif
    if not(this.oPgFrm.Page2.oPag.oIMPRES_2_26.value==this.w_IMPRES)
      this.oPgFrm.Page2.oPag.oIMPRES_2_26.value=this.w_IMPRES
    endif
    if not(this.oPgFrm.Page1.oPag.oDIFLGCLO_1_110.RadioValue()==this.w_DIFLGCLO)
      this.oPgFrm.Page1.oPag.oDIFLGCLO_1_110.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMDOC_COLL_1_113.value==this.w_NUMDOC_COLL)
      this.oPgFrm.Page1.oPag.oNUMDOC_COLL_1_113.value=this.w_NUMDOC_COLL
    endif
    if not(this.oPgFrm.Page1.oPag.oSERDOC_COLL_1_114.value==this.w_SERDOC_COLL)
      this.oPgFrm.Page1.oPag.oSERDOC_COLL_1_114.value=this.w_SERDOC_COLL
    endif
    cp_SetControlsValueExtFlds(this,'DIC_INTE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DINUMDIC))  and (.w_DIFLSTAM<>'R')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDINUMDIC_1_6.SetFocus()
            i_bnoObbl = !empty(.w_DINUMDIC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DI__ANNO))  and (.w_DIFLSTAM<>'R')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDI__ANNO_1_9.SetFocus()
            i_bnoObbl = !empty(.w_DI__ANNO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DIDATDIC))  and (.w_DIFLSTAM<>'R')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDIDATDIC_1_10.SetFocus()
            i_bnoObbl = !empty(.w_DIDATDIC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DINUMDOC))  and (.w_DIFLSTAM<>'R')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDINUMDOC_1_12.SetFocus()
            i_bnoObbl = !empty(.w_DINUMDOC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and (.w_DIFLSTAM<>'R')  and not(empty(.w_DICODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDICODICE_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto")
          case   ((empty(.w_DICODIVA)) or not(((.w_PERIVA=0 AND .w_DITIPIVA='S') OR .w_DITIPIVA='N') and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)))  and (.w_DIFLSTAM<>'R')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDICODIVA_1_25.SetFocus()
            i_bnoObbl = !empty(.w_DICODIVA)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice IVA inesistente oppure obsoleto")
          case   (empty(.w_DITIPOPE))  and not(.w_DITIPOPE $ 'IO')  and (.cFunction='Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDITIPOPE_1_29.SetFocus()
            i_bnoObbl = !empty(.w_DITIPOPE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DITIPOPE))  and not(NOT .w_DITIPOPE $ 'IO')  and (.cFunction='Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDITIPOPE_1_30.SetFocus()
            i_bnoObbl = !empty(.w_DITIPOPE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_DIDATREV) and (.w_DIFLGCLO='S')) or not(EMPTY(.w_DIDATREV) OR (.w_DIDATREV>=.w_DIDATINI AND .w_DIDATREV<=.w_DIDATFIN)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDIDATREV_1_35.SetFocus()
            i_bnoObbl = !empty(.w_DIDATREV) or !(.w_DIFLGCLO='S')
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data di revoca deve essere maggiore o uguale alla data d'inizio validit� e minore o uguale alla data di fine validit�")
          case   (empty(.w_DIDATINI) and (.w_DITIPOPE='D'))  and not(.w_DITIPOPE<>'D')  and (.w_DIFLSTAM<>'R')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDIDATINI_1_79.SetFocus()
            i_bnoObbl = !empty(.w_DIDATINI) or !(.w_DITIPOPE='D')
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DIDATFIN) and (.w_DITIPOPE='D' ))  and not(.w_DITIPOPE<>'D')  and (.w_DIFLSTAM<>'R')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDIDATFIN_1_80.SetFocus()
            i_bnoObbl = !empty(.w_DIDATFIN) or !(.w_DITIPOPE='D' )
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .GSCG_MDU.CheckForm()
      if i_bres
        i_bres=  .GSCG_MDU.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      *i_bRes = i_bRes .and. .GSCG_MDF.CheckForm()
      if i_bres
        i_bres=  .GSCG_MDF.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gscg_adi
      * Controllo se la dichirazione pu� essere salvata / cancellata
      .w_RESCHK=0
      .NotifyEvent('Check')
      if .w_RESCHK<>0
        i_bRes=.f.
      endif
      
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DISERIAL = this.w_DISERIAL
    this.o_DITIPCON = this.w_DITIPCON
    this.o_DITIPIVA = this.w_DITIPIVA
    this.o_DIDATDIC = this.w_DIDATDIC
    this.o_DIALFDOC = this.w_DIALFDOC
    this.o_DICODICE = this.w_DICODICE
    this.o_DIPROTEC = this.w_DIPROTEC
    this.o_DITIPOPE = this.w_DITIPOPE
    this.o_DIIMPDIC = this.w_DIIMPDIC
    this.o_DIIMPUTI = this.w_DIIMPUTI
    this.o_DIFLSTAM = this.w_DIFLSTAM
    this.o_ANFLSOAL = this.w_ANFLSOAL
    this.o_DIDICCOL = this.w_DIDICCOL
    this.o_DICSN = this.w_DICSN
    this.o_DIFLGCLO = this.w_DIFLGCLO
    * --- GSCG_MDU : Depends On
    this.GSCG_MDU.SaveDependsOn()
    * --- GSCG_MDF : Depends On
    this.GSCG_MDF.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgscg_adiPag1 as StdContainer
  Width  = 770
  height = 542
  stdWidth  = 770
  stdheight = 542
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oDITIPCON_1_4 as StdCombo with uid="VAUTJOSBOK",rtseq=4,rtrep=.f.,left=113,top=7,width=132,height=21;
    , ToolTipText = "Tipo lettera d'intento";
    , HelpContextID = 46426244;
    , cFormVar="w_DITIPCON",RowSource=""+"Ricevute,"+"Emesse", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDITIPCON_1_4.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oDITIPCON_1_4.GetRadio()
    this.Parent.oContained.w_DITIPCON = this.RadioValue()
    return .t.
  endfunc

  func oDITIPCON_1_4.SetRadio()
    this.Parent.oContained.w_DITIPCON=trim(this.Parent.oContained.w_DITIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_DITIPCON=='C',1,;
      iif(this.Parent.oContained.w_DITIPCON=='F',2,;
      0))
  endfunc

  func oDITIPCON_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DIFLSTAM<>'R')
    endwith
   endif
  endfunc

  func oDITIPCON_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_DICODICE)
        bRes2=.link_1_16('Full')
      endif
    endwith
    return bRes
  endfunc


  add object oDITIPIVA_1_5 as StdCombo with uid="OQYDNYOSAM",rtseq=5,rtrep=.f.,left=290,top=8,width=130,height=21;
    , tabstop=.f.;
    , ToolTipText = "Tipo IVA";
    , HelpContextID = 121345929;
    , cFormVar="w_DITIPIVA",RowSource=""+"No applicazione IVA,"+"Con IVA agevolata", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDITIPIVA_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oDITIPIVA_1_5.GetRadio()
    this.Parent.oContained.w_DITIPIVA = this.RadioValue()
    return .t.
  endfunc

  func oDITIPIVA_1_5.SetRadio()
    this.Parent.oContained.w_DITIPIVA=trim(this.Parent.oContained.w_DITIPIVA)
    this.value = ;
      iif(this.Parent.oContained.w_DITIPIVA=='S',1,;
      iif(this.Parent.oContained.w_DITIPIVA=='N',2,;
      0))
  endfunc

  func oDITIPIVA_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  add object oDINUMDIC_1_6 as StdField with uid="RZXRKUPOJY",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DINUMDIC", cQueryName = "DINUMDIC",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero della lettera di intento",;
    HelpContextID = 207615879,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=113, Top=39, cSayPict='"999999"', cGetPict='"999999"'

  func oDINUMDIC_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DIFLSTAM<>'R')
    endwith
   endif
  endfunc

  add object oDISERDIC_1_8 as StdField with uid="MQBQXHNJAN",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DISERDIC", cQueryName = "DISERDIC",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 203401095,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=197, Top=39, InputMask=replicate('X',3)

  func oDISERDIC_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DIFLSTAM<>'R')
    endwith
   endif
  endfunc

  add object oDI__ANNO_1_9 as StdField with uid="OEGRSZQEWJ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DI__ANNO", cQueryName = "DI__ANNO",nZero=4,;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno di riferimento",;
    HelpContextID = 216733829,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=250, Top=39, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oDI__ANNO_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DIFLSTAM<>'R')
    endwith
   endif
  endfunc

  add object oDIDATDIC_1_10 as StdField with uid="GQHBCHJGIW",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DIDATDIC", cQueryName = "DIDATDIC",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data della lettera d'intento",;
    HelpContextID = 201627527,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=343, Top=39

  func oDIDATDIC_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DIFLSTAM<>'R')
    endwith
   endif
  endfunc

  func oDIDATDIC_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if bRes and !(.w_DIDATLET<=.w_DIDATDIC)
         bRes=(cp_WarningMsg(thisform.msgFmt("La data della dichiarazione � maggiore della data di registrazione")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  add object oDINUMDOC_1_12 as StdField with uid="NFPBDBQCKY",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DINUMDOC", cQueryName = "DINUMDOC",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento",;
    HelpContextID = 60819577,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=478, Top=39, cSayPict='"999999"', cGetPict='"999999"'

  func oDINUMDOC_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DIFLSTAM<>'R')
    endwith
   endif
  endfunc

  add object oDISERDOC_1_13 as StdField with uid="LQQUJCNAFH",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DISERDOC", cQueryName = "DISERDOC",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 65034361,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=564, Top=39, InputMask=replicate('X',3)

  func oDISERDOC_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DIFLSTAM<>'R')
    endwith
   endif
  endfunc

  add object oDIDATLET_1_14 as StdField with uid="ENZXOHPSVB",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DIDATLET", cQueryName = "DIDATLET",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "data della lettera di intenti",;
    HelpContextID = 67409782,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=642, Top=39

  func oDIDATLET_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DIFLSTAM<>'R')
    endwith
   endif
  endfunc

  func oDIDATLET_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if bRes and !(.w_DIDATLET<=.w_DIDATDIC)
         bRes=(cp_WarningMsg(thisform.msgFmt("La data della dichiarazione � maggiore della data di registrazione")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  add object oVALESE_1_15 as StdField with uid="SYGKZCVKHJ",rtseq=15,rtrep=.f.,;
    cFormVar = "w_VALESE", cQueryName = "VALESE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice moneta di conto alla data di riferimento",;
    HelpContextID = 82829654,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=629, Top=377, InputMask=replicate('X',3), cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_VALESE"

  func oVALESE_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDICODICE_1_16 as StdField with uid="FWSMWNBUPH",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DICODICE", cQueryName = "DICODICE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto",;
    ToolTipText = "Codice cliente/fornitore",;
    HelpContextID = 134830203,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=113, Top=65, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_DITIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_DICODICE"

  func oDICODICE_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DIFLSTAM<>'R')
    endwith
   endif
  endfunc

  func oDICODICE_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oDICODICE_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDICODICE_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_DITIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_DITIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oDICODICE_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti/fornitori",'',this.parent.oContained
  endproc
  proc oDICODICE_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_DITIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_DICODICE
     i_obj.ecpSave()
  endproc

  add object oDESCON_1_17 as StdField with uid="SLXLUDFKIX",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 229528630,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=254, Top=65, InputMask=replicate('X',40)

  add object oDIDOGANA_1_18 as StdField with uid="GEPRPMGUOQ",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DIDOGANA", cQueryName = "DIDOGANA",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Dogana",;
    HelpContextID = 3762295,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=113, Top=92, InputMask=replicate('X',30)

  func oDIDOGANA_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DITIPCON='F' and empty(.w_DICODICE) And .w_DIFLSTAM<>'R')
    endwith
   endif
  endfunc

  add object oDIUFFIVA_1_19 as StdField with uid="ESRTTPUMSW",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DIUFFIVA", cQueryName = "DIUFFIVA",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Codice ufficio IVA",;
    HelpContextID = 132024201,;
   bGlobalFont=.t.,;
    Height=21, Width=212, Left=113, Top=121, InputMask=replicate('X',25)

  func oDIUFFIVA_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DIFLSTAM<>'R')
    endwith
   endif
  endfunc

  add object oDIDATAPL_1_20 as StdField with uid="DOZHDXSOYL",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DIDATAPL", cQueryName = "DIDATAPL",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data prima applicazione",;
    HelpContextID = 16476290,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=564, Top=121

  func oDIDATAPL_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Ditipcon='C' And .w_Ditipiva='S')
    endwith
   endif
  endfunc

  add object oDIUFIVFO_1_22 as StdField with uid="GYUVZXUMTB",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DIUFIVFO", cQueryName = "DIUFIVFO",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Ufficio IVA fornitore",;
    HelpContextID = 179210107,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=172, Top=185, InputMask=replicate('X',25)

  func oDIUFIVFO_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DIFLSTAM<>'R' And .w_DITIPCON = 'F')
    endwith
   endif
  endfunc

  func oDIUFIVFO_1_22.mHide()
    with this.Parent.oContained
      return (isahe())
    endwith
  endfunc

  add object oDIPROTEC_1_23 as StdField with uid="FDUSXJYMER",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DIPROTEC", cQueryName = "DIPROTEC",;
    bObbl = .f. , nPag = 1, value=space(17), bMultilanguage =  .f.,;
    ToolTipText = "Protocollo telematico - Prima parte",;
    HelpContextID = 205707143,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=172, Top=219, cSayPict='"99999999999999999"', cGetPict='"99999999999999999"', InputMask=replicate('X',17), Alignment=1

  func oDIPROTEC_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (( .w_Ditipcon='C' Or  (.cFunction='Edit'  And .w_Ditipcon='F')) And .w_Ditipiva='S')
    endwith
   endif
  endfunc

  add object oDIPRODOC_1_24 as StdField with uid="SPAPRMDSJQ",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DIPRODOC", cQueryName = "DIPRODOC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Protocollo telematico - Parte seconda",;
    HelpContextID = 62728313,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=326, Top=219, cSayPict='"999999"', cGetPict='"999999"'

  func oDIPRODOC_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (( .w_Ditipcon='C' Or  (.cFunction='Edit'  And .w_Ditipcon='F')) And .w_Ditipiva='S' And Not Empty(.w_Diprotec))
    endwith
   endif
  endfunc

  add object oDICODIVA_1_25 as StdField with uid="RFUDNKVEPD",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DICODIVA", cQueryName = "DICODIVA",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice IVA inesistente oppure obsoleto",;
    ToolTipText = "Codice IVA di esenzione",;
    HelpContextID = 133605257,;
   bGlobalFont=.t.,;
    Height=21, Width=66, Left=158, Top=261, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_DICODIVA"

  func oDICODIVA_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DIFLSTAM<>'R')
    endwith
   endif
  endfunc

  func oDICODIVA_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oDICODIVA_1_25.ecpDrop(oSource)
    this.Parent.oContained.link_1_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDICODIVA_1_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oDICODIVA_1_25'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Codici IVA esenti",'GSCG0ADI.VOCIIVA_VZM',this.parent.oContained
  endproc
  proc oDICODIVA_1_25.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_DICODIVA
     i_obj.ecpSave()
  endproc

  add object oDIARTESE_1_26 as StdField with uid="XULYSHJYPO",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DIARTESE", cQueryName = "DIARTESE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Articolo esenzione del D.P.R. 633/72",;
    HelpContextID = 84686971,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=158, Top=295, cSayPict='"XXXXX"', cGetPict='"XXXXX"', InputMask=replicate('X',5)

  func oDIARTESE_1_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DIFLSTAM<>'R')
    endwith
   endif
  endfunc

  add object oDIDESOPE_1_27 as StdMemo with uid="MYKOFKWJGG",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DIDESOPE", cQueryName = "DIDESOPE",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione operazione",;
    HelpContextID = 250570875,;
   bGlobalFont=.t.,;
    Height=38, Width=506, Left=158, Top=328

  func oDIDESOPE_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DIFLSTAM<>'R')
    endwith
   endif
  endfunc

  add object oDESIVA_1_28 as StdField with uid="JGMSWKFXNG",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DESIVA", cQueryName = "DESIVA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 19158070,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=228, Top=261, InputMask=replicate('X',35)

  add object oDITIPOPE_1_29 as StdRadio with uid="ZMFGYDDIOQ",rtseq=29,rtrep=.f.,left=158, top=377, width=152,height=47;
    , ToolTipText = "Tipo di operazione";
    , cFormVar="w_DITIPOPE", ButtonCount=3, bObbl=.t., nPag=1;
  , bGlobalFont=.t.

    proc oDITIPOPE_1_29.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Importo definito"
      this.Buttons(1).HelpContextID = 247752827
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Operazione specifica"
      this.Buttons(2).HelpContextID = 247752827
      this.Buttons(2).Top=15
      this.Buttons(3).Caption="Definizione periodo"
      this.Buttons(3).HelpContextID = 247752827
      this.Buttons(3).Top=30
      this.SetAll("Width",150)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Tipo di operazione")
      StdRadio::init()
    endproc

  func oDITIPOPE_1_29.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'O',;
    iif(this.value =3,'D',;
    space(1)))))
  endfunc
  func oDITIPOPE_1_29.GetRadio()
    this.Parent.oContained.w_DITIPOPE = this.RadioValue()
    return .t.
  endfunc

  func oDITIPOPE_1_29.SetRadio()
    this.Parent.oContained.w_DITIPOPE=trim(this.Parent.oContained.w_DITIPOPE)
    this.value = ;
      iif(this.Parent.oContained.w_DITIPOPE=='I',1,;
      iif(this.Parent.oContained.w_DITIPOPE=='O',2,;
      iif(this.Parent.oContained.w_DITIPOPE=='D',3,;
      0)))
  endfunc

  func oDITIPOPE_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oDITIPOPE_1_29.mHide()
    with this.Parent.oContained
      return (.w_DITIPOPE $ 'IO')
    endwith
  endfunc

  add object oDITIPOPE_1_30 as StdRadio with uid="KEVSQTEUPS",rtseq=30,rtrep=.f.,left=158, top=377, width=152,height=36;
    , ToolTipText = "Tipo di operazione";
    , cFormVar="w_DITIPOPE", ButtonCount=2, bObbl=.t., nPag=1;
  , bGlobalFont=.t.

    proc oDITIPOPE_1_30.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Importo definito"
      this.Buttons(1).HelpContextID = 247752827
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Operazione specifica"
      this.Buttons(2).HelpContextID = 247752827
      this.Buttons(2).Top=17
      this.SetAll("Width",150)
      this.SetAll("Height",19)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Tipo di operazione")
      StdRadio::init()
    endproc

  func oDITIPOPE_1_30.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'O',;
    space(1))))
  endfunc
  func oDITIPOPE_1_30.GetRadio()
    this.Parent.oContained.w_DITIPOPE = this.RadioValue()
    return .t.
  endfunc

  func oDITIPOPE_1_30.SetRadio()
    this.Parent.oContained.w_DITIPOPE=trim(this.Parent.oContained.w_DITIPOPE)
    this.value = ;
      iif(this.Parent.oContained.w_DITIPOPE=='I',1,;
      iif(this.Parent.oContained.w_DITIPOPE=='O',2,;
      0))
  endfunc

  func oDITIPOPE_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oDITIPOPE_1_30.mHide()
    with this.Parent.oContained
      return (NOT .w_DITIPOPE $ 'IO')
    endwith
  endfunc

  add object oDIIMPDIC_1_31 as StdField with uid="JDPXLVNBDM",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DIIMPDIC", cQueryName = "DIIMPDIC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale importo definito",;
    HelpContextID = 205014919,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=448, Top=377, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  func oDIIMPDIC_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_DITIPOPE='I' OR .w_DITIPOPE='O') And .w_DIFLSTAM<>'R')
    endwith
   endif
  endfunc

  add object oDIIMPUTI_1_32 as StdField with uid="SVGVNNEVKU",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DIIMPUTI", cQueryName = "DIIMPUTI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale importo utilizzato",;
    HelpContextID = 188237697,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=448, Top=407, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  add object oDIDATINI_1_33 as StdField with uid="RCMEPDFZIJ",rtseq=33,rtrep=.f.,;
    cFormVar = "w_DIDATINI", cQueryName = "DIDATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio validit�",;
    HelpContextID = 150694015,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=158, Top=515

  func oDIDATINI_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DIFLSTAM<>'R')
    endwith
   endif
  endfunc

  func oDIDATINI_1_33.mHide()
    with this.Parent.oContained
      return (.w_DITIPOPE='D')
    endwith
  endfunc

  add object oDIDATFIN_1_34 as StdField with uid="WVENNDFVWT",rtseq=34,rtrep=.f.,;
    cFormVar = "w_DIDATFIN", cQueryName = "DIDATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di fine validit�",;
    HelpContextID = 168073084,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=350, Top=515

  func oDIDATFIN_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DIFLSTAM<>'R')
    endwith
   endif
  endfunc

  func oDIDATFIN_1_34.mHide()
    with this.Parent.oContained
      return (.w_DITIPOPE='D')
    endwith
  endfunc

  add object oDIDATREV_1_35 as StdField with uid="DLBBIRGCWY",rtseq=35,rtrep=.f.,;
    cFormVar = "w_DIDATREV", cQueryName = "DIDATREV",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data di revoca deve essere maggiore o uguale alla data d'inizio validit� e minore o uguale alla data di fine validit�",;
    ToolTipText = "Data di revoca della dichiarazione d'intento",;
    HelpContextID = 235181940,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=685, Top=515

  func oDIDATREV_1_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_DIDATREV) OR (.w_DIDATREV>=.w_DIDATINI AND .w_DIDATREV<=.w_DIDATFIN))
    endwith
    return bRes
  endfunc

  func oDIDATREV_1_35.CondObbl()
    local i_bRes
    i_bRes = .t.
    with this.Parent.oContained
      i_bres=.w_DIFLGCLO='S'
    endwith
    return i_bres
  endfunc


  add object oObj_1_48 as cp_runprogram with uid="MNNKQNCQKX",left=-3, top=555, width=285,height=19,;
    caption='GSCG_BLI(1)',;
   bGlobalFont=.t.,;
    prg='GSCG_BLI(1)',;
    cEvent = "Record Inserted,Record Updated",;
    nPag=1;
    , ToolTipText = "Lancia la procedura di stampa";
    , HelpContextID = 45360687


  add object oObj_1_49 as cp_runprogram with uid="JPZMSOXKOG",left=-3, top=576, width=285,height=19,;
    caption='GSCG_BLI(0)',;
   bGlobalFont=.t.,;
    prg="GSCG_BLI(0)",;
    cEvent = "Check",;
    nPag=1;
    , ToolTipText = "Controllo data stampa registri";
    , HelpContextID = 45360431


  add object oObj_1_52 as cp_calclbl with uid="FTJWDDIHCA",left=12, top=65, width=93,height=15,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",alignment=1,;
    nPag=1;
    , HelpContextID = 85058022


  add object oDIFLSTAM_1_56 as StdCombo with uid="POSEZZUUIY",rtseq=38,rtrep=.f.,left=113,top=151,width=152,height=21, enabled=.f.;
    , HelpContextID = 66488451;
    , cFormVar="w_DIFLSTAM",RowSource=""+"Non stampata,"+"Stampata,"+"Stampata su registro", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDIFLSTAM_1_56.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'R',;
    space(1)))))
  endfunc
  func oDIFLSTAM_1_56.GetRadio()
    this.Parent.oContained.w_DIFLSTAM = this.RadioValue()
    return .t.
  endfunc

  func oDIFLSTAM_1_56.SetRadio()
    this.Parent.oContained.w_DIFLSTAM=trim(this.Parent.oContained.w_DIFLSTAM)
    this.value = ;
      iif(this.Parent.oContained.w_DIFLSTAM=='N',1,;
      iif(this.Parent.oContained.w_DIFLSTAM=='S',2,;
      iif(this.Parent.oContained.w_DIFLSTAM=='R',3,;
      0)))
  endfunc


  add object oBtn_1_58 as StdButton with uid="PENEOLGORI",left=619, top=150, width=48,height=45,;
    CpPicture="bmp\DrillD.bmp", caption="", nPag=1,tabstop=.f.;
    , ToolTipText = "Premere per accedere alla comunicazione delle dichiarazioni di intento";
    , HelpContextID = 92939530;
    , caption='\<Comunic.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_58.Click()
      with this.Parent.oContained
        GSCG_BLE(this.Parent.oContained,11)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_58.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ( .cFunction<>'Load')
      endwith
    endif
  endfunc

  func oBtn_1_58.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY (.w_SERIALE))
     endwith
    endif
  endfunc


  add object oObj_1_67 as cp_runprogram with uid="QFHQVXEWOV",left=-3, top=597, width=285,height=19,;
    caption='GSCG_BLI(5)',;
   bGlobalFont=.t.,;
    prg="GSCG_BLI(5)",;
    cEvent = "HasEvent",;
    nPag=1;
    , ToolTipText = "Controllo data stampa registri";
    , HelpContextID = 45361711


  add object oObj_1_70 as cp_runprogram with uid="NJBTLGCBDY",left=-3, top=618, width=285,height=19,;
    caption='GSCG_BLI(6)',;
   bGlobalFont=.t.,;
    prg="GSCG_BLI(6)",;
    cEvent = "w_DINUMDOC Changed,w_DITIPCON Changed",;
    nPag=1;
    , HelpContextID = 45361967

  add object oDIDATINI_1_79 as StdField with uid="MLJIOQQECJ",rtseq=48,rtrep=.f.,;
    cFormVar = "w_DIDATINI", cQueryName = "DIDATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio validit�",;
    HelpContextID = 150694015,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=158, Top=515

  func oDIDATINI_1_79.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DIFLSTAM<>'R')
    endwith
   endif
  endfunc

  func oDIDATINI_1_79.mHide()
    with this.Parent.oContained
      return (.w_DITIPOPE<>'D')
    endwith
  endfunc

  func oDIDATINI_1_79.CondObbl()
    local i_bRes
    i_bRes = .t.
    with this.Parent.oContained
      i_bres=.w_DITIPOPE='D'
    endwith
    return i_bres
  endfunc

  add object oDIDATFIN_1_80 as StdField with uid="VOBVKECIHY",rtseq=49,rtrep=.f.,;
    cFormVar = "w_DIDATFIN", cQueryName = "DIDATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di fine validit�",;
    HelpContextID = 168073084,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=350, Top=515

  func oDIDATFIN_1_80.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DIFLSTAM<>'R')
    endwith
   endif
  endfunc

  func oDIDATFIN_1_80.mHide()
    with this.Parent.oContained
      return (.w_DITIPOPE<>'D')
    endwith
  endfunc

  func oDIDATFIN_1_80.CondObbl()
    local i_bRes
    i_bRes = .t.
    with this.Parent.oContained
      i_bres=.w_DITIPOPE='D' 
    endwith
    return i_bres
  endfunc


  add object oObj_1_84 as cp_runprogram with uid="TMWYMFAFHR",left=-3, top=639, width=285,height=19,;
    caption='GSCG_BLE(10)',;
   bGlobalFont=.t.,;
    prg="GSCG_BLE(10)",;
    cEvent = "Load",;
    nPag=1;
    , ToolTipText = "Lancia la procedura di stampa";
    , HelpContextID = 48076331


  add object oObj_1_86 as cp_runprogram with uid="OOXLNAVSFN",left=-3, top=660, width=285,height=19,;
    caption='GSCG_BLE(12)',;
   bGlobalFont=.t.,;
    prg="GSCG_BLE(12)",;
    cEvent = "Delete end",;
    nPag=1;
    , ToolTipText = "Lancia la procedura di stampa";
    , HelpContextID = 48084523

  add object oDIFLGDOG_1_87 as StdCheck with uid="FPJCFMIRSE",rtseq=50,rtrep=.f.,left=522, top=74, caption="Soggetto terzo",;
    ToolTipText = "Se attivo, il fornitore sar� considerato come soggetto terzo (Dogana)",;
    HelpContextID = 53905533,;
    cFormVar="w_DIFLGDOG", bObbl = .f. , nPag = 1;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oDIFLGDOG_1_87.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDIFLGDOG_1_87.GetRadio()
    this.Parent.oContained.w_DIFLGDOG = this.RadioValue()
    return .t.
  endfunc

  func oDIFLGDOG_1_87.SetRadio()
    this.Parent.oContained.w_DIFLGDOG=trim(this.Parent.oContained.w_DIFLGDOG)
    this.value = ;
      iif(this.Parent.oContained.w_DIFLGDOG=='S',1,;
      0)
  endfunc

  func oDIFLGDOG_1_87.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty(.w_DICODICE) And .w_DIFLSTAM<>'R')
    endwith
   endif
  endfunc

  func oDIFLGDOG_1_87.mHide()
    with this.Parent.oContained
      return (.w_DITIPCON='C')
    endwith
  endfunc


  add object oBtn_1_92 as StdButton with uid="PZDTGDEQJR",left=715, top=452, width=48,height=45,;
    CpPicture="bmp\wf_nuovo.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere allo zoom delle dichiarazioni di intento da collegare";
    , HelpContextID = 92939530;
    , tabstop=.f.,caption='\<Dic. Col.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_92.Click()
      do GSCG_KDL with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_92.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ( .cFunction='Edit' )
      endwith
    endif
  endfunc

  func oBtn_1_92.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_Ditipope='D' Or .w_Di__Anno<'2017' Or Empty(.w_Dicodice) Or Not Empty(.w_Didiccol) Or Not Empty(.w_Didatrev))
     endwith
    endif
  endfunc

  add object oNUMDIC_COLL_1_94 as StdField with uid="KGUPCIWMIY",rtseq=65,rtrep=.f.,;
    cFormVar = "w_NUMDIC_COLL", cQueryName = "NUMDIC_COLL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 39065097,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=436, Top=475

  func oNUMDIC_COLL_1_94.mHide()
    with this.Parent.oContained
      return (Empty(.w_DIDICCOL) Or .w_DITIPCON='C')
    endwith
  endfunc

  add object oANNDIC_COLL_1_95 as StdField with uid="IXSRIKSQAS",rtseq=66,rtrep=.f.,;
    cFormVar = "w_ANNDIC_COLL", cQueryName = "ANNDIC_COLL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 39067193,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=555, Top=475, InputMask=replicate('X',4)

  func oANNDIC_COLL_1_95.mHide()
    with this.Parent.oContained
      return (Empty(.w_DIDICCOL))
    endwith
  endfunc

  add object oDATDIC_COLL_1_96 as StdField with uid="HCGMVBILXG",rtseq=67,rtrep=.f.,;
    cFormVar = "w_DATDIC_COLL", cQueryName = "DATDIC_COLL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 39088489,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=633, Top=475

  func oDATDIC_COLL_1_96.mHide()
    with this.Parent.oContained
      return (Empty(.w_DIDICCOL))
    endwith
  endfunc

  add object oSERDIC_COLL_1_97 as StdField with uid="ESNUJASEOJ",rtseq=68,rtrep=.f.,;
    cFormVar = "w_SERDIC_COLL", cQueryName = "SERDIC_COLL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 39081561,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=510, Top=475, InputMask=replicate('X',3)

  func oSERDIC_COLL_1_97.mHide()
    with this.Parent.oContained
      return (Empty(.w_DIDICCOL) Or .w_DITIPCON='C')
    endwith
  endfunc


  add object oLinkPC_1_101 as stdDynamicChildContainer with uid="IRVBGISMHA",left=0, top=481, width=43, height=42, bOnScreen=.t.;
    , tabstop=.f.

  func oLinkPC_1_101.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (1=1)
     endwith
    endif
  endfunc

  add object oDICSN_1_103 as StdCheck with uid="ASUBAALKAD",rtseq=70,rtrep=.f.,left=227, top=475, caption="Dichiarazione di intento collegata:",;
    ToolTipText = "Dichiarazione di intento collegata utilizzata in mancanza di capienza importo disponibile",;
    HelpContextID = 5416906,;
    cFormVar="w_DICSN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDICSN_1_103.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDICSN_1_103.GetRadio()
    this.Parent.oContained.w_DICSN = this.RadioValue()
    return .t.
  endfunc

  func oDICSN_1_103.SetRadio()
    this.Parent.oContained.w_DICSN=trim(this.Parent.oContained.w_DICSN)
    this.value = ;
      iif(this.Parent.oContained.w_DICSN=='S',1,;
      0)
  endfunc

  func oDICSN_1_103.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (! .w_DIC_COLL_UTI And Not Empty(.w_DIDICCOL))
    endwith
   endif
  endfunc

  func oDICSN_1_103.mHide()
    with this.Parent.oContained
      return (Empty(.w_DIDICCOL))
    endwith
  endfunc


  add object oBtn_1_105 as StdButton with uid="LSKPOSFTDP",left=715, top=452, width=48,height=45,;
    CpPicture="bmp\INTENTI.bmp", caption="", nPag=1,tabstop=.f.;
    , ToolTipText = "Premere per accedere allo dichiarazione di intento collegata";
    , HelpContextID = 92939530;
    , caption='\<Dic. Col.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_105.Click()
      with this.Parent.oContained
        GSCG_BDN(this.Parent.oContained,"Dich.Intento")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_105.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (Empty(.w_DIDICCOL) )
     endwith
    endif
  endfunc


  add object oBtn_1_107 as StdButton with uid="GHOHCARKMI",left=711, top=261, width=48,height=45,;
    CpPicture="BMP\RISCHIO.BMP", caption="", nPag=1,tabstop=.f.;
    , ToolTipText = "Premere per ricostruire l'importo utilizzato";
    , HelpContextID = 204580026;
    , Caption='\<Elabora';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_107.Click()
      with this.Parent.oContained
        Gscg_Bdn(this.Parent.oContained,"Ricos")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_107.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction='Edit' And .w_DIFLGCLO<>'S')
      endwith
    endif
  endfunc

  func oBtn_1_107.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (Empty(.w_Diserial) Or .w_Ditipope='D' Or .w_Di__Anno<'2017' Or Empty(.w_Dicodice) Or Not Empty(.w_Didatrev))
     endwith
    endif
  endfunc

  add object oDIIMPRES_1_108 as StdField with uid="VEFXFXIFAX",rtseq=73,rtrep=.f.,;
    cFormVar = "w_DIIMPRES", cQueryName = "DIIMPRES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 238569335,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=448, Top=437, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  add object oDIFLGCLO_1_110 as StdCheck with uid="LKMGCXPUHI",rtseq=76,rtrep=.f.,left=439, top=516, caption="Chiusura dichiarazione",;
    HelpContextID = 37128325,;
    cFormVar="w_DIFLGCLO", bObbl = .f. , nPag = 1;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oDIFLGCLO_1_110.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDIFLGCLO_1_110.GetRadio()
    this.Parent.oContained.w_DIFLGCLO = this.RadioValue()
    return .t.
  endfunc

  func oDIFLGCLO_1_110.SetRadio()
    this.Parent.oContained.w_DIFLGCLO=trim(this.Parent.oContained.w_DIFLGCLO)
    this.value = ;
      iif(this.Parent.oContained.w_DIFLGCLO=='S',1,;
      0)
  endfunc

  func oDIFLGCLO_1_110.mHide()
    with this.Parent.oContained
      return (Not Empty(.w_DIDATREV) And .w_DIFLGCLO<>'S' )
    endwith
  endfunc

  add object oNUMDOC_COLL_1_113 as StdField with uid="HUPTWJPGCH",rtseq=77,rtrep=.f.,;
    cFormVar = "w_NUMDOC_COLL", cQueryName = "NUMDOC_COLL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 45356553,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=436, Top=475

  func oNUMDOC_COLL_1_113.mHide()
    with this.Parent.oContained
      return (Empty(.w_DIDICCOL) Or .w_DITIPCON='F')
    endwith
  endfunc

  add object oSERDOC_COLL_1_114 as StdField with uid="XINAIAADIA",rtseq=78,rtrep=.f.,;
    cFormVar = "w_SERDOC_COLL", cQueryName = "SERDOC_COLL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 45373017,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=510, Top=475, InputMask=replicate('X',3)

  func oSERDOC_COLL_1_114.mHide()
    with this.Parent.oContained
      return (Empty(.w_DIDICCOL) Or .w_DITIPCON='F')
    endwith
  endfunc


  add object oBtn_1_116 as StdButton with uid="NLYYQVKUEY",left=711, top=372, width=48,height=45,;
    CpPicture="BMP\CARATTER.BMP", caption="", nPag=1,tabstop=.f.;
    , ToolTipText = "Premere per inserire dati integrativi";
    , HelpContextID = 212264310;
    , Caption='\<Integrat.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_116.Click()
      with this.Parent.oContained
        Gscg_Bdn(this.Parent.oContained,"Integra")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_116.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction='Edit' And .w_DIFLGCLO<>'S'  And Not Empty(.w_Diprotec) And Not Empty(.w_Diprodoc))
      endwith
    endif
  endfunc

  func oBtn_1_116.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (Empty(.w_Diserial) Or .w_Ditipope='D' Or .w_Di__Anno<'2017' Or Empty(.w_Dicodice) Or Not Empty(.w_Didatrev) Or .w_Ditipiva='N')
     endwith
    endif
  endfunc


  add object oLinkPC_1_117 as stdDynamicChildContainer with uid="TWXBTXKRAM",left=0, top=436, width=43, height=42, bOnScreen=.t.;
    , tabstop=.f.

  func oLinkPC_1_117.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (1=1)
     endwith
    endif
  endfunc


  add object oBtn_1_118 as StdButton with uid="PKKSUWHQWC",left=711, top=150, width=48,height=45,;
    CpPicture="bmp\INTENTI.bmp", caption="", nPag=1,tabstop=.f.;
    , ToolTipText = "Premere per accedere al visualizza utilizzo dichiarazioni di intento";
    , HelpContextID = 92939530;
    , caption='\<Dic. Int.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_118.Click()
      do GSCG_KDT with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_118.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ( .cFunction<>'Load')
      endwith
    endif
  endfunc

  add object oStr_1_36 as StdString with uid="RRTBJDXHLU",Visible=.t., Left=63, Top=261,;
    Alignment=1, Width=88, Height=15,;
    Caption="Codice IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="SWXMIVAVZO",Visible=.t., Left=12, Top=39,;
    Alignment=1, Width=94, Height=15,;
    Caption="Protocollo n.:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_38 as StdString with uid="ZXQTNSBVSP",Visible=.t., Left=307, Top=39,;
    Alignment=1, Width=31, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="WVABMDYUXG",Visible=.t., Left=55, Top=517,;
    Alignment=1, Width=96, Height=15,;
    Caption="Inizio validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="HGTCQHKOOZ",Visible=.t., Left=244, Top=517,;
    Alignment=1, Width=101, Height=15,;
    Caption="Fine validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="HTHEVWIXYK",Visible=.t., Left=323, Top=410,;
    Alignment=1, Width=120, Height=15,;
    Caption="Importo utilizzato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="RIGGZCVYVH",Visible=.t., Left=37, Top=377,;
    Alignment=1, Width=114, Height=15,;
    Caption="Tipo operazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="QRMYXXAJPG",Visible=.t., Left=182, Top=39,;
    Alignment=2, Width=12, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="MBQKRVRPNC",Visible=.t., Left=21, Top=121,;
    Alignment=1, Width=85, Height=15,;
    Caption="Ufficio IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="TBAFAUJLSU",Visible=.t., Left=42, Top=295,;
    Alignment=1, Width=109, Height=15,;
    Caption="Art. esenzione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="ZGUMIITPYH",Visible=.t., Left=32, Top=92,;
    Alignment=1, Width=74, Height=15,;
    Caption="Dogana:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="UVEYVZYOYX",Visible=.t., Left=75, Top=8,;
    Alignment=1, Width=31, Height=15,;
    Caption="Tipo:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_53 as StdString with uid="LMSPUVIIOU",Visible=.t., Left=17, Top=328,;
    Alignment=1, Width=134, Height=15,;
    Caption="Descr. operazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="FPFKUFRPKY",Visible=.t., Left=250, Top=8,;
    Alignment=1, Width=35, Height=15,;
    Caption="IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_59 as StdString with uid="LTBAGGCLDV",Visible=.t., Left=62, Top=153,;
    Alignment=1, Width=44, Height=15,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_62 as StdString with uid="ZDDZEDVQRE",Visible=.t., Left=605, Top=377,;
    Alignment=1, Width=19, Height=15,;
    Caption="in:"  ;
  , bGlobalFont=.t.

  add object oStr_1_63 as StdString with uid="AZJUXADCIN",Visible=.t., Left=395, Top=380,;
    Alignment=1, Width=48, Height=15,;
    Caption="Importo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_64 as StdString with uid="NSYJWZRPDL",Visible=.t., Left=425, Top=39,;
    Alignment=1, Width=50, Height=15,;
    Caption="N. doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_65 as StdString with uid="WACZTIRPVV",Visible=.t., Left=608, Top=39,;
    Alignment=1, Width=30, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_71 as StdString with uid="EAPKOTUKVC",Visible=.t., Left=586, Top=521,;
    Alignment=1, Width=93, Height=15,;
    Caption="Revocata il:"  ;
  , bGlobalFont=.t.

  func oStr_1_71.mHide()
    with this.Parent.oContained
      return (.w_DIFLGCLO='S')
    endwith
  endfunc

  add object oStr_1_72 as StdString with uid="XEXZUPWEJX",Visible=.t., Left=234, Top=39,;
    Alignment=2, Width=12, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_73 as StdString with uid="CBCWPXFQNI",Visible=.t., Left=547, Top=39,;
    Alignment=2, Width=12, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_77 as StdString with uid="TWCNUEZSAT",Visible=.t., Left=14, Top=187,;
    Alignment=1, Width=153, Height=18,;
    Caption="Uff. IVA fornitore:"  ;
  , bGlobalFont=.t.

  func oStr_1_77.mHide()
    with this.Parent.oContained
      return (isahe())
    endwith
  endfunc

  add object oStr_1_81 as StdString with uid="CCEMHTKDAP",Visible=.t., Left=397, Top=121,;
    Alignment=1, Width=163, Height=18,;
    Caption="Data prima applicazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_82 as StdString with uid="OMWBJMFOMH",Visible=.t., Left=36, Top=221,;
    Alignment=1, Width=131, Height=18,;
    Caption="Protocollo telematico:"  ;
  , bGlobalFont=.t.

  add object oStr_1_83 as StdString with uid="DAOKKOAWNW",Visible=.t., Left=315, Top=220,;
    Alignment=0, Width=13, Height=18,;
    Caption="-"  ;
  , bGlobalFont=.t.

  add object oStr_1_85 as StdString with uid="XPLRXVSRAF",Visible=.t., Left=414, Top=162,;
    Alignment=1, Width=199, Height=18,;
    Caption="Dichiarazione d'intento comunicata"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_85.mHide()
    with this.Parent.oContained
      return (EMPTY (.w_SERIALE))
    endwith
  endfunc

  add object oStr_1_98 as StdString with uid="SWGKZDMMXN",Visible=.t., Left=499, Top=478,;
    Alignment=0, Width=10, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_98.mHide()
    with this.Parent.oContained
      return (Empty(.w_DIDICCOL))
    endwith
  endfunc

  add object oStr_1_99 as StdString with uid="WRTBXCHVYH",Visible=.t., Left=547, Top=478,;
    Alignment=0, Width=10, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_99.mHide()
    with this.Parent.oContained
      return (Empty(.w_DIDICCOL))
    endwith
  endfunc

  add object oStr_1_100 as StdString with uid="DYAVGHIQOG",Visible=.t., Left=599, Top=478,;
    Alignment=1, Width=31, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  func oStr_1_100.mHide()
    with this.Parent.oContained
      return (Empty(.w_DIDICCOL))
    endwith
  endfunc

  add object oStr_1_106 as StdString with uid="CAJRDTJWFC",Visible=.t., Left=331, Top=439,;
    Alignment=1, Width=112, Height=18,;
    Caption="Importo residuo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_111 as StdString with uid="PNYSMYCGRD",Visible=.t., Left=594, Top=521,;
    Alignment=1, Width=85, Height=15,;
    Caption="Chiusa il:"  ;
  , bGlobalFont=.t.

  func oStr_1_111.mHide()
    with this.Parent.oContained
      return (.w_DIFLGCLO<>'S')
    endwith
  endfunc

  add object oBox_1_42 as StdBox with uid="PJWWHFBHEM",left=19, top=253, width=700,height=1
enddefine
define class tgscg_adiPag2 as StdContainer
  Width  = 770
  height = 542
  stdWidth  = 770
  stdheight = 542
  resizeXpos=414
  resizeYpos=251
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oNUMDIC_2_1 as StdField with uid="WJRUXYZUJV",rtseq=55,rtrep=.f.,;
    cFormVar = "w_NUMDIC", cQueryName = "NUMDIC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 38733014,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=104, Top=13, cSayPict='"999999"', cGetPict='"999999"'

  add object oSERDIC_2_2 as StdField with uid="IYDXQYGVWT",rtseq=56,rtrep=.f.,;
    cFormVar = "w_SERDIC", cQueryName = "SERDIC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 38749478,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=188, Top=13, InputMask=replicate('X',3)

  add object oANNO_2_3 as StdField with uid="KMNLDCOJZC",rtseq=57,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 87421690,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=241, Top=13, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  add object oDATDIC_2_11 as StdField with uid="QBRJVCWOID",rtseq=58,rtrep=.f.,;
    cFormVar = "w_DATDIC", cQueryName = "DATDIC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 38756406,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=334, Top=13

  add object oNUMDOC_2_12 as StdField with uid="RVFRDECZXC",rtseq=59,rtrep=.f.,;
    cFormVar = "w_NUMDOC", cQueryName = "NUMDOC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 45024470,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=466, Top=13

  add object oSERDOC_2_13 as StdField with uid="PRTIPMZUFA",rtseq=60,rtrep=.f.,;
    cFormVar = "w_SERDOC", cQueryName = "SERDOC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 45040934,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=555, Top=13, InputMask=replicate('X',3)

  add object oDATLET_2_14 as StdField with uid="VNGHIKLJLU",rtseq=61,rtrep=.f.,;
    cFormVar = "w_DATLET", cQueryName = "DATLET",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 51863606,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=635, Top=13


  add object Elabora_Dic_Inte as cp_zoombox with uid="IMOFZJPOGI",left=-1, top=53, width=766,height=395,;
    caption='Elabora_Dic_Inte',;
   bGlobalFont=.t.,;
    cZoomOnZoom="",cZoomFile="GSCG_ADI",bOptions=.t.,bAdvOptions=.t.,bQueryOnDblClick=.f.,cTable="DICDINTE",bReadOnly=.t.,cMenuFile="GSCG_ADI",bNoZoomGridShape=.f.,bRetriveAllRows=.f.,bQueryOnLoad=.t.,;
    cEvent = "Elabora_Zoom",;
    nPag=2;
    , HelpContextID = 113506634

  add object oIMPDIC_2_23 as StdField with uid="VGEXHPWCVC",rtseq=71,rtrep=.f.,;
    cFormVar = "w_IMPDIC", cQueryName = "IMPDIC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 38743174,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=264, Top=454, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  add object oIMPUTI_2_24 as StdField with uid="POJNMGPMPM",rtseq=72,rtrep=.f.,;
    cFormVar = "w_IMPUTI", cQueryName = "IMPUTI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 152054918,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=264, Top=483, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  add object oIMPRES_2_26 as StdField with uid="LKXJNJXBFN",rtseq=74,rtrep=.f.,;
    cFormVar = "w_IMPRES", cQueryName = "IMPRES",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 35466374,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=264, Top=512, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"


  add object oObj_2_29 as cp_showimage with uid="KAMQIHHXEX",left=25, top=37, width=16,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    stretch=1,default="bmp\rosso.ico",;
    nPag=2;
    , HelpContextID = 85058022


  add object oBtn_2_31 as StdButton with uid="ELNSICJVGH",left=713, top=460, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per lanciare la stampa degli utilizzi";
    , HelpContextID = 48849958;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_31.Click()
      with this.Parent.oContained
        Gscg_Bdn(this.Parent.oContained,"StampaUti")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_31.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction<>'Load')
      endwith
    endif
  endfunc

  func oBtn_2_31.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.cFunction='Load')
     endwith
    endif
  endfunc


  add object oBtn_2_32 as StdButton with uid="MFSAIBYYJM",left=661, top=460, width=48,height=45,;
    CpPicture="BMP\Stampa_c.ico", caption="", nPag=2;
    , ToolTipText = "Premere per lanciare la stampa dello storico utilizzi";
    , HelpContextID = 48849958;
    , Caption='St\<orico';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_32.Click()
      with this.Parent.oContained
        Gscg_Bdn(this.Parent.oContained,"StampaDett")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_32.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction<>'Load' And ! .w_Zoom_Dett)
      endwith
    endif
  endfunc

  func oBtn_2_32.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.cFunction='Load' )
     endwith
    endif
  endfunc

  add object oStr_2_4 as StdString with uid="PWVABBTHXI",Visible=.t., Left=3, Top=13,;
    Alignment=1, Width=94, Height=15,;
    Caption="Protocollo n.:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_5 as StdString with uid="FJDZRQYFYJ",Visible=.t., Left=298, Top=13,;
    Alignment=1, Width=31, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_2_6 as StdString with uid="FRCKKXKVSL",Visible=.t., Left=176, Top=13,;
    Alignment=2, Width=12, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_2_7 as StdString with uid="YLMEEVKQNP",Visible=.t., Left=416, Top=13,;
    Alignment=1, Width=50, Height=15,;
    Caption="N. doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_8 as StdString with uid="ELQYYSBNXU",Visible=.t., Left=599, Top=13,;
    Alignment=1, Width=30, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_2_9 as StdString with uid="LWHKLOYZEE",Visible=.t., Left=225, Top=13,;
    Alignment=2, Width=12, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_2_10 as StdString with uid="DOUSPUJCRC",Visible=.t., Left=542, Top=13,;
    Alignment=2, Width=12, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_2_21 as StdString with uid="USQNMGGWNG",Visible=.t., Left=139, Top=487,;
    Alignment=1, Width=120, Height=15,;
    Caption="Importo utilizzato:"  ;
  , bGlobalFont=.t.

  add object oStr_2_22 as StdString with uid="ROUAOATORB",Visible=.t., Left=211, Top=456,;
    Alignment=1, Width=48, Height=15,;
    Caption="Importo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_25 as StdString with uid="ZAEFUTPDEO",Visible=.t., Left=147, Top=515,;
    Alignment=1, Width=112, Height=18,;
    Caption="Importo residuo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_28 as StdString with uid="SINLCHERCY",Visible=.t., Left=339, Top=40,;
    Alignment=0, Width=235, Height=18,;
    Caption="Utilizzo dichiarazione di intento collegata"    , BackStyle=1, BackColor=RGB(255,255,0);
  ;
  , bGlobalFont=.t.

  add object oStr_2_30 as StdString with uid="UUBCCWHFWI",Visible=.t., Left=46, Top=40,;
    Alignment=0, Width=284, Height=18,;
    Caption="Causale priva di aggiornamento lettere di intento"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_adi','DIC_INTE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DISERIAL=DIC_INTE.DISERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
