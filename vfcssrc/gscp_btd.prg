* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscp_btd                                                        *
*              Tracciabilità documenti per AHI                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-07-12                                                      *
* Last revis.: 2012-02-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscp_btd",oParentObject)
return(i_retval)

define class tgscp_btd as StdBatch
  * --- Local variables
  w_PADRES = space(50)
  w_RIGAS = 0
  w_ROWORIG = 0
  w_QTYORIG = 0
  w_EVORIG = 0
  w_QTYORIG1 = 0
  w_EVORIG1 = 0
  w_SERIALE = space(10)
  w_ROWRIF = 0
  w_RIGA = 0
  w_SERRIF = space(10)
  w_CICLO = space(1)
  w_TERMINAD = .f.
  w_SERORIG = space(10)
  w_SERWB = space(50)
  w_DATAREG = ctod("  /  /  ")
  w_LIVELLOD = 0
  w_NUMCAUVIS = 0
  w_CODAZI = space(5)
  w_RIGAEVASA = space(1)
  w_VALUTA = space(3)
  w_CODCAU = space(5)
  w_CODRIC = space(20)
  w_TIPART = space(2)
  w_QTAEVAS = 0
  w_NONEVASO = space(1)
  * --- WorkFile variables
  TMPNODI_idx=0
  DOC_MAST_idx=0
  TMPDOCU_idx=0
  TRACCIADOC_idx=0
  CAUVIAHI_idx=0
  TMPEVARIGORD_idx=0
  TAB_TRAC_DOC_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Costruzione tabella temporanea per tracciabilità ordini 'Invio web'
    this.w_CICLO = "V"
    this.w_TERMINAD = .F.
    this.w_CODAZI = i_CODAZI
    * --- Select from CAUVIAHI
    i_nConn=i_TableProp[this.CAUVIAHI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAUVIAHI_idx,2],.t.,this.CAUVIAHI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select count(*) as contatore  from "+i_cTable+" CAUVIAHI ";
          +" where TDCODAZI = "+cp_ToStrODBC(this.w_CODAZI)+"";
           ,"_Curs_CAUVIAHI")
    else
      select count(*) as contatore from (i_cTable);
       where TDCODAZI = this.w_CODAZI;
        into cursor _Curs_CAUVIAHI
    endif
    if used('_Curs_CAUVIAHI')
      select _Curs_CAUVIAHI
      locate for 1=1
      do while not(eof())
      this.w_NUMCAUVIS = _Curs_CAUVIAHI.CONTATORE
      exit
        select _Curs_CAUVIAHI
        continue
      enddo
      use
    endif
    if this.w_NUMCAUVIS = 0 
      Ah_ErrorMsg("Caricare la lista delle causali 'visibili' in Infinity dall'apposita funzionalità!%0Impossibile proseguire","!","")
      i_retcode = 'stop'
      return
    endif
    if !Ah_YesNo("Si vuole calcolare la tracciabilità per gli ordini?%0NOTA: Verrà cancellata la tracciabilità precedentemente calcolata.")
      i_retcode = 'stop'
      return
    endif
    Ah_Msg("Cancellazione tracciabilità attuale....")
    * --- Delete from TAB_TRAC_DOC
    i_nConn=i_TableProp[this.TAB_TRAC_DOC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TAB_TRAC_DOC_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"1 = "+cp_ToStrODBC(1);
             )
    else
      delete from (i_cTable) where;
            1 = 1;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- 1 Fase: Creazione Tabella Temporanea di Partenza Livello 0 contentente gli Ordini
    Ah_Msg("Fase 1: Inserimento ordini Web e pubblicabili....")
    * --- Creazione Temporanea TMPNODI vuota
    * --- Create temporary table TMPNODI
    i_nIdx=cp_AddTableDef('TMPNODI') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    declare indexes_BMCRNQFEIZ[4]
    indexes_BMCRNQFEIZ[1]='SERIALE'
    indexes_BMCRNQFEIZ[2]='RIGA'
    indexes_BMCRNQFEIZ[3]='SERRIF'
    indexes_BMCRNQFEIZ[4]='RIGARIF'
    vq_exec('query\ordini_tmp',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_BMCRNQFEIZ,.f.)
    this.TMPNODI_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Il contenuto iniziale di TMPNODI sarà caratterizzato dall'insieme dei documenti di (TMPNODI)
    * --- Select from query\ord_tra_detail
    do vq_exec with 'query\ord_tra_detail',this,'_Curs_query_ord_tra_detail','',.f.,.t.
    if used('_Curs_query_ord_tra_detail')
      select _Curs_query_ord_tra_detail
      locate for 1=1
      do while not(eof())
      if LEFT(ALLTRIM(_Curs_query_ord_tra_detail.Ownercodeorder),1)="G"
        this.w_SERORIG = RIGHT(ALLTRIM(_Curs_query_ord_tra_detail.Ownercodeorder),LEN(ALLTRIM(_Curs_query_ord_tra_detail.Ownercodeorder))-1)
        this.w_SERWB = _Curs_query_ord_tra_detail.Ownercodeorder
        this.w_DATAREG = _Curs_query_ord_tra_detail.MVDATREG
        i_rows=1
      else
        this.w_SERWB = _Curs_query_ord_tra_detail.Ownercodeorder
        * --- Read from DOC_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MVSERIAL,MVDATREG"+;
            " from "+i_cTable+" DOC_MAST where ";
                +"MVSERWEB = "+cp_ToStrODBC(this.w_SERWB);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MVSERIAL,MVDATREG;
            from (i_cTable) where;
                MVSERWEB = this.w_SERWB;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SERORIG = NVL(cp_ToDate(_read_.MVSERIAL),cp_NullValue(_read_.MVSERIAL))
          this.w_DATAREG = NVL(cp_ToDate(_read_.MVDATREG),cp_NullValue(_read_.MVDATREG))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      if i_rows>0
        this.w_PADRES = this.w_SERWB
        this.w_RIGAS = _Curs_query_ord_tra_detail.Progressiveofline
        this.w_ROWORIG = this.w_RIGAS
        this.w_QTYORIG = _Curs_query_ord_tra_detail.Enlivenedqty
        this.w_EVORIG = 0
        this.w_QTYORIG1 = _Curs_query_ord_tra_detail.Enlivenedqtyfirstum
        this.w_EVORIG1 = 0
        this.w_SERIALE = alltrim(this.w_SERORIG)
        this.w_RIGA = this.w_ROWORIG
        this.w_SERRIF = alltrim(_Curs_query_ord_tra_detail.MVSERRIF)
        this.w_ROWRIF = _Curs_query_ord_tra_detail.MVROWRIF
        this.w_RIGAEVASA = _Curs_query_ord_tra_detail.FullFillmentFlag
        this.w_VALUTA = _Curs_query_ord_tra_detail.MVCODVAL
        this.w_CODCAU = _Curs_query_ord_tra_detail.MVTIPDOC
        this.w_CODRIC = alltrim(_Curs_query_ord_tra_detail.MVCODICE)
        this.w_TIPART = _Curs_query_ord_tra_detail.FLGART
        this.w_QTAEVAS = _Curs_query_ord_tra_detail.QTAUMORD
        this.w_NONEVASO = IIF(_Curs_query_ord_tra_detail.FullFillmentFlag # "S" AND _Curs_query_ord_tra_detail.FullFillmentQty = 0,"S","N")
        * --- Inserimento Record (Riga Ordine)
        * --- Insert into TMPNODI
        i_nConn=i_TableProp[this.TMPNODI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPNODI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPNODI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"SERIALE"+",RIGA"+",PADRES"+",RIGAS"+",SERRIF"+",RIGARIF"+",SERIALIZE"+",RIGALIZE"+",CAUDOC"+",QTAUMDOC"+",QTAUM1"+",EVUMDOC"+",EVUM1"+",SW"+",DATAREG"+",LIVELLO"+",SERIAL"+",NUMDOC"+",ALFDOC"+",DATDOC"+",ALFPRO"+",FLRIGEVA"+",QTAUMORD"+",MVCODVAL"+",TOVALSTS"+",TOFLGSTS"+",MVCODICE"+",FLGART"+",NOEVAS"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_SERIALE),'TMPNODI','SERIALE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RIGA),'TMPNODI','RIGA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PADRES),'TMPNODI','PADRES');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RIGAS),'TMPNODI','RIGAS');
          +","+cp_NullLink(cp_ToStrODBC(this.w_SERRIF),'TMPNODI','SERRIF');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ROWRIF),'TMPNODI','RIGARIF');
          +","+cp_NullLink(cp_ToStrODBC(this.w_SERIALE),'TMPNODI','SERIALIZE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RIGA),'TMPNODI','RIGALIZE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CODCAU),'TMPNODI','CAUDOC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_QTYORIG),'TMPNODI','QTAUMDOC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_QTYORIG1),'TMPNODI','QTAUM1');
          +","+cp_NullLink(cp_ToStrODBC(this.w_EVORIG),'TMPNODI','EVUMDOC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_EVORIG1),'TMPNODI','EVUM1');
          +","+cp_NullLink(cp_ToStrODBC(this.w_SERWB),'TMPNODI','SW');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DATAREG),'TMPNODI','DATAREG');
          +","+cp_NullLink(cp_ToStrODBC(0),'TMPNODI','LIVELLO');
          +","+cp_NullLink(cp_ToStrODBC("          "),'TMPNODI','SERIAL');
          +","+cp_NullLink(cp_ToStrODBC(999999999999999 - 999999999999999),'TMPNODI','NUMDOC');
          +","+cp_NullLink(cp_ToStrODBC("          "),'TMPNODI','ALFDOC');
          +","+cp_NullLink(cp_ToStrODBC(nullDate()),'TMPNODI','DATDOC');
          +","+cp_NullLink(cp_ToStrODBC(space(10)),'TMPNODI','ALFPRO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RIGAEVASA),'TMPNODI','FLRIGEVA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_QTAEVAS),'TMPNODI','QTAUMORD');
          +","+cp_NullLink(cp_ToStrODBC(this.w_VALUTA),'TMPNODI','MVCODVAL');
          +","+cp_NullLink(cp_ToStrODBC(0),'TMPNODI','TOVALSTS');
          +","+cp_NullLink(cp_ToStrODBC(SPACE(1)),'TMPNODI','TOFLGSTS');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CODRIC),'TMPNODI','MVCODICE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_TIPART),'TMPNODI','FLGART');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NONEVASO),'TMPNODI','NOEVAS');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'SERIALE',this.w_SERIALE,'RIGA',this.w_RIGA,'PADRES',this.w_PADRES,'RIGAS',this.w_RIGAS,'SERRIF',this.w_SERRIF,'RIGARIF',this.w_ROWRIF,'SERIALIZE',this.w_SERIALE,'RIGALIZE',this.w_RIGA,'CAUDOC',this.w_CODCAU,'QTAUMDOC',this.w_QTYORIG,'QTAUM1',this.w_QTYORIG1,'EVUMDOC',this.w_EVORIG)
          insert into (i_cTable) (SERIALE,RIGA,PADRES,RIGAS,SERRIF,RIGARIF,SERIALIZE,RIGALIZE,CAUDOC,QTAUMDOC,QTAUM1,EVUMDOC,EVUM1,SW,DATAREG,LIVELLO,SERIAL,NUMDOC,ALFDOC,DATDOC,ALFPRO,FLRIGEVA,QTAUMORD,MVCODVAL,TOVALSTS,TOFLGSTS,MVCODICE,FLGART,NOEVAS &i_ccchkf. );
             values (;
               this.w_SERIALE;
               ,this.w_RIGA;
               ,this.w_PADRES;
               ,this.w_RIGAS;
               ,this.w_SERRIF;
               ,this.w_ROWRIF;
               ,this.w_SERIALE;
               ,this.w_RIGA;
               ,this.w_CODCAU;
               ,this.w_QTYORIG;
               ,this.w_QTYORIG1;
               ,this.w_EVORIG;
               ,this.w_EVORIG1;
               ,this.w_SERWB;
               ,this.w_DATAREG;
               ,0;
               ,"          ";
               ,999999999999999 - 999999999999999;
               ,"          ";
               ,nullDate();
               ,space(10);
               ,this.w_RIGAEVASA;
               ,this.w_QTAEVAS;
               ,this.w_VALUTA;
               ,0;
               ,SPACE(1);
               ,this.w_CODRIC;
               ,this.w_TIPART;
               ,this.w_NONEVASO;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
        select _Curs_query_ord_tra_detail
        continue
      enddo
      use
    endif
    * --- 2 Fase: Elaborazione Tracciabilità ed Eventuale Aggiornamento Q.tà Evase
    * --- Creo la tabella contenente l'elenco dei documenti
    Ah_Msg("Fase 2: Elaborazione tracciabilità....")
    * --- Tabella Temporanea TMPDOCU contenente i documenti, andrà in join con TMPNODI 
    *     per individure prima l'insieme dei documenti di ORIGINE (GENERANTI) e quindi l'insieme dei 
    *     documenti di destinazione (GENERATI)
    * --- Create temporary table TMPDOCU
    i_nIdx=cp_AddTableDef('TMPDOCU') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    declare indexes_QDAOHNPLLL[4]
    indexes_QDAOHNPLLL[1]='MVSERIAL'
    indexes_QDAOHNPLLL[2]='CPROWNUM'
    indexes_QDAOHNPLLL[3]='MVSERRIF'
    indexes_QDAOHNPLLL[4]='MVROWRIF'
    vq_exec('query\ordini_eledoc',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_QDAOHNPLLL,.f.)
    this.TMPDOCU_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Ciclo per completare TMPNODI con tutti  i documenti "collegati" a quelli contenuti in  (TMPNODI)
    do while Not this.w_TERMINAD 
      * --- Elaborazione della tracciabilità per rilevazione dei documenti GENERATI
      *     (Flusso Down)
      if Not this.w_TERMINAD
        * --- Insert into TMPNODI
        i_nConn=i_TableProp[this.TMPNODI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPNODI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\ordini_tracc_1",this.TMPNODI_idx)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        * --- i_Rows = 0 Non ci sono più documenti "generati"
        this.w_TERMINAD = i_Rows = 0
        this.w_LIVELLOD = this.w_LIVELLOD + 1
      endif
    enddo
    * --- Elimino dalla tracciabilità tutti i documenti le cui causali non sono presenti nell'apposita funzionalità di 'visibilità in AHI'
    * --- Delete from TMPNODI
    i_nConn=i_TableProp[this.TMPNODI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPNODI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".CAUDOC = "+i_cQueryTable+".CAUDOC";
    
      do vq_exec with 'QUERY\ORDINI_CAUDOC_ESC',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Calcolo dell'evasione in base al documento 'evasore' con livello più basso riferente all'ordine in esame
    * --- Create temporary table TMPEVARIGORD
    i_nIdx=cp_AddTableDef('TMPEVARIGORD') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('QUERY\QTAEVA_RIGA_ORDINE',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPEVARIGORD_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Aggiorno i campi di evasione con il valore calcolato in precedenza, per gli ordini
    * --- Write into TMPNODI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPNODI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPNODI_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPEVARIGORD_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPNODI_idx,i_nConn)
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMPNODI.SW = _t2.SW";
              +" and "+"TMPNODI.RIGALIZE = _t2.RIGALIZE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"EVUMDOC = _t2.QTAEVA";
          +",EVUM1 = _t2.QTAEVA1";
          +i_ccchkf;
          +" from "+i_cTable+" TMPNODI, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMPNODI.SW = _t2.SW";
              +" and "+"TMPNODI.RIGALIZE = _t2.RIGALIZE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPNODI, "+i_cQueryTable+" _t2 set ";
          +"TMPNODI.EVUMDOC = _t2.QTAEVA";
          +",TMPNODI.EVUM1 = _t2.QTAEVA1";
          +Iif(Empty(i_ccchkf),"",",TMPNODI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="PostgreSQL"
        i_cWhere="TMPNODI.SW = _t2.SW";
              +" and "+"TMPNODI.RIGALIZE = _t2.RIGALIZE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPNODI set ";
          +"EVUMDOC = _t2.QTAEVA";
          +",EVUM1 = _t2.QTAEVA1";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".SW = "+i_cQueryTable+".SW";
              +" and "+i_cTable+".RIGALIZE = "+i_cQueryTable+".RIGALIZE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"EVUMDOC = (select QTAEVA from "+i_cQueryTable+" where "+i_cWhere+")";
          +",EVUM1 = (select QTAEVA1 from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Drop temporary table TMPEVARIGORD
    i_nIdx=cp_GetTableDefIdx('TMPEVARIGORD')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPEVARIGORD')
    endif
    * --- Aggiorno dati evasione sui documenti di evasione
    * --- Create temporary table TMPEVARIGORD
    i_nIdx=cp_AddTableDef('TMPEVARIGORD') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('QUERY\QTAEVA_RIGA_ORDINE_2',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPEVARIGORD_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Write into TMPNODI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPNODI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPNODI_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="SERIALE,RIGA"
      do vq_exec with 'QUERY\ORDINI_DOCEVAS_QTA',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPNODI_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMPNODI.SERIALE = _t2.SERIALE";
              +" and "+"TMPNODI.RIGA = _t2.RIGA";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"EVUMDOC = _t2.MVQTAMOV";
          +",EVUM1 = _t2.MVQTAUM1";
          +",TOVALSTS = _t2.MVQTAMOV";
      +",TOFLGSTS ="+cp_NullLink(cp_ToStrODBC("+"),'TMPNODI','TOFLGSTS');
          +i_ccchkf;
          +" from "+i_cTable+" TMPNODI, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMPNODI.SERIALE = _t2.SERIALE";
              +" and "+"TMPNODI.RIGA = _t2.RIGA";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPNODI, "+i_cQueryTable+" _t2 set ";
          +"TMPNODI.EVUMDOC = _t2.MVQTAMOV";
          +",TMPNODI.EVUM1 = _t2.MVQTAUM1";
          +",TMPNODI.TOVALSTS = _t2.MVQTAMOV";
      +",TMPNODI.TOFLGSTS ="+cp_NullLink(cp_ToStrODBC("+"),'TMPNODI','TOFLGSTS');
          +Iif(Empty(i_ccchkf),"",",TMPNODI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TMPNODI.SERIALE = t2.SERIALE";
              +" and "+"TMPNODI.RIGA = t2.RIGA";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPNODI set (";
          +"EVUMDOC,";
          +"EVUM1,";
          +"TOVALSTS,";
          +"TOFLGSTS";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.MVQTAMOV,";
          +"t2.MVQTAUM1,";
          +"t2.MVQTAMOV,";
          +cp_NullLink(cp_ToStrODBC("+"),'TMPNODI','TOFLGSTS')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TMPNODI.SERIALE = _t2.SERIALE";
              +" and "+"TMPNODI.RIGA = _t2.RIGA";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPNODI set ";
          +"EVUMDOC = _t2.MVQTAMOV";
          +",EVUM1 = _t2.MVQTAUM1";
          +",TOVALSTS = _t2.MVQTAMOV";
      +",TOFLGSTS ="+cp_NullLink(cp_ToStrODBC("+"),'TMPNODI','TOFLGSTS');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".SERIALE = "+i_cQueryTable+".SERIALE";
              +" and "+i_cTable+".RIGA = "+i_cQueryTable+".RIGA";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"EVUMDOC = (select MVQTAMOV from "+i_cQueryTable+" where "+i_cWhere+")";
          +",EVUM1 = (select MVQTAUM1 from "+i_cQueryTable+" where "+i_cWhere+")";
          +",TOVALSTS = (select MVQTAMOV from "+i_cQueryTable+" where "+i_cWhere+")";
      +",TOFLGSTS ="+cp_NullLink(cp_ToStrODBC("+"),'TMPNODI','TOFLGSTS');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Create temporary table TRACCIADOC
    i_nIdx=cp_AddTableDef('TRACCIADOC') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('query\ordini_nodi',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TRACCIADOC_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Azzero il valori relativi all'evasione per tutti i documenti generati da ordini aventi nella
    *     causale il flag di 'No evasione' attivo
    * --- Write into TRACCIADOC
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TRACCIADOC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TRACCIADOC_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="SERRIF,RIGARIF"
      do vq_exec with 'QUERY\ORDINI_NOEVAS_2',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TRACCIADOC_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TRACCIADOC.SERRIF = _t2.SERRIF";
              +" and "+"TRACCIADOC.RIGARIF = _t2.RIGARIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"EVUMDOC ="+cp_NullLink(cp_ToStrODBC(0),'TRACCIADOC','EVUMDOC');
      +",EVUM1 ="+cp_NullLink(cp_ToStrODBC(0),'TRACCIADOC','EVUM1');
      +",TOVALSTS ="+cp_NullLink(cp_ToStrODBC(0),'TRACCIADOC','TOVALSTS');
      +",TOFLGSTS ="+cp_NullLink(cp_ToStrODBC(" "),'TRACCIADOC','TOFLGSTS');
      +",FLRIGEVA ="+cp_NullLink(cp_ToStrODBC(0),'TRACCIADOC','FLRIGEVA');
          +i_ccchkf;
          +" from "+i_cTable+" TRACCIADOC, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TRACCIADOC.SERRIF = _t2.SERRIF";
              +" and "+"TRACCIADOC.RIGARIF = _t2.RIGARIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TRACCIADOC, "+i_cQueryTable+" _t2 set ";
      +"TRACCIADOC.EVUMDOC ="+cp_NullLink(cp_ToStrODBC(0),'TRACCIADOC','EVUMDOC');
      +",TRACCIADOC.EVUM1 ="+cp_NullLink(cp_ToStrODBC(0),'TRACCIADOC','EVUM1');
      +",TRACCIADOC.TOVALSTS ="+cp_NullLink(cp_ToStrODBC(0),'TRACCIADOC','TOVALSTS');
      +",TRACCIADOC.TOFLGSTS ="+cp_NullLink(cp_ToStrODBC(" "),'TRACCIADOC','TOFLGSTS');
      +",TRACCIADOC.FLRIGEVA ="+cp_NullLink(cp_ToStrODBC(0),'TRACCIADOC','FLRIGEVA');
          +Iif(Empty(i_ccchkf),"",",TRACCIADOC.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TRACCIADOC.SERRIF = t2.SERRIF";
              +" and "+"TRACCIADOC.RIGARIF = t2.RIGARIF";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TRACCIADOC set (";
          +"EVUMDOC,";
          +"EVUM1,";
          +"TOVALSTS,";
          +"TOFLGSTS,";
          +"FLRIGEVA";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC(0),'TRACCIADOC','EVUMDOC')+",";
          +cp_NullLink(cp_ToStrODBC(0),'TRACCIADOC','EVUM1')+",";
          +cp_NullLink(cp_ToStrODBC(0),'TRACCIADOC','TOVALSTS')+",";
          +cp_NullLink(cp_ToStrODBC(" "),'TRACCIADOC','TOFLGSTS')+",";
          +cp_NullLink(cp_ToStrODBC(0),'TRACCIADOC','FLRIGEVA')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TRACCIADOC.SERRIF = _t2.SERRIF";
              +" and "+"TRACCIADOC.RIGARIF = _t2.RIGARIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TRACCIADOC set ";
      +"EVUMDOC ="+cp_NullLink(cp_ToStrODBC(0),'TRACCIADOC','EVUMDOC');
      +",EVUM1 ="+cp_NullLink(cp_ToStrODBC(0),'TRACCIADOC','EVUM1');
      +",TOVALSTS ="+cp_NullLink(cp_ToStrODBC(0),'TRACCIADOC','TOVALSTS');
      +",TOFLGSTS ="+cp_NullLink(cp_ToStrODBC(" "),'TRACCIADOC','TOFLGSTS');
      +",FLRIGEVA ="+cp_NullLink(cp_ToStrODBC(0),'TRACCIADOC','FLRIGEVA');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".SERRIF = "+i_cQueryTable+".SERRIF";
              +" and "+i_cTable+".RIGARIF = "+i_cQueryTable+".RIGARIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"EVUMDOC ="+cp_NullLink(cp_ToStrODBC(0),'TRACCIADOC','EVUMDOC');
      +",EVUM1 ="+cp_NullLink(cp_ToStrODBC(0),'TRACCIADOC','EVUM1');
      +",TOVALSTS ="+cp_NullLink(cp_ToStrODBC(0),'TRACCIADOC','TOVALSTS');
      +",TOFLGSTS ="+cp_NullLink(cp_ToStrODBC(" "),'TRACCIADOC','TOFLGSTS');
      +",FLRIGEVA ="+cp_NullLink(cp_ToStrODBC(0),'TRACCIADOC','FLRIGEVA');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Azzero il campo TOVALSTS nei documenti di evasione diretta, la cui causale ha attivo il flag di no evasione per l'ordine.
    * --- Write into TRACCIADOC
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TRACCIADOC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TRACCIADOC_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="SERIALE,RIGA"
      do vq_exec with 'QUERY\DOCUMENTI_NOEVAS',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TRACCIADOC_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TRACCIADOC.SERIALE = _t2.SERIALE";
              +" and "+"TRACCIADOC.RIGA = _t2.RIGA";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"TOVALSTS ="+cp_NullLink(cp_ToStrODBC(0),'TRACCIADOC','TOVALSTS');
          +i_ccchkf;
          +" from "+i_cTable+" TRACCIADOC, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TRACCIADOC.SERIALE = _t2.SERIALE";
              +" and "+"TRACCIADOC.RIGA = _t2.RIGA";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TRACCIADOC, "+i_cQueryTable+" _t2 set ";
      +"TRACCIADOC.TOVALSTS ="+cp_NullLink(cp_ToStrODBC(0),'TRACCIADOC','TOVALSTS');
          +Iif(Empty(i_ccchkf),"",",TRACCIADOC.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TRACCIADOC.SERIALE = t2.SERIALE";
              +" and "+"TRACCIADOC.RIGA = t2.RIGA";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TRACCIADOC set (";
          +"TOVALSTS";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC(0),'TRACCIADOC','TOVALSTS')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TRACCIADOC.SERIALE = _t2.SERIALE";
              +" and "+"TRACCIADOC.RIGA = _t2.RIGA";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TRACCIADOC set ";
      +"TOVALSTS ="+cp_NullLink(cp_ToStrODBC(0),'TRACCIADOC','TOVALSTS');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".SERIALE = "+i_cQueryTable+".SERIALE";
              +" and "+i_cTable+".RIGA = "+i_cQueryTable+".RIGA";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"TOVALSTS ="+cp_NullLink(cp_ToStrODBC(0),'TRACCIADOC','TOVALSTS');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Creo il cursore compatibile alla gestione della tracciabilità in AHI
    * --- Inserisco i dati della tracciabilità  documentale nell'apposita tabella di AdHoc
    Ah_Msg("Fase 3: Inserimento della tracciabilità nella tabella di AdHoc....")
    * --- Insert into TAB_TRAC_DOC
    i_nConn=i_TableProp[this.TAB_TRAC_DOC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TAB_TRAC_DOC_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TRACCIADOC_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.TAB_TRAC_DOC_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore durante inserimento dati tracciabilità nella tabella di AdHoc'
      return
    endif
    * --- Controllo congruità flag di evasione con quantità effettivamente evase
    * --- Write into TAB_TRAC_DOC
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TAB_TRAC_DOC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TAB_TRAC_DOC_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="SERRIF,RIGARIF"
      do vq_exec with 'QUERY\ORDINI_FLRIGEVA',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TAB_TRAC_DOC_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TAB_TRAC_DOC.SERRIF = _t2.SERRIF";
              +" and "+"TAB_TRAC_DOC.RIGARIF = _t2.RIGARIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"FLRIGEVA = _t2.FLRIGEVA";
          +i_ccchkf;
          +" from "+i_cTable+" TAB_TRAC_DOC, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TAB_TRAC_DOC.SERRIF = _t2.SERRIF";
              +" and "+"TAB_TRAC_DOC.RIGARIF = _t2.RIGARIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TAB_TRAC_DOC, "+i_cQueryTable+" _t2 set ";
          +"TAB_TRAC_DOC.FLRIGEVA = _t2.FLRIGEVA";
          +Iif(Empty(i_ccchkf),"",",TAB_TRAC_DOC.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TAB_TRAC_DOC.SERRIF = t2.SERRIF";
              +" and "+"TAB_TRAC_DOC.RIGARIF = t2.RIGARIF";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TAB_TRAC_DOC set (";
          +"FLRIGEVA";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.FLRIGEVA";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TAB_TRAC_DOC.SERRIF = _t2.SERRIF";
              +" and "+"TAB_TRAC_DOC.RIGARIF = _t2.RIGARIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TAB_TRAC_DOC set ";
          +"FLRIGEVA = _t2.FLRIGEVA";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".SERRIF = "+i_cQueryTable+".SERRIF";
              +" and "+i_cTable+".RIGARIF = "+i_cQueryTable+".RIGARIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"FLRIGEVA = (select FLRIGEVA from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Drop temporary table TMPNODI
    i_nIdx=cp_GetTableDefIdx('TMPNODI')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPNODI')
    endif
    * --- Drop temporary table TMPDOCU
    i_nIdx=cp_GetTableDefIdx('TMPDOCU')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPDOCU')
    endif
    * --- Drop temporary table TRACCIADOC
    i_nIdx=cp_GetTableDefIdx('TRACCIADOC')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TRACCIADOC')
    endif
    * --- Drop temporary table TMPEVARIGORD
    i_nIdx=cp_GetTableDefIdx('TMPEVARIGORD')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPEVARIGORD')
    endif
    Ah_ErrorMsg("Elaborazione terminata!",64,"")
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='*TMPNODI'
    this.cWorkTables[2]='DOC_MAST'
    this.cWorkTables[3]='*TMPDOCU'
    this.cWorkTables[4]='*TRACCIADOC'
    this.cWorkTables[5]='CAUVIAHI'
    this.cWorkTables[6]='*TMPEVARIGORD'
    this.cWorkTables[7]='TAB_TRAC_DOC'
    return(this.OpenAllTables(7))

  proc CloseCursors()
    if used('_Curs_CAUVIAHI')
      use in _Curs_CAUVIAHI
    endif
    if used('_Curs_query_ord_tra_detail')
      use in _Curs_query_ord_tra_detail
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
