* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_amd                                                        *
*              Modelli documenti                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-07-04                                                      *
* Last revis.: 2014-10-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gsut_amd
public g_Debug_Query
g_Debug_Query = ''
* --- Fine Area Manuale
return(createobject("tgsut_amd"))

* --- Class definition
define class tgsut_amd as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 708
  Height = 291+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-10-02"
  HelpContextID=210322281
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=32

  * --- Constant Properties
  PROMODEL_IDX = 0
  PROMCLAS_IDX = 0
  KEY_ARTI_IDX = 0
  PRE_ITER_IDX = 0
  PRA_TIPI_IDX = 0
  PRA_MATE_IDX = 0
  PRA_ENTI_IDX = 0
  PAR_ALTE_IDX = 0
  cFile = "PROMODEL"
  cKeySelect = "MOCODICE"
  cKeyWhere  = "MOCODICE=this.w_MOCODICE"
  cKeyWhereODBC = '"MOCODICE="+cp_ToStrODBC(this.w_MOCODICE)';

  cKeyWhereODBCqualified = '"PROMODEL.MOCODICE="+cp_ToStrODBC(this.w_MOCODICE)';

  cPrg = "gsut_amd"
  cComment = "Modelli documenti"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_LeggePar_Alte = space(5)
  w_MOCODICE = space(10)
  w_MODESCRI = space(60)
  o_MODESCRI = space(60)
  w_MOPREFER = space(1)
  w_MOCLASSE = space(15)
  w_DESCLA = space(50)
  w_MOPATMOD = space(254)
  w_PATMOD = space(254)
  o_PATMOD = space(254)
  w_MOQUERY = space(254)
  o_MOQUERY = space(254)
  w_QUERY = space(60)
  o_QUERY = space(60)
  w_MOCODPRE = space(20)
  w_MORAGPRE = space(10)
  w_MO__NOTE = space(0)
  w_DIR_INST = space(50)
  w_MODARC = space(1)
  w_DESPRE = space(40)
  w_RCACODART = space(20)
  w_RCADTOBSO = ctod('  /  /  ')
  w_DESCRITER = space(60)
  w_OBTEST = ctod('  /  /  ')
  w_TIPOENTE = space(1)
  w_TABRIF = space(20)
  w_MODTINVA = ctod('  /  /  ')
  w_MODTOBSO = ctod('  /  /  ')
  w_MOTIPPRA = space(10)
  w_FLGIUD = space(1)
  w_MOMATPRA = space(10)
  w_MATETIPOL = space(1)
  o_MATETIPOL = space(1)
  w_MO__ENTE = space(10)
  w_CLASSE = space(15)
  w_DIRMOD = space(254)
  w_QUERYMOD = space(254)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_CODAZI = this.W_CODAZI
  op_MOCODICE = this.W_MOCODICE
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'PROMODEL','gsut_amd')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_amdPag1","gsut_amd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Modello documento")
      .Pages(1).HelpContextID = 49159562
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMOCODICE_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[8]
    this.cWorkTables[1]='PROMCLAS'
    this.cWorkTables[2]='KEY_ARTI'
    this.cWorkTables[3]='PRE_ITER'
    this.cWorkTables[4]='PRA_TIPI'
    this.cWorkTables[5]='PRA_MATE'
    this.cWorkTables[6]='PRA_ENTI'
    this.cWorkTables[7]='PAR_ALTE'
    this.cWorkTables[8]='PROMODEL'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(8))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PROMODEL_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PROMODEL_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_MOCODICE = NVL(MOCODICE,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_6_joined
    link_1_6_joined=.f.
    local link_1_17_joined
    link_1_17_joined=.f.
    local link_1_36_joined
    link_1_36_joined=.f.
    local link_1_38_joined
    link_1_38_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from PROMODEL where MOCODICE=KeySet.MOCODICE
    *
    i_nConn = i_TableProp[this.PROMODEL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PROMODEL_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PROMODEL')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PROMODEL.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PROMODEL '
      link_1_6_joined=this.AddJoinedLink_1_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_17_joined=this.AddJoinedLink_1_17(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_36_joined=this.AddJoinedLink_1_36(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_38_joined=this.AddJoinedLink_1_38(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MOCODICE',this.w_MOCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_LeggePar_Alte = i_CodAzi
        .w_DESCLA = space(50)
        .w_PATMOD = space(254)
        .w_QUERY = space(60)
        .w_DIR_INST = sys(5) + sys(2003) + "\"
        .w_MODARC = space(1)
        .w_DESPRE = space(40)
        .w_RCACODART = space(20)
        .w_RCADTOBSO = ctod("  /  /  ")
        .w_DESCRITER = space(60)
        .w_OBTEST = i_datsys
        .w_TIPOENTE = space(1)
        .w_TABRIF = space(20)
        .w_FLGIUD = space(1)
        .w_MATETIPOL = space(1)
        .w_CLASSE = space(15)
        .w_DIRMOD = space(254)
        .w_QUERYMOD = space(254)
          .link_1_1('Load')
        .w_MOCODICE = NVL(MOCODICE,space(10))
        .op_MOCODICE = .w_MOCODICE
        .w_MODESCRI = NVL(MODESCRI,space(60))
        .w_MOPREFER = NVL(MOPREFER,space(1))
        .w_MOCLASSE = NVL(MOCLASSE,space(15))
          if link_1_6_joined
            this.w_MOCLASSE = NVL(CDCODCLA106,NVL(this.w_MOCLASSE,space(15)))
            this.w_DESCLA = NVL(CDDESCLA106,space(50))
            this.w_MODARC = NVL(CDMODALL106,space(1))
            this.w_TABRIF = NVL(CDRIFTAB106,space(20))
          else
          .link_1_6('Load')
          endif
        .w_MOPATMOD = NVL(MOPATMOD,space(254))
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .w_MOQUERY = NVL(MOQUERY,space(254))
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .w_MOCODPRE = NVL(MOCODPRE,space(20))
          if link_1_17_joined
            this.w_MOCODPRE = NVL(CACODICE117,NVL(this.w_MOCODPRE,space(20)))
            this.w_DESPRE = NVL(CADESART117,space(40))
            this.w_RCACODART = NVL(CACODART117,space(20))
            this.w_RCADTOBSO = NVL(cp_ToDate(CADTOBSO117),ctod("  /  /  "))
          else
          .link_1_17('Load')
          endif
        .w_MORAGPRE = NVL(MORAGPRE,space(10))
          .link_1_18('Load')
        .w_MO__NOTE = NVL(MO__NOTE,space(0))
        .w_MODTINVA = NVL(cp_ToDate(MODTINVA),ctod("  /  /  "))
        .w_MODTOBSO = NVL(cp_ToDate(MODTOBSO),ctod("  /  /  "))
        .w_MOTIPPRA = NVL(MOTIPPRA,space(10))
          if link_1_36_joined
            this.w_MOTIPPRA = NVL(TPCODICE136,NVL(this.w_MOTIPPRA,space(10)))
            this.w_FLGIUD = NVL(TPFLGIUD136,space(1))
          else
          .link_1_36('Load')
          endif
        .w_MOMATPRA = NVL(MOMATPRA,space(10))
          if link_1_38_joined
            this.w_MOMATPRA = NVL(MPCODICE138,NVL(this.w_MOMATPRA,space(10)))
            this.w_MATETIPOL = NVL(MP_TIPOL138,space(1))
          else
          .link_1_38('Load')
          endif
        .w_MO__ENTE = NVL(MO__ENTE,space(10))
          * evitabile
          *.link_1_40('Load')
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'PROMODEL')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_LeggePar_Alte = space(5)
      .w_MOCODICE = space(10)
      .w_MODESCRI = space(60)
      .w_MOPREFER = space(1)
      .w_MOCLASSE = space(15)
      .w_DESCLA = space(50)
      .w_MOPATMOD = space(254)
      .w_PATMOD = space(254)
      .w_MOQUERY = space(254)
      .w_QUERY = space(60)
      .w_MOCODPRE = space(20)
      .w_MORAGPRE = space(10)
      .w_MO__NOTE = space(0)
      .w_DIR_INST = space(50)
      .w_MODARC = space(1)
      .w_DESPRE = space(40)
      .w_RCACODART = space(20)
      .w_RCADTOBSO = ctod("  /  /  ")
      .w_DESCRITER = space(60)
      .w_OBTEST = ctod("  /  /  ")
      .w_TIPOENTE = space(1)
      .w_TABRIF = space(20)
      .w_MODTINVA = ctod("  /  /  ")
      .w_MODTOBSO = ctod("  /  /  ")
      .w_MOTIPPRA = space(10)
      .w_FLGIUD = space(1)
      .w_MOMATPRA = space(10)
      .w_MATETIPOL = space(1)
      .w_MO__ENTE = space(10)
      .w_CLASSE = space(15)
      .w_DIRMOD = space(254)
      .w_QUERYMOD = space(254)
      if .cFunction<>"Filter"
        .w_LeggePar_Alte = i_CodAzi
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_LeggePar_Alte))
          .link_1_1('Full')
          endif
          .DoRTCalc(2,4,.f.)
        .w_MOCLASSE = IIF(ISALT() AND .cFunction='Load' , .w_CLASSE, space(15))
        .DoRTCalc(5,5,.f.)
          if not(empty(.w_MOCLASSE))
          .link_1_6('Full')
          endif
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
          .DoRTCalc(6,8,.f.)
        .w_MOQUERY = IIF(ISALT() AND .cFunction='Load' , .w_QUERYMOD, space(254))
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .DoRTCalc(10,11,.f.)
          if not(empty(.w_MOCODPRE))
          .link_1_17('Full')
          endif
        .DoRTCalc(12,12,.f.)
          if not(empty(.w_MORAGPRE))
          .link_1_18('Full')
          endif
          .DoRTCalc(13,13,.f.)
        .w_DIR_INST = sys(5) + sys(2003) + "\"
          .DoRTCalc(15,19,.f.)
        .w_OBTEST = i_datsys
        .DoRTCalc(21,25,.f.)
          if not(empty(.w_MOTIPPRA))
          .link_1_36('Full')
          endif
        .DoRTCalc(26,27,.f.)
          if not(empty(.w_MOMATPRA))
          .link_1_38('Full')
          endif
        .DoRTCalc(28,29,.f.)
          if not(empty(.w_MO__ENTE))
          .link_1_40('Full')
          endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'PROMODEL')
    this.DoRTCalc(30,32,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PROMODEL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PROMODEL_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SEMDO","i_CODAZI,w_MOCODICE")
      .op_CODAZI = .w_CODAZI
      .op_MOCODICE = .w_MOCODICE
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oMOCODICE_1_2.enabled = !i_bVal
      .Page1.oPag.oMODESCRI_1_4.enabled = i_bVal
      .Page1.oPag.oMOPREFER_1_5.enabled = i_bVal
      .Page1.oPag.oMOCLASSE_1_6.enabled = i_bVal
      .Page1.oPag.oMOPATMOD_1_9.enabled = i_bVal
      .Page1.oPag.oMOQUERY_1_13.enabled = i_bVal
      .Page1.oPag.oMOCODPRE_1_17.enabled = i_bVal
      .Page1.oPag.oMORAGPRE_1_18.enabled = i_bVal
      .Page1.oPag.oMO__NOTE_1_19.enabled = i_bVal
      .Page1.oPag.oMODTINVA_1_34.enabled = i_bVal
      .Page1.oPag.oMODTOBSO_1_35.enabled = i_bVal
      .Page1.oPag.oMOTIPPRA_1_36.enabled = i_bVal
      .Page1.oPag.oMOMATPRA_1_38.enabled = i_bVal
      .Page1.oPag.oMO__ENTE_1_40.enabled = i_bVal
      .Page1.oPag.oBtn_1_20.enabled = .Page1.oPag.oBtn_1_20.mCond()
      .Page1.oPag.oObj_1_11.enabled = i_bVal
      .Page1.oPag.oObj_1_16.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oMODESCRI_1_4.enabled = .t.
        .Page1.oPag.oMOCLASSE_1_6.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'PROMODEL',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PROMODEL_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOCODICE,"MOCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MODESCRI,"MODESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOPREFER,"MOPREFER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOCLASSE,"MOCLASSE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOPATMOD,"MOPATMOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOQUERY,"MOQUERY",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOCODPRE,"MOCODPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MORAGPRE,"MORAGPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MO__NOTE,"MO__NOTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MODTINVA,"MODTINVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MODTOBSO,"MODTOBSO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOTIPPRA,"MOTIPPRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOMATPRA,"MOMATPRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MO__ENTE,"MO__ENTE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PROMODEL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PROMODEL_IDX,2])
    i_lTable = "PROMODEL"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.PROMODEL_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      vx_exec("QUERY\GSUT_AMD.VQR,QUERY\GSUT_AMD.FRX",this)
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PROMODEL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PROMODEL_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.PROMODEL_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SEMDO","i_CODAZI,w_MOCODICE")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into PROMODEL
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PROMODEL')
        i_extval=cp_InsertValODBCExtFlds(this,'PROMODEL')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(MOCODICE,MODESCRI,MOPREFER,MOCLASSE,MOPATMOD"+;
                  ",MOQUERY,MOCODPRE,MORAGPRE,MO__NOTE,MODTINVA"+;
                  ",MODTOBSO,MOTIPPRA,MOMATPRA,MO__ENTE "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_MOCODICE)+;
                  ","+cp_ToStrODBC(this.w_MODESCRI)+;
                  ","+cp_ToStrODBC(this.w_MOPREFER)+;
                  ","+cp_ToStrODBCNull(this.w_MOCLASSE)+;
                  ","+cp_ToStrODBC(this.w_MOPATMOD)+;
                  ","+cp_ToStrODBC(this.w_MOQUERY)+;
                  ","+cp_ToStrODBCNull(this.w_MOCODPRE)+;
                  ","+cp_ToStrODBCNull(this.w_MORAGPRE)+;
                  ","+cp_ToStrODBC(this.w_MO__NOTE)+;
                  ","+cp_ToStrODBC(this.w_MODTINVA)+;
                  ","+cp_ToStrODBC(this.w_MODTOBSO)+;
                  ","+cp_ToStrODBCNull(this.w_MOTIPPRA)+;
                  ","+cp_ToStrODBCNull(this.w_MOMATPRA)+;
                  ","+cp_ToStrODBCNull(this.w_MO__ENTE)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PROMODEL')
        i_extval=cp_InsertValVFPExtFlds(this,'PROMODEL')
        cp_CheckDeletedKey(i_cTable,0,'MOCODICE',this.w_MOCODICE)
        INSERT INTO (i_cTable);
              (MOCODICE,MODESCRI,MOPREFER,MOCLASSE,MOPATMOD,MOQUERY,MOCODPRE,MORAGPRE,MO__NOTE,MODTINVA,MODTOBSO,MOTIPPRA,MOMATPRA,MO__ENTE  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_MOCODICE;
                  ,this.w_MODESCRI;
                  ,this.w_MOPREFER;
                  ,this.w_MOCLASSE;
                  ,this.w_MOPATMOD;
                  ,this.w_MOQUERY;
                  ,this.w_MOCODPRE;
                  ,this.w_MORAGPRE;
                  ,this.w_MO__NOTE;
                  ,this.w_MODTINVA;
                  ,this.w_MODTOBSO;
                  ,this.w_MOTIPPRA;
                  ,this.w_MOMATPRA;
                  ,this.w_MO__ENTE;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.PROMODEL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PROMODEL_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.PROMODEL_IDX,i_nConn)
      *
      * update PROMODEL
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'PROMODEL')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " MODESCRI="+cp_ToStrODBC(this.w_MODESCRI)+;
             ",MOPREFER="+cp_ToStrODBC(this.w_MOPREFER)+;
             ",MOCLASSE="+cp_ToStrODBCNull(this.w_MOCLASSE)+;
             ",MOPATMOD="+cp_ToStrODBC(this.w_MOPATMOD)+;
             ",MOQUERY="+cp_ToStrODBC(this.w_MOQUERY)+;
             ",MOCODPRE="+cp_ToStrODBCNull(this.w_MOCODPRE)+;
             ",MORAGPRE="+cp_ToStrODBCNull(this.w_MORAGPRE)+;
             ",MO__NOTE="+cp_ToStrODBC(this.w_MO__NOTE)+;
             ",MODTINVA="+cp_ToStrODBC(this.w_MODTINVA)+;
             ",MODTOBSO="+cp_ToStrODBC(this.w_MODTOBSO)+;
             ",MOTIPPRA="+cp_ToStrODBCNull(this.w_MOTIPPRA)+;
             ",MOMATPRA="+cp_ToStrODBCNull(this.w_MOMATPRA)+;
             ",MO__ENTE="+cp_ToStrODBCNull(this.w_MO__ENTE)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'PROMODEL')
        i_cWhere = cp_PKFox(i_cTable  ,'MOCODICE',this.w_MOCODICE  )
        UPDATE (i_cTable) SET;
              MODESCRI=this.w_MODESCRI;
             ,MOPREFER=this.w_MOPREFER;
             ,MOCLASSE=this.w_MOCLASSE;
             ,MOPATMOD=this.w_MOPATMOD;
             ,MOQUERY=this.w_MOQUERY;
             ,MOCODPRE=this.w_MOCODPRE;
             ,MORAGPRE=this.w_MORAGPRE;
             ,MO__NOTE=this.w_MO__NOTE;
             ,MODTINVA=this.w_MODTINVA;
             ,MODTOBSO=this.w_MODTOBSO;
             ,MOTIPPRA=this.w_MOTIPPRA;
             ,MOMATPRA=this.w_MOMATPRA;
             ,MO__ENTE=this.w_MO__ENTE;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PROMODEL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PROMODEL_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.PROMODEL_IDX,i_nConn)
      *
      * delete PROMODEL
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'MOCODICE',this.w_MOCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PROMODEL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PROMODEL_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        if .o_MOQUERY<>.w_MOQUERY
          .Calculate_KSQPOFPPKT()
        endif
        if .o_MATETIPOL<>.w_MATETIPOL
          .Calculate_TGIMXKPUCZ()
        endif
        if .o_PATMOD<>.w_PATMOD
          .Calculate_UFXGOLDOFC()
        endif
        if .o_QUERY<>.w_QUERY
          .Calculate_MHXOIQDMFU()
        endif
        if .o_MODESCRI<>.w_MODESCRI
          .Calculate_XXDQEWHRFF()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_CODAZI<>.w_CODAZI
           cp_AskTableProg(this,i_nConn,"SEMDO","i_CODAZI,w_MOCODICE")
          .op_MOCODICE = .w_MOCODICE
        endif
        .op_CODAZI = .w_CODAZI
      endwith
      this.DoRTCalc(2,32,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
    endwith
  return

  proc Calculate_KSQPOFPPKT()
    with this
          * --- Assegno la query alla variabile g_Debug_Query
          AssegQuery(this;
             )
    endwith
  endproc
  proc Calculate_TGIMXKPUCZ()
    with this
          * --- Rivalorizzo MO__ENTE
          RivalorizzoCombo(this;
             )
    endwith
  endproc
  proc Calculate_UFXGOLDOFC()
    with this
          * --- Aggiorna MOPATMOD
          .w_MOPATMOD = strtran(.w_PATMOD, .w_DIR_INST, "")
    endwith
  endproc
  proc Calculate_MHXOIQDMFU()
    with this
          * --- Aggiorna MOQUERY
          .w_MOQUERY = strtran(.w_QUERY, .w_DIR_INST, "")
    endwith
  endproc
  proc Calculate_XXDQEWHRFF()
    with this
          * --- Valorizza MOPATMOD
          .w_MOPATMOD = IIF(ISALT() AND .cFunction='Load' AND NOT EMPTY(.w_DIRMOD), alltrim(.w_DIRMOD)+alltrim(.w_MODESCRI)+ '.DOC', .w_MOPATMOD)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oMOCODPRE_1_17.enabled = this.oPgFrm.Page1.oPag.oMOCODPRE_1_17.mCond()
    this.oPgFrm.Page1.oPag.oMORAGPRE_1_18.enabled = this.oPgFrm.Page1.oPag.oMORAGPRE_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oMOCODPRE_1_17.visible=!this.oPgFrm.Page1.oPag.oMOCODPRE_1_17.mHide()
    this.oPgFrm.Page1.oPag.oMORAGPRE_1_18.visible=!this.oPgFrm.Page1.oPag.oMORAGPRE_1_18.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_20.visible=!this.oPgFrm.Page1.oPag.oBtn_1_20.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_25.visible=!this.oPgFrm.Page1.oPag.oStr_1_25.mHide()
    this.oPgFrm.Page1.oPag.oDESPRE_1_26.visible=!this.oPgFrm.Page1.oPag.oDESPRE_1_26.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_29.visible=!this.oPgFrm.Page1.oPag.oStr_1_29.mHide()
    this.oPgFrm.Page1.oPag.oDESCRITER_1_30.visible=!this.oPgFrm.Page1.oPag.oDESCRITER_1_30.mHide()
    this.oPgFrm.Page1.oPag.oMOTIPPRA_1_36.visible=!this.oPgFrm.Page1.oPag.oMOTIPPRA_1_36.mHide()
    this.oPgFrm.Page1.oPag.oMOMATPRA_1_38.visible=!this.oPgFrm.Page1.oPag.oMOMATPRA_1_38.mHide()
    this.oPgFrm.Page1.oPag.oMO__ENTE_1_40.visible=!this.oPgFrm.Page1.oPag.oMO__ENTE_1_40.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_41.visible=!this.oPgFrm.Page1.oPag.oStr_1_41.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_42.visible=!this.oPgFrm.Page1.oPag.oStr_1_42.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_43.visible=!this.oPgFrm.Page1.oPag.oStr_1_43.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_44.visible=!this.oPgFrm.Page1.oPag.oStr_1_44.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_11.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_16.Event(cEvent)
        if lower(cEvent)==lower("Load")
          .Calculate_TGIMXKPUCZ()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=LeggePar_Alte
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_ALTE_IDX,3]
    i_lTable = "PAR_ALTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2], .t., this.PAR_ALTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LeggePar_Alte) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LeggePar_Alte)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PACLASSE,PAPATMOD,PAQUERY";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_LeggePar_Alte);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_LeggePar_Alte)
            select PACODAZI,PACLASSE,PAPATMOD,PAQUERY;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LeggePar_Alte = NVL(_Link_.PACODAZI,space(5))
      this.w_CLASSE = NVL(_Link_.PACLASSE,space(15))
      this.w_DIRMOD = NVL(_Link_.PAPATMOD,space(254))
      this.w_QUERYMOD = NVL(_Link_.PAQUERY,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_LeggePar_Alte = space(5)
      endif
      this.w_CLASSE = space(15)
      this.w_DIRMOD = space(254)
      this.w_QUERYMOD = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_ALTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LeggePar_Alte Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MOCLASSE
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PROMCLAS_IDX,3]
    i_lTable = "PROMCLAS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2], .t., this.PROMCLAS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOCLASSE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsut_mcd',True,'PROMCLAS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CDCODCLA like "+cp_ToStrODBC(trim(this.w_MOCLASSE)+"%");

          i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDMODALL,CDRIFTAB";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CDCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CDCODCLA',trim(this.w_MOCLASSE))
          select CDCODCLA,CDDESCLA,CDMODALL,CDRIFTAB;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CDCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOCLASSE)==trim(_Link_.CDCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MOCLASSE) and !this.bDontReportError
            deferred_cp_zoom('PROMCLAS','*','CDCODCLA',cp_AbsName(oSource.parent,'oMOCLASSE_1_6'),i_cWhere,'gsut_mcd',"Classi documentali",'GSUT_AMD.PROMCLAS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDMODALL,CDRIFTAB";
                     +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDCODCLA',oSource.xKey(1))
            select CDCODCLA,CDDESCLA,CDMODALL,CDRIFTAB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOCLASSE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDMODALL,CDRIFTAB";
                   +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(this.w_MOCLASSE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDCODCLA',this.w_MOCLASSE)
            select CDCODCLA,CDDESCLA,CDMODALL,CDRIFTAB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOCLASSE = NVL(_Link_.CDCODCLA,space(15))
      this.w_DESCLA = NVL(_Link_.CDDESCLA,space(50))
      this.w_MODARC = NVL(_Link_.CDMODALL,space(1))
      this.w_TABRIF = NVL(_Link_.CDRIFTAB,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_MOCLASSE = space(15)
      endif
      this.w_DESCLA = space(50)
      this.w_MODARC = space(1)
      this.w_TABRIF = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=( SUBSTR( CHKPECLA( .w_MOCLASSE, i_CODUTE), 2, 1 ) = 'S' ) and .w_MODARC='F'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Classe documentale non valida")
        endif
        this.w_MOCLASSE = space(15)
        this.w_DESCLA = space(50)
        this.w_MODARC = space(1)
        this.w_TABRIF = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])+'\'+cp_ToStr(_Link_.CDCODCLA,1)
      cp_ShowWarn(i_cKey,this.PROMCLAS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOCLASSE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PROMCLAS_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_6.CDCODCLA as CDCODCLA106"+ ",link_1_6.CDDESCLA as CDDESCLA106"+ ",link_1_6.CDMODALL as CDMODALL106"+ ",link_1_6.CDRIFTAB as CDRIFTAB106"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_6 on PROMODEL.MOCLASSE=link_1_6.CDCODCLA"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_6"
          i_cKey=i_cKey+'+" and PROMODEL.MOCLASSE=link_1_6.CDCODCLA(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MOCODPRE
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOCODPRE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_MOCODPRE)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_MOCODPRE))
          select CACODICE,CADESART,CACODART,CADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOCODPRE)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_MOCODPRE)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_MOCODPRE)+"%");

            select CACODICE,CADESART,CACODART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MOCODPRE) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oMOCODPRE_1_17'),i_cWhere,'GSMA_BZA',"Prestazioni",'Inspre.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CACODART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOCODPRE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_MOCODPRE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_MOCODPRE)
            select CACODICE,CADESART,CACODART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOCODPRE = NVL(_Link_.CACODICE,space(20))
      this.w_DESPRE = NVL(_Link_.CADESART,space(40))
      this.w_RCACODART = NVL(_Link_.CACODART,space(20))
      this.w_RCADTOBSO = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MOCODPRE = space(20)
      endif
      this.w_DESPRE = space(40)
      this.w_RCACODART = space(20)
      this.w_RCADTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=ChkTipArt(.w_RCACODART, "FM-FO-DE") AND (EMPTY(.w_RCADTOBSO) OR .w_RCADTOBSO>=i_datsys)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Prestazione selezionata inesistente o obsoleta")
        endif
        this.w_MOCODPRE = space(20)
        this.w_DESPRE = space(40)
        this.w_RCACODART = space(20)
        this.w_RCADTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOCODPRE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_17(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_17.CACODICE as CACODICE117"+ ",link_1_17.CADESART as CADESART117"+ ",link_1_17.CACODART as CACODART117"+ ",link_1_17.CADTOBSO as CADTOBSO117"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_17 on PROMODEL.MOCODPRE=link_1_17.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_17"
          i_cKey=i_cKey+'+" and PROMODEL.MOCODPRE=link_1_17.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MORAGPRE
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PRE_ITER_IDX,3]
    i_lTable = "PRE_ITER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PRE_ITER_IDX,2], .t., this.PRE_ITER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PRE_ITER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MORAGPRE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MIT',True,'PRE_ITER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ITCODICE like "+cp_ToStrODBC(trim(this.w_MORAGPRE)+"%");

          i_ret=cp_SQL(i_nConn,"select ITCODICE,ITDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ITCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ITCODICE',trim(this.w_MORAGPRE))
          select ITCODICE,ITDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ITCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MORAGPRE)==trim(_Link_.ITCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ITDESCRI like "+cp_ToStrODBC(trim(this.w_MORAGPRE)+"%");

            i_ret=cp_SQL(i_nConn,"select ITCODICE,ITDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ITDESCRI like "+cp_ToStr(trim(this.w_MORAGPRE)+"%");

            select ITCODICE,ITDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MORAGPRE) and !this.bDontReportError
            deferred_cp_zoom('PRE_ITER','*','ITCODICE',cp_AbsName(oSource.parent,'oMORAGPRE_1_18'),i_cWhere,'GSAG_MIT',"Raggruppamenti prestazioni",'GSAG_MIT.PRE_ITER_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ITCODICE,ITDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ITCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ITCODICE',oSource.xKey(1))
            select ITCODICE,ITDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MORAGPRE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ITCODICE,ITDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ITCODICE="+cp_ToStrODBC(this.w_MORAGPRE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ITCODICE',this.w_MORAGPRE)
            select ITCODICE,ITDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MORAGPRE = NVL(_Link_.ITCODICE,space(10))
      this.w_DESCRITER = NVL(_Link_.ITDESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_MORAGPRE = space(10)
      endif
      this.w_DESCRITER = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PRE_ITER_IDX,2])+'\'+cp_ToStr(_Link_.ITCODICE,1)
      cp_ShowWarn(i_cKey,this.PRE_ITER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MORAGPRE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MOTIPPRA
  func Link_1_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PRA_TIPI_IDX,3]
    i_lTable = "PRA_TIPI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PRA_TIPI_IDX,2], .t., this.PRA_TIPI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PRA_TIPI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOTIPPRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPR_ATP',True,'PRA_TIPI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TPCODICE like "+cp_ToStrODBC(trim(this.w_MOTIPPRA)+"%");

          i_ret=cp_SQL(i_nConn,"select TPCODICE,TPFLGIUD";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TPCODICE',trim(this.w_MOTIPPRA))
          select TPCODICE,TPFLGIUD;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOTIPPRA)==trim(_Link_.TPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TPFLGIUD like "+cp_ToStrODBC(trim(this.w_MOTIPPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select TPCODICE,TPFLGIUD";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TPFLGIUD like "+cp_ToStr(trim(this.w_MOTIPPRA)+"%");

            select TPCODICE,TPFLGIUD;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MOTIPPRA) and !this.bDontReportError
            deferred_cp_zoom('PRA_TIPI','*','TPCODICE',cp_AbsName(oSource.parent,'oMOTIPPRA_1_36'),i_cWhere,'GSPR_ATP',"Tipi pratica",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TPCODICE,TPFLGIUD";
                     +" from "+i_cTable+" "+i_lTable+" where TPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TPCODICE',oSource.xKey(1))
            select TPCODICE,TPFLGIUD;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOTIPPRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TPCODICE,TPFLGIUD";
                   +" from "+i_cTable+" "+i_lTable+" where TPCODICE="+cp_ToStrODBC(this.w_MOTIPPRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TPCODICE',this.w_MOTIPPRA)
            select TPCODICE,TPFLGIUD;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOTIPPRA = NVL(_Link_.TPCODICE,space(10))
      this.w_FLGIUD = NVL(_Link_.TPFLGIUD,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MOTIPPRA = space(10)
      endif
      this.w_FLGIUD = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PRA_TIPI_IDX,2])+'\'+cp_ToStr(_Link_.TPCODICE,1)
      cp_ShowWarn(i_cKey,this.PRA_TIPI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOTIPPRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_36(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PRA_TIPI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PRA_TIPI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_36.TPCODICE as TPCODICE136"+ ",link_1_36.TPFLGIUD as TPFLGIUD136"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_36 on PROMODEL.MOTIPPRA=link_1_36.TPCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_36"
          i_cKey=i_cKey+'+" and PROMODEL.MOTIPPRA=link_1_36.TPCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MOMATPRA
  func Link_1_38(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PRA_MATE_IDX,3]
    i_lTable = "PRA_MATE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PRA_MATE_IDX,2], .t., this.PRA_MATE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PRA_MATE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOMATPRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPR_AMP',True,'PRA_MATE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MPCODICE like "+cp_ToStrODBC(trim(this.w_MOMATPRA)+"%");

          i_ret=cp_SQL(i_nConn,"select MPCODICE,MP_TIPOL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MPCODICE',trim(this.w_MOMATPRA))
          select MPCODICE,MP_TIPOL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOMATPRA)==trim(_Link_.MPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MP_TIPOL like "+cp_ToStrODBC(trim(this.w_MOMATPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select MPCODICE,MP_TIPOL";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MP_TIPOL like "+cp_ToStr(trim(this.w_MOMATPRA)+"%");

            select MPCODICE,MP_TIPOL;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MOMATPRA) and !this.bDontReportError
            deferred_cp_zoom('PRA_MATE','*','MPCODICE',cp_AbsName(oSource.parent,'oMOMATPRA_1_38'),i_cWhere,'GSPR_AMP',"Materie pratica",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MPCODICE,MP_TIPOL";
                     +" from "+i_cTable+" "+i_lTable+" where MPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MPCODICE',oSource.xKey(1))
            select MPCODICE,MP_TIPOL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOMATPRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MPCODICE,MP_TIPOL";
                   +" from "+i_cTable+" "+i_lTable+" where MPCODICE="+cp_ToStrODBC(this.w_MOMATPRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MPCODICE',this.w_MOMATPRA)
            select MPCODICE,MP_TIPOL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOMATPRA = NVL(_Link_.MPCODICE,space(10))
      this.w_MATETIPOL = NVL(_Link_.MP_TIPOL,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MOMATPRA = space(10)
      endif
      this.w_MATETIPOL = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PRA_MATE_IDX,2])+'\'+cp_ToStr(_Link_.MPCODICE,1)
      cp_ShowWarn(i_cKey,this.PRA_MATE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOMATPRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_38(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PRA_MATE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PRA_MATE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_38.MPCODICE as MPCODICE138"+ ",link_1_38.MP_TIPOL as MP_TIPOL138"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_38 on PROMODEL.MOMATPRA=link_1_38.MPCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_38"
          i_cKey=i_cKey+'+" and PROMODEL.MOMATPRA=link_1_38.MPCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MO__ENTE
  func Link_1_40(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PRA_ENTI_IDX,3]
    i_lTable = "PRA_ENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PRA_ENTI_IDX,2], .t., this.PRA_ENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PRA_ENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MO__ENTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPR_AEP',True,'PRA_ENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" EPCODICE like "+cp_ToStrODBC(trim(this.w_MO__ENTE)+"%");

          i_ret=cp_SQL(i_nConn,"select EPCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by EPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'EPCODICE',trim(this.w_MO__ENTE))
          select EPCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by EPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MO__ENTE)==trim(_Link_.EPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"  like "+cp_ToStrODBC(trim(this.w_MO__ENTE)+"%");

            i_ret=cp_SQL(i_nConn,"select EPCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+"  like "+cp_ToStr(trim(this.w_MO__ENTE)+"%");

            select EPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MO__ENTE) and !this.bDontReportError
            deferred_cp_zoom('PRA_ENTI','*','EPCODICE',cp_AbsName(oSource.parent,'oMO__ENTE_1_40'),i_cWhere,'GSPR_AEP',"Enti pratica",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EPCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where EPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'EPCODICE',oSource.xKey(1))
            select EPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MO__ENTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EPCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where EPCODICE="+cp_ToStrODBC(this.w_MO__ENTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'EPCODICE',this.w_MO__ENTE)
            select EPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MO__ENTE = NVL(_Link_.EPCODICE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_MO__ENTE = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PRA_ENTI_IDX,2])+'\'+cp_ToStr(_Link_.EPCODICE,1)
      cp_ShowWarn(i_cKey,this.PRA_ENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MO__ENTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMOCODICE_1_2.value==this.w_MOCODICE)
      this.oPgFrm.Page1.oPag.oMOCODICE_1_2.value=this.w_MOCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oMODESCRI_1_4.value==this.w_MODESCRI)
      this.oPgFrm.Page1.oPag.oMODESCRI_1_4.value=this.w_MODESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oMOPREFER_1_5.RadioValue()==this.w_MOPREFER)
      this.oPgFrm.Page1.oPag.oMOPREFER_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMOCLASSE_1_6.value==this.w_MOCLASSE)
      this.oPgFrm.Page1.oPag.oMOCLASSE_1_6.value=this.w_MOCLASSE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLA_1_8.value==this.w_DESCLA)
      this.oPgFrm.Page1.oPag.oDESCLA_1_8.value=this.w_DESCLA
    endif
    if not(this.oPgFrm.Page1.oPag.oMOPATMOD_1_9.value==this.w_MOPATMOD)
      this.oPgFrm.Page1.oPag.oMOPATMOD_1_9.value=this.w_MOPATMOD
    endif
    if not(this.oPgFrm.Page1.oPag.oMOQUERY_1_13.value==this.w_MOQUERY)
      this.oPgFrm.Page1.oPag.oMOQUERY_1_13.value=this.w_MOQUERY
    endif
    if not(this.oPgFrm.Page1.oPag.oMOCODPRE_1_17.value==this.w_MOCODPRE)
      this.oPgFrm.Page1.oPag.oMOCODPRE_1_17.value=this.w_MOCODPRE
    endif
    if not(this.oPgFrm.Page1.oPag.oMORAGPRE_1_18.value==this.w_MORAGPRE)
      this.oPgFrm.Page1.oPag.oMORAGPRE_1_18.value=this.w_MORAGPRE
    endif
    if not(this.oPgFrm.Page1.oPag.oMO__NOTE_1_19.value==this.w_MO__NOTE)
      this.oPgFrm.Page1.oPag.oMO__NOTE_1_19.value=this.w_MO__NOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPRE_1_26.value==this.w_DESPRE)
      this.oPgFrm.Page1.oPag.oDESPRE_1_26.value=this.w_DESPRE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRITER_1_30.value==this.w_DESCRITER)
      this.oPgFrm.Page1.oPag.oDESCRITER_1_30.value=this.w_DESCRITER
    endif
    if not(this.oPgFrm.Page1.oPag.oMODTINVA_1_34.value==this.w_MODTINVA)
      this.oPgFrm.Page1.oPag.oMODTINVA_1_34.value=this.w_MODTINVA
    endif
    if not(this.oPgFrm.Page1.oPag.oMODTOBSO_1_35.value==this.w_MODTOBSO)
      this.oPgFrm.Page1.oPag.oMODTOBSO_1_35.value=this.w_MODTOBSO
    endif
    if not(this.oPgFrm.Page1.oPag.oMOTIPPRA_1_36.RadioValue()==this.w_MOTIPPRA)
      this.oPgFrm.Page1.oPag.oMOTIPPRA_1_36.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMOMATPRA_1_38.RadioValue()==this.w_MOMATPRA)
      this.oPgFrm.Page1.oPag.oMOMATPRA_1_38.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMO__ENTE_1_40.RadioValue()==this.w_MO__ENTE)
      this.oPgFrm.Page1.oPag.oMO__ENTE_1_40.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'PROMODEL')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_MODESCRI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMODESCRI_1_4.SetFocus()
            i_bnoObbl = !empty(.w_MODESCRI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_MOCLASSE)) or not(( SUBSTR( CHKPECLA( .w_MOCLASSE, i_CODUTE), 2, 1 ) = 'S' ) and .w_MODARC='F'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMOCLASSE_1_6.SetFocus()
            i_bnoObbl = !empty(.w_MOCLASSE)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Classe documentale non valida")
          case   not(ChkTipArt(.w_RCACODART, "FM-FO-DE") AND (EMPTY(.w_RCADTOBSO) OR .w_RCADTOBSO>=i_datsys))  and not(NOT ISALT() OR ALLTRIM(.w_TABRIF)<>'CAN_TIER')  and (EMPTY(.w_MORAGPRE))  and not(empty(.w_MOCODPRE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMOCODPRE_1_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Prestazione selezionata inesistente o obsoleta")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsut_amd
      if empty(this.w_MOPATMOD)
          Ah_ErrorMsg('Inserire un percorso valido per il modello!',48,'')
          i_bres=.f.
      endif
      if i_bres and IsAlt()
          if not empty(this.w_DIRMOD) or not empty(this.w_QUERYMOD)
            if !(this.w_MOCLASSE==this.w_CLASSE) OR !(this.w_MOQUERY==this.w_QUERYMOD) OR !(alltrim(this.w_DIRMOD)==alltrim(ADDBS(JUSTPATH(this.w_MOPATMOD))))
              i_bres = AH_YesNo('Attenzione, classe documentale, percorso di archiviazione o query associata non congruente con i parametri modelli pratiche. Confermare ugualmente?')
            endif
          endif
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MODESCRI = this.w_MODESCRI
    this.o_PATMOD = this.w_PATMOD
    this.o_MOQUERY = this.w_MOQUERY
    this.o_QUERY = this.w_QUERY
    this.o_MATETIPOL = this.w_MATETIPOL
    return

enddefine

* --- Define pages as container
define class tgsut_amdPag1 as StdContainer
  Width  = 704
  height = 291
  stdWidth  = 704
  stdheight = 291
  resizeXpos=456
  resizeYpos=164
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMOCODICE_1_2 as StdField with uid="UOJXJPRATW",rtseq=2,rtrep=.f.,;
    cFormVar = "w_MOCODICE", cQueryName = "MOCODICE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Chiave progressiva del modello",;
    HelpContextID = 17449227,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=149, Top=12, InputMask=replicate('X',10)

  add object oMODESCRI_1_4 as StdField with uid="QZYRQFATJI",rtseq=3,rtrep=.f.,;
    cFormVar = "w_MODESCRI", cQueryName = "MODESCRI",;
    bObbl = .t. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione del modello",;
    HelpContextID = 200298767,;
   bGlobalFont=.t.,;
    Height=21, Width=325, Left=235, Top=12, InputMask=replicate('X',60)

  add object oMOPREFER_1_5 as StdCheck with uid="SXXVXBHJVJ",rtseq=4,rtrep=.f.,left=576, top=12, caption="Preferenziale",;
    ToolTipText = "Modello preferenziale",;
    HelpContextID = 236851480,;
    cFormVar="w_MOPREFER", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMOPREFER_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oMOPREFER_1_5.GetRadio()
    this.Parent.oContained.w_MOPREFER = this.RadioValue()
    return .t.
  endfunc

  func oMOPREFER_1_5.SetRadio()
    this.Parent.oContained.w_MOPREFER=trim(this.Parent.oContained.w_MOPREFER)
    this.value = ;
      iif(this.Parent.oContained.w_MOPREFER=='S',1,;
      0)
  endfunc

  add object oMOCLASSE_1_6 as StdField with uid="YDMJFORUIW",rtseq=5,rtrep=.f.,;
    cFormVar = "w_MOCLASSE", cQueryName = "MOCLASSE",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Classe documentale non valida",;
    ToolTipText = "Classe documentale",;
    HelpContextID = 181879051,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=149, Top=36, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="PROMCLAS", cZoomOnZoom="gsut_mcd", oKey_1_1="CDCODCLA", oKey_1_2="this.w_MOCLASSE"

  func oMOCLASSE_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOCLASSE_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOCLASSE_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PROMCLAS','*','CDCODCLA',cp_AbsName(this.parent,'oMOCLASSE_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'gsut_mcd',"Classi documentali",'GSUT_AMD.PROMCLAS_VZM',this.parent.oContained
  endproc
  proc oMOCLASSE_1_6.mZoomOnZoom
    local i_obj
    i_obj=gsut_mcd()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CDCODCLA=this.parent.oContained.w_MOCLASSE
     i_obj.ecpSave()
  endproc

  add object oDESCLA_1_8 as StdField with uid="QAYXTXHQLF",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DESCLA", cQueryName = "DESCLA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione classe documentale",;
    HelpContextID = 109103562,;
   bGlobalFont=.t.,;
    Height=21, Width=426, Left=270, Top=36, InputMask=replicate('X',50)

  add object oMOPATMOD_1_9 as StdField with uid="OJGANSGHLZ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_MOPATMOD", cQueryName = "MOPATMOD",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Percorso modello",;
    HelpContextID = 167964406,;
   bGlobalFont=.t.,;
    Height=21, Width=519, Left=149, Top=60, InputMask=replicate('X',254)


  add object oObj_1_11 as cp_askfile with uid="IUIYUHYDTB",left=674, top=60, width=22,height=22,;
    caption='...',;
   bGlobalFont=.t.,;
    var="w_PATMOD",;
    nPag=1;
    , ToolTipText = "Premere per selezionare il file";
    , HelpContextID = 210121258

  add object oMOQUERY_1_13 as StdField with uid="SZORNDXKDR",rtseq=9,rtrep=.f.,;
    cFormVar = "w_MOQUERY", cQueryName = "MOQUERY",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Query collegata al modello",;
    HelpContextID = 98492218,;
   bGlobalFont=.t.,;
    Height=21, Width=519, Left=149, Top=84, InputMask=replicate('X',254)


  add object oObj_1_16 as cp_askfile with uid="ZPYLBBOQOY",left=674, top=84, width=22,height=22,;
    caption='...',;
   bGlobalFont=.t.,;
    var="w_QUERY",;
    nPag=1;
    , ToolTipText = "Premere per selezionare la query collegata";
    , HelpContextID = 210121258

  add object oMOCODPRE_1_17 as StdField with uid="JWBVUUKONI",rtseq=11,rtrep=.f.,;
    cFormVar = "w_MOCODPRE", cQueryName = "MOCODPRE",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Prestazione selezionata inesistente o obsoleta",;
    ToolTipText = "Inserire il codice della prestazione che verr� inclusa nell'inserimento provvisorio delle prestazioni in fase di creazione di un documento",;
    HelpContextID = 134889739,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=149, Top=108, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_BZA", oKey_1_1="CACODICE", oKey_1_2="this.w_MOCODPRE"

  func oMOCODPRE_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_MORAGPRE))
    endwith
   endif
  endfunc

  func oMOCODPRE_1_17.mHide()
    with this.Parent.oContained
      return (NOT ISALT() OR ALLTRIM(.w_TABRIF)<>'CAN_TIER')
    endwith
  endfunc

  func oMOCODPRE_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOCODPRE_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOCODPRE_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oMOCODPRE_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Prestazioni",'Inspre.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oMOCODPRE_1_17.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_MOCODPRE
     i_obj.ecpSave()
  endproc

  add object oMORAGPRE_1_18 as StdField with uid="FNSRWGMUYG",rtseq=12,rtrep=.f.,;
    cFormVar = "w_MORAGPRE", cQueryName = "MORAGPRE",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Inserire il raggruppamento di prestazioni che verranno incluse nell'inserimento provvisorio delle prestazioni in fase di creazione di un documento",;
    HelpContextID = 137179403,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=149, Top=132, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="PRE_ITER", cZoomOnZoom="GSAG_MIT", oKey_1_1="ITCODICE", oKey_1_2="this.w_MORAGPRE"

  func oMORAGPRE_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_MOCODPRE))
    endwith
   endif
  endfunc

  func oMORAGPRE_1_18.mHide()
    with this.Parent.oContained
      return (NOT ISALT() OR ALLTRIM(.w_TABRIF)<>'CAN_TIER')
    endwith
  endfunc

  func oMORAGPRE_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oMORAGPRE_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMORAGPRE_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PRE_ITER','*','ITCODICE',cp_AbsName(this.parent,'oMORAGPRE_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MIT',"Raggruppamenti prestazioni",'GSAG_MIT.PRE_ITER_VZM',this.parent.oContained
  endproc
  proc oMORAGPRE_1_18.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MIT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ITCODICE=this.parent.oContained.w_MORAGPRE
     i_obj.ecpSave()
  endproc

  add object oMO__NOTE_1_19 as StdMemo with uid="ZAOCTNRBTG",rtseq=13,rtrep=.f.,;
    cFormVar = "w_MO__NOTE", cQueryName = "MO__NOTE",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Note",;
    HelpContextID = 129761547,;
   bGlobalFont=.t.,;
    Height=56, Width=491, Left=149, Top=156


  add object oBtn_1_20 as StdButton with uid="WYHHAODCYC",left=648, top=168, width=48,height=45,;
    CpPicture="bmp\codici.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare il modello";
    , HelpContextID = 167525264;
    , Caption='\<apri file';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      with this.Parent.oContained
        GSUT_BMD(this.Parent.oContained,.w_MOPATMOD)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_20.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not (empty(.w_MOPATMOD) or empty(.w_MOCODICE)))
      endwith
    endif
  endfunc

  func oBtn_1_20.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (empty(.w_MOPATMOD) or empty(.w_MOCODICE))
     endwith
    endif
  endfunc

  add object oDESPRE_1_26 as StdField with uid="BGSQMALBPF",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DESPRE", cQueryName = "DESPRE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 34851274,;
   bGlobalFont=.t.,;
    Height=21, Width=391, Left=305, Top=108, InputMask=replicate('X',40)

  func oDESPRE_1_26.mHide()
    with this.Parent.oContained
      return (NOT ISALT() OR ALLTRIM(.w_TABRIF)<>'CAN_TIER')
    endwith
  endfunc

  add object oDESCRITER_1_30 as StdField with uid="UFQGYTGLXW",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESCRITER", cQueryName = "DESCRITER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 31407003,;
   bGlobalFont=.t.,;
    Height=21, Width=461, Left=235, Top=132, InputMask=replicate('X',60)

  func oDESCRITER_1_30.mHide()
    with this.Parent.oContained
      return (NOT ISALT() OR ALLTRIM(.w_TABRIF)<>'CAN_TIER')
    endwith
  endfunc

  add object oMODTINVA_1_34 as StdField with uid="CCHYZDRXUW",rtseq=23,rtrep=.f.,;
    cFormVar = "w_MODTINVA", cQueryName = "MODTINVA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio validit�",;
    HelpContextID = 106909959,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=149, Top=215

  add object oMODTOBSO_1_35 as StdField with uid="OKUTJPPSLZ",rtseq=24,rtrep=.f.,;
    cFormVar = "w_MODTOBSO", cQueryName = "MODTOBSO",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Ultima data di validit�",;
    HelpContextID = 180310293,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=564, Top=215


  add object oMOTIPPRA_1_36 as StdTableCombo with uid="QDRZGNYYZD",rtseq=25,rtrep=.f.,left=149,top=239,width=245,height=21;
    , ToolTipText = "Tipo pratica";
    , HelpContextID = 147149063;
    , cFormVar="w_MOTIPPRA",tablefilter="", bObbl = .f. , nPag = 1;
    , cLinkFile="PRA_TIPI";
    , cTable='PRA_TIPI',cKey='TPCODICE',cValue='TPDESCRI',cOrderBy='TPDESCRI',xDefault=space(10);
  , bGlobalFont=.t.


  func oMOTIPPRA_1_36.mHide()
    with this.Parent.oContained
      return (NOT ISALT() OR ALLTRIM(.w_TABRIF)<>'CAN_TIER')
    endwith
  endfunc

  func oMOTIPPRA_1_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_36('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOTIPPRA_1_36.ecpDrop(oSource)
    this.Parent.oContained.link_1_36('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc


  add object oMOMATPRA_1_38 as StdTableCombo with uid="KLCNQTFGRK",rtseq=27,rtrep=.f.,left=451,top=239,width=245,height=21;
    , ToolTipText = "Materia pratica";
    , HelpContextID = 150790407;
    , cFormVar="w_MOMATPRA",tablefilter="", bObbl = .f. , nPag = 1;
    , cLinkFile="PRA_MATE";
    , cTable='PRA_MATE',cKey='MPCODICE',cValue='MPDESCRI',cOrderBy='MPDESCRI',xDefault=space(10);
  , bGlobalFont=.t.


  func oMOMATPRA_1_38.mHide()
    with this.Parent.oContained
      return (NOT ISALT() OR ALLTRIM(.w_TABRIF)<>'CAN_TIER')
    endwith
  endfunc

  func oMOMATPRA_1_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_38('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOMATPRA_1_38.ecpDrop(oSource)
    this.Parent.oContained.link_1_38('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc


  add object oMO__ENTE_1_40 as StdTableCombo with uid="ATGDRWEVNU",rtseq=29,rtrep=.f.,left=451,top=263,width=245,height=21;
    , ToolTipText = "Ente/ufficio giudiziario pratica";
    , HelpContextID = 103547147;
    , cFormVar="w_MO__ENTE",tablefilter="", bObbl = .f. , nPag = 1;
    , cLinkFile="PRA_ENTI";
    , cTable='QUERY\PRAENTIZOOM.VQR',cKey='EPCODICE',cValue='EPDESCRI',cOrderBy='',xDefault=space(10);
  , bGlobalFont=.t.


  func oMO__ENTE_1_40.mHide()
    with this.Parent.oContained
      return (NOT ISALT() OR ALLTRIM(.w_TABRIF)<>'CAN_TIER')
    endwith
  endfunc

  func oMO__ENTE_1_40.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_40('Part',this)
    endwith
    return bRes
  endfunc

  proc oMO__ENTE_1_40.ecpDrop(oSource)
    this.Parent.oContained.link_1_40('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oStr_1_3 as StdString with uid="JTMSQPYWEW",Visible=.t., Left=10, Top=16,;
    Alignment=1, Width=136, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="YXSIFOGXSM",Visible=.t., Left=1, Top=40,;
    Alignment=1, Width=145, Height=18,;
    Caption="Classe documentale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="YBGFLCQTVZ",Visible=.t., Left=10, Top=64,;
    Alignment=1, Width=136, Height=18,;
    Caption="Percorso:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="PERFLHHBRZ",Visible=.t., Left=10, Top=88,;
    Alignment=1, Width=136, Height=18,;
    Caption="Query collegata:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="RSRFWPZEWQ",Visible=.t., Left=10, Top=156,;
    Alignment=1, Width=136, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="IZQQCWBFQC",Visible=.t., Left=50, Top=111,;
    Alignment=1, Width=96, Height=18,;
    Caption="Cod. prestazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_25.mHide()
    with this.Parent.oContained
      return (NOT ISALT() OR ALLTRIM(.w_TABRIF)<>'CAN_TIER')
    endwith
  endfunc

  add object oStr_1_29 as StdString with uid="TBWNFYAZPJ",Visible=.t., Left=46, Top=134,;
    Alignment=1, Width=100, Height=18,;
    Caption="Raggruppamento:"  ;
  , bGlobalFont=.t.

  func oStr_1_29.mHide()
    with this.Parent.oContained
      return (NOT ISALT() OR ALLTRIM(.w_TABRIF)<>'CAN_TIER')
    endwith
  endfunc

  add object oStr_1_41 as StdString with uid="CKWRSFNLWA",Visible=.t., Left=46, Top=239,;
    Alignment=1, Width=100, Height=15,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  func oStr_1_41.mHide()
    with this.Parent.oContained
      return (NOT ISALT() OR ALLTRIM(.w_TABRIF)<>'CAN_TIER')
    endwith
  endfunc

  add object oStr_1_42 as StdString with uid="NZZOJDNASN",Visible=.t., Left=385, Top=239,;
    Alignment=1, Width=64, Height=15,;
    Caption="Materia:"  ;
  , bGlobalFont=.t.

  func oStr_1_42.mHide()
    with this.Parent.oContained
      return (NOT ISALT() OR ALLTRIM(.w_TABRIF)<>'CAN_TIER')
    endwith
  endfunc

  add object oStr_1_43 as StdString with uid="ZPQFKFPUGN",Visible=.t., Left=385, Top=263,;
    Alignment=1, Width=64, Height=15,;
    Caption="Ente:"  ;
  , bGlobalFont=.t.

  func oStr_1_43.mHide()
    with this.Parent.oContained
      return (NOT ISALT() OR ALLTRIM(.w_TABRIF)<>'CAN_TIER' OR .w_FLGIUD='G')
    endwith
  endfunc

  add object oStr_1_44 as StdString with uid="FDSDASDXIO",Visible=.t., Left=385, Top=263,;
    Alignment=1, Width=64, Height=15,;
    Caption="Uff. Giudiz.:"  ;
  , bGlobalFont=.t.

  func oStr_1_44.mHide()
    with this.Parent.oContained
      return (NOT ISALT() OR ALLTRIM(.w_TABRIF)<>'CAN_TIER' OR .w_FLGIUD<>'G')
    endwith
  endfunc

  add object oStr_1_52 as StdString with uid="XXVHLTXLSK",Visible=.t., Left=433, Top=215,;
    Alignment=1, Width=128, Height=18,;
    Caption="Data obsolescenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="GRDSOUAFEK",Visible=.t., Left=43, Top=215,;
    Alignment=1, Width=103, Height=15,;
    Caption="Data validit�:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_amd','PROMODEL','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MOCODICE=PROMODEL.MOCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut_amd
Procedure AssegQuery
  parameters Obj
  g_Debug_Query = Obj.w_MOQUERY
endproc

Proc RivalorizzoCombo(pParent)
   local obj
   obj = pParent.getCtrl("w_MO__ENTE")
   obj.Clear()
   obj.Init()
   obj.SetRadio()
   * Aggiorniamo la variabile
   pParent.w_MO__ENTE=obj.RadioValue()
   obj = .null.
Endproc
* --- Fine Area Manuale
