* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bc4                                                        *
*              Ricalcola importi c.di costo 3                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_4]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-21                                                      *
* Last revis.: 2000-03-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bc4",oParentObject)
return(i_retval)

define class tgscg_bc4 as StdBatch
  * --- Local variables
  w_RPAR = 0
  w_IMPTOT = 0
  w_RECO = 0
  w_APPO = 0
  w_DIFFER = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Riassegna importi sul Temporaneo (da GSCA_MCA)
    this.w_RPAR = 0
    WITH this.oParentObject.oParentObject
    this.w_IMPTOT = .w_PNIMPDAR - .w_PNIMPAVE
    ENDWITH
    this.oParentObject.w_TOTPAR = 100
    * --- Cicla sul Temporaneo...
    this.oParentObject.w_TOTRIG = 0
    SELECT (this.oParentObject.cTrsName)
    this.w_RECO = recno()
    GO TOP
    SCAN FOR t_MRCODVOC<>SPACE(15) AND t_MRCODICE<>SPACE(15)
    this.oParentObject.w_TOTRIG = this.oParentObject.w_TOTRIG + (t_MRTOTIMP * IIF(t_MR_SEGNO="D", 1, -1))
    ENDSCAN
    this.w_DIFFER = this.w_IMPTOT-this.oParentObject.w_TOTRIG
    GO TOP
    if this.w_DIFFER<>0
      SCAN FOR t_MRCODVOC<>SPACE(15) AND t_MRCODICE<>SPACE(15) AND NOT DELETED()
      * --- Carica il Temporaneo dei Dati e skippa al record successivo
      this.oParentObject.WorkFromTrs()
      * --- Ripartisce gli Importi DARE / AVERE
      if RECNO()=this.w_RECO
        this.w_APPO = this.oParentObject.w_MRTOTIMP+this.w_DIFFER
        this.oParentObject.w_MR_SEGNO = IIF(this.w_APPO<0, "A", "D")
        this.oParentObject.w_MRTOTIMP = ABS(this.w_APPO)
        this.oParentObject.w_IMPRIG = this.oParentObject.w_MRTOTIMP * IIF(this.oParentObject.w_MR_SEGNO="D", 1, -1)
      endif
      * --- Ripartisce i Parametri
      if this.oParentObject.w_MRTOTIMP=0 OR this.oParentObject.w_TOTPAR=0 OR this.w_IMPTOT=0
        this.oParentObject.w_MRPARAME = 0
        this.oParentObject.w_PERCEN = 0
      else
        this.oParentObject.w_MRPARAME = cp_ROUND((this.oParentObject.w_MRTOTIMP * this.oParentObject.w_TOTPAR) / ABS(this.w_IMPTOT), 4)
        this.oParentObject.w_PERCEN = cp_round((this.oParentObject.w_MRPARAME/this.oParentObject.w_TOTPAR) * 100,2)
      endif
      * --- Per calcolo resto
      this.w_RPAR = this.w_RPAR + this.oParentObject.w_MRPARAME
      this.oParentObject.TrsFromWork()
      if I_SRV=" "
        * --- Notifica Riga Variata
        SELECT (this.oParentObject.cTrsName)
        replace I_SRV with "U"
      endif
      ENDSCAN
      if this.w_RPAR<>this.oParentObject.w_TOTPAR
        GO TOP
        * --- Ripartisce il resto sulla prima riga
        this.oParentObject.WorkFromTrs()
        this.oParentObject.w_MRPARAME = this.oParentObject.w_MRPARAME + (this.oParentObject.w_TOTPAR - this.w_RPAR)
        this.oParentObject.TrsFromWork()
      endif
    endif
    if this.w_RECO>0 AND this.w_RECO<=RECCOUNT()
      GOTO this.w_RECO
      this.oParentObject.WorkFromTrs()
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
