* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_sas                                                        *
*              Analisi scostamento                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-21                                                      *
* Last revis.: 2016-11-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgste_sas",oParentObject))

* --- Class definition
define class tgste_sas as StdForm
  Top    = 24
  Left   = 57

  * --- Standard Properties
  Width  = 628
  Height = 300
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-11-14"
  HelpContextID=124400745
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=41

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  AZIENDA_IDX = 0
  VALUTE_IDX = 0
  ESERCIZI_IDX = 0
  CATECOMM_IDX = 0
  AGENTI_IDX = 0
  ZONE_IDX = 0
  cPrg = "gste_sas"
  cComment = "Analisi scostamento"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_STAMPA = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_SCAINI = ctod('  /  /  ')
  w_SCAFIN = ctod('  /  /  ')
  w_DATAELA = ctod('  /  /  ')
  w_TIPOST = space(1)
  o_TIPOST = space(1)
  w_CODINICT = space(3)
  w_CODINIAG = space(5)
  w_CODINIZO = space(3)
  w_CODINI = space(15)
  w_CODFIN = space(15)
  w_CODFINCT = space(3)
  w_CODFINAG = space(5)
  w_CODFINZO = space(3)
  w_DATOBSO = ctod('  /  /  ')
  w_VALUTA = space(3)
  o_VALUTA = space(3)
  w_DATEMU = ctod('  /  /  ')
  w_DECIMALI = 0
  w_CAMBIO1 = 0
  w_CAMBIO = 0
  w_EXCEL = space(1)
  w_EXCEL = space(1)
  w_DESCRI1 = space(40)
  w_DESCRI2 = space(40)
  w_PERCEN = 0
  w_TIPOANALISI1 = space(1)
  w_PAGANT = space(1)
  w_PAGSCA = space(2)
  w_DETTRIG = space(1)
  w_PAGSCA = space(2)
  w_DESCRI3 = space(35)
  w_DESCRI4 = space(35)
  w_SIMVAL = space(5)
  w_PAGBO = space(2)
  w_PAGMA = space(2)
  w_PAGRI = space(2)
  w_PAGRB = space(2)
  w_PAGRD = space(2)
  w_PAGCA = space(2)
  w_PAGRA = space(2)
  w_AZIENDA = space(4)
  * --- Area Manuale = Declare Variables
  * --- gste_sas
  w_ESPOS=""
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgste_sasPag1","gste_sas",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSCAINI_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gste_sas
    WITH THIS.PARENT
       .w_STAMPA=Oparentobject
       IF .w_STAMPA='S'
          .cComment= ah_msgformat("Analisi scostamento")
       ELSE
          .cComment= ah_msgformat("Analisi durata crediti")
       ENDIF
    ENDWITH
    
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='AZIENDA'
    this.cWorkTables[3]='VALUTE'
    this.cWorkTables[4]='ESERCIZI'
    this.cWorkTables[5]='CATECOMM'
    this.cWorkTables[6]='AGENTI'
    this.cWorkTables[7]='ZONE'
    return(this.OpenAllTables(7))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gste_sas
    IF UPPER(g_APPLICATION)<>"ADHOC REVOLUTION"  
    GSTE_BAS (this,.T.)
    ENDIF
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_STAMPA=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_SCAINI=ctod("  /  /  ")
      .w_SCAFIN=ctod("  /  /  ")
      .w_DATAELA=ctod("  /  /  ")
      .w_TIPOST=space(1)
      .w_CODINICT=space(3)
      .w_CODINIAG=space(5)
      .w_CODINIZO=space(3)
      .w_CODINI=space(15)
      .w_CODFIN=space(15)
      .w_CODFINCT=space(3)
      .w_CODFINAG=space(5)
      .w_CODFINZO=space(3)
      .w_DATOBSO=ctod("  /  /  ")
      .w_VALUTA=space(3)
      .w_DATEMU=ctod("  /  /  ")
      .w_DECIMALI=0
      .w_CAMBIO1=0
      .w_CAMBIO=0
      .w_EXCEL=space(1)
      .w_EXCEL=space(1)
      .w_DESCRI1=space(40)
      .w_DESCRI2=space(40)
      .w_PERCEN=0
      .w_TIPOANALISI1=space(1)
      .w_PAGANT=space(1)
      .w_PAGSCA=space(2)
      .w_DETTRIG=space(1)
      .w_PAGSCA=space(2)
      .w_DESCRI3=space(35)
      .w_DESCRI4=space(35)
      .w_SIMVAL=space(5)
      .w_PAGBO=space(2)
      .w_PAGMA=space(2)
      .w_PAGRI=space(2)
      .w_PAGRB=space(2)
      .w_PAGRD=space(2)
      .w_PAGCA=space(2)
      .w_PAGRA=space(2)
      .w_AZIENDA=space(4)
        .w_STAMPA = Oparentobject
        .w_OBTEST = i_DATSYS
        .w_SCAINI = i_datsys-60
        .w_SCAFIN = i_datsys
        .w_DATAELA = i_datsys
        .w_TIPOST = 'C'
        .w_CODINICT = SPACE(3)
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_CODINICT))
          .link_1_7('Full')
        endif
        .w_CODINIAG = SPACE(3)
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_CODINIAG))
          .link_1_8('Full')
        endif
        .w_CODINIZO = SPACE(3)
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_CODINIZO))
          .link_1_9('Full')
        endif
        .w_CODINI = SPACE(15)
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_CODINI))
          .link_1_10('Full')
        endif
        .w_CODFIN = SPACE(15)
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_CODFIN))
          .link_1_11('Full')
        endif
        .w_CODFINCT = SPACE(3)
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_CODFINCT))
          .link_1_12('Full')
        endif
        .w_CODFINAG = SPACE(5)
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_CODFINAG))
          .link_1_13('Full')
        endif
        .w_CODFINZO = SPACE(3)
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_CODFINZO))
          .link_1_14('Full')
        endif
          .DoRTCalc(15,15,.f.)
        .w_VALUTA = g_PERVAL
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_VALUTA))
          .link_1_16('Full')
        endif
          .DoRTCalc(17,19,.f.)
        .w_CAMBIO = IIF(.w_CAMBIO1=0 OR i_DATSYS<.w_DATEMU,GETCAM(.w_VALUTA, i_DATSYS, 7),.w_CAMBIO1)
        .w_EXCEL = 'N'
        .w_EXCEL = 'N'
          .DoRTCalc(23,25,.f.)
        .w_TIPOANALISI1 = 'N'
        .w_PAGANT = ' '
        .w_PAGSCA = 'S'
        .w_DETTRIG = ' '
        .w_PAGSCA = 'S'
          .DoRTCalc(31,33,.f.)
        .w_PAGBO = 'BO'
        .w_PAGMA = 'MA'
        .w_PAGRI = 'RI'
        .w_PAGRB = 'RB'
        .w_PAGRD = 'RD'
        .w_PAGCA = 'CA'
        .w_PAGRA = 'RA'
        .w_AZIENDA = IIF(UPPER(g_APPLICATION)="ADHOC REVOLUTION"    ,  i_codazi   , .w_AZIENDA )
        .DoRTCalc(41,41,.f.)
        if not(empty(.w_AZIENDA))
          .link_1_54('Full')
        endif
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,6,.t.)
        if .o_TIPOST<>.w_TIPOST
            .w_CODINICT = SPACE(3)
          .link_1_7('Full')
        endif
        if .o_TIPOST<>.w_TIPOST
            .w_CODINIAG = SPACE(3)
          .link_1_8('Full')
        endif
        if .o_TIPOST<>.w_TIPOST
            .w_CODINIZO = SPACE(3)
          .link_1_9('Full')
        endif
        if .o_TIPOST<>.w_TIPOST
            .w_CODINI = SPACE(15)
          .link_1_10('Full')
        endif
        if .o_TIPOST<>.w_TIPOST
            .w_CODFIN = SPACE(15)
          .link_1_11('Full')
        endif
        if .o_TIPOST<>.w_TIPOST
            .w_CODFINCT = SPACE(3)
          .link_1_12('Full')
        endif
        if .o_TIPOST<>.w_TIPOST
            .w_CODFINAG = SPACE(5)
          .link_1_13('Full')
        endif
        if .o_TIPOST<>.w_TIPOST
            .w_CODFINZO = SPACE(3)
          .link_1_14('Full')
        endif
        .DoRTCalc(15,19,.t.)
        if .o_VALUTA<>.w_VALUTA
            .w_CAMBIO = IIF(.w_CAMBIO1=0 OR i_DATSYS<.w_DATEMU,GETCAM(.w_VALUTA, i_DATSYS, 7),.w_CAMBIO1)
        endif
        .DoRTCalc(21,40,.t.)
          .link_1_54('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCAMBIO_1_20.enabled = this.oPgFrm.Page1.oPag.oCAMBIO_1_20.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCODINICT_1_7.visible=!this.oPgFrm.Page1.oPag.oCODINICT_1_7.mHide()
    this.oPgFrm.Page1.oPag.oCODINIAG_1_8.visible=!this.oPgFrm.Page1.oPag.oCODINIAG_1_8.mHide()
    this.oPgFrm.Page1.oPag.oCODINIZO_1_9.visible=!this.oPgFrm.Page1.oPag.oCODINIZO_1_9.mHide()
    this.oPgFrm.Page1.oPag.oCODINI_1_10.visible=!this.oPgFrm.Page1.oPag.oCODINI_1_10.mHide()
    this.oPgFrm.Page1.oPag.oCODFIN_1_11.visible=!this.oPgFrm.Page1.oPag.oCODFIN_1_11.mHide()
    this.oPgFrm.Page1.oPag.oCODFINCT_1_12.visible=!this.oPgFrm.Page1.oPag.oCODFINCT_1_12.mHide()
    this.oPgFrm.Page1.oPag.oCODFINAG_1_13.visible=!this.oPgFrm.Page1.oPag.oCODFINAG_1_13.mHide()
    this.oPgFrm.Page1.oPag.oCODFINZO_1_14.visible=!this.oPgFrm.Page1.oPag.oCODFINZO_1_14.mHide()
    this.oPgFrm.Page1.oPag.oEXCEL_1_21.visible=!this.oPgFrm.Page1.oPag.oEXCEL_1_21.mHide()
    this.oPgFrm.Page1.oPag.oEXCEL_1_22.visible=!this.oPgFrm.Page1.oPag.oEXCEL_1_22.mHide()
    this.oPgFrm.Page1.oPag.oDESCRI1_1_23.visible=!this.oPgFrm.Page1.oPag.oDESCRI1_1_23.mHide()
    this.oPgFrm.Page1.oPag.oDESCRI2_1_24.visible=!this.oPgFrm.Page1.oPag.oDESCRI2_1_24.mHide()
    this.oPgFrm.Page1.oPag.oPAGANT_1_27.visible=!this.oPgFrm.Page1.oPag.oPAGANT_1_27.mHide()
    this.oPgFrm.Page1.oPag.oPAGSCA_1_28.visible=!this.oPgFrm.Page1.oPag.oPAGSCA_1_28.mHide()
    this.oPgFrm.Page1.oPag.oPAGSCA_1_30.visible=!this.oPgFrm.Page1.oPag.oPAGSCA_1_30.mHide()
    this.oPgFrm.Page1.oPag.oDESCRI3_1_40.visible=!this.oPgFrm.Page1.oPag.oDESCRI3_1_40.mHide()
    this.oPgFrm.Page1.oPag.oDESCRI4_1_41.visible=!this.oPgFrm.Page1.oPag.oDESCRI4_1_41.mHide()
    this.oPgFrm.Page1.oPag.oPAGRA_1_52.visible=!this.oPgFrm.Page1.oPag.oPAGRA_1_52.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_53.visible=!this.oPgFrm.Page1.oPag.oStr_1_53.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_55.visible=!this.oPgFrm.Page1.oPag.oStr_1_55.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODINICT
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATECOMM_IDX,3]
    i_lTable = "CATECOMM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2], .t., this.CATECOMM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODINICT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACT',True,'CATECOMM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_CODINICT)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_CODINICT))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODINICT)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CTDESCRI like "+cp_ToStrODBC(trim(this.w_CODINICT)+"%");

            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CTDESCRI like "+cp_ToStr(trim(this.w_CODINICT)+"%");

            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODINICT) and !this.bDontReportError
            deferred_cp_zoom('CATECOMM','*','CTCODICE',cp_AbsName(oSource.parent,'oCODINICT_1_7'),i_cWhere,'GSAR_ACT',"Categ.commerciale",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODINICT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_CODINICT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_CODINICT)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODINICT = NVL(_Link_.CTCODICE,space(3))
      this.w_DESCRI3 = NVL(_Link_.CTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODINICT = space(3)
      endif
      this.w_DESCRI3 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODINICT<=.w_CODFINCT OR EMPTY(.w_CODFINCT)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure codice finale minore")
        endif
        this.w_CODINICT = space(3)
        this.w_DESCRI3 = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CATECOMM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODINICT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODINIAG
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODINIAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_CODINIAG)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_CODINIAG))
          select AGCODAGE,AGDESAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODINIAG)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStrODBC(trim(this.w_CODINIAG)+"%");

            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStr(trim(this.w_CODINIAG)+"%");

            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODINIAG) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oCODINIAG_1_8'),i_cWhere,'GSAR_AGE',"Elenco agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODINIAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_CODINIAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_CODINIAG)
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODINIAG = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESCRI3 = NVL(_Link_.AGDESAGE,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODINIAG = space(5)
      endif
      this.w_DESCRI3 = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO) AND (.w_CODINIAG<=.w_CODFINAG OR EMPTY(.w_CODFINAG))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente, obsoleto oppure codice finale maggiore")
        endif
        this.w_CODINIAG = space(5)
        this.w_DESCRI3 = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODINIAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODINIZO
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZONE_IDX,3]
    i_lTable = "ZONE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2], .t., this.ZONE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODINIZO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AZO',True,'ZONE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ZOCODZON like "+cp_ToStrODBC(trim(this.w_CODINIZO)+"%");

          i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON,ZODTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ZOCODZON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ZOCODZON',trim(this.w_CODINIZO))
          select ZOCODZON,ZODESZON,ZODTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ZOCODZON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODINIZO)==trim(_Link_.ZOCODZON) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ZODESZON like "+cp_ToStrODBC(trim(this.w_CODINIZO)+"%");

            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON,ZODTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ZODESZON like "+cp_ToStr(trim(this.w_CODINIZO)+"%");

            select ZOCODZON,ZODESZON,ZODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODINIZO) and !this.bDontReportError
            deferred_cp_zoom('ZONE','*','ZOCODZON',cp_AbsName(oSource.parent,'oCODINIZO_1_9'),i_cWhere,'GSAR_AZO',"Elenco zone",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON,ZODTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',oSource.xKey(1))
            select ZOCODZON,ZODESZON,ZODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODINIZO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON,ZODTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(this.w_CODINIZO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',this.w_CODINIZO)
            select ZOCODZON,ZODESZON,ZODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODINIZO = NVL(_Link_.ZOCODZON,space(3))
      this.w_DESCRI3 = NVL(_Link_.ZODESZON,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ZODTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODINIZO = space(3)
      endif
      this.w_DESCRI3 = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO) AND (.w_CODINIZO<=.w_CODFINZO OR EMPTY(.w_CODFINZO))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente, obsoleto oppure codice finale minore")
        endif
        this.w_CODINIZO = space(3)
        this.w_DESCRI3 = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])+'\'+cp_ToStr(_Link_.ZOCODZON,1)
      cp_ShowWarn(i_cKey,this.ZONE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODINIZO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODINI
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPOST);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPOST;
                     ,'ANCODICE',trim(this.w_CODINI))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODINI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPOST);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODINI)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPOST);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODINI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODINI_1_10'),i_cWhere,'GSAR_BZC',"Clienti / fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPOST<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente, obsoleto oppure codice finale minore")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPOST);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODINI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPOST);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPOST;
                       ,'ANCODICE',this.w_CODINI)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODINI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRI1 = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODINI = space(15)
      endif
      this.w_DESCRI1 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO) AND (.w_CODINI<=.w_CODFIN OR EMPTY(.w_CODFIN))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente, obsoleto oppure codice finale minore")
        endif
        this.w_CODINI = space(15)
        this.w_DESCRI1 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFIN
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPOST);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPOST;
                     ,'ANCODICE',trim(this.w_CODFIN))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFIN)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPOST);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODFIN)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPOST);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODFIN) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODFIN_1_11'),i_cWhere,'GSAR_BZC',"Clienti / fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPOST<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente, obsoleto oppure codice iniziale maggiore")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPOST);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODFIN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPOST);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPOST;
                       ,'ANCODICE',this.w_CODFIN)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFIN = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRI2 = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODFIN = space(15)
      endif
      this.w_DESCRI2 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO) AND (.w_CODINI<=.w_CODFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente, obsoleto oppure codice iniziale maggiore")
        endif
        this.w_CODFIN = space(15)
        this.w_DESCRI2 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFINCT
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATECOMM_IDX,3]
    i_lTable = "CATECOMM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2], .t., this.CATECOMM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFINCT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACT',True,'CATECOMM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_CODFINCT)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_CODFINCT))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFINCT)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CTDESCRI like "+cp_ToStrODBC(trim(this.w_CODFINCT)+"%");

            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CTDESCRI like "+cp_ToStr(trim(this.w_CODFINCT)+"%");

            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODFINCT) and !this.bDontReportError
            deferred_cp_zoom('CATECOMM','*','CTCODICE',cp_AbsName(oSource.parent,'oCODFINCT_1_12'),i_cWhere,'GSAR_ACT',"Categ.commerciale",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFINCT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_CODFINCT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_CODFINCT)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFINCT = NVL(_Link_.CTCODICE,space(3))
      this.w_DESCRI4 = NVL(_Link_.CTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODFINCT = space(3)
      endif
      this.w_DESCRI4 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODINICT<=.w_CODFINCT
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure codice iniziale maggiore")
        endif
        this.w_CODFINCT = space(3)
        this.w_DESCRI4 = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CATECOMM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFINCT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFINAG
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFINAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_CODFINAG)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_CODFINAG))
          select AGCODAGE,AGDESAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFINAG)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStrODBC(trim(this.w_CODFINAG)+"%");

            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStr(trim(this.w_CODFINAG)+"%");

            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODFINAG) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oCODFINAG_1_13'),i_cWhere,'GSAR_AGE',"Elenco agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFINAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_CODFINAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_CODFINAG)
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFINAG = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESCRI4 = NVL(_Link_.AGDESAGE,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODFINAG = space(5)
      endif
      this.w_DESCRI4 = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO) AND (.w_CODINIAG<=.w_CODFINAG)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente, obsoleto oppure codice iniziale maggiore")
        endif
        this.w_CODFINAG = space(5)
        this.w_DESCRI4 = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFINAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFINZO
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZONE_IDX,3]
    i_lTable = "ZONE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2], .t., this.ZONE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFINZO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AZO',True,'ZONE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ZOCODZON like "+cp_ToStrODBC(trim(this.w_CODFINZO)+"%");

          i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON,ZODTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ZOCODZON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ZOCODZON',trim(this.w_CODFINZO))
          select ZOCODZON,ZODESZON,ZODTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ZOCODZON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFINZO)==trim(_Link_.ZOCODZON) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ZODESZON like "+cp_ToStrODBC(trim(this.w_CODFINZO)+"%");

            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON,ZODTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ZODESZON like "+cp_ToStr(trim(this.w_CODFINZO)+"%");

            select ZOCODZON,ZODESZON,ZODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODFINZO) and !this.bDontReportError
            deferred_cp_zoom('ZONE','*','ZOCODZON',cp_AbsName(oSource.parent,'oCODFINZO_1_14'),i_cWhere,'GSAR_AZO',"Elenco zone",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON,ZODTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',oSource.xKey(1))
            select ZOCODZON,ZODESZON,ZODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFINZO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON,ZODTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(this.w_CODFINZO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',this.w_CODFINZO)
            select ZOCODZON,ZODESZON,ZODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFINZO = NVL(_Link_.ZOCODZON,space(3))
      this.w_DESCRI4 = NVL(_Link_.ZODESZON,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ZODTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODFINZO = space(3)
      endif
      this.w_DESCRI4 = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO) AND (.w_CODINIZO<=.w_CODFINZO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente, obsoleto oppure codice iniziale maggiore")
        endif
        this.w_CODFINZO = space(3)
        this.w_DESCRI4 = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])+'\'+cp_ToStr(_Link_.ZOCODZON,1)
      cp_ShowWarn(i_cKey,this.ZONE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFINZO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALUTA
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALUTA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_VALUTA)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VACAOVAL,VADECTOT,VADATEUR,VASIMVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_VALUTA))
          select VACODVAL,VACAOVAL,VADECTOT,VADATEUR,VASIMVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VALUTA)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VACAOVAL like "+cp_ToStrODBC(trim(this.w_VALUTA)+"%");

            i_ret=cp_SQL(i_nConn,"select VACODVAL,VACAOVAL,VADECTOT,VADATEUR,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VACAOVAL like "+cp_ToStr(trim(this.w_VALUTA)+"%");

            select VACODVAL,VACAOVAL,VADECTOT,VADATEUR,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_VALUTA) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oVALUTA_1_16'),i_cWhere,'GSAR_AVL',"Valuta",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VACAOVAL,VADECTOT,VADATEUR,VASIMVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VACAOVAL,VADECTOT,VADATEUR,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALUTA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VACAOVAL,VADECTOT,VADATEUR,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALUTA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALUTA)
            select VACODVAL,VACAOVAL,VADECTOT,VADATEUR,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALUTA = NVL(_Link_.VACODVAL,space(3))
      this.w_CAMBIO1 = NVL(_Link_.VACAOVAL,0)
      this.w_DECIMALI = NVL(_Link_.VADECTOT,0)
      this.w_DATEMU = NVL(cp_ToDate(_Link_.VADATEUR),ctod("  /  /  "))
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_VALUTA = space(3)
      endif
      this.w_CAMBIO1 = 0
      this.w_DECIMALI = 0
      this.w_DATEMU = ctod("  /  /  ")
      this.w_SIMVAL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto")
        endif
        this.w_VALUTA = space(3)
        this.w_CAMBIO1 = 0
        this.w_DECIMALI = 0
        this.w_DATEMU = ctod("  /  /  ")
        this.w_SIMVAL = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALUTA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AZIENDA
  func Link_1_54(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZIENDA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZIENDA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_AZIENDA)
            select AZCODAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZIENDA = NVL(_Link_.AZCODAZI,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_AZIENDA = space(4)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZIENDA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSCAINI_1_3.value==this.w_SCAINI)
      this.oPgFrm.Page1.oPag.oSCAINI_1_3.value=this.w_SCAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oSCAFIN_1_4.value==this.w_SCAFIN)
      this.oPgFrm.Page1.oPag.oSCAFIN_1_4.value=this.w_SCAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAELA_1_5.value==this.w_DATAELA)
      this.oPgFrm.Page1.oPag.oDATAELA_1_5.value=this.w_DATAELA
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOST_1_6.RadioValue()==this.w_TIPOST)
      this.oPgFrm.Page1.oPag.oTIPOST_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODINICT_1_7.value==this.w_CODINICT)
      this.oPgFrm.Page1.oPag.oCODINICT_1_7.value=this.w_CODINICT
    endif
    if not(this.oPgFrm.Page1.oPag.oCODINIAG_1_8.value==this.w_CODINIAG)
      this.oPgFrm.Page1.oPag.oCODINIAG_1_8.value=this.w_CODINIAG
    endif
    if not(this.oPgFrm.Page1.oPag.oCODINIZO_1_9.value==this.w_CODINIZO)
      this.oPgFrm.Page1.oPag.oCODINIZO_1_9.value=this.w_CODINIZO
    endif
    if not(this.oPgFrm.Page1.oPag.oCODINI_1_10.value==this.w_CODINI)
      this.oPgFrm.Page1.oPag.oCODINI_1_10.value=this.w_CODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFIN_1_11.value==this.w_CODFIN)
      this.oPgFrm.Page1.oPag.oCODFIN_1_11.value=this.w_CODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFINCT_1_12.value==this.w_CODFINCT)
      this.oPgFrm.Page1.oPag.oCODFINCT_1_12.value=this.w_CODFINCT
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFINAG_1_13.value==this.w_CODFINAG)
      this.oPgFrm.Page1.oPag.oCODFINAG_1_13.value=this.w_CODFINAG
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFINZO_1_14.value==this.w_CODFINZO)
      this.oPgFrm.Page1.oPag.oCODFINZO_1_14.value=this.w_CODFINZO
    endif
    if not(this.oPgFrm.Page1.oPag.oVALUTA_1_16.value==this.w_VALUTA)
      this.oPgFrm.Page1.oPag.oVALUTA_1_16.value=this.w_VALUTA
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMBIO_1_20.value==this.w_CAMBIO)
      this.oPgFrm.Page1.oPag.oCAMBIO_1_20.value=this.w_CAMBIO
    endif
    if not(this.oPgFrm.Page1.oPag.oEXCEL_1_21.RadioValue()==this.w_EXCEL)
      this.oPgFrm.Page1.oPag.oEXCEL_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oEXCEL_1_22.RadioValue()==this.w_EXCEL)
      this.oPgFrm.Page1.oPag.oEXCEL_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI1_1_23.value==this.w_DESCRI1)
      this.oPgFrm.Page1.oPag.oDESCRI1_1_23.value=this.w_DESCRI1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI2_1_24.value==this.w_DESCRI2)
      this.oPgFrm.Page1.oPag.oDESCRI2_1_24.value=this.w_DESCRI2
    endif
    if not(this.oPgFrm.Page1.oPag.oPERCEN_1_25.value==this.w_PERCEN)
      this.oPgFrm.Page1.oPag.oPERCEN_1_25.value=this.w_PERCEN
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOANALISI1_1_26.RadioValue()==this.w_TIPOANALISI1)
      this.oPgFrm.Page1.oPag.oTIPOANALISI1_1_26.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGANT_1_27.RadioValue()==this.w_PAGANT)
      this.oPgFrm.Page1.oPag.oPAGANT_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGSCA_1_28.RadioValue()==this.w_PAGSCA)
      this.oPgFrm.Page1.oPag.oPAGSCA_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGSCA_1_30.RadioValue()==this.w_PAGSCA)
      this.oPgFrm.Page1.oPag.oPAGSCA_1_30.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI3_1_40.value==this.w_DESCRI3)
      this.oPgFrm.Page1.oPag.oDESCRI3_1_40.value=this.w_DESCRI3
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI4_1_41.value==this.w_DESCRI4)
      this.oPgFrm.Page1.oPag.oDESCRI4_1_41.value=this.w_DESCRI4
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_42.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_42.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGBO_1_45.RadioValue()==this.w_PAGBO)
      this.oPgFrm.Page1.oPag.oPAGBO_1_45.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGMA_1_46.RadioValue()==this.w_PAGMA)
      this.oPgFrm.Page1.oPag.oPAGMA_1_46.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGRI_1_47.RadioValue()==this.w_PAGRI)
      this.oPgFrm.Page1.oPag.oPAGRI_1_47.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGRB_1_48.RadioValue()==this.w_PAGRB)
      this.oPgFrm.Page1.oPag.oPAGRB_1_48.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGRD_1_50.RadioValue()==this.w_PAGRD)
      this.oPgFrm.Page1.oPag.oPAGRD_1_50.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGCA_1_51.RadioValue()==this.w_PAGCA)
      this.oPgFrm.Page1.oPag.oPAGCA_1_51.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGRA_1_52.RadioValue()==this.w_PAGRA)
      this.oPgFrm.Page1.oPag.oPAGRA_1_52.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_SCAINI)) or not(EMPTY(.w_SCAFIN) OR (.w_SCAINI<=.w_SCAFIN)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSCAINI_1_3.SetFocus()
            i_bnoObbl = !empty(.w_SCAINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data iniziale superiore a quella finale")
          case   ((empty(.w_SCAFIN)) or not(EMPTY(.w_SCAFIN) OR (.w_SCAINI<=.w_SCAFIN)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSCAFIN_1_4.SetFocus()
            i_bnoObbl = !empty(.w_SCAFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data iniziale superiore a quella finale")
          case   (empty(.w_DATAELA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATAELA_1_5.SetFocus()
            i_bnoObbl = !empty(.w_DATAELA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_TIPOST))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTIPOST_1_6.SetFocus()
            i_bnoObbl = !empty(.w_TIPOST)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CODINICT<=.w_CODFINCT OR EMPTY(.w_CODFINCT))  and not(.w_TIPOST<>'M')  and not(empty(.w_CODINICT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODINICT_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure codice finale minore")
          case   not((EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO) AND (.w_CODINIAG<=.w_CODFINAG OR EMPTY(.w_CODFINAG)))  and not(.w_TIPOST<>'A')  and not(empty(.w_CODINIAG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODINIAG_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente, obsoleto oppure codice finale maggiore")
          case   not((EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO) AND (.w_CODINIZO<=.w_CODFINZO OR EMPTY(.w_CODFINZO)))  and not(.w_TIPOST<>'Z')  and not(empty(.w_CODINIZO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODINIZO_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente, obsoleto oppure codice finale minore")
          case   not((EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO) AND (.w_CODINI<=.w_CODFIN OR EMPTY(.w_CODFIN)))  and not(NOT (.w_TIPOST='C' OR .w_TIPOST='F'))  and not(empty(.w_CODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODINI_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente, obsoleto oppure codice finale minore")
          case   not((EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO) AND (.w_CODINI<=.w_CODFIN))  and not(NOT (.w_TIPOST='C' OR .w_TIPOST='F'))  and not(empty(.w_CODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFIN_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente, obsoleto oppure codice iniziale maggiore")
          case   not(.w_CODINICT<=.w_CODFINCT)  and not(.w_TIPOST<>'M')  and not(empty(.w_CODFINCT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFINCT_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure codice iniziale maggiore")
          case   not((EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO) AND (.w_CODINIAG<=.w_CODFINAG))  and not(.w_TIPOST<>'A')  and not(empty(.w_CODFINAG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFINAG_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente, obsoleto oppure codice iniziale maggiore")
          case   not((EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO) AND (.w_CODINIZO<=.w_CODFINZO))  and not(.w_TIPOST<>'Z')  and not(empty(.w_CODFINZO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFINZO_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente, obsoleto oppure codice iniziale maggiore")
          case   ((empty(.w_VALUTA)) or not((EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oVALUTA_1_16.SetFocus()
            i_bnoObbl = !empty(.w_VALUTA)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente o obsoleto")
          case   (empty(.w_CAMBIO))  and ((.w_CAMBIO1=0 OR i_DATSYS<.w_DATEMU))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAMBIO_1_20.SetFocus()
            i_bnoObbl = !empty(.w_CAMBIO)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPOST = this.w_TIPOST
    this.o_VALUTA = this.w_VALUTA
    return

enddefine

* --- Define pages as container
define class tgste_sasPag1 as StdContainer
  Width  = 624
  height = 300
  stdWidth  = 624
  stdheight = 300
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSCAINI_1_3 as StdField with uid="DSGXEJKMPR",rtseq=3,rtrep=.f.,;
    cFormVar = "w_SCAINI", cQueryName = "SCAINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data iniziale superiore a quella finale",;
    ToolTipText = "Data inizio filtro sulle scadenze",;
    HelpContextID = 154983386,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=127, Top=19

  func oSCAINI_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_SCAFIN) OR (.w_SCAINI<=.w_SCAFIN))
    endwith
    return bRes
  endfunc

  add object oSCAFIN_1_4 as StdField with uid="FYODLKYDTY",rtseq=4,rtrep=.f.,;
    cFormVar = "w_SCAFIN", cQueryName = "SCAFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data iniziale superiore a quella finale",;
    ToolTipText = "Data fine filtro sulle scadenze",;
    HelpContextID = 76536794,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=315, Top=19

  func oSCAFIN_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_SCAFIN) OR (.w_SCAINI<=.w_SCAFIN))
    endwith
    return bRes
  endfunc

  add object oDATAELA_1_5 as StdField with uid="UVYNCELRAY",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DATAELA", cQueryName = "DATAELA",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data riferimento scostamento partite di apertura",;
    HelpContextID = 114536138,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=514, Top=19


  add object oTIPOST_1_6 as StdCombo with uid="UYBAAQZGHL",rtseq=6,rtrep=.f.,left=127,top=49,width=135,height=21;
    , ToolTipText = "Tipo: cliente, fornitore, categoria commerciale, agente o zona";
    , HelpContextID = 233170378;
    , cFormVar="w_TIPOST",RowSource=""+"Cliente,"+"Fornitore,"+"Categ. commerciale,"+"Agente,"+"Zona", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOST_1_6.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'M',;
    iif(this.value =4,'A',;
    iif(this.value =5,'Z',;
    space(1)))))))
  endfunc
  func oTIPOST_1_6.GetRadio()
    this.Parent.oContained.w_TIPOST = this.RadioValue()
    return .t.
  endfunc

  func oTIPOST_1_6.SetRadio()
    this.Parent.oContained.w_TIPOST=trim(this.Parent.oContained.w_TIPOST)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOST=='C',1,;
      iif(this.Parent.oContained.w_TIPOST=='F',2,;
      iif(this.Parent.oContained.w_TIPOST=='M',3,;
      iif(this.Parent.oContained.w_TIPOST=='A',4,;
      iif(this.Parent.oContained.w_TIPOST=='Z',5,;
      0)))))
  endfunc

  func oTIPOST_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CODINI)
        bRes2=.link_1_10('Full')
      endif
      if .not. empty(.w_CODFIN)
        bRes2=.link_1_11('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oCODINICT_1_7 as StdField with uid="VESHEDJAYJ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CODINICT", cQueryName = "CODINICT",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure codice finale minore",;
    ToolTipText = "Codice categoria commerciale di inizio selezione",;
    HelpContextID = 113467258,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=127, Top=82, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CATECOMM", cZoomOnZoom="GSAR_ACT", oKey_1_1="CTCODICE", oKey_1_2="this.w_CODINICT"

  func oCODINICT_1_7.mHide()
    with this.Parent.oContained
      return (.w_TIPOST<>'M')
    endwith
  endfunc

  func oCODINICT_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODINICT_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODINICT_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATECOMM','*','CTCODICE',cp_AbsName(this.parent,'oCODINICT_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACT',"Categ.commerciale",'',this.parent.oContained
  endproc
  proc oCODINICT_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_CODINICT
     i_obj.ecpSave()
  endproc

  add object oCODINIAG_1_8 as StdField with uid="AUPMBHACSZ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CODINIAG", cQueryName = "CODINIAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente, obsoleto oppure codice finale maggiore",;
    ToolTipText = "Codice agente di inizio selezione",;
    HelpContextID = 154968211,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=127, Top=82, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_CODINIAG"

  func oCODINIAG_1_8.mHide()
    with this.Parent.oContained
      return (.w_TIPOST<>'A')
    endwith
  endfunc

  func oCODINIAG_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODINIAG_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODINIAG_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oCODINIAG_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"Elenco agenti",'',this.parent.oContained
  endproc
  proc oCODINIAG_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_CODINIAG
     i_obj.ecpSave()
  endproc

  add object oCODINIZO_1_9 as StdField with uid="OLMENGYFLM",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CODINIZO", cQueryName = "CODINIZO",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente, obsoleto oppure codice finale minore",;
    ToolTipText = "Codice zona di inizio selezione",;
    HelpContextID = 154968203,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=127, Top=82, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="ZONE", cZoomOnZoom="GSAR_AZO", oKey_1_1="ZOCODZON", oKey_1_2="this.w_CODINIZO"

  func oCODINIZO_1_9.mHide()
    with this.Parent.oContained
      return (.w_TIPOST<>'Z')
    endwith
  endfunc

  func oCODINIZO_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODINIZO_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODINIZO_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZONE','*','ZOCODZON',cp_AbsName(this.parent,'oCODINIZO_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AZO',"Elenco zone",'',this.parent.oContained
  endproc
  proc oCODINIZO_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AZO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ZOCODZON=this.parent.oContained.w_CODINIZO
     i_obj.ecpSave()
  endproc

  add object oCODINI_1_10 as StdField with uid="RPYAZIFBFO",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CODINI", cQueryName = "CODINI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente, obsoleto oppure codice finale minore",;
    ToolTipText = "Codice cliente o fornitore di inizio selezione",;
    HelpContextID = 154968282,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=127, Top=82, cSayPict="p_CLF", cGetPict="p_CLF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPOST", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODINI"

  func oCODINI_1_10.mHide()
    with this.Parent.oContained
      return (NOT (.w_TIPOST='C' OR .w_TIPOST='F'))
    endwith
  endfunc

  func oCODINI_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODINI_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODINI_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPOST)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPOST)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODINI_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti / fornitori",'',this.parent.oContained
  endproc
  proc oCODINI_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPOST
     i_obj.w_ANCODICE=this.parent.oContained.w_CODINI
     i_obj.ecpSave()
  endproc

  add object oCODFIN_1_11 as StdField with uid="TGCBKWQFRT",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CODFIN", cQueryName = "CODFIN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente, obsoleto oppure codice iniziale maggiore",;
    ToolTipText = "Codice cliente o fornitore di fine selezione",;
    HelpContextID = 76521690,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=127, Top=110, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPOST", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODFIN"

  func oCODFIN_1_11.mHide()
    with this.Parent.oContained
      return (NOT (.w_TIPOST='C' OR .w_TIPOST='F'))
    endwith
  endfunc

  func oCODFIN_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFIN_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFIN_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPOST)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPOST)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODFIN_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti / fornitori",'',this.parent.oContained
  endproc
  proc oCODFIN_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPOST
     i_obj.w_ANCODICE=this.parent.oContained.w_CODFIN
     i_obj.ecpSave()
  endproc

  add object oCODFINCT_1_12 as StdField with uid="AQDVOUGFZJ",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CODFINCT", cQueryName = "CODFINCT",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure codice iniziale maggiore",;
    ToolTipText = "Codice categoria commerciale fine selezione",;
    HelpContextID = 191913850,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=127, Top=110, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CATECOMM", cZoomOnZoom="GSAR_ACT", oKey_1_1="CTCODICE", oKey_1_2="this.w_CODFINCT"

  func oCODFINCT_1_12.mHide()
    with this.Parent.oContained
      return (.w_TIPOST<>'M')
    endwith
  endfunc

  func oCODFINCT_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFINCT_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFINCT_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATECOMM','*','CTCODICE',cp_AbsName(this.parent,'oCODFINCT_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACT',"Categ.commerciale",'',this.parent.oContained
  endproc
  proc oCODFINCT_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_CODFINCT
     i_obj.ecpSave()
  endproc

  add object oCODFINAG_1_13 as StdField with uid="ZSOBEFXRZQ",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CODFINAG", cQueryName = "CODFINAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente, obsoleto oppure codice iniziale maggiore",;
    ToolTipText = "Codice agente fine selezione",;
    HelpContextID = 76521619,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=127, Top=110, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_CODFINAG"

  func oCODFINAG_1_13.mHide()
    with this.Parent.oContained
      return (.w_TIPOST<>'A')
    endwith
  endfunc

  func oCODFINAG_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFINAG_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFINAG_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oCODFINAG_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"Elenco agenti",'',this.parent.oContained
  endproc
  proc oCODFINAG_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_CODFINAG
     i_obj.ecpSave()
  endproc

  add object oCODFINZO_1_14 as StdField with uid="QXRCRIIPFB",rtseq=14,rtrep=.f.,;
    cFormVar = "w_CODFINZO", cQueryName = "CODFINZO",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente, obsoleto oppure codice iniziale maggiore",;
    ToolTipText = "Codice zona fine selezione",;
    HelpContextID = 76521611,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=127, Top=110, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="ZONE", cZoomOnZoom="GSAR_AZO", oKey_1_1="ZOCODZON", oKey_1_2="this.w_CODFINZO"

  func oCODFINZO_1_14.mHide()
    with this.Parent.oContained
      return (.w_TIPOST<>'Z')
    endwith
  endfunc

  func oCODFINZO_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFINZO_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFINZO_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZONE','*','ZOCODZON',cp_AbsName(this.parent,'oCODFINZO_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AZO',"Elenco zone",'',this.parent.oContained
  endproc
  proc oCODFINZO_1_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AZO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ZOCODZON=this.parent.oContained.w_CODFINZO
     i_obj.ecpSave()
  endproc

  add object oVALUTA_1_16 as StdField with uid="IUOTZFNJBA",rtseq=16,rtrep=.f.,;
    cFormVar = "w_VALUTA", cQueryName = "VALUTA",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente o obsoleto",;
    ToolTipText = "Codice valuta di stampa",;
    HelpContextID = 13643178,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=127, Top=138, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_VALUTA"

  func oVALUTA_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oVALUTA_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVALUTA_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oVALUTA_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valuta",'',this.parent.oContained
  endproc
  proc oVALUTA_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_VALUTA
     i_obj.ecpSave()
  endproc

  add object oCAMBIO_1_20 as StdField with uid="TVFFCVLDEJ",rtseq=20,rtrep=.f.,;
    cFormVar = "w_CAMBIO", cQueryName = "CAMBIO",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Cambio per conversione valuta",;
    HelpContextID = 59973338,;
   bGlobalFont=.t.,;
    Height=21, Width=107, Left=302, Top=138, cSayPict='"99999.999999"', cGetPict='"99999.999999"'

  func oCAMBIO_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_CAMBIO1=0 OR i_DATSYS<.w_DATEMU))
    endwith
   endif
  endfunc

  add object oEXCEL_1_21 as StdCheck with uid="AUWALBQRGA",rtseq=21,rtrep=.f.,left=433, top=138, caption="Stampa su Excel",;
    ToolTipText = "Se attivo viene lanciata la stampa su Excel",;
    HelpContextID = 39888826,;
    cFormVar="w_EXCEL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oEXCEL_1_21.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oEXCEL_1_21.GetRadio()
    this.Parent.oContained.w_EXCEL = this.RadioValue()
    return .t.
  endfunc

  func oEXCEL_1_21.SetRadio()
    this.Parent.oContained.w_EXCEL=trim(this.Parent.oContained.w_EXCEL)
    this.value = ;
      iif(this.Parent.oContained.w_EXCEL=='S',1,;
      0)
  endfunc

  func oEXCEL_1_21.mHide()
    with this.Parent.oContained
      return (g_office<>'M')
    endwith
  endfunc

  add object oEXCEL_1_22 as StdCheck with uid="GOIERWSDUU",rtseq=22,rtrep=.f.,left=433, top=138, caption="Stampa su calc",;
    ToolTipText = "Se attivo viene lanciata la stampa su calc",;
    HelpContextID = 39888826,;
    cFormVar="w_EXCEL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oEXCEL_1_22.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oEXCEL_1_22.GetRadio()
    this.Parent.oContained.w_EXCEL = this.RadioValue()
    return .t.
  endfunc

  func oEXCEL_1_22.SetRadio()
    this.Parent.oContained.w_EXCEL=trim(this.Parent.oContained.w_EXCEL)
    this.value = ;
      iif(this.Parent.oContained.w_EXCEL=='S',1,;
      0)
  endfunc

  func oEXCEL_1_22.mHide()
    with this.Parent.oContained
      return (g_office='M')
    endwith
  endfunc

  add object oDESCRI1_1_23 as StdField with uid="AURSHWSHEJ",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESCRI1", cQueryName = "DESCRI1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 151108298,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=264, Top=82, InputMask=replicate('X',40)

  func oDESCRI1_1_23.mHide()
    with this.Parent.oContained
      return (NOT (.w_TIPOST='C' OR .w_TIPOST='F'))
    endwith
  endfunc

  add object oDESCRI2_1_24 as StdField with uid="EBVQRNDLGD",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESCRI2", cQueryName = "DESCRI2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 117327158,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=263, Top=110, InputMask=replicate('X',40)

  func oDESCRI2_1_24.mHide()
    with this.Parent.oContained
      return (NOT (.w_TIPOST='C' OR .w_TIPOST='F'))
    endwith
  endfunc

  add object oPERCEN_1_25 as StdField with uid="VAPIZBESVH",rtseq=25,rtrep=.f.,;
    cFormVar = "w_PERCEN", cQueryName = "PERCEN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale di interesse da applicare al calcolo del costo finanziario",;
    HelpContextID = 80857610,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=127, Top=166, cSayPict='"999.999"', cGetPict='"999.999"'

  add object oTIPOANALISI1_1_26 as StdCheck with uid="UOQOCATSTD",rtseq=26,rtrep=.f.,left=127, top=192, caption="Escludi intestatari con saldo opposto",;
    ToolTipText = "Se attivo, la procedura escluder� gli intestatari aventi un importo totale scadenze derivanti da note di credito superiore all'importo delle scadenze derivanti da fatture",;
    HelpContextID = 80739822,;
    cFormVar="w_TIPOANALISI1", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTIPOANALISI1_1_26.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oTIPOANALISI1_1_26.GetRadio()
    this.Parent.oContained.w_TIPOANALISI1 = this.RadioValue()
    return .t.
  endfunc

  func oTIPOANALISI1_1_26.SetRadio()
    this.Parent.oContained.w_TIPOANALISI1=trim(this.Parent.oContained.w_TIPOANALISI1)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOANALISI1=='S',1,;
      0)
  endfunc

  add object oPAGANT_1_27 as StdCheck with uid="PXWNSLAYSF",rtseq=27,rtrep=.f.,left=127, top=210, caption="Interessi su partite di acconto",;
    ToolTipText = "Nel calcolo degli interessi considera anche le partite di acconto",;
    HelpContextID = 239369738,;
    cFormVar="w_PAGANT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAGANT_1_27.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPAGANT_1_27.GetRadio()
    this.Parent.oContained.w_PAGANT = this.RadioValue()
    return .t.
  endfunc

  func oPAGANT_1_27.SetRadio()
    this.Parent.oContained.w_PAGANT=trim(this.Parent.oContained.w_PAGANT)
    this.value = ;
      iif(this.Parent.oContained.w_PAGANT=='S',1,;
      0)
  endfunc

  func oPAGANT_1_27.mHide()
    with this.Parent.oContained
      return (.w_STAMPA<>'C')
    endwith
  endfunc


  add object oPAGSCA_1_28 as StdCombo with uid="IDYINOXXCD",rtseq=28,rtrep=.f.,left=443,top=181,width=147,height=21;
    , ToolTipText = "Data da considerare come data di pagamento delle distinte riba contabilizzate";
    , HelpContextID = 31620618;
    , cFormVar="w_PAGSCA",RowSource=""+"Data scadenza,"+"Data primanota", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPAGSCA_1_28.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'P',;
    space(2))))
  endfunc
  func oPAGSCA_1_28.GetRadio()
    this.Parent.oContained.w_PAGSCA = this.RadioValue()
    return .t.
  endfunc

  func oPAGSCA_1_28.SetRadio()
    this.Parent.oContained.w_PAGSCA=trim(this.Parent.oContained.w_PAGSCA)
    this.value = ;
      iif(this.Parent.oContained.w_PAGSCA=='S',1,;
      iif(this.Parent.oContained.w_PAGSCA=='P',2,;
      0))
  endfunc

  func oPAGSCA_1_28.mHide()
    with this.Parent.oContained
      return (.w_PAGRB <> 'RB' OR UPPER(g_APPLICATION)<>"ADHOC REVOLUTION")
    endwith
  endfunc


  add object oPAGSCA_1_30 as StdCombo with uid="OQPNGUDUDP",rtseq=30,rtrep=.f.,left=443,top=181,width=147,height=21;
    , ToolTipText = "Data da considerare come data di pagamento delle distinte riba e cambiali contabilizzate";
    , HelpContextID = 31620618;
    , cFormVar="w_PAGSCA",RowSource=""+"Data scadenza,"+"Data registrazione", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPAGSCA_1_30.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'P',;
    space(2))))
  endfunc
  func oPAGSCA_1_30.GetRadio()
    this.Parent.oContained.w_PAGSCA = this.RadioValue()
    return .t.
  endfunc

  func oPAGSCA_1_30.SetRadio()
    this.Parent.oContained.w_PAGSCA=trim(this.Parent.oContained.w_PAGSCA)
    this.value = ;
      iif(this.Parent.oContained.w_PAGSCA=='S',1,;
      iif(this.Parent.oContained.w_PAGSCA=='P',2,;
      0))
  endfunc

  func oPAGSCA_1_30.mHide()
    with this.Parent.oContained
      return ((.w_PAGRB <> 'RB' AND .w_PAGRA<>'RA' AND .w_PAGCA<>'CA') OR (UPPER(g_APPLICATION)<>"ADHOC REVOLUTION"  AND .w_ESPOS<>'S'  )  OR .w_STAMPA<>'C' OR  UPPER(g_APPLICATION)="ADHOC REVOLUTION")
    endwith
  endfunc


  add object oBtn_1_31 as StdButton with uid="GJCWDUJHRR",left=509, top=245, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per avviare la stampa";
    , HelpContextID = 234980630;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_31.Click()
      with this.Parent.oContained
        do GSTE_BAS with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_32 as StdButton with uid="HWLBJXNCED",left=562, top=245, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 234980630;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_32.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCRI3_1_40 as StdField with uid="CFPLRUHIYK",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DESCRI3", cQueryName = "DESCRI3",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 117327158,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=192, Top=82, InputMask=replicate('X',35)

  func oDESCRI3_1_40.mHide()
    with this.Parent.oContained
      return (.w_TIPOST='C' OR .w_TIPOST='F')
    endwith
  endfunc

  add object oDESCRI4_1_41 as StdField with uid="ZQLMQCVGFN",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DESCRI4", cQueryName = "DESCRI4",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 117327158,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=192, Top=110, InputMask=replicate('X',35)

  func oDESCRI4_1_41.mHide()
    with this.Parent.oContained
      return (.w_TIPOST='C' OR .w_TIPOST='F')
    endwith
  endfunc

  add object oSIMVAL_1_42 as StdField with uid="YPAJZBLMNL",rtseq=33,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 117380570,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=179, Top=138, InputMask=replicate('X',5)

  add object oPAGBO_1_45 as StdCheck with uid="UHUFUMTJOS",rtseq=34,rtrep=.f.,left=327, top=261, caption="Bonifico",;
    ToolTipText = "Se attivo: seleziona i tipi pagamenti tramite bonifico",;
    HelpContextID = 36929034,;
    cFormVar="w_PAGBO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAGBO_1_45.RadioValue()
    return(iif(this.value =1,'BO',;
    'XX'))
  endfunc
  func oPAGBO_1_45.GetRadio()
    this.Parent.oContained.w_PAGBO = this.RadioValue()
    return .t.
  endfunc

  func oPAGBO_1_45.SetRadio()
    this.Parent.oContained.w_PAGBO=trim(this.Parent.oContained.w_PAGBO)
    this.value = ;
      iif(this.Parent.oContained.w_PAGBO=='BO',1,;
      0)
  endfunc

  add object oPAGMA_1_46 as StdCheck with uid="OXYWDXMXXW",rtseq=35,rtrep=.f.,left=186, top=261, caption="M.AV.",;
    ToolTipText = "Se attivo: seleziona i tipi pagamenti tramite M.AV. (mediante avviso)",;
    HelpContextID = 50888202,;
    cFormVar="w_PAGMA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAGMA_1_46.RadioValue()
    return(iif(this.value =1,'MA',;
    'XX'))
  endfunc
  func oPAGMA_1_46.GetRadio()
    this.Parent.oContained.w_PAGMA = this.RadioValue()
    return .t.
  endfunc

  func oPAGMA_1_46.SetRadio()
    this.Parent.oContained.w_PAGMA=trim(this.Parent.oContained.w_PAGMA)
    this.value = ;
      iif(this.Parent.oContained.w_PAGMA=='MA',1,;
      0)
  endfunc

  add object oPAGRI_1_47 as StdCheck with uid="CIJZFYBCCS",rtseq=36,rtrep=.f.,left=327, top=244, caption="R.I.D.",;
    ToolTipText = "Se attivo: seleziona i tipi pagamenti tramite R.I.D.",;
    HelpContextID = 42171914,;
    cFormVar="w_PAGRI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAGRI_1_47.RadioValue()
    return(iif(this.value =1,'RI',;
    'XX'))
  endfunc
  func oPAGRI_1_47.GetRadio()
    this.Parent.oContained.w_PAGRI = this.RadioValue()
    return .t.
  endfunc

  func oPAGRI_1_47.SetRadio()
    this.Parent.oContained.w_PAGRI=trim(this.Parent.oContained.w_PAGRI)
    this.value = ;
      iif(this.Parent.oContained.w_PAGRI=='RI',1,;
      0)
  endfunc

  add object oPAGRB_1_48 as StdCheck with uid="CCUEDWMQSO",rtseq=37,rtrep=.f.,left=186, top=244, caption="Ricevuta bancaria",;
    ToolTipText = "Stampa le partite con pagamento in ricevute bancarie",;
    HelpContextID = 49511946,;
    cFormVar="w_PAGRB", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAGRB_1_48.RadioValue()
    return(iif(this.value =1,'RB',;
    'XX'))
  endfunc
  func oPAGRB_1_48.GetRadio()
    this.Parent.oContained.w_PAGRB = this.RadioValue()
    return .t.
  endfunc

  func oPAGRB_1_48.SetRadio()
    this.Parent.oContained.w_PAGRB=trim(this.Parent.oContained.w_PAGRB)
    this.value = ;
      iif(this.Parent.oContained.w_PAGRB=='RB',1,;
      0)
  endfunc

  add object oPAGRD_1_50 as StdCheck with uid="WYJEWFDOLH",rtseq=38,rtrep=.f.,left=19, top=244, caption="Rimessa diretta",;
    ToolTipText = "Se attivo: seleziona i tipi pagamenti con rimessa diretta",;
    HelpContextID = 47414794,;
    cFormVar="w_PAGRD", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAGRD_1_50.RadioValue()
    return(iif(this.value =1,'RD',;
    'XX'))
  endfunc
  func oPAGRD_1_50.GetRadio()
    this.Parent.oContained.w_PAGRD = this.RadioValue()
    return .t.
  endfunc

  func oPAGRD_1_50.SetRadio()
    this.Parent.oContained.w_PAGRD=trim(this.Parent.oContained.w_PAGRD)
    this.value = ;
      iif(this.Parent.oContained.w_PAGRD=='RD',1,;
      0)
  endfunc

  add object oPAGCA_1_51 as StdCheck with uid="WMQWIJEKXN",rtseq=39,rtrep=.f.,left=19, top=261, caption="Cambiale",;
    ToolTipText = "Se attivo: seleziona i tipi pagamenti tramite cambiale",;
    HelpContextID = 51543562,;
    cFormVar="w_PAGCA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAGCA_1_51.RadioValue()
    return(iif(this.value =1,'CA',;
    'XX'))
  endfunc
  func oPAGCA_1_51.GetRadio()
    this.Parent.oContained.w_PAGCA = this.RadioValue()
    return .t.
  endfunc

  func oPAGCA_1_51.SetRadio()
    this.Parent.oContained.w_PAGCA=trim(this.Parent.oContained.w_PAGCA)
    this.value = ;
      iif(this.Parent.oContained.w_PAGCA=='CA',1,;
      0)
  endfunc

  add object oPAGRA_1_52 as StdCheck with uid="PRXHUHHTTH",rtseq=40,rtrep=.f.,left=19, top=278, caption="Ri.Ba.",;
    ToolTipText = "Stampa le partite con pagamento in RiBa",;
    HelpContextID = 50560522,;
    cFormVar="w_PAGRA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAGRA_1_52.RadioValue()
    return(iif(this.value =1,'RA',;
    'XX'))
  endfunc
  func oPAGRA_1_52.GetRadio()
    this.Parent.oContained.w_PAGRA = this.RadioValue()
    return .t.
  endfunc

  func oPAGRA_1_52.SetRadio()
    this.Parent.oContained.w_PAGRA=trim(this.Parent.oContained.w_PAGRA)
    this.value = ;
      iif(this.Parent.oContained.w_PAGRA=='RA',1,;
      0)
  endfunc

  func oPAGRA_1_52.mHide()
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)="ADHOC REVOLUTION" )
    endwith
  endfunc

  add object oStr_1_33 as StdString with uid="FSHDYQUGYR",Visible=.t., Left=91, Top=82,;
    Alignment=1, Width=34, Height=15,;
    Caption="Da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="KLMRFFNKBZ",Visible=.t., Left=102, Top=110,;
    Alignment=1, Width=23, Height=15,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="JLIOHXGGCD",Visible=.t., Left=5, Top=20,;
    Alignment=1, Width=120, Height=18,;
    Caption="Da data scadenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="TAAEEVMXSB",Visible=.t., Left=210, Top=20,;
    Alignment=1, Width=103, Height=18,;
    Caption="A data scadenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="QWDXRIPQFA",Visible=.t., Left=45, Top=50,;
    Alignment=1, Width=80, Height=15,;
    Caption="Tipo stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="ODCTKGPVKG",Visible=.t., Left=16, Top=140,;
    Alignment=1, Width=109, Height=18,;
    Caption="Valuta di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="YOEXQOUBTN",Visible=.t., Left=35, Top=166,;
    Alignment=1, Width=90, Height=15,;
    Caption="Perc.interessi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="JPTWMFBPCE",Visible=.t., Left=241, Top=138,;
    Alignment=1, Width=57, Height=18,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="WZKSPJKUAN",Visible=.t., Left=394, Top=20,;
    Alignment=1, Width=117, Height=18,;
    Caption="Data elaborazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="KUNVVILUNM",Visible=.t., Left=283, Top=166,;
    Alignment=1, Width=307, Height=18,;
    Caption="Data pagamento Cambiale - Ricevuta Bancaria - Ri.Ba. :"  ;
  , bGlobalFont=.t.

  func oStr_1_53.mHide()
    with this.Parent.oContained
      return ((.w_PAGRB <> 'RB' AND .w_PAGRA<>'RA'  AND .w_PAGCA<>'CA'  ) OR (.w_ESPOS<>'S' AND UPPER(g_APPLICATION)<>"ADHOC REVOLUTION"  ) OR .w_STAMPA<>'C' OR UPPER(g_APPLICATION) = "ADHOC REVOLUTION")
    endwith
  endfunc

  add object oStr_1_55 as StdString with uid="DFVYVTNPYY",Visible=.t., Left=315, Top=181,;
    Alignment=1, Width=124, Height=18,;
    Caption="Data pagamento Riba:"  ;
  , bGlobalFont=.t.

  func oStr_1_55.mHide()
    with this.Parent.oContained
      return (.w_PAGRB <> 'RB' OR UPPER(g_APPLICATION)<>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oBox_1_49 as StdBox with uid="ANYXGSBBLX",left=1, top=235, width=620,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gste_sas','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
