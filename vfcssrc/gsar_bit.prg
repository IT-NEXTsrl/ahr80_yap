* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bit                                                        *
*              Controllo frequenza INTRA                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_35]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-03-13                                                      *
* Last revis.: 2010-04-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bit",oParentObject)
return(i_retval)

define class tgsar_bit as StdBatch
  * --- Local variables
  Mese = 0
  w_TIPACQ = space(1)
  w_TIPVEN = space(1)
  w_CODAZI = space(5)
  * --- WorkFile variables
  AZIENDA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo Frequenza Intra (da GSAR_MIT)
    this.Mese = 0
    if this.oParentObject.w_PITIPPER = "M"
      this.oParentObject.w_DESPER = space(25)
      if this.oParentObject.w_PIPERRET >= 1 .and. this.oParentObject.w_PIPERRET <= 12
        this.Mese = this.oParentObject.w_PIPERRET
        this.oParentObject.w_DESPER = g_MESE[this.Mese]
      endif
    endif
    if this.oParentObject.w_PITIPPER = "T"
      this.oParentObject.w_DESPER = space(25)
      do case
        case this.oParentObject.w_PIPERRET=1
          this.oParentObject.w_DESPER = ah_Msgformat("Gennaio-Febbraio-Marzo")
        case this.oParentObject.w_PIPERRET=2
          this.oParentObject.w_DESPER = ah_Msgformat("Aprile-Maggio-Giugno")
        case this.oParentObject.w_PIPERRET=3
          this.oParentObject.w_DESPER = ah_Msgformat("Luglio-Agosto-Settembre")
        case this.oParentObject.w_PIPERRET=4
          this.oParentObject.w_DESPER = ah_Msgformat("Ottobre-Novembre-Dicembre")
      endcase
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='AZIENDA'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
