* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsca1bma                                                        *
*              Stampa movimenti di analitica                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_32]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-07-03                                                      *
* Last revis.: 2000-07-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsca1bma",oParentObject)
return(i_retval)

define class tgsca1bma as StdBatch
  * --- Local variables
  w_SIMBOLO = space(5)
  * --- WorkFile variables
  VALUTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa Movimenti di Analitica (da GSCA_SMA)
    * --- Il tipo rappresenta la provenienza del Movimento, pu� valere:
    * --- PN - Prima Nota, AN Mov. Analitica, RA MOVIRIPA, RM
    if empty( this.oParentObject.w_DATA1 ) OR empty( this.oParentObject.w_DATA2 )
      ah_ErrorMsg("Intervallo di date non valido",,"")
      i_retcode = 'stop'
      return
    endif
    do case
      case this.oParentObject.w_ORIGINE="A" AND this.oParentObject.w_PROVCONF="N"
        * --- Movimenti Manuali + documenti
        vq_exec("QUERY\GSCA_SMA.VQR",this,"__TMP__")
      case this.oParentObject.w_ORIGINE="P" AND this.oParentObject.w_TIPO="E"
        * --- Prima Nota e Magazzino
        vq_exec("QUERY\GSCA1SMA.VQR",this,"PNOTA")
        * --- Metto tutto assieme
        SELECT CMNUMREG,CMCOMPET,CMDATREG,CMDESAGG,CMFLGMOV,CMNUMDOC,CMALFDOC,CMDATDOC,;
        MRCODVOC,MRCODICE,MR_SEGNO,MRTOTIMP,MRPARAME,CMCODICE,CMCODSEC,VADECTOT,;
        CMVALNAZ,VCDESCRI,PROVIENE,MRFLRIPA,CCNUMLIV,ORDINE,MRPERIOD,MRCODCOM,;
        VACODVAL,VACAOVAL,MRSERRIF,MRORDRIF,MRNUMRIF,PNTIPCON,PNCODCON,ANDESCRI,;
        CCDESPIA,CNDESCAN,MRINICOM,MRFINCOM,0 as CAMPO1 FROM __TMP__;
        UNION ;
        SELECT CMNUMREG,CMCOMPET,CMDATREG,CMDESAGG,CMFLGMOV,CMNUMDOC,CMALFDOC,CMDATDOC,;
        MRCODVOC,MRCODICE,MR_SEGNO,MRTOTIMP,MRPARAME,CMCODICE,CMCODSEC,VADECTOT,;
        CMVALNAZ,VCDESCRI,PROVIENE,MRFLRIPA,CCNUMLIV,ORDINE,MRPERIOD,MRCODCOM,;
        VACODVAL,VACAOVAL,MRSERRIF,MRORDRIF,MRNUMRIF,PNTIPCON,PNCODCON,ANDESCRI,;
        CCDESPIA,CNDESCAN,MRINICOM,MRFINCOM,1 as CAMPO1 FROM PNOTA;
        ORDER BY 3,1,19,31,9,10,24 INTO CURSOR __TMP__
      otherwise
        * --- Movimenti Manuali + documenti
        vq_exec("QUERY\GSCA_SMA.VQR",this,"__TMP__")
        * --- Prima Nota e Magazzino
        vq_exec("QUERY\GSCA1SMA.VQR",this,"PNOTA")
        SELECT * FROM __TMP__ UNION SELECT * FROM PNOTA INTO CURSOR __TMP__
    endcase
    if this.oParentObject.w_PROVE<>"T"
      * --- Prendo anche quelli ripartiti
      vq_exec("QUERY\GSCA3SMC.VQR",this,"PNOTAMAGA")
      * --- Se Escluso Ripartiti ho gi� escluso gli Originari e ora aggiungo i "Ripartiti"
      * --- Se Tutti ho gli Originari e ora aggiungo i "Ripartiti"
      * --- Metto tutto assieme
      SELECT MRCODICE,MRCODVOC,MR_SEGNO,MRTOTIMP,MRINICOM,;
      MRFINCOM,VACAOVAL,VACODVAL,VCDESCRI,CCDESPIA,TIPO,;
      CCNUMLIV,MRFLRIPA,CHIAVE1,CHIAVE2,CHIAVE3,campo1,0 FROM __TMP__;
      UNION ;
      SELECT MRCODICE,MRCODVOC,MR_SEGNO,MRTOTIMP,MRINICOM,;
      MRFINCOM,VACAOVAL,VACODVAL,VCDESCRI,CCDESPIA,TIPO,;
      CCNUMLIV,MRFLRIPA,CHIAVE1,CHIAVE2,CHIAVE3,2 as campo1,1 FROM PNOTAMAGA;
      ORDER BY 12 DESC,1,2,13 INTO CURSOR __TMP__
    endif
    * --- Leggo il simbolo della valuta di conto - la stampa � comunque espressa nella valuta di conto
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VASIMVAL"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(g_PERVAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VASIMVAL;
        from (i_cTable) where;
            VACODVAL = g_PERVAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_SIMBOLO = NVL(cp_ToDate(_read_.VASIMVAL),cp_NullValue(_read_.VASIMVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Variabili utilizzate nel Report
    L_TIPO=this.oParentObject.w_TIPO
    L_PROVE=this.oParentObject.w_PROVE
    L_ORIGINE=this.oParentObject.w_ORIGINE
    L_PROVCONF=this.oParentObject.w_PROVCONF
    L_MRCODCOM=this.oParentObject.w_MRCODCOM
    L_MRCODICE=this.oParentObject.w_MRCODICE
    L_MRCODVOC=this.oParentObject.w_MRCODVOC
    L_PNCODCON=this.oParentObject.w_PNCODCON
    L_DATA1=this.oParentObject.w_DATA1
    L_DATA2=this.oParentObject.w_DATA2
    L_NUMER1=this.oParentObject.w_NUMER1
    L_NUMER2=this.oParentObject.w_NUMER2
    L_LIVE1=this.oParentObject.w_LIVE1
    L_LIVE2=this.oParentObject.w_LIVE2
    * --- Lancio la Stampa
    CP_CHPRN("QUERY\GSCA_SMA.FRX", " ", this)
    if used("Pnota")
      select Pnota
      use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='VALUTE'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
