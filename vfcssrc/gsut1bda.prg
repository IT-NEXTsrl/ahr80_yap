* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut1bda                                                        *
*              Elimina postit azienda                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-04-27                                                      *
* Last revis.: 2010-04-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_CODAZI
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut1bda",oParentObject,m.w_CODAZI)
return(i_retval)

define class tgsut1bda as StdBatch
  * --- Local variables
  w_CODAZI = space(5)
  w_CODE = space(10)
  w_TABLE = space(50)
  w_TABLECODE = space(50)
  * --- WorkFile variables
  CPWARN_idx=0
  postit_idx=0
  cpwarn_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Cancella postit azienda
    * --- Parametri:
    *     w_CODAZI: Codice azienda
    *     Elimina tutte le associazioni di un postit con i record associati ad un'azienda (cpwarn)
    *     Se un postit non � pi� associato elimina il postit dalla tabella (postit)
    w_CURSOR = SYS(2015)
    VQ_EXEC(".\QUERY\GSUT1KDA", This, w_CURSOR)
    if USED(w_CURSOR)
      SELECT(w_CURSOR)
      SCAN
      this.w_CODE = &w_CURSOR..CODE
      this.w_TABLE = &w_CURSOR..TABLEDEF
      this.w_TABLECODE = &w_CURSOR..TABLECODE
      * --- Se la tabella esiste ho individuato un record che deve essere eliminato dalla cpwarn
      if cp_ExistTableDef(this.w_TABLE)
        * --- Delete from cpwarn
        i_nConn=i_TableProp[this.cpwarn_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.cpwarn_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"warncode = "+cp_ToStrODBC(this.w_CODE);
                +" and tablecode = "+cp_ToStrODBC(this.w_TABLECODE);
                 )
        else
          delete from (i_cTable) where;
                warncode = this.w_CODE;
                and tablecode = this.w_TABLECODE;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      endif
      * --- Read from cpwarn
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.cpwarn_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.cpwarn_idx,2],.t.,this.cpwarn_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "tablecode"+;
          " from "+i_cTable+" cpwarn where ";
              +"warncode = "+cp_ToStrODBC(this.w_CODE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          tablecode;
          from (i_cTable) where;
              warncode = this.w_CODE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_TABLECODE = NVL(cp_ToDate(_read_.tablecode),cp_NullValue(_read_.tablecode))
        use
        if i_Rows=0
          * --- Non ci sono pi� occorrenze elimino il postit anche dalla tabella postit
          * --- Delete from postit
          i_nConn=i_TableProp[this.postit_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.postit_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"code = "+cp_ToStrODBC(this.w_CODE);
                   )
          else
            delete from (i_cTable) where;
                  code = this.w_CODE;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
        endif
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      SELECT(w_CURSOR)
      ENDSCAN
    endif
    * --- Elimino il cursore
    USE IN SELECT(w_CURSOR)
  endproc


  proc Init(oParentObject,w_CODAZI)
    this.w_CODAZI=w_CODAZI
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='CPWARN'
    this.cWorkTables[2]='postit'
    this.cWorkTables[3]='cpwarn'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_CODAZI"
endproc
