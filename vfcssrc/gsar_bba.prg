* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bba                                                        *
*              Carica codici ABI-CAB                                           *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_34]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-11                                                      *
* Last revis.: 2012-04-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOpz
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bba",oParentObject,m.pOpz)
return(i_retval)

define class tgsar_bba as StdBatch
  * --- Local variables
  pOpz = space(1)
  w_ABCODABI = space(5)
  w_ABDESABI = space(80)
  w_FICODABI = space(5)
  w_FICODCAB = space(5)
  w_FIDESFIL = space(40)
  w_FIINDIRI = space(50)
  w_FI___CAP = space(8)
  w_TESBAN = .f.
  w_MESS = space(100)
  * --- WorkFile variables
  BAN_CONTI_idx=0
  DOC_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carica i Codici ABI-CAB nell'archivio delle Banche (da GSAR_ABA)
    * --- Carica i Codici ABI-CAB nell'archivio dei Conti Banche (da GSTE_ACB)
    this.w_ABCODABI = SPACE(5)
    this.w_ABDESABI = SPACE(80)
    this.w_FICODABI = SPACE(5)
    this.w_FICODCAB = SPACE(5)
    this.w_FIDESFIL = SPACE(40)
    this.w_FIINDIRI = SPACE(50)
    this.w_FI___CAP = SPACE(8)
    * --- Ricerca Codici ABI-CAB
    do case
      case this.pOpz="A"
        this.oParentObject.w_BACODABI = SPACE(5)
        vx_exec("QUERY\GSAR_QBA.VZM",this)
        if NOT EMPTY(this.w_ABCODABI)
          * --- Carica Codici
          this.oParentObject.w_BADESBAN = IIF(EMPTY(this.oParentObject.w_BADESBAN), LEFT(this.w_ABDESABI, 50), this.oParentObject.w_BADESBAN)
          this.oParentObject.w_BACODABI = this.w_ABCODABI
          this.oParentObject.w_BADESABI = this.w_ABDESABI
        else
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.pOpz="C"
        this.oParentObject.w_BACODCAB = SPACE(5)
        vx_exec("QUERY\GSAR_QBC.VZM",this)
        if NOT EMPTY(this.w_FICODABI) AND NOT EMPTY(this.w_FICODCAB)
          * --- Carica Codici
          this.oParentObject.w_BACODBAN = IIF(EMPTY(this.oParentObject.w_BACODBAN), this.w_FICODABI+this.w_FICODCAB, this.oParentObject.w_BACODBAN)
          this.oParentObject.w_BACODCAB = this.w_FICODCAB
          this.oParentObject.w_BADESFIL = NVL(this.w_FIDESFIL, SPACE(40))
          this.oParentObject.w_BAINDIRI = NVL(this.w_FIINDIRI, SPACE(50))
          this.oParentObject.w_BA___CAP = RIGHT("00000"+ALLTRIM(NVL(this.w_FI___CAP,"")), 8)
        else
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- pOpz='D' Ricerca il Codice ABI ma � da GSTE_ACB (invece che da GSAR_ABA)
      case this.pOpz="D"
        vx_exec("QUERY\GSAR_QBA.VZM",this)
        if NOT EMPTY(this.w_ABCODABI)
          * --- Carica Codici
          this.oParentObject.w_BACODABI = this.w_ABCODABI
        else
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- pOpz='E' Ricerca il Codice CAB ma � da GSTE_ACB (invece che da GSAR_ABA)
      case this.pOpz="E"
        vx_exec("QUERY\GSAR_QBC.VZM",this)
        if NOT EMPTY(this.w_FICODABI) AND NOT EMPTY(this.w_FICODCAB)
          * --- Carica Codici
          this.oParentObject.w_BACODCAB = this.w_FICODCAB
        else
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.pOpz="F"
        if NOT EMPTY(this.oParentObject.w_BACODABI) AND NOT EMPTY(this.oParentObject.w_BACODCAB)
          * --- Carica Codici In caso di inserimento manuale.
          this.oParentObject.w_BACODBAN = IIF(EMPTY(this.oParentObject.w_BACODBAN), this.oParentObject.w_BACODABI+this.oParentObject.w_BACODCAB, this.oParentObject.w_BACODBAN)
          this.oParentObject.w_BADESBAN = IIF(EMPTY(this.oParentObject.w_BADESBAN), LEFT(this.oParentObject.w_BADESABI, 50), this.oParentObject.w_BADESBAN)
        else
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.pOpz="G"
        * --- Controlli in cancellazione banche
        this.w_TESBAN = .T.
        * --- Select from BAN_CONTI
        i_nConn=i_TableProp[this.BAN_CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.BAN_CONTI_idx,2],.t.,this.BAN_CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select CCCODBAN,CCTIPCON,CCCODCON  from "+i_cTable+" BAN_CONTI ";
              +" where CCCODBAN="+cp_ToStrODBC(this.oParentObject.w_BACODBAN)+"";
               ,"_Curs_BAN_CONTI")
        else
          select CCCODBAN,CCTIPCON,CCCODCON from (i_cTable);
           where CCCODBAN=this.oParentObject.w_BACODBAN;
            into cursor _Curs_BAN_CONTI
        endif
        if used('_Curs_BAN_CONTI')
          select _Curs_BAN_CONTI
          locate for 1=1
          do while not(eof())
          this.w_TESBAN = .F.
          if nvl(_Curs_BAN_CONTI.CCTIPCON," ")="C"
            this.w_MESS = ah_MsgFormat("Impossibile cancellare, banca  presente dettaglio pagamenti del cliente: %1", alltrim(_Curs_BAN_CONTI.CCCODCON))
          else
            this.w_MESS = ah_MsgFormat("Impossibile cancellare, banca  presente dettaglio pagamenti del fornitore: %1", alltrim(_Curs_BAN_CONTI.CCCODCON))
          endif
            select _Curs_BAN_CONTI
            continue
          enddo
          use
        endif
        * --- Select from DOC_MAST
        i_nConn=i_TableProp[this.DOC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" DOC_MAST ";
              +" where MVCODBA2="+cp_ToStrODBC(this.oParentObject.w_BACODBAN)+" OR MVCODBAN="+cp_ToStrODBC(this.oParentObject.w_BACODBAN)+"";
               ,"_Curs_DOC_MAST")
        else
          select * from (i_cTable);
           where MVCODBA2=this.oParentObject.w_BACODBAN OR MVCODBAN=this.oParentObject.w_BACODBAN;
            into cursor _Curs_DOC_MAST
        endif
        if used('_Curs_DOC_MAST')
          select _Curs_DOC_MAST
          locate for 1=1
          do while not(eof())
          this.w_TESBAN = .F.
          this.w_MESS = ah_MsgFormat("La banca che si intende cancellare � presente nei documenti%0Impossibile cancellare")
            select _Curs_DOC_MAST
            continue
          enddo
          use
        endif
        if NOT this.w_TESBAN
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Messaggio di errore (Mancata selezione)
    Ah_ErrorMsg("Attenzione: non � stato selezionato alcun codice.",48,"")
  endproc


  proc Init(oParentObject,pOpz)
    this.pOpz=pOpz
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='BAN_CONTI'
    this.cWorkTables[2]='DOC_MAST'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_BAN_CONTI')
      use in _Curs_BAN_CONTI
    endif
    if used('_Curs_DOC_MAST')
      use in _Curs_DOC_MAST
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOpz"
endproc
