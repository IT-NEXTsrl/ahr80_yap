* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_acf                                                        *
*              Modello F24 contribuente                                        *
*                                                                              *
*      Author: Zucchetti TAM Srl & Zucchetti                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-06-14                                                      *
* Last revis.: 2011-05-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgscg_acf")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgscg_acf")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgscg_acf")
  return

* --- Class definition
define class tgscg_acf as StdPCForm
  Width  = 731
  Height = 305
  Top    = 8
  Left   = 44
  cComment = "Modello F24 contribuente"
  cPrg = "gscg_acf"
  HelpContextID=109716841
  add object cnt as tcgscg_acf
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgscg_acf as PCContext
  w_CFSERIAL = space(10)
  w_COD_SOC = space(5)
  w_ANNRIF = space(4)
  w_COD_PRE = space(5)
  w_NOME_PER = space(40)
  w_INDIRSOC = space(35)
  w_SEDE = space(2)
  w_COD_FIS = space(5)
  w_NOME_SOC = space(40)
  w_INDIRPER = space(30)
  w_COMU_SOC = space(30)
  w_PROV_SOC = space(2)
  w_PROV_PER = space(2)
  w_COMU_PER = space(30)
  w_UNO = 0
  w_PERAZ = space(1)
  w_FISINDIR = space(35)
  w_FISPROV = space(2)
  w_FISLOCAL = space(30)
  w_FISCODES = space(5)
  w_CFDELEGT = space(50)
  w_CFDELEGB = space(50)
  w_CFCODAGE = space(40)
  w_CFAGPROV = space(2)
  w_CFCODFIS = space(16)
  w_CFPERFIS = space(1)
  w_CFRAGSOC = space(80)
  w_CF__NOME = space(25)
  w_CF_SESSO = space(1)
  w_CFDATNAS = space(8)
  w_CFLOCNAS = space(30)
  w_CFPRONAS = space(2)
  w_CFINDIRI = space(30)
  w_CFLOCALI = space(30)
  w_CFPROVIN = space(2)
  w_DATAPP = space(8)
  w_RESCHK = 0
  w_SEDI = space(5)
  w_CFINDIRI2 = space(30)
  w_CFLOCALI2 = space(30)
  w_CFPROVIN2 = space(2)
  proc Save(oFrom)
    this.w_CFSERIAL = oFrom.w_CFSERIAL
    this.w_COD_SOC = oFrom.w_COD_SOC
    this.w_ANNRIF = oFrom.w_ANNRIF
    this.w_COD_PRE = oFrom.w_COD_PRE
    this.w_NOME_PER = oFrom.w_NOME_PER
    this.w_INDIRSOC = oFrom.w_INDIRSOC
    this.w_SEDE = oFrom.w_SEDE
    this.w_COD_FIS = oFrom.w_COD_FIS
    this.w_NOME_SOC = oFrom.w_NOME_SOC
    this.w_INDIRPER = oFrom.w_INDIRPER
    this.w_COMU_SOC = oFrom.w_COMU_SOC
    this.w_PROV_SOC = oFrom.w_PROV_SOC
    this.w_PROV_PER = oFrom.w_PROV_PER
    this.w_COMU_PER = oFrom.w_COMU_PER
    this.w_UNO = oFrom.w_UNO
    this.w_PERAZ = oFrom.w_PERAZ
    this.w_FISINDIR = oFrom.w_FISINDIR
    this.w_FISPROV = oFrom.w_FISPROV
    this.w_FISLOCAL = oFrom.w_FISLOCAL
    this.w_FISCODES = oFrom.w_FISCODES
    this.w_CFDELEGT = oFrom.w_CFDELEGT
    this.w_CFDELEGB = oFrom.w_CFDELEGB
    this.w_CFCODAGE = oFrom.w_CFCODAGE
    this.w_CFAGPROV = oFrom.w_CFAGPROV
    this.w_CFCODFIS = oFrom.w_CFCODFIS
    this.w_CFPERFIS = oFrom.w_CFPERFIS
    this.w_CFRAGSOC = oFrom.w_CFRAGSOC
    this.w_CF__NOME = oFrom.w_CF__NOME
    this.w_CF_SESSO = oFrom.w_CF_SESSO
    this.w_CFDATNAS = oFrom.w_CFDATNAS
    this.w_CFLOCNAS = oFrom.w_CFLOCNAS
    this.w_CFPRONAS = oFrom.w_CFPRONAS
    this.w_CFINDIRI = oFrom.w_CFINDIRI
    this.w_CFLOCALI = oFrom.w_CFLOCALI
    this.w_CFPROVIN = oFrom.w_CFPROVIN
    this.w_DATAPP = oFrom.w_DATAPP
    this.w_RESCHK = oFrom.w_RESCHK
    this.w_SEDI = oFrom.w_SEDI
    this.w_CFINDIRI2 = oFrom.w_CFINDIRI2
    this.w_CFLOCALI2 = oFrom.w_CFLOCALI2
    this.w_CFPROVIN2 = oFrom.w_CFPROVIN2
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_CFSERIAL = this.w_CFSERIAL
    oTo.w_COD_SOC = this.w_COD_SOC
    oTo.w_ANNRIF = this.w_ANNRIF
    oTo.w_COD_PRE = this.w_COD_PRE
    oTo.w_NOME_PER = this.w_NOME_PER
    oTo.w_INDIRSOC = this.w_INDIRSOC
    oTo.w_SEDE = this.w_SEDE
    oTo.w_COD_FIS = this.w_COD_FIS
    oTo.w_NOME_SOC = this.w_NOME_SOC
    oTo.w_INDIRPER = this.w_INDIRPER
    oTo.w_COMU_SOC = this.w_COMU_SOC
    oTo.w_PROV_SOC = this.w_PROV_SOC
    oTo.w_PROV_PER = this.w_PROV_PER
    oTo.w_COMU_PER = this.w_COMU_PER
    oTo.w_UNO = this.w_UNO
    oTo.w_PERAZ = this.w_PERAZ
    oTo.w_FISINDIR = this.w_FISINDIR
    oTo.w_FISPROV = this.w_FISPROV
    oTo.w_FISLOCAL = this.w_FISLOCAL
    oTo.w_FISCODES = this.w_FISCODES
    oTo.w_CFDELEGT = this.w_CFDELEGT
    oTo.w_CFDELEGB = this.w_CFDELEGB
    oTo.w_CFCODAGE = this.w_CFCODAGE
    oTo.w_CFAGPROV = this.w_CFAGPROV
    oTo.w_CFCODFIS = this.w_CFCODFIS
    oTo.w_CFPERFIS = this.w_CFPERFIS
    oTo.w_CFRAGSOC = this.w_CFRAGSOC
    oTo.w_CF__NOME = this.w_CF__NOME
    oTo.w_CF_SESSO = this.w_CF_SESSO
    oTo.w_CFDATNAS = this.w_CFDATNAS
    oTo.w_CFLOCNAS = this.w_CFLOCNAS
    oTo.w_CFPRONAS = this.w_CFPRONAS
    oTo.w_CFINDIRI = this.w_CFINDIRI
    oTo.w_CFLOCALI = this.w_CFLOCALI
    oTo.w_CFPROVIN = this.w_CFPROVIN
    oTo.w_DATAPP = this.w_DATAPP
    oTo.w_RESCHK = this.w_RESCHK
    oTo.w_SEDI = this.w_SEDI
    oTo.w_CFINDIRI2 = this.w_CFINDIRI2
    oTo.w_CFLOCALI2 = this.w_CFLOCALI2
    oTo.w_CFPROVIN2 = this.w_CFPROVIN2
    PCContext::Load(oTo)
enddefine

define class tcgscg_acf as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 731
  Height = 305
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-05-09"
  HelpContextID=109716841
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=41

  * --- Constant Properties
  MODCPAG_IDX = 0
  TITOLARI_IDX = 0
  AZIENDA_IDX = 0
  cpazi_IDX = 0
  SEDIAZIE_IDX = 0
  DAT_IVAN_IDX = 0
  cFile = "MODCPAG"
  cKeySelect = "CFSERIAL"
  cKeyWhere  = "CFSERIAL=this.w_CFSERIAL"
  cKeyWhereODBC = '"CFSERIAL="+cp_ToStrODBC(this.w_CFSERIAL)';

  cKeyWhereODBCqualified = '"MODCPAG.CFSERIAL="+cp_ToStrODBC(this.w_CFSERIAL)';

  cPrg = "gscg_acf"
  cComment = "Modello F24 contribuente"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CFSERIAL = space(10)
  w_COD_SOC = space(5)
  o_COD_SOC = space(5)
  w_ANNRIF = space(4)
  o_ANNRIF = space(4)
  w_COD_PRE = space(5)
  o_COD_PRE = space(5)
  w_NOME_PER = space(40)
  o_NOME_PER = space(40)
  w_INDIRSOC = space(35)
  o_INDIRSOC = space(35)
  w_SEDE = space(2)
  w_COD_FIS = space(5)
  o_COD_FIS = space(5)
  w_NOME_SOC = space(40)
  o_NOME_SOC = space(40)
  w_INDIRPER = space(30)
  o_INDIRPER = space(30)
  w_COMU_SOC = space(30)
  o_COMU_SOC = space(30)
  w_PROV_SOC = space(2)
  o_PROV_SOC = space(2)
  w_PROV_PER = space(2)
  o_PROV_PER = space(2)
  w_COMU_PER = space(30)
  o_COMU_PER = space(30)
  w_UNO = 0
  w_PERAZ = space(1)
  o_PERAZ = space(1)
  w_FISINDIR = space(35)
  w_FISPROV = space(2)
  w_FISLOCAL = space(30)
  w_FISCODES = space(5)
  w_CFDELEGT = space(50)
  w_CFDELEGB = space(50)
  w_CFCODAGE = space(40)
  o_CFCODAGE = space(40)
  w_CFAGPROV = space(2)
  w_CFCODFIS = space(16)
  w_CFPERFIS = space(1)
  o_CFPERFIS = space(1)
  w_CFRAGSOC = space(80)
  w_CF__NOME = space(25)
  w_CF_SESSO = space(1)
  w_CFDATNAS = ctod('  /  /  ')
  w_CFLOCNAS = space(30)
  w_CFPRONAS = space(2)
  w_CFINDIRI = space(30)
  w_CFLOCALI = space(30)
  w_CFPROVIN = space(2)
  w_DATAPP = ctod('  /  /  ')
  o_DATAPP = ctod('  /  /  ')
  w_RESCHK = 0
  w_SEDI = space(5)
  w_CFINDIRI2 = space(30)
  w_CFLOCALI2 = space(30)
  w_CFPROVIN2 = space(2)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_acfPag1","gscg_acf",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).HelpContextID = 51269386
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCFDELEGB_1_22
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='TITOLARI'
    this.cWorkTables[2]='AZIENDA'
    this.cWorkTables[3]='cpazi'
    this.cWorkTables[4]='SEDIAZIE'
    this.cWorkTables[5]='DAT_IVAN'
    this.cWorkTables[6]='MODCPAG'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(6))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MODCPAG_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MODCPAG_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgscg_acf'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from MODCPAG where CFSERIAL=KeySet.CFSERIAL
    *
    i_nConn = i_TableProp[this.MODCPAG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MODCPAG_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MODCPAG')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MODCPAG.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' MODCPAG '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CFSERIAL',this.w_CFSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_COD_SOC = i_CODAZI
        .w_ANNRIF = STR(YEAR(i_DATSYS), 4, 0)
        .w_NOME_PER = space(40)
        .w_INDIRSOC = space(35)
        .w_SEDE = 'DF'
        .w_NOME_SOC = space(40)
        .w_INDIRPER = space(30)
        .w_COMU_SOC = space(30)
        .w_PROV_SOC = space(2)
        .w_PROV_PER = space(2)
        .w_COMU_PER = space(30)
        .w_UNO = 1
        .w_PERAZ = space(1)
        .w_FISINDIR = space(35)
        .w_FISPROV = space(2)
        .w_FISLOCAL = space(30)
        .w_FISCODES = space(5)
        .w_DATAPP = ctod("  /  /  ")
        .w_RESCHK = 0
        .w_SEDI = i_CODAZI
        .w_CFINDIRI2 = space(30)
        .w_CFLOCALI2 = space(30)
        .w_CFPROVIN2 = space(2)
        .w_CFSERIAL = NVL(CFSERIAL,space(10))
          .link_1_2('Load')
          .link_1_3('Load')
        .w_COD_PRE = i_CODAZI
          .link_1_4('Load')
        .w_COD_FIS = i_CODAZI
          .link_1_8('Load')
        .w_CFDELEGT = NVL(CFDELEGT,space(50))
        .w_CFDELEGB = NVL(CFDELEGB,space(50))
        .w_CFCODAGE = NVL(CFCODAGE,space(40))
        .w_CFAGPROV = NVL(CFAGPROV,space(2))
        .w_CFCODFIS = NVL(CFCODFIS,space(16))
        .w_CFPERFIS = NVL(CFPERFIS,space(1))
        .w_CFRAGSOC = NVL(CFRAGSOC,space(80))
        .w_CF__NOME = NVL(CF__NOME,space(25))
        .w_CF_SESSO = NVL(CF_SESSO,space(1))
        .w_CFDATNAS = NVL(cp_ToDate(CFDATNAS),ctod("  /  /  "))
        .w_CFLOCNAS = NVL(CFLOCNAS,space(30))
        .w_CFPRONAS = NVL(CFPRONAS,space(2))
        .w_CFINDIRI = NVL(CFINDIRI,space(30))
        .w_CFLOCALI = NVL(CFLOCALI,space(30))
        .w_CFPROVIN = NVL(CFPROVIN,space(2))
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
          .link_1_57('Load')
        cp_LoadRecExtFlds(this,'MODCPAG')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_CFSERIAL = space(10)
      .w_COD_SOC = space(5)
      .w_ANNRIF = space(4)
      .w_COD_PRE = space(5)
      .w_NOME_PER = space(40)
      .w_INDIRSOC = space(35)
      .w_SEDE = space(2)
      .w_COD_FIS = space(5)
      .w_NOME_SOC = space(40)
      .w_INDIRPER = space(30)
      .w_COMU_SOC = space(30)
      .w_PROV_SOC = space(2)
      .w_PROV_PER = space(2)
      .w_COMU_PER = space(30)
      .w_UNO = 0
      .w_PERAZ = space(1)
      .w_FISINDIR = space(35)
      .w_FISPROV = space(2)
      .w_FISLOCAL = space(30)
      .w_FISCODES = space(5)
      .w_CFDELEGT = space(50)
      .w_CFDELEGB = space(50)
      .w_CFCODAGE = space(40)
      .w_CFAGPROV = space(2)
      .w_CFCODFIS = space(16)
      .w_CFPERFIS = space(1)
      .w_CFRAGSOC = space(80)
      .w_CF__NOME = space(25)
      .w_CF_SESSO = space(1)
      .w_CFDATNAS = ctod("  /  /  ")
      .w_CFLOCNAS = space(30)
      .w_CFPRONAS = space(2)
      .w_CFINDIRI = space(30)
      .w_CFLOCALI = space(30)
      .w_CFPROVIN = space(2)
      .w_DATAPP = ctod("  /  /  ")
      .w_RESCHK = 0
      .w_SEDI = space(5)
      .w_CFINDIRI2 = space(30)
      .w_CFLOCALI2 = space(30)
      .w_CFPROVIN2 = space(2)
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_COD_SOC = i_CODAZI
        .DoRTCalc(2,2,.f.)
          if not(empty(.w_COD_SOC))
          .link_1_2('Full')
          endif
        .w_ANNRIF = STR(YEAR(i_DATSYS), 4, 0)
        .DoRTCalc(3,3,.f.)
          if not(empty(.w_ANNRIF))
          .link_1_3('Full')
          endif
        .w_COD_PRE = i_CODAZI
        .DoRTCalc(4,4,.f.)
          if not(empty(.w_COD_PRE))
          .link_1_4('Full')
          endif
          .DoRTCalc(5,6,.f.)
        .w_SEDE = 'DF'
        .w_COD_FIS = i_CODAZI
        .DoRTCalc(8,8,.f.)
          if not(empty(.w_COD_FIS))
          .link_1_8('Full')
          endif
          .DoRTCalc(9,14,.f.)
        .w_UNO = 1
          .DoRTCalc(16,23,.f.)
        .w_CFAGPROV = iif(empty(.w_CFCODAGE),'  ',.w_CFAGPROV)
          .DoRTCalc(25,25,.f.)
        .w_CFPERFIS = iif(.w_PERAZ='S','S','N')
        .w_CFRAGSOC = iif(.w_PERAZ='S',upper(.w_NOME_PER),upper(.w_NOME_SOC))
        .w_CF__NOME = iif(.w_CFPERFIS='S',upper(.w_CF__NOME),'')
        .w_CF_SESSO = iif(.w_CFPERFIS='S',.w_CF_SESSO,'M')
        .w_CFDATNAS = iif(.w_CFPERFIS='S',.w_CFDATNAS,.w_DATAPP)
        .w_CFLOCNAS = iif(.w_CFPERFIS='S',upper(.w_CFLOCNAS),'')
        .w_CFPRONAS = iif(.w_CFPERFIS='S',upper(.w_CFPRONAS),'  ')
        .w_CFINDIRI = iif(not empty(.w_FISCODES),left(upper(.w_FISINDIR),30),iif(.w_PERAZ='S',iif(empty(upper(.w_INDIRPER)),upper(left(.w_INDIRSOC,30)),left(upper(.w_INDIRPER),30)),upper(left(.w_INDIRSOC,30))))
        .w_CFLOCALI = iif(not empty(.w_FISCODES),left(upper(.w_FISLOCAL),30),iif(.w_PERAZ='S',iif(empty(upper(.w_COMU_PER)),left(upper(.w_COMU_SOC),30),left(upper(.w_COMU_PER),30)),left(upper(.w_COMU_SOC),30)))
        .w_CFPROVIN = iif(not empty(.w_FISCODES),upper(.w_FISPROV),iif(.w_PERAZ='S',iif(empty(upper(.w_PROV_PER)),upper(.w_PROV_SOC),upper(.w_PROV_PER)),upper(.w_PROV_SOC)))
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
          .DoRTCalc(36,37,.f.)
        .w_SEDI = i_CODAZI
        .DoRTCalc(38,38,.f.)
          if not(empty(.w_SEDI))
          .link_1_57('Full')
          endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'MODCPAG')
    this.DoRTCalc(39,41,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gscg_acf
    * Forza aggiornamento del database
    this.bupdated=.t.
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oCFDELEGB_1_22.enabled = i_bVal
      .Page1.oPag.oCFCODAGE_1_23.enabled = i_bVal
      .Page1.oPag.oCFAGPROV_1_24.enabled = i_bVal
      .Page1.oPag.oCFCODFIS_1_25.enabled = i_bVal
      .Page1.oPag.oCFPERFIS_1_26.enabled_(i_bVal)
      .Page1.oPag.oCFRAGSOC_1_27.enabled = i_bVal
      .Page1.oPag.oCF__NOME_1_28.enabled = i_bVal
      .Page1.oPag.oCF_SESSO_1_29.enabled = i_bVal
      .Page1.oPag.oCFDATNAS_1_30.enabled = i_bVal
      .Page1.oPag.oCFLOCNAS_1_31.enabled = i_bVal
      .Page1.oPag.oCFPRONAS_1_32.enabled = i_bVal
      .Page1.oPag.oCFINDIRI_1_33.enabled = i_bVal
      .Page1.oPag.oCFLOCALI_1_34.enabled = i_bVal
      .Page1.oPag.oCFPROVIN_1_35.enabled = i_bVal
      .Page1.oPag.oObj_1_56.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'MODCPAG',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MODCPAG_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFSERIAL,"CFSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFDELEGT,"CFDELEGT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFDELEGB,"CFDELEGB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFCODAGE,"CFCODAGE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFAGPROV,"CFAGPROV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFCODFIS,"CFCODFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFPERFIS,"CFPERFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFRAGSOC,"CFRAGSOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CF__NOME,"CF__NOME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CF_SESSO,"CF_SESSO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFDATNAS,"CFDATNAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFLOCNAS,"CFLOCNAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFPRONAS,"CFPRONAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFINDIRI,"CFINDIRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFLOCALI,"CFLOCALI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFPROVIN,"CFPROVIN",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MODCPAG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MODCPAG_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.MODCPAG_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into MODCPAG
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MODCPAG')
        i_extval=cp_InsertValODBCExtFlds(this,'MODCPAG')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(CFSERIAL,CFDELEGT,CFDELEGB,CFCODAGE,CFAGPROV"+;
                  ",CFCODFIS,CFPERFIS,CFRAGSOC,CF__NOME,CF_SESSO"+;
                  ",CFDATNAS,CFLOCNAS,CFPRONAS,CFINDIRI,CFLOCALI"+;
                  ",CFPROVIN "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_CFSERIAL)+;
                  ","+cp_ToStrODBC(this.w_CFDELEGT)+;
                  ","+cp_ToStrODBC(this.w_CFDELEGB)+;
                  ","+cp_ToStrODBC(this.w_CFCODAGE)+;
                  ","+cp_ToStrODBC(this.w_CFAGPROV)+;
                  ","+cp_ToStrODBC(this.w_CFCODFIS)+;
                  ","+cp_ToStrODBC(this.w_CFPERFIS)+;
                  ","+cp_ToStrODBC(this.w_CFRAGSOC)+;
                  ","+cp_ToStrODBC(this.w_CF__NOME)+;
                  ","+cp_ToStrODBC(this.w_CF_SESSO)+;
                  ","+cp_ToStrODBC(this.w_CFDATNAS)+;
                  ","+cp_ToStrODBC(this.w_CFLOCNAS)+;
                  ","+cp_ToStrODBC(this.w_CFPRONAS)+;
                  ","+cp_ToStrODBC(this.w_CFINDIRI)+;
                  ","+cp_ToStrODBC(this.w_CFLOCALI)+;
                  ","+cp_ToStrODBC(this.w_CFPROVIN)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MODCPAG')
        i_extval=cp_InsertValVFPExtFlds(this,'MODCPAG')
        cp_CheckDeletedKey(i_cTable,0,'CFSERIAL',this.w_CFSERIAL)
        INSERT INTO (i_cTable);
              (CFSERIAL,CFDELEGT,CFDELEGB,CFCODAGE,CFAGPROV,CFCODFIS,CFPERFIS,CFRAGSOC,CF__NOME,CF_SESSO,CFDATNAS,CFLOCNAS,CFPRONAS,CFINDIRI,CFLOCALI,CFPROVIN  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_CFSERIAL;
                  ,this.w_CFDELEGT;
                  ,this.w_CFDELEGB;
                  ,this.w_CFCODAGE;
                  ,this.w_CFAGPROV;
                  ,this.w_CFCODFIS;
                  ,this.w_CFPERFIS;
                  ,this.w_CFRAGSOC;
                  ,this.w_CF__NOME;
                  ,this.w_CF_SESSO;
                  ,this.w_CFDATNAS;
                  ,this.w_CFLOCNAS;
                  ,this.w_CFPRONAS;
                  ,this.w_CFINDIRI;
                  ,this.w_CFLOCALI;
                  ,this.w_CFPROVIN;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.MODCPAG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MODCPAG_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.MODCPAG_IDX,i_nConn)
      *
      * update MODCPAG
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'MODCPAG')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " CFDELEGT="+cp_ToStrODBC(this.w_CFDELEGT)+;
             ",CFDELEGB="+cp_ToStrODBC(this.w_CFDELEGB)+;
             ",CFCODAGE="+cp_ToStrODBC(this.w_CFCODAGE)+;
             ",CFAGPROV="+cp_ToStrODBC(this.w_CFAGPROV)+;
             ",CFCODFIS="+cp_ToStrODBC(this.w_CFCODFIS)+;
             ",CFPERFIS="+cp_ToStrODBC(this.w_CFPERFIS)+;
             ",CFRAGSOC="+cp_ToStrODBC(this.w_CFRAGSOC)+;
             ",CF__NOME="+cp_ToStrODBC(this.w_CF__NOME)+;
             ",CF_SESSO="+cp_ToStrODBC(this.w_CF_SESSO)+;
             ",CFDATNAS="+cp_ToStrODBC(this.w_CFDATNAS)+;
             ",CFLOCNAS="+cp_ToStrODBC(this.w_CFLOCNAS)+;
             ",CFPRONAS="+cp_ToStrODBC(this.w_CFPRONAS)+;
             ",CFINDIRI="+cp_ToStrODBC(this.w_CFINDIRI)+;
             ",CFLOCALI="+cp_ToStrODBC(this.w_CFLOCALI)+;
             ",CFPROVIN="+cp_ToStrODBC(this.w_CFPROVIN)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'MODCPAG')
        i_cWhere = cp_PKFox(i_cTable  ,'CFSERIAL',this.w_CFSERIAL  )
        UPDATE (i_cTable) SET;
              CFDELEGT=this.w_CFDELEGT;
             ,CFDELEGB=this.w_CFDELEGB;
             ,CFCODAGE=this.w_CFCODAGE;
             ,CFAGPROV=this.w_CFAGPROV;
             ,CFCODFIS=this.w_CFCODFIS;
             ,CFPERFIS=this.w_CFPERFIS;
             ,CFRAGSOC=this.w_CFRAGSOC;
             ,CF__NOME=this.w_CF__NOME;
             ,CF_SESSO=this.w_CF_SESSO;
             ,CFDATNAS=this.w_CFDATNAS;
             ,CFLOCNAS=this.w_CFLOCNAS;
             ,CFPRONAS=this.w_CFPRONAS;
             ,CFINDIRI=this.w_CFINDIRI;
             ,CFLOCALI=this.w_CFLOCALI;
             ,CFPROVIN=this.w_CFPROVIN;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.MODCPAG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MODCPAG_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.MODCPAG_IDX,i_nConn)
      *
      * delete MODCPAG
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'CFSERIAL',this.w_CFSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MODCPAG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MODCPAG_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_COD_SOC<>.w_COD_SOC
          .link_1_2('Full')
        endif
        if .o_ANNRIF<>.w_ANNRIF
          .link_1_3('Full')
        endif
        if .o_COD_PRE<>.w_COD_PRE
            .w_COD_PRE = i_CODAZI
          .link_1_4('Full')
        endif
        .DoRTCalc(5,7,.t.)
        if .o_COD_FIS<>.w_COD_FIS
            .w_COD_FIS = i_CODAZI
          .link_1_8('Full')
        endif
        .DoRTCalc(9,23,.t.)
        if .o_CFCODAGE<>.w_CFCODAGE
            .w_CFAGPROV = iif(empty(.w_CFCODAGE),'  ',.w_CFAGPROV)
        endif
        .DoRTCalc(25,25,.t.)
        if .o_PERAZ<>.w_PERAZ
            .w_CFPERFIS = iif(.w_PERAZ='S','S','N')
        endif
        if .o_NOME_SOC<>.w_NOME_SOC.or. .o_NOME_PER<>.w_NOME_PER.or. .o_PERAZ<>.w_PERAZ
            .w_CFRAGSOC = iif(.w_PERAZ='S',upper(.w_NOME_PER),upper(.w_NOME_SOC))
        endif
        if .o_CFPERFIS<>.w_CFPERFIS
            .w_CF__NOME = iif(.w_CFPERFIS='S',upper(.w_CF__NOME),'')
        endif
        if .o_CFPERFIS<>.w_CFPERFIS
            .w_CF_SESSO = iif(.w_CFPERFIS='S',.w_CF_SESSO,'M')
        endif
        if .o_CFPERFIS<>.w_CFPERFIS.or. .o_DATAPP<>.w_DATAPP
            .w_CFDATNAS = iif(.w_CFPERFIS='S',.w_CFDATNAS,.w_DATAPP)
        endif
        if .o_CFPERFIS<>.w_CFPERFIS
            .w_CFLOCNAS = iif(.w_CFPERFIS='S',upper(.w_CFLOCNAS),'')
        endif
        if .o_CFPERFIS<>.w_CFPERFIS
            .w_CFPRONAS = iif(.w_CFPERFIS='S',upper(.w_CFPRONAS),'  ')
        endif
        if .o_PERAZ<>.w_PERAZ.or. .o_INDIRPER<>.w_INDIRPER.or. .o_INDIRSOC<>.w_INDIRSOC
            .w_CFINDIRI = iif(not empty(.w_FISCODES),left(upper(.w_FISINDIR),30),iif(.w_PERAZ='S',iif(empty(upper(.w_INDIRPER)),upper(left(.w_INDIRSOC,30)),left(upper(.w_INDIRPER),30)),upper(left(.w_INDIRSOC,30))))
        endif
        if .o_COMU_SOC<>.w_COMU_SOC.or. .o_COMU_PER<>.w_COMU_PER.or. .o_PERAZ<>.w_PERAZ
            .w_CFLOCALI = iif(not empty(.w_FISCODES),left(upper(.w_FISLOCAL),30),iif(.w_PERAZ='S',iif(empty(upper(.w_COMU_PER)),left(upper(.w_COMU_SOC),30),left(upper(.w_COMU_PER),30)),left(upper(.w_COMU_SOC),30)))
        endif
        if .o_PROV_SOC<>.w_PROV_SOC.or. .o_PROV_PER<>.w_PROV_PER.or. .o_PERAZ<>.w_PERAZ
            .w_CFPROVIN = iif(not empty(.w_FISCODES),upper(.w_FISPROV),iif(.w_PERAZ='S',iif(empty(upper(.w_PROV_PER)),upper(.w_PROV_SOC),upper(.w_PROV_PER)),upper(.w_PROV_SOC)))
        endif
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
        .DoRTCalc(36,37,.t.)
          .link_1_57('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(39,41,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCFAGPROV_1_24.enabled = this.oPgFrm.Page1.oPag.oCFAGPROV_1_24.mCond()
    this.oPgFrm.Page1.oPag.oCF__NOME_1_28.enabled = this.oPgFrm.Page1.oPag.oCF__NOME_1_28.mCond()
    this.oPgFrm.Page1.oPag.oCFDATNAS_1_30.enabled = this.oPgFrm.Page1.oPag.oCFDATNAS_1_30.mCond()
    this.oPgFrm.Page1.oPag.oCFLOCNAS_1_31.enabled = this.oPgFrm.Page1.oPag.oCFLOCNAS_1_31.mCond()
    this.oPgFrm.Page1.oPag.oCFPRONAS_1_32.enabled = this.oPgFrm.Page1.oPag.oCFPRONAS_1_32.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCF_SESSO_1_29.visible=!this.oPgFrm.Page1.oPag.oCF_SESSO_1_29.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_53.visible=!this.oPgFrm.Page1.oPag.oStr_1_53.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_56.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=COD_SOC
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COD_SOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COD_SOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI,AZPERAZI,AZCOFAZI,AZINDAZI,AZLOCAZI,AZPROAZI,AZDESBAN";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_COD_SOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_COD_SOC)
            select AZCODAZI,AZRAGAZI,AZPERAZI,AZCOFAZI,AZINDAZI,AZLOCAZI,AZPROAZI,AZDESBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COD_SOC = NVL(_Link_.AZCODAZI,space(5))
      this.w_NOME_SOC = NVL(_Link_.AZRAGAZI,space(40))
      this.w_PERAZ = NVL(_Link_.AZPERAZI,space(1))
      this.w_CFCODFIS = NVL(_Link_.AZCOFAZI,space(16))
      this.w_INDIRSOC = NVL(_Link_.AZINDAZI,space(35))
      this.w_COMU_SOC = NVL(_Link_.AZLOCAZI,space(30))
      this.w_PROV_SOC = NVL(_Link_.AZPROAZI,space(2))
      this.w_CFDELEGB = NVL(_Link_.AZDESBAN,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_COD_SOC = space(5)
      endif
      this.w_NOME_SOC = space(40)
      this.w_PERAZ = space(1)
      this.w_CFCODFIS = space(16)
      this.w_INDIRSOC = space(35)
      this.w_COMU_SOC = space(30)
      this.w_PROV_SOC = space(2)
      this.w_CFDELEGB = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COD_SOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANNRIF
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DAT_IVAN_IDX,3]
    i_lTable = "DAT_IVAN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DAT_IVAN_IDX,2], .t., this.DAT_IVAN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DAT_IVAN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANNRIF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANNRIF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IACODAZI,IA__ANNO";
                   +" from "+i_cTable+" "+i_lTable+" where IA__ANNO="+cp_ToStrODBC(this.w_ANNRIF);
                   +" and IACODAZI="+cp_ToStrODBC(this.w_COD_SOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IACODAZI',this.w_COD_SOC;
                       ,'IA__ANNO',this.w_ANNRIF)
            select IACODAZI,IA__ANNO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANNRIF = NVL(_Link_.IA__ANNO,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_ANNRIF = space(4)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DAT_IVAN_IDX,2])+'\'+cp_ToStr(_Link_.IACODAZI,1)+'\'+cp_ToStr(_Link_.IA__ANNO,1)
      cp_ShowWarn(i_cKey,this.DAT_IVAN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANNRIF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COD_PRE
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TITOLARI_IDX,3]
    i_lTable = "TITOLARI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2], .t., this.TITOLARI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COD_PRE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COD_PRE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TTCODAZI,TTCOGTIT,TTNOMTIT,TTDATNAS,TTLUONAS,TTPRONAS,TT_SESSO,TTINDIRI,TTLOCTIT,TTPROTIT";
                   +" from "+i_cTable+" "+i_lTable+" where TTCODAZI="+cp_ToStrODBC(this.w_COD_PRE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TTCODAZI',this.w_COD_PRE)
            select TTCODAZI,TTCOGTIT,TTNOMTIT,TTDATNAS,TTLUONAS,TTPRONAS,TT_SESSO,TTINDIRI,TTLOCTIT,TTPROTIT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COD_PRE = NVL(_Link_.TTCODAZI,space(5))
      this.w_NOME_PER = NVL(_Link_.TTCOGTIT,space(40))
      this.w_CF__NOME = NVL(_Link_.TTNOMTIT,space(25))
      this.w_CFDATNAS = NVL(cp_ToDate(_Link_.TTDATNAS),ctod("  /  /  "))
      this.w_CFLOCNAS = NVL(_Link_.TTLUONAS,space(30))
      this.w_CFPRONAS = NVL(_Link_.TTPRONAS,space(2))
      this.w_CF_SESSO = NVL(_Link_.TT_SESSO,space(1))
      this.w_INDIRPER = NVL(_Link_.TTINDIRI,space(30))
      this.w_COMU_PER = NVL(_Link_.TTLOCTIT,space(30))
      this.w_PROV_PER = NVL(_Link_.TTPROTIT,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_COD_PRE = space(5)
      endif
      this.w_NOME_PER = space(40)
      this.w_CF__NOME = space(25)
      this.w_CFDATNAS = ctod("  /  /  ")
      this.w_CFLOCNAS = space(30)
      this.w_CFPRONAS = space(2)
      this.w_CF_SESSO = space(1)
      this.w_INDIRPER = space(30)
      this.w_COMU_PER = space(30)
      this.w_PROV_PER = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2])+'\'+cp_ToStr(_Link_.TTCODAZI,1)
      cp_ShowWarn(i_cKey,this.TITOLARI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COD_PRE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COD_FIS
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SEDIAZIE_IDX,3]
    i_lTable = "SEDIAZIE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2], .t., this.SEDIAZIE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COD_FIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COD_FIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SETIPRIF,SECODAZI,SEINDIRI,SELOCALI,SEPROVIN,SECODDES";
                   +" from "+i_cTable+" "+i_lTable+" where SECODAZI="+cp_ToStrODBC(this.w_COD_FIS);
                   +" and SETIPRIF="+cp_ToStrODBC(this.w_SEDE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SETIPRIF',this.w_SEDE;
                       ,'SECODAZI',this.w_COD_FIS)
            select SETIPRIF,SECODAZI,SEINDIRI,SELOCALI,SEPROVIN,SECODDES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COD_FIS = NVL(_Link_.SECODAZI,space(5))
      this.w_FISINDIR = NVL(_Link_.SEINDIRI,space(35))
      this.w_FISLOCAL = NVL(_Link_.SELOCALI,space(30))
      this.w_FISPROV = NVL(_Link_.SEPROVIN,space(2))
      this.w_FISCODES = NVL(_Link_.SECODDES,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_COD_FIS = space(5)
      endif
      this.w_FISINDIR = space(35)
      this.w_FISLOCAL = space(30)
      this.w_FISPROV = space(2)
      this.w_FISCODES = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])+'\'+cp_ToStr(_Link_.SETIPRIF,1)+'\'+cp_ToStr(_Link_.SECODAZI,1)
      cp_ShowWarn(i_cKey,this.SEDIAZIE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COD_FIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SEDI
  func Link_1_57(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SEDIAZIE_IDX,3]
    i_lTable = "SEDIAZIE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2], .t., this.SEDIAZIE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SEDI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SEDI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SECODAZI,SEINDIRI,SELOCALI,SEPROVIN";
                   +" from "+i_cTable+" "+i_lTable+" where SECODAZI="+cp_ToStrODBC(this.w_SEDI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SECODAZI',this.w_SEDI)
            select SECODAZI,SEINDIRI,SELOCALI,SEPROVIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SEDI = NVL(_Link_.SECODAZI,space(5))
      this.w_CFINDIRI2 = NVL(_Link_.SEINDIRI,space(30))
      this.w_CFLOCALI2 = NVL(_Link_.SELOCALI,space(30))
      this.w_CFPROVIN2 = NVL(_Link_.SEPROVIN,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_SEDI = space(5)
      endif
      this.w_CFINDIRI2 = space(30)
      this.w_CFLOCALI2 = space(30)
      this.w_CFPROVIN2 = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])+'\'+cp_ToStr(_Link_.SECODAZI,1)
      cp_ShowWarn(i_cKey,this.SEDIAZIE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SEDI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCFDELEGB_1_22.value==this.w_CFDELEGB)
      this.oPgFrm.Page1.oPag.oCFDELEGB_1_22.value=this.w_CFDELEGB
    endif
    if not(this.oPgFrm.Page1.oPag.oCFCODAGE_1_23.value==this.w_CFCODAGE)
      this.oPgFrm.Page1.oPag.oCFCODAGE_1_23.value=this.w_CFCODAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oCFAGPROV_1_24.value==this.w_CFAGPROV)
      this.oPgFrm.Page1.oPag.oCFAGPROV_1_24.value=this.w_CFAGPROV
    endif
    if not(this.oPgFrm.Page1.oPag.oCFCODFIS_1_25.value==this.w_CFCODFIS)
      this.oPgFrm.Page1.oPag.oCFCODFIS_1_25.value=this.w_CFCODFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oCFPERFIS_1_26.RadioValue()==this.w_CFPERFIS)
      this.oPgFrm.Page1.oPag.oCFPERFIS_1_26.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCFRAGSOC_1_27.value==this.w_CFRAGSOC)
      this.oPgFrm.Page1.oPag.oCFRAGSOC_1_27.value=this.w_CFRAGSOC
    endif
    if not(this.oPgFrm.Page1.oPag.oCF__NOME_1_28.value==this.w_CF__NOME)
      this.oPgFrm.Page1.oPag.oCF__NOME_1_28.value=this.w_CF__NOME
    endif
    if not(this.oPgFrm.Page1.oPag.oCF_SESSO_1_29.RadioValue()==this.w_CF_SESSO)
      this.oPgFrm.Page1.oPag.oCF_SESSO_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCFDATNAS_1_30.value==this.w_CFDATNAS)
      this.oPgFrm.Page1.oPag.oCFDATNAS_1_30.value=this.w_CFDATNAS
    endif
    if not(this.oPgFrm.Page1.oPag.oCFLOCNAS_1_31.value==this.w_CFLOCNAS)
      this.oPgFrm.Page1.oPag.oCFLOCNAS_1_31.value=this.w_CFLOCNAS
    endif
    if not(this.oPgFrm.Page1.oPag.oCFPRONAS_1_32.value==this.w_CFPRONAS)
      this.oPgFrm.Page1.oPag.oCFPRONAS_1_32.value=this.w_CFPRONAS
    endif
    if not(this.oPgFrm.Page1.oPag.oCFINDIRI_1_33.value==this.w_CFINDIRI)
      this.oPgFrm.Page1.oPag.oCFINDIRI_1_33.value=this.w_CFINDIRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCFLOCALI_1_34.value==this.w_CFLOCALI)
      this.oPgFrm.Page1.oPag.oCFLOCALI_1_34.value=this.w_CFLOCALI
    endif
    if not(this.oPgFrm.Page1.oPag.oCFPROVIN_1_35.value==this.w_CFPROVIN)
      this.oPgFrm.Page1.oPag.oCFPROVIN_1_35.value=this.w_CFPROVIN
    endif
    cp_SetControlsValueExtFlds(this,'MODCPAG')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(iif(EMPTY(.w_CFCODFIS),.T.,iif(.w_CFPERFIS='S',chkcfp(.w_CFCODFIS,'CF'),chkcfp(.w_CFCODFIS,'PI'))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCFCODFIS_1_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gscg_acf
      if i_bRes
        .w_RESCHK=0
        .NotifyEvent('ControllaDati')
        if .w_RESCHK<>0
          i_bRes=.f.
        endif
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_COD_SOC = this.w_COD_SOC
    this.o_ANNRIF = this.w_ANNRIF
    this.o_COD_PRE = this.w_COD_PRE
    this.o_NOME_PER = this.w_NOME_PER
    this.o_INDIRSOC = this.w_INDIRSOC
    this.o_COD_FIS = this.w_COD_FIS
    this.o_NOME_SOC = this.w_NOME_SOC
    this.o_INDIRPER = this.w_INDIRPER
    this.o_COMU_SOC = this.w_COMU_SOC
    this.o_PROV_SOC = this.w_PROV_SOC
    this.o_PROV_PER = this.w_PROV_PER
    this.o_COMU_PER = this.w_COMU_PER
    this.o_PERAZ = this.w_PERAZ
    this.o_CFCODAGE = this.w_CFCODAGE
    this.o_CFPERFIS = this.w_CFPERFIS
    this.o_DATAPP = this.w_DATAPP
    return

enddefine

* --- Define pages as container
define class tgscg_acfPag1 as StdContainer
  Width  = 727
  height = 305
  stdWidth  = 727
  stdheight = 305
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCFDELEGB_1_22 as StdField with uid="EDZUIUBLEO",rtseq=22,rtrep=.f.,;
    cFormVar = "w_CFDELEGB", cQueryName = "CFDELEGB",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Delega banca",;
    HelpContextID = 209754776,;
   bGlobalFont=.t.,;
    Height=21, Width=372, Left=179, Top=13, cSayPict='repl("!",50)', cGetPict='repl("!",50)', InputMask=replicate('X',50)

  add object oCFCODAGE_1_23 as StdField with uid="ODBDZFGSFW",rtseq=23,rtrep=.f.,;
    cFormVar = "w_CFCODAGE", cQueryName = "CFCODAGE",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Nome dell'agenzia di riferimento",;
    HelpContextID = 252269931,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=179, Top=37, cSayPict='repl("!",40)', cGetPict='repl("!",40)', InputMask=replicate('X',40)

  add object oCFAGPROV_1_24 as StdField with uid="GPIQXFSTSS",rtseq=24,rtrep=.f.,;
    cFormVar = "w_CFAGPROV", cQueryName = "CFAGPROV",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia",;
    HelpContextID = 12662140,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=521, Top=37, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oCFAGPROV_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_CFCODAGE))
    endwith
   endif
  endfunc

  add object oCFCODFIS_1_25 as StdField with uid="OVNFASGHGU",rtseq=25,rtrep=.f.,;
    cFormVar = "w_CFCODFIS", cQueryName = "CFCODFIS",;
    bObbl = .f. , nPag = 1, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale dell'azienda",;
    HelpContextID = 200714887,;
   bGlobalFont=.t.,;
    Height=21, Width=136, Left=179, Top=116, cSayPict='repl("!",20)', cGetPict='repl("!",20)', InputMask=replicate('X',16)

  func oCFCODFIS_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(EMPTY(.w_CFCODFIS),.T.,iif(.w_CFPERFIS='S',chkcfp(.w_CFCODFIS,'CF'),chkcfp(.w_CFCODFIS,'PI'))))
    endwith
    return bRes
  endfunc

  add object oCFPERFIS_1_26 as StdRadio with uid="QNHHCOKMEG",rtseq=26,rtrep=.f.,left=441, top=116, width=266,height=17;
    , cFormVar="w_CFPERFIS", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oCFPERFIS_1_26.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Persona fisica"
      this.Buttons(1).HelpContextID = 186636935
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Persona fisica","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Societ�"
      this.Buttons(2).HelpContextID = 186636935
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Societ�","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",17)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oCFPERFIS_1_26.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oCFPERFIS_1_26.GetRadio()
    this.Parent.oContained.w_CFPERFIS = this.RadioValue()
    return .t.
  endfunc

  func oCFPERFIS_1_26.SetRadio()
    this.Parent.oContained.w_CFPERFIS=trim(this.Parent.oContained.w_CFPERFIS)
    this.value = ;
      iif(this.Parent.oContained.w_CFPERFIS=='S',1,;
      iif(this.Parent.oContained.w_CFPERFIS=='N',2,;
      0))
  endfunc

  add object oCFRAGSOC_1_27 as StdField with uid="WPFDGRBNYC",rtseq=27,rtrep=.f.,;
    cFormVar = "w_CFRAGSOC", cQueryName = "CFRAGSOC",;
    bObbl = .f. , nPag = 1, value=space(80), bMultilanguage =  .f.,;
    ToolTipText = "Ragione sociale dell'azienda",;
    HelpContextID = 19678569,;
   bGlobalFont=.t.,;
    Height=21, Width=514, Left=179, Top=145, cSayPict='repl("!",80)', cGetPict='repl("!",80)', InputMask=replicate('X',80)

  add object oCF__NOME_1_28 as StdField with uid="QGIAZGCNMB",rtseq=28,rtrep=.f.,;
    cFormVar = "w_CF__NOME", cQueryName = "CF__NOME",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Nome del titolare in caso di persona fisica",;
    HelpContextID = 38070933,;
   bGlobalFont=.t.,;
    Height=21, Width=208, Left=179, Top=173, cSayPict='repl("!",25)', cGetPict='repl("!",25)', InputMask=replicate('X',25)

  func oCF__NOME_1_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFPERFIS='S')
    endwith
   endif
  endfunc


  add object oCF_SESSO_1_29 as StdCombo with uid="EQZTNWGFNO",rtseq=29,rtrep=.f.,left=489,top=173,width=110,height=21;
    , ToolTipText = "Sesso del titolare in caso di persona fisica";
    , HelpContextID = 18814325;
    , cFormVar="w_CF_SESSO",RowSource=""+"Maschio,"+"Femmina", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCF_SESSO_1_29.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oCF_SESSO_1_29.GetRadio()
    this.Parent.oContained.w_CF_SESSO = this.RadioValue()
    return .t.
  endfunc

  func oCF_SESSO_1_29.SetRadio()
    this.Parent.oContained.w_CF_SESSO=trim(this.Parent.oContained.w_CF_SESSO)
    this.value = ;
      iif(this.Parent.oContained.w_CF_SESSO=='M',1,;
      iif(this.Parent.oContained.w_CF_SESSO=='F',2,;
      0))
  endfunc

  func oCF_SESSO_1_29.mHide()
    with this.Parent.oContained
      return (.w_CFPERFIS='N')
    endwith
  endfunc

  add object oCFDATNAS_1_30 as StdField with uid="LBDUZSBPDM",rtseq=30,rtrep=.f.,;
    cFormVar = "w_CFDATNAS", cQueryName = "CFDATNAS",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita del titolare in caso di persona fisica",;
    HelpContextID = 217802105,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=179, Top=207

  func oCFDATNAS_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFPERFIS='S')
    endwith
   endif
  endfunc

  add object oCFLOCNAS_1_31 as StdField with uid="TAEJEWFIBZ",rtseq=31,rtrep=.f.,;
    cFormVar = "w_CFLOCNAS", cQueryName = "CFLOCNAS",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Luogo di nascita del titolare in caso di persona fisica",;
    HelpContextID = 200926585,;
   bGlobalFont=.t.,;
    Height=21, Width=248, Left=378, Top=207, cSayPict='repl("!",30)', cGetPict='repl("!",30)', InputMask=replicate('X',30)

  func oCFLOCNAS_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFPERFIS='S')
    endwith
   endif
  endfunc

  add object oCFPRONAS_1_32 as StdField with uid="AHNSNBNYXP",rtseq=32,rtrep=.f.,;
    cFormVar = "w_CFPRONAS", cQueryName = "CFPRONAS",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia",;
    HelpContextID = 213722489,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=691, Top=207, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oCFPRONAS_1_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFPERFIS='S')
    endwith
   endif
  endfunc

  add object oCFINDIRI_1_33 as StdField with uid="PNIBKBWKLL",rtseq=33,rtrep=.f.,;
    cFormVar = "w_CFINDIRI", cQueryName = "CFINDIRI",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo del domicilio fiscale azienda (se assente, indirizzo principale azienda)",;
    HelpContextID = 118011247,;
   bGlobalFont=.t.,;
    Height=21, Width=248, Left=67, Top=275, cSayPict='repl("!",30)', cGetPict='repl("!",30)', InputMask=replicate('X',30)

  add object oCFLOCALI_1_34 as StdField with uid="DEJGDWTAEA",rtseq=34,rtrep=.f.,;
    cFormVar = "w_CFLOCALI", cQueryName = "CFLOCALI",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Comune del domicilio fiscale azienda (se assente, comune dell'indirizzo principale azienda)",;
    HelpContextID = 17177233,;
   bGlobalFont=.t.,;
    Height=21, Width=248, Left=399, Top=275, cSayPict='repl("!",30)', cGetPict='repl("!",30)', InputMask=replicate('X',30)

  add object oCFPROVIN_1_35 as StdField with uid="TZVEHRJOLH",rtseq=35,rtrep=.f.,;
    cFormVar = "w_CFPROVIN", cQueryName = "CFPROVIN",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia",;
    HelpContextID = 188930700,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=691, Top=275, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)


  add object oObj_1_56 as cp_runprogram with uid="IMVHWAQFRK",left=590, top=311, width=132,height=31,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSTE_BFC('DC')",;
    cEvent = "ControllaDati",;
    nPag=1;
    , HelpContextID = 68280806

  add object oStr_1_36 as StdString with uid="QCWLLSPEJC",Visible=.t., Left=34, Top=144,;
    Alignment=1, Width=143, Height=18,;
    Caption="Rag.soc\Cognome:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="ODRGIITHRR",Visible=.t., Left=43, Top=173,;
    Alignment=1, Width=134, Height=15,;
    Caption="Nome:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="FDYWNCDKUL",Visible=.t., Left=6, Top=85,;
    Alignment=0, Width=544, Height=18,;
    Caption="Contribuente"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="NLKFIDCEHN",Visible=.t., Left=14, Top=13,;
    Alignment=1, Width=163, Height=15,;
    Caption="Delega irrevocabile a:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="PZJFWKWISW",Visible=.t., Left=37, Top=37,;
    Alignment=1, Width=140, Height=15,;
    Caption="Agenzia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="ZFOLXIMYSB",Visible=.t., Left=482, Top=37,;
    Alignment=1, Width=38, Height=15,;
    Caption="Prov.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="DTXRJLUTVV",Visible=.t., Left=62, Top=65,;
    Alignment=0, Width=406, Height=15,;
    Caption="per l'accredito alla tesoreria competente."  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="TKMQOTEDUR",Visible=.t., Left=43, Top=209,;
    Alignment=1, Width=134, Height=15,;
    Caption="Data di nascita:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="FCFKPJRJOH",Visible=.t., Left=272, Top=209,;
    Alignment=1, Width=104, Height=15,;
    Caption="Luogo di nascita:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="CMGVBNBESH",Visible=.t., Left=646, Top=209,;
    Alignment=1, Width=43, Height=15,;
    Caption="Prov.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="QTMPMPJWOH",Visible=.t., Left=6, Top=236,;
    Alignment=0, Width=552, Height=18,;
    Caption="Domicilio fiscale"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="NVQOAWYOVT",Visible=.t., Left=319, Top=277,;
    Alignment=1, Width=77, Height=15,;
    Caption="Comune:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="WRFDXXFEUI",Visible=.t., Left=6, Top=275,;
    Alignment=1, Width=59, Height=15,;
    Caption="Indirizzo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="TXIUHIEHMQ",Visible=.t., Left=651, Top=277,;
    Alignment=1, Width=38, Height=15,;
    Caption="Prov.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="JHVHRUFCFW",Visible=.t., Left=43, Top=116,;
    Alignment=1, Width=134, Height=15,;
    Caption="Codice fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="MTCUXQDQBN",Visible=.t., Left=401, Top=173,;
    Alignment=1, Width=84, Height=15,;
    Caption="Sesso:"  ;
  , bGlobalFont=.t.

  func oStr_1_53.mHide()
    with this.Parent.oContained
      return (.w_CFPERFIS='N')
    endwith
  endfunc

  add object oBox_1_38 as StdBox with uid="ZRQWKWMQIX",left=7, top=105, width=709,height=1

  add object oBox_1_48 as StdBox with uid="BFFXBSUZVB",left=6, top=255, width=709,height=0
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_acf','MODCPAG','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CFSERIAL=MODCPAG.CFSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
