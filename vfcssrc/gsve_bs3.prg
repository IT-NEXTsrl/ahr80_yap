* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bs3                                                        *
*              Ritorna progressivo differita                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_20]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-04-03                                                      *
* Last revis.: 2011-10-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bs3",oParentObject,m.pOper)
return(i_retval)

define class tgsve_bs3 as StdBatch
  * --- Local variables
  pOper = space(1)
  w_TMPS = space(200)
  * --- WorkFile variables
  DOC_MAST_idx=0
  TIP_DOCU_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Test da Fatturazione Differita (da GSVE_AFD, GSVE_KFD)
    *     Utilizzato anche in 'Modifica Causale Documento' : GSVE_KND (lanciata da bottone Modifica su GSVE_MDV)
    * --- S - Cambio Stato Piano di Fatturazione
    *     C  - Check sequenza
    *     D - Ricalcolo progressivo
    do case
      case this.pOper="S"
        * --- Cambia lo Status, se imposto Provvisorio metto PR altrimenti 
        *     reimposto l'Alfa relativo alla Causale Documento
        this.oParentObject.w_PSALFDOC = IIF(this.oParentObject.w_PSSTATUS="S", "PR        ", this.oParentObject.w_OALFDOC)
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pOper="C"
        * --- Verifica sequenzialità numerazione fatture...
        this.w_TMPS = CHKNUMD(this.oParentObject.w_PSDATDOC,this.oParentObject.w_PSNUMDOC,this.oParentObject.w_MVALFDOC,this.oParentObject.w_MVANNDOC,this.oParentObject.w_MVPRD,Space(10),"D")
        * --- Non permetto la conferma
        if Not Empty( this.w_TMPS )
          this.oParentObject.w_OKAGG = .F.
          ah_ErrorMsg("%1","!","", this.w_TMPS)
        endif
      case this.pOper="D"
        * --- Calcola primo progressivo libero..
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Propone il Primo Progressivo Documento Disponibile
    this.oParentObject.w_MVNUMDOC = 0
    this.oParentObject.w_MVANNDOC = STR(YEAR(this.oParentObject.w_PSDATDOC), 4, 0)
    this.oParentObject.w_MVALFDOC = this.oParentObject.w_PSALFDOC
    i_Conn=i_TableProp[this.DOC_MAST_IDX, 3]
    cp_AskTableProg(this.oParentObject, i_Conn, "PRDOC", "i_CODAZI,w_MVANNDOC,w_MVPRD,w_MVALFDOC,w_MVNUMDOC")
    this.oParentObject.w_PSNUMDOC = this.oParentObject.w_MVNUMDOC
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='DOC_MAST'
    this.cWorkTables[2]='TIP_DOCU'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
