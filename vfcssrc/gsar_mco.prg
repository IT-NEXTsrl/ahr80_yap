* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_mco                                                        *
*              Lista contatti                                                  *
*                                                                              *
*      Author: Zucchetti S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_56]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-06-03                                                      *
* Last revis.: 2014-11-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsar_mco")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsar_mco")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsar_mco")
  return

* --- Class definition
define class tgsar_mco as StdPCForm
  Width  = 707
  Height = 384
  Top    = 19
  Left   = 8
  cComment = "Lista contatti"
  cPrg = "gsar_mco"
  HelpContextID=171346071
  add object cnt as tcgsar_mco
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsar_mco as PCContext
  w_COTIPCON = space(1)
  w_COCODCON = space(15)
  w_COTIPDAT = space(2)
  w_CPROWORD = 0
  w_CORIFPER = space(125)
  w_COINDMAI = space(254)
  w_COINDPEC = space(254)
  w_CONUMTEL = space(20)
  w_CPROWNUM = 0
  w_CONUMCEL = space(20)
  w_CO_SKYPE = space(50)
  w_CODESDIV = space(5)
  w_VRCODCON = space(15)
  w_VRTIPCON = space(1)
  proc Save(i_oFrom)
    this.w_COTIPCON = i_oFrom.w_COTIPCON
    this.w_COCODCON = i_oFrom.w_COCODCON
    this.w_COTIPDAT = i_oFrom.w_COTIPDAT
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_CORIFPER = i_oFrom.w_CORIFPER
    this.w_COINDMAI = i_oFrom.w_COINDMAI
    this.w_COINDPEC = i_oFrom.w_COINDPEC
    this.w_CONUMTEL = i_oFrom.w_CONUMTEL
    this.w_CPROWNUM = i_oFrom.w_CPROWNUM
    this.w_CONUMCEL = i_oFrom.w_CONUMCEL
    this.w_CO_SKYPE = i_oFrom.w_CO_SKYPE
    this.w_CODESDIV = i_oFrom.w_CODESDIV
    this.w_VRCODCON = i_oFrom.w_VRCODCON
    this.w_VRTIPCON = i_oFrom.w_VRTIPCON
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_COTIPCON = this.w_COTIPCON
    i_oTo.w_COCODCON = this.w_COCODCON
    i_oTo.w_COTIPDAT = this.w_COTIPDAT
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_CORIFPER = this.w_CORIFPER
    i_oTo.w_COINDMAI = this.w_COINDMAI
    i_oTo.w_COINDPEC = this.w_COINDPEC
    i_oTo.w_CONUMTEL = this.w_CONUMTEL
    i_oTo.w_CPROWNUM = this.w_CPROWNUM
    i_oTo.w_CONUMCEL = this.w_CONUMCEL
    i_oTo.w_CO_SKYPE = this.w_CO_SKYPE
    i_oTo.w_CODESDIV = this.w_CODESDIV
    i_oTo.w_VRCODCON = this.w_VRCODCON
    i_oTo.w_VRTIPCON = this.w_VRTIPCON
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsar_mco as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 707
  Height = 384
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-11-17"
  HelpContextID=171346071
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=14

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  CONTATTI_IDX = 0
  DES_DIVE_IDX = 0
  cFile = "CONTATTI"
  cKeySelect = "COTIPCON,COCODCON"
  cKeyWhere  = "COTIPCON=this.w_COTIPCON and COCODCON=this.w_COCODCON"
  cKeyDetail  = "COTIPCON=this.w_COTIPCON and COCODCON=this.w_COCODCON and CPROWNUM=this.w_CPROWNUM"
  cKeyWhereODBC = '"COTIPCON="+cp_ToStrODBC(this.w_COTIPCON)';
      +'+" and COCODCON="+cp_ToStrODBC(this.w_COCODCON)';

  cKeyDetailWhereODBC = '"COTIPCON="+cp_ToStrODBC(this.w_COTIPCON)';
      +'+" and COCODCON="+cp_ToStrODBC(this.w_COCODCON)';
      +'+" and CPROWNUM="+cp_ToStrODBC(this.w_CPROWNUM)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"CONTATTI.COTIPCON="+cp_ToStrODBC(this.w_COTIPCON)';
      +'+" and CONTATTI.COCODCON="+cp_ToStrODBC(this.w_COCODCON)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'CONTATTI.CPROWORD '
  cPrg = "gsar_mco"
  cComment = "Lista contatti"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 14
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_COTIPCON = space(1)
  w_COCODCON = space(15)
  w_COTIPDAT = space(2)
  w_CPROWORD = 0
  w_CORIFPER = space(125)
  w_COINDMAI = space(254)
  w_COINDPEC = space(254)
  w_CONUMTEL = space(20)
  w_CPROWNUM = 0
  w_CONUMCEL = space(20)
  w_CO_SKYPE = space(50)
  w_CODESDIV = space(5)
  w_VRCODCON = space(15)
  w_VRTIPCON = space(1)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_mcoPag1","gsar_mco",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='DES_DIVE'
    this.cWorkTables[2]='CONTATTI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CONTATTI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CONTATTI_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsar_mco'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from CONTATTI where COTIPCON=KeySet.COTIPCON
    *                            and COCODCON=KeySet.COCODCON
    *                            and CPROWNUM=KeySet.CPROWNUM
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.CONTATTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONTATTI_IDX,2],this.bLoadRecFilter,this.CONTATTI_IDX,"gsar_mco")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CONTATTI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CONTATTI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CONTATTI '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'COTIPCON',this.w_COTIPCON  ,'COCODCON',this.w_COCODCON  )
      select * from (i_cTable) CONTATTI where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_VRCODCON = space(15)
        .w_VRTIPCON = space(1)
        .w_COTIPCON = NVL(COTIPCON,space(1))
        .w_COCODCON = NVL(COCODCON,space(15))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'CONTATTI')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_COTIPDAT = NVL(COTIPDAT,space(2))
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_CORIFPER = NVL(CORIFPER,space(125))
          .w_COINDMAI = NVL(COINDMAI,space(254))
          .w_COINDPEC = NVL(COINDPEC,space(254))
          .w_CONUMTEL = NVL(CONUMTEL,space(20))
          .w_CPROWNUM = NVL(CPROWNUM,0)
          .w_CONUMCEL = NVL(CONUMCEL,space(20))
          .w_CO_SKYPE = NVL(CO_SKYPE,space(50))
          .w_CODESDIV = NVL(CODESDIV,space(5))
          * evitabile
          *.link_2_12('Load')
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_COTIPCON=space(1)
      .w_COCODCON=space(15)
      .w_COTIPDAT=space(2)
      .w_CPROWORD=10
      .w_CORIFPER=space(125)
      .w_COINDMAI=space(254)
      .w_COINDPEC=space(254)
      .w_CONUMTEL=space(20)
      .w_CPROWNUM=0
      .w_CONUMCEL=space(20)
      .w_CO_SKYPE=space(50)
      .w_CODESDIV=space(5)
      .w_VRCODCON=space(15)
      .w_VRTIPCON=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        .w_COTIPDAT = 'GE'
        .DoRTCalc(4,12,.f.)
        if not(empty(.w_CODESDIV))
         .link_2_12('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'CONTATTI')
    this.DoRTCalc(13,14,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oCOINDPEC_2_5.enabled = i_bVal
      .Page1.oPag.oCONUMTEL_2_6.enabled = i_bVal
      .Page1.oPag.oCONUMCEL_2_8.enabled = i_bVal
      .Page1.oPag.oCO_SKYPE_2_10.enabled = i_bVal
      .Page1.oPag.oCODESDIV_2_12.enabled = i_bVal
      .Page1.oPag.oBtn_2_7.enabled = i_bVal
      .Page1.oPag.oBtn_2_9.enabled = i_bVal
      .Page1.oPag.oBtn_2_11.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'CONTATTI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CONTATTI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COTIPCON,"COTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCODCON,"COCODCON",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(4);
      ,t_CORIFPER C(125);
      ,t_COINDMAI C(254);
      ,t_COINDPEC C(254);
      ,t_CONUMTEL C(20);
      ,t_CONUMCEL C(20);
      ,t_CO_SKYPE C(50);
      ,t_CODESDIV C(5);
      ,CPROWNUM N(10);
      ,t_COTIPDAT C(2);
      ,t_CPROWNUM N(4);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsar_mcobodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCORIFPER_2_3.controlsource=this.cTrsName+'.t_CORIFPER'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCOINDMAI_2_4.controlsource=this.cTrsName+'.t_COINDMAI'
    this.oPgFRm.Page1.oPag.oCOINDPEC_2_5.controlsource=this.cTrsName+'.t_COINDPEC'
    this.oPgFRm.Page1.oPag.oCONUMTEL_2_6.controlsource=this.cTrsName+'.t_CONUMTEL'
    this.oPgFRm.Page1.oPag.oCONUMCEL_2_8.controlsource=this.cTrsName+'.t_CONUMCEL'
    this.oPgFRm.Page1.oPag.oCO_SKYPE_2_10.controlsource=this.cTrsName+'.t_CO_SKYPE'
    this.oPgFRm.Page1.oPag.oCODESDIV_2_12.controlsource=this.cTrsName+'.t_CODESDIV'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(41)
    this.AddVLine(361)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CONTATTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONTATTI_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CONTATTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONTATTI_IDX,2])
      *
      * insert into CONTATTI
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CONTATTI')
        i_extval=cp_InsertValODBCExtFlds(this,'CONTATTI')
        i_cFldBody=" "+;
                  "(COTIPCON,COCODCON,COTIPDAT,CPROWORD,CORIFPER"+;
                  ",COINDMAI,COINDPEC,CONUMTEL,CONUMCEL,CO_SKYPE"+;
                  ",CODESDIV,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_COTIPCON)+","+cp_ToStrODBC(this.w_COCODCON)+","+cp_ToStrODBC(this.w_COTIPDAT)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_CORIFPER)+;
             ","+cp_ToStrODBC(this.w_COINDMAI)+","+cp_ToStrODBC(this.w_COINDPEC)+","+cp_ToStrODBC(this.w_CONUMTEL)+","+cp_ToStrODBC(this.w_CONUMCEL)+","+cp_ToStrODBC(this.w_CO_SKYPE)+;
             ","+cp_ToStrODBCNull(this.w_CODESDIV)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CONTATTI')
        i_extval=cp_InsertValVFPExtFlds(this,'CONTATTI')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'COTIPCON',this.w_COTIPCON,'COCODCON',this.w_COCODCON,'CPROWNUM',this.w_CPROWNUM)
        INSERT INTO (i_cTable) (;
                   COTIPCON;
                  ,COCODCON;
                  ,COTIPDAT;
                  ,CPROWORD;
                  ,CORIFPER;
                  ,COINDMAI;
                  ,COINDPEC;
                  ,CONUMTEL;
                  ,CONUMCEL;
                  ,CO_SKYPE;
                  ,CODESDIV;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_COTIPCON;
                  ,this.w_COCODCON;
                  ,this.w_COTIPDAT;
                  ,this.w_CPROWORD;
                  ,this.w_CORIFPER;
                  ,this.w_COINDMAI;
                  ,this.w_COINDPEC;
                  ,this.w_CONUMTEL;
                  ,this.w_CONUMCEL;
                  ,this.w_CO_SKYPE;
                  ,this.w_CODESDIV;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.CONTATTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONTATTI_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
          if this.w_CPROWNUM<>t_CPROWNUM
            i_bUpdAll = .t.
          endif
        endif
        if not(i_bUpdAll)
          scan for (not (empty(t_CPROWORD)) and (not(empty(t_CORIFPER)) or not(empty(t_COINDMAI)))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'CONTATTI')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'CONTATTI')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
          if this.w_CPROWNUM<>t_CPROWNUM
            i_bUpdAll = .t.
          endif
        endif
        scan for (not (empty(t_CPROWORD)) and (not(empty(t_CORIFPER)) or not(empty(t_COINDMAI)))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update CONTATTI
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'CONTATTI')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " COTIPDAT="+cp_ToStrODBC(this.w_COTIPDAT)+;
                     ",CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",CORIFPER="+cp_ToStrODBC(this.w_CORIFPER)+;
                     ",COINDMAI="+cp_ToStrODBC(this.w_COINDMAI)+;
                     ",COINDPEC="+cp_ToStrODBC(this.w_COINDPEC)+;
                     ",CONUMTEL="+cp_ToStrODBC(this.w_CONUMTEL)+;
                     ",CONUMCEL="+cp_ToStrODBC(this.w_CONUMCEL)+;
                     ",CO_SKYPE="+cp_ToStrODBC(this.w_CO_SKYPE)+;
                     ",CODESDIV="+cp_ToStrODBCNull(this.w_CODESDIV)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'CONTATTI')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      COTIPDAT=this.w_COTIPDAT;
                     ,CPROWORD=this.w_CPROWORD;
                     ,CORIFPER=this.w_CORIFPER;
                     ,COINDMAI=this.w_COINDMAI;
                     ,COINDPEC=this.w_COINDPEC;
                     ,CONUMTEL=this.w_CONUMTEL;
                     ,CONUMCEL=this.w_CONUMCEL;
                     ,CO_SKYPE=this.w_CO_SKYPE;
                     ,CODESDIV=this.w_CODESDIV;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CONTATTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONTATTI_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not (empty(t_CPROWORD)) and (not(empty(t_CORIFPER)) or not(empty(t_COINDMAI)))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete CONTATTI
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not (empty(t_CPROWORD)) and (not(empty(t_CORIFPER)) or not(empty(t_COINDMAI)))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CONTATTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONTATTI_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,14,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_COTIPDAT with this.w_COTIPDAT
      replace t_CPROWNUM with this.w_CPROWNUM
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBtn_2_7.enabled =this.oPgFrm.Page1.oPag.oBtn_2_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_9.enabled =this.oPgFrm.Page1.oPag.oBtn_2_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_11.enabled =this.oPgFrm.Page1.oPag.oBtn_2_11.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oBtn_2_7.visible=!this.oPgFrm.Page1.oPag.oBtn_2_7.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_9.visible=!this.oPgFrm.Page1.oPag.oBtn_2_9.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_11.visible=!this.oPgFrm.Page1.oPag.oBtn_2_11.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODESDIV
  func Link_2_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DES_DIVE_IDX,3]
    i_lTable = "DES_DIVE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2], .t., this.DES_DIVE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODESDIV) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DES_DIVE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DDCODDES like "+cp_ToStrODBC(trim(this.w_CODESDIV)+"%");
                   +" and DDTIPCON="+cp_ToStrODBC(this.w_VRTIPCON);
                   +" and DDCODICE="+cp_ToStrODBC(this.w_VRCODCON);

          i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDTIPRIF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DDTIPCON,DDCODICE,DDCODDES","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DDTIPCON',this.w_VRTIPCON;
                     ,'DDCODICE',this.w_VRCODCON;
                     ,'DDCODDES',trim(this.w_CODESDIV))
          select DDTIPCON,DDCODICE,DDCODDES,DDTIPRIF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DDTIPCON,DDCODICE,DDCODDES into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODESDIV)==trim(_Link_.DDCODDES) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODESDIV) and !this.bDontReportError
            deferred_cp_zoom('DES_DIVE','*','DDTIPCON,DDCODICE,DDCODDES',cp_AbsName(oSource.parent,'oCODESDIV_2_12'),i_cWhere,'',"Elenco sedi cliente/fornitore",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_VRTIPCON<>oSource.xKey(1);
           .or. this.w_VRCODCON<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDTIPRIF";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DDTIPCON,DDCODICE,DDCODDES,DDTIPRIF;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDTIPRIF";
                     +" from "+i_cTable+" "+i_lTable+" where DDCODDES="+cp_ToStrODBC(oSource.xKey(3));
                     +" and DDTIPCON="+cp_ToStrODBC(this.w_VRTIPCON);
                     +" and DDCODICE="+cp_ToStrODBC(this.w_VRCODCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DDTIPCON',oSource.xKey(1);
                       ,'DDCODICE',oSource.xKey(2);
                       ,'DDCODDES',oSource.xKey(3))
            select DDTIPCON,DDCODICE,DDCODDES,DDTIPRIF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODESDIV)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDTIPRIF";
                   +" from "+i_cTable+" "+i_lTable+" where DDCODDES="+cp_ToStrODBC(this.w_CODESDIV);
                   +" and DDTIPCON="+cp_ToStrODBC(this.w_VRTIPCON);
                   +" and DDCODICE="+cp_ToStrODBC(this.w_VRCODCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DDTIPCON',this.w_VRTIPCON;
                       ,'DDCODICE',this.w_VRCODCON;
                       ,'DDCODDES',this.w_CODESDIV)
            select DDTIPCON,DDCODICE,DDCODDES,DDTIPRIF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODESDIV = NVL(_Link_.DDCODDES,space(5))
      this.w_COTIPDAT = NVL(_Link_.DDTIPRIF,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_CODESDIV = space(5)
      endif
      this.w_COTIPDAT = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])+'\'+cp_ToStr(_Link_.DDTIPCON,1)+'\'+cp_ToStr(_Link_.DDCODICE,1)+'\'+cp_ToStr(_Link_.DDCODDES,1)
      cp_ShowWarn(i_cKey,this.DES_DIVE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODESDIV Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oCOINDPEC_2_5.value==this.w_COINDPEC)
      this.oPgFrm.Page1.oPag.oCOINDPEC_2_5.value=this.w_COINDPEC
      replace t_COINDPEC with this.oPgFrm.Page1.oPag.oCOINDPEC_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCONUMTEL_2_6.value==this.w_CONUMTEL)
      this.oPgFrm.Page1.oPag.oCONUMTEL_2_6.value=this.w_CONUMTEL
      replace t_CONUMTEL with this.oPgFrm.Page1.oPag.oCONUMTEL_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCONUMCEL_2_8.value==this.w_CONUMCEL)
      this.oPgFrm.Page1.oPag.oCONUMCEL_2_8.value=this.w_CONUMCEL
      replace t_CONUMCEL with this.oPgFrm.Page1.oPag.oCONUMCEL_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCO_SKYPE_2_10.value==this.w_CO_SKYPE)
      this.oPgFrm.Page1.oPag.oCO_SKYPE_2_10.value=this.w_CO_SKYPE
      replace t_CO_SKYPE with this.oPgFrm.Page1.oPag.oCO_SKYPE_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCODESDIV_2_12.value==this.w_CODESDIV)
      this.oPgFrm.Page1.oPag.oCODESDIV_2_12.value=this.w_CODESDIV
      replace t_CODESDIV with this.oPgFrm.Page1.oPag.oCODESDIV_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCORIFPER_2_3.value==this.w_CORIFPER)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCORIFPER_2_3.value=this.w_CORIFPER
      replace t_CORIFPER with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCORIFPER_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOINDMAI_2_4.value==this.w_COINDMAI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOINDMAI_2_4.value=this.w_COINDMAI
      replace t_COINDMAI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOINDMAI_2_4.value
    endif
    cp_SetControlsValueExtFlds(this,'CONTATTI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not (empty(.w_CPROWORD)) and (not(empty(.w_CORIFPER)) or not(empty(.w_COINDMAI)))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not (empty(t_CPROWORD)) and (not(empty(t_CORIFPER)) or not(empty(t_COINDMAI))))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_COTIPDAT=space(2)
      .w_CPROWORD=MIN(9999,cp_maxroword()+10)
      .w_CORIFPER=space(125)
      .w_COINDMAI=space(254)
      .w_COINDPEC=space(254)
      .w_CONUMTEL=space(20)
      .w_CONUMCEL=space(20)
      .w_CO_SKYPE=space(50)
      .w_CODESDIV=space(5)
      .DoRTCalc(1,2,.f.)
        .w_COTIPDAT = 'GE'
      .DoRTCalc(4,12,.f.)
      if not(empty(.w_CODESDIV))
        .link_2_12('Full')
      endif
    endwith
    this.DoRTCalc(13,14,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_COTIPDAT = t_COTIPDAT
    this.w_CPROWORD = t_CPROWORD
    this.w_CORIFPER = t_CORIFPER
    this.w_COINDMAI = t_COINDMAI
    this.w_COINDPEC = t_COINDPEC
    this.w_CONUMTEL = t_CONUMTEL
    this.w_CPROWNUM = t_CPROWNUM
    this.w_CONUMCEL = t_CONUMCEL
    this.w_CO_SKYPE = t_CO_SKYPE
    this.w_CODESDIV = t_CODESDIV
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_COTIPDAT with this.w_COTIPDAT
    replace t_CPROWORD with this.w_CPROWORD
    replace t_CORIFPER with this.w_CORIFPER
    replace t_COINDMAI with this.w_COINDMAI
    replace t_COINDPEC with this.w_COINDPEC
    replace t_CONUMTEL with this.w_CONUMTEL
    replace t_CPROWNUM with this.w_CPROWNUM
    replace t_CONUMCEL with this.w_CONUMCEL
    replace t_CO_SKYPE with this.w_CO_SKYPE
    replace t_CODESDIV with this.w_CODESDIV
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsar_mcoPag1 as StdContainer
  Width  = 703
  height = 384
  stdWidth  = 703
  stdheight = 384
  resizeXpos=268
  resizeYpos=195
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=5, top=2, width=692,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=3,Field1="CPROWORD",Label1="Riga",Field2="CORIFPER",Label2="Riferimento persona",Field3="COINDMAI",Label3="Indirizzo e-mail",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 49433990

  add object oStr_1_3 as StdString with uid="ESDMRNHSDU",Visible=.t., Left=8, Top=328,;
    Alignment=1, Width=123, Height=18,;
    Caption="Numero di telefono:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="FMRHZNOLZF",Visible=.t., Left=359, Top=354,;
    Alignment=1, Width=123, Height=18,;
    Caption="Codice sede:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="CGVKFLWCPM",Visible=.t., Left=359, Top=328,;
    Alignment=1, Width=123, Height=18,;
    Caption="Numero cellulare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="FYQGAXNQTZ",Visible=.t., Left=8, Top=354,;
    Alignment=1, Width=123, Height=18,;
    Caption="Utente Skype"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="DGLBBWIDWT",Visible=.t., Left=8, Top=302,;
    Alignment=1, Width=123, Height=18,;
    Caption="PEC:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-5,top=21,;
    width=688+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*14*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-4,top=22,width=687+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*14*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oCOINDPEC_2_5.Refresh()
      this.Parent.oCONUMTEL_2_6.Refresh()
      this.Parent.oCONUMCEL_2_8.Refresh()
      this.Parent.oCO_SKYPE_2_10.Refresh()
      this.Parent.oCODESDIV_2_12.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oCOINDPEC_2_5 as StdTrsField with uid="WXFLOUAEBN",rtseq=7,rtrep=.t.,;
    cFormVar="w_COINDPEC",value=space(254),;
    ToolTipText = "Indirizzo e-mail posta elettronica certificata (PEC)",;
    HelpContextID = 20353943,;
    cTotal="", bFixedPos=.t., cQueryName = "COINDPEC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=502, Left=137, Top=302, InputMask=replicate('X',254)

  add object oCONUMTEL_2_6 as StdTrsField with uid="GDZADZSTEM",rtseq=8,rtrep=.t.,;
    cFormVar="w_CONUMTEL",value=space(20),;
    ToolTipText = "Numero di telefono",;
    HelpContextID = 56671346,;
    cTotal="", bFixedPos=.t., cQueryName = "CONUMTEL",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=137, Top=328, InputMask=replicate('X',20)

  add object oBtn_2_7 as StdButton with uid="AQGUXYFYGB",width=22,height=22,;
   left=294, top=328,;
    CpPicture="BMP\PHONE.bmp", caption="", nPag=2;
    , ToolTipText = "Permette di effettuare chiamate al numero indicato";
    , HelpContextID = 16681766;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_7.Click()
      with this.Parent.oContained
        GSUT_BSK(this.Parent.oContained,.w_CONUMTEL, "MN")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_7.mCond()
    with this.Parent.oContained
      return (!empty(.w_CONUMTEL) And (g_SkypeService='C' Or !Empty(g_PhoneService)))
    endwith
  endfunc

  func oBtn_2_7.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_CONUMTEL) Or (g_SkypeService<>'C' And Empty(g_PhoneService)))
    endwith
   endif
  endfunc

  add object oCONUMCEL_2_8 as StdTrsField with uid="OTZYOAVSEY",rtseq=10,rtrep=.t.,;
    cFormVar="w_CONUMCEL",value=space(20),;
    ToolTipText = "Numero cellulare",;
    HelpContextID = 228541326,;
    cTotal="", bFixedPos=.t., cQueryName = "CONUMCEL",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=486, Top=328, InputMask=replicate('X',20)

  add object oBtn_2_9 as StdButton with uid="RPLPWGQTNU",width=22,height=22,;
   left=643, top=328,;
    CpPicture="BMP\PHONE.bmp", caption="", nPag=2;
    , ToolTipText = "Permette di effettuare chiamate al numero indicato";
    , HelpContextID = 16681766;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_9.Click()
      with this.Parent.oContained
        GSUT_BSK(this.Parent.oContained,.w_CONUMCEL, "MN")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_9.mCond()
    with this.Parent.oContained
      return (!empty(.w_CONUMCEL) And (g_SkypeService='C' Or !Empty(g_PhoneService)))
    endwith
  endfunc

  func oBtn_2_9.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_CONUMCEL) Or (g_SkypeService<>'C' And Empty(g_PhoneService)))
    endwith
   endif
  endfunc

  add object oCO_SKYPE_2_10 as StdTrsField with uid="SZKYHEVMVA",rtseq=11,rtrep=.t.,;
    cFormVar="w_CO_SKYPE",value=space(50),;
    HelpContextID = 130036629,;
    cTotal="", bFixedPos=.t., cQueryName = "CO_SKYPE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=137, Top=354, InputMask=replicate('X',50)

  add object oBtn_2_11 as StdButton with uid="UKVDSMJARB",width=22,height=22,;
   left=294, top=354,;
    CpPicture="BMP\SKYPE.bmp", caption="", nPag=2;
    , ToolTipText = "Permette di effettuare chiamate via Skype al numero indicato";
    , HelpContextID = 16681766;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_11.Click()
      with this.Parent.oContained
        GSUT_BSK(this.Parent.oContained,.w_CO_SKYPE, "CC")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_11.mCond()
    with this.Parent.oContained
      return (!empty(.w_CO_SKYPE) AND g_SkypeService<>'N')
    endwith
  endfunc

  func oBtn_2_11.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_CO_SKYPE) OR g_SkypeService='N')
    endwith
   endif
  endfunc

  add object oCODESDIV_2_12 as StdTrsField with uid="SJITDXVNME",rtseq=12,rtrep=.t.,;
    cFormVar="w_CODESDIV",value=space(5),;
    ToolTipText = "Codice sede",;
    HelpContextID = 61873276,;
    cTotal="", bFixedPos=.t., cQueryName = "CODESDIV",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=96, Left=486, Top=354, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DES_DIVE", oKey_1_1="DDTIPCON", oKey_1_2="this.w_VRTIPCON", oKey_2_1="DDCODICE", oKey_2_2="this.w_VRCODCON", oKey_3_1="DDCODDES", oKey_3_2="this.w_CODESDIV"

  proc oCODESDIV_2_12.mAfter
    with this.Parent.oContained
      .w_CODESDIV=chkdesdiv(.w_CODESDIV, .w_VRTIPCON, .w_VRCODCON)
    endwith
  endproc

  func oCODESDIV_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODESDIV_2_12.ecpDrop(oSource)
    this.Parent.oContained.link_2_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oCODESDIV_2_12.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DES_DIVE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_VRTIPCON)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDCODICE="+cp_ToStrODBC(this.Parent.oContained.w_VRCODCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDTIPCON="+cp_ToStr(this.Parent.oContained.w_VRTIPCON)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDCODICE="+cp_ToStr(this.Parent.oContained.w_VRCODCON)
    endif
    do cp_zoom with 'DES_DIVE','*','DDTIPCON,DDCODICE,DDCODDES',cp_AbsName(this.parent,'oCODESDIV_2_12'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco sedi cliente/fornitore",'',this.parent.oContained
  endproc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsar_mcoBodyRow as CPBodyRowCnt
  Width=678
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_2 as StdTrsField with uid="CQYICPWASE",rtseq=4,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 17105558,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=35, Left=-2, Top=0

  add object oCORIFPER_2_3 as StdTrsField with uid="HFISGJOTFV",rtseq=5,rtrep=.t.,;
    cFormVar="w_CORIFPER",value=space(125),;
    ToolTipText = "Riferimento persona",;
    HelpContextID = 18547592,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=317, Left=35, Top=0, InputMask=replicate('X',125)

  add object oCOINDMAI_2_4 as StdTrsField with uid="OSMSBKAEXA",rtseq=6,rtrep=.t.,;
    cFormVar="w_COINDMAI",value=space(254),;
    ToolTipText = "Indirizzo e-mail",;
    HelpContextID = 70685585,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=316, Left=357, Top=0, InputMask=replicate('X',254)
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_2.When()
    return(.t.)
  proc oCPROWORD_2_2.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_2.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=13
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_mco','CONTATTI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".COTIPCON=CONTATTI.COTIPCON";
  +" and "+i_cAliasName2+".COCODCON=CONTATTI.COCODCON";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
