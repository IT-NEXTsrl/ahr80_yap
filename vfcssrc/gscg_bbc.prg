* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bbc                                                        *
*              Stampa controllo conti Basilea2                                 *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][207]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-05-10                                                      *
* Last revis.: 2005-12-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bbc",oParentObject)
return(i_retval)

define class tgscg_bbc as StdBatch
  * --- Local variables
  * --- WorkFile variables
  TMPSALDI_idx=0
  OUTPUTMP_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa Controllo Conti basilea 2
    *     Recupera i conti ed i mastri legati direttamente o tramite voci di bilancio
    *     a Voci di Raccordo Basilea2.
    *     Esegue l'esplosione dei mastri, nel senso che per ogni mastro va a recuperare
    *     i mastri ad esso collegati, fin quando ne trova. Al termine
    *     avr� solo i conti e le voci in cui conluiscono
    * --- Creo un temporaneo con l'elenco delle Voci di Raccordo ed i conti / Mastri
    *     legati direttamente ad esse o tramite voci...
    * --- Create temporary table TMPSALDI
    i_nIdx=cp_AddTableDef('TMPSALDI') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('QUERY\GSCG_BBC_0',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPSALDI_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Ora ho i mastri di livello 1, devo metterli in Join con i conti che li puntano..
    *     Creo la tabella temporanea da utilizzare come Output utente...
    * --- Create temporary table OUTPUTMP
    i_nIdx=cp_AddTableDef('OUTPUTMP') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('QUERY\GSCG_BBC_2',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.OUTPUTMP_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Rimuovo i temporanei
    * --- Drop temporary table ZOOMPART
    i_nIdx=cp_GetTableDefIdx('ZOOMPART')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('ZOOMPART')
    endif
    * --- Drop temporary table TMPSALDI
    i_nIdx=cp_GetTableDefIdx('TMPSALDI')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPSALDI')
    endif
    * --- Lancio il report
    VX_EXEC(""+ALLTRIM(this.oParentObject.w_OQRY)+", "+ALLTRIM(this.oParentObject.w_OREP)+"",THIS)
    * --- Rimuovo il cursore e la tabella tmp
    * --- Drop temporary table OUTPUTMP
    i_nIdx=cp_GetTableDefIdx('OUTPUTMP')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('OUTPUTMP')
    endif
    if USED("__TMP__")
       
 Select __tmp__ 
 Use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='*TMPSALDI'
    this.cWorkTables[2]='*OUTPUTMP'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
