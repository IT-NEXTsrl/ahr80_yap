* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_smc                                                        *
*              Stampa piano dei conti                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_24]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-12                                                      *
* Last revis.: 2008-02-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_smc",oParentObject))

* --- Class definition
define class tgsar_smc as StdForm
  Top    = 39
  Left   = 119

  * --- Standard Properties
  Width  = 423
  Height = 146
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-02-01"
  HelpContextID=191461225
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsar_smc"
  cComment = "Stampa piano dei conti"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_codaz1 = space(10)
  w_scelta = space(10)
  o_scelta = space(10)
  w_DATSTA = ctod('  /  /  ')
  w_MASTSIGN = space(1)
  w_obsodat1 = space(1)
  w_Analit = space(1)
  w_ColCronol = space(1)
  * --- Area Manuale = Declare Variables
  * --- gsar_smc
  proc SetCPToolBar()
          doDefault()
          oCpToolBar.b4.enabled=.f.
  endproc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_smcPag1","gsar_smc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oscelta_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSAR_BSP with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsar_smc
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_codaz1=space(10)
      .w_scelta=space(10)
      .w_DATSTA=ctod("  /  /  ")
      .w_MASTSIGN=space(1)
      .w_obsodat1=space(1)
      .w_Analit=space(1)
      .w_ColCronol=space(1)
        .w_codaz1 = i_codazi
        .w_scelta = 'tutti'
        .w_DATSTA = i_datsys
        .w_MASTSIGN = 'S'
        .w_obsodat1 = 'N'
        .w_Analit = 'N'
        .w_ColCronol = IIF(VARTYPE(this.oParentObject)='C',this.oParentObject,'N')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
        if .o_scelta<>.w_scelta
            .w_Analit = 'N'
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(7,7,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_NHGZPPOKMU()
    with this
          * --- Cambia caption form
          .Caption = IIF(.w_ColCronol=='S',AH_MSGFORMAT('Stampa piano dei conti con colonne cronologico'),.Caption)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oAnalit_1_10.enabled = this.oPgFrm.Page1.oPag.oAnalit_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Init")
          .Calculate_NHGZPPOKMU()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oscelta_1_2.RadioValue()==this.w_scelta)
      this.oPgFrm.Page1.oPag.oscelta_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATSTA_1_3.value==this.w_DATSTA)
      this.oPgFrm.Page1.oPag.oDATSTA_1_3.value=this.w_DATSTA
    endif
    if not(this.oPgFrm.Page1.oPag.oMASTSIGN_1_4.RadioValue()==this.w_MASTSIGN)
      this.oPgFrm.Page1.oPag.oMASTSIGN_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oobsodat1_1_5.RadioValue()==this.w_obsodat1)
      this.oPgFrm.Page1.oPag.oobsodat1_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAnalit_1_10.RadioValue()==this.w_Analit)
      this.oPgFrm.Page1.oPag.oAnalit_1_10.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DATSTA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATSTA_1_3.SetFocus()
            i_bnoObbl = !empty(.w_DATSTA)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_scelta = this.w_scelta
    return

enddefine

* --- Define pages as container
define class tgsar_smcPag1 as StdContainer
  Width  = 419
  height = 146
  stdWidth  = 419
  stdheight = 146
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oscelta_1_2 as StdCombo with uid="LIZRJMXZMB",rtseq=2,rtrep=.f.,left=89,top=18,width=127,height=21;
    , ToolTipText = "Selezionare la tipologia conti da stampare";
    , HelpContextID = 222905126;
    , cFormVar="w_scelta",RowSource=""+"Tutti,"+"Patrimoniali,"+"Economici,"+"Banche", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oscelta_1_2.RadioValue()
    return(iif(this.value =1,'tutti',;
    iif(this.value =2,'patrim',;
    iif(this.value =3,'econ',;
    iif(this.value =4,'banche',;
    space(10))))))
  endfunc
  func oscelta_1_2.GetRadio()
    this.Parent.oContained.w_scelta = this.RadioValue()
    return .t.
  endfunc

  func oscelta_1_2.SetRadio()
    this.Parent.oContained.w_scelta=trim(this.Parent.oContained.w_scelta)
    this.value = ;
      iif(this.Parent.oContained.w_scelta=='tutti',1,;
      iif(this.Parent.oContained.w_scelta=='patrim',2,;
      iif(this.Parent.oContained.w_scelta=='econ',3,;
      iif(this.Parent.oContained.w_scelta=='banche',4,;
      0))))
  endfunc

  add object oDATSTA_1_3 as StdField with uid="NXONIXQSAK",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DATSTA", cQueryName = "DATSTA",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di riferimento per il controllo dell'obsolescenza",;
    HelpContextID = 80802250,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=331, Top=18

  add object oMASTSIGN_1_4 as StdCheck with uid="SAPNOITSCU",rtseq=4,rtrep=.f.,left=30, top=61, caption="Mastri significativi",;
    ToolTipText = "Ignora i mastri senza conti",;
    HelpContextID = 52428564,;
    cFormVar="w_MASTSIGN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMASTSIGN_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oMASTSIGN_1_4.GetRadio()
    this.Parent.oContained.w_MASTSIGN = this.RadioValue()
    return .t.
  endfunc

  func oMASTSIGN_1_4.SetRadio()
    this.Parent.oContained.w_MASTSIGN=trim(this.Parent.oContained.w_MASTSIGN)
    this.value = ;
      iif(this.Parent.oContained.w_MASTSIGN=='S',1,;
      0)
  endfunc

  add object oobsodat1_1_5 as StdCheck with uid="GRRGCHDTRE",rtseq=5,rtrep=.f.,left=30, top=82, caption="Solo obsoleti",;
    ToolTipText = "Stampa solo obsoleti",;
    HelpContextID = 206381591,;
    cFormVar="w_obsodat1", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oobsodat1_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oobsodat1_1_5.GetRadio()
    this.Parent.oContained.w_obsodat1 = this.RadioValue()
    return .t.
  endfunc

  func oobsodat1_1_5.SetRadio()
    this.Parent.oContained.w_obsodat1=trim(this.Parent.oContained.w_obsodat1)
    this.value = ;
      iif(this.Parent.oContained.w_obsodat1=='S',1,;
      0)
  endfunc


  add object oBtn_1_6 as StdButton with uid="NLAJTLMRMQ",left=309, top=95, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , HelpContextID = 191432474;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      with this.Parent.oContained
        do GSAR_BSP with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_6.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_datsta))
      endwith
    endif
  endfunc


  add object oBtn_1_7 as StdButton with uid="NHKYPEIZCC",left=362, top=95, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 184143802;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oAnalit_1_10 as StdCheck with uid="SRJKGFTSDZ",rtseq=6,rtrep=.f.,left=30, top=103, caption="Dati analitica",;
    ToolTipText = "Stampa i riferimenti alle voci di costo e ai centri di costo",;
    HelpContextID = 261688070,;
    cFormVar="w_Analit", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAnalit_1_10.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAnalit_1_10.GetRadio()
    this.Parent.oContained.w_Analit = this.RadioValue()
    return .t.
  endfunc

  func oAnalit_1_10.SetRadio()
    this.Parent.oContained.w_Analit=trim(this.Parent.oContained.w_Analit)
    this.value = ;
      iif(this.Parent.oContained.w_Analit=='S',1,;
      0)
  endfunc

  func oAnalit_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_scelta='econ')
    endwith
   endif
  endfunc

  add object oStr_1_8 as StdString with uid="MUCWVZRLCZ",Visible=.t., Left=218, Top=20,;
    Alignment=1, Width=111, Height=15,;
    Caption="Data di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="FEFSVXYQGI",Visible=.t., Left=8, Top=20,;
    Alignment=1, Width=80, Height=15,;
    Caption="Tipo conti:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_smc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
