* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_aba                                                        *
*              Banche                                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_58]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-11                                                      *
* Last revis.: 2015-03-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_aba"))

* --- Class definition
define class tgsar_aba as StdForm
  Top    = 25
  Left   = 15

  * --- Standard Properties
  Width  = 585
  Height = 200+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-03-12"
  HelpContextID=141985943
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=16

  * --- Constant Properties
  BAN_CHE_IDX = 0
  COD_ABI_IDX = 0
  COD_CAB_IDX = 0
  cFile = "BAN_CHE"
  cKeySelect = "BACODBAN"
  cKeyWhere  = "BACODBAN=this.w_BACODBAN"
  cKeyWhereODBC = '"BACODBAN="+cp_ToStrODBC(this.w_BACODBAN)';

  cKeyWhereODBCqualified = '"BAN_CHE.BACODBAN="+cp_ToStrODBC(this.w_BACODBAN)';

  cPrg = "gsar_aba"
  cComment = "Banche"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_BACODBAN = space(10)
  w_BAFLBEST = space(1)
  o_BAFLBEST = space(1)
  w_BAFLNOAR = space(1)
  w_BACODABI = space(5)
  w_BADESABI = space(80)
  w_BACINABI = space(1)
  w_BACODCAB = space(5)
  w_BADESFIL = space(40)
  w_BACINCAB = space(1)
  w_BAPASEUR = space(2)
  w_BADESBAN = space(50)
  w_BAINDIRI = space(50)
  w_BA___CAP = space(8)
  w_BALOCALI = space(32)
  w_BAPROVIN = space(2)
  w_BACINEUR = space(2)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'BAN_CHE','gsar_aba')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_abaPag1","gsar_aba",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Banca")
      .Pages(1).HelpContextID = 250662422
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oBACODBAN_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='COD_ABI'
    this.cWorkTables[2]='COD_CAB'
    this.cWorkTables[3]='BAN_CHE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.BAN_CHE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.BAN_CHE_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_BACODBAN = NVL(BACODBAN,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from BAN_CHE where BACODBAN=KeySet.BACODBAN
    *
    i_nConn = i_TableProp[this.BAN_CHE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('BAN_CHE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "BAN_CHE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' BAN_CHE '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'BACODBAN',this.w_BACODBAN  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_BACODBAN = NVL(BACODBAN,space(10))
        .w_BAFLBEST = NVL(BAFLBEST,space(1))
        .w_BAFLNOAR = NVL(BAFLNOAR,space(1))
        .w_BACODABI = NVL(BACODABI,space(5))
          * evitabile
          *.link_1_4('Load')
        .w_BADESABI = NVL(BADESABI,space(80))
        .w_BACINABI = NVL(BACINABI,space(1))
        .w_BACODCAB = NVL(BACODCAB,space(5))
          * evitabile
          *.link_1_7('Load')
        .w_BADESFIL = NVL(BADESFIL,space(40))
        .w_BACINCAB = NVL(BACINCAB,space(1))
        .w_BAPASEUR = NVL(BAPASEUR,space(2))
        .w_BADESBAN = NVL(BADESBAN,space(50))
        .w_BAINDIRI = NVL(BAINDIRI,space(50))
        .w_BA___CAP = NVL(BA___CAP,space(8))
        .w_BALOCALI = NVL(BALOCALI,space(32))
        .w_BAPROVIN = NVL(BAPROVIN,space(2))
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .w_BACINEUR = NVL(BACINEUR,space(2))
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate()
        cp_LoadRecExtFlds(this,'BAN_CHE')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_BACODBAN = space(10)
      .w_BAFLBEST = space(1)
      .w_BAFLNOAR = space(1)
      .w_BACODABI = space(5)
      .w_BADESABI = space(80)
      .w_BACINABI = space(1)
      .w_BACODCAB = space(5)
      .w_BADESFIL = space(40)
      .w_BACINCAB = space(1)
      .w_BAPASEUR = space(2)
      .w_BADESBAN = space(50)
      .w_BAINDIRI = space(50)
      .w_BA___CAP = space(8)
      .w_BALOCALI = space(32)
      .w_BAPROVIN = space(2)
      .w_BACINEUR = space(2)
      if .cFunction<>"Filter"
          .DoRTCalc(1,2,.f.)
        .w_BAFLNOAR = IIF(.w_BAFLBEST<>'S','N',.w_BAFLNOAR)
        .w_BACODABI = IIF(.w_BAFLBEST='S', SPACE(5), .w_BACODABI)
        .DoRTCalc(4,4,.f.)
          if not(empty(.w_BACODABI))
          .link_1_4('Full')
          endif
          .DoRTCalc(5,6,.f.)
        .w_BACODCAB = IIF(.w_BAFLBEST='S', SPACE(5), .w_BACODCAB)
        .DoRTCalc(7,7,.f.)
          if not(empty(.w_BACODCAB))
          .link_1_7('Full')
          endif
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'BAN_CHE')
    this.DoRTCalc(8,16,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oBACODBAN_1_1.enabled = i_bVal
      .Page1.oPag.oBAFLBEST_1_2.enabled = i_bVal
      .Page1.oPag.oBAFLNOAR_1_3.enabled = i_bVal
      .Page1.oPag.oBACODABI_1_4.enabled = i_bVal
      .Page1.oPag.oBADESABI_1_5.enabled = i_bVal
      .Page1.oPag.oBACODCAB_1_7.enabled = i_bVal
      .Page1.oPag.oBADESFIL_1_8.enabled = i_bVal
      .Page1.oPag.oBAPASEUR_1_10.enabled = i_bVal
      .Page1.oPag.oBADESBAN_1_11.enabled = i_bVal
      .Page1.oPag.oBAINDIRI_1_12.enabled = i_bVal
      .Page1.oPag.oBA___CAP_1_13.enabled = i_bVal
      .Page1.oPag.oBALOCALI_1_14.enabled = i_bVal
      .Page1.oPag.oBAPROVIN_1_15.enabled = i_bVal
      .Page1.oPag.oObj_1_23.enabled = i_bVal
      .Page1.oPag.oObj_1_26.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oBACODBAN_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oBACODBAN_1_1.enabled = .t.
        .Page1.oPag.oBACODABI_1_4.enabled = .t.
        .Page1.oPag.oBACODCAB_1_7.enabled = .t.
        .Page1.oPag.oBADESBAN_1_11.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'BAN_CHE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.BAN_CHE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BACODBAN,"BACODBAN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BAFLBEST,"BAFLBEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BAFLNOAR,"BAFLNOAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BACODABI,"BACODABI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BADESABI,"BADESABI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BACINABI,"BACINABI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BACODCAB,"BACODCAB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BADESFIL,"BADESFIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BACINCAB,"BACINCAB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BAPASEUR,"BAPASEUR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BADESBAN,"BADESBAN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BAINDIRI,"BAINDIRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BA___CAP,"BA___CAP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BALOCALI,"BALOCALI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BAPROVIN,"BAPROVIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BACINEUR,"BACINEUR",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.BAN_CHE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])
    i_lTable = "BAN_CHE"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.BAN_CHE_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSAR_SBA with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.BAN_CHE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.BAN_CHE_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into BAN_CHE
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'BAN_CHE')
        i_extval=cp_InsertValODBCExtFlds(this,'BAN_CHE')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(BACODBAN,BAFLBEST,BAFLNOAR,BACODABI,BADESABI"+;
                  ",BACINABI,BACODCAB,BADESFIL,BACINCAB,BAPASEUR"+;
                  ",BADESBAN,BAINDIRI,BA___CAP,BALOCALI,BAPROVIN"+;
                  ",BACINEUR "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_BACODBAN)+;
                  ","+cp_ToStrODBC(this.w_BAFLBEST)+;
                  ","+cp_ToStrODBC(this.w_BAFLNOAR)+;
                  ","+cp_ToStrODBCNull(this.w_BACODABI)+;
                  ","+cp_ToStrODBC(this.w_BADESABI)+;
                  ","+cp_ToStrODBC(this.w_BACINABI)+;
                  ","+cp_ToStrODBCNull(this.w_BACODCAB)+;
                  ","+cp_ToStrODBC(this.w_BADESFIL)+;
                  ","+cp_ToStrODBC(this.w_BACINCAB)+;
                  ","+cp_ToStrODBC(this.w_BAPASEUR)+;
                  ","+cp_ToStrODBC(this.w_BADESBAN)+;
                  ","+cp_ToStrODBC(this.w_BAINDIRI)+;
                  ","+cp_ToStrODBC(this.w_BA___CAP)+;
                  ","+cp_ToStrODBC(this.w_BALOCALI)+;
                  ","+cp_ToStrODBC(this.w_BAPROVIN)+;
                  ","+cp_ToStrODBC(this.w_BACINEUR)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'BAN_CHE')
        i_extval=cp_InsertValVFPExtFlds(this,'BAN_CHE')
        cp_CheckDeletedKey(i_cTable,0,'BACODBAN',this.w_BACODBAN)
        INSERT INTO (i_cTable);
              (BACODBAN,BAFLBEST,BAFLNOAR,BACODABI,BADESABI,BACINABI,BACODCAB,BADESFIL,BACINCAB,BAPASEUR,BADESBAN,BAINDIRI,BA___CAP,BALOCALI,BAPROVIN,BACINEUR  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_BACODBAN;
                  ,this.w_BAFLBEST;
                  ,this.w_BAFLNOAR;
                  ,this.w_BACODABI;
                  ,this.w_BADESABI;
                  ,this.w_BACINABI;
                  ,this.w_BACODCAB;
                  ,this.w_BADESFIL;
                  ,this.w_BACINCAB;
                  ,this.w_BAPASEUR;
                  ,this.w_BADESBAN;
                  ,this.w_BAINDIRI;
                  ,this.w_BA___CAP;
                  ,this.w_BALOCALI;
                  ,this.w_BAPROVIN;
                  ,this.w_BACINEUR;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.BAN_CHE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.BAN_CHE_IDX,i_nConn)
      *
      * update BAN_CHE
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'BAN_CHE')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " BAFLBEST="+cp_ToStrODBC(this.w_BAFLBEST)+;
             ",BAFLNOAR="+cp_ToStrODBC(this.w_BAFLNOAR)+;
             ",BACODABI="+cp_ToStrODBCNull(this.w_BACODABI)+;
             ",BADESABI="+cp_ToStrODBC(this.w_BADESABI)+;
             ",BACINABI="+cp_ToStrODBC(this.w_BACINABI)+;
             ",BACODCAB="+cp_ToStrODBCNull(this.w_BACODCAB)+;
             ",BADESFIL="+cp_ToStrODBC(this.w_BADESFIL)+;
             ",BACINCAB="+cp_ToStrODBC(this.w_BACINCAB)+;
             ",BAPASEUR="+cp_ToStrODBC(this.w_BAPASEUR)+;
             ",BADESBAN="+cp_ToStrODBC(this.w_BADESBAN)+;
             ",BAINDIRI="+cp_ToStrODBC(this.w_BAINDIRI)+;
             ",BA___CAP="+cp_ToStrODBC(this.w_BA___CAP)+;
             ",BALOCALI="+cp_ToStrODBC(this.w_BALOCALI)+;
             ",BAPROVIN="+cp_ToStrODBC(this.w_BAPROVIN)+;
             ",BACINEUR="+cp_ToStrODBC(this.w_BACINEUR)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'BAN_CHE')
        i_cWhere = cp_PKFox(i_cTable  ,'BACODBAN',this.w_BACODBAN  )
        UPDATE (i_cTable) SET;
              BAFLBEST=this.w_BAFLBEST;
             ,BAFLNOAR=this.w_BAFLNOAR;
             ,BACODABI=this.w_BACODABI;
             ,BADESABI=this.w_BADESABI;
             ,BACINABI=this.w_BACINABI;
             ,BACODCAB=this.w_BACODCAB;
             ,BADESFIL=this.w_BADESFIL;
             ,BACINCAB=this.w_BACINCAB;
             ,BAPASEUR=this.w_BAPASEUR;
             ,BADESBAN=this.w_BADESBAN;
             ,BAINDIRI=this.w_BAINDIRI;
             ,BA___CAP=this.w_BA___CAP;
             ,BALOCALI=this.w_BALOCALI;
             ,BAPROVIN=this.w_BAPROVIN;
             ,BACINEUR=this.w_BACINEUR;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.BAN_CHE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.BAN_CHE_IDX,i_nConn)
      *
      * delete BAN_CHE
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'BACODBAN',this.w_BACODBAN  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.BAN_CHE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_BAFLBEST<>.w_BAFLBEST
            .w_BAFLNOAR = IIF(.w_BAFLBEST<>'S','N',.w_BAFLNOAR)
        endif
        if .o_BAFLBEST<>.w_BAFLBEST
            .w_BACODABI = IIF(.w_BAFLBEST='S', SPACE(5), .w_BACODABI)
          .link_1_4('Full')
        endif
        .DoRTCalc(5,6,.t.)
        if .o_BAFLBEST<>.w_BAFLBEST
            .w_BACODCAB = IIF(.w_BAFLBEST='S', SPACE(5), .w_BACODCAB)
          .link_1_7('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(8,16,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBACODABI_1_4.enabled = this.oPgFrm.Page1.oPag.oBACODABI_1_4.mCond()
    this.oPgFrm.Page1.oPag.oBADESABI_1_5.enabled = this.oPgFrm.Page1.oPag.oBADESABI_1_5.mCond()
    this.oPgFrm.Page1.oPag.oBACODCAB_1_7.enabled = this.oPgFrm.Page1.oPag.oBACODCAB_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBADESFIL_1_8.enabled = this.oPgFrm.Page1.oPag.oBADESFIL_1_8.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBAFLNOAR_1_3.visible=!this.oPgFrm.Page1.oPag.oBAFLNOAR_1_3.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_23.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_26.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=BACODABI
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_ABI_IDX,3]
    i_lTable = "COD_ABI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_ABI_IDX,2], .t., this.COD_ABI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_ABI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_BACODABI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ABI',True,'COD_ABI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ABCODABI like "+cp_ToStrODBC(trim(this.w_BACODABI)+"%");

          i_ret=cp_SQL(i_nConn,"select ABCODABI,ABDESABI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ABCODABI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ABCODABI',trim(this.w_BACODABI))
          select ABCODABI,ABDESABI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ABCODABI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_BACODABI)==trim(_Link_.ABCODABI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_BACODABI) and !this.bDontReportError
            deferred_cp_zoom('COD_ABI','*','ABCODABI',cp_AbsName(oSource.parent,'oBACODABI_1_4'),i_cWhere,'GSAR_ABI',"",'GSAR_BBA("A").COD_ABI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ABCODABI,ABDESABI";
                     +" from "+i_cTable+" "+i_lTable+" where ABCODABI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ABCODABI',oSource.xKey(1))
            select ABCODABI,ABDESABI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_BACODABI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ABCODABI,ABDESABI";
                   +" from "+i_cTable+" "+i_lTable+" where ABCODABI="+cp_ToStrODBC(this.w_BACODABI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ABCODABI',this.w_BACODABI)
            select ABCODABI,ABDESABI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_BACODABI = NVL(_Link_.ABCODABI,space(5))
      this.w_BADESABI = NVL(_Link_.ABDESABI,space(80))
    else
      if i_cCtrl<>'Load'
        this.w_BACODABI = space(5)
      endif
      this.w_BADESABI = space(80)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_ABI_IDX,2])+'\'+cp_ToStr(_Link_.ABCODABI,1)
      cp_ShowWarn(i_cKey,this.COD_ABI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_BACODABI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=BACODCAB
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_CAB_IDX,3]
    i_lTable = "COD_CAB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_CAB_IDX,2], .t., this.COD_CAB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_CAB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_BACODCAB) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFI',True,'COD_CAB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FICODCAB like "+cp_ToStrODBC(trim(this.w_BACODCAB)+"%");
                   +" and FICODABI="+cp_ToStrODBC(this.w_BACODABI);

          i_ret=cp_SQL(i_nConn,"select FICODABI,FICODCAB,FI___CAP,FIINDIRI,FIDESFIL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FICODABI,FICODCAB","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FICODABI',this.w_BACODABI;
                     ,'FICODCAB',trim(this.w_BACODCAB))
          select FICODABI,FICODCAB,FI___CAP,FIINDIRI,FIDESFIL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FICODABI,FICODCAB into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_BACODCAB)==trim(_Link_.FICODCAB) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_BACODCAB) and !this.bDontReportError
            deferred_cp_zoom('COD_CAB','*','FICODABI,FICODCAB',cp_AbsName(oSource.parent,'oBACODCAB_1_7'),i_cWhere,'GSAR_AFI',"",'GSAR_BBA("C").COD_CAB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_BACODABI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FICODABI,FICODCAB,FI___CAP,FIINDIRI,FIDESFIL";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select FICODABI,FICODCAB,FI___CAP,FIINDIRI,FIDESFIL;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FICODABI,FICODCAB,FI___CAP,FIINDIRI,FIDESFIL";
                     +" from "+i_cTable+" "+i_lTable+" where FICODCAB="+cp_ToStrODBC(oSource.xKey(2));
                     +" and FICODABI="+cp_ToStrODBC(this.w_BACODABI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FICODABI',oSource.xKey(1);
                       ,'FICODCAB',oSource.xKey(2))
            select FICODABI,FICODCAB,FI___CAP,FIINDIRI,FIDESFIL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_BACODCAB)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FICODABI,FICODCAB,FI___CAP,FIINDIRI,FIDESFIL";
                   +" from "+i_cTable+" "+i_lTable+" where FICODCAB="+cp_ToStrODBC(this.w_BACODCAB);
                   +" and FICODABI="+cp_ToStrODBC(this.w_BACODABI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FICODABI',this.w_BACODABI;
                       ,'FICODCAB',this.w_BACODCAB)
            select FICODABI,FICODCAB,FI___CAP,FIINDIRI,FIDESFIL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_BACODCAB = NVL(_Link_.FICODCAB,space(5))
      this.w_BA___CAP = NVL(_Link_.FI___CAP,space(8))
      this.w_BAINDIRI = NVL(_Link_.FIINDIRI,space(50))
      this.w_BADESFIL = NVL(_Link_.FIDESFIL,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_BACODCAB = space(5)
      endif
      this.w_BA___CAP = space(8)
      this.w_BAINDIRI = space(50)
      this.w_BADESFIL = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_CAB_IDX,2])+'\'+cp_ToStr(_Link_.FICODABI,1)+'\'+cp_ToStr(_Link_.FICODCAB,1)
      cp_ShowWarn(i_cKey,this.COD_CAB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_BACODCAB Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oBACODBAN_1_1.value==this.w_BACODBAN)
      this.oPgFrm.Page1.oPag.oBACODBAN_1_1.value=this.w_BACODBAN
    endif
    if not(this.oPgFrm.Page1.oPag.oBAFLBEST_1_2.RadioValue()==this.w_BAFLBEST)
      this.oPgFrm.Page1.oPag.oBAFLBEST_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oBAFLNOAR_1_3.RadioValue()==this.w_BAFLNOAR)
      this.oPgFrm.Page1.oPag.oBAFLNOAR_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oBACODABI_1_4.value==this.w_BACODABI)
      this.oPgFrm.Page1.oPag.oBACODABI_1_4.value=this.w_BACODABI
    endif
    if not(this.oPgFrm.Page1.oPag.oBADESABI_1_5.value==this.w_BADESABI)
      this.oPgFrm.Page1.oPag.oBADESABI_1_5.value=this.w_BADESABI
    endif
    if not(this.oPgFrm.Page1.oPag.oBACODCAB_1_7.value==this.w_BACODCAB)
      this.oPgFrm.Page1.oPag.oBACODCAB_1_7.value=this.w_BACODCAB
    endif
    if not(this.oPgFrm.Page1.oPag.oBADESFIL_1_8.value==this.w_BADESFIL)
      this.oPgFrm.Page1.oPag.oBADESFIL_1_8.value=this.w_BADESFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oBAPASEUR_1_10.value==this.w_BAPASEUR)
      this.oPgFrm.Page1.oPag.oBAPASEUR_1_10.value=this.w_BAPASEUR
    endif
    if not(this.oPgFrm.Page1.oPag.oBADESBAN_1_11.value==this.w_BADESBAN)
      this.oPgFrm.Page1.oPag.oBADESBAN_1_11.value=this.w_BADESBAN
    endif
    if not(this.oPgFrm.Page1.oPag.oBAINDIRI_1_12.value==this.w_BAINDIRI)
      this.oPgFrm.Page1.oPag.oBAINDIRI_1_12.value=this.w_BAINDIRI
    endif
    if not(this.oPgFrm.Page1.oPag.oBA___CAP_1_13.value==this.w_BA___CAP)
      this.oPgFrm.Page1.oPag.oBA___CAP_1_13.value=this.w_BA___CAP
    endif
    if not(this.oPgFrm.Page1.oPag.oBALOCALI_1_14.value==this.w_BALOCALI)
      this.oPgFrm.Page1.oPag.oBALOCALI_1_14.value=this.w_BALOCALI
    endif
    if not(this.oPgFrm.Page1.oPag.oBAPROVIN_1_15.value==this.w_BAPROVIN)
      this.oPgFrm.Page1.oPag.oBAPROVIN_1_15.value=this.w_BAPROVIN
    endif
    cp_SetControlsValueExtFlds(this,'BAN_CHE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- gsar_aba
      if i_bRes = .t. and EMPTY(.w_BACODBAN)
         i_cErrorMsg = Ah_MsgFormat("Codice Banca non Definito")
          i_bRes = .f.
          i_bnoChk = .f.
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_BAFLBEST = this.w_BAFLBEST
    return

enddefine

* --- Define pages as container
define class tgsar_abaPag1 as StdContainer
  Width  = 581
  height = 200
  stdWidth  = 581
  stdheight = 200
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oBACODBAN_1_1 as StdField with uid="EGWMDUCNRB",rtseq=1,rtrep=.f.,;
    cFormVar = "w_BACODBAN", cQueryName = "BACODBAN",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice mnemonico della banca",;
    HelpContextID = 16122268,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=100, Left=102, Top=12, InputMask=replicate('X',10)

  add object oBAFLBEST_1_2 as StdCheck with uid="DMKRCSAMKK",rtseq=2,rtrep=.f.,left=215, top=12, caption="Banca estera",;
    ToolTipText = "Se attivo: banca estera",;
    HelpContextID = 236507542,;
    cFormVar="w_BAFLBEST", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oBAFLBEST_1_2.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oBAFLBEST_1_2.GetRadio()
    this.Parent.oContained.w_BAFLBEST = this.RadioValue()
    return .t.
  endfunc

  func oBAFLBEST_1_2.SetRadio()
    this.Parent.oContained.w_BAFLBEST=trim(this.Parent.oContained.w_BAFLBEST)
    this.value = ;
      iif(this.Parent.oContained.w_BAFLBEST=='S',1,;
      0)
  endfunc

  add object oBAFLNOAR_1_3 as StdCheck with uid="KCZZEXHQHH",rtseq=3,rtrep=.f.,left=353, top=12, caption="No Area SEPA",;
    ToolTipText = "Se attivo identifica banche localizzate in paesi non aderenti all'area SEPA",;
    HelpContextID = 56152472,;
    cFormVar="w_BAFLNOAR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oBAFLNOAR_1_3.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oBAFLNOAR_1_3.GetRadio()
    this.Parent.oContained.w_BAFLNOAR = this.RadioValue()
    return .t.
  endfunc

  func oBAFLNOAR_1_3.SetRadio()
    this.Parent.oContained.w_BAFLNOAR=trim(this.Parent.oContained.w_BAFLNOAR)
    this.value = ;
      iif(this.Parent.oContained.w_BAFLNOAR=='S',1,;
      0)
  endfunc

  func oBAFLNOAR_1_3.mHide()
    with this.Parent.oContained
      return (.w_BAFLBEST<>'S')
    endwith
  endfunc

  add object oBACODABI_1_4 as StdField with uid="ZCNVUGZOWL",rtseq=4,rtrep=.f.,;
    cFormVar = "w_BACODABI", cQueryName = "BACODABI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice ABI della banca (F9 carica i codici da archivio ABI)",;
    HelpContextID = 32899489,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=102, Top=39, cSayPict="'99999'", cGetPict="'99999'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_ABI", cZoomOnZoom="GSAR_ABI", oKey_1_1="ABCODABI", oKey_1_2="this.w_BACODABI"

  func oBACODABI_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_BAFLBEST<>'S')
    endwith
   endif
  endfunc

  func oBACODABI_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
      if .not. empty(.w_BACODCAB)
        bRes2=.link_1_7('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oBACODABI_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oBACODABI_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_ABI','*','ABCODABI',cp_AbsName(this.parent,'oBACODABI_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ABI',"",'GSAR_BBA("A").COD_ABI_VZM',this.parent.oContained
  endproc
  proc oBACODABI_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ABI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ABCODABI=this.parent.oContained.w_BACODABI
     i_obj.ecpSave()
  endproc

  add object oBADESABI_1_5 as StdField with uid="EQMHXXMNPE",rtseq=5,rtrep=.f.,;
    cFormVar = "w_BADESABI", cQueryName = "BADESABI",;
    bObbl = .f. , nPag = 1, value=space(80), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione completa codice ABI",;
    HelpContextID = 17822113,;
   bGlobalFont=.t.,;
    Height=21, Width=401, Left=169, Top=39, InputMask=replicate('X',80)

  func oBADESABI_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_BACODABI))
    endwith
   endif
  endfunc

  add object oBACODCAB_1_7 as StdField with uid="LJDPKQGVVW",rtseq=7,rtrep=.f.,;
    cFormVar = "w_BACODCAB", cQueryName = "BACODABI,BACODCAB",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice CAB della filiale (F9 carica i codici da archivio CAB)",;
    HelpContextID = 267780520,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=102, Top=66, cSayPict="'99999'", cGetPict="'99999'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_CAB", cZoomOnZoom="GSAR_AFI", oKey_1_1="FICODABI", oKey_1_2="this.w_BACODABI", oKey_2_1="FICODCAB", oKey_2_2="this.w_BACODCAB"

  func oBACODCAB_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_BACODABI) AND .w_BAFLBEST<>'S')
    endwith
   endif
  endfunc

  func oBACODCAB_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oBACODCAB_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oBACODCAB_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.COD_CAB_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"FICODABI="+cp_ToStrODBC(this.Parent.oContained.w_BACODABI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"FICODABI="+cp_ToStr(this.Parent.oContained.w_BACODABI)
    endif
    do cp_zoom with 'COD_CAB','*','FICODABI,FICODCAB',cp_AbsName(this.parent,'oBACODCAB_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFI',"",'GSAR_BBA("C").COD_CAB_VZM',this.parent.oContained
  endproc
  proc oBACODCAB_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.FICODABI=w_BACODABI
     i_obj.w_FICODCAB=this.parent.oContained.w_BACODCAB
     i_obj.ecpSave()
  endproc

  add object oBADESFIL_1_8 as StdField with uid="HQGHEKXUNI",rtseq=8,rtrep=.f.,;
    cFormVar = "w_BADESFIL", cQueryName = "BADESFIL",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione completa codice CAB della filiale",;
    HelpContextID = 66063970,;
   bGlobalFont=.t.,;
    Height=21, Width=401, Left=169, Top=66, InputMask=replicate('X',40)

  func oBADESFIL_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_BACODCAB))
    endwith
   endif
  endfunc

  add object oBAPASEUR_1_10 as StdField with uid="BIJSZODFOH",rtseq=10,rtrep=.f.,;
    cFormVar = "w_BAPASEUR", cQueryName = "BAPASEUR",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Codice paese europeo",;
    HelpContextID = 49073768,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=102, Top=93, cSayPict="'!!'", cGetPict="'!!'", InputMask=replicate('X',2)

  add object oBADESBAN_1_11 as StdField with uid="ZAOBRCCFJE",rtseq=11,rtrep=.f.,;
    cFormVar = "w_BADESBAN", cQueryName = "BADESBAN",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Denominazione della banca",;
    HelpContextID = 1044892,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=102, Top=120, InputMask=replicate('X',50)

  add object oBAINDIRI_1_12 as StdField with uid="AAZGNMQTFK",rtseq=12,rtrep=.f.,;
    cFormVar = "w_BAINDIRI", cQueryName = "BAINDIRI",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo della banca",;
    HelpContextID = 167158177,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=102, Top=147, InputMask=replicate('X',50)

  add object oBA___CAP_1_13 as StdField with uid="SEVLKGCALE",rtseq=13,rtrep=.f.,;
    cFormVar = "w_BA___CAP", cQueryName = "BA___CAP",;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Codice di avviamento postale del luogo dove � situata la banca",;
    HelpContextID = 238305690,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=102, Top=174, cSayPict='"99999999"', cGetPict='"99999999"', InputMask=replicate('X',8), bHasZoom = .t. 

  proc oBA___CAP_1_13.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_BA___CAP",".w_BALOCALI",".w_BAPROVIN")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oBALOCALI_1_14 as StdField with uid="FQMSSBRLBG",rtseq=14,rtrep=.f.,;
    cFormVar = "w_BALOCALI", cQueryName = "BALOCALI",;
    bObbl = .f. , nPag = 1, value=space(32), bMultilanguage =  .f.,;
    ToolTipText = "Localit� dove � situata la banca",;
    HelpContextID = 234524255,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=176, Top=174, InputMask=replicate('X',32), bHasZoom = .t. 

  proc oBALOCALI_1_14.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_BA___CAP",".w_BALOCALI",".w_BAPROVIN")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oBAPROVIN_1_15 as StdField with uid="ETECPFVPHK",rtseq=15,rtrep=.f.,;
    cFormVar = "w_BAPROVIN", cQueryName = "BAPROVIN",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia della localit� dove � situata la banca",;
    HelpContextID = 62770788,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=467, Top=174, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. 

  proc oBAPROVIN_1_15.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P"," "," ",".w_BAPROVIN")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc


  add object oObj_1_23 as cp_runprogram with uid="THRHJIZAZQ",left=-4, top=222, width=224,height=19,;
    caption='GSAR_BBA(F)',;
   bGlobalFont=.t.,;
    prg="gsar_bba('F')",;
    cEvent = "w_BACODCAB Changed",;
    nPag=1;
    , HelpContextID = 255866585


  add object oObj_1_26 as cp_runprogram with uid="FKHDLRRBII",left=230, top=223, width=224,height=19,;
    caption='GSAR_BBA(G)',;
   bGlobalFont=.t.,;
    prg="gsar_bba('G')",;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 255866329

  add object oStr_1_16 as StdString with uid="CLYTRXSKMP",Visible=.t., Left=2, Top=147,;
    Alignment=1, Width=97, Height=15,;
    Caption="Indirizzo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="DKREINQOMW",Visible=.t., Left=2, Top=174,;
    Alignment=1, Width=97, Height=15,;
    Caption="CAP - localit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="SIDLXBSSGY",Visible=.t., Left=415, Top=174,;
    Alignment=1, Width=50, Height=15,;
    Caption="Prov.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="ACVSWISVGS",Visible=.t., Left=2, Top=39,;
    Alignment=1, Width=97, Height=18,;
    Caption="Codice ABI:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="IGSAERTLES",Visible=.t., Left=2, Top=66,;
    Alignment=1, Width=97, Height=18,;
    Caption="Codice CAB:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="RRXDIJLXLI",Visible=.t., Left=2, Top=12,;
    Alignment=1, Width=97, Height=15,;
    Caption="Codice banca:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_22 as StdString with uid="HVJPZJUSAU",Visible=.t., Left=2, Top=120,;
    Alignment=1, Width=97, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="TZBZOOUNNI",Visible=.t., Left=2, Top=93,;
    Alignment=1, Width=97, Height=18,;
    Caption="Paese:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_aba','BAN_CHE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".BACODBAN=BAN_CHE.BACODBAN";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
