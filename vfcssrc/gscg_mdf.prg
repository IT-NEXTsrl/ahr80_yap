* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_mdf                                                        *
*              Dettaglio importo definito                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2017-11-07                                                      *
* Last revis.: 2017-11-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgscg_mdf")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgscg_mdf")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgscg_mdf")
  return

* --- Class definition
define class tgscg_mdf as StdPCForm
  Width  = 769
  Height = 277
  Top    = 10
  Left   = 10
  cComment = "Dettaglio importo definito"
  cPrg = "gscg_mdf"
  HelpContextID=80356713
  add object cnt as tcgscg_mdf
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgscg_mdf as PCContext
  w_DFSERIAL = space(15)
  w_DFPROTEC = space(17)
  w_DFPRODOC = 0
  w_DFIMPDIC = 0
  w_DFDATOPE = space(8)
  w_DFCODUTE = 0
  w_DFNOTOPE = space(100)
  w_CPROWNUM = 0
  proc Save(i_oFrom)
    this.w_DFSERIAL = i_oFrom.w_DFSERIAL
    this.w_DFPROTEC = i_oFrom.w_DFPROTEC
    this.w_DFPRODOC = i_oFrom.w_DFPRODOC
    this.w_DFIMPDIC = i_oFrom.w_DFIMPDIC
    this.w_DFDATOPE = i_oFrom.w_DFDATOPE
    this.w_DFCODUTE = i_oFrom.w_DFCODUTE
    this.w_DFNOTOPE = i_oFrom.w_DFNOTOPE
    this.w_CPROWNUM = i_oFrom.w_CPROWNUM
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_DFSERIAL = this.w_DFSERIAL
    i_oTo.w_DFPROTEC = this.w_DFPROTEC
    i_oTo.w_DFPRODOC = this.w_DFPRODOC
    i_oTo.w_DFIMPDIC = this.w_DFIMPDIC
    i_oTo.w_DFDATOPE = this.w_DFDATOPE
    i_oTo.w_DFCODUTE = this.w_DFCODUTE
    i_oTo.w_DFNOTOPE = this.w_DFNOTOPE
    i_oTo.w_CPROWNUM = this.w_CPROWNUM
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgscg_mdf as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 769
  Height = 277
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2017-11-08"
  HelpContextID=80356713
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=8

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  DETTINTG_IDX = 0
  cFile = "DETTINTG"
  cKeySelect = "DFSERIAL"
  cKeyWhere  = "DFSERIAL=this.w_DFSERIAL"
  cKeyDetail  = "DFSERIAL=this.w_DFSERIAL and CPROWNUM=this.w_CPROWNUM"
  cKeyWhereODBC = '"DFSERIAL="+cp_ToStrODBC(this.w_DFSERIAL)';

  cKeyDetailWhereODBC = '"DFSERIAL="+cp_ToStrODBC(this.w_DFSERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC(this.w_CPROWNUM)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"DETTINTG.DFSERIAL="+cp_ToStrODBC(this.w_DFSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'DETTINTG.CPROWNUM '
  cPrg = "gscg_mdf"
  cComment = "Dettaglio importo definito"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 5
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DFSERIAL = space(15)
  w_DFPROTEC = space(17)
  w_DFPRODOC = 0
  w_DFIMPDIC = 0
  w_DFDATOPE = ctod('  /  /  ')
  w_DFCODUTE = 0
  w_DFNOTOPE = space(100)
  w_CPROWNUM = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_mdfPag1","gscg_mdf",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDFSERIAL_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='DETTINTG'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DETTINTG_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DETTINTG_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgscg_mdf'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from DETTINTG where DFSERIAL=KeySet.DFSERIAL
    *                            and CPROWNUM=KeySet.CPROWNUM
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.DETTINTG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DETTINTG_IDX,2],this.bLoadRecFilter,this.DETTINTG_IDX,"gscg_mdf")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DETTINTG')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DETTINTG.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DETTINTG '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DFSERIAL',this.w_DFSERIAL  )
      select * from (i_cTable) DETTINTG where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DFSERIAL = NVL(DFSERIAL,space(15))
        cp_LoadRecExtFlds(this,'DETTINTG')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_DFPROTEC = NVL(DFPROTEC,space(17))
          .w_DFPRODOC = NVL(DFPRODOC,0)
          .w_DFIMPDIC = NVL(DFIMPDIC,0)
          .w_DFDATOPE = NVL(cp_ToDate(DFDATOPE),ctod("  /  /  "))
          .w_DFCODUTE = NVL(DFCODUTE,0)
          .w_DFNOTOPE = NVL(DFNOTOPE,space(100))
          .w_CPROWNUM = NVL(CPROWNUM,0)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_DFSERIAL=space(15)
      .w_DFPROTEC=space(17)
      .w_DFPRODOC=0
      .w_DFIMPDIC=0
      .w_DFDATOPE=ctod("  /  /  ")
      .w_DFCODUTE=0
      .w_DFNOTOPE=space(100)
      .w_CPROWNUM=0
      if .cFunction<>"Filter"
      endif
    endwith
    cp_BlankRecExtFlds(this,'DETTINTG')
    this.DoRTCalc(1,8,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oDFSERIAL_1_1.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oDFSERIAL_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oDFSERIAL_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'DETTINTG',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DETTINTG_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFSERIAL,"DFSERIAL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_DFPROTEC C(17);
      ,t_DFPRODOC N(6);
      ,t_DFIMPDIC N(18,4);
      ,t_DFDATOPE D(8);
      ,t_DFCODUTE N(4);
      ,t_DFNOTOPE C(100);
      ,CPROWNUM N(10);
      ,t_CPROWNUM N(6);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgscg_mdfbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDFPROTEC_2_1.controlsource=this.cTrsName+'.t_DFPROTEC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDFPRODOC_2_2.controlsource=this.cTrsName+'.t_DFPRODOC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDFIMPDIC_2_3.controlsource=this.cTrsName+'.t_DFIMPDIC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDFDATOPE_2_4.controlsource=this.cTrsName+'.t_DFDATOPE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDFCODUTE_2_5.controlsource=this.cTrsName+'.t_DFCODUTE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDFNOTOPE_2_6.controlsource=this.cTrsName+'.t_DFNOTOPE'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFPROTEC_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DETTINTG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DETTINTG_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DETTINTG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DETTINTG_IDX,2])
      *
      * insert into DETTINTG
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DETTINTG')
        i_extval=cp_InsertValODBCExtFlds(this,'DETTINTG')
        i_cFldBody=" "+;
                  "(DFSERIAL,DFPROTEC,DFPRODOC,DFIMPDIC,DFDATOPE"+;
                  ",DFCODUTE,DFNOTOPE,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_DFSERIAL)+","+cp_ToStrODBC(this.w_DFPROTEC)+","+cp_ToStrODBC(this.w_DFPRODOC)+","+cp_ToStrODBC(this.w_DFIMPDIC)+","+cp_ToStrODBC(this.w_DFDATOPE)+;
             ","+cp_ToStrODBC(this.w_DFCODUTE)+","+cp_ToStrODBC(this.w_DFNOTOPE)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DETTINTG')
        i_extval=cp_InsertValVFPExtFlds(this,'DETTINTG')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'DFSERIAL',this.w_DFSERIAL,'CPROWNUM',this.w_CPROWNUM)
        INSERT INTO (i_cTable) (;
                   DFSERIAL;
                  ,DFPROTEC;
                  ,DFPRODOC;
                  ,DFIMPDIC;
                  ,DFDATOPE;
                  ,DFCODUTE;
                  ,DFNOTOPE;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_DFSERIAL;
                  ,this.w_DFPROTEC;
                  ,this.w_DFPRODOC;
                  ,this.w_DFIMPDIC;
                  ,this.w_DFDATOPE;
                  ,this.w_DFCODUTE;
                  ,this.w_DFNOTOPE;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.DETTINTG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DETTINTG_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_DFPROTEC))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'DETTINTG')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'DETTINTG')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_DFPROTEC))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update DETTINTG
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'DETTINTG')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " DFPROTEC="+cp_ToStrODBC(this.w_DFPROTEC)+;
                     ",DFPRODOC="+cp_ToStrODBC(this.w_DFPRODOC)+;
                     ",DFIMPDIC="+cp_ToStrODBC(this.w_DFIMPDIC)+;
                     ",DFDATOPE="+cp_ToStrODBC(this.w_DFDATOPE)+;
                     ",DFCODUTE="+cp_ToStrODBC(this.w_DFCODUTE)+;
                     ",DFNOTOPE="+cp_ToStrODBC(this.w_DFNOTOPE)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'DETTINTG')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      DFPROTEC=this.w_DFPROTEC;
                     ,DFPRODOC=this.w_DFPRODOC;
                     ,DFIMPDIC=this.w_DFIMPDIC;
                     ,DFDATOPE=this.w_DFDATOPE;
                     ,DFCODUTE=this.w_DFCODUTE;
                     ,DFNOTOPE=this.w_DFNOTOPE;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DETTINTG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DETTINTG_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_DFPROTEC))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete DETTINTG
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_DFPROTEC))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DETTINTG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DETTINTG_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,8,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_CPROWNUM with this.w_CPROWNUM
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oDFSERIAL_1_1.value==this.w_DFSERIAL)
      this.oPgFrm.Page1.oPag.oDFSERIAL_1_1.value=this.w_DFSERIAL
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFPROTEC_2_1.value==this.w_DFPROTEC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFPROTEC_2_1.value=this.w_DFPROTEC
      replace t_DFPROTEC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFPROTEC_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFPRODOC_2_2.value==this.w_DFPRODOC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFPRODOC_2_2.value=this.w_DFPRODOC
      replace t_DFPRODOC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFPRODOC_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFIMPDIC_2_3.value==this.w_DFIMPDIC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFIMPDIC_2_3.value=this.w_DFIMPDIC
      replace t_DFIMPDIC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFIMPDIC_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFDATOPE_2_4.value==this.w_DFDATOPE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFDATOPE_2_4.value=this.w_DFDATOPE
      replace t_DFDATOPE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFDATOPE_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFCODUTE_2_5.value==this.w_DFCODUTE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFCODUTE_2_5.value=this.w_DFCODUTE
      replace t_DFCODUTE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFCODUTE_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFNOTOPE_2_6.value==this.w_DFNOTOPE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFNOTOPE_2_6.value=this.w_DFNOTOPE
      replace t_DFNOTOPE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFNOTOPE_2_6.value
    endif
    cp_SetControlsValueExtFlds(this,'DETTINTG')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_DFPROTEC))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_DFPROTEC)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_DFPROTEC=space(17)
      .w_DFPRODOC=0
      .w_DFIMPDIC=0
      .w_DFDATOPE=ctod("  /  /  ")
      .w_DFCODUTE=0
      .w_DFNOTOPE=space(100)
    endwith
    this.DoRTCalc(1,8,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_DFPROTEC = t_DFPROTEC
    this.w_DFPRODOC = t_DFPRODOC
    this.w_DFIMPDIC = t_DFIMPDIC
    this.w_DFDATOPE = t_DFDATOPE
    this.w_DFCODUTE = t_DFCODUTE
    this.w_DFNOTOPE = t_DFNOTOPE
    this.w_CPROWNUM = t_CPROWNUM
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_DFPROTEC with this.w_DFPROTEC
    replace t_DFPRODOC with this.w_DFPRODOC
    replace t_DFIMPDIC with this.w_DFIMPDIC
    replace t_DFDATOPE with this.w_DFDATOPE
    replace t_DFCODUTE with this.w_DFCODUTE
    replace t_DFNOTOPE with this.w_DFNOTOPE
    replace t_CPROWNUM with this.w_CPROWNUM
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgscg_mdfPag1 as StdContainer
  Width  = 765
  height = 277
  stdWidth  = 765
  stdheight = 277
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDFSERIAL_1_1 as StdField with uid="JTNQLWHQDJ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_DFSERIAL", cQueryName = "DFSERIAL",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 161502594,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=178, Top=25, InputMask=replicate('X',15)

  add object oStr_1_2 as StdString with uid="BHEONVOQEN",Visible=.t., Left=6, Top=29,;
    Alignment=1, Width=171, Height=18,;
    Caption="Seriale dichiarazione di intento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="OHKUNBXDTZ",Visible=.t., Left=19, Top=51,;
    Alignment=0, Width=189, Height=18,;
    Caption="Protocollo telematico - Prima parte"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="PGGUPITTPT",Visible=.t., Left=156, Top=51,;
    Alignment=0, Width=205, Height=18,;
    Caption="Protocollo telematico - Parte seconda"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="HHNVCOTVNF",Visible=.t., Left=216, Top=51,;
    Alignment=0, Width=121, Height=18,;
    Caption="Totale importo definito"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="WEKXJAXAUZ",Visible=.t., Left=360, Top=51,;
    Alignment=0, Width=113, Height=18,;
    Caption="Data aggiornamento"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="LBXBOUWIBE",Visible=.t., Left=434, Top=51,;
    Alignment=0, Width=76, Height=18,;
    Caption="Codice utente"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="ZAOWMUEFGL",Visible=.t., Left=19, Top=61,;
    Alignment=0, Width=26, Height=18,;
    Caption="Note"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=10,top=73,;
    width=726+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=11,top=74,width=725+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgscg_mdfBodyRow as CPBodyRowCnt
  Width=716
  Height=int(fontmetric(1,"Arial",9,"")*2*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oDFPROTEC_2_1 as StdTrsField with uid="GWROEJDUIB",rtseq=2,rtrep=.t.,;
    cFormVar="w_DFPROTEC",value=space(17),;
    HelpContextID = 75310457,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=132, Left=-2, Top=0, InputMask=replicate('X',17)

  add object oDFPRODOC_2_2 as StdTrsField with uid="KKMIDSSASL",rtseq=3,rtrep=.t.,;
    cFormVar="w_DFPRODOC",value=0,;
    HelpContextID = 75310457,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=135, Top=0, cSayPict=["999999"], cGetPict=["999999"]

  add object oDFIMPDIC_2_3 as StdTrsField with uid="EOKYDFQNWV",rtseq=4,rtrep=.t.,;
    cFormVar="w_DFIMPDIC",value=0,;
    HelpContextID = 192432775,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=139, Left=195, Top=0, cSayPict=["9999999999999.9999"], cGetPict=["9999999999999.9999"]

  add object oDFDATOPE_2_4 as StdTrsField with uid="HQBWZUHUJZ",rtseq=5,rtrep=.t.,;
    cFormVar="w_DFDATOPE",value=ctod("  /  /  "),;
    HelpContextID = 263939451,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=69, Left=339, Top=0

  add object oDFCODUTE_2_5 as StdTrsField with uid="ESUCFSWYTL",rtseq=6,rtrep=.t.,;
    cFormVar="w_DFCODUTE",value=0,;
    HelpContextID = 80303483,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=41, Left=413, Top=0, cSayPict=["9999"], cGetPict=["9999"]

  add object oDFNOTOPE_2_6 as StdTrsField with uid="ZNJZYSCQGZ",rtseq=7,rtrep=.t.,;
    cFormVar="w_DFNOTOPE",value=space(100),;
    HelpContextID = 264897915,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=713, Left=-2, Top=19, InputMask=replicate('X',100)
  add object oLast as LastKeyMover
  * ---
  func oDFPROTEC_2_1.When()
    return(.t.)
  proc oDFPROTEC_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oDFPROTEC_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=4
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_mdf','DETTINTG','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DFSERIAL=DETTINTG.DFSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
