* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_msc                                                        *
*              Gestione saldaconto                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_134]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-12-16                                                      *
* Last revis.: 2016-04-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gscg_msc
PARAMETERS pTipGes
* --- Fine Area Manuale
return(createobject("tgscg_msc"))

* --- Class definition
define class tgscg_msc as StdTrsForm
  Top    = 10
  Left   = 15

  * --- Standard Properties
  Width  = 845
  Height = 530+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-04-15"
  HelpContextID=97133929
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=122

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  SALMDACO_IDX = 0
  SALDDACO_IDX = 0
  PAR_TITE_IDX = 0
  CONTI_IDX = 0
  ESERCIZI_IDX = 0
  VALUTE_IDX = 0
  CAU_CONT_IDX = 0
  CONTROPA_IDX = 0
  BAN_CHE_IDX = 0
  COC_MAST_IDX = 0
  PAG_2AME_IDX = 0
  cFile = "SALMDACO"
  cFileDetail = "SALDDACO"
  cKeySelect = "SCSERIAL"
  cKeyWhere  = "SCSERIAL=this.w_SCSERIAL"
  cKeyDetail  = "SCSERIAL=this.w_SCSERIAL and SCSERRIF=this.w_SCSERRIF and SCORDRIF=this.w_SCORDRIF and SCNUMRIF=this.w_SCNUMRIF"
  cKeyWhereODBC = '"SCSERIAL="+cp_ToStrODBC(this.w_SCSERIAL)';

  cKeyDetailWhereODBC = '"SCSERIAL="+cp_ToStrODBC(this.w_SCSERIAL)';
      +'+" and SCSERRIF="+cp_ToStrODBC(this.w_SCSERRIF)';
      +'+" and SCORDRIF="+cp_ToStrODBC(this.w_SCORDRIF)';
      +'+" and SCNUMRIF="+cp_ToStrODBC(this.w_SCNUMRIF)';

  cKeyWhereODBCqualified = '"SALDDACO.SCSERIAL="+cp_ToStrODBC(this.w_SCSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'SALDDACO.CPROWORD '
  cPrg = "gscg_msc"
  cComment = "Gestione saldaconto"
  i_nRowNum = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  cAutoZoom = 'gscg_msc'
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_READAZI = space(5)
  w_CONN = 0
  w_CAUSALE = space(5)
  w_SCTIPGES = space(1)
  o_SCTIPGES = space(1)
  w_SCSERIAL = space(10)
  o_SCSERIAL = space(10)
  w_PARSAL = space(1)
  w_SCDATREG = ctod('  /  /  ')
  o_SCDATREG = ctod('  /  /  ')
  w_SCNUMREG = 0
  w_SCNUMDOC = 0
  w_SCALFDOC = space(10)
  w_SCDATDOC = ctod('  /  /  ')
  o_SCDATDOC = ctod('  /  /  ')
  w_SCCODESE = space(4)
  w_SCDATINI = ctod('  /  /  ')
  w_SCDATFIN = ctod('  /  /  ')
  w_SCTIPCON = space(1)
  o_SCTIPCON = space(1)
  w_TIPCON = space(1)
  w_GTIPCON = space(1)
  w_SCCAOVAL = 0
  w_SCCODVAL = space(5)
  o_SCCODVAL = space(5)
  w_CAOVAL = 0
  w_DTVOBSO = ctod('  /  /  ')
  w_SCCONINI = space(15)
  o_SCCONINI = space(15)
  w_SCCONFIN = space(15)
  o_SCCONFIN = space(15)
  w_SCDESSUP = space(50)
  w_SCBANAPP = space(10)
  w_SCBANNOS = space(15)
  w_IMPSAL = 0
  w_IMPLIMITE = 0
  w_CODPAG = space(5)
  w_SCMODGEN = space(1)
  w_SCRIFCON = space(10)
  w_EXECSTAM = space(1)
  w_SCDESCRI = space(35)
  w_DESINI = space(40)
  w_DESFIN = space(40)
  w_DATOBSO = ctod('  /  /  ')
  w_PARTSN = space(1)
  w_PNIMPDAR = 0
  w_PNIMPAVE = 0
  w_OGG = space(10)
  w_OBTEST = ctod('  /  /  ')
  w_SIMVAL = space(3)
  w_PNVALNAZ = space(5)
  w_CAONAZ = 0
  w_DECTOP = 0
  w_PNDATDOC = ctod('  /  /  ')
  w_PNDATREG = ctod('  /  /  ')
  w_PNCODVAL = space(5)
  w_PNCAOVAL = 0
  w_PNTIPCON = space(1)
  w_PNCODCON = space(1)
  w_DECTOT = 0
  w_FASE = 0
  w_CONABBA = space(15)
  w_CONABBP = space(15)
  w_DESVAL = space(35)
  w_FLAGAC = space(1)
  w_RIFCONPN = space(10)
  w_CAUUSED = space(5)
  w_DESBAN = space(50)
  w_DESNOSBA = space(35)
  w_CONSBF = space(1)
  w_SCSERRIF = space(10)
  w_SCORDRIF = 0
  w_SCNUMRIF = 0
  w_CPROWORD = 0
  w_PTTIPCON = space(1)
  w_DATSCA = ctod('  /  /  ')
  w_NUMPAR = space(31)
  w_CODCON = space(15)
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_TOTIMP = 0
  w_HASEVENT = .F.
  w_HASEVCOP = space(50)
  w_SCRIFCOR = space(10)
  w_RAGSOC = space(60)
  w_PSERIAL = space(10)
  w_PROWORD = 0
  w_SCPAGRBA = space(2)
  w_SCPAGCAM = space(2)
  w_SCPAGRID = space(2)
  w_SCPAGRDR = space(2)
  w_SCPAGMAV = space(2)
  w_SCPAGBON = space(2)
  w_SCCAUCON = space(5)
  w_SCCAUCOF = space(5)
  w_SCCAUCOC = space(5)
  w_CAUCOC = space(5)
  o_CAUCOC = space(5)
  w_CAUSBF = space(5)
  o_CAUSBF = space(5)
  w_SCCONTRO = space(15)
  w_SCCONDCA = space(15)
  w_DESCAU = space(35)
  w_SCCONDCP = space(15)
  w_TIPREG = space(1)
  w_DESDCA = space(40)
  w_FLPART = space(1)
  w_DESDCP = space(40)
  w_DESCON = space(40)
  w_DESCAUF = space(35)
  w_DESCAUC = space(35)
  w_RIFERIMC = space(1)
  o_RIFERIMC = space(1)
  w_RIFERIMF = space(1)
  o_RIFERIMF = space(1)
  w_PNPRDC = space(2)
  w_TIPREGC = space(1)
  w_TIPREGF = space(1)
  w_TIPREGG = space(1)
  w_TIPDOCC = space(2)
  w_TIPDOCF = space(2)
  w_TIPDOCG = space(2)
  w_PNPRDF = space(2)
  w_PNPRDG = space(2)
  w_FLPDOCC = space(1)
  w_FLPDOCF = space(1)
  w_FLPDOCG = space(1)
  w_SERDOCC = space(10)
  w_SERDOCF = space(10)
  w_SERDOCG = space(10)
  w_PNPRD = space(1)
  w_SERDOC = space(10)
  w_FLPDOC = space(1)
  o_FLPDOC = space(1)
  w_OLDNUMDOC = 0

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_SCSERIAL = this.W_SCSERIAL
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_SCCODESE = this.W_SCCODESE
  op_SCNUMREG = this.W_SCNUMREG
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gscg_msc
  * ---- Parametro Gestione
   * ---- B: Basilea2
   * ---- M: Fisco azienda
  oserial=.null.
  pTipGes=' '
  * ---- Disattiva F6
  Proc F6()
      return
      DoDefault()
  EndProc
  
  Func ah_HasCPEvents(i_cOp)
  	  This.w_HASEVCOP=i_cop
  	  If (Upper(This.cFunction)="QUERY" And Upper(i_cop)='ECPDELETE')
        if this.nLoadTime=0
          this.LoadRecWarn()  && rilettura se sono passati piu' di 60 secondi dall' ultima lettura
          This.w_HASEVCOP=i_cop
        ENDIF      
        this.NotifyEvent('HasEvent')
  			return(this.w_HASEVENT)
  	  endif 
      if Upper(i_cop)='ECPQUIT' and used('selcon')
          USE IN SELECT("selcon")
      endif
    EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'SALMDACO','gscg_msc')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_mscPag1","gscg_msc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Saldaconto")
      .Pages(1).HelpContextID = 61962196
      .Pages(2).addobject("oPag","tgscg_mscPag2")
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Parametri")
      .Pages(2).HelpContextID = 107898872
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- gscg_msc
    * --- Imposta il Titolo della Finestra della Gestione in Base al tipo
    WITH THIS.PARENT
       IF Type('pTipGes')='U' Or EMPTY(pTipGes)
          .pTipges = 'S'
       ELSE
          .pTipges = pTipges
       ENDIF
    
       DO CASE
             CASE .pTipGes = 'S'
                   .cComment = Ah_Msgformat('Gestione saldaconto')
                   .cAutoZoom = 'GSCGSMSC'
                   This.Pages(1).Caption=cp_Translate("Saldaconto")
             CASE .pTipGes = 'B'
                   .cComment = Ah_Msgformat('Storno conti salvo buon fine')
                   .cAutoZoom = 'GSCGBMSC'
                   This.Pages(1).Caption=cp_Translate("Storno conti SBF")
       ENDCASE
    ENDWITH
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[11]
    this.cWorkTables[1]='PAR_TITE'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='ESERCIZI'
    this.cWorkTables[4]='VALUTE'
    this.cWorkTables[5]='CAU_CONT'
    this.cWorkTables[6]='CONTROPA'
    this.cWorkTables[7]='BAN_CHE'
    this.cWorkTables[8]='COC_MAST'
    this.cWorkTables[9]='PAG_2AME'
    this.cWorkTables[10]='SALMDACO'
    this.cWorkTables[11]='SALDDACO'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(11))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.SALMDACO_IDX,5],7]
    this.nPostItConn=i_TableProp[this.SALMDACO_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_SCSERIAL = NVL(SCSERIAL,space(10))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_19_joined
    link_1_19_joined=.f.
    local link_1_22_joined
    link_1_22_joined=.f.
    local link_1_23_joined
    link_1_23_joined=.f.
    local link_1_25_joined
    link_1_25_joined=.f.
    local link_1_26_joined
    link_1_26_joined=.f.
    local link_2_3_joined
    link_2_3_joined=.f.
    local link_4_7_joined
    link_4_7_joined=.f.
    local link_4_8_joined
    link_4_8_joined=.f.
    local link_4_9_joined
    link_4_9_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from SALMDACO where SCSERIAL=KeySet.SCSERIAL
    *
    i_nConn = i_TableProp[this.SALMDACO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SALMDACO_IDX,2],this.bLoadRecFilter,this.SALMDACO_IDX,"gscg_msc")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('SALMDACO')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "SALMDACO.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"SALDDACO.","SALMDACO.")
      i_cTable = i_cTable+' SALMDACO '
      link_1_19_joined=this.AddJoinedLink_1_19(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_22_joined=this.AddJoinedLink_1_22(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_23_joined=this.AddJoinedLink_1_23(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_25_joined=this.AddJoinedLink_1_25(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_26_joined=this.AddJoinedLink_1_26(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_7_joined=this.AddJoinedLink_4_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_8_joined=this.AddJoinedLink_4_8(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_9_joined=this.AddJoinedLink_4_9(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'SCSERIAL',this.w_SCSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_READAZI = i_CODAZI
        .w_CONN = 0
        .w_PARSAL = space(1)
        .w_CAOVAL = 0
        .w_DTVOBSO = ctod("  /  /  ")
        .w_IMPLIMITE = 0
        .w_CODPAG = space(5)
        .w_EXECSTAM = 'N'
        .w_DESINI = space(40)
        .w_DESFIN = space(40)
        .w_DATOBSO = ctod("  /  /  ")
        .w_PARTSN = space(1)
        .w_OGG = SPACE(10)
        .w_SIMVAL = space(3)
        .w_CAONAZ = 0
        .w_DECTOP = 0
        .w_DECTOT = 0
        .w_CONABBA = space(15)
        .w_CONABBP = space(15)
        .w_DESVAL = space(35)
        .w_RIFCONPN = space(10)
        .w_DESBAN = space(50)
        .w_DESNOSBA = space(35)
        .w_CONSBF = space(1)
        .w_HASEVENT = .f.
        .w_HASEVCOP = space(50)
        .w_CAUCOC = space(5)
        .w_CAUSBF = space(5)
        .w_DESCAU = space(35)
        .w_TIPREG = space(1)
        .w_DESDCA = space(40)
        .w_FLPART = space(1)
        .w_DESDCP = space(40)
        .w_DESCON = space(40)
        .w_DESCAUF = space(35)
        .w_DESCAUC = space(35)
        .w_RIFERIMC = space(1)
        .w_RIFERIMF = space(1)
        .w_TIPREGC = space(1)
        .w_TIPREGF = space(1)
        .w_TIPREGG = space(1)
        .w_TIPDOCC = space(2)
        .w_TIPDOCF = space(2)
        .w_TIPDOCG = space(2)
        .w_FLPDOCC = space(1)
        .w_FLPDOCF = space(1)
        .w_FLPDOCG = space(1)
        .w_SERDOCC = space(10)
        .w_SERDOCF = space(10)
        .w_SERDOCG = space(10)
        .w_OLDNUMDOC = 0
          .link_1_1('Load')
        .w_CAUSALE = IIF(.w_SCTIPCON='C',.w_SCCAUCON,IIF(.w_SCTIPCON='F',.w_SCCAUCOF,.w_SCCAUCOC))
        .w_SCTIPGES = NVL(SCTIPGES,space(1))
        .w_SCSERIAL = NVL(SCSERIAL,space(10))
        .op_SCSERIAL = .w_SCSERIAL
        .w_SCDATREG = NVL(cp_ToDate(SCDATREG),ctod("  /  /  "))
        .w_SCNUMREG = NVL(SCNUMREG,0)
        .op_SCNUMREG = .w_SCNUMREG
        .w_SCNUMDOC = NVL(SCNUMDOC,0)
        .w_SCALFDOC = NVL(SCALFDOC,space(10))
        .w_SCDATDOC = NVL(cp_ToDate(SCDATDOC),ctod("  /  /  "))
        .w_SCCODESE = NVL(SCCODESE,space(4))
        .op_SCCODESE = .w_SCCODESE
          * evitabile
          *.link_1_12('Load')
        .w_SCDATINI = NVL(cp_ToDate(SCDATINI),ctod("  /  /  "))
        .w_SCDATFIN = NVL(cp_ToDate(SCDATFIN),ctod("  /  /  "))
        .w_SCTIPCON = NVL(SCTIPCON,space(1))
        .w_TIPCON = .w_SCTIPCON
        .w_GTIPCON = 'G'
        .w_SCCAOVAL = NVL(SCCAOVAL,0)
        .w_SCCODVAL = NVL(SCCODVAL,space(5))
          if link_1_19_joined
            this.w_SCCODVAL = NVL(VACODVAL119,NVL(this.w_SCCODVAL,space(5)))
            this.w_DESVAL = NVL(VADESVAL119,space(35))
            this.w_CAOVAL = NVL(VACAOVAL119,0)
            this.w_DECTOT = NVL(VADECTOT119,0)
            this.w_SIMVAL = NVL(VASIMVAL119,space(3))
            this.w_DTVOBSO = NVL(cp_ToDate(VADTOBSO119),ctod("  /  /  "))
          else
          .link_1_19('Load')
          endif
        .w_SCCONINI = NVL(SCCONINI,space(15))
          if link_1_22_joined
            this.w_SCCONINI = NVL(ANCODICE122,NVL(this.w_SCCONINI,space(15)))
            this.w_DESINI = NVL(ANDESCRI122,space(40))
            this.w_PARTSN = NVL(ANPARTSN122,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO122),ctod("  /  /  "))
          else
          .link_1_22('Load')
          endif
        .w_SCCONFIN = NVL(SCCONFIN,space(15))
          if link_1_23_joined
            this.w_SCCONFIN = NVL(ANCODICE123,NVL(this.w_SCCONFIN,space(15)))
            this.w_DESFIN = NVL(ANDESCRI123,space(40))
            this.w_PARTSN = NVL(ANPARTSN123,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO123),ctod("  /  /  "))
          else
          .link_1_23('Load')
          endif
        .w_SCDESSUP = NVL(SCDESSUP,space(50))
        .w_SCBANAPP = NVL(SCBANAPP,space(10))
          if link_1_25_joined
            this.w_SCBANAPP = NVL(BACODBAN125,NVL(this.w_SCBANAPP,space(10)))
            this.w_DESBAN = NVL(BADESBAN125,space(50))
          else
          .link_1_25('Load')
          endif
        .w_SCBANNOS = NVL(SCBANNOS,space(15))
          if link_1_26_joined
            this.w_SCBANNOS = NVL(BACODBAN126,NVL(this.w_SCBANNOS,space(15)))
            this.w_DESNOSBA = NVL(BADESCRI126,space(35))
            this.w_CONSBF = NVL(BACONSBF126,space(1))
          else
          .link_1_26('Load')
          endif
        .w_IMPSAL = IIF(.w_SCCONINI == .w_SCCONFIN AND !EMPTY(.w_SCCONINI+.w_SCCONFIN),.w_IMPSAL,0)
          .link_1_29('Load')
        .w_SCMODGEN = NVL(SCMODGEN,space(1))
        .w_SCRIFCON = NVL(SCRIFCON,space(10))
        .w_SCDESCRI = NVL(SCDESCRI,space(35))
        .w_PNIMPDAR = iif(.w_SCTIPCON#'C' and .w_IMPSAL#0,.w_IMPSAL,0)
        .w_PNIMPAVE = IIF(.w_SCTIPCON='C' AND .w_IMPSAL#0,.w_IMPSAL,0)
        .w_OBTEST = i_DATSYS
        .w_PNVALNAZ = g_PERVAL
          .link_1_52('Load')
        .w_PNDATDOC = .w_SCDATDOC
        .w_PNDATREG = .w_SCDATREG
        .w_PNCODVAL = .w_SCCODVAL
        .w_PNCAOVAL = .w_SCCAOVAL
        .w_PNTIPCON = .w_SCTIPCON
        .w_PNCODCON = .w_SCCONINI
        .oPgFrm.Page1.oPag.oObj_1_64.Calculate()
        .w_FASE = 0
        .oPgFrm.Page1.oPag.oObj_1_73.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_77.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_78.Calculate()
        .w_FLAGAC = ' '
        .oPgFrm.Page1.oPag.oObj_1_81.Calculate(AH_Msgformat(IIF(.w_SCTIPGES='B', 'Codice conto SBF:','Codice conto iniziale:')))
        .oPgFrm.Page1.oPag.oObj_1_82.Calculate(AH_Msgformat(IIF(.w_SCTIPGES='B', 'Data valuta da:','Scadenze da:')))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_CAUUSED = icase(.w_SCTIPCON='C',.w_SCCAUCON,.w_SCTIPCON='F',.w_SCCAUCOF,.w_SCCAUCOC)
        .oPgFrm.Page1.oPag.oObj_3_1.Calculate(iif(.w_SCTIPCON='C','Rag. soc. cliente:',iif(.w_SCTIPCON='F','Rag. soc. fornitore:','Desc. conto:')))
        .w_SCPAGRBA = NVL(SCPAGRBA,space(2))
        .w_SCPAGCAM = NVL(SCPAGCAM,space(2))
        .w_SCPAGRID = NVL(SCPAGRID,space(2))
        .w_SCPAGRDR = NVL(SCPAGRDR,space(2))
        .w_SCPAGMAV = NVL(SCPAGMAV,space(2))
        .w_SCPAGBON = NVL(SCPAGBON,space(2))
        .w_SCCAUCON = NVL(SCCAUCON,space(5))
          if link_4_7_joined
            this.w_SCCAUCON = NVL(CCCODICE407,NVL(this.w_SCCAUCON,space(5)))
            this.w_DESCAU = NVL(CCDESCRI407,space(35))
            this.w_TIPREG = NVL(CCTIPREG407,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(CCDTOBSO407),ctod("  /  /  "))
            this.w_RIFERIMC = NVL(CCFLRIFE407,space(1))
            this.w_TIPDOCC = NVL(CCTIPDOC407,space(2))
            this.w_TIPREGC = NVL(CCTIPREG407,space(1))
            this.w_FLPDOCC = NVL(CCFLPDOC407,space(1))
            this.w_SERDOCC = NVL(CCSERDOC407,space(10))
          else
          .link_4_7('Load')
          endif
        .w_SCCAUCOF = NVL(SCCAUCOF,space(5))
          if link_4_8_joined
            this.w_SCCAUCOF = NVL(CCCODICE408,NVL(this.w_SCCAUCOF,space(5)))
            this.w_DESCAUF = NVL(CCDESCRI408,space(35))
            this.w_TIPREG = NVL(CCTIPREG408,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(CCDTOBSO408),ctod("  /  /  "))
            this.w_RIFERIMF = NVL(CCFLRIFE408,space(1))
            this.w_TIPDOCF = NVL(CCTIPDOC408,space(2))
            this.w_TIPREGF = NVL(CCTIPREG408,space(1))
            this.w_FLPDOCF = NVL(CCFLPDOC408,space(1))
            this.w_SERDOCF = NVL(CCSERDOC408,space(10))
          else
          .link_4_8('Load')
          endif
        .w_SCCAUCOC = NVL(SCCAUCOC,space(5))
          if link_4_9_joined
            this.w_SCCAUCOC = NVL(CCCODICE409,NVL(this.w_SCCAUCOC,space(5)))
            this.w_DESCAUC = NVL(CCDESCRI409,space(35))
            this.w_TIPREG = NVL(CCTIPREG409,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(CCDTOBSO409),ctod("  /  /  "))
            this.w_TIPDOCG = NVL(CCTIPDOC409,space(2))
            this.w_TIPREGG = NVL(CCTIPREG409,space(1))
            this.w_FLPDOCG = NVL(CCFLPDOC409,space(1))
            this.w_SERDOCG = NVL(CCSERDOC409,space(10))
          else
          .link_4_9('Load')
          endif
        .w_SCCONTRO = NVL(SCCONTRO,space(15))
          .link_4_12('Load')
        .w_SCCONDCA = NVL(SCCONDCA,space(15))
          .link_4_13('Load')
        .w_SCCONDCP = NVL(SCCONDCP,space(15))
          .link_4_15('Load')
        .oPgFrm.Page2.oPag.oObj_4_28.Calculate(AH_Msgformat(IIF(.w_SCTIPGES='B', 'Causale storno SBF:','Causale cont. conti:')))
        .w_PNPRDC = IIF(.w_TIPREGC='V' OR (.w_TIPREGC='C' AND .w_TIPDOCC='FC'), 'FV', 'NN')
        .w_PNPRDF = IIF(.w_TIPREGF='V' OR (.w_TIPREGF='C' AND .w_TIPDOCF='FC'), 'FV', 'NN')
        .w_PNPRDG = IIF(.w_TIPREGG='V' OR (.w_TIPREGG='C' AND .w_TIPDOCG='FC'), 'FV', 'NN')
        .w_PNPRD = IIF(.w_SCTIPCON='C',.w_PNPRDC,IIF(.w_SCTIPCON='F',.w_PNPRDF,.w_PNPRDG))
        .w_SERDOC = IIF(.w_SCTIPCON='C',.w_SERDOCC,IIF(.w_SCTIPCON='F',.w_SERDOCF,.w_SERDOCG))
        .w_FLPDOC = IIF(.w_SCTIPCON='C',.w_FLPDOCC,IIF(.w_SCTIPCON='F',.w_FLPDOCF,.w_FLPDOCG))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'SALMDACO')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from SALDDACO where SCSERIAL=KeySet.SCSERIAL
      *                            and SCSERRIF=KeySet.SCSERRIF
      *                            and SCORDRIF=KeySet.SCORDRIF
      *                            and SCNUMRIF=KeySet.SCNUMRIF
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.SALDDACO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SALDDACO_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('SALDDACO')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "SALDDACO.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" SALDDACO"
        link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'SCSERIAL',this.w_SCSERIAL  )
        select * from (i_cTable) SALDDACO where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_PTTIPCON = space(1)
          .w_DATSCA = ctod("  /  /  ")
          .w_NUMPAR = space(31)
          .w_CODCON = space(15)
          .w_NUMDOC = 0
          .w_ALFDOC = space(10)
          .w_TOTIMP = 0
          .w_RAGSOC = space(60)
          .w_PSERIAL = space(10)
          .w_PROWORD = 0
          .w_SCSERRIF = NVL(SCSERRIF,space(10))
          .w_SCORDRIF = NVL(SCORDRIF,0)
          .w_SCNUMRIF = NVL(SCNUMRIF,0)
          if link_2_3_joined
            this.w_SCNUMRIF = NVL(CPROWNUM203,NVL(this.w_SCNUMRIF,0))
            this.w_NUMPAR = NVL(PTNUMPAR203,space(31))
            this.w_DATSCA = NVL(cp_ToDate(PTDATSCA203),ctod("  /  /  "))
            this.w_NUMDOC = NVL(PTNUMDOC203,0)
            this.w_ALFDOC = NVL(PTALFDOC203,space(10))
            this.w_TOTIMP = NVL(PTTOTIMP203,0)
            this.w_CODCON = NVL(PTCODCON203,space(15))
            this.w_PROWORD = NVL(PTROWORD203,0)
            this.w_PSERIAL = NVL(PTSERIAL203,space(10))
            this.w_PTTIPCON = NVL(PTTIPCON203,space(1))
          else
          .link_2_3('Load')
          endif
          .w_CPROWORD = NVL(CPROWORD,0)
          .link_2_8('Load')
        .oPgFrm.Page1.oPag.oObj_2_12.Calculate(IIF(empty(.w_NUMPAR) AND .w_SCORDRIF<>0, AH_Msgformat('<Riga eliminata in primanota>'),''))
          .w_SCRIFCOR = NVL(SCRIFCOR,space(10))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace SCSERRIF with .w_SCSERRIF
          replace SCORDRIF with .w_SCORDRIF
          replace SCNUMRIF with .w_SCNUMRIF
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .w_CAUSALE = IIF(.w_SCTIPCON='C',.w_SCCAUCON,IIF(.w_SCTIPCON='F',.w_SCCAUCOF,.w_SCCAUCOC))
        .w_TIPCON = .w_SCTIPCON
        .w_GTIPCON = 'G'
        .w_IMPSAL = IIF(.w_SCCONINI == .w_SCCONFIN AND !EMPTY(.w_SCCONINI+.w_SCCONFIN),.w_IMPSAL,0)
        .w_PNIMPDAR = iif(.w_SCTIPCON#'C' and .w_IMPSAL#0,.w_IMPSAL,0)
        .w_PNIMPAVE = IIF(.w_SCTIPCON='C' AND .w_IMPSAL#0,.w_IMPSAL,0)
        .w_OBTEST = i_DATSYS
        .w_PNVALNAZ = g_PERVAL
        .w_PNDATDOC = .w_SCDATDOC
        .w_PNDATREG = .w_SCDATREG
        .w_PNCODVAL = .w_SCCODVAL
        .w_PNCAOVAL = .w_SCCAOVAL
        .w_PNTIPCON = .w_SCTIPCON
        .w_PNCODCON = .w_SCCONINI
        .oPgFrm.Page1.oPag.oObj_1_64.Calculate()
        .w_FASE = 0
        .oPgFrm.Page1.oPag.oObj_1_73.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_77.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_78.Calculate()
        .w_FLAGAC = ' '
        .oPgFrm.Page1.oPag.oObj_1_81.Calculate(AH_Msgformat(IIF(.w_SCTIPGES='B', 'Codice conto SBF:','Codice conto iniziale:')))
        .oPgFrm.Page1.oPag.oObj_1_82.Calculate(AH_Msgformat(IIF(.w_SCTIPGES='B', 'Data valuta da:','Scadenze da:')))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_CAUUSED = icase(.w_SCTIPCON='C',.w_SCCAUCON,.w_SCTIPCON='F',.w_SCCAUCOF,.w_SCCAUCOC)
        .oPgFrm.Page1.oPag.oObj_3_1.Calculate(iif(.w_SCTIPCON='C','Rag. soc. cliente:',iif(.w_SCTIPCON='F','Rag. soc. fornitore:','Desc. conto:')))
        .oPgFrm.Page2.oPag.oObj_4_28.Calculate(AH_Msgformat(IIF(.w_SCTIPGES='B', 'Causale storno SBF:','Causale cont. conti:')))
        .w_PNPRDC = IIF(.w_TIPREGC='V' OR (.w_TIPREGC='C' AND .w_TIPDOCC='FC'), 'FV', 'NN')
        .w_PNPRDF = IIF(.w_TIPREGF='V' OR (.w_TIPREGF='C' AND .w_TIPDOCF='FC'), 'FV', 'NN')
        .w_PNPRDG = IIF(.w_TIPREGG='V' OR (.w_TIPREGG='C' AND .w_TIPDOCG='FC'), 'FV', 'NN')
        .w_PNPRD = IIF(.w_SCTIPCON='C',.w_PNPRDC,IIF(.w_SCTIPCON='F',.w_PNPRDF,.w_PNPRDG))
        .w_SERDOC = IIF(.w_SCTIPCON='C',.w_SERDOCC,IIF(.w_SCTIPCON='F',.w_SERDOCF,.w_SERDOCG))
        .w_FLPDOC = IIF(.w_SCTIPCON='C',.w_FLPDOCC,IIF(.w_SCTIPCON='F',.w_FLPDOCF,.w_FLPDOCG))
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_1_34.enabled = .oPgFrm.Page1.oPag.oBtn_1_34.mCond()
        .oPgFrm.Page1.oPag.oBtn_1_35.enabled = .oPgFrm.Page1.oPag.oBtn_1_35.mCond()
        .oPgFrm.Page1.oPag.oBtn_1_38.enabled = .oPgFrm.Page1.oPag.oBtn_1_38.mCond()
        .oPgFrm.Page1.oPag.oBtn_1_40.enabled = .oPgFrm.Page1.oPag.oBtn_1_40.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_READAZI=space(5)
      .w_CONN=0
      .w_CAUSALE=space(5)
      .w_SCTIPGES=space(1)
      .w_SCSERIAL=space(10)
      .w_PARSAL=space(1)
      .w_SCDATREG=ctod("  /  /  ")
      .w_SCNUMREG=0
      .w_SCNUMDOC=0
      .w_SCALFDOC=space(10)
      .w_SCDATDOC=ctod("  /  /  ")
      .w_SCCODESE=space(4)
      .w_SCDATINI=ctod("  /  /  ")
      .w_SCDATFIN=ctod("  /  /  ")
      .w_SCTIPCON=space(1)
      .w_TIPCON=space(1)
      .w_GTIPCON=space(1)
      .w_SCCAOVAL=0
      .w_SCCODVAL=space(5)
      .w_CAOVAL=0
      .w_DTVOBSO=ctod("  /  /  ")
      .w_SCCONINI=space(15)
      .w_SCCONFIN=space(15)
      .w_SCDESSUP=space(50)
      .w_SCBANAPP=space(10)
      .w_SCBANNOS=space(15)
      .w_IMPSAL=0
      .w_IMPLIMITE=0
      .w_CODPAG=space(5)
      .w_SCMODGEN=space(1)
      .w_SCRIFCON=space(10)
      .w_EXECSTAM=space(1)
      .w_SCDESCRI=space(35)
      .w_DESINI=space(40)
      .w_DESFIN=space(40)
      .w_DATOBSO=ctod("  /  /  ")
      .w_PARTSN=space(1)
      .w_PNIMPDAR=0
      .w_PNIMPAVE=0
      .w_OGG=space(10)
      .w_OBTEST=ctod("  /  /  ")
      .w_SIMVAL=space(3)
      .w_PNVALNAZ=space(5)
      .w_CAONAZ=0
      .w_DECTOP=0
      .w_PNDATDOC=ctod("  /  /  ")
      .w_PNDATREG=ctod("  /  /  ")
      .w_PNCODVAL=space(5)
      .w_PNCAOVAL=0
      .w_PNTIPCON=space(1)
      .w_PNCODCON=space(1)
      .w_DECTOT=0
      .w_FASE=0
      .w_CONABBA=space(15)
      .w_CONABBP=space(15)
      .w_DESVAL=space(35)
      .w_FLAGAC=space(1)
      .w_RIFCONPN=space(10)
      .w_CAUUSED=space(5)
      .w_DESBAN=space(50)
      .w_DESNOSBA=space(35)
      .w_CONSBF=space(1)
      .w_SCSERRIF=space(10)
      .w_SCORDRIF=0
      .w_SCNUMRIF=0
      .w_CPROWORD=10
      .w_PTTIPCON=space(1)
      .w_DATSCA=ctod("  /  /  ")
      .w_NUMPAR=space(31)
      .w_CODCON=space(15)
      .w_NUMDOC=0
      .w_ALFDOC=space(10)
      .w_TOTIMP=0
      .w_HASEVENT=.f.
      .w_HASEVCOP=space(50)
      .w_SCRIFCOR=space(10)
      .w_RAGSOC=space(60)
      .w_PSERIAL=space(10)
      .w_PROWORD=0
      .w_SCPAGRBA=space(2)
      .w_SCPAGCAM=space(2)
      .w_SCPAGRID=space(2)
      .w_SCPAGRDR=space(2)
      .w_SCPAGMAV=space(2)
      .w_SCPAGBON=space(2)
      .w_SCCAUCON=space(5)
      .w_SCCAUCOF=space(5)
      .w_SCCAUCOC=space(5)
      .w_CAUCOC=space(5)
      .w_CAUSBF=space(5)
      .w_SCCONTRO=space(15)
      .w_SCCONDCA=space(15)
      .w_DESCAU=space(35)
      .w_SCCONDCP=space(15)
      .w_TIPREG=space(1)
      .w_DESDCA=space(40)
      .w_FLPART=space(1)
      .w_DESDCP=space(40)
      .w_DESCON=space(40)
      .w_DESCAUF=space(35)
      .w_DESCAUC=space(35)
      .w_RIFERIMC=space(1)
      .w_RIFERIMF=space(1)
      .w_PNPRDC=space(2)
      .w_TIPREGC=space(1)
      .w_TIPREGF=space(1)
      .w_TIPREGG=space(1)
      .w_TIPDOCC=space(2)
      .w_TIPDOCF=space(2)
      .w_TIPDOCG=space(2)
      .w_PNPRDF=space(2)
      .w_PNPRDG=space(2)
      .w_FLPDOCC=space(1)
      .w_FLPDOCF=space(1)
      .w_FLPDOCG=space(1)
      .w_SERDOCC=space(10)
      .w_SERDOCF=space(10)
      .w_SERDOCG=space(10)
      .w_PNPRD=space(1)
      .w_SERDOC=space(10)
      .w_FLPDOC=space(1)
      .w_OLDNUMDOC=0
      if .cFunction<>"Filter"
        .w_READAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_READAZI))
         .link_1_1('Full')
        endif
        .DoRTCalc(2,2,.f.)
        .w_CAUSALE = IIF(.w_SCTIPCON='C',.w_SCCAUCON,IIF(.w_SCTIPCON='F',.w_SCCAUCOF,.w_SCCAUCOC))
        .w_SCTIPGES = .pTipGes
        .DoRTCalc(5,6,.f.)
        .w_SCDATREG = i_datsys
        .DoRTCalc(8,10,.f.)
        .w_SCDATDOC = .w_SCDATREG
        .w_SCCODESE = g_CODESE
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_SCCODESE))
         .link_1_12('Full')
        endif
        .DoRTCalc(13,13,.f.)
        .w_SCDATFIN = IIF(.w_SCTIPGES='B',.w_SCDATREG,.w_SCDATFIN)
        .w_SCTIPCON = IIF(.w_SCTIPGES='B','G','C')
        .w_TIPCON = .w_SCTIPCON
        .w_GTIPCON = 'G'
        .w_SCCAOVAL = GETCAM(.w_SCCODVAL, IIF(EMPTY(.w_SCDATDOC), .w_SCDATREG, .w_SCDATDOC), 7)
        .w_SCCODVAL = g_PERVAL
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_SCCODVAL))
         .link_1_19('Full')
        endif
        .DoRTCalc(20,22,.f.)
        if not(empty(.w_SCCONINI))
         .link_1_22('Full')
        endif
        .w_SCCONFIN = .w_SCCONINI
        .DoRTCalc(23,23,.f.)
        if not(empty(.w_SCCONFIN))
         .link_1_23('Full')
        endif
        .DoRTCalc(24,24,.f.)
        .w_SCBANAPP = iif(.w_SCTIPCON = 'G','  ',.w_SCBANAPP)
        .DoRTCalc(25,25,.f.)
        if not(empty(.w_SCBANAPP))
         .link_1_25('Full')
        endif
        .w_SCBANNOS = iif(.w_SCTIPCON = 'G','  ',.w_SCBANNOS)
        .DoRTCalc(26,26,.f.)
        if not(empty(.w_SCBANNOS))
         .link_1_26('Full')
        endif
        .w_IMPSAL = IIF(.w_SCCONINI == .w_SCCONFIN AND !EMPTY(.w_SCCONINI+.w_SCCONFIN),.w_IMPSAL,0)
        .DoRTCalc(28,29,.f.)
        if not(empty(.w_CODPAG))
         .link_1_29('Full')
        endif
        .w_SCMODGEN = iif((.w_SCTIPCON+.w_RIFERIMC='CC') or (.w_SCTIPCON+.w_RIFERIMF='FF'),'I','G')
        .DoRTCalc(31,31,.f.)
        .w_EXECSTAM = 'N'
        .DoRTCalc(33,37,.f.)
        .w_PNIMPDAR = iif(.w_SCTIPCON#'C' and .w_IMPSAL#0,.w_IMPSAL,0)
        .w_PNIMPAVE = IIF(.w_SCTIPCON='C' AND .w_IMPSAL#0,.w_IMPSAL,0)
        .w_OGG = SPACE(10)
        .w_OBTEST = i_DATSYS
        .DoRTCalc(42,42,.f.)
        .w_PNVALNAZ = g_PERVAL
        .DoRTCalc(43,43,.f.)
        if not(empty(.w_PNVALNAZ))
         .link_1_52('Full')
        endif
        .DoRTCalc(44,45,.f.)
        .w_PNDATDOC = .w_SCDATDOC
        .w_PNDATREG = .w_SCDATREG
        .w_PNCODVAL = .w_SCCODVAL
        .w_PNCAOVAL = .w_SCCAOVAL
        .w_PNTIPCON = .w_SCTIPCON
        .w_PNCODCON = .w_SCCONINI
        .oPgFrm.Page1.oPag.oObj_1_64.Calculate()
        .DoRTCalc(52,52,.f.)
        .w_FASE = 0
        .oPgFrm.Page1.oPag.oObj_1_73.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_77.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_78.Calculate()
        .DoRTCalc(54,56,.f.)
        .w_FLAGAC = ' '
        .oPgFrm.Page1.oPag.oObj_1_81.Calculate(AH_Msgformat(IIF(.w_SCTIPGES='B', 'Codice conto SBF:','Codice conto iniziale:')))
        .oPgFrm.Page1.oPag.oObj_1_82.Calculate(AH_Msgformat(IIF(.w_SCTIPGES='B', 'Data valuta da:','Scadenze da:')))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(58,58,.f.)
        .w_CAUUSED = icase(.w_SCTIPCON='C',.w_SCCAUCON,.w_SCTIPCON='F',.w_SCCAUCOF,.w_SCCAUCOC)
        .DoRTCalc(60,65,.f.)
        if not(empty(.w_SCNUMRIF))
         .link_2_3('Full')
        endif
        .DoRTCalc(66,70,.f.)
        if not(empty(.w_CODCON))
         .link_2_8('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_2_12.Calculate(IIF(empty(.w_NUMPAR) AND .w_SCORDRIF<>0, AH_Msgformat('<Riga eliminata in primanota>'),''))
        .oPgFrm.Page1.oPag.oObj_3_1.Calculate(iif(.w_SCTIPCON='C','Rag. soc. cliente:',iif(.w_SCTIPCON='F','Rag. soc. fornitore:','Desc. conto:')))
        .DoRTCalc(71,79,.f.)
        .w_SCPAGRBA = 'XX'
        .w_SCPAGCAM = 'XX'
        .w_SCPAGRID = 'XX'
        .w_SCPAGRDR = 'XX'
        .w_SCPAGMAV = 'XX'
        .w_SCPAGBON = 'XX'
        .DoRTCalc(86,86,.f.)
        if not(empty(.w_SCCAUCON))
         .link_4_7('Full')
        endif
        .DoRTCalc(87,87,.f.)
        if not(empty(.w_SCCAUCOF))
         .link_4_8('Full')
        endif
        .w_SCCAUCOC = IIF(.w_SCTIPGES='S',.w_CAUCOC,.w_CAUSBF)
        .DoRTCalc(88,88,.f.)
        if not(empty(.w_SCCAUCOC))
         .link_4_9('Full')
        endif
        .DoRTCalc(89,91,.f.)
        if not(empty(.w_SCCONTRO))
         .link_4_12('Full')
        endif
        .DoRTCalc(92,92,.f.)
        if not(empty(.w_SCCONDCA))
         .link_4_13('Full')
        endif
        .DoRTCalc(93,94,.f.)
        if not(empty(.w_SCCONDCP))
         .link_4_15('Full')
        endif
        .oPgFrm.Page2.oPag.oObj_4_28.Calculate(AH_Msgformat(IIF(.w_SCTIPGES='B', 'Causale storno SBF:','Causale cont. conti:')))
        .DoRTCalc(95,103,.f.)
        .w_PNPRDC = IIF(.w_TIPREGC='V' OR (.w_TIPREGC='C' AND .w_TIPDOCC='FC'), 'FV', 'NN')
        .DoRTCalc(105,110,.f.)
        .w_PNPRDF = IIF(.w_TIPREGF='V' OR (.w_TIPREGF='C' AND .w_TIPDOCF='FC'), 'FV', 'NN')
        .w_PNPRDG = IIF(.w_TIPREGG='V' OR (.w_TIPREGG='C' AND .w_TIPDOCG='FC'), 'FV', 'NN')
        .DoRTCalc(113,118,.f.)
        .w_PNPRD = IIF(.w_SCTIPCON='C',.w_PNPRDC,IIF(.w_SCTIPCON='F',.w_PNPRDF,.w_PNPRDG))
        .w_SERDOC = IIF(.w_SCTIPCON='C',.w_SERDOCC,IIF(.w_SCTIPCON='F',.w_SERDOCF,.w_SERDOCG))
        .w_FLPDOC = IIF(.w_SCTIPCON='C',.w_FLPDOCC,IIF(.w_SCTIPCON='F',.w_FLPDOCF,.w_FLPDOCG))
      endif
    endwith
    cp_BlankRecExtFlds(this,'SALMDACO')
    this.DoRTCalc(122,122,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_1_34.enabled = this.oPgFrm.Page1.oPag.oBtn_1_34.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_35.enabled = this.oPgFrm.Page1.oPag.oBtn_1_35.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_38.enabled = this.oPgFrm.Page1.oPag.oBtn_1_38.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_40.enabled = this.oPgFrm.Page1.oPag.oBtn_1_40.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gscg_msc
    if this.w_FLPDOC#'N' And this.cFunction<>'Filter'
      this.w_CONN = i_TableProp[this.SALMDACO_IDX,3]
      cp_AskTableProg(this,this.w_CONN,"NDSALDAC","i_codazi,w_CAUSALE,w_SCCODESE,w_PNPRD,w_SERDOC,w_SCNUMDOC")
    else
      this.w_SCNUMDOC=0
    endif  
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oSCDATREG_1_7.enabled = i_bVal
      .Page1.oPag.oSCNUMREG_1_8.enabled = i_bVal
      .Page1.oPag.oSCNUMDOC_1_9.enabled = i_bVal
      .Page1.oPag.oSCALFDOC_1_10.enabled = i_bVal
      .Page1.oPag.oSCDATDOC_1_11.enabled = i_bVal
      .Page1.oPag.oSCCODESE_1_12.enabled = i_bVal
      .Page1.oPag.oSCDATINI_1_13.enabled = i_bVal
      .Page1.oPag.oSCDATFIN_1_14.enabled = i_bVal
      .Page1.oPag.oSCTIPCON_1_15.enabled = i_bVal
      .Page1.oPag.oSCCAOVAL_1_18.enabled = i_bVal
      .Page1.oPag.oSCCODVAL_1_19.enabled = i_bVal
      .Page1.oPag.oSCCONINI_1_22.enabled = i_bVal
      .Page1.oPag.oSCCONFIN_1_23.enabled = i_bVal
      .Page1.oPag.oSCDESSUP_1_24.enabled = i_bVal
      .Page1.oPag.oSCBANAPP_1_25.enabled = i_bVal
      .Page1.oPag.oSCBANNOS_1_26.enabled = i_bVal
      .Page1.oPag.oIMPSAL_1_27.enabled = i_bVal
      .Page1.oPag.oIMPLIMITE_1_28.enabled = i_bVal
      .Page1.oPag.oCODPAG_1_29.enabled = i_bVal
      .Page1.oPag.oEXECSTAM_1_32.enabled = i_bVal
      .Page2.oPag.oSCPAGRBA_4_1.enabled = i_bVal
      .Page2.oPag.oSCPAGCAM_4_2.enabled = i_bVal
      .Page2.oPag.oSCPAGRID_4_3.enabled = i_bVal
      .Page2.oPag.oSCPAGRDR_4_4.enabled = i_bVal
      .Page2.oPag.oSCPAGMAV_4_5.enabled = i_bVal
      .Page2.oPag.oSCPAGBON_4_6.enabled = i_bVal
      .Page2.oPag.oSCCAUCON_4_7.enabled = i_bVal
      .Page2.oPag.oSCCAUCOF_4_8.enabled = i_bVal
      .Page2.oPag.oSCCAUCOC_4_9.enabled = i_bVal
      .Page2.oPag.oSCCONTRO_4_12.enabled = i_bVal
      .Page2.oPag.oSCCONDCA_4_13.enabled = i_bVal
      .Page2.oPag.oSCCONDCP_4_15.enabled = i_bVal
      .Page1.oPag.oBtn_1_33.enabled = i_bVal
      .Page1.oPag.oBtn_1_34.enabled = .Page1.oPag.oBtn_1_34.mCond()
      .Page1.oPag.oBtn_1_35.enabled = .Page1.oPag.oBtn_1_35.mCond()
      .Page1.oPag.oBtn_1_37.enabled = i_bVal
      .Page1.oPag.oBtn_1_38.enabled = .Page1.oPag.oBtn_1_38.mCond()
      .Page1.oPag.oBtn_1_40.enabled = .Page1.oPag.oBtn_1_40.mCond()
      .Page1.oPag.oObj_1_64.enabled = i_bVal
      .Page1.oPag.oObj_1_73.enabled = i_bVal
      .Page1.oPag.oObj_1_77.enabled = i_bVal
      .Page1.oPag.oObj_1_78.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oSCDATREG_1_7.enabled = .t.
        .Page1.oPag.oSCNUMREG_1_8.enabled = .t.
        .Page1.oPag.oSCCODESE_1_12.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'SALMDACO',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.SALMDACO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SALMDACO_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SESALDAC","i_codazi,w_SCSERIAL")
      cp_AskTableProg(this,i_nConn,"PRSALDAC","i_codazi,w_SCCODESE,w_SCNUMREG")
      .op_codazi = .w_codazi
      .op_SCSERIAL = .w_SCSERIAL
      .op_codazi = .w_codazi
      .op_SCCODESE = .w_SCCODESE
      .op_SCNUMREG = .w_SCNUMREG
    endwith
    this.SetControlsValue()
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.SALMDACO_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCTIPGES,"SCTIPGES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCSERIAL,"SCSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCDATREG,"SCDATREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCNUMREG,"SCNUMREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCNUMDOC,"SCNUMDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCALFDOC,"SCALFDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCDATDOC,"SCDATDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCCODESE,"SCCODESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCDATINI,"SCDATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCDATFIN,"SCDATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCTIPCON,"SCTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCCAOVAL,"SCCAOVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCCODVAL,"SCCODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCCONINI,"SCCONINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCCONFIN,"SCCONFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCDESSUP,"SCDESSUP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCBANAPP,"SCBANAPP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCBANNOS,"SCBANNOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCMODGEN,"SCMODGEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCRIFCON,"SCRIFCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCDESCRI,"SCDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCPAGRBA,"SCPAGRBA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCPAGCAM,"SCPAGCAM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCPAGRID,"SCPAGRID",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCPAGRDR,"SCPAGRDR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCPAGMAV,"SCPAGMAV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCPAGBON,"SCPAGBON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCCAUCON,"SCCAUCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCCAUCOF,"SCCAUCOF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCCAUCOC,"SCCAUCOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCCONTRO,"SCCONTRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCCONDCA,"SCCONDCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCCONDCP,"SCCONDCP",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.SALMDACO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SALMDACO_IDX,2])
    i_lTable = "SALMDACO"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.SALMDACO_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(4);
      ,t_DATSCA D(8);
      ,t_NUMPAR C(31);
      ,t_CODCON C(15);
      ,t_NUMDOC N(15);
      ,t_ALFDOC C(10);
      ,t_TOTIMP N(18,4);
      ,t_RAGSOC C(60);
      ,SCSERRIF C(10);
      ,SCORDRIF N(3);
      ,SCNUMRIF N(4);
      ,t_SCSERRIF C(10);
      ,t_SCORDRIF N(3);
      ,t_SCNUMRIF N(4);
      ,t_PTTIPCON C(1);
      ,t_SCRIFCOR C(10);
      ,t_PSERIAL C(10);
      ,t_PROWORD N(4);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgscg_mscbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_4.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDATSCA_2_6.controlsource=this.cTrsName+'.t_DATSCA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oNUMPAR_2_7.controlsource=this.cTrsName+'.t_NUMPAR'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCODCON_2_8.controlsource=this.cTrsName+'.t_CODCON'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oNUMDOC_2_9.controlsource=this.cTrsName+'.t_NUMDOC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oALFDOC_2_10.controlsource=this.cTrsName+'.t_ALFDOC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTOTIMP_2_11.controlsource=this.cTrsName+'.t_TOTIMP'
    this.oPgFRm.Page1.oPag.oRAGSOC_2_16.controlsource=this.cTrsName+'.t_RAGSOC'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(53)
    this.AddVLine(123)
    this.AddVLine(355)
    this.AddVLine(475)
    this.AddVLine(595)
    this.AddVLine(680)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_4
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- gscg_msc
    actser=this.w_SCSERIAL
    this.w_SCSERIAL=space(10) 
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.SALMDACO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SALMDACO_IDX,2])
      with this
          cp_NextTableProg(this,i_nConn,"SESALDAC","i_codazi,w_SCSERIAL")
          cp_NextTableProg(this,i_nConn,"PRSALDAC","i_codazi,w_SCCODESE,w_SCNUMREG")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into SALMDACO
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'SALMDACO')
        i_extval=cp_InsertValODBCExtFlds(this,'SALMDACO')
        local i_cFld
        i_cFld=" "+;
                  "(SCTIPGES,SCSERIAL,SCDATREG,SCNUMREG,SCNUMDOC"+;
                  ",SCALFDOC,SCDATDOC,SCCODESE,SCDATINI,SCDATFIN"+;
                  ",SCTIPCON,SCCAOVAL,SCCODVAL,SCCONINI,SCCONFIN"+;
                  ",SCDESSUP,SCBANAPP,SCBANNOS,SCMODGEN,SCRIFCON"+;
                  ",SCDESCRI,SCPAGRBA,SCPAGCAM,SCPAGRID,SCPAGRDR"+;
                  ",SCPAGMAV,SCPAGBON,SCCAUCON,SCCAUCOF,SCCAUCOC"+;
                  ",SCCONTRO,SCCONDCA,SCCONDCP"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_SCTIPGES)+;
                    ","+cp_ToStrODBC(this.w_SCSERIAL)+;
                    ","+cp_ToStrODBC(this.w_SCDATREG)+;
                    ","+cp_ToStrODBC(this.w_SCNUMREG)+;
                    ","+cp_ToStrODBC(this.w_SCNUMDOC)+;
                    ","+cp_ToStrODBC(this.w_SCALFDOC)+;
                    ","+cp_ToStrODBC(this.w_SCDATDOC)+;
                    ","+cp_ToStrODBCNull(this.w_SCCODESE)+;
                    ","+cp_ToStrODBC(this.w_SCDATINI)+;
                    ","+cp_ToStrODBC(this.w_SCDATFIN)+;
                    ","+cp_ToStrODBC(this.w_SCTIPCON)+;
                    ","+cp_ToStrODBC(this.w_SCCAOVAL)+;
                    ","+cp_ToStrODBCNull(this.w_SCCODVAL)+;
                    ","+cp_ToStrODBCNull(this.w_SCCONINI)+;
                    ","+cp_ToStrODBCNull(this.w_SCCONFIN)+;
                    ","+cp_ToStrODBC(this.w_SCDESSUP)+;
                    ","+cp_ToStrODBCNull(this.w_SCBANAPP)+;
                    ","+cp_ToStrODBCNull(this.w_SCBANNOS)+;
                    ","+cp_ToStrODBC(this.w_SCMODGEN)+;
                    ","+cp_ToStrODBC(this.w_SCRIFCON)+;
                    ","+cp_ToStrODBC(this.w_SCDESCRI)+;
                    ","+cp_ToStrODBC(this.w_SCPAGRBA)+;
                    ","+cp_ToStrODBC(this.w_SCPAGCAM)+;
                    ","+cp_ToStrODBC(this.w_SCPAGRID)+;
                    ","+cp_ToStrODBC(this.w_SCPAGRDR)+;
                    ","+cp_ToStrODBC(this.w_SCPAGMAV)+;
                    ","+cp_ToStrODBC(this.w_SCPAGBON)+;
                    ","+cp_ToStrODBCNull(this.w_SCCAUCON)+;
                    ","+cp_ToStrODBCNull(this.w_SCCAUCOF)+;
                    ","+cp_ToStrODBCNull(this.w_SCCAUCOC)+;
                    ","+cp_ToStrODBCNull(this.w_SCCONTRO)+;
                    ","+cp_ToStrODBCNull(this.w_SCCONDCA)+;
                    ","+cp_ToStrODBCNull(this.w_SCCONDCP)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'SALMDACO')
        i_extval=cp_InsertValVFPExtFlds(this,'SALMDACO')
        cp_CheckDeletedKey(i_cTable,0,'SCSERIAL',this.w_SCSERIAL)
        INSERT INTO (i_cTable);
              (SCTIPGES,SCSERIAL,SCDATREG,SCNUMREG,SCNUMDOC,SCALFDOC,SCDATDOC,SCCODESE,SCDATINI,SCDATFIN,SCTIPCON,SCCAOVAL,SCCODVAL,SCCONINI,SCCONFIN,SCDESSUP,SCBANAPP,SCBANNOS,SCMODGEN,SCRIFCON,SCDESCRI,SCPAGRBA,SCPAGCAM,SCPAGRID,SCPAGRDR,SCPAGMAV,SCPAGBON,SCCAUCON,SCCAUCOF,SCCAUCOC,SCCONTRO,SCCONDCA,SCCONDCP &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_SCTIPGES;
                  ,this.w_SCSERIAL;
                  ,this.w_SCDATREG;
                  ,this.w_SCNUMREG;
                  ,this.w_SCNUMDOC;
                  ,this.w_SCALFDOC;
                  ,this.w_SCDATDOC;
                  ,this.w_SCCODESE;
                  ,this.w_SCDATINI;
                  ,this.w_SCDATFIN;
                  ,this.w_SCTIPCON;
                  ,this.w_SCCAOVAL;
                  ,this.w_SCCODVAL;
                  ,this.w_SCCONINI;
                  ,this.w_SCCONFIN;
                  ,this.w_SCDESSUP;
                  ,this.w_SCBANAPP;
                  ,this.w_SCBANNOS;
                  ,this.w_SCMODGEN;
                  ,this.w_SCRIFCON;
                  ,this.w_SCDESCRI;
                  ,this.w_SCPAGRBA;
                  ,this.w_SCPAGCAM;
                  ,this.w_SCPAGRID;
                  ,this.w_SCPAGRDR;
                  ,this.w_SCPAGMAV;
                  ,this.w_SCPAGBON;
                  ,this.w_SCCAUCON;
                  ,this.w_SCCAUCOF;
                  ,this.w_SCCAUCOC;
                  ,this.w_SCCONTRO;
                  ,this.w_SCCONDCA;
                  ,this.w_SCCONDCP;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.SALDDACO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SALDDACO_IDX,2])
      *
      * insert into SALDDACO
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(SCSERIAL,SCSERRIF,SCORDRIF,SCNUMRIF,CPROWORD"+;
                  ",SCRIFCOR,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_SCSERIAL)+","+cp_ToStrODBC(this.w_SCSERRIF)+","+cp_ToStrODBC(this.w_SCORDRIF)+","+cp_ToStrODBCNull(this.w_SCNUMRIF)+","+cp_ToStrODBC(this.w_CPROWORD)+;
             ","+cp_ToStrODBC(this.w_SCRIFCOR)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,0,'SCSERIAL',this.w_SCSERIAL,'SCSERRIF',this.w_SCSERRIF,'SCORDRIF',this.w_SCORDRIF,'SCNUMRIF',this.w_SCNUMRIF)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_SCSERIAL,this.w_SCSERRIF,this.w_SCORDRIF,this.w_SCNUMRIF,this.w_CPROWORD"+;
                ",this.w_SCRIFCOR,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.SALMDACO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SALMDACO_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update SALMDACO
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'SALMDACO')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " SCTIPGES="+cp_ToStrODBC(this.w_SCTIPGES)+;
             ",SCDATREG="+cp_ToStrODBC(this.w_SCDATREG)+;
             ",SCNUMREG="+cp_ToStrODBC(this.w_SCNUMREG)+;
             ",SCNUMDOC="+cp_ToStrODBC(this.w_SCNUMDOC)+;
             ",SCALFDOC="+cp_ToStrODBC(this.w_SCALFDOC)+;
             ",SCDATDOC="+cp_ToStrODBC(this.w_SCDATDOC)+;
             ",SCCODESE="+cp_ToStrODBCNull(this.w_SCCODESE)+;
             ",SCDATINI="+cp_ToStrODBC(this.w_SCDATINI)+;
             ",SCDATFIN="+cp_ToStrODBC(this.w_SCDATFIN)+;
             ",SCTIPCON="+cp_ToStrODBC(this.w_SCTIPCON)+;
             ",SCCAOVAL="+cp_ToStrODBC(this.w_SCCAOVAL)+;
             ",SCCODVAL="+cp_ToStrODBCNull(this.w_SCCODVAL)+;
             ",SCCONINI="+cp_ToStrODBCNull(this.w_SCCONINI)+;
             ",SCCONFIN="+cp_ToStrODBCNull(this.w_SCCONFIN)+;
             ",SCDESSUP="+cp_ToStrODBC(this.w_SCDESSUP)+;
             ",SCBANAPP="+cp_ToStrODBCNull(this.w_SCBANAPP)+;
             ",SCBANNOS="+cp_ToStrODBCNull(this.w_SCBANNOS)+;
             ",SCMODGEN="+cp_ToStrODBC(this.w_SCMODGEN)+;
             ",SCRIFCON="+cp_ToStrODBC(this.w_SCRIFCON)+;
             ",SCDESCRI="+cp_ToStrODBC(this.w_SCDESCRI)+;
             ",SCPAGRBA="+cp_ToStrODBC(this.w_SCPAGRBA)+;
             ",SCPAGCAM="+cp_ToStrODBC(this.w_SCPAGCAM)+;
             ",SCPAGRID="+cp_ToStrODBC(this.w_SCPAGRID)+;
             ",SCPAGRDR="+cp_ToStrODBC(this.w_SCPAGRDR)+;
             ",SCPAGMAV="+cp_ToStrODBC(this.w_SCPAGMAV)+;
             ",SCPAGBON="+cp_ToStrODBC(this.w_SCPAGBON)+;
             ",SCCAUCON="+cp_ToStrODBCNull(this.w_SCCAUCON)+;
             ",SCCAUCOF="+cp_ToStrODBCNull(this.w_SCCAUCOF)+;
             ",SCCAUCOC="+cp_ToStrODBCNull(this.w_SCCAUCOC)+;
             ",SCCONTRO="+cp_ToStrODBCNull(this.w_SCCONTRO)+;
             ",SCCONDCA="+cp_ToStrODBCNull(this.w_SCCONDCA)+;
             ",SCCONDCP="+cp_ToStrODBCNull(this.w_SCCONDCP)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'SALMDACO')
          i_cWhere = cp_PKFox(i_cTable  ,'SCSERIAL',this.w_SCSERIAL  )
          UPDATE (i_cTable) SET;
              SCTIPGES=this.w_SCTIPGES;
             ,SCDATREG=this.w_SCDATREG;
             ,SCNUMREG=this.w_SCNUMREG;
             ,SCNUMDOC=this.w_SCNUMDOC;
             ,SCALFDOC=this.w_SCALFDOC;
             ,SCDATDOC=this.w_SCDATDOC;
             ,SCCODESE=this.w_SCCODESE;
             ,SCDATINI=this.w_SCDATINI;
             ,SCDATFIN=this.w_SCDATFIN;
             ,SCTIPCON=this.w_SCTIPCON;
             ,SCCAOVAL=this.w_SCCAOVAL;
             ,SCCODVAL=this.w_SCCODVAL;
             ,SCCONINI=this.w_SCCONINI;
             ,SCCONFIN=this.w_SCCONFIN;
             ,SCDESSUP=this.w_SCDESSUP;
             ,SCBANAPP=this.w_SCBANAPP;
             ,SCBANNOS=this.w_SCBANNOS;
             ,SCMODGEN=this.w_SCMODGEN;
             ,SCRIFCON=this.w_SCRIFCON;
             ,SCDESCRI=this.w_SCDESCRI;
             ,SCPAGRBA=this.w_SCPAGRBA;
             ,SCPAGCAM=this.w_SCPAGCAM;
             ,SCPAGRID=this.w_SCPAGRID;
             ,SCPAGRDR=this.w_SCPAGRDR;
             ,SCPAGMAV=this.w_SCPAGMAV;
             ,SCPAGBON=this.w_SCPAGBON;
             ,SCCAUCON=this.w_SCCAUCON;
             ,SCCAUCOF=this.w_SCCAUCOF;
             ,SCCAUCOC=this.w_SCCAUCOC;
             ,SCCONTRO=this.w_SCCONTRO;
             ,SCCONDCA=this.w_SCCONDCA;
             ,SCCONDCP=this.w_SCCONDCP;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_SCSERRIF))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.SALDDACO_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.SALDDACO_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from SALDDACO
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and SCSERRIF="+cp_ToStrODBC(&i_TN.->SCSERRIF)+;
                            " and SCORDRIF="+cp_ToStrODBC(&i_TN.->SCORDRIF)+;
                            " and SCNUMRIF="+cp_ToStrODBC(&i_TN.->SCNUMRIF)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and SCSERRIF=&i_TN.->SCSERRIF;
                            and SCORDRIF=&i_TN.->SCORDRIF;
                            and SCNUMRIF=&i_TN.->SCNUMRIF;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = 0
              replace SCSERRIF with this.w_SCSERRIF
              replace SCORDRIF with this.w_SCORDRIF
              replace SCNUMRIF with this.w_SCNUMRIF
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update SALDDACO
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",SCRIFCOR="+cp_ToStrODBC(this.w_SCRIFCOR)+;
                     " ,SCSERRIF="+cp_ToStrODBC(this.w_SCSERRIF)+;
                     " ,SCORDRIF="+cp_ToStrODBC(this.w_SCORDRIF)+;
                     " ,SCNUMRIF="+cp_ToStrODBC(this.w_SCNUMRIF)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+;
                             " and SCSERRIF="+cp_ToStrODBC(SCSERRIF)+;
                             " and SCORDRIF="+cp_ToStrODBC(SCORDRIF)+;
                             " and SCNUMRIF="+cp_ToStrODBC(SCNUMRIF)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,SCRIFCOR=this.w_SCRIFCOR;
                     ,SCSERRIF=this.w_SCSERRIF;
                     ,SCORDRIF=this.w_SCORDRIF;
                     ,SCNUMRIF=this.w_SCNUMRIF;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere;
                                      and SCSERRIF=&i_TN.->SCSERRIF;
                                      and SCORDRIF=&i_TN.->SCORDRIF;
                                      and SCNUMRIF=&i_TN.->SCNUMRIF;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- gscg_msc
    this.OSERIAL=This.w_SCSERIAL
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_SCSERRIF))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.SALDDACO_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.SALDDACO_IDX,2])
        *
        * delete SALDDACO
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and SCSERRIF="+cp_ToStrODBC(&i_TN.->SCSERRIF)+;
                            " and SCORDRIF="+cp_ToStrODBC(&i_TN.->SCORDRIF)+;
                            " and SCNUMRIF="+cp_ToStrODBC(&i_TN.->SCNUMRIF)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and SCSERRIF=&i_TN.->SCSERRIF;
                              and SCORDRIF=&i_TN.->SCORDRIF;
                              and SCNUMRIF=&i_TN.->SCNUMRIF;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.SALMDACO_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.SALMDACO_IDX,2])
        *
        * delete SALMDACO
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_SCSERRIF))) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.SALMDACO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SALMDACO_IDX,2])
    if i_bUpd
      with this
        if .o_SCSERIAL<>.w_SCSERIAL
          .link_1_1('Full')
        endif
        .DoRTCalc(2,2,.t.)
          .w_CAUSALE = IIF(.w_SCTIPCON='C',.w_SCCAUCON,IIF(.w_SCTIPCON='F',.w_SCCAUCOF,.w_SCCAUCOC))
          .w_SCTIPGES = .pTipGes
        .DoRTCalc(5,10,.t.)
        if .o_SCDATREG<>.w_SCDATREG
          .w_SCDATDOC = .w_SCDATREG
        endif
        .DoRTCalc(12,14,.t.)
        if .o_SCTIPGES<>.w_SCTIPGES
          .w_SCTIPCON = IIF(.w_SCTIPGES='B','G','C')
        endif
        if .o_SCTIPCON<>.w_SCTIPCON
          .w_TIPCON = .w_SCTIPCON
        endif
        if .o_SCTIPCON<>.w_SCTIPCON
          .w_GTIPCON = 'G'
        endif
        if .o_SCCODVAL<>.w_SCCODVAL
          .w_SCCAOVAL = GETCAM(.w_SCCODVAL, IIF(EMPTY(.w_SCDATDOC), .w_SCDATREG, .w_SCDATDOC), 7)
        endif
        .DoRTCalc(19,22,.t.)
        if .o_SCCONINI<>.w_SCCONINI
          .w_SCCONFIN = .w_SCCONINI
          .link_1_23('Full')
        endif
        .DoRTCalc(24,24,.t.)
        if .o_SCTIPCON<>.w_SCTIPCON
          .w_SCBANAPP = iif(.w_SCTIPCON = 'G','  ',.w_SCBANAPP)
          .link_1_25('Full')
        endif
        if .o_SCTIPCON<>.w_SCTIPCON
          .w_SCBANNOS = iif(.w_SCTIPCON = 'G','  ',.w_SCBANNOS)
          .link_1_26('Full')
        endif
        if .o_SCCONINI<>.w_SCCONINI.or. .o_SCCONFIN<>.w_SCCONFIN
          .w_IMPSAL = IIF(.w_SCCONINI == .w_SCCONFIN AND !EMPTY(.w_SCCONINI+.w_SCCONFIN),.w_IMPSAL,0)
        endif
        .DoRTCalc(28,29,.t.)
        if .o_SCTIPCON<>.w_SCTIPCON.or. .o_RIFERIMC<>.w_RIFERIMC.or. .o_RIFERIMF<>.w_RIFERIMF
          .w_SCMODGEN = iif((.w_SCTIPCON+.w_RIFERIMC='CC') or (.w_SCTIPCON+.w_RIFERIMF='FF'),'I','G')
        endif
        .DoRTCalc(31,37,.t.)
          .w_PNIMPDAR = iif(.w_SCTIPCON#'C' and .w_IMPSAL#0,.w_IMPSAL,0)
          .w_PNIMPAVE = IIF(.w_SCTIPCON='C' AND .w_IMPSAL#0,.w_IMPSAL,0)
        .DoRTCalc(40,40,.t.)
          .w_OBTEST = i_DATSYS
        .DoRTCalc(42,42,.t.)
          .w_PNVALNAZ = g_PERVAL
          .link_1_52('Full')
        .DoRTCalc(44,45,.t.)
        if .o_SCDATDOC<>.w_SCDATDOC
          .w_PNDATDOC = .w_SCDATDOC
        endif
        if .o_SCDATREG<>.w_SCDATREG
          .w_PNDATREG = .w_SCDATREG
        endif
        if .o_SCCODVAL<>.w_SCCODVAL
          .w_PNCODVAL = .w_SCCODVAL
        endif
        if .o_SCCODVAL<>.w_SCCODVAL
          .w_PNCAOVAL = .w_SCCAOVAL
        endif
        if .o_SCTIPCON<>.w_SCTIPCON
          .w_PNTIPCON = .w_SCTIPCON
        endif
        if .o_SCTIPCON<>.w_SCTIPCON
          .w_PNCODCON = .w_SCCONINI
        endif
        .oPgFrm.Page1.oPag.oObj_1_64.Calculate()
        .DoRTCalc(52,52,.t.)
          .w_FASE = 0
        .oPgFrm.Page1.oPag.oObj_1_73.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_77.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_78.Calculate()
        .DoRTCalc(54,56,.t.)
          .w_FLAGAC = ' '
        .oPgFrm.Page1.oPag.oObj_1_81.Calculate(AH_Msgformat(IIF(.w_SCTIPGES='B', 'Codice conto SBF:','Codice conto iniziale:')))
        .oPgFrm.Page1.oPag.oObj_1_82.Calculate(AH_Msgformat(IIF(.w_SCTIPGES='B', 'Data valuta da:','Scadenze da:')))
        if .o_SCTIPCON<>.w_SCTIPCON
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        endif
        .DoRTCalc(58,58,.t.)
        if .o_SCTIPCON<>.w_SCTIPCON
          .w_CAUUSED = icase(.w_SCTIPCON='C',.w_SCCAUCON,.w_SCTIPCON='F',.w_SCCAUCOF,.w_SCCAUCOC)
        endif
        .DoRTCalc(60,64,.t.)
          .link_2_3('Full')
        .DoRTCalc(66,69,.t.)
          .link_2_8('Full')
        .oPgFrm.Page1.oPag.oObj_2_12.Calculate(IIF(empty(.w_NUMPAR) AND .w_SCORDRIF<>0, AH_Msgformat('<Riga eliminata in primanota>'),''))
        .oPgFrm.Page1.oPag.oObj_3_1.Calculate(iif(.w_SCTIPCON='C','Rag. soc. cliente:',iif(.w_SCTIPCON='F','Rag. soc. fornitore:','Desc. conto:')))
        .DoRTCalc(71,87,.t.)
        if .o_CAUCOC<>.w_CAUCOC.or. .o_CAUSBF<>.w_CAUSBF
          .w_SCCAUCOC = IIF(.w_SCTIPGES='S',.w_CAUCOC,.w_CAUSBF)
          .link_4_9('Full')
        endif
        .oPgFrm.Page2.oPag.oObj_4_28.Calculate(AH_Msgformat(IIF(.w_SCTIPGES='B', 'Causale storno SBF:','Causale cont. conti:')))
        .DoRTCalc(89,103,.t.)
          .w_PNPRDC = IIF(.w_TIPREGC='V' OR (.w_TIPREGC='C' AND .w_TIPDOCC='FC'), 'FV', 'NN')
        .DoRTCalc(105,110,.t.)
          .w_PNPRDF = IIF(.w_TIPREGF='V' OR (.w_TIPREGF='C' AND .w_TIPDOCF='FC'), 'FV', 'NN')
          .w_PNPRDG = IIF(.w_TIPREGG='V' OR (.w_TIPREGG='C' AND .w_TIPDOCG='FC'), 'FV', 'NN')
        .DoRTCalc(113,118,.t.)
          .w_PNPRD = IIF(.w_SCTIPCON='C',.w_PNPRDC,IIF(.w_SCTIPCON='F',.w_PNPRDF,.w_PNPRDG))
          .w_SERDOC = IIF(.w_SCTIPCON='C',.w_SERDOCC,IIF(.w_SCTIPCON='F',.w_SERDOCF,.w_SERDOCG))
        if .o_SCTIPCON<>.w_SCTIPCON
          .Calculate_VWFNDSBKPY()
        endif
          .w_FLPDOC = IIF(.w_SCTIPCON='C',.w_FLPDOCC,IIF(.w_SCTIPCON='F',.w_FLPDOCF,.w_FLPDOCG))
        if .o_FLPDOC<>.w_FLPDOC
          .Calculate_WXPGHQKZND()
        endif
        if .o_SCTIPCON<>.w_SCTIPCON
          .Calculate_ZLKIECMTYS()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"SESALDAC","i_codazi,w_SCSERIAL")
          .op_SCSERIAL = .w_SCSERIAL
        endif
        if .op_codazi<>.w_codazi .or. .op_SCCODESE<>.w_SCCODESE
           cp_AskTableProg(this,i_nConn,"PRSALDAC","i_codazi,w_SCCODESE,w_SCNUMREG")
          .op_SCNUMREG = .w_SCNUMREG
        endif
        .op_codazi = .w_codazi
        .op_codazi = .w_codazi
        .op_SCCODESE = .w_SCCODESE
      endwith
      this.DoRTCalc(122,122,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_SCSERRIF with this.w_SCSERRIF
      replace t_SCORDRIF with this.w_SCORDRIF
      replace t_SCNUMRIF with this.w_SCNUMRIF
      replace t_PTTIPCON with this.w_PTTIPCON
      replace t_SCRIFCOR with this.w_SCRIFCOR
      replace t_PSERIAL with this.w_PSERIAL
      replace t_PROWORD with this.w_PROWORD
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_64.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_73.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_77.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_78.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_81.Calculate(AH_Msgformat(IIF(.w_SCTIPGES='B', 'Codice conto SBF:','Codice conto iniziale:')))
        .oPgFrm.Page1.oPag.oObj_1_82.Calculate(AH_Msgformat(IIF(.w_SCTIPGES='B', 'Data valuta da:','Scadenze da:')))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_12.Calculate(IIF(empty(.w_NUMPAR) AND .w_SCORDRIF<>0, AH_Msgformat('<Riga eliminata in primanota>'),''))
        .oPgFrm.Page1.oPag.oObj_3_1.Calculate(iif(.w_SCTIPCON='C','Rag. soc. cliente:',iif(.w_SCTIPCON='F','Rag. soc. fornitore:','Desc. conto:')))
        .oPgFrm.Page2.oPag.oObj_4_28.Calculate(AH_Msgformat(IIF(.w_SCTIPGES='B', 'Causale storno SBF:','Causale cont. conti:')))
    endwith
  return

  proc mCalcRowObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_2_12.Calculate(IIF(empty(.w_NUMPAR) AND .w_SCORDRIF<>0, AH_Msgformat('<Riga eliminata in primanota>'),''))
    endwith
  return
  proc Calculate_VWFNDSBKPY()
    with this
          * --- Valorizzazione serie documento  pag.1
          .w_SCALFDOC = .w_SERDOC
    endwith
  endproc
  proc Calculate_WXPGHQKZND()
    with this
          * --- Gestione numero documento
          .w_OLDNUMDOC = IIF(!EMPTY(.w_SCNUMDOC),.w_SCNUMDOC,.w_OLDNUMDOC)
          .w_SCNUMDOC = iif(.w_FLPDOC='N',0,.w_OLDNUMDOC)
    endwith
  endproc
  proc Calculate_ZLKIECMTYS()
    with this
          * --- Calcolo del numero documento in base alle scelte fatte
          calcnumdoc(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oSCNUMDOC_1_9.enabled = this.oPgFrm.Page1.oPag.oSCNUMDOC_1_9.mCond()
    this.oPgFrm.Page1.oPag.oSCALFDOC_1_10.enabled = this.oPgFrm.Page1.oPag.oSCALFDOC_1_10.mCond()
    this.oPgFrm.Page1.oPag.oSCCAOVAL_1_18.enabled = this.oPgFrm.Page1.oPag.oSCCAOVAL_1_18.mCond()
    this.oPgFrm.Page1.oPag.oSCBANAPP_1_25.enabled = this.oPgFrm.Page1.oPag.oSCBANAPP_1_25.mCond()
    this.oPgFrm.Page1.oPag.oSCBANNOS_1_26.enabled = this.oPgFrm.Page1.oPag.oSCBANNOS_1_26.mCond()
    this.oPgFrm.Page1.oPag.oIMPSAL_1_27.enabled = this.oPgFrm.Page1.oPag.oIMPSAL_1_27.mCond()
    this.oPgFrm.Page1.oPag.oIMPLIMITE_1_28.enabled = this.oPgFrm.Page1.oPag.oIMPLIMITE_1_28.mCond()
    this.oPgFrm.Page1.oPag.oCODPAG_1_29.enabled = this.oPgFrm.Page1.oPag.oCODPAG_1_29.mCond()
    this.oPgFrm.Page1.oPag.oEXECSTAM_1_32.enabled = this.oPgFrm.Page1.oPag.oEXECSTAM_1_32.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_34.enabled = this.oPgFrm.Page1.oPag.oBtn_1_34.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_40.enabled = this.oPgFrm.Page1.oPag.oBtn_1_40.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oSCTIPCON_1_15.visible=!this.oPgFrm.Page1.oPag.oSCTIPCON_1_15.mHide()
    this.oPgFrm.Page1.oPag.oSCCAOVAL_1_18.visible=!this.oPgFrm.Page1.oPag.oSCCAOVAL_1_18.mHide()
    this.oPgFrm.Page1.oPag.oSCCONFIN_1_23.visible=!this.oPgFrm.Page1.oPag.oSCCONFIN_1_23.mHide()
    this.oPgFrm.Page1.oPag.oIMPLIMITE_1_28.visible=!this.oPgFrm.Page1.oPag.oIMPLIMITE_1_28.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_33.visible=!this.oPgFrm.Page1.oPag.oBtn_1_33.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_34.visible=!this.oPgFrm.Page1.oPag.oBtn_1_34.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_35.visible=!this.oPgFrm.Page1.oPag.oBtn_1_35.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_37.visible=!this.oPgFrm.Page1.oPag.oBtn_1_37.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_38.visible=!this.oPgFrm.Page1.oPag.oBtn_1_38.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_40.visible=!this.oPgFrm.Page1.oPag.oBtn_1_40.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_41.visible=!this.oPgFrm.Page1.oPag.oStr_1_41.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_42.visible=!this.oPgFrm.Page1.oPag.oStr_1_42.mHide()
    this.oPgFrm.Page1.oPag.oDESFIN_1_44.visible=!this.oPgFrm.Page1.oPag.oDESFIN_1_44.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_63.visible=!this.oPgFrm.Page1.oPag.oStr_1_63.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_94.visible=!this.oPgFrm.Page1.oPag.oStr_1_94.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_95.visible=!this.oPgFrm.Page1.oPag.oStr_1_95.mHide()
    this.oPgFrm.Page2.oPag.oSCPAGRBA_4_1.visible=!this.oPgFrm.Page2.oPag.oSCPAGRBA_4_1.mHide()
    this.oPgFrm.Page2.oPag.oSCPAGCAM_4_2.visible=!this.oPgFrm.Page2.oPag.oSCPAGCAM_4_2.mHide()
    this.oPgFrm.Page2.oPag.oSCPAGRID_4_3.visible=!this.oPgFrm.Page2.oPag.oSCPAGRID_4_3.mHide()
    this.oPgFrm.Page2.oPag.oSCPAGRDR_4_4.visible=!this.oPgFrm.Page2.oPag.oSCPAGRDR_4_4.mHide()
    this.oPgFrm.Page2.oPag.oSCPAGMAV_4_5.visible=!this.oPgFrm.Page2.oPag.oSCPAGMAV_4_5.mHide()
    this.oPgFrm.Page2.oPag.oSCPAGBON_4_6.visible=!this.oPgFrm.Page2.oPag.oSCPAGBON_4_6.mHide()
    this.oPgFrm.Page2.oPag.oSCCAUCON_4_7.visible=!this.oPgFrm.Page2.oPag.oSCCAUCON_4_7.mHide()
    this.oPgFrm.Page2.oPag.oSCCAUCOF_4_8.visible=!this.oPgFrm.Page2.oPag.oSCCAUCOF_4_8.mHide()
    this.oPgFrm.Page2.oPag.oSCCONTRO_4_12.visible=!this.oPgFrm.Page2.oPag.oSCCONTRO_4_12.mHide()
    this.oPgFrm.Page2.oPag.oSCCONDCA_4_13.visible=!this.oPgFrm.Page2.oPag.oSCCONDCA_4_13.mHide()
    this.oPgFrm.Page2.oPag.oDESCAU_4_14.visible=!this.oPgFrm.Page2.oPag.oDESCAU_4_14.mHide()
    this.oPgFrm.Page2.oPag.oSCCONDCP_4_15.visible=!this.oPgFrm.Page2.oPag.oSCCONDCP_4_15.mHide()
    this.oPgFrm.Page2.oPag.oStr_4_17.visible=!this.oPgFrm.Page2.oPag.oStr_4_17.mHide()
    this.oPgFrm.Page2.oPag.oStr_4_18.visible=!this.oPgFrm.Page2.oPag.oStr_4_18.mHide()
    this.oPgFrm.Page2.oPag.oDESDCA_4_19.visible=!this.oPgFrm.Page2.oPag.oDESDCA_4_19.mHide()
    this.oPgFrm.Page2.oPag.oStr_4_21.visible=!this.oPgFrm.Page2.oPag.oStr_4_21.mHide()
    this.oPgFrm.Page2.oPag.oDESDCP_4_22.visible=!this.oPgFrm.Page2.oPag.oDESDCP_4_22.mHide()
    this.oPgFrm.Page2.oPag.oDESCON_4_23.visible=!this.oPgFrm.Page2.oPag.oDESCON_4_23.mHide()
    this.oPgFrm.Page2.oPag.oStr_4_24.visible=!this.oPgFrm.Page2.oPag.oStr_4_24.mHide()
    this.oPgFrm.Page2.oPag.oDESCAUF_4_25.visible=!this.oPgFrm.Page2.oPag.oDESCAUF_4_25.mHide()
    this.oPgFrm.Page2.oPag.oStr_4_26.visible=!this.oPgFrm.Page2.oPag.oStr_4_26.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_99.visible=!this.oPgFrm.Page1.oPag.oStr_1_99.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_64.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_73.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_77.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_78.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_81.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_82.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_12.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_3_1.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_4_28.Event(cEvent)
        if lower(cEvent)==lower("Blank")
          .Calculate_VWFNDSBKPY()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank")
          .Calculate_WXPGHQKZND()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("CFGLoaded")
          .Calculate_ZLKIECMTYS()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gscg_msc
    IF cEvent == 'New record'
       this.o_RIFERIMC='X'
       this.mCalc(.t.)
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=READAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_lTable = "CONTROPA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2], .t., this.CONTROPA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COCODAZI,COSADIFC,COSADIFN,COCAUSAL,COCONSAL,COSAABBA,COSAABBP,COCAUSAF,COCAUSAC,COCAUSBF,COPAGRIB,COPAGRIM,COPAGCAM,COPAGMAV,COPAGRID,COPAGBON";
                   +" from "+i_cTable+" "+i_lTable+" where COCODAZI="+cp_ToStrODBC(this.w_READAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COCODAZI',this.w_READAZI)
            select COCODAZI,COSADIFC,COSADIFN,COCAUSAL,COCONSAL,COSAABBA,COSAABBP,COCAUSAF,COCAUSAC,COCAUSBF,COPAGRIB,COPAGRIM,COPAGCAM,COPAGMAV,COPAGRID,COPAGBON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READAZI = NVL(_Link_.COCODAZI,space(5))
      this.w_SCCONDCA = NVL(_Link_.COSADIFC,space(15))
      this.w_SCCONDCP = NVL(_Link_.COSADIFN,space(15))
      this.w_SCCAUCON = NVL(_Link_.COCAUSAL,space(5))
      this.w_SCCONTRO = NVL(_Link_.COCONSAL,space(15))
      this.w_SCCAUCON = NVL(_Link_.COCAUSAL,space(5))
      this.w_CONABBA = NVL(_Link_.COSAABBA,space(15))
      this.w_CONABBP = NVL(_Link_.COSAABBP,space(15))
      this.w_SCCAUCOF = NVL(_Link_.COCAUSAF,space(5))
      this.w_CAUCOC = NVL(_Link_.COCAUSAC,space(5))
      this.w_CAUSBF = NVL(_Link_.COCAUSBF,space(5))
      this.w_SCPAGRBA = NVL(_Link_.COPAGRIB,space(2))
      this.w_SCPAGRDR = NVL(_Link_.COPAGRIM,space(2))
      this.w_SCPAGCAM = NVL(_Link_.COPAGCAM,space(2))
      this.w_SCPAGMAV = NVL(_Link_.COPAGMAV,space(2))
      this.w_SCPAGRID = NVL(_Link_.COPAGRID,space(2))
      this.w_SCPAGBON = NVL(_Link_.COPAGBON,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_READAZI = space(5)
      endif
      this.w_SCCONDCA = space(15)
      this.w_SCCONDCP = space(15)
      this.w_SCCAUCON = space(5)
      this.w_SCCONTRO = space(15)
      this.w_SCCAUCON = space(5)
      this.w_CONABBA = space(15)
      this.w_CONABBP = space(15)
      this.w_SCCAUCOF = space(5)
      this.w_CAUCOC = space(5)
      this.w_CAUSBF = space(5)
      this.w_SCPAGRBA = space(2)
      this.w_SCPAGRDR = space(2)
      this.w_SCPAGCAM = space(2)
      this.w_SCPAGMAV = space(2)
      this.w_SCPAGRID = space(2)
      this.w_SCPAGBON = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])+'\'+cp_ToStr(_Link_.COCODAZI,1)
      cp_ShowWarn(i_cKey,this.CONTROPA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SCCODESE
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCCODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_SCCODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_READAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_READAZI;
                     ,'ESCODESE',trim(this.w_SCCODESE))
          select ESCODAZI,ESCODESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SCCODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SCCODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oSCCODESE_1_12'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_READAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_READAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCCODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_SCCODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_READAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_READAZI;
                       ,'ESCODESE',this.w_SCCODESE)
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCCODESE = NVL(_Link_.ESCODESE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_SCCODESE = space(4)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCCODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SCCODVAL
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_SCCODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VACAOVAL,VADECTOT,VASIMVAL,VADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_SCCODVAL))
          select VACODVAL,VADESVAL,VACAOVAL,VADECTOT,VASIMVAL,VADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SCCODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStrODBC(trim(this.w_SCCODVAL)+"%");

            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VACAOVAL,VADECTOT,VASIMVAL,VADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStr(trim(this.w_SCCODVAL)+"%");

            select VACODVAL,VADESVAL,VACAOVAL,VADECTOT,VASIMVAL,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SCCODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oSCCODVAL_1_19'),i_cWhere,'GSAR_AVL',"Elenco valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VACAOVAL,VADECTOT,VASIMVAL,VADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL,VACAOVAL,VADECTOT,VASIMVAL,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VACAOVAL,VADECTOT,VASIMVAL,VADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_SCCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_SCCODVAL)
            select VACODVAL,VADESVAL,VACAOVAL,VADECTOT,VASIMVAL,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCCODVAL = NVL(_Link_.VACODVAL,space(5))
      this.w_DESVAL = NVL(_Link_.VADESVAL,space(35))
      this.w_CAOVAL = NVL(_Link_.VACAOVAL,0)
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(3))
      this.w_DTVOBSO = NVL(cp_ToDate(_Link_.VADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_SCCODVAL = space(5)
      endif
      this.w_DESVAL = space(35)
      this.w_CAOVAL = 0
      this.w_DECTOT = 0
      this.w_SIMVAL = space(3)
      this.w_DTVOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_19(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+6<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_19.VACODVAL as VACODVAL119"+ ",link_1_19.VADESVAL as VADESVAL119"+ ",link_1_19.VACAOVAL as VACAOVAL119"+ ",link_1_19.VADECTOT as VADECTOT119"+ ",link_1_19.VASIMVAL as VASIMVAL119"+ ",link_1_19.VADTOBSO as VADTOBSO119"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_19 on SALMDACO.SCCODVAL=link_1_19.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_19"
          i_cKey=i_cKey+'+" and SALMDACO.SCCODVAL=link_1_19.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=SCCONINI
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCCONINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_SCCONINI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_SCTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_SCTIPCON;
                     ,'ANCODICE',trim(this.w_SCCONINI))
          select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SCCONINI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_SCCONINI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_SCTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_SCCONINI)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_SCTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SCCONINI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oSCCONINI_1_22'),i_cWhere,'GSAR_BZC',"Clienti\fornitori\conti",'GSCG_ACB.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_SCTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale, non gestito a partite oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_SCTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCCONINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_SCCONINI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_SCTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_SCTIPCON;
                       ,'ANCODICE',this.w_SCCONINI)
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCCONINI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESINI = NVL(_Link_.ANDESCRI,space(40))
      this.w_PARTSN = NVL(_Link_.ANPARTSN,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_SCCONINI = space(15)
      endif
      this.w_DESINI = space(40)
      this.w_PARTSN = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_SCCONFIN)) OR  (.w_SCCONINI<=.w_SCCONFIN)) AND (EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO) And .w_PARTSN='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale, non gestito a partite oppure obsoleto")
        endif
        this.w_SCCONINI = space(15)
        this.w_DESINI = space(40)
        this.w_PARTSN = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCCONINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_22(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_22.ANCODICE as ANCODICE122"+ ",link_1_22.ANDESCRI as ANDESCRI122"+ ",link_1_22.ANPARTSN as ANPARTSN122"+ ",link_1_22.ANDTOBSO as ANDTOBSO122"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_22 on SALMDACO.SCCONINI=link_1_22.ANCODICE"+" and SALMDACO.SCTIPCON=link_1_22.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_22"
          i_cKey=i_cKey+'+" and SALMDACO.SCCONINI=link_1_22.ANCODICE(+)"'+'+" and SALMDACO.SCTIPCON=link_1_22.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=SCCONFIN
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCCONFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_SCCONFIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_SCTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_SCTIPCON;
                     ,'ANCODICE',trim(this.w_SCCONFIN))
          select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SCCONFIN)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_SCCONFIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_SCTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_SCCONFIN)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_SCTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SCCONFIN) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oSCCONFIN_1_23'),i_cWhere,'GSAR_BZC',"Clienti\fornitori\conti",'GSCG_ACB.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_SCTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale, non gestito a partite oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_SCTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCCONFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_SCCONFIN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_SCTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_SCTIPCON;
                       ,'ANCODICE',this.w_SCCONFIN)
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCCONFIN = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFIN = NVL(_Link_.ANDESCRI,space(40))
      this.w_PARTSN = NVL(_Link_.ANPARTSN,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_SCCONFIN = space(15)
      endif
      this.w_DESFIN = space(40)
      this.w_PARTSN = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_SCCONFIN>=.w_SCCONINI) or (empty(.w_SCCONINI))) AND (EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO) And .w_PARTSN='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale, non gestito a partite oppure obsoleto")
        endif
        this.w_SCCONFIN = space(15)
        this.w_DESFIN = space(40)
        this.w_PARTSN = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCCONFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_23(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_23.ANCODICE as ANCODICE123"+ ",link_1_23.ANDESCRI as ANDESCRI123"+ ",link_1_23.ANPARTSN as ANPARTSN123"+ ",link_1_23.ANDTOBSO as ANDTOBSO123"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_23 on SALMDACO.SCCONFIN=link_1_23.ANCODICE"+" and SALMDACO.SCTIPCON=link_1_23.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_23"
          i_cKey=i_cKey+'+" and SALMDACO.SCCONFIN=link_1_23.ANCODICE(+)"'+'+" and SALMDACO.SCTIPCON=link_1_23.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=SCBANAPP
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BAN_CHE_IDX,3]
    i_lTable = "BAN_CHE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2], .t., this.BAN_CHE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCBANAPP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ABA',True,'BAN_CHE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_SCBANAPP)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESBAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_SCBANAPP))
          select BACODBAN,BADESBAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SCBANAPP)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SCBANAPP) and !this.bDontReportError
            deferred_cp_zoom('BAN_CHE','*','BACODBAN',cp_AbsName(oSource.parent,'oSCBANAPP_1_25'),i_cWhere,'GSAR_ABA',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESBAN";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCBANAPP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESBAN";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_SCBANAPP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_SCBANAPP)
            select BACODBAN,BADESBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCBANAPP = NVL(_Link_.BACODBAN,space(10))
      this.w_DESBAN = NVL(_Link_.BADESBAN,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_SCBANAPP = space(10)
      endif
      this.w_DESBAN = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.BAN_CHE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCBANAPP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_25(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.BAN_CHE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_25.BACODBAN as BACODBAN125"+ ",link_1_25.BADESBAN as BADESBAN125"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_25 on SALMDACO.SCBANAPP=link_1_25.BACODBAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_25"
          i_cKey=i_cKey+'+" and SALMDACO.SCBANAPP=link_1_25.BACODBAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=SCBANNOS
  func Link_1_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCBANNOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_SCBANNOS)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BACONSBF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_SCBANNOS))
          select BACODBAN,BADESCRI,BACONSBF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SCBANNOS)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SCBANNOS) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oSCBANNOS_1_26'),i_cWhere,'GSTE_ACB',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BACONSBF";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESCRI,BACONSBF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCBANNOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BACONSBF";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_SCBANNOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_SCBANNOS)
            select BACODBAN,BADESCRI,BACONSBF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCBANNOS = NVL(_Link_.BACODBAN,space(15))
      this.w_DESNOSBA = NVL(_Link_.BADESCRI,space(35))
      this.w_CONSBF = NVL(_Link_.BACONSBF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_SCBANNOS = space(15)
      endif
      this.w_DESNOSBA = space(35)
      this.w_CONSBF = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CONSBF='C'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice di tipo salvo buon fine")
        endif
        this.w_SCBANNOS = space(15)
        this.w_DESNOSBA = space(35)
        this.w_CONSBF = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCBANNOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_26(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COC_MAST_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_26.BACODBAN as BACODBAN126"+ ",link_1_26.BADESCRI as BADESCRI126"+ ",link_1_26.BACONSBF as BACONSBF126"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_26 on SALMDACO.SCBANNOS=link_1_26.BACODBAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_26"
          i_cKey=i_cKey+'+" and SALMDACO.SCBANNOS=link_1_26.BACODBAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CODPAG
  func Link_1_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_2AME_IDX,3]
    i_lTable = "PAG_2AME"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_2AME_IDX,2], .t., this.PAG_2AME_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_2AME_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MPA',True,'PAG_2AME')
        if i_nConn<>0
          i_cWhere = i_cFlt+" P2CODICE like "+cp_ToStrODBC(trim(this.w_CODPAG)+"%");

          i_ret=cp_SQL(i_nConn,"select P2CODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by P2CODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'P2CODICE',trim(this.w_CODPAG))
          select P2CODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by P2CODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODPAG)==trim(_Link_.P2CODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODPAG) and !this.bDontReportError
            deferred_cp_zoom('PAG_2AME','*','P2CODICE',cp_AbsName(oSource.parent,'oCODPAG_1_29'),i_cWhere,'GSAR_MPA',"Codici pagamento",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select P2CODICE";
                     +" from "+i_cTable+" "+i_lTable+" where P2CODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'P2CODICE',oSource.xKey(1))
            select P2CODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select P2CODICE";
                   +" from "+i_cTable+" "+i_lTable+" where P2CODICE="+cp_ToStrODBC(this.w_CODPAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'P2CODICE',this.w_CODPAG)
            select P2CODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPAG = NVL(_Link_.P2CODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CODPAG = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_2AME_IDX,2])+'\'+cp_ToStr(_Link_.P2CODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_2AME_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PNVALNAZ
  func Link_1_52(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PNVALNAZ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PNVALNAZ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT,VACAOVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_PNVALNAZ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_PNVALNAZ)
            select VACODVAL,VADECTOT,VACAOVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PNVALNAZ = NVL(_Link_.VACODVAL,space(5))
      this.w_DECTOP = NVL(_Link_.VADECTOT,0)
      this.w_CAONAZ = NVL(_Link_.VACAOVAL,0)
    else
      if i_cCtrl<>'Load'
        this.w_PNVALNAZ = space(5)
      endif
      this.w_DECTOP = 0
      this.w_CAONAZ = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PNVALNAZ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SCNUMRIF
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_TITE_IDX,3]
    i_lTable = "PAR_TITE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2], .t., this.PAR_TITE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCNUMRIF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCNUMRIF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PTSERIAL,PTROWORD,CPROWNUM,PTNUMPAR,PTDATSCA,PTNUMDOC,PTALFDOC,PTTOTIMP,PTCODCON,PTTIPCON";
                   +" from "+i_cTable+" "+i_lTable+" where CPROWNUM="+cp_ToStrODBC(this.w_SCNUMRIF);
                   +" and PTSERIAL="+cp_ToStrODBC(this.w_SCSERRIF);
                   +" and PTROWORD="+cp_ToStrODBC(this.w_SCORDRIF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PTSERIAL',this.w_SCSERRIF;
                       ,'PTROWORD',this.w_SCORDRIF;
                       ,'CPROWNUM',this.w_SCNUMRIF)
            select PTSERIAL,PTROWORD,CPROWNUM,PTNUMPAR,PTDATSCA,PTNUMDOC,PTALFDOC,PTTOTIMP,PTCODCON,PTTIPCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCNUMRIF = NVL(_Link_.CPROWNUM,0)
      this.w_NUMPAR = NVL(_Link_.PTNUMPAR,space(31))
      this.w_DATSCA = NVL(cp_ToDate(_Link_.PTDATSCA),ctod("  /  /  "))
      this.w_NUMDOC = NVL(_Link_.PTNUMDOC,0)
      this.w_ALFDOC = NVL(_Link_.PTALFDOC,space(10))
      this.w_TOTIMP = NVL(_Link_.PTTOTIMP,0)
      this.w_CODCON = NVL(_Link_.PTCODCON,space(15))
      this.w_PROWORD = NVL(_Link_.PTROWORD,0)
      this.w_PSERIAL = NVL(_Link_.PTSERIAL,space(10))
      this.w_PTTIPCON = NVL(_Link_.PTTIPCON,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_SCNUMRIF = 0
      endif
      this.w_NUMPAR = space(31)
      this.w_DATSCA = ctod("  /  /  ")
      this.w_NUMDOC = 0
      this.w_ALFDOC = space(10)
      this.w_TOTIMP = 0
      this.w_CODCON = space(15)
      this.w_PROWORD = 0
      this.w_PSERIAL = space(10)
      this.w_PTTIPCON = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2])+'\'+cp_ToStr(_Link_.PTSERIAL,1)+'\'+cp_ToStr(_Link_.PTROWORD,1)+'\'+cp_ToStr(_Link_.CPROWNUM,1)
      cp_ShowWarn(i_cKey,this.PAR_TITE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCNUMRIF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 10 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PAR_TITE_IDX,3] and i_nFlds+10<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.CPROWNUM as CPROWNUM203"+ ",link_2_3.PTNUMPAR as PTNUMPAR203"+ ",link_2_3.PTDATSCA as PTDATSCA203"+ ",link_2_3.PTNUMDOC as PTNUMDOC203"+ ",link_2_3.PTALFDOC as PTALFDOC203"+ ",link_2_3.PTTOTIMP as PTTOTIMP203"+ ",link_2_3.PTCODCON as PTCODCON203"+ ",link_2_3.PTROWORD as PTROWORD203"+ ",link_2_3.PTSERIAL as PTSERIAL203"+ ",link_2_3.PTTIPCON as PTTIPCON203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on SALDDACO.SCNUMRIF=link_2_3.CPROWNUM"+" and SALDDACO.SCSERRIF=link_2_3.PTSERIAL"+" and SALDDACO.SCORDRIF=link_2_3.PTROWORD"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+10
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and SALDDACO.SCNUMRIF=link_2_3.CPROWNUM(+)"'+'+" and SALDDACO.SCSERRIF=link_2_3.PTSERIAL(+)"'+'+" and SALDDACO.SCORDRIF=link_2_3.PTROWORD(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+10
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CODCON
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PTTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_PTTIPCON;
                       ,'ANCODICE',this.w_CODCON)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_RAGSOC = NVL(_Link_.ANDESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON = space(15)
      endif
      this.w_RAGSOC = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SCCAUCON
  func Link_4_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCCAUCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_SCCAUCON)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO,CCFLRIFE,CCTIPDOC,CCFLPDOC,CCSERDOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_SCCAUCON))
          select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO,CCFLRIFE,CCTIPDOC,CCFLPDOC,CCSERDOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SCCAUCON)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SCCAUCON) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oSCCAUCON_4_7'),i_cWhere,'GSCG_ACC',"Causali contabili",'CAUCOEFA.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO,CCFLRIFE,CCTIPDOC,CCFLPDOC,CCSERDOC";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO,CCFLRIFE,CCTIPDOC,CCFLPDOC,CCSERDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCCAUCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO,CCFLRIFE,CCTIPDOC,CCFLPDOC,CCSERDOC";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_SCCAUCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_SCCAUCON)
            select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO,CCFLRIFE,CCTIPDOC,CCFLPDOC,CCSERDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCCAUCON = NVL(_Link_.CCCODICE,space(5))
      this.w_DESCAU = NVL(_Link_.CCDESCRI,space(35))
      this.w_TIPREG = NVL(_Link_.CCTIPREG,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
      this.w_RIFERIMC = NVL(_Link_.CCFLRIFE,space(1))
      this.w_TIPDOCC = NVL(_Link_.CCTIPDOC,space(2))
      this.w_TIPREGC = NVL(_Link_.CCTIPREG,space(1))
      this.w_FLPDOCC = NVL(_Link_.CCFLPDOC,space(1))
      this.w_SERDOCC = NVL(_Link_.CCSERDOC,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_SCCAUCON = space(5)
      endif
      this.w_DESCAU = space(35)
      this.w_TIPREG = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_RIFERIMC = space(1)
      this.w_TIPDOCC = space(2)
      this.w_TIPREGC = space(1)
      this.w_FLPDOCC = space(1)
      this.w_SERDOCC = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_TIPREG='N'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale contabile inesistente, obsoleta o di tipo IVA")
        endif
        this.w_SCCAUCON = space(5)
        this.w_DESCAU = space(35)
        this.w_TIPREG = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_RIFERIMC = space(1)
        this.w_TIPDOCC = space(2)
        this.w_TIPREGC = space(1)
        this.w_FLPDOCC = space(1)
        this.w_SERDOCC = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCCAUCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 9 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+9<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_7.CCCODICE as CCCODICE407"+ ",link_4_7.CCDESCRI as CCDESCRI407"+ ",link_4_7.CCTIPREG as CCTIPREG407"+ ",link_4_7.CCDTOBSO as CCDTOBSO407"+ ",link_4_7.CCFLRIFE as CCFLRIFE407"+ ",link_4_7.CCTIPDOC as CCTIPDOC407"+ ",link_4_7.CCTIPREG as CCTIPREG407"+ ",link_4_7.CCFLPDOC as CCFLPDOC407"+ ",link_4_7.CCSERDOC as CCSERDOC407"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_7 on SALMDACO.SCCAUCON=link_4_7.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+9
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_7"
          i_cKey=i_cKey+'+" and SALMDACO.SCCAUCON=link_4_7.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+9
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=SCCAUCOF
  func Link_4_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCCAUCOF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_SCCAUCOF)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO,CCFLRIFE,CCTIPDOC,CCFLPDOC,CCSERDOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_SCCAUCOF))
          select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO,CCFLRIFE,CCTIPDOC,CCFLPDOC,CCSERDOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SCCAUCOF)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SCCAUCOF) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oSCCAUCOF_4_8'),i_cWhere,'GSCG_ACC',"Causali contabili",'CAUCOEFA.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO,CCFLRIFE,CCTIPDOC,CCFLPDOC,CCSERDOC";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO,CCFLRIFE,CCTIPDOC,CCFLPDOC,CCSERDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCCAUCOF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO,CCFLRIFE,CCTIPDOC,CCFLPDOC,CCSERDOC";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_SCCAUCOF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_SCCAUCOF)
            select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO,CCFLRIFE,CCTIPDOC,CCFLPDOC,CCSERDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCCAUCOF = NVL(_Link_.CCCODICE,space(5))
      this.w_DESCAUF = NVL(_Link_.CCDESCRI,space(35))
      this.w_TIPREG = NVL(_Link_.CCTIPREG,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
      this.w_RIFERIMF = NVL(_Link_.CCFLRIFE,space(1))
      this.w_TIPDOCF = NVL(_Link_.CCTIPDOC,space(2))
      this.w_TIPREGF = NVL(_Link_.CCTIPREG,space(1))
      this.w_FLPDOCF = NVL(_Link_.CCFLPDOC,space(1))
      this.w_SERDOCF = NVL(_Link_.CCSERDOC,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_SCCAUCOF = space(5)
      endif
      this.w_DESCAUF = space(35)
      this.w_TIPREG = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_RIFERIMF = space(1)
      this.w_TIPDOCF = space(2)
      this.w_TIPREGF = space(1)
      this.w_FLPDOCF = space(1)
      this.w_SERDOCF = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_TIPREG='N'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale contabile inesistente, obsoleta o di tipo IVA")
        endif
        this.w_SCCAUCOF = space(5)
        this.w_DESCAUF = space(35)
        this.w_TIPREG = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_RIFERIMF = space(1)
        this.w_TIPDOCF = space(2)
        this.w_TIPREGF = space(1)
        this.w_FLPDOCF = space(1)
        this.w_SERDOCF = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCCAUCOF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_8(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 9 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+9<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_8.CCCODICE as CCCODICE408"+ ",link_4_8.CCDESCRI as CCDESCRI408"+ ",link_4_8.CCTIPREG as CCTIPREG408"+ ",link_4_8.CCDTOBSO as CCDTOBSO408"+ ",link_4_8.CCFLRIFE as CCFLRIFE408"+ ",link_4_8.CCTIPDOC as CCTIPDOC408"+ ",link_4_8.CCTIPREG as CCTIPREG408"+ ",link_4_8.CCFLPDOC as CCFLPDOC408"+ ",link_4_8.CCSERDOC as CCSERDOC408"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_8 on SALMDACO.SCCAUCOF=link_4_8.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+9
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_8"
          i_cKey=i_cKey+'+" and SALMDACO.SCCAUCOF=link_4_8.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+9
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=SCCAUCOC
  func Link_4_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCCAUCOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_SCCAUCOC)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO,CCTIPDOC,CCFLPDOC,CCSERDOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_SCCAUCOC))
          select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO,CCTIPDOC,CCFLPDOC,CCSERDOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SCCAUCOC)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SCCAUCOC) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oSCCAUCOC_4_9'),i_cWhere,'GSCG_ACC',"Causali contabili",'CAUCOEFA.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO,CCTIPDOC,CCFLPDOC,CCSERDOC";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO,CCTIPDOC,CCFLPDOC,CCSERDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCCAUCOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO,CCTIPDOC,CCFLPDOC,CCSERDOC";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_SCCAUCOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_SCCAUCOC)
            select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO,CCTIPDOC,CCFLPDOC,CCSERDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCCAUCOC = NVL(_Link_.CCCODICE,space(5))
      this.w_DESCAUC = NVL(_Link_.CCDESCRI,space(35))
      this.w_TIPREG = NVL(_Link_.CCTIPREG,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
      this.w_TIPDOCG = NVL(_Link_.CCTIPDOC,space(2))
      this.w_TIPREGG = NVL(_Link_.CCTIPREG,space(1))
      this.w_FLPDOCG = NVL(_Link_.CCFLPDOC,space(1))
      this.w_SERDOCG = NVL(_Link_.CCSERDOC,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_SCCAUCOC = space(5)
      endif
      this.w_DESCAUC = space(35)
      this.w_TIPREG = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_TIPDOCG = space(2)
      this.w_TIPREGG = space(1)
      this.w_FLPDOCG = space(1)
      this.w_SERDOCG = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_TIPREG='N'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale contabile inesistente, obsoleta o di tipo IVA")
        endif
        this.w_SCCAUCOC = space(5)
        this.w_DESCAUC = space(35)
        this.w_TIPREG = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_TIPDOCG = space(2)
        this.w_TIPREGG = space(1)
        this.w_FLPDOCG = space(1)
        this.w_SERDOCG = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCCAUCOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_9(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 8 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+8<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_9.CCCODICE as CCCODICE409"+ ",link_4_9.CCDESCRI as CCDESCRI409"+ ",link_4_9.CCTIPREG as CCTIPREG409"+ ",link_4_9.CCDTOBSO as CCDTOBSO409"+ ",link_4_9.CCTIPDOC as CCTIPDOC409"+ ",link_4_9.CCTIPREG as CCTIPREG409"+ ",link_4_9.CCFLPDOC as CCFLPDOC409"+ ",link_4_9.CCSERDOC as CCSERDOC409"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_9 on SALMDACO.SCCAUCOC=link_4_9.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_9"
          i_cKey=i_cKey+'+" and SALMDACO.SCCAUCOC=link_4_9.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=SCCONTRO
  func Link_4_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCCONTRO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_SCCONTRO)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_GTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_GTIPCON;
                     ,'ANCODICE',trim(this.w_SCCONTRO))
          select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SCCONTRO)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SCCONTRO) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oSCCONTRO_4_12'),i_cWhere,'GSAR_BZC',"Conti",'CONCOEFA.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_GTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_GTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCCONTRO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_SCCONTRO);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_GTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_GTIPCON;
                       ,'ANCODICE',this.w_SCCONTRO)
            select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCCONTRO = NVL(_Link_.ANCODICE,space(15))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_SCCONTRO = space(15)
      endif
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_DESCON = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND EMPTY(.w_FLPART)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
        endif
        this.w_SCCONTRO = space(15)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_DESCON = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCCONTRO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SCCONDCA
  func Link_4_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCCONDCA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_SCCONDCA)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_GTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_GTIPCON;
                     ,'ANCODICE',trim(this.w_SCCONDCA))
          select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SCCONDCA)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SCCONDCA) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oSCCONDCA_4_13'),i_cWhere,'GSAR_BZC',"Conti",'CONCOEFA.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_GTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_GTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCCONDCA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_SCCONDCA);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_GTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_GTIPCON;
                       ,'ANCODICE',this.w_SCCONDCA)
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCCONDCA = NVL(_Link_.ANCODICE,space(15))
      this.w_DESDCA = NVL(_Link_.ANDESCRI,space(40))
      this.w_FLPART = NVL(_Link_.ANPARTSN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_SCCONDCA = space(15)
      endif
      this.w_DESDCA = space(40)
      this.w_FLPART = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND EMPTY(.w_FLPART)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
        endif
        this.w_SCCONDCA = space(15)
        this.w_DESDCA = space(40)
        this.w_FLPART = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCCONDCA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SCCONDCP
  func Link_4_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCCONDCP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_SCCONDCP)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_GTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_GTIPCON;
                     ,'ANCODICE',trim(this.w_SCCONDCP))
          select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SCCONDCP)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SCCONDCP) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oSCCONDCP_4_15'),i_cWhere,'GSAR_BZC',"Conti",'CONCOEFA.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_GTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_GTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCCONDCP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_SCCONDCP);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_GTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_GTIPCON;
                       ,'ANCODICE',this.w_SCCONDCP)
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCCONDCP = NVL(_Link_.ANCODICE,space(15))
      this.w_DESDCP = NVL(_Link_.ANDESCRI,space(40))
      this.w_FLPART = NVL(_Link_.ANPARTSN,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_SCCONDCP = space(15)
      endif
      this.w_DESDCP = space(40)
      this.w_FLPART = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND EMPTY(.w_FLPART)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
        endif
        this.w_SCCONDCP = space(15)
        this.w_DESDCP = space(40)
        this.w_FLPART = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCCONDCP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oSCDATREG_1_7.value==this.w_SCDATREG)
      this.oPgFrm.Page1.oPag.oSCDATREG_1_7.value=this.w_SCDATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oSCNUMREG_1_8.value==this.w_SCNUMREG)
      this.oPgFrm.Page1.oPag.oSCNUMREG_1_8.value=this.w_SCNUMREG
    endif
    if not(this.oPgFrm.Page1.oPag.oSCNUMDOC_1_9.value==this.w_SCNUMDOC)
      this.oPgFrm.Page1.oPag.oSCNUMDOC_1_9.value=this.w_SCNUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oSCALFDOC_1_10.value==this.w_SCALFDOC)
      this.oPgFrm.Page1.oPag.oSCALFDOC_1_10.value=this.w_SCALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oSCDATDOC_1_11.value==this.w_SCDATDOC)
      this.oPgFrm.Page1.oPag.oSCDATDOC_1_11.value=this.w_SCDATDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oSCCODESE_1_12.value==this.w_SCCODESE)
      this.oPgFrm.Page1.oPag.oSCCODESE_1_12.value=this.w_SCCODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oSCDATINI_1_13.value==this.w_SCDATINI)
      this.oPgFrm.Page1.oPag.oSCDATINI_1_13.value=this.w_SCDATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oSCDATFIN_1_14.value==this.w_SCDATFIN)
      this.oPgFrm.Page1.oPag.oSCDATFIN_1_14.value=this.w_SCDATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oSCTIPCON_1_15.RadioValue()==this.w_SCTIPCON)
      this.oPgFrm.Page1.oPag.oSCTIPCON_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSCCAOVAL_1_18.value==this.w_SCCAOVAL)
      this.oPgFrm.Page1.oPag.oSCCAOVAL_1_18.value=this.w_SCCAOVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oSCCODVAL_1_19.value==this.w_SCCODVAL)
      this.oPgFrm.Page1.oPag.oSCCODVAL_1_19.value=this.w_SCCODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oSCCONINI_1_22.value==this.w_SCCONINI)
      this.oPgFrm.Page1.oPag.oSCCONINI_1_22.value=this.w_SCCONINI
    endif
    if not(this.oPgFrm.Page1.oPag.oSCCONFIN_1_23.value==this.w_SCCONFIN)
      this.oPgFrm.Page1.oPag.oSCCONFIN_1_23.value=this.w_SCCONFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oSCDESSUP_1_24.value==this.w_SCDESSUP)
      this.oPgFrm.Page1.oPag.oSCDESSUP_1_24.value=this.w_SCDESSUP
    endif
    if not(this.oPgFrm.Page1.oPag.oSCBANAPP_1_25.value==this.w_SCBANAPP)
      this.oPgFrm.Page1.oPag.oSCBANAPP_1_25.value=this.w_SCBANAPP
    endif
    if not(this.oPgFrm.Page1.oPag.oSCBANNOS_1_26.value==this.w_SCBANNOS)
      this.oPgFrm.Page1.oPag.oSCBANNOS_1_26.value=this.w_SCBANNOS
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPSAL_1_27.value==this.w_IMPSAL)
      this.oPgFrm.Page1.oPag.oIMPSAL_1_27.value=this.w_IMPSAL
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPLIMITE_1_28.value==this.w_IMPLIMITE)
      this.oPgFrm.Page1.oPag.oIMPLIMITE_1_28.value=this.w_IMPLIMITE
    endif
    if not(this.oPgFrm.Page1.oPag.oCODPAG_1_29.value==this.w_CODPAG)
      this.oPgFrm.Page1.oPag.oCODPAG_1_29.value=this.w_CODPAG
    endif
    if not(this.oPgFrm.Page1.oPag.oEXECSTAM_1_32.RadioValue()==this.w_EXECSTAM)
      this.oPgFrm.Page1.oPag.oEXECSTAM_1_32.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINI_1_43.value==this.w_DESINI)
      this.oPgFrm.Page1.oPag.oDESINI_1_43.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIN_1_44.value==this.w_DESFIN)
      this.oPgFrm.Page1.oPag.oDESFIN_1_44.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESBAN_1_86.value==this.w_DESBAN)
      this.oPgFrm.Page1.oPag.oDESBAN_1_86.value=this.w_DESBAN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESNOSBA_1_87.value==this.w_DESNOSBA)
      this.oPgFrm.Page1.oPag.oDESNOSBA_1_87.value=this.w_DESNOSBA
    endif
    if not(this.oPgFrm.Page1.oPag.oRAGSOC_2_16.value==this.w_RAGSOC)
      this.oPgFrm.Page1.oPag.oRAGSOC_2_16.value=this.w_RAGSOC
      replace t_RAGSOC with this.oPgFrm.Page1.oPag.oRAGSOC_2_16.value
    endif
    if not(this.oPgFrm.Page2.oPag.oSCPAGRBA_4_1.RadioValue()==this.w_SCPAGRBA)
      this.oPgFrm.Page2.oPag.oSCPAGRBA_4_1.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oSCPAGCAM_4_2.RadioValue()==this.w_SCPAGCAM)
      this.oPgFrm.Page2.oPag.oSCPAGCAM_4_2.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oSCPAGRID_4_3.RadioValue()==this.w_SCPAGRID)
      this.oPgFrm.Page2.oPag.oSCPAGRID_4_3.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oSCPAGRDR_4_4.RadioValue()==this.w_SCPAGRDR)
      this.oPgFrm.Page2.oPag.oSCPAGRDR_4_4.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oSCPAGMAV_4_5.RadioValue()==this.w_SCPAGMAV)
      this.oPgFrm.Page2.oPag.oSCPAGMAV_4_5.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oSCPAGBON_4_6.RadioValue()==this.w_SCPAGBON)
      this.oPgFrm.Page2.oPag.oSCPAGBON_4_6.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oSCCAUCON_4_7.value==this.w_SCCAUCON)
      this.oPgFrm.Page2.oPag.oSCCAUCON_4_7.value=this.w_SCCAUCON
    endif
    if not(this.oPgFrm.Page2.oPag.oSCCAUCOF_4_8.value==this.w_SCCAUCOF)
      this.oPgFrm.Page2.oPag.oSCCAUCOF_4_8.value=this.w_SCCAUCOF
    endif
    if not(this.oPgFrm.Page2.oPag.oSCCAUCOC_4_9.value==this.w_SCCAUCOC)
      this.oPgFrm.Page2.oPag.oSCCAUCOC_4_9.value=this.w_SCCAUCOC
    endif
    if not(this.oPgFrm.Page2.oPag.oSCCONTRO_4_12.value==this.w_SCCONTRO)
      this.oPgFrm.Page2.oPag.oSCCONTRO_4_12.value=this.w_SCCONTRO
    endif
    if not(this.oPgFrm.Page2.oPag.oSCCONDCA_4_13.value==this.w_SCCONDCA)
      this.oPgFrm.Page2.oPag.oSCCONDCA_4_13.value=this.w_SCCONDCA
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAU_4_14.value==this.w_DESCAU)
      this.oPgFrm.Page2.oPag.oDESCAU_4_14.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page2.oPag.oSCCONDCP_4_15.value==this.w_SCCONDCP)
      this.oPgFrm.Page2.oPag.oSCCONDCP_4_15.value=this.w_SCCONDCP
    endif
    if not(this.oPgFrm.Page2.oPag.oDESDCA_4_19.value==this.w_DESDCA)
      this.oPgFrm.Page2.oPag.oDESDCA_4_19.value=this.w_DESDCA
    endif
    if not(this.oPgFrm.Page2.oPag.oDESDCP_4_22.value==this.w_DESDCP)
      this.oPgFrm.Page2.oPag.oDESDCP_4_22.value=this.w_DESDCP
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCON_4_23.value==this.w_DESCON)
      this.oPgFrm.Page2.oPag.oDESCON_4_23.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAUF_4_25.value==this.w_DESCAUF)
      this.oPgFrm.Page2.oPag.oDESCAUF_4_25.value=this.w_DESCAUF
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAUC_4_27.value==this.w_DESCAUC)
      this.oPgFrm.Page2.oPag.oDESCAUC_4_27.value=this.w_DESCAUC
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_4.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_4.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDATSCA_2_6.value==this.w_DATSCA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDATSCA_2_6.value=this.w_DATSCA
      replace t_DATSCA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDATSCA_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNUMPAR_2_7.value==this.w_NUMPAR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNUMPAR_2_7.value=this.w_NUMPAR
      replace t_NUMPAR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNUMPAR_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCODCON_2_8.value==this.w_CODCON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCODCON_2_8.value=this.w_CODCON
      replace t_CODCON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCODCON_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNUMDOC_2_9.value==this.w_NUMDOC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNUMDOC_2_9.value=this.w_NUMDOC
      replace t_NUMDOC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNUMDOC_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oALFDOC_2_10.value==this.w_ALFDOC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oALFDOC_2_10.value=this.w_ALFDOC
      replace t_ALFDOC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oALFDOC_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTOTIMP_2_11.value==this.w_TOTIMP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTOTIMP_2_11.value=this.w_TOTIMP
      replace t_TOTIMP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTOTIMP_2_11.value
    endif
    cp_SetControlsValueExtFlds(this,'SALMDACO')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_SCDATREG))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oSCDATREG_1_7.SetFocus()
            i_bnoObbl = !empty(.w_SCDATREG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_SCCODESE))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oSCCODESE_1_12.SetFocus()
            i_bnoObbl = !empty(.w_SCCODESE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((empty(.w_SCDATFIN) or (.w_SCDATFIN>=.w_SCDATINI)))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oSCDATINI_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore della data finale")
          case   not((empty(.w_SCDATFIN) or (.w_SCDATFIN>=.w_SCDATINI)))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oSCDATFIN_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore della data finale")
          case   not(((empty(.w_SCCONFIN)) OR  (.w_SCCONINI<=.w_SCCONFIN)) AND (EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO) And .w_PARTSN='S')  and not(empty(.w_SCCONINI))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oSCCONINI_1_22.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale, non gestito a partite oppure obsoleto")
          case   not(((.w_SCCONFIN>=.w_SCCONINI) or (empty(.w_SCCONINI))) AND (EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO) And .w_PARTSN='S')  and not(.w_SCTIPGES='B')  and not(empty(.w_SCCONFIN))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oSCCONFIN_1_23.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale, non gestito a partite oppure obsoleto")
          case   not(.w_CONSBF='C')  and (.w_SCTIPCON # 'G')  and not(empty(.w_SCBANNOS))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oSCBANNOS_1_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice di tipo salvo buon fine")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_TIPREG='N')  and not(.w_SCTIPGES='B')  and not(empty(.w_SCCAUCON))
            .oPgFrm.ActivePage=2
            .oPgFrm.Page2.oPag.oSCCAUCON_4_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale contabile inesistente, obsoleta o di tipo IVA")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_TIPREG='N')  and not(.w_SCTIPGES='B')  and not(empty(.w_SCCAUCOF))
            .oPgFrm.ActivePage=2
            .oPgFrm.Page2.oPag.oSCCAUCOF_4_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale contabile inesistente, obsoleta o di tipo IVA")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_TIPREG='N')  and not(empty(.w_SCCAUCOC))
            .oPgFrm.ActivePage=2
            .oPgFrm.Page2.oPag.oSCCAUCOC_4_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale contabile inesistente, obsoleta o di tipo IVA")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND EMPTY(.w_FLPART))  and not(.w_SCTIPGES='B')  and not(empty(.w_SCCONTRO))
            .oPgFrm.ActivePage=2
            .oPgFrm.Page2.oPag.oSCCONTRO_4_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND EMPTY(.w_FLPART))  and not(.w_SCTIPGES='B')  and not(empty(.w_SCCONDCA))
            .oPgFrm.ActivePage=2
            .oPgFrm.Page2.oPag.oSCCONDCA_4_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND EMPTY(.w_FLPART))  and not(.w_SCTIPGES='B')  and not(empty(.w_SCCONDCP))
            .oPgFrm.ActivePage=2
            .oPgFrm.Page2.oPag.oSCCONDCP_4_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gscg_msc
      * ---
       if Not Empty(CHKCONS('P',.w_SCDATREG,'B','N'))
         i_bnoChk=.F.
         i_bRes=.F.
         i_cErrorMsg=CHKCONS('P',.w_SCDATREG,'B','N')
       Endif
       if .w_SCTIPGES='B' AND .w_SCDATFIN>.w_SCDATREG
         i_bnoChk=.F.
         i_bRes=.F.
         i_cErrorMsg=ah_MsgFormat("La data di fine valuta deve essere minore o uguale alla data registrazione")
         .w_FASE=-1
       Endif
      
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_SCSERRIF))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_SCTIPGES = this.w_SCTIPGES
    this.o_SCSERIAL = this.w_SCSERIAL
    this.o_SCDATREG = this.w_SCDATREG
    this.o_SCDATDOC = this.w_SCDATDOC
    this.o_SCTIPCON = this.w_SCTIPCON
    this.o_SCCODVAL = this.w_SCCODVAL
    this.o_SCCONINI = this.w_SCCONINI
    this.o_SCCONFIN = this.w_SCCONFIN
    this.o_CAUCOC = this.w_CAUCOC
    this.o_CAUSBF = this.w_CAUSBF
    this.o_RIFERIMC = this.w_RIFERIMC
    this.o_RIFERIMF = this.w_RIFERIMF
    this.o_FLPDOC = this.w_FLPDOC
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_SCSERRIF)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_SCSERRIF=space(10)
      .w_SCORDRIF=0
      .w_SCNUMRIF=0
      .w_CPROWORD=MIN(9999,cp_maxroword()+10)
      .w_PTTIPCON=space(1)
      .w_DATSCA=ctod("  /  /  ")
      .w_NUMPAR=space(31)
      .w_CODCON=space(15)
      .w_NUMDOC=0
      .w_ALFDOC=space(10)
      .w_TOTIMP=0
      .w_SCRIFCOR=space(10)
      .w_RAGSOC=space(60)
      .w_PSERIAL=space(10)
      .w_PROWORD=0
      .DoRTCalc(1,65,.f.)
      if not(empty(.w_SCNUMRIF))
        .link_2_3('Full')
      endif
      .DoRTCalc(66,70,.f.)
      if not(empty(.w_CODCON))
        .link_2_8('Full')
      endif
        .oPgFrm.Page1.oPag.oObj_2_12.Calculate(IIF(empty(.w_NUMPAR) AND .w_SCORDRIF<>0, AH_Msgformat('<Riga eliminata in primanota>'),''))
    endwith
    this.DoRTCalc(71,122,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_SCSERRIF = t_SCSERRIF
    this.w_SCORDRIF = t_SCORDRIF
    this.w_SCNUMRIF = t_SCNUMRIF
    this.w_CPROWORD = t_CPROWORD
    this.w_PTTIPCON = t_PTTIPCON
    this.w_DATSCA = t_DATSCA
    this.w_NUMPAR = t_NUMPAR
    this.w_CODCON = t_CODCON
    this.w_NUMDOC = t_NUMDOC
    this.w_ALFDOC = t_ALFDOC
    this.w_TOTIMP = t_TOTIMP
    this.w_SCRIFCOR = t_SCRIFCOR
    this.w_RAGSOC = t_RAGSOC
    this.w_PSERIAL = t_PSERIAL
    this.w_PROWORD = t_PROWORD
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_SCSERRIF with this.w_SCSERRIF
    replace t_SCORDRIF with this.w_SCORDRIF
    replace t_SCNUMRIF with this.w_SCNUMRIF
    replace t_CPROWORD with this.w_CPROWORD
    replace t_PTTIPCON with this.w_PTTIPCON
    replace t_DATSCA with this.w_DATSCA
    replace t_NUMPAR with this.w_NUMPAR
    replace t_CODCON with this.w_CODCON
    replace t_NUMDOC with this.w_NUMDOC
    replace t_ALFDOC with this.w_ALFDOC
    replace t_TOTIMP with this.w_TOTIMP
    replace t_SCRIFCOR with this.w_SCRIFCOR
    replace t_RAGSOC with this.w_RAGSOC
    replace t_PSERIAL with this.w_PSERIAL
    replace t_PROWORD with this.w_PROWORD
    if i_srv='A'
      replace SCSERRIF with this.w_SCSERRIF
      replace SCORDRIF with this.w_SCORDRIF
      replace SCNUMRIF with this.w_SCNUMRIF
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
  func CanEdit()
    local i_res
    i_res=Empty(CHKCONS('P',this.w_SCDATREG,'B','N'))
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Attenzione, data registrazione di primanota antecedente alla data di consolidamento"))
    endif
    return(i_res)
  func CanDelete()
    local i_res
    i_res=Empty(CHKCONS('P',this.w_SCDATREG,'B','N'))
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Attenzione, data registrazione di primanota antecedente alla data di consolidamento"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgscg_mscPag1 as StdContainer
  Width  = 841
  height = 530
  stdWidth  = 841
  stdheight = 530
  resizeXpos=374
  resizeYpos=286
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSCDATREG_1_7 as StdField with uid="NHTAGGBLNS",rtseq=7,rtrep=.f.,;
    cFormVar = "w_SCDATREG", cQueryName = "SCDATREG",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data registrazione",;
    HelpContextID = 29057901,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=142, Top=11

  add object oSCNUMREG_1_8 as StdField with uid="URZZKRQDTV",rtseq=8,rtrep=.f.,;
    cFormVar = "w_SCNUMREG", cQueryName = "SCNUMREG",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero registrazione",;
    HelpContextID = 23069549,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=304, Top=11, cSayPict='"999999"', cGetPict='"999999"'

  add object oSCNUMDOC_1_9 as StdField with uid="YHDVSLKHFD",rtseq=9,rtrep=.f.,;
    cFormVar = "w_SCNUMDOC", cQueryName = "SCNUMDOC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento",;
    HelpContextID = 211811479,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=489, Top=11, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oSCNUMDOC_1_9.mCond()
    with this.Parent.oContained
      return (.w_SCMODGEN#'I')
    endwith
  endfunc

  add object oSCALFDOC_1_10 as StdField with uid="KCGQNVMHIZ",rtseq=10,rtrep=.f.,;
    cFormVar = "w_SCALFDOC", cQueryName = "SCALFDOC",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento",;
    HelpContextID = 219794583,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=615, Top=11, cSayPict="'!!!!!!!!!!'", cGetPict="'!!!!!!!!!!'", InputMask=replicate('X',10)

  func oSCALFDOC_1_10.mCond()
    with this.Parent.oContained
      return (.w_SCMODGEN#'I')
    endwith
  endfunc

  add object oSCDATDOC_1_11 as StdField with uid="HYVXHABCTW",rtseq=11,rtrep=.f.,;
    cFormVar = "w_SCDATDOC", cQueryName = "SCDATDOC",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento",;
    HelpContextID = 205823127,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=761, Top=11

  add object oSCCODESE_1_12 as StdField with uid="FBKYPRXRHJ",rtseq=12,rtrep=.f.,;
    cFormVar = "w_SCCODESE", cQueryName = "SCCODESE",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Codice esercizio",;
    HelpContextID = 63525739,;
   bGlobalFont=.t.,;
    Height=21, Width=52, Left=366, Top=11, InputMask=replicate('X',4), bHasZoom = .t. , tabstop=.f., cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_READAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_SCCODESE"

  func oSCCODESE_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oSCCODESE_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSCCODESE_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_READAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_READAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oSCCODESE_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oSCDATINI_1_13 as StdField with uid="DRCNKISGOL",rtseq=13,rtrep=.f.,;
    cFormVar = "w_SCDATINI", cQueryName = "SCDATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore della data finale",;
    ToolTipText = "Data inizio selezione",;
    HelpContextID = 121937041,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=142, Top=42

  func oSCDATINI_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_SCDATFIN) or (.w_SCDATFIN>=.w_SCDATINI)))
    endwith
    return bRes
  endfunc

  add object oSCDATFIN_1_14 as StdField with uid="EVSJOPJNUF",rtseq=14,rtrep=.f.,;
    cFormVar = "w_SCDATFIN", cQueryName = "SCDATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore della data finale",;
    ToolTipText = "Data fine selezione",;
    HelpContextID = 172268684,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=304, Top=42

  func oSCDATFIN_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_SCDATFIN) or (.w_SCDATFIN>=.w_SCDATINI)))
    endwith
    return bRes
  endfunc


  add object oSCTIPCON_1_15 as StdCombo with uid="LSZQFXHQLM",rtseq=15,rtrep=.f.,left=142,top=74,width=101,height=21;
    , ToolTipText = "Tipo conto";
    , HelpContextID = 226204812;
    , cFormVar="w_SCTIPCON",RowSource=""+"Cliente,"+"Fornitore,"+"Conto", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSCTIPCON_1_15.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..SCTIPCON,&i_cF..t_SCTIPCON),this.value)
    return(iif(xVal =1,'C',;
    iif(xVal =2,'F',;
    iif(xVal =3,'G',;
    space(1)))))
  endfunc
  func oSCTIPCON_1_15.GetRadio()
    this.Parent.oContained.w_SCTIPCON = this.RadioValue()
    return .t.
  endfunc

  func oSCTIPCON_1_15.ToRadio()
    this.Parent.oContained.w_SCTIPCON=trim(this.Parent.oContained.w_SCTIPCON)
    return(;
      iif(this.Parent.oContained.w_SCTIPCON=='C',1,;
      iif(this.Parent.oContained.w_SCTIPCON=='F',2,;
      iif(this.Parent.oContained.w_SCTIPCON=='G',3,;
      0))))
  endfunc

  func oSCTIPCON_1_15.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oSCTIPCON_1_15.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SCTIPGES='B')
    endwith
    endif
  endfunc

  func oSCTIPCON_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_SCCONINI)
        bRes2=.link_1_22('Full')
      endif
      if .not. empty(.w_SCCONFIN)
        bRes2=.link_1_23('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oSCCAOVAL_1_18 as StdField with uid="HAHCVBOZHL",rtseq=18,rtrep=.f.,;
    cFormVar = "w_SCCAOVAL", cQueryName = "SCCAOVAL",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Cambio valuta",;
    HelpContextID = 90919794,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=470, Top=74, cSayPict='"99999.999999"', cGetPict='"99999.999999"'

  func oSCCAOVAL_1_18.mCond()
    with this.Parent.oContained
      return ((GETVALUT(g_PERVAL, 'VADATEUR')>=IIF(EMPTY(.w_SCDATDOC), .w_SCDATREG, .w_SCDATDOC) OR .w_CAOVAL=0))
    endwith
  endfunc

  func oSCCAOVAL_1_18.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_SCCODVAL) or .w_SCTIPGES='B')
    endwith
    endif
  endfunc

  add object oSCCODVAL_1_19 as StdField with uid="WDESMPHEAZ",rtseq=19,rtrep=.f.,;
    cFormVar = "w_SCCODVAL", cQueryName = "SCCODVAL",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice valuta",;
    HelpContextID = 80302962,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=304, Top=74, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_SCCODVAL"

  func oSCCODVAL_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oSCCODVAL_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSCCODVAL_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oSCCODVAL_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Elenco valute",'',this.parent.oContained
  endproc
  proc oSCCODVAL_1_19.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_SCCODVAL
    i_obj.ecpSave()
  endproc

  add object oSCCONINI_1_22 as StdField with uid="LOVPNDPLGM",rtseq=22,rtrep=.f.,;
    cFormVar = "w_SCCONINI", cQueryName = "SCCONINI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale, non gestito a partite oppure obsoleto",;
    ToolTipText = "Codice conto iniziale",;
    HelpContextID = 127315089,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=142, Top=108, cSayPict="p_CCF", cGetPict="p_CCF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_SCTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_SCCONINI"

  func oSCCONINI_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oSCCONINI_1_22.ecpDrop(oSource)
    this.Parent.oContained.link_1_22('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSCCONINI_1_22.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_SCTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_SCTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oSCCONINI_1_22'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti\fornitori\conti",'GSCG_ACB.CONTI_VZM',this.parent.oContained
  endproc
  proc oSCCONINI_1_22.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_SCTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_SCCONINI
    i_obj.ecpSave()
  endproc

  add object oSCCONFIN_1_23 as StdField with uid="TVNKIOOHGB",rtseq=23,rtrep=.f.,;
    cFormVar = "w_SCCONFIN", cQueryName = "SCCONFIN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale, non gestito a partite oppure obsoleto",;
    ToolTipText = "Codice conto finale",;
    HelpContextID = 177646732,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=142, Top=145, cSayPict="p_CCF", cGetPict="p_CCF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_SCTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_SCCONFIN"

  func oSCCONFIN_1_23.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SCTIPGES='B')
    endwith
    endif
  endfunc

  func oSCCONFIN_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oSCCONFIN_1_23.ecpDrop(oSource)
    this.Parent.oContained.link_1_23('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSCCONFIN_1_23.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_SCTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_SCTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oSCCONFIN_1_23'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti\fornitori\conti",'GSCG_ACB.CONTI_VZM',this.parent.oContained
  endproc
  proc oSCCONFIN_1_23.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_SCTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_SCCONFIN
    i_obj.ecpSave()
  endproc

  add object oSCDESSUP_1_24 as StdField with uid="VRCFEWRZNL",rtseq=24,rtrep=.f.,;
    cFormVar = "w_SCDESSUP", cQueryName = "SCDESSUP",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale descrizione aggiuntiva",;
    HelpContextID = 45048694,;
   bGlobalFont=.t.,;
    Height=21, Width=312, Left=142, Top=182, InputMask=replicate('X',50)

  add object oSCBANAPP_1_25 as StdField with uid="QCVNOZEPIW",rtseq=25,rtrep=.f.,;
    cFormVar = "w_SCBANAPP", cQueryName = "SCBANAPP",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 262454410,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=142, Top=218, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="BAN_CHE", cZoomOnZoom="GSAR_ABA", oKey_1_1="BACODBAN", oKey_1_2="this.w_SCBANAPP"

  func oSCBANAPP_1_25.mCond()
    with this.Parent.oContained
      return (.w_SCTIPCON # 'G')
    endwith
  endfunc

  func oSCBANAPP_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oSCBANAPP_1_25.ecpDrop(oSource)
    this.Parent.oContained.link_1_25('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSCBANAPP_1_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'BAN_CHE','*','BACODBAN',cp_AbsName(this.parent,'oSCBANAPP_1_25'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ABA',"",'',this.parent.oContained
  endproc
  proc oSCBANAPP_1_25.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ABA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_SCBANAPP
    i_obj.ecpSave()
  endproc

  add object oSCBANNOS_1_26 as StdField with uid="DEIQAHZOAG",rtseq=26,rtrep=.f.,;
    cFormVar = "w_SCBANNOS", cQueryName = "SCBANNOS",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice di tipo salvo buon fine",;
    HelpContextID = 44350599,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=142, Top=256, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_SCBANNOS"

  func oSCBANNOS_1_26.mCond()
    with this.Parent.oContained
      return (.w_SCTIPCON # 'G')
    endwith
  endfunc

  func oSCBANNOS_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_26('Part',this)
    endwith
    return bRes
  endfunc

  proc oSCBANNOS_1_26.ecpDrop(oSource)
    this.Parent.oContained.link_1_26('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSCBANNOS_1_26.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oSCBANNOS_1_26'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"",'',this.parent.oContained
  endproc
  proc oSCBANNOS_1_26.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_SCBANNOS
    i_obj.ecpSave()
  endproc

  add object oIMPSAL_1_27 as StdField with uid="EEYEUGEPGN",rtseq=27,rtrep=.f.,;
    cFormVar = "w_IMPSAL", cQueryName = "IMPSAL",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo da saldare",;
    HelpContextID = 90297210,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=698, Top=74, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oIMPSAL_1_27.mCond()
    with this.Parent.oContained
      return (.w_SCCONINI == .w_SCCONFIN AND !EMPTY(.w_SCCONINI+.w_SCCONFIN) and .cfunction='Edit')
    endwith
  endfunc

  add object oIMPLIMITE_1_28 as StdField with uid="SOLHEKMFIL",rtseq=28,rtrep=.f.,;
    cFormVar = "w_IMPLIMITE", cQueryName = "IMPLIMITE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Permette di filtrare le scadenze con importo minore o uguale al limite indicato",;
    HelpContextID = 65588950,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=698, Top=108, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oIMPLIMITE_1_28.mCond()
    with this.Parent.oContained
      return (.cfunction='Edit')
    endwith
  endfunc

  func oIMPLIMITE_1_28.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SCTIPGES='B')
    endwith
    endif
  endfunc

  add object oCODPAG_1_29 as StdField with uid="DAZVUSZPXS",rtseq=29,rtrep=.f.,;
    cFormVar = "w_CODPAG", cQueryName = "CODPAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice pagamento",;
    HelpContextID = 174428634,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=698, Top=145, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="PAG_2AME", cZoomOnZoom="GSAR_MPA", oKey_1_1="P2CODICE", oKey_1_2="this.w_CODPAG"

  func oCODPAG_1_29.mCond()
    with this.Parent.oContained
      return (.cfunction='Edit')
    endwith
  endfunc

  func oCODPAG_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_29('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODPAG_1_29.ecpDrop(oSource)
    this.Parent.oContained.link_1_29('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODPAG_1_29.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PAG_2AME','*','P2CODICE',cp_AbsName(this.parent,'oCODPAG_1_29'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MPA',"Codici pagamento",'',this.parent.oContained
  endproc
  proc oCODPAG_1_29.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MPA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_P2CODICE=this.parent.oContained.w_CODPAG
    i_obj.ecpSave()
  endproc

  add object oEXECSTAM_1_32 as StdCheck with uid="MPZJPWEPWM",rtseq=32,rtrep=.f.,left=729, top=204, caption="Stampa partite",;
    ToolTipText = "Se attivo: al termine dell'elaborazione sar� avviata la stampa delle partite generate dal saldaconto",;
    HelpContextID = 61704083,;
    cFormVar="w_EXECSTAM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oEXECSTAM_1_32.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..EXECSTAM,&i_cF..t_EXECSTAM),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oEXECSTAM_1_32.GetRadio()
    this.Parent.oContained.w_EXECSTAM = this.RadioValue()
    return .t.
  endfunc

  func oEXECSTAM_1_32.ToRadio()
    this.Parent.oContained.w_EXECSTAM=trim(this.Parent.oContained.w_EXECSTAM)
    return(;
      iif(this.Parent.oContained.w_EXECSTAM=='S',1,;
      0))
  endfunc

  func oEXECSTAM_1_32.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oEXECSTAM_1_32.mCond()
    with this.Parent.oContained
      return (.cfunction # 'Query')
    endwith
  endfunc


  add object oBtn_1_33 as Btn_Mpn with uid="QMLVMPYQSC",left=736, top=235, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare saldaconto";
    , HelpContextID = 218943865;
    , Caption='\<Conferma ';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_33.Click()
      with this.Parent.oContained
        GSCG_BSS(this.Parent.oContained,"C")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_33.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
   endif
  endfunc


  add object oBtn_1_34 as StdButton with uid="RWVHEPYZPP",left=788, top=235, width=48,height=45,;
    CpPicture="bmp\TESO.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per per accedere alla registrazione contabile associata";
    , HelpContextID = 48885238;
    , TabStop=.f.,Caption='P\<rimanota';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_34.Click()
      with this.Parent.oContained
        GSAR_BZP(this.Parent.oContained,IIF(.w_SCMODGEN="G",.w_SCRIFCON,.w_SCRIFCOR))
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_34.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_SCRIFCON))
    endwith
  endfunc

  func oBtn_1_34.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_SCRIFCON))
    endwith
   endif
  endfunc


  add object oBtn_1_35 as StdButton with uid="JAUDZWTEYF",left=684, top=235, width=48,height=45,;
    CpPicture="bmp\DETTAGLI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per selezionare i conti da saldare";
    , HelpContextID = 247386586;
    , Caption='\<Conti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_35.Click()
      do GSCG_SMB with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_35.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_scconini) and empty(.w_scconfin) or .w_scconini=.w_scconfin or .cfunction#'Load')
    endwith
   endif
  endfunc


  add object oBtn_1_37 as StdButton with uid="VTIXHKIHCG",left=789, top=235, width=48,height=45,;
    CpPicture="BMP\ScadTL.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per chiudere le partite";
    , HelpContextID = 256835290;
    , tabstop=.f., Caption='\<Salda ';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_37.Click()
      with this.Parent.oContained
        GSCG_BSP(this.Parent.oContained,"S")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_37.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_SCRIFCON) OR .cFunction='Load')
    endwith
   endif
  endfunc


  add object oBtn_1_38 as StdButton with uid="XXXPHTMLQY",left=736, top=235, width=48,height=45,;
    CpPicture="bmp\PRINT.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare elenco partite";
    , HelpContextID = 44655654;
    , Caption='\<Stampa ';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_38.Click()
      with this.Parent.oContained
        GSCG_BSS(this.Parent.oContained,"P")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_38.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc


  add object oBtn_1_40 as StdButton with uid="RUMHWGUEQV",left=684, top=235, width=48,height=45,;
    CpPicture="bmp\dettagli.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere alla manutenzione delle partite";
    , HelpContextID = 138312545;
    , Caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_40.Click()
      with this.Parent.oContained
        GSTE_BZP(this.Parent.oContained,.w_PSERIAL, .w_PROWORD,.w_SCNUMRIF, 0,3,0)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_40.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_PSERIAL))
    endwith
  endfunc

  func oBtn_1_40.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cfunction='Load')
    endwith
   endif
  endfunc

  add object oDESINI_1_43 as StdField with uid="ZFDGBZBWPM",rtseq=34,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 127642570,;
   bGlobalFont=.t.,;
    Height=21, Width=323, Left=262, Top=108, InputMask=replicate('X',40)

  add object oDESFIN_1_44 as StdField with uid="SSVHXKHHVZ",rtseq=35,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 49195978,;
   bGlobalFont=.t.,;
    Height=21, Width=323, Left=262, Top=145, InputMask=replicate('X',40)

  func oDESFIN_1_44.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SCTIPGES='B')
    endwith
    endif
  endfunc


  add object oObj_1_64 as cp_runprogram with uid="WMKNJKLMGH",left=-2, top=544, width=203,height=19,;
    caption='GSCG_BSS(S)',;
   bGlobalFont=.t.,;
    prg="GSCG_BSS('S')",;
    cEvent = "ChiudePartite",;
    nPag=1;
    , ToolTipText = "Esegue creazione registrazione di chiusura in primanota";
    , HelpContextID = 41175097


  add object oObj_1_73 as cp_runprogram with uid="YXIZJTAQRC",left=203, top=544, width=203,height=19,;
    caption='GSCG_BSS(E)',;
   bGlobalFont=.t.,;
    prg="GSCG_BSS('E')",;
    cEvent = "Legge",;
    nPag=1;
    , ToolTipText = "Esegue chiusura saldaconto dopo la cancellazione";
    , HelpContextID = 41171513


  add object oObj_1_77 as cp_runprogram with uid="RDIRJLTYGB",left=407, top=544, width=294,height=19,;
    caption='GSCG_BZP(ELI ,w_RIFCONPN)',;
   bGlobalFont=.t.,;
    prg="GSCG_BZP('ELI',w_RIFCONPN)",;
    cEvent = "Elimina",;
    nPag=1;
    , ToolTipText = "Esegue chiusura saldaconto dopo la cancellazione";
    , HelpContextID = 201909357


  add object oObj_1_78 as cp_runprogram with uid="ZFLGPLXKZO",left=407, top=564, width=294,height=19,;
    caption='GSCG_BSS(D)',;
   bGlobalFont=.t.,;
    prg="GSCG_BSS('D')",;
    cEvent = "HasEvent",;
    nPag=1;
    , ToolTipText = "Esegue cancellazione saldaconto";
    , HelpContextID = 41171257


  add object oObj_1_81 as cp_calclbl with uid="UWJXQNTCXX",left=4, top=109, width=137,height=15,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",alignment=1,;
    nPag=1;
    , HelpContextID = 80863718


  add object oObj_1_82 as cp_calclbl with uid="WPTYHZREFL",left=8, top=46, width=133,height=15,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",alignment=1,;
    nPag=1;
    , HelpContextID = 80863718


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=369, top=589, width=334,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=7,Field1="CPROWORD",Label1="Riga",Field2="DATSCA",Label2="Data",Field3="NUMPAR",Label3="Partita",Field4="CODCON",Label4="icase(w_SCTIPCON='C',Ah_msgformat('Cliente'),w_SCTIPCON='F',AH_MSGFORMAT('Fornitore'),AH_MSGFORMAT('Conto'))",Field5="NUMDOC",Label5="N.doc.",Field6="ALFDOC",Label6="Serie",Field7="TOTIMP",Label7="Importo",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 218956922

  add object oDESBAN_1_86 as StdField with uid="WILWCXIWEN",rtseq=60,rtrep=.f.,;
    cFormVar = "w_DESBAN", cQueryName = "DESBAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 57846730,;
   bGlobalFont=.t.,;
    Height=21, Width=377, Left=228, Top=218, InputMask=replicate('X',50)

  add object oDESNOSBA_1_87 as StdField with uid="VHJUKKOCAU",rtseq=61,rtrep=.f.,;
    cFormVar = "w_DESNOSBA", cQueryName = "DESNOSBA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 41505911,;
   bGlobalFont=.t.,;
    Height=21, Width=272, Left=262, Top=256, InputMask=replicate('X',35)

  add object oStr_1_39 as StdString with uid="QRHAVWBNZG",Visible=.t., Left=256, Top=44,;
    Alignment=1, Width=47, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="KKQVZPOHTT",Visible=.t., Left=68, Top=73,;
    Alignment=1, Width=73, Height=18,;
    Caption="Tipo conto:"  ;
  , bGlobalFont=.t.

  func oStr_1_41.mHide()
    with this.Parent.oContained
      return (.w_SCTIPGES='B')
    endwith
  endfunc

  add object oStr_1_42 as StdString with uid="WKJFXFFJCQ",Visible=.t., Left=4, Top=146,;
    Alignment=1, Width=137, Height=18,;
    Caption="Codice conto finale:"  ;
  , bGlobalFont=.t.

  func oStr_1_42.mHide()
    with this.Parent.oContained
      return (.w_SCTIPGES='B')
    endwith
  endfunc

  add object oStr_1_62 as StdString with uid="AVRWMKTYCQ",Visible=.t., Left=248, Top=74,;
    Alignment=1, Width=55, Height=18,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_63 as StdString with uid="FFCBGCIVIB",Visible=.t., Left=406, Top=74,;
    Alignment=1, Width=63, Height=18,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  func oStr_1_63.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_SCCODVAL) or .w_SCTIPGES='B')
    endwith
  endfunc

  add object oStr_1_66 as StdString with uid="WJFQJZIDLP",Visible=.t., Left=21, Top=11,;
    Alignment=1, Width=120, Height=18,;
    Caption="Data registrazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_67 as StdString with uid="DRCURBYLSY",Visible=.t., Left=362, Top=13,;
    Alignment=0, Width=8, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_68 as StdString with uid="LIXTJHKQXQ",Visible=.t., Left=237, Top=13,;
    Alignment=1, Width=66, Height=18,;
    Caption="Num.reg.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_69 as StdString with uid="IXARCHPLBS",Visible=.t., Left=609, Top=11,;
    Alignment=2, Width=5, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_70 as StdString with uid="OZHMVYRSNE",Visible=.t., Left=702, Top=11,;
    Alignment=1, Width=58, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_71 as StdString with uid="MWIXNQRWZL",Visible=.t., Left=424, Top=11,;
    Alignment=1, Width=64, Height=18,;
    Caption="Num. doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_72 as StdString with uid="INXMYPUZXM",Visible=.t., Left=59, Top=182,;
    Alignment=1, Width=82, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_76 as StdString with uid="SMYIFIWRWC",Visible=.t., Left=-6, Top=565,;
    Alignment=0, Width=373, Height=21,;
    Caption="Attenzione bottone pnota sovrapposto"  ;
    , FontName = "@Arial Unicode MS", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_88 as StdString with uid="AZUPZZKROP",Visible=.t., Left=34, Top=220,;
    Alignment=1, Width=107, Height=18,;
    Caption="Banca appoggio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_89 as StdString with uid="DZMLEITZCM",Visible=.t., Left=48, Top=258,;
    Alignment=1, Width=93, Height=18,;
    Caption="Nostra banca:"  ;
  , bGlobalFont=.t.

  add object oStr_1_91 as StdString with uid="XYZAFVJFWA",Visible=.t., Left=584, Top=74,;
    Alignment=1, Width=112, Height=18,;
    Caption="Imp. da saldare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_92 as StdString with uid="ARKWAOUWYT",Visible=.t., Left=640, Top=146,;
    Alignment=1, Width=56, Height=18,;
    Caption="Cod. pag.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_93 as StdString with uid="MUDXYOJQWX",Visible=.t., Left=600, Top=179,;
    Alignment=1, Width=96, Height=18,;
    Caption="Mod. generaz.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_94 as StdString with uid="XLKHFSKIRE",Visible=.t., Left=698, Top=179,;
    Alignment=0, Width=47, Height=17,;
    Caption="Globale"    , ToolTipText="Modalit� globale: genera un'unica reg. di saldaconto; "+chr(13)+chr(10)+"Modalit� individuale: genera una reg. di saldaconto per ogni intestatario.";
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_94.mHide()
    with this.Parent.oContained
      return (.w_SCMODGEN = 'I')
    endwith
  endfunc

  add object oStr_1_95 as StdString with uid="OIIDNUNVPN",Visible=.t., Left=698, Top=179,;
    Alignment=0, Width=64, Height=17,;
    Caption="Individuale"    , ToolTipText="Modalit� globale: genera un'unica reg. di saldaconto; "+chr(13)+chr(10)+"Modalit� individuale: genera una reg. di saldaconto per ogni intestatario.";
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_95.mHide()
    with this.Parent.oContained
      return (.w_SCMODGEN='G')
    endwith
  endfunc

  add object oStr_1_99 as StdString with uid="DRUBVBAAND",Visible=.t., Left=589, Top=109,;
    Alignment=1, Width=107, Height=18,;
    Caption="Importo limite:"  ;
  , bGlobalFont=.t.

  func oStr_1_99.mHide()
    with this.Parent.oContained
      return (.w_SCTIPGES='B')
    endwith
  endfunc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-7,top=304,;
    width=830+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-6,top=305,width=829+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oRAGSOC_2_16.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc


  add object oObj_2_12 as cp_calclbl with uid="UHYPVCCROL",width=256,height=20,;
   left=3, top=589,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",alignment=1,;
    nPag=2;
    , HelpContextID = 80863718

  add object oRAGSOC_2_16 as StdTrsField with uid="RPGKKKJKXT",rtseq=77,rtrep=.t.,;
    cFormVar="w_RAGSOC",value=space(60),enabled=.f.,;
    HelpContextID = 226651882,;
    cTotal="", bFixedPos=.t., cQueryName = "RAGSOC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=358, Top=504, InputMask=replicate('X',60)

  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oObj_3_1 as cp_calclbl with uid="DMUAXJECJV",width=100,height=22,;
   left=255, top=505,;
    caption='etichetta',;
   bGlobalFont=.t.,;
    caption="label text",Alignment=1,;
    cEvent = "Init",;
    nPag=3;
    , HelpContextID = 102755274
enddefine

  define class tgscg_mscPag2 as StdContainer
    Width  = 841
    height = 530
    stdWidth  = 841
    stdheight = 530
  resizeXpos=529
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSCPAGRBA_4_1 as StdCheck with uid="VPNCMPDIXY",rtseq=80,rtrep=.f.,left=199, top=15, caption="Ri.Ba.",;
    ToolTipText = "Pagamento RiBa",;
    HelpContextID = 15475559,;
    cFormVar="w_SCPAGRBA", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oSCPAGRBA_4_1.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..SCPAGRBA,&i_cF..t_SCPAGRBA),this.value)
    return(iif(xVal =1,'RB',;
    'XX'))
  endfunc
  func oSCPAGRBA_4_1.GetRadio()
    this.Parent.oContained.w_SCPAGRBA = this.RadioValue()
    return .t.
  endfunc

  func oSCPAGRBA_4_1.ToRadio()
    this.Parent.oContained.w_SCPAGRBA=trim(this.Parent.oContained.w_SCPAGRBA)
    return(;
      iif(this.Parent.oContained.w_SCPAGRBA=='RB',1,;
      0))
  endfunc

  func oSCPAGRBA_4_1.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oSCPAGRBA_4_1.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SCTIPGES='B')
    endwith
    endif
  endfunc

  add object oSCPAGCAM_4_2 as StdCheck with uid="PPVPICEYPF",rtseq=81,rtrep=.f.,left=391, top=15, caption="Cambiale",;
    ToolTipText = "Pagamento cambiale",;
    HelpContextID = 32252787,;
    cFormVar="w_SCPAGCAM", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oSCPAGCAM_4_2.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..SCPAGCAM,&i_cF..t_SCPAGCAM),this.value)
    return(iif(xVal =1,'CA',;
    'XX'))
  endfunc
  func oSCPAGCAM_4_2.GetRadio()
    this.Parent.oContained.w_SCPAGCAM = this.RadioValue()
    return .t.
  endfunc

  func oSCPAGCAM_4_2.ToRadio()
    this.Parent.oContained.w_SCPAGCAM=trim(this.Parent.oContained.w_SCPAGCAM)
    return(;
      iif(this.Parent.oContained.w_SCPAGCAM=='CA',1,;
      0))
  endfunc

  func oSCPAGCAM_4_2.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oSCPAGCAM_4_2.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SCTIPGES='B')
    endwith
    endif
  endfunc

  add object oSCPAGRID_4_3 as StdCheck with uid="LDJZYQVIZQ",rtseq=82,rtrep=.f.,left=524, top=15, caption="R.I.D.",;
    ToolTipText = "Pagamento R.I.D.",;
    HelpContextID = 252959894,;
    cFormVar="w_SCPAGRID", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oSCPAGRID_4_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..SCPAGRID,&i_cF..t_SCPAGRID),this.value)
    return(iif(xVal =1,'RI',;
    'XX'))
  endfunc
  func oSCPAGRID_4_3.GetRadio()
    this.Parent.oContained.w_SCPAGRID = this.RadioValue()
    return .t.
  endfunc

  func oSCPAGRID_4_3.ToRadio()
    this.Parent.oContained.w_SCPAGRID=trim(this.Parent.oContained.w_SCPAGRID)
    return(;
      iif(this.Parent.oContained.w_SCPAGRID=='RI',1,;
      0))
  endfunc

  func oSCPAGRID_4_3.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oSCPAGRID_4_3.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SCTIPGES='B')
    endwith
    endif
  endfunc

  add object oSCPAGRDR_4_4 as StdCheck with uid="COVRHKDUKZ",rtseq=83,rtrep=.f.,left=199, top=35, caption="Rimessa diretta",;
    ToolTipText = "Pagamento rimessa diretta",;
    HelpContextID = 15475576,;
    cFormVar="w_SCPAGRDR", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oSCPAGRDR_4_4.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..SCPAGRDR,&i_cF..t_SCPAGRDR),this.value)
    return(iif(xVal =1,'RD',;
    'XX'))
  endfunc
  func oSCPAGRDR_4_4.GetRadio()
    this.Parent.oContained.w_SCPAGRDR = this.RadioValue()
    return .t.
  endfunc

  func oSCPAGRDR_4_4.ToRadio()
    this.Parent.oContained.w_SCPAGRDR=trim(this.Parent.oContained.w_SCPAGRDR)
    return(;
      iif(this.Parent.oContained.w_SCPAGRDR=='RD',1,;
      0))
  endfunc

  func oSCPAGRDR_4_4.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oSCPAGRDR_4_4.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SCTIPGES='B')
    endwith
    endif
  endfunc

  add object oSCPAGMAV_4_5 as StdCheck with uid="NPCFNHZQOX",rtseq=84,rtrep=.f.,left=391, top=35, caption="M.AV.",;
    HelpContextID = 200024956,;
    cFormVar="w_SCPAGMAV", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oSCPAGMAV_4_5.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..SCPAGMAV,&i_cF..t_SCPAGMAV),this.value)
    return(iif(xVal =1,'MA',;
    'XX'))
  endfunc
  func oSCPAGMAV_4_5.GetRadio()
    this.Parent.oContained.w_SCPAGMAV = this.RadioValue()
    return .t.
  endfunc

  func oSCPAGMAV_4_5.ToRadio()
    this.Parent.oContained.w_SCPAGMAV=trim(this.Parent.oContained.w_SCPAGMAV)
    return(;
      iif(this.Parent.oContained.w_SCPAGMAV=='MA',1,;
      0))
  endfunc

  func oSCPAGMAV_4_5.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oSCPAGMAV_4_5.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SCTIPGES='B')
    endwith
    endif
  endfunc

  add object oSCPAGBON_4_6 as StdCheck with uid="GFPLWYLHCB",rtseq=85,rtrep=.f.,left=524, top=35, caption="Bonifico",;
    ToolTipText = "Pagamento bonifico",;
    HelpContextID = 252959884,;
    cFormVar="w_SCPAGBON", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oSCPAGBON_4_6.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..SCPAGBON,&i_cF..t_SCPAGBON),this.value)
    return(iif(xVal =1,'BO',;
    'XX'))
  endfunc
  func oSCPAGBON_4_6.GetRadio()
    this.Parent.oContained.w_SCPAGBON = this.RadioValue()
    return .t.
  endfunc

  func oSCPAGBON_4_6.ToRadio()
    this.Parent.oContained.w_SCPAGBON=trim(this.Parent.oContained.w_SCPAGBON)
    return(;
      iif(this.Parent.oContained.w_SCPAGBON=='BO',1,;
      0))
  endfunc

  func oSCPAGBON_4_6.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oSCPAGBON_4_6.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SCTIPGES='B')
    endwith
    endif
  endfunc

  add object oSCCAUCON_4_7 as StdField with uid="SIYAUUHBVR",rtseq=86,rtrep=.f.,;
    cFormVar = "w_SCCAUCON", cQueryName = "SCCAUCON",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale contabile inesistente, obsoleta o di tipo IVA",;
    ToolTipText = "Causale contabile utilizzata per saldaconto per i clienti",;
    HelpContextID = 221555852,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=197, Top=63, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_SCCAUCON"

  func oSCCAUCON_4_7.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SCTIPGES='B')
    endwith
    endif
  endfunc

  func oSCCAUCON_4_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oSCCAUCON_4_7.ecpDrop(oSource)
    this.Parent.oContained.link_4_7('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSCCAUCON_4_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oSCCAUCON_4_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'CAUCOEFA.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oSCCAUCON_4_7.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_SCCAUCON
    i_obj.ecpSave()
  endproc

  add object oSCCAUCOF_4_8 as StdField with uid="DJIZLEFKOV",rtseq=87,rtrep=.f.,;
    cFormVar = "w_SCCAUCOF", cQueryName = "SCCAUCOF",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale contabile inesistente, obsoleta o di tipo IVA",;
    ToolTipText = "Causale contabile utilizzata per saldaconto per i fornitori",;
    HelpContextID = 221555860,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=197, Top=90, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_SCCAUCOF"

  func oSCCAUCOF_4_8.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SCTIPGES='B')
    endwith
    endif
  endfunc

  func oSCCAUCOF_4_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oSCCAUCOF_4_8.ecpDrop(oSource)
    this.Parent.oContained.link_4_8('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSCCAUCOF_4_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oSCCAUCOF_4_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'CAUCOEFA.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oSCCAUCOF_4_8.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_SCCAUCOF
    i_obj.ecpSave()
  endproc

  add object oSCCAUCOC_4_9 as StdField with uid="YLMCLKBWHF",rtseq=88,rtrep=.f.,;
    cFormVar = "w_SCCAUCOC", cQueryName = "SCCAUCOC",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale contabile inesistente, obsoleta o di tipo IVA",;
    ToolTipText = "Causale contabile utilizzata per saldaconto per i conti",;
    HelpContextID = 221555863,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=197, Top=117, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_SCCAUCOC"

  func oSCCAUCOC_4_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oSCCAUCOC_4_9.ecpDrop(oSource)
    this.Parent.oContained.link_4_9('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSCCAUCOC_4_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oSCCAUCOC_4_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'CAUCOEFA.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oSCCAUCOC_4_9.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_SCCAUCOC
    i_obj.ecpSave()
  endproc

  add object oSCCONTRO_4_12 as StdField with uid="HDOLWSUUUQ",rtseq=91,rtrep=.f.,;
    cFormVar = "w_SCCONTRO", cQueryName = "SCCONTRO",;
    bObbl = .f. , nPag = 4, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto o gestito a partite",;
    ToolTipText = "Conto contropartita da utilizzare",;
    HelpContextID = 57234293,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=197, Top=144, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_GTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_SCCONTRO"

  func oSCCONTRO_4_12.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SCTIPGES='B')
    endwith
    endif
  endfunc

  func oSCCONTRO_4_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oSCCONTRO_4_12.ecpDrop(oSource)
    this.Parent.oContained.link_4_12('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSCCONTRO_4_12.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_GTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_GTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oSCCONTRO_4_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'CONCOEFA.CONTI_VZM',this.parent.oContained
  endproc
  proc oSCCONTRO_4_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_GTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_SCCONTRO
    i_obj.ecpSave()
  endproc

  add object oSCCONDCA_4_13 as StdField with uid="KEODFYWRYI",rtseq=92,rtrep=.f.,;
    cFormVar = "w_SCCONDCA", cQueryName = "SCCONDCA",;
    bObbl = .f. , nPag = 4, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto o gestito a partite",;
    ToolTipText = "Conto per eventuale registrazione delle differenze cambi attive",;
    HelpContextID = 57234279,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=197, Top=171, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_GTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_SCCONDCA"

  func oSCCONDCA_4_13.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SCTIPGES='B')
    endwith
    endif
  endfunc

  func oSCCONDCA_4_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oSCCONDCA_4_13.ecpDrop(oSource)
    this.Parent.oContained.link_4_13('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSCCONDCA_4_13.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_GTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_GTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oSCCONDCA_4_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'CONCOEFA.CONTI_VZM',this.parent.oContained
  endproc
  proc oSCCONDCA_4_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_GTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_SCCONDCA
    i_obj.ecpSave()
  endproc

  add object oDESCAU_4_14 as StdField with uid="IDYGLPXQDU",rtseq=93,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 59659318,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=262, Top=63, InputMask=replicate('X',35)

  func oDESCAU_4_14.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SCTIPGES='B')
    endwith
    endif
  endfunc

  add object oSCCONDCP_4_15 as StdField with uid="UACEJSOAQT",rtseq=94,rtrep=.f.,;
    cFormVar = "w_SCCONDCP", cQueryName = "SCCONDCP",;
    bObbl = .f. , nPag = 4, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto o gestito a partite",;
    ToolTipText = "Conto per eventuale registrazione delle differenze cambi passive",;
    HelpContextID = 57234294,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=197, Top=198, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_GTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_SCCONDCP"

  func oSCCONDCP_4_15.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SCTIPGES='B')
    endwith
    endif
  endfunc

  func oSCCONDCP_4_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oSCCONDCP_4_15.ecpDrop(oSource)
    this.Parent.oContained.link_4_15('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSCCONDCP_4_15.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_GTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_GTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oSCCONDCP_4_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'CONCOEFA.CONTI_VZM',this.parent.oContained
  endproc
  proc oSCCONDCP_4_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_GTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_SCCONDCP
    i_obj.ecpSave()
  endproc

  add object oDESDCA_4_19 as StdField with uid="AYSVAPHLAC",rtseq=96,rtrep=.f.,;
    cFormVar = "w_DESDCA", cQueryName = "DESDCA",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 5286858,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=327, Top=171, InputMask=replicate('X',40)

  func oDESDCA_4_19.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SCTIPGES='B')
    endwith
    endif
  endfunc

  add object oDESDCP_4_22 as StdField with uid="UWMNJYKTGH",rtseq=98,rtrep=.f.,;
    cFormVar = "w_DESDCP", cQueryName = "DESDCP",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 22064074,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=327, Top=198, InputMask=replicate('X',40)

  func oDESDCP_4_22.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SCTIPGES='B')
    endwith
    endif
  endfunc

  add object oDESCON_4_23 as StdField with uid="MALPSAEPNH",rtseq=99,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 43101130,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=327, Top=144, InputMask=replicate('X',40)

  func oDESCON_4_23.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SCTIPGES='B')
    endwith
    endif
  endfunc

  add object oDESCAUF_4_25 as StdField with uid="HAGFGFJDLV",rtseq=100,rtrep=.f.,;
    cFormVar = "w_DESCAUF", cQueryName = "DESCAUF",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 59659318,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=262, Top=90, InputMask=replicate('X',35)

  func oDESCAUF_4_25.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SCTIPGES='B')
    endwith
    endif
  endfunc

  add object oDESCAUC_4_27 as StdField with uid="AXBAGPTCQS",rtseq=101,rtrep=.f.,;
    cFormVar = "w_DESCAUC", cQueryName = "DESCAUC",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 59659318,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=262, Top=117, InputMask=replicate('X',35)


  add object oObj_4_28 as cp_calclbl with uid="SOYJLGTNDL",left=9, top=118, width=183,height=15,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",alignment=1,;
    nPag=4;
    , HelpContextID = 80863718

  add object oStr_4_17 as StdString with uid="TYHBKPGLEH",Visible=.t., Left=31, Top=64,;
    Alignment=1, Width=161, Height=18,;
    Caption="Causale cont. clienti:"  ;
  , bGlobalFont=.t.

  func oStr_4_17.mHide()
    with this.Parent.oContained
      return (.w_SCTIPGES='B')
    endwith
  endfunc

  add object oStr_4_18 as StdString with uid="FLIDIWZRAL",Visible=.t., Left=92, Top=170,;
    Alignment=1, Width=100, Height=18,;
    Caption="Conto D.C.attive:"  ;
  , bGlobalFont=.t.

  func oStr_4_18.mHide()
    with this.Parent.oContained
      return (.w_SCTIPGES='B')
    endwith
  endfunc

  add object oStr_4_21 as StdString with uid="JCTHOPHSCB",Visible=.t., Left=79, Top=197,;
    Alignment=1, Width=113, Height=18,;
    Caption="Conto D.C.passive:"  ;
  , bGlobalFont=.t.

  func oStr_4_21.mHide()
    with this.Parent.oContained
      return (.w_SCTIPGES='B')
    endwith
  endfunc

  add object oStr_4_24 as StdString with uid="XLAHRVALZT",Visible=.t., Left=78, Top=144,;
    Alignment=1, Width=114, Height=18,;
    Caption="Conto contropartita:"  ;
  , bGlobalFont=.t.

  func oStr_4_24.mHide()
    with this.Parent.oContained
      return (.w_SCTIPGES='B')
    endwith
  endfunc

  add object oStr_4_26 as StdString with uid="ABCHASYWBC",Visible=.t., Left=12, Top=91,;
    Alignment=1, Width=180, Height=18,;
    Caption="Causale cont. fornitori:"  ;
  , bGlobalFont=.t.

  func oStr_4_26.mHide()
    with this.Parent.oContained
      return (.w_SCTIPGES='B')
    endwith
  endfunc
enddefine

* --- Defining Body row
define class tgscg_mscBodyRow as CPBodyRowCnt
  Width=820
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_4 as StdTrsField with uid="DKDGXAGKFH",rtseq=66,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 251285354,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=50, Left=-2, Top=0, cSayPict=["9999"], cGetPict=["9999"]

  add object oDATSCA_2_6 as StdTrsField with uid="LOJLDNIXBC",rtseq=68,rtrep=.t.,;
    cFormVar="w_DATSCA",value=ctod("  /  /  "),enabled=.f.,;
    ToolTipText = "Data scadenza",;
    HelpContextID = 4300746,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=69, Left=49, Top=0

  add object oNUMPAR_2_7 as StdTrsField with uid="KOIQIXJJDE",rtseq=69,rtrep=.t.,;
    cFormVar="w_NUMPAR",value=space(31),enabled=.f.,;
    ToolTipText = "N.partita",;
    HelpContextID = 10159318,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=230, Left=119, Top=0, InputMask=replicate('X',31)

  add object oCODCON_2_8 as StdTrsField with uid="RBCGDDNWAX",rtseq=70,rtrep=.t.,;
    cFormVar="w_CODCON",value=space(15),enabled=.f.,;
    ToolTipText = "Codice conto",;
    HelpContextID = 43160026,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=118, Left=352, Top=0, InputMask=replicate('X',15), cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_PTTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCON"

  func oCODCON_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oNUMDOC_2_9 as StdTrsField with uid="VVVFRGKGHE",rtseq=71,rtrep=.t.,;
    cFormVar="w_NUMDOC",value=0,enabled=.f.,;
    ToolTipText = "Numero documento",;
    HelpContextID = 227605290,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=118, Left=471, Top=0

  add object oALFDOC_2_10 as StdTrsField with uid="PLHDLGTKXZ",rtseq=72,rtrep=.t.,;
    cFormVar="w_ALFDOC",value=space(10),enabled=.f.,;
    ToolTipText = "Serie documento",;
    HelpContextID = 227636474,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=83, Left=592, Top=0, InputMask=replicate('X',10)

  add object oTOTIMP_2_11 as StdTrsField with uid="GZEXSIRUDP",rtseq=73,rtrep=.t.,;
    cFormVar="w_TOTIMP",value=0,enabled=.f.,;
    ToolTipText = "Importo partita",;
    HelpContextID = 11243722,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=139, Left=676, Top=0, cSayPict=[v_PU(40+VVL)], cGetPict=[v_GU(40+VVL)]
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_4.When()
    return(.t.)
  proc oCPROWORD_2_4.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_4.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_msc','SALMDACO','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".SCSERIAL=SALMDACO.SCSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- gscg_msc
* --- Classe per bottone automatismi
* --- alla perdita del focus fa scattare
* --- il lancio del batch per creare il dettaglio
* --- in base all'eventuale automatismo

Define class Btn_Mpn as StdButton

 Proc LostFocus

  with this.Parent.oContained
   If .cFunction ='Load'
    This.Click()
   endif
  Endwith
  DoDefault()
 Endproc

Enddefine

Proc calcnumdoc()
  parameters padre
  if padre.w_FLPDOC#'N'
      padre.w_CONN = i_TableProp[padre.SALMDACO_IDX,3]
      cp_AskTableProg(padre,padre.w_CONN,"NDSALDAC","i_codazi,w_CAUSALE,w_SCCODESE,w_PNPRD,w_SERDOC,w_SCNUMDOC")
  else
      padre.w_SCNUMDOC=0
  endif 
endproc  
* --- Fine Area Manuale
