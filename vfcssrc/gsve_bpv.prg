* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bpv                                                        *
*              Stampa prospetto del venduto                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_408]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-08-03                                                      *
* Last revis.: 2009-07-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bpv",oParentObject)
return(i_retval)

define class tgsve_bpv as StdBatch
  * --- Local variables
  w_CAMBIOL = 0
  w_VALCOS = space(3)
  w_CAOCOS = 0
  w_CAOESE1 = 0
  w_CAOVAL = 0
  w_DECTOT = 0
  w_CAMBIO2L = 0
  w_VALU = space(3)
  w_SIMVAL = space(5)
  w_DATAIN = ctod("  /  /  ")
  w_DATAFIN = ctod("  /  /  ")
  w_FLVEAC = space(1)
  w_QTAMOV = 0
  w_COUNT = 0
  w_COSTO = 0
  w_SERIALE = space(10)
  w_RIFRIGA = 0
  w_ARTICOLO = space(20)
  w_SERVEN = space(10)
  w_ROWVEN = 0
  w_COSTOT = 0
  w_CLADOC = space(2)
  w_TIPRIG = space(1)
  w_TIPRIG1 = space(1)
  w_TEST = .f.
  w_CONTA_DT = 0
  w_CONTA_NC = 0
  w_APPO = space(10)
  w_oERRORLOG = .NULL.
  w_LCAMBIO = 0
  * --- WorkFile variables
  TMPVEND1_idx=0
  TMPVEND2_idx=0
  TMPVEND3_idx=0
  VALUTE_idx=0
  TMP_ACQU_idx=0
  GSVE67PV_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch elaborazione stampa prospetto del venduto lanciato dalla maschera  GSMA_SDV
    * --- Variabili Elaborazione LIFO\FIFO Puntuale
    if this.oParentObject.w_COSTMARG="S" and (this.oParentObject.w_AGGIORN<>"UCST" AND this.oParentObject.w_AGGIORN<>"LIST")
      if EMPTY(NVL(this.oParentObject.w_NUMERO,""))
        ah_ErrorMsg("Inserire numero inventario")
        i_retcode = 'stop'
        return
      endif
    endif
    if this.oParentObject.w_CAMBIO <> this.oParentObject.w_CAMBIO1
      ah_ErrorMsg("I cambi devono essere uguali")
      i_retcode = 'stop'
      return
    endif
    this.w_VALU = this.oParentObject.w_VALU2
    this.w_CAOESE1 = this.oParentObject.w_CAOESE
    this.w_CAOVAL = g_CAOVAL
    if this.oParentObject.w_CAOVAL1=0 AND this.oParentObject.w_costmarg="S" and this.oParentObject.w_Aggiorn="LIST"
      this.w_CAMBIOL = this.oParentObject.w_CAMBIO1
    else
      this.w_CAMBIOL = 1
    endif
    this.w_CAMBIO2L = this.oParentObject.w_CAMBIO2
    this.w_DECTOT = this.oParentObject.w_DECTOT2
    this.w_VALCOS = this.oParentObject.w_VALUTA
    this.w_CAOCOS = this.oParentObject.w_CAOVAL1
    this.w_DATAFIN = this.oParentObject.w_ADATA
    this.w_DATAIN = this.oParentObject.w_DADATA
    * --- Utilizzata per discriminare prospetto del Venduto da quello degli acquisti nelle query in comune
    this.w_FLVEAC = "V"
    * --- Creo la tabella temporanea TMPVEND1 dalla query GSVE50PV che prende i dati dalla tabella documenti.
    *     Nella query vengono gi� eseguiti i filtri sui documenti che devono essere presi in considerazione a seconda 
    *     dei check spuntati sulla maschera di stampa.
    *     Inoltre per i Documenti di Trasporto viene presa in considerazione la quantit� evasa e spuntato il flag evaso 
    *     solo per le righe totalmente evase.  Per gli altri documenti questi campi sono vuoti.
    *     Nel campo MVSPEACC vengono sommate:spese di trasporto, di incasso,di imballo e di bollo.
    *     Vengono anche predisposti due campi TOTDOC e TOTVEN che assumeranno i valori di totale documento
    *     e totale di riga con tutte le spese ripartite.
    ah_Msg("Seleziono i documenti")
    if this.oParentObject.w_SPEACC="S"
      * --- Query di lettura righe nel ceso di spese accessorie incluse. Calcola le spese accessorie ripartite sulle righe dei documenti. 
      * --- Create temporary table TMPVEND1
      i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('query\gsve50pv',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPVEND1_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    else
      * --- Query di lettura righe nel ceso di spese accessorie non incluse. Filtra anche i servizi accessori.
      * --- Create temporary table TMPVEND1
      i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('query\gsveb50p',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPVEND1_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    endif
    * --- Poich� i servizi accessori sono gi� stati sommati alle spese accessorie, li cancello.
    * --- Delete from TMPVEND1
    i_nConn=i_TableProp[this.TMPVEND1_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"ARTIPSER = "+cp_ToStrODBC("S");
             )
    else
      delete from (i_cTable) where;
            ARTIPSER = "S";

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- SE ho scelto come tipo di aggiornamento LISTINO:
    this.w_TIPRIG = IIF(this.oParentObject.w_AGGIORN="LIST", " ", "F")
    this.w_TIPRIG1 = IIF(this.oParentObject.w_AGGIORN="LIST", " ", "M")
    if this.oParentObject.w_COSTMARG="S"
      if NOT EMPTY(this.oParentObject.w_LISTINO)
        ah_Msg("Aggiornamento da listino")
        * --- Considerando solo i listini validi, per ogni riga vado a considerare gli scaglioni prendendo i prezzi.
        *     Predispongo anche un campo di appoggio NUMSCA che inizializzer� a 9999999999999 se quantit� scaglione
        *     � OLTRE (0) e alla differenza tra quantit� scaglione e MVQTAUM1 negli altri casi.
        *     Questo poich� mi interesser� solo lo scaglione con il NUMSCA minore fra i positivi 
        * --- Create temporary table TMPVEND2
        i_nIdx=cp_AddTableDef('TMPVEND2') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('query\gsve5pdv',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPVEND2_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- Da TMPVEND2 prendo solo i listini con data di attivazione pi� vicina a quella che si � scelta per l'elaborazione
        ah_Msg("Selezione listino valido")
        * --- Create temporary table TMPVEND3
        i_nIdx=cp_AddTableDef('TMPVEND3') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('query\gsve6pdv',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPVEND3_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- In TMPVEND2 per ogni riga dei documento con data attivazione uguale a quella di TMPVEND3
        *     e cio� quella pi� valida, vado a scrivere il prezzo presente sul listino
        *     su COSTO1(campo precedentemente inizializzato a 0)
        ah_Msg("Scrittura prezzo di listino")
        * --- Write into TMPVEND2
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMPVEND2_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND2_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPVEND3_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND2_idx,i_nConn)
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="TMPVEND2.MVSERIAL = _t2.MVSERIAL";
                  +" and "+"TMPVEND2.CPROWNUM = _t2.CPROWNUM";
                  +" and "+"TMPVEND2.DATATT = _t2.DATATT";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"COSTO1 ="+cp_NullLink(cp_ToStrODBC(987),'TMPVEND2','COSTO1');
              +i_ccchkf;
              +" from "+i_cTable+" TMPVEND2, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="TMPVEND2.MVSERIAL = _t2.MVSERIAL";
                  +" and "+"TMPVEND2.CPROWNUM = _t2.CPROWNUM";
                  +" and "+"TMPVEND2.DATATT = _t2.DATATT";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND2, "+i_cQueryTable+" _t2 set ";
          +"TMPVEND2.COSTO1 ="+cp_NullLink(cp_ToStrODBC(987),'TMPVEND2','COSTO1');
              +Iif(Empty(i_ccchkf),"",",TMPVEND2.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="PostgreSQL"
            i_cWhere="TMPVEND2.MVSERIAL = _t2.MVSERIAL";
                  +" and "+"TMPVEND2.CPROWNUM = _t2.CPROWNUM";
                  +" and "+"TMPVEND2.DATATT = _t2.DATATT";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND2 set ";
          +"COSTO1 ="+cp_NullLink(cp_ToStrODBC(987),'TMPVEND2','COSTO1');
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
                  +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                  +" and "+i_cTable+".DATATT = "+i_cQueryTable+".DATATT";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"COSTO1 ="+cp_NullLink(cp_ToStrODBC(987),'TMPVEND2','COSTO1');
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Elimino da TMPVEND2 tutte le righe che hanno COSTO1 = 0 poich� sono quelle che non hanno un listino valido
        *     e tutte quelle che hanno NUMSCA minore di 0  
        ah_Msg("Eliminazione righe vuote")
        * --- Delete from TMPVEND2
        i_nConn=i_TableProp[this.TMPVEND2_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND2_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"COSTO1 <> "+cp_ToStrODBC(987);
                 )
        else
          delete from (i_cTable) where;
                COSTO1 <> 987;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Da TMPVEND2 prendo tra le righe uguali quelle che hanno il NUMSCA minore 
        * --- Create temporary table TMPVEND3
        i_nIdx=cp_AddTableDef('TMPVEND3') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('query\gsve7pdv',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPVEND3_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- Scrivo in TMPVEND2 il COSTO della riga dove NUMSCA � uguale a quello di TMPVEND3
        *     precedentemente filtrato
        ah_Msg("Scrittura costo")
        * --- Write into TMPVEND2
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMPVEND2_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND2_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPVEND3_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND2_idx,i_nConn)
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="TMPVEND2.MVSERIAL = _t2.MVSERIAL";
                  +" and "+"TMPVEND2.CPROWNUM = _t2.CPROWNUM";
                  +" and "+"TMPVEND2.MVCODART = _t2.MVCODART";
                  +" and "+"TMPVEND2.NUMSCA = _t2.NUMSCA";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"COSTO1 ="+cp_NullLink(cp_ToStrODBC(789),'TMPVEND2','COSTO1');
              +i_ccchkf;
              +" from "+i_cTable+" TMPVEND2, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="TMPVEND2.MVSERIAL = _t2.MVSERIAL";
                  +" and "+"TMPVEND2.CPROWNUM = _t2.CPROWNUM";
                  +" and "+"TMPVEND2.MVCODART = _t2.MVCODART";
                  +" and "+"TMPVEND2.NUMSCA = _t2.NUMSCA";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND2, "+i_cQueryTable+" _t2 set ";
          +"TMPVEND2.COSTO1 ="+cp_NullLink(cp_ToStrODBC(789),'TMPVEND2','COSTO1');
              +Iif(Empty(i_ccchkf),"",",TMPVEND2.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="PostgreSQL"
            i_cWhere="TMPVEND2.MVSERIAL = _t2.MVSERIAL";
                  +" and "+"TMPVEND2.CPROWNUM = _t2.CPROWNUM";
                  +" and "+"TMPVEND2.MVCODART = _t2.MVCODART";
                  +" and "+"TMPVEND2.NUMSCA = _t2.NUMSCA";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND2 set ";
          +"COSTO1 ="+cp_NullLink(cp_ToStrODBC(789),'TMPVEND2','COSTO1');
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
                  +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                  +" and "+i_cTable+".MVCODART = "+i_cQueryTable+".MVCODART";
                  +" and "+i_cTable+".NUMSCA = "+i_cQueryTable+".NUMSCA";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"COSTO1 ="+cp_NullLink(cp_ToStrODBC(789),'TMPVEND2','COSTO1');
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Cancello ancora da TMPVEND2 le righe con COSTO = 0 poich� sono quelle che non ci interessano
        * --- Delete from TMPVEND2
        i_nConn=i_TableProp[this.TMPVEND2_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND2_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"COSTO1 <> "+cp_ToStrODBC(789);
                 )
        else
          delete from (i_cTable) where;
                COSTO1 <> 789;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Prendo i dati utili dalla TMPVEND2 insieme a quelli della TMPVEND1 e li memorizzo in TMPVEND3
        *     Metto in join le due tabelle scrivendo il COSTO, prezzo di listino, dove MVSERIAL e CPROWNUM sono uguali 
        * --- Create temporary table TMPVEND3
        i_nIdx=cp_AddTableDef('TMPVEND3') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('query\gsve8pdv',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPVEND3_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- Riscrivo esattamente la TMPVEND3 in TMPVEND1 con i dati definitivi
        ah_Msg("Elaborazione temporaneo")
        * --- Create temporary table TMPVEND1
        i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('query\gsve9pdv',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPVEND1_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- Write into TMPVEND1
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMPVEND1_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPVEND3_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND1_idx,i_nConn)
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="TMPVEND1.MVSERIAL = _t2.MVSERIAL";
                  +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                  +" and "+"TMPVEND1.MVCODART = _t2.MVCODART";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"COSTO = _t2.COSTO";
              +i_ccchkf;
              +" from "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="TMPVEND1.MVSERIAL = _t2.MVSERIAL";
                  +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                  +" and "+"TMPVEND1.MVCODART = _t2.MVCODART";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 set ";
              +"TMPVEND1.COSTO = _t2.COSTO";
              +Iif(Empty(i_ccchkf),"",",TMPVEND1.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="PostgreSQL"
            i_cWhere="TMPVEND1.MVSERIAL = _t2.MVSERIAL";
                  +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                  +" and "+"TMPVEND1.MVCODART = _t2.MVCODART";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1 set ";
              +"COSTO = _t2.COSTO";
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
                  +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                  +" and "+i_cTable+".MVCODART = "+i_cQueryTable+".MVCODART";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"COSTO = (select COSTO from "+i_cQueryTable+" where "+i_cWhere+")";
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      if this.oParentObject.w_AGGIORN<>"LIST"
        if this.oParentObject.w_AGGIORN="UCST"
          * --- Caso in cui viene scelto come valorizzazione di costi L'ultimo costo standard presente nella 
          *     anagrafica degli articoli
          ah_Msg("Valorizzazione costi da ultimo costo standard")
          * --- Create temporary table TMPVEND3
          i_nIdx=cp_AddTableDef('TMPVEND3') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('query\gsve11pv',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.TMPVEND3_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        else
          * --- Caso di scelta di valorizzazione dei costi attraverso inventario
          ah_Msg("Valorizzazione costi da inventario")
          * --- Create temporary table TMPVEND3
          i_nIdx=cp_AddTableDef('TMPVEND3') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('query\gsve10pv',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.TMPVEND3_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        endif
        ah_Msg("Scrittura dei costi nel temporaneo")
        * --- Ogni valorizzazione del costo viene tradotta in EURO
        do case
          case this.oParentObject.w_AGGIORN="CMPA"
            * --- Costo Medio Ponderato Annuo
            * --- Write into TMPVEND1
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TMPVEND1_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
              declare i_aIndex[1]
              i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPVEND3_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND1_idx,i_nConn)
              i_cDB=cp_GetDatabaseType(i_nConn)
              do case
              case i_cDB="SQLServer"
                i_cWhere="TMPVEND1.MVSERIAL = _t2.MVSERIAL";
                      +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"TMPVEND1.MVCODART = _t2.MVCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"COSTO = _t2.DICOSMPA";
                  +i_ccchkf;
                  +" from "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 where "+i_cWhere)
              case i_cDB="MySQL"
                i_cWhere="TMPVEND1.MVSERIAL = _t2.MVSERIAL";
                      +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"TMPVEND1.MVCODART = _t2.MVCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 set ";
                  +"TMPVEND1.COSTO = _t2.DICOSMPA";
                  +Iif(Empty(i_ccchkf),"",",TMPVEND1.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                  +" where "+i_cWhere)
              case i_cDB="PostgreSQL"
                i_cWhere="TMPVEND1.MVSERIAL = _t2.MVSERIAL";
                      +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"TMPVEND1.MVCODART = _t2.MVCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1 set ";
                  +"COSTO = _t2.DICOSMPA";
                  +i_ccchkf;
                  +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
              otherwise
                i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
                      +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                      +" and "+i_cTable+".MVCODART = "+i_cQueryTable+".MVCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"COSTO = (select DICOSMPA from "+i_cQueryTable+" where "+i_cWhere+")";
                  +i_ccchkf;
                  +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
              endcase
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          case this.oParentObject.w_AGGIORN="CMPP"
            * --- Costo Medio Ponderato Periodico
            * --- Write into TMPVEND1
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TMPVEND1_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
              declare i_aIndex[1]
              i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPVEND3_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND1_idx,i_nConn)
              i_cDB=cp_GetDatabaseType(i_nConn)
              do case
              case i_cDB="SQLServer"
                i_cWhere="TMPVEND1.MVSERIAL = _t2.MVSERIAL";
                      +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"TMPVEND1.MVCODART = _t2.MVCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"COSTO = _t2.DICOSMPP";
                  +i_ccchkf;
                  +" from "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 where "+i_cWhere)
              case i_cDB="MySQL"
                i_cWhere="TMPVEND1.MVSERIAL = _t2.MVSERIAL";
                      +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"TMPVEND1.MVCODART = _t2.MVCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 set ";
                  +"TMPVEND1.COSTO = _t2.DICOSMPP";
                  +Iif(Empty(i_ccchkf),"",",TMPVEND1.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                  +" where "+i_cWhere)
              case i_cDB="PostgreSQL"
                i_cWhere="TMPVEND1.MVSERIAL = _t2.MVSERIAL";
                      +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"TMPVEND1.MVCODART = _t2.MVCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1 set ";
                  +"COSTO = _t2.DICOSMPP";
                  +i_ccchkf;
                  +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
              otherwise
                i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
                      +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                      +" and "+i_cTable+".MVCODART = "+i_cQueryTable+".MVCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"COSTO = (select DICOSMPP from "+i_cQueryTable+" where "+i_cWhere+")";
                  +i_ccchkf;
                  +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
              endcase
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          case this.oParentObject.w_AGGIORN="COUL"
            * --- Costo Ultimo
            * --- Write into TMPVEND1
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TMPVEND1_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
              declare i_aIndex[1]
              i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPVEND3_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND1_idx,i_nConn)
              i_cDB=cp_GetDatabaseType(i_nConn)
              do case
              case i_cDB="SQLServer"
                i_cWhere="TMPVEND1.MVSERIAL = _t2.MVSERIAL";
                      +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"TMPVEND1.MVCODART = _t2.MVCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"COSTO = _t2.DICOSULT";
                  +i_ccchkf;
                  +" from "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 where "+i_cWhere)
              case i_cDB="MySQL"
                i_cWhere="TMPVEND1.MVSERIAL = _t2.MVSERIAL";
                      +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"TMPVEND1.MVCODART = _t2.MVCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 set ";
                  +"TMPVEND1.COSTO = _t2.DICOSULT";
                  +Iif(Empty(i_ccchkf),"",",TMPVEND1.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                  +" where "+i_cWhere)
              case i_cDB="PostgreSQL"
                i_cWhere="TMPVEND1.MVSERIAL = _t2.MVSERIAL";
                      +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"TMPVEND1.MVCODART = _t2.MVCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1 set ";
                  +"COSTO = _t2.DICOSULT";
                  +i_ccchkf;
                  +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
              otherwise
                i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
                      +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                      +" and "+i_cTable+".MVCODART = "+i_cQueryTable+".MVCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"COSTO = (select DICOSULT from "+i_cQueryTable+" where "+i_cWhere+")";
                  +i_ccchkf;
                  +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
              endcase
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          case this.oParentObject.w_AGGIORN="COST"
            * --- Costo Standard
            * --- Write into TMPVEND1
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TMPVEND1_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
              declare i_aIndex[1]
              i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPVEND3_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND1_idx,i_nConn)
              i_cDB=cp_GetDatabaseType(i_nConn)
              do case
              case i_cDB="SQLServer"
                i_cWhere="TMPVEND1.MVSERIAL = _t2.MVSERIAL";
                      +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"TMPVEND1.MVCODART = _t2.MVCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"COSTO = _t2.DICOSSTA";
                  +i_ccchkf;
                  +" from "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 where "+i_cWhere)
              case i_cDB="MySQL"
                i_cWhere="TMPVEND1.MVSERIAL = _t2.MVSERIAL";
                      +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"TMPVEND1.MVCODART = _t2.MVCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 set ";
                  +"TMPVEND1.COSTO = _t2.DICOSSTA";
                  +Iif(Empty(i_ccchkf),"",",TMPVEND1.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                  +" where "+i_cWhere)
              case i_cDB="PostgreSQL"
                i_cWhere="TMPVEND1.MVSERIAL = _t2.MVSERIAL";
                      +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"TMPVEND1.MVCODART = _t2.MVCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1 set ";
                  +"COSTO = _t2.DICOSSTA";
                  +i_ccchkf;
                  +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
              otherwise
                i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
                      +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                      +" and "+i_cTable+".MVCODART = "+i_cQueryTable+".MVCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"COSTO = (select DICOSSTA from "+i_cQueryTable+" where "+i_cWhere+")";
                  +i_ccchkf;
                  +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
              endcase
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          case this.oParentObject.w_AGGIORN="LICO"
            * --- Lifo Continuo
            if this.oParentObject.w_FLPUNT="S"
              * --- Elaborazione Lifo Continuo Puntuale
              * --- Create temporary table TMP_ACQU
              i_nIdx=cp_AddTableDef('TMP_ACQU') && aggiunge la definizione nella lista delle tabelle
              i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
              vq_exec('query\gsve_qlp',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
              this.TMP_ACQU_idx=i_nIdx
              i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
              this.Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            else
              * --- Write into TMPVEND1
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.TMPVEND1_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
              if i_nConn<>0
                local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
                declare i_aIndex[1]
                i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPVEND3_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND1_idx,i_nConn)
                i_cDB=cp_GetDatabaseType(i_nConn)
                do case
                case i_cDB="SQLServer"
                  i_cWhere="TMPVEND1.MVSERIAL = _t2.MVSERIAL";
                        +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                        +" and "+"TMPVEND1.MVCODART = _t2.MVCODART";
              
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"COSTO = _t2.DICOSLCO";
                    +i_ccchkf;
                    +" from "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 where "+i_cWhere)
                case i_cDB="MySQL"
                  i_cWhere="TMPVEND1.MVSERIAL = _t2.MVSERIAL";
                        +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                        +" and "+"TMPVEND1.MVCODART = _t2.MVCODART";
              
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 set ";
                    +"TMPVEND1.COSTO = _t2.DICOSLCO";
                    +Iif(Empty(i_ccchkf),"",",TMPVEND1.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                    +" where "+i_cWhere)
                case i_cDB="PostgreSQL"
                  i_cWhere="TMPVEND1.MVSERIAL = _t2.MVSERIAL";
                        +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                        +" and "+"TMPVEND1.MVCODART = _t2.MVCODART";
              
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1 set ";
                    +"COSTO = _t2.DICOSLCO";
                    +i_ccchkf;
                    +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
                otherwise
                  i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
                        +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                        +" and "+i_cTable+".MVCODART = "+i_cQueryTable+".MVCODART";
              
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"COSTO = (select DICOSLCO from "+i_cQueryTable+" where "+i_cWhere+")";
                    +i_ccchkf;
                    +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
                endcase
              else
                error "not yet implemented!"
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
          case this.oParentObject.w_AGGIORN="FICO"
            * --- Fifo Continuo
            if this.oParentObject.w_FLPUNT="S"
              * --- Elaborazione Fifo Continuo Puntuale
              * --- Create temporary table TMP_ACQU
              i_nIdx=cp_AddTableDef('TMP_ACQU') && aggiunge la definizione nella lista delle tabelle
              i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
              vq_exec('query\gsve_qfp',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
              this.TMP_ACQU_idx=i_nIdx
              i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
              this.Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            else
              * --- Write into TMPVEND1
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.TMPVEND1_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
              if i_nConn<>0
                local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
                declare i_aIndex[1]
                i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPVEND3_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND1_idx,i_nConn)
                i_cDB=cp_GetDatabaseType(i_nConn)
                do case
                case i_cDB="SQLServer"
                  i_cWhere="TMPVEND1.MVSERIAL = _t2.MVSERIAL";
                        +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                        +" and "+"TMPVEND1.MVCODART = _t2.MVCODART";
              
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"COSTO = _t2.DICOSFCO";
                    +i_ccchkf;
                    +" from "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 where "+i_cWhere)
                case i_cDB="MySQL"
                  i_cWhere="TMPVEND1.MVSERIAL = _t2.MVSERIAL";
                        +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                        +" and "+"TMPVEND1.MVCODART = _t2.MVCODART";
              
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 set ";
                    +"TMPVEND1.COSTO = _t2.DICOSFCO";
                    +Iif(Empty(i_ccchkf),"",",TMPVEND1.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                    +" where "+i_cWhere)
                case i_cDB="PostgreSQL"
                  i_cWhere="TMPVEND1.MVSERIAL = _t2.MVSERIAL";
                        +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                        +" and "+"TMPVEND1.MVCODART = _t2.MVCODART";
              
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1 set ";
                    +"COSTO = _t2.DICOSFCO";
                    +i_ccchkf;
                    +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
                otherwise
                  i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
                        +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                        +" and "+i_cTable+".MVCODART = "+i_cQueryTable+".MVCODART";
              
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"COSTO = (select DICOSFCO from "+i_cQueryTable+" where "+i_cWhere+")";
                    +i_ccchkf;
                    +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
                endcase
              else
                error "not yet implemented!"
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
          case this.oParentObject.w_AGGIORN="LISC"
            * --- Lifo a Scatti
            * --- Write into TMPVEND1
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TMPVEND1_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
              declare i_aIndex[1]
              i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPVEND3_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND1_idx,i_nConn)
              i_cDB=cp_GetDatabaseType(i_nConn)
              do case
              case i_cDB="SQLServer"
                i_cWhere="TMPVEND1.MVSERIAL = _t2.MVSERIAL";
                      +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"TMPVEND1.MVCODART = _t2.MVCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"COSTO = _t2.DICOSLSC";
                  +i_ccchkf;
                  +" from "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 where "+i_cWhere)
              case i_cDB="MySQL"
                i_cWhere="TMPVEND1.MVSERIAL = _t2.MVSERIAL";
                      +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"TMPVEND1.MVCODART = _t2.MVCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 set ";
                  +"TMPVEND1.COSTO = _t2.DICOSLSC";
                  +Iif(Empty(i_ccchkf),"",",TMPVEND1.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                  +" where "+i_cWhere)
              case i_cDB="PostgreSQL"
                i_cWhere="TMPVEND1.MVSERIAL = _t2.MVSERIAL";
                      +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"TMPVEND1.MVCODART = _t2.MVCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1 set ";
                  +"COSTO = _t2.DICOSLSC";
                  +i_ccchkf;
                  +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
              otherwise
                i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
                      +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                      +" and "+i_cTable+".MVCODART = "+i_cQueryTable+".MVCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"COSTO = (select DICOSLSC from "+i_cQueryTable+" where "+i_cWhere+")";
                  +i_ccchkf;
                  +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
              endcase
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          case this.oParentObject.w_AGGIORN="UCST"
            * --- Ultimo Costo Standard
            * --- Write into TMPVEND1
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TMPVEND1_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
              declare i_aIndex[1]
              i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPVEND3_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND1_idx,i_nConn)
              i_cDB=cp_GetDatabaseType(i_nConn)
              do case
              case i_cDB="SQLServer"
                i_cWhere="TMPVEND1.MVSERIAL = _t2.MVSERIAL";
                      +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"TMPVEND1.MVCODART = _t2.MVCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"COSTO = _t2.PRCOSSTA";
                  +i_ccchkf;
                  +" from "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 where "+i_cWhere)
              case i_cDB="MySQL"
                i_cWhere="TMPVEND1.MVSERIAL = _t2.MVSERIAL";
                      +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"TMPVEND1.MVCODART = _t2.MVCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 set ";
                  +"TMPVEND1.COSTO = _t2.PRCOSSTA";
                  +Iif(Empty(i_ccchkf),"",",TMPVEND1.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                  +" where "+i_cWhere)
              case i_cDB="PostgreSQL"
                i_cWhere="TMPVEND1.MVSERIAL = _t2.MVSERIAL";
                      +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"TMPVEND1.MVCODART = _t2.MVCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1 set ";
                  +"COSTO = _t2.PRCOSSTA";
                  +i_ccchkf;
                  +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
              otherwise
                i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
                      +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                      +" and "+i_cTable+".MVCODART = "+i_cQueryTable+".MVCODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"COSTO = (select PRCOSSTA from "+i_cQueryTable+" where "+i_cWhere+")";
                  +i_ccchkf;
                  +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
              endcase
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
        endcase
      endif
    endif
    * --- Nel caso di Note di credito metto il venduto e il costo in negativo! Inoltre converto i valori in EURO
    * --- Nel caso di DDT moltiplico il costo per la quantit� ancora da evadere.
    * --- Eseguo la conversione dei valori nel caso in cui scelgo come valuta finale la moneta di conto
    * --- Create temporary table GSVE67PV
    i_nIdx=cp_AddTableDef('GSVE67PV') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('GSVE67PV',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.GSVE67PV_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Write into TMPVEND1
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPVEND1_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.GSVE67PV_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND1_idx,i_nConn)
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMPVEND1.MVSERIAL = _t2.MVSERIAL";
              +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
              +" and "+"TMPVEND1.MVCODART = _t2.MVCODART";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"TOTVEN = _t2.TOTVEN ";
          +",COSTO = _t2.COSTO";
          +i_ccchkf;
          +" from "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMPVEND1.MVSERIAL = _t2.MVSERIAL";
              +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
              +" and "+"TMPVEND1.MVCODART = _t2.MVCODART";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 set ";
          +"TMPVEND1.TOTVEN = _t2.TOTVEN ";
          +",TMPVEND1.COSTO = _t2.COSTO";
          +Iif(Empty(i_ccchkf),"",",TMPVEND1.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="PostgreSQL"
        i_cWhere="TMPVEND1.MVSERIAL = _t2.MVSERIAL";
              +" and "+"TMPVEND1.CPROWNUM = _t2.CPROWNUM";
              +" and "+"TMPVEND1.MVCODART = _t2.MVCODART";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1 set ";
          +"TOTVEN = _t2.TOTVEN ";
          +",COSTO = _t2.COSTO";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
              +" and "+i_cTable+".MVCODART = "+i_cQueryTable+".MVCODART";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"TOTVEN = (select TOTVEN  from "+i_cQueryTable+" where "+i_cWhere+")";
          +",COSTO = (select COSTO from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Delete from TMPVEND1
    i_nConn=i_TableProp[this.TMPVEND1_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"MVFLEVAS = "+cp_ToStrODBC("S");
             )
    else
      delete from (i_cTable) where;
            MVFLEVAS = "S";

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Arrotondo i totali ai decimali della valuta
    * --- Create temporary table TMPVEND2
    i_nIdx=cp_AddTableDef('TMPVEND2') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('query\gsve55pv',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPVEND2_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Metto in negativo i Documenti di trasporto di reso da Cliente........
    * --- Create temporary table TMPVEND1
    i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('query\gsveb56p',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPVEND1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- ......e riporto tutto in TMPVEND1 per la stampa.
    * --- In caso di Note di credito con causale Magazzino con variazione di Valore attiva,
    *     azzero le quantit� ed il costo. Prendo in considerazione il solo valore
    * --- Write into TMPVEND1
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPVEND1_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND1_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"COSTO ="+cp_NullLink(cp_ToStrODBC(0),'TMPVEND1','COSTO');
      +",MVQTAEV1 ="+cp_NullLink(cp_ToStrODBC(0),'TMPVEND1','MVQTAEV1');
      +",MVQTAUM1 ="+cp_NullLink(cp_ToStrODBC(0),'TMPVEND1','MVQTAUM1');
          +i_ccchkf ;
      +" where ";
          +"MVCLADOC = "+cp_ToStrODBC("NC");
          +" and CMVARVAL <> "+cp_ToStrODBC("N");
             )
    else
      update (i_cTable) set;
          COSTO = 0;
          ,MVQTAEV1 = 0;
          ,MVQTAUM1 = 0;
          &i_ccchkf. ;
       where;
          MVCLADOC = "NC";
          and CMVARVAL <> "N";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if this.oParentObject.w_OQRY="..\INFO\EXE\QUERY\GSIN_BPV.VQR"
      i_retcode = 'stop'
      return
    else
      * --- Nel caso in cui non lancio il batch da Inforeader ed ho attivato il flag Stampa Log
      if this.oParentObject.w_STALOG="S"
        this.w_CONTA_DT = 0
        this.w_CONTA_NC = 0
        this.w_TEST = .T.
        vq_exec("gsve_bpv",this,"ERRORI")
        * --- Eseguo la stampa solo se ci sono DDT di Reso da cliente aperti e Note di Credito slegate dal ciclo documentale.
        *     Questi possono essere causa di problemi
         
 Select ERRORI 
 Go Top 
 COUNT FOR MVCLADOC = "DT" TO this.w_CONTA_DT 
 Go Top 
 COUNT FOR MVCLADOC = "NC" TO this.w_CONTA_NC
        if this.w_CONTA_DT >0 And this.w_CONTA_NC >0
          if Reccount("ERRORI")>0 And ah_YesNo("Stampo log dei documenti incongruenti?")
            * --- Stanzia l'oggetto per la gestione delle messaggistiche di errore
            this.w_oERRORLOG=createobject("AH_ErrorLog")
            this.w_oERRORLOG.AddMsgLog("Note di credito che non evadono documenti di carico oppure sono slegate dal ciclo documentale")     
             
 Select ERRORI 
 Go Top 
 Scan
            if MVCLADOC = "DT" And this.w_TEST
              * --- Quando incontro il primo DDT inserisco la descrizione
              this.w_oERRORLOG.AddMsgLogNoTranslate(Chr(13) + Repl("-",60) + CHR(13))     
              this.w_oERRORLOG.AddMsgLog("Documenti di trasporto di reso ancora da evadere")     
              this.w_TEST = .F.
            endif
            * --- Messaggio di Errore 
            this.w_oERRORLOG.AddMsgLog("Doc.: %1 n. %2 del %3%0Intestato a %4", Alltrim(MVTIPDOC), Alltrim(Str(MVNUMDOC))+ IIF(Not Empty(MVALFDOC),"/"+Alltrim(MVALFDOC),""), Alltrim(DTOC(CP_TODATE(MVDATDOC))), Alltrim(MVCODCON) + " "+ Alltrim(ANDESCRI) )     
             
 EndScan
            this.w_oERRORLOG.PrintLog(this.oParentObject,"Errori riscontrati",.F.)     
            if USED("__TMP__")
               
 Select __TMP__ 
 Use
            endif
          endif
        endif
        if USED("ERRORI")
           
 Select ERRORI 
 Use
        endif
      endif
    endif
    * --- Lettura della descrizione della valuta per la stampa e passaggio delle selezioni di stampa
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VASIMVAL"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_VALU);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VASIMVAL;
        from (i_cTable) where;
            VACODVAL = this.w_VALU;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_SIMVAL = NVL(cp_ToDate(_read_.VASIMVAL),cp_NullValue(_read_.VASIMVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Select from GSVETBPV
    do vq_exec with 'GSVETBPV',this,'_Curs_GSVETBPV','',.f.,.t.
    if used('_Curs_GSVETBPV')
      select _Curs_GSVETBPV
      locate for 1=1
      do while not(eof())
      L_TOTALE=_Curs_GSVETBPV.TOTALE
        select _Curs_GSVETBPV
        continue
      enddo
      use
    endif
    L_AGGIORN=this.oParentObject.w_AGGIORN
    L_STATO=this.oParentObject.w_STATO
    L_CLIENTE=this.oParentObject.w_CLIENTE
    L_CODART=this.oParentObject.w_CODART
    L_CODART1=this.oParentObject.w_CODART1
    L_GRUMER=this.oParentObject.w_GRUMER
    L_AGENTE=this.oParentObject.w_AGENTE
    L_ZONA=this.oParentObject.w_ZONA
    L_PAGAM=this.oParentObject.w_PAGAM
    L_STATO=this.oParentObject.w_STATO
    L_CATCOMM=this.oParentObject.w_CATCOMM
    L_LISTINO=this.oParentObject.w_LISTINO
    L_NUMERO=this.oParentObject.w_NUMERO
    L_COSTMARG=this.oParentObject.w_COSTMARG
    L_DECTOT2=this.oParentObject.w_DECTOT2
    L_SIMVAL=this.w_SIMVAL
    L_VALU=this.w_VALU
    L_CAMBIO=this.w_CAMBIO2L
    L_DADATA=this.w_DATAIN
    L_ADATA=this.w_DATAFIN
    L_SPEACC=this.oParentObject.w_SPEACC
    L_FLPUNT=this.oParentObject.w_FLPUNT
    L_CODFAM=this.oParentObject.w_CODFAM
    L_CODMAR=this.oParentObject.w_CODMAR
    L_CODCAT=this.oParentObject.w_CODCAT
    L_CODVET = this.oParentObject.w_CODVET
    L_DATSTA=this.oParentObject.w_DATSTA
    * --- Eseguo la query per la Stampa
    ah_Msg("Stampa in corso...")
    * --- Lancio la stampa
    VX_EXEC(""+ALLTRIM(this.oParentObject.w_OQRY)+", "+ALLTRIM(this.oParentObject.w_OREP)+"",THIS)
    * --- Drop temporary table TMPVEND1
    i_nIdx=cp_GetTableDefIdx('TMPVEND1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPVEND1')
    endif
    * --- Drop temporary table TMPVEND2
    i_nIdx=cp_GetTableDefIdx('TMPVEND2')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPVEND2')
    endif
    * --- Drop temporary table TMPVEND3
    i_nIdx=cp_GetTableDefIdx('TMPVEND3')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPVEND3')
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_QTAMOV = 0
    ah_Msg("Elaborazione LIFO\FIFO puntuale")
    vq_exec(alltrim("QUERY\GSVE3QLP"),this,"TMPCSM")
    Select TMPCSM
    Go top
    scan
    * --- Write into TMP_ACQU
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMP_ACQU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_ACQU_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_ACQU_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CSMEDIO ="+cp_NullLink(cp_ToStrODBC(TMPCSM.MEDIA),'TMP_ACQU','CSMEDIO');
          +i_ccchkf ;
      +" where ";
          +"ARTICOLO = "+cp_ToStrODBC(TMPCSM.CODART);
             )
    else
      update (i_cTable) set;
          CSMEDIO = TMPCSM.MEDIA;
          &i_ccchkf. ;
       where;
          ARTICOLO = TMPCSM.CODART;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    endscan
    if USED("TMPCSM")
      SELECT TMPCSM
      USE
    endif
    vq_exec(alltrim("QUERY\GSVE4QLP"),this,"TMPCSM")
    * --- Select from TMPVEND1
    i_nConn=i_TableProp[this.TMPVEND1_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2],.t.,this.TMPVEND1_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMPVEND1 ";
          +" order by MVDATDOC";
           ,"_Curs_TMPVEND1")
    else
      select * from (i_cTable);
       order by MVDATDOC;
        into cursor _Curs_TMPVEND1
    endif
    if used('_Curs_TMPVEND1')
      select _Curs_TMPVEND1
      locate for 1=1
      do while not(eof())
      this.w_COSTOT = 0
      this.w_COSTO = 0
      this.w_COUNT = 0
      * --- Devo anche eseguire la conversione in Euro nel caso ho 0 (possibili documenti importati)
      *     considero Euro e quindi cambio 1
      this.w_LCAMBIO = NVL(_Curs_TMPVEND1.MVCAOVAL,1)
      this.w_LCAMBIO = IIF(this.w_LCAMBIO=0,1,this.w_LCAMBIO)
      this.w_ARTICOLO = NVL(_Curs_TMPVEND1.MVCODART,SPACE(20))
      this.w_QTAMOV = NVL(_Curs_TMPVEND1.MVQTAUM1,0) - NVL(_Curs_TMPVEND1.MVQTAEV1,0)
      this.w_SERVEN = _Curs_TMPVEND1.MVSERIAL
      this.w_ROWVEN = _Curs_TMPVEND1.CPROWNUM
      this.w_CLADOC = IIF(NVL(_Curs_TMPVEND1.CMFLCASC,"") $ "+-",_Curs_TMPVEND1.MVCLADOC,"@@")
      * --- Nel caso di Note di Credito non consumo scaglioni...
      if _Curs_TMPVEND1.MVCLADOC<>"NC"
        if this.w_QTAMOV <>0
          if this.oParentObject.w_AGGIORN="LICO"
            * --- Select from TMP_ACQU
            i_nConn=i_TableProp[this.TMP_ACQU_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMP_ACQU_idx,2],.t.,this.TMP_ACQU_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMP_ACQU ";
                  +" where ARTICOLO= "+cp_ToStrODBC(this.w_ARTICOLO)+"";
                  +" order by DATREG desc,SERIALE desc,RIFRIGA desc";
                   ,"_Curs_TMP_ACQU")
            else
              select * from (i_cTable);
               where ARTICOLO= this.w_ARTICOLO;
               order by DATREG desc,SERIALE desc,RIFRIGA desc;
                into cursor _Curs_TMP_ACQU
            endif
            if used('_Curs_TMP_ACQU')
              select _Curs_TMP_ACQU
              locate for 1=1
              do while not(eof())
              this.w_SERIALE = _Curs_TMP_ACQU.SERIALE
              this.w_RIFRIGA = _Curs_TMP_ACQU.RIFRIGA
              if this.w_QTAMOV<>0
                do case
                  case this.w_QTAMOV >= NVL(_Curs_TMP_ACQU.QTAINACQ,0)
                    this.w_QTAMOV = this.w_QTAMOV- NVL(_Curs_TMP_ACQU.QTAINACQ,0)
                    if _Curs_TMP_ACQU.TESTAGG<>"NN"
                      this.w_COSTO = this.w_COSTO+(NVL(_Curs_TMP_ACQU.COSINACQ,0)*NVL(_Curs_TMP_ACQU.QTAINACQ,0))
                      UPDATE TMPCSM SET COSTO=COSTO+NVL(_Curs_TMP_ACQU.COSINACQ,0)*NVL(_Curs_TMP_ACQU.QTAINACQ,0) ,; 
 QTA=QTA+NVL(_Curs_TMP_ACQU.QTAINACQ,0) WHERE MVCODART=this.w_ARTICOLO
                    else
                      this.w_COSTO = this.w_COSTO+(NVL(_Curs_TMP_ACQU.CSMEDIO,0)*NVL(_Curs_TMP_ACQU.QTAINACQ,0))
                    endif
                    this.w_COUNT = this.w_COUNT+NVL(_Curs_TMP_ACQU.QTAINACQ,0)
                    * --- Delete from TMP_ACQU
                    i_nConn=i_TableProp[this.TMP_ACQU_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.TMP_ACQU_idx,2])
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    if i_nConn<>0
                      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                            +"SERIALE = "+cp_ToStrODBC(this.w_SERIALE);
                            +" and ARTICOLO = "+cp_ToStrODBC(this.w_ARTICOLO);
                            +" and RIFRIGA = "+cp_ToStrODBC(this.w_RIFRIGA);
                             )
                    else
                      delete from (i_cTable) where;
                            SERIALE = this.w_SERIALE;
                            and ARTICOLO = this.w_ARTICOLO;
                            and RIFRIGA = this.w_RIFRIGA;

                      i_Rows=_tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      * --- Error: delete not accepted
                      i_Error=MSG_DELETE_ERROR
                      return
                    endif
                  case this.w_QTAMOV < NVL(_Curs_TMP_ACQU.QTAINACQ,0)
                    if _Curs_TMP_ACQU.TESTAGG<>"NN"
                      this.w_COSTO = this.w_COSTO+(NVL(_Curs_TMP_ACQU.COSINACQ,0)*this.w_QTAMOV)
                      UPDATE TMPCSM SET COSTO=COSTO+NVL(_Curs_TMP_ACQU.COSINACQ,0)*NVL(_Curs_TMP_ACQU.QTAINACQ,0) ,; 
 QTA=QTA+NVL(_Curs_TMP_ACQU.QTAINACQ,0) WHERE MVCODART=this.w_ARTICOLO
                    else
                      this.w_COSTO = this.w_COSTO+(NVL(_Curs_TMP_ACQU.CSMEDIO,0)*this.w_QTAMOV)
                    endif
                    this.w_COUNT = this.w_COUNT+this.w_QTAMOV
                    * --- Write into TMP_ACQU
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.TMP_ACQU_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.TMP_ACQU_idx,2])
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_ACQU_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"QTAINACQ =QTAINACQ- "+cp_ToStrODBC(this.w_QTAMOV);
                          +i_ccchkf ;
                      +" where ";
                          +"SERIALE = "+cp_ToStrODBC(this.w_SERIALE);
                          +" and ARTICOLO = "+cp_ToStrODBC(this.w_ARTICOLO);
                          +" and RIFRIGA = "+cp_ToStrODBC(this.w_RIFRIGA);
                             )
                    else
                      update (i_cTable) set;
                          QTAINACQ = QTAINACQ - this.w_QTAMOV;
                          &i_ccchkf. ;
                       where;
                          SERIALE = this.w_SERIALE;
                          and ARTICOLO = this.w_ARTICOLO;
                          and RIFRIGA = this.w_RIFRIGA;

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error=MSG_WRITE_ERROR
                      return
                    endif
                    this.w_QTAMOV = 0
                endcase
              endif
                select _Curs_TMP_ACQU
                continue
              enddo
              use
            endif
          else
            * --- Select from TMP_ACQU
            i_nConn=i_TableProp[this.TMP_ACQU_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMP_ACQU_idx,2],.t.,this.TMP_ACQU_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMP_ACQU ";
                  +" where ARTICOLO= "+cp_ToStrODBC(this.w_ARTICOLO)+"";
                  +" order by DATREG,SERIALE,RIFRIGA";
                   ,"_Curs_TMP_ACQU")
            else
              select * from (i_cTable);
               where ARTICOLO= this.w_ARTICOLO;
               order by DATREG,SERIALE,RIFRIGA;
                into cursor _Curs_TMP_ACQU
            endif
            if used('_Curs_TMP_ACQU')
              select _Curs_TMP_ACQU
              locate for 1=1
              do while not(eof())
              this.w_SERIALE = _Curs_TMP_ACQU.SERIALE
              this.w_RIFRIGA = _Curs_TMP_ACQU.RIFRIGA
              if this.w_QTAMOV<>0
                do case
                  case this.w_QTAMOV >= NVL(_Curs_TMP_ACQU.QTAINACQ,0)
                    this.w_QTAMOV = this.w_QTAMOV- NVL(_Curs_TMP_ACQU.QTAINACQ,0)
                    if _Curs_TMP_ACQU.TESTAGG<>"NN"
                      this.w_COSTO = this.w_COSTO+(NVL(_Curs_TMP_ACQU.COSINACQ,0)*NVL(_Curs_TMP_ACQU.QTAINACQ,0))
                      UPDATE TMPCSM SET COSTO=COSTO+NVL(_Curs_TMP_ACQU.COSINACQ,0)*NVL(_Curs_TMP_ACQU.QTAINACQ,0) ,; 
 QTA=QTA+NVL(_Curs_TMP_ACQU.QTAINACQ,0) WHERE MVCODART=this.w_ARTICOLO
                    else
                      this.w_COSTO = this.w_COSTO+(NVL(_Curs_TMP_ACQU.CSMEDIO,0)*NVL(_Curs_TMP_ACQU.QTAINACQ,0))
                    endif
                    this.w_COUNT = this.w_COUNT+NVL(_Curs_TMP_ACQU.QTAINACQ,0)
                    * --- Delete from TMP_ACQU
                    i_nConn=i_TableProp[this.TMP_ACQU_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.TMP_ACQU_idx,2])
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    if i_nConn<>0
                      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                            +"SERIALE = "+cp_ToStrODBC(this.w_SERIALE);
                            +" and ARTICOLO = "+cp_ToStrODBC(this.w_ARTICOLO);
                            +" and RIFRIGA = "+cp_ToStrODBC(this.w_RIFRIGA);
                             )
                    else
                      delete from (i_cTable) where;
                            SERIALE = this.w_SERIALE;
                            and ARTICOLO = this.w_ARTICOLO;
                            and RIFRIGA = this.w_RIFRIGA;

                      i_Rows=_tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      * --- Error: delete not accepted
                      i_Error=MSG_DELETE_ERROR
                      return
                    endif
                  case this.w_QTAMOV < NVL(_Curs_TMP_ACQU.QTAINACQ,0)
                    if _Curs_TMP_ACQU.TESTAGG<>"NN"
                      this.w_COSTO = this.w_COSTO+(NVL(_Curs_TMP_ACQU.COSINACQ,0)*this.w_QTAMOV)
                      UPDATE TMPCSM SET COSTO=COSTO+NVL(_Curs_TMP_ACQU.COSINACQ,0)*NVL(_Curs_TMP_ACQU.QTAINACQ,0) ,; 
 QTA=QTA+NVL(_Curs_TMP_ACQU.QTAINACQ,0) WHERE MVCODART=this.w_ARTICOLO
                    else
                      this.w_COSTO = this.w_COSTO+(NVL(_Curs_TMP_ACQU.CSMEDIO,0)*this.w_QTAMOV)
                    endif
                    this.w_COUNT = this.w_COUNT+this.w_QTAMOV
                    * --- Write into TMP_ACQU
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.TMP_ACQU_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.TMP_ACQU_idx,2])
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_ACQU_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"QTAINACQ =QTAINACQ- "+cp_ToStrODBC(this.w_QTAMOV);
                          +i_ccchkf ;
                      +" where ";
                          +"SERIALE = "+cp_ToStrODBC(this.w_SERIALE);
                          +" and ARTICOLO = "+cp_ToStrODBC(this.w_ARTICOLO);
                          +" and RIFRIGA = "+cp_ToStrODBC(this.w_RIFRIGA);
                             )
                    else
                      update (i_cTable) set;
                          QTAINACQ = QTAINACQ - this.w_QTAMOV;
                          &i_ccchkf. ;
                       where;
                          SERIALE = this.w_SERIALE;
                          and ARTICOLO = this.w_ARTICOLO;
                          and RIFRIGA = this.w_RIFRIGA;

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error=MSG_WRITE_ERROR
                      return
                    endif
                    this.w_QTAMOV = 0
                endcase
              endif
                select _Curs_TMP_ACQU
                continue
              enddo
              use
            endif
          endif
          if this.w_COUNT<>0
            this.w_COSTO = this.w_COSTO/this.w_COUNT
            * --- Write into TMPVEND1
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TMPVEND1_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND1_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"COSTO ="+cp_NullLink(cp_ToStrODBC(this.w_COSTO),'TMPVEND1','COSTO');
                  +i_ccchkf ;
              +" where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.w_SERVEN);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWVEN);
                     )
            else
              update (i_cTable) set;
                  COSTO = this.w_COSTO;
                  &i_ccchkf. ;
               where;
                  MVSERIAL = this.w_SERVEN;
                  and CPROWNUM = this.w_ROWVEN;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        endif
      else
        * --- Costifico le Note di Credito che aggiornano l'esistenza con la media dei costi fino adesso processati.
        SELECT TMPCSM
        GO TOP
        LOCATE FOR MVCODART=this.w_ARTICOLO AND QTA<>0
        if FOUND()
          this.w_COSTOT = COSTO/QTA*(-1)
          * --- Write into TMPVEND1
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TMPVEND1_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND1_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"COSTO ="+cp_NullLink(cp_ToStrODBC(this.w_COSTOT),'TMPVEND1','COSTO');
                +i_ccchkf ;
            +" where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_SERVEN);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWVEN);
                +" and MVCLADOC = "+cp_ToStrODBC(this.w_CLADOC);
                   )
          else
            update (i_cTable) set;
                COSTO = this.w_COSTOT;
                &i_ccchkf. ;
             where;
                MVSERIAL = this.w_SERVEN;
                and CPROWNUM = this.w_ROWVEN;
                and MVCLADOC = this.w_CLADOC;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
        select _Curs_TMPVEND1
        continue
      enddo
      use
    endif
    if USED("TMPCSM")
      SELECT TMPCSM
      USE
    endif
    * --- Drop temporary table TMP_ACQU
    i_nIdx=cp_GetTableDefIdx('TMP_ACQU')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_ACQU')
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='*TMPVEND1'
    this.cWorkTables[2]='*TMPVEND2'
    this.cWorkTables[3]='*TMPVEND3'
    this.cWorkTables[4]='VALUTE'
    this.cWorkTables[5]='*TMP_ACQU'
    this.cWorkTables[6]='*GSVE67PV'
    return(this.OpenAllTables(6))

  proc CloseCursors()
    if used('_Curs_GSVETBPV')
      use in _Curs_GSVETBPV
    endif
    if used('_Curs_TMPVEND1')
      use in _Curs_TMPVEND1
    endif
    if used('_Curs_TMP_ACQU')
      use in _Curs_TMP_ACQU
    endif
    if used('_Curs_TMP_ACQU')
      use in _Curs_TMP_ACQU
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
