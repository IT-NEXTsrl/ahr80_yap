* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_mts                                                        *
*              Tabella sconti/maggiorazioni                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_11]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-18                                                      *
* Last revis.: 2011-02-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_mts"))

* --- Class definition
define class tgsar_mts as StdTrsForm
  Top    = 30
  Left   = 37

  * --- Standard Properties
  Width  = 450
  Height = 232+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-02-25"
  HelpContextID=80312169
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=14

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  TAB_SCON_IDX = 0
  CAT_SCMA_IDX = 0
  cFile = "TAB_SCON"
  cKeySelect = "TSCATCLF,TSCATART"
  cKeyWhere  = "TSCATCLF=this.w_TSCATCLF and TSCATART=this.w_TSCATART"
  cKeyDetail  = "TSCATCLF=this.w_TSCATCLF and TSCATART=this.w_TSCATART"
  cKeyWhereODBC = '"TSCATCLF="+cp_ToStrODBC(this.w_TSCATCLF)';
      +'+" and TSCATART="+cp_ToStrODBC(this.w_TSCATART)';

  cKeyDetailWhereODBC = '"TSCATCLF="+cp_ToStrODBC(this.w_TSCATCLF)';
      +'+" and TSCATART="+cp_ToStrODBC(this.w_TSCATART)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"TAB_SCON.TSCATCLF="+cp_ToStrODBC(this.w_TSCATCLF)';
      +'+" and TAB_SCON.TSCATART="+cp_ToStrODBC(this.w_TSCATART)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'TAB_SCON.CPROWNUM '
  cPrg = "gsar_mts"
  cComment = "Tabella sconti/maggiorazioni"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 7
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TSCATCLI = space(5)
  w_TSCATARR = space(5)
  w_TSCATCLF = space(5)
  o_TSCATCLF = space(5)
  w_TSCATART = space(5)
  o_TSCATART = space(5)
  w_DESCLI = space(35)
  w_DESART = space(35)
  w_TIPCA1 = space(1)
  w_TIPCA2 = space(1)
  w_TSDATINI = ctod('  /  /  ')
  w_TSDATFIN = ctod('  /  /  ')
  w_TSSCONT1 = 0
  o_TSSCONT1 = 0
  w_TSSCONT2 = 0
  o_TSSCONT2 = 0
  w_TSSCONT3 = 0
  o_TSSCONT3 = 0
  w_TSSCONT4 = 0
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'TAB_SCON','gsar_mts')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_mtsPag1","gsar_mts",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Sconti/Maggiorazioni")
      .Pages(1).HelpContextID = 172223347
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTSCATCLF_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='CAT_SCMA'
    this.cWorkTables[2]='TAB_SCON'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.TAB_SCON_IDX,5],7]
    this.nPostItConn=i_TableProp[this.TAB_SCON_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_TSCATCLF = NVL(TSCATCLF,space(5))
      .w_TSCATART = NVL(TSCATART,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_3_joined
    link_1_3_joined=.f.
    local link_1_4_joined
    link_1_4_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from TAB_SCON where TSCATCLF=KeySet.TSCATCLF
    *                            and TSCATART=KeySet.TSCATART
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- gsar_mts
      * --- Setta Ordine per Listino, Date Attivazione
      i_cOrder = 'order by TSDATINI, TSDATFIN '
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.TAB_SCON_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_SCON_IDX,2],this.bLoadRecFilter,this.TAB_SCON_IDX,"gsar_mts")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('TAB_SCON')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "TAB_SCON.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' TAB_SCON '
      link_1_3_joined=this.AddJoinedLink_1_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_4_joined=this.AddJoinedLink_1_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'TSCATCLF',this.w_TSCATCLF  ,'TSCATART',this.w_TSCATART  )
      select * from (i_cTable) TAB_SCON where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESCLI = space(35)
        .w_DESART = space(35)
        .w_TIPCA1 = space(1)
        .w_TIPCA2 = space(1)
        .w_TSCATCLI = NVL(TSCATCLI,space(5))
        .w_TSCATARR = NVL(TSCATARR,space(5))
        .w_TSCATCLF = NVL(TSCATCLF,space(5))
          if link_1_3_joined
            this.w_TSCATCLF = NVL(CSCODICE103,NVL(this.w_TSCATCLF,space(5)))
            this.w_DESCLI = NVL(CSDESCRI103,space(35))
            this.w_TIPCA1 = NVL(CSTIPCAT103,space(1))
          else
          .link_1_3('Load')
          endif
        .w_TSCATART = NVL(TSCATART,space(5))
          if link_1_4_joined
            this.w_TSCATART = NVL(CSCODICE104,NVL(this.w_TSCATART,space(5)))
            this.w_DESART = NVL(CSDESCRI104,space(35))
            this.w_TIPCA2 = NVL(CSTIPCAT104,space(1))
          else
          .link_1_4('Load')
          endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'TAB_SCON')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_TSDATINI = NVL(cp_ToDate(TSDATINI),ctod("  /  /  "))
          .w_TSDATFIN = NVL(cp_ToDate(TSDATFIN),ctod("  /  /  "))
          .w_TSSCONT1 = NVL(TSSCONT1,0)
          .w_TSSCONT2 = NVL(TSSCONT2,0)
          .w_TSSCONT3 = NVL(TSSCONT3,0)
          .w_TSSCONT4 = NVL(TSSCONT4,0)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_TSCATCLI=space(5)
      .w_TSCATARR=space(5)
      .w_TSCATCLF=space(5)
      .w_TSCATART=space(5)
      .w_DESCLI=space(35)
      .w_DESART=space(35)
      .w_TIPCA1=space(1)
      .w_TIPCA2=space(1)
      .w_TSDATINI=ctod("  /  /  ")
      .w_TSDATFIN=ctod("  /  /  ")
      .w_TSSCONT1=0
      .w_TSSCONT2=0
      .w_TSSCONT3=0
      .w_TSSCONT4=0
      if .cFunction<>"Filter"
        .w_TSCATCLI = .w_TSCATCLF
        .w_TSCATARR = IIF(EMPTY(.w_TSCATART), '#', .w_TSCATART)
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_TSCATCLF))
         .link_1_3('Full')
        endif
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_TSCATART))
         .link_1_4('Full')
        endif
        .DoRTCalc(5,11,.f.)
        .w_TSSCONT2 = 0
        .w_TSSCONT3 = 0
        .w_TSSCONT4 = 0
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'TAB_SCON')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oTSCATCLF_1_3.enabled = i_bVal
      .Page1.oPag.oTSCATART_1_4.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oTSCATCLF_1_3.enabled = .f.
        .Page1.oPag.oTSCATART_1_4.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oTSCATCLF_1_3.enabled = .t.
        .Page1.oPag.oTSCATART_1_4.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'TAB_SCON',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.TAB_SCON_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TSCATCLI,"TSCATCLI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TSCATARR,"TSCATARR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TSCATCLF,"TSCATCLF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TSCATART,"TSCATART",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.TAB_SCON_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_SCON_IDX,2])
    i_lTable = "TAB_SCON"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.TAB_SCON_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select distinct "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select distinct &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSAR_STS with this
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_TSDATINI D(8);
      ,t_TSDATFIN D(8);
      ,t_TSSCONT1 N(6,2);
      ,t_TSSCONT2 N(6,2);
      ,t_TSSCONT3 N(6,2);
      ,t_TSSCONT4 N(6,2);
      ,CPROWNUM N(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsar_mtsbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTSDATINI_2_1.controlsource=this.cTrsName+'.t_TSDATINI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTSDATFIN_2_2.controlsource=this.cTrsName+'.t_TSDATFIN'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTSSCONT1_2_3.controlsource=this.cTrsName+'.t_TSSCONT1'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTSSCONT2_2_4.controlsource=this.cTrsName+'.t_TSSCONT2'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTSSCONT3_2_5.controlsource=this.cTrsName+'.t_TSSCONT3'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTSSCONT4_2_6.controlsource=this.cTrsName+'.t_TSSCONT4'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(84)
    this.AddVLine(168)
    this.AddVLine(224)
    this.AddVLine(280)
    this.AddVLine(336)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTSDATINI_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.TAB_SCON_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_SCON_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.TAB_SCON_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_SCON_IDX,2])
      *
      * insert into TAB_SCON
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'TAB_SCON')
        i_extval=cp_InsertValODBCExtFlds(this,'TAB_SCON')
        i_cFldBody=" "+;
                  "(TSCATCLI,TSCATARR,TSCATCLF,TSCATART,TSDATINI"+;
                  ",TSDATFIN,TSSCONT1,TSSCONT2,TSSCONT3,TSSCONT4,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_TSCATCLI)+","+cp_ToStrODBC(this.w_TSCATARR)+","+cp_ToStrODBCNull(this.w_TSCATCLF)+","+cp_ToStrODBCNull(this.w_TSCATART)+","+cp_ToStrODBC(this.w_TSDATINI)+;
             ","+cp_ToStrODBC(this.w_TSDATFIN)+","+cp_ToStrODBC(this.w_TSSCONT1)+","+cp_ToStrODBC(this.w_TSSCONT2)+","+cp_ToStrODBC(this.w_TSSCONT3)+","+cp_ToStrODBC(this.w_TSSCONT4)+;
             ","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'TAB_SCON')
        i_extval=cp_InsertValVFPExtFlds(this,'TAB_SCON')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'TSCATCLF',this.w_TSCATCLF,'TSCATART',this.w_TSCATART)
        INSERT INTO (i_cTable) (;
                   TSCATCLI;
                  ,TSCATARR;
                  ,TSCATCLF;
                  ,TSCATART;
                  ,TSDATINI;
                  ,TSDATFIN;
                  ,TSSCONT1;
                  ,TSSCONT2;
                  ,TSSCONT3;
                  ,TSSCONT4;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_TSCATCLI;
                  ,this.w_TSCATARR;
                  ,this.w_TSCATCLF;
                  ,this.w_TSCATART;
                  ,this.w_TSDATINI;
                  ,this.w_TSDATFIN;
                  ,this.w_TSSCONT1;
                  ,this.w_TSSCONT2;
                  ,this.w_TSSCONT3;
                  ,this.w_TSSCONT4;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.TAB_SCON_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_SCON_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (t_TSSCONT1<>0) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'TAB_SCON')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " TSCATCLI="+cp_ToStrODBC(this.w_TSCATCLI)+;
                 ",TSCATARR="+cp_ToStrODBC(this.w_TSCATARR)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'TAB_SCON')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  TSCATCLI=this.w_TSCATCLI;
                 ,TSCATARR=this.w_TSCATARR;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_TSSCONT1<>0) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update TAB_SCON
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'TAB_SCON')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " TSCATCLI="+cp_ToStrODBC(this.w_TSCATCLI)+;
                     ",TSCATARR="+cp_ToStrODBC(this.w_TSCATARR)+;
                     ",TSDATINI="+cp_ToStrODBC(this.w_TSDATINI)+;
                     ",TSDATFIN="+cp_ToStrODBC(this.w_TSDATFIN)+;
                     ",TSSCONT1="+cp_ToStrODBC(this.w_TSSCONT1)+;
                     ",TSSCONT2="+cp_ToStrODBC(this.w_TSSCONT2)+;
                     ",TSSCONT3="+cp_ToStrODBC(this.w_TSSCONT3)+;
                     ",TSSCONT4="+cp_ToStrODBC(this.w_TSSCONT4)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'TAB_SCON')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      TSCATCLI=this.w_TSCATCLI;
                     ,TSCATARR=this.w_TSCATARR;
                     ,TSDATINI=this.w_TSDATINI;
                     ,TSDATFIN=this.w_TSDATFIN;
                     ,TSSCONT1=this.w_TSSCONT1;
                     ,TSSCONT2=this.w_TSSCONT2;
                     ,TSSCONT3=this.w_TSSCONT3;
                     ,TSSCONT4=this.w_TSSCONT4;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.TAB_SCON_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_SCON_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_TSSCONT1<>0) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete TAB_SCON
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_TSSCONT1<>0) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.TAB_SCON_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_SCON_IDX,2])
    if i_bUpd
      with this
        if .o_TSCATCLF<>.w_TSCATCLF
          .w_TSCATCLI = .w_TSCATCLF
        endif
        if .o_TSCATART<>.w_TSCATART
          .w_TSCATARR = IIF(EMPTY(.w_TSCATART), '#', .w_TSCATART)
        endif
        .DoRTCalc(3,11,.t.)
        if .o_TSSCONT1<>.w_TSSCONT1
          .w_TSSCONT2 = 0
        endif
        if .o_TSSCONT2<>.w_TSSCONT2
          .w_TSSCONT3 = 0
        endif
        if .o_TSSCONT3<>.w_TSSCONT3
          .w_TSSCONT4 = 0
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTSSCONT1_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTSSCONT1_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTSSCONT2_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTSSCONT2_2_4.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTSSCONT3_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTSSCONT3_2_5.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTSSCONT4_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTSSCONT4_2_6.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=TSCATCLF
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_SCMA_IDX,3]
    i_lTable = "CAT_SCMA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2], .t., this.CAT_SCMA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TSCATCLF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ASM',True,'CAT_SCMA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CSCODICE like "+cp_ToStrODBC(trim(this.w_TSCATCLF)+"%");

          i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI,CSTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CSCODICE',trim(this.w_TSCATCLF))
          select CSCODICE,CSDESCRI,CSTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TSCATCLF)==trim(_Link_.CSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TSCATCLF) and !this.bDontReportError
            deferred_cp_zoom('CAT_SCMA','*','CSCODICE',cp_AbsName(oSource.parent,'oTSCATCLF_1_3'),i_cWhere,'GSAR_ASM',"Categorie sconti clienti\fornitori",'GSVE1MTS.CAT_SCMA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI,CSTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODICE',oSource.xKey(1))
            select CSCODICE,CSDESCRI,CSTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TSCATCLF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI,CSTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CSCODICE="+cp_ToStrODBC(this.w_TSCATCLF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODICE',this.w_TSCATCLF)
            select CSCODICE,CSDESCRI,CSTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TSCATCLF = NVL(_Link_.CSCODICE,space(5))
      this.w_DESCLI = NVL(_Link_.CSDESCRI,space(35))
      this.w_TIPCA1 = NVL(_Link_.CSTIPCAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TSCATCLF = space(5)
      endif
      this.w_DESCLI = space(35)
      this.w_TIPCA1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPCA1='C'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_TSCATCLF = space(5)
        this.w_DESCLI = space(35)
        this.w_TIPCA1 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2])+'\'+cp_ToStr(_Link_.CSCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_SCMA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TSCATCLF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAT_SCMA_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_3.CSCODICE as CSCODICE103"+ ",link_1_3.CSDESCRI as CSDESCRI103"+ ",link_1_3.CSTIPCAT as CSTIPCAT103"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_3 on TAB_SCON.TSCATCLF=link_1_3.CSCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_3"
          i_cKey=i_cKey+'+" and TAB_SCON.TSCATCLF=link_1_3.CSCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=TSCATART
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_SCMA_IDX,3]
    i_lTable = "CAT_SCMA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2], .t., this.CAT_SCMA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TSCATART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ASM',True,'CAT_SCMA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CSCODICE like "+cp_ToStrODBC(trim(this.w_TSCATART)+"%");

          i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI,CSTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CSCODICE',trim(this.w_TSCATART))
          select CSCODICE,CSDESCRI,CSTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TSCATART)==trim(_Link_.CSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TSCATART) and !this.bDontReportError
            deferred_cp_zoom('CAT_SCMA','*','CSCODICE',cp_AbsName(oSource.parent,'oTSCATART_1_4'),i_cWhere,'GSAR_ASM',"Categorie sconti articoli",'GSVE2MTS.CAT_SCMA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI,CSTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODICE',oSource.xKey(1))
            select CSCODICE,CSDESCRI,CSTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TSCATART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI,CSTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CSCODICE="+cp_ToStrODBC(this.w_TSCATART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODICE',this.w_TSCATART)
            select CSCODICE,CSDESCRI,CSTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TSCATART = NVL(_Link_.CSCODICE,space(5))
      this.w_DESART = NVL(_Link_.CSDESCRI,space(35))
      this.w_TIPCA2 = NVL(_Link_.CSTIPCAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TSCATART = space(5)
      endif
      this.w_DESART = space(35)
      this.w_TIPCA2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPCA2='A' OR EMPTY(.w_TSCATART)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_TSCATART = space(5)
        this.w_DESART = space(35)
        this.w_TIPCA2 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2])+'\'+cp_ToStr(_Link_.CSCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_SCMA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TSCATART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAT_SCMA_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_4.CSCODICE as CSCODICE104"+ ",link_1_4.CSDESCRI as CSDESCRI104"+ ",link_1_4.CSTIPCAT as CSTIPCAT104"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_4 on TAB_SCON.TSCATART=link_1_4.CSCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_4"
          i_cKey=i_cKey+'+" and TAB_SCON.TSCATART=link_1_4.CSCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oTSCATCLF_1_3.value==this.w_TSCATCLF)
      this.oPgFrm.Page1.oPag.oTSCATCLF_1_3.value=this.w_TSCATCLF
    endif
    if not(this.oPgFrm.Page1.oPag.oTSCATART_1_4.value==this.w_TSCATART)
      this.oPgFrm.Page1.oPag.oTSCATART_1_4.value=this.w_TSCATART
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLI_1_7.value==this.w_DESCLI)
      this.oPgFrm.Page1.oPag.oDESCLI_1_7.value=this.w_DESCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_8.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_8.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTSDATINI_2_1.value==this.w_TSDATINI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTSDATINI_2_1.value=this.w_TSDATINI
      replace t_TSDATINI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTSDATINI_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTSDATFIN_2_2.value==this.w_TSDATFIN)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTSDATFIN_2_2.value=this.w_TSDATFIN
      replace t_TSDATFIN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTSDATFIN_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTSSCONT1_2_3.value==this.w_TSSCONT1)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTSSCONT1_2_3.value=this.w_TSSCONT1
      replace t_TSSCONT1 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTSSCONT1_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTSSCONT2_2_4.value==this.w_TSSCONT2)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTSSCONT2_2_4.value=this.w_TSSCONT2
      replace t_TSSCONT2 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTSSCONT2_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTSSCONT3_2_5.value==this.w_TSSCONT3)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTSSCONT3_2_5.value=this.w_TSSCONT3
      replace t_TSSCONT3 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTSSCONT3_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTSSCONT4_2_6.value==this.w_TSSCONT4)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTSSCONT4_2_6.value=this.w_TSSCONT4
      replace t_TSSCONT4 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTSSCONT4_2_6.value
    endif
    cp_SetControlsValueExtFlds(this,'TAB_SCON')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_TSCATCLF) or not(.w_TIPCA1='C'))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oTSCATCLF_1_3.SetFocus()
            i_bnoObbl = !empty(.w_TSCATCLF)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_TSCATART) or not(.w_TIPCA2='A' OR EMPTY(.w_TSCATART)))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oTSCATART_1_4.SetFocus()
            i_bnoObbl = !empty(.w_TSCATART)
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (this.cTrsName);
       where not(deleted()) and (t_TSSCONT1<>0);
        into cursor __chk__
    if not(max(0,1)<=cnt)
      do cp_ErrorMsg with MSG_YOU_MUST_INSERT_AT_LEAST_ONE_ROW
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(EMPTY(.w_TSDATFIN) OR .w_TSDATFIN>=.w_TSDATINI) and (.w_TSSCONT1<>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTSDATFIN_2_2
          i_bRes = .f.
          i_bnoChk = .f.
      endcase
      if .w_TSSCONT1<>0
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TSCATCLF = this.w_TSCATCLF
    this.o_TSCATART = this.w_TSCATART
    this.o_TSSCONT1 = this.w_TSSCONT1
    this.o_TSSCONT2 = this.w_TSSCONT2
    this.o_TSSCONT3 = this.w_TSSCONT3
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_TSSCONT1<>0)
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_TSDATINI=ctod("  /  /  ")
      .w_TSDATFIN=ctod("  /  /  ")
      .w_TSSCONT1=0
      .w_TSSCONT2=0
      .w_TSSCONT3=0
      .w_TSSCONT4=0
      .DoRTCalc(1,11,.f.)
        .w_TSSCONT2 = 0
        .w_TSSCONT3 = 0
        .w_TSSCONT4 = 0
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_TSDATINI = t_TSDATINI
    this.w_TSDATFIN = t_TSDATFIN
    this.w_TSSCONT1 = t_TSSCONT1
    this.w_TSSCONT2 = t_TSSCONT2
    this.w_TSSCONT3 = t_TSSCONT3
    this.w_TSSCONT4 = t_TSSCONT4
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_TSDATINI with this.w_TSDATINI
    replace t_TSDATFIN with this.w_TSDATFIN
    replace t_TSSCONT1 with this.w_TSSCONT1
    replace t_TSSCONT2 with this.w_TSSCONT2
    replace t_TSSCONT3 with this.w_TSSCONT3
    replace t_TSSCONT4 with this.w_TSSCONT4
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsar_mtsPag1 as StdContainer
  Width  = 446
  height = 232
  stdWidth  = 446
  stdheight = 232
  resizeYpos=185
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTSCATCLF_1_3 as StdField with uid="HLVINQQGRN",rtseq=3,rtrep=.f.,;
    cFormVar = "w_TSCATCLF", cQueryName = "TSCATCLF",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Categoria sconti / maggiorazioni cliente o fornitore",;
    HelpContextID = 205778564,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=117, Top=9, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAT_SCMA", cZoomOnZoom="GSAR_ASM", oKey_1_1="CSCODICE", oKey_1_2="this.w_TSCATCLF"

  func oTSCATCLF_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oTSCATCLF_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTSCATCLF_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oTSCATCLF_1_3.readonly and this.parent.oTSCATCLF_1_3.isprimarykey)
    do cp_zoom with 'CAT_SCMA','*','CSCODICE',cp_AbsName(this.parent,'oTSCATCLF_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ASM',"Categorie sconti clienti\fornitori",'GSVE1MTS.CAT_SCMA_VZM',this.parent.oContained
   endif
  endproc
  proc oTSCATCLF_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ASM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CSCODICE=this.parent.oContained.w_TSCATCLF
    i_obj.ecpSave()
  endproc

  add object oTSCATART_1_4 as StdField with uid="JHSGKNJXQV",rtseq=4,rtrep=.f.,;
    cFormVar = "w_TSCATART", cQueryName = "TSCATCLF,TSCATART",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Categoria sconti / maggiorazioni articolo",;
    HelpContextID = 29102474,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=117, Top=39, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAT_SCMA", cZoomOnZoom="GSAR_ASM", oKey_1_1="CSCODICE", oKey_1_2="this.w_TSCATART"

  func oTSCATART_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oTSCATART_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTSCATART_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oTSCATART_1_4.readonly and this.parent.oTSCATART_1_4.isprimarykey)
    do cp_zoom with 'CAT_SCMA','*','CSCODICE',cp_AbsName(this.parent,'oTSCATART_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ASM',"Categorie sconti articoli",'GSVE2MTS.CAT_SCMA_VZM',this.parent.oContained
   endif
  endproc
  proc oTSCATART_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ASM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CSCODICE=this.parent.oContained.w_TSCATART
    i_obj.ecpSave()
  endproc

  add object oDESCLI_1_7 as StdField with uid="ECRZBGSFGD",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESCLI", cQueryName = "DESCLI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 113311178,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=184, Top=9, InputMask=replicate('X',35)

  add object oDESART_1_8 as StdField with uid="KWTUVDICOE",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 77398582,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=184, Top=39, InputMask=replicate('X',35)


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=3, top=70, width=404,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=6,Field1="TSDATINI",Label1="Validit� dal",Field2="TSDATFIN",Label2="Al",Field3="TSSCONT1",Label3="Sconto1",Field4="TSSCONT2",Label4="Sconto2",Field5="TSSCONT3",Label5="Sconto3",Field6="TSSCONT4",Label6="Sconto4",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 235778682

  add object oStr_1_5 as StdString with uid="BHUIIXOLXR",Visible=.t., Left=0, Top=9,;
    Alignment=1, Width=116, Height=15,;
    Caption="Categoria cli/for:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_6 as StdString with uid="OTOEPPSSSZ",Visible=.t., Left=0, Top=39,;
    Alignment=1, Width=117, Height=15,;
    Caption="Categoria articolo:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-7,top=89,;
    width=400+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*7*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-6,top=90,width=399+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*7*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsar_mtsBodyRow as CPBodyRowCnt
  Width=390
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oTSDATINI_2_1 as StdTrsField with uid="YDAPQJPHUB",rtseq=9,rtrep=.t.,;
    cFormVar="w_TSDATINI",value=ctod("  /  /  "),;
    ToolTipText = "Data di entrata in vigore",;
    HelpContextID = 105111169,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=79, Left=-2, Top=0

  add object oTSDATFIN_2_2 as StdTrsField with uid="JAPTZFRIXK",rtseq=10,rtrep=.t.,;
    cFormVar="w_TSDATFIN",value=ctod("  /  /  "),;
    ToolTipText = "Data di fine attivit� dello sconto",;
    HelpContextID = 155442812,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=79, Left=82, Top=0

  func oTSDATFIN_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_TSDATFIN) OR .w_TSDATFIN>=.w_TSDATINI)
    endwith
    return bRes
  endfunc

  add object oTSSCONT1_2_3 as StdTrsField with uid="OSLMZINGLY",rtseq=11,rtrep=.t.,;
    cFormVar="w_TSSCONT1",value=0,;
    ToolTipText = "Prima percentuale di maggiorazione (se positiva) o sconto (se negativa)",;
    HelpContextID = 242159975,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=51, Left=166, Top=0, cSayPict=["999.99"], cGetPict=["999.99"]

  func oTSSCONT1_2_3.mCond()
    with this.Parent.oContained
      return (g_NUMSCO>0)
    endwith
  endfunc

  add object oTSSCONT2_2_4 as StdTrsField with uid="MOFTHOXDHI",rtseq=12,rtrep=.t.,;
    cFormVar="w_TSSCONT2",value=0,;
    ToolTipText = "Seconda percentuale di maggiorazione (se positiva) o sconto (se negativa)",;
    HelpContextID = 242159976,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=51, Left=222, Top=0, cSayPict=["999.99"], cGetPict=["999.99"]

  func oTSSCONT2_2_4.mCond()
    with this.Parent.oContained
      return (.w_TSSCONT1<>0 AND g_NUMSCO>1)
    endwith
  endfunc

  add object oTSSCONT3_2_5 as StdTrsField with uid="QVRIOGBNUF",rtseq=13,rtrep=.t.,;
    cFormVar="w_TSSCONT3",value=0,;
    ToolTipText = "Terza percentuale di maggiorazione (se positiva) o sconto (se negativa)",;
    HelpContextID = 242159977,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=51, Left=278, Top=0, cSayPict=["999.99"], cGetPict=["999.99"]

  func oTSSCONT3_2_5.mCond()
    with this.Parent.oContained
      return (.w_TSSCONT2<>0 AND g_NUMSCO>2)
    endwith
  endfunc

  add object oTSSCONT4_2_6 as StdTrsField with uid="GEIJNJTQYO",rtseq=14,rtrep=.t.,;
    cFormVar="w_TSSCONT4",value=0,;
    ToolTipText = "Quarta percentuale di maggiorazione (se positiva) o sconto (se negativa)",;
    HelpContextID = 242159978,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=51, Left=334, Top=0, cSayPict=["999.99"], cGetPict=["999.99"]

  func oTSSCONT4_2_6.mCond()
    with this.Parent.oContained
      return (.w_TSSCONT3<>0 AND g_NUMSCO>3)
    endwith
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oTSDATINI_2_1.When()
    return(.t.)
  proc oTSDATINI_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oTSDATINI_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=6
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_mts','TAB_SCON','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".TSCATCLF=TAB_SCON.TSCATCLF";
  +" and "+i_cAliasName2+".TSCATART=TAB_SCON.TSCATART";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
