* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bcp                                                        *
*              Caricamento parametri                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-07-11                                                      *
* Last revis.: 2009-03-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_MASK,w_GEST,w_ACTION,w_NOMEAP,w_ROWNUM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bcp",oParentObject,m.w_MASK,m.w_GEST,m.w_ACTION,m.w_NOMEAP,m.w_ROWNUM)
return(i_retval)

define class tgsut_bcp as StdBatch
  * --- Local variables
  w_NOMDES = space(254)
  w_MASK = .NULL.
  w_GEST = .NULL.
  w_ACTION = space(1)
  w_NOMEAP = space(30)
  w_ROWNUM = 0
  w_COUNT = 0
  w_VARIABLE = space(20)
  w_DESC = space(254)
  w_VALUE = space(254)
  w_INIT = .f.
  w_GSVALVAR = space(254)
  w_GSFLGACT = space(1)
  w_NOHIDE = .f.
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_READAZI = space(5)
  w_NUNHIDEVAR = 0
  w_FIELD = space(100)
  w_NCHR = 0
  w_FOUND = .f.
  * --- WorkFile variables
  GESTSJBD_idx=0
  PRCFGGES_idx=0
  CFG_GEST_idx=0
  MODDCONF_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Popolamento figli integrati
    * --- w_MASK - Gestione da interrogare
    *     w_GEST - Gestione da popolare 
    *     w_ACTION - 'J'=GSJB_BMS, 'G'=GSJB_BGS, 'C'=CONFIGURAZIONI, 'S'=Salva cfg, 'A'=Salva come
    *     w_NOMEAP = Valorizzato solo per w_ACTION='J'(GSJB_BMS), w_ACTION='S', w_ACTION='A'
    *     w_ROWNUM = CPROWNUM della cfg
    * --- Se Salva come, cerco ultimo cprownum
    if this.w_ACTION="A" And this.w_ROWNUM=0 
      if not(Empty(this.oParentObject.w_DESCFG)) And (this.oParentObject.w_TIPCFG="I" Or Not Empty(this.oParentObject.w_UTEGRP))
        * --- Determino CPROWNUM
        if this.oParentObject.w_DEFCFG="S"
          * --- Read from CFG_GEST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CFG_GEST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CFG_GEST_idx,2],.t.,this.CFG_GEST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CGCODAZI"+;
              " from "+i_cTable+" CFG_GEST where ";
                  +"CGNOMGES = "+cp_ToStrODBC(this.w_NOMEAP);
                  +" and CGDEFCFG = "+cp_ToStrODBC("S");
                  +" and CGCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CGCODAZI;
              from (i_cTable) where;
                  CGNOMGES = this.w_NOMEAP;
                  and CGDEFCFG = "S";
                  and CGCODAZI = this.oParentObject.w_CODAZI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_READAZI = NVL(cp_ToDate(_read_.CGCODAZI),cp_NullValue(_read_.CGCODAZI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if i_ROWS=0 or this.oParentObject.w_DEFCFG<>"S"
          * --- Read from CFG_GEST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CFG_GEST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CFG_GEST_idx,2],.t.,this.CFG_GEST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CGCODAZI"+;
              " from "+i_cTable+" CFG_GEST where ";
                  +"CGNOMGES = "+cp_ToStrODBC(this.w_NOMEAP);
                  +" and CGDESCFG = "+cp_ToStrODBC(this.oParentObject.w_DESCFG);
                  +" and CGCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CGCODAZI;
              from (i_cTable) where;
                  CGNOMGES = this.w_NOMEAP;
                  and CGDESCFG = this.oParentObject.w_DESCFG;
                  and CGCODAZI = this.oParentObject.w_CODAZI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_READAZI = NVL(cp_ToDate(_read_.CGCODAZI),cp_NullValue(_read_.CGCODAZI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_rows <>0
            Ah_errormsg("Attenzione: esiste gi� una configurazione con lo stesso nome.")
            i_retcode = 'stop'
            i_retval = 0
            return
          else
            * --- Select from CFG_GEST
            i_nConn=i_TableProp[this.CFG_GEST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CFG_GEST_idx,2],.t.,this.CFG_GEST_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select MAX(CPROWNUM) AS CPROWNUM  from "+i_cTable+" CFG_GEST ";
                  +" where CGNOMGES="+cp_ToStrODBC(this.w_NOMEAP)+"";
                  +" group by CGNOMGES";
                  +" order by CPROWNUM";
                   ,"_Curs_CFG_GEST")
            else
              select MAX(CPROWNUM) AS CPROWNUM from (i_cTable);
               where CGNOMGES=this.w_NOMEAP;
               group by CGNOMGES;
               order by CPROWNUM;
                into cursor _Curs_CFG_GEST
            endif
            if used('_Curs_CFG_GEST')
              select _Curs_CFG_GEST
              locate for 1=1
              do while not(eof())
              this.w_ROWNUM = _Curs_CFG_GEST.CPROWNUM
                select _Curs_CFG_GEST
                continue
              enddo
              use
            endif
            this.w_ROWNUM = this.w_ROWNUM + 1
            * --- Inserisco la nuova configurazione
            * --- Insert into CFG_GEST
            i_nConn=i_TableProp[this.CFG_GEST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CFG_GEST_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CFG_GEST_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"CGNOMGES"+",CPROWNUM"+",CGDESCFG"+",CGMODSAV"+",CGDEFCFG"+",CGTIPCFG"+",CGUTEGRP"+",CGCODAZI"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_NOMEAP),'CFG_GEST','CGNOMGES');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'CFG_GEST','CPROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DESCFG),'CFG_GEST','CGDESCFG');
              +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MODSAV),'CFG_GEST','CGMODSAV');
              +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DEFCFG),'CFG_GEST','CGDEFCFG');
              +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TIPCFG),'CFG_GEST','CGTIPCFG');
              +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_UTEGRP),'CFG_GEST','CGUTEGRP');
              +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODAZI),'CFG_GEST','CGCODAZI');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'CGNOMGES',this.w_NOMEAP,'CPROWNUM',this.w_ROWNUM,'CGDESCFG',this.oParentObject.w_DESCFG,'CGMODSAV',this.oParentObject.w_MODSAV,'CGDEFCFG',this.oParentObject.w_DEFCFG,'CGTIPCFG',this.oParentObject.w_TIPCFG,'CGUTEGRP',this.oParentObject.w_UTEGRP,'CGCODAZI',this.oParentObject.w_CODAZI)
              insert into (i_cTable) (CGNOMGES,CPROWNUM,CGDESCFG,CGMODSAV,CGDEFCFG,CGTIPCFG,CGUTEGRP,CGCODAZI &i_ccchkf. );
                 values (;
                   this.w_NOMEAP;
                   ,this.w_ROWNUM;
                   ,this.oParentObject.w_DESCFG;
                   ,this.oParentObject.w_MODSAV;
                   ,this.oParentObject.w_DEFCFG;
                   ,this.oParentObject.w_TIPCFG;
                   ,this.oParentObject.w_UTEGRP;
                   ,this.oParentObject.w_CODAZI;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
        else
          Ah_errormsg("Attenzione: esiste gi� una configurazione di default.")
          i_retcode = 'stop'
          i_retval = 0
          return
        endif
      else
        Ah_errormsg("Attenzione: indicare configurazione.")
        i_retcode = 'stop'
        i_retval = 0
        return
      endif
    endif
    * --- Controllo se � presente la pagina elenco
    if Empty(this.w_MASK.cfile)
      this.w_COUNT = this.w_MASK.oPgFrm.PageCount
    else
      this.w_COUNT = this.w_MASK.oPgFrm.PageCount - 1
    endif
    this.w_NOHIDE = .T.
    * --- Ciclo sulle pagine
    if !ISNULL(this.w_GEST)
      this.w_GEST.MarkPos()     
    endif
    * --- Cursore per esclusione campi obbligatori
    CREATE CURSOR OUT_VAR_CUR (PROP C(200))
    * --- Ciclo sulle pagine
    For w_NumPage=1 to this.w_COUNT
    * --- Controllo se esiste oPag, per evitare problemi con pagina elenco, postit integrati
    if Type("this.w_MASK.oPgFrm.Pages(w_NumPage).oPag")<>"U"
      For w_NumCtrl=1 to this.w_MASK.oPgFrm.Pages(w_NumPage).oPag.ControlCount
      do case
        case UPPER(this.w_MASK.oPgFrm.Pages(w_NumPage).oPag.Controls(w_NumCtrl).class) $ "STDFIELD-STDCHECK-STDCOMBO-STDRADIO-STDTABLECOMBO-STDMEMO-CP_OUTPUTCOMBO"
          * --- Inserisco variabile/campo
          if UPPER(this.w_MASK.oPgFrm.Pages(w_NumPage).oPag.Controls(w_NumCtrl).class) $ "CP_OUTPUTCOMBO"
            * --- Combo output utente
            this.w_VARIABLE = "w_" + this.w_MASK.oPgFrm.Pages(w_NumPage).oPag.Controls(w_NumCtrl).name+".value"
            this.w_DESC = "Output utente"
          else
            this.w_VARIABLE = this.w_MASK.oPgFrm.Pages(w_NumPage).oPag.Controls(w_NumCtrl).cFormVar
            this.w_DESC = this.w_MASK.oPgFrm.Pages(w_NumPage).oPag.Controls(w_NumCtrl).tooltiptext
          endif
          do case
            case this.w_ACTION="J"
              * --- Read from GESTSJBD
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.GESTSJBD_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.GESTSJBD_idx,2],.t.,this.GESTSJBD_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "GSFLGACT,GSVALVAR"+;
                  " from "+i_cTable+" GESTSJBD where ";
                      +"GSNOME = "+cp_ToStrODBC(this.w_NOMEAP);
                      +" and GSNOMVAL = "+cp_ToStrODBC(this.w_VARIABLE);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  GSFLGACT,GSVALVAR;
                  from (i_cTable) where;
                      GSNOME = this.w_NOMEAP;
                      and GSNOMVAL = this.w_VARIABLE;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_GSFLGACT = NVL(cp_ToDate(_read_.GSFLGACT),cp_NullValue(_read_.GSFLGACT))
                this.w_GSVALVAR = NVL(cp_ToDate(_read_.GSVALVAR),cp_NullValue(_read_.GSVALVAR))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              do case
                case this.w_GSFLGACT="E"
                  * --- Non faccio nulla, il parametro non viene inizializzato
                  LOOP
                case this.w_GSFLGACT="S"
                  * --- Il parametro viene inizializzato con il valore della tab gestioni
                  w_valore=this.w_GSVALVAR
                otherwise
                  w_valore=this.w_MASK.oPgFrm.Pages(w_NumPage).oPag.Controls(w_NumCtrl).value
                  this.Pag2()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
              endcase
            case this.w_ACTION="S"
              * --- Read from PRCFGGES
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.PRCFGGES_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PRCFGGES_idx,2],.t.,this.PRCFGGES_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "CPROWNUM"+;
                  " from "+i_cTable+" PRCFGGES where ";
                      +"PRNOMGES = "+cp_ToStrODBC(this.w_NOMEAP);
                      +" and PRROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
                      +" and PRNOMVAR = "+cp_ToStrODBC(this.w_VARIABLE);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  CPROWNUM;
                  from (i_cTable) where;
                      PRNOMGES = this.w_NOMEAP;
                      and PRROWNUM = this.w_ROWNUM;
                      and PRNOMVAR = this.w_VARIABLE;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_CPROWNUM = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if i_Rows>0
                w_valore=this.w_MASK.oPgFrm.Pages(w_NumPage).oPag.Controls(w_NumCtrl).value
                this.Pag2()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                * --- Write into PRCFGGES
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.PRCFGGES_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PRCFGGES_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.PRCFGGES_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"PRVALVAR ="+cp_NullLink(cp_ToStrODBC(w_valore),'PRCFGGES','PRVALVAR');
                      +i_ccchkf ;
                  +" where ";
                      +"PRNOMGES = "+cp_ToStrODBC(this.w_NOMEAP);
                      +" and PRROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
                      +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                         )
                else
                  update (i_cTable) set;
                      PRVALVAR = w_valore;
                      &i_ccchkf. ;
                   where;
                      PRNOMGES = this.w_NOMEAP;
                      and PRROWNUM = this.w_ROWNUM;
                      and CPROWNUM = this.w_CPROWNUM;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
                LOOP
              else
                * --- Non faccio nulla, il parametro non viene inizializzato
                LOOP
              endif
            case this.w_ACTION="A"
              w_valore=this.w_MASK.oPgFrm.Pages(w_NumPage).oPag.Controls(w_NumCtrl).value
              this.Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              this.Pag3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              LOOP
            otherwise
              w_valore=this.w_MASK.oPgFrm.Pages(w_NumPage).oPag.Controls(w_NumCtrl).value
              this.Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
          endcase
          this.w_GEST.AddRow()     
          this.w_VALUE = w_valore
          * --- Valorizzo campi
          do case
            case this.w_ACTION="J"
              this.w_GEST.w_PRNOMVAL = this.w_VARIABLE
              this.w_GEST.w_PRVALVAR = this.w_VALUE
              this.w_GEST.w_PRNOMDES = this.w_DESC
              this.w_GEST.w_PRTIPPAR = "V"
            case this.w_ACTION="G"
              this.w_GEST.w_GSNOMVAL = this.w_VARIABLE
              this.w_GEST.w_GSTIPPAR = "V"
              this.w_GEST.w_GSVALVAR = this.w_VALUE
              this.w_GEST.w_GSNOMDES = this.w_DESC
              this.w_GEST.w_GSFLGACT = " "
            case this.w_ACTION="C"
              this.w_GEST.w_MCNOMVAR = this.w_VARIABLE
              this.w_GEST.w_MCDESVAR = this.w_DESC
          endcase
          * --- Salvo la riga
          this.w_GEST.SaveRow()     
          this.w_GEST.mCalc(.t.)     
        case this.w_ACTION<>"C" AND this.w_ACTION<>"S" AND this.w_ACTION<>"A" AND UPPER(this.w_MASK.oPgFrm.Pages(w_NumPage).oPag.Controls(w_NumCtrl).class) $ "STDBUTTON" AND this.w_MASK.oPgFrm.Pages(w_NumPage).oPag.Controls(w_NumCtrl).visible
          * --- Inserisco bottone solo se visibile
          this.w_VARIABLE = this.w_MASK.oPgFrm.Pages(w_NumPage).oPag.Controls(w_NumCtrl).uid
          this.w_DESC = ""
          if NOT EMPTY(this.w_MASK.oPgFrm.Pages(w_NumPage).oPag.Controls(w_NumCtrl).tooltiptext)
            this.w_DESC = " ("+ ALLTRIM(this.w_MASK.oPgFrm.Pages(w_NumPage).oPag.Controls(w_NumCtrl).tooltiptext)+")"
          endif
          if this.w_ACTION="J"
            * --- Read from GESTSJBD
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.GESTSJBD_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.GESTSJBD_idx,2],.t.,this.GESTSJBD_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "GSFLGACT"+;
                " from "+i_cTable+" GESTSJBD where ";
                    +"GSNOME = "+cp_ToStrODBC(this.w_NOMEAP);
                    +" and GSNOMVAL = "+cp_ToStrODBC(this.w_VARIABLE);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                GSFLGACT;
                from (i_cTable) where;
                    GSNOME = this.w_NOMEAP;
                    and GSNOMVAL = this.w_VARIABLE;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_GSFLGACT = NVL(cp_ToDate(_read_.GSFLGACT),cp_NullValue(_read_.GSFLGACT))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if this.w_GSFLGACT="E"
              * --- Non faccio nulla, il parametro non viene inizializzato
              LOOP
            endif
            this.w_GEST.AddRow()     
            this.w_GEST.w_PRTIPPAR = "B"
            this.w_GEST.w_PRNOMVAL = this.w_VARIABLE
            * --- Nella descrizione del bottone metto Caption_NumPag (Tooltip)
            this.w_GEST.w_PRNOMDES = STRTRAN(ALLTRIM(this.w_MASK.oPgFrm.Pages(w_NumPage).oPag.Controls(w_NumCtrl).caption), "\<","") + "_"+ALLTRIM(STR(w_NumPage))+this.w_DESC
          else
            this.w_GEST.AddRow()     
            this.w_GEST.w_GSNOMVAL = this.w_MASK.oPgFrm.Pages(w_NumPage).oPag.Controls(w_NumCtrl).uid
            this.w_GEST.w_GSVALVAR = ""
            this.w_GEST.w_GSTIPPAR = "B"
            * --- Nella descrizione del bottone metto Caption_NumPag (Tooltip)
            this.w_GEST.w_GSNOMDES = STRTRAN(ALLTRIM(this.w_MASK.oPgFrm.Pages(w_NumPage).oPag.Controls(w_NumCtrl).caption), "\<","") + "_"+ALLTRIM(STR(w_NumPage))+this.w_DESC
            this.w_GEST.w_GSFLGACT = " "
          endif
          * --- Salvo la riga
          this.w_GEST.SaveRow()     
          this.w_GEST.mCalc(.t.)     
      endcase
      EndFor
    endif
    EndFor
    * --- Gestione variabili nascoste
    if this.w_ACTION="J"
      * --- Seleziono i nomi delle variabili che sono gi� presenti sul dettaglio
      this.w_GEST.Exec_Select("CursSele", "distinct t_PRNOMVAL as PRNOMVAL")     
      * --- Seleziono tutte la variabili che non devono essere eliminate dalla tabelle
      *     delle gestioni schedulatore
      VQ_EXEC("..\JBSH\EXE\GSJB_BMS.VQR",this,"UltVariabili")
      * --- Seleziono tutte le variabili non ancora inserite presenti sulla tabella delle gestioni
      Select * from UltVariabili where GSNOMVAL not in (select PRNOMVAL from CursSele) into cursor UltVariabili
      this.w_NOHIDE = .F.
      if Used("UltVariabili")
        Select UltVariabili 
 Go top 
 SCAN
        this.w_GSFLGACT = UltVariabili.GSFLGACT
        this.w_VARIABLE = Alltrim(UltVariabili.GSNOMVAL)
        this.w_DESC = UltVariabili.GSNOMDES
        w_valore="this.w_MASK."+this.w_VARIABLE
        * --- Testo se esiste la variabile
        if TYPE(w_valore) <> "U" AND TYPE(w_valore)<>"O"
          * --- La variabile esite sulla maschera 
          if this.w_GSFLGACT="S"
            * --- Il parametro viene inizializzato con il valore della tab gestioni
            w_valore=UltVariabili.GSVALVAR
          else
            * --- Prelevo il contenuto della variabile utilizzando una macro
            w_valore = &w_valore
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          * --- Aggiungo solo se variabili che sono presenti sulla maschera (w_MASK)
          this.w_GEST.AddRow()     
          this.w_VALUE = w_valore
          this.w_GEST.w_PRNOMVAL = this.w_VARIABLE
          this.w_GEST.w_PRVALVAR = this.w_VALUE
          this.w_GEST.w_PRNOMDES = this.w_DESC
          this.w_GEST.SaveRow()     
          this.w_GEST.mCalc(.t.)     
        endif
        Select UltVariabili
        ENDSCAN
        USE IN SELECT("UltVariabili")
      endif
      USE IN SELECT("CursSele")
    else
      this.w_NOHIDE = .F.
      CREATE CURSOR HIDE_VAR_CUR (PROP C(200))
      this.w_NUNHIDEVAR = AMEMBERS(L_aHideVar, this.w_MASK,1)
      SELECT HIDE_VAR_CUR
      APPEND FROM ARRAY L_aHideVar FOR UPPER(LEFT(PROP,2)) = "W_"
      if !ISNULL(this.w_GEST)
        if this.w_ACTION="G"
          this.w_FIELD = "DISTINCT UPPER(t_GSNOMVAL) as PROP"
        else
          this.w_FIELD = "DISTINCT UPPER(t_MCNOMVAR) as PROP"
        endif
        this.w_GEST.Exec_Select("CursSele", this.w_FIELD)     
        SELECT * FROM HIDE_VAR_CUR WHERE UPPER(PROP) NOT IN (SELECT UPPER(PROP) AS PROP FROM CursSele) INTO CURSOR HIDE_VAR_CUR
        USE IN SELECT("CursSele")
      endif
      if this.w_ACTION = "A"
        * --- Elimino campi obbligatori
        SELECT * FROM HIDE_VAR_CUR WHERE UPPER(PROP) NOT IN (SELECT UPPER(PROP) AS PROP FROM OUT_VAR_CUR) INTO CURSOR HIDE_VAR_CUR
        USE IN SELECT("OUT_VAR_CUR")
      endif
      SELECT HIDE_VAR_CUR
      GO TOP
      SCAN
      if !ISNULL(this.w_GEST)
        this.w_GEST.AddRow()     
      endif
      this.w_VARIABLE = ALLTRIM(PROP)
       
 local l_hidevar 
 l_hidevar="This.w_Mask."+this.w_VARIABLE 
 w_valore=&l_hidevar
      * --- Se tipo diverso da oggetto
      if Type("w_valore")<>"O"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        do case
          case this.w_ACTION="S"
            * --- Read from PRCFGGES
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PRCFGGES_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PRCFGGES_idx,2],.t.,this.PRCFGGES_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CPROWNUM"+;
                " from "+i_cTable+" PRCFGGES where ";
                    +"PRNOMGES = "+cp_ToStrODBC(this.w_NOMEAP);
                    +" and PRROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
                    +" and PRNOMVAR = "+cp_ToStrODBC(this.w_VARIABLE);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CPROWNUM;
                from (i_cTable) where;
                    PRNOMGES = this.w_NOMEAP;
                    and PRROWNUM = this.w_ROWNUM;
                    and PRNOMVAR = this.w_VARIABLE;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CPROWNUM = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if i_Rows>0
              * --- Write into PRCFGGES
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.PRCFGGES_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PRCFGGES_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.PRCFGGES_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"PRVALVAR ="+cp_NullLink(cp_ToStrODBC(w_valore),'PRCFGGES','PRVALVAR');
                    +i_ccchkf ;
                +" where ";
                    +"PRNOMGES = "+cp_ToStrODBC(this.w_NOMEAP);
                    +" and PRROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                       )
              else
                update (i_cTable) set;
                    PRVALVAR = w_valore;
                    &i_ccchkf. ;
                 where;
                    PRNOMGES = this.w_NOMEAP;
                    and PRROWNUM = this.w_ROWNUM;
                    and CPROWNUM = this.w_CPROWNUM;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              LOOP
            else
              LOOP
            endif
          case this.w_ACTION="G"
            this.w_GEST.w_GSNOMVAL = LOWER(LEFT(this.w_VARIABLE,1))+RIGHT(this.w_VARIABLE, LEN(this.w_VARIABLE)-1)
            this.w_GEST.w_GSTIPPAR = "V"
            this.w_GEST.w_GSVALVAR = w_valore
            this.w_GEST.w_GSNOMDES = Ah_MsgFormat("Variabile nascosta")
            this.w_GEST.w_GSFLGACT = " "
          case this.w_ACTION="A"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            LOOP
          otherwise
            this.w_GEST.w_MCNOMVAR = LOWER(LEFT(this.w_VARIABLE,1))+RIGHT(this.w_VARIABLE, LEN(this.w_VARIABLE)-1)
            this.w_GEST.w_MCDESVAR = Ah_MsgFormat("Variabile nascosta")
        endcase
        this.w_GEST.SaveRow()     
        this.w_GEST.mCalc(.t.)     
      endif
      ENDSCAN
      USE IN SELECT("HIDE_VAR_CUR") 
    endif
    if !ISNULL(this.w_GEST)
      this.w_GEST.RePos()     
      this.w_GEST.Refresh()     
    endif
    if this.w_ACTION="A"
      * --- Ritorno il CPROWNUM della cfg inserita, solo se su solita azienda
      if Alltrim(Upper(this.oParentObject.w_CODAZI)) == Alltrim(Upper(i_CodAzi)) Or Empty(this.oParentObject.w_CODAZI)
        i_retcode = 'stop'
        i_retval = this.w_ROWNUM
        return
      else
        * --- Se ho salvato per un altra azienda non carico la cfg
        i_retcode = 'stop'
        i_retval = -1
        return
      endif
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo tipo
    if ISNULL(w_VALORE)
      * --- Valore nullo
      w_valore=".NULL."
    else
      * --- Valore non nullo, determino il tipo
      do case
        case type("w_valore")="D"
          w_valore="cp_CharToDate('"+DTOC(w_valore)+"')"
        case type("w_valore")="T"
          w_valore="cp_CharToDateTime('"+TTOC(w_valore)+"')"
        case type("w_valore")="N"
          do case
            case this.w_NOHIDE AND UPPER(this.w_MASK.oPgFrm.Pages(w_NumPage).oPag.Controls(w_NumCtrl).class) $ "STDCOMBO-STDCHECK-STDRADIO-STDTABLECOMBO"
              w_valore=this.w_MASK.oPgFrm.Pages(w_NumPage).oPag.Controls(w_NumCtrl).RadioValue()
              do case
                case type("w_valore")="C"
                  w_valore='"'+LEFT(w_valore, 252)+'"'
                case type("w_valore")="N"
                  w_valore="VAL('"+STR(w_valore , 25, 8)+"')"
                case type("w_valore")="L"
                  w_valore=iif(w_valore,"true","false")
              endcase
            otherwise
              w_valore="VAL('"+STR(w_valore , 25, 8)+"')"
          endcase
        case type("w_valore")="L"
          w_valore=iif(w_valore,"true","false")
        case type("w_valore")="C"
          this.w_NCHR = AT(CHR(13),w_valore)-1
          if this.w_NCHR < 0
            * --- Ho solo una riga
            w_valore='"'+LEFT(w_valore, 252)+'"'
          else
            * --- Ho pi� di una riga, quindi prendo solo la prima
            w_valore='"'+LEFT(SUBSTR(w_valore, 1, this.w_NCHR), 252)+'"'
          endif
      endcase
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Cerco nel modello se il parametro � presente
    this.w_FOUND = .F.
    * --- Select from MODDCONF
    i_nConn=i_TableProp[this.MODDCONF_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MODDCONF_idx,2],.t.,this.MODDCONF_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select MCDESVAR  from "+i_cTable+" MODDCONF ";
          +" where MCNOMGES = "+cp_ToStrODBC(this.w_NOMEAP)+" AND UPPER(MCNOMVAR) = UPPER("+cp_ToStrODBC(this.w_VARIABLE)+")";
           ,"_Curs_MODDCONF")
    else
      select MCDESVAR from (i_cTable);
       where MCNOMGES = this.w_NOMEAP AND UPPER(MCNOMVAR) = UPPER(this.w_VARIABLE);
        into cursor _Curs_MODDCONF
    endif
    if used('_Curs_MODDCONF')
      select _Curs_MODDCONF
      locate for 1=1
      do while not(eof())
      this.w_FOUND = .T.
      this.w_NOMDES = _Curs_MODDCONF.MCDESVAR
        select _Curs_MODDCONF
        continue
      enddo
      use
    endif
    if this.w_FOUND
      this.w_FOUND = .F.
      * --- Select from PRCFGGES
      i_nConn=i_TableProp[this.PRCFGGES_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PRCFGGES_idx,2],.t.,this.PRCFGGES_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select CPROWNUM  from "+i_cTable+" PRCFGGES ";
            +" where PRNOMGES = "+cp_ToStrODBC(this.w_NOMEAP)+" AND PRROWNUM = "+cp_ToStrODBC(this.w_ROWNUM)+" AND UPPER(PRNOMVAR) = UPPER("+cp_ToStrODBC(this.w_VARIABLE)+")";
             ,"_Curs_PRCFGGES")
      else
        select CPROWNUM from (i_cTable);
         where PRNOMGES = this.w_NOMEAP AND PRROWNUM = this.w_ROWNUM AND UPPER(PRNOMVAR) = UPPER(this.w_VARIABLE);
          into cursor _Curs_PRCFGGES
      endif
      if used('_Curs_PRCFGGES')
        select _Curs_PRCFGGES
        locate for 1=1
        do while not(eof())
        this.w_FOUND = .T.
        this.w_CPROWNUM = _Curs_PRCFGGES.CPROWNUM
          select _Curs_PRCFGGES
          continue
        enddo
        use
      endif
      if this.w_FOUND
        * --- Write into PRCFGGES
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PRCFGGES_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRCFGGES_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PRCFGGES_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PRVALVAR ="+cp_NullLink(cp_ToStrODBC(w_valore),'PRCFGGES','PRVALVAR');
              +i_ccchkf ;
          +" where ";
              +"PRNOMGES = "+cp_ToStrODBC(this.w_NOMEAP);
              +" and PRROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                 )
        else
          update (i_cTable) set;
              PRVALVAR = w_valore;
              &i_ccchkf. ;
           where;
              PRNOMGES = this.w_NOMEAP;
              and PRROWNUM = this.w_ROWNUM;
              and CPROWNUM = this.w_CPROWNUM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      else
        if this.w_NOHIDE And this.w_MASK.oPgFrm.Pages(w_NumPage).oPag.Controls(w_NumCtrl).bObbl And Empty(this.w_MASK.oPgFrm.Pages(w_NumPage).oPag.Controls(w_NumCtrl).value)
          * --- Da escludere
           
 Local l_olderr 
 l_olderr = On("Error") 
 On Error l_msg=message()
          INSERT INTO OUT_VAR_CUR (PROP) VALUES (this.w_VARIABLE)
          On Error &l_olderr
        else
          * --- Se sto salvando un control verifico la condizione di obbligatoriet�
          *     Se � obbligatorio e vuoto non lo salvo
          this.w_CPROWNUM = 0
          this.w_CPROWORD = 0
          * --- Determino ultimo cprownum e cproword
          * --- Select from PRCFGGES
          i_nConn=i_TableProp[this.PRCFGGES_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PRCFGGES_idx,2],.t.,this.PRCFGGES_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select MAX(CPROWNUM) as CPROWNUM ,MAX(CPROWORD) as CPROWORD  from "+i_cTable+" PRCFGGES ";
                +" where PRNOMGES="+cp_ToStrODBC(this.w_NOMEAP)+" AND PRROWNUM="+cp_ToStrODBC(this.w_ROWNUM)+"";
                +" group by PRNOMGES, PRROWNUM";
                 ,"_Curs_PRCFGGES")
          else
            select MAX(CPROWNUM) as CPROWNUM ,MAX(CPROWORD) as CPROWORD from (i_cTable);
             where PRNOMGES=this.w_NOMEAP AND PRROWNUM=this.w_ROWNUM;
             group by PRNOMGES, PRROWNUM;
              into cursor _Curs_PRCFGGES
          endif
          if used('_Curs_PRCFGGES')
            select _Curs_PRCFGGES
            locate for 1=1
            do while not(eof())
            this.w_CPROWNUM = _Curs_PRCFGGES.CPROWNUM
            this.w_CPROWORD = _Curs_PRCFGGES.CPROWORD
              select _Curs_PRCFGGES
              continue
            enddo
            use
          endif
          this.w_CPROWNUM = this.w_CPROWNUM + 1
          this.w_CPROWORD = this.w_CPROWORD + 10
          * --- Insert into PRCFGGES
          i_nConn=i_TableProp[this.PRCFGGES_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PRCFGGES_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRCFGGES_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"PRNOMGES"+",PRROWNUM"+",CPROWNUM"+",PRVALVAR"+",PRNOMVAR"+",PRNOMDES"+",CPROWORD"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_NOMEAP),'PRCFGGES','PRNOMGES');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'PRCFGGES','PRROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PRCFGGES','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(w_valore),'PRCFGGES','PRVALVAR');
            +","+cp_NullLink(cp_ToStrODBC(this.w_VARIABLE),'PRCFGGES','PRNOMVAR');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NOMDES),'PRCFGGES','PRNOMDES');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'PRCFGGES','CPROWORD');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'PRNOMGES',this.w_NOMEAP,'PRROWNUM',this.w_ROWNUM,'CPROWNUM',this.w_CPROWNUM,'PRVALVAR',w_valore,'PRNOMVAR',this.w_VARIABLE,'PRNOMDES',this.w_NOMDES,'CPROWORD',this.w_CPROWORD)
            insert into (i_cTable) (PRNOMGES,PRROWNUM,CPROWNUM,PRVALVAR,PRNOMVAR,PRNOMDES,CPROWORD &i_ccchkf. );
               values (;
                 this.w_NOMEAP;
                 ,this.w_ROWNUM;
                 ,this.w_CPROWNUM;
                 ,w_valore;
                 ,this.w_VARIABLE;
                 ,this.w_NOMDES;
                 ,this.w_CPROWORD;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
      endif
    endif
  endproc


  proc Init(oParentObject,w_MASK,w_GEST,w_ACTION,w_NOMEAP,w_ROWNUM)
    this.w_MASK=w_MASK
    this.w_GEST=w_GEST
    this.w_ACTION=w_ACTION
    this.w_NOMEAP=w_NOMEAP
    this.w_ROWNUM=w_ROWNUM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='GESTSJBD'
    this.cWorkTables[2]='PRCFGGES'
    this.cWorkTables[3]='CFG_GEST'
    this.cWorkTables[4]='MODDCONF'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_CFG_GEST')
      use in _Curs_CFG_GEST
    endif
    if used('_Curs_MODDCONF')
      use in _Curs_MODDCONF
    endif
    if used('_Curs_PRCFGGES')
      use in _Curs_PRCFGGES
    endif
    if used('_Curs_PRCFGGES')
      use in _Curs_PRCFGGES
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_MASK,w_GEST,w_ACTION,w_NOMEAP,w_ROWNUM"
endproc
