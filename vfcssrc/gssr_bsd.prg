* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gssr_bsd                                                        *
*              Storicizzazione documenti                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_276]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-06                                                      *
* Last revis.: 2017-03-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgssr_bsd",oParentObject)
return(i_retval)

define class tgssr_bsd as StdBatch
  * --- Local variables
  w_DATOBSO = ctod("  /  /  ")
  w_LSSERIAL = space(10)
  w_CONTA = 0
  w_NUMSPAZI = 0
  w_NUMSPAZ2 = 0
  w_NUMINV = space(6)
  w_MESS = space(250)
  w_OKPROD = .f.
  w_PRODST = .f.
  w_TESTATT = space(1)
  w_NUMDOCMAST = 0
  w_NUMDOCDETT = 0
  w_NUMDOCRATE = 0
  w_NUMDOCLOTT = 0
  w_NUMMVMMAST = 0
  w_NUMMVMDETT = 0
  w_NUMMVMLOTT = 0
  w_NUMANAGLOTT = 0
  w_NUMMATRIC = 0
  w_CODMAG = space(5)
  * --- WorkFile variables
  DOC_DETT_idx=0
  DOC_MAST_idx=0
  DOC_RATE_idx=0
  DOCSDETT_idx=0
  DOCSMAST_idx=0
  DOCSRATE_idx=0
  MVM_DETT_idx=0
  MVM_MAST_idx=0
  MVMSDETT_idx=0
  MVMSMAST_idx=0
  SDANALISI_idx=0
  MAGAZZIN_idx=0
  INVENTAR_idx=0
  LOG_STOR_idx=0
  CAN_TIER_idx=0
  SALDIART_idx=0
  SALLOTUBI_idx=0
  TMP_SALLOUB_idx=0
  LOTSIART_idx=0
  LOTTIART_idx=0
  PDA_DETT_idx=0
  RILEVAZI_idx=0
  MOVSMATR_idx=0
  TMP_STOR_idx=0
  MOVIMATR_idx=0
  STO_MATR_idx=0
  MATRICOL_idx=0
  CON_PAGA_idx=0
  SALDILOT_idx=0
  SALDISART_idx=0
  TMPSALDI_idx=0
  SALDICOM_idx=0
  SALDISCOM_idx=0
  TMPSALAGG_idx=0
  TMPSALDI1_idx=0
  SASLOTCOM_idx=0
  SALOTCOM_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch di Storicizzazione
    * --- Check per verificare che il numero di record storicizzati corrisponda a quelli effettivamente cancellati dal database in linea
    * --- Identatura messaggi
    this.w_NUMSPAZI = 30
    this.w_NUMSPAZ2 = 20
    this.w_TESTATT = IIF(g_APPLICATION <> "ADHOC REVOLUTION",g_DIBA,g_PROD)
    this.oParentObject.w_Msg = ""
    if !ah_YesNo("ATTENZIONE%0Verr� effettuata la storicizzazione dei documenti e dei movimenti di magazzino%0Confermi l'elaborazione?")
      i_retcode = 'stop'
      return
    endif
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    AddMsgNL("Elaborazione iniziata alle: %1",this.oparentobject,Time() )
    * --- Eseguo tutto sotto transazione, sebbene questa operazione potrebbe
    *     essere molto pesante (dala maschera � possibile disabilitare la transazione)
    * --- Try
    local bErr_055F1CD0
    bErr_055F1CD0=bTrsErr
    this.Try_055F1CD0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      if this.oParentObject.w_TRAN="S"
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
      endif
      ah_ErrorMsg("Storicizzazione terminata con errore: %1","!","", message())
      * --- Registro errore nel log
      * --- begin transaction
      cp_BeginTrs()
      this.w_LSSERIAL = cp_GetProg("LOG_STOR", "STLOG", this.w_LSSERIAL, i_CODAZI)
      if Not empty(Message())
        AddMsg("%0Errore: %1", this.oparentobject, Message() )
      endif
      * --- Insert into LOG_STOR
      i_nConn=i_TableProp[this.LOG_STOR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.LOG_STOR_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LOG_STOR_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"LSSERIAL"+",LSCODESE"+",LG__TIPO"+",LG___OPE"+",LGCODUTE"+",LG__DATA"+",LG__NOTE"+",LGPRODOK"+",LG_ESITO"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_LSSERIAL),'LOG_STOR','LSSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODESE),'LOG_STOR','LSCODESE');
        +","+cp_NullLink(cp_ToStrODBC("L"),'LOG_STOR','LG__TIPO');
        +","+cp_NullLink(cp_ToStrODBC("A"),'LOG_STOR','LG___OPE');
        +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'LOG_STOR','LGCODUTE');
        +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'LOG_STOR','LG__DATA');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MSG),'LOG_STOR','LG__NOTE');
        +","+cp_NullLink(cp_ToStrODBC(" "),'LOG_STOR','LGPRODOK');
        +","+cp_NullLink(cp_ToStrODBC("N"),'LOG_STOR','LG_ESITO');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'LSSERIAL',this.w_LSSERIAL,'LSCODESE',this.oParentObject.w_CODESE,'LG__TIPO',"L",'LG___OPE',"A",'LGCODUTE',i_CODUTE,'LG__DATA',i_DATSYS,'LG__NOTE',this.oParentObject.w_MSG,'LGPRODOK'," ",'LG_ESITO',"N")
        insert into (i_cTable) (LSSERIAL,LSCODESE,LG__TIPO,LG___OPE,LGCODUTE,LG__DATA,LG__NOTE,LGPRODOK,LG_ESITO &i_ccchkf. );
           values (;
             this.w_LSSERIAL;
             ,this.oParentObject.w_CODESE;
             ,"L";
             ,"A";
             ,i_CODUTE;
             ,i_DATSYS;
             ,this.oParentObject.w_MSG;
             ," ";
             ,"N";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error='Errore inserimento log'
        return
      endif
      * --- commit
      cp_EndTrs(.t.)
    endif
    bTrsErr=bTrsErr or bErr_055F1CD0
    * --- End
  endproc
  proc Try_055F1CD0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if this.oParentObject.w_TRAN="S"
      * --- begin transaction
      cp_BeginTrs()
      AddMsgNL("Elaborazione sotto transazione",this.oparentobject)
    else
      AddMsgNL("Elaborazione fuori transazione",this.oparentobject)
    endif
    this.w_OKPROD = True
    if this.w_PRODST and this.w_TESTATT="S" AND 1=0
      GSSR_BSO(this,"STPRO")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if not this.w_OKPROD
        i_retcode = 'stop'
        return
      endif
    endif
    AddMsgNL("Aggiornamento saldi articoli dello storico",this.oparentobject)
    * --- Try
    local bErr_055F5C60
    bErr_055F5C60=bTrsErr
    this.Try_055F5C60()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      AddMsgNL("Errore aggiornamento saldi articoli dello storico: %1",this.oparentobject, Message() )
      * --- Raise
      i_Error=Message()
      return
    endif
    bTrsErr=bTrsErr or bErr_055F5C60
    * --- End
    AddMsgNL("%1 records modificati: %2",this.oparentobject, space(this.w_NUMSPAZ2), Str( i_Rows ) )
    * --- Devo falra direttamente sui movimenti in linea prima che vengano eliminati.
    *     Non posso farlo sui dati storici al termine perch� questi saldi possono essere 
    *     utilizzati a seguito di eliminazione dei movimenti storici
    AddMsgNL("Aggiornamento saldi di magazzino fuori linea",this.oparentobject)
    * --- Try
    local bErr_055F4040
    bErr_055F4040=bTrsErr
    this.Try_055F4040()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      AddMsgNL("Errore saldi di magazzino: %1",this.oparentobject, Message() )
      * --- Raise
      i_Error=Message()
      return
    endif
    bTrsErr=bTrsErr or bErr_055F4040
    * --- End
    AddMsgNL("%1 records modificati: %2",this.oparentobject, space(this.w_NUMSPAZ2), Str( i_Rows ) )
    AddMsgNL("Aggiornamento saldi lotti / ubicazioni fuori linea",this.oparentobject)
    * --- Try
    local bErr_055F1FD0
    bErr_055F1FD0=bTrsErr
    this.Try_055F1FD0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      AddMsgNL("Errore saldi lotti / ubicazioni: %1",this.oparentobject, Message())
      * --- Raise
      i_Error=Message()
      return
    endif
    bTrsErr=bTrsErr or bErr_055F1FD0
    * --- End
    this.Page_3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    AddMsgNL("%1: OK", this.oparentobject, space(this.w_NUMSPAZI) )
    * --- Eseguo Inserimento Documenti
    *     =========================================================
    AddMsgNL("Storicizzazione documenti",this.oparentobject)
    AddMsgNL("%1 - Documenti (master)", this.oparentobject, space(this.w_NUMSPAZ2) )
    * --- Try
    local bErr_055C85A0
    bErr_055C85A0=bTrsErr
    this.Try_055C85A0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      AddMsgNL("Errore (DOC_MAST): %1", this.oparentobject, Message() )
      * --- Raise
      i_Error=Message()
      return
    endif
    bTrsErr=bTrsErr or bErr_055C85A0
    * --- End
    AddMsgNL("%1 records: %2", this.oparentobject, space(this.w_NUMSPAZ2), Str( i_Rows ) )
    this.w_NUMDOCMAST = i_Rows
    AddMsgNL("%1 - Documenti (detail)", this.oparentobject, space(this.w_NUMSPAZ2) )
    * --- Try
    local bErr_055C8060
    bErr_055C8060=bTrsErr
    this.Try_055C8060()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      AddMsgNL("Errore (DOC_DETT): %1", this.oparentobject, Message() )
      * --- Raise
      i_Error=Message()
      return
    endif
    bTrsErr=bTrsErr or bErr_055C8060
    * --- End
    AddMsgNL("%1 records: %2", this.oparentobject, space(this.w_NUMSPAZ2), Str( i_Rows ) )
    this.w_NUMDOCDETT = i_Rows
    AddMsgNL("%1 - Rate", this.oparentobject, space(this.w_NUMSPAZ2) )
    * --- Try
    local bErr_055C7A00
    bErr_055C7A00=bTrsErr
    this.Try_055C7A00()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      AddMsgNL("Errore (DOC_RATE): %1", this.oparentobject, Message() )
      * --- Raise
      i_Error=Message()
      return
    endif
    bTrsErr=bTrsErr or bErr_055C7A00
    * --- End
    AddMsgNL("%1 records: %2", this.oparentobject, space(this.w_NUMSPAZ2), Str( i_Rows ) )
    this.w_NUMDOCRATE = i_Rows
    if g_APPLICATION = "ADHOC REVOLUTION" And g_VEFA ="S"
      GSVA_BRS(this,"STORICO","S", this.oParentObject.w_CODESE)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if g_APPLICATION <> "ADHOC REVOLUTION"
      AddMsgNL("%1 - Lotti", this.oparentobject, space(this.w_NUMSPAZ2) )
      * --- Try
      local bErr_055D47B0
      bErr_055D47B0=bTrsErr
      this.Try_055D47B0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        AddMsgNL("Errore (MOVILOTT x documenti): %1", this.oparentobject, Message() )
        * --- Raise
        i_Error=Message()
        return
      endif
      bTrsErr=bTrsErr or bErr_055D47B0
      * --- End
      AddMsgNL("%1 records: %2", this.oparentobject, space(this.w_NUMSPAZ2), Str( i_Rows ) )
    endif
    this.w_NUMDOCLOTT = i_Rows
    * --- =========================================================
    *     Creo Tabella Temporanea con Chiavi Documenti\Movimenti Storicizzati
    *     per storicizzazione matricole
    *     =========================================================
    * --- Create temporary table TMP_STOR
    i_nIdx=cp_AddTableDef('TMP_STOR') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('GSSR_SMM',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMP_STOR_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- ==========================================================
    *     Cancellazione dati in linea - Documenti 
    *     ==========================================================
    AddMsgNL("Cancellazione dati in linea - documenti",this.oparentobject)
    AddMsgNL("%1 - Scadenze documenti", this.oparentobject, space(this.w_NUMSPAZ2) )
    * --- Delete from DOC_RATE
    i_nConn=i_TableProp[this.DOC_RATE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_RATE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.DOCSRATE_idx,2])
      i_cWhere=i_cTable+".RSSERIAL = "+i_cQueryTable+".RSSERIAL";
    
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where ";
            +""+i_cTable+".RSSERIAL = "+i_cQueryTable+".RSSERIAL";
            +")")
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error='Errore eliminazione scadenze documenti'
      return
    endif
    if i_Rows<>this.w_NUMDOCRATE
      * --- Raise
      i_Error=ah_Msgformat("Errore - creati %1 records ma eliminati dall'archivio in linea %2", Alltrim(Str(this.w_NUMDOCRATE)), Alltrim(Str(i_ROWS)) )
      return
    endif
    AddMsgNL("%1 records: %2", this.oparentobject, space(this.w_NUMSPAZ2), Str( i_Rows ) )
    * --- Elimino Figlio Integrato delle Contropartite
    * --- Delete from CON_PAGA
    i_nConn=i_TableProp[this.CON_PAGA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CON_PAGA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".CPSERIAL = "+i_cQueryTable+".CPSERIAL";
    
      do vq_exec with 'GSSR_QCP',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error='Errore eliminazione scadenze documenti'
      return
    endif
    if g_APPLICATION <> "ADHOC REVOLUTION"
      AddMsgNL("%1 - Mov.lotti / ubicazioni documenti", this.oparentobject, space(this.w_NUMSPAZ2) )
      GSSR_BGE(this,"DELE","MOVILOTT")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if i_Rows<>this.w_NUMDOCLOTT
        * --- Raise
        i_Error=ah_Msgformat("Errore - creati %1 records ma eliminati dall'archivio in linea %2", Alltrim(Str(this.w_NUMDOCLOTT)), Alltrim(Str(i_ROWS)) )
        return
      endif
      AddMsgNL("%1 records: %2", this.oparentobject, space(this.w_NUMSPAZ2), Str( i_Rows ) )
    endif
    AddMsgNL("%1 - Dettaglio documenti", this.oparentobject, space(this.w_NUMSPAZ2) )
    * --- Delete from DOC_DETT
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.DOCSMAST_idx,2])
      i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
    
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where ";
            +""+i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
            +")")
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error='Errore eliminazione dettaglio documenti'
      return
    endif
    if i_Rows<>this.w_NUMDOCDETT
      * --- Raise
      i_Error=ah_Msgformat("Errore - creati %1 records ma eliminati dall'archivio in linea %2", Alltrim(Str(this.w_NUMDOCDETT)), Alltrim(Str(i_ROWS)) )
      return
    endif
    AddMsgNL("%1 records: %2", this.oparentobject, space(this.w_NUMSPAZ2), Str( i_Rows ) )
    AddMsgNL("%1 records: %2", this.oparentobject, space(this.w_NUMSPAZ2), Str( i_Rows ) )
    AddMsgNL("%1 - Testate documenti", this.oparentobject, space(this.w_NUMSPAZ2) )
    * --- Delete from DOC_MAST
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.DOCSMAST_idx,2])
      i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
    
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where ";
            +""+i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
            +")")
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error='Errore eliminazione testate documenti'
      return
    endif
    if i_Rows<>this.w_NUMDOCMAST
      * --- Raise
      i_Error=ah_Msgformat("Errore - creati %1 records ma eliminati dall'archivio in linea %2", Alltrim(Str(this.w_NUMDOCMAST)), Alltrim(Str(i_ROWS)) )
      return
    endif
    AddMsgNL("%1 records: %2", this.oparentobject, space(this.w_NUMSPAZ2), Str( i_Rows ) )
    AddMsgNL("Storicizzazione movimenti di magazzino",this.oparentobject)
    AddMsgNL("%1 - Testate movimenti di magazzino", this.oparentobject, space(this.w_NUMSPAZ2) )
    * --- Try
    local bErr_055CD460
    bErr_055CD460=bTrsErr
    this.Try_055CD460()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      AddMsgNL("Errore (MVM_MAST): %1", this.oparentobject, Message() )
      * --- Raise
      i_Error=Message()
      return
    endif
    bTrsErr=bTrsErr or bErr_055CD460
    * --- End
    this.w_NUMMVMMAST = i_Rows
    AddMsgNL("%1 records: %2", this.oparentobject, space(this.w_NUMSPAZ2), Str( i_Rows ) )
    AddMsgNL("%1 - Dettaglio movimenti", this.oparentobject, space(this.w_NUMSPAZ2) )
    * --- Try
    local bErr_055CCC20
    bErr_055CCC20=bTrsErr
    this.Try_055CCC20()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      AddMsgNL("Errore (MVM_DETT): %1", this.oparentobject, Message() )
      * --- Raise
      i_Error=Message()
      return
    endif
    bTrsErr=bTrsErr or bErr_055CCC20
    * --- End
    this.w_NUMMVMDETT = i_Rows
    AddMsgNL("%1 records: %2", this.oparentobject, space(this.w_NUMSPAZ2), Str( i_Rows ) )
    if g_APPLICATION <> "ADHOC REVOLUTION"
      AddMsgNL("%1 - Mov.lotti / ubicazioni (magazzino)", this.oparentobject, space(this.w_NUMSPAZ2) )
      * --- Try
      local bErr_055CB420
      bErr_055CB420=bTrsErr
      this.Try_055CB420()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        AddMsgNL("Errore (MOVILOTT x documenti): %1", this.oparentobject, Message() )
        * --- Raise
        i_Error=Message()
        return
      endif
      bTrsErr=bTrsErr or bErr_055CB420
      * --- End
      this.w_NUMMVMLOTT = i_Rows
      AddMsgNL("%1 records: %2", this.oparentobject, space(this.w_NUMSPAZ2), Str( i_Rows ) )
      AddMsgNL("Cancellazione dati in linea - mov.di magazzino",this.oparentobject)
      AddMsgNL("%1 - Mov.lotti / ubicazioni (magazzino)", this.oparentobject, space(this.w_NUMSPAZ2) )
      GSSR_BGE(this,"DELE","MOVILOTT")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if i_Rows<>this.w_NUMMVMLOTT
        * --- Raise
        i_Error=ah_Msgformat("Errore - creati %1 records ma eliminati dall'archivio in linea %2", Alltrim(Str(this.w_NUMMVMLOTT)), Alltrim(Str(i_ROWS)) )
        return
      endif
      AddMsgNL("%1 records: %2", this.oparentobject, space(this.w_NUMSPAZ2), Str( i_Rows ) )
    else
      AddMsgNL("Eliminazione dati in linea - mov.di magazzino",this.oparentobject)
    endif
    AddMsgNL("%1 - Dettaglio movimenti", this.oparentobject, space(this.w_NUMSPAZ2) )
    * --- Delete from MVM_DETT
    i_nConn=i_TableProp[this.MVM_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.MVMSMAST_idx,2])
      i_cWhere=i_cTable+".MMSERIAL = "+i_cQueryTable+".MMSERIAL";
    
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where ";
            +""+i_cTable+".MMSERIAL = "+i_cQueryTable+".MMSERIAL";
            +")")
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error='Errore eliminazione dettaglio movimenti di magazzino in linea'
      return
    endif
    if i_Rows<>this.w_NUMMVMDETT
      * --- Raise
      i_Error=ah_Msgformat("Errore - creati %1 records ma eliminati dall'archivio in linea %2", Alltrim(Str(this.w_NUMMVMDETT)), Alltrim(Str(i_ROWS)) )
      return
    endif
    AddMsgNL("%1 records: %2", this.oparentobject, space(this.w_NUMSPAZ2), Str( i_Rows ) )
    AddMsgNL("%1 - Testate movimenti", this.oparentobject, space(this.w_NUMSPAZ2) )
    * --- Delete from MVM_MAST
    i_nConn=i_TableProp[this.MVM_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MVM_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.MVMSMAST_idx,2])
      i_cWhere=i_cTable+".MMSERIAL = "+i_cQueryTable+".MMSERIAL";
    
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where ";
            +""+i_cTable+".MMSERIAL = "+i_cQueryTable+".MMSERIAL";
            +")")
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error='Errore cancellazione movimenti di magazzino in linea (testata)'
      return
    endif
    if i_Rows<>this.w_NUMMVMMAST
      * --- Raise
      i_Error=ah_Msgformat("Errore - creati %1 records ma eliminati dall'archivio in linea %2", Alltrim(Str(this.w_NUMMVMMAST)), Alltrim(Str(i_ROWS)) )
      return
    endif
    AddMsgNL("%1 records: %2", this.oparentobject, space(this.w_NUMSPAZ2), Str( i_Rows ) )
    AddMsgNL("Storicizzazione matricole",this.oparentobject)
    AddMsgNL("%1 - Movimenti matricole", this.oparentobject, space(this.w_NUMSPAZ2) )
    * --- Try
    local bErr_055D0B20
    bErr_055D0B20=bTrsErr
    this.Try_055D0B20()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      AddMsgNL("Errore (MOVIMATR): %1", this.oparentobject, Message() )
      * --- Raise
      i_Error=Message()
      return
    endif
    bTrsErr=bTrsErr or bErr_055D0B20
    * --- End
    AddMsgNL("%1 records: %2", this.oparentobject, space(this.w_NUMSPAZ2), Str( i_Rows ) )
    this.w_NUMMATRIC = i_Rows
    * --- ==========================================================
    *     Eliminazione Saldi lotti riferiti a matricole delle quali � gi� stato storicizzato
    *     il relativo movimento di carico.
    *     ==========================================================
    AddMsgNL("%1 - Saldi carichi gi� storicizzati eliminati", this.oparentobject, space(this.w_NUMSPAZ2) )
    * --- Delete from SALLOTUBI
    i_nConn=i_TableProp[this.SALLOTUBI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALLOTUBI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".SUCODICE = "+i_cQueryTable+".SUCODICE";
            +" and "+i_cTable+".SUCODSAL = "+i_cQueryTable+".SUCODSAL";
            +" and "+i_cTable+".SUCODMAG = "+i_cQueryTable+".SUCODMAG";
    
      do vq_exec with 'GSSRDSTD',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error='Errore nella cancellazione saldi lotti ubicazioni'
      return
    endif
    AddMsgNL("%1 records: %2", this.oparentobject, space(this.w_NUMSPAZ2), Str( i_Rows ) )
    * --- ===========================================================
    *     Inserimento Carichi Matricole (ultimi o con scarichi fuori esercizio) nei Saldi
    *     ===========================================================
    AddMsgNL("%1 - Saldi carichi inseriti", this.oparentobject, space(this.w_NUMSPAZ2) )
    * --- Insert into SALLOTUBI
    i_nConn=i_TableProp[this.SALLOTUBI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALLOTUBI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSSR_SMS",this.SALLOTUBI_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore Inserimento Saldi lotti\ubicazioni\matricole'
      return
    endif
    AddMsgNL("%1 records: %2", this.oparentobject, space(this.w_NUMSPAZ2), Str( i_Rows ) )
    * --- ==========================================================
    *     Eseguo cancellazione Movimenti Matricole Storicizzati
    *     ==========================================================
    AddMsgNL("Cancellazione dati in linea - mov. matricole",this.oparentobject)
    * --- Delete from MOVIMATR
    i_nConn=i_TableProp[this.MOVIMATR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.MOVSMATR_idx,2])
      i_cWhere=i_cTable+".MTSERIAL = "+i_cQueryTable+".MTSERIAL";
            +" and "+i_cTable+".MTNUMRIF = "+i_cQueryTable+".MTNUMRIF";
    
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where ";
            +""+i_cTable+".MTSERIAL = "+i_cQueryTable+".MTSERIAL";
            +" and "+i_cTable+".MTNUMRIF = "+i_cQueryTable+".MTNUMRIF";
            +")")
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error='Errore eliminazione movimenti matricole'
      return
    endif
    if i_Rows<>this.w_NUMMATRIC
      * --- Raise
      i_Error=ah_Msgformat("Errore - creati %1 records ma eliminati dall'archivio in linea %2", Alltrim(Str(this.w_NUMMATRIC)), Alltrim(Str(i_ROWS)) )
      return
    else
      AddMsgNL("%1 records: %2", this.oparentobject, space(this.w_NUMSPAZ2), Str( i_Rows ) )
    endif
    * --- Elimino Tabella Temporanea Chiavi Documenti\Movimenti Storicizzati
    * --- Drop temporary table TMP_STOR
    i_nIdx=cp_GetTableDefIdx('TMP_STOR')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_STOR')
    endif
    * --- ==========================================================
    * --- Aggiornamento anagrafica saldi/lotti
    * --- Insert into SALDILOT
    i_nConn=i_TableProp[this.SALDILOT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDILOT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSAR_SLs",this.SALDILOT_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Write into SALDILOT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDILOT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDILOT_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="SUCODART,SUCODMAG,SUCODLOT ,SUCODUBI "
      do vq_exec with 'GSAR_SLu',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDILOT_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="SALDILOT.SUCODART = _t2.SUCODART";
              +" and "+"SALDILOT.SUCODMAG = _t2.SUCODMAG";
              +" and "+"SALDILOT.SUCODLOT  = _t2.SUCODLOT ";
              +" and "+"SALDILOT.SUCODUBI  = _t2.SUCODUBI ";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SUQTAPRO = _t2.SUQTAPRO";
          +",SUQTRPRO = _t2.SUQTRPRO";
          +i_ccchkf;
          +" from "+i_cTable+" SALDILOT, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="SALDILOT.SUCODART = _t2.SUCODART";
              +" and "+"SALDILOT.SUCODMAG = _t2.SUCODMAG";
              +" and "+"SALDILOT.SUCODLOT  = _t2.SUCODLOT ";
              +" and "+"SALDILOT.SUCODUBI  = _t2.SUCODUBI ";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDILOT, "+i_cQueryTable+" _t2 set ";
          +"SALDILOT.SUQTAPRO = _t2.SUQTAPRO";
          +",SALDILOT.SUQTRPRO = _t2.SUQTRPRO";
          +Iif(Empty(i_ccchkf),"",",SALDILOT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="SALDILOT.SUCODART = t2.SUCODART";
              +" and "+"SALDILOT.SUCODMAG = t2.SUCODMAG";
              +" and "+"SALDILOT.SUCODLOT  = t2.SUCODLOT ";
              +" and "+"SALDILOT.SUCODUBI  = t2.SUCODUBI ";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDILOT set (";
          +"SUQTAPRO,";
          +"SUQTRPRO";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.SUQTAPRO,";
          +"t2.SUQTRPRO";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="SALDILOT.SUCODART = _t2.SUCODART";
              +" and "+"SALDILOT.SUCODMAG = _t2.SUCODMAG";
              +" and "+"SALDILOT.SUCODLOT  = _t2.SUCODLOT ";
              +" and "+"SALDILOT.SUCODUBI  = _t2.SUCODUBI ";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDILOT set ";
          +"SUQTAPRO = _t2.SUQTAPRO";
          +",SUQTRPRO = _t2.SUQTRPRO";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".SUCODART = "+i_cQueryTable+".SUCODART";
              +" and "+i_cTable+".SUCODMAG = "+i_cQueryTable+".SUCODMAG";
              +" and "+i_cTable+".SUCODLOT  = "+i_cQueryTable+".SUCODLOT ";
              +" and "+i_cTable+".SUCODUBI  = "+i_cQueryTable+".SUCODUBI ";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SUQTAPRO = (select SUQTAPRO from "+i_cQueryTable+" where "+i_cWhere+")";
          +",SUQTRPRO = (select SUQTRPRO from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    AddMsgNL("Cancellazione dati in linea - rettifiche inventariali",this.oparentobject)
    * --- Delete from RILEVAZI
    i_nConn=i_TableProp[this.RILEVAZI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RILEVAZI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".DRSERIAL = "+i_cQueryTable+".DRSERIAL";
    
      do vq_exec with 'GSSR4BSD',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    AddMsgNL("%1 records: %2", this.oparentobject, space(this.w_NUMSPAZ2), Str( i_Rows ) )
    * --- Storicizzo Anagrafica Lotti, Storicizzo i Lotti "Esauriti" e non pi� movimentati
    *     in linea
    AddMsgNL("Storicizzo anagrafica lotti",this.oparentobject)
    * --- Try
    local bErr_055D2B30
    bErr_055D2B30=bTrsErr
    this.Try_055D2B30()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      AddMsgNL("Errore (LOTTIART, anagrafica lotti ): %1", this.oparentobject, Message() )
      * --- Raise
      i_Error=Message()
      return
    endif
    bTrsErr=bTrsErr or bErr_055D2B30
    * --- End
    this.w_NUMANAGLOTT = i_Rows
    AddMsgNL("%1 records: %2", this.oparentobject, space(this.w_NUMSPAZ2), Str( i_Rows ) )
    AddMsgNL("%1 - Elimino anagrafica lotti in linea", this.oparentobject, space(this.w_NUMSPAZ2) )
    * --- Elimino dall'anagrafica i Lotti storicizzati
    * --- Delete from LOTTIART
    i_nConn=i_TableProp[this.LOTTIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LOTTIART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.LOTSIART_idx,2])
      i_cWhere=i_cTable+".LOCODICE = "+i_cQueryTable+".LOCODICE";
            +" and "+i_cTable+".LOCODART = "+i_cQueryTable+".LOCODART";
    
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where ";
            +""+i_cTable+".LOCODICE = "+i_cQueryTable+".LOCODICE";
            +" and "+i_cTable+".LOCODART = "+i_cQueryTable+".LOCODART";
            +")")
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    if i_Rows<>this.w_NUMANAGLOTT
      AddMsgNL("Errore - creati %1 records ma eliminati dall'archivio in linea %2",this.oparentobject, Alltrim(Str( this.w_NUMANAGLOTT )), Alltrim(Str(i_ROWS)) )
      * --- Raise
      i_Error=ah_Msgformat("Errore - creati %1 records ma eliminati dall'archivio in linea %2", Alltrim(Str( this.w_NUMANAGLOTT )) , Alltrim(Str(i_ROWS)) )
      return
    endif
    AddMsgNL("%1 records: %2", this.oparentobject, space(this.w_NUMSPAZ2), Str( i_Rows ) )
    * --- Storicizzo Anagrafica Matricole, e non pi� movimentate
    AddMsgNL("Storicizzo anagrafica matricole",this.oparentobject)
    * --- Try
    local bErr_055F2630
    bErr_055F2630=bTrsErr
    this.Try_055F2630()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      AddMsgNL("Errore (MATRICOL, anagrafica matricole) : %1", this.oparentobject, Message() )
      * --- Raise
      i_Error=Message()
      return
    endif
    bTrsErr=bTrsErr or bErr_055F2630
    * --- End
    this.w_NUMANAGLOTT = i_Rows
    AddMsgNL("%1 records: %2", this.oparentobject, space(this.w_NUMSPAZ2), Str( i_Rows ) )
    AddMsgNL("%1 - Elimino anagrafica matricole in linea", this.oparentobject, space(this.w_NUMSPAZ2) )
    * --- Elimino dall'anagrafica i Lotti storicizzati
    * --- Delete from MATRICOL
    i_nConn=i_TableProp[this.MATRICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MATRICOL_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".AMKEYSAL = "+i_cQueryTable+".AMKEYSAL";
            +" and "+i_cTable+".AMCODICE = "+i_cQueryTable+".AMCODICE";
    
      do vq_exec with 'GSSR1QDM',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    if i_Rows <>this.w_NUMANAGLOTT
      AddMsgNL("Errore - creati %1 records ma eliminati dall'archivio in linea %2",this.oparentobject, Alltrim(Str( this.w_NUMANAGLOTT )), Alltrim(Str(i_ROWS)) )
      * --- Raise
      i_Error=ah_Msgformat("Errore - creati %1 records ma eliminati dall'archivio in linea %2", Alltrim(Str( this.w_NUMANAGLOTT )) , Alltrim(Str(i_ROWS)) )
      return
    endif
    AddMsgNL("%1 records: %2", this.oparentobject, space(this.w_NUMSPAZ2), Str( i_Rows ) )
    if this.w_PRODST and g_COLA="S" AND 1=0
      * --- Pulisce il dettaglio della generazione Doc. di conto Lavoro
      GSSR_BSO(this,"DELPG")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if not this.w_OKPROD
        i_retcode = 'stop'
        return
      endif
    endif
    AddMsgNL("Eliminazione PDA ordinate",this.oparentobject)
    * --- Delete from PDA_DETT
    i_nConn=i_TableProp[this.PDA_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PDA_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"PDSTATUS = "+cp_ToStrODBC("O");
            +" and PDDATEVA >= "+cp_ToStrODBC(this.oParentObject.w_xesiniese);
            +" and PDDATEVA <= "+cp_ToStrODBC(this.oParentObject.w_xesfinese);
             )
    else
      delete from (i_cTable) where;
            PDSTATUS = "O";
            and PDDATEVA >= this.oParentObject.w_xesiniese;
            and PDDATEVA <= this.oParentObject.w_xesfinese;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    AddMsgNL("%1 records: %2", this.oparentobject, space(this.w_NUMSPAZ2), Str( i_Rows ) )
    AddMsgNL("Creazione Log su file di analisi",this.oparentobject)
    * --- Write into SDANALISI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SDANALISI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SDANALISI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SDANALISI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ANSTO_OK ="+cp_NullLink(cp_ToStrODBC("T"),'SDANALISI','ANSTO_OK');
          +i_ccchkf ;
      +" where ";
          +"ANCODESE = "+cp_ToStrODBC(this.oParentObject.w_CODESE);
          +" and ANSTO_OK = "+cp_ToStrODBC("S");
             )
    else
      update (i_cTable) set;
          ANSTO_OK = "T";
          &i_ccchkf. ;
       where;
          ANCODESE = this.oParentObject.w_CODESE;
          and ANSTO_OK = "S";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errore scrittura flag log di analsisi'
      return
    endif
    AddMsgNL("%1 records: %2", this.oparentobject, space(this.w_NUMSPAZ2), Str( i_Rows ) )
    * --- Marco le commesse come da storicizzare
    if g_COMM="S"
      AddMsgNL("Modifico lo stato delle commesse aperte nell'esercizio", this.oparentobject)
      i_IndexOracle = IIF(upper(CP_DBTYPE)="ORACLE","CNCODCAN"," ")
      * --- Write into CAN_TIER
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.CAN_TIER_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="CNCODCAN"
        do vq_exec with 'GSSR2BSD',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CAN_TIER_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="CAN_TIER.CNCODCAN = _t2.CNCODCAN";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CN_STATO ="+cp_NullLink(cp_ToStrODBC("S"),'CAN_TIER','CN_STATO');
            +i_ccchkf;
            +" from "+i_cTable+" CAN_TIER, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="CAN_TIER.CNCODCAN = _t2.CNCODCAN";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CAN_TIER, "+i_cQueryTable+" _t2 set ";
        +"CAN_TIER.CN_STATO ="+cp_NullLink(cp_ToStrODBC("S"),'CAN_TIER','CN_STATO');
            +Iif(Empty(i_ccchkf),"",",CAN_TIER.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="CAN_TIER.CNCODCAN = t2.CNCODCAN";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CAN_TIER set (";
            +"CN_STATO";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +cp_NullLink(cp_ToStrODBC("S"),'CAN_TIER','CN_STATO')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="CAN_TIER.CNCODCAN = _t2.CNCODCAN";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CAN_TIER set ";
        +"CN_STATO ="+cp_NullLink(cp_ToStrODBC("S"),'CAN_TIER','CN_STATO');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".CNCODCAN = "+i_cQueryTable+".CNCODCAN";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CN_STATO ="+cp_NullLink(cp_ToStrODBC("S"),'CAN_TIER','CN_STATO');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      AddMsgNL("%1 Commesse: %2", this.oparentobject, space(this.w_NUMSPAZ2), Str( i_Rows ) )
    endif
    if this.oParentObject.w_TRAN="S"
      * --- commit
      cp_EndTrs(.t.)
    endif
    AddMsgNL("%0Elaborazione terminata alle: %1",this.oparentobject,Time())
    ah_ErrorMsg("Storicizzazione terminata","!","")
    * --- Drop temporary table TMP_SALLOUB
    i_nIdx=cp_GetTableDefIdx('TMP_SALLOUB')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_SALLOUB')
    endif
    * --- Drop temporary table TMPSALDI
    i_nIdx=cp_GetTableDefIdx('TMPSALDI')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPSALDI')
    endif
    return
  proc Try_055F5C60()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    i_IndexOracle = IIF(upper(CP_DBTYPE)="ORACLE","SLCODICE,SLCODMAG"," ")
    * --- Elimino in toto i saldi articoli dello storico. Il loro contenuto � compreso nel
    *     temporaneo TMPSALDI
    * --- Create temporary table TMPSALDI
    i_nIdx=cp_AddTableDef('TMPSALDI') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('GSSRUSAL',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPSALDI_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Delete from SALDISART
    i_nConn=i_TableProp[this.SALDISART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDISART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"1 = "+cp_ToStrODBC(1);
             )
    else
      delete from (i_cTable) where;
            1 = 1;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Insert into SALDISART
    i_nConn=i_TableProp[this.SALDISART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDISART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMPSALDI_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.SALDISART_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_055F4040()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Non considero il caso di articolo movimentato nei dati passati a storico 
    *     e non presente nell'anagrafica Saldi.
    *     La cosa migliore � costringere, prima di stroicizzare, ad eseguire
    *     una ricostruzione saldi di magazzino
    i_IndexOracle = IIF(upper(CP_DBTYPE)="ORACLE","SLCODICE,SLCODMAG"," ")
    * --- Write into SALDIART
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="SLCODICE,SLCODMAG"
      do vq_exec with 'GSSR_SAL',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="SALDIART.SLCODICE = _t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = _t2.SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLQTAPRO = SALDIART.SLQTAPRO+_t2.SLQTAPRO";
          +",SLQTRPRO = SALDIART.SLQTRPRO+_t2.SLQTRPRO";
          +",SLQTFPRO = SALDIART.SLQTFPRO+_t2.SLQTFPRO";
          +i_ccchkf;
          +" from "+i_cTable+" SALDIART, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="SALDIART.SLCODICE = _t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = _t2.SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDIART, "+i_cQueryTable+" _t2 set ";
          +"SALDIART.SLQTAPRO = SALDIART.SLQTAPRO+_t2.SLQTAPRO";
          +",SALDIART.SLQTRPRO = SALDIART.SLQTRPRO+_t2.SLQTRPRO";
          +",SALDIART.SLQTFPRO = SALDIART.SLQTFPRO+_t2.SLQTFPRO";
          +Iif(Empty(i_ccchkf),"",",SALDIART.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="SALDIART.SLCODICE = t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = t2.SLCODMAG";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDIART set (";
          +"SLQTAPRO,";
          +"SLQTRPRO,";
          +"SLQTFPRO";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"SLQTAPRO+t2.SLQTAPRO,";
          +"SLQTRPRO+t2.SLQTRPRO,";
          +"SLQTFPRO+t2.SLQTFPRO";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="SALDIART.SLCODICE = _t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = _t2.SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDIART set ";
          +"SLQTAPRO = SALDIART.SLQTAPRO+_t2.SLQTAPRO";
          +",SLQTRPRO = SALDIART.SLQTRPRO+_t2.SLQTRPRO";
          +",SLQTFPRO = SALDIART.SLQTFPRO+_t2.SLQTFPRO";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".SLCODICE = "+i_cQueryTable+".SLCODICE";
              +" and "+i_cTable+".SLCODMAG = "+i_cQueryTable+".SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLQTAPRO = (select "+i_cTable+".SLQTAPRO+SLQTAPRO from "+i_cQueryTable+" where "+i_cWhere+")";
          +",SLQTRPRO = (select "+i_cTable+".SLQTRPRO+SLQTRPRO from "+i_cQueryTable+" where "+i_cWhere+")";
          +",SLQTFPRO = (select "+i_cTable+".SLQTFPRO+SLQTFPRO from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errore aggiornamento saldi magazzino'
      return
    endif
    return
  proc Try_055F1FD0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Tento l'inserimento per le combinazioni non presenti
    * --- Create temporary table TMP_SALLOUB
    i_nIdx=cp_AddTableDef('TMP_SALLOUB') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('GSSR_LUS',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMP_SALLOUB_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Elimino in toto i saldi Lotti e ubicazioni. Il loro contenuto � compreso nel
    *     temporaneo TMP_SALLOUB (Query GSSR_SSU )
    * --- Delete from SALLOTUBI
    i_nConn=i_TableProp[this.SALLOTUBI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALLOTUBI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"1 = "+cp_ToStrODBC(1);
             )
    else
      delete from (i_cTable) where;
            1 = 1;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Insert into SALLOTUBI
    i_nConn=i_TableProp[this.SALLOTUBI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALLOTUBI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMP_SALLOUB_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.SALLOTUBI_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_055C85A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into DOCSMAST
    i_nConn=i_TableProp[this.DOCSMAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOCSMAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSSR_STD",this.DOCSMAST_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore inserimento testata documenti'
      return
    endif
    return
  proc Try_055C8060()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into DOCSDETT
    i_nConn=i_TableProp[this.DOCSDETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOCSDETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSSR1STD",this.DOCSDETT_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore inserimento dettaglio documenti'
      return
    endif
    return
  proc Try_055C7A00()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into DOCSRATE
    i_nConn=i_TableProp[this.DOCSRATE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOCSRATE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSSR2STD",this.DOCSRATE_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore inserimento rate documenti'
      return
    endif
    return
  proc Try_055D47B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    GSSR_BGE(this,"INSE","MOVILOTT")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    return
  proc Try_055CD460()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MVMSMAST
    i_nConn=i_TableProp[this.MVMSMAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MVMSMAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSSR_STM",this.MVMSMAST_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore inserzione testate movimenti di magazzino'
      return
    endif
    return
  proc Try_055CCC20()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MVMSDETT
    i_nConn=i_TableProp[this.MVMSDETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MVMSDETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSSR1STM",this.MVMSDETT_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore inserzione dettaglio movimenti di magazzino'
      return
    endif
    return
  proc Try_055CB420()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    GSSR_BGE(this,"INSS","MOVILOTT")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    return
  proc Try_055D0B20()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MOVSMATR
    i_nConn=i_TableProp[this.MOVSMATR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOVSMATR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSSRMSTD",this.MOVSMATR_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore inserimento matricole'
      return
    endif
    return
  proc Try_055D2B30()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into LOTSIART
    i_nConn=i_TableProp[this.LOTSIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LOTSIART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSSR_SLO",this.LOTSIART_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore inserzione storico lotti'
      return
    endif
    return
  proc Try_055F2630()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if g_COLA="S"
      * --- Controllo Tabella Dic_Matr 
      * --- Insert into STO_MATR
      i_nConn=i_TableProp[this.STO_MATR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.STO_MATR_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSSR2SMA",this.STO_MATR_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error='Errore inserzione storico matricole'
        return
      endif
    else
      * --- Insert into STO_MATR
      i_nConn=i_TableProp[this.STO_MATR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.STO_MATR_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSSR1SMA",this.STO_MATR_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error='Errore inserzione storico matricole'
        return
      endif
    endif
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    AddMsgNL("Controlli",this.oparentobject)
    AddMsgNL("%1- Check analisi pre-storicizzazione",this.oparentobject, space(8))
    this.w_CONTA = 0
    * --- Select from SDANALISI
    i_nConn=i_TableProp[this.SDANALISI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SDANALISI_idx,2],.t.,this.SDANALISI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" SDANALISI ";
          +" where ANCODESE= "+cp_ToStrODBC(this.oParentObject.w_CODESE)+"";
           ,"_Curs_SDANALISI")
    else
      select Count(*) As Conta from (i_cTable);
       where ANCODESE= this.oParentObject.w_CODESE;
        into cursor _Curs_SDANALISI
    endif
    if used('_Curs_SDANALISI')
      select _Curs_SDANALISI
      locate for 1=1
      do while not(eof())
      this.w_CONTA = Nvl( _Curs_SDANALISI.CONTA , 0 )
        select _Curs_SDANALISI
        continue
      enddo
      use
    endif
    if this.w_CONTA=0
      * --- L'utente non ha eseguito l'analisi pre requisito necessario per svolgere la storicizzazione
      ah_ErrorMsg("Occorre lanciare l'analisi pre-storicizzazione prima di avviare l'elaborazione","!","")
      AddMsgNL("%1: FALLITO",this.oparentobject, space(this.w_NUMSPAZI) )
      i_retcode = 'stop'
      return
    else
      * --- Verifico l'utente e la data dell'ultima analisi per l'esercizio (ordinate per Serial)
      * --- Select from LOG_STOR
      i_nConn=i_TableProp[this.LOG_STOR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.LOG_STOR_idx,2],.t.,this.LOG_STOR_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select LGCODUTE,LG__DATA  from "+i_cTable+" LOG_STOR ";
            +" where LSCODESE="+cp_ToStrODBC(this.oParentObject.w_CODESE)+" And LG___OPE='A'   And LG__TIPO='L'";
            +" order by LSSERIAL Desc";
             ,"_Curs_LOG_STOR")
      else
        select LGCODUTE,LG__DATA from (i_cTable);
         where LSCODESE=this.oParentObject.w_CODESE And LG___OPE="A"   And LG__TIPO="L";
         order by LSSERIAL Desc;
          into cursor _Curs_LOG_STOR
      endif
      if used('_Curs_LOG_STOR')
        select _Curs_LOG_STOR
        locate for 1=1
        do while not(eof())
        if Nvl(_Curs_LOG_STOR.LG__DATA,cp_CharToDate("  /  /    "))<i_DATSYS Or Nvl(_Curs_LOG_STOR.LGCODUTE,0)<>i_CODUTE
          if !ah_YesNo("ATTENZIONE%0%0L'ultima analisi � stata lanciata il: %1 dall'utente %2%0%0Confermi comunque l'elaborazione?","", dtoc(Nvl(_Curs_LOG_STOR.LG__DATA,cp_CharToDate("/ /"))), Alltrim(str(Nvl(_Curs_LOG_STOR.LGCODUTE,0))) )
            AddMsgNL("%1: FALLITO",this.oparentobject, space(this.w_NUMSPAZI) )
            i_retcode = 'stop'
            return
          else
            AddMsgNL("%1: OK (richiesta accettata dall'utente - l'ultima analisi � stata lanciata il: %2 dall'utente %3",this.oparentobject, space(this.w_NUMSPAZI), dtoc(Nvl(_Curs_LOG_STOR.LG__DATA,cp_CharToDate("/ /"))), Alltrim(str(Nvl(_Curs_LOG_STOR.LGCODUTE,0))) )
          endif
        else
          AddMsgNL("%1: OK",this.oparentobject, space(this.w_NUMSPAZI) )
        endif
        Exit
          select _Curs_LOG_STOR
          continue
        enddo
        use
      endif
    endif
    if this.w_TESTATT="S" and 1=0
      do GSSR_BKO with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if not this.w_OKPROD
        i_retcode = 'stop'
        return
      endif
    endif
    * --- La procedura verifica la compatibilit� tra le strutture dei Documenti
    *     e dello storico (se esistono personalizzazioni)
    AddMsgNL("%1- Check struttura master documenti",this.oparentobject,space(8))
    * --- Try
    local bErr_055A8CF0
    bErr_055A8CF0=bTrsErr
    this.Try_055A8CF0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      AddMsgNL("%1: FALLITO %2",this.oparentobject, space(this.w_NUMSPAZI), i_ErrMsg )
      i_retcode = 'stop'
      return
    endif
    bTrsErr=bTrsErr or bErr_055A8CF0
    * --- End
    AddMsgNL("%1- Check date consolidamento", this.oparentobject,space(8))
    if g_APPLICATION <> "ADHOC REVOLUTION"
      if this.oParentObject.w_CONMAG<this.oParentObject.w_xesfinese
        * --- L'utente non ha eseguito l'analisi pre requisito necessario per svolgere la storicizzazione
        ah_ErrorMsg("Data consolidamento magazzino precedente alla data fine esercizio. Impossibile storicizzare","!","")
        AddMsgNL("%1: FALLITO",this.oparentobject, space(this.w_NUMSPAZI) )
        i_retcode = 'stop'
        return
      else
        AddMsgNL("%1: Magazzino OK",this.oparentobject, space(this.w_NUMSPAZI) )
      endif
      if this.oParentObject.w_CONACQ<this.oParentObject.w_xesfinese
        * --- L'utente non ha eseguito l'analisi pre requisito necessario per svolgere la storicizzazione
        ah_ErrorMsg("Data consolidamento ciclo acquisti precedente alla data fine esercizio. Impossibile storicizzare","!","")
        AddMsgNL("%1: FALLITO",this.oparentobject, space(this.w_NUMSPAZI) )
        i_retcode = 'stop'
        return
      else
        AddMsgNL("%1: Ciclo acquisti OK",this.oparentobject, space(this.w_NUMSPAZI) )
      endif
      if this.oParentObject.w_CONVEN<this.oParentObject.w_xesfinese
        * --- L'utente non ha eseguito l'analisi pre requisito necessario per svolgere la storicizzazione
        ah_ErrorMsg("Data consolidamento ciclo vendite precedente alla data fine esercizio. Impossibile storicizzare","!","")
        AddMsgNL("%1: FALLITO",this.oparentobject, space(this.w_NUMSPAZI) )
        i_retcode = 'stop'
        return
      else
        AddMsgNL("%1: Ciclo vendite OK",this.oparentobject, space(this.w_NUMSPAZI) )
      endif
    else
      * --- Eseguo controllo date Consolidamento
      this.w_MESS = CHKCONS("MDVA",this.oParentObject.w_xesfinese,"S","N")
      if Not Empty(this.w_MESS)
        * --- L'utente non ha eseguito l'analisi pre requisito necessario per svolgere la storicizzazione
        ah_ErrorMsg("%1","!","", this.w_MESS)
        AddMsgNL("%1: FALLITO",this.oparentobject, space(this.w_NUMSPAZI) )
        i_retcode = 'stop'
        return
      else
        AddMsgNL("%1: Ciclo vendite OK",this.oparentobject, space(this.w_NUMSPAZI) )
        AddMsgNL("%1: Ciclo acquisti OK",this.oparentobject, space(this.w_NUMSPAZI) )
        AddMsgNL("%1: Magazzino OK",this.oparentobject, space(this.w_NUMSPAZI) )
        AddMsgNL("%1: Analitica OK",this.oparentobject, space(this.w_NUMSPAZI) )
      endif
    endif
    if g_MAGA="S"
      if this.oParentObject.w_CHECKLIB="S"
        AddMsgNL("%1- Verifica libro giornale di magazzino",this.oparentobject, space(8))
        if g_APPLICATION <> "ADHOC REVOLUTION"
          this.w_CONTA = 0
          * --- Select from GSSR_BSD
          do vq_exec with 'GSSR_BSD',this,'_Curs_GSSR_BSD','',.f.,.t.
          if used('_Curs_GSSR_BSD')
            select _Curs_GSSR_BSD
            locate for 1=1
            do while not(eof())
            this.w_CONTA = this.w_CONTA + Nvl( _Curs_GSSR_BSD.CONTA , 0 )
              select _Curs_GSSR_BSD
              continue
            enddo
            use
          endif
          if this.w_CONTA<>0
            * --- Vi sono dei movimenti da stampare nel Libro Giornale di Magazzino. Possibile
            *     in caso di Import da altra procedura
            AddMsgNL("%1: FALLITO",this.oparentobject, space(this.w_NUMSPAZI) )
            AddMsgNL("Esistono %1 movimenti di magazzino e/o documenti non stampati nel libro giornale di magazzino",this.oparentobject, Alltrim(Str(this.w_CONTA)) )
            i_retcode = 'stop'
            return
          else
            AddMsgNL("%1: OK",this.oparentobject, space(this.w_NUMSPAZI) )
          endif
        else
          * --- Select from MAGAZZIN
          i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select MGCODMAG  from "+i_cTable+" MAGAZZIN ";
                +" where MGMAGRAG Is Null And MGFISMAG='S'";
                 ,"_Curs_MAGAZZIN")
          else
            select MGCODMAG from (i_cTable);
             where MGMAGRAG Is Null And MGFISMAG="S";
              into cursor _Curs_MAGAZZIN
          endif
          if used('_Curs_MAGAZZIN')
            select _Curs_MAGAZZIN
            locate for 1=1
            do while not(eof())
            this.w_CODMAG = _Curs_MAGAZZIN.MGCODMAG
            if CHKGIOM(this.oParentObject.w_CODESE,this.w_CODMAG,this.oParentObject.w_xesfinese,"S")
              * --- Vi sono dei movimenti da stampare nel Libro Giornale di Magazzino. Possibile
              *     in caso di Import da altra procedura
              AddMsgNL("%1: FALLITO",this.oparentobject, space(this.w_NUMSPAZI) )
              AddMsgNL("Magazzino %1 non stampato in definitivo nel libro giornale di magazzino",this.oparentobject, Alltrim(this.w_CODMAG) )
              i_retcode = 'stop'
              return
            endif
              select _Curs_MAGAZZIN
              continue
            enddo
            use
          endif
          AddMsgNL("%1: OK",this.oparentobject, space(this.w_NUMSPAZI) )
        endif
      endif
      AddMsgNL("%1- Check inventari", this.oparentobject, space(8))
      * --- Ciclo su tutti i magazzini fiscali che non hanno raggruppamento fiscale (quindi principali)
      *     e per ognuno testo la presenza di un invnetario che abbia:
      *     a) Esercizio storicizzando
      *     b) Movimenti fino allla data di fine esercizio
      *     c) flag storico attivo
      *     d) Inventario globale
      * --- Select from MAGAZZIN
      i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" MAGAZZIN ";
            +" where MGMAGRAG Is Null And MGFISMAG='S'";
             ,"_Curs_MAGAZZIN")
      else
        select * from (i_cTable);
         where MGMAGRAG Is Null And MGFISMAG="S";
          into cursor _Curs_MAGAZZIN
      endif
      if used('_Curs_MAGAZZIN')
        select _Curs_MAGAZZIN
        locate for 1=1
        do while not(eof())
        * --- Se magazzino valido prima della fine esercizio e non obsoleto prima dell'inizio
        if Nvl( _Curs_MAGAZZIN.MGDTINVA , this.oParentObject.w_xesiniese )<=this.oParentObject.w_xesfinese And Not Nvl( _Curs_MAGAZZIN.MGDTOBSO , this.oParentObject.w_xesiniese)< this.oParentObject.w_xesiniese
          this.w_NUMINV = ""
          * --- Read from INVENTAR
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.INVENTAR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.INVENTAR_idx,2],.t.,this.INVENTAR_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "INNUMINV"+;
              " from "+i_cTable+" INVENTAR where ";
                  +"INCODESE = "+cp_ToStrODBC(this.oParentObject.w_CODESE);
                  +" and INSTAINV = "+cp_ToStrODBC("S");
                  +" and INDATINV = "+cp_ToStrODBC(this.oParentObject.w_xesfinese);
                  +" and INTIPINV = "+cp_ToStrODBC("G");
                  +" and INCODMAG = "+cp_ToStrODBC(_Curs_MAGAZZIN.MGCODMAG);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              INNUMINV;
              from (i_cTable) where;
                  INCODESE = this.oParentObject.w_CODESE;
                  and INSTAINV = "S";
                  and INDATINV = this.oParentObject.w_xesfinese;
                  and INTIPINV = "G";
                  and INCODMAG = _Curs_MAGAZZIN.MGCODMAG;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_NUMINV = NVL(cp_ToDate(_read_.INNUMINV),cp_NullValue(_read_.INNUMINV))
            use
          else
            * --- Error: sql sentence error.
            i_Error = 'Errore lettura inventari'
            return
          endif
          select (i_nOldArea)
          if Empty(this.w_NUMINV)
            * --- L'utente non ha eseguito l'analisi pre requisito necessario per svolgere la storicizzazione
            AddMsgNL("%1: FALLITO",this.oparentobject, space(this.w_NUMSPAZI) )
            AddMsgNL("Per il magazzino %1 manca un inventario storico finale per l'esercizio %2",this.oparentobject, _Curs_MAGAZZIN.MGCODMAG, this.oParentObject.w_CODESE)
            ah_ErrorMsg("Per il magazzino %1 manca un inventario storico finale per l'esercizio %2", "!","", _Curs_MAGAZZIN.MGCODMAG, this.oParentObject.w_CODESE)
            i_retcode = 'stop'
            return
          endif
        endif
          select _Curs_MAGAZZIN
          continue
        enddo
        use
      endif
    endif
    AddMsgNL("%1: OK",this.oparentobject, space(this.w_NUMSPAZI) )
  endproc
  proc Try_055A8CF0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Try
    local bErr_055850B0
    bErr_055850B0=bTrsErr
    this.Try_055850B0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Raise
      i_Error=ah_Msgformat("Errore master documenti: %1", Message())
      return
    endif
    bTrsErr=bTrsErr or bErr_055850B0
    * --- End
    AddMsgNL("%1: OK",this.oparentobject, space(this.w_NUMSPAZI) )
    AddMsgNL("%1- Check struttura dettaglio documenti",this.oparentobject, space(8))
    * --- Provo il dettaglio documenti
    * --- Try
    local bErr_05584B70
    bErr_05584B70=bTrsErr
    this.Try_05584B70()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Raise
      i_Error=ah_Msgformat("Errore dettaglio documenti: %1", Message() )
      return
    endif
    bTrsErr=bTrsErr or bErr_05584B70
    * --- End
    AddMsgNL("%1: OK",this.oparentobject, space(this.w_NUMSPAZI) )
    if g_APPLICATION <> "ADHOC REVOLUTION"
      AddMsgNL("%1- Check struttura movimenti lotti / ubicazioni",this.oparentobject, space(8))
      * --- Try
      local bErr_05581960
      bErr_05581960=bTrsErr
      this.Try_05581960()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- Raise
        i_Error=ah_Msgformat("Errore movimenti lotti/ubicazioni %1",Message())
        return
      endif
      bTrsErr=bTrsErr or bErr_05581960
      * --- End
      AddMsgNL("%1: OK",this.oparentobject, space(this.w_NUMSPAZI) )
    endif
    AddMsgNL("%1- Check scadenze documenti",this.oparentobject, space(8))
    * --- Rate Scadenze
    * --- Try
    local bErr_055843F0
    bErr_055843F0=bTrsErr
    this.Try_055843F0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Raise
      i_Error=ah_Msgformat("Errore dettaglio scadenze documenti %1", Message() )
      return
    endif
    bTrsErr=bTrsErr or bErr_055843F0
    * --- End
    AddMsgNL("%1: OK",this.oparentobject, space(this.w_NUMSPAZI) )
    AddMsgNL("%1- Check master movimenti di magazzino", this.oparentobject,space(8))
    * --- Try
    local bErr_05583F70
    bErr_05583F70=bTrsErr
    this.Try_05583F70()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Raise
      i_Error=ah_Msgformat("Errore master movimenti di magazzino %1", Message() )
      return
    endif
    bTrsErr=bTrsErr or bErr_05583F70
    * --- End
    AddMsgNL("%1: OK",this.oparentobject, space(this.w_NUMSPAZI) )
    AddMsgNL("%1- Check dettaglio movimenti di magazzino", this.oparentobject, space(8))
    * --- Check Dettaglio movimenti
    * --- Try
    local bErr_05583880
    bErr_05583880=bTrsErr
    this.Try_05583880()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Raise
      i_Error=ah_Msgformat("Errore dettaglio movimenti di magazzino %1", Message() )
      return
    endif
    bTrsErr=bTrsErr or bErr_05583880
    * --- End
    AddMsgNL("%1: OK",this.oparentobject, space(this.w_NUMSPAZI) )
    * --- Eseguo comunque la Rollback
    * --- rollback
    bTrsErr=.t.
    cp_EndTrs(.t.)
    return
  proc Try_055850B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Creo una riga fittizia all'interno dei docuemnti
    * --- Insert into DOC_MAST
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOC_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MVSERIAL"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("ZUCCHETTI"),'DOC_MAST','MVSERIAL');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MVSERIAL',"ZUCCHETTI")
      insert into (i_cTable) (MVSERIAL &i_ccchkf. );
         values (;
           "ZUCCHETTI";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Insert into DOCSMAST
    i_nConn=i_TableProp[this.DOCSMAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOCSMAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable+" where MVSERIAL='ZUCCHETTI'",this.DOCSMAST_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_05584B70()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into DOC_DETT
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOC_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MVSERIAL"+",CPROWNUM"+",MVNUMRIF"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("ZUCCHETTI"),'DOC_DETT','MVSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(-100),'DOC_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(-100),'DOC_DETT','MVNUMRIF');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MVSERIAL',"ZUCCHETTI",'CPROWNUM',-100,'MVNUMRIF',-100)
      insert into (i_cTable) (MVSERIAL,CPROWNUM,MVNUMRIF &i_ccchkf. );
         values (;
           "ZUCCHETTI";
           ,-100;
           ,-100;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Insert into DOCSDETT
    i_nConn=i_TableProp[this.DOCSDETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOCSDETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable+" where MVSERIAL='ZUCCHETTI'",this.DOCSDETT_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_05581960()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Movimenti Lotti ubicazioni
    GSSR_BGE(this,"RIGA","MOVILOTT")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    return
  proc Try_055843F0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into DOC_RATE
    i_nConn=i_TableProp[this.DOC_RATE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_RATE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOC_RATE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"RSSERIAL"+",RSNUMRAT"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("ZUCCHETTI"),'DOC_RATE','RSSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(-10),'DOC_RATE','RSNUMRAT');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'RSSERIAL',"ZUCCHETTI",'RSNUMRAT',-10)
      insert into (i_cTable) (RSSERIAL,RSNUMRAT &i_ccchkf. );
         values (;
           "ZUCCHETTI";
           ,-10;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Insert into DOCSRATE
    i_nConn=i_TableProp[this.DOCSRATE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOCSRATE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.DOC_RATE_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable+" where RSSERIAL='ZUCCHETTI'",this.DOCSRATE_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_05583F70()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Check Movimenti di magazzino Master
    * --- Insert into MVM_MAST
    i_nConn=i_TableProp[this.MVM_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MVM_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MVM_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MMSERIAL"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("ZUCCHETTI"),'MVM_MAST','MMSERIAL');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MMSERIAL',"ZUCCHETTI")
      insert into (i_cTable) (MMSERIAL &i_ccchkf. );
         values (;
           "ZUCCHETTI";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Insert into MVMSMAST
    i_nConn=i_TableProp[this.MVMSMAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MVMSMAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.MVM_MAST_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable+" where MMSERIAL='ZUCCHETTI'",this.MVMSMAST_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_05583880()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MVM_DETT
    i_nConn=i_TableProp[this.MVM_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MVM_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MMSERIAL"+",CPROWNUM"+",MMNUMRIF"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("ZUCCHETTI"),'MVM_DETT','MMSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(-100),'MVM_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(-100),'MVM_DETT','MMNUMRIF');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MMSERIAL',"ZUCCHETTI",'CPROWNUM',-100,'MMNUMRIF',-100)
      insert into (i_cTable) (MMSERIAL,CPROWNUM,MMNUMRIF &i_ccchkf. );
         values (;
           "ZUCCHETTI";
           ,-100;
           ,-100;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Insert into MVMSDETT
    i_nConn=i_TableProp[this.MVMSDETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MVMSDETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable+" where MMSERIAL='ZUCCHETTI'",this.MVMSDETT_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento saldi commesse e storico
    AddMsgNL("Aggiornamento saldi commesse dello storico",this.oparentobject)
    * --- Create temporary table TMPSALAGG
    i_nIdx=cp_AddTableDef('TMPSALAGG') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('GSSRCSAL',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPSALAGG_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    AddMsgNL("Fase scrittura saldi storici commesse",this.oparentobject)
    * --- Try
    local bErr_0558AD80
    bErr_0558AD80=bTrsErr
    this.Try_0558AD80()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      AddMsgNL("Errore aggiornamento saldi commesse dello storico: %1",this.oparentobject, Message() )
      * --- Raise
      i_Error=Message()
      return
    endif
    bTrsErr=bTrsErr or bErr_0558AD80
    * --- End
    AddMsgNL("%1 records modificati: %2",this.oparentobject, space(this.w_NUMSPAZ2), Str( i_Rows ) )
    * --- Drop temporary table TMPSALAGG
    i_nIdx=cp_GetTableDefIdx('TMPSALAGG')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPSALAGG')
    endif
    AddMsgNL("Aggiornamento saldi commesse fuori linea",this.oparentobject)
    * --- Try
    local bErr_0558A450
    bErr_0558A450=bTrsErr
    this.Try_0558A450()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      AddMsgNL("Errore saldi di commesse: %1",this.oparentobject, Message() )
      * --- Raise
      i_Error=Message()
      return
    endif
    bTrsErr=bTrsErr or bErr_0558A450
    * --- End
    AddMsgNL("%1 records modificati: %2",this.oparentobject, space(this.w_NUMSPAZ2), Str( i_Rows ) )
    if g_PERUBI = "S" or g_PERLOT ="S"
      * --- Aggiornamento saldi lotti/ubicazioni /commesse e storico
      AddMsgNL("Aggiornamento saldi commesse/lotti/ubicazione dello storico",this.oparentobject)
      * --- Create temporary table TMPSALDI1
      i_nIdx=cp_AddTableDef('TMPSALDI1') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('GSSRMSAL',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPSALDI1_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      AddMsgNL("Fase scrittura saldi storici commesse/lotti/ubicazione",this.oparentobject)
      * --- Try
      local bErr_0557C1A0
      bErr_0557C1A0=bTrsErr
      this.Try_0557C1A0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        AddMsgNL("Errore aggiornamento saldi commesse/lotti/ubicazione dello storico: %1",this.oparentobject, Message() )
        * --- Raise
        i_Error=Message()
        return
      endif
      bTrsErr=bTrsErr or bErr_0557C1A0
      * --- End
      AddMsgNL("%1 records modificati: %2",this.oparentobject, space(this.w_NUMSPAZ2), Str( i_Rows ) )
      * --- Drop temporary table TMPSALDI1
      i_nIdx=cp_GetTableDefIdx('TMPSALDI1')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPSALDI1')
      endif
      AddMsgNL("Aggiornamento saldi commesse/lotti/ubicazione fuori linea",this.oparentobject)
      * --- Try
      local bErr_0557BCC0
      bErr_0557BCC0=bTrsErr
      this.Try_0557BCC0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        AddMsgNL("Errore commesse/lotti/ubicazione: %1",this.oparentobject, Message() )
        * --- Raise
        i_Error=Message()
        return
      endif
      bTrsErr=bTrsErr or bErr_0557BCC0
      * --- End
      AddMsgNL("%1 records modificati: %2",this.oparentobject, space(this.w_NUMSPAZ2), Str( i_Rows ) )
    endif
  endproc
  proc Try_0558AD80()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from SALDISCOM
    i_nConn=i_TableProp[this.SALDISCOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDISCOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"1 = "+cp_ToStrODBC(1);
             )
    else
      delete from (i_cTable) where;
            1 = 1;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Insert into SALDISCOM
    i_nConn=i_TableProp[this.SALDISCOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDISCOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMPSALAGG_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.SALDISCOM_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_0558A450()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Aggiungo le giacenze fuori linea
    * --- Write into SALDICOM
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="SCCODICE,SCCODMAG,SCCODCAN"
      do vq_exec with 'gsmacqrc',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="SALDICOM.SCCODICE = _t2.SCCODICE";
              +" and "+"SALDICOM.SCCODMAG = _t2.SCCODMAG";
              +" and "+"SALDICOM.SCCODCAN = _t2.SCCODCAN";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SCQTAPRO = _t2.SCQTAPRO";
          +",SCQTRPRO = _t2.SCQTRPRO";
          +i_ccchkf;
          +" from "+i_cTable+" SALDICOM, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="SALDICOM.SCCODICE = _t2.SCCODICE";
              +" and "+"SALDICOM.SCCODMAG = _t2.SCCODMAG";
              +" and "+"SALDICOM.SCCODCAN = _t2.SCCODCAN";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDICOM, "+i_cQueryTable+" _t2 set ";
          +"SALDICOM.SCQTAPRO = _t2.SCQTAPRO";
          +",SALDICOM.SCQTRPRO = _t2.SCQTRPRO";
          +Iif(Empty(i_ccchkf),"",",SALDICOM.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="SALDICOM.SCCODICE = t2.SCCODICE";
              +" and "+"SALDICOM.SCCODMAG = t2.SCCODMAG";
              +" and "+"SALDICOM.SCCODCAN = t2.SCCODCAN";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDICOM set (";
          +"SCQTAPRO,";
          +"SCQTRPRO";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.SCQTAPRO,";
          +"t2.SCQTRPRO";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="SALDICOM.SCCODICE = _t2.SCCODICE";
              +" and "+"SALDICOM.SCCODMAG = _t2.SCCODMAG";
              +" and "+"SALDICOM.SCCODCAN = _t2.SCCODCAN";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDICOM set ";
          +"SCQTAPRO = _t2.SCQTAPRO";
          +",SCQTRPRO = _t2.SCQTRPRO";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".SCCODICE = "+i_cQueryTable+".SCCODICE";
              +" and "+i_cTable+".SCCODMAG = "+i_cQueryTable+".SCCODMAG";
              +" and "+i_cTable+".SCCODCAN = "+i_cQueryTable+".SCCODCAN";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SCQTAPRO = (select SCQTAPRO from "+i_cQueryTable+" where "+i_cWhere+")";
          +",SCQTRPRO = (select SCQTRPRO from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errore aggiunta giacenze fuori linea'
      return
    endif
    return
  proc Try_0557C1A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from SASLOTCOM
    i_nConn=i_TableProp[this.SASLOTCOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SASLOTCOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"1 = "+cp_ToStrODBC(1);
             )
    else
      delete from (i_cTable) where;
            1 = 1;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Insert into SASLOTCOM
    i_nConn=i_TableProp[this.SASLOTCOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SASLOTCOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMPSALDI1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.SASLOTCOM_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_0557BCC0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Aggiungo le giacenze fuori linea
    * --- Write into SALOTCOM
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALOTCOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALOTCOM_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="SMCODCAN,SMCODART,SMCODMAG,SMCODLOT,SMCODUBI"
      do vq_exec with 'gssrmsal7',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALOTCOM_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="SALOTCOM.SMCODCAN = _t2.SMCODCAN";
              +" and "+"SALOTCOM.SMCODART = _t2.SMCODART";
              +" and "+"SALOTCOM.SMCODMAG = _t2.SMCODMAG";
              +" and "+"SALOTCOM.SMCODLOT = _t2.SMCODLOT";
              +" and "+"SALOTCOM.SMCODUBI = _t2.SMCODUBI";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SMQTAPRO = _t2.SMQTAPRO";
          +",SMQTRPRO = _t2.SMQTRPRO";
          +i_ccchkf;
          +" from "+i_cTable+" SALOTCOM, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="SALOTCOM.SMCODCAN = _t2.SMCODCAN";
              +" and "+"SALOTCOM.SMCODART = _t2.SMCODART";
              +" and "+"SALOTCOM.SMCODMAG = _t2.SMCODMAG";
              +" and "+"SALOTCOM.SMCODLOT = _t2.SMCODLOT";
              +" and "+"SALOTCOM.SMCODUBI = _t2.SMCODUBI";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALOTCOM, "+i_cQueryTable+" _t2 set ";
          +"SALOTCOM.SMQTAPRO = _t2.SMQTAPRO";
          +",SALOTCOM.SMQTRPRO = _t2.SMQTRPRO";
          +Iif(Empty(i_ccchkf),"",",SALOTCOM.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="SALOTCOM.SMCODCAN = t2.SMCODCAN";
              +" and "+"SALOTCOM.SMCODART = t2.SMCODART";
              +" and "+"SALOTCOM.SMCODMAG = t2.SMCODMAG";
              +" and "+"SALOTCOM.SMCODLOT = t2.SMCODLOT";
              +" and "+"SALOTCOM.SMCODUBI = t2.SMCODUBI";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALOTCOM set (";
          +"SMQTAPRO,";
          +"SMQTRPRO";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.SMQTAPRO,";
          +"t2.SMQTRPRO";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="SALOTCOM.SMCODCAN = _t2.SMCODCAN";
              +" and "+"SALOTCOM.SMCODART = _t2.SMCODART";
              +" and "+"SALOTCOM.SMCODMAG = _t2.SMCODMAG";
              +" and "+"SALOTCOM.SMCODLOT = _t2.SMCODLOT";
              +" and "+"SALOTCOM.SMCODUBI = _t2.SMCODUBI";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALOTCOM set ";
          +"SMQTAPRO = _t2.SMQTAPRO";
          +",SMQTRPRO = _t2.SMQTRPRO";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".SMCODCAN = "+i_cQueryTable+".SMCODCAN";
              +" and "+i_cTable+".SMCODART = "+i_cQueryTable+".SMCODART";
              +" and "+i_cTable+".SMCODMAG = "+i_cQueryTable+".SMCODMAG";
              +" and "+i_cTable+".SMCODLOT = "+i_cQueryTable+".SMCODLOT";
              +" and "+i_cTable+".SMCODUBI = "+i_cQueryTable+".SMCODUBI";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SMQTAPRO = (select SMQTAPRO from "+i_cQueryTable+" where "+i_cWhere+")";
          +",SMQTRPRO = (select SMQTRPRO from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errore aggiunta giacenze fuori linea'
      return
    endif
    return

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,37)]
    this.cWorkTables[1]='DOC_DETT'
    this.cWorkTables[2]='DOC_MAST'
    this.cWorkTables[3]='DOC_RATE'
    this.cWorkTables[4]='DOCSDETT'
    this.cWorkTables[5]='DOCSMAST'
    this.cWorkTables[6]='DOCSRATE'
    this.cWorkTables[7]='MVM_DETT'
    this.cWorkTables[8]='MVM_MAST'
    this.cWorkTables[9]='MVMSDETT'
    this.cWorkTables[10]='MVMSMAST'
    this.cWorkTables[11]='SDANALISI'
    this.cWorkTables[12]='MAGAZZIN'
    this.cWorkTables[13]='INVENTAR'
    this.cWorkTables[14]='LOG_STOR'
    this.cWorkTables[15]='CAN_TIER'
    this.cWorkTables[16]='SALDIART'
    this.cWorkTables[17]='SALLOTUBI'
    this.cWorkTables[18]='*TMP_SALLOUB'
    this.cWorkTables[19]='LOTSIART'
    this.cWorkTables[20]='LOTTIART'
    this.cWorkTables[21]='PDA_DETT'
    this.cWorkTables[22]='RILEVAZI'
    this.cWorkTables[23]='MOVSMATR'
    this.cWorkTables[24]='*TMP_STOR'
    this.cWorkTables[25]='MOVIMATR'
    this.cWorkTables[26]='STO_MATR'
    this.cWorkTables[27]='MATRICOL'
    this.cWorkTables[28]='CON_PAGA'
    this.cWorkTables[29]='SALDILOT'
    this.cWorkTables[30]='SALDISART'
    this.cWorkTables[31]='*TMPSALDI'
    this.cWorkTables[32]='SALDICOM'
    this.cWorkTables[33]='SALDISCOM'
    this.cWorkTables[34]='*TMPSALAGG'
    this.cWorkTables[35]='*TMPSALDI1'
    this.cWorkTables[36]='SASLOTCOM'
    this.cWorkTables[37]='SALOTCOM'
    return(this.OpenAllTables(37))

  proc CloseCursors()
    if used('_Curs_SDANALISI')
      use in _Curs_SDANALISI
    endif
    if used('_Curs_LOG_STOR')
      use in _Curs_LOG_STOR
    endif
    if used('_Curs_GSSR_BSD')
      use in _Curs_GSSR_BSD
    endif
    if used('_Curs_MAGAZZIN')
      use in _Curs_MAGAZZIN
    endif
    if used('_Curs_MAGAZZIN')
      use in _Curs_MAGAZZIN
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
