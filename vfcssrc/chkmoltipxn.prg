* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: chkmoltipxn                                                     *
*              Check moltiplica record                                         *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_30]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-10-15                                                      *
* Last revis.: 2014-02-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pMAX
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tchkmoltipxn",oParentObject,m.pMAX)
return(i_retval)

define class tchkmoltipxn as StdBatch
  * --- Local variables
  pMAX = 0
  w_MAXROWNUM = 0
  w_INSERT = 0
  w_MAX = 0
  w_N = 0
  w_MAXNUM = 0
  * --- WorkFile variables
  MOLTIPXN_idx=0
  TMPMOLXN_idx=0
  TMPMOLTI_idx=0
  AGENTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questo batch controlla la dimensione della tabella MOLTIPXN per
    *     verificare se ha dimensione abbastanza grande da poter eseguire la
    *     duplicazione richiesta.
    * --- Parametri:
    *     pMAX:
    *          Specifica il numero massimo di duplicati che si vuole ottenere.
    * --- Controllo la dimensione dellla tabella MOLTIPXN
    * --- Numero massimo di duplicati
    this.w_MAXNUM = 1000000
    * --- Se non specifico nessun parametro assegno 0 a w_MAX
    this.w_MAX = IIF(TYPE("pMAX")="L",0,this.pMAX)
    * --- Verifico se MOLTIPXN ha la dimensione sufficiente per la duplicazione
    * --- Select from QUERY\MAXMOLTI
    do vq_exec with 'QUERY\MAXMOLTI',this,'_Curs_QUERY_MAXMOLTI','',.f.,.t.
    if used('_Curs_QUERY_MAXMOLTI')
      select _Curs_QUERY_MAXMOLTI
      locate for 1=1
      do while not(eof())
      this.w_MAXROWNUM = MAXROWNUM
        select _Curs_QUERY_MAXMOLTI
        continue
      enddo
      use
    endif
    if this.w_MAXROWNUM < this.w_MAX
      * --- Devo incrementare la dimensione della tabella MOLTIPXN
      * --- Inserisco 10 righe nella tabella TMPMOLTI 
      * --- Create temporary table TMPMOLTI
      i_nIdx=cp_AddTableDef('TMPMOLTI') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('QUERY\SETMOLTI.VQR',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPMOLTI_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      this.w_INSERT = 0
      do while 10 > this.w_INSERT
        * --- Insert into TMPMOLTI
        i_nConn=i_TableProp[this.TMPMOLTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPMOLTI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPMOLTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"ROW__NUM"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_INSERT),'TMPMOLTI','ROW__NUM');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'ROW__NUM',this.w_INSERT)
          insert into (i_cTable) (ROW__NUM &i_ccchkf. );
             values (;
               this.w_INSERT;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        this.w_INSERT = this.w_INSERT + 1
      enddo
      * --- Inserisco 100 record nella tabella TMPMOLXN
      * --- Create temporary table TMPMOLXN
      i_nIdx=cp_AddTableDef('TMPMOLXN') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('QUERY\SETMOLTI2.VQR',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPMOLXN_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      * --- Inserisco in MOLTIPXN il doppio dei record richiesti
      if this.w_MAX * 2 > this.w_MAXNUM
        Ah_ErrorMsg("Non � possibile duplicare pi� di %1 record", 48, "", alltrim(str(this.w_MAXNUM)))
      endif
      this.w_N = this.w_MAX * 2 - this.w_MAXROWNUM
      * --- Verifico su quale db mi trovo, in db2 non funziona il comando TOP devo utilizzare 
      *     fetch first
      do case
        case upper(CP_DBTYPE)="DB2" OR upper(CP_DBTYPE)="POSTGRESQL"
          * --- Sono in DB2, devo costruire la frase sql per l'inserzione in MOLTIPXN
          L_Table=cp_SetAzi(i_TableProp[this.TMPMOLXN_idx,2])
          L_FraseSQL= "select (TMPMOLXN.ROW__NUM*10000+TMPMOLXN1.ROW__NUM*100+TMPMOLXN2.ROW__NUM) as ROW__NUM from "; 
 +L_Table+" TMPMOLXN,"+L_Table+" TMPMOLXN1,"+L_Table+; 
 " TMPMOLXN2 where ((TMPMOLXN.ROW__NUM*10000+TMPMOLXN1.ROW__NUM*100+TMPMOLXN2.ROW__NUM) >=" +ALLTRIM(STR(this.w_MAXROWNUM))+ ; 
 " ) order by  1  fetch first "+ALLTRIM(STR(this.w_N))+" rows only"
          * --- Eseguo l'insert in MOLTIPXN
          i_nConn=i_TableProp[this.MOLTIPXN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MOLTIPXN_idx,2])
          i_commit = .f.
          if type("nTrsConnCnt")="U" or nTrsConnCnt=0
             
 cp_BeginTrs() 
 i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+" (ROW__NUM) ("+L_FraseSQL+")")
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
             
 i_Error=MSG_INSERT_ERROR 
 return
          endif
        case upper(CP_DBTYPE)="ORACLE"
          * --- Sono in ORACLE, devo costruire la frase sql per l'inserzione in MOLTIPXN
          L_Table=cp_SetAzi(i_TableProp[this.TMPMOLXN_idx,2])
          L_FraseSQL= "select ROW__NUM from (select (TMPMOLXN.ROW__NUM*10000+TMPMOLXN1.ROW__NUM*100+TMPMOLXN2.ROW__NUM) as ROW__NUM from "; 
 +L_Table+" TMPMOLXN,"+L_Table+" TMPMOLXN1,"+L_Table+; 
 " TMPMOLXN2 where ((TMPMOLXN.ROW__NUM*10000+TMPMOLXN1.ROW__NUM*100+TMPMOLXN2.ROW__NUM) >=" +ALLTRIM(STR(this.w_MAXROWNUM))+ ; 
 " ) order by  1)  where ROWNUM <"+ALLTRIM(STR(this.w_N))
          * --- Eseguo l'insert in MOLTIPXN
          i_nConn=i_TableProp[this.MOLTIPXN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MOLTIPXN_idx,2])
          i_commit = .f.
          if type("nTrsConnCnt")="U" or nTrsConnCnt=0
             
 cp_BeginTrs() 
 i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+" (ROW__NUM) ("+L_FraseSQL+")")
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
             
 i_Error=MSG_INSERT_ERROR 
 return
          endif
        otherwise
          * --- Sono in SQLServer
          * --- Insert into MOLTIPXN
          i_nConn=i_TableProp[this.MOLTIPXN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MOLTIPXN_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\moltipxn",this.MOLTIPXN_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
      endcase
      * --- Drop temporary table TMPMOLTI
      i_nIdx=cp_GetTableDefIdx('TMPMOLTI')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPMOLTI')
      endif
      * --- Drop temporary table TMPMOLXN
      i_nIdx=cp_GetTableDefIdx('TMPMOLXN')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPMOLXN')
      endif
    endif
  endproc


  proc Init(oParentObject,pMAX)
    this.pMAX=pMAX
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='MOLTIPXN'
    this.cWorkTables[2]='*TMPMOLXN'
    this.cWorkTables[3]='*TMPMOLTI'
    this.cWorkTables[4]='AGENTI'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_QUERY_MAXMOLTI')
      use in _Curs_QUERY_MAXMOLTI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pMAX"
endproc
