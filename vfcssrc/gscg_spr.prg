* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_spr                                                        *
*              Stampa prospetto dei versamenti                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_83]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-06-23                                                      *
* Last revis.: 2008-07-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_spr",oParentObject))

* --- Class definition
define class tgscg_spr as StdForm
  Top    = 23
  Left   = 66

  * --- Standard Properties
  Width  = 467
  Height = 204
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-07-08"
  HelpContextID=141174121
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=12

  * --- Constant Properties
  _IDX = 0
  ESERCIZI_IDX = 0
  AZIENDA_IDX = 0
  VOCIIVA_IDX = 0
  BAN_CHE_IDX = 0
  COD_CAB_IDX = 0
  ATTIMAST_IDX = 0
  cPrg = "gscg_spr"
  cComment = "Stampa prospetto dei versamenti"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_CODATT = space(5)
  w_DESCRATT = space(35)
  w_KEYATT = space(5)
  w_DATVERINI = ctod('  /  /  ')
  w_DATVERFIN = ctod('  /  /  ')
  w_ANNOINI = space(4)
  w_PERIOINI = 0
  o_PERIOINI = 0
  w_ANNOFIN = space(4)
  w_PERIOFIN = 0
  o_PERIOFIN = 0
  w_DESCRI = space(30)
  w_DESCRI1 = space(30)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_sprPag1","gscg_spr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDATVERINI_1_5
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='AZIENDA'
    this.cWorkTables[3]='VOCIIVA'
    this.cWorkTables[4]='BAN_CHE'
    this.cWorkTables[5]='COD_CAB'
    this.cWorkTables[6]='ATTIMAST'
    return(this.OpenAllTables(6))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gscg_spr
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_CODATT=space(5)
      .w_DESCRATT=space(35)
      .w_KEYATT=space(5)
      .w_DATVERINI=ctod("  /  /  ")
      .w_DATVERFIN=ctod("  /  /  ")
      .w_ANNOINI=space(4)
      .w_PERIOINI=0
      .w_ANNOFIN=space(4)
      .w_PERIOFIN=0
      .w_DESCRI=space(30)
      .w_DESCRI1=space(30)
        .w_CODAZI = i_CODAZI
        .w_CODATT = g_CATAZI
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODATT))
          .link_1_2('Full')
        endif
          .DoRTCalc(3,3,.f.)
        .w_KEYATT = IIF(g_ATTIVI='S', '#####', g_CATAZI)
          .DoRTCalc(5,6,.f.)
        .w_ANNOINI = ALLTRIM(STR(YEAR(i_DATSYS),4,0))
        .w_PERIOINI = 1
        .w_ANNOFIN = ALLTRIM(STR(YEAR(i_DATSYS),4,0))
        .w_PERIOFIN = .w_PERIOINI
        .w_DESCRI = IIF(.w_PERIOINI>0, CALCPER(.w_PERIOINI, g_TIPDEN), SPACE(30))
        .w_DESCRI1 = IIF(.w_PERIOFIN>0, CALCPER(.w_PERIOFIN, g_TIPDEN), SPACE(30))
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
          .link_1_2('Full')
        .DoRTCalc(3,3,.t.)
            .w_KEYATT = IIF(g_ATTIVI='S', '#####', g_CATAZI)
        .DoRTCalc(5,9,.t.)
        if .o_PERIOINI<>.w_PERIOINI
            .w_PERIOFIN = .w_PERIOINI
        endif
        if .o_PERIOINI<>.w_PERIOINI
            .w_DESCRI = IIF(.w_PERIOINI>0, CALCPER(.w_PERIOINI, g_TIPDEN), SPACE(30))
        endif
        if .o_PERIOFIN<>.w_PERIOFIN
            .w_DESCRI1 = IIF(.w_PERIOFIN>0, CALCPER(.w_PERIOFIN, g_TIPDEN), SPACE(30))
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODATT
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIMAST_IDX,3]
    i_lTable = "ATTIMAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2], .t., this.ATTIMAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODATT,ATDESATT";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_CODATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODATT',this.w_CODATT)
            select ATCODATT,ATDESATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATT = NVL(_Link_.ATCODATT,space(5))
      this.w_DESCRATT = NVL(_Link_.ATDESATT,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODATT = space(5)
      endif
      this.w_DESCRATT = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2])+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIMAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATVERINI_1_5.value==this.w_DATVERINI)
      this.oPgFrm.Page1.oPag.oDATVERINI_1_5.value=this.w_DATVERINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATVERFIN_1_6.value==this.w_DATVERFIN)
      this.oPgFrm.Page1.oPag.oDATVERFIN_1_6.value=this.w_DATVERFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oANNOINI_1_7.value==this.w_ANNOINI)
      this.oPgFrm.Page1.oPag.oANNOINI_1_7.value=this.w_ANNOINI
    endif
    if not(this.oPgFrm.Page1.oPag.oPERIOINI_1_8.value==this.w_PERIOINI)
      this.oPgFrm.Page1.oPag.oPERIOINI_1_8.value=this.w_PERIOINI
    endif
    if not(this.oPgFrm.Page1.oPag.oANNOFIN_1_9.value==this.w_ANNOFIN)
      this.oPgFrm.Page1.oPag.oANNOFIN_1_9.value=this.w_ANNOFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPERIOFIN_1_10.value==this.w_PERIOFIN)
      this.oPgFrm.Page1.oPag.oPERIOFIN_1_10.value=this.w_PERIOFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_12.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_12.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI1_1_18.value==this.w_DESCRI1)
      this.oPgFrm.Page1.oPag.oDESCRI1_1_18.value=this.w_DESCRI1
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_ANNOINI)) or not(VAL(.w_ANNOINI)>1900))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANNOINI_1_7.SetFocus()
            i_bnoObbl = !empty(.w_ANNOINI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_PERIOINI)) or not(.w_PERIOINI<5 OR (.w_PERIOINI<13 AND g_TIPDEN="M")))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPERIOINI_1_8.SetFocus()
            i_bnoObbl = !empty(.w_PERIOINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Numero periodo non congruente")
          case   ((empty(.w_ANNOFIN)) or not(VAL(.w_ANNOFIN)>1900))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANNOFIN_1_9.SetFocus()
            i_bnoObbl = !empty(.w_ANNOFIN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_PERIOFIN)) or not(.w_PERIOFIN<5 OR (.w_PERIOFIN<13 AND g_TIPDEN="M")))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPERIOFIN_1_10.SetFocus()
            i_bnoObbl = !empty(.w_PERIOFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Numero periodo non congruente")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PERIOINI = this.w_PERIOINI
    this.o_PERIOFIN = this.w_PERIOFIN
    return

enddefine

* --- Define pages as container
define class tgscg_sprPag1 as StdContainer
  Width  = 463
  height = 204
  stdWidth  = 463
  stdheight = 204
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATVERINI_1_5 as StdField with uid="PIKZRKBLQD",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DATVERINI", cQueryName = "DATVERINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 239166740,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=96, Top=33

  add object oDATVERFIN_1_6 as StdField with uid="KYTALQSIQC",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DATVERFIN", cQueryName = "DATVERFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 239166815,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=96, Top=65

  add object oANNOINI_1_7 as StdField with uid="DITKXJLBMK",rtseq=7,rtrep=.f.,;
    cFormVar = "w_ANNOINI", cQueryName = "ANNOINI",nZero=4,;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno di riferimento dei saldi IVA da calcolare",;
    HelpContextID = 175770886,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=96, Top=97, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oANNOINI_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_ANNOINI)>1900)
    endwith
    return bRes
  endfunc

  add object oPERIOINI_1_8 as StdField with uid="GNUQORRVDZ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_PERIOINI", cQueryName = "PERIOINI",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Numero periodo non congruente",;
    ToolTipText = "Periodo: 1..4 per trimestrale; 1..12 per mensile",;
    HelpContextID = 170638017,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=205, Top=97, cSayPict='"99"', cGetPict='"99"'

  func oPERIOINI_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PERIOINI<5 OR (.w_PERIOINI<13 AND g_TIPDEN="M"))
    endwith
    return bRes
  endfunc

  add object oANNOFIN_1_9 as StdField with uid="KGJZQNOWMS",rtseq=9,rtrep=.f.,;
    cFormVar = "w_ANNOFIN", cQueryName = "ANNOFIN",nZero=4,;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno di riferimento dei saldi IVA da calcolare",;
    HelpContextID = 179696378,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=96, Top=129, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oANNOFIN_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_ANNOFIN)>1900)
    endwith
    return bRes
  endfunc

  add object oPERIOFIN_1_10 as StdField with uid="NLSDSDDEBI",rtseq=10,rtrep=.f.,;
    cFormVar = "w_PERIOFIN", cQueryName = "PERIOFIN",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Numero periodo non congruente",;
    ToolTipText = "Periodo: 1..4 per trimestrale; 1..12 per mensile",;
    HelpContextID = 47465796,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=205, Top=129, cSayPict='"99"', cGetPict='"99"'

  func oPERIOFIN_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PERIOFIN<5 OR (.w_PERIOFIN<13 AND g_TIPDEN="M"))
    endwith
    return bRes
  endfunc


  add object oBtn_1_11 as StdButton with uid="SENKBIZSYY",left=356, top=154, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per iniziare l'elaborazione";
    , HelpContextID = 141145370;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      with this.Parent.oContained
        do GSCG_BPV with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCRI_1_12 as StdField with uid="KINVSYYWJX",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 167881674,;
   bGlobalFont=.t.,;
    Height=21, Width=214, Left=242, Top=97, InputMask=replicate('X',30)


  add object oBtn_1_14 as StdButton with uid="BYDIHEHRWY",left=406, top=154, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 133856698;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCRI1_1_18 as StdField with uid="HQYQZJNOQA",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESCRI1", cQueryName = "DESCRI1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 167881674,;
   bGlobalFont=.t.,;
    Height=21, Width=214, Left=242, Top=129, InputMask=replicate('X',30)

  add object oStr_1_13 as StdString with uid="WRMIESLCDW",Visible=.t., Left=5, Top=65,;
    Alignment=1, Width=88, Height=18,;
    Caption="Alla data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="PUHMGWINIU",Visible=.t., Left=5, Top=97,;
    Alignment=1, Width=88, Height=18,;
    Caption="Da anno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="RDYUWVGEWE",Visible=.t., Left=144, Top=97,;
    Alignment=1, Width=58, Height=18,;
    Caption="Periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="PAUYJQEAIN",Visible=.t., Left=5, Top=129,;
    Alignment=1, Width=88, Height=18,;
    Caption="A anno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="VUHEUPRFUZ",Visible=.t., Left=144, Top=129,;
    Alignment=1, Width=58, Height=18,;
    Caption="Periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="WOXWXRVLGR",Visible=.t., Left=5, Top=33,;
    Alignment=1, Width=88, Height=18,;
    Caption="Dalla data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="IEGZSYLWBC",Visible=.t., Left=16, Top=6,;
    Alignment=0, Width=172, Height=18,;
    Caption="Intervallo date di versamento"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.t., FontStrikeThru=.f.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_spr','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
