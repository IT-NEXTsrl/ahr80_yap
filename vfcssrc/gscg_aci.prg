* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_aci                                                        *
*              Contabilizzazione indiretta effetti                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [116] [VRS_282]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-08                                                      *
* Last revis.: 2013-12-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gscg_aci
if cp_TablePropScan('TMP_PART_IND') > 0
   ah_errormsg("Attenzione: E' gi� aperta un'istanza di Contabilizzazione indiretta effetti, quella corrente verr� chiusa",48,'')
   return
endif
* --- Fine Area Manuale
return(createobject("tgscg_aci"))

* --- Class definition
define class tgscg_aci as StdForm
  Top    = 9
  Left   = 10

  * --- Standard Properties
  Width  = 739
  Height = 418+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-12-04"
  HelpContextID=109716841
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=60

  * --- Constant Properties
  CON_INDI_IDX = 0
  CONTROPA_IDX = 0
  ESERCIZI_IDX = 0
  VALUTE_IDX = 0
  CAU_CONT_IDX = 0
  CONTI_IDX = 0
  cFile = "CON_INDI"
  cKeySelect = "CISERIAL"
  cKeyWhere  = "CISERIAL=this.w_CISERIAL"
  cKeyWhereODBC = '"CISERIAL="+cp_ToStrODBC(this.w_CISERIAL)';

  cKeyWhereODBCqualified = '"CON_INDI.CISERIAL="+cp_ToStrODBC(this.w_CISERIAL)';

  cPrg = "gscg_aci"
  cComment = "Contabilizzazione indiretta effetti"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CODAZI = space(5)
  w_CISERIAL = space(10)
  o_CISERIAL = space(10)
  w_VALNAZ = space(3)
  w_TIPCON = space(1)
  w_CITIPCLF = space(1)
  w_CINUMERO = 0
  w_CICODESE = space(4)
  o_CICODESE = space(4)
  w_CIDATREG = ctod('  /  /  ')
  o_CIDATREG = ctod('  /  /  ')
  w_CISCAINI = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_CISCAFIN = ctod('  /  /  ')
  w_CIFLDEFI = space(1)
  w_CIDATVAL = ctod('  /  /  ')
  o_CIDATVAL = ctod('  /  /  ')
  w_CIDESCRI = space(35)
  w_DATOBSO = ctod('  /  /  ')
  w_TIPREG = space(1)
  w_FLPART = space(1)
  w_CICODVAL = space(3)
  o_CICODVAL = space(3)
  w_DESAPP = space(35)
  w_TESVAL = 0
  w_DECTOT = 0
  o_DECTOT = 0
  w_CALCPICT = 0
  w_SIMVAL = space(5)
  w_CICAOVAL = 0
  w_CIRIFCON = space(10)
  w_SELEZI = space(1)
  w_SERIALE = space(10)
  w_TIPMOV = 0
  w_ROWRIF = 0
  w_FLSELE = 0
  w_CIPAG_RB = space(2)
  w_CIPAG_RD = space(2)
  w_CIPAG_RI = space(2)
  w_CIPAG_BO = space(2)
  w_CIPAG_MA = space(2)
  w_CIFLSIPA = space(1)
  w_CIPAG_CA = space(2)
  w_CICODCLF = space(15)
  w_DESCLF = space(40)
  w_CICODCAU = space(5)
  w_DESCAU = space(35)
  w_CICONEFA = space(15)
  w_DESEFA = space(40)
  w_CICONDCA = space(15)
  w_DESDCA = space(40)
  w_CICONDCP = space(15)
  w_DESDCP = space(40)
  w_DTOBSO = ctod('  /  /  ')
  w_PTSERIAL = space(10)
  w_PTROWORD = 0
  w_CPROWNUM = 0
  w_FLSIPA = space(1)
  w_PTRIFIND = space(10)
  w_NAME_ZOOM = space(30)
  w_FASE = 0
  w_DATA = ctod('  /  /  ')
  w_IMPORTO = 0
  w_FLSELE = 0
  w_TIPSCA = space(1)
  w_MESSEGN = space(1)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_CISERIAL = this.W_CISERIAL
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_CICODESE = this.W_CICODESE
  op_CINUMERO = this.W_CINUMERO
  w_CalcZoom = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gscg_aci
  * Conterr� il w_CISERIAL (valorizzato Insert End)
  * per re interrogare il database
  w_OLDCISER=Space(10)
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CON_INDI','gscg_aci')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_aciPag1","gscg_aci",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Distinta")
      .Pages(1).HelpContextID = 25069417
      .Pages(2).addobject("oPag","tgscg_aciPag2","gscg_aci",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Parametri")
      .Pages(2).HelpContextID = 173119496
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
    proc Init()
      this.w_CalcZoom = this.oPgFrm.Pages(1).oPag.CalcZoom
      DoDefault()
    proc Destroy()
      this.w_CalcZoom = .NULL.
      DoDefault()

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='CONTROPA'
    this.cWorkTables[2]='ESERCIZI'
    this.cWorkTables[3]='VALUTE'
    this.cWorkTables[4]='CAU_CONT'
    this.cWorkTables[5]='CONTI'
    this.cWorkTables[6]='CON_INDI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(6))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CON_INDI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CON_INDI_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_CISERIAL = NVL(CISERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_18_joined
    link_1_18_joined=.f.
    local link_2_9_joined
    link_2_9_joined=.f.
    local link_2_13_joined
    link_2_13_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from CON_INDI where CISERIAL=KeySet.CISERIAL
    *
    i_nConn = i_TableProp[this.CON_INDI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CON_INDI_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CON_INDI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CON_INDI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CON_INDI '
      link_1_18_joined=this.AddJoinedLink_1_18(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_9_joined=this.AddJoinedLink_2_9(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_13_joined=this.AddJoinedLink_2_13(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CISERIAL',this.w_CISERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CODAZI = i_CODAZI
        .w_VALNAZ = g_PERVAL
        .w_DATOBSO = ctod("  /  /  ")
        .w_TIPREG = space(1)
        .w_FLPART = space(1)
        .w_DESAPP = space(35)
        .w_TESVAL = 0
        .w_DECTOT = 0
        .w_SIMVAL = space(5)
        .w_SELEZI = 'D'
        .w_SERIALE = 'XXXXXXXXXX'
        .w_TIPMOV = 1
        .w_ROWRIF = 1
        .w_FLSELE = 0
        .w_DESCLF = space(40)
        .w_DESCAU = space(35)
        .w_DESEFA = space(40)
        .w_DESDCA = space(40)
        .w_DESDCP = space(40)
        .w_DTOBSO = ctod("  /  /  ")
        .w_FLSIPA = space(1)
        .w_NAME_ZOOM = 'Query\GSCG_KCI'
        .w_FASE = 0
        .w_IMPORTO = 0
        .w_FLSELE = 0
        .w_TIPSCA = space(1)
          .link_1_1('Load')
        .w_CISERIAL = NVL(CISERIAL,space(10))
        .op_CISERIAL = .w_CISERIAL
        .w_TIPCON = 'G'
        .w_CITIPCLF = NVL(CITIPCLF,space(1))
        .w_CINUMERO = NVL(CINUMERO,0)
        .op_CINUMERO = .w_CINUMERO
        .w_CICODESE = NVL(CICODESE,space(4))
        .op_CICODESE = .w_CICODESE
          .link_1_7('Load')
        .w_CIDATREG = NVL(cp_ToDate(CIDATREG),ctod("  /  /  "))
        .w_CISCAINI = NVL(cp_ToDate(CISCAINI),ctod("  /  /  "))
        .w_OBTEST = .w_CIDATREG
        .w_CISCAFIN = NVL(cp_ToDate(CISCAFIN),ctod("  /  /  "))
        .w_CIFLDEFI = NVL(CIFLDEFI,space(1))
        .w_CIDATVAL = NVL(cp_ToDate(CIDATVAL),ctod("  /  /  "))
        .w_CIDESCRI = NVL(CIDESCRI,space(35))
        .w_CICODVAL = NVL(CICODVAL,space(3))
          if link_1_18_joined
            this.w_CICODVAL = NVL(VACODVAL118,NVL(this.w_CICODVAL,space(3)))
            this.w_DESAPP = NVL(VADESVAL118,space(35))
            this.w_TESVAL = NVL(VACAOVAL118,0)
            this.w_SIMVAL = NVL(VASIMVAL118,space(5))
            this.w_DECTOT = NVL(VADECTOT118,0)
            this.w_DTOBSO = NVL(cp_ToDate(VADTOBSO118),ctod("  /  /  "))
          else
          .link_1_18('Load')
          endif
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_CICAOVAL = NVL(CICAOVAL,0)
        .w_CIRIFCON = NVL(CIRIFCON,space(10))
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
        .oPgFrm.Page1.oPag.CalcZoom.Calculate()
        .w_CIPAG_RB = NVL(CIPAG_RB,space(2))
        .w_CIPAG_RD = NVL(CIPAG_RD,space(2))
        .w_CIPAG_RI = NVL(CIPAG_RI,space(2))
        .w_CIPAG_BO = NVL(CIPAG_BO,space(2))
        .w_CIPAG_MA = NVL(CIPAG_MA,space(2))
        .w_CIFLSIPA = NVL(CIFLSIPA,space(1))
        .w_CIPAG_CA = NVL(CIPAG_CA,space(2))
        .w_CICODCLF = NVL(CICODCLF,space(15))
          if link_2_9_joined
            this.w_CICODCLF = NVL(ANCODICE209,NVL(this.w_CICODCLF,space(15)))
            this.w_DESCLF = NVL(ANDESCRI209,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO209),ctod("  /  /  "))
          else
          .link_2_9('Load')
          endif
        .w_CICODCAU = NVL(CICODCAU,space(5))
          if link_2_13_joined
            this.w_CICODCAU = NVL(CCCODICE213,NVL(this.w_CICODCAU,space(5)))
            this.w_DESCAU = NVL(CCDESCRI213,space(35))
            this.w_TIPREG = NVL(CCTIPREG213,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(CCDTOBSO213),ctod("  /  /  "))
          else
          .link_2_13('Load')
          endif
        .w_CICONEFA = NVL(CICONEFA,space(15))
          .link_2_15('Load')
        .w_CICONDCA = NVL(CICONDCA,space(15))
          .link_2_17('Load')
        .w_CICONDCP = NVL(CICONDCP,space(15))
          .link_2_19('Load')
        .w_PTSERIAL = nvl(.w_CalcZoom.getVar('PTSERIAL'), space(10))
        .w_PTROWORD = nvl(.w_CalcZoom.getVar('PTROWORD'), 0)
        .w_CPROWNUM = nvl(.w_CalcZoom.getVar('CPROWNUM'), 0)
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate()
        .w_PTRIFIND = nvl(.w_CalcZoom.getVar('PTRIFIND'), space(10))
        .oPgFrm.Page1.oPag.oObj_1_53.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_55.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
        .w_DATA = nvl(.w_CalcZoom.getVar('DATSCA'), cp_CharToDate('  -  -  '))
        .oPgFrm.Page1.oPag.oObj_1_64.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate()
        .w_MESSEGN = nvl(.w_CalcZoom.GetVar('SEGNO'), space(1))
        .oPgFrm.Page1.oPag.oObj_1_67.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_68.Calculate()
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'CON_INDI')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_34.enabled = this.oPgFrm.Page1.oPag.oBtn_1_34.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI = space(5)
      .w_CISERIAL = space(10)
      .w_VALNAZ = space(3)
      .w_TIPCON = space(1)
      .w_CITIPCLF = space(1)
      .w_CINUMERO = 0
      .w_CICODESE = space(4)
      .w_CIDATREG = ctod("  /  /  ")
      .w_CISCAINI = ctod("  /  /  ")
      .w_OBTEST = ctod("  /  /  ")
      .w_CISCAFIN = ctod("  /  /  ")
      .w_CIFLDEFI = space(1)
      .w_CIDATVAL = ctod("  /  /  ")
      .w_CIDESCRI = space(35)
      .w_DATOBSO = ctod("  /  /  ")
      .w_TIPREG = space(1)
      .w_FLPART = space(1)
      .w_CICODVAL = space(3)
      .w_DESAPP = space(35)
      .w_TESVAL = 0
      .w_DECTOT = 0
      .w_CALCPICT = 0
      .w_SIMVAL = space(5)
      .w_CICAOVAL = 0
      .w_CIRIFCON = space(10)
      .w_SELEZI = space(1)
      .w_SERIALE = space(10)
      .w_TIPMOV = 0
      .w_ROWRIF = 0
      .w_FLSELE = 0
      .w_CIPAG_RB = space(2)
      .w_CIPAG_RD = space(2)
      .w_CIPAG_RI = space(2)
      .w_CIPAG_BO = space(2)
      .w_CIPAG_MA = space(2)
      .w_CIFLSIPA = space(1)
      .w_CIPAG_CA = space(2)
      .w_CICODCLF = space(15)
      .w_DESCLF = space(40)
      .w_CICODCAU = space(5)
      .w_DESCAU = space(35)
      .w_CICONEFA = space(15)
      .w_DESEFA = space(40)
      .w_CICONDCA = space(15)
      .w_DESDCA = space(40)
      .w_CICONDCP = space(15)
      .w_DESDCP = space(40)
      .w_DTOBSO = ctod("  /  /  ")
      .w_PTSERIAL = space(10)
      .w_PTROWORD = 0
      .w_CPROWNUM = 0
      .w_FLSIPA = space(1)
      .w_PTRIFIND = space(10)
      .w_NAME_ZOOM = space(30)
      .w_FASE = 0
      .w_DATA = ctod("  /  /  ")
      .w_IMPORTO = 0
      .w_FLSELE = 0
      .w_TIPSCA = space(1)
      .w_MESSEGN = space(1)
      if .cFunction<>"Filter"
        .w_CODAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_CODAZI))
          .link_1_1('Full')
          endif
          .DoRTCalc(2,2,.f.)
        .w_VALNAZ = g_PERVAL
        .w_TIPCON = 'G'
        .w_CITIPCLF = 'C'
          .DoRTCalc(6,6,.f.)
        .w_CICODESE = g_CODESE
        .DoRTCalc(7,7,.f.)
          if not(empty(.w_CICODESE))
          .link_1_7('Full')
          endif
        .w_CIDATREG = i_datsys
        .w_CISCAINI = .w_CIDATREG
        .w_OBTEST = .w_CIDATREG
        .w_CISCAFIN = .w_CISCAINI+120
          .DoRTCalc(12,12,.f.)
        .w_CIDATVAL = .w_CIDATREG
          .DoRTCalc(14,17,.f.)
        .w_CICODVAL = .w_VALNAZ
        .DoRTCalc(18,18,.f.)
          if not(empty(.w_CICODVAL))
          .link_1_18('Full')
          endif
          .DoRTCalc(19,21,.f.)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
          .DoRTCalc(23,23,.f.)
        .w_CICAOVAL = IIF(EMPTY(.w_CICODVAL) OR EMPTY(.w_CIDATVAL), 0, GETCAM(.w_CICODVAL, .w_CIDATVAL, 7))
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
        .oPgFrm.Page1.oPag.CalcZoom.Calculate()
          .DoRTCalc(25,25,.f.)
        .w_SELEZI = 'D'
        .w_SERIALE = 'XXXXXXXXXX'
        .w_TIPMOV = 1
        .w_ROWRIF = 1
        .w_FLSELE = 0
        .w_CIPAG_RB = 'RB'
        .w_CIPAG_RD = '  '
        .w_CIPAG_RI = 'RI'
        .w_CIPAG_BO = '  '
        .w_CIPAG_MA = 'MA'
        .w_CIFLSIPA = .w_FLSIPA
        .w_CIPAG_CA = 'CA'
        .DoRTCalc(38,38,.f.)
          if not(empty(.w_CICODCLF))
          .link_2_9('Full')
          endif
        .DoRTCalc(39,40,.f.)
          if not(empty(.w_CICODCAU))
          .link_2_13('Full')
          endif
        .DoRTCalc(41,42,.f.)
          if not(empty(.w_CICONEFA))
          .link_2_15('Full')
          endif
        .DoRTCalc(43,44,.f.)
          if not(empty(.w_CICONDCA))
          .link_2_17('Full')
          endif
        .DoRTCalc(45,46,.f.)
          if not(empty(.w_CICONDCP))
          .link_2_19('Full')
          endif
          .DoRTCalc(47,48,.f.)
        .w_PTSERIAL = nvl(.w_CalcZoom.getVar('PTSERIAL'), space(10))
        .w_PTROWORD = nvl(.w_CalcZoom.getVar('PTROWORD'), 0)
        .w_CPROWNUM = nvl(.w_CalcZoom.getVar('CPROWNUM'), 0)
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate()
          .DoRTCalc(52,52,.f.)
        .w_PTRIFIND = nvl(.w_CalcZoom.getVar('PTRIFIND'), space(10))
        .oPgFrm.Page1.oPag.oObj_1_53.Calculate()
        .w_NAME_ZOOM = 'Query\GSCG_KCI'
        .oPgFrm.Page1.oPag.oObj_1_55.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
        .w_FASE = 0
        .w_DATA = nvl(.w_CalcZoom.getVar('DATSCA'), cp_CharToDate('  -  -  '))
          .DoRTCalc(57,57,.f.)
        .w_FLSELE = 0
        .oPgFrm.Page1.oPag.oObj_1_64.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate()
          .DoRTCalc(59,59,.f.)
        .w_MESSEGN = nvl(.w_CalcZoom.GetVar('SEGNO'), space(1))
        .oPgFrm.Page1.oPag.oObj_1_67.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_68.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'CON_INDI')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_34.enabled = this.oPgFrm.Page1.oPag.oBtn_1_34.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gscg_aci
    * --- Refresh dello Zoom (se w_FASE=0, SE 2 da GSCG_BI3 non occorre aggiornare)
    If this.w_FASE=0
     this.NotifyEvent('Legge')
    EndIf
    
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CON_INDI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CON_INDI_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SECONIND","i_codazi,w_CISERIAL")
      cp_AskTableProg(this,i_nConn,"PRCONIND","i_codazi,w_CICODESE,w_CINUMERO")
      .op_codazi = .w_codazi
      .op_CISERIAL = .w_CISERIAL
      .op_codazi = .w_codazi
      .op_CICODESE = .w_CICODESE
      .op_CINUMERO = .w_CINUMERO
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCINUMERO_1_6.enabled = i_bVal
      .Page1.oPag.oCICODESE_1_7.enabled = i_bVal
      .Page1.oPag.oCIDATREG_1_8.enabled = i_bVal
      .Page1.oPag.oCISCAINI_1_9.enabled = i_bVal
      .Page1.oPag.oCISCAFIN_1_11.enabled = i_bVal
      .Page1.oPag.oCIDESCRI_1_14.enabled = i_bVal
      .Page1.oPag.oCICODVAL_1_18.enabled = i_bVal
      .Page1.oPag.oCICAOVAL_1_24.enabled = i_bVal
      .Page1.oPag.oSELEZI_1_38.enabled_(i_bVal)
      .Page2.oPag.oCIPAG_RB_2_2.enabled = i_bVal
      .Page2.oPag.oCIPAG_RD_2_3.enabled = i_bVal
      .Page2.oPag.oCIPAG_RI_2_4.enabled = i_bVal
      .Page2.oPag.oCIPAG_BO_2_5.enabled = i_bVal
      .Page2.oPag.oCIPAG_MA_2_6.enabled = i_bVal
      .Page2.oPag.oCIFLSIPA_2_7.enabled = i_bVal
      .Page2.oPag.oCIPAG_CA_2_8.enabled = i_bVal
      .Page2.oPag.oCICODCLF_2_9.enabled = i_bVal
      .Page2.oPag.oCICODCAU_2_13.enabled = i_bVal
      .Page2.oPag.oCICONEFA_2_15.enabled = i_bVal
      .Page2.oPag.oCICONDCA_2_17.enabled = i_bVal
      .Page2.oPag.oCICONDCP_2_19.enabled = i_bVal
      .Page1.oPag.oBtn_1_32.enabled = i_bVal
      .Page1.oPag.oBtn_1_34.enabled = .Page1.oPag.oBtn_1_34.mCond()
      .Page1.oPag.oBtn_1_39.enabled = i_bVal
      .Page1.oPag.oBtn_1_46.enabled = i_bVal
      .Page1.oPag.oBtn_1_48.enabled = i_bVal
      .Page1.oPag.oBtn_1_49.enabled = i_bVal
      .Page1.oPag.oObj_1_36.enabled = i_bVal
      .Page1.oPag.CalcZoom.enabled = i_bVal
      .Page1.oPag.oObj_1_50.enabled = i_bVal
      .Page1.oPag.oObj_1_53.enabled = i_bVal
      .Page1.oPag.oObj_1_55.enabled = i_bVal
      .Page1.oPag.oObj_1_56.enabled = i_bVal
      .Page1.oPag.oObj_1_64.enabled = i_bVal
      .Page1.oPag.oObj_1_65.enabled = i_bVal
      .Page1.oPag.oObj_1_67.enabled = i_bVal
      .Page1.oPag.oObj_1_68.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oCINUMERO_1_6.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'CON_INDI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CON_INDI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CISERIAL,"CISERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CITIPCLF,"CITIPCLF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CINUMERO,"CINUMERO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CICODESE,"CICODESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIDATREG,"CIDATREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CISCAINI,"CISCAINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CISCAFIN,"CISCAFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIFLDEFI,"CIFLDEFI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIDATVAL,"CIDATVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIDESCRI,"CIDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CICODVAL,"CICODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CICAOVAL,"CICAOVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIRIFCON,"CIRIFCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIPAG_RB,"CIPAG_RB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIPAG_RD,"CIPAG_RD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIPAG_RI,"CIPAG_RI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIPAG_BO,"CIPAG_BO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIPAG_MA,"CIPAG_MA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIFLSIPA,"CIFLSIPA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CIPAG_CA,"CIPAG_CA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CICODCLF,"CICODCLF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CICODCAU,"CICODCAU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CICONEFA,"CICONEFA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CICONDCA,"CICONDCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CICONDCP,"CICONDCP",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CON_INDI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CON_INDI_IDX,2])
    i_lTable = "CON_INDI"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CON_INDI_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      with this
        do GSCG_BCI with this
      endwith
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CON_INDI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CON_INDI_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.CON_INDI_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SECONIND","i_codazi,w_CISERIAL")
          cp_NextTableProg(this,i_nConn,"PRCONIND","i_codazi,w_CICODESE,w_CINUMERO")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CON_INDI
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CON_INDI')
        i_extval=cp_InsertValODBCExtFlds(this,'CON_INDI')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(CISERIAL,CITIPCLF,CINUMERO,CICODESE,CIDATREG"+;
                  ",CISCAINI,CISCAFIN,CIFLDEFI,CIDATVAL,CIDESCRI"+;
                  ",CICODVAL,CICAOVAL,CIRIFCON,CIPAG_RB,CIPAG_RD"+;
                  ",CIPAG_RI,CIPAG_BO,CIPAG_MA,CIFLSIPA,CIPAG_CA"+;
                  ",CICODCLF,CICODCAU,CICONEFA,CICONDCA,CICONDCP "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_CISERIAL)+;
                  ","+cp_ToStrODBC(this.w_CITIPCLF)+;
                  ","+cp_ToStrODBC(this.w_CINUMERO)+;
                  ","+cp_ToStrODBCNull(this.w_CICODESE)+;
                  ","+cp_ToStrODBC(this.w_CIDATREG)+;
                  ","+cp_ToStrODBC(this.w_CISCAINI)+;
                  ","+cp_ToStrODBC(this.w_CISCAFIN)+;
                  ","+cp_ToStrODBC(this.w_CIFLDEFI)+;
                  ","+cp_ToStrODBC(this.w_CIDATVAL)+;
                  ","+cp_ToStrODBC(this.w_CIDESCRI)+;
                  ","+cp_ToStrODBCNull(this.w_CICODVAL)+;
                  ","+cp_ToStrODBC(this.w_CICAOVAL)+;
                  ","+cp_ToStrODBC(this.w_CIRIFCON)+;
                  ","+cp_ToStrODBC(this.w_CIPAG_RB)+;
                  ","+cp_ToStrODBC(this.w_CIPAG_RD)+;
                  ","+cp_ToStrODBC(this.w_CIPAG_RI)+;
                  ","+cp_ToStrODBC(this.w_CIPAG_BO)+;
                  ","+cp_ToStrODBC(this.w_CIPAG_MA)+;
                  ","+cp_ToStrODBC(this.w_CIFLSIPA)+;
                  ","+cp_ToStrODBC(this.w_CIPAG_CA)+;
                  ","+cp_ToStrODBCNull(this.w_CICODCLF)+;
                  ","+cp_ToStrODBCNull(this.w_CICODCAU)+;
                  ","+cp_ToStrODBCNull(this.w_CICONEFA)+;
                  ","+cp_ToStrODBCNull(this.w_CICONDCA)+;
                  ","+cp_ToStrODBCNull(this.w_CICONDCP)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CON_INDI')
        i_extval=cp_InsertValVFPExtFlds(this,'CON_INDI')
        cp_CheckDeletedKey(i_cTable,0,'CISERIAL',this.w_CISERIAL)
        INSERT INTO (i_cTable);
              (CISERIAL,CITIPCLF,CINUMERO,CICODESE,CIDATREG,CISCAINI,CISCAFIN,CIFLDEFI,CIDATVAL,CIDESCRI,CICODVAL,CICAOVAL,CIRIFCON,CIPAG_RB,CIPAG_RD,CIPAG_RI,CIPAG_BO,CIPAG_MA,CIFLSIPA,CIPAG_CA,CICODCLF,CICODCAU,CICONEFA,CICONDCA,CICONDCP  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_CISERIAL;
                  ,this.w_CITIPCLF;
                  ,this.w_CINUMERO;
                  ,this.w_CICODESE;
                  ,this.w_CIDATREG;
                  ,this.w_CISCAINI;
                  ,this.w_CISCAFIN;
                  ,this.w_CIFLDEFI;
                  ,this.w_CIDATVAL;
                  ,this.w_CIDESCRI;
                  ,this.w_CICODVAL;
                  ,this.w_CICAOVAL;
                  ,this.w_CIRIFCON;
                  ,this.w_CIPAG_RB;
                  ,this.w_CIPAG_RD;
                  ,this.w_CIPAG_RI;
                  ,this.w_CIPAG_BO;
                  ,this.w_CIPAG_MA;
                  ,this.w_CIFLSIPA;
                  ,this.w_CIPAG_CA;
                  ,this.w_CICODCLF;
                  ,this.w_CICODCAU;
                  ,this.w_CICONEFA;
                  ,this.w_CICONDCA;
                  ,this.w_CICONDCP;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- gscg_aci
    * Mantengo il valore di w_CISERIAL
    * per reinterrogare il database e ricaricare
    * la gestione (GSCG_BI3)
    This.w_OLDCISER=This.w_CISERIAL
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.CON_INDI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CON_INDI_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.CON_INDI_IDX,i_nConn)
      *
      * update CON_INDI
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'CON_INDI')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " CITIPCLF="+cp_ToStrODBC(this.w_CITIPCLF)+;
             ",CINUMERO="+cp_ToStrODBC(this.w_CINUMERO)+;
             ",CICODESE="+cp_ToStrODBCNull(this.w_CICODESE)+;
             ",CIDATREG="+cp_ToStrODBC(this.w_CIDATREG)+;
             ",CISCAINI="+cp_ToStrODBC(this.w_CISCAINI)+;
             ",CISCAFIN="+cp_ToStrODBC(this.w_CISCAFIN)+;
             ",CIFLDEFI="+cp_ToStrODBC(this.w_CIFLDEFI)+;
             ",CIDATVAL="+cp_ToStrODBC(this.w_CIDATVAL)+;
             ",CIDESCRI="+cp_ToStrODBC(this.w_CIDESCRI)+;
             ",CICODVAL="+cp_ToStrODBCNull(this.w_CICODVAL)+;
             ",CICAOVAL="+cp_ToStrODBC(this.w_CICAOVAL)+;
             ",CIRIFCON="+cp_ToStrODBC(this.w_CIRIFCON)+;
             ",CIPAG_RB="+cp_ToStrODBC(this.w_CIPAG_RB)+;
             ",CIPAG_RD="+cp_ToStrODBC(this.w_CIPAG_RD)+;
             ",CIPAG_RI="+cp_ToStrODBC(this.w_CIPAG_RI)+;
             ",CIPAG_BO="+cp_ToStrODBC(this.w_CIPAG_BO)+;
             ",CIPAG_MA="+cp_ToStrODBC(this.w_CIPAG_MA)+;
             ",CIFLSIPA="+cp_ToStrODBC(this.w_CIFLSIPA)+;
             ",CIPAG_CA="+cp_ToStrODBC(this.w_CIPAG_CA)+;
             ",CICODCLF="+cp_ToStrODBCNull(this.w_CICODCLF)+;
             ",CICODCAU="+cp_ToStrODBCNull(this.w_CICODCAU)+;
             ",CICONEFA="+cp_ToStrODBCNull(this.w_CICONEFA)+;
             ",CICONDCA="+cp_ToStrODBCNull(this.w_CICONDCA)+;
             ",CICONDCP="+cp_ToStrODBCNull(this.w_CICONDCP)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'CON_INDI')
        i_cWhere = cp_PKFox(i_cTable  ,'CISERIAL',this.w_CISERIAL  )
        UPDATE (i_cTable) SET;
              CITIPCLF=this.w_CITIPCLF;
             ,CINUMERO=this.w_CINUMERO;
             ,CICODESE=this.w_CICODESE;
             ,CIDATREG=this.w_CIDATREG;
             ,CISCAINI=this.w_CISCAINI;
             ,CISCAFIN=this.w_CISCAFIN;
             ,CIFLDEFI=this.w_CIFLDEFI;
             ,CIDATVAL=this.w_CIDATVAL;
             ,CIDESCRI=this.w_CIDESCRI;
             ,CICODVAL=this.w_CICODVAL;
             ,CICAOVAL=this.w_CICAOVAL;
             ,CIRIFCON=this.w_CIRIFCON;
             ,CIPAG_RB=this.w_CIPAG_RB;
             ,CIPAG_RD=this.w_CIPAG_RD;
             ,CIPAG_RI=this.w_CIPAG_RI;
             ,CIPAG_BO=this.w_CIPAG_BO;
             ,CIPAG_MA=this.w_CIPAG_MA;
             ,CIFLSIPA=this.w_CIFLSIPA;
             ,CIPAG_CA=this.w_CIPAG_CA;
             ,CICODCLF=this.w_CICODCLF;
             ,CICODCAU=this.w_CICODCAU;
             ,CICONEFA=this.w_CICONEFA;
             ,CICONDCA=this.w_CICONDCA;
             ,CICONDCP=this.w_CICONDCP;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CON_INDI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CON_INDI_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.CON_INDI_IDX,i_nConn)
      *
      * delete CON_INDI
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'CISERIAL',this.w_CISERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CON_INDI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CON_INDI_IDX,2])
    if i_bUpd
      with this
        if .o_CISERIAL<>.w_CISERIAL
          .link_1_1('Full')
        endif
        .DoRTCalc(2,3,.t.)
            .w_TIPCON = 'G'
        .DoRTCalc(5,8,.t.)
        if .o_CIDATREG<>.w_CIDATREG
            .w_CISCAINI = .w_CIDATREG
        endif
            .w_OBTEST = .w_CIDATREG
        .DoRTCalc(11,12,.t.)
            .w_CIDATVAL = .w_CIDATREG
        .DoRTCalc(14,17,.t.)
        if .o_CICODESE<>.w_CICODESE
            .w_CICODVAL = .w_VALNAZ
          .link_1_18('Full')
        endif
        .DoRTCalc(19,21,.t.)
        if .o_DECTOT<>.w_DECTOT
            .w_CALCPICT = DEFPIC(.w_DECTOT)
        endif
        .DoRTCalc(23,23,.t.)
        if .o_CICODVAL<>.w_CICODVAL.or. .o_CIDATVAL<>.w_CIDATVAL.or. .o_CIDATREG<>.w_CIDATREG
            .w_CICAOVAL = IIF(EMPTY(.w_CICODVAL) OR EMPTY(.w_CIDATVAL), 0, GETCAM(.w_CICODVAL, .w_CIDATVAL, 7))
        endif
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
        .oPgFrm.Page1.oPag.CalcZoom.Calculate()
        .DoRTCalc(25,48,.t.)
            .w_PTSERIAL = nvl(.w_CalcZoom.getVar('PTSERIAL'), space(10))
            .w_PTROWORD = nvl(.w_CalcZoom.getVar('PTROWORD'), 0)
            .w_CPROWNUM = nvl(.w_CalcZoom.getVar('CPROWNUM'), 0)
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate()
        .DoRTCalc(52,52,.t.)
            .w_PTRIFIND = nvl(.w_CalcZoom.getVar('PTRIFIND'), space(10))
        .oPgFrm.Page1.oPag.oObj_1_53.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_55.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
        .DoRTCalc(54,55,.t.)
            .w_DATA = nvl(.w_CalcZoom.getVar('DATSCA'), cp_CharToDate('  -  -  '))
        .oPgFrm.Page1.oPag.oObj_1_64.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate()
        .DoRTCalc(57,59,.t.)
            .w_MESSEGN = nvl(.w_CalcZoom.GetVar('SEGNO'), space(1))
        .oPgFrm.Page1.oPag.oObj_1_67.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_68.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"SECONIND","i_codazi,w_CISERIAL")
          .op_CISERIAL = .w_CISERIAL
        endif
        if .op_codazi<>.w_codazi .or. .op_CICODESE<>.w_CICODESE
           cp_AskTableProg(this,i_nConn,"PRCONIND","i_codazi,w_CICODESE,w_CINUMERO")
          .op_CINUMERO = .w_CINUMERO
        endif
        .op_codazi = .w_codazi
        .op_codazi = .w_codazi
        .op_CICODESE = .w_CICODESE
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
        .oPgFrm.Page1.oPag.CalcZoom.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_53.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_55.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_64.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_67.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_68.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCINUMERO_1_6.enabled = this.oPgFrm.Page1.oPag.oCINUMERO_1_6.mCond()
    this.oPgFrm.Page1.oPag.oCICODESE_1_7.enabled = this.oPgFrm.Page1.oPag.oCICODESE_1_7.mCond()
    this.oPgFrm.Page1.oPag.oCIDATREG_1_8.enabled = this.oPgFrm.Page1.oPag.oCIDATREG_1_8.mCond()
    this.oPgFrm.Page1.oPag.oCICODVAL_1_18.enabled = this.oPgFrm.Page1.oPag.oCICODVAL_1_18.mCond()
    this.oPgFrm.Page1.oPag.oCICAOVAL_1_24.enabled = this.oPgFrm.Page1.oPag.oCICAOVAL_1_24.mCond()
    this.oPgFrm.Page1.oPag.oSELEZI_1_38.enabled_(this.oPgFrm.Page1.oPag.oSELEZI_1_38.mCond())
    this.oPgFrm.Page2.oPag.oCIFLSIPA_2_7.enabled = this.oPgFrm.Page2.oPag.oCIFLSIPA_2_7.mCond()
    this.oPgFrm.Page2.oPag.oCICODCLF_2_9.enabled = this.oPgFrm.Page2.oPag.oCICODCLF_2_9.mCond()
    this.oPgFrm.Page2.oPag.oCICODCAU_2_13.enabled = this.oPgFrm.Page2.oPag.oCICODCAU_2_13.mCond()
    this.oPgFrm.Page2.oPag.oCICONEFA_2_15.enabled = this.oPgFrm.Page2.oPag.oCICONEFA_2_15.mCond()
    this.oPgFrm.Page2.oPag.oCICONDCA_2_17.enabled = this.oPgFrm.Page2.oPag.oCICONDCA_2_17.mCond()
    this.oPgFrm.Page2.oPag.oCICONDCP_2_19.enabled = this.oPgFrm.Page2.oPag.oCICONDCP_2_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_34.enabled = this.oPgFrm.Page1.oPag.oBtn_1_34.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_39.enabled = this.oPgFrm.Page1.oPag.oBtn_1_39.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_46.enabled = this.oPgFrm.Page1.oPag.oBtn_1_46.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_48.enabled = this.oPgFrm.Page1.oPag.oBtn_1_48.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_49.enabled = this.oPgFrm.Page1.oPag.oBtn_1_49.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_34.visible=!this.oPgFrm.Page1.oPag.oBtn_1_34.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_46.visible=!this.oPgFrm.Page1.oPag.oBtn_1_46.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_48.visible=!this.oPgFrm.Page1.oPag.oBtn_1_48.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_49.visible=!this.oPgFrm.Page1.oPag.oBtn_1_49.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_58.visible=!this.oPgFrm.Page1.oPag.oStr_1_58.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gscg_aci
    * Allo scatenarsi dell'evento Legge
    * lo zoom � aggiornato da procedura
    * w_NOME_ZOOM contiene la query da utilizzare (GSCG_BI3)
    If cEvent='Legge'
     This.w_CalcZoom.cCpQueryName=This.w_NAME_ZOOM
    endif
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_36.Event(cEvent)
      .oPgFrm.Page1.oPag.CalcZoom.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_50.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_53.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_55.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_56.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_64.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_65.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_67.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_68.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gscg_aci
     *this.NotifyEvent('Aggiorna')
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_lTable = "CONTROPA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2], .t., this.CONTROPA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COCODAZI,COCONEFA,COCAUEFA,COSADIFC,COSADIFN,COFLSIPA";
                   +" from "+i_cTable+" "+i_lTable+" where COCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COCODAZI',this.w_CODAZI)
            select COCODAZI,COCONEFA,COCAUEFA,COSADIFC,COSADIFN,COFLSIPA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.COCODAZI,space(5))
      this.w_CICONEFA = NVL(_Link_.COCONEFA,space(15))
      this.w_CICODCAU = NVL(_Link_.COCAUEFA,space(5))
      this.w_CICONDCA = NVL(_Link_.COSADIFC,space(15))
      this.w_CICONDCP = NVL(_Link_.COSADIFN,space(15))
      this.w_FLSIPA = NVL(_Link_.COFLSIPA,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_CICONEFA = space(15)
      this.w_CICODCAU = space(5)
      this.w_CICONDCA = space(15)
      this.w_CICONDCP = space(15)
      this.w_FLSIPA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])+'\'+cp_ToStr(_Link_.COCODAZI,1)
      cp_ShowWarn(i_cKey,this.CONTROPA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CICODESE
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CICODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_CICODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_CICODESE))
          select ESCODAZI,ESCODESE,ESVALNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CICODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CICODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oCICODESE_1_7'),i_cWhere,'',"Elenco esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CICODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_CICODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_CICODESE)
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CICODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_VALNAZ = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CICODESE = space(4)
      endif
      this.w_VALNAZ = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CICODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CICODVAL
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CICODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_CICODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VACAOVAL,VASIMVAL,VADECTOT,VADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_CICODVAL))
          select VACODVAL,VADESVAL,VACAOVAL,VASIMVAL,VADECTOT,VADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CICODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStrODBC(trim(this.w_CICODVAL)+"%");

            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VACAOVAL,VASIMVAL,VADECTOT,VADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStr(trim(this.w_CICODVAL)+"%");

            select VACODVAL,VADESVAL,VACAOVAL,VASIMVAL,VADECTOT,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CICODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oCICODVAL_1_18'),i_cWhere,'GSAR_AVL',"Elenco valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VACAOVAL,VASIMVAL,VADECTOT,VADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL,VACAOVAL,VASIMVAL,VADECTOT,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CICODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VACAOVAL,VASIMVAL,VADECTOT,VADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CICODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CICODVAL)
            select VACODVAL,VADESVAL,VACAOVAL,VASIMVAL,VADECTOT,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CICODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DESAPP = NVL(_Link_.VADESVAL,space(35))
      this.w_TESVAL = NVL(_Link_.VACAOVAL,0)
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.VADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CICODVAL = space(3)
      endif
      this.w_DESAPP = space(35)
      this.w_TESVAL = 0
      this.w_SIMVAL = space(5)
      this.w_DECTOT = 0
      this.w_DTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CICODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_18(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+6<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_18.VACODVAL as VACODVAL118"+ ",link_1_18.VADESVAL as VADESVAL118"+ ",link_1_18.VACAOVAL as VACAOVAL118"+ ",link_1_18.VASIMVAL as VASIMVAL118"+ ",link_1_18.VADECTOT as VADECTOT118"+ ",link_1_18.VADTOBSO as VADTOBSO118"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_18 on CON_INDI.CICODVAL=link_1_18.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_18"
          i_cKey=i_cKey+'+" and CON_INDI.CICODVAL=link_1_18.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CICODCLF
  func Link_2_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CICODCLF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CICODCLF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CITIPCLF);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_CITIPCLF;
                     ,'ANCODICE',trim(this.w_CICODCLF))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CICODCLF)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CICODCLF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CITIPCLF);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CICODCLF)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_CITIPCLF);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CICODCLF) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCICODCLF_2_9'),i_cWhere,'GSAR_BZC',"Elenco clienti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CITIPCLF<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_CITIPCLF);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CICODCLF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CICODCLF);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CITIPCLF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_CITIPCLF;
                       ,'ANCODICE',this.w_CICODCLF)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CICODCLF = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLF = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CICODCLF = space(15)
      endif
      this.w_DESCLF = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CICODCLF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_9(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_9.ANCODICE as ANCODICE209"+ ",link_2_9.ANDESCRI as ANDESCRI209"+ ",link_2_9.ANDTOBSO as ANDTOBSO209"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_9 on CON_INDI.CICODCLF=link_2_9.ANCODICE"+" and CON_INDI.CITIPCLF=link_2_9.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_9"
          i_cKey=i_cKey+'+" and CON_INDI.CICODCLF=link_2_9.ANCODICE(+)"'+'+" and CON_INDI.CITIPCLF=link_2_9.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CICODCAU
  func Link_2_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CICODCAU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_CICODCAU)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_CICODCAU))
          select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CICODCAU)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_CICODCAU)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_CICODCAU)+"%");

            select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CICODCAU) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCICODCAU_2_13'),i_cWhere,'GSCG_ACC',"Causali contabili",'GSTE_ACD.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CICODCAU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CICODCAU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CICODCAU)
            select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CICODCAU = NVL(_Link_.CCCODICE,space(5))
      this.w_DESCAU = NVL(_Link_.CCDESCRI,space(35))
      this.w_TIPREG = NVL(_Link_.CCTIPREG,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CICODCAU = space(5)
      endif
      this.w_DESCAU = space(35)
      this.w_TIPREG = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_TIPREG='N'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale contabile inesistente, obsoleta o di tipo IVA")
        endif
        this.w_CICODCAU = space(5)
        this.w_DESCAU = space(35)
        this.w_TIPREG = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CICODCAU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_13(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_13.CCCODICE as CCCODICE213"+ ",link_2_13.CCDESCRI as CCDESCRI213"+ ",link_2_13.CCTIPREG as CCTIPREG213"+ ",link_2_13.CCDTOBSO as CCDTOBSO213"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_13 on CON_INDI.CICODCAU=link_2_13.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_13"
          i_cKey=i_cKey+'+" and CON_INDI.CICODCAU=link_2_13.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CICONEFA
  func Link_2_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CICONEFA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CICONEFA)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CICONEFA))
          select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CICONEFA)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CICONEFA)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CICONEFA)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CICONEFA) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCICONEFA_2_15'),i_cWhere,'GSAR_BZC',"Conti effetti attivi",'CONCOEFA.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CICONEFA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CICONEFA);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CICONEFA)
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CICONEFA = NVL(_Link_.ANCODICE,space(15))
      this.w_DESEFA = NVL(_Link_.ANDESCRI,space(40))
      this.w_FLPART = NVL(_Link_.ANPARTSN,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CICONEFA = space(15)
      endif
      this.w_DESEFA = space(40)
      this.w_FLPART = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST AND EMPTY(.w_FLPART)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
        endif
        this.w_CICONEFA = space(15)
        this.w_DESEFA = space(40)
        this.w_FLPART = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CICONEFA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CICONDCA
  func Link_2_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CICONDCA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CICONDCA)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CICONDCA))
          select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CICONDCA)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CICONDCA)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CICONDCA)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CICONDCA) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCICONDCA_2_17'),i_cWhere,'GSAR_BZC',"Conti differenze cambi",'CONCOEFA.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CICONDCA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CICONDCA);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CICONDCA)
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CICONDCA = NVL(_Link_.ANCODICE,space(15))
      this.w_DESDCA = NVL(_Link_.ANDESCRI,space(40))
      this.w_FLPART = NVL(_Link_.ANPARTSN,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CICONDCA = space(15)
      endif
      this.w_DESDCA = space(40)
      this.w_FLPART = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST AND EMPTY(.w_FLPART)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
        endif
        this.w_CICONDCA = space(15)
        this.w_DESDCA = space(40)
        this.w_FLPART = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CICONDCA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CICONDCP
  func Link_2_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CICONDCP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CICONDCP)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CICONDCP))
          select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CICONDCP)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CICONDCP)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CICONDCP)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CICONDCP) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCICONDCP_2_19'),i_cWhere,'GSAR_BZC',"Conti differenze cambi",'CONCOEFA.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CICONDCP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CICONDCP);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CICONDCP)
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CICONDCP = NVL(_Link_.ANCODICE,space(15))
      this.w_DESDCP = NVL(_Link_.ANDESCRI,space(40))
      this.w_FLPART = NVL(_Link_.ANPARTSN,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CICONDCP = space(15)
      endif
      this.w_DESDCP = space(40)
      this.w_FLPART = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST AND EMPTY(.w_FLPART)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
        endif
        this.w_CICONDCP = space(15)
        this.w_DESDCP = space(40)
        this.w_FLPART = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CICONDCP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCINUMERO_1_6.value==this.w_CINUMERO)
      this.oPgFrm.Page1.oPag.oCINUMERO_1_6.value=this.w_CINUMERO
    endif
    if not(this.oPgFrm.Page1.oPag.oCICODESE_1_7.value==this.w_CICODESE)
      this.oPgFrm.Page1.oPag.oCICODESE_1_7.value=this.w_CICODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oCIDATREG_1_8.value==this.w_CIDATREG)
      this.oPgFrm.Page1.oPag.oCIDATREG_1_8.value=this.w_CIDATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oCISCAINI_1_9.value==this.w_CISCAINI)
      this.oPgFrm.Page1.oPag.oCISCAINI_1_9.value=this.w_CISCAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCISCAFIN_1_11.value==this.w_CISCAFIN)
      this.oPgFrm.Page1.oPag.oCISCAFIN_1_11.value=this.w_CISCAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCIFLDEFI_1_12.RadioValue()==this.w_CIFLDEFI)
      this.oPgFrm.Page1.oPag.oCIFLDEFI_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCIDESCRI_1_14.value==this.w_CIDESCRI)
      this.oPgFrm.Page1.oPag.oCIDESCRI_1_14.value=this.w_CIDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCICODVAL_1_18.value==this.w_CICODVAL)
      this.oPgFrm.Page1.oPag.oCICODVAL_1_18.value=this.w_CICODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oCICAOVAL_1_24.value==this.w_CICAOVAL)
      this.oPgFrm.Page1.oPag.oCICAOVAL_1_24.value=this.w_CICAOVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_38.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_38.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCIPAG_RB_2_2.RadioValue()==this.w_CIPAG_RB)
      this.oPgFrm.Page2.oPag.oCIPAG_RB_2_2.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCIPAG_RD_2_3.RadioValue()==this.w_CIPAG_RD)
      this.oPgFrm.Page2.oPag.oCIPAG_RD_2_3.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCIPAG_RI_2_4.RadioValue()==this.w_CIPAG_RI)
      this.oPgFrm.Page2.oPag.oCIPAG_RI_2_4.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCIPAG_BO_2_5.RadioValue()==this.w_CIPAG_BO)
      this.oPgFrm.Page2.oPag.oCIPAG_BO_2_5.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCIPAG_MA_2_6.RadioValue()==this.w_CIPAG_MA)
      this.oPgFrm.Page2.oPag.oCIPAG_MA_2_6.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCIFLSIPA_2_7.RadioValue()==this.w_CIFLSIPA)
      this.oPgFrm.Page2.oPag.oCIFLSIPA_2_7.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCIPAG_CA_2_8.RadioValue()==this.w_CIPAG_CA)
      this.oPgFrm.Page2.oPag.oCIPAG_CA_2_8.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCICODCLF_2_9.value==this.w_CICODCLF)
      this.oPgFrm.Page2.oPag.oCICODCLF_2_9.value=this.w_CICODCLF
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCLF_2_10.value==this.w_DESCLF)
      this.oPgFrm.Page2.oPag.oDESCLF_2_10.value=this.w_DESCLF
    endif
    if not(this.oPgFrm.Page2.oPag.oCICODCAU_2_13.value==this.w_CICODCAU)
      this.oPgFrm.Page2.oPag.oCICODCAU_2_13.value=this.w_CICODCAU
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAU_2_14.value==this.w_DESCAU)
      this.oPgFrm.Page2.oPag.oDESCAU_2_14.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page2.oPag.oCICONEFA_2_15.value==this.w_CICONEFA)
      this.oPgFrm.Page2.oPag.oCICONEFA_2_15.value=this.w_CICONEFA
    endif
    if not(this.oPgFrm.Page2.oPag.oDESEFA_2_16.value==this.w_DESEFA)
      this.oPgFrm.Page2.oPag.oDESEFA_2_16.value=this.w_DESEFA
    endif
    if not(this.oPgFrm.Page2.oPag.oCICONDCA_2_17.value==this.w_CICONDCA)
      this.oPgFrm.Page2.oPag.oCICONDCA_2_17.value=this.w_CICONDCA
    endif
    if not(this.oPgFrm.Page2.oPag.oDESDCA_2_18.value==this.w_DESDCA)
      this.oPgFrm.Page2.oPag.oDESDCA_2_18.value=this.w_DESDCA
    endif
    if not(this.oPgFrm.Page2.oPag.oCICONDCP_2_19.value==this.w_CICONDCP)
      this.oPgFrm.Page2.oPag.oCICONDCP_2_19.value=this.w_CICONDCP
    endif
    if not(this.oPgFrm.Page2.oPag.oDESDCP_2_20.value==this.w_DESDCP)
      this.oPgFrm.Page2.oPag.oDESDCP_2_20.value=this.w_DESDCP
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPORTO_1_60.value==this.w_IMPORTO)
      this.oPgFrm.Page1.oPag.oIMPORTO_1_60.value=this.w_IMPORTO
    endif
    cp_SetControlsValueExtFlds(this,'CON_INDI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CINUMERO))  and (.cFunction<>'Edit')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCINUMERO_1_6.SetFocus()
            i_bnoObbl = !empty(.w_CINUMERO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CICODESE))  and (.cFunction='Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCICODESE_1_7.SetFocus()
            i_bnoObbl = !empty(.w_CICODESE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CIDATREG))  and (.cFunction<>'Edit')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCIDATREG_1_8.SetFocus()
            i_bnoObbl = !empty(.w_CIDATREG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CISCAFIN>=.w_CISCAINI OR EMPTY(.w_CISCAFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCISCAINI_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_CISCAFIN)) or not(.w_CISCAFIN>=.w_CISCAINI OR EMPTY(.w_CISCAINI)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCISCAFIN_1_11.SetFocus()
            i_bnoObbl = !empty(.w_CISCAFIN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CICODVAL))  and (.cFunction='Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCICODVAL_1_18.SetFocus()
            i_bnoObbl = !empty(.w_CICODVAL)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valuta inesistente o incongruente o obsoleta")
          case   (empty(.w_CICAOVAL))  and (.w_CIDATVAL<GETVALUT(G_PERVAL,'VADATEUR') OR .w_TESVAL=0 and .cFunction='Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCICAOVAL_1_24.SetFocus()
            i_bnoObbl = !empty(.w_CICAOVAL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_CICODCAU)) or not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_TIPREG='N'))  and (.cFunction='Load')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCICODCAU_2_13.SetFocus()
            i_bnoObbl = !empty(.w_CICODCAU)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale contabile inesistente, obsoleta o di tipo IVA")
          case   ((empty(.w_CICONEFA)) or not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST AND EMPTY(.w_FLPART)))  and (.cFunction='Load')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCICONEFA_2_15.SetFocus()
            i_bnoObbl = !empty(.w_CICONEFA)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
          case   ((empty(.w_CICONDCA)) or not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST AND EMPTY(.w_FLPART)))  and (.cFunction='Load')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCICONDCA_2_17.SetFocus()
            i_bnoObbl = !empty(.w_CICONDCA)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
          case   ((empty(.w_CICONDCP)) or not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST AND EMPTY(.w_FLPART)))  and (.cFunction='Load')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCICONDCP_2_19.SetFocus()
            i_bnoObbl = !empty(.w_CICONDCP)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CISERIAL = this.w_CISERIAL
    this.o_CICODESE = this.w_CICODESE
    this.o_CIDATREG = this.w_CIDATREG
    this.o_CIDATVAL = this.w_CIDATVAL
    this.o_CICODVAL = this.w_CICODVAL
    this.o_DECTOT = this.w_DECTOT
    return

  func CanEdit()
    local i_res
    i_res=Empty(CHKCONS('P',this.w_CIDATVAL,'B','N'))
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Attenzione, data registrazione di primanota antecedente alla data di consolidamento"))
    endif
    return(i_res)
  func CanDelete()
    local i_res
    i_res=Empty(CHKCONS('P',this.w_CIDATVAL,'B','N'))
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Attenzione, data registrazione di primanota antecedente alla data di consolidamento"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgscg_aciPag1 as StdContainer
  Width  = 735
  height = 418
  stdWidth  = 735
  stdheight = 418
  resizeXpos=338
  resizeYpos=248
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCINUMERO_1_6 as StdField with uid="QMFVWTHZRN",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CINUMERO", cQueryName = "CINUMERO",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero progressivo registrazione",;
    HelpContextID = 60819573,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=90, Top=9, cSayPict='"999999"', cGetPict='"999999"'

  func oCINUMERO_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
   endif
  endfunc

  add object oCICODESE_1_7 as StdField with uid="DOLLSQMFIQ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CICODESE", cQueryName = "CICODESE",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio di competenza",;
    HelpContextID = 50944107,;
   bGlobalFont=.t.,;
    Height=21, Width=46, Left=169, Top=9, InputMask=replicate('X',4), bHasZoom = .t. , tabstop=.f., cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_CICODESE"

  func oCICODESE_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oCICODESE_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oCICODESE_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCICODESE_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oCICODESE_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco esercizi",'',this.parent.oContained
  endproc

  add object oCIDATREG_1_8 as StdField with uid="FNUHTQLIIV",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CIDATREG", cQueryName = "CIDATREG",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data della registrazione",;
    HelpContextID = 251959187,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=252, Top=9

  func oCIDATREG_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
   endif
  endfunc

  add object oCISCAINI_1_9 as StdField with uid="OSOCQCOEXM",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CISCAINI", cQueryName = "CISCAINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio selezione scadenze",;
    HelpContextID = 114186351,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=451, Top=9

  func oCISCAINI_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CISCAFIN>=.w_CISCAINI OR EMPTY(.w_CISCAFIN))
    endwith
    return bRes
  endfunc

  add object oCISCAFIN_1_11 as StdField with uid="KSVUBQADVU",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CISCAFIN", cQueryName = "CISCAFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di fine selezione scadenze",;
    HelpContextID = 204580748,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=597, Top=9

  func oCISCAFIN_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CISCAFIN>=.w_CISCAINI OR EMPTY(.w_CISCAINI))
    endwith
    return bRes
  endfunc

  add object oCIFLDEFI_1_12 as StdCheck with uid="TAEOHNOJFZ",rtseq=12,rtrep=.f.,left=568, top=61, caption="Definitiva", enabled=.f.,;
    ToolTipText = "Se attivo: gi� eseguita la contabilizzazione",;
    HelpContextID = 217675665,;
    cFormVar="w_CIFLDEFI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCIFLDEFI_1_12.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCIFLDEFI_1_12.GetRadio()
    this.Parent.oContained.w_CIFLDEFI = this.RadioValue()
    return .t.
  endfunc

  func oCIFLDEFI_1_12.SetRadio()
    this.Parent.oContained.w_CIFLDEFI=trim(this.Parent.oContained.w_CIFLDEFI)
    this.value = ;
      iif(this.Parent.oContained.w_CIFLDEFI=='S',1,;
      0)
  endfunc

  add object oCIDESCRI_1_14 as StdField with uid="NFKAUKIQHU",rtseq=14,rtrep=.f.,;
    cFormVar = "w_CIDESCRI", cQueryName = "CIDESCRI",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale descrizione aggiuntiva",;
    HelpContextID = 32467055,;
   bGlobalFont=.t.,;
    Height=21, Width=317, Left=90, Top=36, InputMask=replicate('X',35)

  add object oCICODVAL_1_18 as StdField with uid="JVPWSJTECZ",rtseq=18,rtrep=.f.,;
    cFormVar = "w_CICODVAL", cQueryName = "CICODVAL",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Valuta inesistente o incongruente o obsoleta",;
    ToolTipText = "Codice valuta delle scadenze selezionate",;
    HelpContextID = 67721330,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=479, Top=36, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_CICODVAL"

  func oCICODVAL_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oCICODVAL_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oCICODVAL_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCICODVAL_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oCICODVAL_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Elenco valute",'',this.parent.oContained
  endproc
  proc oCICODVAL_1_18.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_CICODVAL
     i_obj.ecpSave()
  endproc

  add object oCICAOVAL_1_24 as StdField with uid="YBUXCJGLGK",rtseq=24,rtrep=.f.,;
    cFormVar = "w_CICAOVAL", cQueryName = "CICAOVAL",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Cambio contabilizzazione valuta partite",;
    HelpContextID = 78338162,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=597, Top=36, cSayPict='"99999.999999"', cGetPict='"99999.999999"'

  func oCICAOVAL_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CIDATVAL<GETVALUT(G_PERVAL,'VADATEUR') OR .w_TESVAL=0 and .cFunction='Load')
    endwith
   endif
  endfunc


  add object oBtn_1_32 as StdButton with uid="SSGWGHEQKF",left=682, top=371, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per memorizzare i dati inseriti e ricercare le scadenze da contabilizzare";
    , HelpContextID = 67205398;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_32.Click()
      with this.Parent.oContained
        GSCG_BI3(this.Parent.oContained,.w_CISERIAL,this.name)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_32.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ((.w_CIFLDEFI<>'S' AND NOT EMPTY(.w_CISERIAL)) OR .cFunction='Load')
      endwith
    endif
  endfunc


  add object oBtn_1_34 as StdButton with uid="OBJKGIRSSV",left=685, top=11, width=48,height=45,;
    CpPicture="bmp\TESO.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere alla reg.contabile associata";
    , HelpContextID = 234481786;
    , tabstop=.f.,Caption='\<Primanota';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_34.Click()
      with this.Parent.oContained
        GSAR_BZP(this.Parent.oContained,.w_PTRIFIND)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_34.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_CIRIFCON))
      endwith
    endif
  endfunc

  func oBtn_1_34.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_CIRIFCON))
     endwith
    endif
  endfunc


  add object oObj_1_36 as cp_runprogram with uid="RRQDKDRONJ",left=572, top=434, width=168,height=20,;
    caption='GSCG_BI4(0)',;
   bGlobalFont=.t.,;
    prg="GSCG_BI4(0)",;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 239852262


  add object CalcZoom as cp_szoombox with uid="GEWSSFXCZX",left=2, top=77, width=731,height=254,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="PAR_TITE",cZoomFile="GSCG_KCI",bOptions=.f.,bQueryOnLoad=.f.,bAdvOptions=.t.,bNoZoomGridShape=.f.,cZoomOnZoom="",bQueryOnDblClick=.t.,bReadOnly=.t.,cMenuFile="",;
    cEvent = "Legge",;
    nPag=1;
    , HelpContextID = 68280806

  add object oSELEZI_1_38 as StdRadio with uid="IHAJBZKUNR",rtseq=26,rtrep=.f.,left=14, top=370, width=165,height=32;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_38.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 140502310
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 140502310
      this.Buttons(2).Top=15
      this.SetAll("Width",163)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_1_38.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_38.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_38.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc

  func oSELEZI_1_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (VAL(.w_CISERIAL)<>0 AND .w_CIFLDEFI<>'S')
    endwith
   endif
  endfunc


  add object oBtn_1_39 as StdButton with uid="ASBPFHFRCA",left=633, top=370, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per contabilizzare le scadenze abbinate";
    , HelpContextID = 109688090;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_39.Click()
      with this.Parent.oContained
        do GSCG_BCI with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_39.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CIFLDEFI<>'S'  AND RecCount( .w_CalcZoom.cCursor )>0 And .cFunction='Edit')
      endwith
    endif
  endfunc


  add object oBtn_1_46 as StdButton with uid="XXSGHTOZIT",left=534, top=370, width=48,height=45,;
    CpPicture="bmp\elimina.bmp", caption="", nPag=1;
    , ToolTipText = "Elimina la reg. contabile generata dalla scadenza selezionata";
    , HelpContextID = 242867014;
    , tabstop=.f.,Caption='\<Singola';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_46.Click()
      with this.Parent.oContained
        GSCG_BZP(this.Parent.oContained,"ELI",.w_PTRIFIND)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_46.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction='Edit' AND NVL(.w_CIFLDEFI,' ')='S')
      endwith
    endif
  endfunc

  func oBtn_1_46.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.cFunction='Load' Or .w_CIFLSIPA<>'S')
     endwith
    endif
  endfunc


  add object oBtn_1_48 as StdButton with uid="KXPGHDZHZN",left=534, top=370, width=48,height=45,;
    CpPicture="bmp\elimina.bmp", caption="", nPag=1;
    , ToolTipText = "Elimina la reg. contabile generata dalla contabilizzazione indiretta";
    , HelpContextID = 99627103;
    , tabstop=.f.,Caption='\<di Riepilogo';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_48.Click()
      with this.Parent.oContained
        GSCG_BZP(this.Parent.oContained,"ELI",.w_PTRIFIND)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_48.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction='Edit' AND NVL(.w_CIFLDEFI,' ')='S')
      endwith
    endif
  endfunc

  func oBtn_1_48.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.cFunction='Load' Or .w_CIFLSIPA='S')
     endwith
    endif
  endfunc


  add object oBtn_1_49 as StdButton with uid="APHEQEVWXD",left=584, top=370, width=48,height=45,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare ulteriori dati";
    , HelpContextID = 117541775;
    , Caption='\<Dettaglio';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_49.Click()
      with this.Parent.oContained
        gscg_bi5(this.Parent.oContained,"PARTITE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_49.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_PTSERIAL) AND .w_CPROWNUM<>0 AND .w_PTROWORD=-3 And .cFunction='Edit')
      endwith
    endif
  endfunc

  func oBtn_1_49.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.cFunction='Load')
     endwith
    endif
  endfunc


  add object oObj_1_50 as cp_runprogram with uid="CBSSEIIXTI",left=194, top=434, width=265,height=20,;
    caption='GSCG_BI5(VALUTA)',;
   bGlobalFont=.t.,;
    prg="GSCG_BI5('VALUTA')",;
    cEvent = "w_CICODVAL Changed",;
    nPag=1;
    , HelpContextID = 189909828


  add object oObj_1_53 as cp_runprogram with uid="FHKPIFVPII",left=601, top=476, width=139,height=20,;
    caption='GSCG_BI4(1)',;
   bGlobalFont=.t.,;
    prg="GSCG_BI4(1)",;
    cEvent = "Done",;
    nPag=1;
    , ToolTipText = "Rimuove il temporaneo lato server utilizzato per lo zoom in carica";
    , HelpContextID = 239852006


  add object oObj_1_55 as cp_runprogram with uid="DSTZTCZBCH",left=547, top=456, width=193,height=19,;
    caption='GSCG_BI2',;
   bGlobalFont=.t.,;
    prg="GSCG_BI2",;
    cEvent = "Load,Edit Aborted",;
    nPag=1;
    , ToolTipText = "Aggiorna lo zoom (evento legge) al caricamento di una registrazione o all'abort di una modifica";
    , HelpContextID = 240033128


  add object oObj_1_56 as cp_runprogram with uid="TMNGNAWZGN",left=-7, top=434, width=199,height=19,;
    caption='GSCG_BI1',;
   bGlobalFont=.t.,;
    prg="GSCG_BI1(1)",;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 240033129

  add object oIMPORTO_1_60 as StdField with uid="YPRBLYBBJV",rtseq=57,rtrep=.f.,;
    cFormVar = "w_IMPORTO", cQueryName = "IMPORTO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 48901254,;
   bGlobalFont=.t.,;
    Height=21, Width=129, Left=535, Top=340, cSayPict="v_PV(38+VVP)", cGetPict="v_GV(38+VVP)"


  add object oObj_1_64 as cp_runprogram with uid="WRSCMUVHGB",left=-7, top=454, width=199,height=19,;
    caption='GSCG_BI1',;
   bGlobalFont=.t.,;
    prg="GSCG_BI1(2)",;
    cEvent = "calczoom row checked,calczoom menucheck,calczoom row unchecked",;
    nPag=1;
    , HelpContextID = 240033129


  add object oObj_1_65 as cp_runprogram with uid="RPZCQUNPDX",left=-7, top=514, width=199,height=19,;
    caption='GSCG_BI1',;
   bGlobalFont=.t.,;
    prg="GSCG_BI1(3)",;
    cEvent = "Legge",;
    nPag=1;
    , HelpContextID = 240033129


  add object oObj_1_67 as cp_runprogram with uid="KELJPXIZRY",left=-7, top=474, width=199,height=19,;
    caption='GSCG_BI1',;
   bGlobalFont=.t.,;
    prg="GSCG_BI1(5)",;
    cEvent = "calczoom row checked",;
    nPag=1;
    , ToolTipText = " calczoom rowcheckall, calczoom rowcheckto, calczoom rowcheckfrom, calczoom rowinvertselection, calczoom rowuncheckall";
    , HelpContextID = 240033129


  add object oObj_1_68 as cp_runprogram with uid="KTDJIAOVYU",left=-7, top=494, width=199,height=19,;
    caption='GSCG_BI1',;
   bGlobalFont=.t.,;
    prg="GSCG_BI1(4)",;
    cEvent = "calczoom menucheck",;
    nPag=1;
    , ToolTipText = " calczoom rowcheckall, calczoom rowcheckto, calczoom rowcheckfrom, calczoom rowinvertselection, calczoom rowuncheckall";
    , HelpContextID = 240033129

  add object oStr_1_25 as StdString with uid="XCAEFGCYVD",Visible=.t., Left=223, Top=9,;
    Alignment=1, Width=27, Height=15,;
    Caption="Del:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_26 as StdString with uid="YZRZAOOHCN",Visible=.t., Left=355, Top=11,;
    Alignment=1, Width=94, Height=15,;
    Caption="Scadenze da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="SITHNVZEPS",Visible=.t., Left=577, Top=11,;
    Alignment=1, Width=18, Height=15,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="GRYCQIKXCT",Visible=.t., Left=530, Top=36,;
    Alignment=1, Width=65, Height=15,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="QYKCIHIUFR",Visible=.t., Left=10, Top=9,;
    Alignment=1, Width=79, Height=15,;
    Caption="Numero:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_30 as StdString with uid="CKTWSMLJJH",Visible=.t., Left=7, Top=36,;
    Alignment=1, Width=82, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="UXJPVHHUYU",Visible=.t., Left=159, Top=9,;
    Alignment=0, Width=9, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="JOEZVDOMUO",Visible=.t., Left=422, Top=36,;
    Alignment=1, Width=55, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_58 as StdString with uid="JBTVOBMIXW",Visible=.t., Left=359, Top=385,;
    Alignment=1, Width=173, Height=17,;
    Caption="Elimina registrazione contabile:"  ;
  , bGlobalFont=.t.

  func oStr_1_58.mHide()
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
  endfunc

  add object oStr_1_61 as StdString with uid="HZNAGVALMR",Visible=.t., Left=373, Top=343,;
    Alignment=1, Width=159, Height=18,;
    Caption="Totale scadenze selezionate:"  ;
  , bGlobalFont=.t.
enddefine
define class tgscg_aciPag2 as StdContainer
  Width  = 735
  height = 418
  stdWidth  = 735
  stdheight = 418
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCIPAG_RB_2_2 as StdCheck with uid="DGEBNWIHKA",rtseq=31,rtrep=.f.,left=209, top=31, caption="Ric.bancaria",;
    ToolTipText = "Se attivo: seleziona le ricevute bancarie",;
    HelpContextID = 220997736,;
    cFormVar="w_CIPAG_RB", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCIPAG_RB_2_2.RadioValue()
    return(iif(this.value =1,'RB',;
    '  '))
  endfunc
  func oCIPAG_RB_2_2.GetRadio()
    this.Parent.oContained.w_CIPAG_RB = this.RadioValue()
    return .t.
  endfunc

  func oCIPAG_RB_2_2.SetRadio()
    this.Parent.oContained.w_CIPAG_RB=trim(this.Parent.oContained.w_CIPAG_RB)
    this.value = ;
      iif(this.Parent.oContained.w_CIPAG_RB=='RB',1,;
      0)
  endfunc

  add object oCIPAG_RD_2_3 as StdCheck with uid="EFSDLIJCAF",rtseq=32,rtrep=.f.,left=209, top=49, caption="Rim.dir.",;
    ToolTipText = "Se attivo: seleziona le rimesse dirette",;
    HelpContextID = 220997738,;
    cFormVar="w_CIPAG_RD", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCIPAG_RD_2_3.RadioValue()
    return(iif(this.value =1,'RD',;
    '  '))
  endfunc
  func oCIPAG_RD_2_3.GetRadio()
    this.Parent.oContained.w_CIPAG_RD = this.RadioValue()
    return .t.
  endfunc

  func oCIPAG_RD_2_3.SetRadio()
    this.Parent.oContained.w_CIPAG_RD=trim(this.Parent.oContained.w_CIPAG_RD)
    this.value = ;
      iif(this.Parent.oContained.w_CIPAG_RD=='RD',1,;
      0)
  endfunc

  add object oCIPAG_RI_2_4 as StdCheck with uid="ZFOQGCDOIH",rtseq=33,rtrep=.f.,left=209, top=67, caption="R.I.D.",;
    ToolTipText = "Se attivo: seleziona le R.I.D.",;
    HelpContextID = 220997743,;
    cFormVar="w_CIPAG_RI", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCIPAG_RI_2_4.RadioValue()
    return(iif(this.value =1,'RI',;
    '  '))
  endfunc
  func oCIPAG_RI_2_4.GetRadio()
    this.Parent.oContained.w_CIPAG_RI = this.RadioValue()
    return .t.
  endfunc

  func oCIPAG_RI_2_4.SetRadio()
    this.Parent.oContained.w_CIPAG_RI=trim(this.Parent.oContained.w_CIPAG_RI)
    this.value = ;
      iif(this.Parent.oContained.w_CIPAG_RI=='RI',1,;
      0)
  endfunc

  add object oCIPAG_BO_2_5 as StdCheck with uid="BMTXUKIZOQ",rtseq=34,rtrep=.f.,left=331, top=31, caption="Bonifico",;
    ToolTipText = "Se attivo: seleziona i bonifici",;
    HelpContextID = 220997749,;
    cFormVar="w_CIPAG_BO", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCIPAG_BO_2_5.RadioValue()
    return(iif(this.value =1,'BO',;
    '  '))
  endfunc
  func oCIPAG_BO_2_5.GetRadio()
    this.Parent.oContained.w_CIPAG_BO = this.RadioValue()
    return .t.
  endfunc

  func oCIPAG_BO_2_5.SetRadio()
    this.Parent.oContained.w_CIPAG_BO=trim(this.Parent.oContained.w_CIPAG_BO)
    this.value = ;
      iif(this.Parent.oContained.w_CIPAG_BO=='BO',1,;
      0)
  endfunc

  add object oCIPAG_MA_2_6 as StdCheck with uid="YEGFAHNRAB",rtseq=35,rtrep=.f.,left=331, top=49, caption="M.AV.",;
    ToolTipText = "Se attivo: seleziona le M.AV.",;
    HelpContextID = 220997735,;
    cFormVar="w_CIPAG_MA", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCIPAG_MA_2_6.RadioValue()
    return(iif(this.value =1,'MA',;
    '  '))
  endfunc
  func oCIPAG_MA_2_6.GetRadio()
    this.Parent.oContained.w_CIPAG_MA = this.RadioValue()
    return .t.
  endfunc

  func oCIPAG_MA_2_6.SetRadio()
    this.Parent.oContained.w_CIPAG_MA=trim(this.Parent.oContained.w_CIPAG_MA)
    this.value = ;
      iif(this.Parent.oContained.w_CIPAG_MA=='MA',1,;
      0)
  endfunc


  add object oCIFLSIPA_2_7 as StdCombo with uid="SCDZTJLHSR",rtseq=36,rtrep=.f.,left=209,top=97,width=135,height=22;
    , ToolTipText = "Tipo di contabilizzazione";
    , HelpContextID = 133597287;
    , cFormVar="w_CIFLSIPA",RowSource=""+"Riepilogativa,"+"Singola partita", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oCIFLSIPA_2_7.RadioValue()
    return(iif(this.value =1,'R',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oCIFLSIPA_2_7.GetRadio()
    this.Parent.oContained.w_CIFLSIPA = this.RadioValue()
    return .t.
  endfunc

  func oCIFLSIPA_2_7.SetRadio()
    this.Parent.oContained.w_CIFLSIPA=trim(this.Parent.oContained.w_CIFLSIPA)
    this.value = ;
      iif(this.Parent.oContained.w_CIFLSIPA=='R',1,;
      iif(this.Parent.oContained.w_CIFLSIPA=='S',2,;
      0))
  endfunc

  func oCIFLSIPA_2_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  add object oCIPAG_CA_2_8 as StdCheck with uid="HFOMTQZUQS",rtseq=37,rtrep=.f.,left=331, top=67, caption="Cambiali",;
    ToolTipText = "Se attivo: seleziona le cambiali/tratte",;
    HelpContextID = 47437721,;
    cFormVar="w_CIPAG_CA", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCIPAG_CA_2_8.RadioValue()
    return(iif(this.value =1,'CA',;
    '  '))
  endfunc
  func oCIPAG_CA_2_8.GetRadio()
    this.Parent.oContained.w_CIPAG_CA = this.RadioValue()
    return .t.
  endfunc

  func oCIPAG_CA_2_8.SetRadio()
    this.Parent.oContained.w_CIPAG_CA=trim(this.Parent.oContained.w_CIPAG_CA)
    this.value = ;
      iif(this.Parent.oContained.w_CIPAG_CA=='CA',1,;
      0)
  endfunc

  add object oCICODCLF_2_9 as StdField with uid="CBTFOOCMFA",rtseq=38,rtrep=.f.,;
    cFormVar = "w_CICODCLF", cQueryName = "CICODCLF",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice cliente di selezione (vuoto=no selezione)",;
    HelpContextID = 17389676,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=208, Top=131, cSayPict="p_CLI", cGetPict="p_CLI", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_CITIPCLF", oKey_2_1="ANCODICE", oKey_2_2="this.w_CICODCLF"

  func oCICODCLF_2_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oCICODCLF_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCICODCLF_2_9.ecpDrop(oSource)
    this.Parent.oContained.link_2_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCICODCLF_2_9.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_CITIPCLF)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_CITIPCLF)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCICODCLF_2_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco clienti",'',this.parent.oContained
  endproc
  proc oCICODCLF_2_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_CITIPCLF
     i_obj.w_ANCODICE=this.parent.oContained.w_CICODCLF
     i_obj.ecpSave()
  endproc

  add object oDESCLF_2_10 as StdField with uid="RRKZUVHQXK",rtseq=39,rtrep=.f.,;
    cFormVar = "w_DESCLF", cQueryName = "DESCLF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 75387958,;
   bGlobalFont=.t.,;
    Height=21, Width=303, Left=351, Top=131, InputMask=replicate('X',40)

  add object oCICODCAU_2_13 as StdField with uid="DKWINEPTVR",rtseq=40,rtrep=.f.,;
    cFormVar = "w_CICODCAU", cQueryName = "CICODCAU",;
    bObbl = .t. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale contabile inesistente, obsoleta o di tipo IVA",;
    ToolTipText = "Causale contabile utilizzata per generare i movimenti di chiusura dei clienti",;
    HelpContextID = 17389691,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=208, Top=159, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_CICODCAU"

  func oCICODCAU_2_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oCICODCAU_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oCICODCAU_2_13.ecpDrop(oSource)
    this.Parent.oContained.link_2_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCICODCAU_2_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCICODCAU_2_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'GSTE_ACD.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oCICODCAU_2_13.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_CICODCAU
     i_obj.ecpSave()
  endproc

  add object oDESCAU_2_14 as StdField with uid="ESIYGFWGOI",rtseq=41,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 47076406,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=274, Top=159, InputMask=replicate('X',35)

  add object oCICONEFA_2_15 as StdField with uid="PUDYEBHLMX",rtseq=42,rtrep=.f.,;
    cFormVar = "w_CICONEFA", cQueryName = "CICONEFA",;
    bObbl = .t. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto o gestito a partite",;
    ToolTipText = "Conto effetti attivi",;
    HelpContextID = 207005593,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=208, Top=187, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CICONEFA"

  func oCICONEFA_2_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oCICONEFA_2_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oCICONEFA_2_15.ecpDrop(oSource)
    this.Parent.oContained.link_2_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCICONEFA_2_15.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCICONEFA_2_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti effetti attivi",'CONCOEFA.CONTI_VZM',this.parent.oContained
  endproc
  proc oCICONEFA_2_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CICONEFA
     i_obj.ecpSave()
  endproc

  add object oDESEFA_2_16 as StdField with uid="TPFXRDBCKW",rtseq=43,rtrep=.f.,;
    cFormVar = "w_DESEFA", cQueryName = "DESEFA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 253776950,;
   bGlobalFont=.t.,;
    Height=21, Width=303, Left=351, Top=187, InputMask=replicate('X',40)

  add object oCICONDCA_2_17 as StdField with uid="RGULVEEOSR",rtseq=44,rtrep=.f.,;
    cFormVar = "w_CICONDCA", cQueryName = "CICONDCA",;
    bObbl = .t. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto o gestito a partite",;
    ToolTipText = "Conto per eventuale registrazione delle differenze cambi attive",;
    HelpContextID = 44652647,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=208, Top=215, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CICONDCA"

  func oCICONDCA_2_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oCICONDCA_2_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oCICONDCA_2_17.ecpDrop(oSource)
    this.Parent.oContained.link_2_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCICONDCA_2_17.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCICONDCA_2_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti differenze cambi",'CONCOEFA.CONTI_VZM',this.parent.oContained
  endproc
  proc oCICONDCA_2_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CICONDCA
     i_obj.ecpSave()
  endproc

  add object oDESDCA_2_18 as StdField with uid="UYNLAHQKMF",rtseq=45,rtrep=.f.,;
    cFormVar = "w_DESDCA", cQueryName = "DESDCA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 250565686,;
   bGlobalFont=.t.,;
    Height=21, Width=303, Left=351, Top=215, InputMask=replicate('X',40)

  add object oCICONDCP_2_19 as StdField with uid="JTSZHCLYOG",rtseq=46,rtrep=.f.,;
    cFormVar = "w_CICONDCP", cQueryName = "CICONDCP",;
    bObbl = .t. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto o gestito a partite",;
    ToolTipText = "Conto per eventuale registrazione delle differenze cambi passive",;
    HelpContextID = 44652662,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=208, Top=243, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CICONDCP"

  func oCICONDCP_2_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oCICONDCP_2_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oCICONDCP_2_19.ecpDrop(oSource)
    this.Parent.oContained.link_2_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCICONDCP_2_19.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCICONDCP_2_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti differenze cambi",'CONCOEFA.CONTI_VZM',this.parent.oContained
  endproc
  proc oCICONDCP_2_19.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CICONDCP
     i_obj.ecpSave()
  endproc

  add object oDESDCP_2_20 as StdField with uid="HWBYEKUOYK",rtseq=47,rtrep=.f.,;
    cFormVar = "w_DESDCP", cQueryName = "DESDCP",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 233788470,;
   bGlobalFont=.t.,;
    Height=21, Width=303, Left=351, Top=243, InputMask=replicate('X',40)

  add object oStr_2_11 as StdString with uid="DYIUFCUKAU",Visible=.t., Left=14, Top=131,;
    Alignment=1, Width=191, Height=15,;
    Caption="Cod. cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_12 as StdString with uid="ETEBPYHRGZ",Visible=.t., Left=72, Top=31,;
    Alignment=1, Width=131, Height=15,;
    Caption="Pagamenti:"  ;
  , bGlobalFont=.t.

  add object oStr_2_21 as StdString with uid="RBTGFXOZYY",Visible=.t., Left=14, Top=159,;
    Alignment=1, Width=191, Height=15,;
    Caption="Causale contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_2_22 as StdString with uid="QDDWNKLNOK",Visible=.t., Left=14, Top=215,;
    Alignment=1, Width=191, Height=15,;
    Caption="Conto D.C.attive:"  ;
  , bGlobalFont=.t.

  add object oStr_2_23 as StdString with uid="ARAMVAKNYO",Visible=.t., Left=14, Top=243,;
    Alignment=1, Width=191, Height=15,;
    Caption="Conto D.C.passive:"  ;
  , bGlobalFont=.t.

  add object oStr_2_24 as StdString with uid="LAUVXCZUAU",Visible=.t., Left=14, Top=96,;
    Alignment=1, Width=191, Height=18,;
    Caption="Tipo di contabilizzazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_25 as StdString with uid="WMXLHPUAXD",Visible=.t., Left=14, Top=188,;
    Alignment=1, Width=191, Height=18,;
    Caption="Conto effetti attivi/conto banche:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_aci','CON_INDI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CISERIAL=CON_INDI.CISERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
