* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_kuc                                                        *
*              Ultimi costi da fornitori                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_154]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-10-24                                                      *
* Last revis.: 2013-03-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_kuc",oParentObject))

* --- Class definition
define class tgsar_kuc as StdForm
  Top    = 135
  Left   = 14

  * --- Standard Properties
  Width  = 569
  Height = 322
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-03-15"
  HelpContextID=65632105
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=28

  * --- Constant Properties
  _IDX = 0
  DOC_MAST_IDX = 0
  MVM_MAST_IDX = 0
  KEY_ARTI_IDX = 0
  cPrg = "gsar_kuc"
  cComment = "Ultimi costi da fornitori"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_MVSERIAL = space(10)
  o_MVSERIAL = space(10)
  w_CPROWNUM = 0
  w_NUMRIF = 0
  w_MVCODICE = space(20)
  w_NUMDOCD = 0
  w_SERDOCD = space(10)
  w_CLADOC = space(2)
  w_PARAME = space(3)
  w_FLVEAC = space(1)
  w_MMSERIAL = space(10)
  o_MMSERIAL = space(10)
  w_NUMDOCM = 0
  w_SERDOCM = space(10)
  w_MVCODART = space(20)
  w_MVDATDOC = ctod('  /  /  ')
  w_CODART = space(20)
  w_DATDOC = ctod('  /  /  ')
  o_DATDOC = ctod('  /  /  ')
  w_DATDOC1 = ctod('  /  /  ')
  w_MVCODVAL = space(3)
  w_MVCAOVAL = 0
  w_DECUNI = 0
  w_NUMRIF1 = 0
  w_TIPDOC = space(5)
  w_CAMAG = space(5)
  w_VALDOC = space(3)
  w_VALDOCM = space(3)
  w_DATDOC2 = ctod('  /  /  ')
  w_TIPCON = space(1)
  w_MVCODCON = space(15)
  w_ZoomCOS = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_kucPag1","gsar_kuc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomCOS = this.oPgFrm.Pages(1).oPag.ZoomCOS
    DoDefault()
    proc Destroy()
      this.w_ZoomCOS = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='DOC_MAST'
    this.cWorkTables[2]='MVM_MAST'
    this.cWorkTables[3]='KEY_ARTI'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_MVSERIAL=space(10)
      .w_CPROWNUM=0
      .w_NUMRIF=0
      .w_MVCODICE=space(20)
      .w_NUMDOCD=0
      .w_SERDOCD=space(10)
      .w_CLADOC=space(2)
      .w_PARAME=space(3)
      .w_FLVEAC=space(1)
      .w_MMSERIAL=space(10)
      .w_NUMDOCM=0
      .w_SERDOCM=space(10)
      .w_MVCODART=space(20)
      .w_MVDATDOC=ctod("  /  /  ")
      .w_CODART=space(20)
      .w_DATDOC=ctod("  /  /  ")
      .w_DATDOC1=ctod("  /  /  ")
      .w_MVCODVAL=space(3)
      .w_MVCAOVAL=0
      .w_DECUNI=0
      .w_NUMRIF1=0
      .w_TIPDOC=space(5)
      .w_CAMAG=space(5)
      .w_VALDOC=space(3)
      .w_VALDOCM=space(3)
      .w_DATDOC2=ctod("  /  /  ")
      .w_TIPCON=space(1)
      .w_MVCODCON=space(15)
      .w_MVCODICE=oParentObject.w_MVCODICE
      .w_MVCODART=oParentObject.w_MVCODART
      .w_MVDATDOC=oParentObject.w_MVDATDOC
      .w_MVCODVAL=oParentObject.w_MVCODVAL
      .w_MVCAOVAL=oParentObject.w_MVCAOVAL
      .w_DECUNI=oParentObject.w_DECUNI
      .w_MVCODCON=oParentObject.w_MVCODCON
        .w_MVSERIAL = .w_ZoomCOS.getVar("MVSERIAL")
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_MVSERIAL))
          .link_1_4('Full')
        endif
        .w_CPROWNUM = .w_ZoomCOS.getVar("CPROWNUM")
        .w_NUMRIF = .w_ZoomCOS.getVar("MVNUMRIF")
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_MVCODICE))
          .link_1_7('Full')
        endif
          .DoRTCalc(5,7,.f.)
        .w_PARAME = .w_FLVEAC + .w_CLADOC
        .w_FLVEAC = 'A'
        .w_MMSERIAL = .w_ZoomCOS.getVar("MVSERIAL")
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_MMSERIAL))
          .link_1_15('Full')
        endif
          .DoRTCalc(11,14,.f.)
        .w_CODART = .w_MVCODART
        .w_DATDOC = .w_MVDATDOC
        .w_DATDOC1 = iif(g_DTINDC<>i_INIDAT,g_DTINDC,.w_DATDOC-365)
      .oPgFrm.Page1.oPag.ZoomCOS.Calculate()
          .DoRTCalc(18,20,.f.)
        .w_NUMRIF1 = .w_ZoomCOS.getVar("MVNUMRIF")
      .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
          .DoRTCalc(22,25,.f.)
        .w_DATDOC2 = iif(g_DTINMM<>i_INIDAT,g_DTINMM,.w_DATDOC-365)
        .w_TIPCON = 'F'
        .w_MVCODCON = ' '
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_38.enabled = this.oPgFrm.Page1.oPag.oBtn_1_38.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_MVCODICE=.w_MVCODICE
      .oParentObject.w_MVCODART=.w_MVCODART
      .oParentObject.w_MVDATDOC=.w_MVDATDOC
      .oParentObject.w_MVCODVAL=.w_MVCODVAL
      .oParentObject.w_MVCAOVAL=.w_MVCAOVAL
      .oParentObject.w_DECUNI=.w_DECUNI
      .oParentObject.w_MVCODCON=.w_MVCODCON
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_MVSERIAL = .w_ZoomCOS.getVar("MVSERIAL")
          .link_1_4('Full')
            .w_CPROWNUM = .w_ZoomCOS.getVar("CPROWNUM")
            .w_NUMRIF = .w_ZoomCOS.getVar("MVNUMRIF")
          .link_1_7('Full')
        .DoRTCalc(5,7,.t.)
            .w_PARAME = .w_FLVEAC + .w_CLADOC
        .DoRTCalc(9,9,.t.)
            .w_MMSERIAL = .w_ZoomCOS.getVar("MVSERIAL")
          .link_1_15('Full')
        .DoRTCalc(11,16,.t.)
        if .o_DATDOC<>.w_DATDOC
            .w_DATDOC1 = iif(g_DTINDC<>i_INIDAT,g_DTINDC,.w_DATDOC-365)
        endif
        .oPgFrm.Page1.oPag.ZoomCOS.Calculate()
        .DoRTCalc(18,20,.t.)
            .w_NUMRIF1 = .w_ZoomCOS.getVar("MVNUMRIF")
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .DoRTCalc(22,25,.t.)
        if .o_DATDOC<>.w_DATDOC
            .w_DATDOC2 = iif(g_DTINMM<>i_INIDAT,g_DTINMM,.w_DATDOC-365)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(27,28,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomCOS.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oNUMDOCD_1_8.visible=!this.oPgFrm.Page1.oPag.oNUMDOCD_1_8.mHide()
    this.oPgFrm.Page1.oPag.oSERDOCD_1_9.visible=!this.oPgFrm.Page1.oPag.oSERDOCD_1_9.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_12.visible=!this.oPgFrm.Page1.oPag.oBtn_1_12.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_14.visible=!this.oPgFrm.Page1.oPag.oBtn_1_14.mHide()
    this.oPgFrm.Page1.oPag.oNUMDOCM_1_16.visible=!this.oPgFrm.Page1.oPag.oNUMDOCM_1_16.mHide()
    this.oPgFrm.Page1.oPag.oSERDOCM_1_17.visible=!this.oPgFrm.Page1.oPag.oSERDOCM_1_17.mHide()
    this.oPgFrm.Page1.oPag.oTIPDOC_1_28.visible=!this.oPgFrm.Page1.oPag.oTIPDOC_1_28.mHide()
    this.oPgFrm.Page1.oPag.oCAMAG_1_29.visible=!this.oPgFrm.Page1.oPag.oCAMAG_1_29.mHide()
    this.oPgFrm.Page1.oPag.oVALDOC_1_32.visible=!this.oPgFrm.Page1.oPag.oVALDOC_1_32.mHide()
    this.oPgFrm.Page1.oPag.oVALDOCM_1_33.visible=!this.oPgFrm.Page1.oPag.oVALDOCM_1_33.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomCOS.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_34.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MVSERIAL
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DOC_MAST_IDX,3]
    i_lTable = "DOC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2], .t., this.DOC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVSERIAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVSERIAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MVSERIAL,MVNUMDOC,MVALFDOC,MVCLADOC,MVFLVEAC,MVTIPDOC,MVCODVAL";
                   +" from "+i_cTable+" "+i_lTable+" where MVSERIAL="+cp_ToStrODBC(this.w_MVSERIAL);
                   +" and MVSERIAL="+cp_ToStrODBC(this.w_MVSERIAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MVSERIAL',this.w_MVSERIAL;
                       ,'MVSERIAL',this.w_MVSERIAL)
            select MVSERIAL,MVNUMDOC,MVALFDOC,MVCLADOC,MVFLVEAC,MVTIPDOC,MVCODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVSERIAL = NVL(_Link_.MVSERIAL,space(10))
      this.w_NUMDOCD = NVL(_Link_.MVNUMDOC,0)
      this.w_SERDOCD = NVL(_Link_.MVALFDOC,space(10))
      this.w_CLADOC = NVL(_Link_.MVCLADOC,space(2))
      this.w_FLVEAC = NVL(_Link_.MVFLVEAC,space(1))
      this.w_TIPDOC = NVL(_Link_.MVTIPDOC,space(5))
      this.w_VALDOC = NVL(_Link_.MVCODVAL,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_MVSERIAL = space(10)
      endif
      this.w_NUMDOCD = 0
      this.w_SERDOCD = space(10)
      this.w_CLADOC = space(2)
      this.w_FLVEAC = space(1)
      this.w_TIPDOC = space(5)
      this.w_VALDOC = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.MVSERIAL,1)+'\'+cp_ToStr(_Link_.MVSERIAL,1)
      cp_ShowWarn(i_cKey,this.DOC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVSERIAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCODICE
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_MVCODICE);
                   +" and CACODICE="+cp_ToStrODBC(this.w_MVCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_MVCODICE;
                       ,'CACODICE',this.w_MVCODICE)
            select CACODICE,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCODICE = NVL(_Link_.CACODICE,space(20))
      this.w_MVCODART = NVL(_Link_.CACODART,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_MVCODICE = space(20)
      endif
      this.w_MVCODART = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MMSERIAL
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MVM_MAST_IDX,3]
    i_lTable = "MVM_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MVM_MAST_IDX,2], .t., this.MVM_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MVM_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MMSERIAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MMSERIAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MMSERIAL,MMNUMDOC,MMALFDOC,MMTCAMAG,MMCODVAL";
                   +" from "+i_cTable+" "+i_lTable+" where MMSERIAL="+cp_ToStrODBC(this.w_MMSERIAL);
                   +" and MMSERIAL="+cp_ToStrODBC(this.w_MMSERIAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MMSERIAL',this.w_MMSERIAL;
                       ,'MMSERIAL',this.w_MMSERIAL)
            select MMSERIAL,MMNUMDOC,MMALFDOC,MMTCAMAG,MMCODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MMSERIAL = NVL(_Link_.MMSERIAL,space(10))
      this.w_NUMDOCM = NVL(_Link_.MMNUMDOC,0)
      this.w_SERDOCM = NVL(_Link_.MMALFDOC,space(10))
      this.w_CAMAG = NVL(_Link_.MMTCAMAG,space(5))
      this.w_VALDOCM = NVL(_Link_.MMCODVAL,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_MMSERIAL = space(10)
      endif
      this.w_NUMDOCM = 0
      this.w_SERDOCM = space(10)
      this.w_CAMAG = space(5)
      this.w_VALDOCM = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MVM_MAST_IDX,2])+'\'+cp_ToStr(_Link_.MMSERIAL,1)+'\'+cp_ToStr(_Link_.MMSERIAL,1)
      cp_ShowWarn(i_cKey,this.MVM_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MMSERIAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oNUMDOCD_1_8.value==this.w_NUMDOCD)
      this.oPgFrm.Page1.oPag.oNUMDOCD_1_8.value=this.w_NUMDOCD
    endif
    if not(this.oPgFrm.Page1.oPag.oSERDOCD_1_9.value==this.w_SERDOCD)
      this.oPgFrm.Page1.oPag.oSERDOCD_1_9.value=this.w_SERDOCD
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMDOCM_1_16.value==this.w_NUMDOCM)
      this.oPgFrm.Page1.oPag.oNUMDOCM_1_16.value=this.w_NUMDOCM
    endif
    if not(this.oPgFrm.Page1.oPag.oSERDOCM_1_17.value==this.w_SERDOCM)
      this.oPgFrm.Page1.oPag.oSERDOCM_1_17.value=this.w_SERDOCM
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPDOC_1_28.value==this.w_TIPDOC)
      this.oPgFrm.Page1.oPag.oTIPDOC_1_28.value=this.w_TIPDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMAG_1_29.value==this.w_CAMAG)
      this.oPgFrm.Page1.oPag.oCAMAG_1_29.value=this.w_CAMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oVALDOC_1_32.value==this.w_VALDOC)
      this.oPgFrm.Page1.oPag.oVALDOC_1_32.value=this.w_VALDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oVALDOCM_1_33.value==this.w_VALDOCM)
      this.oPgFrm.Page1.oPag.oVALDOCM_1_33.value=this.w_VALDOCM
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MVSERIAL = this.w_MVSERIAL
    this.o_MMSERIAL = this.w_MMSERIAL
    this.o_DATDOC = this.w_DATDOC
    return

enddefine

* --- Define pages as container
define class tgsar_kucPag1 as StdContainer
  Width  = 565
  height = 322
  stdWidth  = 565
  stdheight = 322
  resizeXpos=317
  resizeYpos=208
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oNUMDOCD_1_8 as StdField with uid="JLHABGZCMB",rtseq=5,rtrep=.f.,;
    cFormVar = "w_NUMDOCD", cQueryName = "NUMDOCD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento selezionato",;
    HelpContextID = 72331990,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=220, Top=269

  func oNUMDOCD_1_8.mHide()
    with this.Parent.oContained
      return (.w_NUMRIF=-10)
    endwith
  endfunc

  add object oSERDOCD_1_9 as StdField with uid="AKTVRTHJSQ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_SERDOCD", cQueryName = "SERDOCD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento selezionato",;
    HelpContextID = 72348454,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=348, Top=269, InputMask=replicate('X',10)

  func oSERDOCD_1_9.mHide()
    with this.Parent.oContained
      return (.w_NUMRIF=-10)
    endwith
  endfunc


  add object oBtn_1_12 as StdButton with uid="FRSHLUKCSC",left=456, top=267, width=48,height=45,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=1;
    , ToolTipText = "Visualizza il documento/movimento di magazzino associato alla riga selezionata";
    , HelpContextID = 106810721;
    , Caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      with this.Parent.oContained
        GSVE_BZM(this.Parent.oContained,.w_MVSERIAL, .w_PARAME)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_12.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_MVSERIAL))
      endwith
    endif
  endfunc

  func oBtn_1_12.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_NUMRIF=-10)
     endwith
    endif
  endfunc


  add object oBtn_1_14 as StdButton with uid="EMKIAWJZRA",left=456, top=267, width=48,height=45,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=1;
    , ToolTipText = "Visualizza il documento/movimento di magazzino associato alla riga selezionata";
    , HelpContextID = 106810721;
    , Caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      with this.Parent.oContained
        GSAR_BZM(this.Parent.oContained,.w_MMSERIAL, .w_NUMRIF)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_14.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_MMSERIAL))
      endwith
    endif
  endfunc

  func oBtn_1_14.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_NUMRIF=-20)
     endwith
    endif
  endfunc

  add object oNUMDOCM_1_16 as StdField with uid="UJLEEMRLOL",rtseq=11,rtrep=.f.,;
    cFormVar = "w_NUMDOCM", cQueryName = "NUMDOCM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento selezionato",;
    HelpContextID = 196103466,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=220, Top=269

  func oNUMDOCM_1_16.mHide()
    with this.Parent.oContained
      return (.w_NUMRIF=-20)
    endwith
  endfunc

  add object oSERDOCM_1_17 as StdField with uid="SYVSRAKKER",rtseq=12,rtrep=.f.,;
    cFormVar = "w_SERDOCM", cQueryName = "SERDOCM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento selezionato",;
    HelpContextID = 196087002,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=348, Top=269, InputMask=replicate('X',10)

  func oSERDOCM_1_17.mHide()
    with this.Parent.oContained
      return (.w_NUMRIF=-20)
    endwith
  endfunc


  add object ZoomCOS as cp_zoombox with uid="IINBHLOGZY",left=2, top=9, width=561,height=256,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="GSVE_KUC",bOptions=.f.,bAdvOptions=.f.,bQueryOnDblClick=.f.,bReadOnly=.f.,bQueryOnLoad=.f.,cTable="DOC_MAST",cZoomOnZoom="",cMenuFile="",bRetriveAllRows=.f.,;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 112365542

  add object oTIPDOC_1_28 as StdField with uid="QUOYBTROTN",rtseq=22,rtrep=.f.,;
    cFormVar = "w_TIPDOC", cQueryName = "TIPDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipo documento selezionato",;
    HelpContextID = 196094154,;
   bGlobalFont=.t.,;
    Height=21, Width=52, Left=102, Top=269, InputMask=replicate('X',5)

  func oTIPDOC_1_28.mHide()
    with this.Parent.oContained
      return (.w_NUMRIF=-10)
    endwith
  endfunc

  add object oCAMAG_1_29 as StdField with uid="IXDNZYAMHD",rtseq=23,rtrep=.f.,;
    cFormVar = "w_CAMAG", cQueryName = "CAMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipo documento selezionato",;
    HelpContextID = 255025626,;
   bGlobalFont=.t.,;
    Height=21, Width=52, Left=102, Top=269, InputMask=replicate('X',5)

  func oCAMAG_1_29.mHide()
    with this.Parent.oContained
      return (.w_NUMRIF=-20)
    endwith
  endfunc

  add object oVALDOC_1_32 as StdField with uid="OHUFVBAUHE",rtseq=24,rtrep=.f.,;
    cFormVar = "w_VALDOC", cQueryName = "VALDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Valuta documento selezionato",;
    HelpContextID = 196112554,;
   bGlobalFont=.t.,;
    Height=21, Width=52, Left=102, Top=294, InputMask=replicate('X',3)

  func oVALDOC_1_32.mHide()
    with this.Parent.oContained
      return (.w_NUMRIF=-10)
    endwith
  endfunc

  add object oVALDOCM_1_33 as StdField with uid="UTYYTPBKUS",rtseq=25,rtrep=.f.,;
    cFormVar = "w_VALDOCM", cQueryName = "VALDOCM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Valuta documento selezionato",;
    HelpContextID = 196112554,;
   bGlobalFont=.t.,;
    Height=21, Width=52, Left=102, Top=294, InputMask=replicate('X',3)

  func oVALDOCM_1_33.mHide()
    with this.Parent.oContained
      return (.w_NUMRIF=-20)
    endwith
  endfunc


  add object oObj_1_34 as cp_runprogram with uid="LJCCJQURTX",left=5, top=323, width=223,height=23,;
    caption='GSAR_BZM',;
   bGlobalFont=.t.,;
    prg="GSAR_BZM(iif(empty(w_MVSERIAL), w_MMSERIAL, w_MVSERIAL),w_NUMRIF1)",;
    cEvent = "w_zoomcos selected",;
    nPag=1;
    , HelpContextID = 195235661


  add object oBtn_1_38 as StdButton with uid="KNQTJANIEM",left=508, top=267, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 58314682;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_38.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_1 as StdString with uid="KYFYOIVTHS",Visible=.t., Left=2, Top=269,;
    Alignment=1, Width=58, Height=19,;
    Caption="Rif.doc.:"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.t., FontStrikeThru=.f.

  add object oStr_1_2 as StdString with uid="PGJHBENFDX",Visible=.t., Left=156, Top=269,;
    Alignment=1, Width=61, Height=18,;
    Caption="Num. doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="XQMHUIITOL",Visible=.t., Left=340, Top=269,;
    Alignment=2, Width=7, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="ZJVYYMKPCE",Visible=.t., Left=69, Top=269,;
    Alignment=1, Width=31, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="GFFRVFVTVB",Visible=.t., Left=36, Top=294,;
    Alignment=1, Width=65, Height=18,;
    Caption="Val. doc.:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_kuc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
