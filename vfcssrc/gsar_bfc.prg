* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bfc                                                        *
*              CALCOLA CODICE FISCALE (ZOOM)                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-01-07                                                      *
* Last revis.: 2014-03-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_CONTROL,w_COGNOME,w_NOME,w_ANLOCNAS,w_PRONAS,w_DATNAS,w_SESSO,w_TIPCTRLS,w_CODISO,w_CODCOM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bfc",oParentObject,m.w_CONTROL,m.w_COGNOME,m.w_NOME,m.w_ANLOCNAS,m.w_PRONAS,m.w_DATNAS,m.w_SESSO,m.w_TIPCTRLS,m.w_CODISO,m.w_CODCOM)
return(i_retval)

define class tgsar_bfc as StdBatch
  * --- Local variables
  w_CONTROL = space(10)
  w_COGNOME = space(40)
  w_NOME = space(20)
  w_ANLOCNAS = space(30)
  w_PRONAS = space(2)
  w_DATNAS = ctod("  /  /  ")
  w_SESSO = space(1)
  w_TIPCTRLS = space(1)
  w_CODISO = space(1)
  w_CODCOM = space(4)
  w_LOCNAS = space(50)
  w_FRANAS = space(50)
  w_CODFIS = space(16)
  * --- WorkFile variables
  ANAG_CAP_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Routine da invocare sullo zoom dei campi codice fiscale
    *     Calcola il codice fiscale riceve come parametri:
    *     w_CONTROL = nome controllo sul quale scrivere il codice fiscale calcolato
    *     w_COGNOME = Cognome
    *     w_NOME = Nome
    *     w_ANLOCNAS = localit� di nascita
    *     w_PRONAS = provincia del luogo di nascita (questi due parametri determinani il codice catastale comune)
    *     w_DATNAS = Data di nascita
    *     w_SESSO = Sesso (M/F)
    *     w_TIPCTRLS = M/D, se il control � fisso (M( o ripetuto (D) di default se non definito M
    *     w_CODISO = Codice ISO della nazione di appartenenza
    *     w_CODCOM = codice comune di nascita
    this.w_FRANAS = ""
    * --- Calcoli e controlli sul codice fiscale partono solo se la nazione di appartenenza (del cliente/fornitore) � I' talia oppure non � stat specificat
    if TYPE ("w_CODISO")="L" OR (TYPE ("w_CODISO")="C" AND ( EMPTY(this.w_CODISO) OR this.w_CODISO="IT" ))
      if not Empty( this.w_CONTROL )
        if empty(nvl(this.w_CODCOM,""))
          if "-"$this.w_ANLOCNAS
            this.w_FRANAS = ALLTRIM(substr(this.w_ANLOCNAS,1,at(" - ",this.w_ANLOCNAS)))+"%"
            this.w_ANLOCNAS = substr(this.w_ANLOCNAS+space(30),at(" - ",this.w_ANLOCNAS)+3,30)
          endif
          this.w_ANLOCNAS = ALLTRIM(this.w_ANLOCNAS)
          * --- Cerco prima per entrambi se non trovo ricerco solo la localit�
          if CP_DBTYPE="Oracle" or CP_DBTYPE="PostgreSQL"
            * --- uso la upper per trendere la ricerca case insensitive
            if !empty(nvl(this.w_ANLOCNAS,"")) AND !empty(nvl(this.w_PRONAS,""))
              * --- Cerco prima per entrambi se non trovo ricerco solo la localit�
              * --- Select from ANAG_CAP
              i_nConn=i_TableProp[this.ANAG_CAP_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ANAG_CAP_idx,2],.t.,this.ANAG_CAP_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select CPCODFIS  from "+i_cTable+" ANAG_CAP ";
                    +" where UPPER( CPDESLOC ) = UPPER("+cp_ToStrODBC(this.w_ANLOCNAS)+") AND   UPPER( CPCODPRO ) = UPPER("+cp_ToStrODBC(this.w_PRONAS)+")";
                     ,"_Curs_ANAG_CAP")
              else
                select CPCODFIS from (i_cTable);
                 where UPPER( CPDESLOC ) = UPPER(this.w_ANLOCNAS) AND   UPPER( CPCODPRO ) = UPPER(this.w_PRONAS);
                  into cursor _Curs_ANAG_CAP
              endif
              if used('_Curs_ANAG_CAP')
                select _Curs_ANAG_CAP
                locate for 1=1
                do while not(eof())
                this.w_CODCOM = NVL(_Curs_ANAG_CAP.CPCODFIS,"")
                  select _Curs_ANAG_CAP
                  continue
                enddo
                use
              endif
            endif
            if empty(nvl(this.w_CODCOM,"")) AND !empty(nvl(this.w_ANLOCNAS,""))
              * --- Select from ANAG_CAP
              i_nConn=i_TableProp[this.ANAG_CAP_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ANAG_CAP_idx,2],.t.,this.ANAG_CAP_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select CPCODFIS  from "+i_cTable+" ANAG_CAP ";
                    +" where UPPER( CPDESLOC ) = UPPER("+cp_ToStrODBC(this.w_ANLOCNAS)+")";
                     ,"_Curs_ANAG_CAP")
              else
                select CPCODFIS from (i_cTable);
                 where UPPER( CPDESLOC ) = UPPER(this.w_ANLOCNAS);
                  into cursor _Curs_ANAG_CAP
              endif
              if used('_Curs_ANAG_CAP')
                select _Curs_ANAG_CAP
                locate for 1=1
                do while not(eof())
                this.w_CODCOM = NVL(_Curs_ANAG_CAP.CPCODFIS,"")
                  select _Curs_ANAG_CAP
                  continue
                enddo
                use
              endif
            endif
          else
            if !empty(nvl(this.w_ANLOCNAS,"")) AND !empty(nvl(this.w_PRONAS,""))
              * --- Cerco prima per entrambi se non trovo ricerco solo la localit�
              * --- Read from ANAG_CAP
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.ANAG_CAP_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ANAG_CAP_idx,2],.t.,this.ANAG_CAP_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "CPCODFIS"+;
                  " from "+i_cTable+" ANAG_CAP where ";
                      +"CPDESLOC = "+cp_ToStrODBC(this.w_ANLOCNAS);
                      +" and CPCODPRO = "+cp_ToStrODBC(this.w_PRONAS);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  CPCODFIS;
                  from (i_cTable) where;
                      CPDESLOC = this.w_ANLOCNAS;
                      and CPCODPRO = this.w_PRONAS;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_CODCOM = NVL(cp_ToDate(_read_.CPCODFIS),cp_NullValue(_read_.CPCODFIS))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            if empty(nvl(this.w_CODCOM,"")) AND !empty(nvl(this.w_ANLOCNAS,""))
              * --- Read from ANAG_CAP
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.ANAG_CAP_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ANAG_CAP_idx,2],.t.,this.ANAG_CAP_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "CPCODFIS"+;
                  " from "+i_cTable+" ANAG_CAP where ";
                      +"CPDESLOC = "+cp_ToStrODBC(this.w_ANLOCNAS);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  CPCODFIS;
                  from (i_cTable) where;
                      CPDESLOC = this.w_ANLOCNAS;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_CODCOM = NVL(cp_ToDate(_read_.CPCODFIS),cp_NullValue(_read_.CPCODFIS))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
          endif
          * --- se w_CODCOM � ancora vuoto significa che la localit� era pi� lunga di 30 caratteri ed � stata tagliata, quindi non viene trovata dalle precedenti query
          if empty(nvl(this.w_CODCOM,""))
            this.w_LOCNAS = this.w_ANLOCNAS+"%"
            * --- Select from query\GSAR_BFC
            do vq_exec with 'query\GSAR_BFC',this,'_Curs_query_GSAR_BFC','',.f.,.t.
            if used('_Curs_query_GSAR_BFC')
              select _Curs_query_GSAR_BFC
              locate for 1=1
              do while not(eof())
              this.w_CODCOM = CPCODFIS
              exit
                select _Curs_query_GSAR_BFC
                continue
              enddo
              use
            endif
          endif
        endif
        this.w_CODFIS = calccodfis( this.w_COGNOME , this.w_NOME , this.w_DATNAS , this.w_SESSO , this.w_CODCOM )
        * --- Se il codice fiscale � calcolato allora lo scrivo nel control.
        if not Empty( this.w_CODFIS )
          * --- Code Painter trasforma il parametro scritto come 'w_XXX' nel codice in
          *     '.w_XXX'....
          this.w_CONTROL = Strtran( this.w_CONTROL , "." , "")
          setvaluelinked( iif( vartype( this.w_TIPCTRLS ) ="C" , this.w_TIPCTRLS , "MN" ) , this.oParentObject , this.w_CONTROL , this.w_CODFIS )
        endif
      endif
    endif
  endproc


  proc Init(oParentObject,w_CONTROL,w_COGNOME,w_NOME,w_ANLOCNAS,w_PRONAS,w_DATNAS,w_SESSO,w_TIPCTRLS,w_CODISO,w_CODCOM)
    this.w_CONTROL=w_CONTROL
    this.w_COGNOME=w_COGNOME
    this.w_NOME=w_NOME
    this.w_ANLOCNAS=w_ANLOCNAS
    this.w_PRONAS=w_PRONAS
    this.w_DATNAS=w_DATNAS
    this.w_SESSO=w_SESSO
    this.w_TIPCTRLS=w_TIPCTRLS
    this.w_CODISO=w_CODISO
    this.w_CODCOM=w_CODCOM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ANAG_CAP'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_ANAG_CAP')
      use in _Curs_ANAG_CAP
    endif
    if used('_Curs_ANAG_CAP')
      use in _Curs_ANAG_CAP
    endif
    if used('_Curs_query_GSAR_BFC')
      use in _Curs_query_GSAR_BFC
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_CONTROL,w_COGNOME,w_NOME,w_ANLOCNAS,w_PRONAS,w_DATNAS,w_SESSO,w_TIPCTRLS,w_CODISO,w_CODCOM"
endproc
