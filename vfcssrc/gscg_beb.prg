* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_beb                                                        *
*              Esportazione basilea 2                                          *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_158]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-04-23                                                      *
* Last revis.: 2011-04-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_beb",oParentObject)
return(i_retval)

define class tgscg_beb as StdBatch
  * --- Local variables
  w_ESERCIZIO = space(4)
  w_ESPRECPREC = space(4)
  w_PRINIESE = ctod("  /  /  ")
  w_DATA1_F = ctod("  /  /  ")
  w_DATA2_F = ctod("  /  /  ")
  w_OKGEN = 0
  w_FILE_XML = space(254)
  w_HANDLEFILE = 0
  w_LINEFEED = space(2)
  w_LIMPORTO = 0
  w_MATPRO = space(10)
  w_NAGAZI = space(5)
  w_PERAZI = space(1)
  w_PIVAZI = space(12)
  w_COFAZI = space(16)
  w_CODFISCALE = space(16)
  w_CODCAT = space(4)
  w_TMPS = space(254)
  w_NAGAZI = space(5)
  w_PERAZI = space(1)
  w_PIVAZI = space(12)
  w_COFAZI = space(16)
  w_CODFISCALE = space(16)
  w_CODCAT = space(4)
  w_TMPS = space(254)
  w_FORMULA = space(100)
  w_IMPORTO = 0
  w_DAT_ESE_IN = ctod("  /  /  ")
  w_DAT_ESE_FIN = ctod("  /  /  ")
  w_DAT_ESE_IN_W = ctod("  /  /  ")
  w_DAT_ESE_FIN_W = ctod("  /  /  ")
  w_DATREG = ctod("  /  /  ")
  w_CODAZI = space(5)
  w_CODOLT = 0
  w_CODICE = space(15)
  w_DESCRI = space(120)
  w_COLUMN1 = space(18)
  w_COLUMN2 = space(18)
  w_CODVOC = space(20)
  w_CODFIS = space(16)
  w_INIESE = space(10)
  w_FINESE = space(10)
  w_IMPARRO = 0
  * --- WorkFile variables
  TMPPNT_MAST_idx=0
  TMP_MAST_idx=0
  TMPMOVIMAST_idx=0
  AZIENDA_idx=0
  ESERCIZI_idx=0
  ZOOMPART_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esporta la contabilit� generale in un formato XML per il
    *     servizio Basilea 2 presente sul Supermercato Dell'informazione
    *     Zucchetti
    * --- B : esegue generazione file XML per Basilea2
    *     M : esegue generazione file UNIX per Fisco Azienda
    * --- Crea TMPMOVIMAST contenente tutti gli importi da contabilit�.
    *     Da questi prelever� i dati da assocciare alle Voci SDI-Basilea 2
    this.w_ESERCIZIO = this.oParentObject.w_ESE
    if !chknfile( alltrim(this.oParentObject.w_PATH),"F")
      i_retcode = 'stop'
      return
    endif
    GSAR_BGB(this, this.w_ESERCIZIO , this.oParentObject.w_TIPSTA , this.oParentObject.w_ESSALDI , this.oParentObject.w_ESPREC , this.oParentObject.w_DATA1 , this.oParentObject.w_DATA2 , this.oParentObject.w_PROVVI , this.oParentObject.w_SUPERBU , this.oParentObject.w_CODBUN , this.oParentObject.w_MASABI,"N" )
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.oParentObject.w_CONFRONTO="S" AND this.oParentObject.w_GESTIONE="B"
      this.w_ESERCIZIO = this.oParentObject.w_esepre
      if Empty( this.w_ESERCIZIO )
        ah_ErrorMsg("L'esercizio prescelto non ha esercizi precendenti definiti",,"")
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Esercizio precedente al precedente...
      * --- Read from ESERCIZI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ESERCIZI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ESINIESE"+;
          " from "+i_cTable+" ESERCIZI where ";
              +"ESCODAZI = "+cp_ToStrODBC(i_CODAZI);
              +" and ESCODESE = "+cp_ToStrODBC(this.w_ESERCIZIO);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ESINIESE;
          from (i_cTable) where;
              ESCODAZI = i_CODAZI;
              and ESCODESE = this.w_ESERCIZIO;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PRINIESE = NVL(cp_ToDate(_read_.ESINIESE),cp_NullValue(_read_.ESINIESE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_ESPRECPREC = CALCESPR(i_CODAZI,this.w_PRINIESE,.T.)
      if Empty( this.w_ESPRECPREC )
        ah_ErrorMsg("L'esercizio precedente dell'esercizio prescelto non ha esercizi precendenti definiti",,"")
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Se filtro da data a data riporto la data nell'anno precedente..
      if this.oParentObject.w_TOTDAT="D"
        this.w_DATA1_F = CTOD(STR(DAY(this.oParentObject.w_DATA1))+"-"+STR(MONTH(this.oParentObject.w_DATA1))+"-"+STR(YEAR(this.w_PRINIESE)))
        this.w_DATA2_F = CTOD(STR(DAY(this.oParentObject.w_DATA2))+"-"+STR(MONTH(this.oParentObject.w_DATA2))+"-"+STR(YEAR(this.w_PRINIESE)+YEAR(this.oParentObject.w_DATA2)-YEAR(this.oParentObject.w_DATA1)))
      endif
      * --- Recupero dati esercizio precedente..
      GSAR_BGB(this, this.w_ESERCIZIO , this.oParentObject.w_TIPSTA , this.oParentObject.w_ESSALDI , this.oParentObject.w_ESPREC , this.w_DATA1_F , this.w_DATA2_F , this.oParentObject.w_PROVVI , this.oParentObject.w_SUPERBU , this.oParentObject.w_CODBUN , this.oParentObject.w_MASABI )
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Avvio la generazione del file Xml
    this.w_OKGEN = 0
    this.w_FILE_XML = Alltrim( this.oParentObject.w_PATH ) + Alltrim( this.oParentObject.w_FILE )
    if Not File(this.w_FILE_XML)Or ah_YesNo("Il file esiste gia, si desidera sovrascriverlo?")
      * --- Handle del file Xml..
      * --- A capo..
      if this.oParentObject.w_GESTIONE $ "M-B"
        this.w_HANDLEFILE = FCREATE(this.w_FILE_XML)
        if this.w_HANDLEFILE < 0
          ah_ErrorMsg("Errore nella creazione del file %1 %2.","!","",SUBSTR(this.oParentObject.w_TIPFILE,2,3),this.w_FILE_XML)
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
      * --- Read from AZIENDA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AZIENDA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AZPERAZI,AZPIVAZI,AZCOFAZI,AZCODCAT,AZNAGAZI"+;
          " from "+i_cTable+" AZIENDA where ";
              +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AZPERAZI,AZPIVAZI,AZCOFAZI,AZCODCAT,AZNAGAZI;
          from (i_cTable) where;
              AZCODAZI = i_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PERAZI = NVL(cp_ToDate(_read_.AZPERAZI),cp_NullValue(_read_.AZPERAZI))
        this.w_PIVAZI = NVL(cp_ToDate(_read_.AZPIVAZI),cp_NullValue(_read_.AZPIVAZI))
        this.w_COFAZI = NVL(cp_ToDate(_read_.AZCOFAZI),cp_NullValue(_read_.AZCOFAZI))
        this.w_CODCAT = NVL(cp_ToDate(_read_.AZCODCAT),cp_NullValue(_read_.AZCODCAT))
        this.w_NAGAZI = NVL(cp_ToDate(_read_.AZNAGAZI),cp_NullValue(_read_.AZNAGAZI))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      do case
        case this.oParentObject.w_GESTIONE="B"
          this.w_LINEFEED = CHR(13)+CHR(10)
          this.w_OKGEN = 1
          FWRITE(this.w_HANDLEFILE,"<?xml version="+CHR(34)+"1.0"+CHR(34)+" encoding="+CHR(34)+"utf-8"+CHR(34)+"?>"+this.w_LINEFEED)
          FWRITE(this.w_HANDLEFILE,'<file xmlns="http://cogesweb.zucchetti.it/tracciatoImportazione" versione="01-00-01" provenienza='+iif( g_APPLICATION = "ADHOC REVOLUTION", '"AHR"','"AHE"' )+">"+this.w_LINEFEED)
          * --- Recupero la matricola dal file di licenza
           
 w_PRODOTTO = space(3) 
 w_NUMREL = space(10) 
 w_PIVINS = space(12) 
 w_PIVCLI = space(12) 
 w_RAGINS = space(40) 
 w_MAXUTE = 0 
 w_TIPDBF = space(2) 
 w_INDCLI = space(40) 
 w_RAGINST = space(40) 
 L_MATPRO = space(10)
          if GetRelease( @w_PRODOTTO , @w_NUMREL ,@w_PIVINS, @w_PIVCLI, @w_RAGINS , @w_MAXUTE, @w_TIPDBF, @w_INDCLI, @w_RAGINST, @L_MATPRO )
            this.w_MATPRO = L_MATPRO
          endif
          this.w_MATPRO = This.convertXML_Basilea2 ( this.w_MATPRO )
          * --- Se anonimo non calcolo il codice fiscale...
          * --- Read from AZIENDA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.AZIENDA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "AZPERAZI,AZPIVAZI,AZCOFAZI,AZCODCAT,AZNAGAZI"+;
              " from "+i_cTable+" AZIENDA where ";
                  +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              AZPERAZI,AZPIVAZI,AZCOFAZI,AZCODCAT,AZNAGAZI;
              from (i_cTable) where;
                  AZCODAZI = i_CODAZI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_PERAZI = NVL(cp_ToDate(_read_.AZPERAZI),cp_NullValue(_read_.AZPERAZI))
            this.w_PIVAZI = NVL(cp_ToDate(_read_.AZPIVAZI),cp_NullValue(_read_.AZPIVAZI))
            this.w_COFAZI = NVL(cp_ToDate(_read_.AZCOFAZI),cp_NullValue(_read_.AZCOFAZI))
            this.w_CODCAT = NVL(cp_ToDate(_read_.AZCODCAT),cp_NullValue(_read_.AZCODCAT))
            this.w_NAGAZI = NVL(cp_ToDate(_read_.AZNAGAZI),cp_NullValue(_read_.AZNAGAZI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.oParentObject.w_ANONIMO="S"
            this.w_CODFISCALE = ""
          else
            * --- Codice Fiscale...
            this.w_CODFISCALE = This.convertXML_Basilea2 ( IIF( this.w_PERAZI="S" , this.w_COFAZI , this.w_PIVAZI ) )
          endif
          this.w_MATPRO = This.convertXML_Basilea2 ( this.w_MATPRO )
          * --- Se anonimo non calcolo il codice fiscale...
          if this.oParentObject.w_ANONIMO="S"
            this.w_CODFISCALE = ""
          else
            * --- Codice Fiscale...
            this.w_CODFISCALE = This.convertXML_Basilea2 ( IIF( this.w_PERAZI="S" , this.w_COFAZI , this.w_PIVAZI ) )
          endif
          * --- Codice Cliente
          *     Codice Azienda
          *     codice fiscale
          this.w_TMPS = 'azienda codiceCliente="'+ Alltrim( this.w_MATPRO ) +'" codiceAzienda="'+This.convertXML_Basilea2 ( Alltrim(i_CODAZI) )+'" codiceFiscale="'+Alltrim( this.w_CODFISCALE )+'"'
          * --- denominazione
          this.w_TMPS = this.w_TMPS+' denominazione="'+This.convertXML_Basilea2(Alltrim( g_RAGAZI)) +'"'
          * --- Codice Catastale
          this.w_TMPS = this.w_TMPS+' codiceCatastale="'+This.convertXML_Basilea2( this.w_CODCAT ) +'"'
          * --- Indirizzo
          this.w_TMPS = this.w_TMPS+' indirizzo="'+This.convertXML_Basilea2( g_INDAZI ) +'"'
          * --- Natura Giuridica - Se maggiore di 3 char lo sbianco...
          this.w_NAGAZI = IIF( Len ( Alltrim( this.w_NAGAZI ) )>3 , Space(3), alltrim( this.w_NAGAZI ) )
          this.w_TMPS = this.w_TMPS+' naturaGiuridica="'+This.convertXML_Basilea2( this.w_NAGAZI ) +'"'
          * --- Attivita vuota...
          this.w_TMPS = this.w_TMPS+' attivita="" codiceAziendaAlt="" consolidato="'+ This.convertXML_Basilea2( this.oParentObject.w_CONSOLIDATO ) +'"'
          FWRITE(this.w_HANDLEFILE,"<"+ this.w_TMPS + ">"+this.w_LINEFEED)
          this.w_ESERCIZIO = this.oParentObject.w_ESE
          this.w_DAT_ESE_IN = this.oParentObject.w_DATA1
          this.w_DAT_ESE_FIN = this.oParentObject.w_DATA2
          this.w_DAT_ESE_IN_W = this.oParentObject.w_DATINIESE
          this.w_DAT_ESE_FIN_W = this.oParentObject.w_DATFINESE
          this.Pag6()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.oParentObject.w_CONFRONTO="S"
            this.w_ESERCIZIO = this.oParentObject.w_ESEPRE
            * --- Rileggo l'inizio fine esercizio
            * --- Read from ESERCIZI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ESERCIZI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ESINIESE,ESFINESE"+;
                " from "+i_cTable+" ESERCIZI where ";
                    +"ESCODAZI = "+cp_ToStrODBC(i_CODAZI);
                    +" and ESCODESE = "+cp_ToStrODBC(this.w_ESERCIZIO);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ESINIESE,ESFINESE;
                from (i_cTable) where;
                    ESCODAZI = i_CODAZI;
                    and ESCODESE = this.w_ESERCIZIO;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_DAT_ESE_IN_W = NVL(cp_ToDate(_read_.ESINIESE),cp_NullValue(_read_.ESINIESE))
              this.w_DAT_ESE_FIN_W = NVL(cp_ToDate(_read_.ESFINESE),cp_NullValue(_read_.ESFINESE))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if this.oParentObject.w_TOTDAT="D"
              this.w_DAT_ESE_IN = CTOD(STR(DAY(this.oParentObject.w_DATA1))+"-"+STR(MONTH(this.oParentObject.w_DATA1))+"-"+STR(YEAR(this.w_PRINIESE)))
              this.w_DAT_ESE_FIN = CTOD(STR(DAY(this.oParentObject.w_DATA2))+"-"+STR(MONTH(this.oParentObject.w_DATA2))+"-"+STR(YEAR(this.w_PRINIESE)+YEAR(this.oParentObject.w_DATA2)-YEAR(this.oParentObject.w_DATA1)))
            else
              * --- Nel caso non filtro per data per recuperare la data pi� alta filtro solo per
              *     esercizio
              this.w_DAT_ESE_IN = Ctod("  -  -    ")
              this.w_DAT_ESE_FIN = Ctod("  -  -    ")
            endif
            this.Pag6()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          FWRITE(this.w_HANDLEFILE,"</azienda>"+this.w_LINEFEED)
          FWRITE(this.w_HANDLEFILE,"</file>"+this.w_LINEFEED)
        case this.oParentObject.w_GESTIONE="M"
          * --- Creazione File Fisco Azienda
          this.w_LINEFEED = CHR(10)
          this.w_ESERCIZIO = this.oParentObject.w_ESE
          this.w_OKGEN = 2
          * --- Select from GSCG_BEB_7
          do vq_exec with 'GSCG_BEB_7',this,'_Curs_GSCG_BEB_7','',.f.,.t.
          if used('_Curs_GSCG_BEB_7')
            select _Curs_GSCG_BEB_7
            locate for 1=1
            do while not(eof())
            * --- Se la natura e Costo o Ricavo metto 'E'
            this.w_OKGEN = 1
            this.w_TMPS = this.w_ESERCIZIO
            * --- tipodichiarazione : 1 = UnicoPF; 2 =UnicoSP; 3 = UnicoSC
            this.w_TMPS = this.w_TMPS + icase(Upper(alltrim(_Curs_GSCG_BEB_7.VBMASTRO))="PF","1",Upper(alltrim(_Curs_GSCG_BEB_7.VBMASTRO))="SP","2","3")
            * --- quadro : AA che assume valore "RE o RF o RG o IQ o IS" a seconda del
            *     quadro di destinazione
            *     ATTENZIONE : IQ indica che trattasi di IRAP relativa a impresa in contabilit� ordinaria; IS indica
            *     che trattasi di IRAP relativa a impresa in contabilit� semplificata.
            this.w_TMPS = this.w_TMPS + Left(alltrim(_Curs_GSCG_BEB_7.VBGRUPPO)+Space(2),2)
            * --- rigo : nnn NNN dove nnn = numero rigo e NNN = numero colonna
            this.w_TMPS = this.w_TMPS + Left(alltrim(_Curs_GSCG_BEB_7.VB_CONTO)+Space(6),6)
            this.w_IMPORTO = Nvl(_Curs_GSCG_BEB_7.IMPORTO, 0 )
            * --- Se la natura e Costo o ricavo diviene per Basilea 2
            *     cambio di segno l'importo (in ad hoc tutti gli importi di bilancio sono mediati
            *     dalla loro sezione di bilancio) in basilea tutto � espresso come DARE-AVERE
            if _Curs_GSCG_BEB_7.VBNATURA="P" Or _Curs_GSCG_BEB_7.VBNATURA="R"
              this.w_IMPORTO =  - this.w_IMPORTO
            endif
            * --- segno : + oppure -. Se non impostato vale +
            this.w_TMPS = this.w_TMPS+IIF(SIGN(this.w_IMPORTO)=-1,"-","+")
            * --- L'importo va arrotondato all'unit� di euro...
            * --- importo : 9(11)
            this.w_LIMPORTO = cp_Round( this.w_IMPORTO , 0 )
            this.w_TMPS = this.w_TMPS + Right("0000000000000"+ALLTRIM(STRTRAN(Str(this.w_IMPORTO,11,0),"-","")),11)
            * ---  valuta di ingresso : E (Euro)
            this.w_TMPS = this.w_TMPS +"E"
            * --- descrizione : 40 caratteri alfanumerico
            this.w_TMPS = this.w_TMPS + Left(alltrim(_Curs_GSCG_BEB_7.VBDESCRI) + SPACE(40),40)
            FWRITE(this.w_HANDLEFILE,this.w_TMPS +this.w_LINEFEED)
              select _Curs_GSCG_BEB_7
              continue
            enddo
            use
          endif
        case this.oParentObject.w_GESTIONE="O"
          this.w_OKGEN = 2
          this.Pag7()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
      endcase
    endif
    * --- Aggiungere controllo sulla chiusura, per vedere se � andato tutto a buon fine?
    if  this.oParentObject.w_GESTIONE $ "B-M" AND not (FCLOSE(this.w_HANDLEFILE)) and this.w_OKGEN<>0
      ah_ErrorMsg("Errore in chiusura del file %1.%0Operazione annullata","!","",this.w_FILE_XML)
    else
      do case
        case this.w_OKGEN=0
          ah_ErrorMsg("Operazione annullata","i","")
        case this.w_OKGEN=1
          ah_ErrorMsg("File generato con successo","i","")
        otherwise
          ah_ErrorMsg("Per le selezioni effettuate nessun dato riscontrato","i","")
      endcase
    endif
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Applica Formule
    *     =====================
    *     Svolgo il calcolo della formula tramite macro...
    this.w_IMPORTO = _Curs_TMPPNT_MAST.IMPORTO
    this.w_FORMULA = _Curs_TMPPNT_MAST.VBFORMUL
     
 L_Err=.f. 
 L_OLDERROR=On("Error") 
 On Error L_Err=.t. 
 L_Cmd= "This.w_IMPORTO"+ this.w_FORMULA
    this.w_IMPORTO = &L_Cmd
     
 On Error &L_OLDERROR
    if L_Err
      ah_ErrorMsg("Errore nell'applicazione della formula %1 con mesaggio di errore: %2",,"",Alltrim(this.w_FORMULA),Message())
      i_retcode = 'stop'
      return
    endif
    * --- Write into TMPPNT_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPPNT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"IMPORTO ="+cp_NullLink(cp_ToStrODBC(this.w_IMPORTO),'TMPPNT_MAST','IMPORTO');
          +i_ccchkf ;
      +" where ";
          +"VBGRUPPO = "+cp_ToStrODBC(_Curs_TMPPNT_MAST.VBGRUPPO);
          +" and VBMASTRO = "+cp_ToStrODBC(_Curs_TMPPNT_MAST.VBMASTRO);
          +" and VB_CONTO = "+cp_ToStrODBC(_Curs_TMPPNT_MAST.VB_CONTO);
          +" and VB__VOCE = "+cp_ToStrODBC(_Curs_TMPPNT_MAST.VB__VOCE);
          +" and CPROWNUM = "+cp_ToStrODBC(_Curs_TMPPNT_MAST.CPROWNUM);
             )
    else
      update (i_cTable) set;
          IMPORTO = this.w_IMPORTO;
          &i_ccchkf. ;
       where;
          VBGRUPPO = _Curs_TMPPNT_MAST.VBGRUPPO;
          and VBMASTRO = _Curs_TMPPNT_MAST.VBMASTRO;
          and VB_CONTO = _Curs_TMPPNT_MAST.VB_CONTO;
          and VB__VOCE = _Curs_TMPPNT_MAST.VB__VOCE;
          and CPROWNUM = _Curs_TMPPNT_MAST.CPROWNUM;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- APPLICA VINCOLI
    *     ==========================
    *     Elimino gli importi a zero
    * --- Delete from TMPPNT_MAST
    i_nConn=i_TableProp[this.TMPPNT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"IMPORTO = "+cp_ToStrODBC(0);
             )
    else
      delete from (i_cTable) where;
            IMPORTO = 0;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Applico i vincoli, quindi vado ad eliminare i record con importi incongruenti
    *     con il vincolo...
    * --- Delete from TMPPNT_MAST
    i_nConn=i_TableProp[this.TMPPNT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".VBNATURA  = "+i_cQueryTable+".VBNATURA ";
            +" and "+i_cTable+".VBGRUPPO = "+i_cQueryTable+".VBGRUPPO";
            +" and "+i_cTable+".VB__VOCE = "+i_cQueryTable+".VB__VOCE";
            +" and "+i_cTable+".VB_CONTO  = "+i_cQueryTable+".VB_CONTO ";
            +" and "+i_cTable+".VBMASTRO   = "+i_cQueryTable+".VBMASTRO  ";
            +" and "+i_cTable+".VBCODCON = "+i_cQueryTable+".VBCODCON";
            +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
            +" and "+i_cTable+".SEGNO       = "+i_cQueryTable+".SEGNO      ";
    
      do vq_exec with 'QUERY\GSCG_BEB_10',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Cambio di segno gli importi negativi legati a vincoli in 'A' oppure legati
    *     a mastri di tipo Cli/For con vincolo...
    if this.oParentObject.w_GESTIONE<>"O"
      * --- Write into TMPPNT_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPPNT_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_MAST_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="VBNATURA ,VBGRUPPO,VB__VOCE,VB_CONTO ,VBMASTRO  ,CPROWNUM,SEGNO      ,VOC_ROWNUM"
        do vq_exec with 'QUERY\GSCG_BEB_11',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_MAST_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPPNT_MAST.VBNATURA  = _t2.VBNATURA ";
                +" and "+"TMPPNT_MAST.VBGRUPPO = _t2.VBGRUPPO";
                +" and "+"TMPPNT_MAST.VB__VOCE = _t2.VB__VOCE";
                +" and "+"TMPPNT_MAST.VB_CONTO  = _t2.VB_CONTO ";
                +" and "+"TMPPNT_MAST.VBMASTRO   = _t2.VBMASTRO  ";
                +" and "+"TMPPNT_MAST.CPROWNUM = _t2.CPROWNUM";
                +" and "+"TMPPNT_MAST.SEGNO       = _t2.SEGNO      ";
                +" and "+"TMPPNT_MAST.VOC_ROWNUM = _t2.VOC_ROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"IMPORTO = _t2.IMPORTO";
            +i_ccchkf;
            +" from "+i_cTable+" TMPPNT_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPPNT_MAST.VBNATURA  = _t2.VBNATURA ";
                +" and "+"TMPPNT_MAST.VBGRUPPO = _t2.VBGRUPPO";
                +" and "+"TMPPNT_MAST.VB__VOCE = _t2.VB__VOCE";
                +" and "+"TMPPNT_MAST.VB_CONTO  = _t2.VB_CONTO ";
                +" and "+"TMPPNT_MAST.VBMASTRO   = _t2.VBMASTRO  ";
                +" and "+"TMPPNT_MAST.CPROWNUM = _t2.CPROWNUM";
                +" and "+"TMPPNT_MAST.SEGNO       = _t2.SEGNO      ";
                +" and "+"TMPPNT_MAST.VOC_ROWNUM = _t2.VOC_ROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_MAST, "+i_cQueryTable+" _t2 set ";
            +"TMPPNT_MAST.IMPORTO = _t2.IMPORTO";
            +Iif(Empty(i_ccchkf),"",",TMPPNT_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPPNT_MAST.VBNATURA  = t2.VBNATURA ";
                +" and "+"TMPPNT_MAST.VBGRUPPO = t2.VBGRUPPO";
                +" and "+"TMPPNT_MAST.VB__VOCE = t2.VB__VOCE";
                +" and "+"TMPPNT_MAST.VB_CONTO  = t2.VB_CONTO ";
                +" and "+"TMPPNT_MAST.VBMASTRO   = t2.VBMASTRO  ";
                +" and "+"TMPPNT_MAST.CPROWNUM = t2.CPROWNUM";
                +" and "+"TMPPNT_MAST.SEGNO       = t2.SEGNO      ";
                +" and "+"TMPPNT_MAST.VOC_ROWNUM = t2.VOC_ROWNUM";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_MAST set (";
            +"IMPORTO";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.IMPORTO";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPPNT_MAST.VBNATURA  = _t2.VBNATURA ";
                +" and "+"TMPPNT_MAST.VBGRUPPO = _t2.VBGRUPPO";
                +" and "+"TMPPNT_MAST.VB__VOCE = _t2.VB__VOCE";
                +" and "+"TMPPNT_MAST.VB_CONTO  = _t2.VB_CONTO ";
                +" and "+"TMPPNT_MAST.VBMASTRO   = _t2.VBMASTRO  ";
                +" and "+"TMPPNT_MAST.CPROWNUM = _t2.CPROWNUM";
                +" and "+"TMPPNT_MAST.SEGNO       = _t2.SEGNO      ";
                +" and "+"TMPPNT_MAST.VOC_ROWNUM = _t2.VOC_ROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_MAST set ";
            +"IMPORTO = _t2.IMPORTO";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".VBNATURA  = "+i_cQueryTable+".VBNATURA ";
                +" and "+i_cTable+".VBGRUPPO = "+i_cQueryTable+".VBGRUPPO";
                +" and "+i_cTable+".VB__VOCE = "+i_cQueryTable+".VB__VOCE";
                +" and "+i_cTable+".VB_CONTO  = "+i_cQueryTable+".VB_CONTO ";
                +" and "+i_cTable+".VBMASTRO   = "+i_cQueryTable+".VBMASTRO  ";
                +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                +" and "+i_cTable+".SEGNO       = "+i_cQueryTable+".SEGNO      ";
                +" and "+i_cTable+".VOC_ROWNUM = "+i_cQueryTable+".VOC_ROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"IMPORTO = (select IMPORTO from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Per finire applico la sezione di bilancio per tutte le righe mastro non
      *     di tipo cli/for o prive di vincoli
      * --- Write into TMPPNT_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPPNT_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_MAST_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="VBNATURA ,VBGRUPPO,VB__VOCE,VB_CONTO ,VBMASTRO  ,CPROWNUM,VOC_ROWNUM"
        do vq_exec with 'QUERY\GSCG_BEB_9',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_MAST_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPPNT_MAST.VBNATURA  = _t2.VBNATURA ";
                +" and "+"TMPPNT_MAST.VBGRUPPO = _t2.VBGRUPPO";
                +" and "+"TMPPNT_MAST.VB__VOCE = _t2.VB__VOCE";
                +" and "+"TMPPNT_MAST.VB_CONTO  = _t2.VB_CONTO ";
                +" and "+"TMPPNT_MAST.VBMASTRO   = _t2.VBMASTRO  ";
                +" and "+"TMPPNT_MAST.CPROWNUM = _t2.CPROWNUM";
                +" and "+"TMPPNT_MAST.VOC_ROWNUM = _t2.VOC_ROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"IMPORTO = _t2.IMPORTO";
            +i_ccchkf;
            +" from "+i_cTable+" TMPPNT_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPPNT_MAST.VBNATURA  = _t2.VBNATURA ";
                +" and "+"TMPPNT_MAST.VBGRUPPO = _t2.VBGRUPPO";
                +" and "+"TMPPNT_MAST.VB__VOCE = _t2.VB__VOCE";
                +" and "+"TMPPNT_MAST.VB_CONTO  = _t2.VB_CONTO ";
                +" and "+"TMPPNT_MAST.VBMASTRO   = _t2.VBMASTRO  ";
                +" and "+"TMPPNT_MAST.CPROWNUM = _t2.CPROWNUM";
                +" and "+"TMPPNT_MAST.VOC_ROWNUM = _t2.VOC_ROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_MAST, "+i_cQueryTable+" _t2 set ";
            +"TMPPNT_MAST.IMPORTO = _t2.IMPORTO";
            +Iif(Empty(i_ccchkf),"",",TMPPNT_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPPNT_MAST.VBNATURA  = t2.VBNATURA ";
                +" and "+"TMPPNT_MAST.VBGRUPPO = t2.VBGRUPPO";
                +" and "+"TMPPNT_MAST.VB__VOCE = t2.VB__VOCE";
                +" and "+"TMPPNT_MAST.VB_CONTO  = t2.VB_CONTO ";
                +" and "+"TMPPNT_MAST.VBMASTRO   = t2.VBMASTRO  ";
                +" and "+"TMPPNT_MAST.CPROWNUM = t2.CPROWNUM";
                +" and "+"TMPPNT_MAST.VOC_ROWNUM = t2.VOC_ROWNUM";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_MAST set (";
            +"IMPORTO";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.IMPORTO";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPPNT_MAST.VBNATURA  = _t2.VBNATURA ";
                +" and "+"TMPPNT_MAST.VBGRUPPO = _t2.VBGRUPPO";
                +" and "+"TMPPNT_MAST.VB__VOCE = _t2.VB__VOCE";
                +" and "+"TMPPNT_MAST.VB_CONTO  = _t2.VB_CONTO ";
                +" and "+"TMPPNT_MAST.VBMASTRO   = _t2.VBMASTRO  ";
                +" and "+"TMPPNT_MAST.CPROWNUM = _t2.CPROWNUM";
                +" and "+"TMPPNT_MAST.VOC_ROWNUM = _t2.VOC_ROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_MAST set ";
            +"IMPORTO = _t2.IMPORTO";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".VBNATURA  = "+i_cQueryTable+".VBNATURA ";
                +" and "+i_cTable+".VBGRUPPO = "+i_cQueryTable+".VBGRUPPO";
                +" and "+i_cTable+".VB__VOCE = "+i_cQueryTable+".VB__VOCE";
                +" and "+i_cTable+".VB_CONTO  = "+i_cQueryTable+".VB_CONTO ";
                +" and "+i_cTable+".VBMASTRO   = "+i_cQueryTable+".VBMASTRO  ";
                +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                +" and "+i_cTable+".VOC_ROWNUM = "+i_cQueryTable+".VOC_ROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"IMPORTO = (select IMPORTO from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Rimuovo le tabelle temporanee dal server
    *     Esco dall'elaborazione
    * --- Drop temporary table TMPPNT_MAST
    i_nIdx=cp_GetTableDefIdx('TMPPNT_MAST')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPPNT_MAST')
    endif
    * --- Drop temporary table TMP_MAST
    i_nIdx=cp_GetTableDefIdx('TMP_MAST')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_MAST')
    endif
    * --- Drop temporary table ZOOMPART
    i_nIdx=cp_GetTableDefIdx('ZOOMPART')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('ZOOMPART')
    endif
    * --- Drop temporary table TMPMOVIMAST
    i_nIdx=cp_GetTableDefIdx('TMPMOVIMAST')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPMOVIMAST')
    endif
    i_retcode = 'stop'
    return
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancio l'elaborazione per l'esercizio w_ESERCIZIO...
    * --- A seconda delle selezioni posso avere pi� record a fronte dello stesso Conto
    *     per questo occorre raggruppare e sommare gli importi
    * --- Create temporary table TMP_MAST
    i_nIdx=cp_AddTableDef('TMP_MAST') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('QUERY\GSCG_BEB_5',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMP_MAST_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Drop temporary table TMPMOVIMAST
    i_nIdx=cp_GetTableDefIdx('TMPMOVIMAST')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPMOVIMAST')
    endif
    * --- Ora devo matchare gli importi determinati per l'esercizio con le voci
    *     di classificazione SDI-Basilea 2.
    *     
    *     Creo un Temporaneo SQL Contenenente la struttura (Conto / Voce Esplosa / Mastro)
    * --- Create temporary table TMPPNT_MAST
    i_nIdx=cp_AddTableDef('TMPPNT_MAST') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('QUERY\GSCG_BEB_1',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPPNT_MAST_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Vado a matchare gli importi per Conti con TMPMOVIMAST..
    * --- Write into TMPPNT_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPPNT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_MAST_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="VBCODCON,VB__TIPO"
      do vq_exec with 'QUERY\GSCG_BEB_2',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_MAST_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMPPNT_MAST.VBCODCON = _t2.VBCODCON";
              +" and "+"TMPPNT_MAST.VB__TIPO = _t2.VB__TIPO";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"IMPORTO = _t2.IMP_TOT";
          +",SEZBIL = _t2.SEZBIL";
          +",TIPMAS = _t2.TIPMAS";
          +i_ccchkf;
          +" from "+i_cTable+" TMPPNT_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMPPNT_MAST.VBCODCON = _t2.VBCODCON";
              +" and "+"TMPPNT_MAST.VB__TIPO = _t2.VB__TIPO";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_MAST, "+i_cQueryTable+" _t2 set ";
          +"TMPPNT_MAST.IMPORTO = _t2.IMP_TOT";
          +",TMPPNT_MAST.SEZBIL = _t2.SEZBIL";
          +",TMPPNT_MAST.TIPMAS = _t2.TIPMAS";
          +Iif(Empty(i_ccchkf),"",",TMPPNT_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TMPPNT_MAST.VBCODCON = t2.VBCODCON";
              +" and "+"TMPPNT_MAST.VB__TIPO = t2.VB__TIPO";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_MAST set (";
          +"IMPORTO,";
          +"SEZBIL,";
          +"TIPMAS";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.IMP_TOT,";
          +"t2.SEZBIL,";
          +"t2.TIPMAS";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TMPPNT_MAST.VBCODCON = _t2.VBCODCON";
              +" and "+"TMPPNT_MAST.VB__TIPO = _t2.VB__TIPO";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_MAST set ";
          +"IMPORTO = _t2.IMP_TOT";
          +",SEZBIL = _t2.SEZBIL";
          +",TIPMAS = _t2.TIPMAS";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".VBCODCON = "+i_cQueryTable+".VBCODCON";
              +" and "+i_cTable+".VB__TIPO = "+i_cQueryTable+".VB__TIPO";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"IMPORTO = (select IMP_TOT from "+i_cQueryTable+" where "+i_cWhere+")";
          +",SEZBIL = (select SEZBIL from "+i_cQueryTable+" where "+i_cWhere+")";
          +",TIPMAS = (select TIPMAS from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- e Mastri.
    * --- Vado ad aggiungere al temporaneo creato i mastri di tipo C/F che hanno
    *     associato un vincolo con segno Dare in modo che possa associare ad essi
    *     gli importi in avere dei mastri clienti / fornitori...
    * --- Aggiorno gli importi per i mastri con vincoli, devo in questo caso dividerli
    *     per gli importi (caso mastro cliente / fornitore ) 
    *     VBCONDIZ mi filtra questa situazione...
    * --- Write into TMPPNT_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPPNT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_MAST_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="VBCODMAS,VB__TIPO,SEGNO,VBCONDIZ"
      do vq_exec with 'QUERY\GSCG_BEB_3',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_MAST_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMPPNT_MAST.VBCODMAS = _t2.VBCODMAS";
              +" and "+"TMPPNT_MAST.VB__TIPO = _t2.VB__TIPO";
              +" and "+"TMPPNT_MAST.SEGNO = _t2.SEGNO";
              +" and "+"TMPPNT_MAST.VBCONDIZ = _t2.VBCONDIZ";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"IMPORTO = _t2.IMP_TOT";
          +",SEZBIL = _t2.SEZBIL";
          +",TIPMAS = _t2.TIPMAS";
          +i_ccchkf;
          +" from "+i_cTable+" TMPPNT_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMPPNT_MAST.VBCODMAS = _t2.VBCODMAS";
              +" and "+"TMPPNT_MAST.VB__TIPO = _t2.VB__TIPO";
              +" and "+"TMPPNT_MAST.SEGNO = _t2.SEGNO";
              +" and "+"TMPPNT_MAST.VBCONDIZ = _t2.VBCONDIZ";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_MAST, "+i_cQueryTable+" _t2 set ";
          +"TMPPNT_MAST.IMPORTO = _t2.IMP_TOT";
          +",TMPPNT_MAST.SEZBIL = _t2.SEZBIL";
          +",TMPPNT_MAST.TIPMAS = _t2.TIPMAS";
          +Iif(Empty(i_ccchkf),"",",TMPPNT_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TMPPNT_MAST.VBCODMAS = t2.VBCODMAS";
              +" and "+"TMPPNT_MAST.VB__TIPO = t2.VB__TIPO";
              +" and "+"TMPPNT_MAST.SEGNO = t2.SEGNO";
              +" and "+"TMPPNT_MAST.VBCONDIZ = t2.VBCONDIZ";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_MAST set (";
          +"IMPORTO,";
          +"SEZBIL,";
          +"TIPMAS";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.IMP_TOT,";
          +"t2.SEZBIL,";
          +"t2.TIPMAS";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TMPPNT_MAST.VBCODMAS = _t2.VBCODMAS";
              +" and "+"TMPPNT_MAST.VB__TIPO = _t2.VB__TIPO";
              +" and "+"TMPPNT_MAST.SEGNO = _t2.SEGNO";
              +" and "+"TMPPNT_MAST.VBCONDIZ = _t2.VBCONDIZ";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_MAST set ";
          +"IMPORTO = _t2.IMP_TOT";
          +",SEZBIL = _t2.SEZBIL";
          +",TIPMAS = _t2.TIPMAS";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".VBCODMAS = "+i_cQueryTable+".VBCODMAS";
              +" and "+i_cTable+".VB__TIPO = "+i_cQueryTable+".VB__TIPO";
              +" and "+i_cTable+".SEGNO = "+i_cQueryTable+".SEGNO";
              +" and "+i_cTable+".VBCONDIZ = "+i_cQueryTable+".VBCONDIZ";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"IMPORTO = (select IMP_TOT from "+i_cQueryTable+" where "+i_cWhere+")";
          +",SEZBIL = (select SEZBIL from "+i_cQueryTable+" where "+i_cWhere+")";
          +",TIPMAS = (select TIPMAS from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Applico la formule.
    *     Le formule vanno applicate come primo passo (anche se importi a zero 
    *     posso sommare, opppure trasformare dare in avere (*-1)).
    *     NON APPLICO LE FORMULE LEGATE ALLE VOCI DEVO ANCORA TOTALIZZARE
    * --- Select from TMPPNT_MAST
    i_nConn=i_TableProp[this.TMPPNT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_MAST_idx,2],.t.,this.TMPPNT_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMPPNT_MAST ";
          +" where VBFORMUL<>'' And ISVOCE=0 AND VBESPDET<>'S'";
           ,"_Curs_TMPPNT_MAST")
    else
      select * from (i_cTable);
       where VBFORMUL<>"" And ISVOCE=0 AND VBESPDET<>"S";
        into cursor _Curs_TMPPNT_MAST
    endif
    if used('_Curs_TMPPNT_MAST')
      select _Curs_TMPPNT_MAST
      locate for 1=1
      do while not(eof())
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
        select _Curs_TMPPNT_MAST
        continue
      enddo
      use
    endif
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.oParentObject.w_GESTIONE="O"
      * --- seleziono Mastri da Dettagliate solo nel caso di bilncio e oltre
      * --- Create temporary table TMPMOVIMAST
      i_nIdx=cp_AddTableDef('TMPMOVIMAST') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('QUERY\GSCGOBEB_1',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPMOVIMAST_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      * --- Valorizzo Importi solo nei record esplosi 
      * --- Write into TMPMOVIMAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="VBCODCON,VB__TIPO"
        do vq_exec with 'QUERY\GSCG_BEB_2',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPMOVIMAST_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPMOVIMAST.VBCODCON = _t2.VBCODCON";
                +" and "+"TMPMOVIMAST.VB__TIPO = _t2.VB__TIPO";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"IMPORTO = _t2.IMP_TOT";
            +",SEZBIL = _t2.SEZBIL";
            +",TIPMAS = _t2.TIPMAS";
            +i_ccchkf;
            +" from "+i_cTable+" TMPMOVIMAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPMOVIMAST.VBCODCON = _t2.VBCODCON";
                +" and "+"TMPMOVIMAST.VB__TIPO = _t2.VB__TIPO";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPMOVIMAST, "+i_cQueryTable+" _t2 set ";
            +"TMPMOVIMAST.IMPORTO = _t2.IMP_TOT";
            +",TMPMOVIMAST.SEZBIL = _t2.SEZBIL";
            +",TMPMOVIMAST.TIPMAS = _t2.TIPMAS";
            +Iif(Empty(i_ccchkf),"",",TMPMOVIMAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPMOVIMAST.VBCODCON = t2.VBCODCON";
                +" and "+"TMPMOVIMAST.VB__TIPO = t2.VB__TIPO";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPMOVIMAST set (";
            +"IMPORTO,";
            +"SEZBIL,";
            +"TIPMAS";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.IMP_TOT,";
            +"t2.SEZBIL,";
            +"t2.TIPMAS";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPMOVIMAST.VBCODCON = _t2.VBCODCON";
                +" and "+"TMPMOVIMAST.VB__TIPO = _t2.VB__TIPO";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPMOVIMAST set ";
            +"IMPORTO = _t2.IMP_TOT";
            +",SEZBIL = _t2.SEZBIL";
            +",TIPMAS = _t2.TIPMAS";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".VBCODCON = "+i_cQueryTable+".VBCODCON";
                +" and "+i_cTable+".VB__TIPO = "+i_cQueryTable+".VB__TIPO";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"IMPORTO = (select IMP_TOT from "+i_cQueryTable+" where "+i_cWhere+")";
            +",SEZBIL = (select SEZBIL from "+i_cQueryTable+" where "+i_cWhere+")";
            +",TIPMAS = (select TIPMAS from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Applico vincolo sui mastri in funzione del saldo anche nei mastri
      *     di tipo generico
      * --- Write into TMPMOVIMAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="VBCODMAS,VB__TIPO,SEGNO,VBCONDIZ"
        do vq_exec with 'QUERY\GSCG_BEB_3',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPMOVIMAST_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPMOVIMAST.VBCODMAS = _t2.VBCODMAS";
                +" and "+"TMPMOVIMAST.VB__TIPO = _t2.VB__TIPO";
                +" and "+"TMPMOVIMAST.SEGNO = _t2.SEGNO";
                +" and "+"TMPMOVIMAST.VBCONDIZ = _t2.VBCONDIZ";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"IMPORTO = _t2.IMP_TOT";
            +",SEZBIL = _t2.SEZBIL";
            +",TIPMAS = _t2.TIPMAS";
            +i_ccchkf;
            +" from "+i_cTable+" TMPMOVIMAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPMOVIMAST.VBCODMAS = _t2.VBCODMAS";
                +" and "+"TMPMOVIMAST.VB__TIPO = _t2.VB__TIPO";
                +" and "+"TMPMOVIMAST.SEGNO = _t2.SEGNO";
                +" and "+"TMPMOVIMAST.VBCONDIZ = _t2.VBCONDIZ";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPMOVIMAST, "+i_cQueryTable+" _t2 set ";
            +"TMPMOVIMAST.IMPORTO = _t2.IMP_TOT";
            +",TMPMOVIMAST.SEZBIL = _t2.SEZBIL";
            +",TMPMOVIMAST.TIPMAS = _t2.TIPMAS";
            +Iif(Empty(i_ccchkf),"",",TMPMOVIMAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPMOVIMAST.VBCODMAS = t2.VBCODMAS";
                +" and "+"TMPMOVIMAST.VB__TIPO = t2.VB__TIPO";
                +" and "+"TMPMOVIMAST.SEGNO = t2.SEGNO";
                +" and "+"TMPMOVIMAST.VBCONDIZ = t2.VBCONDIZ";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPMOVIMAST set (";
            +"IMPORTO,";
            +"SEZBIL,";
            +"TIPMAS";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.IMP_TOT,";
            +"t2.SEZBIL,";
            +"t2.TIPMAS";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPMOVIMAST.VBCODMAS = _t2.VBCODMAS";
                +" and "+"TMPMOVIMAST.VB__TIPO = _t2.VB__TIPO";
                +" and "+"TMPMOVIMAST.SEGNO = _t2.SEGNO";
                +" and "+"TMPMOVIMAST.VBCONDIZ = _t2.VBCONDIZ";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPMOVIMAST set ";
            +"IMPORTO = _t2.IMP_TOT";
            +",SEZBIL = _t2.SEZBIL";
            +",TIPMAS = _t2.TIPMAS";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".VBCODMAS = "+i_cQueryTable+".VBCODMAS";
                +" and "+i_cTable+".VB__TIPO = "+i_cQueryTable+".VB__TIPO";
                +" and "+i_cTable+".SEGNO = "+i_cQueryTable+".SEGNO";
                +" and "+i_cTable+".VBCONDIZ = "+i_cQueryTable+".VBCONDIZ";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"IMPORTO = (select IMP_TOT from "+i_cQueryTable+" where "+i_cWhere+")";
            +",SEZBIL = (select SEZBIL from "+i_cQueryTable+" where "+i_cWhere+")";
            +",TIPMAS = (select TIPMAS from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Cumulo record esplosi con record in cui non era necessario effetuare esplosione
      * --- Create temporary table ZOOMPART
      if cp_ExistTableDef('ZOOMPART')
         * add to existing table
        i_nIdx=cp_AddTableDef('ZOOMPART',.t.) && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('QUERY\GSCGOBEB_2',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.t.)
        if this.ZOOMPART_idx=0
          this.ZOOMPART_idx=i_nIdx
        endif
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      else
        i_nIdx=cp_AddTableDef('ZOOMPART',.t.) && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('QUERY\GSCGOBEB_2',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.ZOOMPART_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      endif
      * --- Drop temporary table TMPPNT_MAST
      i_nIdx=cp_GetTableDefIdx('TMPPNT_MAST')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPPNT_MAST')
      endif
      * --- Devo fare questo trasferimento per rientrare nel algoritmo principale
      *     visto che non posso creare una tabella prendendo i dati dalla stessa
      * --- Create temporary table TMPPNT_MAST
      if cp_ExistTableDef('TMPPNT_MAST')
         * add to existing table
        i_nIdx=cp_AddTableDef('TMPPNT_MAST',.t.) && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        i_nConn=i_TableProp[this.ZOOMPART_idx,3] && recupera la connessione
        i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2])
        cp_InsertIntoTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
              )
        if this.TMPPNT_MAST_idx=0
          this.TMPPNT_MAST_idx=i_nIdx
        endif
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      else
        i_nIdx=cp_AddTableDef('TMPPNT_MAST',.t.) && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        i_nConn=i_TableProp[this.ZOOMPART_idx,3] && recupera la connessione
        i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2])
        cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
              )
        this.TMPPNT_MAST_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      endif
      * --- Elimino tabelle di appoggio
      * --- Drop temporary table ZOOMPART
      i_nIdx=cp_GetTableDefIdx('ZOOMPART')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('ZOOMPART')
      endif
      * --- Drop temporary table TMPMOVIMAST
      i_nIdx=cp_GetTableDefIdx('TMPMOVIMAST')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPMOVIMAST')
      endif
    endif
    * --- Ora ho conti e mastri con relativi importi, devo totalizzare in base al segno
    *     ogni singola riga ( devo ancora dare un segno ed un vincolo alle voci )
    *     Questa ha lo scopo di costruire un importo per le voci..
    * --- Create temporary table TMPMOVIMAST
    i_nIdx=cp_AddTableDef('TMPMOVIMAST') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('QUERY\GSCG_BEB_6',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPMOVIMAST_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Rimetto il risultato in TMPPNT_MAST
    * --- Drop temporary table TMPPNT_MAST
    i_nIdx=cp_GetTableDefIdx('TMPPNT_MAST')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPPNT_MAST')
    endif
    * --- Create temporary table TMPPNT_MAST
    i_nIdx=cp_AddTableDef('TMPPNT_MAST') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.TMPPNT_MAST_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Drop temporary table TMPMOVIMAST
    i_nIdx=cp_GetTableDefIdx('TMPMOVIMAST')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPMOVIMAST')
    endif
    * --- Ora Applico le eventuali formule presenti sulle voci..
    * --- Select from TMPPNT_MAST
    i_nConn=i_TableProp[this.TMPPNT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_MAST_idx,2],.t.,this.TMPPNT_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMPPNT_MAST ";
          +" where VBFORMUL<>' ' And ISVOCE=1 AND VBESPDET<>'S'";
           ,"_Curs_TMPPNT_MAST")
    else
      select * from (i_cTable);
       where VBFORMUL<>" " And ISVOCE=1 AND VBESPDET<>"S";
        into cursor _Curs_TMPPNT_MAST
    endif
    if used('_Curs_TMPPNT_MAST')
      select _Curs_TMPPNT_MAST
      locate for 1=1
      do while not(eof())
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
        select _Curs_TMPPNT_MAST
        continue
      enddo
      use
    endif
    * --- Create temporary table ZOOMPART
    if cp_ExistTableDef('ZOOMPART')
       * add to existing table
      i_nIdx=cp_AddTableDef('ZOOMPART',.t.) && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      i_nConn=i_TableProp[this.TMPPNT_MAST_idx,3] && recupera la connessione
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_MAST_idx,2])
      cp_InsertIntoTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
            )
      if this.ZOOMPART_idx=0
        this.ZOOMPART_idx=i_nIdx
      endif
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    else
      i_nIdx=cp_AddTableDef('ZOOMPART',.t.) && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      i_nConn=i_TableProp[this.TMPPNT_MAST_idx,3] && recupera la connessione
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_MAST_idx,2])
      cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
            )
      this.ZOOMPART_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    endif
  endproc


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Dati esercizio..
    * --- Filtri per recuperare la data registrazione pi� alta del movimento..
    * --- Date di inizio e fine esercizio (scritte nel TAG esercizio)
    * --- leggo la data registrazione pi� alta tra quelle considerate, quindi
    *     se da saldi la reg. di prima nota confermate altrimenti riapplico
    *     i soliti filtri, non posso fare altrimenti, le query sono raggurppate
    *     sulla prima nota per conto..
    * --- Select from GSCG_BEB_8
    do vq_exec with 'GSCG_BEB_8',this,'_Curs_GSCG_BEB_8','',.f.,.t.
    if used('_Curs_GSCG_BEB_8')
      select _Curs_GSCG_BEB_8
      locate for 1=1
      do while not(eof())
      * --- Se nessun movimento la data � passata vuota
      this.w_DATREG = Nvl( _Curs_GSCG_BEB_8.PNDATREG , Ctod("  -  -    " ) )
        select _Curs_GSCG_BEB_8
        continue
      enddo
      use
    endif
    if Empty( this.w_DATREG )
      * --- Se nessun movimento assumo come data la data di inizio esercizio
      this.w_DATREG = this.w_DAT_ESE_IN_W
    endif
    this.w_TMPS = 'esercizio anno="'+ alltrim(Str( Year( this.w_DAT_ESE_IN_W ) )) +'"'
    * --- Imposto cmq SET CENTURY ON e SET DETA TO ITALIAN 
    *     perch� le date vanno memorizzate come dd-mm-yyyy..
    Local Old_Date, Old_century, Old_Mark 
 Old_Date=Set("Date") 
 Old_Century=Set("Century") 
 Old_Mark = Set("MARK") 
 SET DATE TO ITALIAN 
 SET CENTURY ON 
 SET MARK TO "-"
    * --- Data inizio
    this.w_TMPS = this.w_TMPS + ' dataInizio="'+Dtoc( this.w_DAT_ESE_IN_W )+'"'
    * --- Data fine
    this.w_TMPS = this.w_TMPS + ' dataFine="'+Dtoc( this.w_DAT_ESE_FIN_W )+'"'
    * --- Data ultimo movimento Prima nota
    this.w_TMPS = this.w_TMPS + ' dataRegistrazione="'+Dtoc( this.w_DATREG )+'"'
    FWRITE(this.w_HANDLEFILE,"<"+ this.w_TMPS + ">"+this.w_LINEFEED)
    * --- Re imposto i settaggi sulle date..
     
 SET DATE TO &Old_Date 
 SET CENTURY &Old_Century 
 SET MARK TO (Old_Mark)
    * --- Itero sui risultati della query � scrivo il file Xml..
    * --- Ora posso sommare gli importi divisi per voci applicando il segno
    *     (per i conti ed i Mastri � sempre uno a questo punto, solo le voci 
    *     hanno ancora il segno valido...)
    * --- Select from GSCG_BEB_7
    do vq_exec with 'GSCG_BEB_7',this,'_Curs_GSCG_BEB_7','',.f.,.t.
    if used('_Curs_GSCG_BEB_7')
      select _Curs_GSCG_BEB_7
      locate for 1=1
      do while not(eof())
      * --- Se la natura e Costo o Ricavo metto 'E'
      if _Curs_GSCG_BEB_7.VBNATURA="C" Or _Curs_GSCG_BEB_7.VBNATURA="R"
        this.w_TMPS = 'cee nE="E"'
      else
        this.w_TMPS = 'cee nE="'+alltrim(_Curs_GSCG_BEB_7.VBNATURA)+'"'
      endif
      this.w_TMPS = this.w_TMPS +' gruppo="'+alltrim(_Curs_GSCG_BEB_7.VBGRUPPO) +'"'
      this.w_TMPS = this.w_TMPS +' mastro="'+alltrim(_Curs_GSCG_BEB_7.VBMASTRO) +'"'
      this.w_TMPS = this.w_TMPS +' conto="'+alltrim(_Curs_GSCG_BEB_7.VB_CONTO) +'"'
      this.w_TMPS = this.w_TMPS +' voce="'+alltrim(_Curs_GSCG_BEB_7.VB__VOCE) +'"'
      this.w_IMPORTO = Nvl(_Curs_GSCG_BEB_7.IMPORTO, 0 )
      * --- Se la natura e Costo o ricavo diviene per Basilea 2
      *     cambio di segno l'importo (in ad hoc tutti gli importi di bilancio sono mediati
      *     dalla loro sezione di bilancio) in basilea tutto � espresso come DARE-AVERE
      if _Curs_GSCG_BEB_7.VBNATURA="P" Or _Curs_GSCG_BEB_7.VBNATURA="R"
        this.w_IMPORTO =  - this.w_IMPORTO
      endif
      * --- Converto l'importo determinato nella valuta richiesta..
      *     (moltiplico per il cambio)
      if this.w_IMPORTO<>0 And this.oParentObject.w_MONETE="a" And this.oParentObject.w_CAMVAL<>1
        this.w_IMPORTO = this.w_IMPORTO * this.oParentObject.w_CAMVAL
      endif
      * --- L'importo va arrotondato all'unit� di euro...
      this.w_IMPORTO = cp_Round( this.w_IMPORTO , 0 )
      this.w_TMPS = this.w_TMPS +' importo="'+ alltrim(Str(this.w_IMPORTO)) +'"'
      FWRITE(this.w_HANDLEFILE,"<"+ this.w_TMPS + "/>"+this.w_LINEFEED)
        select _Curs_GSCG_BEB_7
        continue
      enddo
      use
    endif
    FWRITE(this.w_HANDLEFILE,"</esercizio>"+this.w_LINEFEED)
  endproc


  procedure Pag7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
     
 CREATE CURSOR BIO (Codice C(15),Descrizione C(120),Importo C(18),Sezione C(18),Voce C(20),DataIniziale C(10),DataFinale C(10))
    * --- Inserisco Record  di Testata
    this.w_CODAZI = i_CODAZI
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZCODOLT,AZRAGAZI,AZCOFAZI"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZCODOLT,AZRAGAZI,AZCOFAZI;
        from (i_cTable) where;
            AZCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODOLT = NVL(cp_ToDate(_read_.AZCODOLT),cp_NullValue(_read_.AZCODOLT))
      this.w_DESCRI = NVL(cp_ToDate(_read_.AZRAGAZI),cp_NullValue(_read_.AZRAGAZI))
      this.w_CODFIS = NVL(cp_ToDate(_read_.AZCOFAZI),cp_NullValue(_read_.AZCOFAZI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_CODICE = this.w_CODAZI
    this.w_COLUMN1 = "ANNO/N�"
    this.w_COLUMN2 = this.oParentObject.w_ESE
    this.w_CODVOC = Alltrim(Str(this.w_CODOLT))
    this.w_INIESE = dtoc(this.oParentObject.w_DATINIESE)
    this.w_FINESE = dtoc(this.oParentObject.w_DATFINESE)
    * --- Inserisco primo record di testata
* --- Area Manuale = INSERT
* --- gscg_beb
INSERT INTO BIO ;
(Codice,Descrizione,Importo,Sezione,Voce,DataIniziale,DataFinale) Values ;
(This.w_CODICE,This.w_DESCRI,This.w_COLUMN1,This.w_COLUMN2,;
This.w_CODVOC,This.w_INIESE,This.w_FINESE)
* --- Fine Area Manuale
    this.w_CODICE = "           C.F."
    this.w_DESCRI = this.w_CODFIS
    this.w_COLUMN1 = ""
    this.w_COLUMN2 = ""
    this.w_CODVOC = ""
    this.w_INIESE = ""
    this.w_FINESE = ""
    * --- Inserisco secondo record di testata
* --- Area Manuale = INSERT
* --- gscg_beb
INSERT INTO BIO ;
(Codice,Descrizione,Importo,Sezione,Voce,DataIniziale,DataFinale) Values ;
(This.w_CODICE,This.w_DESCRI,This.w_COLUMN1,This.w_COLUMN2,;
This.w_CODVOC,This.w_INIESE,This.w_FINESE)
* --- Fine Area Manuale
    this.w_CODICE = "C.SOTT."
    this.w_DESCRI = "DESCRIZIONE"
    this.w_COLUMN1 = "SALDO"
    this.w_COLUMN2 = "D\A"
    this.w_CODVOC = "Codice B&O"
    this.w_INIESE = ""
    this.w_FINESE = ""
    * --- Inserisco Terzo record di testata
* --- Area Manuale = INSERT
* --- gscg_beb
INSERT INTO BIO ;
(Codice,Descrizione,Importo,Sezione,Voce,DataIniziale,DataFinale) Values ;
(This.w_CODICE,This.w_DESCRI,This.w_COLUMN1,This.w_COLUMN2,;
This.w_CODVOC,This.w_INIESE,This.w_FINESE)
* --- Fine Area Manuale
    * --- Select from GSCG_BEB_O
    do vq_exec with 'GSCG_BEB_O',this,'_Curs_GSCG_BEB_O','',.f.,.t.
    if used('_Curs_GSCG_BEB_O')
      select _Curs_GSCG_BEB_O
      locate for 1=1
      do while not(eof())
      * --- Converto l'importo determinato nella valuta richiesta..
      *     (moltiplico per il cambio)
      this.w_IMPORTO = Nvl(_Curs_GSCG_BEB_O.IMPORTO, 0 )
      * --- Se la natura e Costo o ricavo diviene per Basilea 2
      *     cambio di segno l'importo (in ad hoc tutti gli importi di bilancio sono mediati
      *     dalla loro sezione di bilancio) in basilea tutto � espresso come DARE-AVERE
      if this.w_IMPORTO<>0 And this.oParentObject.w_MONETE="a" And this.oParentObject.w_CAMVAL<>1
        this.w_IMPORTO = this.w_IMPORTO * this.oParentObject.w_CAMVAL
      endif
      this.w_CODICE = _Curs_GSCG_BEB_O.CODICE
      this.w_DESCRI = _Curs_GSCG_BEB_O.VBDESCRI
      do case
        case this.oParentObject.w_ARROT="S"
          * --- Arrotondo
          this.w_COLUMN1 = ALLTRIM(STR(Abs( cp_ROUND(this.w_IMPORTO,0))))
        case this.oParentObject.w_ARROT="T"
          * --- Tronco
          this.w_IMPARRO = 0.4999
          this.w_COLUMN1 = ALLTRIM(STR(Abs( cp_ROUND(abs(this.w_IMPORTO)-this.w_IMPARRO,0))))
        otherwise
          * --- Importi originali
          this.w_COLUMN1 = ALLTRIM(STR(Abs(this.w_IMPORTO),18,2))
      endcase
      this.w_COLUMN2 = IIF(this.w_IMPORTO>0,"D","A")
      this.w_CODVOC = _Curs_GSCG_BEB_O.VB__VOCE
      this.w_INIESE = ""
      this.w_FINESE = ""
* --- Area Manuale = INSERT
* --- gscg_beb
INSERT INTO BIO ;
(Codice,Descrizione,Importo,Sezione,Voce,DataIniziale,DataFinale) Values ;
(This.w_CODICE,This.w_DESCRI,This.w_COLUMN1,This.w_COLUMN2,;
This.w_CODVOC,This.w_INIESE,This.w_FINESE)
* --- Fine Area Manuale
        select _Curs_GSCG_BEB_O
        continue
      enddo
      use
    endif
    * --- Genero FIle
     
 Select * From BIO into cursor __TMP__
    if reccount("__TMP__")>3
      if CP_DBTYPE="DB2" or reccount("__TMP__")>16000
         
 copy to (this.w_FILE_XML) TYPE FOX2X
      else
         
 copy to (this.w_FILE_XML) TYPE XL5
      endif
      cp_chprn(this.w_file_xml)
      this.w_OKGEN = 1
    endif
    if Used("__TMP__")
       
 Select __TMP__ 
 Use
    endif
    if Used("BIO")
       
 Select BIO 
 Use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='*TMPPNT_MAST'
    this.cWorkTables[2]='*TMP_MAST'
    this.cWorkTables[3]='*TMPMOVIMAST'
    this.cWorkTables[4]='AZIENDA'
    this.cWorkTables[5]='ESERCIZI'
    this.cWorkTables[6]='*ZOOMPART'
    return(this.OpenAllTables(6))

  proc CloseCursors()
    if used('_Curs_GSCG_BEB_7')
      use in _Curs_GSCG_BEB_7
    endif
    if used('_Curs_GSCG_BEB_8')
      use in _Curs_GSCG_BEB_8
    endif
    if used('_Curs_GSCG_BEB_7')
      use in _Curs_GSCG_BEB_7
    endif
    if used('_Curs_GSCG_BEB_O')
      use in _Curs_GSCG_BEB_O
    endif
    if used('_Curs_TMPPNT_MAST')
      use in _Curs_TMPPNT_MAST
    endif
    if used('_Curs_TMPPNT_MAST')
      use in _Curs_TMPPNT_MAST
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- gscg_beb
    * --- Pulizia caratteri non validi Xml
    * --- Basilea2 Accetta solo i caratteri [a..z]+[A..Z]+[0..9]+[',",-,_,(,)]
    * --- Se esiste un carattere al di fuori di questi � sostituito con un
    * --- _
    FUNCTION convertXML_Basilea2
    PARAMETERS sXml
    Local n_i,nTmp
    * Rimuovo gli spazi iniziale e finali, non permessi da Basilea2
    sXml=Alltrim(sXml)
    For n_i=1 to Len( sXml )
     nTmp=Asc(SubStr(sXml,n_i,1))
     * a-z, A-Z, 0-9,", ',(,),_,-, SPAZIO
     If not((nTmp>=65 And nTmp<=90)or(nTmp>=97 And nTmp<=122)Or(nTmp>=48 And nTmp<=57)Or nTmp=34 Or nTmp=39;
        Or nTmp=40 Or nTmp=41 Or nTmp=45 Or nTmp=95 Or nTmp=32)
        sXml=Strtran( sXml, SubStr(sXml,n_i,1) , '_' )
     Endif
    Endfor
    RETURN sXml 
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
