* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_svp                                                        *
*              Stampa verifica plafond                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-07-18                                                      *
* Last revis.: 2012-10-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_svp",oParentObject))

* --- Class definition
define class tgscg_svp as StdForm
  Top    = 55
  Left   = 71

  * --- Standard Properties
  Width  = 561
  Height = 286
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-10-02"
  HelpContextID=227924631
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=14

  * --- Constant Properties
  _IDX = 0
  AZIENDA_IDX = 0
  VALUTE_IDX = 0
  ATTIMAST_IDX = 0
  ATTIDETT_IDX = 0
  PRO_RATA_IDX = 0
  DAT_IVAN_IDX = 0
  cPrg = "gscg_svp"
  cComment = "Stampa verifica plafond"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODATT = space(10)
  w_MOBPLA = space(1)
  w_VALPLA = space(3)
  w_DECTOT = 0
  w_CALCPICT = space(10)
  w_MESEINI = 0
  o_MESEINI = 0
  w_MESEFIN = 0
  o_MESEFIN = 0
  w_DESCRI = space(35)
  w_DESATT = space(35)
  w_DESVAL = space(35)
  w_DESCRIF = space(35)
  w_MESESTAMPATO = 0
  w_MESESTAMPATOP = 0
  w_ANNO = space(4)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_svpPag1","gscg_svp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMESEINI_1_6
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='ATTIMAST'
    this.cWorkTables[4]='ATTIDETT'
    this.cWorkTables[5]='PRO_RATA'
    this.cWorkTables[6]='DAT_IVAN'
    return(this.OpenAllTables(6))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODATT=space(10)
      .w_MOBPLA=space(1)
      .w_VALPLA=space(3)
      .w_DECTOT=0
      .w_CALCPICT=space(10)
      .w_MESEINI=0
      .w_MESEFIN=0
      .w_DESCRI=space(35)
      .w_DESATT=space(35)
      .w_DESVAL=space(35)
      .w_DESCRIF=space(35)
      .w_MESESTAMPATO=0
      .w_MESESTAMPATOP=0
      .w_ANNO=space(4)
        .w_CODATT = i_codazi
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODATT))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,3,.f.)
        if not(empty(.w_VALPLA))
          .link_1_3('Full')
        endif
          .DoRTCalc(4,4,.f.)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
      .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
          .DoRTCalc(6,13,.f.)
        .w_ANNO = STR(YEAR(i_DATSYS),4,0)
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_ANNO))
          .link_1_26('Full')
        endif
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,2,.t.)
          .link_1_3('Full')
        .DoRTCalc(4,4,.t.)
            .w_CALCPICT = DEFPIC(.w_DECTOT)
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
        if .o_MESEINI<>.w_MESEINI
          .Calculate_UUDLMEQCPJ()
        endif
        if .o_MESEFIN<>.w_MESEFIN
          .Calculate_ETMFXPMJJJ()
        endif
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(6,14,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
    endwith
  return

  proc Calculate_UUDLMEQCPJ()
    with this
          * --- Modifica mese finale e descrizione mese
          .w_MESEFIN = iif(.w_MESEINI>.w_MESEFIN,.w_MESEINI,.w_MESEFIN)
          .w_DESCRI = IIF(.w_MESEINI=0,' ',g_MESE[.w_MESEINI])
    endwith
  endproc
  proc Calculate_ETMFXPMJJJ()
    with this
          * --- Descrizione mese finale
          .w_DESCRIF = IIF(.w_MESEFIN=0,' ',g_MESE[.w_MESEFIN])
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_9.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_23.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODATT
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODATT)
            select AZCODAZI,AZRAGAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATT = NVL(_Link_.AZCODAZI,space(10))
      this.w_DESATT = NVL(_Link_.AZRAGAZI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODATT = space(10)
      endif
      this.w_DESATT = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALPLA
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALPLA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALPLA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT,VADESVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALPLA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALPLA)
            select VACODVAL,VADECTOT,VADESVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALPLA = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
      this.w_DESVAL = NVL(_Link_.VADESVAL,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_VALPLA = space(3)
      endif
      this.w_DECTOT = 0
      this.w_DESVAL = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALPLA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANNO
  func Link_1_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DAT_IVAN_IDX,3]
    i_lTable = "DAT_IVAN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DAT_IVAN_IDX,2], .t., this.DAT_IVAN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DAT_IVAN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANNO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DAT_IVAN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IA__ANNO like "+cp_ToStrODBC(trim(this.w_ANNO)+"%");
                   +" and IACODAZI="+cp_ToStrODBC(this.w_CODATT);

          i_ret=cp_SQL(i_nConn,"select IACODAZI,IA__ANNO,IAVALPLA,IAPLAMOB";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IACODAZI,IA__ANNO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IACODAZI',this.w_CODATT;
                     ,'IA__ANNO',trim(this.w_ANNO))
          select IACODAZI,IA__ANNO,IAVALPLA,IAPLAMOB;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IACODAZI,IA__ANNO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANNO)==trim(_Link_.IA__ANNO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANNO) and !this.bDontReportError
            deferred_cp_zoom('DAT_IVAN','*','IACODAZI,IA__ANNO',cp_AbsName(oSource.parent,'oANNO_1_26'),i_cWhere,'',"Elenco dati IVA",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODATT<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IACODAZI,IA__ANNO,IAVALPLA,IAPLAMOB";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select IACODAZI,IA__ANNO,IAVALPLA,IAPLAMOB;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Anno di selezione non definito nei parametri IVA")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IACODAZI,IA__ANNO,IAVALPLA,IAPLAMOB";
                     +" from "+i_cTable+" "+i_lTable+" where IA__ANNO="+cp_ToStrODBC(oSource.xKey(2));
                     +" and IACODAZI="+cp_ToStrODBC(this.w_CODATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IACODAZI',oSource.xKey(1);
                       ,'IA__ANNO',oSource.xKey(2))
            select IACODAZI,IA__ANNO,IAVALPLA,IAPLAMOB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANNO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IACODAZI,IA__ANNO,IAVALPLA,IAPLAMOB";
                   +" from "+i_cTable+" "+i_lTable+" where IA__ANNO="+cp_ToStrODBC(this.w_ANNO);
                   +" and IACODAZI="+cp_ToStrODBC(this.w_CODATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IACODAZI',this.w_CODATT;
                       ,'IA__ANNO',this.w_ANNO)
            select IACODAZI,IA__ANNO,IAVALPLA,IAPLAMOB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANNO = NVL(_Link_.IA__ANNO,space(4))
      this.w_VALPLA = NVL(_Link_.IAVALPLA,space(3))
      this.w_MOBPLA = NVL(_Link_.IAPLAMOB,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ANNO = space(4)
      endif
      this.w_VALPLA = space(3)
      this.w_MOBPLA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=Val(.w_ANNO)>1900
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Anno di selezione non definito nei parametri IVA")
        endif
        this.w_ANNO = space(4)
        this.w_VALPLA = space(3)
        this.w_MOBPLA = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DAT_IVAN_IDX,2])+'\'+cp_ToStr(_Link_.IACODAZI,1)+'\'+cp_ToStr(_Link_.IA__ANNO,1)
      cp_ShowWarn(i_cKey,this.DAT_IVAN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANNO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODATT_1_1.value==this.w_CODATT)
      this.oPgFrm.Page1.oPag.oCODATT_1_1.value=this.w_CODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oVALPLA_1_3.value==this.w_VALPLA)
      this.oPgFrm.Page1.oPag.oVALPLA_1_3.value=this.w_VALPLA
    endif
    if not(this.oPgFrm.Page1.oPag.oMESEINI_1_6.value==this.w_MESEINI)
      this.oPgFrm.Page1.oPag.oMESEINI_1_6.value=this.w_MESEINI
    endif
    if not(this.oPgFrm.Page1.oPag.oMESEFIN_1_7.value==this.w_MESEFIN)
      this.oPgFrm.Page1.oPag.oMESEFIN_1_7.value=this.w_MESEFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_8.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_8.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESATT_1_12.value==this.w_DESATT)
      this.oPgFrm.Page1.oPag.oDESATT_1_12.value=this.w_DESATT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESVAL_1_14.value==this.w_DESVAL)
      this.oPgFrm.Page1.oPag.oDESVAL_1_14.value=this.w_DESVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRIF_1_19.value==this.w_DESCRIF)
      this.oPgFrm.Page1.oPag.oDESCRIF_1_19.value=this.w_DESCRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oANNO_1_26.value==this.w_ANNO)
      this.oPgFrm.Page1.oPag.oANNO_1_26.value=this.w_ANNO
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CODATT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODATT_1_1.SetFocus()
            i_bnoObbl = !empty(.w_CODATT)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_MESEINI)) or not(.w_MESEINI<13))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMESEINI_1_6.SetFocus()
            i_bnoObbl = !empty(.w_MESEINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Mese di inizio stampa incongruente")
          case   ((empty(.w_MESEFIN)) or not(.w_MESEFIN<13 And .w_MESEINI<=.w_MESEFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMESEFIN_1_7.SetFocus()
            i_bnoObbl = !empty(.w_MESEFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Mese di fine stampa minore del mese di inizio stampa oppure incongruente")
          case   (empty(.w_MESESTAMPATO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMESESTAMPATO_1_22.SetFocus()
            i_bnoObbl = !empty(.w_MESESTAMPATO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MESESTAMPATOP))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMESESTAMPATOP_1_25.SetFocus()
            i_bnoObbl = !empty(.w_MESESTAMPATOP)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_ANNO)) or not(Val(.w_ANNO)>1900))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANNO_1_26.SetFocus()
            i_bnoObbl = !empty(.w_ANNO)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Anno di selezione non definito nei parametri IVA")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MESEINI = this.w_MESEINI
    this.o_MESEFIN = this.w_MESEFIN
    return

enddefine

* --- Define pages as container
define class tgscg_svpPag1 as StdContainer
  Width  = 557
  height = 286
  stdWidth  = 557
  stdheight = 286
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODATT_1_1 as StdField with uid="HNTIYWGLOC",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODATT", cQueryName = "CODATT",enabled=.f.,;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice azienda",;
    HelpContextID = 149197274,;
   bGlobalFont=.t.,;
    Height=21, Width=53, Left=115, Top=21, InputMask=replicate('X',10), cLinkFile="AZIENDA", cZoomOnZoom="GSAR_AZI", oKey_1_1="AZCODAZI", oKey_1_2="this.w_CODATT"

  func oCODATT_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_ANNO)
        bRes2=.link_1_26('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oVALPLA_1_3 as StdField with uid="VJJPHPYXPP",rtseq=3,rtrep=.f.,;
    cFormVar = "w_VALPLA", cQueryName = "VALPLA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Valuta riferimento plafond",;
    HelpContextID = 206905002,;
   bGlobalFont=.t.,;
    Height=21, Width=52, Left=232, Top=62, InputMask=replicate('X',3), cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_VALPLA"

  func oVALPLA_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oMESEINI_1_6 as StdField with uid="XIKMKDHMFY",rtseq=6,rtrep=.f.,;
    cFormVar = "w_MESEINI", cQueryName = "MESEINI",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Mese di inizio stampa incongruente",;
    ToolTipText = "Mese di inizio stampa dei dati del plafond",;
    HelpContextID = 7361734,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=115, Top=103, cSayPict='"99"', cGetPict='"99"'

  func oMESEINI_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MESEINI<13)
    endwith
    return bRes
  endfunc

  add object oMESEFIN_1_7 as StdField with uid="JYKIXLADQA",rtseq=7,rtrep=.f.,;
    cFormVar = "w_MESEFIN", cQueryName = "MESEFIN",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Mese di fine stampa minore del mese di inizio stampa oppure incongruente",;
    ToolTipText = "Mese di fine stampa dei dati del plafond",;
    HelpContextID = 79670074,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=115, Top=138, cSayPict='"99"', cGetPict='"99"'

  func oMESEFIN_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MESEFIN<13 And .w_MESEINI<=.w_MESEFIN)
    endwith
    return bRes
  endfunc

  add object oDESCRI_1_8 as StdField with uid="YJLNDUMPCM",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 67218378,;
   bGlobalFont=.t.,;
    Height=21, Width=278, Left=171, Top=103, InputMask=replicate('X',35)


  add object oObj_1_9 as cp_runprogram with uid="XQDCEAZWUJ",left=5, top=292, width=224,height=23,;
    caption='GSCG_BPW',;
   bGlobalFont=.t.,;
    prg="GSCG_BPW('A')",;
    cEvent = "Init,w_ANNO Changed",;
    nPag=1;
    , ToolTipText = "Inizializza i dati sulla maschera";
    , HelpContextID = 170827075

  add object oDESATT_1_12 as StdField with uid="ASMPFLVDHQ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESATT", cQueryName = "DESATT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 149138378,;
   bGlobalFont=.t.,;
    Height=21, Width=331, Left=171, Top=21, InputMask=replicate('X',35)

  add object oDESVAL_1_14 as StdField with uid="PWJIOVAEJF",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DESVAL", cQueryName = "DESVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 33467338,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=288, Top=62, InputMask=replicate('X',35)


  add object oBtn_1_16 as StdButton with uid="FXECVDYSXT",left=447, top=236, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per avviare la stampa";
    , HelpContextID = 253791210;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      with this.Parent.oContained
        GSCG_BPW(this.Parent.oContained,"S")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_16.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty( .w_CODATT ) And Not Empty( .w_ANNO ) And Not Empty( .w_MESEINI ) And Not Empty( .w_MESEFIN))
      endwith
    endif
  endfunc


  add object oBtn_1_17 as StdButton with uid="LTXXODMRFM",left=498, top=236, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 235242054;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCRIF_1_19 as StdField with uid="MLQBAZJMLQ",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESCRIF", cQueryName = "DESCRIF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 201217078,;
   bGlobalFont=.t.,;
    Height=21, Width=278, Left=171, Top=138, InputMask=replicate('X',35)


  add object oObj_1_23 as cp_outputCombo with uid="ZLERXIVPWT",left=115, top=191, width=432,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 130948634

  add object oANNO_1_26 as StdField with uid="BGOLHXOVAM",rtseq=14,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",nZero=4,;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Anno di selezione non definito nei parametri IVA",;
    ToolTipText = "Anno di selezione dei documenti da elaborare",;
    HelpContextID = 233442566,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=115, Top=62, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="DAT_IVAN", oKey_1_1="IACODAZI", oKey_1_2="this.w_CODATT", oKey_2_1="IA__ANNO", oKey_2_2="this.w_ANNO"

  func oANNO_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_26('Part',this)
    endwith
    return bRes
  endfunc

  proc oANNO_1_26.ecpDrop(oSource)
    this.Parent.oContained.link_1_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANNO_1_26.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DAT_IVAN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"IACODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"IACODAZI="+cp_ToStr(this.Parent.oContained.w_CODATT)
    endif
    do cp_zoom with 'DAT_IVAN','*','IACODAZI,IA__ANNO',cp_AbsName(this.parent,'oANNO_1_26'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco dati IVA",'',this.parent.oContained
  endproc

  add object oStr_1_10 as StdString with uid="WHXZRAYIPF",Visible=.t., Left=49, Top=106,;
    Alignment=1, Width=54, Height=18,;
    Caption="Da mese:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="FWSNNYLMXO",Visible=.t., Left=50, Top=25,;
    Alignment=1, Width=53, Height=18,;
    Caption="Azienda:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="VCGPNMZWZO",Visible=.t., Left=176, Top=66,;
    Alignment=1, Width=50, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="NWLJYNQMIL",Visible=.t., Left=60, Top=66,;
    Alignment=1, Width=43, Height=15,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="VKBPDMCQIQ",Visible=.t., Left=49, Top=141,;
    Alignment=1, Width=54, Height=18,;
    Caption="A mese:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="EUTCMWXAEH",Visible=.t., Left=6, Top=194,;
    Alignment=1, Width=97, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_svp','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
