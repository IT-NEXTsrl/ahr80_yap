* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gssr_bce                                                        *
*              Storicizzazione dettaglio cespiti                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_24]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-07-26                                                      *
* Last revis.: 2004-07-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPARAM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgssr_bce",oParentObject,m.pPARAM)
return(i_retval)

define class tgssr_bce as StdBatch
  * --- Local variables
  pPARAM = space(4)
  w_nc = 0
  * --- WorkFile variables
  TMPTPNT_CESP_idx=0
  ZOOMPART_idx=0
  PNT_CESP_idx=0
  TMPTPNT_IVA_idx=0
  PNTSCESP_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eseguito da GSSR_BSP  
    do case
      case this.pPARAM="INSE"
        * --- Insert into PNTSCESP
        i_nConn=i_TableProp[this.PNTSCESP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PNTSCESP_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_cTempTable=cp_SetAzi(i_TableProp[this.TMPTPNT_CESP_idx,2])
          i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.PNTSCESP_idx)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        this.oParentObject.w_rows = i_rows
      case this.pPARAM="DELE"
        * --- Delete from TMPTPNT_CESP
        i_nConn=i_TableProp[this.TMPTPNT_CESP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPTPNT_CESP_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex
          declare i_aIndex[1]
          i_cQueryTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2])
          i_cTempTable=cp_GetTempTableName(i_nConn)
          i_aIndex[1]='ACSERIAL'
          cp_CreateTempTable(i_nConn,i_cTempTable,"distinct PTSERIAL AS ACSERIAL "," from "+i_cQueryTable+" where PTROWORD>0",.f.,@i_aIndex)
          i_cQueryTable=i_cTempTable
          i_cWhere=i_cTable+".ACSERIAL = "+i_cQueryTable+".ACSERIAL";
        
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                +" where exists( select 1 from "+i_cQueryTable+" where ";
                +""+i_cTable+".ACSERIAL = "+i_cQueryTable+".ACSERIAL";
                +")")
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      case this.pPARAM="DELM"
        * --- Delete from PNT_CESP
        i_nConn=i_TableProp[this.PNT_CESP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PNT_CESP_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex
          declare i_aIndex[1]
          i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPTPNT_CESP_idx,2])
          i_cWhere=i_cTable+".ACSERIAL = "+i_cQueryTable+".ACSERIAL";
                +" and "+i_cTable+".ACMOVCES = "+i_cQueryTable+".ACMOVCES";
                +" and "+i_cTable+".ACTIPASS = "+i_cQueryTable+".ACTIPASS";
        
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                +" where exists( select 1 from "+i_cQueryTable+" where ";
                +""+i_cTable+".ACSERIAL = "+i_cQueryTable+".ACSERIAL";
                +" and "+i_cTable+".ACMOVCES = "+i_cQueryTable+".ACMOVCES";
                +" and "+i_cTable+".ACTIPASS = "+i_cQueryTable+".ACTIPASS";
                +")")
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      case this.pPARAM="DROP"
        * --- Drop temporary table TMPTPNT_CESP
        i_nIdx=cp_GetTableDefIdx('TMPTPNT_CESP')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPTPNT_CESP')
        endif
    endcase
  endproc


  proc Init(oParentObject,pPARAM)
    this.pPARAM=pPARAM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='TMPTPNT_CESP'
    this.cWorkTables[2]='ZOOMPART'
    this.cWorkTables[3]='PNT_CESP'
    this.cWorkTables[4]='TMPTPNT_IVA'
    this.cWorkTables[5]='PNTSCESP'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPARAM"
endproc
