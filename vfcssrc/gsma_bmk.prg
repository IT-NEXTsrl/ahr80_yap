* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bmk                                                        *
*              Mov.magazzino controlli finali                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_139]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-18                                                      *
* Last revis.: 2015-03-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bmk",oParentObject)
return(i_retval)

define class tgsma_bmk as StdBatch
  * --- Local variables
  w_DATG = ctod("  /  /  ")
  w_OK = .f.
  w_DATB = ctod("  /  /  ")
  w_DATG1 = ctod("  /  /  ")
  w_MESS = space(10)
  w_APPO = space(10)
  w_DATG2 = ctod("  /  /  ")
  w_CFUNC = space(10)
  w_DATB1 = ctod("  /  /  ")
  w_QTAP = 0
  w_DATB2 = ctod("  /  /  ")
  w_QTAR = 0
  w_CAUCOL = space(5)
  w_FLDISPL = .f.
  w_CODMAG = space(5)
  w_KEYSAL = space(20)
  w_CODMAT = space(5)
  w_RECO = 0
  w_CODUBI = space(20)
  w_CODUB2 = space(20)
  w_COMAG = space(5)
  w_COART = space(20)
  w_CODLOT = space(20)
  w_FLUBICL = space(1)
  w_F2UBICL = space(1)
  w_FLSTATL = space(1)
  w_FLLOTTL = space(1)
  w_FLAGLO = space(1)
  w_FLAG2LO = space(1)
  w_QTAESI = 0
  w_QTAUM1 = 0
  w_OLDQTA = 0
  w_FLDEL = .f.
  w_NR = space(4)
  w_TESDIS = .f.
  w_LFLRISE = space(1)
  w_SUQTRPER = 0
  w_SUQTAPER = 0
  w_PADRE = .NULL.
  w_OLDCODLOT = space(20)
  w_OLDFLSTAT = space(1)
  w_OLDKEYSAL = space(20)
  w_DMAGCAR = space(5)
  w_DMAGSCA = space(5)
  w_SERIAL = space(10)
  w_NUMRIF = 0
  * --- WorkFile variables
  SALDIART_idx=0
  GIORMAGA_idx=0
  LOTTIART_idx=0
  SALDILOT_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Movimenti di Magazzino, Controlli Finali (da GSMA_MVM)
    * --- Inibisce la Transazione se:
    * --- - Gia' stampata sul Giornale di Magazzino o Stampa G.M in Corso
    * --- Variabili Lotti
    * --- Legge Righe, Temporaneo Movimenti Magazzino
    this.w_PADRE = This.oParentObject
    this.w_OK = .T.
    this.w_FLDEL = .F.
    this.w_MESS = ah_msgformat("Transazione abbandonata")
    this.w_CFUNC = this.w_PADRE.cFunction
    if this.w_OK AND (this.w_CFUNC <> "Query" OR (g_PERLOT="S" OR g_PERUBI="S"))
      this.w_PADRE.MarkPos()     
      if this.w_OK
        ah_Msg("Controlli finali lotti / ubicazioni")
        * --- Test se cancellata
        this.w_FLDEL = this.w_CFUNC="Query"
        this.w_PADRE.FirstRow()     
        * --- Primo giro sulle righe non cancellate...
        do while Not this.w_PADRE.Eof_Trs()
          this.w_PADRE.SetRow()     
          this.w_FLLOTTL = NVL(this.oParentObject.w_FLLOTT," ")
          * --- Scorro le righe piene, cancellate (se non in salvataggio), o aggiunte e modificate
          *     se non cancellate
          if this.w_PADRE.FullRow() And ( this.w_PADRE.RowStatus() $ "A-U" Or this.w_FLDEL)
            * --- Ho almeno una riga in append\modificata 
            if (g_PERLOT="S" AND this.oParentObject.w_FLLOTT $ "SC") OR (g_PERUBI="S") AND this.w_OK
              this.Page_2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
          * --- Se tutto ok passo alla prossima riga altrimenti esco...
          if this.w_OK
            this.w_PADRE.NextRow()     
          else
            Exit
          endif
        enddo
        if this.w_OK And this.w_CFUNC<>"Query"
          this.w_PADRE.FirstRowDel()     
          * --- Test Se riga Eliminata
          this.w_FLDEL = .T.
          do while Not this.w_PADRE.Eof_Trs()
            this.w_PADRE.SetRow()     
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Se tutto ok passo alla prossima riga altrimenti esco...
            if this.w_OK
              this.w_PADRE.NextRowDel()     
            else
              Exit
            endif
          enddo
        endif
      endif
      this.w_PADRE.Repos(.T.)     
      if (g_PERLOT="S" OR g_PERUBI="S") AND this.w_OK
        this.w_OK = GSAR_BLK( This , "", This.oParentObject )
      endif
      if This.oParentObject.cFunction<>"Query" And g_MATR="S" And this.oParentObject.w_MMDATREG>=nvl(g_DATMAT,cp_CharToDate("  /  /    "))
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    WAIT CLEAR
    this.bUpdateParentObject=.f.
    if this.w_OK And g_PERDIS="S" 
      * --- Verifico per gli articoli con check disponibilit� senza conferma
      *     (controllo gia svolto in GSMA_BCK sul transitorio). Replicato
      *     sotto transazione per assicurarsi che il check sia sicuro
      this.w_OK = GSAR_BDA( This , "", This.oParentObject )
    endif
    if Not this.w_OK
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo Disponibilit� Lotti\Ubicazioni
    this.w_CAUCOL = NVL(this.oParentObject.w_MMCAUCOL, " ")
    this.w_CODMAG = NVL(this.oParentObject.w_MMCODMAG, " ")
    this.w_CODMAT = NVL(this.oParentObject.w_MMCODMAT, " ")
    this.w_KEYSAL = NVL(this.oParentObject.w_MMKEYSAL, SPACE(20))
    this.w_FLDISPL = NVL(this.oParentObject.w_FLDISP, " ")
    this.w_FLAGLO = NVL(this.oParentObject.w_MMFLLOTT," ")
    this.w_FLAG2LO = NVL(this.oParentObject.w_MMF2LOTT," ")
    this.w_FLSTATL = NVL(this.oParentObject.w_FLSTAT," ")
    this.w_CODLOT = NVL(this.oParentObject.w_MMCODLOT,SPACE(20))
    * --- Recupera dal transitorio il vecchio valore del codice lotto e codice articolo eventualmente modificati per aggiornare lo status
    this.w_OLDCODLOT = NVL(this.w_PADRE.GET("MMCODLOT"), this.w_CODLOT)
    this.w_OLDKEYSAL = NVL(this.w_PADRE.GET("MMKEYSAL"), this.w_COART)
    this.w_COART = NVL(this.oParentObject.w_MMCODART,SPACE(20))
    this.w_QTAUM1 = NVL(this.oParentObject.w_MMQTAUM1,0)
    this.w_CODUBI = NVL(this.oParentObject.w_MMCODUBI,SPACE(20))
    this.w_CODUB2 = NVL(this.oParentObject.w_MMCODUB2,SPACE(20))
    this.w_COMAG = NVL(this.oParentObject.w_MMCODMAG,SPACE(5))
    this.w_OLDQTA = NVL( this.w_PADRE. Get( "MMQTAUM1" ) ,0)
    this.w_FLUBICL = NVL(this.oParentObject.w_FLUBIC," ")
    this.w_F2UBICL = NVL(this.oParentObject.w_F2UBIC," ")
    this.w_NR = ALLTRIM(STR(this.oParentObject.w_CPROWORD))
    this.w_LFLRISE = NVL(this.oParentObject.w_MMFLRISE," ")
    if EMPTY(this.w_CODLOT) AND (this.w_FLAGLO $ "+-" OR this.w_FLAG2LO $ "+-") AND this.w_QTAUM1<>0 AND g_PERLOT="S" AND this.oParentObject.w_FLLOTT $ "SC" AND Not this.w_FLDEL
      * --- Controllo signicatifivit� Codice Lotto
      this.w_MESS = ah_msgformat("Riga movimento: %1%0Articolo: %2%0Inserire codice lotto", this.w_NR,ALLTRIM(this.w_COART) )
      this.w_OK = .F.
    endif
    if g_PERUBI="S" AND Not this.w_FLDEL
      if EMPTY(this.w_CODUBI) AND NOT EMPTY(this.w_COMAG) AND this.w_FLUBICL="S" AND this.w_FLAGLO $ "+-" AND this.w_OK
        * --- Controllo significativit� Codice Ubicazione
        this.w_MESS = ah_msgformat("Riga movimento: %1%0Magazzino: %2%0Inserire codice ubicazione", this.w_NR, alltrim(this.w_CODMAG) )
        this.w_OK = .F.
      endif
      if EMPTY(this.w_CODUB2) AND NOT EMPTY(this.w_CODMAT) AND this.w_F2UBICL="S" AND this.w_FLAG2LO $ "+-" AND this.w_OK
        * --- Controllo significativit� Codice Ubicazione Collegata
        this.w_MESS = ah_msgformat("Riga movimento: %1%0Magazzino: %2%0Inserire codice ubicazione collegata", this.w_NR, alltrim(this.w_CODMAT) )
        this.w_OK = .F.
      endif
    endif
    if this.w_OK And (((g_PERLOT="S" AND this.oParentObject.w_FLLOTT $ "SC" ) OR g_PERUBI="S") AND this.oParentObject.w_TIPRIG $ "RM" ) 
      * --- Aggiorno Status Lotto
      do case
        case this.oParentObject.w_FLSTAT="E"
          if (((this.w_QTAUM1<this.w_OLDQTA OR this.w_FLDEL) AND (this.w_FLAGLO="-" OR this.w_FLAG2LO="-")) OR (this.w_QTAUM1>this.w_OLDQTA AND (this.w_FLAGLO="+" OR this.w_FLAG2LO="+"))) 
            * --- Aggiorno Status in caso di Modifica della Quantit� o Cancellazione di uno Scarico
            * --- Eseguo  Query per desterminare lo stato del lotto
            this.w_QTAESI = 0
            this.w_TESDIS = .F.
            this.w_SUQTRPER = 0
            this.w_SUQTAPER = 0
            * --- Select from SALDILOT
            i_nConn=i_TableProp[this.SALDILOT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDILOT_idx,2],.t.,this.SALDILOT_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select SUM (SUQTAPER) as SUQTAPER , SUM (SUQTRPER) as SUQTRPER  from "+i_cTable+" SALDILOT ";
                  +" where "+cp_ToStrODBC(this.w_COART)+"=SUCODART AND "+cp_ToStrODBC(this.w_CODLOT)+"=SUCODLOT";
                  +" group by SUCODART,SUCODLOT";
                   ,"_Curs_SALDILOT")
            else
              select SUM (SUQTAPER) as SUQTAPER , SUM (SUQTRPER) as SUQTRPER from (i_cTable);
               where this.w_COART=SUCODART AND this.w_CODLOT=SUCODLOT;
               group by SUCODART,SUCODLOT;
                into cursor _Curs_SALDILOT
            endif
            if used('_Curs_SALDILOT')
              select _Curs_SALDILOT
              locate for 1=1
              do while not(eof())
              this.w_TESDIS = .T.
              this.w_QTAESI = this.w_QTAESI +_Curs_SALDILOT.SUQTAPER - _Curs_SALDILOT.SUQTRPER
                select _Curs_SALDILOT
                continue
              enddo
              use
            endif
            this.w_QTAESI = icase(this.w_FLAGLO="-" OR this.w_FLAG2LO="-",this.w_QTAESI +this.w_OLDQTA - this.w_QTAUM1,this.w_FLAGLO="+" OR this.w_FLAG2LO="+",this.w_QTAESI -this.w_OLDQTA + this.w_QTAUM1)
            if this.w_QTAESI>0 AND this.w_TESDIS AND EMPTY(this.w_LFLRISE)
              * --- Write into LOTTIART
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.LOTTIART_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.LOTTIART_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.LOTTIART_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"LOFLSTAT ="+cp_NullLink(cp_ToStrODBC("D"),'LOTTIART','LOFLSTAT');
                    +i_ccchkf ;
                +" where ";
                    +"LOCODICE = "+cp_ToStrODBC(this.w_CODLOT);
                    +" and LOCODART = "+cp_ToStrODBC(this.w_COART);
                       )
              else
                update (i_cTable) set;
                    LOFLSTAT = "D";
                    &i_ccchkf. ;
                 where;
                    LOCODICE = this.w_CODLOT;
                    and LOCODART = this.w_COART;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='errore scrittura lotti'
                return
              endif
            endif
          else
            if this.w_OLDQTA<>this.w_QTAUM1 AND this.oParentObject.w_DISLOT="S"
              this.w_MESS = ah_msgformat("Disponibilit� lotto: %1 esaurita", ALLTRIM(this.w_CODLOT) )
              this.w_OK = .F.
            endif
          endif
        case this.oParentObject.w_FLSTAT="S"
          if this.w_QTAUM1>this.w_OLDQTA
            this.w_MESS = ah_msgformat("Lotto: %1 sospeso", ALLTRIM(this.w_CODLOT) )
            this.w_OK = .F.
          endif
        otherwise
          * --- Eseguo  Query per desterminare lo stato del lotto
          this.w_QTAESI = 0
          this.w_TESDIS = .F.
          this.w_SUQTRPER = 0
          this.w_SUQTAPER = 0
          * --- Select from SALDILOT
          i_nConn=i_TableProp[this.SALDILOT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDILOT_idx,2],.t.,this.SALDILOT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select SUM (SUQTAPER) as SUQTAPER , SUM (SUQTRPER) as SUQTRPER  from "+i_cTable+" SALDILOT ";
                +" where "+cp_ToStrODBC(this.w_COART)+"=SUCODART AND "+cp_ToStrODBC(this.w_CODLOT)+"=SUCODLOT";
                +" group by SUCODART,SUCODLOT";
                 ,"_Curs_SALDILOT")
          else
            select SUM (SUQTAPER) as SUQTAPER , SUM (SUQTRPER) as SUQTRPER from (i_cTable);
             where this.w_COART=SUCODART AND this.w_CODLOT=SUCODLOT;
             group by SUCODART,SUCODLOT;
              into cursor _Curs_SALDILOT
          endif
          if used('_Curs_SALDILOT')
            select _Curs_SALDILOT
            locate for 1=1
            do while not(eof())
            this.w_TESDIS = .T.
            this.w_QTAESI = this.w_QTAESI +_Curs_SALDILOT.SUQTAPER - _Curs_SALDILOT.SUQTRPER
              select _Curs_SALDILOT
              continue
            enddo
            use
          endif
          if this.w_QTAESI<=0 AND this.w_TESDIS AND EMPTY(this.w_LFLRISE)
            * --- Write into LOTTIART
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.LOTTIART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.LOTTIART_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.LOTTIART_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"LOFLSTAT ="+cp_NullLink(cp_ToStrODBC("E"),'LOTTIART','LOFLSTAT');
                  +i_ccchkf ;
              +" where ";
                  +"LOCODICE = "+cp_ToStrODBC(this.w_CODLOT);
                  +" and LOCODART = "+cp_ToStrODBC(this.w_COART);
                     )
            else
              update (i_cTable) set;
                  LOFLSTAT = "E";
                  &i_ccchkf. ;
               where;
                  LOCODICE = this.w_CODLOT;
                  and LOCODART = this.w_COART;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
      endcase
      if (this.w_CODLOT<>this.w_OLDCODLOT OR this.w_COART<>this.w_OLDKEYSAL) AND this.w_CFUNC="Edit"
        * --- Read from LOTTIART
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.LOTTIART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LOTTIART_idx,2],.t.,this.LOTTIART_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "LOFLSTAT"+;
            " from "+i_cTable+" LOTTIART where ";
                +"LOCODICE = "+cp_ToStrODBC(this.w_OLDCODLOT);
                +" and LOCODART = "+cp_ToStrODBC(this.w_OLDKEYSAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            LOFLSTAT;
            from (i_cTable) where;
                LOCODICE = this.w_OLDCODLOT;
                and LOCODART = this.w_OLDKEYSAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_OLDFLSTAT = NVL(cp_ToDate(_read_.LOFLSTAT),cp_NullValue(_read_.LOFLSTAT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        do case
          case this.w_OLDFLSTAT="E" 
            if (this.w_FLAGLO="-" OR this.w_FLAG2LO="-") OR (this.w_QTAUM1>this.w_OLDQTA AND (this.w_FLAGLO="+" OR this.w_FLAG2LO="+"))
              * --- Aggiorno Status in caso di Modifica della Quantit� o Cancellazione di uno Scarico
              if Not Empty(this.w_OLDCODLOT)
                this.w_QTAESI = 0
                this.w_TESDIS = .F.
                this.w_SUQTRPER = 0
                this.w_SUQTAPER = 0
                * --- Select from SALDILOT
                i_nConn=i_TableProp[this.SALDILOT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.SALDILOT_idx,2],.t.,this.SALDILOT_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select SUM (SUQTAPER) as SUQTAPER , SUM (SUQTRPER) as SUQTRPER  from "+i_cTable+" SALDILOT ";
                      +" where "+cp_ToStrODBC(this.w_OLDKEYSAL)+"=SUCODART AND "+cp_ToStrODBC(this.w_OLDCODLOT)+"=SUCODLOT";
                      +" group by SUCODART,SUCODLOT";
                       ,"_Curs_SALDILOT")
                else
                  select SUM (SUQTAPER) as SUQTAPER , SUM (SUQTRPER) as SUQTRPER from (i_cTable);
                   where this.w_OLDKEYSAL=SUCODART AND this.w_OLDCODLOT=SUCODLOT;
                   group by SUCODART,SUCODLOT;
                    into cursor _Curs_SALDILOT
                endif
                if used('_Curs_SALDILOT')
                  select _Curs_SALDILOT
                  locate for 1=1
                  do while not(eof())
                  this.w_TESDIS = .T.
                  this.w_QTAESI = this.w_QTAESI +_Curs_SALDILOT.SUQTAPER - _Curs_SALDILOT.SUQTRPER
                    select _Curs_SALDILOT
                    continue
                  enddo
                  use
                endif
                this.w_QTAESI = icase(this.w_FLAGLO="-" OR this.w_FLAG2LO="-",this.w_QTAESI +this.w_OLDQTA - this.w_QTAUM1,this.w_FLAGLO="+" OR this.w_FLAG2LO="+",this.w_QTAESI -this.w_OLDQTA + this.w_QTAUM1)
                if this.w_QTAESI>0 AND this.w_TESDIS AND EMPTY(this.w_LFLRISE)
                  * --- Write into LOTTIART
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.LOTTIART_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.LOTTIART_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.LOTTIART_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"LOFLSTAT ="+cp_NullLink(cp_ToStrODBC("D"),'LOTTIART','LOFLSTAT');
                        +i_ccchkf ;
                    +" where ";
                        +"LOCODICE = "+cp_ToStrODBC(this.w_OLDCODLOT);
                        +" and LOCODART = "+cp_ToStrODBC(this.w_OLDKEYSAL);
                           )
                  else
                    update (i_cTable) set;
                        LOFLSTAT = "D";
                        &i_ccchkf. ;
                     where;
                        LOCODICE = this.w_OLDCODLOT;
                        and LOCODART = this.w_OLDKEYSAL;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error='errore scrittura lotti'
                    return
                  endif
                endif
              endif
            endif
          case this.w_OLDFLSTAT="D" 
            * --- Aggiorno Status Recuperando esistenza generale del Lotto
            if this.w_FLAGLO $ "+-" 
              this.w_QTAESI = 0
              this.w_TESDIS = .F.
              this.w_SUQTRPER = 0
              this.w_SUQTAPER = 0
              * --- Select from SALDILOT
              i_nConn=i_TableProp[this.SALDILOT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDILOT_idx,2],.t.,this.SALDILOT_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select SUM (SUQTAPER) as SUQTAPER , SUM (SUQTRPER) as SUQTRPER  from "+i_cTable+" SALDILOT ";
                    +" where "+cp_ToStrODBC(this.w_OLDKEYSAL)+"=SUCODART AND "+cp_ToStrODBC(this.w_OLDCODLOT)+"=SUCODLOT";
                    +" group by SUCODART,SUCODLOT";
                     ,"_Curs_SALDILOT")
              else
                select SUM (SUQTAPER) as SUQTAPER , SUM (SUQTRPER) as SUQTRPER from (i_cTable);
                 where this.w_OLDKEYSAL=SUCODART AND this.w_OLDCODLOT=SUCODLOT;
                 group by SUCODART,SUCODLOT;
                  into cursor _Curs_SALDILOT
              endif
              if used('_Curs_SALDILOT')
                select _Curs_SALDILOT
                locate for 1=1
                do while not(eof())
                this.w_TESDIS = .T.
                this.w_QTAESI = this.w_QTAESI +_Curs_SALDILOT.SUQTAPER - _Curs_SALDILOT.SUQTRPER
                  select _Curs_SALDILOT
                  continue
                enddo
                use
              endif
              if this.w_QTAESI=0 AND this.w_TESDIS
                * --- Write into LOTTIART
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.LOTTIART_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.LOTTIART_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.LOTTIART_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"LOFLSTAT ="+cp_NullLink(cp_ToStrODBC("E"),'LOTTIART','LOFLSTAT');
                      +i_ccchkf ;
                  +" where ";
                      +"LOCODICE = "+cp_ToStrODBC(this.w_OLDCODLOT);
                      +" and LOCODART = "+cp_ToStrODBC(this.w_OLDKEYSAL);
                         )
                else
                  update (i_cTable) set;
                      LOFLSTAT = "E";
                      &i_ccchkf. ;
                   where;
                      LOCODICE = this.w_OLDCODLOT;
                      and LOCODART = this.w_OLDKEYSAL;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
            endif
        endcase
      endif
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.w_OK 
      * --- Congurenza matricole con qt� di riga
      * --- Select from GSMA_BMT
      do vq_exec with 'GSMA_BMT',this,'_Curs_GSMA_BMT','',.f.,.t.
      if used('_Curs_GSMA_BMT')
        select _Curs_GSMA_BMT
        locate for 1=1
        do while not(eof())
        this.w_OK = .F.
        if NOT EMPTY(NVL(_Curs_GSMA_BMT.DOMAGSCA," "))
          * --- Se si tratta di un trasferimento determino il magazzino di carico\scarico corretto del documento
          this.w_DMAGCAR = Nvl(_Curs_GSMA_BMT.DOMAGCAR,Space(5) )
          this.w_DMAGSCA = Nvl(_Curs_GSMA_BMT.DOMAGSCA,Space(5) )
        else
          this.w_DMAGCAR = NVL(_Curs_GSMA_BMT.DOMAGCAR,Space(5))
          this.w_DMAGSCA = NVL(_Curs_GSMA_BMT.DOMAGCAR,Space(5))
        endif
        do case
          case Nvl(qtaum1,0)<>Nvl(qtamat,0)
            if cp_Round(Nvl(qtaum1,0),0)<>Nvl(qtaum1,0)
              this.w_MESS = ah_msgformat("Riga %1: impossibile movimentare articoli gestiti a matricole per quantit� frazionabili", Alltrim( Str( _Curs_GSMA_BMT.ROWORD ) ) )
            else
              this.w_MESS = ah_msgformat("Riga %1 con numero matricole incongruente. Qt� riga: %2, matricole movimentate: %3", Alltrim( Str( _Curs_GSMA_BMT.ROWORD ) ), Alltrim( Str( _Curs_GSMA_BMT.QTAUM1 ) ), Alltrim( Str( _Curs_GSMA_BMT.QTAMAT ) ) )
            endif
          case ALLTRIM(Nvl(Mmcodice,Space(20)))<>ALLTRIM(Nvl(Mtkeysal,Space(20)))
            this.w_MESS = ah_msgformat("Riga %1 con articolo incongruente. Articolo movimento: %2, articolo matricole: %3", Alltrim( Str( _Curs_GSMA_BMT.ROWORD ) ) , Alltrim( _Curs_GSMA_BMT.MMCODICE ), Alltrim( _Curs_GSMA_BMT.MTKEYSAL ) )
          case Nvl(Mmcodlot,Space(20))<>Nvl(Mtcodlot,Space(20)) AND g_PERLOT="S"
            this.w_MESS = ah_msgformat("Riga %1 con lotto incongruente. Lotto movimento: %2, lotto matricole: %3", Alltrim( Str( _Curs_GSMA_BMT.ROWORD ) ) , Alltrim( NVL(_Curs_GSMA_BMT.MMCODLOT,SPACE(20)) ), Alltrim( NVL(_Curs_GSMA_BMT.MTCODLOT,SPACE(20)) ) ) 
          case ALLTRIM(Nvl(Mtmagpri,Space(5)))<> Alltrim(this.w_DMAGCAR)
            this.w_MESS = ah_msgformat("Riga %1 con magazzino di carico incongruente. Magazzino documento: %2, magazzino matricole: %3", Alltrim( Str( _Curs_GSMA_BMT.ROWORD ) ), Alltrim(this.w_DMAGCAR), Alltrim( NVL(_Curs_GSMA_BMT.MTMAGPRI,Space(5))) )
          case Nvl(Mmcodubi,Space(20))<>Nvl(Mtcodubi,Space(20)) AND g_PERUBI="S"
            this.w_MESS = ah_msgformat("Riga %1 con ubicazione incongruente. Ubicazione movimento: %2, ubicazione matricole: %3", Alltrim( Str( _Curs_GSMA_BMT.ROWORD ) ) , Alltrim( NVL(_Curs_GSMA_BMT.MMCODUBI,SPACE(20)) ), Alltrim( NVL(_Curs_GSMA_BMT.MTCODUBI,SPACE(20)) ) )
          case ALLTRIM(Nvl(Mtmagsca,Space(5)))<> Alltrim(this.w_DMAGSCA) and Not Empty(Nvl(Mtmagsca,Space(5)))
            this.w_MESS = ah_msgformat("Riga %1 con magazzino di scarico incongruente. Magazzino documento: %2, magazzino matricole: %3", Alltrim( Str( _Curs_GSMA_BMT.ROWORD ) ), Alltrim(this.w_DMAGSCA), Alltrim( NVL(_Curs_GSMA_BMT.MTMAGSCA,Space(5))) )
        endcase
        Exit
          select _Curs_GSMA_BMT
          continue
        enddo
        use
      endif
    endif
    if this.w_OK 
      * --- Verifica se il Movimento contiene la stessa matricola due volte (cosa vietata)
      * --- Se ci� avviene la variabile caller w_OK viene posta a false
      this.w_SERIAL = this.oParentObject.w_MMSERIAL
      this.w_NUMRIF = this.oParentObject.w_MMNUMRIF
      * --- Select from GSVE_BML
      do vq_exec with 'GSVE_BML',this,'_Curs_GSVE_BML','',.f.,.t.
      if used('_Curs_GSVE_BML')
        select _Curs_GSVE_BML
        locate for 1=1
        do while not(eof())
        this.w_OK = ( _Curs_GSVE_BML.CODMAT<=1 )
        Exit
          select _Curs_GSVE_BML
          continue
        enddo
        use
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='SALDIART'
    this.cWorkTables[2]='GIORMAGA'
    this.cWorkTables[3]='LOTTIART'
    this.cWorkTables[4]='SALDILOT'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_SALDILOT')
      use in _Curs_SALDILOT
    endif
    if used('_Curs_SALDILOT')
      use in _Curs_SALDILOT
    endif
    if used('_Curs_SALDILOT')
      use in _Curs_SALDILOT
    endif
    if used('_Curs_SALDILOT')
      use in _Curs_SALDILOT
    endif
    if used('_Curs_GSMA_BMT')
      use in _Curs_GSMA_BMT
    endif
    if used('_Curs_GSVE_BML')
      use in _Curs_GSVE_BML
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
