* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kpa                                                        *
*              Associazione account utente                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-12-22                                                      *
* Last revis.: 2013-10-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_kpa",oParentObject))

* --- Class definition
define class tgsut_kpa as StdForm
  Top    = 11
  Left   = 12

  * --- Standard Properties
  Width  = 585
  Height = 369
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-10-29"
  HelpContextID=149504873
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=2

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsut_kpa"
  cComment = "Associazione account utente"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_PASERIAL = space(10)
  o_PASERIAL = space(10)
  w_DOMINIO = space(50)
  o_DOMINIO = space(50)

  * --- Children pointers
  GSUT_MPS = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    *set procedure to GSUT_MPS additive
    with this
      .Pages(1).addobject("oPag","tgsut_kpaPag1","gsut_kpa",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDOMINIO_1_6
    this.parent.cComment=cp_Translate(this.parent.cComment)
    *release procedure GSUT_MPS
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return

  function CreateChildren()
    this.GSUT_MPS = CREATEOBJECT('stdDynamicChild',this,'GSUT_MPS',this.oPgFrm.Page1.oPag.oLinkPC_1_2)
    this.GSUT_MPS.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSUT_MPS)
      this.GSUT_MPS.DestroyChildrenChain()
      this.GSUT_MPS=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_2')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSUT_MPS.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSUT_MPS.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSUT_MPS.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSUT_MPS.SetKey(;
            .w_PASERIAL,"PASERIAL";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSUT_MPS.ChangeRow(this.cRowID+'      1',1;
             ,.w_PASERIAL,"PASERIAL";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSUT_MPS)
        i_f=.GSUT_MPS.BuildFilter()
        if !(i_f==.GSUT_MPS.cQueryFilter)
          i_fnidx=.GSUT_MPS.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSUT_MPS.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSUT_MPS.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSUT_MPS.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSUT_MPS.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.Save_GSUT_MPS(.f.)
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    if  this.GSUT_MPS.IsAChildUpdated()
      return cp_YesNo(MSG_DISCARD_CHANGES_QP)
    endif
    return .t.
  endfunc
  proc Save_GSUT_MPS(i_ask)
    if this.GSUT_MPS.IsAChildUpdated() and (!i_ask or cp_YesNo(MSG_SAVE_CHANGES_QP+' (LinkPC)'))
      cp_BeginTrs()
      this.GSUT_MPS.mReplace(.t.)
      cp_EndTrs()
    endif
  endproc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PASERIAL=space(10)
      .w_DOMINIO=space(50)
        .w_PASERIAL = '0000000001'
      .GSUT_MPS.NewDocument()
      .GSUT_MPS.ChangeRow('1',1,.w_PASERIAL,"PASERIAL")
      if not(.GSUT_MPS.bLoaded)
        .GSUT_MPS.SetKey(.w_PASERIAL,"PASERIAL")
      endif
        .w_DOMINIO = LEFT(SYS(0), RAT(' # ',SYS(0))-1)
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_3.enabled = this.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    this.GSUT_MPS.SetStatus(i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSUT_MPS.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        if .o_DOMINIO<>.w_DOMINIO
          .Calculate_IFTSVGFVJZ()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .w_PASERIAL<>.o_PASERIAL
          .Save_GSUT_MPS(.t.)
          .GSUT_MPS.NewDocument()
          .GSUT_MPS.ChangeRow('1',1,.w_PASERIAL,"PASERIAL")
          if not(.GSUT_MPS.bLoaded)
            .GSUT_MPS.SetKey(.w_PASERIAL,"PASERIAL")
          endif
        endif
      endwith
      this.DoRTCalc(1,2,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_IFTSVGFVJZ()
    with this
          * --- Popola tabella TMP_ACCOUNT
          GSUT_BPA(this;
              ,'Z';
             )
    endwith
  endproc
  proc Calculate_VPMJLADTFT()
    with this
          * --- Drop TMP_ACCOUNT
          GSUT_BPA(this;
              ,'D';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Init")
          .Calculate_IFTSVGFVJZ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Done")
          .Calculate_VPMJLADTFT()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDOMINIO_1_6.RadioValue()==this.w_DOMINIO)
      this.oPgFrm.Page1.oPag.oDOMINIO_1_6.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      *i_bRes = i_bRes .and. .GSUT_MPS.CheckForm()
      if i_bres
        i_bres=  .GSUT_MPS.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PASERIAL = this.w_PASERIAL
    this.o_DOMINIO = this.w_DOMINIO
    * --- GSUT_MPS : Depends On
    this.GSUT_MPS.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsut_kpaPag1 as StdContainer
  Width  = 581
  height = 369
  stdWidth  = 581
  stdheight = 369
  resizeXpos=392
  resizeYpos=183
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_1_2 as stdDynamicChildContainer with uid="HYXMFRIIUC",left=3, top=2, width=572, height=311, bOnScreen=.t.;



  add object oBtn_1_3 as StdButton with uid="UUNSDLSMLD",left=466, top=318, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Ok";
    , HelpContextID = 149476122;
    , caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_3.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_4 as StdButton with uid="ODXAAKXYKD",left=521, top=318, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 142187450;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_5 as StdButton with uid="ZTQKFDTJCT",left=7, top=318, width=48,height=45,;
    CpPicture="BMP\CARICA.BMP", caption="", nPag=1;
    , ToolTipText = "Carica utenti";
    , HelpContextID = 21544410;
    , caption='\<Carica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      with this.Parent.oContained
        GSUT_BPA(this.Parent.oContained,"L")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oDOMINIO_1_6 as StdTableComboDom with uid="BBORIXCSVR",rtseq=2,rtrep=.f.,left=132,top=320,width=256,height=21;
    , ToolTipText = "Selezionare un dominio";
    , HelpContextID = 180035530;
    , cFormVar="w_DOMINIO",tablefilter="", bObbl = .f. , nPag = 1;
    , cTable='',cKey='',cValue='',cOrderBy='',xDefault=space(50);
  , bGlobalFont=.t.


  add object oStr_1_7 as StdString with uid="HWYMEVJPXJ",Visible=.t., Left=59, Top=322,;
    Alignment=1, Width=71, Height=18,;
    Caption="Dominio:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kpa','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut_kpa
define class StdTableComboDom as StdTableCombo
  proc Init
    * --- Zucchetti Aulla - Inizio Interfaccia
  	IF VARTYPE(this.bNoBackColor)='U'
		  This.backcolor=i_EBackColor
	  ENDIF
    * --- Zucchetti Aulla - Fine Interfaccia	
    this.ToolTipText=cp_Translate(this.ToolTipText)
    local l_numf, l_i
    l_i = 1
    local WshNetwork, l_old_Err, l_err
    l_err = .f.
    l_old_Err = ON('ERROR')
    ON ERROR l_err = .t.
    WshNetwork = CreateObject("Wscript.NetWork")
    ON ERROR &l_old_Err
    DIMENSION this.combovalues(1)
    this.combovalues[1] = "(Local)"
    If !l_err
      If Not Empty(WshNetwork.ComputerName)
        this.combovalues[1] = Alltrim(WshNetwork.ComputerName)
      Endif
      If Not Empty(WshNetwork.UserDomain)
        DIMENSION this.combovalues(2)
        this.combovalues[2] = Alltrim(WshNetwork.UserDomain)
      EndIf
    EndIf
    WshNetwork = .Null.
    This.nValues = ALEN(this.combovalues)
    do while l_i <= This.nValues
        this.AddItem(this.combovalues[l_i])
        l_i = l_i + 1
    enddo
    This.SetFont()
  endproc
enddefine
* --- Fine Area Manuale
