* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bci                                                        *
*              Contabilizzazione indiretta                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_298]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-08                                                      *
* Last revis.: 2013-12-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bci",oParentObject)
return(i_retval)

define class tgscg_bci as StdBatch
  * --- Local variables
  w_ZOOM = space(10)
  w_DESSUP = space(50)
  w_FLMOVC = space(1)
  w_CAUMOV = space(5)
  w_TIPCAU = space(1)
  w_FLCRDE = space(1)
  w_CONBAN = space(15)
  w_APPVAL = 0
  w_IMPDEB = 0
  w_IMPCRE = 0
  w_NUMCOR = space(15)
  w_TIPBAN = space(1)
  w_CALFES = space(3)
  w_CODVAL = space(3)
  w_CCFLCRED = space(1)
  w_CCFLDEBI = space(1)
  w_CCFLCOMM = space(1)
  w_DATVAL = ctod("  /  /  ")
  w_FLERR = .f.
  w_MESS_ERR = space(200)
  w_oMess = .NULL.
  w_oPart = .NULL.
  w_CONTA = 0
  w_ERR_CNT = 0
  w_UTCC = 0
  w_CAOVAC = 0
  w_PTDATSCA = ctod("  /  /  ")
  w_UTDC = ctod("  /  /  ")
  w_PTNUMPAR = space(31)
  w_UTCV = 0
  w_PNCODESE = space(4)
  w_PNCODPAG = space(5)
  w_PT_SEGNO = space(1)
  w_UTDV = ctod("  /  /  ")
  w_CODVAP = space(3)
  w_CAOVAP = 0
  w_PNCODUTE = 0
  w_PTTOTIMP = 0
  w_PERPVL = 0
  w_PNCODVAL = space(3)
  w_PTCODVAL = space(3)
  w_PNINICOM = ctod("  /  /  ")
  w_PNCAOVAL = 0
  w_PTCAOVAL = 0
  w_PNFINCOM = ctod("  /  /  ")
  w_PNNUMPRO = 0
  w_PTCAOAPE = 0
  w_PNVALNAZ = space(3)
  w_PNCODCAU = space(5)
  w_PTMODPAG = space(10)
  w_CCFLPART = space(1)
  w_PNNUMRER = 0
  w_PTFLCRSA = space(1)
  w_FLPPRO = space(1)
  w_CPROWNUM = 0
  w_PTFLSOSP = space(1)
  w_PNPRP = space(2)
  w_CPROWORD = 0
  w_PTROWORD = 0
  w_TIPCON = space(1)
  w_PNTIPCON = space(1)
  w_PTROWNUM = 0
  w_CODCON = space(15)
  w_PNCODCON = space(15)
  w_PTTIPCON = space(1)
  w_IMPBAN = 0
  w_PNFLPART = space(1)
  w_PTCODCON = space(15)
  w_IMPCLF = 0
  w_PNSERIAL = space(10)
  w_PTIMPDOC = 0
  w_MESS = space(30)
  w_OK = .f.
  w_PTNUMDOC = 0
  w_AGGTES = .f.
  w_DATBLO = ctod("  /  /  ")
  w_PTALFDOC = space(10)
  w_SQUAD = 0
  w_STALIG = ctod("  /  /  ")
  w_PTDATDOC = space(2)
  w_PNTIPDOC = space(2)
  w_PNDATREG = ctod("  /  /  ")
  w_PTBANNOS = space(15)
  w_PNTIPREG = space(1)
  w_PNALFPRO = space(10)
  w_PNANNPRO = space(4)
  w_PTBANAPP = space(10)
  w_PNNUMREG = 0
  w_CAONAZ = 0
  w_CPROWNU1 = 0
  w_PNFLSALD = space(1)
  w_PERPVL = 0
  w_PNRIFDIS = space(10)
  w_PNCAOVAL = 0
  w_SALROW = 0
  w_CAOVAL = 0
  w_PARROW = 0
  w_DATAPE = ctod("  /  /  ")
  w_CODAZI = space(5)
  w_EURVAL = 0
  w_VAL1 = 0
  w_IMPDCA = 0
  w_VAL2 = 0
  w_IMPDCP = 0
  w_PNFLPART = space(1)
  w_APPO1 = 0
  w_PNIMPDAR = 0
  w_PNPRG = space(8)
  w_PNIMPAVE = 0
  w_APPO = 0
  w_SEZB = space(1)
  w_PTFLRAGG = space(1)
  w_PTDESRIG = space(50)
  w_SERIAL = space(10)
  w_ROWORD = 0
  w_ROWNUM = 0
  w_SEGNO = space(1)
  w_PNCODAGE = space(5)
  w_PNFLVABD = space(1)
  w_OSERIAL = space(10)
  w_OROWORD = 0
  w_OROWNUM = 0
  w_CONSUP = space(15)
  w_OLDSER = space(10)
  w_OLDROW = 0
  w_RIFINDI = space(10)
  w_OLDNUM = 0
  w_CCFLANAL = space(1)
  w_PTNUMCOR = space(25)
  w_PNDESRIG = space(50)
  w_CCDESRIG = space(254)
  w_CCDESSUP = space(254)
  w_FLZERO = space(1)
  * --- WorkFile variables
  CAU_CONT_idx=0
  ESERCIZI_idx=0
  VALUTE_idx=0
  PNT_DETT_idx=0
  SALDICON_idx=0
  PNT_MAST_idx=0
  PAR_TITE_idx=0
  CON_INDI_idx=0
  AZIENDA_idx=0
  CONTI_idx=0
  COC_MAST_idx=0
  CCM_DETT_idx=0
  CCC_DETT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Contabilizzazione Indiretta Effetti (da GSCG_ACI)
    * --- Variabili C\C
     
 DIMENSION ARPARAM[12,2]
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Controlli Preliminari
    do case
      case EMPTY(this.oParentObject.w_CICONEFA)
        ah_ErrorMsg("Conto effetti attivi per contab.indiretta inesistente",,"")
        i_retcode = 'stop'
        return
      case EMPTY(this.oParentObject.w_CICODCAU)
        ah_ErrorMsg("Causale contabile per contab.indiretta inesistente",,"")
        i_retcode = 'stop'
        return
      case EMPTY(this.oParentObject.w_CICONDCA) OR EMPTY(this.oParentObject.w_CICONDCP)
        ah_ErrorMsg("Contropartite differenze cambi non definite",,"")
        i_retcode = 'stop'
        return
      case EMPTY(this.oParentObject.w_CICODVAL)
        ah_ErrorMsg("Codice valuta non definito",,"")
        i_retcode = 'stop'
        return
      case EMPTY(this.oParentObject.w_CIDATVAL)
        ah_ErrorMsg("Data valuta non definita",,"")
        i_retcode = 'stop'
        return
      case EMPTY(this.w_CAUMOV) AND g_BANC="S" AND this.w_FLMOVC="S"
        ah_ErrorMsg("Causale movimenti C\C non definita nella causale contabile",,"")
        i_retcode = 'stop'
        return
    endcase
    this.w_OK = .T.
    * --- seleziono il cursore
    this.w_ZOOM = this.oParentObject.w_CalcZoom
    NC = this.w_ZOOM.cCursor
    SELECT &NC
    GO TOP
    * --- Cicla sui record Selezionati.
    *     Conto i record selezionati dall'utente per verificare che almeno una partita
    *     sia contabilizzazbile
    Count For Xchk=1 To this.w_CONTA
    this.w_ERR_CNT = 0
    GO TOP
    SCAN FOR XCHK<>0 AND EMPTY(NVL(BANAPP,""))
    REPLACE XCHK WITH 0
    this.w_ERR_CNT = this.w_ERR_CNT + 1
    ah_ErrorMsg("La partita: %1 del %2 non ha la banca di appoggio",,"",ALLTRIM(NVL(NUMPAR,"")),DTOC(CP_TODATE(DATSCA)))
    ENDSCAN
    * --- Se nessuna partita � valida non proseguo
    if this.w_CONTA=0 Or this.w_ERR_CNT=this.w_CONTA
      ah_ErrorMsg("Non ci sono scadenze da abbinare",,"")
      i_retcode = 'stop'
      return
    endif
    * --- Se almeno una partita non ha la banca d'appoggio
    *     e se qualche partita � valida (ha la banca)
    if (this.w_ERR_CNT>0 And this.w_ERR_CNT<this.w_CONTA) And not ah_YesNo("Assegno comunque le altre partite?")
      * --- esco
      i_retcode = 'stop'
      return
    endif
    * --- Try
    local bErr_03B2E398
    bErr_03B2E398=bTrsErr
    this.Try_03B2E398()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      ah_ErrorMsg(i_errmsg,,"")
      if used ("CONTABIL")
        select CONTABIL
        use
      endif
      i_retcode = 'stop'
      return
    endif
    bTrsErr=bTrsErr or bErr_03B2E398
    * --- End
    * --- Try
    local bErr_0380F888
    bErr_0380F888=bTrsErr
    this.Try_0380F888()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      if NOT EMPTY(I_ERRMSG)
        ah_ErrorMsg(i_errmsg,,"")
      else
        ah_ErrorMsg("Errore durante aggiornamento. Operazione abbandonata",,"")
      endif
    endif
    bTrsErr=bTrsErr or bErr_0380F888
    * --- End
    if Not this.w_FLERR
      * --- Non ho errori bloccanti
      * --- Inserisco sulle partite la data di registrazione
      * --- Write into PAR_TITE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PTDATREG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CIDATREG),'PAR_TITE','PTDATREG');
            +i_ccchkf ;
        +" where ";
            +"PTSERIAL = "+cp_ToStrODBC(this.oParentObject.w_CISERIAL);
            +" and PTROWORD = "+cp_ToStrODBC(-3);
               )
      else
        update (i_cTable) set;
            PTDATREG = this.oParentObject.w_CIDATREG;
            &i_ccchkf. ;
         where;
            PTSERIAL = this.oParentObject.w_CISERIAL;
            and PTROWORD = -3;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      if this.w_OK
        * --- Scrive Contabilizzazione
        * --- Write into CON_INDI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CON_INDI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CON_INDI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CON_INDI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CIFLDEFI ="+cp_NullLink(cp_ToStrODBC("S"),'CON_INDI','CIFLDEFI');
          +",CIRIFCON ="+cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'CON_INDI','CIRIFCON');
          +",CIDESCRI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CIDESCRI),'CON_INDI','CIDESCRI');
              +i_ccchkf ;
          +" where ";
              +"CISERIAL = "+cp_ToStrODBC(this.oParentObject.w_CISERIAL);
                 )
        else
          update (i_cTable) set;
              CIFLDEFI = "S";
              ,CIRIFCON = this.w_PNSERIAL;
              ,CIDESCRI = this.oParentObject.w_CIDESCRI;
              &i_ccchkf. ;
           where;
              CISERIAL = this.oParentObject.w_CISERIAL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Aggiorno lo zoom, per dare la possibilit� all'utente di visualizzare il risultato
        *     subito dopo il messaggio chiudo la maschera
        This.oParentObject.NotifyEvent("Legge")
        ah_ErrorMsg("Aggiornamento completato",,"")
        * --- Chiudo la Maschera
        This.bUpdateParentObject=.f.
        This.oParentObject.bUpdated=.f.
        This.oParentObject.ecpQuit()
      else
        ah_ErrorMsg("Non ci sono scadenze da abbinare",,"")
      endif
    endif
    if used ("CONTABIL")
      select CONTABIL
      use
    endif
    * --- Toglie <Blocco> per Primanota
    * --- Try
    local bErr_03EDBFA0
    bErr_03EDBFA0=bTrsErr
    this.Try_03EDBFA0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      ah_ErrorMsg("Impossibile rimuovere il blocco della prima nota. Rimuoverlo dai dati azienda",,"")
    endif
    bTrsErr=bTrsErr or bErr_03EDBFA0
    * --- End
  endproc
  proc Try_03B2E398()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Leggo la data dei blocco e la data stampa del libro giornale
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZDATBLO,AZSTALIG"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZDATBLO,AZSTALIG;
        from (i_cTable) where;
            AZCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DATBLO = NVL(cp_ToDate(_read_.AZDATBLO),cp_NullValue(_read_.AZDATBLO))
      this.w_STALIG = NVL(cp_ToDate(_read_.AZSTALIG),cp_NullValue(_read_.AZSTALIG))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    do case
      case this.oParentObject.w_CIDATVAL<=this.w_STALIG AND NOT EMPTY(this.w_STALIG)
        * --- Raise
        i_Error="Data di contabilizzazione inferiore a data ultima stampa L.G."
        return
      case EMPTY(this.w_DATBLO)
        * --- Inserisce <Blocco> per Primanota
        * --- Write into AZIENDA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.AZIENDA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"AZDATBLO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CIDATVAL),'AZIENDA','AZDATBLO');
              +i_ccchkf ;
          +" where ";
              +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                 )
        else
          update (i_cTable) set;
              AZDATBLO = this.oParentObject.w_CIDATVAL;
              &i_ccchkf. ;
           where;
              AZCODAZI = this.w_CODAZI;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case Not EMPTY(this.w_DATBLO)
        * --- UN altro utente ha impostato il blocco - controllo concorrenza
        * --- Raise
        i_Error="Prima nota bloccata - verificare semaforo bollati in dati azienda"
        return
    endcase
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_0380F888()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_OK = .F.
    this.w_ZOOM = this.oParentObject.w_CalcZoom
    NC = this.w_ZOOM.cCursor
    * --- Ordino le righe del cursore prima di creare la registrazione contabile
    vq_exec("query\GSCG5KCI.VQR",this,"CHK_PART")
    SELECT * FROM &NC ORDER BY TIPCON,CODCON INTO CURSOR CONTABIL
    select * from CHK_PART INNER JOIN CONTABIL ON CHK_PART.PTSERRIF=CONTABIL.PTSERIAL AND CHK_PART.PTORDRIF=CONTABIL.PTROWORD AND CHK_PART.PTNUMRIF=CONTABIL.CPROWNUM; 
 INTO CURSOR CHK_PART
    SELECT CHK_PART
    if RECCOUNT()>0
      * --- Raise
      i_Error="Esistono partite gi� inserite in una contabilizzazione indiretta"
      return
    endif
    if used ("CHK_PART")
      select CHK_PART
      use
    endif
    SELECT CONTABIL
    * --- Cicla sui record Selezionati
    this.w_CPROWNU1 = 0
    this.w_PNSERIAL = SPACE(10)
    this.w_AGGTES = .F.
    this.w_SQUAD = 0
    if this.oParentObject.w_CIFLSIPA="S"
      if RECCOUNT()>0
        GO TOP
        SCAN FOR XCHK<>0 AND NVL(TIPCON,"") $ "CFG" AND NOT EMPTY(NVL(CODCON, " ")) AND NVL(TOTIMP,0)<>0
        if (this.w_OLDSER<>PTSERIAL OR this.w_OLDROW<>PTROWORD OR this.w_OLDNUM<>CPROWNUM) 
          this.Pag5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_OLDSER = PTSERIAL
          this.w_OLDROW = PTROWORD
          this.w_RIFINDI = this.w_PNSERIAL
          this.w_OLDNUM = CPROWNUM
          this.w_TIPCON = "X"
          this.w_CODCON = SPACE(15)
          this.Pag6()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_PTDATSCA = cp_CharToDate("  -  -  ")
          this.w_PTNUMPAR = ""
          this.Pag7()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if Isalt() and Not Empty(this.w_PNSERIAL)
          gsal_baf(this,this.w_PNSERIAL,"P",Cp_chartodate("  -  -    "),Cp_chartodate("  -  -    "),"A")
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        SELECT CONTABIL
        ENDSCAN
      endif
    else
      if RECCOUNT()>0
        GO TOP
        SCAN FOR XCHK<>0 AND NVL(TIPCON,"") $ "CFG" AND NOT EMPTY(NVL(CODCON, " ")) AND NVL(TOTIMP,0)<>0
        * --- Se Raggruppata, non eseguo il controllo sul cambio del seriale e del ptroword, ma scrivo una sola registrazione.
        if this.w_AGGTES=.F.
          this.w_AGGTES = .T.
          this.Pag5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_RIFINDI = this.w_PNSERIAL
        endif
        this.Pag6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if Isalt() and Not Empty(this.w_PNSERIAL)
          gsal_baf(this,this.w_PNSERIAL,"P",Cp_chartodate("  -  -    "),Cp_chartodate("  -  -    "),"A")
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        SELECT CONTABIL
        ENDSCAN
      endif
    endif
    if this.w_AGGTES=.T.
      this.w_PTDATSCA = cp_CharToDate("  -  -  ")
      this.w_PTNUMPAR = ""
      this.Pag7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if (this.oParentObject.w_CIFLSIPA<>"S" AND (this.w_SQUAD<>0 OR this.w_AGGTES=.F. )) OR (this.oParentObject.w_CIFLSIPA="S" AND (this.w_SQUAD<>0 OR this.w_AGGTES=.T.)) 
      this.w_oMess=createobject("Ah_Message")
      this.w_oMess.AddMsgPartNL("ATTENZIONE:")     
      if this.w_AGGTES=.F.
        this.w_oMess.AddMsgPartNL("Registrazione di contabilizzazione indiretta non generata")     
      else
        this.w_oPart = this.w_oMess.AddMsgPartNL("Errore di quadratura (%1)")
        this.w_oPart.AddParam(ALLTRIM(STR(this.w_SQUAD)))     
        this.w_oMess.AddMsgPartNL("Nella registrazione di contabilizzazione indiretta")     
      endif
      this.w_oMess.AddMsgPartNL("Confermi ugualmente?")     
      if NOT this.w_oMess.ah_YesNo()
        * --- Raise
        i_Error="Errore durante la generazione del movimento contabile; operazione annullata"
        return
      endif
    endif
    this.w_OK = .T.
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_03EDBFA0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into AZIENDA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AZDATBLO ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'AZIENDA','AZDATBLO');
          +i_ccchkf ;
      +" where ";
          +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             )
    else
      update (i_cTable) set;
          AZDATBLO = cp_CharToDate("  -  -  ");
          &i_ccchkf. ;
       where;
          AZCODAZI = this.w_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inizializza variabili di Lavoro
    * --- Inizializa Valori
    this.w_FLERR = .F.
    this.w_PNFLSALD = "+"
    this.w_IMPBAN = 0
    this.w_UTCC = i_CODUTE
    this.w_UTDC = SetInfoDate( g_CALUTD )
    this.w_UTCV = 0
    this.w_UTDV = cp_CharToDate("  -  -    ")
    this.w_PNCAOVAL = GETCAM(g_PERVAL, i_DATSYS)
    this.w_IMPCLF = 0
    this.w_PNDATREG = this.oParentObject.w_CIDATVAL
    this.w_PNCODPAG = SPACE(5)
    this.w_PNCODAGE = SPACE(5)
    this.w_PNFLVABD = " "
    this.w_APPO = 0
    this.w_CCFLPART = " "
    this.w_CODAZI = i_CODAZI
    this.w_PNTIPDOC = "  "
    this.w_FLPPRO = "N"
    this.w_DATBLO = cp_CharToDate("  -  -  ")
    this.w_PNTIPREG = "N"
    this.w_PNANNPRO = SPACE(4)
    this.w_STALIG = cp_CharToDate("  -  -  ")
    this.w_PNNUMREG = 0
    this.w_PNCODESE = g_CODESE
    this.w_PNALFPRO = Space(10)
    this.w_IMPDCA = 0
    this.w_CAOVAC = this.oParentObject.w_CICAOVAL
    this.w_IMPDCP = 0
    this.w_PNCODVAL = this.oParentObject.w_CICODVAL
    this.w_TIPCON = "X"
    this.w_CODCON = SPACE(15)
    this.w_PNCAOVAL = this.oParentObject.w_CICAOVAL
    this.w_PNCODCAU = this.oParentObject.w_CICODCAU
    this.w_PNCODUTE = IIF(g_UNIUTE $ "UE", i_CODUTE, 0)
    this.w_PNPRG = IIF(g_UNIUTE $ "GE", DTOS(this.w_PNDATREG), SPACE(8))
    this.w_PTFLRAGG = " "
    this.w_CCFLANAL = " "
    * --- Legge dati collegati alla Causale Contabile
    * --- Read from CAU_CONT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAU_CONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2],.t.,this.CAU_CONT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CCSERPRO,CCTIPDOC,CCTIPREG,CCNUMREG,CCFLPPRO,CCFLPART,CCFLMOVC,CCCAUMOV,CCFLANAL,CCDESSUP,CCDESRIG"+;
        " from "+i_cTable+" CAU_CONT where ";
            +"CCCODICE = "+cp_ToStrODBC(this.w_PNCODCAU);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CCSERPRO,CCTIPDOC,CCTIPREG,CCNUMREG,CCFLPPRO,CCFLPART,CCFLMOVC,CCCAUMOV,CCFLANAL,CCDESSUP,CCDESRIG;
        from (i_cTable) where;
            CCCODICE = this.w_PNCODCAU;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PNALFPRO = NVL(cp_ToDate(_read_.CCSERPRO),cp_NullValue(_read_.CCSERPRO))
      this.w_PNTIPDOC = NVL(cp_ToDate(_read_.CCTIPDOC),cp_NullValue(_read_.CCTIPDOC))
      this.w_PNTIPREG = NVL(cp_ToDate(_read_.CCTIPREG),cp_NullValue(_read_.CCTIPREG))
      this.w_PNNUMREG = NVL(cp_ToDate(_read_.CCNUMREG),cp_NullValue(_read_.CCNUMREG))
      this.w_FLPPRO = NVL(cp_ToDate(_read_.CCFLPPRO),cp_NullValue(_read_.CCFLPPRO))
      this.w_CCFLPART = NVL(cp_ToDate(_read_.CCFLPART),cp_NullValue(_read_.CCFLPART))
      this.w_FLMOVC = NVL(cp_ToDate(_read_.CCFLMOVC),cp_NullValue(_read_.CCFLMOVC))
      this.w_CAUMOV = NVL(cp_ToDate(_read_.CCCAUMOV),cp_NullValue(_read_.CCCAUMOV))
      this.w_CCFLANAL = NVL(cp_ToDate(_read_.CCFLANAL),cp_NullValue(_read_.CCFLANAL))
      this.w_CCDESSUP = NVL(cp_ToDate(_read_.CCDESSUP),cp_NullValue(_read_.CCDESSUP))
      this.w_CCDESRIG = NVL(cp_ToDate(_read_.CCDESRIG),cp_NullValue(_read_.CCDESRIG))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    do case
      case this.w_FLPPRO="D"
        this.w_PNANNPRO = STR(YEAR(this.w_PNDATREG), 4, 0)
      case this.w_FLPPRO="E"
        this.w_PNANNPRO = this.w_PNCODESE
    endcase
    this.w_PNINICOM = g_INIESE
    this.w_PNFINCOM = g_FINESE
    this.w_PNVALNAZ = g_PERVAL
    this.w_CAONAZ = GETCAM(this.w_PNVALNAZ, this.w_PNDATREG, 0)
    this.w_CAONAZ = IIF(this.w_CAONAZ=0, 1, this.w_CAONAZ)
    this.w_PERPVL = g_PERPVL
    if this.w_PNDATREG<g_INIESE OR this.w_PNDATREG>g_FINESE
      * --- Se di un altro esercizio legge Codice e Divisa
      * --- Select from ESERCIZI
      i_nConn=i_TableProp[this.ESERCIZI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select ESCODESE, ESVALNAZ, ESINIESE, ESFINESE  from "+i_cTable+" ESERCIZI ";
            +" where ESINIESE<="+cp_ToStrODBC(this.w_PNDATREG)+" AND ESFINESE>="+cp_ToStrODBC(this.w_PNDATREG)+" AND ESCODAZI="+cp_ToStrODBC(this.w_CODAZI)+"";
             ,"_Curs_ESERCIZI")
      else
        select ESCODESE, ESVALNAZ, ESINIESE, ESFINESE from (i_cTable);
         where ESINIESE<=this.w_PNDATREG AND ESFINESE>=this.w_PNDATREG AND ESCODAZI=this.w_CODAZI;
          into cursor _Curs_ESERCIZI
      endif
      if used('_Curs_ESERCIZI')
        select _Curs_ESERCIZI
        locate for 1=1
        do while not(eof())
        this.w_PNCODESE = ESCODESE
        this.w_PNVALNAZ = NVL(ESVALNAZ, g_PERVAL)
        this.w_PNINICOM = CP_TODATE(ESINIESE)
        this.w_PNFINCOM = CP_TODATE(ESFINESE)
          select _Curs_ESERCIZI
          continue
        enddo
        use
      endif
    endif
    * --- Simbolo Valuta Nazionale e cambio Fisso EURO dell'esercizio Letto
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VADECTOT"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_PNVALNAZ);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VADECTOT;
        from (i_cTable) where;
            VACODVAL = this.w_PNVALNAZ;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PERPVL = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_PNVALNAZ<>g_PERVAL
      this.w_CAONAZ = GETCAM(this.w_PNVALNAZ, this.oParentObject.w_CIDATVAL, 0)
      this.w_CAONAZ = IIF(this.w_CAONAZ=0, 1, this.w_CAONAZ)
    endif
    this.w_EURVAL = 999
    * --- Calcola Differenza Cambio (Passiva in Dare; Attiva in Avere) (PTCAOVAL=C.Apertura; w_CAOVAC=C.Chiusura)
    if this.w_PNCODVAL<>this.w_PNVALNAZ
      this.w_EURVAL = 0
      this.w_EURVAL = GETCAM(this.w_PNCODVAL, I_DATSYS)
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna Dettaglio Primanota e Saldi
    this.w_CPROWNUM = this.w_CPROWNUM + 1
    this.w_CPROWORD = this.w_CPROWORD + 10
    this.w_PNIMPDAR = IIF(this.w_APPO>0, this.w_APPO, 0)
    this.w_PNIMPAVE = IIF(this.w_APPO<0, ABS(this.w_APPO), 0)
    * --- Try
    local bErr_03D76400
    bErr_03D76400=bTrsErr
    this.Try_03D76400()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Raise
      i_Error="Impossibile inserire movimento in prima nota"
      return
    endif
    bTrsErr=bTrsErr or bErr_03D76400
    * --- End
    * --- Aggiorna Saldo
    * --- Try
    local bErr_0402E9B0
    bErr_0402E9B0=bTrsErr
    this.Try_0402E9B0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_0402E9B0
    * --- End
    * --- Write into SALDICON
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICON_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SLDARPER =SLDARPER+ "+cp_ToStrODBC(this.w_PNIMPDAR);
      +",SLAVEPER =SLAVEPER+ "+cp_ToStrODBC(this.w_PNIMPAVE);
          +i_ccchkf ;
      +" where ";
          +"SLTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
          +" and SLCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
          +" and SLCODESE = "+cp_ToStrODBC(this.w_PNCODESE);
             )
    else
      update (i_cTable) set;
          SLDARPER = SLDARPER + this.w_PNIMPDAR;
          ,SLAVEPER = SLAVEPER + this.w_PNIMPAVE;
          &i_ccchkf. ;
       where;
          SLTIPCON = this.w_PNTIPCON;
          and SLCODICE = this.w_PNCODCON;
          and SLCODESE = this.w_PNCODESE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Gestione Analitica
    if this.w_CCFLANAL="S" And this.w_PNTIPCON="G" And g_PERCCR="S"
      this.w_MESS_ERR = GSAR_BRA(This,this.w_PNSERIAL, this.w_CPROWNUM, this.w_PNTIPCON, this.w_PNCODCON, this.w_APPO)
      if Not EMPTY(this.w_MESS_ERR)
        ah_ErrorMsg("%1",,"",this.w_MESS_ERR)
      endif
    endif
  endproc
  proc Try_03D76400()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Leggo dai Conti il mastro di raggruppamento associato
    * --- Quindi lo utilizzo per fare la calcsez
    * --- Read from CONTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ANCONSUP"+;
        " from "+i_cTable+" CONTI where ";
            +"ANCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
            +" and ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ANCONSUP;
        from (i_cTable) where;
            ANCODICE = this.w_PNCODCON;
            and ANTIPCON = this.w_PNTIPCON;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CONSUP = NVL(cp_ToDate(_read_.ANCONSUP),cp_NullValue(_read_.ANCONSUP))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_SEZB = CALCSEZ(this.w_CONSUP)
    if this.w_SEZB$"PA"
      * --- Se la sezione di Bilancio � un' attivit� o una passivit�, sbianco le date di inizio e fine competenza
      this.w_PNFINCOM = cp_CharToDate("  -  -  ")
      this.w_PNINICOM = cp_CharToDate("  -  -  ")
    endif
    this.w_SQUAD = this.w_SQUAD + (this.w_PNIMPDAR-this.w_PNIMPAVE)
    this.w_FLZERO = IIF( this.w_APPO=0 , "S", " " )
    this.w_PNDESRIG = this.oParentObject.w_CIDESCRI
    if Empty(this.w_PNDESRIG) and Not EMPTY(this.w_CCDESRIG) and this.w_PNTIPCON<>"G"
       
 ARPARAM[8,1]="CODAGE" 
 ARPARAM[8,2]=ALLTRIM(this.w_PNCODAGE) 
 ARPARAM[10,1]="NUMPAR" 
 ARPARAM[10,2]=Alltrim(this.w_PTNUMPAR) 
 ARPARAM[11,1]="DATSCA" 
 ARPARAM[11,2]=DTOC(this.w_PTDATSCA)
      this.w_PNDESRIG = CALDESPA(this.w_CCDESRIG,@ARPARAM)
    endif
    * --- Insert into PNT_DETT
    i_nConn=i_TableProp[this.PNT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PNSERIAL"+",CPROWNUM"+",CPROWORD"+",PNTIPCON"+",PNCODCON"+",PNIMPDAR"+",PNIMPAVE"+",PNFLPART"+",PNDESRIG"+",PNCAURIG"+",PNFLSALD"+",PNFLSALF"+",PNFLSALI"+",PNCODPAG"+",PNCODAGE"+",PNFLVABD"+",PNINICOM"+",PNFINCOM"+",PNFLZERO"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_DETT','PNSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PNT_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'PNT_DETT','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCON),'PNT_DETT','PNTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'PNT_DETT','PNCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNIMPDAR),'PNT_DETT','PNIMPDAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNIMPAVE),'PNT_DETT','PNIMPAVE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLPART),'PNT_DETT','PNFLPART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESRIG),'PNT_DETT','PNDESRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCAU),'PNT_DETT','PNCAURIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLSALD),'PNT_DETT','PNFLSALD');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALF');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODPAG),'PNT_DETT','PNCODPAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODAGE),'PNT_DETT','PNCODAGE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLVABD),'PNT_DETT','PNFLVABD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNINICOM),'PNT_DETT','PNINICOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFINCOM),'PNT_DETT','PNFINCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FLZERO),'PNT_DETT','PNFLZERO');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'PNTIPCON',this.w_PNTIPCON,'PNCODCON',this.w_PNCODCON,'PNIMPDAR',this.w_PNIMPDAR,'PNIMPAVE',this.w_PNIMPAVE,'PNFLPART',this.w_PNFLPART,'PNDESRIG',this.w_PNDESRIG,'PNCAURIG',this.w_PNCODCAU,'PNFLSALD',this.w_PNFLSALD,'PNFLSALF'," ")
      insert into (i_cTable) (PNSERIAL,CPROWNUM,CPROWORD,PNTIPCON,PNCODCON,PNIMPDAR,PNIMPAVE,PNFLPART,PNDESRIG,PNCAURIG,PNFLSALD,PNFLSALF,PNFLSALI,PNCODPAG,PNCODAGE,PNFLVABD,PNINICOM,PNFINCOM,PNFLZERO &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           ,this.w_CPROWNUM;
           ,this.w_CPROWORD;
           ,this.w_PNTIPCON;
           ,this.w_PNCODCON;
           ,this.w_PNIMPDAR;
           ,this.w_PNIMPAVE;
           ,this.w_PNFLPART;
           ,this.w_PNDESRIG;
           ,this.w_PNCODCAU;
           ,this.w_PNFLSALD;
           ," ";
           ," ";
           ,this.w_PNCODPAG;
           ,this.w_PNCODAGE;
           ,this.w_PNFLVABD;
           ,this.w_PNINICOM;
           ,this.w_PNFINCOM;
           ,this.w_FLZERO;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_0402E9B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICON
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICON_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLTIPCON"+",SLCODICE"+",SLCODESE"+",SLDARPRO"+",SLAVEPRO"+",SLDARPER"+",SLAVEPER"+",SLDARINI"+",SLAVEINI"+",SLDARFIN"+",SLAVEFIN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNTIPCON),'SALDICON','SLTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'SALDICON','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODESE),'SALDICON','SLCODESE');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARINI');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEINI');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARFIN');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEFIN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLTIPCON',this.w_PNTIPCON,'SLCODICE',this.w_PNCODCON,'SLCODESE',this.w_PNCODESE,'SLDARPRO',0,'SLAVEPRO',0,'SLDARPER',0,'SLAVEPER',0,'SLDARINI',0,'SLAVEINI',0,'SLDARFIN',0,'SLAVEFIN',0)
      insert into (i_cTable) (SLTIPCON,SLCODICE,SLCODESE,SLDARPRO,SLAVEPRO,SLDARPER,SLAVEPER,SLDARINI,SLAVEINI,SLDARFIN,SLAVEFIN &i_ccchkf. );
         values (;
           this.w_PNTIPCON;
           ,this.w_PNCODCON;
           ,this.w_PNCODESE;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrive le Partite associate alla Reg. di Primanota
    * --- Partite da Movimenti Distinta, Insoluto, Saldaconto (ATTENZIONE: Siamo Posizionati dentro il Cursore CONTABIL)
    * --- Dati per Chusura Partite
    this.w_PTTIPCON = TIPCON
    this.w_PTCODCON = CODCON
    this.w_PT_SEGNO = IIF(NVL(SEGNO, "D")="D", "A", "D")
    this.w_PTTOTIMP = TOTIMP
    this.w_PTCODVAL = this.oParentObject.w_CICODVAL
    this.w_PTCAOVAL = this.w_CAOVAC
    this.w_PTCAOAPE = CAOVAL
    this.w_PTMODPAG = MODPAG
    this.w_PTIMPDOC = IMPDOC
    this.w_PTNUMDOC = NUMDOC
    this.w_PTALFDOC = ALFDOC
    this.w_PTDATDOC = DATDOC
    this.w_PTBANNOS = BANNOS
    this.w_PTBANAPP = BANAPP
    this.w_PTFLRAGG = NVL(FLRAGG," ")
    this.w_PTDESRIG = NVL(DESRIG, " ")
    this.w_PNCODAGE = NVL(CODAGE, SPACE(5))
    this.w_PNFLVABD = NVL(FLVABD, " ")
    this.w_PTNUMCOR = NVL(PTNUMCOR, Space(25))
    * --- Riferimenti Partita Originaria
    this.w_OSERIAL = PTSERIAL
    this.w_OROWORD = PTROWORD
    this.w_OROWNUM = CPROWNUM
    * --- Flag Saldato: Se da Insoluto Riapre, altrimenti se da Distinta setta a seconda che vi sia l'Importo saldato
    this.w_PTFLCRSA = "S"
    this.w_PTFLSOSP = " "
    * --- Scrive la Partita di Primanota
    GSCG_BBP(this,"I",this.w_OSERIAL,this.w_OROWORD,this.w_OROWNUM,this.w_PNSERIAL,this.w_PTROWORD,; 
 this.w_PTROWNUM,this.w_PTTOTIMP,0,0," "," ",space(10),this.w_PTCAOVAL,this.w_PTCAOAPE,this.w_DATAPE,this.w_PTDESRIG)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Scrive la Partita 'fittizia' associata alla Distinta di Contabilizzazione Indiretta
    this.w_CPROWNU1 = this.w_CPROWNU1 + 1
    this.w_PT_SEGNO = NVL(SEGNO, "D")
    this.w_PTFLCRSA = "C"
    * --- Insert into PAR_TITE
    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTDATSCA"+",PTNUMPAR"+",PT_SEGNO"+",PTTOTIMP"+",PTCODVAL"+",PTCAOVAL"+",PTMODPAG"+",PTFLCRSA"+",PTTIPCON"+",PTCODCON"+",PTCAOAPE"+",PTNUMDOC"+",PTALFDOC"+",PTDATDOC"+",PTIMPDOC"+",PTFLSOSP"+",PTDATAPE"+",PTBANNOS"+",PTFLRAGG"+",PTFLINDI"+",PTBANAPP"+",PTFLIMPE"+",PTNUMDIS"+",PTDESRIG"+",PTCODAGE"+",PTFLVABD"+",PTSERRIF"+",PTORDRIF"+",PTNUMRIF"+",PTRIFIND"+",PTNUMCOR"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CISERIAL),'PAR_TITE','PTSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(-3),'PAR_TITE','PTROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNU1),'PAR_TITE','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATSCA),'PAR_TITE','PTDATSCA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PT_SEGNO),'PAR_TITE','PT_SEGNO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTTOTIMP),'PAR_TITE','PTTOTIMP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODVAL),'PAR_TITE','PTCODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTCAOVAL),'PAR_TITE','PTCAOVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLCRSA),'PAR_TITE','PTFLCRSA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTTIPCON),'PAR_TITE','PTTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODCON),'PAR_TITE','PTCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTCAOAPE),'PAR_TITE','PTCAOAPE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMDOC),'PAR_TITE','PTNUMDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTALFDOC),'PAR_TITE','PTALFDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATDOC),'PAR_TITE','PTDATDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTIMPDOC),'PAR_TITE','PTIMPDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLSOSP),'PAR_TITE','PTFLSOSP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DATAPE),'PAR_TITE','PTDATAPE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANNOS),'PAR_TITE','PTBANNOS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLRAGG),'PAR_TITE','PTFLRAGG');
      +","+cp_NullLink(cp_ToStrODBC("S"),'PAR_TITE','PTFLINDI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_TITE','PTBANAPP');
      +","+cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTNUMDIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTDESRIG),'PAR_TITE','PTDESRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODAGE),'PAR_TITE','PTCODAGE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLVABD),'PAR_TITE','PTFLVABD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OSERIAL),'PAR_TITE','PTSERRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OROWORD),'PAR_TITE','PTORDRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OROWNUM),'PAR_TITE','PTNUMRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RIFINDI),'PAR_TITE','PTRIFIND');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMCOR),'PAR_TITE','PTNUMCOR');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.oParentObject.w_CISERIAL,'PTROWORD',-3,'CPROWNUM',this.w_CPROWNU1,'PTDATSCA',this.w_PTDATSCA,'PTNUMPAR',this.w_PTNUMPAR,'PT_SEGNO',this.w_PT_SEGNO,'PTTOTIMP',this.w_PTTOTIMP,'PTCODVAL',this.w_PTCODVAL,'PTCAOVAL',this.w_PTCAOVAL,'PTMODPAG',this.w_PTMODPAG,'PTFLCRSA',this.w_PTFLCRSA,'PTTIPCON',this.w_PTTIPCON)
      insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTDATSCA,PTNUMPAR,PT_SEGNO,PTTOTIMP,PTCODVAL,PTCAOVAL,PTMODPAG,PTFLCRSA,PTTIPCON,PTCODCON,PTCAOAPE,PTNUMDOC,PTALFDOC,PTDATDOC,PTIMPDOC,PTFLSOSP,PTDATAPE,PTBANNOS,PTFLRAGG,PTFLINDI,PTBANAPP,PTFLIMPE,PTNUMDIS,PTDESRIG,PTCODAGE,PTFLVABD,PTSERRIF,PTORDRIF,PTNUMRIF,PTRIFIND,PTNUMCOR &i_ccchkf. );
         values (;
           this.oParentObject.w_CISERIAL;
           ,-3;
           ,this.w_CPROWNU1;
           ,this.w_PTDATSCA;
           ,this.w_PTNUMPAR;
           ,this.w_PT_SEGNO;
           ,this.w_PTTOTIMP;
           ,this.w_PTCODVAL;
           ,this.w_PTCAOVAL;
           ,this.w_PTMODPAG;
           ,this.w_PTFLCRSA;
           ,this.w_PTTIPCON;
           ,this.w_PTCODCON;
           ,this.w_PTCAOAPE;
           ,this.w_PTNUMDOC;
           ,this.w_PTALFDOC;
           ,this.w_PTDATDOC;
           ,this.w_PTIMPDOC;
           ,this.w_PTFLSOSP;
           ,this.w_DATAPE;
           ,this.w_PTBANNOS;
           ,this.w_PTFLRAGG;
           ,"S";
           ,this.w_PTBANAPP;
           ,"  ";
           ," ";
           ,this.w_PTDESRIG;
           ,this.w_PNCODAGE;
           ,this.w_PNFLVABD;
           ,this.w_OSERIAL;
           ,this.w_OROWORD;
           ,this.w_OROWNUM;
           ,this.w_RIFINDI;
           ,this.w_PTNUMCOR;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Seleziono le partite in distinta al Dopo Incasso (PTFLIMPE='DI') che punta alla solita partita di creazione (medesimo PTSERRIF, PTORDRIF, PTNUMRIF)
    *     e PTFLINDI=' ' (Distinta senza contabilizzazione Indiretta Effetti).
    *     Le partite individuate non punteranno pi� alle partite di creazione ma 
    *     alla partita di contabilizzazione indiretta (PTROWORD=-3)
    * --- Write into PAR_TITE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PTSERRIF ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CISERIAL),'PAR_TITE','PTSERRIF');
      +",PTORDRIF ="+cp_NullLink(cp_ToStrODBC(-3),'PAR_TITE','PTORDRIF');
      +",PTNUMRIF ="+cp_NullLink(cp_ToStrODBC(this.w_CPROWNU1),'PAR_TITE','PTNUMRIF');
      +",PTFLINDI ="+cp_NullLink(cp_ToStrODBC("S"),'PAR_TITE','PTFLINDI');
          +i_ccchkf ;
      +" where ";
          +"PTSERRIF = "+cp_ToStrODBC(this.w_OSERIAL);
          +" and PTORDRIF = "+cp_ToStrODBC(this.w_OROWORD);
          +" and PTNUMRIF = "+cp_ToStrODBC(this.w_OROWNUM);
          +" and PTROWORD = "+cp_ToStrODBC(-2);
          +" and PTFLIMPE = "+cp_ToStrODBC("DI");
          +" and PTFLINDI = "+cp_ToStrODBC(SPACE(1));
             )
    else
      update (i_cTable) set;
          PTSERRIF = this.oParentObject.w_CISERIAL;
          ,PTORDRIF = -3;
          ,PTNUMRIF = this.w_CPROWNU1;
          ,PTFLINDI = "S";
          &i_ccchkf. ;
       where;
          PTSERRIF = this.w_OSERIAL;
          and PTORDRIF = this.w_OROWORD;
          and PTNUMRIF = this.w_OROWNUM;
          and PTROWORD = -2;
          and PTFLIMPE = "DI";
          and PTFLINDI = SPACE(1);

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Se la Write scrive almeno un record, ho finito l'aggoirnamento riferimenti.
    *     Se la write non scrive, o non ho nessuna distinta al dopo incasso, o la 
    *     distinta al dopo incasso � stata creata con una release precedente alla2.2
    *     (Senza riferimenti)
    if I_ROWS=0
      * --- Recupera le partite per numero partita, data scadenza, tipo conto, codice conto, codice valuta
      *     provenienti da una Distinta al dopo incasso creata con una release precedente alla 2.2
      * --- Cerco le partite di segno opposto a quella che sto scrivendo legata alla primanota.
      this.w_SEGNO = IIF(this.w_PT_SEGNO="D", "A", "D")
      * --- Select from PAR_TITE
      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select PTSERIAL,PTROWORD,CPROWNUM  from "+i_cTable+" PAR_TITE ";
            +" where PTNUMPAR="+cp_ToStrODBC(this.w_PTNUMPAR)+" AND PTDATSCA="+cp_ToStrODBC(this.w_PTDATSCA)+" AND PTTIPCON="+cp_ToStrODBC(this.w_PTTIPCON)+" AND PTCODCON="+cp_ToStrODBC(this.w_PTCODCON)+" AND PTCODVAL="+cp_ToStrODBC(this.w_PTCODVAL)+" AND PTROWORD=-2 AND PTFLIMPE='DI' AND PT_SEGNO="+cp_ToStrODBC(this.w_SEGNO)+" AND PTFLINDI=' ' AND PTNUMRIF=0";
             ,"_Curs_PAR_TITE")
      else
        select PTSERIAL,PTROWORD,CPROWNUM from (i_cTable);
         where PTNUMPAR=this.w_PTNUMPAR AND PTDATSCA=this.w_PTDATSCA AND PTTIPCON=this.w_PTTIPCON AND PTCODCON=this.w_PTCODCON AND PTCODVAL=this.w_PTCODVAL AND PTROWORD=-2 AND PTFLIMPE="DI" AND PT_SEGNO=this.w_SEGNO AND PTFLINDI=" " AND PTNUMRIF=0;
          into cursor _Curs_PAR_TITE
      endif
      if used('_Curs_PAR_TITE')
        select _Curs_PAR_TITE
        locate for 1=1
        do while not(eof())
        this.w_SERIAL = _Curs_PAR_TITE.PTSERIAL
        this.w_ROWORD = _Curs_PAR_TITE.PTROWORD
        this.w_ROWNUM = _Curs_PAR_TITE.CPROWNUM
        * --- Scrivo nella partita selezionata il PTFLINDI='S' per evitare che sia visibile in manutenzione distinte
        * --- Write into PAR_TITE
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PAR_TITE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PTFLINDI ="+cp_NullLink(cp_ToStrODBC("S"),'PAR_TITE','PTFLINDI');
          +",PTSERRIF ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CISERIAL),'PAR_TITE','PTSERRIF');
          +",PTORDRIF ="+cp_NullLink(cp_ToStrODBC(-3),'PAR_TITE','PTORDRIF');
          +",PTNUMRIF ="+cp_NullLink(cp_ToStrODBC(this.w_CPROWNU1),'PAR_TITE','PTNUMRIF');
              +i_ccchkf ;
          +" where ";
              +"PTSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
              +" and PTROWORD = "+cp_ToStrODBC(this.w_ROWORD);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
                 )
        else
          update (i_cTable) set;
              PTFLINDI = "S";
              ,PTSERRIF = this.oParentObject.w_CISERIAL;
              ,PTORDRIF = -3;
              ,PTNUMRIF = this.w_CPROWNU1;
              &i_ccchkf. ;
           where;
              PTSERIAL = this.w_SERIAL;
              and PTROWORD = this.w_ROWORD;
              and CPROWNUM = this.w_ROWNUM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
          select _Curs_PAR_TITE
          continue
        enddo
        use
      endif
    endif
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creo le Testate di primanota
    * --- Calcola PNSERIAL e PNNUMRER
    this.w_DESSUP = ""
    this.w_PNSERIAL = SPACE(10)
    this.w_PNNUMRER = 0
    this.w_CPROWNUM = 0
    this.w_CPROWORD = 0
    this.w_PNNUMPRO = 0
    this.w_PNPRP = "NN"
    i_Conn=i_TableProp[this.PNT_MAST_IDX, 3]
    cp_NextTableProg(this, i_Conn, "SEPNT", "i_codazi,w_PNSERIAL")
    cp_NextTableProg(this, i_Conn, "PRPNT", "i_codazi,w_PNCODESE,w_PNCODUTE,w_PNPRG,w_PNNUMRER")
    * --- Protocollo (se Gestito)
    if NOT EMPTY(this.w_PNANNPRO)
      cp_NextTableProg(this, i_Conn, "PRPRO", "i_codazi,w_PNANNPRO,w_PNPRP,w_PNALFPRO,w_PNNUMPRO")
    endif
    * --- Segno - per distinguerla dalle Distinte Effetti
    this.w_PNRIFDIS = "-"+RIGHT(this.oParentObject.w_CISERIAL, 9)
    this.w_CODVAP = this.w_PNCODVAL
    this.w_CAOVAP = this.w_PNCAOVAL
    if NOT EMPTY(this.oParentObject.w_DTOBSO) AND this.oParentObject.w_DTOBSO<=this.oParentObject.w_CIDATVAL
      * --- Vauta EMU Obsoleta ; Converte in EURO
      this.w_CODVAP = g_CODEUR
      this.w_CAOVAP = 1
    endif
    if (Not Empty(this.w_CCDESSUP) OR Not Empty(this.w_CCDESRIG))
      * --- Array elenco parametri per descrizioni di riga e testata
       
 ARPARAM[1,1]="NUMDOC" 
 ARPARAM[1,2]=ALLTRIM(STR(this.oParentObject.w_CINUMERO)) 
 ARPARAM[2,1]="ALFDOC" 
 ARPARAM[2,2]="" 
 ARPARAM[3,1]="DATDOC" 
 ARPARAM[3,2]=DTOC(this.oParentObject.w_CIDATVAL) 
 ARPARAM[4,1]="ALFPRO" 
 ARPARAM[4,2]=Alltrim(this.w_PNALFPRO) 
 ARPARAM[5,1]="DATREG" 
 ARPARAM[5,2]=DTOC(this.w_PNDATREG) 
 ARPARAM[6,1]="CODCON" 
 ARPARAM[6,2]="" 
 ARPARAM[7,1]="ELEDOC" 
 ARPARAM[7,2]="" 
 ARPARAM[8,1]="CODAGE" 
 ARPARAM[8,2]="" 
 ARPARAM[9,1]="NUMPRO" 
 ARPARAM[9,2]=ALLTRIM(STR(this.w_PNNUMPRO)) 
 ARPARAM[10,1]="NUMPAR" 
 ARPARAM[10,2]="" 
 ARPARAM[11,1]="DATSCA" 
 ARPARAM[11,2]=ALLTRIM(DTOC(DATSCA)) 
 ARPARAM[12,1]="TIPCON" 
 ARPARAM[12,2]=""
      this.w_DESSUP = CALDESPA(this.w_CCDESSUP,@ARPARAM)
      * --- Nel caso di singola registrazione aggiungo parte parametrica in coda all'eventuale
      *     descrizione indicata per mantenere precedente costruzione:
      *     IIF(NOT EMPTY(ALLTRIM(DTOC(DATSCA))), 'Data. Scad:','')+ ALLTRIM(DTOC(DATSCA))
      this.w_DESSUP = LEFT(ALLTRIM(this.oParentObject.w_CIDESCRI) + IIF(NOT EMPTY(ALLTRIM(this.oParentObject.w_CIDESCRI)), ", ", "") + Alltrim(this.w_DESSUP), 50)
      this.w_DESSUP = ALLTRIM(iif(right(ALLTRIM(this.w_DESSUP),1)=",",STRTRAN(this.w_DESSUP,",",""),this.w_DESSUP))
    endif
    if ! this.oParentObject.w_CIFLSIPA="S" or Empty(this.w_DESSUP)
      this.w_DESSUP = this.oParentObject.w_CIDESCRI
    endif
    * --- Insert into PNT_MAST
    i_nConn=i_TableProp[this.PNT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PNSERIAL"+",PNFLIVDF"+",PNNUMREG"+",PNTIPDOC"+",PNDATREG"+",PNPRD"+",PNCODCAU"+",PNTIPREG"+",PNCODVAL"+",PNPRG"+",PNCAOVAL"+",PNDESSUP"+",PNCOMPET"+",PNVALNAZ"+",UTCC"+",UTCV"+",UTDC"+",UTDV"+",PNCODESE"+",PNCODUTE"+",PNNUMRER"+",PNDATDOC"+",PNNUMDOC"+",PNALFDOC"+",PNFLPROV"+",PNPRP"+",PNANNDOC"+",PNANNPRO"+",PNALFPRO"+",PNNUMPRO"+",PNRIFDIS"+",PNCOMIVA"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_MAST','PNSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_MAST','PNFLIVDF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMREG),'PNT_MAST','PNNUMREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPDOC),'PNT_MAST','PNTIPDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PNT_MAST','PNDATREG');
      +","+cp_NullLink(cp_ToStrODBC("NN"),'PNT_MAST','PNPRD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCAU),'PNT_MAST','PNCODCAU');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPREG),'PNT_MAST','PNTIPREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODVAP),'PNT_MAST','PNCODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNPRG),'PNT_MAST','PNPRG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAOVAP),'PNT_MAST','PNCAOVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESSUP),'PNT_MAST','PNDESSUP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODESE),'PNT_MAST','PNCOMPET');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNVALNAZ),'PNT_MAST','PNVALNAZ');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UTCC),'PNT_MAST','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UTCV),'PNT_MAST','UTCV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UTDC),'PNT_MAST','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UTDV),'PNT_MAST','UTDV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODESE),'PNT_MAST','PNCODESE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODUTE),'PNT_MAST','PNCODUTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMRER),'PNT_MAST','PNNUMRER');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CIDATVAL),'PNT_MAST','PNDATDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CINUMERO),'PNT_MAST','PNNUMDOC');
      +","+cp_NullLink(cp_ToStrODBC("  "),'PNT_MAST','PNALFDOC');
      +","+cp_NullLink(cp_ToStrODBC("N"),'PNT_MAST','PNFLPROV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNPRP),'PNT_MAST','PNPRP');
      +","+cp_NullLink(cp_ToStrODBC("    "),'PNT_MAST','PNANNDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNANNPRO),'PNT_MAST','PNANNPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNALFPRO),'PNT_MAST','PNALFPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMPRO),'PNT_MAST','PNNUMPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNRIFDIS),'PNT_MAST','PNRIFDIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PNT_MAST','PNCOMIVA');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL,'PNFLIVDF'," ",'PNNUMREG',this.w_PNNUMREG,'PNTIPDOC',this.w_PNTIPDOC,'PNDATREG',this.w_PNDATREG,'PNPRD',"NN",'PNCODCAU',this.w_PNCODCAU,'PNTIPREG',this.w_PNTIPREG,'PNCODVAL',this.w_CODVAP,'PNPRG',this.w_PNPRG,'PNCAOVAL',this.w_CAOVAP,'PNDESSUP',this.w_DESSUP)
      insert into (i_cTable) (PNSERIAL,PNFLIVDF,PNNUMREG,PNTIPDOC,PNDATREG,PNPRD,PNCODCAU,PNTIPREG,PNCODVAL,PNPRG,PNCAOVAL,PNDESSUP,PNCOMPET,PNVALNAZ,UTCC,UTCV,UTDC,UTDV,PNCODESE,PNCODUTE,PNNUMRER,PNDATDOC,PNNUMDOC,PNALFDOC,PNFLPROV,PNPRP,PNANNDOC,PNANNPRO,PNALFPRO,PNNUMPRO,PNRIFDIS,PNCOMIVA &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           ," ";
           ,this.w_PNNUMREG;
           ,this.w_PNTIPDOC;
           ,this.w_PNDATREG;
           ,"NN";
           ,this.w_PNCODCAU;
           ,this.w_PNTIPREG;
           ,this.w_CODVAP;
           ,this.w_PNPRG;
           ,this.w_CAOVAP;
           ,this.w_DESSUP;
           ,this.w_PNCODESE;
           ,this.w_PNVALNAZ;
           ,this.w_UTCC;
           ,this.w_UTCV;
           ,this.w_UTDC;
           ,this.w_UTDV;
           ,this.w_PNCODESE;
           ,this.w_PNCODUTE;
           ,this.w_PNNUMRER;
           ,this.oParentObject.w_CIDATVAL;
           ,this.oParentObject.w_CINUMERO;
           ,"  ";
           ,"N";
           ,this.w_PNPRP;
           ,"    ";
           ,this.w_PNANNPRO;
           ,this.w_PNALFPRO;
           ,this.w_PNNUMPRO;
           ,this.w_PNRIFDIS;
           ,this.w_PNDATREG;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
  endproc


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creo il Dettaglio delle Righe di primanota
    SELECT CONTABIL
    this.w_CAOVAL = IIF(NVL(CAOVAL,0)=0, this.w_CAONAZ, CAOVAL)
    this.w_DATAPE = IIF(EMPTY(CP_TODATE(DATAPE)), IIF(EMPTY(CP_TODATE(DATDOC)), DATSCA, DATDOC), DATAPE)
    this.w_DATAPE = CP_TODATE(this.w_DATAPE)
    this.w_PTDATSCA = CP_TODATE(DATSCA)
    this.w_PTNUMPAR = NUMPAR
    * --- Calcola Differenza Cambio (Passiva in Dare; Attiva in Avere) (PTCAOVAL=C.Apertura; w_CAOVAC=C.Chiusura)
    if this.w_EURVAL=0 OR this.w_PNDATREG<GETVALUT(g_PERVAL, "VADATEUR")
      this.w_APPO1 = TOTIMP
      * --- Fase Pre EURO o Valuta non EURO; Calcola Differenze Cambi
      this.w_VAL1 = VAL2MON(this.w_APPO1, this.w_CAOVAL, this.w_CAONAZ, this.w_DATAPE, this.w_PNVALNAZ)
      this.w_VAL2 = VAL2MON(this.w_APPO1, this.w_CAOVAC, this.w_CAONAZ, this.w_PNDATREG, this.w_PNVALNAZ)
      this.w_APPO1 = cp_ROUND(this.w_VAL1 - this.w_VAL2, 6)
      if NVL(SEGNO, "D")="D"
        this.w_IMPDCA = this.w_IMPDCA - IIF(this.w_APPO1>0, 0, ABS(this.w_APPO1))
        this.w_IMPDCP = this.w_IMPDCP + IIF(this.w_APPO1>0, ABS(this.w_APPO1), 0)
      else
        this.w_IMPDCA = this.w_IMPDCA - IIF(this.w_APPO1>0, ABS(this.w_APPO1), 0)
        this.w_IMPDCP = this.w_IMPDCP + IIF(this.w_APPO1>0, 0, ABS(this.w_APPO1))
      endif
      SELECT CONTABIL
    endif
    * --- Dettaglia Righe Cli/For
    if (this.w_TIPCON<>TIPCON OR this.w_CODCON<>CODCON) OR this.oParentObject.w_CIFLSIPA="S"
      this.w_IMPCLF = cp_ROUND(this.w_IMPCLF, this.w_PERPVL)
      if this.w_IMPCLF<>0
        * --- Se ci sono dei valori (o e' la prima volta): Nuovo Conto:
        * --- Aggiorna Importo Riga Clienti/Fornitori e Inizializzo i Valori
        this.w_PNIMPDAR = IIF(this.w_IMPCLF>0, this.w_IMPCLF, 0)
        this.w_PNIMPAVE = IIF(this.w_IMPCLF<0, ABS(this.w_IMPCLF), 0)
        this.w_FLZERO = IIF( this.w_IMPCLF=0 , "S", " " )
        * --- Write into PNT_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PNT_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PNIMPDAR ="+cp_NullLink(cp_ToStrODBC(this.w_PNIMPDAR),'PNT_DETT','PNIMPDAR');
          +",PNIMPAVE ="+cp_NullLink(cp_ToStrODBC(this.w_PNIMPAVE),'PNT_DETT','PNIMPAVE');
          +",PNFLZERO ="+cp_NullLink(cp_ToStrODBC(this.w_FLZERO),'PNT_DETT','PNFLZERO');
              +i_ccchkf ;
          +" where ";
              +"PNSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_SALROW);
                 )
        else
          update (i_cTable) set;
              PNIMPDAR = this.w_PNIMPDAR;
              ,PNIMPAVE = this.w_PNIMPAVE;
              ,PNFLZERO = this.w_FLZERO;
              &i_ccchkf. ;
           where;
              PNSERIAL = this.w_PNSERIAL;
              and CPROWNUM = this.w_SALROW;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        if this.oParentObject.w_CIFLSIPA="S"
          this.w_SQUAD = (this.w_PNIMPDAR-this.w_PNIMPAVE)
        else
          this.w_SQUAD = this.w_SQUAD + (this.w_PNIMPDAR-this.w_PNIMPAVE)
        endif
        * --- Aggiorna Saldo
        * --- Write into SALDICON
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDICON_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICON_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLDARPER =SLDARPER+ "+cp_ToStrODBC(this.w_PNIMPDAR);
          +",SLAVEPER =SLAVEPER+ "+cp_ToStrODBC(this.w_PNIMPAVE);
              +i_ccchkf ;
          +" where ";
              +"SLTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
              +" and SLCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
              +" and SLCODESE = "+cp_ToStrODBC(this.w_PNCODESE);
                 )
        else
          update (i_cTable) set;
              SLDARPER = SLDARPER + this.w_PNIMPDAR;
              ,SLAVEPER = SLAVEPER + this.w_PNIMPAVE;
              &i_ccchkf. ;
           where;
              SLTIPCON = this.w_PNTIPCON;
              and SLCODICE = this.w_PNCODCON;
              and SLCODESE = this.w_PNCODESE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Inizializza Totali
        this.w_IMPCLF = 0
        SELECT CONTABIL
      endif
      this.w_TIPCON = TIPCON
      this.w_CODCON = CODCON
      * --- Scrive nuova Riga Clienti/Fornitori
      this.w_PNTIPCON = this.w_TIPCON
      this.w_PNCODCON = this.w_CODCON
      this.w_PNCODPAG = NVL(CODPAG, SPACE(5))
      this.w_PNCODAGE = NVL(CODAGE, SPACE(5))
      this.w_PNFLVABD = NVL(FLVABD, " ")
      this.w_PNFLPART = "S"
      this.w_APPO = 0
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      SELECT CONTABIL
      * --- Salva Il Puntatore alla Riga P.N. per aggiornare alla Fine il Totale di Riga
      this.w_SALROW = this.w_CPROWNUM
      * --- Riazzera Progressivo Partite
      this.w_PARROW = 0
    endif
    * --- Totalizza Importo Riga Cliente/Fornitore
    this.w_APPO1 = TOTIMP * IIF(NVL(SEGNO, "D")="A", 1, -1)
    if this.w_PNCODVAL<>this.w_PNVALNAZ
      this.w_APPO1 = cp_ROUND(VAL2MON(this.w_APPO1, this.w_CAOVAL, this.w_CAONAZ, this.w_DATAPE, this.w_PNVALNAZ), 6)
      SELECT CONTABIL
    endif
    if this.oParentObject.w_CIFLSIPA="S"
      this.w_IMPCLF = this.w_APPO1
      this.w_IMPBAN = this.w_APPO1
    else
      this.w_IMPCLF = this.w_IMPCLF + this.w_APPO1
      this.w_IMPBAN = this.w_IMPBAN + this.w_APPO1
    endif
    * --- Scrive la Partita di Chiusura Associata al Cliente/Fornitore
    this.w_PARROW = this.w_PARROW + 1
    this.w_PTROWORD = this.w_SALROW
    this.w_PTROWNUM = this.w_PARROW
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_PNCODAGE = SPACE(5)
    this.w_PNFLVABD = " "
  endproc


  procedure Pag7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Dettaglia Righe Cli/For (Chiude ultima Riga)
    this.w_IMPCLF = cp_ROUND(this.w_IMPCLF, this.w_PERPVL)
    if this.w_IMPCLF<>0
      this.w_PNIMPDAR = IIF(this.w_IMPCLF>0, this.w_IMPCLF, 0)
      this.w_PNIMPAVE = IIF(this.w_IMPCLF<0, ABS(this.w_IMPCLF), 0)
      this.w_FLZERO = IIF( this.w_IMPCLF=0 , "S", " " )
      * --- Write into PNT_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PNT_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PNIMPDAR ="+cp_NullLink(cp_ToStrODBC(this.w_PNIMPDAR),'PNT_DETT','PNIMPDAR');
        +",PNIMPAVE ="+cp_NullLink(cp_ToStrODBC(this.w_PNIMPAVE),'PNT_DETT','PNIMPAVE');
        +",PNFLZERO ="+cp_NullLink(cp_ToStrODBC(this.w_FLZERO),'PNT_DETT','PNFLZERO');
            +i_ccchkf ;
        +" where ";
            +"PNSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_SALROW);
               )
      else
        update (i_cTable) set;
            PNIMPDAR = this.w_PNIMPDAR;
            ,PNIMPAVE = this.w_PNIMPAVE;
            ,PNFLZERO = this.w_FLZERO;
            &i_ccchkf. ;
         where;
            PNSERIAL = this.w_PNSERIAL;
            and CPROWNUM = this.w_SALROW;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      if this.oParentObject.w_CIFLSIPA="S"
        this.w_SQUAD = (this.w_PNIMPDAR-this.w_PNIMPAVE)
      else
        this.w_SQUAD = this.w_SQUAD + (this.w_PNIMPDAR-this.w_PNIMPAVE)
      endif
      * --- Aggiorna Saldo
      * --- Write into SALDICON
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDICON_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICON_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SLDARPER =SLDARPER+ "+cp_ToStrODBC(this.w_PNIMPDAR);
        +",SLAVEPER =SLAVEPER+ "+cp_ToStrODBC(this.w_PNIMPAVE);
            +i_ccchkf ;
        +" where ";
            +"SLTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
            +" and SLCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
            +" and SLCODESE = "+cp_ToStrODBC(this.w_PNCODESE);
               )
      else
        update (i_cTable) set;
            SLDARPER = SLDARPER + this.w_PNIMPDAR;
            ,SLAVEPER = SLAVEPER + this.w_PNIMPAVE;
            &i_ccchkf. ;
         where;
            SLTIPCON = this.w_PNTIPCON;
            and SLCODICE = this.w_PNCODCON;
            and SLCODESE = this.w_PNCODESE;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      this.w_IMPCLF = 0
    endif
    * --- Scrive Reg.Contabili (Movimentazione)
    this.w_PNFLPART = "N"
    this.w_PNTIPCON = "G"
    this.w_PNCODPAG = SPACE(5)
    this.w_PNCODAGE = SPACE(5)
    this.w_PNFLVABD = " "
    * --- Importo Riga Effetti Attivi: Totale Clienti - (Diff.Cambio Attiva + Passiva)
    this.w_IMPBAN = cp_ROUND(this.w_IMPBAN, this.w_PERPVL)
    this.w_IMPDCA = cp_ROUND(this.w_IMPDCA, this.w_PERPVL)
    this.w_IMPDCP = cp_ROUND(this.w_IMPDCP, this.w_PERPVL)
    this.w_IMPBAN = (this.w_IMPBAN + (this.w_IMPDCA + this.w_IMPDCP)) * -1
    * --- Differenza Cambi Attiva
    if this.w_IMPDCA<>0
      this.w_PNCODCON = this.oParentObject.w_CICONDCA
      this.w_APPO = this.w_IMPDCA
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Differenza Cambi Passiva
    if this.w_IMPDCP<>0
      this.w_PNCODCON = this.oParentObject.w_CICONDCP
      this.w_APPO = this.w_IMPDCP
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- A Credito: Dare / a Debito: Avere
    if this.w_IMPBAN<>0
      this.w_PNCODCON = this.oParentObject.w_CICONEFA
      this.w_APPO = this.w_IMPBAN
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Genero Relativi Movimenti C\C
      if g_BANC="S" AND this.w_FLMOVC="S"
        * --- Se presente modulo C\C e la causale contabile movimenta i C\C
        this.w_CONBAN = this.w_PTBANNOS
        * --- Read from COC_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.COC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2],.t.,this.COC_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "BACONCOL,BACALFES,BATIPCON,BACODVAL"+;
            " from "+i_cTable+" COC_MAST where ";
                +"BACODBAN = "+cp_ToStrODBC(this.w_CONBAN);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            BACONCOL,BACALFES,BATIPCON,BACODVAL;
            from (i_cTable) where;
                BACODBAN = this.w_CONBAN;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_NUMCOR = NVL(cp_ToDate(_read_.BACONCOL),cp_NullValue(_read_.BACONCOL))
          this.w_CALFES = NVL(cp_ToDate(_read_.BACALFES),cp_NullValue(_read_.BACALFES))
          this.w_TIPBAN = NVL(cp_ToDate(_read_.BATIPCON),cp_NullValue(_read_.BATIPCON))
          this.w_CODVAL = NVL(cp_ToDate(_read_.BACODVAL),cp_NullValue(_read_.BACODVAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Select from gscg_bmc
        do vq_exec with 'gscg_bmc',this,'_Curs_gscg_bmc','',.f.,.t.
        if used('_Curs_gscg_bmc')
          select _Curs_gscg_bmc
          locate for 1=1
          do while not(eof())
          this.w_TIPCAU = _Curs_gscg_bmc.CATIPCON
          this.w_FLCRDE = _Curs_gscg_bmc.CAFLCRDE
            select _Curs_gscg_bmc
            continue
          enddo
          use
        endif
        if this.w_CODVAL<>this.w_PNCODVAL
          this.w_FLERR = .T.
          * --- Raise
          i_Error="Valuta del conto corrente incongruente con la valuta della partita"
          return
        else
          * --- Read from CCC_DETT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CCC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CCC_DETT_idx,2],.t.,this.CCC_DETT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CANUMCOR"+;
              " from "+i_cTable+" CCC_DETT where ";
                  +"CACODICE = "+cp_ToStrODBC(this.w_CAUMOV);
                  +" and CANUMCOR = "+cp_ToStrODBC(this.w_CONBAN);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CANUMCOR;
              from (i_cTable) where;
                  CACODICE = this.w_CAUMOV;
                  and CANUMCOR = this.w_CONBAN;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CONBAN = NVL(cp_ToDate(_read_.CANUMCOR),cp_NullValue(_read_.CANUMCOR))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_ROWS=0
            this.w_FLERR = .T.
            * --- Raise
            i_Error="Conto corrente incongruente con la causale movimenti C\C"
            return
          else
            this.w_APPVAL = this.w_IMPBAN
            this.w_IMPDEB = IIF(this.w_PTTIPCON="C",0,this.w_APPVAL)
            this.w_IMPCRE = IIF(this.w_PTTIPCON="C",this.w_APPVAL,0)
            this.w_CCFLCRED = IIF(this.w_FLCRDE="C", "+", " ")
            this.w_CCFLDEBI = IIF(this.w_FLCRDE="D", "+", " ")
            this.w_CCFLCOMM = "+"
            this.w_DATVAL = CALCFEST(this.w_PNDATREG, this.w_CALFES, this.w_CONBAN, this.w_CAUMOV, this.w_TIPCAU, 1)
            * --- Insert into CCM_DETT
            i_nConn=i_TableProp[this.CCM_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CCM_DETT_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CCM_DETT_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"CCSERIAL"+",CCROWRIF"+",CCCODCAU"+",CCNUMCOR"+",CCDATVAL"+",CCIMPCRE"+",CCIMPDEB"+",CPROWNUM"+",CPROWORD"+",CCFLDEBI"+",CCFLCRED"+",CCFLCOMM"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'CCM_DETT','CCSERIAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'CCM_DETT','CCROWRIF');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CAUMOV),'CCM_DETT','CCCODCAU');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CONBAN),'CCM_DETT','CCNUMCOR');
              +","+cp_NullLink(cp_ToStrODBC(this.w_DATVAL),'CCM_DETT','CCDATVAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IMPCRE),'CCM_DETT','CCIMPCRE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IMPDEB),'CCM_DETT','CCIMPDEB');
              +","+cp_NullLink(cp_ToStrODBC(1),'CCM_DETT','CPROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(10),'CCM_DETT','CPROWORD');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLDEBI),'CCM_DETT','CCFLDEBI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLCRED),'CCM_DETT','CCFLCRED');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLCOMM),'CCM_DETT','CCFLCOMM');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'CCSERIAL',this.w_PNSERIAL,'CCROWRIF',this.w_CPROWNUM,'CCCODCAU',this.w_CAUMOV,'CCNUMCOR',this.w_CONBAN,'CCDATVAL',this.w_DATVAL,'CCIMPCRE',this.w_IMPCRE,'CCIMPDEB',this.w_IMPDEB,'CPROWNUM',1,'CPROWORD',10,'CCFLDEBI',this.w_CCFLDEBI,'CCFLCRED',this.w_CCFLCRED,'CCFLCOMM',this.w_CCFLCOMM)
              insert into (i_cTable) (CCSERIAL,CCROWRIF,CCCODCAU,CCNUMCOR,CCDATVAL,CCIMPCRE,CCIMPDEB,CPROWNUM,CPROWORD,CCFLDEBI,CCFLCRED,CCFLCOMM &i_ccchkf. );
                 values (;
                   this.w_PNSERIAL;
                   ,this.w_CPROWNUM;
                   ,this.w_CAUMOV;
                   ,this.w_CONBAN;
                   ,this.w_DATVAL;
                   ,this.w_IMPCRE;
                   ,this.w_IMPDEB;
                   ,1;
                   ,10;
                   ,this.w_CCFLDEBI;
                   ,this.w_CCFLCRED;
                   ,this.w_CCFLCOMM;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
            * --- Aggiorno Saldi C\C
            * --- Write into COC_MAST
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.COC_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.COC_MAST_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"BASALCRE =BASALCRE+ "+cp_ToStrODBC(this.w_IMPCRE);
              +",BASALDEB =BASALDEB+ "+cp_ToStrODBC(this.w_IMPDEB);
                  +i_ccchkf ;
              +" where ";
                  +"BACODBAN = "+cp_ToStrODBC(this.w_CONBAN);
                     )
            else
              update (i_cTable) set;
                  BASALCRE = BASALCRE + this.w_IMPCRE;
                  ,BASALDEB = BASALDEB + this.w_IMPDEB;
                  &i_ccchkf. ;
               where;
                  BACODBAN = this.w_CONBAN;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        endif
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,13)]
    this.cWorkTables[1]='CAU_CONT'
    this.cWorkTables[2]='ESERCIZI'
    this.cWorkTables[3]='VALUTE'
    this.cWorkTables[4]='PNT_DETT'
    this.cWorkTables[5]='SALDICON'
    this.cWorkTables[6]='PNT_MAST'
    this.cWorkTables[7]='PAR_TITE'
    this.cWorkTables[8]='CON_INDI'
    this.cWorkTables[9]='AZIENDA'
    this.cWorkTables[10]='CONTI'
    this.cWorkTables[11]='COC_MAST'
    this.cWorkTables[12]='CCM_DETT'
    this.cWorkTables[13]='CCC_DETT'
    return(this.OpenAllTables(13))

  proc CloseCursors()
    if used('_Curs_ESERCIZI')
      use in _Curs_ESERCIZI
    endif
    if used('_Curs_PAR_TITE')
      use in _Curs_PAR_TITE
    endif
    if used('_Curs_gscg_bmc')
      use in _Curs_gscg_bmc
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
