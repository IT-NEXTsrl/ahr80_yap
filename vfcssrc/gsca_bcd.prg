* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsca_bcd                                                        *
*              Bilancio per voce c./C.R. e conto                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_56]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-22                                                      *
* Last revis.: 2007-11-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsca_bcd",oParentObject)
return(i_retval)

define class tgsca_bcd as StdBatch
  * --- Local variables
  w_SIMBOLO = space(5)
  * --- WorkFile variables
  VALUTE_idx=0
  RIPATMP1_idx=0
  TMP_ANA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa Bilancio per Voce, C./C.R. e Conto(da GSCA_SCP)
    *     GSCA_SCP.VQR => Manuali Originari
    *     GSCA1SCP.VQR => Primanota Originari
    *     GSCA2SCP.VQR => Documenti Originari
    *     GSCA4SCP.VQR => Primanota Ripartiti
    *     GSCA5SCP.VQR => Documenti Ripartiti
    *     GSCA6SCP.VQR => Manuali Ripartiti
    if empty( this.oParentObject.w_DATA1 ) OR empty( this.oParentObject.w_DATA2 )
      ah_ErrorMsg("Intervallo di date non valido",,"")
      i_retcode = 'stop'
      return
    endif
    * --- Creazione Tabella Temporanea
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('query\gsca_scp',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- sistemazione del conto contabile (contropartita) per i documenti, combinando la categoria contabile articolo con la categoria contabile cliente/fornitore
    * --- Write into RIPATMP1
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.RIPATMP1_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="CHIAVE1,CHIAVE2,CHIAVE3,TIPO,ORIGINE,MRCODICE"
      do vq_exec with 'gsca7scp',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.RIPATMP1_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="RIPATMP1.CHIAVE1 = _t2.CHIAVE1";
              +" and "+"RIPATMP1.CHIAVE2 = _t2.CHIAVE2";
              +" and "+"RIPATMP1.CHIAVE3 = _t2.CHIAVE3";
              +" and "+"RIPATMP1.TIPO = _t2.TIPO";
              +" and "+"RIPATMP1.ORIGINE = _t2.ORIGINE";
              +" and "+"RIPATMP1.MRCODICE = _t2.MRCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PNCODCON = _t2.PNCODCO2";
          +",ANDESCRI = _t2.ANDESCR2";
          +i_ccchkf;
          +" from "+i_cTable+" RIPATMP1, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="RIPATMP1.CHIAVE1 = _t2.CHIAVE1";
              +" and "+"RIPATMP1.CHIAVE2 = _t2.CHIAVE2";
              +" and "+"RIPATMP1.CHIAVE3 = _t2.CHIAVE3";
              +" and "+"RIPATMP1.TIPO = _t2.TIPO";
              +" and "+"RIPATMP1.ORIGINE = _t2.ORIGINE";
              +" and "+"RIPATMP1.MRCODICE = _t2.MRCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPATMP1, "+i_cQueryTable+" _t2 set ";
          +"RIPATMP1.PNCODCON = _t2.PNCODCO2";
          +",RIPATMP1.ANDESCRI = _t2.ANDESCR2";
          +Iif(Empty(i_ccchkf),"",",RIPATMP1.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="RIPATMP1.CHIAVE1 = t2.CHIAVE1";
              +" and "+"RIPATMP1.CHIAVE2 = t2.CHIAVE2";
              +" and "+"RIPATMP1.CHIAVE3 = t2.CHIAVE3";
              +" and "+"RIPATMP1.TIPO = t2.TIPO";
              +" and "+"RIPATMP1.ORIGINE = t2.ORIGINE";
              +" and "+"RIPATMP1.MRCODICE = t2.MRCODICE";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPATMP1 set (";
          +"PNCODCON,";
          +"ANDESCRI";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.PNCODCO2,";
          +"t2.ANDESCR2";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="RIPATMP1.CHIAVE1 = _t2.CHIAVE1";
              +" and "+"RIPATMP1.CHIAVE2 = _t2.CHIAVE2";
              +" and "+"RIPATMP1.CHIAVE3 = _t2.CHIAVE3";
              +" and "+"RIPATMP1.TIPO = _t2.TIPO";
              +" and "+"RIPATMP1.ORIGINE = _t2.ORIGINE";
              +" and "+"RIPATMP1.MRCODICE = _t2.MRCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPATMP1 set ";
          +"PNCODCON = _t2.PNCODCO2";
          +",ANDESCRI = _t2.ANDESCR2";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".CHIAVE1 = "+i_cQueryTable+".CHIAVE1";
              +" and "+i_cTable+".CHIAVE2 = "+i_cQueryTable+".CHIAVE2";
              +" and "+i_cTable+".CHIAVE3 = "+i_cQueryTable+".CHIAVE3";
              +" and "+i_cTable+".TIPO = "+i_cQueryTable+".TIPO";
              +" and "+i_cTable+".ORIGINE = "+i_cQueryTable+".ORIGINE";
              +" and "+i_cTable+".MRCODICE = "+i_cQueryTable+".MRCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PNCODCON = (select PNCODCO2 from "+i_cQueryTable+" where "+i_cWhere+")";
          +",ANDESCRI = (select ANDESCR2 from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Leggo il simbolo della valuta di conto attuale - la stampa � comunque nella valuta di conto
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VASIMVAL"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(g_PERVAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VASIMVAL;
        from (i_cTable) where;
            VACODVAL = g_PERVAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_SIMBOLO = NVL(cp_ToDate(_read_.VASIMVAL),cp_NullValue(_read_.VASIMVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Calcolo gli importi in base al periodo se attivo il check
    if this.oParentObject.w_COMPET ="S"
      * --- Per calcolare il RATEO moltipiclo l'importo per il numero di giorni della competenza interni al periodo
      * --- il risultato lo divido l'importo per il numero di giorni della competenza
      * --- I cambi e gli arrotondamenti li calcolo nel Report
      * --- Create temporary table TMP_ANA
      i_nIdx=cp_AddTableDef('TMP_ANA') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('query\gsca3scp',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMP_ANA_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    else
      * --- Create temporary table TMP_ANA
      i_nIdx=cp_AddTableDef('TMP_ANA') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      i_nConn=i_TableProp[this.RIPATMP1_idx,3] && recupera la connessione
      i_cTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
            )
      this.TMP_ANA_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    endif
    * --- Esecuzione Query Output Utente (su Tabella Temporanea)
    vq_exec(alltrim(this.oParentObject.w_OQRY),this,"__tmp__")
    * --- Variabili utilizzate nel Report
    L_TIPO=this.oParentObject.w_TIPO
    L_VOCE1=this.oParentObject.w_VOCE1
    L_COMPET=this.oParentObject.w_COMPET
    L_VOCE2=this.oParentObject.w_VOCE2
    L_PROVE=this.oParentObject.w_PROVE
    L_SIMBOLO=this.w_SIMBOLO
    L_DATA1=this.oParentObject.w_DATA1
    L_DATA2=this.oParentObject.w_DATA2
    * --- Lancio Stampa Output Utente
    CP_CHPRN( ALLTRIM(this.oParentObject.w_OREP) , " ", this)
    * --- Eliminazione Tabelle Temporanee
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    * --- Drop temporary table TMP_ANA
    i_nIdx=cp_GetTableDefIdx('TMP_ANA')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_ANA')
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='VALUTE'
    this.cWorkTables[2]='*RIPATMP1'
    this.cWorkTables[3]='*TMP_ANA'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
