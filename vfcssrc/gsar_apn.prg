* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_apn                                                        *
*              Parametri nominativi                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][134]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-10                                                      *
* Last revis.: 2011-01-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsar_apn")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsar_apn")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsar_apn")
  return

* --- Class definition
define class tgsar_apn as StdPCForm
  Width  = 627
  Height = 377
  Top    = 4
  Left   = 21
  cComment = "Parametri nominativi"
  cPrg = "gsar_apn"
  HelpContextID=108431511
  add object cnt as tcgsar_apn
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsar_apn as PCContext
  w_POMASOFF = space(15)
  w_POFLCOOF = space(1)
  w_POFLCONF = space(1)
  w_POCHKIVA = space(1)
  w_POCODAZI = space(5)
  w_POOUTEXP = space(40)
  w_POOUTRAG = space(1)
  w_POOUTIMP = space(40)
  w_POMASCOC = space(15)
  w_POCATCOC = space(5)
  w_POCODPAG = space(5)
  w_POMASCOF = space(15)
  w_POCATCOF = space(5)
  w_POCODPAF = space(5)
  w_NULIVC = 0
  w_TIPMASC = space(1)
  w_DESSUPC = space(40)
  w_DESSUPF = space(40)
  w_NULIVF = 0
  w_TIPMASF = space(1)
  w_DESCONC = space(35)
  w_DESCONF = space(35)
  w_DESPAGAM = space(30)
  w_OBTEST = space(8)
  w_DATOBSO = space(8)
  w_DESPAGF = space(30)
  proc Save(oFrom)
    this.w_POMASOFF = oFrom.w_POMASOFF
    this.w_POFLCOOF = oFrom.w_POFLCOOF
    this.w_POFLCONF = oFrom.w_POFLCONF
    this.w_POCHKIVA = oFrom.w_POCHKIVA
    this.w_POCODAZI = oFrom.w_POCODAZI
    this.w_POOUTEXP = oFrom.w_POOUTEXP
    this.w_POOUTRAG = oFrom.w_POOUTRAG
    this.w_POOUTIMP = oFrom.w_POOUTIMP
    this.w_POMASCOC = oFrom.w_POMASCOC
    this.w_POCATCOC = oFrom.w_POCATCOC
    this.w_POCODPAG = oFrom.w_POCODPAG
    this.w_POMASCOF = oFrom.w_POMASCOF
    this.w_POCATCOF = oFrom.w_POCATCOF
    this.w_POCODPAF = oFrom.w_POCODPAF
    this.w_NULIVC = oFrom.w_NULIVC
    this.w_TIPMASC = oFrom.w_TIPMASC
    this.w_DESSUPC = oFrom.w_DESSUPC
    this.w_DESSUPF = oFrom.w_DESSUPF
    this.w_NULIVF = oFrom.w_NULIVF
    this.w_TIPMASF = oFrom.w_TIPMASF
    this.w_DESCONC = oFrom.w_DESCONC
    this.w_DESCONF = oFrom.w_DESCONF
    this.w_DESPAGAM = oFrom.w_DESPAGAM
    this.w_OBTEST = oFrom.w_OBTEST
    this.w_DATOBSO = oFrom.w_DATOBSO
    this.w_DESPAGF = oFrom.w_DESPAGF
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_POMASOFF = this.w_POMASOFF
    oTo.w_POFLCOOF = this.w_POFLCOOF
    oTo.w_POFLCONF = this.w_POFLCONF
    oTo.w_POCHKIVA = this.w_POCHKIVA
    oTo.w_POCODAZI = this.w_POCODAZI
    oTo.w_POOUTEXP = this.w_POOUTEXP
    oTo.w_POOUTRAG = this.w_POOUTRAG
    oTo.w_POOUTIMP = this.w_POOUTIMP
    oTo.w_POMASCOC = this.w_POMASCOC
    oTo.w_POCATCOC = this.w_POCATCOC
    oTo.w_POCODPAG = this.w_POCODPAG
    oTo.w_POMASCOF = this.w_POMASCOF
    oTo.w_POCATCOF = this.w_POCATCOF
    oTo.w_POCODPAF = this.w_POCODPAF
    oTo.w_NULIVC = this.w_NULIVC
    oTo.w_TIPMASC = this.w_TIPMASC
    oTo.w_DESSUPC = this.w_DESSUPC
    oTo.w_DESSUPF = this.w_DESSUPF
    oTo.w_NULIVF = this.w_NULIVF
    oTo.w_TIPMASF = this.w_TIPMASF
    oTo.w_DESCONC = this.w_DESCONC
    oTo.w_DESCONF = this.w_DESCONF
    oTo.w_DESPAGAM = this.w_DESPAGAM
    oTo.w_OBTEST = this.w_OBTEST
    oTo.w_DATOBSO = this.w_DATOBSO
    oTo.w_DESPAGF = this.w_DESPAGF
    PCContext::Load(oTo)
enddefine

define class tcgsar_apn as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 627
  Height = 377
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-01-18"
  HelpContextID=108431511
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=26

  * --- Constant Properties
  PAR_OFFE_IDX = 0
  MASTRI_IDX = 0
  CACOCLFO_IDX = 0
  PAG_AMEN_IDX = 0
  cFile = "PAR_OFFE"
  cKeySelect = "POCODAZI"
  cKeyWhere  = "POCODAZI=this.w_POCODAZI"
  cKeyWhereODBC = '"POCODAZI="+cp_ToStrODBC(this.w_POCODAZI)';

  cKeyWhereODBCqualified = '"PAR_OFFE.POCODAZI="+cp_ToStrODBC(this.w_POCODAZI)';

  cPrg = "gsar_apn"
  cComment = "Parametri nominativi"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_POMASOFF = space(15)
  w_POFLCOOF = space(1)
  w_POFLCONF = space(1)
  w_POCHKIVA = space(1)
  w_POCODAZI = space(5)
  o_POCODAZI = space(5)
  w_POOUTEXP = space(40)
  w_POOUTRAG = space(1)
  w_POOUTIMP = space(40)
  w_POMASCOC = space(15)
  w_POCATCOC = space(5)
  w_POCODPAG = space(5)
  w_POMASCOF = space(15)
  w_POCATCOF = space(5)
  w_POCODPAF = space(5)
  w_NULIVC = 0
  w_TIPMASC = space(1)
  w_DESSUPC = space(40)
  w_DESSUPF = space(40)
  w_NULIVF = 0
  w_TIPMASF = space(1)
  w_DESCONC = space(35)
  w_DESCONF = space(35)
  w_DESPAGAM = space(30)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_DESPAGF = space(30)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_apnPag1","gsar_apn",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).HelpContextID = 166878966
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPOMASOFF_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='MASTRI'
    this.cWorkTables[2]='CACOCLFO'
    this.cWorkTables[3]='PAG_AMEN'
    this.cWorkTables[4]='PAR_OFFE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PAR_OFFE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PAR_OFFE_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsar_apn'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_9_joined
    link_1_9_joined=.f.
    local link_1_10_joined
    link_1_10_joined=.f.
    local link_1_11_joined
    link_1_11_joined=.f.
    local link_1_12_joined
    link_1_12_joined=.f.
    local link_1_13_joined
    link_1_13_joined=.f.
    local link_1_14_joined
    link_1_14_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from PAR_OFFE where POCODAZI=KeySet.POCODAZI
    *
    i_nConn = i_TableProp[this.PAR_OFFE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_OFFE_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PAR_OFFE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PAR_OFFE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PAR_OFFE '
      link_1_9_joined=this.AddJoinedLink_1_9(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_10_joined=this.AddJoinedLink_1_10(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_11_joined=this.AddJoinedLink_1_11(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_12_joined=this.AddJoinedLink_1_12(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_13_joined=this.AddJoinedLink_1_13(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_14_joined=this.AddJoinedLink_1_14(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'POCODAZI',this.w_POCODAZI  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_NULIVC = 0
        .w_TIPMASC = space(1)
        .w_DESSUPC = space(40)
        .w_DESSUPF = space(40)
        .w_NULIVF = 0
        .w_TIPMASF = space(1)
        .w_DESCONC = space(35)
        .w_DESCONF = space(35)
        .w_DESPAGAM = space(30)
        .w_DATOBSO = ctod("  /  /  ")
        .w_DESPAGF = space(30)
        .w_POMASOFF = NVL(POMASOFF,space(15))
        .w_POFLCOOF = NVL(POFLCOOF,space(1))
        .w_POFLCONF = NVL(POFLCONF,space(1))
        .w_POCHKIVA = NVL(POCHKIVA,space(1))
        .w_POCODAZI = NVL(POCODAZI,space(5))
        .w_POOUTEXP = NVL(POOUTEXP,space(40))
        .w_POOUTRAG = NVL(POOUTRAG,space(1))
        .w_POOUTIMP = NVL(POOUTIMP,space(40))
        .w_POMASCOC = NVL(POMASCOC,space(15))
          if link_1_9_joined
            this.w_POMASCOC = NVL(MCCODICE109,NVL(this.w_POMASCOC,space(15)))
            this.w_DESSUPC = NVL(MCDESCRI109,space(40))
            this.w_NULIVC = NVL(MCNUMLIV109,0)
            this.w_TIPMASC = NVL(MCTIPMAS109,space(1))
          else
          .link_1_9('Load')
          endif
        .w_POCATCOC = NVL(POCATCOC,space(5))
          if link_1_10_joined
            this.w_POCATCOC = NVL(C2CODICE110,NVL(this.w_POCATCOC,space(5)))
            this.w_DESCONC = NVL(C2DESCRI110,space(35))
          else
          .link_1_10('Load')
          endif
        .w_POCODPAG = NVL(POCODPAG,space(5))
          if link_1_11_joined
            this.w_POCODPAG = NVL(PACODICE111,NVL(this.w_POCODPAG,space(5)))
            this.w_DESPAGAM = NVL(PADESCRI111,space(30))
            this.w_DATOBSO = NVL(cp_ToDate(PADTOBSO111),ctod("  /  /  "))
          else
          .link_1_11('Load')
          endif
        .w_POMASCOF = NVL(POMASCOF,space(15))
          if link_1_12_joined
            this.w_POMASCOF = NVL(MCCODICE112,NVL(this.w_POMASCOF,space(15)))
            this.w_DESSUPF = NVL(MCDESCRI112,space(40))
            this.w_NULIVF = NVL(MCNUMLIV112,0)
            this.w_TIPMASF = NVL(MCTIPMAS112,space(1))
          else
          .link_1_12('Load')
          endif
        .w_POCATCOF = NVL(POCATCOF,space(5))
          if link_1_13_joined
            this.w_POCATCOF = NVL(C2CODICE113,NVL(this.w_POCATCOF,space(5)))
            this.w_DESCONF = NVL(C2DESCRI113,space(35))
          else
          .link_1_13('Load')
          endif
        .w_POCODPAF = NVL(POCODPAF,space(5))
          if link_1_14_joined
            this.w_POCODPAF = NVL(PACODICE114,NVL(this.w_POCODPAF,space(5)))
            this.w_DESPAGF = NVL(PADESCRI114,space(30))
            this.w_DATOBSO = NVL(cp_ToDate(PADTOBSO114),ctod("  /  /  "))
          else
          .link_1_14('Load')
          endif
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
        .w_OBTEST = i_datsys
        cp_LoadRecExtFlds(this,'PAR_OFFE')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_POMASOFF = space(15)
      .w_POFLCOOF = space(1)
      .w_POFLCONF = space(1)
      .w_POCHKIVA = space(1)
      .w_POCODAZI = space(5)
      .w_POOUTEXP = space(40)
      .w_POOUTRAG = space(1)
      .w_POOUTIMP = space(40)
      .w_POMASCOC = space(15)
      .w_POCATCOC = space(5)
      .w_POCODPAG = space(5)
      .w_POMASCOF = space(15)
      .w_POCATCOF = space(5)
      .w_POCODPAF = space(5)
      .w_NULIVC = 0
      .w_TIPMASC = space(1)
      .w_DESSUPC = space(40)
      .w_DESSUPF = space(40)
      .w_NULIVF = 0
      .w_TIPMASF = space(1)
      .w_DESCONC = space(35)
      .w_DESCONF = space(35)
      .w_DESPAGAM = space(30)
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_DESPAGF = space(30)
      if .cFunction<>"Filter"
          .DoRTCalc(1,4,.f.)
        .w_POCODAZI = i_codazi
          .DoRTCalc(6,6,.f.)
        .w_POOUTRAG = 'N'
        .DoRTCalc(8,9,.f.)
          if not(empty(.w_POMASCOC))
          .link_1_9('Full')
          endif
        .DoRTCalc(10,10,.f.)
          if not(empty(.w_POCATCOC))
          .link_1_10('Full')
          endif
        .DoRTCalc(11,11,.f.)
          if not(empty(.w_POCODPAG))
          .link_1_11('Full')
          endif
        .DoRTCalc(12,12,.f.)
          if not(empty(.w_POMASCOF))
          .link_1_12('Full')
          endif
        .DoRTCalc(13,13,.f.)
          if not(empty(.w_POCATCOF))
          .link_1_13('Full')
          endif
        .DoRTCalc(14,14,.f.)
          if not(empty(.w_POCODPAF))
          .link_1_14('Full')
          endif
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
          .DoRTCalc(15,23,.f.)
        .w_OBTEST = i_datsys
      endif
    endwith
    cp_BlankRecExtFlds(this,'PAR_OFFE')
    this.DoRTCalc(25,26,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oPOMASOFF_1_1.enabled = i_bVal
      .Page1.oPag.oPOFLCOOF_1_2.enabled = i_bVal
      .Page1.oPag.oPOFLCONF_1_3.enabled = i_bVal
      .Page1.oPag.oPOCHKIVA_1_4.enabled = i_bVal
      .Page1.oPag.oPOOUTEXP_1_6.enabled = i_bVal
      .Page1.oPag.oPOOUTRAG_1_7.enabled = i_bVal
      .Page1.oPag.oPOOUTIMP_1_8.enabled = i_bVal
      .Page1.oPag.oPOMASCOC_1_9.enabled = i_bVal
      .Page1.oPag.oPOCATCOC_1_10.enabled = i_bVal
      .Page1.oPag.oPOCODPAG_1_11.enabled = i_bVal
      .Page1.oPag.oPOMASCOF_1_12.enabled = i_bVal
      .Page1.oPag.oPOCATCOF_1_13.enabled = i_bVal
      .Page1.oPag.oPOCODPAF_1_14.enabled = i_bVal
      .Page1.oPag.oBtn_1_16.enabled = i_bVal
      .Page1.oPag.oBtn_1_18.enabled = .Page1.oPag.oBtn_1_18.mCond()
    endwith
    cp_SetEnabledExtFlds(this,'PAR_OFFE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PAR_OFFE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_POMASOFF,"POMASOFF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_POFLCOOF,"POFLCOOF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_POFLCONF,"POFLCONF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_POCHKIVA,"POCHKIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_POCODAZI,"POCODAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_POOUTEXP,"POOUTEXP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_POOUTRAG,"POOUTRAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_POOUTIMP,"POOUTIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_POMASCOC,"POMASCOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_POCATCOC,"POCATCOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_POCODPAG,"POCODPAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_POMASCOF,"POMASCOF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_POCATCOF,"POCATCOF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_POCODPAF,"POCODPAF",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PAR_OFFE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_OFFE_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.PAR_OFFE_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into PAR_OFFE
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PAR_OFFE')
        i_extval=cp_InsertValODBCExtFlds(this,'PAR_OFFE')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(POMASOFF,POFLCOOF,POFLCONF,POCHKIVA,POCODAZI"+;
                  ",POOUTEXP,POOUTRAG,POOUTIMP,POMASCOC,POCATCOC"+;
                  ",POCODPAG,POMASCOF,POCATCOF,POCODPAF "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_POMASOFF)+;
                  ","+cp_ToStrODBC(this.w_POFLCOOF)+;
                  ","+cp_ToStrODBC(this.w_POFLCONF)+;
                  ","+cp_ToStrODBC(this.w_POCHKIVA)+;
                  ","+cp_ToStrODBC(this.w_POCODAZI)+;
                  ","+cp_ToStrODBC(this.w_POOUTEXP)+;
                  ","+cp_ToStrODBC(this.w_POOUTRAG)+;
                  ","+cp_ToStrODBC(this.w_POOUTIMP)+;
                  ","+cp_ToStrODBCNull(this.w_POMASCOC)+;
                  ","+cp_ToStrODBCNull(this.w_POCATCOC)+;
                  ","+cp_ToStrODBCNull(this.w_POCODPAG)+;
                  ","+cp_ToStrODBCNull(this.w_POMASCOF)+;
                  ","+cp_ToStrODBCNull(this.w_POCATCOF)+;
                  ","+cp_ToStrODBCNull(this.w_POCODPAF)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PAR_OFFE')
        i_extval=cp_InsertValVFPExtFlds(this,'PAR_OFFE')
        cp_CheckDeletedKey(i_cTable,0,'POCODAZI',this.w_POCODAZI)
        INSERT INTO (i_cTable);
              (POMASOFF,POFLCOOF,POFLCONF,POCHKIVA,POCODAZI,POOUTEXP,POOUTRAG,POOUTIMP,POMASCOC,POCATCOC,POCODPAG,POMASCOF,POCATCOF,POCODPAF  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_POMASOFF;
                  ,this.w_POFLCOOF;
                  ,this.w_POFLCONF;
                  ,this.w_POCHKIVA;
                  ,this.w_POCODAZI;
                  ,this.w_POOUTEXP;
                  ,this.w_POOUTRAG;
                  ,this.w_POOUTIMP;
                  ,this.w_POMASCOC;
                  ,this.w_POCATCOC;
                  ,this.w_POCODPAG;
                  ,this.w_POMASCOF;
                  ,this.w_POCATCOF;
                  ,this.w_POCODPAF;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.PAR_OFFE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_OFFE_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.PAR_OFFE_IDX,i_nConn)
      *
      * update PAR_OFFE
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'PAR_OFFE')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " POMASOFF="+cp_ToStrODBC(this.w_POMASOFF)+;
             ",POFLCOOF="+cp_ToStrODBC(this.w_POFLCOOF)+;
             ",POFLCONF="+cp_ToStrODBC(this.w_POFLCONF)+;
             ",POCHKIVA="+cp_ToStrODBC(this.w_POCHKIVA)+;
             ",POOUTEXP="+cp_ToStrODBC(this.w_POOUTEXP)+;
             ",POOUTRAG="+cp_ToStrODBC(this.w_POOUTRAG)+;
             ",POOUTIMP="+cp_ToStrODBC(this.w_POOUTIMP)+;
             ",POMASCOC="+cp_ToStrODBCNull(this.w_POMASCOC)+;
             ",POCATCOC="+cp_ToStrODBCNull(this.w_POCATCOC)+;
             ",POCODPAG="+cp_ToStrODBCNull(this.w_POCODPAG)+;
             ",POMASCOF="+cp_ToStrODBCNull(this.w_POMASCOF)+;
             ",POCATCOF="+cp_ToStrODBCNull(this.w_POCATCOF)+;
             ",POCODPAF="+cp_ToStrODBCNull(this.w_POCODPAF)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'PAR_OFFE')
        i_cWhere = cp_PKFox(i_cTable  ,'POCODAZI',this.w_POCODAZI  )
        UPDATE (i_cTable) SET;
              POMASOFF=this.w_POMASOFF;
             ,POFLCOOF=this.w_POFLCOOF;
             ,POFLCONF=this.w_POFLCONF;
             ,POCHKIVA=this.w_POCHKIVA;
             ,POOUTEXP=this.w_POOUTEXP;
             ,POOUTRAG=this.w_POOUTRAG;
             ,POOUTIMP=this.w_POOUTIMP;
             ,POMASCOC=this.w_POMASCOC;
             ,POCATCOC=this.w_POCATCOC;
             ,POCODPAG=this.w_POCODPAG;
             ,POMASCOF=this.w_POMASCOF;
             ,POCATCOF=this.w_POCATCOF;
             ,POCODPAF=this.w_POCODPAF;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PAR_OFFE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_OFFE_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.PAR_OFFE_IDX,i_nConn)
      *
      * delete PAR_OFFE
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'POCODAZI',this.w_POCODAZI  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PAR_OFFE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_OFFE_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
        .DoRTCalc(1,23,.t.)
            .w_OBTEST = i_datsys
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(25,26,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oPOMASOFF_1_1.visible=!this.oPgFrm.Page1.oPag.oPOMASOFF_1_1.mHide()
    this.oPgFrm.Page1.oPag.oPOMASCOF_1_12.visible=!this.oPgFrm.Page1.oPag.oPOMASCOF_1_12.mHide()
    this.oPgFrm.Page1.oPag.oPOCATCOF_1_13.visible=!this.oPgFrm.Page1.oPag.oPOCATCOF_1_13.mHide()
    this.oPgFrm.Page1.oPag.oPOCODPAF_1_14.visible=!this.oPgFrm.Page1.oPag.oPOCODPAF_1_14.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_17.visible=!this.oPgFrm.Page1.oPag.oStr_1_17.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_27.visible=!this.oPgFrm.Page1.oPag.oStr_1_27.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_29.visible=!this.oPgFrm.Page1.oPag.oStr_1_29.mHide()
    this.oPgFrm.Page1.oPag.oDESSUPF_1_33.visible=!this.oPgFrm.Page1.oPag.oDESSUPF_1_33.mHide()
    this.oPgFrm.Page1.oPag.oDESCONF_1_37.visible=!this.oPgFrm.Page1.oPag.oDESCONF_1_37.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_42.visible=!this.oPgFrm.Page1.oPag.oStr_1_42.mHide()
    this.oPgFrm.Page1.oPag.oDESPAGF_1_43.visible=!this.oPgFrm.Page1.oPag.oDESPAGF_1_43.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_15.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=POMASCOC
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_lTable = "MASTRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2], .t., this.MASTRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_POMASCOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMC',True,'MASTRI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MCCODICE like "+cp_ToStrODBC(trim(this.w_POMASCOC)+"%");

          i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MCCODICE',trim(this.w_POMASCOC))
          select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_POMASCOC)==trim(_Link_.MCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStrODBC(trim(this.w_POMASCOC)+"%");

            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStr(trim(this.w_POMASCOC)+"%");

            select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_POMASCOC) and !this.bDontReportError
            deferred_cp_zoom('MASTRI','*','MCCODICE',cp_AbsName(oSource.parent,'oPOMASCOC_1_9'),i_cWhere,'GSAR_AMC',"Mastri contabili (clienti)",''+iif(g_APPLICATION="ADHOC REVOLUTION", "GSAR_ACL", "GSAR2AMC")+'.MASTRI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS";
                     +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',oSource.xKey(1))
            select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_POMASCOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS";
                   +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(this.w_POMASCOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',this.w_POMASCOC)
            select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_POMASCOC = NVL(_Link_.MCCODICE,space(15))
      this.w_DESSUPC = NVL(_Link_.MCDESCRI,space(40))
      this.w_NULIVC = NVL(_Link_.MCNUMLIV,0)
      this.w_TIPMASC = NVL(_Link_.MCTIPMAS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_POMASCOC = space(15)
      endif
      this.w_DESSUPC = space(40)
      this.w_NULIVC = 0
      this.w_TIPMASC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_NULIVC=1 AND .w_TIPMASC='C'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_POMASCOC = space(15)
        this.w_DESSUPC = space(40)
        this.w_NULIVC = 0
        this.w_TIPMASC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])+'\'+cp_ToStr(_Link_.MCCODICE,1)
      cp_ShowWarn(i_cKey,this.MASTRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_POMASCOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_9(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MASTRI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_9.MCCODICE as MCCODICE109"+ ",link_1_9.MCDESCRI as MCDESCRI109"+ ",link_1_9.MCNUMLIV as MCNUMLIV109"+ ",link_1_9.MCTIPMAS as MCTIPMAS109"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_9 on PAR_OFFE.POMASCOC=link_1_9.MCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_9"
          i_cKey=i_cKey+'+" and PAR_OFFE.POMASCOC=link_1_9.MCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=POCATCOC
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CACOCLFO_IDX,3]
    i_lTable = "CACOCLFO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2], .t., this.CACOCLFO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_POCATCOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AC2',True,'CACOCLFO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" C2CODICE like "+cp_ToStrODBC(trim(this.w_POCATCOC)+"%");

          i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by C2CODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'C2CODICE',trim(this.w_POCATCOC))
          select C2CODICE,C2DESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by C2CODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_POCATCOC)==trim(_Link_.C2CODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_POCATCOC) and !this.bDontReportError
            deferred_cp_zoom('CACOCLFO','*','C2CODICE',cp_AbsName(oSource.parent,'oPOCATCOC_1_10'),i_cWhere,'GSAR_AC2',"Categorie contabili clienti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',oSource.xKey(1))
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_POCATCOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(this.w_POCATCOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',this.w_POCATCOC)
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_POCATCOC = NVL(_Link_.C2CODICE,space(5))
      this.w_DESCONC = NVL(_Link_.C2DESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_POCATCOC = space(5)
      endif
      this.w_DESCONC = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])+'\'+cp_ToStr(_Link_.C2CODICE,1)
      cp_ShowWarn(i_cKey,this.CACOCLFO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_POCATCOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_10(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CACOCLFO_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_10.C2CODICE as C2CODICE110"+ ",link_1_10.C2DESCRI as C2DESCRI110"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_10 on PAR_OFFE.POCATCOC=link_1_10.C2CODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_10"
          i_cKey=i_cKey+'+" and PAR_OFFE.POCATCOC=link_1_10.C2CODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=POCODPAG
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_lTable = "PAG_AMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2], .t., this.PAG_AMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_POCODPAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APA',True,'PAG_AMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PACODICE like "+cp_ToStrODBC(trim(this.w_POCODPAG)+"%");

          i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PACODICE',trim(this.w_POCODPAG))
          select PACODICE,PADESCRI,PADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_POCODPAG)==trim(_Link_.PACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"( PADESCRI like "+cp_ToStrODBC(trim(this.w_POCODPAG)+"%")+cp_TransWhereFldName('PADESCRI',trim(this.w_POCODPAG))+")";

            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" PADESCRI like "+cp_ToStr(trim(this.w_POCODPAG)+"%");

            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_POCODPAG) and !this.bDontReportError
            deferred_cp_zoom('PAG_AMEN','*','PACODICE',cp_AbsName(oSource.parent,'oPOCODPAG_1_11'),i_cWhere,'GSAR_APA',"Pagamenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',oSource.xKey(1))
            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_POCODPAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_POCODPAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_POCODPAG)
            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_POCODPAG = NVL(_Link_.PACODICE,space(5))
      this.w_DESPAGAM = NVL(cp_TransLoadField('_Link_.PADESCRI'),space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.PADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_POCODPAG = space(5)
      endif
      this.w_DESPAGAM = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice pagamento inesistente oppure obsoleto")
        endif
        this.w_POCODPAG = space(5)
        this.w_DESPAGAM = space(30)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_AMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_POCODPAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_11(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PAG_AMEN_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_11.PACODICE as PACODICE111"+ ","+cp_TransLinkFldName('link_1_11.PADESCRI')+" as PADESCRI111"+ ",link_1_11.PADTOBSO as PADTOBSO111"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_11 on PAR_OFFE.POCODPAG=link_1_11.PACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_11"
          i_cKey=i_cKey+'+" and PAR_OFFE.POCODPAG=link_1_11.PACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=POMASCOF
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_lTable = "MASTRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2], .t., this.MASTRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_POMASCOF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMC',True,'MASTRI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MCCODICE like "+cp_ToStrODBC(trim(this.w_POMASCOF)+"%");

          i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MCCODICE',trim(this.w_POMASCOF))
          select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_POMASCOF)==trim(_Link_.MCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStrODBC(trim(this.w_POMASCOF)+"%");

            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStr(trim(this.w_POMASCOF)+"%");

            select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_POMASCOF) and !this.bDontReportError
            deferred_cp_zoom('MASTRI','*','MCCODICE',cp_AbsName(oSource.parent,'oPOMASCOF_1_12'),i_cWhere,'GSAR_AMC',"Mastri contabili (fornitori)",''+iif(g_APPLICATION="ADHOC REVOLUTION", "GSAR_AFR", "GSAR1AFR")+'.MASTRI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS";
                     +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',oSource.xKey(1))
            select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_POMASCOF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS";
                   +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(this.w_POMASCOF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',this.w_POMASCOF)
            select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_POMASCOF = NVL(_Link_.MCCODICE,space(15))
      this.w_DESSUPF = NVL(_Link_.MCDESCRI,space(40))
      this.w_NULIVF = NVL(_Link_.MCNUMLIV,0)
      this.w_TIPMASF = NVL(_Link_.MCTIPMAS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_POMASCOF = space(15)
      endif
      this.w_DESSUPF = space(40)
      this.w_NULIVF = 0
      this.w_TIPMASF = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_NULIVF=1 AND .w_TIPMASF='F'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_POMASCOF = space(15)
        this.w_DESSUPF = space(40)
        this.w_NULIVF = 0
        this.w_TIPMASF = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])+'\'+cp_ToStr(_Link_.MCCODICE,1)
      cp_ShowWarn(i_cKey,this.MASTRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_POMASCOF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_12(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MASTRI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_12.MCCODICE as MCCODICE112"+ ",link_1_12.MCDESCRI as MCDESCRI112"+ ",link_1_12.MCNUMLIV as MCNUMLIV112"+ ",link_1_12.MCTIPMAS as MCTIPMAS112"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_12 on PAR_OFFE.POMASCOF=link_1_12.MCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_12"
          i_cKey=i_cKey+'+" and PAR_OFFE.POMASCOF=link_1_12.MCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=POCATCOF
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CACOCLFO_IDX,3]
    i_lTable = "CACOCLFO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2], .t., this.CACOCLFO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_POCATCOF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AC2',True,'CACOCLFO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" C2CODICE like "+cp_ToStrODBC(trim(this.w_POCATCOF)+"%");

          i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by C2CODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'C2CODICE',trim(this.w_POCATCOF))
          select C2CODICE,C2DESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by C2CODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_POCATCOF)==trim(_Link_.C2CODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_POCATCOF) and !this.bDontReportError
            deferred_cp_zoom('CACOCLFO','*','C2CODICE',cp_AbsName(oSource.parent,'oPOCATCOF_1_13'),i_cWhere,'GSAR_AC2',"Categorie contabili fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',oSource.xKey(1))
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_POCATCOF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(this.w_POCATCOF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',this.w_POCATCOF)
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_POCATCOF = NVL(_Link_.C2CODICE,space(5))
      this.w_DESCONF = NVL(_Link_.C2DESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_POCATCOF = space(5)
      endif
      this.w_DESCONF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])+'\'+cp_ToStr(_Link_.C2CODICE,1)
      cp_ShowWarn(i_cKey,this.CACOCLFO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_POCATCOF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_13(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CACOCLFO_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_13.C2CODICE as C2CODICE113"+ ",link_1_13.C2DESCRI as C2DESCRI113"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_13 on PAR_OFFE.POCATCOF=link_1_13.C2CODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_13"
          i_cKey=i_cKey+'+" and PAR_OFFE.POCATCOF=link_1_13.C2CODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=POCODPAF
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_lTable = "PAG_AMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2], .t., this.PAG_AMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_POCODPAF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APA',True,'PAG_AMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PACODICE like "+cp_ToStrODBC(trim(this.w_POCODPAF)+"%");

          i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PACODICE',trim(this.w_POCODPAF))
          select PACODICE,PADESCRI,PADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_POCODPAF)==trim(_Link_.PACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"( PADESCRI like "+cp_ToStrODBC(trim(this.w_POCODPAF)+"%")+cp_TransWhereFldName('PADESCRI',trim(this.w_POCODPAF))+")";

            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" PADESCRI like "+cp_ToStr(trim(this.w_POCODPAF)+"%");

            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_POCODPAF) and !this.bDontReportError
            deferred_cp_zoom('PAG_AMEN','*','PACODICE',cp_AbsName(oSource.parent,'oPOCODPAF_1_14'),i_cWhere,'GSAR_APA',"Pagamenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',oSource.xKey(1))
            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_POCODPAF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_POCODPAF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_POCODPAF)
            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_POCODPAF = NVL(_Link_.PACODICE,space(5))
      this.w_DESPAGF = NVL(cp_TransLoadField('_Link_.PADESCRI'),space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.PADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_POCODPAF = space(5)
      endif
      this.w_DESPAGF = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice pagamento inesistente oppure obsoleto")
        endif
        this.w_POCODPAF = space(5)
        this.w_DESPAGF = space(30)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_AMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_POCODPAF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_14(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PAG_AMEN_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_14.PACODICE as PACODICE114"+ ","+cp_TransLinkFldName('link_1_14.PADESCRI')+" as PADESCRI114"+ ",link_1_14.PADTOBSO as PADTOBSO114"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_14 on PAR_OFFE.POCODPAF=link_1_14.PACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_14"
          i_cKey=i_cKey+'+" and PAR_OFFE.POCODPAF=link_1_14.PACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPOMASOFF_1_1.value==this.w_POMASOFF)
      this.oPgFrm.Page1.oPag.oPOMASOFF_1_1.value=this.w_POMASOFF
    endif
    if not(this.oPgFrm.Page1.oPag.oPOFLCOOF_1_2.RadioValue()==this.w_POFLCOOF)
      this.oPgFrm.Page1.oPag.oPOFLCOOF_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPOFLCONF_1_3.RadioValue()==this.w_POFLCONF)
      this.oPgFrm.Page1.oPag.oPOFLCONF_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPOCHKIVA_1_4.RadioValue()==this.w_POCHKIVA)
      this.oPgFrm.Page1.oPag.oPOCHKIVA_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPOOUTEXP_1_6.value==this.w_POOUTEXP)
      this.oPgFrm.Page1.oPag.oPOOUTEXP_1_6.value=this.w_POOUTEXP
    endif
    if not(this.oPgFrm.Page1.oPag.oPOOUTRAG_1_7.RadioValue()==this.w_POOUTRAG)
      this.oPgFrm.Page1.oPag.oPOOUTRAG_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPOOUTIMP_1_8.value==this.w_POOUTIMP)
      this.oPgFrm.Page1.oPag.oPOOUTIMP_1_8.value=this.w_POOUTIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oPOMASCOC_1_9.value==this.w_POMASCOC)
      this.oPgFrm.Page1.oPag.oPOMASCOC_1_9.value=this.w_POMASCOC
    endif
    if not(this.oPgFrm.Page1.oPag.oPOCATCOC_1_10.value==this.w_POCATCOC)
      this.oPgFrm.Page1.oPag.oPOCATCOC_1_10.value=this.w_POCATCOC
    endif
    if not(this.oPgFrm.Page1.oPag.oPOCODPAG_1_11.value==this.w_POCODPAG)
      this.oPgFrm.Page1.oPag.oPOCODPAG_1_11.value=this.w_POCODPAG
    endif
    if not(this.oPgFrm.Page1.oPag.oPOMASCOF_1_12.value==this.w_POMASCOF)
      this.oPgFrm.Page1.oPag.oPOMASCOF_1_12.value=this.w_POMASCOF
    endif
    if not(this.oPgFrm.Page1.oPag.oPOCATCOF_1_13.value==this.w_POCATCOF)
      this.oPgFrm.Page1.oPag.oPOCATCOF_1_13.value=this.w_POCATCOF
    endif
    if not(this.oPgFrm.Page1.oPag.oPOCODPAF_1_14.value==this.w_POCODPAF)
      this.oPgFrm.Page1.oPag.oPOCODPAF_1_14.value=this.w_POCODPAF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESSUPC_1_32.value==this.w_DESSUPC)
      this.oPgFrm.Page1.oPag.oDESSUPC_1_32.value=this.w_DESSUPC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESSUPF_1_33.value==this.w_DESSUPF)
      this.oPgFrm.Page1.oPag.oDESSUPF_1_33.value=this.w_DESSUPF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCONC_1_36.value==this.w_DESCONC)
      this.oPgFrm.Page1.oPag.oDESCONC_1_36.value=this.w_DESCONC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCONF_1_37.value==this.w_DESCONF)
      this.oPgFrm.Page1.oPag.oDESCONF_1_37.value=this.w_DESCONF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPAGAM_1_39.value==this.w_DESPAGAM)
      this.oPgFrm.Page1.oPag.oDESPAGAM_1_39.value=this.w_DESPAGAM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPAGF_1_43.value==this.w_DESPAGF)
      this.oPgFrm.Page1.oPag.oDESPAGF_1_43.value=this.w_DESPAGF
    endif
    cp_SetControlsValueExtFlds(this,'PAR_OFFE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_NULIVC=1 AND .w_TIPMASC='C')  and not(empty(.w_POMASCOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPOMASCOC_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_POCODPAG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPOCODPAG_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice pagamento inesistente oppure obsoleto")
          case   not(.w_NULIVF=1 AND .w_TIPMASF='F')  and not(Isahe())  and not(empty(.w_POMASCOF))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPOMASCOF_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(Isahe())  and not(empty(.w_POCODPAF))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPOCODPAF_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice pagamento inesistente oppure obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsar_apn
      IF (.w_POFLCONF='S' AND EMPTY(.w_POCATCOC))
           i_bnoChk = .f.
           i_bRes = .f.
           i_cErrorMsg = Ah_MsgFormat("Categoria contabile clienti non inserita!")
      ENDIF
      IF (.w_POFLCONF='S' AND EMPTY(.w_POMASCOC))
           i_bnoChk = .f.
           i_bRes = .f.
           i_cErrorMsg = Ah_MsgFormat("Mastro contabile clienti non inserito!")
      ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_POCODAZI = this.w_POCODAZI
    return

enddefine

* --- Define pages as container
define class tgsar_apnPag1 as StdContainer
  Width  = 623
  height = 377
  stdWidth  = 623
  stdheight = 377
  resizeXpos=318
  resizeYpos=133
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPOMASOFF_1_1 as StdField with uid="HHCZQVIGJS",rtseq=1,rtrep=.f.,;
    cFormVar = "w_POMASOFF", cQueryName = "POMASOFF",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Struttura input codici nominativi",;
    HelpContextID = 183283004,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=174, Top=14, InputMask=replicate('X',15)

  func oPOMASOFF_1_1.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oPOFLCOOF_1_2 as StdCheck with uid="WOEWOKGIDF",rtseq=2,rtrep=.f.,left=307, top=10, caption="Codifica nominativi numerica",;
    ToolTipText = "Se attivo: i nominativi verranno codificati numericamente",;
    HelpContextID = 101237444,;
    cFormVar="w_POFLCOOF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPOFLCOOF_1_2.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPOFLCOOF_1_2.GetRadio()
    this.Parent.oContained.w_POFLCOOF = this.RadioValue()
    return .t.
  endfunc

  func oPOFLCOOF_1_2.SetRadio()
    this.Parent.oContained.w_POFLCOOF=trim(this.Parent.oContained.w_POFLCOOF)
    this.value = ;
      iif(this.Parent.oContained.w_POFLCOOF=='S',1,;
      0)
  endfunc

  add object oPOFLCONF_1_3 as StdCheck with uid="ELRKYNLVAN",rtseq=3,rtrep=.f.,left=307, top=31, caption="Non visualizza maschera di conferma clienti",;
    ToolTipText = "Se attivo: non verr� visualizzata la maschera di conferma generando un cliente da nominativo",;
    HelpContextID = 101237444,;
    cFormVar="w_POFLCONF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPOFLCONF_1_3.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPOFLCONF_1_3.GetRadio()
    this.Parent.oContained.w_POFLCONF = this.RadioValue()
    return .t.
  endfunc

  func oPOFLCONF_1_3.SetRadio()
    this.Parent.oContained.w_POFLCONF=trim(this.Parent.oContained.w_POFLCONF)
    this.value = ;
      iif(this.Parent.oContained.w_POFLCONF=='S',1,;
      0)
  endfunc

  add object oPOCHKIVA_1_4 as StdCheck with uid="GBUGCJDAIU",rtseq=4,rtrep=.f.,left=307, top=52, caption="Controlla partita IVA del nominativo",;
    ToolTipText = "Se attivo, controlla la partita IVA del nominativo in fase di inserimento clienti",;
    HelpContextID = 74648887,;
    cFormVar="w_POCHKIVA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPOCHKIVA_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPOCHKIVA_1_4.GetRadio()
    this.Parent.oContained.w_POCHKIVA = this.RadioValue()
    return .t.
  endfunc

  func oPOCHKIVA_1_4.SetRadio()
    this.Parent.oContained.w_POCHKIVA=trim(this.Parent.oContained.w_POCHKIVA)
    this.value = ;
      iif(this.Parent.oContained.w_POCHKIVA=='S',1,;
      0)
  endfunc

  add object oPOOUTEXP_1_6 as StdField with uid="NWIMOADGSU",rtseq=6,rtrep=.f.,;
    cFormVar = "w_POOUTEXP", cQueryName = "POOUTEXP",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Nome della cartella contatti in cui esportare i nominativi",;
    HelpContextID = 17878342,;
   bGlobalFont=.t.,;
    Height=21, Width=230, Left=143, Top=92, InputMask=replicate('X',40)


  add object oPOOUTRAG_1_7 as StdCombo with uid="KJAHFSEKZB",rtseq=7,rtrep=.f.,left=489,top=92,width=99,height=21;
    , ToolTipText = "Tipo di raggruppamento in esportazione contatti";
    , HelpContextID = 32453315;
    , cFormVar="w_POOUTRAG",RowSource=""+"Nessuno,"+"Tipo,"+"Localit�,"+"Provincia,"+"Gruppo nominativo,"+"Origine,"+"Operatore,"+"Zona,"+"Agente,"+"Ruolo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPOOUTRAG_1_7.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'T',;
    iif(this.value =3,'L',;
    iif(this.value =4,'P',;
    iif(this.value =5,'G',;
    iif(this.value =6,'O',;
    iif(this.value =7,'U',;
    iif(this.value =8,'Z',;
    iif(this.value =9,'A',;
    iif(this.value =10,'R',;
    space(1))))))))))))
  endfunc
  func oPOOUTRAG_1_7.GetRadio()
    this.Parent.oContained.w_POOUTRAG = this.RadioValue()
    return .t.
  endfunc

  func oPOOUTRAG_1_7.SetRadio()
    this.Parent.oContained.w_POOUTRAG=trim(this.Parent.oContained.w_POOUTRAG)
    this.value = ;
      iif(this.Parent.oContained.w_POOUTRAG=='N',1,;
      iif(this.Parent.oContained.w_POOUTRAG=='T',2,;
      iif(this.Parent.oContained.w_POOUTRAG=='L',3,;
      iif(this.Parent.oContained.w_POOUTRAG=='P',4,;
      iif(this.Parent.oContained.w_POOUTRAG=='G',5,;
      iif(this.Parent.oContained.w_POOUTRAG=='O',6,;
      iif(this.Parent.oContained.w_POOUTRAG=='U',7,;
      iif(this.Parent.oContained.w_POOUTRAG=='Z',8,;
      iif(this.Parent.oContained.w_POOUTRAG=='A',9,;
      iif(this.Parent.oContained.w_POOUTRAG=='R',10,;
      0))))))))))
  endfunc

  add object oPOOUTIMP_1_8 as StdField with uid="CSPCCUPTIL",rtseq=8,rtrep=.f.,;
    cFormVar = "w_POOUTIMP", cQueryName = "POOUTIMP",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Nome della cartella contatti da cui importare i nominativi",;
    HelpContextID = 84987206,;
   bGlobalFont=.t.,;
    Height=21, Width=230, Left=143, Top=116, InputMask=replicate('X',40)

  add object oPOMASCOC_1_9 as StdField with uid="WDFQOICSHO",rtseq=9,rtrep=.f.,;
    cFormVar = "w_POMASCOC", cQueryName = "POMASCOC",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Mastro di raggruppamento del cliente",;
    HelpContextID = 18043591,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=212, Top=175, cSayPict="p_MAS", cGetPict="p_MAS", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="MASTRI", cZoomOnZoom="GSAR_AMC", oKey_1_1="MCCODICE", oKey_1_2="this.w_POMASCOC"

  func oPOMASCOC_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oPOMASCOC_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPOMASCOC_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MASTRI','*','MCCODICE',cp_AbsName(this.parent,'oPOMASCOC_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMC',"Mastri contabili (clienti)",''+iif(g_APPLICATION="ADHOC REVOLUTION", "GSAR_ACL", "GSAR2AMC")+'.MASTRI_VZM',this.parent.oContained
  endproc
  proc oPOMASCOC_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MCCODICE=this.parent.oContained.w_POMASCOC
     i_obj.ecpSave()
  endproc

  add object oPOCATCOC_1_10 as StdField with uid="ETYDCIOVMN",rtseq=10,rtrep=.f.,;
    cFormVar = "w_POCATCOC", cQueryName = "POCATCOC",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice della categoria contabile del cliente",;
    HelpContextID = 17035975,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=212, Top=201, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CACOCLFO", cZoomOnZoom="GSAR_AC2", oKey_1_1="C2CODICE", oKey_1_2="this.w_POCATCOC"

  func oPOCATCOC_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oPOCATCOC_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPOCATCOC_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CACOCLFO','*','C2CODICE',cp_AbsName(this.parent,'oPOCATCOC_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AC2',"Categorie contabili clienti",'',this.parent.oContained
  endproc
  proc oPOCATCOC_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AC2()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_C2CODICE=this.parent.oContained.w_POCATCOC
     i_obj.ecpSave()
  endproc

  add object oPOCODPAG_1_11 as StdField with uid="KZXBRHQEZZ",rtseq=11,rtrep=.f.,;
    cFormVar = "w_POCODPAG", cQueryName = "POCODPAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice pagamento inesistente oppure obsoleto",;
    ToolTipText = "Codice pagamento del cliente",;
    HelpContextID = 83227331,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=212, Top=227, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="PAG_AMEN", cZoomOnZoom="GSAR_APA", oKey_1_1="PACODICE", oKey_1_2="this.w_POCODPAG"

  func oPOCODPAG_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oPOCODPAG_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPOCODPAG_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PAG_AMEN','*','PACODICE',cp_AbsName(this.parent,'oPOCODPAG_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APA',"Pagamenti",'',this.parent.oContained
  endproc
  proc oPOCODPAG_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PACODICE=this.parent.oContained.w_POCODPAG
     i_obj.ecpSave()
  endproc

  add object oPOMASCOF_1_12 as StdField with uid="JWUKCQMLXW",rtseq=12,rtrep=.f.,;
    cFormVar = "w_POMASCOF", cQueryName = "POMASCOF",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Mastro di raggruppamento del fornitore",;
    HelpContextID = 18043588,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=212, Top=253, cSayPict="p_MAS", cGetPict="p_MAS", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="MASTRI", cZoomOnZoom="GSAR_AMC", oKey_1_1="MCCODICE", oKey_1_2="this.w_POMASCOF"

  func oPOMASCOF_1_12.mHide()
    with this.Parent.oContained
      return (Isahe())
    endwith
  endfunc

  func oPOMASCOF_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oPOMASCOF_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPOMASCOF_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MASTRI','*','MCCODICE',cp_AbsName(this.parent,'oPOMASCOF_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMC',"Mastri contabili (fornitori)",''+iif(g_APPLICATION="ADHOC REVOLUTION", "GSAR_AFR", "GSAR1AFR")+'.MASTRI_VZM',this.parent.oContained
  endproc
  proc oPOMASCOF_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MCCODICE=this.parent.oContained.w_POMASCOF
     i_obj.ecpSave()
  endproc

  add object oPOCATCOF_1_13 as StdField with uid="QUDSVTCCVZ",rtseq=13,rtrep=.f.,;
    cFormVar = "w_POCATCOF", cQueryName = "POCATCOF",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice della categoria contabile del fornitore",;
    HelpContextID = 17035972,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=212, Top=279, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CACOCLFO", cZoomOnZoom="GSAR_AC2", oKey_1_1="C2CODICE", oKey_1_2="this.w_POCATCOF"

  func oPOCATCOF_1_13.mHide()
    with this.Parent.oContained
      return (Isahe())
    endwith
  endfunc

  func oPOCATCOF_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oPOCATCOF_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPOCATCOF_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CACOCLFO','*','C2CODICE',cp_AbsName(this.parent,'oPOCATCOF_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AC2',"Categorie contabili fornitori",'',this.parent.oContained
  endproc
  proc oPOCATCOF_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AC2()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_C2CODICE=this.parent.oContained.w_POCATCOF
     i_obj.ecpSave()
  endproc

  add object oPOCODPAF_1_14 as StdField with uid="LOBWEEFDAD",rtseq=14,rtrep=.f.,;
    cFormVar = "w_POCODPAF", cQueryName = "POCODPAF",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice pagamento inesistente oppure obsoleto",;
    ToolTipText = "Codice pagamento del fornitore",;
    HelpContextID = 83227332,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=212, Top=305, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="PAG_AMEN", cZoomOnZoom="GSAR_APA", oKey_1_1="PACODICE", oKey_1_2="this.w_POCODPAF"

  func oPOCODPAF_1_14.mHide()
    with this.Parent.oContained
      return (Isahe())
    endwith
  endfunc

  func oPOCODPAF_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oPOCODPAF_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPOCODPAF_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PAG_AMEN','*','PACODICE',cp_AbsName(this.parent,'oPOCODPAF_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APA',"Pagamenti",'',this.parent.oContained
  endproc
  proc oPOCODPAF_1_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PACODICE=this.parent.oContained.w_POCODPAF
     i_obj.ecpSave()
  endproc


  add object oObj_1_15 as cp_runprogram with uid="QNFMWJINQX",left=1, top=380, width=233,height=19,;
    caption='GSAR_BAM(modifica)',;
   bGlobalFont=.t.,;
    prg='GSAR_BAM("modifica")',;
    cEvent = "Update end",;
    nPag=1;
    , HelpContextID = 71794778


  add object oBtn_1_16 as StdButton with uid="SXVVCXGXSY",left=516, top=329, width=48,height=45,;
    CpPicture="bmp\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 108452070;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_18 as StdButton with uid="MTRWQEGJEF",left=567, top=329, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 105719034;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESSUPC_1_32 as StdField with uid="RWZBGOCFMA",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESSUPC", cQueryName = "DESSUPC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 65076682,;
   bGlobalFont=.t.,;
    Height=21, Width=281, Left=335, Top=175, InputMask=replicate('X',40)

  add object oDESSUPF_1_33 as StdField with uid="YMTZMIAMKM",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DESSUPF", cQueryName = "DESSUPF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 203358774,;
   bGlobalFont=.t.,;
    Height=21, Width=281, Left=335, Top=253, InputMask=replicate('X',40)

  func oDESSUPF_1_33.mHide()
    with this.Parent.oContained
      return (Isahe())
    endwith
  endfunc

  add object oDESCONC_1_36 as StdField with uid="JAMKNLZVQH",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESCONC", cQueryName = "DESCONC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 105971146,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=280, Top=201, InputMask=replicate('X',35)

  add object oDESCONF_1_37 as StdField with uid="NZHZMCVIGP",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DESCONF", cQueryName = "DESCONF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 162464310,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=280, Top=279, InputMask=replicate('X',35)

  func oDESCONF_1_37.mHide()
    with this.Parent.oContained
      return (Isahe())
    endwith
  endfunc

  add object oDESPAGAM_1_39 as StdField with uid="WTMMFJEXMK",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESPAGAM", cQueryName = "DESPAGAM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 237239677,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=280, Top=227, InputMask=replicate('X',30)

  add object oDESPAGF_1_43 as StdField with uid="UFVGIJLAEX",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DESPAGF", cQueryName = "DESPAGF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 31195702,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=280, Top=305, InputMask=replicate('X',30)

  func oDESPAGF_1_43.mHide()
    with this.Parent.oContained
      return (Isahe())
    endwith
  endfunc

  add object oStr_1_17 as StdString with uid="BFLNHWFYPX",Visible=.t., Left=9, Top=14,;
    Alignment=1, Width=162, Height=18,;
    Caption="Struttura codice nominativo:"  ;
  , bGlobalFont=.t.

  func oStr_1_17.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_21 as StdString with uid="UVDJCGFASA",Visible=.t., Left=8, Top=65,;
    Alignment=0, Width=157, Height=18,;
    Caption="Import/export contatti"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="KKJPEOJOKX",Visible=.t., Left=1, Top=92,;
    Alignment=1, Width=138, Height=18,;
    Caption="Cartella export Outlook:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="BXWBEPAJST",Visible=.t., Left=4, Top=116,;
    Alignment=1, Width=135, Height=18,;
    Caption="Cartella import Outlook:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="KDTKMPRGMS",Visible=.t., Left=380, Top=92,;
    Alignment=1, Width=106, Height=18,;
    Caption="Raggruppamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="GHPCFACZYK",Visible=.t., Left=8, Top=148,;
    Alignment=0, Width=92, Height=18,;
    Caption="Dati contabili"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="BUKMEHHODY",Visible=.t., Left=56, Top=175,;
    Alignment=1, Width=150, Height=18,;
    Caption="Mastro contabile clienti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="JGRFUYDGZN",Visible=.t., Left=53, Top=254,;
    Alignment=1, Width=154, Height=18,;
    Caption="Mastro contabile fornitori:"  ;
  , bGlobalFont=.t.

  func oStr_1_27.mHide()
    with this.Parent.oContained
      return (Isahe())
    endwith
  endfunc

  add object oStr_1_28 as StdString with uid="MAPCZCOYAA",Visible=.t., Left=27, Top=201,;
    Alignment=1, Width=179, Height=18,;
    Caption="Categoria contabile clienti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="ENVZYBMNZO",Visible=.t., Left=9, Top=280,;
    Alignment=1, Width=198, Height=18,;
    Caption="Categoria contabile fornitori:"  ;
  , bGlobalFont=.t.

  func oStr_1_29.mHide()
    with this.Parent.oContained
      return (Isahe())
    endwith
  endfunc

  add object oStr_1_38 as StdString with uid="IXUXJYCBAT",Visible=.t., Left=36, Top=228,;
    Alignment=1, Width=171, Height=18,;
    Caption="Codice pagamento clienti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="RHZTDCSPTE",Visible=.t., Left=36, Top=306,;
    Alignment=1, Width=171, Height=18,;
    Caption="Codice pagamento fornitori:"  ;
  , bGlobalFont=.t.

  func oStr_1_42.mHide()
    with this.Parent.oContained
      return (Isahe())
    endwith
  endfunc

  add object oBox_1_19 as StdBox with uid="CXIUCUFRQK",left=4, top=166, width=612,height=2

  add object oBox_1_20 as StdBox with uid="TZPHGFNRLF",left=4, top=83, width=612,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_apn','PAR_OFFE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".POCODAZI=PAR_OFFE.POCODAZI";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
