* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bta                                                        *
*              Aggiornamento saldi commessa                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-01-22                                                      *
* Last revis.: 2016-05-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bta",oParentObject,m.pOPER)
return(i_retval)

define class tgsve_bta as StdBatch
  * --- Local variables
  pOPER = space(1)
  w_OLDCOMME = space(15)
  w_OLDQTAUM1 = 0
  w_OLDQTASAL = 0
  w_FLCASC = space(1)
  w_F2CASC = space(1)
  w_FLORD = space(1)
  w_F2ORD = space(1)
  w_FLIMP = space(1)
  w_F2IMP = space(1)
  w_FLRIS = space(1)
  w_F2RIS = space(1)
  w_OLDCODMAG = space(5)
  w_OLDCODMAT = space(5)
  w_OLDCODUBI = space(20)
  w_OLDCODLOT = space(20)
  w_KEYSAL = space(20)
  w_CODART = space(20)
  w_OLDFCM = space(1)
  w_COMMDEFA = space(15)
  w_PunPAD = .NULL.
  w_ARCONCAR = space(1)
  w_SRV = space(1)
  w_CODCOM = space(15)
  w_ISOPEN = .f.
  w_LOOP = 0
  w_OLDLOTMAG = space(5)
  w_OLDLOTMAT = space(5)
  w_OLDCODART = space(20)
  w_OLDKEYSAL = space(20)
  w_OLDCODUB2 = space(20)
  w_RowStatus = space(1)
  * --- WorkFile variables
  MVM_DETT_idx=0
  ART_ICOL_idx=0
  SALDICOM_idx=0
  SALOTCOM_idx=0
  DOC_DETT_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- pOper = B (Aggiornamento Saldi Commessa)
    * --- Caller
    * --- Locali
    if this.pOPER="G"
      if g_CCAR="S" and this.oParentObject.w_CONFIG="S"
        this.w_ISOPEN = .f.
        this.w_LOOP = 1
        if !this.w_ISOPEN
          do while this.w_LOOP<=_Screen.FormCount and !this.w_ISOPEN
            if VarType( _Screen.Forms[ this.w_LOOP ].cComment )="C" 
              if UPPER(_Screen.Forms( this.w_Loop ).Class)="TGSCR_KCC"
                * --- Devo dare il focus altrimenti la maschera va dietro
                _screen.forms[this.w_Loop].w_Albero.grd.SetFocus()
                this.w_ISOPEN = .t.
              endif
            endif
            this.w_LOOP = this.w_LOOP + 1
          enddo
          if !this.w_ISOPEN
            this.pOPER = "F"
          endif
        endif
      endif
    endif
    do case
      case this.pOPER="B"
        * --- Aggiornamento Saldi Commessa
        * --- Leggo il vecchio valore dal Documento
        * --- Read from DOC_DETT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MVCODCOM,MVQTAUM1,MVQTASAL,MVFLCASC,MVFLORDI,MVFLIMPE,MVFLRISE,MVF2CASC,MVF2ORDI,MVF2IMPE,MVF2RISE,MVCODMAG,MVCODMAT,MVKEYSAL,MVCODART,MVCODLOT,MVCODUBI,MVCODUB2,MVLOTMAG,MVLOTMAT"+;
            " from "+i_cTable+" DOC_DETT where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL);
                +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_CPROWNUM);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MVCODCOM,MVQTAUM1,MVQTASAL,MVFLCASC,MVFLORDI,MVFLIMPE,MVFLRISE,MVF2CASC,MVF2ORDI,MVF2IMPE,MVF2RISE,MVCODMAG,MVCODMAT,MVKEYSAL,MVCODART,MVCODLOT,MVCODUBI,MVCODUB2,MVLOTMAG,MVLOTMAT;
            from (i_cTable) where;
                MVSERIAL = this.oParentObject.w_MVSERIAL;
                and CPROWNUM = this.oParentObject.w_CPROWNUM;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_OLDCOMME = NVL(cp_ToDate(_read_.MVCODCOM),cp_NullValue(_read_.MVCODCOM))
          this.w_OLDQTAUM1 = NVL(cp_ToDate(_read_.MVQTAUM1),cp_NullValue(_read_.MVQTAUM1))
          this.w_OLDQTASAL = NVL(cp_ToDate(_read_.MVQTASAL),cp_NullValue(_read_.MVQTASAL))
          this.w_FLCASC = NVL(cp_ToDate(_read_.MVFLCASC),cp_NullValue(_read_.MVFLCASC))
          this.w_FLORD = NVL(cp_ToDate(_read_.MVFLORDI),cp_NullValue(_read_.MVFLORDI))
          this.w_FLIMP = NVL(cp_ToDate(_read_.MVFLIMPE),cp_NullValue(_read_.MVFLIMPE))
          this.w_FLRIS = NVL(cp_ToDate(_read_.MVFLRISE),cp_NullValue(_read_.MVFLRISE))
          this.w_F2CASC = NVL(cp_ToDate(_read_.MVF2CASC),cp_NullValue(_read_.MVF2CASC))
          this.w_F2ORD = NVL(cp_ToDate(_read_.MVF2ORDI),cp_NullValue(_read_.MVF2ORDI))
          this.w_F2IMP = NVL(cp_ToDate(_read_.MVF2IMPE),cp_NullValue(_read_.MVF2IMPE))
          this.w_F2RIS = NVL(cp_ToDate(_read_.MVF2RISE),cp_NullValue(_read_.MVF2RISE))
          this.w_OLDCODMAG = NVL(cp_ToDate(_read_.MVCODMAG),cp_NullValue(_read_.MVCODMAG))
          this.w_OLDCODMAT = NVL(cp_ToDate(_read_.MVCODMAT),cp_NullValue(_read_.MVCODMAT))
          this.w_OLDKEYSAL = NVL(cp_ToDate(_read_.MVKEYSAL),cp_NullValue(_read_.MVKEYSAL))
          this.w_OLDCODART = NVL(cp_ToDate(_read_.MVCODART),cp_NullValue(_read_.MVCODART))
          this.w_OLDCODLOT = NVL(cp_ToDate(_read_.MVCODLOT),cp_NullValue(_read_.MVCODLOT))
          this.w_OLDCODUBI = NVL(cp_ToDate(_read_.MVCODUBI),cp_NullValue(_read_.MVCODUBI))
          this.w_OLDCODUB2 = NVL(cp_ToDate(_read_.MVCODUB2),cp_NullValue(_read_.MVCODUB2))
          this.w_OLDLOTMAG = NVL(cp_ToDate(_read_.MVLOTMAG),cp_NullValue(_read_.MVLOTMAG))
          this.w_OLDLOTMAT = NVL(cp_ToDate(_read_.MVLOTMAT),cp_NullValue(_read_.MVLOTMAT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARSALCOM"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_OLDCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARSALCOM;
            from (i_cTable) where;
                ARCODART = this.w_OLDCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_OLDFCM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_PunPAD = this.oParentObject
        * --- Mi assicuro che la variabile sia space di 15 (per evitare problemi con Oracle)
        this.w_COMMDEFA = NVL(g_PPCODCOM, SPACE(15))
        this.oParentObject.w_MVCODCOM = NVL(this.oParentObject.w_MVCODCOM, SPACE(15))
        this.w_OLDCOMME = NVL(this.w_OLDCOMME, SPACE(15))
        if this.oParentObject.w_FLCOM1="S" or this.w_OLDFCM="S"
          if Empty(this.oParentObject.w_MVCODCOM)
            this.w_CODCOM = this.w_COMMDEFA
          else
            this.w_CODCOM = this.oParentObject.w_MVCODCOM
          endif
          if Empty(this.w_OLDCOMME)
            this.w_OLDCOMME = this.w_COMMDEFA
          endif
          * --- Inverto i valori dei flag letti per effettuare lo storno
          this.w_FLCASC = IIF(this.w_FLCASC="+", "-", IIF(this.w_FLCASC="-", "+", " "))
          this.w_FLORD = IIF(this.w_FLORD="+", "-", IIF(this.w_FLORD="-", "+", " "))
          this.w_FLIMP = IIF(this.w_FLIMP="+", "-", IIF(this.w_FLIMP="-", "+", " "))
          this.w_FLRIS = IIF(this.w_FLRIS="+", "-", IIF(this.w_FLRIS="-", "+", " "))
          this.w_F2CASC = IIF(this.w_F2CASC="+", "-", IIF(this.w_F2CASC="-", "+", " "))
          this.w_F2ORD = IIF(this.w_F2ORD="+", "-", IIF(this.w_F2ORD="-", "+", " "))
          this.w_F2IMP = IIF(this.w_F2IMP="+", "-", IIF(this.w_F2IMP="-", "+", " "))
          this.w_F2RIS = IIF(this.w_F2RIS="+", "-", IIF(this.w_F2RIS="-", "+", " "))
          this.w_RowStatus = this.w_PunPad.RowStatus()
          if this.w_OLDFCM="S" And inlist(this.w_PunPAD.cFunction, "Query", "Edit") and this.w_RowStatus <> "A"
            * --- Storno i Saldi Commessa
            if NOT EMPTY(this.w_FLCASC+this.w_FLRIS+this.w_FLORD+this.w_FLIMP) AND NOT EMPTY(this.w_OLDCODMAG)
              * --- Magazzino principale
              * --- Try
              local bErr_03D16148
              bErr_03D16148=bTrsErr
              this.Try_03D16148()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- accept error
                bTrsErr=.f.
              endif
              bTrsErr=bTrsErr or bErr_03D16148
              * --- End
              if g_MADV="S"
                if (g_PERUBI = "S" OR g_PERLOT = "S") and !Empty(this.w_OLDLOTMAG)
                  * --- Try
                  local bErr_03D0E9D8
                  bErr_03D0E9D8=bTrsErr
                  this.Try_03D0E9D8()
                  * --- Catch
                  if !empty(i_Error)
                    i_ErrMsg=i_Error
                    i_Error=''
                    * --- accept error
                    bTrsErr=.f.
                  endif
                  bTrsErr=bTrsErr or bErr_03D0E9D8
                  * --- End
                endif
              endif
            endif
            if NOT EMPTY(this.w_F2CASC+this.w_F2RIS+this.w_F2ORD+this.w_F2IMP) AND NOT EMPTY(this.w_OLDCODMAT)
              * --- Magazzino collegato
              * --- Try
              local bErr_03D0FAB8
              bErr_03D0FAB8=bTrsErr
              this.Try_03D0FAB8()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- accept error
                bTrsErr=.f.
              endif
              bTrsErr=bTrsErr or bErr_03D0FAB8
              * --- End
              if g_MADV="S"
                if (g_PERUBI = "S" OR g_PERLOT = "S") and !Empty(this.w_OLDLOTMAT)
                  * --- Try
                  local bErr_03D10208
                  bErr_03D10208=bTrsErr
                  this.Try_03D10208()
                  * --- Catch
                  if !empty(i_Error)
                    i_ErrMsg=i_Error
                    i_Error=''
                    * --- accept error
                    bTrsErr=.f.
                  endif
                  bTrsErr=bTrsErr or bErr_03D10208
                  * --- End
                endif
              endif
            endif
          endif
          if this.oParentObject.w_FLCOM1="S" And Inlist(this.w_PunPAD.cFunction, "Load", "Edit") and this.w_RowStatus $ "A-U"
            * --- Caso in cui non sono in interrogazione , quindi non st� cancellando
            * --- Aggiorno i saldi commessa con i nuovi valori.
            if NOT EMPTY(this.oParentObject.w_MVFLCASC+this.oParentObject.w_MVFLRISE+this.oParentObject.w_MVFLORDI+this.oParentObject.w_MVFLIMPE) and !Empty(this.oParentObject.w_MVCODMAG)
              * --- Magazzino principale
              * --- Try
              local bErr_03D124B8
              bErr_03D124B8=bTrsErr
              this.Try_03D124B8()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- accept error
                bTrsErr=.f.
              endif
              bTrsErr=bTrsErr or bErr_03D124B8
              * --- End
              * --- Write into SALDICOM
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDICOM_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                i_cOp1=cp_SetTrsOp(this.oParentObject.w_MVFLCASC,'SCQTAPER','this.oParentObject.w_MVQTAUM1',this.oParentObject.w_MVQTAUM1,'update',i_nConn)
                i_cOp2=cp_SetTrsOp(this.oParentObject.w_MVFLORDI,'SCQTOPER','this.oParentObject.w_MVQTAUM1',this.oParentObject.w_MVQTAUM1,'update',i_nConn)
                i_cOp3=cp_SetTrsOp(this.oParentObject.w_MVFLIMPE,'SCQTIPER','this.oParentObject.w_MVQTAUM1',this.oParentObject.w_MVQTAUM1,'update',i_nConn)
                i_cOp4=cp_SetTrsOp(this.oParentObject.w_MVFLRISE,'SCQTRPER','this.oParentObject.w_MVQTAUM1',this.oParentObject.w_MVQTAUM1,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
                +",SCQTOPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTOPER');
                +",SCQTIPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTIPER');
                +",SCQTRPER ="+cp_NullLink(i_cOp4,'SALDICOM','SCQTRPER');
                    +i_ccchkf ;
                +" where ";
                    +"SCCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVKEYSAL);
                    +" and SCCODMAG = "+cp_ToStrODBC(this.oParentObject.w_MVCODMAG);
                    +" and SCCODCAN = "+cp_ToStrODBC(this.w_CODCOM);
                       )
              else
                update (i_cTable) set;
                    SCQTAPER = &i_cOp1.;
                    ,SCQTOPER = &i_cOp2.;
                    ,SCQTIPER = &i_cOp3.;
                    ,SCQTRPER = &i_cOp4.;
                    &i_ccchkf. ;
                 where;
                    SCCODICE = this.oParentObject.w_MVKEYSAL;
                    and SCCODMAG = this.oParentObject.w_MVCODMAG;
                    and SCCODCAN = this.w_CODCOM;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore Aggiornamento Saldi Commessa'
                return
              endif
              if g_MADV="S"
                if (g_PERUBI = "S" OR g_PERLOT = "S") and !Empty(this.oParentObject.w_MVLOTMAG)
                  * --- Try
                  local bErr_03D10838
                  bErr_03D10838=bTrsErr
                  this.Try_03D10838()
                  * --- Catch
                  if !empty(i_Error)
                    i_ErrMsg=i_Error
                    i_Error=''
                    * --- accept error
                    bTrsErr=.f.
                  endif
                  bTrsErr=bTrsErr or bErr_03D10838
                  * --- End
                  * --- Write into SALOTCOM
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.SALOTCOM_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.SALOTCOM_idx,2])
                    i_cOp1=cp_SetTrsOp(this.oParentObject.w_MVFLCASC,'SMQTAPER','this.oParentObject.w_MVQTAUM1',this.oParentObject.w_MVQTAUM1,'update',i_nConn)
                    i_cOp2=cp_SetTrsOp(this.oParentObject.w_MVFLRISE,'SMQTRPER','this.oParentObject.w_MVQTAUM1',this.oParentObject.w_MVQTAUM1,'update',i_nConn)
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.SALOTCOM_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"SMQTAPER ="+cp_NullLink(i_cOp1,'SALOTCOM','SMQTAPER');
                    +",SMQTRPER ="+cp_NullLink(i_cOp2,'SALOTCOM','SMQTRPER');
                        +i_ccchkf ;
                    +" where ";
                        +"SMCODART = "+cp_ToStrODBC(this.oParentObject.w_MVCODART);
                        +" and SMCODMAG = "+cp_ToStrODBC(this.oParentObject.w_MVLOTMAG);
                        +" and SMCODCAN = "+cp_ToStrODBC(this.w_CODCOM);
                        +" and SMCODUBI = "+cp_ToStrODBC(this.oParentObject.w_MVCODUBI);
                        +" and SMCODLOT = "+cp_ToStrODBC(this.oParentObject.w_MVCODLOT);
                           )
                  else
                    update (i_cTable) set;
                        SMQTAPER = &i_cOp1.;
                        ,SMQTRPER = &i_cOp2.;
                        &i_ccchkf. ;
                     where;
                        SMCODART = this.oParentObject.w_MVCODART;
                        and SMCODMAG = this.oParentObject.w_MVLOTMAG;
                        and SMCODCAN = this.w_CODCOM;
                        and SMCODUBI = this.oParentObject.w_MVCODUBI;
                        and SMCODLOT = this.oParentObject.w_MVCODLOT;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                endif
              endif
            endif
            if NOT EMPTY(this.oParentObject.w_MVF2CASC+this.oParentObject.w_MVF2RISE+this.oParentObject.w_MVF2ORDI+this.oParentObject.w_MVF2IMPE) AND NOT EMPTY(this.oParentObject.w_MVCODMAT)
              * --- Magazzino collegato
              * --- Try
              local bErr_03D28918
              bErr_03D28918=bTrsErr
              this.Try_03D28918()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- accept error
                bTrsErr=.f.
              endif
              bTrsErr=bTrsErr or bErr_03D28918
              * --- End
              * --- Write into SALDICOM
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDICOM_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                i_cOp1=cp_SetTrsOp(this.oParentObject.w_MVF2CASC,'SCQTAPER','this.oParentObject.w_MVQTAUM1',this.oParentObject.w_MVQTAUM1,'update',i_nConn)
                i_cOp2=cp_SetTrsOp(this.oParentObject.w_MVF2ORDI,'SCQTOPER','this.oParentObject.w_MVQTAUM1',this.oParentObject.w_MVQTAUM1,'update',i_nConn)
                i_cOp3=cp_SetTrsOp(this.oParentObject.w_MVF2IMPE,'SCQTIPER','this.oParentObject.w_MVQTAUM1',this.oParentObject.w_MVQTAUM1,'update',i_nConn)
                i_cOp4=cp_SetTrsOp(this.oParentObject.w_MVF2RISE,'SCQTRPER','this.oParentObject.w_MVQTAUM1',this.oParentObject.w_MVQTAUM1,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
                +",SCQTOPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTOPER');
                +",SCQTIPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTIPER');
                +",SCQTRPER ="+cp_NullLink(i_cOp4,'SALDICOM','SCQTRPER');
                    +i_ccchkf ;
                +" where ";
                    +"SCCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVKEYSAL);
                    +" and SCCODMAG = "+cp_ToStrODBC(this.oParentObject.w_MVCODMAT);
                    +" and SCCODCAN = "+cp_ToStrODBC(this.w_CODCOM);
                       )
              else
                update (i_cTable) set;
                    SCQTAPER = &i_cOp1.;
                    ,SCQTOPER = &i_cOp2.;
                    ,SCQTIPER = &i_cOp3.;
                    ,SCQTRPER = &i_cOp4.;
                    &i_ccchkf. ;
                 where;
                    SCCODICE = this.oParentObject.w_MVKEYSAL;
                    and SCCODMAG = this.oParentObject.w_MVCODMAT;
                    and SCCODCAN = this.w_CODCOM;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore Aggiornamento Saldi Commessa'
                return
              endif
              if g_MADV="S"
                if (g_PERUBI = "S" OR g_PERLOT = "S") and !Empty(this.oParentObject.w_MVLOTMAT)
                  * --- Try
                  local bErr_03D279E8
                  bErr_03D279E8=bTrsErr
                  this.Try_03D279E8()
                  * --- Catch
                  if !empty(i_Error)
                    i_ErrMsg=i_Error
                    i_Error=''
                    * --- accept error
                    bTrsErr=.f.
                  endif
                  bTrsErr=bTrsErr or bErr_03D279E8
                  * --- End
                  * --- Write into SALOTCOM
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.SALOTCOM_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.SALOTCOM_idx,2])
                    i_cOp1=cp_SetTrsOp(this.oParentObject.w_MVF2CASC,'SMQTAPER','this.oParentObject.w_MVQTAUM1',this.oParentObject.w_MVQTAUM1,'update',i_nConn)
                    i_cOp2=cp_SetTrsOp(this.oParentObject.w_MVF2RISE,'SMQTRPER','this.oParentObject.w_MVQTAUM1',this.oParentObject.w_MVQTAUM1,'update',i_nConn)
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.SALOTCOM_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"SMQTAPER ="+cp_NullLink(i_cOp1,'SALOTCOM','SMQTAPER');
                    +",SMQTRPER ="+cp_NullLink(i_cOp2,'SALOTCOM','SMQTRPER');
                        +i_ccchkf ;
                    +" where ";
                        +"SMCODART = "+cp_ToStrODBC(this.oParentObject.w_MVCODART);
                        +" and SMCODMAG = "+cp_ToStrODBC(this.oParentObject.w_MVLOTMAT);
                        +" and SMCODCAN = "+cp_ToStrODBC(this.w_CODCOM);
                        +" and SMCODUBI = "+cp_ToStrODBC(this.oParentObject.w_MVCODUB2);
                        +" and SMCODLOT = "+cp_ToStrODBC(this.oParentObject.w_MVCODLOT);
                           )
                  else
                    update (i_cTable) set;
                        SMQTAPER = &i_cOp1.;
                        ,SMQTRPER = &i_cOp2.;
                        &i_ccchkf. ;
                     where;
                        SMCODART = this.oParentObject.w_MVCODART;
                        and SMCODMAG = this.oParentObject.w_MVLOTMAT;
                        and SMCODCAN = this.w_CODCOM;
                        and SMCODUBI = this.oParentObject.w_MVCODUB2;
                        and SMCODLOT = this.oParentObject.w_MVCODLOT;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                endif
              endif
            endif
          endif
        endif
        * --- Evito la chiamata alla mCalc comunque fatta dopo la Valid dei vari campi
        this.bUpdateParentObject=.F.
      case this.pOPER="F"
        if g_CCAR="S" and this.oParentObject.w_CONFIG="S"
          this.w_PunPAD = This.oParentObject
          this.w_SRV = this.w_PunPAD.RowStatus()
          if this.w_SRV = "A"
            * --- Read from ART_ICOL
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ART_ICOL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ARCONCAR"+;
                " from "+i_cTable+" ART_ICOL where ";
                    +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_MVCODART);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ARCONCAR;
                from (i_cTable) where;
                    ARCODART = this.oParentObject.w_MVCODART;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_ARCONCAR = NVL(cp_ToDate(_read_.ARCONCAR),cp_NullValue(_read_.ARCONCAR))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if this.w_ARCONCAR = "S"
              * --- Configuratore di Caratteristiche
              do GSCR_BC5 with this
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
        endif
    endcase
  endproc
  proc Try_03D16148()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into SALDICOM
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_FLCASC,'SCQTAPER','this.w_OLDQTAUM1',this.w_OLDQTAUM1,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_FLORD,'SCQTOPER','this.w_OLDQTAUM1',this.w_OLDQTAUM1,'update',i_nConn)
      i_cOp3=cp_SetTrsOp(this.w_FLIMP,'SCQTIPER','this.w_OLDQTAUM1',this.w_OLDQTAUM1,'update',i_nConn)
      i_cOp4=cp_SetTrsOp(this.w_FLRIS,'SCQTRPER','this.w_OLDQTAUM1',this.w_OLDQTAUM1,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
      +",SCQTOPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTOPER');
      +",SCQTIPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTIPER');
      +",SCQTRPER ="+cp_NullLink(i_cOp4,'SALDICOM','SCQTRPER');
          +i_ccchkf ;
      +" where ";
          +"SCCODICE = "+cp_ToStrODBC(this.w_OLDKEYSAL);
          +" and SCCODMAG = "+cp_ToStrODBC(this.w_OLDCODMAG);
          +" and SCCODCAN = "+cp_ToStrODBC(this.w_OLDCOMME);
             )
    else
      update (i_cTable) set;
          SCQTAPER = &i_cOp1.;
          ,SCQTOPER = &i_cOp2.;
          ,SCQTIPER = &i_cOp3.;
          ,SCQTRPER = &i_cOp4.;
          &i_ccchkf. ;
       where;
          SCCODICE = this.w_OLDKEYSAL;
          and SCCODMAG = this.w_OLDCODMAG;
          and SCCODCAN = this.w_OLDCOMME;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errore Aggiornamento Saldi Commessa'
      return
    endif
    return
  proc Try_03D0E9D8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into SALOTCOM
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALOTCOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALOTCOM_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_FLCASC,'SMQTAPER','this.w_OLDQTAUM1',this.w_OLDQTAUM1,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_FLRIS,'SMQTRPER','this.w_OLDQTAUM1',this.w_OLDQTAUM1,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALOTCOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SMQTAPER ="+cp_NullLink(i_cOp1,'SALOTCOM','SMQTAPER');
      +",SMQTRPER ="+cp_NullLink(i_cOp2,'SALOTCOM','SMQTRPER');
          +i_ccchkf ;
      +" where ";
          +"SMCODART = "+cp_ToStrODBC(this.w_OLDKEYSAL);
          +" and SMCODMAG = "+cp_ToStrODBC(this.w_OLDLOTMAG);
          +" and SMCODCAN = "+cp_ToStrODBC(this.w_OLDCOMME);
          +" and SMCODUBI = "+cp_ToStrODBC(this.w_OLDCODUBI);
          +" and SMCODLOT = "+cp_ToStrODBC(this.w_OLDCODLOT);
             )
    else
      update (i_cTable) set;
          SMQTAPER = &i_cOp1.;
          ,SMQTRPER = &i_cOp2.;
          &i_ccchkf. ;
       where;
          SMCODART = this.w_OLDKEYSAL;
          and SMCODMAG = this.w_OLDLOTMAG;
          and SMCODCAN = this.w_OLDCOMME;
          and SMCODUBI = this.w_OLDCODUBI;
          and SMCODLOT = this.w_OLDCODLOT;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_03D0FAB8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into SALDICOM
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_F2CASC,'SCQTAPER','this.w_OLDQTAUM1',this.w_OLDQTAUM1,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_F2ORD,'SCQTOPER','this.w_OLDQTAUM1',this.w_OLDQTAUM1,'update',i_nConn)
      i_cOp3=cp_SetTrsOp(this.w_F2IMP,'SCQTIPER','this.w_OLDQTAUM1',this.w_OLDQTAUM1,'update',i_nConn)
      i_cOp4=cp_SetTrsOp(this.w_F2RIS,'SCQTRPER','this.w_OLDQTAUM1',this.w_OLDQTAUM1,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
      +",SCQTOPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTOPER');
      +",SCQTIPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTIPER');
      +",SCQTRPER ="+cp_NullLink(i_cOp4,'SALDICOM','SCQTRPER');
          +i_ccchkf ;
      +" where ";
          +"SCCODICE = "+cp_ToStrODBC(this.w_OLDKEYSAL);
          +" and SCCODMAG = "+cp_ToStrODBC(this.w_OLDCODMAT);
          +" and SCCODCAN = "+cp_ToStrODBC(this.w_OLDCOMME);
             )
    else
      update (i_cTable) set;
          SCQTAPER = &i_cOp1.;
          ,SCQTOPER = &i_cOp2.;
          ,SCQTIPER = &i_cOp3.;
          ,SCQTRPER = &i_cOp4.;
          &i_ccchkf. ;
       where;
          SCCODICE = this.w_OLDKEYSAL;
          and SCCODMAG = this.w_OLDCODMAT;
          and SCCODCAN = this.w_OLDCOMME;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errore Aggiornamento Saldi Commessa'
      return
    endif
    return
  proc Try_03D10208()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into SALOTCOM
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALOTCOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALOTCOM_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_F2CASC,'SMQTAPER','this.w_OLDQTAUM1',this.w_OLDQTAUM1,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_F2RIS,'SMQTRPER','this.w_OLDQTAUM1',this.w_OLDQTAUM1,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALOTCOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SMQTAPER ="+cp_NullLink(i_cOp1,'SALOTCOM','SMQTAPER');
      +",SMQTRPER ="+cp_NullLink(i_cOp2,'SALOTCOM','SMQTRPER');
          +i_ccchkf ;
      +" where ";
          +"SMCODART = "+cp_ToStrODBC(this.w_OLDKEYSAL);
          +" and SMCODMAG = "+cp_ToStrODBC(this.w_OLDLOTMAT);
          +" and SMCODCAN = "+cp_ToStrODBC(this.w_OLDCOMME);
          +" and SMCODUBI = "+cp_ToStrODBC(this.w_OLDCODUB2);
          +" and SMCODLOT = "+cp_ToStrODBC(this.w_OLDCODLOT);
             )
    else
      update (i_cTable) set;
          SMQTAPER = &i_cOp1.;
          ,SMQTRPER = &i_cOp2.;
          &i_ccchkf. ;
       where;
          SMCODART = this.w_OLDKEYSAL;
          and SMCODMAG = this.w_OLDLOTMAT;
          and SMCODCAN = this.w_OLDCOMME;
          and SMCODUBI = this.w_OLDCODUB2;
          and SMCODLOT = this.w_OLDCODLOT;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_03D124B8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICOM
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SCCODICE"+",SCCODMAG"+",SCCODCAN"+",SCCODART"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVKEYSAL),'SALDICOM','SCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVCODMAG),'SALDICOM','SCCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'SALDICOM','SCCODCAN');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVCODART),'SALDICOM','SCCODART');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SCCODICE',this.oParentObject.w_MVKEYSAL,'SCCODMAG',this.oParentObject.w_MVCODMAG,'SCCODCAN',this.w_CODCOM,'SCCODART',this.oParentObject.w_MVCODART)
      insert into (i_cTable) (SCCODICE,SCCODMAG,SCCODCAN,SCCODART &i_ccchkf. );
         values (;
           this.oParentObject.w_MVKEYSAL;
           ,this.oParentObject.w_MVCODMAG;
           ,this.w_CODCOM;
           ,this.oParentObject.w_MVCODART;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03D10838()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALOTCOM
    i_nConn=i_TableProp[this.SALOTCOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALOTCOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALOTCOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SMCODART"+",SMCODMAG"+",SMCODCAN"+",SMCODUBI"+",SMCODLOT"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVCODART),'SALOTCOM','SMCODART');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVLOTMAG),'SALOTCOM','SMCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'SALOTCOM','SMCODCAN');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVCODUBI),'SALOTCOM','SMCODUBI');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVCODLOT),'SALOTCOM','SMCODLOT');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SMCODART',this.oParentObject.w_MVCODART,'SMCODMAG',this.oParentObject.w_MVLOTMAG,'SMCODCAN',this.w_CODCOM,'SMCODUBI',this.oParentObject.w_MVCODUBI,'SMCODLOT',this.oParentObject.w_MVCODLOT)
      insert into (i_cTable) (SMCODART,SMCODMAG,SMCODCAN,SMCODUBI,SMCODLOT &i_ccchkf. );
         values (;
           this.oParentObject.w_MVCODART;
           ,this.oParentObject.w_MVLOTMAG;
           ,this.w_CODCOM;
           ,this.oParentObject.w_MVCODUBI;
           ,this.oParentObject.w_MVCODLOT;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03D28918()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICOM
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SCCODICE"+",SCCODMAG"+",SCCODCAN"+",SCCODART"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVKEYSAL),'SALDICOM','SCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVCODMAT),'SALDICOM','SCCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'SALDICOM','SCCODCAN');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVCODART),'SALDICOM','SCCODART');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SCCODICE',this.oParentObject.w_MVKEYSAL,'SCCODMAG',this.oParentObject.w_MVCODMAT,'SCCODCAN',this.w_CODCOM,'SCCODART',this.oParentObject.w_MVCODART)
      insert into (i_cTable) (SCCODICE,SCCODMAG,SCCODCAN,SCCODART &i_ccchkf. );
         values (;
           this.oParentObject.w_MVKEYSAL;
           ,this.oParentObject.w_MVCODMAT;
           ,this.w_CODCOM;
           ,this.oParentObject.w_MVCODART;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03D279E8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALOTCOM
    i_nConn=i_TableProp[this.SALOTCOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALOTCOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALOTCOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SMCODART"+",SMCODMAG"+",SMCODCAN"+",SMCODUBI"+",SMCODLOT"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVCODART),'SALOTCOM','SMCODART');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVLOTMAT),'SALOTCOM','SMCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'SALOTCOM','SMCODCAN');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVCODUB2),'SALOTCOM','SMCODUBI');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVCODLOT),'SALOTCOM','SMCODLOT');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SMCODART',this.oParentObject.w_MVCODART,'SMCODMAG',this.oParentObject.w_MVLOTMAT,'SMCODCAN',this.w_CODCOM,'SMCODUBI',this.oParentObject.w_MVCODUB2,'SMCODLOT',this.oParentObject.w_MVCODLOT)
      insert into (i_cTable) (SMCODART,SMCODMAG,SMCODCAN,SMCODUBI,SMCODLOT &i_ccchkf. );
         values (;
           this.oParentObject.w_MVCODART;
           ,this.oParentObject.w_MVLOTMAT;
           ,this.w_CODCOM;
           ,this.oParentObject.w_MVCODUB2;
           ,this.oParentObject.w_MVCODLOT;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='MVM_DETT'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='SALDICOM'
    this.cWorkTables[4]='SALOTCOM'
    this.cWorkTables[5]='DOC_DETT'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
