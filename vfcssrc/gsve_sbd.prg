* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_sbd                                                        *
*              Stampa brogliaccio documenti                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_115]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-14                                                      *
* Last revis.: 2012-10-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsve_sbd",oParentObject))

* --- Class definition
define class tgsve_sbd as StdForm
  Top    = 34
  Left   = 50

  * --- Standard Properties
  Width  = 540
  Height = 331
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-10-31"
  HelpContextID=107623017
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=31

  * --- Constant Properties
  _IDX = 0
  TIP_DOCU_IDX = 0
  CONTI_IDX = 0
  cPrg = "gsve_sbd"
  cComment = "Stampa brogliaccio documenti"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CICLOAV = space(1)
  w_CATDO1 = space(2)
  o_CATDO1 = space(2)
  w_CATDO2 = space(2)
  o_CATDO2 = space(2)
  w_CATDO2 = space(2)
  w_CATDO3 = space(2)
  o_CATDO3 = space(2)
  w_CATEGO = space(2)
  o_CATEGO = space(2)
  w_CICLO = space(1)
  o_CICLO = space(1)
  w_TIPO = space(1)
  o_TIPO = space(1)
  w_TIPODOC1 = space(5)
  w_TIPODOC2 = space(5)
  w_CLIFOR = space(1)
  w_BDATE = ctod('  /  /  ')
  o_BDATE = ctod('  /  /  ')
  w_EDATE = ctod('  /  /  ')
  w_numero = 0
  w_serie1 = space(10)
  w_numero1 = 0
  w_serie2 = space(10)
  w_FLGNOCON = space(1)
  w_FLPROV = space(1)
  w_CLIENTE = space(15)
  w_FORNIT = space(15)
  w_DESCRI1 = space(40)
  w_TIPOF = space(1)
  w_DESCRI = space(35)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_CATDOC = space(2)
  w_FLVEAC = space(1)
  w_ONUME = 0
  w_TIPODOC = space(5)
  o_TIPODOC = space(5)
  w_SELVEAC = space(1)
  * --- Area Manuale = Declare Variables
  * --- gsve_sbd
  * Eseguo le query in modo sincrono
  sync=.t.
  
  proc SetCPToolBar()
          doDefault()
          oCpToolBar.b4.enabled=.f.
  endproc
  
  * --- Per gestione sicurezza procedure con parametri
  func getSecuritycode()
    local l_CICLO
    l_CICLO=this.oParentObject
    return(lower('GSVE_SBD'+IIF(Type('l_CICLO')='C',','+l_CICLO,'')))
  EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_sbdPag1","gsve_sbd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCATDO1_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='TIP_DOCU'
    this.cWorkTables[2]='CONTI'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CICLOAV=space(1)
      .w_CATDO1=space(2)
      .w_CATDO2=space(2)
      .w_CATDO2=space(2)
      .w_CATDO3=space(2)
      .w_CATEGO=space(2)
      .w_CICLO=space(1)
      .w_TIPO=space(1)
      .w_TIPODOC1=space(5)
      .w_TIPODOC2=space(5)
      .w_CLIFOR=space(1)
      .w_BDATE=ctod("  /  /  ")
      .w_EDATE=ctod("  /  /  ")
      .w_numero=0
      .w_serie1=space(10)
      .w_numero1=0
      .w_serie2=space(10)
      .w_FLGNOCON=space(1)
      .w_FLPROV=space(1)
      .w_CLIENTE=space(15)
      .w_FORNIT=space(15)
      .w_DESCRI1=space(40)
      .w_TIPOF=space(1)
      .w_DESCRI=space(35)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_CATDOC=space(2)
      .w_FLVEAC=space(1)
      .w_ONUME=0
      .w_TIPODOC=space(5)
      .w_SELVEAC=space(1)
        .w_CICLOAV = this.oParentObject
        .w_CATDO1 = 'XX'
        .w_CATDO2 = 'XX'
        .w_CATDO2 = 'XX'
        .w_CATDO3 = 'XX'
        .w_CATEGO = IIF(.w_CICLOAV='V',IIF(g_ACQU='S', IIF(.w_CATDO1='XX', '  ', .w_CATDO1), IIF(.w_CATDO2='IF', 'DI', IIF(.w_CATDO2='DF', 'DT', IIF(.w_CATDO2='XX', '  ', .w_CATDO2)))), IIF(.w_CATDO3='XX', '  ', .w_CATDO3))
        .w_CICLO = IIF(.w_CICLOAV='V', IIF(g_ACQU='S', 'V', IIF(.w_CATDO2 $ 'DF-IF', 'A', 'V')),'A')
        .w_TIPO = IIF(.w_CICLOAV='V',IIF(.w_CICLO='A' AND g_ACQU<>'S', 'F', 'C'),'F')
        .w_TIPODOC1 = SPACE(5)
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_TIPODOC1))
          .link_1_9('Full')
        endif
        .w_TIPODOC2 = SPACE(5)
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_TIPODOC2))
          .link_1_10('Full')
        endif
          .DoRTCalc(11,11,.f.)
        .w_BDATE = G_INIESE
        .w_EDATE = G_FINESE
        .w_numero = 1
        .w_serie1 = ''
        .w_numero1 = 999999999999999
        .w_serie2 = ''
        .w_FLGNOCON = IIF(.w_CICLOAV='V',IIF(g_ACQU<>'S',IIF(.w_CATDO2 $ 'FA-NC-RF-XX',.w_FLGNOCON,'N'),IIF(.w_CATDO1 $ 'FA-NC-RF-XX',.w_FLGNOCON,'N')),IIF(.w_CATDO3 $ 'FA-NC-XX',.w_FLGNOCON,'N'))
        .w_FLPROV = 'N'
        .w_CLIENTE = SPACE(15)
        .DoRTCalc(20,20,.f.)
        if not(empty(.w_CLIENTE))
          .link_1_20('Full')
        endif
        .DoRTCalc(21,21,.f.)
        if not(empty(.w_FORNIT))
          .link_1_21('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
          .DoRTCalc(22,22,.f.)
        .w_TIPOF = IIF(.w_CICLOAV='V','C','F')
          .DoRTCalc(24,24,.f.)
        .w_OBTEST = IIF(EMPTY(.w_BDATE),i_INIDAT,.w_BDATE)
          .DoRTCalc(26,29,.f.)
        .w_TIPODOC = IIF(.w_CICLOAV='V', .w_TIPODOC1, .w_TIPODOC2)
        .w_SELVEAC = ''
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
            .w_CATEGO = IIF(.w_CICLOAV='V',IIF(g_ACQU='S', IIF(.w_CATDO1='XX', '  ', .w_CATDO1), IIF(.w_CATDO2='IF', 'DI', IIF(.w_CATDO2='DF', 'DT', IIF(.w_CATDO2='XX', '  ', .w_CATDO2)))), IIF(.w_CATDO3='XX', '  ', .w_CATDO3))
        if .o_CATDO2<>.w_CATDO2
            .w_CICLO = IIF(.w_CICLOAV='V', IIF(g_ACQU='S', 'V', IIF(.w_CATDO2 $ 'DF-IF', 'A', 'V')),'A')
        endif
            .w_TIPO = IIF(.w_CICLOAV='V',IIF(.w_CICLO='A' AND g_ACQU<>'S', 'F', 'C'),'F')
        if .o_CATEGO<>.w_CATEGO.or. .o_CICLO<>.w_CICLO
            .w_TIPODOC1 = SPACE(5)
          .link_1_9('Full')
        endif
        if .o_CATEGO<>.w_CATEGO.or. .o_CICLO<>.w_CICLO
            .w_TIPODOC2 = SPACE(5)
          .link_1_10('Full')
        endif
        .DoRTCalc(11,17,.t.)
        if .o_CATDO3<>.w_CATDO3.or. .o_CATDO2<>.w_CATDO2.or. .o_CATDO1<>.w_CATDO1
            .w_FLGNOCON = IIF(.w_CICLOAV='V',IIF(g_ACQU<>'S',IIF(.w_CATDO2 $ 'FA-NC-RF-XX',.w_FLGNOCON,'N'),IIF(.w_CATDO1 $ 'FA-NC-RF-XX',.w_FLGNOCON,'N')),IIF(.w_CATDO3 $ 'FA-NC-XX',.w_FLGNOCON,'N'))
        endif
        .DoRTCalc(19,19,.t.)
        if .o_TIPO<>.w_TIPO
            .w_CLIENTE = SPACE(15)
          .link_1_20('Full')
        endif
        if .o_TIPODOC<>.w_TIPODOC
          .link_1_21('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .DoRTCalc(22,24,.t.)
        if .o_BDATE<>.w_BDATE
            .w_OBTEST = IIF(EMPTY(.w_BDATE),i_INIDAT,.w_BDATE)
        endif
        .DoRTCalc(26,29,.t.)
            .w_TIPODOC = IIF(.w_CICLOAV='V', .w_TIPODOC1, .w_TIPODOC2)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(31,31,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
    endwith
  return

  proc Calculate_PYWNKKPVLB()
    with this
          * --- Inizializzo flag da contabilizzare
          .w_FLGNOCON = 'N'
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCATDO1_1_2.enabled = this.oPgFrm.Page1.oPag.oCATDO1_1_2.mCond()
    this.oPgFrm.Page1.oPag.oCATDO2_1_3.enabled = this.oPgFrm.Page1.oPag.oCATDO2_1_3.mCond()
    this.oPgFrm.Page1.oPag.oCATDO2_1_4.enabled = this.oPgFrm.Page1.oPag.oCATDO2_1_4.mCond()
    this.oPgFrm.Page1.oPag.oCICLO_1_7.enabled = this.oPgFrm.Page1.oPag.oCICLO_1_7.mCond()
    this.oPgFrm.Page1.oPag.oFLGNOCON_1_18.enabled = this.oPgFrm.Page1.oPag.oFLGNOCON_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCATDO1_1_2.visible=!this.oPgFrm.Page1.oPag.oCATDO1_1_2.mHide()
    this.oPgFrm.Page1.oPag.oCATDO2_1_3.visible=!this.oPgFrm.Page1.oPag.oCATDO2_1_3.mHide()
    this.oPgFrm.Page1.oPag.oCATDO2_1_4.visible=!this.oPgFrm.Page1.oPag.oCATDO2_1_4.mHide()
    this.oPgFrm.Page1.oPag.oCATDO3_1_5.visible=!this.oPgFrm.Page1.oPag.oCATDO3_1_5.mHide()
    this.oPgFrm.Page1.oPag.oCICLO_1_7.visible=!this.oPgFrm.Page1.oPag.oCICLO_1_7.mHide()
    this.oPgFrm.Page1.oPag.oTIPODOC1_1_9.visible=!this.oPgFrm.Page1.oPag.oTIPODOC1_1_9.mHide()
    this.oPgFrm.Page1.oPag.oTIPODOC2_1_10.visible=!this.oPgFrm.Page1.oPag.oTIPODOC2_1_10.mHide()
    this.oPgFrm.Page1.oPag.oCLIENTE_1_20.visible=!this.oPgFrm.Page1.oPag.oCLIENTE_1_20.mHide()
    this.oPgFrm.Page1.oPag.oFORNIT_1_21.visible=!this.oPgFrm.Page1.oPag.oFORNIT_1_21.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_44.visible=!this.oPgFrm.Page1.oPag.oStr_1_44.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsve_sbd
    THIS.CAPTION='Stampa brogliaccio documenti'+IIF(THIS.OPARENTOBJECT='V',' (vendite)',' (acquisti)')
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_22.Event(cEvent)
        if lower(cEvent)==lower("Init")
          .Calculate_PYWNKKPVLB()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=TIPODOC1
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPODOC1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_TIPODOC1)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE,TDCATDOC,TDFLVEAC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_TIPODOC1))
          select TDTIPDOC,TDDESDOC,TDFLINTE,TDCATDOC,TDFLVEAC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPODOC1)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TIPODOC1) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oTIPODOC1_1_9'),i_cWhere,'GSVE_ATD',"Causali documenti",'GSVE1SBD.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE,TDCATDOC,TDFLVEAC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLINTE,TDCATDOC,TDFLVEAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPODOC1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE,TDCATDOC,TDFLVEAC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_TIPODOC1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_TIPODOC1)
            select TDTIPDOC,TDDESDOC,TDFLINTE,TDCATDOC,TDFLVEAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPODOC1 = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESCRI = NVL(_Link_.TDDESDOC,space(35))
      this.w_CLIFOR = NVL(_Link_.TDFLINTE,space(1))
      this.w_CATDOC = NVL(_Link_.TDCATDOC,space(2))
      this.w_FLVEAC = NVL(_Link_.TDFLVEAC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TIPODOC1 = space(5)
      endif
      this.w_DESCRI = space(35)
      this.w_CLIFOR = space(1)
      this.w_CATDOC = space(2)
      this.w_FLVEAC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_TIPODOC) OR (g_ACQU<>'S' OR (.w_CATDOC<>'OR' AND .w_FLVEAC='V'))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_TIPODOC1 = space(5)
        this.w_DESCRI = space(35)
        this.w_CLIFOR = space(1)
        this.w_CATDOC = space(2)
        this.w_FLVEAC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPODOC1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPODOC2
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPODOC2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAC_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_TIPODOC2)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE,TDCATDOC,TDFLVEAC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_TIPODOC2))
          select TDTIPDOC,TDDESDOC,TDFLINTE,TDCATDOC,TDFLVEAC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPODOC2)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TIPODOC2) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oTIPODOC2_1_10'),i_cWhere,'GSAC_ATD',"Causali documenti",'GSVE1SBD.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE,TDCATDOC,TDFLVEAC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLINTE,TDCATDOC,TDFLVEAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPODOC2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE,TDCATDOC,TDFLVEAC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_TIPODOC2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_TIPODOC2)
            select TDTIPDOC,TDDESDOC,TDFLINTE,TDCATDOC,TDFLVEAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPODOC2 = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESCRI = NVL(_Link_.TDDESDOC,space(35))
      this.w_CLIFOR = NVL(_Link_.TDFLINTE,space(1))
      this.w_CATDOC = NVL(_Link_.TDCATDOC,space(2))
      this.w_FLVEAC = NVL(_Link_.TDFLVEAC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TIPODOC2 = space(5)
      endif
      this.w_DESCRI = space(35)
      this.w_CLIFOR = space(1)
      this.w_CATDOC = space(2)
      this.w_FLVEAC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_TIPODOC) OR (.w_CATDOC<>'OR' AND .w_FLVEAC='A')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_TIPODOC2 = space(5)
        this.w_DESCRI = space(35)
        this.w_CLIFOR = space(1)
        this.w_CATDOC = space(2)
        this.w_FLVEAC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPODOC2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLIENTE
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLIENTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CLIENTE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_tipo);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_tipo;
                     ,'ANCODICE',trim(this.w_CLIENTE))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLIENTE)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CLIENTE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_tipo);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CLIENTE)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_tipo);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CLIENTE) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCLIENTE_1_20'),i_cWhere,'GSAR_BZC',"Elenco clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_tipo<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice intestatario inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_tipo);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLIENTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CLIENTE);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_tipo);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_tipo;
                       ,'ANCODICE',this.w_CLIENTE)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLIENTE = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRI1 = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CLIENTE = space(15)
      endif
      this.w_DESCRI1 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice intestatario inesistente oppure obsoleto")
        endif
        this.w_CLIENTE = space(15)
        this.w_DESCRI1 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLIENTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FORNIT
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FORNIT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFR',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_FORNIT)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPOF);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPOF;
                     ,'ANCODICE',trim(this.w_FORNIT))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FORNIT)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_FORNIT)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPOF);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_FORNIT)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPOF);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_FORNIT) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oFORNIT_1_21'),i_cWhere,'GSAR_AFR',"Elenco fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPOF<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPOF);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FORNIT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_FORNIT);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPOF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPOF;
                       ,'ANCODICE',this.w_FORNIT)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FORNIT = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRI1 = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_FORNIT = space(15)
      endif
      this.w_DESCRI1 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente oppure obsoleto")
        endif
        this.w_FORNIT = space(15)
        this.w_DESCRI1 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FORNIT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCATDO1_1_2.RadioValue()==this.w_CATDO1)
      this.oPgFrm.Page1.oPag.oCATDO1_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCATDO2_1_3.RadioValue()==this.w_CATDO2)
      this.oPgFrm.Page1.oPag.oCATDO2_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCATDO2_1_4.RadioValue()==this.w_CATDO2)
      this.oPgFrm.Page1.oPag.oCATDO2_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCATDO3_1_5.RadioValue()==this.w_CATDO3)
      this.oPgFrm.Page1.oPag.oCATDO3_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCICLO_1_7.RadioValue()==this.w_CICLO)
      this.oPgFrm.Page1.oPag.oCICLO_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPODOC1_1_9.value==this.w_TIPODOC1)
      this.oPgFrm.Page1.oPag.oTIPODOC1_1_9.value=this.w_TIPODOC1
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPODOC2_1_10.value==this.w_TIPODOC2)
      this.oPgFrm.Page1.oPag.oTIPODOC2_1_10.value=this.w_TIPODOC2
    endif
    if not(this.oPgFrm.Page1.oPag.oBDATE_1_12.value==this.w_BDATE)
      this.oPgFrm.Page1.oPag.oBDATE_1_12.value=this.w_BDATE
    endif
    if not(this.oPgFrm.Page1.oPag.oEDATE_1_13.value==this.w_EDATE)
      this.oPgFrm.Page1.oPag.oEDATE_1_13.value=this.w_EDATE
    endif
    if not(this.oPgFrm.Page1.oPag.onumero_1_14.value==this.w_numero)
      this.oPgFrm.Page1.oPag.onumero_1_14.value=this.w_numero
    endif
    if not(this.oPgFrm.Page1.oPag.oserie1_1_15.value==this.w_serie1)
      this.oPgFrm.Page1.oPag.oserie1_1_15.value=this.w_serie1
    endif
    if not(this.oPgFrm.Page1.oPag.onumero1_1_16.value==this.w_numero1)
      this.oPgFrm.Page1.oPag.onumero1_1_16.value=this.w_numero1
    endif
    if not(this.oPgFrm.Page1.oPag.oserie2_1_17.value==this.w_serie2)
      this.oPgFrm.Page1.oPag.oserie2_1_17.value=this.w_serie2
    endif
    if not(this.oPgFrm.Page1.oPag.oFLGNOCON_1_18.RadioValue()==this.w_FLGNOCON)
      this.oPgFrm.Page1.oPag.oFLGNOCON_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLPROV_1_19.RadioValue()==this.w_FLPROV)
      this.oPgFrm.Page1.oPag.oFLPROV_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCLIENTE_1_20.value==this.w_CLIENTE)
      this.oPgFrm.Page1.oPag.oCLIENTE_1_20.value=this.w_CLIENTE
    endif
    if not(this.oPgFrm.Page1.oPag.oFORNIT_1_21.value==this.w_FORNIT)
      this.oPgFrm.Page1.oPag.oFORNIT_1_21.value=this.w_FORNIT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI1_1_32.value==this.w_DESCRI1)
      this.oPgFrm.Page1.oPag.oDESCRI1_1_32.value=this.w_DESCRI1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_36.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_36.value=this.w_DESCRI
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CICLO))  and not(g_ACQU='S' OR IsAlt())  and (.w_CATDO2='XX' AND !(g_ACQU='S' OR IsAlt()))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCICLO_1_7.SetFocus()
            i_bnoObbl = !empty(.w_CICLO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_TIPODOC) OR (g_ACQU<>'S' OR (.w_CATDOC<>'OR' AND .w_FLVEAC='V')))  and not(.w_CICLOAV='A')  and not(empty(.w_TIPODOC1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTIPODOC1_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_TIPODOC) OR (.w_CATDOC<>'OR' AND .w_FLVEAC='A'))  and not(.w_CICLOAV='V')  and not(empty(.w_TIPODOC2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTIPODOC2_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_BDATE)) or not((empty(.w_edate)) OR (.w_edate>=.w_bdate)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oBDATE_1_12.SetFocus()
            i_bnoObbl = !empty(.w_BDATE)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data finale � minore della data iniziale")
          case   ((empty(.w_EDATE)) or not((.w_EDATE>=.w_BDATE) OR  ( empty(.w_bdate))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEDATE_1_13.SetFocus()
            i_bnoObbl = !empty(.w_EDATE)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data finale � minore della data iniziale")
          case   not((empty(.w_NUMERO1)) OR  (.w_NUMERO<=.w_NUMERO1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.onumero_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero documento iniziale � pi� grande del codice finale")
          case   not((empty(.w_SERIE2)) OR  (.w_SERIE1<=.w_SERIE2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oserie1_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � pi� grande della serie finale")
          case   not((.w_NUMERO1>=.w_NUMERO) or (empty(.w_NUMERO1)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.onumero1_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero documento iniziale � pi� grande del codice finale")
          case   not((.w_serie2>=.w_serie1) or (empty(.w_serie1)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oserie2_1_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � maggire della serie finale")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(.w_CICLOAV='A')  and not(empty(.w_CLIENTE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCLIENTE_1_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice intestatario inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(.w_CICLOAV='V')  and not(empty(.w_FORNIT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFORNIT_1_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice fornitore inesistente oppure obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CATDO1 = this.w_CATDO1
    this.o_CATDO2 = this.w_CATDO2
    this.o_CATDO3 = this.w_CATDO3
    this.o_CATEGO = this.w_CATEGO
    this.o_CICLO = this.w_CICLO
    this.o_TIPO = this.w_TIPO
    this.o_BDATE = this.w_BDATE
    this.o_TIPODOC = this.w_TIPODOC
    return

enddefine

* --- Define pages as container
define class tgsve_sbdPag1 as StdContainer
  Width  = 536
  height = 331
  stdWidth  = 536
  stdheight = 331
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oCATDO1_1_2 as StdCombo with uid="QDJUHAEQBR",rtseq=2,rtrep=.f.,left=106,top=9,width=130,height=21;
    , ToolTipText = "Categoria documento di selezione";
    , HelpContextID = 3189978;
    , cFormVar="w_CATDO1",RowSource=""+"Tutti i documenti,"+"Documenti interni,"+"Doc. di trasporto,"+"Fatture,"+"Note di credito,"+"Ricevute fiscali", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCATDO1_1_2.RadioValue()
    return(iif(this.value =1,'XX',;
    iif(this.value =2,'DI',;
    iif(this.value =3,'DT',;
    iif(this.value =4,'FA',;
    iif(this.value =5,'NC',;
    iif(this.value =6,'RF',;
    space(2))))))))
  endfunc
  func oCATDO1_1_2.GetRadio()
    this.Parent.oContained.w_CATDO1 = this.RadioValue()
    return .t.
  endfunc

  func oCATDO1_1_2.SetRadio()
    this.Parent.oContained.w_CATDO1=trim(this.Parent.oContained.w_CATDO1)
    this.value = ;
      iif(this.Parent.oContained.w_CATDO1=='XX',1,;
      iif(this.Parent.oContained.w_CATDO1=='DI',2,;
      iif(this.Parent.oContained.w_CATDO1=='DT',3,;
      iif(this.Parent.oContained.w_CATDO1=='FA',4,;
      iif(this.Parent.oContained.w_CATDO1=='NC',5,;
      iif(this.Parent.oContained.w_CATDO1=='RF',6,;
      0))))))
  endfunc

  func oCATDO1_1_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_ACQU='S')
    endwith
   endif
  endfunc

  func oCATDO1_1_2.mHide()
    with this.Parent.oContained
      return (g_ACQU<>'S' AND .w_CICLOAV='V' OR .w_CICLOAV='A')
    endwith
  endfunc


  add object oCATDO2_1_3 as StdCombo with uid="UPJNZIKQML",rtseq=3,rtrep=.f.,left=106,top=9,width=130,height=21;
    , ToolTipText = "Categoria documento di selezione";
    , HelpContextID = 254848218;
    , cFormVar="w_CATDO2",RowSource=""+"Tutti i documenti,"+"Documenti interni,"+"Doc. di trasporto,"+"Fatture,"+"Note di credito,"+"Ricevute fiscali,"+"DDT a fornitore,"+"Carichi a fornitore", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCATDO2_1_3.RadioValue()
    return(iif(this.value =1,'XX',;
    iif(this.value =2,'DI',;
    iif(this.value =3,'DT',;
    iif(this.value =4,'FA',;
    iif(this.value =5,'NC',;
    iif(this.value =6,'RF',;
    iif(this.value =7,'DF',;
    iif(this.value =8,'IF',;
    space(2))))))))))
  endfunc
  func oCATDO2_1_3.GetRadio()
    this.Parent.oContained.w_CATDO2 = this.RadioValue()
    return .t.
  endfunc

  func oCATDO2_1_3.SetRadio()
    this.Parent.oContained.w_CATDO2=trim(this.Parent.oContained.w_CATDO2)
    this.value = ;
      iif(this.Parent.oContained.w_CATDO2=='XX',1,;
      iif(this.Parent.oContained.w_CATDO2=='DI',2,;
      iif(this.Parent.oContained.w_CATDO2=='DT',3,;
      iif(this.Parent.oContained.w_CATDO2=='FA',4,;
      iif(this.Parent.oContained.w_CATDO2=='NC',5,;
      iif(this.Parent.oContained.w_CATDO2=='RF',6,;
      iif(this.Parent.oContained.w_CATDO2=='DF',7,;
      iif(this.Parent.oContained.w_CATDO2=='IF',8,;
      0))))))))
  endfunc

  func oCATDO2_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!(g_ACQU='S' AND .w_CICLOAV='V' OR .w_CICLOAV='A' OR ISALT()))
    endwith
   endif
  endfunc

  func oCATDO2_1_3.mHide()
    with this.Parent.oContained
      return (g_ACQU='S' AND .w_CICLOAV='V' OR .w_CICLOAV='A' OR ISALT())
    endwith
  endfunc


  add object oCATDO2_1_4 as StdCombo with uid="KLSHGSXUVJ",rtseq=4,rtrep=.f.,left=106,top=9,width=130,height=21;
    , ToolTipText = "Categoria documento di selezione";
    , HelpContextID = 254848218;
    , cFormVar="w_CATDO2",RowSource=""+"Tutti i documenti,"+"Documenti interni,"+"Fatture,"+"Note di credito", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCATDO2_1_4.RadioValue()
    return(iif(this.value =1,'XX',;
    iif(this.value =2,'DI',;
    iif(this.value =3,'FA',;
    iif(this.value =4,'NC',;
    space(2))))))
  endfunc
  func oCATDO2_1_4.GetRadio()
    this.Parent.oContained.w_CATDO2 = this.RadioValue()
    return .t.
  endfunc

  func oCATDO2_1_4.SetRadio()
    this.Parent.oContained.w_CATDO2=trim(this.Parent.oContained.w_CATDO2)
    this.value = ;
      iif(this.Parent.oContained.w_CATDO2=='XX',1,;
      iif(this.Parent.oContained.w_CATDO2=='DI',2,;
      iif(this.Parent.oContained.w_CATDO2=='FA',3,;
      iif(this.Parent.oContained.w_CATDO2=='NC',4,;
      0))))
  endfunc

  func oCATDO2_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAlt())
    endwith
   endif
  endfunc

  func oCATDO2_1_4.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc


  add object oCATDO3_1_5 as StdCombo with uid="AZFEXNVYDI",rtseq=5,rtrep=.f.,left=106,top=9,width=130,height=21;
    , ToolTipText = "Categoria di appartenenza del documento selezionata";
    , HelpContextID = 238071002;
    , cFormVar="w_CATDO3",RowSource=""+"Tutti i documenti,"+"Documenti interni,"+"Doc. di trasporto,"+"Fatture,"+"Note di credito", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCATDO3_1_5.RadioValue()
    return(iif(this.value =1,'XX',;
    iif(this.value =2,'DI',;
    iif(this.value =3,'DT',;
    iif(this.value =4,'FA',;
    iif(this.value =5,'NC',;
    space(2)))))))
  endfunc
  func oCATDO3_1_5.GetRadio()
    this.Parent.oContained.w_CATDO3 = this.RadioValue()
    return .t.
  endfunc

  func oCATDO3_1_5.SetRadio()
    this.Parent.oContained.w_CATDO3=trim(this.Parent.oContained.w_CATDO3)
    this.value = ;
      iif(this.Parent.oContained.w_CATDO3=='XX',1,;
      iif(this.Parent.oContained.w_CATDO3=='DI',2,;
      iif(this.Parent.oContained.w_CATDO3=='DT',3,;
      iif(this.Parent.oContained.w_CATDO3=='FA',4,;
      iif(this.Parent.oContained.w_CATDO3=='NC',5,;
      0)))))
  endfunc

  func oCATDO3_1_5.mHide()
    with this.Parent.oContained
      return (.w_CICLOAV='V')
    endwith
  endfunc


  add object oCICLO_1_7 as StdCombo with uid="YIZCQXKROC",rtseq=7,rtrep=.f.,left=310,top=9,width=121,height=21;
    , ToolTipText = "Tipo gestione interessata";
    , HelpContextID = 19510490;
    , cFormVar="w_CICLO",RowSource=""+"Vendite,"+"Acquisti", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oCICLO_1_7.RadioValue()
    return(iif(this.value =1,'V',;
    iif(this.value =2,'A',;
    space(1))))
  endfunc
  func oCICLO_1_7.GetRadio()
    this.Parent.oContained.w_CICLO = this.RadioValue()
    return .t.
  endfunc

  func oCICLO_1_7.SetRadio()
    this.Parent.oContained.w_CICLO=trim(this.Parent.oContained.w_CICLO)
    this.value = ;
      iif(this.Parent.oContained.w_CICLO=='V',1,;
      iif(this.Parent.oContained.w_CICLO=='A',2,;
      0))
  endfunc

  func oCICLO_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CATDO2='XX' AND !(g_ACQU='S' OR IsAlt()))
    endwith
   endif
  endfunc

  func oCICLO_1_7.mHide()
    with this.Parent.oContained
      return (g_ACQU='S' OR IsAlt())
    endwith
  endfunc

  add object oTIPODOC1_1_9 as StdField with uid="QVHWJTDCVW",rtseq=9,rtrep=.f.,;
    cFormVar = "w_TIPODOC1", cQueryName = "TIPODOC1",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipo documento selezionato",;
    HelpContextID = 220863591,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=106, Top=42, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_TIPODOC1"

  func oTIPODOC1_1_9.mHide()
    with this.Parent.oContained
      return (.w_CICLOAV='A')
    endwith
  endfunc

  func oTIPODOC1_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIPODOC1_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIPODOC1_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oTIPODOC1_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Causali documenti",'GSVE1SBD.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oTIPODOC1_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_TIPODOC1
     i_obj.ecpSave()
  endproc

  add object oTIPODOC2_1_10 as StdField with uid="XBKNFLCIMB",rtseq=10,rtrep=.f.,;
    cFormVar = "w_TIPODOC2", cQueryName = "TIPODOC2",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipo documento selezionato",;
    HelpContextID = 220863592,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=106, Top=42, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSAC_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_TIPODOC2"

  func oTIPODOC2_1_10.mHide()
    with this.Parent.oContained
      return (.w_CICLOAV='V')
    endwith
  endfunc

  func oTIPODOC2_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIPODOC2_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIPODOC2_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oTIPODOC2_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAC_ATD',"Causali documenti",'GSVE1SBD.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oTIPODOC2_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSAC_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_TIPODOC2
     i_obj.ecpSave()
  endproc

  add object oBDATE_1_12 as StdField with uid="JWHNCRKDRD",rtseq=12,rtrep=.f.,;
    cFormVar = "w_BDATE", cQueryName = "BDATE",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data finale � minore della data iniziale",;
    ToolTipText = "Data documento di inizio stampa",;
    HelpContextID = 29481450,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=75, Top=110

  func oBDATE_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_edate)) OR (.w_edate>=.w_bdate))
    endwith
    return bRes
  endfunc

  add object oEDATE_1_13 as StdField with uid="OWEPFQDEHL",rtseq=13,rtrep=.f.,;
    cFormVar = "w_EDATE", cQueryName = "EDATE",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data finale � minore della data iniziale",;
    ToolTipText = "Data documento di fine stampa",;
    HelpContextID = 29481402,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=75, Top=136

  func oEDATE_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_EDATE>=.w_BDATE) OR  ( empty(.w_bdate)))
    endwith
    return bRes
  endfunc

  add object onumero_1_14 as StdField with uid="BXBQYPGVTU",rtseq=14,rtrep=.f.,;
    cFormVar = "w_numero", cQueryName = "numero",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero documento iniziale � pi� grande del codice finale",;
    ToolTipText = "Numero del documento iniziale selezionato",;
    HelpContextID = 2234838,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=297, Top=110, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func onumero_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_NUMERO1)) OR  (.w_NUMERO<=.w_NUMERO1))
    endwith
    return bRes
  endfunc

  add object oserie1_1_15 as StdField with uid="HVPVSEHHGU",rtseq=15,rtrep=.f.,;
    cFormVar = "w_serie1", cQueryName = "serie1",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � pi� grande della serie finale",;
    ToolTipText = "Alfa del documento iniziale selezionato",;
    HelpContextID = 245999066,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=426, Top=110, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  func oserie1_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_SERIE2)) OR  (.w_SERIE1<=.w_SERIE2))
    endwith
    return bRes
  endfunc

  add object onumero1_1_16 as StdField with uid="PROBAWFVAK",rtseq=16,rtrep=.f.,;
    cFormVar = "w_numero1", cQueryName = "numero1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero documento iniziale � pi� grande del codice finale",;
    ToolTipText = "Numero del documento finale selezionato",;
    HelpContextID = 2234838,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=297, Top=136, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func onumero1_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_NUMERO1>=.w_NUMERO) or (empty(.w_NUMERO1)))
    endwith
    return bRes
  endfunc

  add object oserie2_1_17 as StdField with uid="LHPNDMBVHJ",rtseq=17,rtrep=.f.,;
    cFormVar = "w_serie2", cQueryName = "serie2",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � maggire della serie finale",;
    ToolTipText = "Alfa del documento finale selezionato",;
    HelpContextID = 229221850,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=426, Top=136, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  func oserie2_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_serie2>=.w_serie1) or (empty(.w_serie1)))
    endwith
    return bRes
  endfunc

  add object oFLGNOCON_1_18 as StdCheck with uid="IDZEZPEEQP",rtseq=18,rtrep=.f.,left=75, top=166, caption="Solo da contabilizzare",;
    ToolTipText = "Mostra solo i documenti da contabilizzare",;
    HelpContextID = 237465948,;
    cFormVar="w_FLGNOCON", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLGNOCON_1_18.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLGNOCON_1_18.GetRadio()
    this.Parent.oContained.w_FLGNOCON = this.RadioValue()
    return .t.
  endfunc

  func oFLGNOCON_1_18.SetRadio()
    this.Parent.oContained.w_FLGNOCON=trim(this.Parent.oContained.w_FLGNOCON)
    this.value = ;
      iif(this.Parent.oContained.w_FLGNOCON=='S',1,;
      0)
  endfunc

  func oFLGNOCON_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IIF(.w_CICLOAV='V',IIF(g_ACQU<>'S', .w_CATDO2 $ 'FA-NC-RF-XX', .w_CATDO1 $ 'FA-NC-RF-XX'), .w_CATDO3 $ 'FA-NC-XX'))
    endwith
   endif
  endfunc


  add object oFLPROV_1_19 as StdCombo with uid="QMVLQXRXPN",value=3,rtseq=19,rtrep=.f.,left=406,top=168,width=103,height=21;
    , ToolTipText = "Permette di selezionare tutti i documenti, solo quelli provvisori o confermati";
    , HelpContextID = 81600086;
    , cFormVar="w_FLPROV",RowSource=""+"Confermati,"+"Provvisori,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLPROV_1_19.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'',;
    space(1)))))
  endfunc
  func oFLPROV_1_19.GetRadio()
    this.Parent.oContained.w_FLPROV = this.RadioValue()
    return .t.
  endfunc

  func oFLPROV_1_19.SetRadio()
    this.Parent.oContained.w_FLPROV=trim(this.Parent.oContained.w_FLPROV)
    this.value = ;
      iif(this.Parent.oContained.w_FLPROV=='N',1,;
      iif(this.Parent.oContained.w_FLPROV=='S',2,;
      iif(this.Parent.oContained.w_FLPROV=='',3,;
      0)))
  endfunc

  add object oCLIENTE_1_20 as StdField with uid="DNOSNHKUKW",rtseq=20,rtrep=.f.,;
    cFormVar = "w_CLIENTE", cQueryName = "CLIENTE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice intestatario inesistente oppure obsoleto",;
    ToolTipText = "Codice intestatario di selezione",;
    HelpContextID = 46116390,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=99, Top=205, cSayPict="p_CLF", cGetPict="p_CLF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_tipo", oKey_2_1="ANCODICE", oKey_2_2="this.w_CLIENTE"

  func oCLIENTE_1_20.mHide()
    with this.Parent.oContained
      return (.w_CICLOAV='A')
    endwith
  endfunc

  func oCLIENTE_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLIENTE_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLIENTE_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_tipo)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_tipo)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCLIENTE_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco clienti/fornitori",'',this.parent.oContained
  endproc
  proc oCLIENTE_1_20.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_tipo
     i_obj.w_ANCODICE=this.parent.oContained.w_CLIENTE
     i_obj.ecpSave()
  endproc

  add object oFORNIT_1_21 as StdField with uid="LNXXFZUZXT",rtseq=21,rtrep=.f.,;
    cFormVar = "w_FORNIT", cQueryName = "FORNIT",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice fornitore inesistente oppure obsoleto",;
    ToolTipText = "Fornitore selezionato",;
    HelpContextID = 41501014,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=99, Top=205, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_AFR", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPOF", oKey_2_1="ANCODICE", oKey_2_2="this.w_FORNIT"

  func oFORNIT_1_21.mHide()
    with this.Parent.oContained
      return (.w_CICLOAV='V')
    endwith
  endfunc

  func oFORNIT_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oFORNIT_1_21.ecpDrop(oSource)
    this.Parent.oContained.link_1_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFORNIT_1_21.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPOF)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPOF)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oFORNIT_1_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFR',"Elenco fornitori",'',this.parent.oContained
  endproc
  proc oFORNIT_1_21.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPOF
     i_obj.w_ANCODICE=this.parent.oContained.w_FORNIT
     i_obj.ecpSave()
  endproc


  add object oObj_1_22 as cp_outputCombo with uid="ZLERXIVPWT",left=99, top=239, width=428,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 70374630


  add object oBtn_1_23 as StdButton with uid="ZNREPGHWCX",left=426, top=274, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , HelpContextID = 107602458;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_23.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_24 as StdButton with uid="XYGIHPUFHG",left=479, top=274, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 215097350;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_24.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCRI1_1_32 as StdField with uid="KLMLYMSVHU",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DESCRI1", cQueryName = "DESCRI1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 134104886,;
   bGlobalFont=.t.,;
    Height=21, Width=272, Left=231, Top=205, InputMask=replicate('X',40)

  add object oDESCRI_1_36 as StdField with uid="RLYFINUUXX",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 134330570,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=170, Top=42, InputMask=replicate('X',35)

  add object oStr_1_25 as StdString with uid="NXSDBFWLJV",Visible=.t., Left=12, Top=110,;
    Alignment=1, Width=61, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="LJKHLIFCHP",Visible=.t., Left=26, Top=136,;
    Alignment=1, Width=47, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="NCIHTWZTBT",Visible=.t., Left=212, Top=110,;
    Alignment=1, Width=83, Height=15,;
    Caption="Da numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="MQFLLNLPCM",Visible=.t., Left=226, Top=136,;
    Alignment=1, Width=69, Height=15,;
    Caption="A numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="OOAUBIWOYB",Visible=.t., Left=11, Top=9,;
    Alignment=1, Width=92, Height=15,;
    Caption="Categoria:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="HDIGEMQYKL",Visible=.t., Left=13, Top=80,;
    Alignment=0, Width=385, Height=15,;
    Caption="Selezione documenti"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="HVWOGBOYOF",Visible=.t., Left=6, Top=239,;
    Alignment=1, Width=92, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="XSZYPLRAJY",Visible=.t., Left=6, Top=42,;
    Alignment=1, Width=97, Height=15,;
    Caption="Tipo documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="LVVBINNLET",Visible=.t., Left=414, Top=110,;
    Alignment=2, Width=12, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="PKVCFMTCZS",Visible=.t., Left=414, Top=136,;
    Alignment=2, Width=12, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="DECCIVKNUZ",Visible=.t., Left=255, Top=9,;
    Alignment=1, Width=52, Height=18,;
    Caption="Ciclo:"  ;
  , bGlobalFont=.t.

  func oStr_1_44.mHide()
    with this.Parent.oContained
      return (g_ACQU='S' OR IsAlt())
    endwith
  endfunc

  add object oStr_1_45 as StdString with uid="DMPRCDDUMT",Visible=.t., Left=335, Top=168,;
    Alignment=1, Width=67, Height=18,;
    Caption="Stato doc:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="GBYRXKSVAS",Visible=.t., Left=10, Top=205,;
    Alignment=1, Width=88, Height=18,;
    Caption="Intestatario:"  ;
  , bGlobalFont=.t.

  add object oBox_1_29 as StdBox with uid="YDFMXVPMAP",left=4, top=96, width=519,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_sbd','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
