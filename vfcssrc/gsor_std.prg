* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsor_std                                                        *
*              Tracciabilità documenti                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30][VRS_90]                                                    *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-04-21                                                      *
* Last revis.: 2015-03-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsor_std",oParentObject))

* --- Class definition
define class tgsor_std as StdForm
  Top    = 11
  Left   = 15

  * --- Standard Properties
  Width  = 775
  Height = 449
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-03-30"
  HelpContextID=74017129
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=41

  * --- Constant Properties
  _IDX = 0
  TIP_DOCU_IDX = 0
  CONTI_IDX = 0
  cPrg = "gsor_std"
  cComment = "Tracciabilità documenti"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TIPCAU = space(1)
  o_TIPCAU = space(1)
  w_CAUDOC = space(5)
  o_CAUDOC = space(5)
  w_CODCLIFOR = space(15)
  w_DATREG1 = ctod('  /  /  ')
  w_DATREG2 = ctod('  /  /  ')
  w_NUMDO1 = 0
  w_ALFDO1 = space(10)
  w_DATDO1 = ctod('  /  /  ')
  w_NUMRIF = 0
  w_ALFRIF = space(10)
  w_DATRIF = ctod('  /  /  ')
  w_DESTIP = space(35)
  w_TIPOCF = space(1)
  w_CODDES = space(35)
  w_MVSERIAL = space(10)
  w_TIPO = space(1)
  w_SERIALE = space(10)
  w_CLADOC = space(2)
  w_VSRIF = space(2)
  w_FLVEAC = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_MVRIFCLI = space(30)
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_DATDOC = ctod('  /  /  ')
  o_DATDOC = ctod('  /  /  ')
  w_NUMRI1 = 0
  w_ALFRI1 = space(10)
  w_DATRI1 = ctod('  /  /  ')
  w_DOCESP = space(1)
  w_LEGIND = space(1)
  w_SELPRV = space(1)
  w_SELBOZ = space(1)
  w_SELNSP = space(1)
  w_SELALT = space(1)
  w_SELPRA = space(1)
  w_SELPRO = space(1)
  w_SELFAA = space(1)
  w_SELFAT = space(1)
  w_SELNCR = space(1)
  w_OFSERIALE = space(10)
  w_Zoom = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsor_stdPag1","gsor_std",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPCAU_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_Zoom = this.oPgFrm.Pages(1).oPag.Zoom
    DoDefault()
    proc Destroy()
      this.w_Zoom = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='TIP_DOCU'
    this.cWorkTables[2]='CONTI'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPCAU=space(1)
      .w_CAUDOC=space(5)
      .w_CODCLIFOR=space(15)
      .w_DATREG1=ctod("  /  /  ")
      .w_DATREG2=ctod("  /  /  ")
      .w_NUMDO1=0
      .w_ALFDO1=space(10)
      .w_DATDO1=ctod("  /  /  ")
      .w_NUMRIF=0
      .w_ALFRIF=space(10)
      .w_DATRIF=ctod("  /  /  ")
      .w_DESTIP=space(35)
      .w_TIPOCF=space(1)
      .w_CODDES=space(35)
      .w_MVSERIAL=space(10)
      .w_TIPO=space(1)
      .w_SERIALE=space(10)
      .w_CLADOC=space(2)
      .w_VSRIF=space(2)
      .w_FLVEAC=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_MVRIFCLI=space(30)
      .w_NUMDOC=0
      .w_ALFDOC=space(10)
      .w_DATDOC=ctod("  /  /  ")
      .w_NUMRI1=0
      .w_ALFRI1=space(10)
      .w_DATRI1=ctod("  /  /  ")
      .w_DOCESP=space(1)
      .w_LEGIND=space(1)
      .w_SELPRV=space(1)
      .w_SELBOZ=space(1)
      .w_SELNSP=space(1)
      .w_SELALT=space(1)
      .w_SELPRA=space(1)
      .w_SELPRO=space(1)
      .w_SELFAA=space(1)
      .w_SELFAT=space(1)
      .w_SELNCR=space(1)
      .w_OFSERIALE=space(10)
        .w_TIPCAU = 'V'
        .w_CAUDOC = SPACE(5)
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CAUDOC))
          .link_1_2('Full')
        endif
        .w_CODCLIFOR = IIF(NVL(.w_TIPOCF,'N') $ 'CF',nvl(.w_CODCLIFOR,''),'')
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODCLIFOR))
          .link_1_3('Full')
        endif
        .w_DATREG1 = g_INIESE
        .w_DATREG2 = g_FINESE
        .w_NUMDO1 = 0
        .w_ALFDO1 = ''
        .w_DATDO1 = cp_CharToDate('  -  -  ')
        .w_NUMRIF = 0
        .w_ALFRIF = ''
        .w_DATRIF = cp_CharToDate('  -  -  ')
          .DoRTCalc(12,16,.f.)
        .w_SERIALE = .w_zoom.getVar('SERIALE')
        .w_CLADOC = .w_zoom.getVar('MVCLADOC')
          .DoRTCalc(19,19,.f.)
        .w_FLVEAC = .w_zoom.getVar('MVFLVEAC')
        .w_OBTEST = IIF(EMPTY(.w_DATDOC),cp_CharToDate('  -  -  '),.w_DATDOC)
      .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
          .DoRTCalc(22,23,.f.)
        .w_NUMDOC = 0
        .w_ALFDOC = ''
        .w_DATDOC = cp_CharToDate(' - - ')
        .w_NUMRI1 = 0
        .w_ALFRI1 = ''
        .w_DATRI1 = cp_CharToDate('  -  -  ')
        .w_DOCESP = IIF(g_EACD='S','S',' ')
        .w_LEGIND = ' '
      .oPgFrm.Page1.oPag.oObj_1_58.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_59.Calculate(IIF(IsAlt(),Ah_MsgFormat("Tipo:"),Ah_MsgFormat("Causale:")))
      .oPgFrm.Page1.oPag.oObj_1_60.Calculate(IIF(IsAlt(), "Tipo del documento", "Causale del documento"))
        .w_SELPRV = IIF(ISALT(), 'V', 'K')
        .w_SELBOZ = IIF(ISALT(), 'B', 'K')
        .w_SELNSP = IIF(ISALT(), 'N', 'K')
        .w_SELALT = IIF(ISALT(), 'L', 'K')
        .w_SELPRA = IIF(ISALT(), 'P', 'K')
        .w_SELPRO = IIF(ISALT(), 'S', 'K')
        .w_SELFAA = IIF(ISALT(), 'A', 'K')
        .w_SELFAT = IIF(ISALT(), 'F', 'K')
        .w_SELNCR = IIF(ISALT(), 'C', 'K')
      .oPgFrm.Page1.oPag.Zoom.Calculate()
        .w_OFSERIALE = .w_zoom.getVar('OFSERIAL')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_72.enabled = this.oPgFrm.Page1.oPag.oBtn_1_72.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_TIPCAU<>.w_TIPCAU
            .w_CAUDOC = SPACE(5)
          .link_1_2('Full')
        endif
        if .o_CAUDOC<>.w_CAUDOC
            .w_CODCLIFOR = IIF(NVL(.w_TIPOCF,'N') $ 'CF',nvl(.w_CODCLIFOR,''),'')
          .link_1_3('Full')
        endif
        .DoRTCalc(4,5,.t.)
        if .o_CAUDOC<>.w_CAUDOC
            .w_NUMDO1 = 0
        endif
        if .o_CAUDOC<>.w_CAUDOC
            .w_ALFDO1 = ''
        endif
        .DoRTCalc(8,8,.t.)
        if .o_CAUDOC<>.w_CAUDOC
            .w_NUMRIF = 0
        endif
        if .o_CAUDOC<>.w_CAUDOC
            .w_ALFRIF = ''
        endif
        if .o_CAUDOC<>.w_CAUDOC
            .w_DATRIF = cp_CharToDate('  -  -  ')
        endif
        .DoRTCalc(12,16,.t.)
            .w_SERIALE = .w_zoom.getVar('SERIALE')
            .w_CLADOC = .w_zoom.getVar('MVCLADOC')
        .DoRTCalc(19,19,.t.)
            .w_FLVEAC = .w_zoom.getVar('MVFLVEAC')
        if .o_DATDOC<>.w_DATDOC
            .w_OBTEST = IIF(EMPTY(.w_DATDOC),cp_CharToDate('  -  -  '),.w_DATDOC)
        endif
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        .DoRTCalc(22,23,.t.)
        if .o_CAUDOC<>.w_CAUDOC
            .w_NUMDOC = 0
        endif
        if .o_CAUDOC<>.w_CAUDOC
            .w_ALFDOC = ''
        endif
        if .o_CAUDOC<>.w_CAUDOC
            .w_DATDOC = cp_CharToDate(' - - ')
        endif
        if .o_CAUDOC<>.w_CAUDOC
            .w_NUMRI1 = 0
        endif
        if .o_CAUDOC<>.w_CAUDOC
            .w_ALFRI1 = ''
        endif
        if .o_CAUDOC<>.w_CAUDOC
            .w_DATRI1 = cp_CharToDate('  -  -  ')
        endif
        .oPgFrm.Page1.oPag.oObj_1_58.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_59.Calculate(IIF(IsAlt(),Ah_MsgFormat("Tipo:"),Ah_MsgFormat("Causale:")))
        .oPgFrm.Page1.oPag.oObj_1_60.Calculate(IIF(IsAlt(), "Tipo del documento", "Causale del documento"))
        .oPgFrm.Page1.oPag.Zoom.Calculate()
        .DoRTCalc(30,40,.t.)
            .w_OFSERIALE = .w_zoom.getVar('OFSERIAL')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_58.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_59.Calculate(IIF(IsAlt(),Ah_MsgFormat("Tipo:"),Ah_MsgFormat("Causale:")))
        .oPgFrm.Page1.oPag.oObj_1_60.Calculate(IIF(IsAlt(), "Tipo del documento", "Causale del documento"))
        .oPgFrm.Page1.oPag.Zoom.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODCLIFOR_1_3.enabled = this.oPgFrm.Page1.oPag.oCODCLIFOR_1_3.mCond()
    this.oPgFrm.Page1.oPag.oDOCESP_1_56.enabled = this.oPgFrm.Page1.oPag.oDOCESP_1_56.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_72.enabled = this.oPgFrm.Page1.oPag.oBtn_1_72.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oTIPCAU_1_1.visible=!this.oPgFrm.Page1.oPag.oTIPCAU_1_1.mHide()
    this.oPgFrm.Page1.oPag.oNUMRIF_1_9.visible=!this.oPgFrm.Page1.oPag.oNUMRIF_1_9.mHide()
    this.oPgFrm.Page1.oPag.oALFRIF_1_10.visible=!this.oPgFrm.Page1.oPag.oALFRIF_1_10.mHide()
    this.oPgFrm.Page1.oPag.oDATRIF_1_11.visible=!this.oPgFrm.Page1.oPag.oDATRIF_1_11.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_14.visible=!this.oPgFrm.Page1.oPag.oBtn_1_14.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_18.visible=!this.oPgFrm.Page1.oPag.oStr_1_18.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_20.visible=!this.oPgFrm.Page1.oPag.oStr_1_20.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_22.visible=!this.oPgFrm.Page1.oPag.oStr_1_22.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_27.visible=!this.oPgFrm.Page1.oPag.oStr_1_27.mHide()
    this.oPgFrm.Page1.oPag.oNUMRI1_1_50.visible=!this.oPgFrm.Page1.oPag.oNUMRI1_1_50.mHide()
    this.oPgFrm.Page1.oPag.oALFRI1_1_51.visible=!this.oPgFrm.Page1.oPag.oALFRI1_1_51.mHide()
    this.oPgFrm.Page1.oPag.oDATRI1_1_52.visible=!this.oPgFrm.Page1.oPag.oDATRI1_1_52.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_53.visible=!this.oPgFrm.Page1.oPag.oStr_1_53.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_54.visible=!this.oPgFrm.Page1.oPag.oStr_1_54.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_55.visible=!this.oPgFrm.Page1.oPag.oStr_1_55.mHide()
    this.oPgFrm.Page1.oPag.oDOCESP_1_56.visible=!this.oPgFrm.Page1.oPag.oDOCESP_1_56.mHide()
    this.oPgFrm.Page1.oPag.oLEGIND_1_57.visible=!this.oPgFrm.Page1.oPag.oLEGIND_1_57.mHide()
    this.oPgFrm.Page1.oPag.oSELPRV_1_61.visible=!this.oPgFrm.Page1.oPag.oSELPRV_1_61.mHide()
    this.oPgFrm.Page1.oPag.oSELBOZ_1_62.visible=!this.oPgFrm.Page1.oPag.oSELBOZ_1_62.mHide()
    this.oPgFrm.Page1.oPag.oSELNSP_1_63.visible=!this.oPgFrm.Page1.oPag.oSELNSP_1_63.mHide()
    this.oPgFrm.Page1.oPag.oSELALT_1_64.visible=!this.oPgFrm.Page1.oPag.oSELALT_1_64.mHide()
    this.oPgFrm.Page1.oPag.oSELPRA_1_65.visible=!this.oPgFrm.Page1.oPag.oSELPRA_1_65.mHide()
    this.oPgFrm.Page1.oPag.oSELPRO_1_66.visible=!this.oPgFrm.Page1.oPag.oSELPRO_1_66.mHide()
    this.oPgFrm.Page1.oPag.oSELFAA_1_67.visible=!this.oPgFrm.Page1.oPag.oSELFAA_1_67.mHide()
    this.oPgFrm.Page1.oPag.oSELFAT_1_68.visible=!this.oPgFrm.Page1.oPag.oSELFAT_1_68.mHide()
    this.oPgFrm.Page1.oPag.oSELNCR_1_69.visible=!this.oPgFrm.Page1.oPag.oSELNCR_1_69.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_70.visible=!this.oPgFrm.Page1.oPag.oStr_1_70.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_72.visible=!this.oPgFrm.Page1.oPag.oBtn_1_72.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_74.visible=!this.oPgFrm.Page1.oPag.oStr_1_74.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_75.visible=!this.oPgFrm.Page1.oPag.oStr_1_75.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_43.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_58.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_59.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_60.Event(cEvent)
      .oPgFrm.Page1.oPag.Zoom.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CAUDOC
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_CAUDOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE,TDFLVEAC,TDFLVSRI,TDCATDOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_CAUDOC))
          select TDTIPDOC,TDDESDOC,TDFLINTE,TDFLVEAC,TDFLVSRI,TDCATDOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAUDOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CAUDOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oCAUDOC_1_2'),i_cWhere,'',""+iif(not isalt (), "Causali documenti", "Tipi documenti") +"",'GSOR_STD.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE,TDFLVEAC,TDFLVSRI,TDCATDOC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLINTE,TDFLVEAC,TDFLVSRI,TDCATDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE,TDFLVEAC,TDFLVSRI,TDCATDOC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_CAUDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_CAUDOC)
            select TDTIPDOC,TDDESDOC,TDFLINTE,TDFLVEAC,TDFLVSRI,TDCATDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESTIP = NVL(_Link_.TDDESDOC,space(35))
      this.w_TIPOCF = NVL(_Link_.TDFLINTE,space(1))
      this.w_TIPO = NVL(_Link_.TDFLVEAC,space(1))
      this.w_VSRIF = NVL(_Link_.TDFLVSRI,space(2))
      this.w_CLADOC = NVL(_Link_.TDCATDOC,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_CAUDOC = space(5)
      endif
      this.w_DESTIP = space(35)
      this.w_TIPOCF = space(1)
      this.w_TIPO = space(1)
      this.w_VSRIF = space(2)
      this.w_CLADOC = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPO=.w_TIPCAU
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CAUDOC = space(5)
        this.w_DESTIP = space(35)
        this.w_TIPOCF = space(1)
        this.w_TIPO = space(1)
        this.w_VSRIF = space(2)
        this.w_CLADOC = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCLIFOR
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCLIFOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCLIFOR)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPOCF);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPOCF;
                     ,'ANCODICE',trim(this.w_CODCLIFOR))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCLIFOR)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODCLIFOR)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPOCF);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODCLIFOR)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPOCF);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCLIFOR) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCLIFOR_1_3'),i_cWhere,'GSAR_BZC',"Anagrafica clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPOCF<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPOCF);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCLIFOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCLIFOR);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPOCF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPOCF;
                       ,'ANCODICE',this.w_CODCLIFOR)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCLIFOR = NVL(_Link_.ANCODICE,space(15))
      this.w_CODDES = NVL(_Link_.ANDESCRI,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODCLIFOR = space(15)
      endif
      this.w_CODDES = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso o obsoleto")
        endif
        this.w_CODCLIFOR = space(15)
        this.w_CODDES = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCLIFOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPCAU_1_1.RadioValue()==this.w_TIPCAU)
      this.oPgFrm.Page1.oPag.oTIPCAU_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUDOC_1_2.value==this.w_CAUDOC)
      this.oPgFrm.Page1.oPag.oCAUDOC_1_2.value=this.w_CAUDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCLIFOR_1_3.value==this.w_CODCLIFOR)
      this.oPgFrm.Page1.oPag.oCODCLIFOR_1_3.value=this.w_CODCLIFOR
    endif
    if not(this.oPgFrm.Page1.oPag.oDATREG1_1_4.value==this.w_DATREG1)
      this.oPgFrm.Page1.oPag.oDATREG1_1_4.value=this.w_DATREG1
    endif
    if not(this.oPgFrm.Page1.oPag.oDATREG2_1_5.value==this.w_DATREG2)
      this.oPgFrm.Page1.oPag.oDATREG2_1_5.value=this.w_DATREG2
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMDO1_1_6.value==this.w_NUMDO1)
      this.oPgFrm.Page1.oPag.oNUMDO1_1_6.value=this.w_NUMDO1
    endif
    if not(this.oPgFrm.Page1.oPag.oALFDO1_1_7.value==this.w_ALFDO1)
      this.oPgFrm.Page1.oPag.oALFDO1_1_7.value=this.w_ALFDO1
    endif
    if not(this.oPgFrm.Page1.oPag.oDATDO1_1_8.value==this.w_DATDO1)
      this.oPgFrm.Page1.oPag.oDATDO1_1_8.value=this.w_DATDO1
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMRIF_1_9.value==this.w_NUMRIF)
      this.oPgFrm.Page1.oPag.oNUMRIF_1_9.value=this.w_NUMRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oALFRIF_1_10.value==this.w_ALFRIF)
      this.oPgFrm.Page1.oPag.oALFRIF_1_10.value=this.w_ALFRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oDATRIF_1_11.value==this.w_DATRIF)
      this.oPgFrm.Page1.oPag.oDATRIF_1_11.value=this.w_DATRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESTIP_1_16.value==this.w_DESTIP)
      this.oPgFrm.Page1.oPag.oDESTIP_1_16.value=this.w_DESTIP
    endif
    if not(this.oPgFrm.Page1.oPag.oCODDES_1_25.value==this.w_CODDES)
      this.oPgFrm.Page1.oPag.oCODDES_1_25.value=this.w_CODDES
    endif
    if not(this.oPgFrm.Page1.oPag.oMVRIFCLI_1_42.value==this.w_MVRIFCLI)
      this.oPgFrm.Page1.oPag.oMVRIFCLI_1_42.value=this.w_MVRIFCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMDOC_1_44.value==this.w_NUMDOC)
      this.oPgFrm.Page1.oPag.oNUMDOC_1_44.value=this.w_NUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oALFDOC_1_45.value==this.w_ALFDOC)
      this.oPgFrm.Page1.oPag.oALFDOC_1_45.value=this.w_ALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDATDOC_1_46.value==this.w_DATDOC)
      this.oPgFrm.Page1.oPag.oDATDOC_1_46.value=this.w_DATDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMRI1_1_50.value==this.w_NUMRI1)
      this.oPgFrm.Page1.oPag.oNUMRI1_1_50.value=this.w_NUMRI1
    endif
    if not(this.oPgFrm.Page1.oPag.oALFRI1_1_51.value==this.w_ALFRI1)
      this.oPgFrm.Page1.oPag.oALFRI1_1_51.value=this.w_ALFRI1
    endif
    if not(this.oPgFrm.Page1.oPag.oDATRI1_1_52.value==this.w_DATRI1)
      this.oPgFrm.Page1.oPag.oDATRI1_1_52.value=this.w_DATRI1
    endif
    if not(this.oPgFrm.Page1.oPag.oDOCESP_1_56.RadioValue()==this.w_DOCESP)
      this.oPgFrm.Page1.oPag.oDOCESP_1_56.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLEGIND_1_57.RadioValue()==this.w_LEGIND)
      this.oPgFrm.Page1.oPag.oLEGIND_1_57.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELPRV_1_61.RadioValue()==this.w_SELPRV)
      this.oPgFrm.Page1.oPag.oSELPRV_1_61.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELBOZ_1_62.RadioValue()==this.w_SELBOZ)
      this.oPgFrm.Page1.oPag.oSELBOZ_1_62.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELNSP_1_63.RadioValue()==this.w_SELNSP)
      this.oPgFrm.Page1.oPag.oSELNSP_1_63.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELALT_1_64.RadioValue()==this.w_SELALT)
      this.oPgFrm.Page1.oPag.oSELALT_1_64.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELPRA_1_65.RadioValue()==this.w_SELPRA)
      this.oPgFrm.Page1.oPag.oSELPRA_1_65.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELPRO_1_66.RadioValue()==this.w_SELPRO)
      this.oPgFrm.Page1.oPag.oSELPRO_1_66.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELFAA_1_67.RadioValue()==this.w_SELFAA)
      this.oPgFrm.Page1.oPag.oSELFAA_1_67.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELFAT_1_68.RadioValue()==this.w_SELFAT)
      this.oPgFrm.Page1.oPag.oSELFAT_1_68.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELNCR_1_69.RadioValue()==this.w_SELNCR)
      this.oPgFrm.Page1.oPag.oSELNCR_1_69.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_TIPO=.w_TIPCAU)  and not(empty(.w_CAUDOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAUDOC_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and (NVL(.w_TIPOCF,'N') $ 'CF')  and not(empty(.w_CODCLIFOR))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCLIFOR_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore non ammesso o obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPCAU = this.w_TIPCAU
    this.o_CAUDOC = this.w_CAUDOC
    this.o_DATDOC = this.w_DATDOC
    return

enddefine

* --- Define pages as container
define class tgsor_stdPag1 as StdContainer
  Width  = 771
  height = 449
  stdWidth  = 771
  stdheight = 449
  resizeXpos=478
  resizeYpos=276
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPCAU_1_1 as StdCombo with uid="ZBDFIEFSOO",rtseq=1,rtrep=.f.,left=70,top=32,width=86,height=21;
    , ToolTipText = "Documenti del ciclo passivo o attivo";
    , HelpContextID = 82765110;
    , cFormVar="w_TIPCAU",RowSource=""+"Vendite,"+"Acquisti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPCAU_1_1.RadioValue()
    return(iif(this.value =1,'V',;
    iif(this.value =2,'A',;
    space(1))))
  endfunc
  func oTIPCAU_1_1.GetRadio()
    this.Parent.oContained.w_TIPCAU = this.RadioValue()
    return .t.
  endfunc

  func oTIPCAU_1_1.SetRadio()
    this.Parent.oContained.w_TIPCAU=trim(this.Parent.oContained.w_TIPCAU)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCAU=='V',1,;
      iif(this.Parent.oContained.w_TIPCAU=='A',2,;
      0))
  endfunc

  func oTIPCAU_1_1.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oCAUDOC_1_2 as StdField with uid="HMSULLRIJD",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CAUDOC", cQueryName = "CAUDOC",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale del documento",;
    HelpContextID = 63974438,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=237, Top=32, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_CAUDOC"

  func oCAUDOC_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAUDOC_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCAUDOC_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oCAUDOC_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',""+iif(not isalt (), "Causali documenti", "Tipi documenti") +"",'GSOR_STD.TIP_DOCU_VZM',this.parent.oContained
  endproc

  add object oCODCLIFOR_1_3 as StdField with uid="DKKVSZWZQO",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODCLIFOR", cQueryName = "CODCLIFOR",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso o obsoleto",;
    ToolTipText = "Cliente/fornitore intestatario del documento",;
    HelpContextID = 161361813,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=70, Top=59, cSayPict="p_CLF", cGetPict="p_CLF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPOCF", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCLIFOR"

  func oCODCLIFOR_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NVL(.w_TIPOCF,'N') $ 'CF')
    endwith
   endif
  endfunc

  func oCODCLIFOR_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCLIFOR_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCLIFOR_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPOCF)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPOCF)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCLIFOR_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Anagrafica clienti/fornitori",'',this.parent.oContained
  endproc
  proc oCODCLIFOR_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPOCF
     i_obj.w_ANCODICE=this.parent.oContained.w_CODCLIFOR
     i_obj.ecpSave()
  endproc

  add object oDATREG1_1_4 as StdField with uid="PBHWCXLIPQ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DATREG1", cQueryName = "DATREG1",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento di inizio selezione",;
    HelpContextID = 121510966,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=70, Top=88

  add object oDATREG2_1_5 as StdField with uid="KEDKYLRBEH",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DATREG2", cQueryName = "DATREG2",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento di fine selezione",;
    HelpContextID = 121510966,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=199, Top=88

  add object oNUMDO1_1_6 as StdField with uid="EDHNNJPCXK",rtseq=6,rtrep=.f.,;
    cFormVar = "w_NUMDO1", cQueryName = "NUMDO1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Nostro documento da ricercare",;
    HelpContextID = 238042922,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=544, Top=32

  add object oALFDO1_1_7 as StdField with uid="JXVZFOHZEM",rtseq=7,rtrep=.f.,;
    cFormVar = "w_ALFDO1", cQueryName = "ALFDO1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie del documento",;
    HelpContextID = 238074106,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=678, Top=32, InputMask=replicate('X',10)

  add object oDATDO1_1_8 as StdField with uid="HGNHBNUHDL",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DATDO1", cQueryName = "DATDO1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data del documento",;
    HelpContextID = 238019530,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=544, Top=56

  add object oNUMRIF_1_9 as StdField with uid="YSCJGTDVFB",rtseq=9,rtrep=.f.,;
    cFormVar = "w_NUMRIF", cQueryName = "NUMRIF",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Vostro numero documento da ricercare",;
    HelpContextID = 108904662,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=70, Top=146, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oNUMRIF_1_9.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oALFRIF_1_10 as StdField with uid="IIJJIJEFKW",rtseq=10,rtrep=.f.,;
    cFormVar = "w_ALFRIF", cQueryName = "ALFRIF",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie del documento",;
    HelpContextID = 108873478,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=199, Top=146, cSayPict="'!!!!!!!!!!'", cGetPict="'!!!!!!!!!!'", InputMask=replicate('X',10)

  func oALFRIF_1_10.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oDATRIF_1_11 as StdField with uid="UBXIXZYJJD",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DATRIF", cQueryName = "DATRIF",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data del documento",;
    HelpContextID = 108928054,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=352, Top=146

  func oDATRIF_1_11.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc


  add object oBtn_1_12 as StdButton with uid="NVTOOYLDIC",left=713, top=110, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Zoom di ricerca documento di partenza";
    , HelpContextID = 102905110;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      do GSOR_ST1 with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_13 as StdButton with uid="ZXFZXTXHHV",left=13, top=400, width=48,height=45,;
    CpPicture="BMP\Verifica.BMP", caption="", nPag=1;
    , ToolTipText = "Visualizza il documento selezionato";
    , HelpContextID = 132268442;
    , caption='\<Docum.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      with this.Parent.oContained
        GSOR_BTD(this.Parent.oContained,"D")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_13.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERIALE))
      endwith
    endif
  endfunc


  add object oBtn_1_14 as StdButton with uid="JHGZZOAZJP",left=662, top=400, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Ricerca documenti della tracciabilità";
    , HelpContextID = 102905110;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      this.parent.oContained.NotifyEvent("EseguoZoom")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_14.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT ISALT())
     endwith
    endif
  endfunc


  add object oBtn_1_15 as StdButton with uid="MKAKRKHWWW",left=713, top=400, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 66699706;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESTIP_1_16 as StdField with uid="HPOPOHEGCT",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESTIP", cQueryName = "DESTIP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 8392758,;
   bGlobalFont=.t.,;
    Height=21, Width=179, Left=300, Top=32, InputMask=replicate('X',35)

  add object oCODDES_1_25 as StdField with uid="SDVTRWQICB",rtseq=14,rtrep=.f.,;
    cFormVar = "w_CODDES", cQueryName = "CODDES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 53422630,;
   bGlobalFont=.t.,;
    Height=21, Width=242, Left=237, Top=59, InputMask=replicate('X',35)

  add object oMVRIFCLI_1_42 as StdField with uid="VWBZQAQARC",rtseq=23,rtrep=.f.,;
    cFormVar = "w_MVRIFCLI", cQueryName = "MVRIFCLI",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Riferimento codice descrittivo",;
    HelpContextID = 213577201,;
   bGlobalFont=.t.,;
    Height=21, Width=127, Left=352, Top=87, InputMask=replicate('X',30)


  add object oObj_1_43 as cp_runprogram with uid="LTPNSLLOIR",left=-1, top=500, width=179,height=21,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSOR_BTD('R')",;
    cEvent = "EseguoZoom",;
    nPag=1;
    , HelpContextID = 103980518

  add object oNUMDOC_1_44 as StdField with uid="QNDZKHFOJQ",rtseq=24,rtrep=.f.,;
    cFormVar = "w_NUMDOC", cQueryName = "NUMDOC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 63946966,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=70, Top=117, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  add object oALFDOC_1_45 as StdField with uid="CSJIBANVSV",rtseq=25,rtrep=.f.,;
    cFormVar = "w_ALFDOC", cQueryName = "ALFDOC",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 63915782,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=199, Top=118, cSayPict="'!!!!!!!!!!'", cGetPict="'!!!!!!!!!!'", InputMask=replicate('X',10)

  add object oDATDOC_1_46 as StdField with uid="LWENNGBWXF",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DATDOC", cQueryName = "DATDOC",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 63970358,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=352, Top=118

  add object oNUMRI1_1_50 as StdField with uid="SXOIIFRUNM",rtseq=27,rtrep=.f.,;
    cFormVar = "w_NUMRI1", cQueryName = "NUMRI1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Vostro numero documento da ricercare",;
    HelpContextID = 243416874,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=544, Top=85

  func oNUMRI1_1_50.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oALFRI1_1_51 as StdField with uid="MGFCPBDBYE",rtseq=28,rtrep=.f.,;
    cFormVar = "w_ALFRI1", cQueryName = "ALFRI1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie del documento",;
    HelpContextID = 243448058,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=678, Top=85, InputMask=replicate('X',10)

  func oALFRI1_1_51.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oDATRI1_1_52 as StdField with uid="QKZIIAOWSU",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DATRI1", cQueryName = "DATRI1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data del documento",;
    HelpContextID = 243393482,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=544, Top=110

  func oDATRI1_1_52.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oDOCESP_1_56 as StdCheck with uid="IXJHPACKLM",rtseq=30,rtrep=.f.,left=544, top=136, caption="Doc. esplosione",;
    ToolTipText = "Attivo: vengono ricercati anche i documenti di esplosione componenti e prodotti finiti",;
    HelpContextID = 17832502,;
    cFormVar="w_DOCESP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDOCESP_1_56.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oDOCESP_1_56.GetRadio()
    this.Parent.oContained.w_DOCESP = this.RadioValue()
    return .t.
  endfunc

  func oDOCESP_1_56.SetRadio()
    this.Parent.oContained.w_DOCESP=trim(this.Parent.oContained.w_DOCESP)
    this.value = ;
      iif(this.Parent.oContained.w_DOCESP=='S',1,;
      0)
  endfunc

  func oDOCESP_1_56.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_EACD='S')
    endwith
   endif
  endfunc

  func oDOCESP_1_56.mHide()
    with this.Parent.oContained
      return (g_EACD<>'S')
    endwith
  endfunc

  add object oLEGIND_1_57 as StdCheck with uid="BPZYKZQJIA",rtseq=31,rtrep=.f.,left=544, top=156, caption="Legame indiretto",;
    ToolTipText = "Attivo: vengono ricercati anche i documenti che indirettamente sono legati al documento selezionato",;
    HelpContextID = 79974582,;
    cFormVar="w_LEGIND", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oLEGIND_1_57.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oLEGIND_1_57.GetRadio()
    this.Parent.oContained.w_LEGIND = this.RadioValue()
    return .t.
  endfunc

  func oLEGIND_1_57.SetRadio()
    this.Parent.oContained.w_LEGIND=trim(this.Parent.oContained.w_LEGIND)
    this.value = ;
      iif(this.Parent.oContained.w_LEGIND=='S',1,;
      0)
  endfunc

  func oLEGIND_1_57.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc


  add object oObj_1_58 as cp_runprogram with uid="DADQJLRJGH",left=0, top=522, width=376,height=23,;
    caption='GSOR_BTD',;
   bGlobalFont=.t.,;
    prg="GSOR_BTD('R')",;
    cEvent = "w_LEGIND Changed,w_DOCESP Changed",;
    nPag=1;
    , HelpContextID = 64872106


  add object oObj_1_59 as cp_calclbl with uid="KZLEWMBSIL",left=163, top=34, width=73,height=20,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",fontname="Arial",fontsize=9,fontBold=.f.,fontItalic=.f.,fontUnderline=.f.,Alignment=1,;
    nPag=1;
    , HelpContextID = 103980518


  add object oObj_1_60 as cp_setobjprop with uid="TTSNMYCGVT",left=446, top=500, width=101,height=21,;
    caption='ToolTip',;
   bGlobalFont=.t.,;
    cObj="w_CAUDOC",cProp="ToolTipText",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 172620598

  add object oSELPRV_1_61 as StdCheck with uid="MSNQMAHPAJ",rtseq=32,rtrep=.f.,left=262, top=404, caption="Preventivo",;
    ToolTipText = "Se attivo, considera i documenti con sottocategoria preventivo",;
    HelpContextID = 118202662,;
    cFormVar="w_SELPRV", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELPRV_1_61.RadioValue()
    return(iif(this.value =1,'V',;
    'K'))
  endfunc
  func oSELPRV_1_61.GetRadio()
    this.Parent.oContained.w_SELPRV = this.RadioValue()
    return .t.
  endfunc

  func oSELPRV_1_61.SetRadio()
    this.Parent.oContained.w_SELPRV=trim(this.Parent.oContained.w_SELPRV)
    this.value = ;
      iif(this.Parent.oContained.w_SELPRV=='V',1,;
      0)
  endfunc

  func oSELPRV_1_61.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oSELBOZ_1_62 as StdCheck with uid="PIFAQEEOQL",rtseq=33,rtrep=.f.,left=345, top=404, caption="Bozza interna",;
    ToolTipText = "Se attivo, considera i documenti con sottocategoria bozza interna",;
    HelpContextID = 181248294,;
    cFormVar="w_SELBOZ", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELBOZ_1_62.RadioValue()
    return(iif(this.value =1,'B',;
    'K'))
  endfunc
  func oSELBOZ_1_62.GetRadio()
    this.Parent.oContained.w_SELBOZ = this.RadioValue()
    return .t.
  endfunc

  func oSELBOZ_1_62.SetRadio()
    this.Parent.oContained.w_SELBOZ=trim(this.Parent.oContained.w_SELBOZ)
    this.value = ;
      iif(this.Parent.oContained.w_SELBOZ=='B',1,;
      0)
  endfunc

  func oSELBOZ_1_62.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oSELNSP_1_63 as StdCheck with uid="STXQAHTBZU",rtseq=34,rtrep=.f.,left=463, top=404, caption="Nota spese",;
    ToolTipText = "Se attivo, considera i documenti con sottocategoria nota spese",;
    HelpContextID = 18456870,;
    cFormVar="w_SELNSP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELNSP_1_63.RadioValue()
    return(iif(this.value =1,'N',;
    'K'))
  endfunc
  func oSELNSP_1_63.GetRadio()
    this.Parent.oContained.w_SELNSP = this.RadioValue()
    return .t.
  endfunc

  func oSELNSP_1_63.SetRadio()
    this.Parent.oContained.w_SELNSP=trim(this.Parent.oContained.w_SELNSP)
    this.value = ;
      iif(this.Parent.oContained.w_SELNSP=='N',1,;
      0)
  endfunc

  func oSELNSP_1_63.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oSELALT_1_64 as StdCheck with uid="VTHHZNVSDS",rtseq=35,rtrep=.f.,left=549, top=404, caption="Altro documento",;
    ToolTipText = "Se attivo, considera i documenti con sottocategoria altro documento",;
    HelpContextID = 77373734,;
    cFormVar="w_SELALT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELALT_1_64.RadioValue()
    return(iif(this.value =1,'L',;
    'K'))
  endfunc
  func oSELALT_1_64.GetRadio()
    this.Parent.oContained.w_SELALT = this.RadioValue()
    return .t.
  endfunc

  func oSELALT_1_64.SetRadio()
    this.Parent.oContained.w_SELALT=trim(this.Parent.oContained.w_SELALT)
    this.value = ;
      iif(this.Parent.oContained.w_SELALT=='L',1,;
      0)
  endfunc

  func oSELALT_1_64.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oSELPRA_1_65 as StdCheck with uid="VNCSYMEHXA",rtseq=36,rtrep=.f.,left=131, top=429, caption="Proforma d'acconto",;
    ToolTipText = "Se attivo, considera i documenti con sottocategoria proforma d'acconto",;
    HelpContextID = 34316582,;
    cFormVar="w_SELPRA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELPRA_1_65.RadioValue()
    return(iif(this.value =1,'P',;
    'K'))
  endfunc
  func oSELPRA_1_65.GetRadio()
    this.Parent.oContained.w_SELPRA = this.RadioValue()
    return .t.
  endfunc

  func oSELPRA_1_65.SetRadio()
    this.Parent.oContained.w_SELPRA=trim(this.Parent.oContained.w_SELPRA)
    this.value = ;
      iif(this.Parent.oContained.w_SELPRA=='P',1,;
      0)
  endfunc

  func oSELPRA_1_65.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oSELPRO_1_66 as StdCheck with uid="OGRRCJXDLD",rtseq=37,rtrep=.f.,left=262, top=429, caption="Proforma",;
    ToolTipText = "Se attivo, considera i documenti con sottocategoria proforma",;
    HelpContextID = 762150,;
    cFormVar="w_SELPRO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELPRO_1_66.RadioValue()
    return(iif(this.value =1,'S',;
    'K'))
  endfunc
  func oSELPRO_1_66.GetRadio()
    this.Parent.oContained.w_SELPRO = this.RadioValue()
    return .t.
  endfunc

  func oSELPRO_1_66.SetRadio()
    this.Parent.oContained.w_SELPRO=trim(this.Parent.oContained.w_SELPRO)
    this.value = ;
      iif(this.Parent.oContained.w_SELPRO=='S',1,;
      0)
  endfunc

  func oSELPRO_1_66.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oSELFAA_1_67 as StdCheck with uid="KSHGLGYFMU",rtseq=38,rtrep=.f.,left=345, top=429, caption="Fattura d'acconto",;
    ToolTipText = "Se attivo, considera i documenti con sottocategoria fattura d'acconto",;
    HelpContextID = 15835430,;
    cFormVar="w_SELFAA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELFAA_1_67.RadioValue()
    return(iif(this.value =1,'A',;
    'K'))
  endfunc
  func oSELFAA_1_67.GetRadio()
    this.Parent.oContained.w_SELFAA = this.RadioValue()
    return .t.
  endfunc

  func oSELFAA_1_67.SetRadio()
    this.Parent.oContained.w_SELFAA=trim(this.Parent.oContained.w_SELFAA)
    this.value = ;
      iif(this.Parent.oContained.w_SELFAA=='A',1,;
      0)
  endfunc

  func oSELFAA_1_67.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oSELFAT_1_68 as StdCheck with uid="EFPRSXAPUH",rtseq=39,rtrep=.f.,left=463, top=429, caption="Fattura",;
    ToolTipText = "Se attivo, considera i documenti con sottocategoria fattura",;
    HelpContextID = 66167078,;
    cFormVar="w_SELFAT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELFAT_1_68.RadioValue()
    return(iif(this.value =1,'F',;
    'K'))
  endfunc
  func oSELFAT_1_68.GetRadio()
    this.Parent.oContained.w_SELFAT = this.RadioValue()
    return .t.
  endfunc

  func oSELFAT_1_68.SetRadio()
    this.Parent.oContained.w_SELFAT=trim(this.Parent.oContained.w_SELFAT)
    this.value = ;
      iif(this.Parent.oContained.w_SELFAT=='F',1,;
      0)
  endfunc

  func oSELFAT_1_68.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oSELNCR_1_69 as StdCheck with uid="RYBJQJMRKW",rtseq=40,rtrep=.f.,left=549, top=429, caption="Nota di credito",;
    ToolTipText = "Se attivo, considera i documenti con sottocategoria nota di credito",;
    HelpContextID = 35234086,;
    cFormVar="w_SELNCR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELNCR_1_69.RadioValue()
    return(iif(this.value =1,'C',;
    'K'))
  endfunc
  func oSELNCR_1_69.GetRadio()
    this.Parent.oContained.w_SELNCR = this.RadioValue()
    return .t.
  endfunc

  func oSELNCR_1_69.SetRadio()
    this.Parent.oContained.w_SELNCR=trim(this.Parent.oContained.w_SELNCR)
    this.value = ;
      iif(this.Parent.oContained.w_SELNCR=='C',1,;
      0)
  endfunc

  func oSELNCR_1_69.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc


  add object Zoom as cp_zoombox with uid="BKJEZMDCSW",left=13, top=182, width=748,height=216,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="DOC_MAST",cZoomFile="GSOR_STD",bOptions=.f.,bAdvOptions=.f.,bQueryOnLoad=.f.,bReadOnly=.f.,bQueryOnDblClick=.f.,cMenuFile="",bRetriveAllRows=.f.,cZoomOnZoom="GSZM_BZM",;
    nPag=1;
    , ToolTipText = "Lo zoom è costruito nel batch, la query ad esso associata da solo la struttura";
    , HelpContextID = 103980518


  add object oBtn_1_72 as StdButton with uid="QJFYSEDROP",left=65, top=400, width=48,height=45,;
    CpPicture="BMP\newoffe.BMP", caption="", nPag=1;
    , ToolTipText = "Visualizza l'offerta selezionata";
    , HelpContextID = 119693798;
    , caption='\<Offerta';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_72.Click()
      with this.Parent.oContained
        GSOR_BTD(this.Parent.oContained,"O")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_72.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_OFSERIALE))
      endwith
    endif
  endfunc

  func oBtn_1_72.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (ISALT())
     endwith
    endif
  endfunc

  add object oStr_1_17 as StdString with uid="OITGWXOZKU",Visible=.t., Left=493, Top=32,;
    Alignment=1, Width=50, Height=18,;
    Caption="Doc. n.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="RPZBKHEUHJ",Visible=.t., Left=18, Top=146,;
    Alignment=1, Width=51, Height=15,;
    Caption="Rifer. n.:"  ;
  , bGlobalFont=.t.

  func oStr_1_18.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_19 as StdString with uid="OOTSBCFEDK",Visible=.t., Left=663, Top=32,;
    Alignment=2, Width=7, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="TLWLJJTEFF",Visible=.t., Left=191, Top=146,;
    Alignment=0, Width=6, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_20.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_21 as StdString with uid="DZNIVNJNTP",Visible=.t., Left=513, Top=56,;
    Alignment=1, Width=30, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="NQDSZLWSDA",Visible=.t., Left=321, Top=146,;
    Alignment=1, Width=30, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  func oStr_1_22.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_23 as StdString with uid="GABZWQEWEK",Visible=.t., Left=3, Top=59,;
    Alignment=1, Width=66, Height=15,;
    Caption="Cli./ for.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="SZLQECGKJH",Visible=.t., Left=34, Top=32,;
    Alignment=1, Width=35, Height=15,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  func oStr_1_27.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_32 as StdString with uid="BKAOPWPHZR",Visible=.t., Left=484, Top=4,;
    Alignment=0, Width=280, Height=15,;
    Caption="Estremi documento di partenza"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="UKLYXPUCZR",Visible=.t., Left=23, Top=4,;
    Alignment=0, Width=200, Height=15,;
    Caption="Parametri di selezione documento"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="QWUMHJSZYN",Visible=.t., Left=8, Top=88,;
    Alignment=1, Width=61, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="JPQHMVXYAZ",Visible=.t., Left=152, Top=88,;
    Alignment=1, Width=46, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="ZOTHAXBXGW",Visible=.t., Left=286, Top=87,;
    Alignment=1, Width=65, Height=15,;
    Caption="Rifer. des.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="MFIVZEXHNN",Visible=.t., Left=19, Top=117,;
    Alignment=1, Width=50, Height=18,;
    Caption="Doc. n.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="RQJKPHERSJ",Visible=.t., Left=191, Top=118,;
    Alignment=0, Width=6, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="XAFQHQKCFX",Visible=.t., Left=321, Top=118,;
    Alignment=1, Width=30, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="JIESITWVCL",Visible=.t., Left=492, Top=85,;
    Alignment=1, Width=51, Height=15,;
    Caption="Rifer. n.:"  ;
  , bGlobalFont=.t.

  func oStr_1_53.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_54 as StdString with uid="CGHGLIPRYL",Visible=.t., Left=664, Top=85,;
    Alignment=0, Width=6, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_54.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_55 as StdString with uid="OYGRCWTOGG",Visible=.t., Left=513, Top=110,;
    Alignment=1, Width=30, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  func oStr_1_55.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_70 as StdString with uid="MSZDBRNNOI",Visible=.t., Left=63, Top=404,;
    Alignment=1, Width=188, Height=18,;
    Caption="Selezione documenti tracciabilità:"  ;
  , bGlobalFont=.t.

  func oStr_1_70.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oStr_1_74 as StdString with uid="WNLFGNCNTL",Visible=.t., Left=18, Top=167,;
    Alignment=0, Width=215, Height=18,;
    Caption="Documento generato da modulo offerte"    , BackStyle=1, BackColor=RGB(255,255,0);
  ;
  , bGlobalFont=.t.

  func oStr_1_74.mHide()
    with this.Parent.oContained
      return (g_offe='N' or Isalt())
    endwith
  endfunc

  add object oStr_1_75 as StdString with uid="RYMFVFVVYG",Visible=.t., Left=18, Top=167,;
    Alignment=0, Width=215, Height=18,;
    Caption="Documento generato da preventivi"    , BackStyle=1, BackColor=RGB(255,255,0);
  ;
  , bGlobalFont=.t.

  func oStr_1_75.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc

  add object oBox_1_31 as StdBox with uid="PXBWCLDVXD",left=484, top=23, width=283,height=1

  add object oBox_1_33 as StdBox with uid="XKFADLCDCH",left=23, top=23, width=456,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsor_std','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
