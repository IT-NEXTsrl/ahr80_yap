* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gszm_bco                                                        *
*              Menu contestuale conti                                          *
*                                                                              *
*      Author: Zucchetti S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_56]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-06-10                                                      *
* Last revis.: 2012-05-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pFUNZ
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgszm_bco",oParentObject,m.pFUNZ)
return(i_retval)

define class tgszm_bco as StdBatch
  * --- Local variables
  pFUNZ = space(1)
  w_OBJECT = .NULL.
  w_BOTTONE = .NULL.
  w_ANTIPCON = space(1)
  w_ANCODICE = space(15)
  w_PNOTA = .f.
  w_PCODESE = space(4)
  w_PDATFIN = ctod("  /  /  ")
  w_PTIPCON = space(1)
  w_PCODCON = space(15)
  w_OBJECTFLD = .NULL.
  w_ANCATCOM = space(3)
  * --- WorkFile variables
  CONTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Routine per avvio funzionalit� legate ai CONTI, da men� contestuale 
    * --- Assegno i valori alle variabili
    this.w_ANTIPCON = g_oMenu.getbyindexkeyvalue(1)
    this.w_ANCODICE = g_oMenu.getbyindexkeyvalue(2)
    do case
      case this.pFUNZ="1"
        * --- Visualizza schede contabili
        this.w_OBJECT = GSCG_SZM(this)
        if !this.w_OBJECT.bSec1
          i_retcode = 'stop'
          return
        endif
        this.w_OBJECT.w_TIPCON = this.w_ANTIPCON
        this.w_OBJECT.w_CODICE = this.w_ANCODICE
        * --- Visualizzo la chiave caricata
        this.w_OBJECT.SetControlsValue()     
        this.w_OBJECT.mCalc(.T.)     
        * --- Avvio la ricerca
        this.w_BOTTONE = this.w_OBJECT.GETCTRL(ah_Msgformat("\<Ricerca"))
        this.w_BOTTONE.Click()     
      case this.pFUNZ="2"
        * --- Manutenzione Partite/Scadenze
        this.w_OBJECT = GSTE_KMS()
        if !this.w_OBJECT.bSec1
          i_retcode = 'stop'
          return
        endif
        this.w_OBJECT.w_TIPCON = this.w_ANTIPCON
        this.w_OBJECT.SetControlsValue()     
        this.w_OBJECTFLD = this.w_OBJECT.getCTRL("w_CODCON")
        this.w_OBJECTFLD.value = this.w_ANCODICE
        this.w_OBJECTFLD.valid()     
        this.w_OBJECT.w_CODCON = this.w_ANCODICE
        * --- Visualizzo la chiave caricata
        * --- Avvio la ricerca
        this.w_BOTTONE = this.w_OBJECT.GETCTRL(ah_Msgformat("\<Ricerca"))
        this.w_BOTTONE.Click()     
      case this.pFUNZ="3"
        * --- Saldi Contabili
        do case
          case this.w_ANTIPCON="C"
            this.w_OBJECT = GSCG_ASC()
          case this.w_ANTIPCON="F"
            this.w_OBJECT = GSCG_ASF()
          case this.w_ANTIPCON="G"
            this.w_OBJECT = GSCG_ASP()
        endcase
        if !(this.w_OBJECT.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_OBJECT.EcpFilter()     
        * --- Valorizzo i campi chiave
        this.w_OBJECT.w_SLTIPCON = this.w_ANTIPCON
        this.w_OBJECT.w_SLCODICE = this.w_ANCODICE
        * --- Carico il record richiesto
        this.w_OBJECT.EcpSave()     
      case this.pFUNZ="4"
        * --- Contratti Cliente/Fornitore
        * --- Leggo la categoria commerciale
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANCATCOM"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.w_ANTIPCON);
                +" and ANCODICE = "+cp_ToStrODBC(this.w_ANCODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANCATCOM;
            from (i_cTable) where;
                ANTIPCON = this.w_ANTIPCON;
                and ANCODICE = this.w_ANCODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_ANCATCOM = NVL(cp_ToDate(_read_.ANCATCOM),cp_NullValue(_read_.ANCATCOM))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Eseguo la routine di apertura contratti associati al cliente o fornitore selezionato
        do GSAR_BCL  with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pFUNZ="5"
        * --- Acquisti/Vendite
        GSAR_BKK(this,"B")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pFUNZ="6"
        * --- Ordini Cliente/Fornitore
        GSAR_BKK(this,"C")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pFUNZ="7"
        * --- Prospetto Venduto/Acquistato
        GSAR_BKK(this,"D")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pFUNZ="8"
        * --- Ultime Vendite/Acquisti
        do case
          case this.w_ANTIPCON="C"
            do GSAR_KUV with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          case this.w_ANTIPCON="F"
            do GSAR_KUA with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
        endcase
      case this.pFUNZ="9"
        do GSCG_KL2 with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
    this.w_BOTTONE = .NULL.
    this.w_OBJECT = .NULL.
  endproc


  proc Init(oParentObject,pFUNZ)
    this.pFUNZ=pFUNZ
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CONTI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pFUNZ"
endproc
