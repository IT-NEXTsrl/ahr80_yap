* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_btk                                                        *
*              Controlli variazione tipo conto                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_80]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-02-07                                                      *
* Last revis.: 2013-11-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEXEC
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_btk",oParentObject,m.pEXEC)
return(i_retval)

define class tgscg_btk as StdBatch
  * --- Local variables
  pEXEC = space(1)
  w_CODCON = space(15)
  w_CFUNC = space(10)
  w_OK = .f.
  w_PROG = .NULL.
  * --- WorkFile variables
  CONTI_idx=0
  PAR_TITE_idx=0
  BAN_CONTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato da :
    *     T: Batch Lanciato Dalla Gestione Primanota GSCG_MPN al cambiare del codice conto e all'evento CAMBIATIPO
    *     D: w_PNDATREG Changed
    *     C: w_PNCODCON Changed
    *     R: GSCG_KPN  w_PNCAURIG Changed
    this.w_CFUNC = This.oParentObject.cFunction
    do case
      case this.pEXEC="T"
        this.w_OK = .T.
        if NOT EMPTY(this.oParentObject.w_PNCODCON)
          * --- Controllo validit� condizione di editabilit� ddel codice conto
          if NOT(this.w_CFUNC<>"Edit" OR I_SRV="A" OR (this.oParentObject.w_VARPAR="S" AND this.oParentObject.w_VARCEN="S"))
            ah_ErrorMsg("Impossibile modificare tipo conto gestito a partite",,"")
            this.oParentObject.w_PNTIPCON = this.oParentObject.o_PNTIPCON
          endif
        endif
        * --- Setta per non eseguire ricalcoli
        this.bUpdateParentObject=.f.
      case this.pEXEC="C" OR this.pEXEC="R"
        if NOT EMPTY(this.oParentObject.w_PNCODCON) AND this.oParentObject.w_PNTIPCON $ "CFG" AND (this.w_CFUNC="Load" or this.pEXEC="R")
          * --- Utilizzando la changeRow() mi posiziono nella riga di Primanota  specifica per visualizzare il relativo  temporaneo delle partite
          if this.pEXEC="C" and this.w_CFUNC="Load"
            * --- Riassegno pagamento nel caso di caricamento in successione di registrazioni con
            *     partite non riesegue la Initson all'apertura del dettaglio partite e la variabile
            *     sfrutto Initson eseguita con la chengerow passandogli PNCODPAG gi� correttamente assegnato
            this.oParentObject.w_PNFLPART = IIF(this.oParentObject.w_PARTSN="S", this.oParentObject.w_RIGPAR, "N")
            this.oParentObject.w_PNCODPAG = IIF(this.oParentObject.w_DATMOR1>=IIF(EMPTY(this.oParentObject.w_PNDATDOC), this.oParentObject.w_PNDATREG, this.oParentObject.w_PNDATDOC) AND NOT EMPTY(this.oParentObject.w_PAGMOR1), this.oParentObject.w_PAGMOR1, IIF((EMPTY(this.oParentObject.w_PAGCLF1) AND this.oParentObject.w_FLPART="A"),g_SAPAGA,this.oParentObject.w_PAGCLF1))
          endif
          this.w_PROG = IIF(this.pEXEC="C",this.oParentObject,this.oParentObject.oParentObject)
          LOCAL i_nRec
          SELECT (this.w_PROG.cTrsName)
          if this.w_CFUNC="Edit" AND I_SRV<>"A" AND this.pEXEC="R" AND this.oParentObject.w_PNFLPART<>"N"
            * --- Se mofico causale di riga su riga non in Append che gestisce le partite
            *     inibisco la modifica per la presenza di partite collegate nel database
            ah_ErrorMsg("Attenzione, riga di primanota gestita a partite impossibile modificare causale di riga",,"")
            this.oParentObject.w_PNCAURIG = this.oParentObject.o_PNCAURIG
          else
            if this.w_CFUNC<>"Edit" OR I_SRV="A" OR (this.oParentObject.w_VARPAR="S" AND this.oParentObject.w_VARCEN="S")
              if Type("This.w_PROG.gscg_mpa.CNT")="O"
                WITH this.w_PROG
                SELECT (.cTrsName)
                i_nRec = RECNO()
                .GSCG_MPA.ChangeRow(.cRowID+ str(i_nRec,7,0), 0, .w_PNSERIAL, "PTSERIAL", .w_CPROWNUM,"PTROWORD")
                SELECT (.GSCG_MPA.CNT.cTrsName)
                ENDWITH
                * --- Annullo eventuali partite caricate
                if Reccount() <>0
                  DELETE ALL
                endif
              endif
            endif
          endif
        endif
        if this.pEXEC="C"
          * --- Annullo eventuali Movimenti di Analitica caricati
          this.w_PROG = this.oParentObject
          SELECT (this.w_PROG.cTrsName)
          if (this.w_CFUNC<>"Edit" OR I_SRV="A" ) and Not ( this.oParentObject.w_SEZB $ "CR" AND this.oParentObject.w_FLANAL="S" AND g_PERCCR="S" AND this.oParentObject.w_CCTAGG $ "AM")
            if Type("This.w_PROG.gscg_mca.CNT")="O"
              WITH this.w_PROG
              SELECT (.cTrsName)
              i_nRec = RECNO()
              .GSCG_MCA.ChangeRow(.cRowID+ str(i_nRec,7,0), 0, .w_PNSERIAL, "MRSERIAL", .w_CPROWNUM,"MRROWORD")
              SELECT (.GSCG_MCA.CNT.cTrsName)
              ENDWITH
              if Reccount() <>0
                DELETE ALL
              endif
            endif
          endif
          * --- Aggiorno dati C\C
          * --- Read from CONTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ANNUMCOR"+;
              " from "+i_cTable+" CONTI where ";
                  +"ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_PNTIPCON);
                  +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_PNCODCON);
                  +" and ANCODBAN = "+cp_ToStrODBC(this.oParentObject.w_BANAPP);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ANNUMCOR;
              from (i_cTable) where;
                  ANTIPCON = this.oParentObject.w_PNTIPCON;
                  and ANCODICE = this.oParentObject.w_PNCODCON;
                  and ANCODBAN = this.oParentObject.w_BANAPP;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_NUMCOR = NVL(cp_ToDate(_read_.ANNUMCOR),cp_NullValue(_read_.ANNUMCOR))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if Empty(this.oParentObject.w_NUMCOR)
            * --- Read from BAN_CONTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.BAN_CONTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.BAN_CONTI_idx,2],.t.,this.BAN_CONTI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CCCONCOR"+;
                " from "+i_cTable+" BAN_CONTI where ";
                    +"CCTIPCON = "+cp_ToStrODBC(this.oParentObject.w_PNTIPCON);
                    +" and CCCODCON = "+cp_ToStrODBC(this.oParentObject.w_PNCODCON);
                    +" and CCCODBAN = "+cp_ToStrODBC(this.oParentObject.w_BANAPP);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CCCONCOR;
                from (i_cTable) where;
                    CCTIPCON = this.oParentObject.w_PNTIPCON;
                    and CCCODCON = this.oParentObject.w_PNCODCON;
                    and CCCODBAN = this.oParentObject.w_BANAPP;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_NUMCOR = NVL(cp_ToDate(_read_.CCCONCOR),cp_NullValue(_read_.CCCONCOR))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
        endif
      case this.pExec="D"
        * --- Se numero registrazioni gestito a giorno o a giorno + utente al cambio 
        *     della data di registrazione segnalo il ricalcolo
        if g_UNIUTE $ "GE" And this.w_CFUNC="Edit"
          ah_ErrorMsg("La data registrazione � stata modificata dall'utente%0� stato riassegnato automaticamente il numero registrazione",,"")
        endif
        this.bUpdateParentObject=.f.
    endcase
  endproc


  proc Init(oParentObject,pEXEC)
    this.pEXEC=pEXEC
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='PAR_TITE'
    this.cWorkTables[3]='BAN_CONTI'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEXEC"
endproc
