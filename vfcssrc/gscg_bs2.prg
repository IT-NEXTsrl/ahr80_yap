* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bs2                                                        *
*              Seleziona/Deseleziona da gscg_msp                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-09-20                                                      *
* Last revis.: 2012-09-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bs2",oParentObject)
return(i_retval)

define class tgscg_bs2 as StdBatch
  * --- Local variables
  w_ObjMSP = .NULL.
  w_ValoreSelez = space(1)
  w_MaxRow = 0
  w_ObjCampo = .NULL.
  * --- WorkFile variables
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Seleziona o deseleziona tutto
    if this.oParentObject.w_SELALL="S"
      * --- Seleziona
      this.w_ValoreSelez = "S"
    else
      * --- Deseleziona
      this.w_ValoreSelez = "N"
    endif
    this.w_ObjMSP = this.oParentObject
    this.w_ObjMSP.MarkPos()     
    this.w_ObjMSP.FirstRow()     
    this.w_MaxRow = this.w_ObjMSP.NumRow()
    this.w_ObjCampo = this.w_ObjMSP.GETBODYCTRL("w_SAFLSALD")
    do while !this.w_ObjMSP.Eof_Trs() OR this.w_ObjMSP.RowIndex()<=this.w_MaxRow
      this.w_ObjMSP.SetRow()     
      this.w_ObjMSP.w_SAFLSALD = this.w_ValoreSelez
      GSCG_BS1(this.w_ObjMSP,"FLAG")
      this.w_ObjMSP.SaveRow()     
      this.w_ObjMSP.NextRow()     
    enddo
    this.w_ObjMSP.RePos(.T.)     
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
