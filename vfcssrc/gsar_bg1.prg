* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bg1                                                        *
*              CHECK RIGA DOCUMENTALE CONTRIBUTI ACCESORI                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-11-24                                                      *
* Last revis.: 2008-06-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_PADRE,pTipo,pRowNum
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bg1",oParentObject,m.w_PADRE,m.pTipo,m.pRowNum)
return(i_retval)

define class tgsar_bg1 as StdBatch
  * --- Local variables
  w_PADRE = .NULL.
  pTipo = space(1)
  pRowNum = 0
  w_RESULT = space(1)
  w_TESTATTU = space(105)
  w_DELESPL = .f.
  w_INSESPL = .f.
  w_UPDESPL = .f.
  w_RIGHERAEECHEEVADONO = 0
  w_TmpN = 0
  w_ROWINDEX = 0
  w_BREPOS = .f.
  w_ACTROW = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esamina la riga corrente e, in base alle informazioni attuali e quanto 
    *     memorizzato rispetto all'ultima verifica esplosione contributi accessori 
    *     decide che azioni intraprendere:
    *     
    *     La routine si occupa di rimuovere eventuali contributi accesori non pi� validi in 
    *     base ai nuovi criteri
    *     
    *     Riceve: 
    *     riferimento all'oggetto GSVE_MDV 
    *     il tipo di ricerca contributi accessori (di riga (ROW) o a peso (DTL), quindi riepilogativi in coda al documento)
    *     Numero riga attuale (se tipo ROW)
    *     
    *     Se DTL elimina tutte le righe sul dettaglio con riferimento MVRIFCAC vuoto 
    *     e categoria contributo piena (MVCACONT)
    *     
    *     Restituisce N se non occorre fare niente, I se occorre riesplodere ed aggiungere le righe
    *     e U se le righe vanno agigornate. Se solo da eliminare 'D'. (solo nel caso
    *     di attributi di riga)
    this.w_RESULT = "N"
    * --- Verifico stato propriet� esplsosione
    if this.pTipo="ROW"
      this.w_TESTATTU = PADR(this.w_PADRE.w_MVCODICE,41," ") + PADR(this.w_PADRE.w_MVTIPCON,1," ")+PADR(iif( empty( this.w_PADRE.w_MVCODORN ) , this.w_PADRE.w_MVCODCON , this.w_PADRE.w_MVCODORN),15," ")+PADR(YEAR(this.w_PADRE.w_MVDATDOC),4," ")+PADR(MONTH(this.w_PADRE.w_MVDATDOC),2," ")+PADR(DAY(this.w_PADRE.w_MVDATDOC),2," ")+PADR(this.w_PADRE.w_MVUNIMIS,3," ") + PADR(this.w_PADRE.w_MVCODVAL,3," ") + PADR(this.w_PADRE.w_FLFRAZ,1," ") + PADR(STR(this.w_PADRE.w_PERIVA,6,2),6," ") + PADR(this.w_PADRE.w_MVCODIVA,5," ")+ PADR(STR(this.w_PADRE.w_MVQTAMOV,18,g_PERPQT),18," ") + PADR(STR(IIF(this.w_PADRE.w_MVQTAMOV=0,0,this.w_PADRE.w_MVVALMAG/this.w_PADRE.w_MVQTAMOV),18, this.w_PADRE.w_DECUNI ),18," ") + PADR(this.w_PADRE.w_TDFLAPCA,1," ") + PADR(this.w_PADRE.w_ANFLAPCA,1," ") + PADR(this.w_PADRE.w_ARFLAPCA,1," ") 
      if this.w_TESTATTU <> this.w_PADRE.w_TESTESPLACC
        * --- Qualcosa � cambiato, abbiamo vari casi:
        *     a) Devo eliminare le righe della prececedente esplosione
        *        1)Se i flag su causale/articolo/cliente da tutti accessi (sul dettaglio) ad almeno uno spento (su ricalcolo)
        *        2) Se una delle condizioni che modificano il calcolo dei contributi � cambiata
        *     
        *     b) Devo re inserire il dettaglio
        *        Se condizione 2) punto a) e flag accessi sul valore attuale
        *     
        *     c)) Non devo ricalcolare ma aggiornare le righe presenti con il risultato dell'esplosione
        *         Se flag accessi sul valore attuale, non vera condizione 2) punto a) e modificati:
        *          %IVA
        *          %CODICE IVA
        *          %QUANTITA
        *          % PREZZO
        *          quindi i caratteri dal 73 per 47 caratteri
        *     
        *     d) Non devo fare nulla
        *        Se i 3 flag finali da spenti sono rimasti spenti..
        * --- Alltrim di garanzia ( se dichiarazione variabile sul documento di lunghezza superiore al suo contenuto)
        if not Empty( this.w_PADRE.w_TESTESPLACC)
          * --- Attivo la cancellazione solo se la riga � stata oggetto di precedente esplosione...
          this.w_DELESPL = Left( Alltrim(this.w_PADRE.w_TESTESPLACC) , 72 ) <> Left( alltrim(this.w_TESTATTU) , 72 ) Or (Right( Alltrim(this.w_PADRE.w_TESTESPLACC) , 3 ) = "SSS" And Right( alltrim(this.w_TESTATTU) , 3 ) <> "SSS")
        endif
        this.w_INSESPL = Left( Alltrim(this.w_PADRE.w_TESTESPLACC) , 72 ) <> Left( alltrim(this.w_TESTATTU) , 72 ) And Right( Alltrim(this.w_TESTATTU) , 3 ) = "SSS"
        this.w_UPDESPL = Left( Alltrim(this.w_PADRE.w_TESTESPLACC) , 72 ) = Left( alltrim(this.w_TESTATTU) , 72 ) And Right( Alltrim(this.w_TESTATTU) , 3 ) = "SSS" And SUBSTR( this.w_TESTATTU , 73 , 47 )<>SUBSTR( this.w_PADRE.w_TESTESPLACC , 73 , 47 )
        * --- Inserire o modificare sono alternativi...
        this.w_RESULT = IIF( this.w_INSESPL ,"I" , IIF( this.w_UPDESPL ,"U" ,iif( this.w_DELESPL , "D" , "N")))
        * --- Comunque aggiorno la riga padre...
        this.w_PADRE.Set("w_TESTESPLACC", this.w_TESTATTU , .T. ,.T.)     
        if this.w_DELESPL
          this.w_PADRE.Exec_Select("CheckKit", "Curs1.I_RECNO as I_RECNO, t_MVFLARIF, t_MVSERRIF" , " t_MVRIFCAC="+ cp_ToStrODBC( this.pRowNum ) ,"","","")     
          * --- Esegue il controllo che ci siano righe di contributo importate che devono essere ricalcolate
           
 Select Checkkit 
 Go Top
          COUNT FOR NOT EMPTY(NVL(t_MVSERRIF,"")) AND t_MVFLARIF$"+-" TO this.w_RIGHERAEECHEEVADONO
          if this.w_RIGHERAEECHEEVADONO > 0 OR !EMPTY( NVL( this.w_PADRE.w_MVSERRIF , "" ) )
            * --- Errore
            this.w_RESULT = "E"
          else
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
      endif
    else
      this.w_PADRE.Exec_Select("CheckKit", "Curs1.I_RECNO as I_RECNO" , " t_MVRIFCAC="+ cp_ToStrODBC( -1 )+" And Not Empty( t_MVCACONT )" ,"","","")     
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    i_retcode = 'stop'
    i_retval = this.w_RESULT
    return
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Determina l'elenco delle righe contributi accessori collegati alla riga corrente..
    *     (Utilizzo i_recno che identifica l'indice di riga, � utilizzare un'informazione di basso livello, se
    *     cambia il nome della colonna di servizio sul transitorio occorre riallineare il codice)
     
 Select Checkkit 
 Go Top
    this.w_TmpN = Nvl ( CheckKit.I_RECNO , 0 )
    if this.w_TMPN>0
      * --- Elimino i contirbuti associati al padre...
       
 Select Checkkit 
 Go Top
      * --- Memorizzo la posizione attuale del cursore e mi ci rimetto (MarkPos e Repos non sono compitro di questa routine...)
      this.w_ROWINDEX = this.w_PADRE.RowIndex()
      * --- L'indice d iriga non mi permette di riconoscere la riga a seguito di una cancellazione
      this.w_ACTROW = this.w_PADRE.Get("I_RECNO")
      if this.w_ACTROW=0
        * --- Sono oltre l'ultimo record
        this.w_PADRE.LastRow()     
        this.w_ACTROW = this.w_PADRE.Get("I_RECNO")
      endif
      this.w_BREPOS = .T.
      do while (.not. eof( "CheckKit" ))
        this.w_PADRE.SetRow(Nvl ( CheckKit.I_RECNO , 0 ))     
        * --- Se elimino la riga dove ero posizionato..
        this.w_BREPOS = this.w_BREPOS And this.w_ACTROW<>this.w_PADRE.Get("I_RECNO")
        this.w_PADRE.SubtractTotals()     
        this.w_PADRE.DeleteRow()     
        if Not Eof("Checkkit")
           
 Select Checkkit 
 Skip
        endif
      enddo
      if this.w_BREPOS And this.w_ACTROW>0
        * --- Mi rimetto nella riga di esplosione...
        this.w_PADRE.SetRow(this.w_ACTROW)     
      else
        * --- Se cancello contributi a peso mi riposiziono sulla prima riga (non so se partivo 
        *     da una riga poi cancellata)..
        this.w_PADRE.FirstRow()     
        this.w_PADRE.SetRow()     
      endif
    endif
    * --- Rimuovo il temporaneo...
    Use in CheckKit
  endproc


  proc Init(oParentObject,w_PADRE,pTipo,pRowNum)
    this.w_PADRE=w_PADRE
    this.pTipo=pTipo
    this.pRowNum=pRowNum
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_PADRE,pTipo,pRowNum"
endproc
