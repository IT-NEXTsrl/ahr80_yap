* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bfi                                                        *
*              Ricalcola codici I.V.A. (Filtri aggiuntivi)                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-09-13                                                      *
* Last revis.: 2011-09-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bfi",oParentObject,m.pParam)
return(i_retval)

define class tgsve_bfi as StdBatch
  * --- Local variables
  pParam = space(10)
  w_RIIVLSIV = space(0)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.bUpdateParentObject=.f.
    if this.pParam="OPEN"
      if this.oParentObject.w_FLRICIVA and ah_YesNo("Attenzione! Si vuole procedere con il ricalcolo dei codici I.V.A.?")
        do GSVE_KFI with this.oParentObject
        i_retcode = 'stop'
        i_retval = .t.
        return
      else
        i_retcode = 'stop'
        i_retval = .f.
        return
      endif
    else
      this.w_RIIVLSIV = ""
      Select (this.oParentObject.w_ZoomCodIva.cCursor)
      SCAN FOR xchk=1 and not empty(IVCODIVA)
      this.w_RIIVLSIV = this.w_RIIVLSIV + IVCODIVA + ","
      ENDSCAN
      i_retcode = 'stop'
      i_retval = this.w_RIIVLSIV
      return
    endif
  endproc


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
