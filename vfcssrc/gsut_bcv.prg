* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bcv                                                        *
*              Gestione archiviazione rapida                                   *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF]                                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-03-12                                                      *
* Last revis.: 2015-11-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_Archivio,w_Chiave,w_Programma,w_Operazione,pRow,pCol,pForm,pTBClass,pPrat,pCDPUBWEB
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bcv",oParentObject,m.w_Archivio,m.w_Chiave,m.w_Programma,m.w_Operazione,m.pRow,m.pCol,m.pForm,m.pTBClass,m.pPrat,m.pCDPUBWEB)
return(i_retval)

define class tgsut_bcv as StdBatch
  * --- Local variables
  w_Archivio = space(10)
  w_Chiave = space(20)
  w_Programma = space(10)
  w_Operazione = space(1)
  pRow = 0
  pCol = 0
  pForm = .f.
  pTBClass = space(15)
  pPrat = .f.
  pCDPUBWEB = space(1)
  w_TMPC = space(10)
  w_CONFMASK = .f.
  w_DESCRIZIONE = space(100)
  w_TMPN = 0
  w_TMPN1 = 0
  w_DBSERVER = space(20)
  w_DBNAME = space(30)
  w_USRNAME = space(30)
  w_USRPWD = space(30)
  w_CODAZI = space(5)
  w_CODUTE = 0
  w_TMPL = .f.
  w_LICLADOC = space(100)
  w_TIPOARCH = space(1)
  w_CLASSE = space(10)
  w_DESCRI = space(40)
  w_FILE = space(20)
  w_CONTA = 0
  w_CHIEDIATTRIBUTI = .f.
  w_CDVLPRED = space(100)
  w_CDCAMCUR = space(100)
  w_CHKVALPRED = .f.
  w_CHKOBBL = .f.
  w_TIPOATTR = space(1)
  w_VALPRED = space(10)
  w_VALATTR = space(100)
  w_curdir = space(50)
  w_CATCH = space(250)
  w_Periodo = ctod("  /  /  ")
  w_EMPTYFOLD = space(1)
  w_NEMPTYFOLD = space(100)
  w_DEMPTYFOLD = space(200)
  w_RICDESCRI = space(1)
  w_DESBRE = space(100)
  w_DESLUN = space(0)
  w_oMESS = .NULL.
  w_oPART = .NULL.
  w_SingMult = 0
  w_MODALLEG = space(1)
  w_TIPOALL = space(5)
  w_CLASALL = space(5)
  w_USCITA = .f.
  w_CLAALLE = space(5)
  w_TIPPATH = space(1)
  w_TIPCLA = space(1)
  w_CDPUBWEB = space(1)
  w_INARCHIVIO = space(1)
  w_IDMSSA = space(1)
  w_AS__TIPO = space(1)
  w_ASCODCAU = space(5)
  w_ASCLADOC = space(15)
  w_ASBAUTOM = space(1)
  w_ASNOREPS = space(1)
  w_ASCLAACQ = space(15)
  w_CDRIFDEF = space(1)
  w_CDCLAINF = space(15)
  w_PATHPR = space(50)
  w_RETVAL = 0
  w_NomeFile = space(10)
  w_NomeRep = space(10)
  w_INCLASSEDOC = space(15)
  w_ATTRIB_VAL = space(10)
  w_NUMATTR = 0
  w_SELCODCLA = space(15)
  w_GSUT_KFG = .NULL.
  w_OBJCTRL = .NULL.
  w_OSOURCE = .NULL.
  w_OLDPOSTIN = .f.
  w_ZOOMSELE = .NULL.
  w_CODATT = space(15)
  w_CAMCUR = space(0)
  w_ATTRIBUTO = space(0)
  w_VALCAM = space(0)
  w_TIPOATTR = space(1)
  w_RIFCAMPO = space(0)
  w_PVPERVIS = 0
  w_PVSELUTE = space(1)
  w_FILTRUTE = 0
  w_OBJ = .NULL.
  w_MODALL = space(1)
  w_OKRIC = .f.
  w_NUMREC = 0
  w_CODMOD = space(10)
  w_CLAMOD = space(15)
  w_PATMOD = space(250)
  w_QUEMOD = space(60)
  w_TESTO = space(0)
  w_TIPOALLE = space(5)
  w_CLASSEALL = space(5)
  w_OGGETTO = space(200)
  w_PATARC = space(250)
  w_FILEORIG = space(60)
  w_CODPRE = space(20)
  w_RAGPRE = space(10)
  w_CN__ENTE = space(10)
  w_CNTIPPRA = space(10)
  w_CNMATPRA = space(10)
  w_FOUND = 0
  * --- WorkFile variables
  LIB_IMMA_idx=0
  PROMCLAS_idx=0
  PRODCLAS_idx=0
  PRO_PERM_idx=0
  PROMINDI_idx=0
  PRODINDI_idx=0
  DMPARAM_idx=0
  CPUSRGRP_idx=0
  PRO_PVIS_idx=0
  DIPENDEN_idx=0
  CONTROPA_idx=0
  ASSCAUDO_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Permette di selezionare la libreria da utilizzare
    * --- Parametri
    * --- Nome della tabella associata alla gestione
    * --- Valore del campo/i chiave
    * --- Non viene utilizzato direttamente nella routine (Nome del programma attivo)
    * --- Tipo operazione da eseguire: Cattura; cattura da Scanner; Visualizza; Nuovo documento (C/S/V/D)
    * --- I parametri successivi sono richiesti solo nell'archiviazione rapida tramite scanner
    * --- Coordinata di riga per apparizione del men� a tendina di selezione acquisizione singola o multipla
    * --- Coordinata di colonna per apparizione del men� a tendina di selezione acquisizione singola o multipla
    * --- Indica il form su cui visualizzare il popup (se .t. indica la presenza di un form attivo nell'oggetto _Screen)
    * --- Parametro valorizzato solo se l'operazione di cattura o cattura da scanner � stata lanciata da toolbar
    *     in questo caso contiene il codice della classe documentale da utilizzare
    * --- Parametro per selezione maschera di apertura in base al bootno clicato sulla toolbar (cp_desk)
    * --- Paraqmetro per inizializzare se pubblica su web
    this.w_CDPUBWEB = "N"
    this.w_INARCHIVIO = "X"
    if vartype(this.pTBClass) = "L"
      * --- Controlli da eseguire solo se l'operazione � stata richiesta  con un form attivo
      do case
        case EMPTY(this.w_CHIAVE)
          * --- DocMan non installato o non � selezionato alcun record
          if VARTYPE(this.OParentObject)="O"
            AH_ERRORMSG("Codice %1 non inserito!",48,"",ALLTRIM(LOWER(SUBSTR(this.OParentObject.caption, 1, AT("/", this.OParentObject.caption) - 1))))
          else
            AH_ERRORMSG("Codice non inserito!",48)
          endif
          i_retcode = 'stop'
          return
        case type("this.oParentObject.cFunction") ="C" and this.oParentObject.cFunction<>"Query" and this.oParentObject.class<>"Tgsal_kbu"
          * --- Nella gestione Pratiche (GSPR_ACN) e nelle Attivit� (GSAG_AAT) anche in modifica di una pratica devo poter vedere i documenti
          if UPPER(this.oParentObject.cPrg)<>"GSPR_ACN" AND UPPER(this.oParentObject.cPrg)<>"GSAG_AAT"
            AH_ERRORMSG("Funzione eseguibile solo in interrogazione",48)
            i_retcode = 'stop'
            return
          endif
        case len(alltrim(this.w_Chiave))=1 and inlist(lower(this.oParentObject.class),"tgsar_acl","tgsar_afr") 
          * --- Caso particolare per Clienti/Fornitori in cui la chiave (ANTIPCON+ANCODICE) non � mai vuota.
          AH_ERRORMSG("Chiave %1 non inserita!",48,"",iif(vartype(this.oparentobject.CCOMMENT)="C",this.oparentobject.CCOMMENT,""))
          i_retcode = 'stop'
          return
      endcase
    endif
    * --- ============================================================================================================================
    do case
      case INLIST(this.w_OPERAZIONE, "C", "S", "U", "M", "I", "B")
        if this.w_OPERAZIONE="I"
          this.w_Archivio = IIF(this.w_Archivio="GBGBGB", space(20), this.w_Archivio)
          this.w_Chiave = IIF(this.w_CHIAVE="GBGBGB", space(100), this.w_CHIAVE)
        endif
        * --- Seleazione il tipo di archiviazione (singola o multipla)
        if INLIST(this.w_OPERAZIONE, "U", "M")
          if this.w_OPERAZIONE="U"
            this.w_SingMult = 1
          else
            this.w_SingMult = 2
          endif
          this.w_Operazione = "S"
        else
          if g_DMIP<>"S"
            if this.w_OPERAZIONE="S" and type("pRow")="N" and type("pCol")="N" 
              if i_VisualTheme = -1
                * --- Il men� a tendina viene creato a partire dalle posizioni precedente calcolate
                *     relative alla toolbar
                 
 Local nCmd 
 nCmd= 0 
 if ! this.pform 
 DEFINE POPUP popCmd FROM this.pRow,this.pCol IN SCREEN SHORTCUT 
 else 
 DEFINE POPUP popCmd FROM this.pRow,this.pCol SHORTCUT 
 endif 
 DEFINE BAR 1 OF popCmd PROMPT Ah_Msgformat("Acquisizione singola") 
 ON SELECTION BAR 1 OF popCmd nCmd = 1 
 DEFINE BAR 2 OF popCmd PROMPT Ah_Msgformat("Acquisizione multipla") 
 ON SELECTION BAR 2 OF popCmd nCmd = 2 
 ACTIVATE POPUP popCmd 
 DEACTIVATE POPUP popCmd 
 RELEASE POPUPS popCmd
              else
                public nCmd 
 nCmd = 0
                local oBtnMenu, oMenuItem 
 m.oBtnMenu = CreateObject("cbPopupMenu") 
 m.oBtnMenu.oPopupMenu.cOnClickProc="execscript(NAMEPROC)" 
 m.oMenuItem = m.oBtnMenu.AddMenuItem() 
 m.oMenuItem.Caption = Ah_Msgformat("Acquisizione singola") 
 m.oMenuItem.Picture = "bmp\scanner.ico" 
 m.oMenuItem.OnClick = "nCmd = 1" 
 m.oMenuItem.Visible=.T. 
 m.oMenuItem = m.oBtnMenu.AddMenuItem() 
 m.oMenuItem.Caption = Ah_Msgformat("Acquisizione multipla") 
 m.oMenuItem.OnClick = "nCmd = 2" 
 m.oMenuItem.Visible=.T. 
 m.oBtnMenu.InitMenu() 
 m.oBtnMenu.ShowMenu()
              endif
              * --- Variabile che indica se l'acquisizione da scanner � singola o multipla
              *     1 = singola 
              *     2 = multipla
              this.w_SingMult = nCmd
              release nCmd
              if this.w_SingMult = 0
                i_retcode = 'stop'
                return
              endif
            endif
          else
            if this.w_OPERAZIONE="S"
              this.w_SingMult = 1
            endif
          endif
        endif
        if this.w_OPERAZIONE="B" AND EMPTY(this.w_Archivio)
          ah_errormsg("Nessuna gestione aperta, operazione annullata")
          i_retcode = 'stop'
          return
        endif
        if NOT(this.w_OPERAZIONE="I" AND EMPTY(this.w_Archivio))
          * --- Legge, se c'�, la classe di archiviazione standard
          if vartype(this.pTBClass) = "C" and ! empty(this.pTBClass)
            this.w_LICLADOC = this.pTBClass
            * --- Modifico la propriet� che contiene il riferimento all'oggetto 'padre'
            this.oParentObject = .null.
            * --- Read from PROMCLAS
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PROMCLAS_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CDPUBWEB,CDCLAINF"+;
                " from "+i_cTable+" PROMCLAS where ";
                    +"CDCODCLA = "+cp_ToStrODBC(this.w_LICLADOC);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CDPUBWEB,CDCLAINF;
                from (i_cTable) where;
                    CDCODCLA = this.w_LICLADOC;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CDPUBWEB = NVL(cp_ToDate(_read_.CDPUBWEB),cp_NullValue(_read_.CDPUBWEB))
              this.w_CDCLAINF = NVL(cp_ToDate(_read_.CDCLAINF),cp_NullValue(_read_.CDCLAINF))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          else
            * --- Cerco un'associazione tra causale e classe
            do case
              case this.w_Archivio="PNT_MAST"
                if type("this.oParentObject.w_PNCODCAU")="C"
                  this.w_ASCODCAU = this.oParentObject.w_PNCODCAU
                  this.w_AS__TIPO = "C"
                endif
              case this.w_Archivio="DOC_MAST"
                if type("this.oParentObject.w_MVTIPDOC")="C"
                  this.w_ASCODCAU = this.oParentObject.w_MVTIPDOC
                  this.w_AS__TIPO = "D"
                endif
            endcase
            if not empty(this.w_ASCODCAU)
              * --- Read from ASSCAUDO
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.ASSCAUDO_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ASSCAUDO_idx,2],.t.,this.ASSCAUDO_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ASCLADOC,ASBAUTOM,ASCLAACQ,ASNOREPS"+;
                  " from "+i_cTable+" ASSCAUDO where ";
                      +"AS__TIPO = "+cp_ToStrODBC(this.w_AS__TIPO);
                      +" and ASCODCAU = "+cp_ToStrODBC(this.w_ASCODCAU);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ASCLADOC,ASBAUTOM,ASCLAACQ,ASNOREPS;
                  from (i_cTable) where;
                      AS__TIPO = this.w_AS__TIPO;
                      and ASCODCAU = this.w_ASCODCAU;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_ASCLADOC = NVL(cp_ToDate(_read_.ASCLADOC),cp_NullValue(_read_.ASCLADOC))
                this.w_ASBAUTOM = NVL(cp_ToDate(_read_.ASBAUTOM),cp_NullValue(_read_.ASBAUTOM))
                this.w_ASCLAACQ = NVL(cp_ToDate(_read_.ASCLAACQ),cp_NullValue(_read_.ASCLAACQ))
                this.w_ASNOREPS = NVL(cp_ToDate(_read_.ASNOREPS),cp_NullValue(_read_.ASNOREPS))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            do case
              case this.w_OPERAZIONE="B"
                * --- Associazione a Infinity tramite barcode
                if not empty(this.w_ASCLADOC)
                  * --- Read from PROMCLAS
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.PROMCLAS_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "CDCODCLA,CDMODALL,CDRIFDEF,CDTIPARC,CDCLAALL,CDCOMTIP,CDCOMCLA,CDPUBWEB,CDCLAINF"+;
                      " from "+i_cTable+" PROMCLAS where ";
                          +"CDCODCLA = "+cp_ToStrODBC(this.w_ASCLADOC);
                          +" and CDRIFTAB = "+cp_ToStrODBC(this.w_ARCHIVIO);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      CDCODCLA,CDMODALL,CDRIFDEF,CDTIPARC,CDCLAALL,CDCOMTIP,CDCOMCLA,CDPUBWEB,CDCLAINF;
                      from (i_cTable) where;
                          CDCODCLA = this.w_ASCLADOC;
                          and CDRIFTAB = this.w_ARCHIVIO;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_LICLADOC = NVL(cp_ToDate(_read_.CDCODCLA),cp_NullValue(_read_.CDCODCLA))
                    this.w_MODALLEG = NVL(cp_ToDate(_read_.CDMODALL),cp_NullValue(_read_.CDMODALL))
                    this.w_CDRIFDEF = NVL(cp_ToDate(_read_.CDRIFDEF),cp_NullValue(_read_.CDRIFDEF))
                    this.w_TIPOARCH = NVL(cp_ToDate(_read_.CDTIPARC),cp_NullValue(_read_.CDTIPARC))
                    this.w_CLAALLE = NVL(cp_ToDate(_read_.CDCLAALL),cp_NullValue(_read_.CDCLAALL))
                    this.w_TIPPATH = NVL(cp_ToDate(_read_.CDCOMTIP),cp_NullValue(_read_.CDCOMTIP))
                    this.w_TIPCLA = NVL(cp_ToDate(_read_.CDCOMCLA),cp_NullValue(_read_.CDCOMCLA))
                    this.w_CDPUBWEB = NVL(cp_ToDate(_read_.CDPUBWEB),cp_NullValue(_read_.CDPUBWEB))
                    this.w_CDCLAINF = NVL(cp_ToDate(_read_.CDCLAINF),cp_NullValue(_read_.CDCLAINF))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                  if this.w_CDRIFDEF="N" or not this.w_MODALLEG $ "IS"
                    this.w_LICLADOC = " "
                  endif
                endif
                if empty(this.w_LICLADOC)
                  * --- Infinity stand alone
                  * --- Read from PROMCLAS
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.PROMCLAS_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "CDCODCLA,CDMODALL,CDTIPARC,CDCLAALL,CDCOMTIP,CDCOMCLA,CDPUBWEB,CDCLAINF"+;
                      " from "+i_cTable+" PROMCLAS where ";
                          +"CDRIFTAB = "+cp_ToStrODBC(this.w_ARCHIVIO);
                          +" and CDRIFDEF = "+cp_ToStrODBC("S");
                          +" and CDMODALL = "+cp_ToStrODBC("S");
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      CDCODCLA,CDMODALL,CDTIPARC,CDCLAALL,CDCOMTIP,CDCOMCLA,CDPUBWEB,CDCLAINF;
                      from (i_cTable) where;
                          CDRIFTAB = this.w_ARCHIVIO;
                          and CDRIFDEF = "S";
                          and CDMODALL = "S";
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_LICLADOC = NVL(cp_ToDate(_read_.CDCODCLA),cp_NullValue(_read_.CDCODCLA))
                    this.w_MODALLEG = NVL(cp_ToDate(_read_.CDMODALL),cp_NullValue(_read_.CDMODALL))
                    this.w_TIPOARCH = NVL(cp_ToDate(_read_.CDTIPARC),cp_NullValue(_read_.CDTIPARC))
                    this.w_CLAALLE = NVL(cp_ToDate(_read_.CDCLAALL),cp_NullValue(_read_.CDCLAALL))
                    this.w_TIPPATH = NVL(cp_ToDate(_read_.CDCOMTIP),cp_NullValue(_read_.CDCOMTIP))
                    this.w_TIPCLA = NVL(cp_ToDate(_read_.CDCOMCLA),cp_NullValue(_read_.CDCOMCLA))
                    this.w_CDPUBWEB = NVL(cp_ToDate(_read_.CDPUBWEB),cp_NullValue(_read_.CDPUBWEB))
                    this.w_CDCLAINF = NVL(cp_ToDate(_read_.CDCLAINF),cp_NullValue(_read_.CDCLAINF))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                  if g_REVI="S" AND EMPTY(this.w_CDCLAINF)
                    this.w_LICLADOC = " "
                  endif
                  if g_DMIP="S" and empty(this.w_LICLADOC)
                    * --- Infinity DMS
                    * --- Read from PROMCLAS
                    i_nOldArea=select()
                    if used('_read_')
                      select _read_
                      use
                    endif
                    i_nConn=i_TableProp[this.PROMCLAS_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select "+;
                        "CDCODCLA,CDMODALL,CDTIPARC,CDCLAALL,CDCOMTIP,CDCOMCLA,CDPUBWEB,CDCLAINF"+;
                        " from "+i_cTable+" PROMCLAS where ";
                            +"CDRIFTAB = "+cp_ToStrODBC(this.w_ARCHIVIO);
                            +" and CDRIFDEF = "+cp_ToStrODBC("S");
                            +" and CDMODALL = "+cp_ToStrODBC("I");
                             ,"_read_")
                      i_Rows=iif(used('_read_'),reccount(),0)
                    else
                      select;
                        CDCODCLA,CDMODALL,CDTIPARC,CDCLAALL,CDCOMTIP,CDCOMCLA,CDPUBWEB,CDCLAINF;
                        from (i_cTable) where;
                            CDRIFTAB = this.w_ARCHIVIO;
                            and CDRIFDEF = "S";
                            and CDMODALL = "I";
                         into cursor _read_
                      i_Rows=_tally
                    endif
                    if used('_read_')
                      locate for 1=1
                      this.w_LICLADOC = NVL(cp_ToDate(_read_.CDCODCLA),cp_NullValue(_read_.CDCODCLA))
                      this.w_MODALLEG = NVL(cp_ToDate(_read_.CDMODALL),cp_NullValue(_read_.CDMODALL))
                      this.w_TIPOARCH = NVL(cp_ToDate(_read_.CDTIPARC),cp_NullValue(_read_.CDTIPARC))
                      this.w_CLAALLE = NVL(cp_ToDate(_read_.CDCLAALL),cp_NullValue(_read_.CDCLAALL))
                      this.w_TIPPATH = NVL(cp_ToDate(_read_.CDCOMTIP),cp_NullValue(_read_.CDCOMTIP))
                      this.w_TIPCLA = NVL(cp_ToDate(_read_.CDCOMCLA),cp_NullValue(_read_.CDCOMCLA))
                      this.w_CDPUBWEB = NVL(cp_ToDate(_read_.CDPUBWEB),cp_NullValue(_read_.CDPUBWEB))
                      this.w_CDCLAINF = NVL(cp_ToDate(_read_.CDCLAINF),cp_NullValue(_read_.CDCLAINF))
                      use
                    else
                      * --- Error: sql sentence error.
                      i_Error = MSG_READ_ERROR
                      return
                    endif
                    select (i_nOldArea)
                    if g_REVI="S" AND EMPTY(this.w_CDCLAINF)
                      this.w_LICLADOC = " "
                    endif
                  endif
                endif
              case this.w_OPERAZIONE$"CS" AND not empty(this.w_ASCLAACQ)
                * --- Acquisizione da file/scanner
                * --- Read from PROMCLAS
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.PROMCLAS_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "CDCODCLA,CDMODALL,CDRIFDEF,CDTIPARC,CDCLAALL,CDCOMTIP,CDCOMCLA,CDPUBWEB,CDCLAINF"+;
                    " from "+i_cTable+" PROMCLAS where ";
                        +"CDCODCLA = "+cp_ToStrODBC(this.w_ASCLAACQ);
                        +" and CDRIFTAB = "+cp_ToStrODBC(this.w_ARCHIVIO);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    CDCODCLA,CDMODALL,CDRIFDEF,CDTIPARC,CDCLAALL,CDCOMTIP,CDCOMCLA,CDPUBWEB,CDCLAINF;
                    from (i_cTable) where;
                        CDCODCLA = this.w_ASCLAACQ;
                        and CDRIFTAB = this.w_ARCHIVIO;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_LICLADOC = NVL(cp_ToDate(_read_.CDCODCLA),cp_NullValue(_read_.CDCODCLA))
                  this.w_MODALLEG = NVL(cp_ToDate(_read_.CDMODALL),cp_NullValue(_read_.CDMODALL))
                  this.w_CDRIFDEF = NVL(cp_ToDate(_read_.CDRIFDEF),cp_NullValue(_read_.CDRIFDEF))
                  this.w_TIPOARCH = NVL(cp_ToDate(_read_.CDTIPARC),cp_NullValue(_read_.CDTIPARC))
                  this.w_CLAALLE = NVL(cp_ToDate(_read_.CDCLAALL),cp_NullValue(_read_.CDCLAALL))
                  this.w_TIPPATH = NVL(cp_ToDate(_read_.CDCOMTIP),cp_NullValue(_read_.CDCOMTIP))
                  this.w_TIPCLA = NVL(cp_ToDate(_read_.CDCOMCLA),cp_NullValue(_read_.CDCOMCLA))
                  this.w_CDPUBWEB = NVL(cp_ToDate(_read_.CDPUBWEB),cp_NullValue(_read_.CDPUBWEB))
                  this.w_CDCLAINF = NVL(cp_ToDate(_read_.CDCLAINF),cp_NullValue(_read_.CDCLAINF))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                if this.w_CDRIFDEF="N" or g_REVI="S" AND EMPTY(this.w_CDCLAINF)
                  this.w_LICLADOC = " "
                endif
                if empty(this.w_LICLADOC)
                  this.Pag2()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                endif
              otherwise
                this.Pag2()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
            endcase
          endif
          if empty(this.w_LICLADOC)
            * --- Verifica se c'� almeno una classe
            * --- Read from PROMCLAS
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PROMCLAS_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "*"+;
                " from "+i_cTable+" PROMCLAS where ";
                    +"CDRIFTAB = "+cp_ToStrODBC(this.w_ARCHIVIO);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                *;
                from (i_cTable) where;
                    CDRIFTAB = this.w_ARCHIVIO;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if i_rows=0
              AH_ERRORMSG("Non sono state definite classi documentali per la tabella %1",48,"",alltrim(this.w_ARCHIVIO))
              i_retcode = 'stop'
              return
            else
              * --- Verifica se c'� una classe da proporre
              if this.w_OPERAZIONE="B"
                * --- Associazione a Infinity tramite barcode
                * --- Infinity stand alone
                * --- Read from PROMCLAS
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.PROMCLAS_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "CDCODCLA,CDDESCLA,CDTIPARC,CDMODALL,CDCOMTIP,CDCOMCLA,CDPUBWEB,CDCLAINF"+;
                    " from "+i_cTable+" PROMCLAS where ";
                        +"CDRIFTAB = "+cp_ToStrODBC(this.w_ARCHIVIO);
                        +" and CDRIFDEF = "+cp_ToStrODBC("D");
                        +" and CDMODALL = "+cp_ToStrODBC("S");
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    CDCODCLA,CDDESCLA,CDTIPARC,CDMODALL,CDCOMTIP,CDCOMCLA,CDPUBWEB,CDCLAINF;
                    from (i_cTable) where;
                        CDRIFTAB = this.w_ARCHIVIO;
                        and CDRIFDEF = "D";
                        and CDMODALL = "S";
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_CLASSE = NVL(cp_ToDate(_read_.CDCODCLA),cp_NullValue(_read_.CDCODCLA))
                  this.w_DESCRI = NVL(cp_ToDate(_read_.CDDESCLA),cp_NullValue(_read_.CDDESCLA))
                  this.w_TIPOARCH = NVL(cp_ToDate(_read_.CDTIPARC),cp_NullValue(_read_.CDTIPARC))
                  this.w_MODALLEG = NVL(cp_ToDate(_read_.CDMODALL),cp_NullValue(_read_.CDMODALL))
                  this.w_TIPPATH = NVL(cp_ToDate(_read_.CDCOMTIP),cp_NullValue(_read_.CDCOMTIP))
                  this.w_TIPCLA = NVL(cp_ToDate(_read_.CDCOMCLA),cp_NullValue(_read_.CDCOMCLA))
                  this.w_CDPUBWEB = NVL(cp_ToDate(_read_.CDPUBWEB),cp_NullValue(_read_.CDPUBWEB))
                  this.w_CDCLAINF = NVL(cp_ToDate(_read_.CDCLAINF),cp_NullValue(_read_.CDCLAINF))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                if g_REVI="S" AND EMPTY(this.w_CDCLAINF)
                  this.w_CLASSE = " "
                endif
                if g_DMIP="S" and empty(this.w_CLASSE)
                  * --- Infinity DMS
                  * --- Read from PROMCLAS
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.PROMCLAS_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "CDCODCLA,CDDESCLA,CDTIPARC,CDMODALL,CDCOMTIP,CDCOMCLA,CDPUBWEB,CDCLAINF"+;
                      " from "+i_cTable+" PROMCLAS where ";
                          +"CDRIFTAB = "+cp_ToStrODBC(this.w_ARCHIVIO);
                          +" and CDRIFDEF = "+cp_ToStrODBC("D");
                          +" and CDMODALL = "+cp_ToStrODBC("I");
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      CDCODCLA,CDDESCLA,CDTIPARC,CDMODALL,CDCOMTIP,CDCOMCLA,CDPUBWEB,CDCLAINF;
                      from (i_cTable) where;
                          CDRIFTAB = this.w_ARCHIVIO;
                          and CDRIFDEF = "D";
                          and CDMODALL = "I";
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_CLASSE = NVL(cp_ToDate(_read_.CDCODCLA),cp_NullValue(_read_.CDCODCLA))
                    this.w_DESCRI = NVL(cp_ToDate(_read_.CDDESCLA),cp_NullValue(_read_.CDDESCLA))
                    this.w_TIPOARCH = NVL(cp_ToDate(_read_.CDTIPARC),cp_NullValue(_read_.CDTIPARC))
                    this.w_MODALLEG = NVL(cp_ToDate(_read_.CDMODALL),cp_NullValue(_read_.CDMODALL))
                    this.w_TIPPATH = NVL(cp_ToDate(_read_.CDCOMTIP),cp_NullValue(_read_.CDCOMTIP))
                    this.w_TIPCLA = NVL(cp_ToDate(_read_.CDCOMCLA),cp_NullValue(_read_.CDCOMCLA))
                    this.w_CDPUBWEB = NVL(cp_ToDate(_read_.CDPUBWEB),cp_NullValue(_read_.CDPUBWEB))
                    this.w_CDCLAINF = NVL(cp_ToDate(_read_.CDCLAINF),cp_NullValue(_read_.CDCLAINF))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                endif
              else
                * --- Read from PROMCLAS
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.PROMCLAS_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "CDCODCLA,CDDESCLA,CDTIPARC,CDMODALL,CDCOMTIP,CDCOMCLA,CDPUBWEB,CDCLAINF"+;
                    " from "+i_cTable+" PROMCLAS where ";
                        +"CDRIFTAB = "+cp_ToStrODBC(this.w_ARCHIVIO);
                        +" and CDRIFDEF = "+cp_ToStrODBC("D");
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    CDCODCLA,CDDESCLA,CDTIPARC,CDMODALL,CDCOMTIP,CDCOMCLA,CDPUBWEB,CDCLAINF;
                    from (i_cTable) where;
                        CDRIFTAB = this.w_ARCHIVIO;
                        and CDRIFDEF = "D";
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_CLASSE = NVL(cp_ToDate(_read_.CDCODCLA),cp_NullValue(_read_.CDCODCLA))
                  this.w_DESCRI = NVL(cp_ToDate(_read_.CDDESCLA),cp_NullValue(_read_.CDDESCLA))
                  this.w_TIPOARCH = NVL(cp_ToDate(_read_.CDTIPARC),cp_NullValue(_read_.CDTIPARC))
                  this.w_MODALLEG = NVL(cp_ToDate(_read_.CDMODALL),cp_NullValue(_read_.CDMODALL))
                  this.w_TIPPATH = NVL(cp_ToDate(_read_.CDCOMTIP),cp_NullValue(_read_.CDCOMTIP))
                  this.w_TIPCLA = NVL(cp_ToDate(_read_.CDCOMCLA),cp_NullValue(_read_.CDCOMCLA))
                  this.w_CDPUBWEB = NVL(cp_ToDate(_read_.CDPUBWEB),cp_NullValue(_read_.CDPUBWEB))
                  this.w_CDCLAINF = NVL(cp_ToDate(_read_.CDCLAINF),cp_NullValue(_read_.CDCLAINF))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
              endif
              if g_REVI="S" AND EMPTY(this.w_CDCLAINF)
                this.w_CLASSE = " "
                this.w_DESCRI = " "
              endif
              * --- Verifica diritti ...
              if not empty(this.w_CLASSE) and SUBSTR( CHKPECLA( this.w_CLASSE, i_CODUTE), 2, 1 ) <> "S"
                this.w_CLASSE = " "
                this.w_DESCRI = " "
              endif
              * --- Lancia Maschera di selezione Classe Documentale
              this.w_CONFMASK = .F.
              do GSUT_KKW with this
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              this.w_LICLADOC = this.w_CLASSE
              if empty(this.w_LICLADOC) or not this.w_CONFMASK
                i_retcode = 'stop'
                return
              endif
            endif
          else
            * --- Non pu� archiviare con SCANNER se la modalit� di archiviazione � COLLEGAMENTO.
            *     In tal caso infatti il file viene salvato dallo scanner nella temp dell'utente, quindi � necessario che l'archiviazione avvenga per copia file.
            *     Se la classe � COLLEGAMENTO, non � specificato nessun percorso di archiviazione, ma se 
            *     mantiene quello originale i files scannerizzati sono nella temp !
            *     Quindi tutto ok solo se: l'archiviazione � da file (quindi non da scanner) oppure se la modalit� di 
            *     archiviazione � copia file (in tal caso va bene anche l'archiviazione da scanner).
            if this.w_OPERAZIONE="S" and this.w_MODALLEG="L"
              AH_ERRORMSG("Classe documentale standard [%1] con modalit� di archiviazione di tipo <collegamento file>.%0Archiviazione da scanner non consentita.",48,"",alltrim(this.w_LICLADOC))
              i_retcode = 'stop'
              return
            endif
            if TYPE("pCDPUBWEB")="C"
              this.w_CDPUBWEB = this.pCDPUBWEB
            else
              * --- Leggo se devo pubblicare su WEB
              * --- Read from PROMCLAS
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.PROMCLAS_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "CDPUBWEB,CDCLAINF"+;
                  " from "+i_cTable+" PROMCLAS where ";
                      +"CDCODCLA = "+cp_ToStrODBC(this.w_LICLADOC);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  CDPUBWEB,CDCLAINF;
                  from (i_cTable) where;
                      CDCODCLA = this.w_LICLADOC;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_CDPUBWEB = NVL(cp_ToDate(_read_.CDPUBWEB),cp_NullValue(_read_.CDPUBWEB))
                this.w_CDCLAINF = NVL(cp_ToDate(_read_.CDCLAINF),cp_NullValue(_read_.CDCLAINF))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
          endif
          * --- Verifica se l'utente pu� archiviare ....
          if SUBSTR( CHKPECLA( this.w_LICLADOC, i_CODUTE, this.w_ARCHIVIO), 2, 1 ) <> "S"
            AH_ERRORMSG("Non si possiedono i diritti per archiviare documenti con la classe documentale %1",48,"",alltrim(this.w_LICLADOC))
            i_retcode = 'stop'
            return
          endif
          * --- Non pu� archiviare con SCANNER se la modalit� di archiviazione � COLLEGAMENTO.
          *     In tal caso infatti il file viene salvato dallo scanner nella temp dell'utente, quindi � necessario che l'archiviazione avvenga per copia file.
          *     Se la classe � COLLEGAMENTO, non � specificato nessun percorso di archiviazione, ma se 
          *     mantiene quello originale i files scannerizzati sono nella temp !
          *     Quindi tutto ok solo se: l'archiviazione � da file (quindi non da scanner) oppure se la modalit� di 
          *     archiviazione � copia file (in tal caso va bene anche l'archiviazione da scanner).
          if this.w_OPERAZIONE="S" and this.w_MODALLEG="L"
            AH_ERRORMSG("La classe documentale selezionata [%1] ha modalit� di archiviazione di tipo <collegamento file>.%0Archiviazione da scanner non consentita.",48,"",alltrim(this.w_LICLADOC))
            i_retcode = 'stop'
            return
          endif
          if g_REVI="S" AND EMPTY(this.w_CDCLAINF)
            AH_ERRORMSG("La classe documentale selezionata [%1] non � associata a nessuna classe Infinity.",48,"",alltrim(this.w_LICLADOC))
            i_retcode = 'stop'
            return
          endif
          if this.w_OPERAZIONE="B"
            * --- Acquisisce il file da infinity, creo l'etichetta bar-code per l'associazione automatica
            if EMPTY(NVL(this.w_CHIAVE,""))
              ah_ErrorMsg("Selezionare un record.")
            else
              if this.w_MODALLEG<>"S"
                * --- creo pdf vuoto per CPIN e DMIP (su IDMSsa non ci deve essere)
                this.w_CATCH = FORCEEXT(ADDBS(tempadhoc())+SUBSTR(SYS(2015),2),"PDF")
                copy file "query\gsut_bcv.pdf" to (this.w_CATCH)
              endif
            endif
          endif
          if g_DMIP<>"S" or this.w_MODALLEG="S"
            * --- Verifica se � attivo il check di folder-vuoto
            * --- Read from PROMCLAS
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PROMCLAS_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CDFOLDER,CDGETDES,CDMODALL"+;
                " from "+i_cTable+" PROMCLAS where ";
                    +"CDCODCLA = "+cp_ToStrODBC(this.w_LICLADOC);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CDFOLDER,CDGETDES,CDMODALL;
                from (i_cTable) where;
                    CDCODCLA = this.w_LICLADOC;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_EMPTYFOLD = NVL(cp_ToDate(_read_.CDFOLDER),cp_NullValue(_read_.CDFOLDER))
              this.w_RICDESCRI = NVL(cp_ToDate(_read_.CDGETDES),cp_NullValue(_read_.CDGETDES))
              this.w_MODALLEG = NVL(cp_ToDate(_read_.CDMODALL),cp_NullValue(_read_.CDMODALL))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Richiede file da archiviare 
            this.w_curdir = sys(5) + sys(2003)
            do case
              case this.w_OPERAZIONE="C"
                * --- Se richiede il folder, si sposta su una cartella temporanea di archiviazione nella tempo dell'utente
                if this.w_EMPTYFOLD="S" and this.w_MODALLEG<>"L"
                  this.w_NEMPTYFOLD = "_DMTMPFOLD"
                  this.w_DEMPTYFOLD = ADDBS(tempadhoc())+this.w_NEMPTYFOLD
                  * --- Abilita gestione errori
                  if DIRECTORY(this.w_DEMPTYFOLD)
                    * --- Pulizia cartella
                    w_ErrorHandler = on("ERROR")
                    w_ERRORE = .F.
                    on error w_ERRORE = .T.
                    cd (this.w_DEMPTYFOLD)
                    DELETE FILE *.*
                    on error &w_ErrorHandler
                    if w_ERRORE
                      AH_ERRORMSG("Impossibile cancellare il contenuto della cartella contenitore: %0%1%0%0Utilizza archiviazione rapida standard singola",48,"",rtrim(this.w_DEMPTYFOLD))
                      this.w_EMPTYFOLD = "N"
                      * --- Ripristina cartella di lavoro ,,,
                      cd (this.w_curdir)
                    endif
                  else
                    * --- Crea la cartella
                    w_ErrorHandler = on("ERROR")
                    w_ERRORE = .F.
                    on error w_ERRORE = .T.
                    MD (this.w_DEMPTYFOLD)
                    on error &w_ErrorHandler
                    if w_ERRORE
                      ah_ERRORMSG("Impossibile creare la cartella contenitore: %0%1%0%0Utilizza archiviazione rapida standard singola",48,"",rtrim(this.w_DEMPTYFOLD))
                      this.w_EMPTYFOLD = "N"
                      * --- Ripristina cartella di lavoro ,,,
                      cd (this.w_curdir)
                    else
                      cd (this.w_DEMPTYFOLD)
                    endif
                  endif
                endif
                * --- Ricerca percorso di 'Prelevamento' in Dipendenti/Persone o nelle Contropartite...
                *     Questo solo se nella classe documentale non � attivo il check su gestione 'Cartella contenitore'.
                if this.w_EMPTYFOLD # "S"
                  this.w_CODAZI = i_CODAZI
                  this.w_CODUTE = i_CODUTE
                  * --- Read from DIPENDEN
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.DIPENDEN_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2],.t.,this.DIPENDEN_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "DPPATHPR"+;
                      " from "+i_cTable+" DIPENDEN where ";
                          +"DPCODUTE = "+cp_ToStrODBC(this.w_CODUTE);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      DPPATHPR;
                      from (i_cTable) where;
                          DPCODUTE = this.w_CODUTE;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_PATHPR = NVL(cp_ToDate(_read_.DPPATHPR),cp_NullValue(_read_.DPPATHPR))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                  if Empty(this.w_PATHPR) OR ! DIRECTORY(this.w_PATHPR)
                    * --- Read from CONTROPA
                    i_nOldArea=select()
                    if used('_read_')
                      select _read_
                      use
                    endif
                    i_nConn=i_TableProp[this.CONTROPA_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2],.t.,this.CONTROPA_idx)
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select "+;
                        "COPATHPR"+;
                        " from "+i_cTable+" CONTROPA where ";
                            +"COCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                             ,"_read_")
                      i_Rows=iif(used('_read_'),reccount(),0)
                    else
                      select;
                        COPATHPR;
                        from (i_cTable) where;
                            COCODAZI = this.w_CODAZI;
                         into cursor _read_
                      i_Rows=_tally
                    endif
                    if used('_read_')
                      locate for 1=1
                      this.w_PATHPR = NVL(cp_ToDate(_read_.COPATHPR),cp_NullValue(_read_.COPATHPR))
                      use
                    else
                      * --- Error: sql sentence error.
                      i_Error = MSG_READ_ERROR
                      return
                    endif
                    select (i_nOldArea)
                  endif
                  if Not Empty(this.w_PATHPR) and DIRECTORY(this.w_PATHPR)
                    * --- Applico path di prelevamento indicato nei parametri
                    cd (this.w_PATHPR)
                  endif
                endif
                * --- Richiede il file ...
                this.w_CATCH = getfile()
                * --- Si prepara ARRAY con elenco dei files presenti nella cartella ...
                if this.w_EMPTYFOLD="S" and this.w_MODALLEG<>"L" and not empty(this.w_CATCH)
                  * --- Se � sulla cartella di archiviazione ...
                  Dimension DMARRFIL(1)
                  if this.w_NEMPTYFOLD $ this.w_CATCH
                    ADIR(DMARRFIL)
                  endif
                endif
              case this.w_OPERAZIONE="S"
                * --- Continua
                this.w_CATCH = GSUT_BBS(this.oParentObject, this.w_SingMult)
            endcase
            * --- Ripristina cartella di lavoro ,,,
            cd (this.w_curdir)
          endif
          this.w_DESBRE = " "
          this.w_DESLUN = " "
          if this.w_RICDESCRI="S"
            this.w_CONFMASK = .F.
            do gsut1ksw with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_DESBRE = IIF(this.w_CONFMASK, this.w_DESBRE, " ")
            this.w_DESLUN = IIF(this.w_CONFMASK, this.w_DESLUN, " ")
          endif
        endif
        if not empty(this.w_CATCH) OR g_DMIP="S" OR this.w_MODALLEG="S"
          * --- Se da cartella contenitore (utilizabile solo se non si archivia da scanner) archivia tutti i files presenti nella cartella ...
          if this.w_OPERAZIONE="C" and this.w_EMPTYFOLD="S" and this.w_MODALLEG<>"L" and vartype(DMARRFIL(1))="C"
            this.w_CONTA = 1
            do while this.w_CONTA <= ALEN(DMARRFIL,1)
              this.w_CATCH = this.w_DEMPTYFOLD+"\"+DMARRFIL(this.w_CONTA,1)
              this.w_TMPN = -1
              if FILE(this.w_CATCH)
                Private L_ArrParam
                dimension L_ArrParam(30)
                L_ArrParam(1)="AR"
                L_ArrParam(2)=this.w_CATCH
                L_ArrParam(3)=this.w_LICLADOC
                L_ArrParam(4)=i_CODUTE
                L_ArrParam(5)=this.oParentObject
                L_ArrParam(6)=this.w_ARCHIVIO
                L_ArrParam(7)=this.w_CHIAVE
                L_ArrParam(8)=this.w_DESBRE
                L_ArrParam(9)=this.w_DESLUN
                L_ArrParam(10)=.T.
                L_ArrParam(11)=.F.
                L_ArrParam(12)=" "
                L_ArrParam(13)=" "
                L_ArrParam(14)=this.oParentObject
                L_ArrParam(15)=this.w_TIPOALLE
                L_ArrParam(16)=this.w_CLASSEALL
                L_ArrParam(17)=" "
                L_ArrParam(18)=this.w_QUEMOD
                L_ArrParam(19)=this.w_OPERAZIONE
                L_ArrParam(23)=(NVL(this.w_CDPUBWEB, "N")="S")
                this.w_TMPN = GSUT_BBA(this,@L_ArrParam)
              endif
              * --- Aggiorna array - Sfrutta posizione 2 per risultato
              DMARRFIL(this.w_CONTA,2) = this.w_TMPN
              * --- Conta ...
              this.w_CONTA = this.w_CONTA+1
            enddo
            * --- Lancia l'archiviazione silente
            if this.w_CONTA>1
              this.w_oMESS=createobject("AH_Message")
              this.w_oMESS.icon = 64
              this.w_oPART = this.w_oMESS.AddMsgPartnl("Resoconto archiviazione:")
              this.w_TMPN = 1
              do while this.w_TMPN < this.w_CONTA
                this.w_TMPN1 = DMARRFIL(this.w_TMPN,2)
                if this.w_TMPN1=0
                  this.w_oPART = this.w_oMESS.AddMsgPartnl("[%1] File: %2 (OK)")
                  this.w_oPART.AddParam(PADL(ALLTRIM(STR(this.w_TMPN,3,0)),3,"0"))     
                  this.w_oPART.AddParam(alltrim(DMARRFIL(this.w_TMPN,1)))     
                else
                  if this.w_TMPN1<0
                    this.w_oPART = this.w_oMESS.AddMsgPartnl("[%1] File: %2 (file non trovato)")
                    this.w_oPART.AddParam(PADL(ALLTRIM(STR(this.w_TMPN,3,0)),3,"0"))     
                    this.w_oPART.AddParam(alltrim(DMARRFIL(this.w_TMPN,1)))     
                  else
                    this.w_oPART = this.w_oMESS.AddMsgPartnl("[%1] File: %2 (errore %3)")
                    this.w_oPART.AddParam(PADL(ALLTRIM(STR(this.w_TMPN,3,0)),3,"0"))     
                    this.w_oPART.AddParam(alltrim(DMARRFIL(this.w_TMPN,1)))     
                    this.w_oPART.AddParam(str(this.w_TMPN1,3,0))     
                  endif
                endif
                this.w_TMPN = this.w_TMPN+1
              enddo
              this.w_oMESS.AH_ErrorMsg(64)     
            endif
          else
            * --- Archiviazione standard singola ...
            if this.w_MODALLEG="S" and this.w_Operazione="S" and not FILE(this.w_CATCH)
              i_retcode = 'stop'
              return
            endif
            if FILE(this.w_CATCH) OR g_DMIP="S" OR this.w_MODALLEG="S"
              if this.w_Operazione="B" or this.w_MODALLEG="S"
                this.w_NomeFile = this.w_CATCH
                this.w_NomeRep = iif(this.w_Operazione="B", "GSUT_BCV.PDF", this.w_CATCH)
                this.w_INCLASSEDOC = this.w_LICLADOC
                this.w_RETVAL = GSUT_BPI(this,4)
              else
                Private L_ArrParam
                dimension L_ArrParam(30)
                L_ArrParam(1)="AR"
                L_ArrParam(2)=this.w_CATCH
                L_ArrParam(3)=this.w_LICLADOC
                L_ArrParam(4)=i_CODUTE
                L_ArrParam(5)=this.oParentObject
                L_ArrParam(6)=this.w_ARCHIVIO
                L_ArrParam(7)=this.w_CHIAVE
                L_ArrParam(8)=this.w_DESBRE
                L_ArrParam(9)=this.w_DESLUN
                L_ArrParam(10)=.F.
                L_ArrParam(11)=.F.
                L_ArrParam(12)=" "
                L_ArrParam(13)=" "
                L_ArrParam(14)=this.oParentObject
                L_ArrParam(15)=this.w_TIPOALLE
                L_ArrParam(16)=this.w_CLASSEALL
                L_ArrParam(17)=" "
                L_ArrParam(18)=this.w_QUEMOD
                L_ArrParam(19)=this.w_OPERAZIONE
                L_ArrParam(23)=(NVL(this.w_CDPUBWEB, "N")="S")
                GSUT_BBA(this,@L_ArrParam)
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            endif
          endif
        endif
        * --- Aggiorno l'icona degli allegati sul form
        if type("this.oParentObject.BaseClass")="C" and lower(this.oParentObject.BaseClass) = "form"
          this.OparentObject.LoadAttach()
        endif
        if this.w_OPERAZIONE="B" AND FILE(this.w_CATCH)
          DELETE FILE (this.w_CATCH)
        endif
      case this.w_OPERAZIONE = "V"
        * --- Lancia la maschera di visualizzazione elenco file
        if g_DMIP="S"
          this.w_Archivio = IIF(this.w_Archivio="GBGBGB", space(20), this.w_Archivio)
          this.w_Chiave = IIF(this.w_CHIAVE="GBGBGB", space(100), this.w_CHIAVE)
          if EMPTY(this.w_Archivio)
            dimension L_ARRAYATTRIBUTI(1, 3)
            INFINITYOPEN (this,"V", 1, "")
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            dimension L_ARRAYATTRIBUTI(2, 3)
            this.w_NUMATTR = 1
            * --- Select from CHKALLEG
            do vq_exec with 'CHKALLEG',this,'_Curs_CHKALLEG','',.f.,.t.
            if used('_Curs_CHKALLEG')
              select _Curs_CHKALLEG
              locate for 1=1
              do while not(eof())
              if g_REVI="S"
                this.w_ATTRIB_VAL = InfinityAttribValue( _Curs_CHKALLEG.CDCODATT , this.w_Chiave)
                if not Empty(this.w_ATTRIB_VAL)
                  L_ARRAYATTRIBUTI(this.w_NUMATTR, 1) = _Curs_CHKALLEG.CDCODATT
                  L_ARRAYATTRIBUTI(this.w_NUMATTR, 2) = 1
                  L_ARRAYATTRIBUTI(this.w_NUMATTR, 3) = this.w_ATTRIB_VAL
                  this.w_NUMATTR = this.w_NUMATTR+1
                  dimension L_ARRAYATTRIBUTI(this.w_NUMATTR, 3)
                endif
              else
                L_ARRAYATTRIBUTI(1, 1) = _Curs_CHKALLEG.CDCODATT
                L_ARRAYATTRIBUTI(1, 2) = 1
                L_ARRAYATTRIBUTI(1, 3) = this.w_Chiave
                this.w_NUMATTR = 2
              endif
                select _Curs_CHKALLEG
                continue
              enddo
              use
            endif
            INFINITYOPEN (this,"V", this.w_NUMATTR, "")
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        else
          this.w_Archivio = IIF(this.w_Archivio="GBGBGB", space(20), this.w_Archivio)
          this.w_Chiave = IIF(this.w_CHIAVE="GBGBGB", space(100), this.w_CHIAVE)
          this.w_FILE = space(20)
          this.w_Periodo = cp_CharToDate("  -  -  ")
          * --- Codice Classe Documentale
          * --- Riferimento alla maschera Ricerca Documenti Archiviati
          * --- Riferimento allo zoom della maschera Ricerca Documenti Archiviati
          * --- Codice attributo
          * --- Campo cursore con valore attributo
          * --- Valore campo cursore attributo
          this.w_CODUTE = i_CODUTE
          this.w_PVPERVIS = 1
          this.w_PVSELUTE = "N"
          * --- Lancio maschera Ricerca Documenti Archiviati
          if this.pPrat 
            if upper(this.w_ARCHIVIO)="CAN_TIER" or empty(this.w_ARCHIVIO)
              this.w_GSUT_KFG = GSUT_KFG("AET")
            else
              AH_ERRORMSG("Gestione attiva non compatibile con la funzionalit� selezionata" ,48,"")
              i_retcode = 'stop'
              return
            endif
          else
            this.w_GSUT_KFG = GSUT_KFG()
          endif
          if vartype(this.w_GSUT_KFG) # "O"
            * --- Si � cercato di aprire la maschera quando ne esiste gi� un'istanza attiva. Esco
            i_retcode = 'stop'
            return
          endif
          if !(this.w_GSUT_KFG.bSec1)
            this.w_GSUT_KFG.Notifyevent("Done")     
            this.w_GSUT_KFG = .null.
            i_retcode = 'stop'
            return
          endif
          this.w_ZOOMSELE = this.w_GSUT_KFG.GSUT_MA1
          this.w_MODALL = "T"
          this.w_GSUT_KFG.w_MODALL = "T"
          if this.pPrat
            * --- Lettura Classe Documentale di riferimento per l'archivio pratiche
            * --- Read from PROMCLAS
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PROMCLAS_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CDCODCLA"+;
                " from "+i_cTable+" PROMCLAS where ";
                    +"CDCLAPRA = "+cp_ToStrODBC("S");
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CDCODCLA;
                from (i_cTable) where;
                    CDCLAPRA = "S";
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_SELCODCLA = NVL(cp_ToDate(_read_.CDCODCLA),cp_NullValue(_read_.CDCODCLA))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          else
            * --- Lettura Classe Documentale 'Standard'
            if Not Empty(this.w_ARCHIVIO)
              * --- Read from PROMCLAS
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.PROMCLAS_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "CDCODCLA,CDMODALL"+;
                  " from "+i_cTable+" PROMCLAS where ";
                      +"CDRIFTAB = "+cp_ToStrODBC(this.w_ARCHIVIO);
                      +" and CDRIFDEF = "+cp_ToStrODBC("S");
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  CDCODCLA,CDMODALL;
                  from (i_cTable) where;
                      CDRIFTAB = this.w_ARCHIVIO;
                      and CDRIFDEF = "S";
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_SELCODCLA = NVL(cp_ToDate(_read_.CDCODCLA),cp_NullValue(_read_.CDCODCLA))
                this.w_MODALL = NVL(cp_ToDate(_read_.CDMODALL),cp_NullValue(_read_.CDMODALL))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if i_ROWS<>0
                this.w_GSUT_KFG.w_MODALL = this.w_MODALL
              endif
            endif
          endif
          this.w_GSUT_KFG.w_CMODALL = this.w_MODALL
          * --- Valorizzazione Classe Documentale
          this.w_OBJCTRL = this.w_GSUT_KFG.GetCtrl("w_SELCODCLA")
          this.w_OSOURCE.xKey( 1 ) = this.w_SELCODCLA
          * --- Disabilitazione post-In
          this.w_OLDPOSTIN = i_ServerConn[1,7]
          i_ServerConn[1,7]=.F.
          this.w_OBJCTRL.ecpDrop(this.w_OSOURCE)     
          * --- Abilitazione post-In
          i_ServerConn[1,7]= this.w_OLDPOSTIN 
          this.w_GSUT_KFG.ClasseMod = .F.
          * --- Verifico presenza 'Preferenze di ricerca' per l'utente in uso
          if ! empty(this.w_Archivio) 
            * --- Valorizzazione Archivio
            this.w_GSUT_KFG.w_ARCHIVIO = this.w_Archivio
          endif
          * --- Valorizzazione Chiave
          this.w_GSUT_KFG.w_Chiave = this.w_Chiave
          this.w_GSUT_KFG.mcalc(.t.)     
          this.w_OBJCTRL = .null.
          this.w_OKRIC = !Isalt()
          if ! empty(this.w_CHIAVE) and Isalt() and Upper(this.w_ARCHIVIO)="CAN_TIER"
            this.w_OBJCTRL = this.w_GSUT_KFG.GetCtrl("w_CODPRAT")
            this.w_OBJCTRL.value = this.w_chiave
            * --- Avendo gi� valorizzata w_SELCODCLA, con la SetValueLinked scatta gi� in GSUT_KFG la chiamata a GSUT_BES
             
 SETVALUELINKED("M", this.w_GSUT_KFG, "w_CODPRAT",this.w_chiave)
            this.w_OBJCTRL.bUpd = .T.
            this.w_OBJCTRL = .null.
            this.w_OKRIC = .t.
          else
            if ! this.pPrat AND NOT EMPTY(NVL(this.w_SELCODCLA,""))
              * --- Select from PRODCLAS
              i_nConn=i_TableProp[this.PRODCLAS_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PRODCLAS_idx,2],.t.,this.PRODCLAS_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select CDCODATT,CDCAMCUR  from "+i_cTable+" PRODCLAS ";
                    +" where CDCODCLA="+cp_ToStrODBC(this.w_SELCODCLA)+"";
                     ,"_Curs_PRODCLAS")
              else
                select CDCODATT,CDCAMCUR from (i_cTable);
                 where CDCODCLA=this.w_SELCODCLA;
                  into cursor _Curs_PRODCLAS
              endif
              if used('_Curs_PRODCLAS')
                select _Curs_PRODCLAS
                locate for 1=1
                do while not(eof())
                this.w_ATTRIBUTO = ""
                * --- Valorizzazione codice attributo
                this.w_CODATT = _Curs_PRODCLAS.CDCODATT
                * --- Valorizzazione valore campo cursore attributo
                this.w_CAMCUR = _Curs_PRODCLAS.CDCAMCUR
                * --- Scansione della stringa associata all'attributo
                do while len(this.w_CAMCUR) > 0
                  do case
                    case at("+",this.w_CAMCUR) = 0
                      if len(this.w_ATTRIBUTO) = 0
                        this.w_ATTRIBUTO = "this.oParentObject.w_"+alltrim(this.w_CAMCUR)
                      else
                        this.w_ATTRIBUTO = this.w_ATTRIBUTO + " + this.oParentObject.w_"+alltrim(this.w_CAMCUR)
                      endif
                      * --- Ho scansionato tutta la stringa
                      this.w_CAMCUR = ""
                      exit
                    case at("+",this.w_CAMCUR) > 0
                      if len(this.w_ATTRIBUTO) = 0
                        this.w_ATTRIBUTO = "this.oParentObject.w_"+alltrim(substr(this.w_CAMCUR,1,at("+",this.w_CAMCUR)-1))
                      else
                        this.w_ATTRIBUTO = this.w_ATTRIBUTO+"+ this.oParentObject.w_"+alltrim(substr(this.w_CAMCUR,1,at("+",this.w_CAMCUR)-1))
                      endif
                      this.w_CAMCUR = substr(this.w_CAMCUR,at("+",this.w_CAMCUR)+1)
                  endcase
                enddo
                a = 0
                * --- Valorizzazione campo cursore con valore attributo
                on error a=1
                this.w_RIFCAMPO = EVAL(this.w_ATTRIBUTO)
                on error 
                if a = 1
                  this.w_RIFCAMPO = ""
                endif
                this.w_TIPOATTR = vartype(this.w_RIFCAMPO)
                do case
                  case this.w_TIPOATTR = "D" 
                    this.w_VALCAM = DTOC(this.w_RIFCAMPO)
                  case this.w_TIPOATTR = "N" 
                    this.w_VALCAM = alltrim(STR(this.w_RIFCAMPO))
                  otherwise
                    this.w_VALCAM = this.w_RIFCAMPO
                endcase
                * --- Valorizzazione campi dello zoom della maschera Ricerca Documenti Archiviati
                if Upper(this.w_CODATT)<>"DESCRIZIONE" or !Isalt()
                  this.w_ZOOMSELE.MarkPos()     
                  this.w_ZOOMSELE.FirstRow()     
                  this.w_NUMREC = this.w_ZOOMSELE.SEARCH("t_IDCODATT="+cp_ToStrOdbc(this.w_CODATT))
                  if this.w_NUMREC<>-1
                    this.w_ZOOMSELE.SetRow(this.w_NUMREC)     
                    this.w_ZOOMSELE.w_IDVALATT = this.w_VALCAM
                    this.w_ZOOMSELE.SaveRow()     
                  endif
                  this.w_ZOOMSELE.RePos()     
                endif
                  select _Curs_PRODCLAS
                  continue
                enddo
                use
              endif
            endif
            if this.w_OKRIC
              GSUT_BES(this.w_GSUT_KFG,this.w_GSUT_KFG.w_ARCHIVIO,this.w_GSUT_KFG.w_CHIAVE, iif(EMPTY(NVL(this.w_SELCODCLA,"")) AND EMPTY(NVL(this.w_ARCHIVIO,"")) , "INIZIA", "QUERY2"))
            endif
          endif
        endif
      case this.w_OPERAZIONE = "D"
        * --- Legge, se c'�, la classe di archiviazione standard solo (tipologia copia file)
        * --- Read from PROMCLAS
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PROMCLAS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CDCODCLA,CDMODALL,CDTIPARC,CDCLAALL,CDCOMTIP,CDCOMCLA,CDPUBWEB"+;
            " from "+i_cTable+" PROMCLAS where ";
                +"CDRIFTAB = "+cp_ToStrODBC(this.w_ARCHIVIO);
                +" and CDRIFDEF = "+cp_ToStrODBC("S");
                +" and CDMODALL = "+cp_ToStrODBC("F");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CDCODCLA,CDMODALL,CDTIPARC,CDCLAALL,CDCOMTIP,CDCOMCLA,CDPUBWEB;
            from (i_cTable) where;
                CDRIFTAB = this.w_ARCHIVIO;
                and CDRIFDEF = "S";
                and CDMODALL = "F";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_LICLADOC = NVL(cp_ToDate(_read_.CDCODCLA),cp_NullValue(_read_.CDCODCLA))
          this.w_MODALLEG = NVL(cp_ToDate(_read_.CDMODALL),cp_NullValue(_read_.CDMODALL))
          this.w_TIPOARCH = NVL(cp_ToDate(_read_.CDTIPARC),cp_NullValue(_read_.CDTIPARC))
          this.w_CLAALLE = NVL(cp_ToDate(_read_.CDCLAALL),cp_NullValue(_read_.CDCLAALL))
          this.w_TIPPATH = NVL(cp_ToDate(_read_.CDCOMTIP),cp_NullValue(_read_.CDCOMTIP))
          this.w_TIPCLA = NVL(cp_ToDate(_read_.CDCOMCLA),cp_NullValue(_read_.CDCOMCLA))
          this.w_CDPUBWEB = NVL(cp_ToDate(_read_.CDPUBWEB),cp_NullValue(_read_.CDPUBWEB))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if empty(this.w_LICLADOC)
          * --- Verifica se c'� almeno una classe
          * --- Read from PROMCLAS
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PROMCLAS_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CDCODCLA"+;
              " from "+i_cTable+" PROMCLAS where ";
                  +"CDRIFTAB = "+cp_ToStrODBC(this.w_ARCHIVIO);
                  +" and CDMODALL = "+cp_ToStrODBC("F");
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CDCODCLA;
              from (i_cTable) where;
                  CDRIFTAB = this.w_ARCHIVIO;
                  and CDMODALL = "F";
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_LICLADOC = NVL(cp_ToDate(_read_.CDCODCLA),cp_NullValue(_read_.CDCODCLA))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if empty(this.w_LICLADOC)
            if i_rows=0
              AH_ERRORMSG("Non sono state definite classi documentali di tipologia <copia file> per la tabella %1",48,"",alltrim(this.w_ARCHIVIO))
              i_retcode = 'stop'
              return
            endif
          endif
        endif
        * --- Prepariamo le variabili per eventuali filtri sui modelli documenti
        if IsAlt()
          if VARTYPE(this.oParentObject.w_CN__ENTE)="C"
            this.w_CN__ENTE = this.oParentObject.w_CN__ENTE
          endif
          if VARTYPE(this.oParentObject.w_CNTIPPRA)="C"
            this.w_CNTIPPRA = this.oParentObject.w_CNTIPPRA
          endif
          if VARTYPE(this.oParentObject.w_CNTIPPRA)="C"
            this.w_CNMATPRA = this.oParentObject.w_CNMATPRA
          endif
        endif
        * --- Lancia la maschera Modelli Documenti
        do GSUT_KMD with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_LICLADOC = this.w_CLAMOD
        this.w_PATARC = addbs(alltrim(this.w_PATARC))+alltrim(this.w_FILEORIG)
        this.w_CATCH = SUBSTR(ALLTRIM(this.w_PATMOD),1,250)
        if not empty(this.w_CATCH)
          * --- Se da cartella contenitore (utilizabile solo se non si archivia da scanner) archivia tutti i files presenti nella cartella ...
          * --- Archiviazione standard singola ...
          if FILE(this.w_CATCH)
            Private L_ArrParam
            dimension L_ArrParam(30)
            L_ArrParam(1)="AR"
            L_ArrParam(2)=this.w_CATCH
            L_ArrParam(3)=this.w_LICLADOC
            L_ArrParam(4)=i_CODUTE
            L_ArrParam(5)=this.oParentObject
            L_ArrParam(6)=this.w_ARCHIVIO
            L_ArrParam(7)=this.w_CHIAVE
            L_ArrParam(8)=this.w_OGGETTO
            L_ArrParam(9)=" "
            L_ArrParam(10)=.F.
            L_ArrParam(11)=.F.
            L_ArrParam(12)=this.w_OPERAZIONE
            L_ArrParam(13)=" "
            L_ArrParam(14)=this.oParentObject
            L_ArrParam(15)=this.w_TIPOALLE
            L_ArrParam(16)=this.w_CLASSEALL
            L_ArrParam(17)=" "
            L_ArrParam(18)=this.w_QUEMOD
            L_ArrParam(19)=this.w_OPERAZIONE
            L_ArrParam(22)=this.w_PATARC
            L_ArrParam(23)=(NVL(this.w_CDPUBWEB, "N")="S")
            GSUT_BBA(this,@L_ArrParam)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
        * --- Aggiorno l'icona degli allegati sul form
        if type("this.oParentObject.BaseClass")="C" and lower(this.oParentObject.BaseClass) = "form"
          this.OparentObject.LoadAttach()
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Cerca una classe documentale standard
    this.w_FOUND = 0
    * --- Select from PROMCLAS
    i_nConn=i_TableProp[this.PROMCLAS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select CDCODCLA,CDMODALL,CDTIPARC,CDCLAALL,CDCOMTIP,CDCOMCLA,CDPUBWEB,CDCLAINF  from "+i_cTable+" PROMCLAS ";
          +" where CDRIFTAB="+cp_ToStrODBC(this.w_ARCHIVIO)+" AND CDRIFDEF='S'";
           ,"_Curs_PROMCLAS")
    else
      select CDCODCLA,CDMODALL,CDTIPARC,CDCLAALL,CDCOMTIP,CDCOMCLA,CDPUBWEB,CDCLAINF from (i_cTable);
       where CDRIFTAB=this.w_ARCHIVIO AND CDRIFDEF="S";
        into cursor _Curs_PROMCLAS
    endif
    if used('_Curs_PROMCLAS')
      select _Curs_PROMCLAS
      locate for 1=1
      do while not(eof())
      this.w_CDCLAINF = _Curs_PROMCLAS.CDCLAINF
      if g_REVI<>"S" OR NOT EMPTY(NVL(this.w_CDCLAINF,""))
        this.w_LICLADOC = _Curs_PROMCLAS.CDCODCLA
        this.w_MODALLEG = _Curs_PROMCLAS.CDMODALL
        this.w_TIPOARCH = _Curs_PROMCLAS.CDTIPARC
        this.w_CLAALLE = _Curs_PROMCLAS.CDCLAALL
        this.w_TIPPATH = _Curs_PROMCLAS.CDCOMTIP
        this.w_TIPCLA = _Curs_PROMCLAS.CDCOMCLA
        this.w_CDPUBWEB = _Curs_PROMCLAS.CDPUBWEB
        this.w_FOUND = this.w_FOUND+1
      endif
      if this.w_FOUND>1
        AH_ERRORMSG("Attenzione: � stata trovata pi� di una classe documentale con tipologia 'Archiviazione rapida' impostata al valore 'Standard'")
      endif
        select _Curs_PROMCLAS
        continue
      enddo
      use
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,w_Archivio,w_Chiave,w_Programma,w_Operazione,pRow,pCol,pForm,pTBClass,pPrat,pCDPUBWEB)
    this.w_Archivio=w_Archivio
    this.w_Chiave=w_Chiave
    this.w_Programma=w_Programma
    this.w_Operazione=w_Operazione
    this.pRow=pRow
    this.pCol=pCol
    this.pForm=pForm
    this.pTBClass=pTBClass
    this.pPrat=pPrat
    this.pCDPUBWEB=pCDPUBWEB
    this.w_OSOURCE=createobject("_Osource")
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,12)]
    this.cWorkTables[1]='LIB_IMMA'
    this.cWorkTables[2]='PROMCLAS'
    this.cWorkTables[3]='PRODCLAS'
    this.cWorkTables[4]='PRO_PERM'
    this.cWorkTables[5]='PROMINDI'
    this.cWorkTables[6]='PRODINDI'
    this.cWorkTables[7]='DMPARAM'
    this.cWorkTables[8]='CPUSRGRP'
    this.cWorkTables[9]='PRO_PVIS'
    this.cWorkTables[10]='DIPENDEN'
    this.cWorkTables[11]='CONTROPA'
    this.cWorkTables[12]='ASSCAUDO'
    return(this.OpenAllTables(12))

  proc CloseCursors()
    if used('_Curs_CHKALLEG')
      use in _Curs_CHKALLEG
    endif
    if used('_Curs_PRODCLAS')
      use in _Curs_PRODCLAS
    endif
    if used('_Curs_PROMCLAS')
      use in _Curs_PROMCLAS
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- gsut_bcv
  * --- Per gestione sicurezza procedure con parametri
  func getSecuritycode()
    return(lower('GSUT_BCV,.Null.' + ','+ ;
      iif(type("this.w_Archivio")<>'C','.null.', this.w_Archivio ) + ','+ ;
      iif(type("this.w_Chiave")<>'C','.null.', this.w_Chiave ) + ','+ ;
      iif(type("this.w_Programma")<>'C','.null.', this.w_Programma ) + ','+ ;
      iif(type("this.w_Operazione")<>'C','.null.', this.w_Operazione )  )) 
  
  Procedure GESTERR()
     parameters Obj
     wait window 'Campo non presente: '+  alltrim(Obj.w_ATTRIBUTO)
     Obj.w_RIFCAMPO=''    
  Endproc
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_Archivio,w_Chiave,w_Programma,w_Operazione,pRow,pCol,pForm,pTBClass,pPrat,pCDPUBWEB"
endproc
