* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_kdi                                                        *
*              Caricamento rapido lettere di intento                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-09-02                                                      *
* Last revis.: 2017-11-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_kdi",oParentObject))

* --- Class definition
define class tgscg_kdi as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 678
  Height = 380+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2017-11-15"
  HelpContextID=82453865
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=41

  * --- Constant Properties
  _IDX = 0
  VOCIIVA_IDX = 0
  CONTI_IDX = 0
  DIC_INTE_IDX = 0
  cPrg = "gscg_kdi"
  cComment = "Caricamento rapido lettere di intento"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TIPCON = space(1)
  w_SERIAL = space(15)
  o_SERIAL = space(15)
  w_TIPIVA = space(1)
  w_DOGANA = space(30)
  w_UFFIVA = space(25)
  w_UFIVFO = space(25)
  w_NUMDIC = 0
  w_ANNO = space(4)
  w_DATDIC = ctod('  /  /  ')
  o_DATDIC = ctod('  /  /  ')
  w_FLSTAM = space(1)
  w_CODIVA = space(5)
  w_DESIVA = space(35)
  w_ARTESE = space(5)
  w_DESOPE = space(0)
  w_TIPOPE = space(1)
  o_TIPOPE = space(1)
  w_IMPDIC = 0
  w_IMPUTI = 0
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_NUMDOC = 0
  w_DATLET = ctod('  /  /  ')
  w_CODCON = space(15)
  w_AGGDOG = space(1)
  o_AGGDOG = space(1)
  w_CODINI = space(15)
  o_CODINI = space(15)
  w_CODFIN = space(15)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_DESCRI = space(35)
  w_DESCR1 = space(35)
  w_ANNO1 = space(4)
  w_DATA = ctod('  /  /  ')
  w_CODICE = space(15)
  w_DESCLI = space(40)
  w_CODDOG = space(30)
  w_SELEZI = space(1)
  o_SELEZI = space(1)
  w_SERDIC = space(3)
  w_SERDOC = space(3)
  w_AUTON1 = space(7)
  w_AUTON2 = space(7)
  w_SERDIC = space(3)
  w_TIPOCF = space(1)
  w_ZOOMFOR = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_kdiPag1","gscg_kdi",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni principali")
      .Pages(2).addobject("oPag","tgscg_kdiPag2","gscg_kdi",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni aggiuntive")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSERIAL_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOMFOR = this.oPgFrm.Pages(2).oPag.ZOOMFOR
    DoDefault()
    proc Destroy()
      this.w_ZOOMFOR = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='VOCIIVA'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='DIC_INTE'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPCON=space(1)
      .w_SERIAL=space(15)
      .w_TIPIVA=space(1)
      .w_DOGANA=space(30)
      .w_UFFIVA=space(25)
      .w_UFIVFO=space(25)
      .w_NUMDIC=0
      .w_ANNO=space(4)
      .w_DATDIC=ctod("  /  /  ")
      .w_FLSTAM=space(1)
      .w_CODIVA=space(5)
      .w_DESIVA=space(35)
      .w_ARTESE=space(5)
      .w_DESOPE=space(0)
      .w_TIPOPE=space(1)
      .w_IMPDIC=0
      .w_IMPUTI=0
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_NUMDOC=0
      .w_DATLET=ctod("  /  /  ")
      .w_CODCON=space(15)
      .w_AGGDOG=space(1)
      .w_CODINI=space(15)
      .w_CODFIN=space(15)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_DESCRI=space(35)
      .w_DESCR1=space(35)
      .w_ANNO1=space(4)
      .w_DATA=ctod("  /  /  ")
      .w_CODICE=space(15)
      .w_DESCLI=space(40)
      .w_CODDOG=space(30)
      .w_SELEZI=space(1)
      .w_SERDIC=space(3)
      .w_SERDOC=space(3)
      .w_AUTON1=space(7)
      .w_AUTON2=space(7)
      .w_SERDIC=space(3)
      .w_TIPOCF=space(1)
        .w_TIPCON = 'F'
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_SERIAL))
          .link_1_3('Full')
        endif
          .DoRTCalc(3,7,.f.)
        .w_ANNO = STR(YEAR(i_DATSYS),4,0)
        .w_DATDIC = cp_CharToDate('  -  -    ')
        .DoRTCalc(10,11,.f.)
        if not(empty(.w_CODIVA))
          .link_1_20('Full')
        endif
          .DoRTCalc(12,15,.f.)
        .w_IMPDIC = IIF(.w_TIPOPE='D',0,.w_IMPDIC)
        .w_IMPUTI = 0
          .DoRTCalc(18,20,.f.)
        .w_DATLET = .w_DATDIC
          .DoRTCalc(22,22,.f.)
        .w_AGGDOG = IIF(not EMPTY(.w_CODICE),' ',.w_AGGDOG)
        .DoRTCalc(24,24,.f.)
        if not(empty(.w_CODINI))
          .link_2_2('Full')
        endif
        .w_CODFIN = .w_CODINI
        .DoRTCalc(25,25,.f.)
        if not(empty(.w_CODFIN))
          .link_2_3('Full')
        endif
        .w_OBTEST = i_DATSYS
        .DoRTCalc(27,32,.f.)
        if not(empty(.w_CODICE))
          .link_1_46('Full')
        endif
          .DoRTCalc(33,33,.f.)
        .w_CODDOG = IIF(not EMPTY(.w_CODICE),SPACE(30),.w_CODDOG)
      .oPgFrm.Page2.oPag.ZOOMFOR.Calculate()
        .w_SELEZI = 'S'
          .DoRTCalc(36,40,.f.)
        .w_TIPOCF = 'F'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_48.enabled = this.oPgFrm.Page1.oPag.oBtn_1_48.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_49.enabled = this.oPgFrm.Page1.oPag.oBtn_1_49.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_13.enabled = this.oPgFrm.Page2.oPag.oBtn_2_13.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_TIPCON = 'F'
        .DoRTCalc(2,8,.t.)
        if .o_SERIAL<>.w_SERIAL
            .w_DATDIC = cp_CharToDate('  -  -    ')
        endif
        .DoRTCalc(10,15,.t.)
        if .o_TIPOPE<>.w_TIPOPE
            .w_IMPDIC = IIF(.w_TIPOPE='D',0,.w_IMPDIC)
        endif
        .DoRTCalc(17,20,.t.)
        if .o_DATDIC<>.w_DATDIC
            .w_DATLET = .w_DATDIC
        endif
        .DoRTCalc(22,22,.t.)
        if .o_SERIAL<>.w_SERIAL
            .w_AGGDOG = IIF(not EMPTY(.w_CODICE),' ',.w_AGGDOG)
        endif
        .DoRTCalc(24,24,.t.)
        if .o_CODINI<>.w_CODINI
            .w_CODFIN = .w_CODINI
          .link_2_3('Full')
        endif
            .w_OBTEST = i_DATSYS
        .DoRTCalc(27,31,.t.)
          .link_1_46('Full')
        .DoRTCalc(33,33,.t.)
        if .o_SERIAL<>.w_SERIAL
            .w_CODDOG = IIF(not EMPTY(.w_CODICE),SPACE(30),.w_CODDOG)
        endif
        .oPgFrm.Page2.oPag.ZOOMFOR.Calculate()
        if .o_SELEZI<>.w_SELEZI
          .Calculate_BMNVBKOOUV()
        endif
        if .o_AGGDOG<>.w_AGGDOG
          .Calculate_UZLUHYRMCT()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(35,41,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page2.oPag.ZOOMFOR.Calculate()
    endwith
  return

  proc Calculate_BXQLHRXBGI()
    with this
          * --- Seleziona zoom
          Seleziona(this;
             )
    endwith
  endproc
  proc Calculate_BMNVBKOOUV()
    with this
          * --- Seleziona zoom
          Seleziona(this;
              ,.w_SELEZI;
             )
    endwith
  endproc
  proc Calculate_UZLUHYRMCT()
    with this
          * --- Seleziona zoom
          .w_SELEZI = iif(.w_AGGDOG='S','D',.w_SELEZI)
          RequeryZoomInt(this;
             )
          Seleziona(this;
              ,.w_SELEZI;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDOGANA_1_7.enabled = this.oPgFrm.Page1.oPag.oDOGANA_1_7.mCond()
    this.oPgFrm.Page1.oPag.oIMPDIC_1_29.enabled = this.oPgFrm.Page1.oPag.oIMPDIC_1_29.mCond()
    this.oPgFrm.Page2.oPag.oAGGDOG_2_1.enabled = this.oPgFrm.Page2.oPag.oAGGDOG_2_1.mCond()
    this.oPgFrm.Page2.oPag.oCODINI_2_2.enabled = this.oPgFrm.Page2.oPag.oCODINI_2_2.mCond()
    this.oPgFrm.Page2.oPag.oCODFIN_2_3.enabled = this.oPgFrm.Page2.oPag.oCODFIN_2_3.mCond()
    this.oPgFrm.Page2.oPag.oCODDOG_2_10.enabled = this.oPgFrm.Page2.oPag.oCODDOG_2_10.mCond()
    this.oPgFrm.Page2.oPag.oSELEZI_2_14.enabled_(this.oPgFrm.Page2.oPag.oSELEZI_2_14.mCond())
    this.oPgFrm.Page1.oPag.oBtn_1_48.enabled = this.oPgFrm.Page1.oPag.oBtn_1_48.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_13.enabled = this.oPgFrm.Page2.oPag.oBtn_2_13.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oUFIVFO_1_9.visible=!this.oPgFrm.Page1.oPag.oUFIVFO_1_9.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_41.visible=!this.oPgFrm.Page1.oPag.oStr_1_41.mHide()
    this.oPgFrm.Page2.oPag.oCODDOG_2_10.visible=!this.oPgFrm.Page2.oPag.oCODDOG_2_10.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_11.visible=!this.oPgFrm.Page2.oPag.oStr_2_11.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page2.oPag.ZOOMFOR.Event(cEvent)
        if lower(cEvent)==lower("Init") or lower(cEvent)==lower("Requery")
          .Calculate_BXQLHRXBGI()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SERIAL
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIC_INTE_IDX,3]
    i_lTable = "DIC_INTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIC_INTE_IDX,2], .t., this.DIC_INTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIC_INTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SERIAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DIC_INTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DISERIAL like "+cp_ToStrODBC(trim(this.w_SERIAL)+"%");

          i_ret=cp_SQL(i_nConn,"select DISERIAL,DITIPCON,DICODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DISERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DISERIAL',trim(this.w_SERIAL))
          select DISERIAL,DITIPCON,DICODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DISERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SERIAL)==trim(_Link_.DISERIAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SERIAL) and !this.bDontReportError
            deferred_cp_zoom('DIC_INTE','*','DISERIAL',cp_AbsName(oSource.parent,'oSERIAL_1_3'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DISERIAL,DITIPCON,DICODICE";
                     +" from "+i_cTable+" "+i_lTable+" where DISERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DISERIAL',oSource.xKey(1))
            select DISERIAL,DITIPCON,DICODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SERIAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DISERIAL,DITIPCON,DICODICE";
                   +" from "+i_cTable+" "+i_lTable+" where DISERIAL="+cp_ToStrODBC(this.w_SERIAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DISERIAL',this.w_SERIAL)
            select DISERIAL,DITIPCON,DICODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SERIAL = NVL(_Link_.DISERIAL,space(15))
      this.w_TIPOCF = NVL(_Link_.DITIPCON,space(1))
      this.w_CODICE = NVL(_Link_.DICODICE,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_SERIAL = space(15)
      endif
      this.w_TIPOCF = space(1)
      this.w_CODICE = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIC_INTE_IDX,2])+'\'+cp_ToStr(_Link_.DISERIAL,1)
      cp_ShowWarn(i_cKey,this.DIC_INTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SERIAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODIVA
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_CODIVA)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_CODIVA))
          select IVCODIVA,IVDESIVA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODIVA)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODIVA) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oCODIVA_1_20'),i_cWhere,'GSAR_AIV',"Codici IVA",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDESIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_CODIVA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_CODIVA)
            select IVCODIVA,IVDESIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODIVA = NVL(_Link_.IVCODIVA,space(5))
      this.w_DESIVA = NVL(_Link_.IVDESIVA,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODIVA = space(5)
      endif
      this.w_DESIVA = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODINI
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFR',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CODINI))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODINI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODINI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODINI_2_2'),i_cWhere,'GSAR_AFR',"Elenco fornitori",'GSAR_SFR.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODINI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODINI)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODINI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRI = NVL(_Link_.ANDESCRI,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODINI = space(15)
      endif
      this.w_DESCRI = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        endif
        this.w_CODINI = space(15)
        this.w_DESCRI = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFIN
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFR',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CODFIN))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFIN)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODFIN) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODFIN_2_3'),i_cWhere,'GSAR_AFR',"Elenco fornitori",'GSAR_SFR.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODFIN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODFIN)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFIN = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCR1 = NVL(_Link_.ANDESCRI,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODFIN = space(15)
      endif
      this.w_DESCR1 = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_CODFIN>=.w_CODINI or empty(.w_CODINI)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
        endif
        this.w_CODFIN = space(15)
        this.w_DESCR1 = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODICE
  func Link_1_46(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODICE);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPOCF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPOCF;
                       ,'ANCODICE',this.w_CODICE)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODICE = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLI = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODICE = space(15)
      endif
      this.w_DESCLI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSERIAL_1_3.value==this.w_SERIAL)
      this.oPgFrm.Page1.oPag.oSERIAL_1_3.value=this.w_SERIAL
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPIVA_1_4.RadioValue()==this.w_TIPIVA)
      this.oPgFrm.Page1.oPag.oTIPIVA_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDOGANA_1_7.value==this.w_DOGANA)
      this.oPgFrm.Page1.oPag.oDOGANA_1_7.value=this.w_DOGANA
    endif
    if not(this.oPgFrm.Page1.oPag.oUFFIVA_1_8.value==this.w_UFFIVA)
      this.oPgFrm.Page1.oPag.oUFFIVA_1_8.value=this.w_UFFIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oUFIVFO_1_9.value==this.w_UFIVFO)
      this.oPgFrm.Page1.oPag.oUFIVFO_1_9.value=this.w_UFIVFO
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMDIC_1_11.value==this.w_NUMDIC)
      this.oPgFrm.Page1.oPag.oNUMDIC_1_11.value=this.w_NUMDIC
    endif
    if not(this.oPgFrm.Page1.oPag.oANNO_1_12.value==this.w_ANNO)
      this.oPgFrm.Page1.oPag.oANNO_1_12.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oDATDIC_1_15.value==this.w_DATDIC)
      this.oPgFrm.Page1.oPag.oDATDIC_1_15.value=this.w_DATDIC
    endif
    if not(this.oPgFrm.Page1.oPag.oFLSTAM_1_16.RadioValue()==this.w_FLSTAM)
      this.oPgFrm.Page1.oPag.oFLSTAM_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODIVA_1_20.value==this.w_CODIVA)
      this.oPgFrm.Page1.oPag.oCODIVA_1_20.value=this.w_CODIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESIVA_1_22.value==this.w_DESIVA)
      this.oPgFrm.Page1.oPag.oDESIVA_1_22.value=this.w_DESIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oARTESE_1_23.value==this.w_ARTESE)
      this.oPgFrm.Page1.oPag.oARTESE_1_23.value=this.w_ARTESE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESOPE_1_25.value==this.w_DESOPE)
      this.oPgFrm.Page1.oPag.oDESOPE_1_25.value=this.w_DESOPE
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOPE_1_27.RadioValue()==this.w_TIPOPE)
      this.oPgFrm.Page1.oPag.oTIPOPE_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPDIC_1_29.value==this.w_IMPDIC)
      this.oPgFrm.Page1.oPag.oIMPDIC_1_29.value=this.w_IMPDIC
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPUTI_1_31.value==this.w_IMPUTI)
      this.oPgFrm.Page1.oPag.oIMPUTI_1_31.value=this.w_IMPUTI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_33.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_33.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_35.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_35.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oAGGDOG_2_1.RadioValue()==this.w_AGGDOG)
      this.oPgFrm.Page2.oPag.oAGGDOG_2_1.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCODINI_2_2.value==this.w_CODINI)
      this.oPgFrm.Page2.oPag.oCODINI_2_2.value=this.w_CODINI
    endif
    if not(this.oPgFrm.Page2.oPag.oCODFIN_2_3.value==this.w_CODFIN)
      this.oPgFrm.Page2.oPag.oCODFIN_2_3.value=this.w_CODFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCRI_2_6.value==this.w_DESCRI)
      this.oPgFrm.Page2.oPag.oDESCRI_2_6.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCR1_2_7.value==this.w_DESCR1)
      this.oPgFrm.Page2.oPag.oDESCR1_2_7.value=this.w_DESCR1
    endif
    if not(this.oPgFrm.Page1.oPag.oANNO1_1_42.value==this.w_ANNO1)
      this.oPgFrm.Page1.oPag.oANNO1_1_42.value=this.w_ANNO1
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA_1_43.value==this.w_DATA)
      this.oPgFrm.Page1.oPag.oDATA_1_43.value=this.w_DATA
    endif
    if not(this.oPgFrm.Page1.oPag.oCODICE_1_46.value==this.w_CODICE)
      this.oPgFrm.Page1.oPag.oCODICE_1_46.value=this.w_CODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLI_1_47.value==this.w_DESCLI)
      this.oPgFrm.Page1.oPag.oDESCLI_1_47.value=this.w_DESCLI
    endif
    if not(this.oPgFrm.Page2.oPag.oCODDOG_2_10.value==this.w_CODDOG)
      this.oPgFrm.Page2.oPag.oCODDOG_2_10.value=this.w_CODDOG
    endif
    if not(this.oPgFrm.Page2.oPag.oSELEZI_2_14.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page2.oPag.oSELEZI_2_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSERDIC_1_50.value==this.w_SERDIC)
      this.oPgFrm.Page1.oPag.oSERDIC_1_50.value=this.w_SERDIC
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_NUMDIC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMDIC_1_11.SetFocus()
            i_bnoObbl = !empty(.w_NUMDIC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ANNO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANNO_1_12.SetFocus()
            i_bnoObbl = !empty(.w_ANNO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DATDIC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATDIC_1_15.SetFocus()
            i_bnoObbl = !empty(.w_DATDIC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_DATINI)) or not((.w_DATINI<=.w_DATFIN) or (empty(.w_DATFIN))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_33.SetFocus()
            i_bnoObbl = !empty(.w_DATINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data di inizio validit� � maggiore della data di fine validit�")
          case   ((empty(.w_DATFIN)) or not((.w_datini<=.w_datfin) or (empty(.w_datini))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_35.SetFocus()
            i_bnoObbl = !empty(.w_DATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data di inizio validit� � maggiore della data di fine validit�")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and (.w_AGGDOG<>'S')  and not(empty(.w_CODINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODINI_2_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure obsoleto")
          case   not((.w_CODFIN>=.w_CODINI or empty(.w_CODINI)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and (.w_AGGDOG<>'S')  and not(empty(.w_CODFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODFIN_2_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
          case   (empty(.w_CODDOG))  and not(.w_AGGDOG<>'S')  and (.w_AGGDOG='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODDOG_2_10.SetFocus()
            i_bnoObbl = !empty(.w_CODDOG)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_SERIAL = this.w_SERIAL
    this.o_DATDIC = this.w_DATDIC
    this.o_TIPOPE = this.w_TIPOPE
    this.o_AGGDOG = this.w_AGGDOG
    this.o_CODINI = this.w_CODINI
    this.o_SELEZI = this.w_SELEZI
    return

enddefine

* --- Define pages as container
define class tgscg_kdiPag1 as StdContainer
  Width  = 674
  height = 380
  stdWidth  = 674
  stdheight = 380
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSERIAL_1_3 as StdField with uid="EFVFVSEYMS",rtseq=2,rtrep=.f.,;
    cFormVar = "w_SERIAL", cQueryName = "SERIAL",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Lettera d'intento di riferimento",;
    HelpContextID = 192169254,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=133, Top=15, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="DIC_INTE", oKey_1_1="DISERIAL", oKey_1_2="this.w_SERIAL"

  func oSERIAL_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oSERIAL_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSERIAL_1_3.mZoom
      with this.Parent.oContained
        GSCG_BCL(this.Parent.oContained,"Z")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc


  add object oTIPIVA_1_4 as StdCombo with uid="BQPYTBJTHD",rtseq=3,rtrep=.f.,left=133,top=47,width=134,height=21;
    , tabstop=.f.;
    , ToolTipText = "Tipologia IVA da modificare";
    , HelpContextID = 29632822;
    , cFormVar="w_TIPIVA",RowSource=""+"No applicazione IVA,"+"Con IVA agevolata", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPIVA_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oTIPIVA_1_4.GetRadio()
    this.Parent.oContained.w_TIPIVA = this.RadioValue()
    return .t.
  endfunc

  func oTIPIVA_1_4.SetRadio()
    this.Parent.oContained.w_TIPIVA=trim(this.Parent.oContained.w_TIPIVA)
    this.value = ;
      iif(this.Parent.oContained.w_TIPIVA=='S',1,;
      iif(this.Parent.oContained.w_TIPIVA=='N',2,;
      0))
  endfunc

  add object oDOGANA_1_7 as StdField with uid="JOABLSQANX",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DOGANA", cQueryName = "DOGANA",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Dogana da modificare",;
    HelpContextID = 20684342,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=133, Top=74, InputMask=replicate('X',30)

  func oDOGANA_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(NVL(.w_CODICE, ' ')))
    endwith
   endif
  endfunc

  add object oUFFIVA_1_8 as StdField with uid="FLDLUUZKPO",rtseq=5,rtrep=.f.,;
    cFormVar = "w_UFFIVA", cQueryName = "UFFIVA",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Ufficio IVA da modificare",;
    HelpContextID = 29591110,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=133, Top=100, InputMask=replicate('X',25)

  add object oUFIVFO_1_9 as StdField with uid="FEIVWPWWDX",rtseq=6,rtrep=.f.,;
    cFormVar = "w_UFIVFO", cQueryName = "UFIVFO",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Ufficio IVA fornitore da modificare",;
    HelpContextID = 248559174,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=133, Top=126, InputMask=replicate('X',25)

  func oUFIVFO_1_9.mHide()
    with this.Parent.oContained
      return (g_APPLICATION='ad hoc ENTERPRISE')
    endwith
  endfunc

  add object oNUMDIC_1_11 as StdField with uid="JPFDZOSROO",rtseq=7,rtrep=.f.,;
    cFormVar = "w_NUMDIC", cQueryName = "NUMDIC",enabled=.f.,;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Protocollo della lettera selezionata",;
    HelpContextID = 49218774,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=373, Top=47, cSayPict="'999999'", cGetPict="'999999'"

  add object oANNO_1_12 as StdField with uid="AWKVWQHJWH",rtseq=8,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Codice esercizio da modificare",;
    HelpContextID = 76935930,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=482, Top=74, cSayPict="'9999'", cGetPict="'9999'", InputMask=replicate('X',4)

  add object oDATDIC_1_15 as StdField with uid="KCGADHCIHH",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DATDIC", cQueryName = "DATDIC",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data da modificare",;
    HelpContextID = 49242166,;
   bGlobalFont=.t.,;
    Height=21, Width=75, Left=594, Top=74


  add object oFLSTAM_1_16 as StdCombo with uid="NAWPAQWHZG",rtseq=10,rtrep=.f.,left=538,top=126,width=131,height=21, enabled=.f.;
    , tabstop=.f.;
    , ToolTipText = "Tipo di stampa da modificare";
    , HelpContextID = 209673046;
    , cFormVar="w_FLSTAM",RowSource=""+"Non stampata,"+"Stampata su registro", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLSTAM_1_16.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'R',;
    space(1))))
  endfunc
  func oFLSTAM_1_16.GetRadio()
    this.Parent.oContained.w_FLSTAM = this.RadioValue()
    return .t.
  endfunc

  func oFLSTAM_1_16.SetRadio()
    this.Parent.oContained.w_FLSTAM=trim(this.Parent.oContained.w_FLSTAM)
    this.value = ;
      iif(this.Parent.oContained.w_FLSTAM=='N',1,;
      iif(this.Parent.oContained.w_FLSTAM=='R',2,;
      0))
  endfunc

  add object oCODIVA_1_20 as StdField with uid="IPGVXWHZDL",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CODIVA", cQueryName = "CODIVA",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice IVA esente o non imponibile o agevolata da modificare",;
    HelpContextID = 29584934,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=140, Top=183, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_CODIVA"

  func oCODIVA_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODIVA_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODIVA_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oCODIVA_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Codici IVA",'',this.parent.oContained
  endproc
  proc oCODIVA_1_20.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_CODIVA
     i_obj.ecpSave()
  endproc

  add object oDESIVA_1_22 as StdField with uid="DKXVCOQJIP",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESIVA", cQueryName = "DESIVA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 29643830,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=204, Top=183, InputMask=replicate('X',35)

  add object oARTESE_1_23 as StdField with uid="FNRADNXUHE",rtseq=13,rtrep=.f.,;
    cFormVar = "w_ARTESE", cQueryName = "ARTESE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Articolo esenzione del D.P.R. 633/72 da modificare",;
    HelpContextID = 93352198,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=621, Top=183, cSayPict='"XXXXX"', cGetPict='"XXXXX"', InputMask=replicate('X',5)

  add object oDESOPE_1_25 as StdMemo with uid="VOXPSXKIFY",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DESOPE", cQueryName = "DESOPE",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione operazione da modificare",;
    HelpContextID = 90854454,;
   bGlobalFont=.t.,;
    Height=60, Width=530, Left=140, Top=212

  add object oTIPOPE_1_27 as StdRadio with uid="UXVVXSWJSY",rtseq=15,rtrep=.f.,left=140, top=278, width=149,height=38;
    , ToolTipText = "Tipo di operazione da modificare";
    , cFormVar="w_TIPOPE", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oTIPOPE_1_27.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Importo definito"
      this.Buttons(1).HelpContextID = 90843446
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Operazione specifica"
      this.Buttons(2).HelpContextID = 90843446
      this.Buttons(2).Top=18
      this.SetAll("Width",147)
      this.SetAll("Height",20)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Tipo di operazione da modificare")
      StdRadio::init()
    endproc

  func oTIPOPE_1_27.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'O',;
    space(1))))
  endfunc
  func oTIPOPE_1_27.GetRadio()
    this.Parent.oContained.w_TIPOPE = this.RadioValue()
    return .t.
  endfunc

  func oTIPOPE_1_27.SetRadio()
    this.Parent.oContained.w_TIPOPE=trim(this.Parent.oContained.w_TIPOPE)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOPE=='I',1,;
      iif(this.Parent.oContained.w_TIPOPE=='O',2,;
      0))
  endfunc

  proc oTIPOPE_1_27.mDefault
    with this.Parent.oContained
      if empty(.w_TIPOPE)
        .w_TIPOPE = 'I'
      endif
    endwith
  endproc

  add object oIMPDIC_1_29 as StdField with uid="TWYNAIHIRI",rtseq=16,rtrep=.f.,;
    cFormVar = "w_IMPDIC", cQueryName = "IMPDIC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale importo massimo comunicato dal fornitore da modificare",;
    HelpContextID = 49228934,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=531, Top=278, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  func oIMPDIC_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPOPE<>'D')
    endwith
   endif
  endfunc

  add object oIMPUTI_1_31 as StdField with uid="LYQBWUZGXZ",rtseq=17,rtrep=.f.,;
    cFormVar = "w_IMPUTI", cQueryName = "IMPUTI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale importo utilizzato da modificare",;
    HelpContextID = 162540678,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=531, Top=306, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  add object oDATINI_1_33 as StdField with uid="QTSNAMNRPT",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data di inizio validit� � maggiore della data di fine validit�",;
    ToolTipText = "Data di inizio validit� da modificare",;
    HelpContextID = 155476022,;
   bGlobalFont=.t.,;
    Height=21, Width=73, Left=222, Top=329

  func oDATINI_1_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATINI<=.w_DATFIN) or (empty(.w_DATFIN)))
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_35 as StdField with uid="LVVAOQJUMW",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data di inizio validit� � maggiore della data di fine validit�",;
    ToolTipText = "Data di fine validit� da modificare",;
    HelpContextID = 233922614,;
   bGlobalFont=.t.,;
    Height=21, Width=75, Left=412, Top=329

  func oDATFIN_1_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_datini<=.w_datfin) or (empty(.w_datini)))
    endwith
    return bRes
  endfunc

  add object oANNO1_1_42 as StdField with uid="ZNVPPXAPHG",rtseq=30,rtrep=.f.,;
    cFormVar = "w_ANNO1", cQueryName = "ANNO1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno della lettera di riferimento",;
    HelpContextID = 25555706,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=482, Top=47, InputMask=replicate('X',4)

  add object oDATA_1_43 as StdField with uid="YFFEAMFJGB",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DATA", cQueryName = "DATA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data della lettera di riferimento",;
    HelpContextID = 77832138,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=593, Top=47

  add object oCODICE_1_46 as StdField with uid="GAKEMVDGOT",rtseq=32,rtrep=.f.,;
    cFormVar = "w_CODICE", cQueryName = "CODICE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 76770854,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=271, Top=15, InputMask=replicate('X',15), cLinkFile="CONTI", cZoomOnZoom="GSAR_API", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPOCF", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODICE"

  func oCODICE_1_46.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESCLI_1_47 as StdField with uid="WRHDKFHDRH",rtseq=33,rtrep=.f.,;
    cFormVar = "w_DESCLI", cQueryName = "DESCLI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 152982582,;
   bGlobalFont=.t.,;
    Height=21, Width=275, Left=394, Top=15, InputMask=replicate('X',40)


  add object oBtn_1_48 as StdButton with uid="YCTTRQWDEB",left=570, top=333, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare il caricamento";
    , HelpContextID = 241136662;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_48.Click()
      with this.Parent.oContained
        GSCG_BCL(this.Parent.oContained,"C")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_48.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_ANNO) AND NOT EMPTY(.w_DATDIC) AND NOT EMPTY(.w_DATFIN))
      endwith
    endif
  endfunc


  add object oBtn_1_49 as StdButton with uid="TAPXNQTUEA",left=622, top=333, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 75136442;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_49.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSERDIC_1_50 as StdField with uid="LXQJULGNZX",rtseq=36,rtrep=.f.,;
    cFormVar = "w_SERDIC", cQueryName = "SERDIC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 49235238,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=434, Top=47, InputMask=replicate('X',3)

  add object oStr_1_1 as StdString with uid="NFPCILVRWR",Visible=.t., Left=1, Top=15,;
    Alignment=1, Width=130, Height=18,;
    Caption="Lettera di riferimento:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_5 as StdString with uid="CYQBQAJJEA",Visible=.t., Left=74, Top=48,;
    Alignment=1, Width=58, Height=18,;
    Caption="Tipo IVA:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_6 as StdString with uid="PHFJDCTXSL",Visible=.t., Left=74, Top=75,;
    Alignment=1, Width=56, Height=18,;
    Caption="Dogana:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="UKSTLIIDZL",Visible=.t., Left=294, Top=48,;
    Alignment=1, Width=75, Height=18,;
    Caption="Protocollo n.:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_13 as StdString with uid="EQKREUIPAC",Visible=.t., Left=473, Top=48,;
    Alignment=0, Width=8, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="ZTHPORLUQW",Visible=.t., Left=549, Top=75,;
    Alignment=1, Width=42, Height=18,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="LYASLGPCZW",Visible=.t., Left=482, Top=127,;
    Alignment=1, Width=44, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="CGXIJRWHXX",Visible=.t., Left=8, Top=157,;
    Alignment=0, Width=126, Height=18,;
    Caption="Dati esenzione"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="SPQXYTJIZP",Visible=.t., Left=4, Top=184,;
    Alignment=1, Width=133, Height=18,;
    Caption="Cod. IVA es./agev.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="JYHJMPEKAN",Visible=.t., Left=521, Top=185,;
    Alignment=1, Width=98, Height=18,;
    Caption="Art. esenzione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="GBXLVHLXMQ",Visible=.t., Left=3, Top=212,;
    Alignment=1, Width=134, Height=18,;
    Caption="Descr. operazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="NRGTNMPLOU",Visible=.t., Left=44, Top=278,;
    Alignment=1, Width=93, Height=18,;
    Caption="Tipo operazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="NUAHQERLIX",Visible=.t., Left=420, Top=278,;
    Alignment=1, Width=106, Height=18,;
    Caption="Importo massimo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="SYGVORNVPR",Visible=.t., Left=413, Top=306,;
    Alignment=1, Width=113, Height=18,;
    Caption="Importo utilizzato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="XHBSMBCGRB",Visible=.t., Left=123, Top=331,;
    Alignment=1, Width=96, Height=18,;
    Caption="Inizio validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="WDXXXEEEYO",Visible=.t., Left=308, Top=329,;
    Alignment=1, Width=101, Height=18,;
    Caption="Fine validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="GFIHTWBZOI",Visible=.t., Left=61, Top=102,;
    Alignment=1, Width=69, Height=18,;
    Caption="Ufficio IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="BKHVJRPGHJ",Visible=.t., Left=7, Top=127,;
    Alignment=1, Width=123, Height=18,;
    Caption="Uff. IVA fornitore:"  ;
  , bGlobalFont=.t.

  func oStr_1_41.mHide()
    with this.Parent.oContained
      return (g_APPLICATION='ad hoc ENTERPRISE')
    endwith
  endfunc

  add object oStr_1_44 as StdString with uid="WCCJJTGEBU",Visible=.t., Left=560, Top=48,;
    Alignment=1, Width=31, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="QAVDUNXDSQ",Visible=.t., Left=426, Top=75,;
    Alignment=1, Width=53, Height=18,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="TLYOVCPBKX",Visible=.t., Left=426, Top=48,;
    Alignment=0, Width=5, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oBox_1_18 as StdBox with uid="OTVZHIVJTZ",left=6, top=172, width=660,height=2
enddefine
define class tgscg_kdiPag2 as StdContainer
  Width  = 674
  height = 380
  stdWidth  = 674
  stdheight = 380
  resizeXpos=580
  resizeYpos=197
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oAGGDOG_2_1 as StdCheck with uid="OYSNJLVTAT",rtseq=23,rtrep=.f.,left=78, top=354, caption="Inserisci lettere a dogana",;
    ToolTipText = "Se attivo inserisce una nuova lettera d'intento alla dogana indicata",;
    HelpContextID = 122590726,;
    cFormVar="w_AGGDOG", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAGGDOG_2_1.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAGGDOG_2_1.GetRadio()
    this.Parent.oContained.w_AGGDOG = this.RadioValue()
    return .t.
  endfunc

  func oAGGDOG_2_1.SetRadio()
    this.Parent.oContained.w_AGGDOG=trim(this.Parent.oContained.w_AGGDOG)
    this.value = ;
      iif(this.Parent.oContained.w_AGGDOG=='S',1,;
      0)
  endfunc

  func oAGGDOG_2_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CODICE))
    endwith
   endif
  endfunc

  add object oCODINI_2_2 as StdField with uid="MLJCFVVALG",rtseq=24,rtrep=.f.,;
    cFormVar = "w_CODINI", cQueryName = "CODINI",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
    ToolTipText = "Fornitore di inizio selezione",;
    HelpContextID = 155414054,;
   bGlobalFont=.t.,;
    Height=21, Width=155, Left=87, Top=18, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_AFR", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODINI"

  func oCODINI_2_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGDOG<>'S')
    endwith
   endif
  endfunc

  func oCODINI_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODINI_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODINI_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODINI_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFR',"Elenco fornitori",'GSAR_SFR.CONTI_VZM',this.parent.oContained
  endproc
  proc oCODINI_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODINI
     i_obj.ecpSave()
  endproc

  add object oCODFIN_2_3 as StdField with uid="TNLKYDVLZE",rtseq=25,rtrep=.f.,;
    cFormVar = "w_CODFIN", cQueryName = "CODFIN",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale oppure obsoleto",;
    ToolTipText = "Fornitore di fine selezione",;
    HelpContextID = 233860646,;
   bGlobalFont=.t.,;
    Height=21, Width=155, Left=87, Top=41, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_AFR", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODFIN"

  func oCODFIN_2_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGDOG<>'S')
    endwith
   endif
  endfunc

  func oCODFIN_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFIN_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFIN_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODFIN_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFR',"Elenco fornitori",'GSAR_SFR.CONTI_VZM',this.parent.oContained
  endproc
  proc oCODFIN_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODFIN
     i_obj.ecpSave()
  endproc

  add object oDESCRI_2_6 as StdField with uid="AWONSGDGWT",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 159274038,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=248, Top=18, InputMask=replicate('X',35)

  add object oDESCR1_2_7 as StdField with uid="EOFYWWZWPA",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESCR1", cQueryName = "DESCR1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 25056310,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=248, Top=41, InputMask=replicate('X',35)

  add object oCODDOG_2_10 as StdField with uid="AAQBGLVCTM",rtseq=34,rtrep=.f.,;
    cFormVar = "w_CODDOG", cQueryName = "CODDOG",;
    bObbl = .t. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Codice dogana a cui intestare la nuova lettera di intento",;
    HelpContextID = 122580518,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=324, Top=356, InputMask=replicate('X',30)

  func oCODDOG_2_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGDOG='S')
    endwith
   endif
  endfunc

  func oCODDOG_2_10.mHide()
    with this.Parent.oContained
      return (.w_AGGDOG<>'S')
    endwith
  endfunc


  add object ZOOMFOR as cp_szoombox with uid="IBGZKLRYLZ",left=0, top=67, width=670,height=248,;
    caption='Object',;
   bGlobalFont=.t.,;
    bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",cTable="CONTI",cZoomFile="GSCGFKDI",bOptions=.f.,bAdvOptions=.f.,;
    cEvent = "Requery, Init, w_AGGDOG Changed",;
    nPag=2;
    , ToolTipText = "Elenco fornitori da associare";
    , HelpContextID = 95543782


  add object oBtn_2_13 as StdButton with uid="PSTUCDPWKC",left=604, top=18, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per rieseguire la query";
    , HelpContextID = 94468374;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_13.Click()
      with this.Parent.oContained
        .NotifyEvent('Requery')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_13.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_AGGDOG<>'S')
      endwith
    endif
  endfunc

  add object oSELEZI_2_14 as StdRadio with uid="TOMWADIXXM",rtseq=35,rtrep=.f.,left=8, top=318, width=129,height=32;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oSELEZI_2_14.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 167765286
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 167765286
      this.Buttons(2).Top=15
      this.SetAll("Width",127)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_2_14.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_2_14.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_2_14.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc

  func oSELEZI_2_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGDOG<>'S')
    endwith
   endif
  endfunc

  add object oStr_2_8 as StdString with uid="EDWBMVKYLI",Visible=.t., Left=2, Top=18,;
    Alignment=1, Width=82, Height=18,;
    Caption="Da fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_2_9 as StdString with uid="VDWNYSTWVN",Visible=.t., Left=6, Top=41,;
    Alignment=1, Width=78, Height=18,;
    Caption="A fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_2_11 as StdString with uid="EDCYPRLFIB",Visible=.t., Left=261, Top=359,;
    Alignment=1, Width=62, Height=18,;
    Caption="Dogana:"  ;
  , bGlobalFont=.t.

  func oStr_2_11.mHide()
    with this.Parent.oContained
      return (.w_AGGDOG<>'S')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_kdi','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gscg_kdi
Proc RequeryZoomInt(obj)
   obj.NotifyEvent('Requery')
endproc
Proc Seleziona(obj,w_sel)
   NC = obj.w_ZOOMFOR.cCursor
   if type("w_sel")='C'
     if w_sel='S'
       UPDATE &NC SET XCHK = 1
     else
       UPDATE &NC SET XCHK = 0
     endif
   else
     UPDATE &NC SET XCHK = 0
     obj.w_SELEZI='D'
   endif
endproc
* --- Fine Area Manuale
