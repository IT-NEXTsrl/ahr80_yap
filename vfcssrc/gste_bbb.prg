* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bbb                                                        *
*              Avviso di bonifico                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_358]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-07                                                      *
* Last revis.: 2015-10-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTIPO
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bbb",oParentObject,m.pTIPO)
return(i_retval)

define class tgste_bbb as StdBatch
  * --- Local variables
  pTIPO = 0
  w_TIPCON = space(1)
  w_CODCON = space(15)
  w_TEMP = 0
  w_SCELTA = space(2)
  w_tipoc = space(1)
  w_ind = space(40)
  w_ZOOM = space(10)
  w_FOUND = .f.
  w_codic = space(15)
  w_cap = space(8)
  w_MESS = space(100)
  w_FLGAVVI = space(1)
  w_nmdes = space(40)
  w_loca = space(30)
  w_prov = space(2)
  w_flgdef = space(1)
  w_pers = space(40)
  w_rif = space(2)
  w_OBTEST = ctod("  /  /  ")
  w_DTOBSO = ctod("  /  /  ")
  w_TEST = .f.
  * --- WorkFile variables
  PRO_NUME_idx=0
  DIS_TINT_idx=0
  CONTI_idx=0
  VALUTE_idx=0
  DES_DIVE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se 0 Lancia La stampa se 1 aggiorna il numero avviso - Lanciato da GSTE_SAB
    * --- Se 2 Aggiorna lo Zoom
    * --- Lancia Lettere di Avviso
    this.w_OBTEST = i_DATSYS
    do case
      case this.pTIPO=0
        * --- La Serie pu� essere vuota
        if NOT EMPTY(this.oParentObject.w_AVVISO)
          * --- Controllo se la distinta � gi� stata usata per un avviso di bonifico
          if NOT ah_YesNo("L'avviso per la distinta selezionata � gi� stato stampato%0Proseguire con l'elaborazione?")
            i_retcode = 'stop'
            return
          endif
        endif
        this.w_ZOOM = this.oParentObject.w_Zoomcc.cCursor
        this.w_TEMP = this.oParentObject.w_NUMAVVISO
        if RECCOUNT(this.w_ZOOM)=0
          ah_ErrorMsg("Nessuna scadenza selezionata",,"")
          i_retcode = 'stop'
          return
        endif
        * --- Verifico quanti record sono stati selezionati per la gestione degli indirizzi email
        SELECT (this.w_ZOOM)
        count for xchk=1 to NUMREC
        if numrec = 1
          SELECT (this.w_ZOOM)
          LOCATE FOR XCHK=1
          * --- Valorizzo le variabili globali i_EMAIL, i_EMAIL_PEC e i_FAXNO
          GSAR_BRM(this,"E_M",ANTIPCON,ANCODICE,"SO","V")
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Valorizzo le variabili globali per il Fax (per la selezione del numero di telefono)
          i_CLIFORDES = ANTIPCON 
 i_CODDES = ANCODICE 
 i_TIPDES = "SO"
        endif
        * --- Cerco l'avviso di sollecito
        SELECT (this.w_ZOOM)
        GO TOP
        SCAN FOR XCHK=1
        this.w_TIPCON = ANTIPCON
        this.w_CODCON = ANCODICE
        this.w_SCELTA = " "
        * --- Cerco in Destinazione diverse il sollecito con quel
        * --- codice e tipo e nel caso che esista modifico i campi
        * --- destinati a contenere i dati relativi alla sede trovata.
        this.w_tipoc = " "
        this.w_TEST = .F.
        * --- Select from DES_DIVE
        i_nConn=i_TableProp[this.DES_DIVE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" DES_DIVE ";
              +" where DDTIPCON="+cp_ToStrODBC(this.w_TIPCON)+" AND DDCODICE="+cp_ToStrODBC(this.w_CODCON)+" AND DDTIPRIF='SO'";
               ,"_Curs_DES_DIVE")
        else
          select * from (i_cTable);
           where DDTIPCON=this.w_TIPCON AND DDCODICE=this.w_CODCON AND DDTIPRIF="SO";
            into cursor _Curs_DES_DIVE
        endif
        if used('_Curs_DES_DIVE')
          select _Curs_DES_DIVE
          locate for 1=1
          do while not(eof())
          this.w_DTOBSO = NVL(CP_TODATE(DDDTOBSO),cp_CharToDate("  -  -  "))
          if (this.w_DTOBSO>this.w_OBTEST OR EMPTY(this.w_DTOBSO)) AND this.w_TEST=.F.
            this.w_TEST = .T.
            this.w_tipoc = NVL(DDTIPCON," ")
            this.w_codic = NVL(DDCODICE,SPACE(15))
            this.w_pers = NVL(DDPERSON,SPACE(40))
            this.w_NMDES = NVL(DDNOMDES,SPACE(40))
            this.w_IND = NVL(DDINDIRI,SPACE(35))
            this.w_CAP = NVL(DD___CAP,SPACE(8))
            this.w_LOCA = NVL(DDLOCALI, SPACE(30))
            this.w_PROV = NVL(DDPROVIN,SPACE(2))
          endif
            select _Curs_DES_DIVE
            continue
          enddo
          use
        endif
        SELECT (this.w_ZOOM)
        if this.w_TIPOC=this.w_TIPCON 
          REPLACE DDTIPCON WITH this.w_TIPOC, DDCODICE WITH this.w_CODIC, DDNOMDES WITH this.w_NMDES, DDINDIRI WITH this.w_IND
          REPLACE DD___CAP WITH this.w_CAP, DDLOCALI WITH this.w_LOCA, DDPROVIN WITH this.w_PROV, DDPERSON WITH this.w_PERS
          REPLACE DDTIPRIF WITH "SO"
        endif
        endscan
        * --- Stampa record
        if this.oParentObject.w_DETPAR="S"
          vq_exec("QUERY\GSTE1SAJ", this, "DETTPAR")
          SELECT PTSERIAL,TIPREC,ALLTRIM(TRAN(PTTOTIMP,"@Z "+V_PV(40+(20*NVL(VADECTOT,0))))) AS PTTOTIMP, DTOC(PTDATSCA) AS PTDATSCA,PTCODVAL,PTBANAPP,ANCODICE,ANDESCRI,ANDESCR2,ANINDIRI,ANINDIR2,AN___CAP,ANLOCALI,ANPROVIN,int(PNNUMDOC) as PNNUMDOC,; 
 PNALFDOC,DTOC(PNDATDOC) AS PNDATDOC,VADECTOT,ANFLGAVV,PTNUMDIS, DINUMERO,BADESCRI,ANTIPCON,BADESBAN,DDTIPCON,DDCODICE,DDNOMDES,DDINDIRI,DD___CAP,DDLOCALI,DDPROVIN,DDPERSON,DDTIPRIF,DIDATAVV,DTOC(DIDATDIS) as DIDATDIS ,PTFLRAGG,CPROWNUM,DINUMDIS,ANFLRAGG,PTNUMEFF,NUMPARTITE,XCHK,; 
 DDNOMDES AS RAGIOSOC,DDPERSON AS RAGIOSO2,DDTIPRIF AS SOLLECITO,; 
 DDINDIRI AS INDIRIZ,"" AS INDIRIZ2,DD___CAP AS CAP,DDLOCALI AS LOCALI,DDPROVIN AS PROVI,; 
 IIF(this.oParentObject.w_RAGFOR="S", ANCODICE, STR(INT(VAL(TRAN(this.w_temp+(RECNO()-1),"@Z 9999999"))))) AS NUM_AVVISO,this.oParentObject.w_SERIE AS SERIE,; 
 this.oParentObject.w_DEL AS DATAAVVISO, ALLTRIM(TRAN(PTTOTIMP,"@Z "+V_PV(40+(20*NVL(VADECTOT,0))))) AS IMPORTO,; 
 ALLTRIM(STR(INT(VAL(TRAN(PNNUMDOC,"@Z 999999999999999"))),15,0)) AS NUMERO, ANFLRAGG AS RAGGRUP,ANDESCR3,ANDESCR4,ANINDIR3,ANINDIR4,ANLOCAL2 FROM (this.w_ZOOM) where xchk=1 INTO CURSOR TEMP
          SELECT DETTPAR.PTSERIAL,DETTPAR.TIPREC,ALLTRIM(TRAN(DETTPAR.PTTOTIMP,"@Z "+V_PV(40+(20*NVL(DETTPAR.VADECTOT,0))))) AS PTTOTIMP,DTOC(DETTPAR.PTDATSCA) AS PTDATSCA,DETTPAR.PTCODVAL,DETTPAR.PTBANAPP,DETTPAR.ANCODICE,DETTPAR.ANDESCRI,DETTPAR.ANDESCR2,DETTPAR.ANINDIRI,DETTPAR.ANINDIR2,DETTPAR.AN___CAP,DETTPAR.ANLOCALI,DETTPAR.ANPROVIN, int(DETTPAR.PNNUMDOC) as PNNUMDOC,DETTPAR.PNALFDOC,DTOC(DETTPAR.PNDATDOC) AS PNDATDOC,DETTPAR.VADECTOT,DETTPAR.ANFLGAVV,DETTPAR.PTNUMDIS,DETTPAR.DINUMERO,DETTPAR.BADESCRI,DETTPAR.ANTIPCON,DETTPAR.BADESBAN,DETTPAR.DDTIPCON,DETTPAR.DDCODICE,DETTPAR.DDNOMDES,DETTPAR.DDINDIRI,DETTPAR.DD___CAP,DETTPAR.DDLOCALI,DETTPAR.DDPROVIN,DETTPAR.DDPERSON,DETTPAR.DDTIPRIF,DETTPAR.DIDATAVV,DTOC(DETTPAR.DIDATDIS) as DIDATDIS,DETTPAR.PTFLRAGG,DETTPAR.CPROWNUM,DETTPAR.DINUMDIS,DETTPAR.ANFLRAGG,DETTPAR.PTNUMEFF,DETTPAR.NUMPARTITE,DETTPAR.XCHK,; 
 DETTPAR.DDNOMDES AS RAGIOSOC,DETTPAR.DDPERSON AS RAGIOSO2,DETTPAR.DDTIPRIF AS SOLLECITO,; 
 DETTPAR.DDINDIRI AS INDIRIZ,"" AS INDIRIZ2,DETTPAR.DD___CAP AS CAP,DETTPAR.DDLOCALI AS LOCALI,DETTPAR.DDPROVIN AS PROVI,; 
 IIF(this.oParentObject.w_RAGFOR="S", DETTPAR.ANCODICE,TEMP.NUM_AVVISO) AS NUM_AVVISO,this.oParentObject.w_SERIE AS SERIE,; 
 this.oParentObject.w_DEL AS DATAAVVISO, ALLTRIM(TRAN(DETTPAR.PTTOTIMP,"@Z "+V_PV(40+(20*NVL(DETTPAR.VADECTOT,0))))) AS IMPORTO,; 
 ALLTRIM(STR(INT(VAL(TRAN(DETTPAR.PNNUMDOC,"@Z 999999999999999"))),15,0)) AS NUMERO, DETTPAR.ANFLRAGG AS RAGGRUP, DETTPAR.ANDESCR3,DETTPAR.ANDESCR4,DETTPAR.ANINDIR3,DETTPAR.ANINDIR4,DETTPAR.ANLOCAL2 FROM; 
 (DETTPAR INNER JOIN TEMP ON DETTPAR.PTSERIAL=TEMP.PTSERIAL AND DETTPAR.PTNUMEFF=TEMP.PTNUMEFF) into cursor TEMP1
           
 SELECT * FROM TEMP; 
 UNION; 
 SELECT * FROM TEMP1 ORDER BY; 
 NUM_AVVISO, ANCODICE, CPROWNUM, TIPREC INTO CURSOR __TMP__
        else
          SELECT PTSERIAL,TIPREC,ALLTRIM(TRAN(PTTOTIMP,"@Z "+V_PV(40+(20*NVL(VADECTOT,0))))) AS PTTOTIMP, DTOC(PTDATSCA) AS PTDATSCA,PTCODVAL,PTBANAPP,ANCODICE,ANDESCRI,ANDESCR2,ANINDIRI,ANINDIR2,AN___CAP,ANLOCALI,ANPROVIN,INT(PNNUMDOC) as PNNUMDOC,PNALFDOC,DTOC(PNDATDOC) AS PNDATDOC,VADECTOT,ANFLGAVV,PTNUMDIS, DINUMERO,BADESCRI,ANTIPCON,BADESBAN,DDTIPCON,DDCODICE,DDNOMDES,DDINDIRI,DD___CAP,DDLOCALI,DDPROVIN,DDPERSON,DDTIPRIF,DIDATAVV,DTOC(DIDATDIS) as DIDATDIS ,PTFLRAGG,CPROWNUM,DINUMDIS,ANFLRAGG,PTNUMEFF,NUMPARTITE,; 
 DDNOMDES AS RAGIOSOC,DDPERSON AS RAGIOSO2,DDTIPRIF AS SOLLECITO,; 
 DDINDIRI AS INDIRIZ,"" AS INDIRIZ2,DD___CAP AS CAP,DDLOCALI AS LOCALI,DDPROVIN AS PROVI,; 
 IIF(this.oParentObject.w_RAGFOR="S", ANCODICE,STR(INT(VAL(TRAN(this.w_temp+(RECNO()-1),"@Z 999999"))))) AS NUM_AVVISO,this.oParentObject.w_SERIE AS SERIE,; 
 this.oParentObject.w_DEL AS DATAAVVISO, ALLTRIM(TRAN(PTTOTIMP,"@Z "+V_PV(40+(20*NVL(VADECTOT,0))))) AS IMPORTO,; 
 ALLTRIM(STR(INT(VAL(TRAN(PNNUMDOC,"@Z 999999999999999"))),15,0)) AS NUMERO, ANFLRAGG AS RAGGRUP, ANDESCR3,ANDESCR4,ANINDIR3,ANINDIR4,ANLOCAL2 FROM (this.w_ZOOM) ORDER BY; 
 NUM_AVVISO, ANCODICE, CPROWNUM, TIPREC where xchk=1 into cursor __tmp__
        endif
        if RECCOUNT("__tmp__")>0
          * --- Controllo che le scadenze siano quelle della distinta nella maschera (se l'utente non preme la query)
          if __tmp__.PTNUMDIS<>this.oParentObject.w_NUMERO OR EMPTY ( this.oParentObject.w_NUMERO )
            ah_ErrorMsg("Premere il bottone di ricerca prima di avviare la stampa%0Le scadenze visualizzate non appartengono alla distinta selezionata",,"")
            i_retcode = 'stop'
            return
          endif
        else
          ah_ErrorMsg("Nessuna scadenza selezionata",,"")
          i_retcode = 'stop'
          return
        endif
        L_numdis=this.oParentObject.w_numdis
        L_RAGFOR=this.oParentObject.w_RAGFOR
        L_DETPAR=this.oParentObject.w_DETPAR
        * --- Cerca il bmp del logo con il nome del codice dell'azienda se non lo trova mette quello di default
         
 L_LOGO=GETBMP(I_CODAZI) 
 =wrcursor( "__Tmp__" ) 
 Select __Tmp__ 
 update __Tmp__ set Andescr2="-" where empty(nvl(andescr2," ")) 
 update __Tmp__ set Anindir2="-" where empty(nvl(anindir2," "))
        if VARTYPE(g_OFFICE_TYPE_CONV) = "C" AND g_OFFICE_TYPE_CONV $ "C-S"
          if !convcuraccentedchars("__tmp__", g_OFFICE_TYPE_CONV )
            ah_ErrorMsg(MSG_CURSOR_CHARS_CONVERSION_ERROR)
          endif
        endif
        CP_CHPRN( this.oParentObject.w_OREP, " ", this )
        * --- Reimposto le variabili per indirizzi email e numero Fax
        i_EMAIL = " "
        i_EMAIL_PEC = " "
        * --- Resetto le variabili globali per il Fax
        i_CLIFORDES = " " 
 i_CODDES = " " 
 i_TIPDES = " "
        * --- Scrivo la data avviso nei clienti / fornitori
        if USED("__TMP__")
          * --- Try
          local bErr_05293860
          bErr_05293860=bTrsErr
          this.Try_05293860()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
            ah_ErrorMsg("Errore durante la scrittura della data di avviso",,"")
            i_retcode = 'stop'
            return
          endif
          bTrsErr=bTrsErr or bErr_05293860
          * --- End
        else
          ah_ErrorMsg("Nessun dato da stampare",,"")
          i_retcode = 'stop'
          return
        endif
        * --- Lo metto a zero perch� non possa essere stampato due volte
        this.oParentObject.w_NUMDIS = 0
        this.oParentObject.w_NUMERO = ""
        this.oParentObject.w_DATDIS = cp_CharToDate("  -  -  ")
        this.oParentObject.w_TIPO = ""
        this.oParentObject.w_AVVISO = cp_CharToDate("  -  -  ")
        * --- Variabile Utilizzata per Azzerare Zoom Dopo Esecuzione della stampa
        this.oParentObject.w_NOFILT = "B"
        * --- Aggiorna lo Zoom - Flagga le scadenze con un cliente con il flag Invio Avviso a true
        this.oParentobject.NotifyEvent("Esegui")
      case this.pTIPO=1
        * --- Calcola il Numero dell'avviso
        * --- Read from PRO_NUME
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PRO_NUME_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRO_NUME_idx,2],.t.,this.PRO_NUME_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PRNUMPRO"+;
            " from "+i_cTable+" PRO_NUME where ";
                +"PRCODAZI = "+cp_ToStrODBC(i_CODAZI);
                +" and PRCODESE = "+cp_ToStrODBC(g_CODESE);
                +" and PRTIPPAG = "+cp_ToStrODBC("AB");
                +" and PRTIPCON = "+cp_ToStrODBC(this.oParentObject.w_SERIE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PRNUMPRO;
            from (i_cTable) where;
                PRCODAZI = i_CODAZI;
                and PRCODESE = g_CODESE;
                and PRTIPPAG = "AB";
                and PRTIPCON = this.oParentObject.w_SERIE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_NUMAVVISO = NVL(cp_ToDate(_read_.PRNUMPRO),cp_NullValue(_read_.PRNUMPRO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.oParentObject.w_NUMAVVISO = NVL(this.oParentObject.w_NUMAVVISO,0)+1
      case this.pTIPO=2
        * --- Aggiorna lo Zoom - Flagga le scadenze con un cliente con il flag Invio Avviso a true
        this.oParentobject.NotifyEvent("Esegui")
        this.w_ZOOM = this.oParentObject.w_Zoomcc
        * --- Leggo i Decimali
        select (this.w_ZOOM.cCursor)
        this.oParentObject.w_DECI = NVL(VADECTOT,0)
        VVL= 20 * this.oParentObject.w_DECI
        * --- Imposto il formato - utilizzo w_DECI
        FOR I=1 TO This.w_Zoom.grd.ColumnCount
        if upper(this.w_ZOOM.grd.columns(i).ControlSource)="PTTOTIMP"
          This.w_Zoom.grd.Columns(i).inputmask=v_PV[20*(this.oParentObject.w_DECI+2)]
        endif
        ENDFOR
        * --- Seleziono i dati estratti nello zoom
        update ( this.w_ZOOM.cCursor ) SET Xchk=1
      case this.pTIPO=3
        * --- Controllo che la distinta scritta si� corretta
        this.w_FOUND = .F.
        * --- Cerco tutte le distinte nell'anno con il numero digitato - una al massimo
        * --- Select from DIS_TINT
        i_nConn=i_TableProp[this.DIS_TINT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DIS_TINT_idx,2],.t.,this.DIS_TINT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" DIS_TINT ";
              +" where DINUMERO="+cp_ToStrODBC(this.oParentObject.w_NUMDIS)+" AND (DITIPDIS = 'BO' OR DITIPDIS = 'SC') and DIFLDEFI = 'S'";
              +" order by DI__ANNO desc";
               ,"_Curs_DIS_TINT")
        else
          select * from (i_cTable);
           where DINUMERO=this.oParentObject.w_NUMDIS AND (DITIPDIS = "BO" OR DITIPDIS = "SC") and DIFLDEFI = "S";
           order by DI__ANNO desc;
            into cursor _Curs_DIS_TINT
        endif
        if used('_Curs_DIS_TINT')
          select _Curs_DIS_TINT
          locate for 1=1
          do while not(eof())
          this.oParentObject.w_NUMERO = _Curs_DIS_TINT.DINUMDIS
          this.oParentObject.w_DATDIS = CP_TODATE(_Curs_DIS_TINT.DIDATDIS)
          this.oParentObject.w_TIPO = _Curs_DIS_TINT.DITIPDIS
          this.oParentObject.w_AVVISO = CP_TODATE(_Curs_DIS_TINT.DIDATAVV)
          this.w_FLGAVVI = NVL(_Curs_DIS_TINT.DIFLAVVI, " ")
          this.w_FOUND = .T.
          this.w_flgdef = _Curs_DIS_TINT.DIFLDEFI
          this.oParentObject.w_ANNO = _Curs_DIS_TINT.DI__ANNO
          exit
            select _Curs_DIS_TINT
            continue
          enddo
          use
        endif
        * --- Se la distinta scritta non � del tipo giusto (BONIFICO) o non ha il flag avviso bonifico o non esiste
        if NOT(this.oParentObject.w_TIPO$ "BO-SC" AND this.w_FOUND=.T. AND this.w_FLGAVVI="S" AND this.w_flgdef="S")
          this.oParentObject.w_NUMDIS = 0
          this.oParentObject.w_NUMERO = ""
          this.oParentObject.w_DATDIS = cp_CharToDate("  -  -  ")
          this.oParentObject.w_TIPO = ""
          ah_ErrorMsg("La distinta o non ha tipo bonifico%0o non ha il flag avviso bonifico oppure non esiste","")
          i_retcode = 'stop'
          return
        endif
    endcase
    if used("__TMP__")
       
 select __TMP__ 
 USE
    endif
  endproc
  proc Try_05293860()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Il progressivo degli avvisi (legato alla serie) cresce al crescere dei bonifici stampati
    * --- Anche se li stampo due volte
    this.w_TEMP = this.oParentObject.w_NUMAVVISO+RECCOUNT("__Tmp__")
    select __tmp__
    SCAN FOR NOT EMPTY(NVL(ANCODICE, " "))
    this.w_CODCON = __tmp__.ANCODICE
    * --- Gli avvisi di bonifico vengono inviati solo a fornitori
    * --- Write into CONTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ANDATAVV ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DEL),'CONTI','ANDATAVV');
          +i_ccchkf ;
      +" where ";
          +"ANTIPCON = "+cp_ToStrODBC("F");
          +" and ANCODICE = "+cp_ToStrODBC(this.w_CODCON);
             )
    else
      update (i_cTable) set;
          ANDATAVV = this.oParentObject.w_DEL;
          &i_ccchkf. ;
       where;
          ANTIPCON = "F";
          and ANCODICE = this.w_CODCON;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    select __tmp__
    ENDSCAN
    * --- Aggiorno il progressivo
    * --- Write into PRO_NUME
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PRO_NUME_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRO_NUME_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PRO_NUME_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PRNUMPRO ="+cp_NullLink(cp_ToStrODBC(this.w_TEMP),'PRO_NUME','PRNUMPRO');
          +i_ccchkf ;
      +" where ";
          +"PRCODAZI = "+cp_ToStrODBC(i_codazi);
          +" and PRCODESE = "+cp_ToStrODBC(g_codese);
          +" and PRTIPPAG = "+cp_ToStrODBC("AB");
          +" and PRTIPCON = "+cp_ToStrODBC(this.oParentObject.w_SERIE);
             )
    else
      update (i_cTable) set;
          PRNUMPRO = this.w_TEMP;
          &i_ccchkf. ;
       where;
          PRCODAZI = i_codazi;
          and PRCODESE = g_codese;
          and PRTIPPAG = "AB";
          and PRTIPCON = this.oParentObject.w_SERIE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.oParentObject.w_NUMAVVISO = this.w_TEMP
    * --- Scrivo nella distinta la data avviso
    * --- Write into DIS_TINT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DIS_TINT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DIS_TINT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DIS_TINT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"DIDATAVV ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DEL),'DIS_TINT','DIDATAVV');
          +i_ccchkf ;
      +" where ";
          +"DINUMDIS = "+cp_ToStrODBC(this.oParentObject.w_NUMERO);
             )
    else
      update (i_cTable) set;
          DIDATAVV = this.oParentObject.w_DEL;
          &i_ccchkf. ;
       where;
          DINUMDIS = this.oParentObject.w_NUMERO;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,pTIPO)
    this.pTIPO=pTIPO
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='PRO_NUME'
    this.cWorkTables[2]='DIS_TINT'
    this.cWorkTables[3]='CONTI'
    this.cWorkTables[4]='VALUTE'
    this.cWorkTables[5]='DES_DIVE'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_DES_DIVE')
      use in _Curs_DES_DIVE
    endif
    if used('_Curs_DIS_TINT')
      use in _Curs_DIS_TINT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTIPO"
endproc
