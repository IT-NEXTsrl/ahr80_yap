* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_klz                                                        *
*              Elenco aziende gestite                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-09-27                                                      *
* Last revis.: 2012-10-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gsut_klz
do GSUT_BLZ with .null.,'C'
* --- Fine Area Manuale
return(createobject("tgsut_klz",oParentObject))

* --- Class definition
define class tgsut_klz as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 704
  Height = 482
  bGlobalFont=.f.
  FontName      = "Arial"
  FontSize      = 8
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-10-12"
  HelpContextID=51821719
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Constant Properties
  _IDX = 0
  ACTKEY_IDX = 0
  cPrg = "gsut_klz"
  cComment = "Elenco aziende gestite"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODAZIEN = space(5)
  w_ZOOMSEL = space(1)
  w_MATPRO = space(10)
  o_MATPRO = space(10)
  w_RAGINS = space(50)
  w_RAGCLI = space(50)
  w_DIR2 = space(250)
  o_DIR2 = space(250)
  w_DIR = space(250)
  w_ZOOMAZI = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_klzPag1","gsut_klz",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oZOOMSEL_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOMAZI = this.oPgFrm.Pages(1).oPag.ZOOMAZI
    DoDefault()
    proc Destroy()
      this.w_ZOOMAZI = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='ACTKEY'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZIEN=space(5)
      .w_ZOOMSEL=space(1)
      .w_MATPRO=space(10)
      .w_RAGINS=space(50)
      .w_RAGCLI=space(50)
      .w_DIR2=space(250)
      .w_DIR=space(250)
        .w_CODAZIEN = '0000000001'
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODAZIEN))
          .link_1_1('Full')
        endif
      .oPgFrm.Page1.oPag.ZOOMAZI.Calculate()
        .w_ZOOMSEL = 'N'
        .w_MATPRO = iif(len(alltrim(.w_MATPRO)) # 10,left(alltrim(.w_MATPRO),1)+'0000'+right(alltrim(.w_MATPRO),5),.w_MATPRO)
      .oPgFrm.Page1.oPag.oObj_1_16.Calculate(.w_DIR)
    endwith
    this.DoRTCalc(4,7,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsut_klz
    Local L_prodotto  , L_MONUMREL  , L_MOPIVINS , L_MOPIVCLI , L_MORAGINS , L_MOMAXUTE , L_MOTIPDBF , L_MOINDCLI , L_MORAGCLI  , L_MOMATPRO
    L_prodotto =''
    L_MONUMREL =''
    L_MOPIVINS=''
    L_MOPIVCLI=''
    L_MORAGINS=''
    L_MOMAXUTE=0
    L_MOTIPDBF=''
    L_MOINDCLI=''
    L_MORAGCLI =''
    L_MOMATPRO=''
    this.NotifyEvent('AggZoom')
    this.w_DIR='Elenco_aziende_gestite.txt'
    this.w_DIR2=addbs(JUSTDRIVE(SYS(2023)))+THIS.w_DIR
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .oPgFrm.Page1.oPag.ZOOMAZI.Calculate()
        .DoRTCalc(2,2,.t.)
        if .o_MATPRO<>.w_MATPRO
            .w_MATPRO = iif(len(alltrim(.w_MATPRO)) # 10,left(alltrim(.w_MATPRO),1)+'0000'+right(alltrim(.w_MATPRO),5),.w_MATPRO)
        endif
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate(.w_DIR)
        if .o_DIR2<>.w_DIR2
          .Calculate_WXEBKFWDVL()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(4,7,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOMAZI.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate(.w_DIR)
    endwith
  return

  proc Calculate_LWJXSRBGJT()
    with this
          * --- Aggiorno le selezioni
          GSUT_BLZ(this;
              ,'S';
             )
    endwith
  endproc
  proc Calculate_MWCPIXWSJQ()
    with this
          * --- Cancello la tabella TMP_PART
          GSUT_BLZ(this;
              ,'D';
             )
    endwith
  endproc
  proc Calculate_KWYEESIKDP()
    with this
          * --- Controllo la presenza della partita IVA o del codice fiscale sul record selezionato
          GSUT_BLZ(this;
              ,'Z';
             )
    endwith
  endproc
  proc Calculate_WXEBKFWDVL()
    with this
          * --- Controllo estensione file di destinazione
          .w_DIR2 = iif(lower(JUSTEXT(.w_DIR2)) # 'txt',alltrim(SUBSTR(.w_DIR2,1,iif(RAT('.',.w_DIR2)-1>0,RAT('.',.w_DIR2)-1,len(.w_DIR2))))+'.txt',.w_DIR2)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOMAZI.Event(cEvent)
        if lower(cEvent)==lower("w_ZOOMSEL Changed")
          .Calculate_LWJXSRBGJT()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_16.Event(cEvent)
        if lower(cEvent)==lower("Done")
          .Calculate_MWCPIXWSJQ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_ZOOMAZI row checked")
          .Calculate_KWYEESIKDP()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZIEN
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ACTKEY_IDX,3]
    i_lTable = "ACTKEY"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ACTKEY_IDX,2], .t., this.ACTKEY_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ACTKEY_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZIEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZIEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOCODICE,MORAGCLI,MORAGINS,MOMATPRO";
                   +" from "+i_cTable+" "+i_lTable+" where MOCODICE="+cp_ToStrODBC(this.w_CODAZIEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOCODICE',this.w_CODAZIEN)
            select MOCODICE,MORAGCLI,MORAGINS,MOMATPRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZIEN = NVL(_Link_.MOCODICE,space(5))
      this.w_RAGCLI = NVL(_Link_.MORAGCLI,space(50))
      this.w_RAGINS = NVL(_Link_.MORAGINS,space(50))
      this.w_MATPRO = NVL(_Link_.MOMATPRO,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZIEN = space(5)
      endif
      this.w_RAGCLI = space(50)
      this.w_RAGINS = space(50)
      this.w_MATPRO = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ACTKEY_IDX,2])+'\'+cp_ToStr(_Link_.MOCODICE,1)
      cp_ShowWarn(i_cKey,this.ACTKEY_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZIEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oZOOMSEL_1_3.RadioValue()==this.w_ZOOMSEL)
      this.oPgFrm.Page1.oPag.oZOOMSEL_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDIR2_1_7.value==this.w_DIR2)
      this.oPgFrm.Page1.oPag.oDIR2_1_7.value=this.w_DIR2
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MATPRO = this.w_MATPRO
    this.o_DIR2 = this.w_DIR2
    return

enddefine

* --- Define pages as container
define class tgsut_klzPag1 as StdContainer
  Width  = 700
  height = 482
  stdWidth  = 700
  stdheight = 482
  resizeXpos=325
  resizeYpos=274
  bGlobalFont=.f.
  FontName      = "Arial"
  FontSize      = 8
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZOOMAZI as cp_szoombox with uid="QMUDOXVHKT",left=13, top=154, width=632,height=264,;
    caption='ZOOMAZI',;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    cZoomFile="gsut_klz",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cTable="AZIENDA",cMenuFile="",cZoomOnZoom="",bRetriveAllRows=.f.,bQueryOnDblClick=.f.,;
    cEvent = "AggZoom",;
    nPag=1;
    , HelpContextID = 24707478

  add object oZOOMSEL_1_3 as StdRadio with uid="COWRNZNLLJ",rtseq=2,rtrep=.f.,left=20, top=442, width=123,height=35;
    , cFormVar="w_ZOOMSEL", ButtonCount=2, bObbl=.f., nPag=1;
   , FntName = "Arial", FntSize = 8, FntBold = .f., FntItalic=.f., FntUnderline=.f., FntStrikeThru=.f.

    proc oZOOMSEL_1_3.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 228131222
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 228131222
      this.Buttons(2).Top=16
      this.SetAll("Width",121)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oZOOMSEL_1_3.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oZOOMSEL_1_3.GetRadio()
    this.Parent.oContained.w_ZOOMSEL = this.RadioValue()
    return .t.
  endfunc

  func oZOOMSEL_1_3.SetRadio()
    this.Parent.oContained.w_ZOOMSEL=trim(this.Parent.oContained.w_ZOOMSEL)
    this.value = ;
      iif(this.Parent.oContained.w_ZOOMSEL=='S',1,;
      iif(this.Parent.oContained.w_ZOOMSEL=='N',2,;
      0))
  endfunc

  add object oDIR2_1_7 as StdField with uid="CQMKULHCPL",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DIR2", cQueryName = "DIR2",;
    bObbl = .f. , nPag = 1, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "Il file pu� avere solo estensione .txt",;
    HelpContextID = 55454262,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=295, Left=161, Top=454, InputMask=replicate('X',250), bHasZoom = .t. 

  proc oDIR2_1_7.mZoom
    this.parent.oContained.w_DIR2=GetFile( "TXT" )
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc


  add object oBtn_1_8 as StdButton with uid="BMZFBTDQXH",left=493, top=430, width=48,height=45,;
    CpPicture="bmp\SAVE.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per salvare";
    , HelpContextID = 161735462;
    , Caption='\<Salva';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      with this.Parent.oContained
        gsut_blz(this.Parent.oContained,"F")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_10 as StdButton with uid="WVKCRLCZSX",left=543, top=430, width=48,height=45,;
    CpPicture="bmp\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 162328826;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_11 as StdButton with uid="ISLODYFBVX",left=593, top=430, width=48,height=45,;
    CpPicture="bmp\fxmail.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per inviare i dati via eMail";
    , HelpContextID = 160927622;
    , Caption='\<Invia';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      with this.Parent.oContained
        gsut_blz(this.Parent.oContained,"I")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_16 as cp_calclbl with uid="MIJBJJAXQY",left=11, top=68, width=521,height=17,;
    caption='Object',;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    caption="label text",;
    nPag=1;
    , HelpContextID = 38616090

  add object oStr_1_12 as StdString with uid="LVPAISUSUX",Visible=.t., Left=162, Top=435,;
    Alignment=0, Width=205, Height=17,;
    Caption="File di destinazione per il salvataggio:"  ;
  ,  FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_13 as StdString with uid="YUAVNTUJKB",Visible=.t., Left=11, Top=4,;
    Alignment=0, Width=118, Height=17,;
    Caption="Stimatissimo Cliente,"  ;
  ,  FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_14 as StdString with uid="VVOBMWHZDX",Visible=.t., Left=11, Top=52,;
    Alignment=0, Width=683, Height=17,;
    Caption="la codifica di tali informazioni nel file che si chiamer�:"  ;
  ,  FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_15 as StdString with uid="RBOOZSCVFJ",Visible=.t., Left=11, Top=88,;
    Alignment=0, Width=683, Height=17,;
    Caption="e l'invio di tale file al Servizio Clienti Zucchetti."  ;
  ,  FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_17 as StdString with uid="PRVXWCJZGE",Visible=.t., Left=11, Top=104,;
    Alignment=0, Width=683, Height=17,;
    Caption="Il file verr� conservato dal servizio clienti ai fini dei calcoli di attivazione delle licenze"  ;
  ,  FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_18 as StdString with uid="DUNPECZVBW",Visible=.t., Left=11, Top=120,;
    Alignment=0, Width=683, Height=17,;
    Caption="Le ricordiamo che per chiarimenti e per l'esercizio dei diritti di cui all'art. 7, Dlgs 196/2003 potr� inviare una email al suo concessionario "  ;
  ,  FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_19 as StdString with uid="QCKHHDKUQT",Visible=.t., Left=11, Top=136,;
    Alignment=0, Width=683, Height=17,;
    Caption="o  contattare telefonicamente il Suo servizio di assistenza."  ;
  ,  FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_22 as StdString with uid="IIBTCDBMAA",Visible=.t., Left=20, Top=420,;
    Alignment=0, Width=442, Height=17,;
    Caption="In rosso: azienda non esportabile perch� priva di codice fiscale e partita iva"    , forecolor = 255;
  ;
  ,  FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_25 as StdString with uid="VTTIUTTUDC",Visible=.t., Left=11, Top=36,;
    Alignment=0, Width=683, Height=17,;
    Caption=" le comunichiamo che con la release che sta per installare vi sar� l'estrazione automatica dei dati Azienda del suo Data Base,"  ;
  ,  FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_26 as StdString with uid="PZKRKTIYLO",Visible=.t., Left=11, Top=20,;
    Alignment=0, Width=683, Height=17,;
    Caption="al fine di verificare il corretto adempimento del contratto da Lei stipulato relativo al Software AdHoc"  ;
  ,  FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_klz','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
