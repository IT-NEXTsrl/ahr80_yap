* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bec                                                        *
*              Eliminazione Documento di Evasione Componenti/Prodotti Finiti   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-10-18                                                      *
* Last revis.: 2016-01-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pSEREVA,pSERIAL,pROWORI,pDOCRIG
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bec",oParentObject,m.pSEREVA,m.pSERIAL,m.pROWORI,m.pDOCRIG)
return(i_retval)

define class tgsve_bec as StdBatch
  * --- Local variables
  pSEREVA = space(10)
  pSERIAL = space(10)
  pROWORI = 0
  pDOCRIG = .f.
  w_EVAKEY = space(20)
  w_EVAMAG = space(5)
  w_EVAMAT = space(5)
  w_EVAQTA = 0
  w_EVAQT1 = 0
  w_EVACAR = space(1)
  w_EVAORD = space(1)
  w_EVAIMP = space(1)
  w_EVARIS = space(1)
  w_EV2CAR = space(1)
  w_EV2ORD = space(1)
  w_EV2IMP = space(1)
  w_EV2RIS = space(1)
  w_EVADAT = ctod("  /  /  ")
  w_EVANUM = 0
  w_EVAALF = space(2)
  w_EVASER = space(10)
  w_EVAROW = 0
  w_EVAFLA = space(1)
  w_EVAKEY2 = space(20)
  w_EVAMAG2 = space(5)
  w_EVQIMP = 0
  w_EVQIM1 = 0
  w_EVFLORDI = space(1)
  w_EVFLIMPE = space(1)
  w_EVFLRISE = space(1)
  w_EVEVAKEY2 = space(20)
  w_EVEVAMAG2 = space(5)
  w_EVOQTAEVA = 0
  w_EVOQTAEV1 = 0
  w_EVOQTAMOV = 0
  w_EVOQTAUM1 = 0
  w_EVOQTASAL = 0
  w_EVNQTAEVA = 0
  w_EVNQTAEV1 = 0
  w_EVNQTASAL = 0
  w_OK = .f.
  w_RETVAL = space(254)
  w_EVAFLE = space(1)
  w_EVNUMRIF = 0
  w_EVAART = space(20)
  w_EVACOM = space(20)
  w_EVAART2 = space(20)
  w_EVACOM2 = space(20)
  w_COMMDEFA = space(15)
  w_SALCOM = space(1)
  w_COMMAPPO = space(15)
  w_EVALOT = space(20)
  w_EVALOT2 = space(20)
  w_EVAUBI = space(20)
  w_EVAUB2 = space(20)
  w_EVAUB3 = space(20)
  * --- WorkFile variables
  DOC_DETT_idx=0
  DOC_MAST_idx=0
  SALDIART_idx=0
  TMPDETTI_idx=0
  ART_ICOL_idx=0
  SALDICOM_idx=0
  SALOTCOM_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eliminazione Documento di Evasione Componenti/Prodotti Finiti
    this.w_OK = .T.
    this.w_RETVAL = ""
    * --- Mi assicuro che la variabile sia space di 15 (per evitare problemi con Oracle)
    this.w_COMMDEFA = NVL(g_PPCODCOM, SPACE(15))
    * --- Aggiorna prima i saldi - Poi cancella dettaglio e testata
    * --- Create temporary table TMPDETTI
    i_nIdx=cp_AddTableDef('TMPDETTI') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.DOC_DETT_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"MVQTAEV1,MVIMPEVA,MVCODMAG,MVCODMAT,MVQTAMOV,MVQTAUM1,MVQTAIMP,MVQTAIM1,MVSERRIF,MVROWRIF,MVFLARIF,MVFLCASC,MVFLORDI,MVFLIMPE,MVFLRISE,MVF2CASC,MVF2ORDI,MVF2IMPE,MVF2RISE,MVKEYSAL,MVNUMRIF,MVFLERIF,MVCODART,MVCODCOM,MVCODLOT,MVCODUBI,MVCODUB2 "," from "+i_cTable;
          +" where MVSERIAL="+cp_ToStrODBC(this.pSEREVA)+" ";
          )
    this.TMPDETTI_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Select from TMPDETTI
    i_nConn=i_TableProp[this.TMPDETTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPDETTI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMPDETTI ";
           ,"_Curs_TMPDETTI")
    else
      select * from (i_cTable);
        into cursor _Curs_TMPDETTI
    endif
    if used('_Curs_TMPDETTI')
      select _Curs_TMPDETTI
      locate for 1=1
      do while not(eof())
      if NVL(_Curs_TMPDETTI.MVQTAEV1, 0)<>0 OR NVL(_Curs_TMPDETTI.MVIMPEVA, 0)<>0
        * --- Read from DOC_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MVDATDOC,MVNUMDOC,MVALFDOC"+;
            " from "+i_cTable+" DOC_MAST where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.pSEREVA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MVDATDOC,MVNUMDOC,MVALFDOC;
            from (i_cTable) where;
                MVSERIAL = this.pSEREVA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_EVADAT = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
          this.w_EVANUM = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
          this.w_EVAALF = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_RETVAL = ah_Msgformat("Documento di evasione componenti n.%1 del %2%0Evaso da altri documenti; impossibile eliminare/variare", ALLTRIM(STR(this.w_EVANUM)) + IIF(EMPTY(this.w_EVAALF), " ", "/"+Alltrim(this.w_EVAALF)) , DTOC(this.w_EVADAT) )
        this.w_OK = .F.
      endif
      if this.w_OK
        this.w_EVAKEY = NVL(_Curs_TMPDETTI.MVKEYSAL, "")
        this.w_EVAMAG = NVL(_Curs_TMPDETTI.MVCODMAG,"")
        this.w_EVAMAT = NVL(_Curs_TMPDETTI.MVCODMAT,"")
        this.w_EVAQTA = NVL(_Curs_TMPDETTI.MVQTAMOV, 0)
        this.w_EVAQT1 = NVL(_Curs_TMPDETTI.MVQTAUM1, 0)
        this.w_EVQIMP = NVL(_Curs_TMPDETTI.MVQTAIMP, 0)
        this.w_EVQIM1 = NVL(_Curs_TMPDETTI.MVQTAIM1, 0)
        this.w_EVASER = NVL(_Curs_TMPDETTI.MVSERRIF,"")
        this.w_EVAROW = NVL(_Curs_TMPDETTI.MVROWRIF, 0)
        this.w_EVAFLA = NVL(_Curs_TMPDETTI.MVFLARIF, " ")
        this.w_EVAFLE = NVL(_Curs_TMPDETTI.MVFLERIF, " ")
        this.w_EVACAR = IIF(_Curs_TMPDETTI.MVFLCASC="-","+",IIF(_Curs_TMPDETTI.MVFLCASC="+","-"," "))
        this.w_EVAORD = IIF(_Curs_TMPDETTI.MVFLORDI="-","+",IIF(_Curs_TMPDETTI.MVFLORDI="+","-"," "))
        this.w_EVAIMP = IIF(_Curs_TMPDETTI.MVFLIMPE="-","+",IIF(_Curs_TMPDETTI.MVFLIMPE="+","-"," "))
        this.w_EVARIS = IIF(_Curs_TMPDETTI.MVFLRISE="-","+",IIF(_Curs_TMPDETTI.MVFLRISE="+","-"," "))
        this.w_EV2CAR = IIF(_Curs_TMPDETTI.MVF2CASC="-","+",IIF(_Curs_TMPDETTI.MVF2CASC="+","-"," "))
        this.w_EV2ORD = IIF(_Curs_TMPDETTI.MVF2ORDI="-","+",IIF(_Curs_TMPDETTI.MVF2ORDI="+","-"," "))
        this.w_EV2IMP = IIF(_Curs_TMPDETTI.MVF2IMPE="-","+",IIF(_Curs_TMPDETTI.MVF2IMPE="+","-"," "))
        this.w_EV2RIS = IIF(_Curs_TMPDETTI.MVF2RISE="-","+",IIF(_Curs_TMPDETTI.MVF2RISE="+","-"," "))
        this.w_EVNUMRIF = _Curs_TMPDETTI.MVNUMRIF
        this.w_EVAART = NVL(_Curs_TMPDETTI.MVCODART, "")
        this.w_EVACOM = NVL(_Curs_TMPDETTI.MVCODCOM, "")
        this.w_EVALOT = NVL(_Curs_TMPDETTI.MVCODLOT, "")
        this.w_EVAUBI = NVL(_Curs_TMPDETTI.MVCODUBI, "")
        this.w_EVAUB2 = NVL(_Curs_TMPDETTI.MVCODUB2, "")
        if NOT EMPTY(this.w_EVAKEY) AND NOT EMPTY(this.w_EVAMAG)
          * --- Write into SALDIART
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDIART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_EVACAR,'SLQTAPER','this.w_EVAQT1',this.w_EVAQT1,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_EVARIS,'SLQTRPER','this.w_EVAQT1',this.w_EVAQT1,'update',i_nConn)
            i_cOp3=cp_SetTrsOp(this.w_EVAORD,'SLQTOPER','this.w_EVAQT1',this.w_EVAQT1,'update',i_nConn)
            i_cOp4=cp_SetTrsOp(this.w_EVAIMP,'SLQTIPER','this.w_EVAQT1',this.w_EVAQT1,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SLQTAPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTAPER');
            +",SLQTRPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTRPER');
            +",SLQTOPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTOPER');
            +",SLQTIPER ="+cp_NullLink(i_cOp4,'SALDIART','SLQTIPER');
                +i_ccchkf ;
            +" where ";
                +"SLCODICE = "+cp_ToStrODBC(this.w_EVAKEY);
                +" and SLCODMAG = "+cp_ToStrODBC(this.w_EVAMAG);
                   )
          else
            update (i_cTable) set;
                SLQTAPER = &i_cOp1.;
                ,SLQTRPER = &i_cOp2.;
                ,SLQTOPER = &i_cOp3.;
                ,SLQTIPER = &i_cOp4.;
                &i_ccchkf. ;
             where;
                SLCODICE = this.w_EVAKEY;
                and SLCODMAG = this.w_EVAMAG;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARSALCOM"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.w_EVAART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARSALCOM;
              from (i_cTable) where;
                  ARCODART = this.w_EVAART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_SALCOM="S"
            if empty(nvl(this.w_EVACOM,""))
              this.w_COMMAPPO = this.w_COMMDEFA
            else
              this.w_COMMAPPO = this.w_EVACOM
            endif
            * --- Write into SALDICOM
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDICOM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_EVACAR,'SCQTAPER','this.w_EVAQT1',this.w_EVAQT1,'update',i_nConn)
              i_cOp2=cp_SetTrsOp(this.w_EVARIS,'SCQTRPER','this.w_EVAQT1',this.w_EVAQT1,'update',i_nConn)
              i_cOp3=cp_SetTrsOp(this.w_EVAORD,'SCQTOPER','this.w_EVAQT1',this.w_EVAQT1,'update',i_nConn)
              i_cOp4=cp_SetTrsOp(this.w_EVAIMP,'SCQTIPER','this.w_EVAQT1',this.w_EVAQT1,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
              +",SCQTRPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTRPER');
              +",SCQTOPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTOPER');
              +",SCQTIPER ="+cp_NullLink(i_cOp4,'SALDICOM','SCQTIPER');
                  +i_ccchkf ;
              +" where ";
                  +"SCCODICE = "+cp_ToStrODBC(this.w_EVAKEY);
                  +" and SCCODMAG = "+cp_ToStrODBC(this.w_EVAMAG);
                  +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                     )
            else
              update (i_cTable) set;
                  SCQTAPER = &i_cOp1.;
                  ,SCQTRPER = &i_cOp2.;
                  ,SCQTOPER = &i_cOp3.;
                  ,SCQTIPER = &i_cOp4.;
                  &i_ccchkf. ;
               where;
                  SCCODICE = this.w_EVAKEY;
                  and SCCODMAG = this.w_EVAMAG;
                  and SCCODCAN = this.w_COMMAPPO;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore Aggiornamento Saldi Commessa'
              return
            endif
            if g_MADV="S"
              if g_PERUBI = "S" OR g_PERLOT = "S"
                * --- Write into SALOTCOM
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.SALOTCOM_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.SALOTCOM_idx,2])
                  i_cOp1=cp_SetTrsOp(this.w_EVACAR,'SMQTAPER','this.w_EVAQT1',this.w_EVAQT1,'update',i_nConn)
                  i_cOp2=cp_SetTrsOp(this.w_EVARIS,'SMQTRPER','this.w_EVAQT1',this.w_EVAQT1,'update',i_nConn)
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.SALOTCOM_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"SMQTAPER ="+cp_NullLink(i_cOp1,'SALOTCOM','SMQTAPER');
                  +",SMQTRPER ="+cp_NullLink(i_cOp2,'SALOTCOM','SMQTRPER');
                      +i_ccchkf ;
                  +" where ";
                      +"SMCODART = "+cp_ToStrODBC(this.w_EVAKEY);
                      +" and SMCODMAG = "+cp_ToStrODBC(this.w_EVAMAG);
                      +" and SMCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                      +" and SMCODUBI = "+cp_ToStrODBC(this.w_EVAUBI);
                      +" and SMCODLOT = "+cp_ToStrODBC(this.w_EVALOT);
                         )
                else
                  update (i_cTable) set;
                      SMQTAPER = &i_cOp1.;
                      ,SMQTRPER = &i_cOp2.;
                      &i_ccchkf. ;
                   where;
                      SMCODART = this.w_EVAKEY;
                      and SMCODMAG = this.w_EVAMAG;
                      and SMCODCAN = this.w_COMMAPPO;
                      and SMCODUBI = this.w_EVAUBI;
                      and SMCODLOT = this.w_EVALOT;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error='Errore Aggiornamento Saldi Commessa'
                  return
                endif
              endif
            endif
          endif
          if NOT EMPTY(this.w_EVAMAT) AND NOT EMPTY(this.w_EV2CAR+this.w_EV2ORD+this.w_EV2IMP+this.w_EV2RIS)
            * --- Write into SALDIART
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDIART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_EV2CAR,'SLQTAPER','this.w_EVAQT1',this.w_EVAQT1,'update',i_nConn)
              i_cOp2=cp_SetTrsOp(this.w_EV2RIS,'SLQTRPER','this.w_EVAQT1',this.w_EVAQT1,'update',i_nConn)
              i_cOp3=cp_SetTrsOp(this.w_EV2ORD,'SLQTOPER','this.w_EVAQT1',this.w_EVAQT1,'update',i_nConn)
              i_cOp4=cp_SetTrsOp(this.w_EV2IMP,'SLQTIPER','this.w_EVAQT1',this.w_EVAQT1,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SLQTAPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTAPER');
              +",SLQTRPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTRPER');
              +",SLQTOPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTOPER');
              +",SLQTIPER ="+cp_NullLink(i_cOp4,'SALDIART','SLQTIPER');
                  +i_ccchkf ;
              +" where ";
                  +"SLCODICE = "+cp_ToStrODBC(this.w_EVAKEY);
                  +" and SLCODMAG = "+cp_ToStrODBC(this.w_EVAMAT);
                     )
            else
              update (i_cTable) set;
                  SLQTAPER = &i_cOp1.;
                  ,SLQTRPER = &i_cOp2.;
                  ,SLQTOPER = &i_cOp3.;
                  ,SLQTIPER = &i_cOp4.;
                  &i_ccchkf. ;
               where;
                  SLCODICE = this.w_EVAKEY;
                  and SLCODMAG = this.w_EVAMAT;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            if this.w_SALCOM="S"
              * --- Write into SALDICOM
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDICOM_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                i_cOp1=cp_SetTrsOp(this.w_EV2CAR,'SCQTAPER','this.w_EVAQT1',this.w_EVAQT1,'update',i_nConn)
                i_cOp2=cp_SetTrsOp(this.w_EV2RIS,'SCQTRPER','this.w_EVAQT1',this.w_EVAQT1,'update',i_nConn)
                i_cOp3=cp_SetTrsOp(this.w_EV2ORD,'SCQTOPER','this.w_EVAQT1',this.w_EVAQT1,'update',i_nConn)
                i_cOp4=cp_SetTrsOp(this.w_EV2IMP,'SCQTIPER','this.w_EVAQT1',this.w_EVAQT1,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
                +",SCQTRPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTRPER');
                +",SCQTOPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTOPER');
                +",SCQTIPER ="+cp_NullLink(i_cOp4,'SALDICOM','SCQTIPER');
                    +i_ccchkf ;
                +" where ";
                    +"SCCODICE = "+cp_ToStrODBC(this.w_EVAKEY);
                    +" and SCCODMAG = "+cp_ToStrODBC(this.w_EVAMAT);
                    +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                       )
              else
                update (i_cTable) set;
                    SCQTAPER = &i_cOp1.;
                    ,SCQTRPER = &i_cOp2.;
                    ,SCQTOPER = &i_cOp3.;
                    ,SCQTIPER = &i_cOp4.;
                    &i_ccchkf. ;
                 where;
                    SCCODICE = this.w_EVAKEY;
                    and SCCODMAG = this.w_EVAMAT;
                    and SCCODCAN = this.w_COMMAPPO;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore Aggiornamento Saldi Commessa'
                return
              endif
              if g_MADV="S"
                if g_PERUBI = "S" OR g_PERLOT = "S"
                  * --- Write into SALOTCOM
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.SALOTCOM_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.SALOTCOM_idx,2])
                    i_cOp1=cp_SetTrsOp(this.w_EV2CAR,'SMQTAPER','this.w_EVAQT1',this.w_EVAQT1,'update',i_nConn)
                    i_cOp2=cp_SetTrsOp(this.w_EV2RIS,'SMQTRPER','this.w_EVAQT1',this.w_EVAQT1,'update',i_nConn)
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.SALOTCOM_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"SMQTAPER ="+cp_NullLink(i_cOp1,'SALOTCOM','SMQTAPER');
                    +",SMQTRPER ="+cp_NullLink(i_cOp2,'SALOTCOM','SMQTRPER');
                        +i_ccchkf ;
                    +" where ";
                        +"SMCODART = "+cp_ToStrODBC(this.w_EVAKEY);
                        +" and SMCODMAG = "+cp_ToStrODBC(this.w_EVAMAT);
                        +" and SMCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                        +" and SMCODUBI = "+cp_ToStrODBC(this.w_EVALOT);
                        +" and SMCODLOT = "+cp_ToStrODBC(this.w_EVAUB2);
                           )
                  else
                    update (i_cTable) set;
                        SMQTAPER = &i_cOp1.;
                        ,SMQTRPER = &i_cOp2.;
                        &i_ccchkf. ;
                     where;
                        SMCODART = this.w_EVAKEY;
                        and SMCODMAG = this.w_EVAMAT;
                        and SMCODCAN = this.w_COMMAPPO;
                        and SMCODUBI = this.w_EVALOT;
                        and SMCODLOT = this.w_EVAUB2;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error='Errore Aggiornamento Saldi Commessa'
                    return
                  endif
                endif
              endif
            endif
          endif
        endif
        if NOT EMPTY(this.w_EVASER) AND this.w_EVAROW<>0 AND this.w_EVAFLA="+"
          * --- Storna Evasione Documento di Origine
          this.w_EVFLORDI = " "
          this.w_EVFLIMPE = " "
          this.w_EVFLRISE = " "
          this.w_EVEVAKEY2 = " "
          this.w_EVEVAMAG2 = " "
          this.w_EVOQTAEVA = 0
          this.w_EVOQTAEV1 = 0
          this.w_EVOQTAMOV = 0
          this.w_EVOQTAUM1 = 0
          this.w_EVOQTASAL = 0
          * --- Read from DOC_DETT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DOC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MVFLORDI,MVFLIMPE,MVFLRISE,MVKEYSAL,MVCODMAG,MVQTAEV1,MVQTAEVA,MVQTAMOV,MVQTAUM1,MVQTASAL,MVCODART,MVCODCOM,MVCODLOT,MVCODUBI"+;
              " from "+i_cTable+" DOC_DETT where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.w_EVASER);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_EVAROW);
                  +" and MVNUMRIF = "+cp_ToStrODBC(this.w_EVNUMRIF);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MVFLORDI,MVFLIMPE,MVFLRISE,MVKEYSAL,MVCODMAG,MVQTAEV1,MVQTAEVA,MVQTAMOV,MVQTAUM1,MVQTASAL,MVCODART,MVCODCOM,MVCODLOT,MVCODUBI;
              from (i_cTable) where;
                  MVSERIAL = this.w_EVASER;
                  and CPROWNUM = this.w_EVAROW;
                  and MVNUMRIF = this.w_EVNUMRIF;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_EVFLORDI = NVL(cp_ToDate(_read_.MVFLORDI),cp_NullValue(_read_.MVFLORDI))
            this.w_EVFLIMPE = NVL(cp_ToDate(_read_.MVFLIMPE),cp_NullValue(_read_.MVFLIMPE))
            this.w_EVFLRISE = NVL(cp_ToDate(_read_.MVFLRISE),cp_NullValue(_read_.MVFLRISE))
            this.w_EVEVAKEY2 = NVL(cp_ToDate(_read_.MVKEYSAL),cp_NullValue(_read_.MVKEYSAL))
            this.w_EVEVAMAG2 = NVL(cp_ToDate(_read_.MVCODMAG),cp_NullValue(_read_.MVCODMAG))
            this.w_EVOQTAEV1 = NVL(cp_ToDate(_read_.MVQTAEV1),cp_NullValue(_read_.MVQTAEV1))
            this.w_EVOQTAEVA = NVL(cp_ToDate(_read_.MVQTAEVA),cp_NullValue(_read_.MVQTAEVA))
            this.w_EVOQTAMOV = NVL(cp_ToDate(_read_.MVQTAMOV),cp_NullValue(_read_.MVQTAMOV))
            this.w_EVOQTAUM1 = NVL(cp_ToDate(_read_.MVQTAUM1),cp_NullValue(_read_.MVQTAUM1))
            this.w_EVOQTASAL = NVL(cp_ToDate(_read_.MVQTASAL),cp_NullValue(_read_.MVQTASAL))
            this.w_EVAART2 = NVL(cp_ToDate(_read_.MVCODART),cp_NullValue(_read_.MVCODART))
            this.w_EVACOM2 = NVL(cp_ToDate(_read_.MVCODCOM),cp_NullValue(_read_.MVCODCOM))
            this.w_EVALOT2 = NVL(cp_ToDate(_read_.MVCODLOT),cp_NullValue(_read_.MVCODLOT))
            this.w_EVAUB3 = NVL(cp_ToDate(_read_.MVCODUBI),cp_NullValue(_read_.MVCODUBI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_EVNQTAEVA = this.w_EVOQTAEVA - this.w_EVQIMP
          this.w_EVNQTAEV1 = this.w_EVOQTAEV1 - this.w_EVQIM1
          this.w_EVNQTASAL = IIF(this.w_EVNQTAEV1>this.w_EVOQTAUM1, 0, this.w_EVOQTAUM1-this.w_EVNQTAEV1)
          * --- Write into DOC_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DOC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVQTAEVA ="+cp_NullLink(cp_ToStrODBC(this.w_EVNQTAEVA),'DOC_DETT','MVQTAEVA');
            +",MVQTAEV1 ="+cp_NullLink(cp_ToStrODBC(this.w_EVNQTAEV1),'DOC_DETT','MVQTAEV1');
            +",MVQTASAL ="+cp_NullLink(cp_ToStrODBC(this.w_EVNQTASAL),'DOC_DETT','MVQTASAL');
            +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLEVAS');
                +i_ccchkf ;
            +" where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_EVASER);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_EVAROW);
                +" and MVNUMRIF = "+cp_ToStrODBC(this.w_EVNUMRIF);
                   )
          else
            update (i_cTable) set;
                MVQTAEVA = this.w_EVNQTAEVA;
                ,MVQTAEV1 = this.w_EVNQTAEV1;
                ,MVQTASAL = this.w_EVNQTASAL;
                ,MVFLEVAS = " ";
                &i_ccchkf. ;
             where;
                MVSERIAL = this.w_EVASER;
                and CPROWNUM = this.w_EVAROW;
                and MVNUMRIF = this.w_EVNUMRIF;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          if NOT EMPTY(this.w_EVEVAKEY2) AND NOT EMPTY(this.w_EVEVAMAG2) AND NOT EMPTY(this.w_EVFLORDI+this.w_EVFLIMPE+this.w_EVFLRISE)
            this.w_EVNQTASAL = this.w_EVNQTASAL - this.w_EVOQTASAL
            * --- Se il documento di evasione componenti resta evaso non devo stornare solo la qtaeva 
            *     ma quanto rimasto aperto del documento ovvero w_NQTASAL
            this.w_EVQIM1 = IIF(this.w_EVAFLE="S",this.w_EVNQTASAL,this.w_EVQIM1)
            * --- Write into SALDIART
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDIART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_EVFLRISE,'SLQTRPER','this.w_EVQIM1',this.w_EVQIM1,'update',i_nConn)
              i_cOp2=cp_SetTrsOp(this.w_EVFLORDI,'SLQTOPER','this.w_EVQIM1',this.w_EVQIM1,'update',i_nConn)
              i_cOp3=cp_SetTrsOp(this.w_EVFLIMPE,'SLQTIPER','this.w_EVQIM1',this.w_EVQIM1,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SLQTRPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTRPER');
              +",SLQTOPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTOPER');
              +",SLQTIPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTIPER');
                  +i_ccchkf ;
              +" where ";
                  +"SLCODICE = "+cp_ToStrODBC(this.w_EVEVAKEY2);
                  +" and SLCODMAG = "+cp_ToStrODBC(this.w_EVEVAMAG2);
                     )
            else
              update (i_cTable) set;
                  SLQTRPER = &i_cOp1.;
                  ,SLQTOPER = &i_cOp2.;
                  ,SLQTIPER = &i_cOp3.;
                  &i_ccchkf. ;
               where;
                  SLCODICE = this.w_EVEVAKEY2;
                  and SLCODMAG = this.w_EVEVAMAG2;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            * --- Read from ART_ICOL
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ART_ICOL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ARSALCOM"+;
                " from "+i_cTable+" ART_ICOL where ";
                    +"ARCODART = "+cp_ToStrODBC(this.w_EVAART2);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ARSALCOM;
                from (i_cTable) where;
                    ARCODART = this.w_EVAART2;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if this.w_SALCOM="S"
              if empty(nvl(this.w_EVACOM2,""))
                this.w_COMMAPPO = this.w_COMMDEFA
              else
                this.w_COMMAPPO = this.w_EVACOM2
              endif
              * --- Write into SALDICOM
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDICOM_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                i_cOp1=cp_SetTrsOp(this.w_EVFLRISE,'SCQTRPER','this.w_EVQIM1',this.w_EVQIM1,'update',i_nConn)
                i_cOp2=cp_SetTrsOp(this.w_EVFLORDI,'SCQTOPER','this.w_EVQIM1',this.w_EVQIM1,'update',i_nConn)
                i_cOp3=cp_SetTrsOp(this.w_EVFLIMPE,'SCQTIPER','this.w_EVQIM1',this.w_EVQIM1,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SCQTRPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTRPER');
                +",SCQTOPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTOPER');
                +",SCQTIPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTIPER');
                    +i_ccchkf ;
                +" where ";
                    +"SCCODICE = "+cp_ToStrODBC(this.w_EVEVAKEY2);
                    +" and SCCODMAG = "+cp_ToStrODBC(this.w_EVEVAMAG2);
                    +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                       )
              else
                update (i_cTable) set;
                    SCQTRPER = &i_cOp1.;
                    ,SCQTOPER = &i_cOp2.;
                    ,SCQTIPER = &i_cOp3.;
                    &i_ccchkf. ;
                 where;
                    SCCODICE = this.w_EVEVAKEY2;
                    and SCCODMAG = this.w_EVEVAMAG2;
                    and SCCODCAN = this.w_COMMAPPO;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              if g_MADV="S"
                if g_PERUBI = "S" OR g_PERLOT = "S"
                  * --- Write into SALOTCOM
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.SALOTCOM_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.SALOTCOM_idx,2])
                    i_cOp1=cp_SetTrsOp(this.w_EVFLRISE,'SMQTRPER','this.w_EVQIM1',this.w_EVQIM1,'update',i_nConn)
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.SALOTCOM_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"SMQTRPER ="+cp_NullLink(i_cOp1,'SALOTCOM','SMQTRPER');
                        +i_ccchkf ;
                    +" where ";
                        +"SMCODART = "+cp_ToStrODBC(this.w_EVEVAKEY2);
                        +" and SMCODMAG = "+cp_ToStrODBC(this.w_EVEVAMAG2);
                        +" and SMCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                        +" and SMCODLOT = "+cp_ToStrODBC(this.w_EVALOT2);
                        +" and SMCODUBI = "+cp_ToStrODBC(this.w_EVAUB3);
                           )
                  else
                    update (i_cTable) set;
                        SMQTRPER = &i_cOp1.;
                        &i_ccchkf. ;
                     where;
                        SMCODART = this.w_EVEVAKEY2;
                        and SMCODMAG = this.w_EVEVAMAG2;
                        and SMCODCAN = this.w_COMMAPPO;
                        and SMCODLOT = this.w_EVALOT2;
                        and SMCODUBI = this.w_EVAUB3;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                endif
              endif
            endif
          endif
        endif
      endif
        select _Curs_TMPDETTI
        continue
      enddo
      use
    endif
    * --- Drop temporary table TMPDETTI
    i_nIdx=cp_GetTableDefIdx('TMPDETTI')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPDETTI')
    endif
    * --- Aggiorna saldi lotti  prima di eliminare i documenti
    if g_MADV="S"
      GSMD_BRL (this, this.pSEREVA , "V" , "-" , ,.T.)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Elimina Documento
    if this.w_OK
      * --- Elimino il documento di evasione componenti
      GSAR_BED(this,this.pSEREVA, -20)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.pDOCRIG
        * --- Write into DOC_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVRIFESC ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'DOC_DETT','MVRIFESC');
              +i_ccchkf ;
          +" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.pSERIAL);
              +" and CPROWNUM = "+cp_ToStrODBC(this.pROWORI);
                 )
        else
          update (i_cTable) set;
              MVRIFESC = SPACE(10);
              &i_ccchkf. ;
           where;
              MVSERIAL = this.pSERIAL;
              and CPROWNUM = this.pROWORI;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      else
        * --- Write into DOC_MAST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DOC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVRIFESP ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'DOC_MAST','MVRIFESP');
              +i_ccchkf ;
          +" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.pSERIAL);
                 )
        else
          update (i_cTable) set;
              MVRIFESP = SPACE(10);
              &i_ccchkf. ;
           where;
              MVSERIAL = this.pSERIAL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    endif
    i_retcode = 'stop'
    i_retval = this.w_RETVAL
    return
  endproc


  proc Init(oParentObject,pSEREVA,pSERIAL,pROWORI,pDOCRIG)
    this.pSEREVA=pSEREVA
    this.pSERIAL=pSERIAL
    this.pROWORI=pROWORI
    this.pDOCRIG=pDOCRIG
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='DOC_DETT'
    this.cWorkTables[2]='DOC_MAST'
    this.cWorkTables[3]='SALDIART'
    this.cWorkTables[4]='*TMPDETTI'
    this.cWorkTables[5]='ART_ICOL'
    this.cWorkTables[6]='SALDICOM'
    this.cWorkTables[7]='SALOTCOM'
    return(this.OpenAllTables(7))

  proc CloseCursors()
    if used('_Curs_TMPDETTI')
      use in _Curs_TMPDETTI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pSEREVA,pSERIAL,pROWORI,pDOCRIG"
endproc
