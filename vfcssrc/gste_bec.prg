* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bec                                                        *
*              Estratto conto                                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_480]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-04                                                      *
* Last revis.: 2014-11-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bec",oParentObject)
return(i_retval)

define class tgste_bec as StdBatch
  * --- Local variables
  w_CAOVAL = 0
  w_TOTIMP = 0
  w_ARRO = 0
  w_OCODVAL = space(6)
  w_CODVAL = space(5)
  w_SIMCLF = space(5)
  w_VALDES = space(5)
  w_ERROR = .f.
  w_CAODES = 0
  w_QUERYP = space(50)
  w_SIMVAL = space(5)
  w_APPO = ctod("  /  /  ")
  w_APPO1 = ctod("  /  /  ")
  w_APPO2 = space(10)
  w_MESS = space(8)
  w_SERIAL = space(10)
  w_PUNT0 = 0
  w_PUNT = 0
  w_SALDO = 0
  w_OK = 0
  w_PERDES = .f.
  w_MASCHERA = .NULL.
  w_TEST = .f.
  w_DTOBSO = ctod("  /  /  ")
  w_GIORIS = 0
  w_DATGIO = ctod("  /  /  ")
  w_TIPO = space(1)
  w_INDIRIZ = space(35)
  w_LOCAL = space(30)
  w_CAP = space(8)
  w_PROV = space(2)
  w_CODDES = space(5)
  w_TIPO = space(1)
  w_CODICE = space(15)
  w_NAZION = space(3)
  w_OLDCOD = space(15)
  w_NOME = space(40)
  w_piva = space(12)
  w_tele = space(18)
  w_mail = space(50)
  w_fax = space(18)
  w_web = space(50)
  w_OLDTIP = space(1)
  w_TIPCON = space(1)
  w_CODCON = space(15)
  w_ORDINE = 0
  w_DATSCA = ctod("  /  /  ")
  w_NUMPAR = space(31)
  w_CODVAL = space(3)
  w_FLCRSA = space(1)
  w_IMPDAR = 0
  w_IMPAVE = 0
  w_SIMVAL = space(5)
  w_DECVAL = 0
  w_MODPAG = space(10)
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_DATDOC = ctod("  /  /  ")
  w_FLSOSP1 = space(1)
  w_DESCON = space(40)
  w_INDIRI = space(35)
  w_LOCALI = space(30)
  w_CAPCLF = space(8)
  w_PROVIN = space(2)
  w_VALCLF = space(3)
  w_SIMCLF = space(5)
  w_ARRCLF = 0
  w_TOTIMP = 0
  w_CAOAPP = 0
  w_PTFLRAGG = space(1)
  w_FLDAVE = space(1)
  w_LCONRIF = space(15)
  w_LCHKRIF = space(1)
  w_RAGSOC = space(40)
  w_ODATSCA = ctod("  /  /  ")
  w_ONUMPAR = space(31)
  w_OCODAGE = space(5)
  w_ODESAGE = space(35)
  w_FLRAGGCR = space(1)
  w_SERIALECR = space(10)
  w_VALFILTER = space(254)
  w_CODSORG = space(20)
  w_PARAMETR = space(40)
  w_CODICEAZ = space(40)
  w_DATAFILT = ctod("  /  /  ")
  w_LROWORD = 0
  w_LROWNUM = 0
  w_LSERIAL = space(10)
  w_ParCont = 0
  w_NumUpd = 0
  w_CODRAG = space(77)
  w_PTROWORD = 0
  w_CPROWNUM = 0
  w_PTSERRIF = space(10)
  w_PTORDRIF = 0
  w_PTNUMRIF = 0
  w_TIPDOC = space(2)
  w_FLRAGG = 0
  w_SIMVAL = space(5)
  * --- WorkFile variables
  AZIENDA_idx=0
  CAM_BI_idx=0
  VALUTE_idx=0
  CONTI_idx=0
  TMP_ESTRATTOCONTO_idx=0
  DES_DIVE_idx=0
  ZOOMPART_idx=0
  PAR_TITE_idx=0
  TMP_PART_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elaborazione Estratto Conto Clienti/Fornitori (da GSTE_KSC)
    * --- Parametri dalla Maschera di Selezione
    * --- Serve per lanciare la stampa numero 2 dell'output utente: Analisi Scaduto per Partita
    *     Vengono inizializzate le selezioni impostate nella maschera estratto conto anche direttamente in quella dell'analisi scaduto.
    *     Viene lanciato il batch che esegue la stampa.
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZPIVAZI,AZTELEFO,AZTELFAX,AZINDWEB,AZ_EMAIL,AZGIORIS"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(i_codazi);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZPIVAZI,AZTELEFO,AZTELFAX,AZINDWEB,AZ_EMAIL,AZGIORIS;
        from (i_cTable) where;
            AZCODAZI = i_codazi;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_piva = NVL(cp_ToDate(_read_.AZPIVAZI),cp_NullValue(_read_.AZPIVAZI))
      this.w_tele = NVL(cp_ToDate(_read_.AZTELEFO),cp_NullValue(_read_.AZTELEFO))
      this.w_fax = NVL(cp_ToDate(_read_.AZTELFAX),cp_NullValue(_read_.AZTELFAX))
      this.w_web = NVL(cp_ToDate(_read_.AZINDWEB),cp_NullValue(_read_.AZINDWEB))
      this.w_mail = NVL(cp_ToDate(_read_.AZ_EMAIL),cp_NullValue(_read_.AZ_EMAIL))
      this.w_gioris = NVL(cp_ToDate(_read_.AZGIORIS),cp_NullValue(_read_.AZGIORIS))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- I giorni di tolleranza sono da considerare solo per i clienti...
    if this.oParentObject.w_TIPCLF = "C"
      this.w_DATGIO = this.oParentObject.w_DATSTA-this.w_GIORIS
    else
      this.w_DATGIO = this.oParentObject.w_DATSTA
    endif
    if this.oParentObject.w_FLANSCA="S"
      * --- Chiamo la maschera Analisi Scaduto
      this.w_MASCHERA = GSTE_SCA()
      * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
      if !(this.w_MASCHERA.bSec1)
        i_retcode = 'stop'
        return
      endif
      this.w_MASCHERA.w_SCAINI = this.oParentObject.w_SCAINI
      this.w_MASCHERA.w_SCAFIN = this.oParentObject.w_SCAFIN
      this.w_MASCHERA.w_DATSTA = this.oParentObject.w_DATSTA
      this.w_MASCHERA.w_TIPCON = this.oParentObject.w_TIPCLF
      this.w_MASCHERA.w_CODCON = this.oParentObject.w_CODINI
      this.w_MASCHERA.w_CODFIN = this.oParentObject.w_CODFIN
      this.w_MASCHERA.w_FLORIG = this.oParentObject.w_FLORIG
      this.w_MASCHERA.w_DESCON = this.oParentObject.w_DESCRI1
      this.w_MASCHERA.w_DESFIN = this.oParentObject.w_DESCRI2
      this.w_MASCHERA.w_FLANSCA = this.oParentObject.w_FLANSCA
      this.w_MASCHERA.w_FLSOSP = this.oParentObject.w_FLSOSP
      this.w_MASCHERA.SetControlsValue()     
      this.w_MASCHERA.NotifyEvent("Output")     
    else
      * --- Se stampa Dettagliata ordino per ragione sociale e non per codice conto
      this.w_PERDES = IIF(SUBSTR(UPPER(ALLTRIM(this.oParentObject.w_OREP)),AT("GSTE",UPPER(ALLTRIM(this.oParentObject.w_OREP))),8)="GSTE1QEC",.T.,.F.)
      * --- Creo il cursore che contiene la messaggistica d'errore
      this.w_ERROR = .F.
      CREATE CURSOR MessErr (COD C(3), MSG C(120))
      this.w_QUERYP = ALLTRIM(this.oParentObject.w_OQRY)
      this.w_TIPO = IIF(this.oParentObject.w_FLSALD = "T","VV","")
      GSTE_BPA (this, this.oParentObject.w_SCAINI , this.oParentObject.w_SCAFIN , this.w_TIPCON , this.oParentObject.w_CODINI ,this.oParentObject.w_CODFIN , this.w_CODVAL , "", "RB","BO","RD","RI","MA","CA", "GSCG_BSA", "N", this.oParentObject.w_DATPAR )
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      do case
        case this.oParentObject.w_FLSALD = "T"
          * --- Test saldate posto a tutte
          vq_exec(this.w_QUERYP, this, "PARTITE")
        case this.oParentObject.w_FLSALD = "R"
          * --- Solo parte aperta
          * --- Create temporary table ZOOMPART
          i_nIdx=cp_AddTableDef('ZOOMPART') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec(''+this.w_QUERYP+'',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.ZOOMPART_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          if this.oParentObject.w_ESPORTAL<>"S" AND this.oParentObject.w_EFFETTI="A"
            this.Pag5()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            vq_exec("QUERY\GSTEPQEC.VQR", this, "PARTITE")
          else
            vq_exec(this.w_QUERYP, this, "PARTITE")
          endif
          * --- Filtra le Partite Saldate legge le partite ancora aperte..
          GSTE_BCP(this,"A", "PARTITE"," ")
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.oParentObject.w_ESPORTAL="S" AND this.oParentObject.w_EFFETTI="A"
            this.Pag5()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Inserisco il valore per il campo INSOLUTO (Partite generate da insoluti o Mancati Pagamenti)
            if Not this.w_ERROR and upper(this.OparentObject.class)="TGSTE_KIP"
              do GSTE1BEC with this
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              this.oParentObject.ecpQUIT()
              * --- Drop temporary table TMP_PART_APE
              i_nIdx=cp_GetTableDefIdx('TMP_PART_APE')
              if i_nIdx<>0
                cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
                cp_RemoveTableDef('TMP_PART_APE')
              endif
            endif
            i_retcode = 'stop'
            return
          endif
          * --- Drop temporary table ZOOMPART
          i_nIdx=cp_GetTableDefIdx('ZOOMPART')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('ZOOMPART')
          endif
        case this.oParentObject.w_FLSALD = "N"
          * --- Dettaglio partite aperte (non saldate)
          * --- Creo un temporaneo con la query messa nell'Output utente..
          * --- Create temporary table TMP_ESTRATTOCONTO
          i_nIdx=cp_AddTableDef('TMP_ESTRATTOCONTO') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec(''+this.w_QUERYP+'',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.TMP_ESTRATTOCONTO_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          * --- Eseguo query per mettere in join partite aperte e temporaneo
          vq_exec("QUERY\GSTE1BEC.VQR", this, "PARTITE")
      endcase
      if this.oParentObject.w_PARNEG="S"
        * --- Escludo le partite con saldo in negativo (debiti netti) nel caso abbia attivato
        *     il flag 'Esclude partite di acconto e creazione di segno avere
         
 DELETE FROM PARTITE WHERE; 
 (NVL(FLCRSA,"") $ "CA" AND NVL(SEGNO," ")="A") or ; 
 ( Not Empty( this.oParentObject.w_CODAGE ) and Nvl(CODAGE,SPACE(5)) <> this.oParentObject.w_CODAGE ) 
      else
        * --- Eseguo filtro per agente e tipo partita
         
 Select * FROM PARTITE WHERE ; 
 (Empty(this.oParentObject.w_FLPART1 ) Or Nvl(FLCRSA, "")= this.oParentObject.w_FLPART1 ) ; 
 And ( Empty( this.oParentObject.w_CODAGE ) Or Nvl(CODAGE,SPACE(5)) = this.oParentObject.w_CODAGE ) ; 
 Into Cursor PARTITE
      endif
      * --- Elabora i Cursori e ritorna il Temporaneo per la Stampa
      if this.oParentObject.w_ESPORTAL<>"S"
        * --- Drop temporary table TMP_PART_APE
        i_nIdx=cp_GetTableDefIdx('TMP_PART_APE')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMP_PART_APE')
        endif
      endif
      SELECT PARTITE
      GO TOP
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Seleziono il cursore da stampare
      ah_Msg("Ricerca partite raggruppate...",.T.)
      if this.oParentObject.w_FLORIG="S"
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      if USED("PARTITE")
        if this.oParentObject.w_SEDE<>"NO"
          this.w_OLDCOD = REPL("#", 15)
          this.w_TEST = .F.
          * --- Se ho  lo stesso Cliente o Fornitore evito di rileggere la Destinazione diversa
          SELECT PARTITE
          GO TOP
          SCAN 
          if this.w_OLDCOD<>NVL(CODCON, SPACE(15))
            this.w_TEST = .F.
            this.w_CODICE = NVL(CODCON, SPACE(15))
            this.w_TIPO = NVL(TIPCON, " ")
            * --- Controllo se esiste una sede predefinita.
            *     In questo caso stampo quella
            * --- Read from DES_DIVE
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.DES_DIVE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "DDINDIRI,DD___CAP,DDLOCALI,DDPROVIN,DDCODNAZ,DDNOMDES,DDDTOBSO"+;
                " from "+i_cTable+" DES_DIVE where ";
                    +"DDCODICE = "+cp_ToStrODBC(this.w_CODICE);
                    +" and DDTIPCON = "+cp_ToStrODBC(this.w_TIPO);
                    +" and DDTIPRIF = "+cp_ToStrODBC(this.oParentObject.w_SEDE);
                    +" and DDPREDEF = "+cp_ToStrODBC("S");
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                DDINDIRI,DD___CAP,DDLOCALI,DDPROVIN,DDCODNAZ,DDNOMDES,DDDTOBSO;
                from (i_cTable) where;
                    DDCODICE = this.w_CODICE;
                    and DDTIPCON = this.w_TIPO;
                    and DDTIPRIF = this.oParentObject.w_SEDE;
                    and DDPREDEF = "S";
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_INDIRIZ = NVL(cp_ToDate(_read_.DDINDIRI),cp_NullValue(_read_.DDINDIRI))
              this.w_CAP = NVL(cp_ToDate(_read_.DD___CAP),cp_NullValue(_read_.DD___CAP))
              this.w_LOCAL = NVL(cp_ToDate(_read_.DDLOCALI),cp_NullValue(_read_.DDLOCALI))
              this.w_PROV = NVL(cp_ToDate(_read_.DDPROVIN),cp_NullValue(_read_.DDPROVIN))
              this.w_NAZION = NVL(cp_ToDate(_read_.DDCODNAZ),cp_NullValue(_read_.DDCODNAZ))
              this.w_NOME = NVL(cp_ToDate(_read_.DDNOMDES),cp_NullValue(_read_.DDNOMDES))
              this.w_DTOBSO = NVL(cp_ToDate(_read_.DDDTOBSO),cp_NullValue(_read_.DDDTOBSO))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if I_ROWS>0 AND this.oParentObject.w_DATSTA<this.w_DTOBSO
              this.w_TEST = .T.
            else
              * --- Select from DES_DIVE
              i_nConn=i_TableProp[this.DES_DIVE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select * from "+i_cTable+" DES_DIVE ";
                    +" where DDTIPCON="+cp_ToStrODBC(this.w_TIPO)+" AND DDCODICE="+cp_ToStrODBC(this.w_CODICE)+" AND DDTIPRIF= "+cp_ToStrODBC(this.oParentObject.w_SEDE)+"";
                     ,"_Curs_DES_DIVE")
              else
                select * from (i_cTable);
                 where DDTIPCON=this.w_TIPO AND DDCODICE=this.w_CODICE AND DDTIPRIF= this.oParentObject.w_SEDE;
                  into cursor _Curs_DES_DIVE
              endif
              if used('_Curs_DES_DIVE')
                select _Curs_DES_DIVE
                locate for 1=1
                do while not(eof())
                if _Curs_DES_DIVE.DDDTOBSO>this.oParentObject.w_DATSTA Or EMPTY(NVL(_Curs_DES_DIVE.DDDTOBSO, cp_CharToDate("  -  -    ")))
                  this.w_INDIRIZ = _Curs_DES_DIVE.DDINDIRI
                  this.w_CAP = _Curs_DES_DIVE.DD___CAP
                  this.w_LOCAL = _Curs_DES_DIVE.DDLOCALI
                  this.w_PROV = _Curs_DES_DIVE.DDPROVIN
                  this.w_CODDES = _Curs_DES_DIVE.DDCODDES
                  this.w_TIPO = _Curs_DES_DIVE.DDTIPCON
                  this.w_CODICE = _Curs_DES_DIVE.DDCODICE
                  this.w_NAZION = _Curs_DES_DIVE.DDCODNAZ
                  this.w_NOME = _Curs_DES_DIVE.DDNOMDES
                  this.w_TEST = .T.
                  EXIT
                endif
                  select _Curs_DES_DIVE
                  continue
                enddo
                use
              endif
            endif
            this.w_OLDCOD = this.w_CODICE
          endif
          SELECT PARTITE
          if this.w_TEST
            Replace RAGSOC with this.w_NOME, INDIRI with this.w_INDIRIZ, LOCALI with this.w_LOCAL, CAPCLF with this.w_CAP, PROVIN with this.w_PROV 
          endif
          ENDSCAN
        endif
        do case
          case this.oParentObject.w_TIPORD="P" AND this.oParentObject.w_TIPRAG<>"S"
            ah_Msg("Ordina per partita...",.T.)
            if this.w_PERDES
              * --- Stampa Dettagliata (OUTPUT N2)
              if this.oParentObject.w_FLORIG="S"
                SELECT * FROM PARTITE INTO CURSOR __tmp__ ORDER BY descon,tipcon,codcon,ordine,numpar,datsca,flragg,ptnumpar,ptdatsca
              else
                SELECT * FROM PARTITE INTO CURSOR __tmp__ ORDER BY descon,tipcon,codcon,ordine,numpar,datsca
              endif
            else
              if this.oParentObject.w_FLORIG="S"
                SELECT * FROM PARTITE INTO CURSOR __tmp__ ORDER BY tipcon,codcon,ordine,numpar,datsca,flragg,ptnumpar,ptdatsca
              else
                SELECT * FROM PARTITE INTO CURSOR __tmp__ ORDER BY tipcon,codcon,ordine,numpar,datsca
              endif
            endif
          case this.oParentObject.w_TIPORD="A"
            ah_Msg("Ordina per AGENTE...",.T.)
            if this.w_PERDES
              * --- Stampa Dettagliata (OUTPUT N2)
              if this.oParentObject.w_FLORIG="S"
                SELECT * FROM PARTITE INTO CURSOR __tmp__ ORDER BY DESAGE,ORDINE,TIPCON.CODCON,DATSCA,FLRAGG,PTDATSCA,PTNUMPAR
              else
                SELECT * FROM PARTITE INTO CURSOR __tmp__ ORDER BY DESAGE,TIPCON,CODCON,ORDINE,DATSCA,NUMPAR
              endif
            else
              if this.oParentObject.w_FLORIG="S"
                SELECT * FROM PARTITE INTO CURSOR __tmp__ ORDER BY CODAGE,ORDINE,TIPCON,CODCON,DATSCA,FLRAGG,PTDATSCA,PTNUMPAR
              else
                SELECT * FROM PARTITE INTO CURSOR __tmp__ ORDER BY CODAGE,ORDINE,TIPCON,CODCON,DATSCA,NUMPAR
              endif
            endif
          otherwise
            ah_Msg("Ordina per data scadenza...",.T.)
            if this.w_PERDES
              * --- Stampa Dettagliata (OUTPUT N2)
              if this.oParentObject.w_FLORIG="S"
                SELECT * FROM PARTITE INTO CURSOR __tmp__ ORDER BY descon,tipcon,codcon,ordine,datsca,numpar,flragg,ptdatsca,ptnumpar
              else
                SELECT * FROM PARTITE INTO CURSOR __tmp__ ORDER BY descon,tipcon,codcon,ordine,datsca
              endif
            else
              if this.oParentObject.w_FLORIG="S"
                SELECT * FROM PARTITE INTO CURSOR __tmp__ ORDER BY tipcon,codcon,ordine,datsca,numpar,flragg,ptdatsca,ptnumpar
              else
                SELECT * FROM PARTITE INTO CURSOR __tmp__ ORDER BY tipcon,codcon,ordine,datsca
              endif
            endif
        endcase
        SELECT PARTITE
        USE
      endif
      * --- dati che vengono passati nel report
      L_Datsta= this.oParentObject.w_DATSTA
      L_tele=this.w_tele
      L_fax=this.w_fax
      L_piva=this.w_piva
      L_mail=this.w_mail
      L_Valcli= this.oParentObject.w_VALCLI
      L_web=this.w_web
      L_Tipclf= this.oParentObject.w_TIPCLF
      L_Tipori = IIF(this.oParentObject.w_TIPORD="P", this.oParentObject.w_TIPRAG, "T")
      L_FLORIG=this.oParentObject.w_FLORIG
      * --- Seleziono il bitmap da stampare
      L_LOGO=GETBMP(I_CODAZI)
      if this.w_ERROR=.T.
        ah_ErrorMsg("Nell'elaborazione sono presenti cambi inesistenti o non aggiornati da pi� di 7 giorni%0Al termine della stampa estratto conto verr� eseguito un report della situazione",,"")
      endif
      * --- Predispone dati per preimpostazione destinatario corporate portal
      if g_IZCP $ "SA"
        if Not(Empty(this.oParentObject.w_CODINI)) and this.oParentObject.w_CODINI=this.oParentObject.w_CODFIN
          g_ZCPTINTEST = this.oParentObject.w_TIPCLF
          g_ZCPCINTEST = this.oParentObject.w_CODINI
          g_ZCPALLENAME = "EC"+this.oParentObject.w_TIPCLF+"_"+alltrim(i_CODAZI)+"_"+Alltrim(this.oParentObject.w_CODINI)+"_"+Dtos(this.oParentObject.w_SCAINI)+"_"+Dtos(this.oParentObject.w_SCAFIN)+".PDF"
        else
          g_ZCPTINTEST = "G"
          g_ZCPCINTEST = " "
          g_ZCPALLENAME = "EC"+this.oParentObject.w_TIPCLF+"_"+alltrim(i_CODAZI)+"_"+Dtos(this.oParentObject.w_SCAINI)+"_"+Dtos(this.oParentObject.w_SCAFIN)+".PDF"
        endif
        g_ZCPALLETITLE = ah_Msgformat("E/C scadenze dal %1 al %2",dtoc(this.oParentObject.w_SCAINI), dtoc(this.oParentObject.w_SCAFIN) )
      endif
      * --- Valorizzo gli indirizzi Email
      if this.oParentObject.w_CODINI = this.oParentObject.w_CODFIN and not empty(this.oParentObject.w_CODINI)
        * --- valorizza le variabili pubbliche i_EMAIL, i_EMAIL_PEC  e i_FAXNO
        GSAR_BRM(this,"E_M",this.oParentObject.w_TIPCLF,this.oParentObject.w_CODINI,"PA","V")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Valorizzo le variabili globali per il Fax (per la selezione del numero di telefono)
        i_DEST = ALLTRIM(this.oParentObject.w_DESCRI1)
        i_CLIFORDES = this.oParentObject.w_TIPCLF 
 i_CODDES = this.oParentObject.w_CODINI 
 i_TIPDES = "PA"
      endif
      * --- Lancio il report
      CP_CHPRN( ALLTRIM(this.oParentObject.w_OREP) , "X", this.oParentObject )
      * --- Pulizia
      if this.oParentObject.w_CODINI = this.oParentObject.w_CODFIN and not empty(this.oParentObject.w_CODINI)
        i_EMAIL = " "
        i_EMAIL_PEC = " "
        * --- Resetto le variabili globali per il Fax
        i_CLIFORDES = " " 
 i_CODDES = " " 
 i_TIPDES = " " 
 i_DEST=" "
      endif
      if g_IZCP $ "SA"
        g_ZCPTINTEST = " "
        g_ZCPCINTEST = " "
        g_ZCPALLENAME = " "
        g_ZCPALLETITLE = " "
      endif
      * --- Chiudo i Cursori utilizzati
      * --- Se c'� un errore
      if this.w_ERROR=.T.
        if ah_YesNo("Stampo riepilogo errori?")
          SELECT MAX(MSG) AS MSG FROM MessErr GROUP BY COD INTO CURSOR __TMP__
          CP_CHPRN("QUERY\GSVE_BCV.FRX", " ", this )
        endif
      endif
      if USED("MessErr")
        SELECT MessErr
        USE
      endif
      if USED("PARTITE")
        SELECT PARTITE
        USE
      endif
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola il Cursore definitivo per la Stampa
    do case
      case this.oParentObject.w_TIPRAG="S"
        ah_Msg("Raggruppa a saldo scadenza...",.T.)
        * --- Ordino per:
        *     Tipo conto
        *     Codice Conto/Descrizione Conto
        *     Data Scadenza
        *     Numero Partita
        *     Valuta
        L_FieldDatsca=" DatSca As DatSca "
        if this.oParentObject.w_ONUME=6
          L_FieldNumpar=" MIN(Numpar) as Numpar "
          if this.w_PERDES
            L_Group = "TIPCON, DESCON ,CODVAL, DATSCA  "
          else
            L_Group = "TIPCON, CODCON ,CODVAL, DATSCA"
          endif
        else
          L_FieldNumpar=" Numpar as Numpar "
          if this.w_PERDES
            L_Group = "TIPCON, DESCON ,DATSCA , NUMPAR , CODVAL  "
          else
            L_Group = "TIPCON, CODCON ,DATSCA , NUMPAR , CODVAL"
          endif
        endif
        * --- Raggruppa a Saldo Partita+Scadenza
      case this.oParentObject.w_TIPRAG="P"
        ah_Msg("Raggruppa a saldo partita+scadenza...",.T.)
        L_FieldDatsca=" Min(DatSca) As DatSca "
        L_FieldNumpar=" MIN(Numpar) as Numpar "
        * --- Ordino per:
        *     Tipo conto
        *     Codice / Descrizione Conto
        *     Numero Partita
        *     Valuta
        if this.w_PERDES
          L_Group = "TIPCON, DESCON , NUMPAR , CODVAL  "
        else
          L_Group = "TIPCON, CODCON , NUMPAR , CODVAL  "
        endif
        * --- Raggruppa a Saldo Partita
    endcase
    if this.oParentObject.w_TIPRAG $"P-S"
      if this.oParentObject.w_ONUME=6
         
 SELECT TIPCON AS TIPCON, CODCON AS CODCON, 0 AS ORDINE, ; 
 &L_FieldDatsca, MIN(NUMPAR) AS NUMPAR, MIN(FLCRSA) AS FLCRSA, CODVAL AS CODVAL, ; 
 SUM(IMPDAR*0) AS IMPDAR, SUM(IMPAVE*0) AS IMPAVE, MAX(TOTIMP) AS TOTIMP, ; 
 MAX(DECVAL) AS DECVAL, MAX(SIMVAL) AS SIMVAL, ; 
 MAX(DESCON) AS DESCON, MAX(INDIRI) AS INDIRI, MAX(LOCALI) AS LOCALI, ; 
 MAX(CAPCLF) AS CAPCLF, MAX(PROVIN) AS PROVIN, MAX(VALCLF) AS VALCLF, ; 
 MAX(NUMDOC) AS NUMDOC, MAX(ALFDOC) AS ALFDOC, MAX(DATDOC) AS DATDOC, ; 
 MAX(MODPAG) AS MODPAG, MAX(FLSOSP) AS FLSOSP, ; 
 MIN(SIMCLF) AS SIMCLF, MIN(ARRCLF) AS ARRCLF, MAX(CAOAPP) AS CAOAPP, ; 
 MIN(FLRAGG) AS FLRAGG, MAX(FLDAVE) AS FLDAVE, ; 
 MIN(CODCAU) AS CODCAU, MIN(MPDESCRI) AS MPDESCRI, MIN(RAGSOC) AS RAGSOC,; 
 MAX(DATRAG) AS PTDATSCA,MAX(PTNUMPAR) AS PTNUMPAR, MAX(CAOVAL) AS CAOVAL, MAX(CODAGE) AS CODAGE,MAX(DESAGE) AS DESAGE, SUM(IMPDAR) AS TOTDAR, SUM(IMPAVE) AS TOTAVE; 
 FROM PARTITE INTO CURSOR RAGGRU ; 
 GROUP BY &L_Group; 
 HAVING SUM(IMPDAR)-SUM(IMPAVE) <> 0 ORDER BY &L_Group
         
 SELECT TIPCON AS TIPCON, CODCON AS CODCON, 0 AS ORDINE, ; 
 &L_FieldDatsca, NUMPAR AS NUMPAR, FLCRSA AS FLCRSA, CODVAL AS CODVAL, ; 
 IMPDAR AS IMPDAR, IMPAVE AS IMPAVE, TOTIMP AS TOTIMP, ; 
 DECVAL AS DECVAL, SIMVAL AS SIMVAL, ; 
 DESCON AS DESCON, INDIRI AS INDIRI, LOCALI AS LOCALI, ; 
 CAPCLF AS CAPCLF, PROVIN AS PROVIN, VALCLF AS VALCLF, ; 
 NUMDOC AS NUMDOC, ALFDOC AS ALFDOC, DATDOC AS DATDOC, ; 
 MODPAG AS MODPAG, FLSOSP AS FLSOSP, ; 
 SIMCLF AS SIMCLF, ARRCLF AS ARRCLF, CAOAPP AS CAOAPP, ; 
 FLRAGG AS FLRAGG, FLDAVE AS FLDAVE, ; 
 CODCAU AS CODCAU, MPDESCRI AS MPDESCRI, RAGSOC AS RAGSOC,; 
 DATRAG AS PTDATSCA,PTNUMPAR , CAOVAL , CODAGE,DESAGE,; 
 IMPDAR*0 AS TOTDAR, IMPAVE*0 AS TOTAVE FROM PARTITE INTO CURSOR DETSCA ORDER BY &L_Group
        SELECT * FROM RAGGRU UNION ALL SELECT * FROM DETSCA INTO CURSOR PARTITE ORDER BY &L_Group NOFILTER
      else
         
 SELECT TIPCON AS TIPCON, CODCON AS CODCON, 0 AS ORDINE, ; 
 &L_FieldDatsca, &L_FieldNumpar,MIN(FLCRSA) AS FLCRSA, CODVAL AS CODVAL, ; 
 SUM(IMPDAR) AS IMPDAR, SUM(IMPAVE) AS IMPAVE, MAX(TOTIMP) AS TOTIMP, ; 
 MAX(DECVAL) AS DECVAL, MAX(SIMVAL) AS SIMVAL, ; 
 MAX(DESCON) AS DESCON, MAX(INDIRI) AS INDIRI, MAX(LOCALI) AS LOCALI, ; 
 MAX(CAPCLF) AS CAPCLF, MAX(PROVIN) AS PROVIN, MAX(VALCLF) AS VALCLF, ; 
 MAX(NUMDOC) AS NUMDOC, MAX(ALFDOC) AS ALFDOC, MAX(DATDOC) AS DATDOC, ; 
 MAX(MODPAG) AS MODPAG, MAX(FLSOSP) AS FLSOSP, ; 
 MIN(SIMCLF) AS SIMCLF, MIN(ARRCLF) AS ARRCLF, MAX(CAOAPP) AS CAOAPP, ; 
 MIN(FLRAGG) AS FLRAGG, MAX(FLDAVE) AS FLDAVE, ; 
 MIN(CODCAU) AS CODCAU, MIN(MPDESCRI) AS MPDESCRI, MIN(RAGSOC) AS RAGSOC,; 
 MAX(DATRAG) AS PTDATSCA,MAX(PTNUMPAR) AS PTNUMPAR, MAX(CAOVAL) AS CAOVAL, MAX(CODAGE) AS CODAGE,MAX(DESAGE) AS DESAGE, SUM(IMPDAR) AS TOTDAR, SUM(IMPAVE) AS TOTAVE; 
 FROM PARTITE INTO CURSOR PARTITE ; 
 GROUP BY &L_Group; 
 HAVING SUM(IMPDAR)-SUM(IMPAVE) <> 0
      endif
    else
      * --- Se non raggruppo cmq devo impostare il temporaneo per
      *     gestire il Dettaglio Partite
       
 SELECT TIPCON AS TIPCON, CODCON AS CODCON, ORDINE AS ORDINE, DATSCA AS DATSCA, NUMPAR AS NUMPAR, CODVAL AS CODVAL, ; 
 FLCRSA AS FLCRSA, IMPDAR AS IMPDAR, IMPAVE AS IMPAVE, SIMVAL AS SIMVAL, DECVAL AS DECVAL, MODPAG AS MODPAG, ; 
 NUMDOC AS NUMDOC, ALFDOC AS ALFDOC, DATDOC AS DATDOC, FLSOSP AS FLSOSP, DESCON AS DESCON, INDIRI AS INDIRI, ; 
 LOCALI AS LOCALI, CAPCLF AS CAPCLF, PROVIN AS PROVIN, VALCLF AS VALCLF, SIMCLF AS SIMCLF, ARRCLF AS ARRCLF, ; 
 TOTIMP AS TOTIMP, CAOAPP AS CAOAPP, PTSERIAL AS PTSERIAL, PTFLRAGG AS PTFLRAGG, SEGNO AS SEGNO, ; 
 FLINDI AS FLINDI, PTROWORD AS PTROWORD, CPROWNUM AS CPROWNUM, BANAPP AS BANAPP, BANNOS AS BANNOS, CAOVAL AS CAOVAL, ; 
 CAOAPE AS CAOAPE, NUMPRO AS NUMPRO, DATREG AS DATREG, BANFIL AS BANFIL, FLRAGG AS FLRAGG, FLDAVE AS FLDAVE, ; 
 CODCAU AS CODCAU, MPDESCRI AS MPDESCRI, RAGSOC AS RAGSOC,DATRAG AS PTDATSCA,PTNUMPAR, CODAGE, IMPDAR AS TOTDAR, IMPAVE AS TOTAVE,DESAGE ; 
 FROM PARTITE INTO CURSOR PARTITE
    endif
    Cur = WrCursor( "PARTITE" )
    * --- Valorizzo eventuali campi valuta con la valuta di conto se vuoti (e relativo simbolo)
    Replace ValClf with g_PERVAL,SIMCLF with g_VALSIM For EMPTY(NVL(VALCLF,""))
    * --- Aggiorna alla Valuta di Stampa
    if USED("PARTITE")
      SELECT PARTITE
      GO TOP
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if USED("VALUTE")
      SELECT VALUTE
      USE
    endif
    if USED("RAGGRU")
      SELECT RAGGRU
      USE
    endif
    if USED("DETSCA")
      SELECT DETSCA
      USE
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna i totali per Valuta
    ah_Msg("Aggiorna alla valuta di stampa...",.T.)
    * --- Prima calcolo i Totali per Valuta / Cliente da Aggiungere alla Stampa
    *     Nel caso di Parte Aperta escludo gli acconti dal calcolo
    *     per farlo alla fine al cambio di origine
    if this.w_PERDES
      SELECT ;
      IIF(this.oParentObject.w_ONUME=5,DESAGE,DESCON) AS DESCON, ;
      IIF(this.oParentObject.w_ONUME=5,Nvl(CODAGE," "),CODCON) AS CODCON, ;
      CODVAL AS CODVAL, IIF(this.oParentObject.w_VALCLI="S", VALCLF, g_PERVAL) AS VALDES, "N" AS ACCONTI,;
      SUM(IMPDAR) AS IMPDAR, SUM(IMPAVE) AS IMPAVE, MAX(DECVAL) AS DECVAL, MAX(SIMVAL) AS SIMVAL ;
      FROM PARTITE WHERE Nvl(FLCRSA," ")<> iif(this.oParentObject.w_FLSALD="R","A","N") INTO CURSOR RIEVAL ;
      GROUP BY 1,2,3,4
    else
      SELECT ;
      IIF(this.oParentObject.w_ONUME=5,Nvl(CODAGE," "),CODCON) AS CODCON, ;
      CODVAL AS CODVAL, IIF(this.oParentObject.w_VALCLI="S", VALCLF, g_PERVAL) AS VALDES,"N" AS ACCONTI,;
      SUM(IMPDAR) AS IMPDAR, SUM(IMPAVE) AS IMPAVE, MAX(DECVAL) AS DECVAL, MAX(SIMVAL) AS SIMVAL ;
      FROM PARTITE WHERE Nvl(FLCRSA," ")<> iif(this.oParentObject.w_FLSALD="R","A","N") INTO CURSOR RIEVAL ;
      GROUP BY 1, 2, 3
    endif
     
 SELECT ; 
 CODVAL AS CODVAL, VALDES AS VALDES, ; 
 9999.9999999-9999.9999999 AS CAOVAL, ; 
 9999.9999999-9999.9999999 AS CAODES, ; 
 0 AS ARRVAL, SPACE(5) AS SIMVAL ; 
 FROM RIEVAL INTO CURSOR VALUTE ; 
 GROUP BY 1, 2
    * --- Cerco di cambiare gli importi nella valuta di conto
    * --- Rendo il cursore dei totali scrivibile per calcolare il cambio con la valuta di conto
     
 CreaCur=WRCURSOR("VALUTE") 
 SELECT VALUTE 
 GO TOP 
 SCAN FOR NOT EMPTY(NVL(CODVAL, " "))
    this.w_VALDES = g_PERVAL
    this.w_CAOVAL = GETCAM(CODVAL,this.oParentObject.w_DATSTA)
    this.w_CAODES = GETCAM(VALDES,this.oParentObject.w_DATSTA)
    this.w_ARRO = g_PERPVL
    this.w_SIMVAL = g_VALSIM
    if NOT EMPTY(NVL(CODVAL," ")) AND NOT EMPTY(NVL(VALDES, " "))
      this.w_CODVAL = CODVAL
      this.w_VALDES = VALDES
      * --- Calcola i cambi riferiti alla Valuta di Destinazione (Di Conto o Cliente/Fornitore)
      if this.w_VALDES<>g_PERVAL
        * --- Simbolo e Decimali e Cambio EURO
        this.w_CAODES = 0
        this.w_CAODES = GETCAM(VALDES,this.oParentObject.w_DATSTA)
        * --- Read from VALUTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VALUTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VASIMVAL,VADECTOT"+;
            " from "+i_cTable+" VALUTE where ";
                +"VACODVAL = "+cp_ToStrODBC(this.w_VALDES);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VASIMVAL,VADECTOT;
            from (i_cTable) where;
                VACODVAL = this.w_VALDES;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SIMVAL = NVL(cp_ToDate(_read_.VASIMVAL),cp_NullValue(_read_.VASIMVAL))
          this.w_ARRO = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      if this.w_CODVAL = this.w_VALDES
        * --- Se Stessa Valuta non c'e' sono Problema
        this.w_CAOVAL = 1
        this.w_CAODES = 1
      else
        if this.w_CODVAL<>g_PERVAL
          * --- Simbolo e Decimali
          this.w_CAOVAL = 0
          * --- Read from VALUTE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VALUTE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "VACAOVAL"+;
              " from "+i_cTable+" VALUTE where ";
                  +"VACODVAL = "+cp_ToStrODBC(this.w_CODVAL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              VACAOVAL;
              from (i_cTable) where;
                  VACODVAL = this.w_CODVAL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CAOVAL = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        * --- Se tutte e 2 sono valute Extra EURO Chiedo il Cambio
        g_APPDAT=cp_CharToDate("  -  -  ")
        this.w_APPO = this.oParentObject.w_DATSTA
        this.w_APPO1 = this.oParentObject.w_DATSTA
        if this.w_CAOVAL=0 AND this.w_CAODES=0
          this.w_CAOVAL = 1
          this.w_CAODES = 0
          do GSTE_KRC with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.w_CAODES=0
            ah_ErrorMsg("Elaborazione abortita",,"")
            * --- Chiudo i Cursori utilizzati
            if USED("MessErr")
              SELECT MessErr
              USE
            endif
            if USED("TOTPAR")
              SELECT PARTITE
              USE
            endif
            if USED("VALUTE")
              SELECT VALUTE
              USE
            endif
            if USED("RIEVAL")
              SELECT RIEVAL
              USE
            endif
          endif
          i_retcode = 'stop'
          return
        else
          * --- Faccio la triangolazione (Divido per w_CAOVAL e Moltiplico per w_CAODES)
          g_APPDAT=cp_CharToDate("  -  -  ")
          this.w_CAOVAL = IIF(this.w_CAOVAL<>0, this.w_CAOVAL, GETCAM(this.w_CODVAL,this.oParentObject.w_DATSTA,0))
          this.w_APPO = IIF(EMPTY(g_APPDAT), this.oParentObject.w_DATSTA, g_APPDAT)
          g_APPDAT=cp_CharToDate("  -  -  ")
          this.w_CAODES = IIF(this.w_CAODES<>0, this.w_CAODES, GETCAM(this.w_VALDES,this.oParentObject.w_DATSTA,0))
          this.w_APPO1 = IIF(EMPTY(g_APPDAT), this.oParentObject.w_DATSTA, g_APPDAT)
        endif
        * --- Se il divisore � 0 lo metto a 1 e aggiungo un messaggio
        do case
          case this.w_CAOVAL=0
            this.w_CAOVAL = 1
            this.w_ERROR = .T.
            select MessErr
            INSERT INTO MessErr (COD, MSG) VALUES (this.w_CODVAL, ah_Msgformat("Manca il cambio per %1",this.w_CODVAL) )
          case (this.oParentObject.w_DATSTA-7)>this.w_APPO
            this.w_ERROR = .T.
            select MessErr
            INSERT INTO MessErr (COD, MSG) VALUES (this.w_CODVAL, ah_Msgformat("Cambio %1 non aggiornato dal %2", this.w_CODVAL, DTOC(this.w_APPO)) )
        endcase
        do case
          case this.w_CAODES=0
            this.w_CAODES = 1
            this.w_ERROR = .T.
            select MessErr
            INSERT INTO MessErr (COD, MSG) VALUES (this.w_VALDES, ah_Msgformat("Manca il cambio per %1",this.w_VALDES) )
          case (this.oParentObject.w_DATSTA-7)>this.w_APPO1
            this.w_ERROR = .T.
            select MessErr
            INSERT INTO MessErr (COD, MSG) VALUES (this.w_VALDES, ah_Msgformat("Cambio %1 non aggiornato dal %2", this.w_VALDES, DTOC(this.w_APPO1)) )
        endcase
      endif
    endif
    * --- Completa i Dati del Cursore
    SELECT VALUTE
    REPLACE VALDES WITH this.w_VALDES
    REPLACE CAOVAL WITH this.w_CAOVAL
    REPLACE CAODES WITH this.w_CAODES
    REPLACE ARRVAL WITH this.w_ARRO
    REPLACE SIMVAL WITH this.w_SIMVAL
    ENDSCAN
    * --- Scrive i Totali Progressivi nella Valuta di Stampa
    CreaCur=WRCURSOR("PARTITE")
    this.w_OCODVAL = "xxxxxx"
    this.w_CAOVAL = 1
    this.w_CAODES = 1
    this.w_ARRO = g_PERPVL
    this.w_SIMCLF = g_VALSIM
    this.w_OLDTIP = "@"
    this.Pag6()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    SELECT PARTITE
    GO TOP
    SCAN
    this.w_CODVAL = CODVAL
    this.w_VALDES = IIF(this.oParentObject.w_VALCLI="S", IIF(EMPTY(NVL(VALCLF, " ")), g_PERVAL, VALCLF), g_PERVAL)
    this.w_TOTIMP = NVL(IMPDAR,0) - NVL(IMPAVE,0)
    if Nvl(PARTITE.FLCRSA," ")="A" AND this.oParentObject.w_FLSALD="R"
      * --- Per gli acconti applico cambio di origine
      this.w_OCODVAL = this.w_CODVAL+this.w_VALDES
      this.w_CAOVAL = IIF(EMPTY(NVL(PARTITE.CAOVAL,0)), 1, PARTITE.CAOVAL)
    else
      if (this.w_OCODVAL<>this.w_CODVAL+this.w_VALDES) OR this.w_OLDTIP<>"A"
        this.w_OCODVAL = this.w_CODVAL+this.w_VALDES
        SELECT VALUTE
        LOCATE FOR CODVAL=this.w_CODVAL AND VALDES=this.w_VALDES
        if FOUND()
          this.w_CAOVAL = IIF(EMPTY(NVL(CAOVAL,0)), 1, CAOVAL)
          this.w_CAODES = IIF(EMPTY(NVL(CAODES,0)), 1, CAODES)
          this.w_ARRO = NVL(ARRVAL, g_PERPVL)
          this.w_SIMCLF = NVL(SIMVAL, g_VALSIM)
        else
          this.w_CAOVAL = 1
          this.w_CAODES = 1
          this.w_ARRO = g_PERPVL
          this.w_SIMCLF = g_VALSIM
        endif
        SELECT PARTITE
      endif
    endif
    if this.w_CODVAL<>this.w_VALDES
      * --- Converte alla Valuta di Stampa
      this.w_TOTIMP = VAL2VAL(this.w_TOTIMP, this.w_CAOVAL,this.oParentObject.w_DATSTA, I_DATSYS, this.w_CAODES)
    endif
    REPLACE TOTIMP WITH this.w_TOTIMP
    REPLACE SIMCLF WITH this.w_SIMCLF
    REPLACE ARRCLF WITH this.w_ARRO
    if NVL(ORDINE, 0)=1
      REPLACE CAOAPP WITH IIF(this.w_CODVAL=g_PERVAL, 1,IIF(this.w_CODVAL=g_CODLIR, GETCAM(this.w_CODVAL,this.oParentObject.w_DATSTA), this.w_CAOVAL))
    endif
    this.w_OLDTIP = Nvl(PARTITE.FLCRSA," ")
    ENDSCAN
    if this.oParentObject.w_FLSALD="R"
      * --- Nel caso di Partite aperte devo convertire eventuali acconti in valuta
      *     al cambio di origine e non al cambio aggiornato come le altre partite
      * --- Aggiunge i Totali Valute per Cliente/Fornitore
      if USED("RIEVAL")
         
 SELECT RIEVAL 
 USE
      endif
      if this.w_PERDES
         
 SELECT ; 
 IIF(this.oParentObject.w_ONUME=5,DESAGE,DESCON) AS DESCON, ; 
 IIF(this.oParentObject.w_ONUME=5,Nvl(CODAGE," "),CODCON) AS CODCON, ; 
 CODVAL AS CODVAL, IIF(this.oParentObject.w_VALCLI="S", VALCLF, g_PERVAL) AS VALDES,"A" AS ACCONTI,CAOVAL,; 
 SUM(IMPDAR) AS IMPDAR, SUM(IMPAVE) AS IMPAVE, MAX(DECVAL) AS DECVAL, MAX(SIMVAL) AS SIMVAL,SUM(TOTIMP) AS TOTIMP ; 
 FROM PARTITE WHERE NVL(FLCRSA," ")="A" INTO CURSOR RIEVAL ; 
 GROUP BY 1,2,3,4 ORDER BY 1,2,3,4,5,6
      else
         
 SELECT ; 
 IIF(this.oParentObject.w_ONUME=5,CODAGE,CODCON) as CODCON, ; 
 CODVAL AS CODVAL, IIF(this.oParentObject.w_VALCLI="S", VALCLF, g_PERVAL) AS VALDES,"A" AS ACCONTI,CAOVAL,; 
 SUM(IMPDAR) AS IMPDAR, SUM(IMPAVE) AS IMPAVE, MAX(DECVAL) AS DECVAL, MAX(SIMVAL) AS SIMVAL,SUM(TOTIMP) AS TOTIMP ; 
 FROM PARTITE WHERE NVL(FLCRSA," ")="A" INTO CURSOR RIEVAL ; 
 GROUP BY 1,2,3,4,5 ORDER BY 1,2,3,4,5
      endif
      this.w_ARRO = g_PERPVL
      this.w_SIMVAL = g_VALSIM
      this.Pag6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creo il cursore APPO con la stessa struttura di Partite, al termine li metter� in Union
    Select * From Partite Where 1=0 into Cursor Appo1 NoFilter
    Cur = WrCursor( "APPO1" )
    * --- Ricerca partite di origine per dettaglio partite raggruppate.
    SELECT PARTITE
    GO TOP
    SCAN FOR FLRAGG="S"
    this.w_SERIAL = PTSERIAL
    this.w_TIPCON = TIPCON
    this.w_CODCON = CODCON
    this.w_ODATSCA = CP_TODATE(DATSCA)
    this.w_ONUMPAR = Nvl(NUMPAR,Space(31))
    this.w_OCODAGE = CODAGE
    this.w_ODESAGE = DESAGE
    * --- Read from CONTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ANCONRIF"+;
        " from "+i_cTable+" CONTI where ";
            +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
            +" and ANCODICE = "+cp_ToStrODBC(this.w_CODCON);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ANCONRIF;
        from (i_cTable) where;
            ANTIPCON = this.w_TIPCON;
            and ANCODICE = this.w_CODCON;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_LCONRIF = NVL(cp_ToDate(_read_.ANCONRIF),cp_NullValue(_read_.ANCONRIF))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if NOT EMPTY(this.w_LCONRIF)
      * --- Nel caso di Cliente con fornitore associato valorizzo LCHKRIF a'T' e la utilizo nella query
      *     in modo che non faccia filtro solo sulle partite dei clienti poich� potrebbero essere state accorpate 
      *     anche partite del fornitore
      this.w_LCHKRIF = "T"
    endif
    SELECT PARTITE
    vq_exec("QUERY\GSTESQEC", this, "APPO")
    if RECCOUNT("APPO") > 0
      this.Pag8()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    SELECT PARTITE
    ENDSCAN
    SELECT APPO1
    GO TOP
    * --- Creo il cursore finale e lo metto in PARTITE
    if RECCOUNT() > 0
      SELECT * FROM PARTITE UNION ALL SELECT * FROM APPO1 INTO CURSOR APPO NOFILTER
      SELECT PARTITE
      USE
      SELECT * FROM APPO INTO CURSOR PARTITE NOFILTER
      * --- Rendo nuovamente scrivibile il cursore
      Cur = WrCursor( "PARTITE" )
    endif
    if USED("APPO")
      SELECT APPO
      USE
    endif
    if USED("APPO1")
      SELECT APPO1
      USE
    endif
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elaborazione Tabella tempranea per Corporate Portal
    * --- Elimino Partite non Conformi
    if this.oParentObject.w_ESPORTAL="S"
      this.w_PARAMETR = "w_INIDATE"
      this.w_CODICEAZ = "document"
      if g_IZCP $ "S"
        do GSTE2BEC with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Se non � attivo il modulo CPZ, verr� richiesto di inserire un valore per il filtro sulla data di registrazione documento
        * --- Delete from ZOOMPART
        i_nConn=i_TableProp[this.ZOOMPART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          local i_cQueryTable,i_cWhere
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_cWhere=i_cTable+".PTSERIAL = "+i_cQueryTable+".PTSERIAL";
                +" and "+i_cTable+".PTROWORD = "+i_cQueryTable+".PTROWORD";
                +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
        
          do vq_exec with 'GSTEDBEC',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      endif
    endif
    * --- Write into ZOOMPART
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ZOOMPART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="PTSERIAL,PTROWORD,CPROWNUM"
      do vq_exec with 'GSTECBEC',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ZOOMPART_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="ZOOMPART.PTSERIAL = _t2.PTSERIAL";
              +" and "+"ZOOMPART.PTROWORD = _t2.PTROWORD";
              +" and "+"ZOOMPART.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SALDO = _t2.SALDO";
          +i_ccchkf;
          +" from "+i_cTable+" ZOOMPART, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="ZOOMPART.PTSERIAL = _t2.PTSERIAL";
              +" and "+"ZOOMPART.PTROWORD = _t2.PTROWORD";
              +" and "+"ZOOMPART.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ZOOMPART, "+i_cQueryTable+" _t2 set ";
          +"ZOOMPART.SALDO = _t2.SALDO";
          +Iif(Empty(i_ccchkf),"",",ZOOMPART.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="ZOOMPART.PTSERIAL = t2.PTSERIAL";
              +" and "+"ZOOMPART.PTROWORD = t2.PTROWORD";
              +" and "+"ZOOMPART.CPROWNUM = t2.CPROWNUM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ZOOMPART set (";
          +"SALDO";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.SALDO";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="ZOOMPART.PTSERIAL = _t2.PTSERIAL";
              +" and "+"ZOOMPART.PTROWORD = _t2.PTROWORD";
              +" and "+"ZOOMPART.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ZOOMPART set ";
          +"SALDO = _t2.SALDO";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".PTSERIAL = "+i_cQueryTable+".PTSERIAL";
              +" and "+i_cTable+".PTROWORD = "+i_cQueryTable+".PTROWORD";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SALDO = (select SALDO from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into ZOOMPART
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ZOOMPART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="PTSERRIF,PTORDRIF,PTNUMRIF"
      do vq_exec with 'GSTESBEC',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ZOOMPART_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="ZOOMPART.PTSERRIF = _t2.PTSERRIF";
              +" and "+"ZOOMPART.PTORDRIF = _t2.PTORDRIF";
              +" and "+"ZOOMPART.PTNUMRIF = _t2.PTNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SALDO = _t2.SALDO";
          +i_ccchkf;
          +" from "+i_cTable+" ZOOMPART, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="ZOOMPART.PTSERRIF = _t2.PTSERRIF";
              +" and "+"ZOOMPART.PTORDRIF = _t2.PTORDRIF";
              +" and "+"ZOOMPART.PTNUMRIF = _t2.PTNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ZOOMPART, "+i_cQueryTable+" _t2 set ";
          +"ZOOMPART.SALDO = _t2.SALDO";
          +Iif(Empty(i_ccchkf),"",",ZOOMPART.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="ZOOMPART.PTSERRIF = t2.PTSERRIF";
              +" and "+"ZOOMPART.PTORDRIF = t2.PTORDRIF";
              +" and "+"ZOOMPART.PTNUMRIF = t2.PTNUMRIF";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ZOOMPART set (";
          +"SALDO";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.SALDO";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="ZOOMPART.PTSERRIF = _t2.PTSERRIF";
              +" and "+"ZOOMPART.PTORDRIF = _t2.PTORDRIF";
              +" and "+"ZOOMPART.PTNUMRIF = _t2.PTNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ZOOMPART set ";
          +"SALDO = _t2.SALDO";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".PTSERRIF = "+i_cQueryTable+".PTSERRIF";
              +" and "+i_cTable+".PTORDRIF = "+i_cQueryTable+".PTORDRIF";
              +" and "+i_cTable+".PTNUMRIF = "+i_cQueryTable+".PTNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SALDO = (select SALDO from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Elimino Creazioni collegate a distinte da considerarsi chiuse per la data di elaborazione 
    * --- Create temporary table TMP_PART
    i_nIdx=cp_AddTableDef('TMP_PART') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('GSTESALDO',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMP_PART_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Write into ZOOMPART
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ZOOMPART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.TMP_PART_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ZOOMPART_idx,i_nConn)
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="ZOOMPART.PTSERIAL = _t2.PTSERIAL";
              +" and "+"ZOOMPART.PTROWORD = _t2.PTROWORD";
              +" and "+"ZOOMPART.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SALDO = _t2.SALDO";
          +i_ccchkf;
          +" from "+i_cTable+" ZOOMPART, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="ZOOMPART.PTSERIAL = _t2.PTSERIAL";
              +" and "+"ZOOMPART.PTROWORD = _t2.PTROWORD";
              +" and "+"ZOOMPART.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ZOOMPART, "+i_cQueryTable+" _t2 set ";
          +"ZOOMPART.SALDO = _t2.SALDO";
          +Iif(Empty(i_ccchkf),"",",ZOOMPART.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="PostgreSQL"
        i_cWhere="ZOOMPART.PTSERIAL = _t2.PTSERIAL";
              +" and "+"ZOOMPART.PTROWORD = _t2.PTROWORD";
              +" and "+"ZOOMPART.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ZOOMPART set ";
          +"SALDO = _t2.SALDO";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".PTSERIAL = "+i_cQueryTable+".PTSERIAL";
              +" and "+i_cTable+".PTROWORD = "+i_cQueryTable+".PTROWORD";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SALDO = (select SALDO from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into ZOOMPART
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ZOOMPART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.TMP_PART_idx,2])
      i_cTempTable=cp_GetTempTableName(i_nConn)
      i_aIndex[1]='PTSERRIF,PTORDRIF,PTNUMRIF'
      cp_CreateTempTable(i_nConn,i_cTempTable,"PTSERIAL AS PTSERRIF,PTROWORD AS PTORDRIF,CPROWNUM AS PTNUMRIF,SALDO AS SALDO "," from "+i_cQueryTable+" where 1=1",.f.,@i_aIndex)
      i_cQueryTable=i_cTempTable
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ZOOMPART_idx,i_nConn)
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="ZOOMPART.PTSERRIF = _t2.PTSERRIF";
              +" and "+"ZOOMPART.PTORDRIF = _t2.PTORDRIF";
              +" and "+"ZOOMPART.PTNUMRIF = _t2.PTNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SALDO = _t2.SALDO";
          +i_ccchkf;
          +" from "+i_cTable+" ZOOMPART, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="ZOOMPART.PTSERRIF = _t2.PTSERRIF";
              +" and "+"ZOOMPART.PTORDRIF = _t2.PTORDRIF";
              +" and "+"ZOOMPART.PTNUMRIF = _t2.PTNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ZOOMPART, "+i_cQueryTable+" _t2 set ";
          +"ZOOMPART.SALDO = _t2.SALDO";
          +Iif(Empty(i_ccchkf),"",",ZOOMPART.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="PostgreSQL"
        i_cWhere="ZOOMPART.PTSERRIF = _t2.PTSERRIF";
              +" and "+"ZOOMPART.PTORDRIF = _t2.PTORDRIF";
              +" and "+"ZOOMPART.PTNUMRIF = _t2.PTNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ZOOMPART set ";
          +"SALDO = _t2.SALDO";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".PTSERRIF = "+i_cQueryTable+".PTSERRIF";
              +" and "+i_cTable+".PTORDRIF = "+i_cQueryTable+".PTORDRIF";
              +" and "+i_cTable+".PTNUMRIF = "+i_cQueryTable+".PTNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SALDO = (select SALDO from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Drop temporary table TMP_PART
    i_nIdx=cp_GetTableDefIdx('TMP_PART')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_PART')
    endif
    * --- Delete from ZOOMPART
    i_nConn=i_TableProp[this.ZOOMPART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".PTSERIAL = "+i_cQueryTable+".PTSERIAL";
            +" and "+i_cTable+".PTROWORD = "+i_cQueryTable+".PTROWORD";
            +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
      do vq_exec with 'GSTE4BEC',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error='Errore cancellazione creazioni distinte'
      return
    endif
    * --- Marco Partite di creazione da Mantenere Aperte
    * --- Write into ZOOMPART
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ZOOMPART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="PTSERIAL,PTROWORD,CPROWNUM"
      do vq_exec with 'GSTE3BEC',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ZOOMPART_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="ZOOMPART.PTSERIAL = _t2.PTSERIAL";
              +" and "+"ZOOMPART.PTROWORD = _t2.PTROWORD";
              +" and "+"ZOOMPART.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SALDO ="+cp_NullLink(cp_ToStrODBC(1),'ZOOMPART','SALDO');
          +i_ccchkf;
          +" from "+i_cTable+" ZOOMPART, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="ZOOMPART.PTSERIAL = _t2.PTSERIAL";
              +" and "+"ZOOMPART.PTROWORD = _t2.PTROWORD";
              +" and "+"ZOOMPART.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ZOOMPART, "+i_cQueryTable+" _t2 set ";
      +"ZOOMPART.SALDO ="+cp_NullLink(cp_ToStrODBC(1),'ZOOMPART','SALDO');
          +Iif(Empty(i_ccchkf),"",",ZOOMPART.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="ZOOMPART.PTSERIAL = t2.PTSERIAL";
              +" and "+"ZOOMPART.PTROWORD = t2.PTROWORD";
              +" and "+"ZOOMPART.CPROWNUM = t2.CPROWNUM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ZOOMPART set (";
          +"SALDO";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC(1),'ZOOMPART','SALDO')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="ZOOMPART.PTSERIAL = _t2.PTSERIAL";
              +" and "+"ZOOMPART.PTROWORD = _t2.PTROWORD";
              +" and "+"ZOOMPART.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ZOOMPART set ";
      +"SALDO ="+cp_NullLink(cp_ToStrODBC(1),'ZOOMPART','SALDO');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".PTSERIAL = "+i_cQueryTable+".PTSERIAL";
              +" and "+i_cTable+".PTROWORD = "+i_cQueryTable+".PTROWORD";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SALDO ="+cp_NullLink(cp_ToStrODBC(1),'ZOOMPART','SALDO');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Marco partite di saldo non a distinta con saldo uguale a zero
    * --- Write into ZOOMPART
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ZOOMPART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="PTSERIAL,PTROWORD,CPROWNUM"
      do vq_exec with 'gste3bec_s',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ZOOMPART_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="ZOOMPART.PTSERIAL = _t2.PTSERIAL";
              +" and "+"ZOOMPART.PTROWORD = _t2.PTROWORD";
              +" and "+"ZOOMPART.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SALDO ="+cp_NullLink(cp_ToStrODBC(1),'ZOOMPART','SALDO');
          +i_ccchkf;
          +" from "+i_cTable+" ZOOMPART, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="ZOOMPART.PTSERIAL = _t2.PTSERIAL";
              +" and "+"ZOOMPART.PTROWORD = _t2.PTROWORD";
              +" and "+"ZOOMPART.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ZOOMPART, "+i_cQueryTable+" _t2 set ";
      +"ZOOMPART.SALDO ="+cp_NullLink(cp_ToStrODBC(1),'ZOOMPART','SALDO');
          +Iif(Empty(i_ccchkf),"",",ZOOMPART.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="ZOOMPART.PTSERIAL = t2.PTSERIAL";
              +" and "+"ZOOMPART.PTROWORD = t2.PTROWORD";
              +" and "+"ZOOMPART.CPROWNUM = t2.CPROWNUM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ZOOMPART set (";
          +"SALDO";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC(1),'ZOOMPART','SALDO')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="ZOOMPART.PTSERIAL = _t2.PTSERIAL";
              +" and "+"ZOOMPART.PTROWORD = _t2.PTROWORD";
              +" and "+"ZOOMPART.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ZOOMPART set ";
      +"SALDO ="+cp_NullLink(cp_ToStrODBC(1),'ZOOMPART','SALDO');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".PTSERIAL = "+i_cQueryTable+".PTSERIAL";
              +" and "+i_cTable+".PTROWORD = "+i_cQueryTable+".PTROWORD";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SALDO ="+cp_NullLink(cp_ToStrODBC(1),'ZOOMPART','SALDO');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Elimino Partite Chiuse 
    * --- Delete from ZOOMPART
    i_nConn=i_TableProp[this.ZOOMPART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"SALDO = "+cp_ToStrODBC(0);
             )
    else
      delete from (i_cTable) where;
            SALDO = 0;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    if this.oParentObject.w_ESPORTAL="S"
      * --- Marco Insoluti
      * --- Write into ZOOMPART
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ZOOMPART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="PTSERIAL,PTROWORD,CPROWNUM"
        do vq_exec with 'GSTE5BEC',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ZOOMPART_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="ZOOMPART.PTSERIAL = _t2.PTSERIAL";
                +" and "+"ZOOMPART.PTROWORD = _t2.PTROWORD";
                +" and "+"ZOOMPART.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"INSOLUTO ="+cp_NullLink(cp_ToStrODBC("I"),'ZOOMPART','INSOLUTO');
            +i_ccchkf;
            +" from "+i_cTable+" ZOOMPART, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="ZOOMPART.PTSERIAL = _t2.PTSERIAL";
                +" and "+"ZOOMPART.PTROWORD = _t2.PTROWORD";
                +" and "+"ZOOMPART.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ZOOMPART, "+i_cQueryTable+" _t2 set ";
        +"ZOOMPART.INSOLUTO ="+cp_NullLink(cp_ToStrODBC("I"),'ZOOMPART','INSOLUTO');
            +Iif(Empty(i_ccchkf),"",",ZOOMPART.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="ZOOMPART.PTSERIAL = t2.PTSERIAL";
                +" and "+"ZOOMPART.PTROWORD = t2.PTROWORD";
                +" and "+"ZOOMPART.CPROWNUM = t2.CPROWNUM";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ZOOMPART set (";
            +"INSOLUTO";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +cp_NullLink(cp_ToStrODBC("I"),'ZOOMPART','INSOLUTO')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="ZOOMPART.PTSERIAL = _t2.PTSERIAL";
                +" and "+"ZOOMPART.PTROWORD = _t2.PTROWORD";
                +" and "+"ZOOMPART.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ZOOMPART set ";
        +"INSOLUTO ="+cp_NullLink(cp_ToStrODBC("I"),'ZOOMPART','INSOLUTO');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".PTSERIAL = "+i_cQueryTable+".PTSERIAL";
                +" and "+i_cTable+".PTROWORD = "+i_cQueryTable+".PTROWORD";
                +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"INSOLUTO ="+cp_NullLink(cp_ToStrODBC("I"),'ZOOMPART','INSOLUTO');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Aggiorno incassi insoluti
      * --- Write into ZOOMPART
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ZOOMPART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="PTSERIAL,PTROWORD,CPROWNUM"
        do vq_exec with 'GSTE6BEC',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ZOOMPART_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="ZOOMPART.PTSERIAL = _t2.PTSERIAL";
                +" and "+"ZOOMPART.PTROWORD = _t2.PTROWORD";
                +" and "+"ZOOMPART.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"INSOLUTO ="+cp_NullLink(cp_ToStrODBC("I"),'ZOOMPART','INSOLUTO');
            +i_ccchkf;
            +" from "+i_cTable+" ZOOMPART, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="ZOOMPART.PTSERIAL = _t2.PTSERIAL";
                +" and "+"ZOOMPART.PTROWORD = _t2.PTROWORD";
                +" and "+"ZOOMPART.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ZOOMPART, "+i_cQueryTable+" _t2 set ";
        +"ZOOMPART.INSOLUTO ="+cp_NullLink(cp_ToStrODBC("I"),'ZOOMPART','INSOLUTO');
            +Iif(Empty(i_ccchkf),"",",ZOOMPART.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="ZOOMPART.PTSERIAL = t2.PTSERIAL";
                +" and "+"ZOOMPART.PTROWORD = t2.PTROWORD";
                +" and "+"ZOOMPART.CPROWNUM = t2.CPROWNUM";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ZOOMPART set (";
            +"INSOLUTO";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +cp_NullLink(cp_ToStrODBC("I"),'ZOOMPART','INSOLUTO')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="ZOOMPART.PTSERIAL = _t2.PTSERIAL";
                +" and "+"ZOOMPART.PTROWORD = _t2.PTROWORD";
                +" and "+"ZOOMPART.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ZOOMPART set ";
        +"INSOLUTO ="+cp_NullLink(cp_ToStrODBC("I"),'ZOOMPART','INSOLUTO');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".PTSERIAL = "+i_cQueryTable+".PTSERIAL";
                +" and "+i_cTable+".PTROWORD = "+i_cQueryTable+".PTROWORD";
                +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"INSOLUTO ="+cp_NullLink(cp_ToStrODBC("I"),'ZOOMPART','INSOLUTO');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    if this.oParentObject.w_ESPORTAL="S"
      * --- Necessario poich� in DB2 la STR() pospone nei numerici convertiti degli  zeri ed il puntino
      *     falsando la costruzione della chiave
      * --- Select from ZOOMPART
      i_nConn=i_TableProp[this.ZOOMPART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2],.t.,this.ZOOMPART_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" ZOOMPART ";
             ,"_Curs_ZOOMPART")
      else
        select * from (i_cTable);
          into cursor _Curs_ZOOMPART
      endif
      if used('_Curs_ZOOMPART')
        select _Curs_ZOOMPART
        locate for 1=1
        do while not(eof())
        this.w_LROWORD = _Curs_ZOOMPART.PTROWORD
        this.w_LROWNUM = _Curs_ZOOMPART.CPROWNUM
        this.w_LSERIAL = ALLTRIM(_Curs_ZOOMPART.PTSERIAL)
        this.w_CODRAG = ALLTRIM(_Curs_ZOOMPART.NUMPAR) +DTOC(_Curs_ZOOMPART.DATSCA)+_Curs_ZOOMPART.TIPCON+ALLTRIM(_Curs_ZOOMPART.CODCON)+ALLTRIM(_Curs_ZOOMPART.CODVAL)+this.w_LSERIAL+ALLTRIM(STR(this.w_LROWORD))+ALLTRIM(STR(this.w_LROWNUM))
        * --- Write into ZOOMPART
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ZOOMPART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ZOOMPART_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CODRAG ="+cp_NullLink(cp_ToStrODBC(this.w_CODRAG),'ZOOMPART','CODRAG');
              +i_ccchkf ;
          +" where ";
              +"PTSERIAL = "+cp_ToStrODBC(this.w_LSERIAL);
              +" and PTROWORD = "+cp_ToStrODBC(this.w_LROWORD);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_LROWNUM);
                 )
        else
          update (i_cTable) set;
              CODRAG = this.w_CODRAG;
              &i_ccchkf. ;
           where;
              PTSERIAL = this.w_LSERIAL;
              and PTROWORD = this.w_LROWORD;
              and CPROWNUM = this.w_LROWNUM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
          select _Curs_ZOOMPART
          continue
        enddo
        use
      endif
      * --- Valorizzo Campo di Raggruppamento
      * --- Inserisco chiave di raggruppamento non prevista direttamente nella query 
      *     per problemi di spazio nell'expression del campo
      Select Partite 
 Go top 
 Scan For Partite.Conta >1
      this.w_TIPCON = NVL(TIPCON," ")
      this.w_CODCON = NVL(CODCON,SPACE(15))
      this.w_DATSCA = CP_TODATE(DATSCA)
      this.w_NUMPAR = NVL(NUMPAR,SPACE(31))
      this.w_CODVAL = NVL(CODVAL,SPACE(3))
      this.w_MODPAG = NVL(MODPAG,SPACE(10))
      this.w_SERIAL = PTSERIAL
      this.w_PTROWORD = PTROWORD
      this.w_CPROWNUM = CPROWNUM
      this.w_PTSERRIF = Nvl(PTSERRIF,Space(10))
      this.w_PTORDRIF = Nvl(PTORDRIF,0)
      this.w_PTNUMRIF = Nvl(PTNUMRIF,0)
      this.w_FLRAGG = Nvl(PTFLRAGG,0)
      this.w_TIPDOC = Nvl(TIPDOC,"  ")
      this.w_SIMVAL = Nvl(SIMVAL," ")
      this.w_CODRAG = ALLTRIM(this.w_NUMPAR) +DTOC(this.w_DATSCA)+this.w_TIPCON+ALLTRIM(this.w_CODCON)+ALLTRIM(this.w_CODVAL)+this.w_SERIAL+ALLTRIM(STR(this.w_PTROWORD))+ALLTRIM(STR(this.w_CPROWNUM))
      this.w_NumUpd = 0
      * --- Non devo contare la partita di creazione che sto processando
      this.w_ParCont = Nvl(Partite.Conta,0) -1
      do while this.w_ParCont>0
        if NOT EMPTY(this.w_SERIAL)
          * --- Aggiorno chiave raggruppamento partite di saldo 
          * --- Write into ZOOMPART
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ZOOMPART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ZOOMPART_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CODRAG ="+cp_NullLink(cp_ToStrODBC(this.w_CODRAG),'ZOOMPART','CODRAG');
            +",PAGRAG ="+cp_NullLink(cp_ToStrODBC(this.w_MODPAG),'ZOOMPART','PAGRAG');
                +i_ccchkf ;
            +" where ";
                +"PTORDRIF <> "+cp_ToStrODBC(0);
                +" and PTSERRIF = "+cp_ToStrODBC(this.w_SERIAL);
                +" and PTORDRIF = "+cp_ToStrODBC(this.w_PTROWORD);
                +" and PTNUMRIF = "+cp_ToStrODBC(this.w_CPROWNUM);
                +" and FLCRSA = "+cp_ToStrODBC("S");
                   )
          else
            update (i_cTable) set;
                CODRAG = this.w_CODRAG;
                ,PAGRAG = this.w_MODPAG;
                &i_ccchkf. ;
             where;
                PTORDRIF <> 0;
                and PTSERRIF = this.w_SERIAL;
                and PTORDRIF = this.w_PTROWORD;
                and PTNUMRIF = this.w_CPROWNUM;
                and FLCRSA = "S";

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          this.w_NumUpd = this.w_NumUpd + i_ROWS
          this.w_ParCont = this.w_ParCont-this.w_Numupd
        endif
        if this.w_ParCont>0
          * --- Esistono ancora partite da aggiornare
          if this.w_PTORDRIF<>0
            * --- Partita Proveniente da insoluto aggiorno relativo campo
            * --- Write into ZOOMPART
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ZOOMPART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ZOOMPART_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"INSOLUTO ="+cp_NullLink(cp_ToStrODBC("I"),'ZOOMPART','INSOLUTO');
                  +i_ccchkf ;
              +" where ";
                  +"PTSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                  +" and PTROWORD = "+cp_ToStrODBC(this.w_PTROWORD);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                     )
            else
              update (i_cTable) set;
                  INSOLUTO = "I";
                  &i_ccchkf. ;
               where;
                  PTSERIAL = this.w_SERIAL;
                  and PTROWORD = this.w_PTROWORD;
                  and CPROWNUM = this.w_CPROWNUM;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore scrittura partite saldo'
              return
            endif
            * --- Aggiorno chiave raggruppamento partite di creazione puntata
            * --- Write into ZOOMPART
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ZOOMPART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ZOOMPART_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CODRAG ="+cp_NullLink(cp_ToStrODBC(this.w_CODRAG),'ZOOMPART','CODRAG');
              +",PAGRAG ="+cp_NullLink(cp_ToStrODBC(this.w_MODPAG),'ZOOMPART','PAGRAG');
                  +i_ccchkf ;
              +" where ";
                  +"PTSERIAL = "+cp_ToStrODBC(this.w_PTSERRIF);
                  +" and PTROWORD = "+cp_ToStrODBC(this.w_PTORDRIF);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_PTNUMRIF);
                     )
            else
              update (i_cTable) set;
                  CODRAG = this.w_CODRAG;
                  ,PAGRAG = this.w_MODPAG;
                  &i_ccchkf. ;
               where;
                  PTSERIAL = this.w_PTSERRIF;
                  and PTROWORD = this.w_PTORDRIF;
                  and CPROWNUM = this.w_PTNUMRIF;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore scrittura partita di creazione originaria'
              return
            endif
            this.w_NumUpd = this.w_NumUpd + i_ROWS
            this.w_ParCont = this.w_ParCont-this.w_Numupd
            * --- Leggo riferimenti partia di creazione puntata per risalire all'ulteriore partita di creazione precedente
            * --- Read from ZOOMPART
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ZOOMPART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2],.t.,this.ZOOMPART_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "PTSERRIF,PTORDRIF,PTNUMRIF"+;
                " from "+i_cTable+" ZOOMPART where ";
                    +"PTSERIAL = "+cp_ToStrODBC(this.w_PTSERRIF);
                    +" and PTROWORD = "+cp_ToStrODBC(this.w_PTORDRIF);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.w_PTNUMRIF);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                PTSERRIF,PTORDRIF,PTNUMRIF;
                from (i_cTable) where;
                    PTSERIAL = this.w_PTSERRIF;
                    and PTROWORD = this.w_PTORDRIF;
                    and CPROWNUM = this.w_PTNUMRIF;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_SERIAL = NVL(cp_ToDate(_read_.PTSERRIF),cp_NullValue(_read_.PTSERRIF))
              this.w_PTROWORD = NVL(cp_ToDate(_read_.PTORDRIF),cp_NullValue(_read_.PTORDRIF))
              this.w_CPROWNUM = NVL(cp_ToDate(_read_.PTNUMRIF),cp_NullValue(_read_.PTNUMRIF))
              use
            else
              * --- Error: sql sentence error.
              i_Error = 'Errore Lettura Riferimenti Partita di Origine'
              return
            endif
            select (i_nOldArea)
            if Empty(this.w_SERIAL)
              * --- Ho raggiunto la partita di Creazione Originaria
              * --- Aggiorno Chiave,Pagamento  Saldi legati alla creazione originaria
              * --- Write into ZOOMPART
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.ZOOMPART_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.ZOOMPART_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"CODRAG ="+cp_NullLink(cp_ToStrODBC(this.w_CODRAG),'ZOOMPART','CODRAG');
                +",PAGRAG ="+cp_NullLink(cp_ToStrODBC(this.w_MODPAG),'ZOOMPART','PAGRAG');
                    +i_ccchkf ;
                +" where ";
                    +"PTORDRIF <> "+cp_ToStrODBC(0);
                    +" and PTSERRIF = "+cp_ToStrODBC(this.w_PTSERRIF);
                    +" and PTORDRIF = "+cp_ToStrODBC(this.w_PTORDRIF);
                    +" and PTNUMRIF = "+cp_ToStrODBC(this.w_PTNUMRIF);
                    +" and FLCRSA = "+cp_ToStrODBC("S");
                       )
              else
                update (i_cTable) set;
                    CODRAG = this.w_CODRAG;
                    ,PAGRAG = this.w_MODPAG;
                    &i_ccchkf. ;
                 where;
                    PTORDRIF <> 0;
                    and PTSERRIF = this.w_PTSERRIF;
                    and PTORDRIF = this.w_PTORDRIF;
                    and PTNUMRIF = this.w_PTNUMRIF;
                    and FLCRSA = "S";

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              if this.w_ParCont>0 
                * --- Caso Limite Segnalo anomalia con Log e forzo uscita  dal ciclo
                this.w_ParCont = 0
              endif
            endif
          else
            * --- Scrittura partite del pregresso
            * --- Write into ZOOMPART
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ZOOMPART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ZOOMPART_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CODRAG ="+cp_NullLink(cp_ToStrODBC(this.w_CODRAG),'ZOOMPART','CODRAG');
              +",PAGRAG ="+cp_NullLink(cp_ToStrODBC(this.w_MODPAG),'ZOOMPART','PAGRAG');
                  +i_ccchkf ;
              +" where ";
                  +"NUMPAR = "+cp_ToStrODBC(this.w_NUMPAR);
                  +" and TIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                  +" and CODCON = "+cp_ToStrODBC(this.w_CODCON);
                  +" and CODVAL = "+cp_ToStrODBC(this.w_CODVAL);
                  +" and DATSCA = "+cp_ToStrODBC(this.w_DATSCA);
                  +" and PTORDRIF = "+cp_ToStrODBC(0);
                  +" and FLCRSA = "+cp_ToStrODBC("S");
                     )
            else
              update (i_cTable) set;
                  CODRAG = this.w_CODRAG;
                  ,PAGRAG = this.w_MODPAG;
                  &i_ccchkf. ;
               where;
                  NUMPAR = this.w_NUMPAR;
                  and TIPCON = this.w_TIPCON;
                  and CODCON = this.w_CODCON;
                  and CODVAL = this.w_CODVAL;
                  and DATSCA = this.w_DATSCA;
                  and PTORDRIF = 0;
                  and FLCRSA = "S";

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore scrittura partite pregresso'
              return
            endif
            this.w_NumUpd = this.w_NumUpd + i_ROWS
            this.w_ParCont = this.w_ParCont-this.w_Numupd
            if (this.w_TIPDOC="NO" Or this.w_FLRAGG>0) And this.w_ParCont>0
              * --- Write into ZOOMPART
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.ZOOMPART_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.ZOOMPART_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"CODRAG ="+cp_NullLink(cp_ToStrODBC(this.w_CODRAG),'ZOOMPART','CODRAG');
                +",PAGRAG ="+cp_NullLink(cp_ToStrODBC(this.w_MODPAG),'ZOOMPART','PAGRAG');
                    +i_ccchkf ;
                +" where ";
                    +"NUMPAR = "+cp_ToStrODBC(this.w_NUMPAR);
                    +" and TIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                    +" and CODCON = "+cp_ToStrODBC(this.w_CODCON);
                    +" and CODVAL = "+cp_ToStrODBC(this.w_CODVAL);
                    +" and DATSCA = "+cp_ToStrODBC(this.w_DATSCA);
                    +" and PTORDRIF = "+cp_ToStrODBC(0);
                    +" and FLCRSA = "+cp_ToStrODBC("C");
                       )
              else
                update (i_cTable) set;
                    CODRAG = this.w_CODRAG;
                    ,PAGRAG = this.w_MODPAG;
                    &i_ccchkf. ;
                 where;
                    NUMPAR = this.w_NUMPAR;
                    and TIPCON = this.w_TIPCON;
                    and CODCON = this.w_CODCON;
                    and CODVAL = this.w_CODVAL;
                    and DATSCA = this.w_DATSCA;
                    and PTORDRIF = 0;
                    and FLCRSA = "C";

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore scrittura partite pregresso'
                return
              endif
              this.w_NumUpd = this.w_NumUpd + i_ROWS
              this.w_ParCont = this.w_ParCont-this.w_Numupd
            endif
            if this.w_ParCont>0 
              * --- Caso Limite Segnalo anomalia con Log e forzo uscita  dal ciclo
              this.w_ParCont = 0
            endif
          endif
        endif
      enddo
      ENDSCAN
      if USED("MessErr")
        SELECT MessErr
        USE
      endif
      if USED("PARTITE")
        SELECT PARTITE
        USE
      endif
    endif
  endproc


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
     
 SELECT RIEVAL 
 GO TOP 
 SCAN FOR NOT EMPTY(NVL(CODVAL," ")) 
 SELECT PARTITE
    if RIEVAL.ACCONTI="A"
      * --- Nel caso di acconti devo controllare se ho gi� caricato una riga totalizzante con le stesse caratteristiche
       
 LOCATE FOR; 
 ORDINE=1 AND RIEVAL.CODVAL=PARTITE.CODVAL AND this.oParentObject.w_TIPCLF=PARTITE.TIPCON AND; 
 IIF(this.w_PERDES,RIEVAL.DESCON=IIF(this.oParentObject.w_ONUME=5,PARTITE.DESAGE,PARTITE.DESCON),; 
 RIEVAL.CODCON=IIF(this.oParentObject.w_ONUME=5,PARTITE.CODAGE,PARTITE.CODCON)) AND; 
 IIF( RIEVAL.CODVAL=g_PERVAL, 1,IIF( RIEVAL.CODVAL=g_CODLIR, GETCAM(RIEVAL.CODVAL,this.oParentObject.w_DATSTA), RIEVAL.CAOVAL))=PARTITE.CAOAPP
      if NOT FOUND()
        APPEND BLANK
         
 REPLACE CAOAPP WITH IIF( RIEVAL.CODVAL=g_CODEUR, 1,IIF( RIEVAL.CODVAL=g_CODLIR, GETCAM(RIEVAL.CODVAL,this.oParentObject.w_DATSTA), RIEVAL.CAOVAL)) 
 REPLACE PARTITE.TOTIMP WITH RIEVAL.TOTIMP 
 REPLACE PARTITE.IMPDAR WITH RIEVAL.IMPDAR 
 REPLACE PARTITE.IMPAVE WITH RIEVAL.IMPAVE 
 REPLACE PARTITE.TOTDAR WITH RIEVAL.IMPDAR 
 REPLACE PARTITE.TOTAVE WITH RIEVAL.IMPAVE
      else
         
 REPLACE PARTITE.TOTIMP WITH NVL(PARTITE.TOTIMP,0) +NVL(RIEVAL.TOTIMP,0) 
 REPLACE PARTITE.IMPAVE WITH NVL(PARTITE.IMPAVE,0) +NVL(RIEVAL.IMPAVE,0) 
 REPLACE PARTITE.IMPDAR WITH NVL(PARTITE.IMPDAR,0) +NVL(RIEVAL.IMPDAR,0) 
 REPLACE PARTITE.TOTAVE WITH NVL(PARTITE.IMPAVE,0) +NVL(RIEVAL.IMPAVE,0) 
 REPLACE PARTITE.TOTDAR WITH NVL(PARTITE.IMPDAR,0) +NVL(RIEVAL.IMPDAR,0)
      endif
    else
      APPEND BLANK 
 REPLACE PARTITE.IMPDAR WITH RIEVAL.IMPDAR 
 REPLACE PARTITE.IMPAVE WITH RIEVAL.IMPAVE 
 REPLACE PARTITE.TOTDAR WITH RIEVAL.IMPDAR 
 REPLACE PARTITE.TOTAVE WITH RIEVAL.IMPAVE
    endif
     
 SELECT PARTITE 
 REPLACE TIPCON WITH this.oParentObject.w_TIPCLF
    if this.w_PERDES
      if this.oParentObject.w_ONUME=5
        REPLACE DESAGE WITH RIEVAL.DESCON
      else
        REPLACE DESCON WITH RIEVAL.DESCON
      endif
      REPLACE CODCON WITH RIEVAL.CODCON
    else
      if this.oParentObject.w_ONUME=5
        REPLACE CODAGE WITH RIEVAL.CODCON
      else
        REPLACE CODCON WITH RIEVAL.CODCON
      endif
    endif
     
 REPLACE CODVAL WITH RIEVAL.CODVAL 
 REPLACE VALCLF WITH RIEVAL.VALDES 
 REPLACE DECVAL WITH RIEVAL.DECVAL 
 REPLACE SIMVAL WITH RIEVAL.SIMVAL 
 REPLACE SIMCLF WITH this.w_SIMVAL 
 REPLACE ARRCLF WITH this.w_ARRO 
 REPLACE ORDINE WITH 1
     
 SELECT RIEVAL 
 ENDSCAN 
 SELECT RIEVAL 
 Use
  endproc


  procedure Pag7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esplosione della partita raggruppata
    this.w_SERIAL = this.w_SERIALECR
    vq_exec("QUERY\GSTESQEC", this, "APPO2")
    this.Pag8()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    USE IN SELECT("APPO2")
  endproc


  procedure Pag8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserimento partite derivanti dall'esplosione della partita raggruppata
    if used("APPO2")
      SELECT APPO2
      GO TOP
    else
      SELECT APPO
      GO TOP
    endif
    SCAN
    this.w_TIPCON = NVL(TIPCON," ")
    this.w_CODCON = NVL(CODCON,SPACE(15))
    this.w_ORDINE = 0
    this.w_DATSCA = CP_TODATE(DATSCA)
    this.w_NUMPAR = NVL(NUMPAR,SPACE(31))
    this.w_CODVAL = NVL(CODVAL,SPACE(3))
    * --- Controllo se la partita attuale � raggruppata, per farlo leggo il campo PTFLRAGG dalla partita di creazione.
    * --- Read from PAR_TITE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PTFLRAGG,PTSERIAL"+;
        " from "+i_cTable+" PAR_TITE where ";
            +"PTNUMPAR = "+cp_ToStrODBC(this.w_NUMPAR);
            +" and PTDATSCA = "+cp_ToStrODBC(this.w_DATSCA);
            +" and PTTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
            +" and PTCODCON = "+cp_ToStrODBC(this.w_CODCON);
            +" and PTCODVAL = "+cp_ToStrODBC(this.w_CODVAL);
            +" and PTFLCRSA = "+cp_ToStrODBC("C");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PTFLRAGG,PTSERIAL;
        from (i_cTable) where;
            PTNUMPAR = this.w_NUMPAR;
            and PTDATSCA = this.w_DATSCA;
            and PTTIPCON = this.w_TIPCON;
            and PTCODCON = this.w_CODCON;
            and PTCODVAL = this.w_CODVAL;
            and PTFLCRSA = "C";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_FLRAGGCR = NVL(cp_ToDate(_read_.PTFLRAGG),cp_NullValue(_read_.PTFLRAGG))
      this.w_SERIALECR = NVL(cp_ToDate(_read_.PTSERIAL),cp_NullValue(_read_.PTSERIAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_FLCRSA = NVL(FLCRSA," ")
    this.w_IMPDAR = NVL(IMPDAR,0)
    this.w_IMPAVE = NVL(IMPAVE,0)
    this.w_SIMVAL = NVL(SIMVAL,SPACE(5))
    this.w_DECVAL = NVL(DECVAL,0)
    this.w_MODPAG = NVL(MODPAG,SPACE(10))
    this.w_NUMDOC = NVL(NUMDOC,0)
    this.w_ALFDOC = NVL(ALFDOC,SPACE(10))
    this.w_DATDOC = CP_TODATE(DATDOC)
    this.w_FLSOSP1 = NVL(FLSOSP," ")
    this.w_DESCON = NVL(DESCON,SPACE(40))
    this.w_INDIRI = NVL(INDIRI,SPACE(35))
    this.w_LOCALI = NVL(LOCALI,SPACE(30))
    this.w_CAPCLF = NVL(CAPCLF,SPACE(8))
    this.w_PROVIN = NVL(PROVIN,SPACE(2))
    this.w_VALCLF = NVL(VALCLF,SPACE(3))
    this.w_SIMCLF = NVL(SIMCLF,SPACE(5))
    this.w_ARRCLF = NVL(ARRCLF,0)
    this.w_TOTIMP = NVL(TOTIMP,0)
    this.w_CAOAPP = NVL(CAOAPP,0)
    this.w_PTFLRAGG = NVL(FLRAGG," ")
    this.w_FLDAVE = NVL(FLDAVE," ")
    this.w_RAGSOC = NVL(RAGSOC, SPACE(40))
    INSERT INTO APPO1;
    (TIPCON, CODCON, ORDINE, DATSCA, NUMPAR, CODVAL, ;
    FLCRSA, IMPDAR, IMPAVE, SIMVAL, DECVAL, MODPAG, ;
    NUMDOC, ALFDOC, DATDOC, FLSOSP, DESCON, INDIRI, ;
    LOCALI, CAPCLF, PROVIN, VALCLF, SIMCLF, ARRCLF, TOTIMP, ; 
    CAOAPP, PTSERIAL, FLRAGG, FLDAVE, RAGSOC,PTDATSCA,PTNUMPAR,CODAGE,DESAGE);
    VALUES;
    (this.w_TIPCON, this.w_CODCON, this.w_ORDINE, this.w_ODATSCA, this.w_ONUMPAR, this.w_CODVAL, ;
    this.w_FLCRSA, this.w_IMPDAR, this.w_IMPAVE, this.w_SIMVAL, this.w_DECVAL, this.w_MODPAG, ;
    this.w_NUMDOC, this.w_ALFDOC, this.w_DATDOC, this.w_FLSOSP1, this.w_DESCON, this.w_INDIRI, ;
    this.w_LOCALI, this.w_CAPCLF, this.w_PROVIN, this.w_VALCLF, this.w_SIMCLF, this.w_ARRCLF, this.w_TOTIMP, ; 
    this.w_CAOAPP, this.w_SERIAL, this.w_PTFLRAGG, this.w_FLDAVE, this.w_RAGSOC,this.w_DATSCA,this.w_NUMPAR,this.w_OCODAGE,this.w_ODESAGE)
    if this.w_FLRAGGCR = "S" and not used("APPO2")
      this.Pag7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if used("APPO2")
      SELECT APPO2
    else
      SELECT APPO
    endif
    ENDSCAN
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,9)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='CAM_BI'
    this.cWorkTables[3]='VALUTE'
    this.cWorkTables[4]='CONTI'
    this.cWorkTables[5]='*TMP_ESTRATTOCONTO'
    this.cWorkTables[6]='DES_DIVE'
    this.cWorkTables[7]='*ZOOMPART'
    this.cWorkTables[8]='PAR_TITE'
    this.cWorkTables[9]='*TMP_PART'
    return(this.OpenAllTables(9))

  proc CloseCursors()
    if used('_Curs_DES_DIVE')
      use in _Curs_DES_DIVE
    endif
    if used('_Curs_ZOOMPART')
      use in _Curs_ZOOMPART
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
