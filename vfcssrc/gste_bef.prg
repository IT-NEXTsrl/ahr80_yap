* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bef                                                        *
*              Stampa effetti/R.B.                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_75]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-03                                                      *
* Last revis.: 2007-11-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bef",oParentObject)
return(i_retval)

define class tgste_bef as StdBatch
  * --- Local variables
  w_NUMDIS = space(10)
  w_DESCRI = space(35)
  w_DIVISA = space(3)
  w_SIMDIV = space(5)
  w_CODCON = space(15)
  w_PROVIN = space(2)
  w_DECIMI = 0
  w_CODNAZ = space(3)
  w_NOMDES = space(40)
  w_CODDES = space(5)
  w_INDIRI = space(35)
  w_CAP = space(8)
  w_TIPODIS = space(2)
  w_LOCALI = space(30)
  w_CODCON = space(15)
  w_TIPCON = space(1)
  w_APPO = 0
  w_APPO1 = space(10)
  w_APPO2 = space(10)
  w_LETTERE = space(60)
  w_DTOBSO = ctod("  /  /  ")
  w_DATVAL = ctod("  /  /  ")
  w_DTOBS1 = ctod("  /  /  ")
  w_CENTESIMI = 0
  w_TESDES = .f.
  * --- WorkFile variables
  DIS_TINT_idx=0
  DES_DIVE_idx=0
  CONTI_idx=0
  VALUTE_idx=0
  COC_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa Effetti (da GSTE_SEF)
    * --- Numero distinta e tipo di stampa
    this.w_NUMDIS = this.oParentObject.w_NUMDISM
    this.w_TIPCON = "G"
    this.w_CODCON = " "
    if EMPTY(this.w_NUMDIS)
      ah_ErrorMsg("Nessuna distinta selezionata",,"")
      i_retcode = 'stop'
      return
    else
      * --- Lettura Conto Corrente di Riferimento
      * --- Read from DIS_TINT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DIS_TINT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DIS_TINT_idx,2],.t.,this.DIS_TINT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "DIBANRIF,DICODVAL,DITIPDIS"+;
          " from "+i_cTable+" DIS_TINT where ";
              +"DINUMDIS = "+cp_ToStrODBC(this.w_NUMDIS);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          DIBANRIF,DICODVAL,DITIPDIS;
          from (i_cTable) where;
              DINUMDIS = this.w_NUMDIS;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODCON = NVL(cp_ToDate(_read_.DIBANRIF),cp_NullValue(_read_.DIBANRIF))
        this.w_DIVISA = NVL(cp_ToDate(_read_.DICODVAL),cp_NullValue(_read_.DICODVAL))
        this.w_TIPODIS = NVL(cp_ToDate(_read_.DITIPDIS),cp_NullValue(_read_.DITIPDIS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Check che il tipo della distinta si� congruente con il tipo di stampa
      if this.w_TIPODIS<>this.oParentObject.w_FLSTA
        ah_ErrorMsg("Tipo di stampa incongruente con la distinta selezionata",,"")
        i_retcode = 'stop'
        return
      else
        if EMPTY(this.w_CODCON)
          ah_ErrorMsg("Manca la banca di presentazione",,"")
          i_retcode = 'stop'
          return
        else
          * --- Descrizione Conto
          * --- Read from COC_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.COC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2],.t.,this.COC_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "BADESCRI"+;
              " from "+i_cTable+" COC_MAST where ";
                  +"BACODBAN = "+cp_ToStrODBC(this.w_CODCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              BADESCRI;
              from (i_cTable) where;
                  BACODBAN = this.w_CODCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DESCRI = NVL(cp_ToDate(_read_.BADESCRI),cp_NullValue(_read_.BADESCRI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
      endif
    endif
    this.w_APPO = 0
    * --- Legge Valuta Originaria Distinta
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VASIMVAL,VADECTOT,VACAOVAL"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_DIVISA);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VASIMVAL,VADECTOT,VACAOVAL;
        from (i_cTable) where;
            VACODVAL = this.w_DIVISA;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_SIMDIV = NVL(cp_ToDate(_read_.VASIMVAL),cp_NullValue(_read_.VASIMVAL))
      this.w_DECIMI = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      this.w_APPO = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Se sulla Maschera imposto il Flag Importi in euro ma la Distinta e' gia' in EURO o e' extra EMU non deve fare nulla
    this.oParentObject.w_FLEURO = IIF(this.w_DIVISA=g_PERVAL OR this.w_APPO=0, " ", this.oParentObject.w_FLEURO)
    if this.oParentObject.w_FLEURO="S"
      * --- Coverto I dati Valuta in Euro
      this.w_DIVISA = g_PERVAL
      * --- Read from VALUTE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VALUTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "VASIMVAL,VADECTOT"+;
          " from "+i_cTable+" VALUTE where ";
              +"VACODVAL = "+cp_ToStrODBC(this.w_DIVISA);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          VASIMVAL,VADECTOT;
          from (i_cTable) where;
              VACODVAL = this.w_DIVISA;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_SIMDIV = NVL(cp_ToDate(_read_.VASIMVAL),cp_NullValue(_read_.VASIMVAL))
        this.w_DECIMI = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    BANCATES=this.w_DESCRI
    SIMDIV=this.w_SIMDIV
    * --- Effetti Ordinati per numero effetto
    VQ_EXEC("QUERY\GSTE_SEF.VQR",this,"EFFETTI")
    select EFFETTI
    go top
    scan
    this.w_DTOBSO = NVL(CP_TODATE(DDDTOBSO),cp_CharToDate("  -  -  "))
    this.w_DATVAL = NVL(CP_TODATE(DIDATVAL),cp_CharToDate("  -  -  "))
    * --- Controllo se il cliente ha una sede di tipo 'PAGAMENTO'
    do case
      case NVL(DDTIPRIF,"")="PA" AND (this.w_DTOBSO>this.w_DATVAL OR EMPTY(this.w_DTOBSO))
        * --- Sostituisco al valore dei campi dell'anagrafica del cliente il valore presente nelle sedi.
        select EFFETTI
        REPLACE ANDESCRI with DDNOMDES
        REPLACE ANINDIRI with DDINDIRI
        REPLACE AN___CAP with DD___CAP
        REPLACE ANLOCALI with DDLOCALI
        REPLACE ANPROVIN with DDPROVIN
        REPLACE ANNAZION with DDCODNAZ
      case NVL(DDTIPRIF,"")<>"PA" OR (this.w_DTOBSO<=this.w_DATVAL AND NOT EMPTY(this.w_DTOBSO))
        this.w_TIPCON = PNTIPCON
        this.w_CODCON = PNCODCON
        * --- Effettuo una READ all'interno delle Destinazioni Diverse per poter vedere se ci sono delle sedi di tipo Fatturazione
        this.w_TESDES = .F.
        * --- Select from DES_DIVE
        i_nConn=i_TableProp[this.DES_DIVE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" DES_DIVE ";
              +" where DDTIPCON="+cp_ToStrODBC(this.w_TIPCON)+" AND DDCODICE="+cp_ToStrODBC(this.w_CODCON)+" AND DDTIPRIF='FA'";
               ,"_Curs_DES_DIVE")
        else
          select * from (i_cTable);
           where DDTIPCON=this.w_TIPCON AND DDCODICE=this.w_CODCON AND DDTIPRIF="FA";
            into cursor _Curs_DES_DIVE
        endif
        if used('_Curs_DES_DIVE')
          select _Curs_DES_DIVE
          locate for 1=1
          do while not(eof())
          this.w_NOMDES = NVL(_Curs_DES_DIVE.DDNOMDES,SPACE(40))
          this.w_INDIRI = NVL(_Curs_DES_DIVE.DDINDIRI,SPACE(35))
          this.w_CAP = NVL(_Curs_DES_DIVE.DD___CAP,SPACE(8))
          this.w_LOCALI = NVL(_Curs_DES_DIVE.DDLOCALI, SPACE(30))
          this.w_PROVIN = NVL(_Curs_DES_DIVE.DDPROVIN,SPACE(2))
          this.w_CODNAZ = NVL(_Curs_DES_DIVE.DDCODNAZ,SPACE(3))
          this.w_CODDES = NVL(_Curs_DES_DIVE.DDCODDES,SPACE(5))
          this.w_DTOBS1 = NVL(CP_TODATE(_Curs_DES_DIVE.DDDTOBSO),cp_CharToDate("  -  -  "))
          this.w_TESDES = .T.
            select _Curs_DES_DIVE
            continue
          enddo
          use
        endif
        * --- Se esiste almeno un record effettuo delle replace sui dati anagrafici del cliente
        select EFFETTI
        if NOT EMPTY(NVL(this.w_CODDES,"")) AND (this.w_DTOBS1 > this.w_DATVAL OR EMPTY(this.w_DTOBS1)) AND this.w_TESDES
          REPLACE ANDESCRI with this.w_NOMDES
          REPLACE ANINDIRI with this.w_INDIRI
          REPLACE AN___CAP with this.w_CAP
          REPLACE ANLOCALI with this.w_LOCALI
          REPLACE ANPROVIN with this.w_PROVIN
          REPLACE ANNAZION with this.w_CODNAZ
        endif
    endcase
    * --- Converte gli Importi EMU in EURO
    if this.oParentObject.w_FLEURO="S" AND PTCODVAL<>g_PERVAL AND NVL(VACAOVAL, 0)<>0
      this.w_APPO = GETCAM(PTCODVAL,this.oParentObject.w_DATSTA)
      REPLACE PTTOTIMP WITH VAL2MON(PTTOTIMP, this.w_APPO,1,this.oParentObject.w_DATSTA, g_PERVAL,this.w_DECIMI)
      REPLACE PNTOTDOC WITH VAL2MON(PNTOTDOC , this.w_APPO,1,this.oParentObject.w_DATSTA, g_PERVAL,this.w_DECIMI)
    endif
    * --- Calcola gli importi in lettere 
    this.w_LETTERE = ALLTRIM(FUNTOC(INT(PTTOTIMP)))
    if this.w_DECIMI>0
      * --- Eventuali Centesimi
      this.w_APPO1 = STR(INT((PTTOTIMP-INT(PTTOTIMP)) * 100))
      * --- Utilizzo questo assegnamento per fare in modo che nel caso di decimali con una sola cifra 
      *     venga inserito lo 0 prima.
      *     Es 111.01 In questo modo riporta 01
      this.w_APPO2 = RIGHT("0000000000" + ALLTRIM(this.w_APPO1), this.w_DECIMI)
      this.w_LETTERE = this.w_LETTERE 
    endif
    REPLACE LETTERE WITH this.w_LETTERE
    REPLACE CENTESIMI WITH this.w_APPO2
    endscan
    * ---  (order by PTNUMEFF)
    select * from EFFETTI order by 8 into cursor __TMP__
    if used("EFFETTI")
      select EFFETTI
      use
    endif
    * --- Decimali della divisa della distinta
    L_DECIMI=this.w_DECIMI
    do case
      case this.oParentObject.w_FLSTA="CA"
        * --- cAMBIALI - TRATTE
        datsta=this.oParentObject.w_DATSTA
        cp_chprn("query\gsteTsef.frx", " ", this)
      case this.oParentObject.w_FLSTA="RB"
        * --- RICEVUTE BANCARIE
        cp_chprn("query\gste_sef.frx", " ", this)
    endcase
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='DIS_TINT'
    this.cWorkTables[2]='DES_DIVE'
    this.cWorkTables[3]='CONTI'
    this.cWorkTables[4]='VALUTE'
    this.cWorkTables[5]='COC_MAST'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_DES_DIVE')
      use in _Curs_DES_DIVE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
