* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_b07                                                        *
*              Elabora doc.evasi x S.A.                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_335]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-02-13                                                      *
* Last revis.: 2016-07-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_b07",oParentObject)
return(i_retval)

define class tgscg_b07 as StdBatch
  * --- Local variables
  w_MVIVAINCDOC = space(5)
  w_MVIVATRADOC = space(5)
  w_MVIVAIMBDOC = space(5)
  w_CODBUN1 = space(3)
  w_TIPSOT = space(1)
  w_LASTIND = 0
  w_LOOP = 0
  AI = space(10)
  w_CODVAL = space(3)
  w_TOTVAL = 0
  w_XINICOM = ctod("  /  /  ")
  w_APPO = space(10)
  w_CAOVAL = 0
  w_PERIVA = 0
  w_PERIVA1 = 0
  w_XFINCOM = ctod("  /  /  ")
  w_OSERIAL = space(10)
  w_DECTOT = 0
  w_PERIND = 0
  w_APPVAL = 0
  w_FATEME = space(15)
  w_TESVAL = 0
  w_APPO1 = 0
  w_APPSEZ = space(1)
  w_FATRIC = space(15)
  w_CODBUN = space(3)
  w_TOTDOC = 0
  w_FLVAL = space(1)
  w_IVADEB = space(15)
  w_CODIVE = space(5)
  w_SEZCLF = space(1)
  w_APPO2 = 0
  w_IVACRE = space(15)
  w_FLRINC = space(1)
  w_ARCATCON = space(3)
  w_SCOMER = 0
  w_CONINC = space(15)
  w_CONIMB = space(15)
  w_CONTRA = space(15)
  w_CONBOL = space(15)
  w_ACQINC = space(15)
  w_ACQIMB = space(15)
  w_ACQTRA = space(15)
  w_ACQBOL = space(15)
  w_FLRIMB = space(1)
  w_CODIVA = space(5)
  w_TOTDAR = 0
  w_FLRTRA = space(1)
  w_APPOK = .f.
  w_TOTAVE = 0
  w_SPEINC = 0
  w_CONTRO = space(15)
  w_IMPDAR = 0
  w_SPEIMB = 0
  w_CONIND = space(15)
  w_IMPAVE = 0
  w_CONDIC = space(15)
  w_SPETRA = 0
  w_FLOMAG = space(1)
  w_MVIVAINC = space(5)
  w_CONARR = space(15)
  w_SPEBOL = 0
  w_VALMAG = 0
  w_MVIVATRA = space(5)
  w_MVCODIVA = space(5)
  w_FLVEAC = space(1)
  w_ARROTA = 0
  w_CONVEA = space(15)
  w_MVIVAIMB = space(5)
  w_CODCON = space(15)
  w_CFCATCON = space(3)
  w_CONOMA = space(15)
  w_APPO3 = 0
  w_CONIVA = space(15)
  w_IVAGLO = 0
  w_VALOMA = 0
  w_CONOMI = space(15)
  w_ALIVA = 0
  w_INICOM = ctod("  /  /  ")
  w_IVADET = 0
  w_CONNAC = space(15)
  w_FLINTR = space(1)
  w_FINCOM = ctod("  /  /  ")
  w_IVAOMA = 0
  w_XCODICE = space(15)
  w_OLDTOTDOC = 0
  w_DATDOC = ctod("  /  /  ")
  w_IVAIND = 0
  w_XCODBUN = space(3)
  w_OLDVALMAG = 0
  w_TESINICOM = ctod("  /  /  ")
  w_COMP = .f.
  w_TESFINCOM = ctod("  /  /  ")
  w_CANCELLA = .f.
  w_PNFLZERO = space(1)
  w_DESCRI = space(50)
  w_TDCONASS = space(1)
  w_MVCODCEN = space(15)
  w_MVTCOCEN = space(15)
  w_MVTCOMME = space(15)
  w_MVVOCCEN = space(15)
  w_MVCODCOM = space(15)
  w_XCODCEN = space(15)
  w_XVOCCEN = space(15)
  w_XCODCOM = space(15)
  w_TIPCON = space(1)
  w_CONSUP = space(15)
  w_SEZB = space(1)
  w_VOCINC = space(15)
  w_VOCIMB = space(15)
  w_VOCTRA = space(15)
  w_VOCBOL = space(15)
  w_VOCARR = space(15)
  w_VOCDIF = space(15)
  w_VOCIND = space(15)
  w_TIPO = space(1)
  w_CODTESTA = space(15)
  w_APPIMB = 0
  w_trov = .f.
  w_INICOMCO = ctod("  /  /  ")
  w_FINCOMCO = ctod("  /  /  ")
  w_GIOCOM = 0
  w_GIOPER = 0
  w_MASTRCON = space(15)
  w_SEZBICON = space(1)
  w_INDIVA = space(1)
  w_ARASSIVA = space(1)
  w_XVALMAG = 0
  w_IVASPESETOT = 0
  w_IVASPESE = 0
  w_RIPSPESE = .f.
  * --- WorkFile variables
  VOCIIVA_idx=0
  CONTVEAC_idx=0
  CONTI_idx=0
  MASTRI_idx=0
  CONTROPA_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questo batch viene usato in Elaborazione Scritture di Assestamento (da GSCG_BAT)
    *     Batch utilizzato per la generazione delle scritture di assestamento per le 
    *     Fatture da Emettere.
    * --- Pagina 5 Inizializza le variabili
    DIMENSION AI[6, 10]
    * --- Testa il Cambio di Documento
    this.w_OSERIAL = REPL("#", 10)
    * --- Parametri dalla Maschera
    this.w_FATEME = this.oParentObject.oParentObject.w_COFATEME
    this.w_FATRIC = this.oParentObject.oParentObject.w_COFATRIC
    this.w_IVADEB = this.oParentObject.oParentObject.w_COIVADEB
    this.w_IVACRE = this.oParentObject.oParentObject.w_COIVACRE
    this.w_CONINC = this.oParentObject.oParentObject.w_COCONINC
    this.w_CONIMB = this.oParentObject.oParentObject.w_COCONIMB
    this.w_CONTRA = this.oParentObject.oParentObject.w_COCONTRA
    this.w_CONBOL = this.oParentObject.oParentObject.w_COCONBOL
    this.w_ACQINC = this.oParentObject.oParentObject.w_COACQINC
    this.w_ACQIMB = this.oParentObject.oParentObject.w_COACQIMB
    this.w_ACQTRA = this.oParentObject.oParentObject.w_COACQTRA
    this.w_ACQBOL = this.oParentObject.oParentObject.w_COACQBOL
    this.w_CONDIC = this.oParentObject.oParentObject.w_COCONDIC
    this.w_CONARR = this.oParentObject.oParentObject.w_COCONARR
    this.w_INDIVA = this.oParentObject.oParentObject.w_COINDIVA
    if USED("EXTFA003")
      * --- Crea il Temporaneo di Elaborazione Primanota
      if NOT USED("EXTCG001")
        CREATE CURSOR EXTCG001 ; 
 (TIPCON C(1), CODICE C(15), CODBUN C(3), INICOM D(8), FINCOM D(8),VALNAZ C(3), DATREG D(8), ; 
 IMPDAR N(18,4), IMPAVE N(18,4), SERIAL C(10), DESCRI C(50),CODCEN C(15),; 
 VOCCEN C(15),CODCOM C(15),TIPO C(1),CODCON C(15))
      endif
      * --- Inizio Aggiornamento vero e proprio
      ah_Msg("Elaborazione documenti da generare...",.T.)
      SELECT EXTFA003
      GO TOP
      SCAN FOR NOT EMPTY(NVL(MVSERIAL," "))
      * --- Testa Cambio Documento
      if this.w_OSERIAL<>MVSERIAL
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        SELECT EXTFA003
        * --- Inizializza i dati di Testata del Nuovo Documento
        this.w_FLVEAC = NVL(MVFLVEAC, " ")
        this.w_CODCON = IIF(this.w_FLVEAC="V", this.w_FATEME, this.w_FATRIC)
        this.w_CONIVA = IIF(this.w_FLVEAC="V", this.w_IVADEB, this.w_IVACRE)
        this.w_TESINICOM = NVL(TINCOM, cp_CharToDate("  -  -  "))
        this.w_TESFINCOM = NVL(TFICOM, cp_CharToDate("  -  -  "))
        this.w_COMP = (this.w_TESINICOM<=this.oParentObject.oParentObject.w_ASDATFIN AND this.w_TESFINCOM>=this.oParentObject.oParentObject.w_ASDATINI)
        this.w_COMP = this.w_COMP OR EMPTY(this.w_TESINICOM)
        this.w_DATDOC = NVL(MVDATDOC, cp_CharToDate("  -  -  "))
        this.w_CODVAL = NVL(MVCODVAL, g_PERVAL)
        this.w_CAOVAL = NVL(MVCAOVAL, 1)
        this.w_DECTOT = NVL(VADECTOT, 0)
        this.w_TESVAL = NVL(VACAOVAL, 0)
        this.w_CODIVE = NVL(MVCODIVE, SPACE(5))
        this.w_FLRINC = NVL(MVFLRINC, " ")
        this.w_FLRIMB = NVL(MVFLRIMB, " ")
        this.w_FLRTRA = NVL(MVFLRTRA, " ")
        this.w_SPEINC = NVL(MVSPEINC, 0)
        this.w_SPEIMB = NVL(MVSPEIMB, 0)
        this.w_SPETRA = NVL(MVSPETRA, 0)
        this.w_MVCODIVA = NVL(MVCODIVA, SPACE(5))
        this.w_MVIVAINC = iif(empty(NVL(MVIVAINC, SPACE(5))),this.w_MVCODIVA,NVL(MVIVAINC, SPACE(5)))
        this.w_MVIVAINCDOC = NVL(MVIVAINC, SPACE(5))
        this.w_MVIVATRA = iif(empty(NVL(MVIVATRA, SPACE(5))),this.w_MVCODIVA,NVL(MVIVATRA, SPACE(5)))
        this.w_MVIVATRADOC = NVL(MVIVATRA, SPACE(5))
        this.w_MVIVAIMB = iif(empty(NVL(MVIVAIMB, SPACE(5))),this.w_MVCODIVA,NVL(MVIVAIMB, SPACE(5)))
        this.w_MVIVAIMBDOC = NVL(MVIVAIMB, SPACE(5))
        this.w_SPEBOL = NVL(MVSPEBOL, 0)
        this.w_ARROTA = NVL(MVIMPARR, 0)
        this.w_TDCONASS = NVL(TDCONASS,"F")
        this.w_CFCATCON = NVL(ANCATCON, SPACE(3))
        this.w_FLINTR = NVL(MVFLINTR, " ")
        this.w_IVAGLO = 0
        this.w_IVADET = 0
        this.w_IVAOMA = 0
        this.w_IVAIND = 0
        this.w_TOTVAL = this.w_ARROTA
        FOR L_i = 1 TO 6
        AI[L_i, 1] = " "
        AI[L_i, 2] = SPACE(5)
        AI[L_i, 3] = 0
        AI[L_i, 4] = 0
        AI[L_i, 5] = 0
        AI[L_i, 6] = 0
        AI[L_i, 7] = SPACE(5)
        AI[L_i, 9] = 0
        AI[L_i, 10] = " "
        ENDFOR
        this.w_ALIVA = 0
        * --- Totale Documento in Moneta di Conto
        this.w_TOTDOC = 0
        this.w_OLDTOTDOC = 0
        * --- Sezione Cli/For (Dare/Avere)
        this.w_SEZCLF = IIF(this.w_FLVEAC="V", "D", "A")
        * --- Salvo la Business Unit della prima riga
        if g_APPLICATION <> "ADHOC REVOLUTION"
          this.w_CODBUN1 = NVL(MVCODBUN, SPACE(3))
        else
          this.w_CODBUN1 = SPACE(3)
        endif
      endif
      SELECT EXTFA003
      this.w_OSERIAL = MVSERIAL
      this.w_DESCRI = alltrim(nvl(MVTIPDOC,""))+" "+ALLTRIM(STR(MVNUMDOC,15))+IIF(EMPTY(NVL(MVALFDOC,"")),"","/")
      this.w_DESCRI = this.w_DESCRI+ALLTRIM(NVL(MVALFDOC,""))+" del "+DTOC(MVDATDOC)
      this.w_DESCRI = this.w_DESCRI + IIF(NVL(MVTIPCON,"")="C"," Cli. ", IIF(NVL(MVTIPCON,"")="F"," For. ","")) + ALLTRIM(NVL(MVCODCON,""))
      this.w_INICOM = CP_TODATE(MVINICOM)
      this.w_FINCOM = CP_TODATE(MVFINCOM)
      this.w_INICOMCO = CP_TODATE(MVTINCOM)
      this.w_FINCOMCO = CP_TODATE(MVTFICOM)
      * --- Scrive nuova Riga sul Temporaneo di Appoggio
      this.w_TIPO = MVTIPCON
      this.w_CODTESTA = MVCODCON
      this.w_ARCATCON = NVL(MVCATCON, SPACE(3))
      this.w_CODIVA = NVL(MVCODIVA, SPACE(5))
      this.w_MVCODCEN = NVL(MVCODCEN,SPACE(15))
      this.w_MVVOCCEN = NVL(MVVOCCEN,SPACE(15))
      this.w_MVCODCOM = NVL(MVCODCOM,SPACE(15))
      if g_APPLICATION <> "ADHOC REVOLUTION"
        this.w_CODBUN = NVL(MVCODBUN, SPACE(3))
        this.w_MVTCOCEN = NVL(MVTCOCEN,SPACE(15))
        this.w_MVTCOMME = NVL(MVTCOMME,SPACE(15))
      else
        this.w_CODBUN = SPACE(3)
        this.w_MVTCOCEN = SPACE(15)
        this.w_MVTCOMME = SPACE(15)
      endif
      if NOT EMPTY(this.w_CODIVE)
        * --- Se Doc. Esente verifica che il Codice IVA della Riga sia Coerente altrimenti prende quello esente
        this.w_APPOK = .F.
        FOR L_i = 1 TO 6
        if this.w_CODIVA = AI[L_i, 7] AND NOT EMPTY(this.w_CODIVA)
          this.w_APPOK = .T.
          EXIT
        endif
        ENDFOR
        this.w_CODIVA = IIF(this.w_APPOK=.F., this.w_CODIVE, this.w_CODIVA)
      endif
      this.w_ARASSIVA = iif(this.w_FLVEAC="V", NVL(ARASSIVA, " "), " ")
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVPERIVA"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.w_CODIVA);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVPERIVA;
          from (i_cTable) where;
              IVCODIVA = this.w_CODIVA;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PERIVA1 = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_CONTRO = NVL(MVCONTRO, SPACE(15))
      this.w_CONIND = NVL(MVCONIND, SPACE(15))
      * --- Legge Contropartite Vendite/Acquisti
      this.w_CONVEA = SPACE(15)
      this.w_CONOMA = SPACE(15)
      this.w_CONOMI = SPACE(15)
      this.w_CONNAC = SPACE(15)
      this.w_VOCINC = SPACE(15)
      this.w_VOCIMB = SPACE(15)
      this.w_VOCTRA = SPACE(15)
      this.w_VOCBOL = SPACE(15)
      this.w_VOCARR = SPACE(15)
      this.w_VOCDIF = SPACE(15)
      this.w_VOCIND = SPACE(15)
      if NOT EMPTY(this.w_ARCATCON) AND NOT EMPTY(this.w_CFCATCON)
        if this.w_FLVEAC="V"
          * --- Contropartite Vendite
          * --- Read from CONTVEAC
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTVEAC_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTVEAC_idx,2],.t.,this.CONTVEAC_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CVCONRIC,CVCONOMV,CVIVAOMA,CVCONNCC"+;
              " from "+i_cTable+" CONTVEAC where ";
                  +"CVCODART = "+cp_ToStrODBC(this.w_ARCATCON);
                  +" and CVCODCLI = "+cp_ToStrODBC(this.w_CFCATCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CVCONRIC,CVCONOMV,CVIVAOMA,CVCONNCC;
              from (i_cTable) where;
                  CVCODART = this.w_ARCATCON;
                  and CVCODCLI = this.w_CFCATCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CONVEA = NVL(cp_ToDate(_read_.CVCONRIC),cp_NullValue(_read_.CVCONRIC))
            this.w_CONOMA = NVL(cp_ToDate(_read_.CVCONOMV),cp_NullValue(_read_.CVCONOMV))
            this.w_CONOMI = NVL(cp_ToDate(_read_.CVIVAOMA),cp_NullValue(_read_.CVIVAOMA))
            this.w_CONNAC = NVL(cp_ToDate(_read_.CVCONNCC),cp_NullValue(_read_.CVCONNCC))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Leggo i dati analitici per le spese 
          if g_APPLICATION <> "ADHOC REVOLUTION"
            * --- Read from CONTROPA
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CONTROPA_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2],.t.,this.CONTROPA_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "COVOCBOL,COVOCTRA,COVOCIMB,COVOCINC,COVOCARR,COVOCDIF,COVOCIND"+;
                " from "+i_cTable+" CONTROPA where ";
                    +"COCODAZI = "+cp_ToStrODBC(i_Codazi);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                COVOCBOL,COVOCTRA,COVOCIMB,COVOCINC,COVOCARR,COVOCDIF,COVOCIND;
                from (i_cTable) where;
                    COCODAZI = i_Codazi;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_VOCBOL = NVL(cp_ToDate(_read_.COVOCBOL),cp_NullValue(_read_.COVOCBOL))
              this.w_VOCTRA = NVL(cp_ToDate(_read_.COVOCTRA),cp_NullValue(_read_.COVOCTRA))
              this.w_VOCIMB = NVL(cp_ToDate(_read_.COVOCIMB),cp_NullValue(_read_.COVOCIMB))
              this.w_VOCINC = NVL(cp_ToDate(_read_.COVOCINC),cp_NullValue(_read_.COVOCINC))
              this.w_VOCARR = NVL(cp_ToDate(_read_.COVOCARR),cp_NullValue(_read_.COVOCARR))
              this.w_VOCDIF = NVL(cp_ToDate(_read_.COVOCDIF),cp_NullValue(_read_.COVOCDIF))
              this.w_VOCIND = NVL(cp_ToDate(_read_.COVOCIND),cp_NullValue(_read_.COVOCIND))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
        else
          * --- Contropartite Acquisti
          * --- Read from CONTVEAC
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTVEAC_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTVEAC_idx,2],.t.,this.CONTVEAC_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CVCONACQ,CVCONOMA,CVIVAOMC,CVCONNCF"+;
              " from "+i_cTable+" CONTVEAC where ";
                  +"CVCODART = "+cp_ToStrODBC(this.w_ARCATCON);
                  +" and CVCODCLI = "+cp_ToStrODBC(this.w_CFCATCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CVCONACQ,CVCONOMA,CVIVAOMC,CVCONNCF;
              from (i_cTable) where;
                  CVCODART = this.w_ARCATCON;
                  and CVCODCLI = this.w_CFCATCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CONVEA = NVL(cp_ToDate(_read_.CVCONACQ),cp_NullValue(_read_.CVCONACQ))
            this.w_CONOMA = NVL(cp_ToDate(_read_.CVCONOMA),cp_NullValue(_read_.CVCONOMA))
            this.w_CONOMI = NVL(cp_ToDate(_read_.CVIVAOMC),cp_NullValue(_read_.CVIVAOMC))
            this.w_CONNAC = NVL(cp_ToDate(_read_.CVCONNCF),cp_NullValue(_read_.CVCONNCF))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Leggo i dati analitici per le spese 
          if g_APPLICATION <> "ADHOC REVOLUTION"
            * --- Read from CONTROPA
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CONTROPA_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2],.t.,this.CONTROPA_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "COVOABOL,COVOATRA,COVOAIMB,COVOAINC,COVOCARR,COVOCDIF,COVOCIND"+;
                " from "+i_cTable+" CONTROPA where ";
                    +"COCODAZI = "+cp_ToStrODBC(i_Codazi);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                COVOABOL,COVOATRA,COVOAIMB,COVOAINC,COVOCARR,COVOCDIF,COVOCIND;
                from (i_cTable) where;
                    COCODAZI = i_Codazi;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_VOCBOL = NVL(cp_ToDate(_read_.COVOABOL),cp_NullValue(_read_.COVOABOL))
              this.w_VOCTRA = NVL(cp_ToDate(_read_.COVOATRA),cp_NullValue(_read_.COVOATRA))
              this.w_VOCIMB = NVL(cp_ToDate(_read_.COVOAIMB),cp_NullValue(_read_.COVOAIMB))
              this.w_VOCINC = NVL(cp_ToDate(_read_.COVOAINC),cp_NullValue(_read_.COVOAINC))
              this.w_VOCARR = NVL(cp_ToDate(_read_.COVOCARR),cp_NullValue(_read_.COVOCARR))
              this.w_VOCDIF = NVL(cp_ToDate(_read_.COVOCDIF),cp_NullValue(_read_.COVOCDIF))
              this.w_VOCIND = NVL(cp_ToDate(_read_.COVOCIND),cp_NullValue(_read_.COVOCIND))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
        endif
        SELECT EXTFA003
      endif
      this.w_TIPSOT = " "
      this.w_SEZBICON = " "
      if Not Empty(this.w_CONTRO)
        * --- Se contropartita specificata nel documento la applico senza fare
        *     incrocio Intestatario\Articolo
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANCONSUP,ANTIPSOT"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC("G");
                +" and ANCODICE = "+cp_ToStrODBC(this.w_CONTRO);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANCONSUP,ANTIPSOT;
            from (i_cTable) where;
                ANTIPCON = "G";
                and ANCODICE = this.w_CONTRO;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MASTRCON = NVL(cp_ToDate(_read_.ANCONSUP),cp_NullValue(_read_.ANCONSUP))
          this.w_TIPSOT = NVL(cp_ToDate(_read_.ANTIPSOT),cp_NullValue(_read_.ANTIPSOT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from MASTRI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.MASTRI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MASTRI_idx,2],.t.,this.MASTRI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MCSEZBIL"+;
            " from "+i_cTable+" MASTRI where ";
                +"MCCODICE = "+cp_ToStrODBC(this.w_MASTRCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MCSEZBIL;
            from (i_cTable) where;
                MCCODICE = this.w_MASTRCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SEZBICON = NVL(cp_ToDate(_read_.MCSEZBIL),cp_NullValue(_read_.MCSEZBIL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if Not (this.w_TIPSOT="M" or this.w_SEZBICON $ "CR")
          * --- Se contropartita specificata non di tipo cespite o costo\ricavo
          *     devo applicare quella determinata attraverso incrocio
          this.w_CONTRO = this.w_CONVEA
        endif
      else
        this.w_CONTRO = this.w_CONVEA
      endif
      if Not (this.w_TIPSOT="M" or this.w_SEZBICON $ "CR")
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANCONSUP,ANTIPSOT"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC("G");
                +" and ANCODICE = "+cp_ToStrODBC(this.w_CONTRO);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANCONSUP,ANTIPSOT;
            from (i_cTable) where;
                ANTIPCON = "G";
                and ANCODICE = this.w_CONTRO;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MASTRCON = NVL(cp_ToDate(_read_.ANCONSUP),cp_NullValue(_read_.ANCONSUP))
          this.w_TIPSOT = NVL(cp_ToDate(_read_.ANTIPSOT),cp_NullValue(_read_.ANTIPSOT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from MASTRI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.MASTRI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MASTRI_idx,2],.t.,this.MASTRI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MCSEZBIL"+;
            " from "+i_cTable+" MASTRI where ";
                +"MCCODICE = "+cp_ToStrODBC(this.w_MASTRCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MCSEZBIL;
            from (i_cTable) where;
                MCCODICE = this.w_MASTRCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SEZBICON = NVL(cp_ToDate(_read_.MCSEZBIL),cp_NullValue(_read_.MCSEZBIL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      this.w_CONIND = IIF(EMPTY(this.w_CONIND), this.w_CONTRO, this.w_CONIND)
      * --- CONTROLLIAMO SEZIONALE CONTO
      * --- ESCLUDIAMO I CONTI DI TIPO PATRIMONIALE
      if this.w_SEZBICON $ "AP" And Nvl(this.w_TIPSOT,"X")<>"M"
        LOOP
      endif
      this.w_FLOMAG = NVL(MVFLOMAG, " ")
      if NOT EMPTY(this.w_INICOMCO) AND NOT EMPTY(this.w_FINCOMCO)
        if this.w_FINCOMCO>=this.oParentObject.oParentObject.w_ASDATINI AND this.w_INICOMCO<=this.oParentObject.oParentObject.w_ASDATFIN
          this.w_GIOCOM = (this.w_FINCOMCO+1) - this.w_INICOMCO
          this.w_GIOPER = MIN(this.oParentObject.oParentObject.w_ASDATFIN, this.w_FINCOMCO) - MAX(this.oParentObject.oParentObject.w_ASDATINI, this.w_INICOMCO) + 1
          this.w_VALMAG = cp_ROUND((NVL(VALMAG,0) * this.w_GIOPER) / this.w_GIOCOM, this.w_DECTOT)
        else
          this.w_GIOCOM = 1
          this.w_GIOPER = 1
          this.w_VALMAG = 0
        endif
      else
        this.w_GIOCOM = 1
        this.w_GIOPER = 1
        this.w_VALMAG = cp_ROUND(NVL(VALMAG,0), this.w_DECTOT)
      endif
      if ARTIPART="FO"
        this.w_OLDVALMAG = IIF(this.w_VALMAG=0,0,cp_ROUND(this.w_VALMAG,g_PERPVL))
        this.w_VALMAG = IIF(this.w_VALMAG=0,0,cp_ROUND(this.w_VALMAG*(VALMAG-IIF(ABS(nvl(MVIMPEVA,0))>ABS(VALMAG),VALMAG,nvl(MVIMPEVA,0)))/VALMAG,g_PERPVL))
      else
        this.w_OLDVALMAG = IIF(MVQTAMOV=0,0,cp_ROUND(NVL(this.w_VALMAG,0),g_PERPVL))
        this.w_VALMAG = IIF(MVQTAMOV=0,0,cp_ROUND(this.w_VALMAG*(MVQTAMOV-IIF(nvl(MVQTAEVA,0)>MVQTAMOV,MVQTAMOV,nvl(MVQTAEVA,0)))/MVQTAMOV,g_PERPVL))
      endif
      this.w_OLDTOTDOC = this.w_OLDTOTDOC + this.w_OLDVALMAG
      this.w_TOTDOC = this.w_TOTDOC + this.w_VALMAG
      * --- Calcolo aliquote castelletto IVA
      this.w_trov = .f.
      FOR i = 1 TO this.w_ALIVA
      if AI[i, 7] = this.w_CODIVA AND AI[i, 1] = MVFLOMAG AND this.w_ARASSIVA=AI[i, 10]
        * --- Aliquota gia' esistente
        AI[i, 3] = AI[i, 3] + this.w_VALMAG
        AI[i, 8] = AI[i, 8] + MVVALRIG + MVIMPSCO + MVIMPACC
        this.w_TROV = .t.
        exit
      endif
      ENDFOR
      if this.w_trov = .f.
        * --- Se aliquota inesistente
        this.w_ALIVA = this.w_ALIVA + 1
        * --- Aggiunge nuova aliquota
        AI[this.w_ALIVA, 7] = this.w_CODIVA
        AI[this.w_ALIVA, 3] = this.w_VALMAG
        AI[this.w_ALIVA, 5] = IIF(this.w_FLINTR="S", 0, this.w_PERIVA1)
        AI[this.w_ALIVA, 6] = NVL(PERIND,0)
        AI[this.w_ALIVA, 8] = MVVALRIG + MVIMPSCO + MVIMPACC
        AI[this.w_ALIVA, 1] = MVFLOMAG
        AI[this.w_ALIVA, 10] = this.w_ARASSIVA
      endif
      if this.w_CODVAL<>g_PERVAL
        this.w_VALMAG = VAL2MON(this.w_VALMAG,this.w_CAOVAL,GETCAM(g_PERVAL,this.w_DATDOC),this.w_DATDOC,g_PERVAL, g_PERPVL)
      endif
      INSERT INTO DettDocu (FLOMAG, CONTRO, CONOMA, CONOMI, ;
      CONIND, VALRIG, CODIVA, CODBUN, CATCLF, CATART,INICOM,FINCOM,SERIAL,CODCEN,VOCCEN,CODCOM,TIPO,CODCON,ARASSIVA) ;
      VALUES (this.w_FLOMAG, this.w_CONTRO, this.w_CONOMA, this.w_CONOMI, this.w_CONIND, this.w_VALMAG, this.w_CODIVA, this.w_CODBUN, ;
      this.w_CFCATCON, this.w_ARCATCON,this.w_INICOM,this.w_FINCOM,this.w_OSERIAL,this.w_MVCODCEN,this.w_MVVOCCEN,this.w_MVCODCOM,this.w_TIPO,this.w_CODTESTA,this.w_ARASSIVA)
      SELECT EXTFA003
      ENDSCAN
      * --- Testa l'Ultima Uscita
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Elimina il Cursore
      SELECT EXTFA003
      USE
      if USED("DettDocu")
        SELECT DettDocu
        USE
      endif
      if USED("AppCur")
        SELECT AppCur
        USE
      endif
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrive Temporaneo di Elaborazione
    this.w_CANCELLA = .F.
    if this.w_OSERIAL <> REPL("#", 10) AND RECCOUNT("DettDocu") > 0 AND this.w_TOTDOC<>0
      * --- Calcola totali
      this.Page_6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_XCODICE = this.w_CODCON
      this.w_TIPCON = "G"
      * --- Devo verificare se il conto che inserisco nel cursore � un costo oppure
      *     un ricavo per gestire l'analitica.
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANCONSUP"+;
          " from "+i_cTable+" CONTI where ";
              +"ANCODICE = "+cp_ToStrODBC(this.w_XCODICE);
              +" and ANTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANCONSUP;
          from (i_cTable) where;
              ANCODICE = this.w_XCODICE;
              and ANTIPCON = this.w_TIPCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CONSUP = NVL(cp_ToDate(_read_.ANCONSUP),cp_NullValue(_read_.ANCONSUP))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Leggo dai Mastri la sezione di bilancio
      * --- Read from MASTRI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MASTRI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MASTRI_idx,2],.t.,this.MASTRI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MCSEZBIL"+;
          " from "+i_cTable+" MASTRI where ";
              +"MCCODICE = "+cp_ToStrODBC(this.w_CONSUP);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MCSEZBIL;
          from (i_cTable) where;
              MCCODICE = this.w_CONSUP;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_SEZB = NVL(cp_ToDate(_read_.MCSEZBIL),cp_NullValue(_read_.MCSEZBIL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_SEZB $ "CR" 
        this.w_XCODCEN = Nvl(this.w_Mvcodcen,space(15))
        this.w_XVOCCEN = Nvl(this.w_Mvvoccen,space(15))
        this.w_XCODCOM = Nvl(this.w_Mvcodcom,space(15))
      else
        this.w_XCODCEN = Space(15)
        this.w_XVOCCEN = Space(15)
        this.w_XCODCOM = Space(15)
      endif
      this.w_APPVAL = this.w_TOTDOC
      this.w_APPSEZ = this.w_SEZCLF
      this.w_XCODBUN = this.w_CODBUN1
      this.w_XINICOM = cp_CharToDate("  -  -  ")
      this.w_XFINCOM = cp_CharToDate("  -  -  ")
      this.w_FLVAL = "N"
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Scrive il Dettaglio: Conto IVA Detraibile
      if this.w_IVADET<>0 AND NOT EMPTY(this.w_CONIVA)
        this.w_XCODICE = this.w_CONIVA
        this.w_APPVAL = this.w_IVADET
        this.w_XCODCEN = Space(15)
        this.w_XVOCCEN = Space(15)
        this.w_XCODCOM = Space(15)
        this.w_APPSEZ = IIF(this.w_SEZCLF="D", "A", "D")
        this.w_XCODBUN = this.w_CODBUN1
        this.w_FLVAL = "N"
        this.w_XINICOM = cp_CharToDate("  -  -  ")
        this.w_XFINCOM = cp_CharToDate("  -  -  ")
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Scrive il Dettaglio: Conti IVA Indetraibile
      this.w_LASTIND = 0
      if this.w_IVAIND<>0
        SELECT CONIND, CODIVA, SUM(VALRIG), CATCLF, CATART, CONTRO, CODBUN, VOCCEN, INICOM, FINCOM, CODCEN,CODCOM FROM DettDocu ; 
 WHERE FLOMAG $ "XIE" AND NVL(VALRIG,0)<>0 INTO ARRAY AppArr GROUP BY CONIND, CONTRO, CODIVA, CODBUN, CODCEN, VOCCEN, INICOM, FINCOM, CODCOM
        this.w_APPO1 = this.w_IVAIND
        this.w_LOOP = 1
        do while this.w_LOOP<=Alen( AppArr, 1 )
          this.w_APPO2 = 0
          FOR L_y = 1 TO 6
          if AI[L_y, 7] = AppArr[ this.w_LOOP , 2]
            if this.w_CODVAL<>g_PERVAL
              * --- Converte in Moneta di Conto
              AppArr[this.w_LOOP, 3] = VAL2MON(AppArr[this.w_LOOP, 3],this.w_CAOVAL,GETCAM(g_PERVAL,this.w_DATDOC),this.w_DATDOC,g_PERVAL, g_PERPVL)
            endif
            if NOT(AppArr[this.w_LOOP, 3]=0 OR AI[L_y, 5]=0 OR AI[L_y, 6]=0)
              this.w_APPO2 = cp_ROUND(((AppArr[this.w_LOOP, 3] * AI[L_y, 5]) / 100) + IIF(g_PERPVL=0, .499, .00499), g_PERPVL)
              this.w_APPO2 = cp_ROUND((this.w_APPO2 * AI[L_y, 6]) / 100, g_PERPVL)
              EXIT
            endif
          endif
          ENDFOR
          this.w_LASTIND = IIF ( this.w_APPO2<>0 , this.w_LOOP ,this.w_LASTIND )
          * --- Scrive Importo Indetraibile sul Vettore
          AppArr[this.w_LOOP, 3] = this.w_APPO2
          * --- Storna dal Totale IVA Indetraibile
          this.w_APPO1 = this.w_APPO1 - this.w_APPO2
          this.w_LOOP = this.w_LOOP + 1
        enddo
        if ALEN(AppArr, 1)>0 and this.w_LASTIND>0
          * --- Mette l'eventuale Resto sulla ultima riga con codice iva indetraibile
          AppArr[ this.w_LASTIND , 3] = AppArr[ this.w_LASTIND , 3] + this.w_APPO1
        endif
        * --- Alla Fine Scrive le Righe P.N.
        this.w_APPVAL = 0
        FOR L_i = 1 TO ALEN(AppArr, 1)
        this.w_XCODICE = AppArr[L_i, 1]
        * --- Somma gli Importi delle Aliquote IVA e Mette l'eventuale Resto sulla Prima Riga
        this.w_APPVAL = this.w_APPVAL + AppArr[L_i, 3] 
        this.w_APPSEZ = IIF(this.w_SEZCLF="D", "A", "D")
        this.w_XCODBUN = AppArr[L_i, 7]
        this.w_FLVAL = "N"
        this.w_TIPCON = "G"
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANCONSUP"+;
            " from "+i_cTable+" CONTI where ";
                +"ANCODICE = "+cp_ToStrODBC(this.w_XCODICE);
                +" and ANTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANCONSUP;
            from (i_cTable) where;
                ANCODICE = this.w_XCODICE;
                and ANTIPCON = this.w_TIPCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CONSUP = NVL(cp_ToDate(_read_.ANCONSUP),cp_NullValue(_read_.ANCONSUP))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Leggo dai Mastri la sezione di bilancio
        * --- Read from MASTRI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.MASTRI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MASTRI_idx,2],.t.,this.MASTRI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MCSEZBIL"+;
            " from "+i_cTable+" MASTRI where ";
                +"MCCODICE = "+cp_ToStrODBC(this.w_CONSUP);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MCSEZBIL;
            from (i_cTable) where;
                MCCODICE = this.w_CONSUP;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SEZB = NVL(cp_ToDate(_read_.MCSEZBIL),cp_NullValue(_read_.MCSEZBIL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_SEZB $ "CR" 
          this.w_XCODCEN = iif(not empty(AppArr[ L_i , 11]),AppArr[ L_i , 11], Nvl(this.w_Mvtcocen,space(15)))
          this.w_XVOCCEN = iif(empty(Nvl(this.w_Vocind,space(15))), AppArr[ L_i , 8], Nvl(this.w_Vocind,space(15)))
          this.w_XCODCOM = iif(not empty(AppArr[ L_i , 12]),AppArr[ L_i , 12], Nvl(this.w_Mvtcomme,space(15)))
        else
          this.w_XCODCEN = Space(15)
          this.w_XVOCCEN = Space(15)
          this.w_XCODCOM = Space(15)
        endif
        this.w_XINICOM = NVL(AppArr[ L_i , 9], cp_CharToDate("  -  -  "))
        this.w_XFINCOM = NVL(AppArr[ L_i , 10], cp_CharToDate("  -  -  "))
        if L_i = ALEN(AppArr, 1)
          * --- Se siamo in Fondo Scrive
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          if AppArr[L_i+1, 1] <> this.w_XCODICE Or AppArr[L_i+1, 7] <> this.w_XCODBUN or AppArr[L_i+1, 11] <> this.w_XCODCEN ; 
 or AppArr[L_i+1, 8] <> this.w_XVOCCEN or AppArr[L_i+1, 12] <> this.w_XCODCOM or AppArr[L_i+1, 9] <> this.w_XINICOM or AppArr[L_i+1, 10] <> this.w_XFINCOM
            * --- ...Oppure Scrive se il Successivo Conto e' diverso
            this.Page_3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_APPVAL = 0
          endif
        endif
        ENDFOR
      endif
      * --- Scrive il Dettaglio: Riga Contropartite
      * --- Faccio il contollo sulle date di competenza a questo livello perch� dovevo leggere tutti i documenti per i totali
      SELECT CONTRO, CODBUN, SUM(VALRIG) AS VALAPP, CATCLF, CATART, INICOM, ; 
 FINCOM, SERIAL,CODCEN,VOCCEN,CODCOM FROM DettDocu ; 
 WHERE FLOMAG $ "XIE" ; 
 INTO CURSOR AppCur GROUP BY CONTRO, CODBUN, INICOM, FINCOM, SERIAL,CODCEN,VOCCEN,CODCOM
      SELECT AppCur
      GO TOP
      SCAN FOR NVL(VALAPP,0)<>0 OR NOT EMPTY(NVL(CONTRO," "))
      this.w_SCOMER = 0
      if NOT EMPTY(CONTRO)
        this.w_XCODICE = CONTRO
        this.w_APPVAL = VALAPP
        this.w_APPSEZ = IIF(this.w_SEZCLF="D", "A", "D")
        this.w_XCODBUN = CODBUN
        this.w_FLVAL = "S"
        this.w_XINICOM = INICOM
        this.w_XFINCOM = FINCOM
        this.w_TIPCON = "G"
        * --- Devo verificare se il conto che inserisco nel cursore � un costo oppure
        *     un ricavo per gestire l'analitica.
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANCONSUP"+;
            " from "+i_cTable+" CONTI where ";
                +"ANCODICE = "+cp_ToStrODBC(this.w_XCODICE);
                +" and ANTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANCONSUP;
            from (i_cTable) where;
                ANCODICE = this.w_XCODICE;
                and ANTIPCON = this.w_TIPCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CONSUP = NVL(cp_ToDate(_read_.ANCONSUP),cp_NullValue(_read_.ANCONSUP))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Leggo dai Mastri la sezione di bilancio
        * --- Read from MASTRI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.MASTRI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MASTRI_idx,2],.t.,this.MASTRI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MCSEZBIL"+;
            " from "+i_cTable+" MASTRI where ";
                +"MCCODICE = "+cp_ToStrODBC(this.w_CONSUP);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MCSEZBIL;
            from (i_cTable) where;
                MCCODICE = this.w_CONSUP;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SEZB = NVL(cp_ToDate(_read_.MCSEZBIL),cp_NullValue(_read_.MCSEZBIL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_SEZB $ "CR" 
          this.w_XCODCEN = Nvl(Codcen,space(15))
          this.w_XVOCCEN = Nvl(Voccen,space(15))
          this.w_XCODCOM = Nvl(Codcom,space(15))
        else
          this.w_XCODCEN = Space(15)
          this.w_XVOCCEN = Space(15)
          this.w_XCODCOM = Space(15)
        endif
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        SELECT AppCur
      else
        this.w_XCODICE = CONTRO
        this.w_CFCATCON = NVL(CATCLF, SPACE(3))
        this.w_ARCATCON = NVL(CATART, SPACE(3))
        INSERT INTO CursError(REGOLA,CODICE,CPROWNUM,DESCRI,TIPCON)VALUES(ah_MsgFormat("Dati fatture"),"",this.oParentObject.w_CPROWNUM,this.w_DESCRI,"")
        INSERT INTO CursError(REGOLA,CODICE,CPROWNUM,DESCRI,TIPCON)VALUES(ah_MsgFormat("Dati fatture"),"",this.oParentObject.w_CPROWNUM,ah_MsgFormat("%1Cat.cli/for: %2 cat. art.: %3 - contropartita non definita",SPACE(10),this.w_CFCATCON,this.w_ARCATCON),"")
        this.oParentObject.w_ERROR = .T.
        this.w_CANCELLA = .T.
        SELECT AppCur
      endif
      ENDSCAN
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.w_CANCELLA
      DELETE FROM EXTCG001 WHERE SERIAL=this.w_OSERIAL
      this.w_CANCELLA = .F.
      * --- Incremento numero scritture elaborate perch� in errore non verr� considerata nel calcolo
      this.oParentObject.w_CONTA1 = this.oParentObject.w_CONTA1 + 1
    endif
    * --- Azzera il Temporaneo di Appoggio
    CREATE CURSOR DettDocu ; 
 (FLOMAG C(1), CONTRO C(15), CONOMA C(15), CONOMI C(15), CONIND C(15), ; 
 VALRIG N(18,4), CODIVA C(5), CODBUN C(3), CATCLF C(3), CATART C(3), INICOM D(8), FINCOM D(8), SERIAL C(10),; 
 CODCEN C(15),VOCCEN C(15),CODCOM C(15),TIPO C(1),CODCON C(15),ARASSIVA C(1))
    this.w_TOTVAL = 0
    this.w_TOTDOC = 0
    this.w_TOTDAR = 0
    this.w_TOTAVE = 0
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna Temporaneo di Elaborazione Primanota
    if this.w_FLVAL="S" AND this.w_CODVAL<>g_PERVAL AND .F.
      * --- Se arriva un Importo in Valuta e il Documento e' in Valuta Converte in Moneta di Conto
      this.w_APPVAL = VAL2MON(this.w_APPVAL,this.w_CAOVAL,GETCAM(g_PERVAL,this.w_DATDOC),this.w_DATDOC,g_PERVAL, g_PERPVL)
    endif
    this.w_APPSEZ = IIF(this.w_APPVAL<0, IIF(this.w_APPSEZ="D", "A", "D"), this.w_APPSEZ)
    * --- Verifico se nella causale documento ho selezionato Fatture da Emettere/Ricevere
    *     oppure Storno Fatture da Emettere/Ricevere.
    if this.w_TDCONASS="S"
      this.w_APPSEZ = IIF(this.w_APPSEZ="D", "A", "D")
    endif
    if this.w_APPSEZ="D"
      this.w_IMPDAR = ABS(this.w_APPVAL)
      this.w_IMPAVE = 0
    else
      this.w_IMPDAR = 0
      this.w_IMPAVE = ABS(this.w_APPVAL)
    endif
    this.w_TOTDAR = this.w_TOTDAR + this.w_IMPDAR
    this.w_TOTAVE = this.w_TOTAVE + this.w_IMPAVE
    INSERT INTO EXTCG001 (TIPCON, CODICE, CODBUN, INICOM, FINCOM, ; 
 VALNAZ, DATREG, IMPDAR, IMPAVE,SERIAL,DESCRI,CODCEN,VOCCEN,CODCOM,TIPO,CODCON) ; 
 VALUES ("G", this.w_XCODICE, this.w_XCODBUN, this.w_XINICOM, this.w_XFINCOM, ; 
 g_PERVAL, this.w_DATDOC, this.w_IMPDAR, this.w_IMPAVE,this.w_OSERIAL,this.w_DESCRI,; 
 this.w_XCODCEN,this.w_XVOCCEN,this.w_XCODCOM,this.w_TIPO,this.w_CODTESTA)
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se non ci Sono Importi da Registrare Verifica se si Tratta di Documento con Importo a 0 (Solo Sconto Merce)
    if this.w_TOTDOC=0 AND this.w_IVAGLO=0 AND this.w_SCOMER=1
      * --- Scrive il Dettaglio Sconto Merce: Riga Contropartite
      SELECT CONTRO, CODBUN, SUM(VALRIG) AS VALAPP, CATCLF, CATART, INICOM, FINCOM, SERIAL FROM DettDocu ; 
 WHERE ((INICOM <= this.oParentObject.oParentObject.w_ASDATFIN AND FINCOM >= this.oParentObject.oParentObject.w_ASDATINI) ; 
 OR EMPTY(INICOM)) AND FLOMAG $ "S" ; 
 INTO CURSOR AppCur GROUP BY CONTRO, CODBUN, INICOM, FINCOM, SERIAL
      SELECT AppCur
      GO TOP
      SCAN FOR NVL(VALAPP,0)<>0 
      this.w_SCOMER = 10
      this.w_XCODICE = CONTRO
      if NOT EMPTY(this.w_XCODICE)
        this.w_APPVAL = 0
        this.w_APPSEZ = IIF(this.w_SEZCLF="D", "A", "D")
        this.w_XCODBUN = CODBUN
        this.w_FLVAL = "N"
        this.w_XINICOM = INICOM
        this.w_XFINCOM = FINCOM
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        this.w_CFCATCON = NVL(CATCLF, SPACE(3))
        this.w_ARCATCON = NVL(CATART, SPACE(3))
        INSERT INTO CursError(REGOLA,CODICE,CPROWNUM,DESCRI,TIPCON)VALUES(ah_MsgFormat("Dati fatture"),"",this.oParentObject.w_CPROWNUM,this.w_DESCRI,"")
        INSERT INTO CursError(REGOLA,CODICE,CPROWNUM,DESCRI,TIPCON)VALUES(ah_MsgFormat("Dati fatture"),"",this.oParentObject.w_CPROWNUM,ah_MsgFormat("%1Cat.cli/for: %2 cat.art.: %3 - contropartita sconto merce non definita",SPACE(10),this.w_CFCATCON,this.w_ARCATCON),"")
        this.oParentObject.w_ERROR = .T.
        this.w_CANCELLA = .T.
      endif
      SELECT AppCur
      ENDSCAN
      * --- Scrive Riga IVA Sc.Merce
      if this.w_SCOMER=10
        this.w_XCODICE = this.w_CONIVA
        this.w_APPVAL = 0
        this.w_APPSEZ = IIF(this.w_SEZCLF="D", "A", "D")
        this.w_XCODBUN = this.w_CODBUN1
        this.w_XCODCEN = Space(15)
        this.w_XVOCCEN = Space(15)
        this.w_XCODCOM = Space(15)
        this.w_FLVAL = "N"
        this.w_XINICOM = cp_CharToDate("  -  -  ")
        this.w_XFINCOM = cp_CharToDate("  -  -  ")
        if NOT EMPTY(this.w_XCODICE)
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          INSERT INTO CursError(REGOLA,CODICE,CPROWNUM,DESCRI,TIPCON)VALUES(ah_MsgFormat("Dati fatture"),"",0,ah_MsgFormat("Contropartita sconto merce non definita"),"")
          this.oParentObject.w_ERROR = .T.
          this.w_CANCELLA = .T.
        endif
      endif
    endif
    * --- Scrive le Spese
    * --- Scrive il Dettaglio: Spese Incasso
    if this.w_SPEINC<>0 AND this.w_FLRINC<>"S" AND this.w_COMP
      this.w_XCODICE = IIF(this.w_FLVEAC="V",this.w_CONINC,this.w_ACQINC)
      this.w_APPVAL = this.w_SPEINC
      this.w_APPSEZ = IIF(this.w_SEZCLF="D", "A", "D")
      this.w_XCODBUN = this.w_CODBUN1
      this.w_FLVAL = "S"
      this.w_TIPCON = "G"
      * --- Devo verificare se il conto che inserisco nel cursore � un costo oppure
      *     un ricavo per gestire l'analitica.
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANCONSUP"+;
          " from "+i_cTable+" CONTI where ";
              +"ANCODICE = "+cp_ToStrODBC(this.w_XCODICE);
              +" and ANTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANCONSUP;
          from (i_cTable) where;
              ANCODICE = this.w_XCODICE;
              and ANTIPCON = this.w_TIPCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CONSUP = NVL(cp_ToDate(_read_.ANCONSUP),cp_NullValue(_read_.ANCONSUP))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Leggo dai Mastri la sezione di bilancio
      * --- Read from MASTRI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MASTRI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MASTRI_idx,2],.t.,this.MASTRI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MCSEZBIL"+;
          " from "+i_cTable+" MASTRI where ";
              +"MCCODICE = "+cp_ToStrODBC(this.w_CONSUP);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MCSEZBIL;
          from (i_cTable) where;
              MCCODICE = this.w_CONSUP;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_SEZB = NVL(cp_ToDate(_read_.MCSEZBIL),cp_NullValue(_read_.MCSEZBIL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_SEZB $ "CR" 
        this.w_XCODCEN = Nvl(this.w_Mvtcocen,space(15))
        this.w_XVOCCEN = Nvl(this.w_Vocinc,space(15))
        this.w_XCODCOM = Nvl(this.w_Mvtcomme,space(15))
      else
        this.w_XCODCEN = Space(15)
        this.w_XVOCCEN = Space(15)
        this.w_XCODCOM = Space(15)
      endif
      this.w_XINICOM = this.w_TESINICOM
      this.w_XFINCOM = this.w_TESFINCOM
      if NOT EMPTY(this.w_XCODICE)
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        INSERT INTO CursError(REGOLA,CODICE,CPROWNUM,DESCRI,TIPCON)VALUES(ah_MsgFormat("Dati fatture"),"",0,iif(this.w_FLVEAC="A",ah_MsgFormat("Contropartita spese incasso acquisti non definita"),ah_MsgFormat("Contropartita spese incasso vendite non definita")),"")
        this.oParentObject.w_ERROR = .T.
        this.w_CANCELLA = .T.
      endif
    endif
    * --- Scrive il Dettaglio: Spese Imballo
    if this.w_SPEIMB<>0 AND this.w_FLRIMB<>"S" AND this.w_COMP
      this.w_XCODICE = IIF(this.w_FLVEAC="V",this.w_CONIMB,this.w_ACQIMB)
      this.w_APPVAL = this.w_SPEIMB
      this.w_APPSEZ = IIF(this.w_SEZCLF="D", "A", "D")
      this.w_XCODBUN = this.w_CODBUN1
      this.w_FLVAL = "S"
      this.w_TIPCON = "G"
      * --- Devo verificare se il conto che inserisco nel cursore � un costo oppure
      *     un ricavo per gestire l'analitica.
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANCONSUP"+;
          " from "+i_cTable+" CONTI where ";
              +"ANCODICE = "+cp_ToStrODBC(this.w_XCODICE);
              +" and ANTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANCONSUP;
          from (i_cTable) where;
              ANCODICE = this.w_XCODICE;
              and ANTIPCON = this.w_TIPCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CONSUP = NVL(cp_ToDate(_read_.ANCONSUP),cp_NullValue(_read_.ANCONSUP))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Leggo dai Mastri la sezione di bilancio
      * --- Read from MASTRI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MASTRI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MASTRI_idx,2],.t.,this.MASTRI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MCSEZBIL"+;
          " from "+i_cTable+" MASTRI where ";
              +"MCCODICE = "+cp_ToStrODBC(this.w_CONSUP);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MCSEZBIL;
          from (i_cTable) where;
              MCCODICE = this.w_CONSUP;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_SEZB = NVL(cp_ToDate(_read_.MCSEZBIL),cp_NullValue(_read_.MCSEZBIL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_SEZB $ "CR" 
        this.w_XCODCEN = Nvl(this.w_Mvtcocen,space(15))
        this.w_XVOCCEN = Nvl(this.w_Vocimb,space(15))
        this.w_XCODCOM = Nvl(this.w_Mvtcomme,space(15))
      else
        this.w_XCODCEN = Space(15)
        this.w_XVOCCEN = Space(15)
        this.w_XCODCOM = Space(15)
      endif
      this.w_XINICOM = this.w_TESINICOM
      this.w_XFINCOM = this.w_TESFINCOM
      if NOT EMPTY(this.w_XCODICE)
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        if this.w_FLVEAC="A"
          INSERT INTO CursError(REGOLA,CODICE,CPROWNUM,DESCRI,TIPCON)VALUES(ah_MsgFormat("Dati fatture"),"",0,Ah_sgFormat("Contropartita spese imballo acquisti non definita!"),"")
        else
          INSERT INTO CursError(REGOLA,CODICE,CPROWNUM,DESCRI,TIPCON)VALUES(ah_MsgFormat("Dati fatture"),"",0,ah_MsgFormat("Contropartita spese imballo vendite non definita"),"")
        endif
        this.oParentObject.w_ERROR = .T.
        this.w_CANCELLA = .T.
      endif
    endif
    * --- Scrive il Dettaglio: Spese Trasporto
    if this.w_SPETRA<>0 AND this.w_FLRTRA<>"S" AND this.w_COMP
      this.w_XCODICE = IIF(this.w_FLVEAC="V",this.w_CONTRA,this.w_ACQTRA)
      this.w_APPVAL = this.w_SPETRA
      this.w_APPSEZ = IIF(this.w_SEZCLF="D", "A", "D")
      this.w_XCODBUN = this.w_CODBUN1
      this.w_FLVAL = "S"
      this.w_TIPCON = "G"
      * --- Devo verificare se il conto che inserisco nel cursore � un costo oppure
      *     un ricavo per gestire l'analitica.
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANCONSUP"+;
          " from "+i_cTable+" CONTI where ";
              +"ANCODICE = "+cp_ToStrODBC(this.w_XCODICE);
              +" and ANTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANCONSUP;
          from (i_cTable) where;
              ANCODICE = this.w_XCODICE;
              and ANTIPCON = this.w_TIPCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CONSUP = NVL(cp_ToDate(_read_.ANCONSUP),cp_NullValue(_read_.ANCONSUP))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Leggo dai Mastri la sezione di bilancio
      * --- Read from MASTRI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MASTRI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MASTRI_idx,2],.t.,this.MASTRI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MCSEZBIL"+;
          " from "+i_cTable+" MASTRI where ";
              +"MCCODICE = "+cp_ToStrODBC(this.w_CONSUP);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MCSEZBIL;
          from (i_cTable) where;
              MCCODICE = this.w_CONSUP;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_SEZB = NVL(cp_ToDate(_read_.MCSEZBIL),cp_NullValue(_read_.MCSEZBIL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_SEZB $ "CR" 
        this.w_XCODCEN = Nvl(this.w_Mvtcocen,space(15))
        this.w_XVOCCEN = Nvl(this.w_Voctra,space(15))
        this.w_XCODCOM = Nvl(this.w_Mvtcomme,space(15))
      else
        this.w_XCODCEN = Space(15)
        this.w_XVOCCEN = Space(15)
        this.w_XCODCOM = Space(15)
      endif
      this.w_XINICOM = this.w_TESINICOM
      this.w_XFINCOM = this.w_TESFINCOM
      if NOT EMPTY(this.w_XCODICE)
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        if this.w_FLVEAC="A"
          INSERT INTO CursError(REGOLA,CODICE,CPROWNUM,DESCRI,TIPCON)VALUES(ah_MsgFormat("Dati fatture"),"",0,ah_MsgFormat("Contropartita spese trasporto acquisti non definita"),"")
        else
          INSERT INTO CursError(REGOLA,CODICE,CPROWNUM,DESCRI,TIPCON)VALUES(ah_MsgFormat("Dati fatture"),"",0,ah_MsgFormat("Contropartita spese trasporto vendite non definita"),"")
        endif
        this.oParentObject.w_ERROR = .T.
        this.w_CANCELLA = .T.
      endif
    endif
    * --- Scrive il Dettaglio: Spese Bolli
    if this.w_SPEBOL<>0 AND this.w_COMP
      this.w_XCODICE = IIF(this.w_FLVEAC="V",this.w_CONBOL,this.w_ACQBOL)
      this.w_APPVAL = this.w_SPEBOL
      this.w_APPSEZ = IIF(this.w_SEZCLF="D", "A", "D")
      this.w_XCODBUN = this.w_CODBUN1
      this.w_FLVAL = "S"
      this.w_TIPCON = "G"
      * --- Devo verificare se il conto che inserisco nel cursore � un costo oppure
      *     un ricavo per gestire l'analitica.
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANCONSUP"+;
          " from "+i_cTable+" CONTI where ";
              +"ANCODICE = "+cp_ToStrODBC(this.w_XCODICE);
              +" and ANTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANCONSUP;
          from (i_cTable) where;
              ANCODICE = this.w_XCODICE;
              and ANTIPCON = this.w_TIPCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CONSUP = NVL(cp_ToDate(_read_.ANCONSUP),cp_NullValue(_read_.ANCONSUP))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Leggo dai Mastri la sezione di bilancio
      * --- Read from MASTRI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MASTRI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MASTRI_idx,2],.t.,this.MASTRI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MCSEZBIL"+;
          " from "+i_cTable+" MASTRI where ";
              +"MCCODICE = "+cp_ToStrODBC(this.w_CONSUP);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MCSEZBIL;
          from (i_cTable) where;
              MCCODICE = this.w_CONSUP;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_SEZB = NVL(cp_ToDate(_read_.MCSEZBIL),cp_NullValue(_read_.MCSEZBIL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_SEZB $ "CR" 
        this.w_XCODCEN = Nvl(this.w_Mvtcocen,space(15))
        this.w_XVOCCEN = Nvl(this.w_Vocbol,space(15))
        this.w_XCODCOM = Nvl(this.w_Mvtcomme,space(15))
      else
        this.w_XCODCEN = Space(15)
        this.w_XVOCCEN = Space(15)
        this.w_XCODCOM = Space(15)
      endif
      this.w_XINICOM = this.w_TESINICOM
      this.w_XFINCOM = this.w_TESFINCOM
      if NOT EMPTY(this.w_XCODICE)
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        if this.w_FLVEAC="A"
          INSERT INTO CursError(REGOLA,CODICE,CPROWNUM,DESCRI,TIPCON)VALUES(ah_MsgFormat("Dati fatture"),"",0,; 
 ah_MsgFormat("Contropartita spese bollo acquisti non definita"),"")
        else
          INSERT INTO CursError(REGOLA,CODICE,CPROWNUM,DESCRI,TIPCON)VALUES(ah_MsgFormat("Dati fatture"),"",0,ah_MsgFormat("Contropartita spese bollo vendite non definita"),"")
        endif
        this.oParentObject.w_ERROR = .T.
        this.w_CANCELLA = .T.
      endif
    endif
    * --- Scrive il Dettaglio: Omaggi di Imponibile
    SELECT CONOMA, CODBUN, SUM(VALRIG) AS VALAPP, CATCLF, CATART, INICOM,; 
 FINCOM, SERIAL,CODCEN,VOCCEN,CODCOM FROM DettDocu ; 
 WHERE FLOMAG $ "IE" INTO CURSOR AppCur GROUP BY CONOMA, CODBUN, ; 
 INICOM, FINCOM, SERIAL,CODCEN,VOCCEN,CODCOM
    SELECT AppCur
    GO TOP
    SCAN FOR VALAPP<>0 
    this.w_XCODICE = CONOMA
    if NOT EMPTY(this.w_XCODICE)
      this.w_APPVAL = VALAPP
      this.w_APPSEZ = this.w_SEZCLF
      this.w_XCODBUN = CODBUN
      this.w_FLVAL = "S"
      this.w_TIPCON = "G"
      * --- Devo verificare se il conto che inserisco nel cursore � un costo oppure
      *     un ricavo per gestire l'analitica.
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANCONSUP"+;
          " from "+i_cTable+" CONTI where ";
              +"ANCODICE = "+cp_ToStrODBC(this.w_XCODICE);
              +" and ANTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANCONSUP;
          from (i_cTable) where;
              ANCODICE = this.w_XCODICE;
              and ANTIPCON = this.w_TIPCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CONSUP = NVL(cp_ToDate(_read_.ANCONSUP),cp_NullValue(_read_.ANCONSUP))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Leggo dai Mastri la sezione di bilancio
      * --- Read from MASTRI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MASTRI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MASTRI_idx,2],.t.,this.MASTRI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MCSEZBIL"+;
          " from "+i_cTable+" MASTRI where ";
              +"MCCODICE = "+cp_ToStrODBC(this.w_CONSUP);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MCSEZBIL;
          from (i_cTable) where;
              MCCODICE = this.w_CONSUP;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_SEZB = NVL(cp_ToDate(_read_.MCSEZBIL),cp_NullValue(_read_.MCSEZBIL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_SEZB $ "CR" 
        this.w_XCODCEN = Nvl(Codcen,space(15))
        this.w_XVOCCEN = Nvl(Voccen,space(15))
        this.w_XCODCOM = Nvl(Codcom,space(15))
      else
        this.w_XCODCEN = Space(15)
        this.w_XVOCCEN = Space(15)
        this.w_XCODCOM = Space(15)
      endif
      this.w_XINICOM = INICOM
      this.w_XFINCOM = FINCOM
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      this.w_CFCATCON = NVL(CATCLF, SPACE(3))
      this.w_ARCATCON = NVL(CATART, SPACE(3))
      INSERT INTO CursError(REGOLA,CODICE,CPROWNUM,DESCRI,TIPCON)VALUES(ah_MsgFormat("Dati fatture"),"",this.oParentObject.w_CPROWNUM,this.w_DESCRI,"")
      INSERT INTO CursError(REGOLA,CODICE,CPROWNUM,DESCRI,TIPCON)VALUES(Ah_MsgFormat("Dati fatture"),"",this.oParentObject.w_CPROWNUM,ah_MsgFormat("%1Cat.cli/for: %2 cat.art.: %3 - contropartita omaggi non definita",SPACE(10),this.w_CFCATCON,this.w_ARCATCON),"")
      this.oParentObject.w_ERROR = .T.
      this.w_CANCELLA = .T.
    endif
    SELECT AppCur
    ENDSCAN
    * --- Scrive il Dettaglio: Conti IVA su Omaggi
    if this.w_IVAOMA<>0
      SELECT CONOMI, CODIVA, SUM(VALRIG) AS VALAPP, CATCLF, CATART, INICOM, FINCOM, SERIAL,SUM(VALRIG) FROM DettDocu ; 
 WHERE FLOMAG = "E" AND NVL(ARASSIVA," ")<>"S"; 
 INTO ARRAY Apparr GROUP BY CONOMI, CODIVA, INICOM, FINCOM, SERIAL
      this.w_APPO1 = this.w_IVAOMA
      FOR L_i = 1 TO ALEN(AppArr, 1)
      if this.w_CODVAL<>g_PERVAL
        * --- Converte in Moneta di Conto
        AppArr[L_i, 3] = VAL2MON(AppArr[L_i, 3],this.w_CAOVAL,GETCAM(g_PERVAL,this.w_DATDOC),this.w_DATDOC,g_PERVAL, g_PERPVL)
      endif
      this.w_APPO2 = 0
      FOR L_y = 1 TO 6
      if AI[L_y, 7] = AppArr[L_i, 2] AND AI[L_y, 10]=" "
        if AppArr[L_i, 3]=0 OR AI[L_y, 5]=0
          EXIT
        else
          this.w_APPO2 = cp_ROUND(((AppArr[L_i, 3] * AI[L_y, 5]) / 100) + IIF(g_PERPVL=0, .499, .00499), g_PERPVL)
          EXIT
        endif
      endif
      ENDFOR
      * --- Scrive Imposta sul Vettore
      AppArr[L_i, 3] = this.w_APPO2
      * --- Storna dal Totale IVA Omaggi
      this.w_APPO1 = this.w_APPO1 - this.w_APPO2
      ENDFOR
      * --- Alla Fine Scrive le Righe P.N.
      this.w_APPVAL = 0
      FOR L_i = 1 TO ALEN(AppArr, 1)
      this.w_XCODICE = AppArr[L_i, 1]
      this.w_XVALMAG = AppArr[L_i, 9]
      * --- Somma gli Importi delle Aliquote IVA e Mette l'eventuale Resto sulla Prima Riga
      if EMPTY(this.w_XCODICE) AND this.w_XVALMAG<>0
        SELECT AppCur
        this.w_CFCATCON = NVL(AppArr[L_i, 4],Space(3))
        this.w_ARCATCON = NVL(AppArr[L_i, 5],Space(3))
        INSERT INTO CursError(REGOLA,CODICE,CPROWNUM,DESCRI,TIPCON)VALUES(ah_MsgFormat("Dati fatture"),"",this.oParentObject.w_CPROWNUM,this.w_DESCRI,"")
        INSERT INTO CursError(REGOLA,CODICE,CPROWNUM,DESCRI,TIPCON)VALUES(Ah_MsgFormat("Dati fatture"),"",this.oParentObject.w_CPROWNUM,ah_MsgFormat("%1Cat.cli/for: %2 cat.art.: %3 - conto IVA su omaggi non definito",SPACE(10),this.w_CFCATCON,this.w_ARCATCON),"")
        this.oParentObject.w_ERROR = .T.
        this.w_CANCELLA = .T.
      endif
      this.w_APPVAL = this.w_APPVAL + AppArr[L_i, 3] + IIF(L_i = 1, this.w_APPO1, 0)
      this.w_APPSEZ = this.w_SEZCLF
      this.w_XCODBUN = this.w_CODBUN1
      this.w_FLVAL = "N"
      this.w_XINICOM = cp_CharToDate("  -  -  ")
      this.w_XFINCOM = cp_CharToDate("  -  -  ")
      if L_i = ALEN(AppArr, 1)
        * --- Se siamo in Fondo Scrive
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        if AppArr[L_i+1, 1] <> this.w_XCODICE
          * --- ...Oppure Scrive se il Successivo Conto e' diverso
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_APPVAL = 0
        endif
      endif
      ENDFOR
    endif
    * --- Scrive il Dettaglio: Arrotondamenti
    if this.w_ARROTA<>0 AND this.w_COMP
      this.w_XCODICE = this.w_CONARR
      this.w_APPVAL = this.w_ARROTA
      this.w_APPSEZ = IIF(this.w_SEZCLF="D", "A", "D")
      this.w_XCODBUN = this.w_CODBUN1
      this.w_FLVAL = "S"
      this.w_TIPCON = "G"
      * --- Devo verificare se il conto che inserisco nel cursore � un costo oppure
      *     un ricavo per gestire l'analitica.
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANCONSUP"+;
          " from "+i_cTable+" CONTI where ";
              +"ANCODICE = "+cp_ToStrODBC(this.w_XCODICE);
              +" and ANTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANCONSUP;
          from (i_cTable) where;
              ANCODICE = this.w_XCODICE;
              and ANTIPCON = this.w_TIPCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CONSUP = NVL(cp_ToDate(_read_.ANCONSUP),cp_NullValue(_read_.ANCONSUP))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Leggo dai Mastri la sezione di bilancio
      * --- Read from MASTRI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MASTRI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MASTRI_idx,2],.t.,this.MASTRI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MCSEZBIL"+;
          " from "+i_cTable+" MASTRI where ";
              +"MCCODICE = "+cp_ToStrODBC(this.w_CONSUP);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MCSEZBIL;
          from (i_cTable) where;
              MCCODICE = this.w_CONSUP;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_SEZB = NVL(cp_ToDate(_read_.MCSEZBIL),cp_NullValue(_read_.MCSEZBIL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_SEZB $ "CR" 
        this.w_XCODCEN = Nvl(this.w_Mvtcocen,space(15))
        this.w_XVOCCEN = Nvl(this.w_Vocarr,space(15))
        this.w_XCODCOM = Nvl(this.w_Mvtcomme,space(15))
      else
        this.w_XCODCEN = Space(15)
        this.w_XVOCCEN = Space(15)
        this.w_XCODCOM = Space(15)
      endif
      this.w_XINICOM = this.w_TESINICOM
      this.w_XFINCOM = this.w_TESFINCOM
      if NOT EMPTY(this.w_XCODICE)
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        INSERT INTO CursError(REGOLA,CODICE,CPROWNUM,DESCRI,TIPCON) VALUES (ah_MsgFormat("Dati fatture"),"",0,ah_MsgFormat("Contropartita arrotondamenti non definita"),"")
        this.oParentObject.w_ERROR = .T.
        this.w_CANCELLA = .T.
      endif
    endif
    * --- Scrive il Dettaglio: Eventuali Differenze di Conversione (Solo alla Fine)
    if this.w_TOTDAR<>this.w_TOTAVE AND this.w_CODVAL<>g_PERVAL AND this.w_COMP
      this.w_XCODICE = this.w_CONDIC
      this.w_APPSEZ = IIF(this.w_TOTDAR-this.w_TOTAVE>0, "A", "D")
      this.w_APPVAL = ABS(this.w_TOTDAR-this.w_TOTAVE)
      this.w_XCODBUN = this.w_CODBUN1
      this.w_TIPCON = "G"
      * --- Devo verificare se il conto che inserisco nel cursore � un costo oppure
      *     un ricavo per gestire l'analitica.
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANCONSUP"+;
          " from "+i_cTable+" CONTI where ";
              +"ANCODICE = "+cp_ToStrODBC(this.w_XCODICE);
              +" and ANTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANCONSUP;
          from (i_cTable) where;
              ANCODICE = this.w_XCODICE;
              and ANTIPCON = this.w_TIPCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CONSUP = NVL(cp_ToDate(_read_.ANCONSUP),cp_NullValue(_read_.ANCONSUP))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Leggo dai Mastri la sezione di bilancio
      * --- Read from MASTRI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MASTRI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MASTRI_idx,2],.t.,this.MASTRI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MCSEZBIL"+;
          " from "+i_cTable+" MASTRI where ";
              +"MCCODICE = "+cp_ToStrODBC(this.w_CONSUP);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MCSEZBIL;
          from (i_cTable) where;
              MCCODICE = this.w_CONSUP;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_SEZB = NVL(cp_ToDate(_read_.MCSEZBIL),cp_NullValue(_read_.MCSEZBIL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_SEZB $ "CR" 
        this.w_XCODCEN = Nvl(this.w_Mvtcocen,space(15))
        this.w_XVOCCEN = Nvl(this.w_Vocdif,space(15))
        this.w_XCODCOM = Nvl(this.w_Mvtcomme,space(15))
      else
        this.w_XCODCEN = Space(15)
        this.w_XVOCCEN = Space(15)
        this.w_XCODCOM = Space(15)
      endif
      this.w_FLVAL = "N"
      this.w_XINICOM = this.w_TESINICOM
      this.w_XFINCOM = this.w_TESFINCOM
      if NOT EMPTY(this.w_XCODICE)
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        INSERT INTO CursError(REGOLA,CODICE,CPROWNUM,DESCRI,TIPCON)VALUES(ah_MsgFormat("Dati fatture"),"",0,ah_MsgFormat("Contropartita differenze di conversione non definita"),"")
        this.oParentObject.w_ERROR = .T.
        this.w_CANCELLA = .T.
      endif
    endif
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inizializzazione Variabili
    * --- Variabili Locali
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- TOTALI FATTURA
    this.w_IVAGLO = 0
    this.w_IVADET = 0
    this.w_IVAOMA = 0
    this.w_VALOMA = 0
    this.w_IVAIND = 0
    this.w_TOTVAL = this.w_ARROTA
    this.w_SPEINC = cp_ROUND( this.w_SPEINC * this.w_TOTDOC / this.w_OLDTOTDOC, this.w_DECTOT )
    this.w_SPEIMB = cp_ROUND( this.w_SPEIMB * this.w_TOTDOC / this.w_OLDTOTDOC, this.w_DECTOT )
    this.w_SPETRA = cp_ROUND( this.w_SPETRA * this.w_TOTDOC / this.w_OLDTOTDOC, this.w_DECTOT )
    this.w_SPEBOL = cp_ROUND( this.w_SPEBOL * this.w_TOTDOC / this.w_OLDTOTDOC, this.w_DECTOT )
    * --- Aggiorna Aliquota IVA Spese
    if this.w_SPEINC<>0 AND NOT EMPTY(this.w_MVIVAINC) AND this.w_FLRINC<>"S"
      this.w_APPO = this.w_MVIVAINC
      this.w_APPO3 = 1
      this.Page_7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.w_SPEIMB<>0 AND NOT EMPTY(this.w_MVIVAIMB) AND this.w_FLRIMB<>"S"
      this.w_APPO = this.w_MVIVAIMB
      this.w_APPO3 = 2
      this.Page_7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_APPIMB = 0
    endif
    if this.w_SPETRA<>0 AND NOT EMPTY(this.w_MVIVATRA) AND this.w_FLRTRA<>"S"
      this.w_APPO = this.w_MVIVATRA
      this.w_APPO3 = 3
      this.Page_7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Calcola Imposta su Castelletto IVA
    FOR i = 1 TO this.w_ALIVA
    * --- Se % IVA <> 0
    if AI[i, 5] > 0
      AI[i, 4] = IVAROUND( (NVL(AI[i, 3],0) * NVL(AI[i, 5],0) / 100) , this.w_DECTOT ,1 , this.w_CODVAL )
    else
      AI[i, 4] = 0
    endif
    ENDFOR
    * --- L iva delle spese distribuita sulle righe omaggio v� comunque pagata (memorizzo l importo per aggiustare l importo da addebitare )
    this.w_IVASPESETOT = 0
    if this.w_FLVEAC="V"
      FOR L_i = 1 TO 6
      * --- Legge %Iva e %Indetraibile
      this.w_PERIVA = 0
      this.w_PERIND = 0
      if EMPTY(this.w_CODIVE) AND NOT EMPTY(AI[L_i, 7])
        this.w_APPO = AI[L_i, 7]
        * --- Read from VOCIIVA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VOCIIVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "IVPERIVA,IVPERIND"+;
            " from "+i_cTable+" VOCIIVA where ";
                +"IVCODIVA = "+cp_ToStrODBC(this.w_APPO);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            IVPERIVA,IVPERIND;
            from (i_cTable) where;
                IVCODIVA = this.w_APPO;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PERIVA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
          this.w_PERIND = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        SELECT EXTFA003
      endif
      AI[L_i, 5] = this.w_PERIVA
      AI[L_i, 6] = this.w_PERIND
      * --- L iva relativa alle spese ripartite su righe omaggio deve essere comunque pagata 
      this.w_IVASPESE = IVAROUND( (NVL(AI[L_i, 9],0) * NVL(AI[L_i, 5],0) / 100) , this.w_DECTOT ,1 , this.w_CODVAL )
      this.w_IVASPESETOT = this.w_IVASPESETOT + this.w_IVASPESE
      * --- Totale Documento (in Valuta)  (Le righe Sconto Merce non vengono memorizzate)
      this.w_TOTVAL = this.w_TOTVAL + (IIF(AI[L_i, 1] $ "IE", 0, AI[L_i, 3]) + IIF(AI[L_i, 1] = "E", 0, iif(NVL(AI[L_i, 10]," ")<>"S", NVL(AI[L_i, 4],0), 0) ))
      if this.w_CODVAL<>g_PERVAL
        * --- Converte in Moneta di Conto
        AI[L_i, 3] = VAL2MON(AI[L_i, 3],this.w_CAOVAL,GETCAM(g_PERVAL,this.w_DATDOC),this.w_DATDOC,g_PERVAL, g_PERPVL)
        AI[L_i, 4] = VAL2MON(AI[L_i, 4],this.w_CAOVAL,GETCAM(g_PERVAL,this.w_DATDOC),this.w_DATDOC,g_PERPVL,g_PERPVL)
      endif
      * --- Totale IVA Globale
      this.w_IVAGLO = this.w_IVAGLO +IIF(AI[L_i, 1]="E",0, iif(NVL(AI[L_i, 10]," ")<>"S", NVL(AI[L_i, 4],0), 0) )
      * --- Calcola la Parte Indetraibile sull'Ammontare dell'Aliquota
      this.w_APPO1 = IIF(NVL(AI[L_i, 4],0)=0 OR NVL(AI[L_i, 6],0)=0, 0, cp_ROUND((NVL(AI[L_i, 4],0) * NVL(AI[L_i, 6],0)) / 100, g_PERPVL))
      * --- Totale IVA Indetraibile
      this.w_IVAIND = this.w_IVAIND + this.w_APPO1
      * --- Totale IVA Detraibile
      this.w_IVADET = this.w_IVADET + iif(NVL(AI[L_i, 10]," ")<>"S",(NVL(AI[L_i, 4],0) - this.w_APPO1),0)
      * --- Totale IVA su Omaggi
      this.w_IVAOMA = this.w_IVAOMA + IIF(AI[L_i, 1]="E" and NVL(AI[L_i, 10]," ")<>"S", NVL(AI[L_i, 4] - this.w_IVASPESE ,0), 0)
      ENDFOR
    else
      * --- Controllo se nelle contropartite ho abilitato il flag : Rilevazione indetraibilit� IVA
      if this.w_INDIVA="S"
        FOR L_i = 1 TO 6
        * --- Legge %Iva e %Indetraibile
        this.w_PERIVA = 0
        this.w_PERIND = 0
        if EMPTY(this.w_CODIVE) AND NOT EMPTY(AI[L_i, 7])
          this.w_APPO = AI[L_i, 7]
          * --- Read from VOCIIVA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VOCIIVA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "IVPERIVA,IVPERIND"+;
              " from "+i_cTable+" VOCIIVA where ";
                  +"IVCODIVA = "+cp_ToStrODBC(this.w_APPO);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              IVPERIVA,IVPERIND;
              from (i_cTable) where;
                  IVCODIVA = this.w_APPO;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_PERIVA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
            this.w_PERIND = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          SELECT EXTFA003
        endif
        AI[L_i, 5] = this.w_PERIVA
        AI[L_i, 6] = this.w_PERIND
        if this.w_CODVAL<>g_PERVAL
          * --- Converte in Moneta di Conto
          AI[L_i, 3] = VAL2MON(AI[L_i, 3],this.w_CAOVAL,GETCAM(g_PERVAL,this.w_DATDOC),this.w_DATDOC,g_PERVAL, g_PERPVL)
          AI[L_i, 4] = VAL2MON(AI[L_i, 4],this.w_CAOVAL,GETCAM(g_PERVAL,this.w_DATDOC),this.w_DATDOC,g_PERPVL,g_PERPVL)
        endif
        * --- Calcola la Parte Indetraibile sull'Ammontare dell'Aliquota
        this.w_APPO1 = IIF(NVL(AI[L_i, 4],0)=0 OR NVL(AI[L_i, 6],0)=0, 0, cp_ROUND((NVL(AI[L_i, 4],0) * NVL(AI[L_i, 6],0)) / 100, g_PERPVL))
        * --- Totale IVA Indetraibile
        this.w_IVAIND = this.w_IVAIND + this.w_APPO1
        * --- Totale Documento (in Valuta)  (Le righe Sconto Merce non vengono memorizzate)
        this.w_TOTVAL = this.w_TOTVAL + (IIF(AI[L_i, 1] $ "IE", 0, AI[L_i, 3]) + IIF(AI[L_i, 1] = "E", 0, this.w_APPO1))
        * --- Totale IVA Globale
        this.w_IVAGLO = this.w_IVAGLO +IIF(AI[L_i, 1]="E",0, this.w_APPO1)
        * --- Totale IVA Detraibile
        this.w_IVADET = 0
        * --- Totale IVA su Omaggi
        this.w_IVAOMA = this.w_IVAOMA + IIF(AI[L_i, 1]="E", this.w_APPO1,0)
        ENDFOR
      else
        * --- Totale Documento (in Valuta)  (Le righe Sconto Merce non vengono memorizzate)
        this.w_TOTVAL = this.w_TOTVAL
        * --- Totale IVA Globale
        this.w_IVAGLO = 0
        * --- Totale IVA Indetraibile
        this.w_IVAIND = 0
        * --- Totale IVA Detraibile
        this.w_IVADET = 0
        * --- Totale IVA su Omaggi
        this.w_IVAOMA = 0
      endif
    endif
    if this.w_CODVAL<>g_PERVAL
      this.w_SPEINC = VAL2MON( this.w_SPEINC ,this.w_CAOVAL,GETCAM(g_PERVAL,this.w_DATDOC),this.w_DATDOC,g_PERVAL, g_PERPVL)
      this.w_SPEIMB = VAL2MON( this.w_SPEIMB ,this.w_CAOVAL,GETCAM(g_PERVAL,this.w_DATDOC),this.w_DATDOC,g_PERVAL, g_PERPVL)
      this.w_SPETRA = VAL2MON( this.w_SPETRA ,this.w_CAOVAL,GETCAM(g_PERVAL,this.w_DATDOC),this.w_DATDOC,g_PERVAL, g_PERPVL)
      this.w_SPEBOL = VAL2MON( this.w_SPEBOL ,this.w_CAOVAL,GETCAM(g_PERVAL,this.w_DATDOC),this.w_DATDOC,g_PERVAL, g_PERPVL)
      this.w_ARROTA = VAL2MON( this.w_ARROTA ,this.w_CAOVAL,GETCAM(g_PERVAL,this.w_DATDOC),this.w_DATDOC,g_PERVAL, g_PERPVL)
      this.w_IVASPESETOT = VAL2MON( this.w_IVASPESETOT ,this.w_CAOVAL,GETCAM(g_PERVAL,this.w_DATDOC),this.w_DATDOC,g_PERVAL, g_PERPVL)
    endif
    if this.w_CODVAL<>g_PERVAL
      * --- Converte in Moneta di Conto
      this.w_TOTDOC = VAL2MON(this.w_TOTDOC,this.w_CAOVAL,GETCAM(g_PERVAL,this.w_DATDOC),this.w_DATDOC,g_PERVAL, g_PERPVL)
      * --- Non serve pi� perch� l'iva � gi� convertita
      *     w_IVADET = cp_ROUND(VAL2MON(this.w_IVADET,w_CAOVAL,g_CAOVAL,w_DATDOC,g_PERVAL), g_PERPVL)
    endif
    this.w_TOTDOC = this.w_TOTDOC + IIF(this.w_FLRINC="S",0,this.w_SPEINC) + IIF(this.w_FLRIMB="S",0,this.w_SPEIMB)
    this.w_TOTDOC = this.w_TOTDOC + IIF(this.w_FLRTRA="S",0,this.w_SPETRA) + this.w_SPEBOL + this.w_ARROTA
    this.w_TOTDOC = this.w_TOTDOC+this.w_IVAGLO
    SELECT DettDocu 
 CALCULATE SUM(VALRIG) FOR FLOMAG $ "IE" TO this.w_VALOMA
    * --- VALOMA lo sottraggo dopo il calcolo della conversione 
    *     perch� l'importo � gi� convertito (l iva dele spese ripartite su righe omaggio v� comunque pagata)
    this.w_TOTDOC = this.w_TOTDOC - this.w_VALOMA + this.w_IVASPESETOT
  endproc


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserisce Aliquote IVA per Spese
    * --- Calcolo aliquote IVA
    * --- Determina se ripartire le spese
    this.w_RIPSPESE = ( EMPTY (this.w_MVIVATRADOC) AND this.w_APPO3=3) OR ( EMPTY (this.w_MVIVAIMBDOC) AND this.w_APPO3=2) OR ( EMPTY (this.w_MVIVAINCDOC) AND this.w_APPO3=1)
    this.w_trov = .f.
    i = 0
    FOR i = 1 TO this.w_ALIVA
    if ((this.w_APPO = AI[i, 7] AND AI[i, 1] = "X" ) OR this.w_RIPSPESE) 
      * --- Aliquota gia' esistente
      do case
        case this.w_APPO3 = 1 
          if AI[i, 1] = "E"
            AI[i, 9] = AI[i, 9] + IIF (this.w_RIPSPESE , (this.w_SPEINC / this.w_TOTDOC ) * AI[i, 3] ,this.w_SPEINC)
          endif
          AI[i, 3] = AI[i, 3] + IIF (this.w_RIPSPESE , (this.w_SPEINC / this.w_TOTDOC ) * AI[i, 3] ,this.w_SPEINC)
          this.w_TROV = .t.
        case this.w_APPO3 = 2
          if AI[i, 1] = "E"
            AI[i, 9] = AI[i, 9] + IIF (this.w_RIPSPESE , (this.w_SPEIMB / this.w_TOTDOC ) * AI[i, 3] ,this.w_SPEIMB)
          endif
          AI[i, 3] = AI[i, 3] + IIF (this.w_RIPSPESE , (this.w_SPEIMB / this.w_TOTDOC ) * AI[i, 3] ,this.w_SPEIMB)
          this.w_TROV = .t.
        case this.w_APPO3 = 3 
          if AI[i, 1] = "E"
            AI[i, 9] = AI[i, 9] + IIF (this.w_RIPSPESE , (this.w_SPETRA / this.w_TOTDOC ) * AI[i, 3] ,this.w_SPETRA)
          endif
          AI[i, 3] = AI[i, 3] + IIF (this.w_RIPSPESE , (this.w_SPETRA / this.w_TOTDOC ) * AI[i, 3] ,this.w_SPETRA)
          this.w_TROV = .t.
      endcase
      if NOT this.w_RIPSPESE
        exit
      endif
    endif
    ENDFOR
    this.w_RIPSPESE = .F.
    * --- Se aliquota inesistente
    if this.w_trov = .f.
      * --- Aggiunge nuova aliquota
      this.w_ALIVA = this.w_ALIVA+1
      AI[this.w_ALIVA, 7] = this.w_APPO
      AI[this.w_ALIVA, 1] = "X"
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVPERIVA,IVPERIND"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.w_APPO);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVPERIVA,IVPERIND;
          from (i_cTable) where;
              IVCODIVA = this.w_APPO;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        AI[this.w_ALIVA, 5] = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
        AI[this.w_ALIVA, 6] = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      do case
        case this.w_APPO3 = 1
          AI[this.w_ALIVA, 3] = this.w_SPEINC
        case this.w_APPO3 = 2
          AI[this.w_ALIVA, 3] = this.w_SPEIMB
        case this.w_APPO3 = 3
          AI[this.w_ALIVA, 3] = this.w_SPETRA
      endcase
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='VOCIIVA'
    this.cWorkTables[2]='CONTVEAC'
    this.cWorkTables[3]='CONTI'
    this.cWorkTables[4]='MASTRI'
    this.cWorkTables[5]='CONTROPA'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
