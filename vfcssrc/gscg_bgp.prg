* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bgp                                                        *
*              Creazione registrazione prima nota                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_20]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-01-09                                                      *
* Last revis.: 2004-01-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER,pSERIAL,pROWNUM,pROWORD,pTIPCON,pCODCON,pCODESE,pIMPDAR,pIMPAVE,pFLPART,pDESRIG,pCAURIG,pFLSALD,pFLSALF,pFLSALI,pCODPAG,pCODAGE,pFLVABD,pINICOM,pFINCOM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bgp",oParentObject,m.pOPER,m.pSERIAL,m.pROWNUM,m.pROWORD,m.pTIPCON,m.pCODCON,m.pCODESE,m.pIMPDAR,m.pIMPAVE,m.pFLPART,m.pDESRIG,m.pCAURIG,m.pFLSALD,m.pFLSALF,m.pFLSALI,m.pCODPAG,m.pCODAGE,m.pFLVABD,m.pINICOM,m.pFINCOM)
return(i_retval)

define class tgscg_bgp as StdBatch
  * --- Local variables
  pOPER = space(2)
  pSERIAL = space(10)
  pROWNUM = space(3)
  pROWORD = space(4)
  pTIPCON = space(1)
  pCODCON = space(15)
  pCODESE = space(4)
  pIMPDAR = 0
  pIMPAVE = 0
  pFLPART = space(1)
  pDESRIG = space(35)
  pCAURIG = space(5)
  pFLSALD = space(1)
  pFLSALF = space(1)
  pFLSALI = space(1)
  pCODPAG = space(5)
  pCODAGE = space(5)
  pFLVABD = space(1)
  pINICOM = ctod("  /  /  ")
  pFINCOM = ctod("  /  /  ")
  w_SERIAL = space(10)
  w_ROWNUM = space(3)
  w_ROWORD = space(4)
  w_TIPCON = space(1)
  w_CODCON = space(15)
  w_CODESE = space(4)
  w_IMPDAR = 0
  w_IMPAVE = 0
  w_FLPART = space(1)
  w_DESRIG = space(35)
  w_CAURIG = space(5)
  w_FLSALD = space(1)
  w_FLSALF = space(1)
  w_FLSALI = space(1)
  w_FLZERO = space(1)
  w_CODPAG = space(5)
  w_CODAGE = space(5)
  w_FLVABD = space(1)
  w_INICOM = ctod("  /  /  ")
  w_FINCOM = ctod("  /  /  ")
  w_PNFLABAN = space(6)
  * --- WorkFile variables
  PNT_DETT_idx=0
  SALDICON_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue Creazione Righe di Primanota e Aggiornamento Saldicon dati i seguenti Parametri:
    *     pOPER     Chr(2) Parametro che identifica il tipo di operazione da eseguire 
    *     La prima lettea identifica la tabella la seconda l'operazione da eseguire
    *     pSERIAL  Chr(10)
    *     pROWNUM N(3)
    *     pROWORD N(4)
    *     pTIPCON    C(1)
    *     pCODCON  C(5)
    *     pCODESE  C(4)
    *     pIMPDAR   N(18,4)
    *     pIMPAVE    N(18,4)
    *     pFLPART    C(1)
    *     pDESRIG    C(35)
    *     PNCAURIG C(5)
    *     PNFLSALD C(1)
    *     PNFLSALF  C(1)
    *     PNFLSALI   C(1)
    *     PNCODPAG C(5)
    *     PNCODAGE C(5)
    *     PNFLVABD  C(1)
    *     PNINICOM   D(8)
    *     PNFINCOM  D(8)
    this.w_SERIAL = this.pSERIAL
    this.w_ROWNUM = this.pROWNUM
    this.w_ROWORD = this.pROWORD
    this.w_TIPCON = this.pTIPCON
    this.w_CODCON = this.pCODCON
    this.w_CODESE = this.pCODESE
    this.w_IMPDAR = this.pIMPDAR
    this.w_IMPAVE = this.pIMPAVE
    this.w_FLPART = this.pFLPART
    this.w_DESRIG = this.pDESRIG
    this.w_CAURIG = this.pCAURIG
    this.w_FLSALF = this.pFLSALF
    this.w_FLSALD = this.pFLSALD
    this.w_FLSALI = this.pFLSALI
    this.w_FLZERO = IIF(this.w_IMPDAR=0 AND this.w_IMPAVE=0,"S"," ")
    this.w_CODPAG = this.pCODPAG
    this.w_CODAGE = this.pCODAGE
    this.w_FLVABD = this.pFLVABD
    this.w_INICOM = this.pINICOM
    this.w_FINCOM = this.pFINCOM
    * --- Se riga di Abbuono Marco relativo campo
    this.w_PNFLABAN = iif(this.pOPER="PA" ,"AB"+ALLTRIM(STR(this.w_ROWNUM, 4, 0)),Space(6))
    do case
      case this.pOPER="PI" OR this.pOPER="PA" 
        * --- Try
        local bErr_03CCFB20
        bErr_03CCFB20=bTrsErr
        this.Try_03CCFB20()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- Raise
          i_Error="Impossibile inserire movimento in prima nota"
          return
        endif
        bTrsErr=bTrsErr or bErr_03CCFB20
        * --- End
      case this.pOPER="SI"
        * --- Aggiorna Saldo
        * --- Try
        local bErr_03CEEBC0
        bErr_03CEEBC0=bTrsErr
        this.Try_03CEEBC0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_03CEEBC0
        * --- End
        * --- Write into SALDICON
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDICON_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICON_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLDARPER =SLDARPER+ "+cp_ToStrODBC(this.w_IMPDAR);
          +",SLAVEPER =SLAVEPER+ "+cp_ToStrODBC(this.w_IMPAVE);
              +i_ccchkf ;
          +" where ";
              +"SLTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
              +" and SLCODICE = "+cp_ToStrODBC(this.w_CODCON);
              +" and SLCODESE = "+cp_ToStrODBC(this.w_CODESE);
                 )
        else
          update (i_cTable) set;
              SLDARPER = SLDARPER + this.w_IMPDAR;
              ,SLAVEPER = SLAVEPER + this.w_IMPAVE;
              &i_ccchkf. ;
           where;
              SLTIPCON = this.w_TIPCON;
              and SLCODICE = this.w_CODCON;
              and SLCODESE = this.w_CODESE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
    endcase
  endproc
  proc Try_03CCFB20()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PNT_DETT
    i_nConn=i_TableProp[this.PNT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PNSERIAL"+",CPROWNUM"+",CPROWORD"+",PNTIPCON"+",PNCODCON"+",PNIMPDAR"+",PNIMPAVE"+",PNFLPART"+",PNDESRIG"+",PNCAURIG"+",PNFLSALD"+",PNFLSALF"+",PNFLSALI"+",PNCODPAG"+",PNCODAGE"+",PNFLVABD"+",PNINICOM"+",PNFINCOM"+",PNFLZERO"+",PNFLABAN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'PNT_DETT','PNSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'PNT_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ROWORD),'PNT_DETT','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPCON),'PNT_DETT','PNTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODCON),'PNT_DETT','PNCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IMPDAR),'PNT_DETT','PNIMPDAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IMPAVE),'PNT_DETT','PNIMPAVE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FLPART),'PNT_DETT','PNFLPART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESRIG),'PNT_DETT','PNDESRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAURIG),'PNT_DETT','PNCAURIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FLSALD),'PNT_DETT','PNFLSALD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FLSALF),'PNT_DETT','PNFLSALF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FLSALI),'PNT_DETT','PNFLSALI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODPAG),'PNT_DETT','PNCODPAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODAGE),'PNT_DETT','PNCODAGE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FLVABD),'PNT_DETT','PNFLVABD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_INICOM),'PNT_DETT','PNINICOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FINCOM),'PNT_DETT','PNFINCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FLZERO),'PNT_DETT','PNFLZERO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLABAN),'PNT_DETT','PNFLABAN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_SERIAL,'CPROWNUM',this.w_ROWNUM,'CPROWORD',this.w_ROWORD,'PNTIPCON',this.w_TIPCON,'PNCODCON',this.w_CODCON,'PNIMPDAR',this.w_IMPDAR,'PNIMPAVE',this.w_IMPAVE,'PNFLPART',this.w_FLPART,'PNDESRIG',this.w_DESRIG,'PNCAURIG',this.w_CAURIG,'PNFLSALD',this.w_FLSALD,'PNFLSALF',this.w_FLSALF)
      insert into (i_cTable) (PNSERIAL,CPROWNUM,CPROWORD,PNTIPCON,PNCODCON,PNIMPDAR,PNIMPAVE,PNFLPART,PNDESRIG,PNCAURIG,PNFLSALD,PNFLSALF,PNFLSALI,PNCODPAG,PNCODAGE,PNFLVABD,PNINICOM,PNFINCOM,PNFLZERO,PNFLABAN &i_ccchkf. );
         values (;
           this.w_SERIAL;
           ,this.w_ROWNUM;
           ,this.w_ROWORD;
           ,this.w_TIPCON;
           ,this.w_CODCON;
           ,this.w_IMPDAR;
           ,this.w_IMPAVE;
           ,this.w_FLPART;
           ,this.w_DESRIG;
           ,this.w_CAURIG;
           ,this.w_FLSALD;
           ,this.w_FLSALF;
           ,this.w_FLSALI;
           ,this.w_CODPAG;
           ,this.w_CODAGE;
           ,this.w_FLVABD;
           ,this.w_INICOM;
           ,this.w_FINCOM;
           ,this.w_FLZERO;
           ,this.w_PNFLABAN;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03CEEBC0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICON
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICON_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLTIPCON"+",SLCODICE"+",SLCODESE"+",SLDARPRO"+",SLAVEPRO"+",SLDARPER"+",SLAVEPER"+",SLDARINI"+",SLAVEINI"+",SLDARFIN"+",SLAVEFIN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_TIPCON),'SALDICON','SLTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODCON),'SALDICON','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODESE),'SALDICON','SLCODESE');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARINI');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEINI');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARFIN');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEFIN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLTIPCON',this.w_TIPCON,'SLCODICE',this.w_CODCON,'SLCODESE',this.w_CODESE,'SLDARPRO',0,'SLAVEPRO',0,'SLDARPER',0,'SLAVEPER',0,'SLDARINI',0,'SLAVEINI',0,'SLDARFIN',0,'SLAVEFIN',0)
      insert into (i_cTable) (SLTIPCON,SLCODICE,SLCODESE,SLDARPRO,SLAVEPRO,SLDARPER,SLAVEPER,SLDARINI,SLAVEINI,SLDARFIN,SLAVEFIN &i_ccchkf. );
         values (;
           this.w_TIPCON;
           ,this.w_CODCON;
           ,this.w_CODESE;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,pOPER,pSERIAL,pROWNUM,pROWORD,pTIPCON,pCODCON,pCODESE,pIMPDAR,pIMPAVE,pFLPART,pDESRIG,pCAURIG,pFLSALD,pFLSALF,pFLSALI,pCODPAG,pCODAGE,pFLVABD,pINICOM,pFINCOM)
    this.pOPER=pOPER
    this.pSERIAL=pSERIAL
    this.pROWNUM=pROWNUM
    this.pROWORD=pROWORD
    this.pTIPCON=pTIPCON
    this.pCODCON=pCODCON
    this.pCODESE=pCODESE
    this.pIMPDAR=pIMPDAR
    this.pIMPAVE=pIMPAVE
    this.pFLPART=pFLPART
    this.pDESRIG=pDESRIG
    this.pCAURIG=pCAURIG
    this.pFLSALD=pFLSALD
    this.pFLSALF=pFLSALF
    this.pFLSALI=pFLSALI
    this.pCODPAG=pCODPAG
    this.pCODAGE=pCODAGE
    this.pFLVABD=pFLVABD
    this.pINICOM=pINICOM
    this.pFINCOM=pFINCOM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PNT_DETT'
    this.cWorkTables[2]='SALDICON'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER,pSERIAL,pROWNUM,pROWORD,pTIPCON,pCODCON,pCODESE,pIMPDAR,pIMPAVE,pFLPART,pDESRIG,pCAURIG,pFLSALD,pFLSALF,pFLSALI,pCODPAG,pCODAGE,pFLVABD,pINICOM,pFINCOM"
endproc
