* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_ari                                                        *
*              Determinazione punto e lotto di riordino                        *
*                                                                              *
*      Author: Zucchetti Tam S.p.A.                                            *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-10                                                      *
* Last revis.: 2014-12-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_ari"))

* --- Class definition
define class tgsma_ari as StdForm
  Top    = 8
  Left   = 15

  * --- Standard Properties
  Width  = 801
  Height = 487+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-12-11"
  HelpContextID=141919383
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=49

  * --- Constant Properties
  RIORDINO_IDX = 0
  MAGAZZIN_IDX = 0
  ESERCIZI_IDX = 0
  CATEGOMO_IDX = 0
  GRUMERC_IDX = 0
  FAM_ARTI_IDX = 0
  AZIENDA_IDX = 0
  ART_ICOL_IDX = 0
  CAU_DOMA_IDX = 0
  cFile = "RIORDINO"
  cKeySelect = "RINUMRIO,RICODESE"
  cKeyWhere  = "RINUMRIO=this.w_RINUMRIO and RICODESE=this.w_RICODESE"
  cKeyWhereODBC = '"RINUMRIO="+cp_ToStrODBC(this.w_RINUMRIO)';
      +'+" and RICODESE="+cp_ToStrODBC(this.w_RICODESE)';

  cKeyWhereODBCqualified = '"RIORDINO.RINUMRIO="+cp_ToStrODBC(this.w_RINUMRIO)';
      +'+" and RIORDINO.RICODESE="+cp_ToStrODBC(this.w_RICODESE)';

  cPrg = "gsma_ari"
  cComment = "Determinazione punto e lotto di riordino"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_AZIENDA = space(5)
  w_RINUMRIO = space(6)
  w_RICODESE = space(4)
  w_INIESE = ctod('  /  /  ')
  o_INIESE = ctod('  /  /  ')
  w_FINESE = ctod('  /  /  ')
  o_FINESE = ctod('  /  /  ')
  w_RIDATINI = ctod('  /  /  ')
  w_RIDATFIN = ctod('  /  /  ')
  w_RICOEFFI = 0
  w_RIDESRIO = space(40)
  w_OBTEST = ctod('  /  /  ')
  w_COLLEFIS = space(5)
  w_DATOBSO = ctod('  /  /  ')
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_DECQT = 0
  w_RICODINI = space(41)
  o_RICODINI = space(41)
  w_DESINI = space(41)
  w_RICODFIN = space(41)
  w_DESFIN = space(41)
  w_RIFAMINI = space(5)
  o_RIFAMINI = space(5)
  w_DESFAMAI = space(40)
  w_RIFAMIFN = space(5)
  w_DESFAMAF = space(40)
  w_RIGRUINI = space(5)
  o_RIGRUINI = space(5)
  w_DESGRUI = space(40)
  w_RIGRUFIN = space(5)
  w_DESGRUF = space(40)
  w_RICATINI = space(5)
  o_RICATINI = space(5)
  w_DESCATI = space(40)
  w_RICATFIN = space(5)
  w_RITIPGES = space(1)
  w_DESCATF = space(40)
  w_RIMAGINI = space(5)
  o_RIMAGINI = space(5)
  w_DESMAGI = space(40)
  w_RIMAGFIN = space(5)
  w_DESMAGF = space(40)
  w_RIFAPINI = space(5)
  w_RIFAPFIN = space(5)
  w_RIPIAINI = space(5)
  o_RIPIAINI = space(5)
  w_RIPIAFIN = space(5)
  w_RICODGRU = space(5)
  w_DESGRU = space(40)
  w_RICOSORD = 0
  w_RICOSBEN = 0
  w_RIPERINT = 0
  w_RIPERIZA = 0
  w_RIPROVEN = space(1)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_RICODESE = this.W_RICODESE
  op_RINUMRIO = this.W_RINUMRIO
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'RIORDINO','gsma_ari')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_ariPag1","gsma_ari",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Determinazione punto e lotto di riordino")
      .Pages(1).HelpContextID = 117379199
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oRINUMRIO_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[9]
    this.cWorkTables[1]='MAGAZZIN'
    this.cWorkTables[2]='ESERCIZI'
    this.cWorkTables[3]='CATEGOMO'
    this.cWorkTables[4]='GRUMERC'
    this.cWorkTables[5]='FAM_ARTI'
    this.cWorkTables[6]='AZIENDA'
    this.cWorkTables[7]='ART_ICOL'
    this.cWorkTables[8]='CAU_DOMA'
    this.cWorkTables[9]='RIORDINO'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(9))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.RIORDINO_IDX,5],7]
    this.nPostItConn=i_TableProp[this.RIORDINO_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_RINUMRIO = NVL(RINUMRIO,space(6))
      .w_RICODESE = NVL(RICODESE,space(4))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_26_joined
    link_1_26_joined=.f.
    local link_1_29_joined
    link_1_29_joined=.f.
    local link_1_32_joined
    link_1_32_joined=.f.
    local link_1_36_joined
    link_1_36_joined=.f.
    local link_1_38_joined
    link_1_38_joined=.f.
    local link_1_41_joined
    link_1_41_joined=.f.
    local link_1_44_joined
    link_1_44_joined=.f.
    local link_1_48_joined
    link_1_48_joined=.f.
    local link_1_51_joined
    link_1_51_joined=.f.
    local link_1_54_joined
    link_1_54_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from RIORDINO where RINUMRIO=KeySet.RINUMRIO
    *                            and RICODESE=KeySet.RICODESE
    *
    i_nConn = i_TableProp[this.RIORDINO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RIORDINO_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('RIORDINO')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "RIORDINO.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' RIORDINO '
      link_1_26_joined=this.AddJoinedLink_1_26(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_29_joined=this.AddJoinedLink_1_29(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_32_joined=this.AddJoinedLink_1_32(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_36_joined=this.AddJoinedLink_1_36(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_38_joined=this.AddJoinedLink_1_38(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_41_joined=this.AddJoinedLink_1_41(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_44_joined=this.AddJoinedLink_1_44(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_48_joined=this.AddJoinedLink_1_48(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_51_joined=this.AddJoinedLink_1_51(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_54_joined=this.AddJoinedLink_1_54(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'RINUMRIO',this.w_RINUMRIO  ,'RICODESE',this.w_RICODESE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_AZIENDA = i_CODAZI
        .w_INIESE = ctod("  /  /  ")
        .w_FINESE = ctod("  /  /  ")
        .w_COLLEFIS = space(5)
        .w_DATOBSO = ctod("  /  /  ")
        .w_DECQT = 0
        .w_DESINI = space(41)
        .w_DESFIN = space(41)
        .w_DESFAMAI = space(40)
        .w_DESFAMAF = space(40)
        .w_DESGRUI = space(40)
        .w_DESGRUF = space(40)
        .w_DESCATI = space(40)
        .w_DESCATF = space(40)
        .w_DESMAGI = space(40)
        .w_DESMAGF = space(40)
        .w_DESGRU = space(40)
          .link_1_1('Load')
        .w_RINUMRIO = NVL(RINUMRIO,space(6))
        .op_RINUMRIO = .w_RINUMRIO
        .w_RICODESE = NVL(RICODESE,space(4))
        .op_RICODESE = .w_RICODESE
          .link_1_3('Load')
        .w_RIDATINI = NVL(cp_ToDate(RIDATINI),ctod("  /  /  "))
        .w_RIDATFIN = NVL(cp_ToDate(RIDATFIN),ctod("  /  /  "))
        .w_RICOEFFI = NVL(RICOEFFI,0)
        .w_RIDESRIO = NVL(RIDESRIO,space(40))
        .w_OBTEST = .w_RIDATINI
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .w_RICODINI = NVL(RICODINI,space(41))
          if link_1_26_joined
            this.w_RICODINI = NVL(ARCODART126,NVL(this.w_RICODINI,space(41)))
            this.w_DESINI = NVL(ARDESART126,space(41))
          else
          .link_1_26('Load')
          endif
        .w_RICODFIN = NVL(RICODFIN,space(41))
          if link_1_29_joined
            this.w_RICODFIN = NVL(ARCODART129,NVL(this.w_RICODFIN,space(41)))
            this.w_DESFIN = NVL(ARDESART129,space(41))
          else
          .link_1_29('Load')
          endif
        .w_RIFAMINI = NVL(RIFAMINI,space(5))
          if link_1_32_joined
            this.w_RIFAMINI = NVL(FACODICE132,NVL(this.w_RIFAMINI,space(5)))
            this.w_DESFAMAI = NVL(FADESCRI132,space(40))
          else
          .link_1_32('Load')
          endif
        .w_RIFAMIFN = NVL(RIFAMIFN,space(5))
          if link_1_36_joined
            this.w_RIFAMIFN = NVL(FACODICE136,NVL(this.w_RIFAMIFN,space(5)))
            this.w_DESFAMAF = NVL(FADESCRI136,space(40))
          else
          .link_1_36('Load')
          endif
        .w_RIGRUINI = NVL(RIGRUINI,space(5))
          if link_1_38_joined
            this.w_RIGRUINI = NVL(GMCODICE138,NVL(this.w_RIGRUINI,space(5)))
            this.w_DESGRUI = NVL(GMDESCRI138,space(40))
          else
          .link_1_38('Load')
          endif
        .w_RIGRUFIN = NVL(RIGRUFIN,space(5))
          if link_1_41_joined
            this.w_RIGRUFIN = NVL(GMCODICE141,NVL(this.w_RIGRUFIN,space(5)))
            this.w_DESGRUF = NVL(GMDESCRI141,space(40))
          else
          .link_1_41('Load')
          endif
        .w_RICATINI = NVL(RICATINI,space(5))
          if link_1_44_joined
            this.w_RICATINI = NVL(OMCODICE144,NVL(this.w_RICATINI,space(5)))
            this.w_DESCATI = NVL(OMDESCRI144,space(40))
          else
          .link_1_44('Load')
          endif
        .w_RICATFIN = NVL(RICATFIN,space(5))
          if link_1_48_joined
            this.w_RICATFIN = NVL(OMCODICE148,NVL(this.w_RICATFIN,space(5)))
            this.w_DESCATF = NVL(OMDESCRI148,space(40))
          else
          .link_1_48('Load')
          endif
        .w_RITIPGES = NVL(RITIPGES,space(1))
        .w_RIMAGINI = NVL(RIMAGINI,space(5))
          if link_1_51_joined
            this.w_RIMAGINI = NVL(MGCODMAG151,NVL(this.w_RIMAGINI,space(5)))
            this.w_DESMAGI = NVL(MGDESMAG151,space(40))
          else
          .link_1_51('Load')
          endif
        .w_RIMAGFIN = NVL(RIMAGFIN,space(5))
          if link_1_54_joined
            this.w_RIMAGFIN = NVL(MGCODMAG154,NVL(this.w_RIMAGFIN,space(5)))
            this.w_DESMAGF = NVL(MGDESMAG154,space(40))
          else
          .link_1_54('Load')
          endif
        .w_RIFAPINI = NVL(RIFAPINI,space(5))
        .w_RIFAPFIN = NVL(RIFAPFIN,space(5))
        .w_RIPIAINI = NVL(RIPIAINI,space(5))
        .w_RIPIAFIN = NVL(RIPIAFIN,space(5))
        .w_RICODGRU = NVL(RICODGRU,space(5))
          .link_1_63('Load')
        .w_RICOSORD = NVL(RICOSORD,0)
        .w_RICOSBEN = NVL(RICOSBEN,0)
        .w_RIPERINT = NVL(RIPERINT,0)
        .w_RIPERIZA = NVL(RIPERIZA,0)
        .w_RIPROVEN = NVL(RIPROVEN,space(1))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'RIORDINO')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AZIENDA = space(5)
      .w_RINUMRIO = space(6)
      .w_RICODESE = space(4)
      .w_INIESE = ctod("  /  /  ")
      .w_FINESE = ctod("  /  /  ")
      .w_RIDATINI = ctod("  /  /  ")
      .w_RIDATFIN = ctod("  /  /  ")
      .w_RICOEFFI = 0
      .w_RIDESRIO = space(40)
      .w_OBTEST = ctod("  /  /  ")
      .w_COLLEFIS = space(5)
      .w_DATOBSO = ctod("  /  /  ")
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_DECQT = 0
      .w_RICODINI = space(41)
      .w_DESINI = space(41)
      .w_RICODFIN = space(41)
      .w_DESFIN = space(41)
      .w_RIFAMINI = space(5)
      .w_DESFAMAI = space(40)
      .w_RIFAMIFN = space(5)
      .w_DESFAMAF = space(40)
      .w_RIGRUINI = space(5)
      .w_DESGRUI = space(40)
      .w_RIGRUFIN = space(5)
      .w_DESGRUF = space(40)
      .w_RICATINI = space(5)
      .w_DESCATI = space(40)
      .w_RICATFIN = space(5)
      .w_RITIPGES = space(1)
      .w_DESCATF = space(40)
      .w_RIMAGINI = space(5)
      .w_DESMAGI = space(40)
      .w_RIMAGFIN = space(5)
      .w_DESMAGF = space(40)
      .w_RIFAPINI = space(5)
      .w_RIFAPFIN = space(5)
      .w_RIPIAINI = space(5)
      .w_RIPIAFIN = space(5)
      .w_RICODGRU = space(5)
      .w_DESGRU = space(40)
      .w_RICOSORD = 0
      .w_RICOSBEN = 0
      .w_RIPERINT = 0
      .w_RIPERIZA = 0
      .w_RIPROVEN = space(1)
      if .cFunction<>"Filter"
        .w_AZIENDA = i_CODAZI
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_AZIENDA))
          .link_1_1('Full')
          endif
          .DoRTCalc(2,2,.f.)
        .w_RICODESE = g_CODESE
        .DoRTCalc(3,3,.f.)
          if not(empty(.w_RICODESE))
          .link_1_3('Full')
          endif
          .DoRTCalc(4,5,.f.)
        .w_RIDATINI = .w_INIESE
        .w_RIDATFIN = .w_FINESE
        .w_RICOEFFI = 0
          .DoRTCalc(9,9,.f.)
        .w_OBTEST = .w_RIDATINI
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .DoRTCalc(11,18,.f.)
          if not(empty(.w_RICODINI))
          .link_1_26('Full')
          endif
        .DoRTCalc(19,20,.f.)
          if not(empty(.w_RICODFIN))
          .link_1_29('Full')
          endif
        .DoRTCalc(21,22,.f.)
          if not(empty(.w_RIFAMINI))
          .link_1_32('Full')
          endif
        .DoRTCalc(23,24,.f.)
          if not(empty(.w_RIFAMIFN))
          .link_1_36('Full')
          endif
        .DoRTCalc(25,26,.f.)
          if not(empty(.w_RIGRUINI))
          .link_1_38('Full')
          endif
        .DoRTCalc(27,28,.f.)
          if not(empty(.w_RIGRUFIN))
          .link_1_41('Full')
          endif
        .DoRTCalc(29,30,.f.)
          if not(empty(.w_RICATINI))
          .link_1_44('Full')
          endif
        .DoRTCalc(31,32,.f.)
          if not(empty(.w_RICATFIN))
          .link_1_48('Full')
          endif
        .w_RITIPGES = 'S'
        .DoRTCalc(34,35,.f.)
          if not(empty(.w_RIMAGINI))
          .link_1_51('Full')
          endif
        .DoRTCalc(36,37,.f.)
          if not(empty(.w_RIMAGFIN))
          .link_1_54('Full')
          endif
        .DoRTCalc(38,43,.f.)
          if not(empty(.w_RICODGRU))
          .link_1_63('Full')
          endif
          .DoRTCalc(44,48,.f.)
        .w_RIPROVEN = 'T'
      endif
    endwith
    cp_BlankRecExtFlds(this,'RIORDINO')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.RIORDINO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RIORDINO_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SERIOR","i_codazi,w_RICODESE,w_RINUMRIO")
      .op_codazi = .w_codazi
      .op_RICODESE = .w_RICODESE
      .op_RINUMRIO = .w_RINUMRIO
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oRINUMRIO_1_2.enabled = i_bVal
      .Page1.oPag.oRICODESE_1_3.enabled = i_bVal
      .Page1.oPag.oRIDATINI_1_6.enabled = i_bVal
      .Page1.oPag.oRIDATFIN_1_7.enabled = i_bVal
      .Page1.oPag.oRICOEFFI_1_8.enabled = i_bVal
      .Page1.oPag.oRIDESRIO_1_10.enabled = i_bVal
      .Page1.oPag.oRICODINI_1_26.enabled = i_bVal
      .Page1.oPag.oRICODFIN_1_29.enabled = i_bVal
      .Page1.oPag.oRIFAMINI_1_32.enabled = i_bVal
      .Page1.oPag.oRIFAMIFN_1_36.enabled = i_bVal
      .Page1.oPag.oRIGRUINI_1_38.enabled = i_bVal
      .Page1.oPag.oRIGRUFIN_1_41.enabled = i_bVal
      .Page1.oPag.oRICATINI_1_44.enabled = i_bVal
      .Page1.oPag.oRICATFIN_1_48.enabled = i_bVal
      .Page1.oPag.oRITIPGES_1_49.enabled = i_bVal
      .Page1.oPag.oRIMAGINI_1_51.enabled = i_bVal
      .Page1.oPag.oRIMAGFIN_1_54.enabled = i_bVal
      .Page1.oPag.oRICODGRU_1_63.enabled = i_bVal
      .Page1.oPag.oRICOSORD_1_68.enabled = i_bVal
      .Page1.oPag.oRICOSBEN_1_69.enabled = i_bVal
      .Page1.oPag.oRIPERINT_1_70.enabled = i_bVal
      .Page1.oPag.oRIPERIZA_1_71.enabled = i_bVal
      .Page1.oPag.oRIPROVEN_1_83.enabled = i_bVal
      .Page1.oPag.oBtn_1_65.enabled = i_bVal
      .Page1.oPag.oObj_1_22.enabled = i_bVal
      .Page1.oPag.oObj_1_23.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oRINUMRIO_1_2.enabled = .f.
        .Page1.oPag.oRICODESE_1_3.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oRINUMRIO_1_2.enabled = .t.
        .Page1.oPag.oRICODESE_1_3.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'RIORDINO',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.RIORDINO_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RINUMRIO,"RINUMRIO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RICODESE,"RICODESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RIDATINI,"RIDATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RIDATFIN,"RIDATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RICOEFFI,"RICOEFFI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RIDESRIO,"RIDESRIO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RICODINI,"RICODINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RICODFIN,"RICODFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RIFAMINI,"RIFAMINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RIFAMIFN,"RIFAMIFN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RIGRUINI,"RIGRUINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RIGRUFIN,"RIGRUFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RICATINI,"RICATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RICATFIN,"RICATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RITIPGES,"RITIPGES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RIMAGINI,"RIMAGINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RIMAGFIN,"RIMAGFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RIFAPINI,"RIFAPINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RIFAPFIN,"RIFAPFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RIPIAINI,"RIPIAINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RIPIAFIN,"RIPIAFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RICODGRU,"RICODGRU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RICOSORD,"RICOSORD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RICOSBEN,"RICOSBEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RIPERINT,"RIPERINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RIPERIZA,"RIPERIZA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RIPROVEN,"RIPROVEN",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.RIORDINO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RIORDINO_IDX,2])
    i_lTable = "RIORDINO"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.RIORDINO_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.RIORDINO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RIORDINO_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.RIORDINO_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SERIOR","i_codazi,w_RICODESE,w_RINUMRIO")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into RIORDINO
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'RIORDINO')
        i_extval=cp_InsertValODBCExtFlds(this,'RIORDINO')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(RINUMRIO,RICODESE,RIDATINI,RIDATFIN,RICOEFFI"+;
                  ",RIDESRIO,UTCC,UTCV,UTDC,UTDV"+;
                  ",RICODINI,RICODFIN,RIFAMINI,RIFAMIFN,RIGRUINI"+;
                  ",RIGRUFIN,RICATINI,RICATFIN,RITIPGES,RIMAGINI"+;
                  ",RIMAGFIN,RIFAPINI,RIFAPFIN,RIPIAINI,RIPIAFIN"+;
                  ",RICODGRU,RICOSORD,RICOSBEN,RIPERINT,RIPERIZA"+;
                  ",RIPROVEN "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_RINUMRIO)+;
                  ","+cp_ToStrODBCNull(this.w_RICODESE)+;
                  ","+cp_ToStrODBC(this.w_RIDATINI)+;
                  ","+cp_ToStrODBC(this.w_RIDATFIN)+;
                  ","+cp_ToStrODBC(this.w_RICOEFFI)+;
                  ","+cp_ToStrODBC(this.w_RIDESRIO)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBCNull(this.w_RICODINI)+;
                  ","+cp_ToStrODBCNull(this.w_RICODFIN)+;
                  ","+cp_ToStrODBCNull(this.w_RIFAMINI)+;
                  ","+cp_ToStrODBCNull(this.w_RIFAMIFN)+;
                  ","+cp_ToStrODBCNull(this.w_RIGRUINI)+;
                  ","+cp_ToStrODBCNull(this.w_RIGRUFIN)+;
                  ","+cp_ToStrODBCNull(this.w_RICATINI)+;
                  ","+cp_ToStrODBCNull(this.w_RICATFIN)+;
                  ","+cp_ToStrODBC(this.w_RITIPGES)+;
                  ","+cp_ToStrODBCNull(this.w_RIMAGINI)+;
                  ","+cp_ToStrODBCNull(this.w_RIMAGFIN)+;
                  ","+cp_ToStrODBC(this.w_RIFAPINI)+;
                  ","+cp_ToStrODBC(this.w_RIFAPFIN)+;
                  ","+cp_ToStrODBC(this.w_RIPIAINI)+;
                  ","+cp_ToStrODBC(this.w_RIPIAFIN)+;
                  ","+cp_ToStrODBCNull(this.w_RICODGRU)+;
                  ","+cp_ToStrODBC(this.w_RICOSORD)+;
                  ","+cp_ToStrODBC(this.w_RICOSBEN)+;
                  ","+cp_ToStrODBC(this.w_RIPERINT)+;
                  ","+cp_ToStrODBC(this.w_RIPERIZA)+;
                  ","+cp_ToStrODBC(this.w_RIPROVEN)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'RIORDINO')
        i_extval=cp_InsertValVFPExtFlds(this,'RIORDINO')
        cp_CheckDeletedKey(i_cTable,0,'RINUMRIO',this.w_RINUMRIO,'RICODESE',this.w_RICODESE)
        INSERT INTO (i_cTable);
              (RINUMRIO,RICODESE,RIDATINI,RIDATFIN,RICOEFFI,RIDESRIO,UTCC,UTCV,UTDC,UTDV,RICODINI,RICODFIN,RIFAMINI,RIFAMIFN,RIGRUINI,RIGRUFIN,RICATINI,RICATFIN,RITIPGES,RIMAGINI,RIMAGFIN,RIFAPINI,RIFAPFIN,RIPIAINI,RIPIAFIN,RICODGRU,RICOSORD,RICOSBEN,RIPERINT,RIPERIZA,RIPROVEN  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_RINUMRIO;
                  ,this.w_RICODESE;
                  ,this.w_RIDATINI;
                  ,this.w_RIDATFIN;
                  ,this.w_RICOEFFI;
                  ,this.w_RIDESRIO;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_RICODINI;
                  ,this.w_RICODFIN;
                  ,this.w_RIFAMINI;
                  ,this.w_RIFAMIFN;
                  ,this.w_RIGRUINI;
                  ,this.w_RIGRUFIN;
                  ,this.w_RICATINI;
                  ,this.w_RICATFIN;
                  ,this.w_RITIPGES;
                  ,this.w_RIMAGINI;
                  ,this.w_RIMAGFIN;
                  ,this.w_RIFAPINI;
                  ,this.w_RIFAPFIN;
                  ,this.w_RIPIAINI;
                  ,this.w_RIPIAFIN;
                  ,this.w_RICODGRU;
                  ,this.w_RICOSORD;
                  ,this.w_RICOSBEN;
                  ,this.w_RIPERINT;
                  ,this.w_RIPERIZA;
                  ,this.w_RIPROVEN;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- gsma_ari
    * Forza aggiornamento del database
    this.bupdated=.t.
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.RIORDINO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RIORDINO_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.RIORDINO_IDX,i_nConn)
      *
      * update RIORDINO
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'RIORDINO')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " RIDATINI="+cp_ToStrODBC(this.w_RIDATINI)+;
             ",RIDATFIN="+cp_ToStrODBC(this.w_RIDATFIN)+;
             ",RICOEFFI="+cp_ToStrODBC(this.w_RICOEFFI)+;
             ",RIDESRIO="+cp_ToStrODBC(this.w_RIDESRIO)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",RICODINI="+cp_ToStrODBCNull(this.w_RICODINI)+;
             ",RICODFIN="+cp_ToStrODBCNull(this.w_RICODFIN)+;
             ",RIFAMINI="+cp_ToStrODBCNull(this.w_RIFAMINI)+;
             ",RIFAMIFN="+cp_ToStrODBCNull(this.w_RIFAMIFN)+;
             ",RIGRUINI="+cp_ToStrODBCNull(this.w_RIGRUINI)+;
             ",RIGRUFIN="+cp_ToStrODBCNull(this.w_RIGRUFIN)+;
             ",RICATINI="+cp_ToStrODBCNull(this.w_RICATINI)+;
             ",RICATFIN="+cp_ToStrODBCNull(this.w_RICATFIN)+;
             ",RITIPGES="+cp_ToStrODBC(this.w_RITIPGES)+;
             ",RIMAGINI="+cp_ToStrODBCNull(this.w_RIMAGINI)+;
             ",RIMAGFIN="+cp_ToStrODBCNull(this.w_RIMAGFIN)+;
             ",RIFAPINI="+cp_ToStrODBC(this.w_RIFAPINI)+;
             ",RIFAPFIN="+cp_ToStrODBC(this.w_RIFAPFIN)+;
             ",RIPIAINI="+cp_ToStrODBC(this.w_RIPIAINI)+;
             ",RIPIAFIN="+cp_ToStrODBC(this.w_RIPIAFIN)+;
             ",RICODGRU="+cp_ToStrODBCNull(this.w_RICODGRU)+;
             ",RICOSORD="+cp_ToStrODBC(this.w_RICOSORD)+;
             ",RICOSBEN="+cp_ToStrODBC(this.w_RICOSBEN)+;
             ",RIPERINT="+cp_ToStrODBC(this.w_RIPERINT)+;
             ",RIPERIZA="+cp_ToStrODBC(this.w_RIPERIZA)+;
             ",RIPROVEN="+cp_ToStrODBC(this.w_RIPROVEN)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'RIORDINO')
        i_cWhere = cp_PKFox(i_cTable  ,'RINUMRIO',this.w_RINUMRIO  ,'RICODESE',this.w_RICODESE  )
        UPDATE (i_cTable) SET;
              RIDATINI=this.w_RIDATINI;
             ,RIDATFIN=this.w_RIDATFIN;
             ,RICOEFFI=this.w_RICOEFFI;
             ,RIDESRIO=this.w_RIDESRIO;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,RICODINI=this.w_RICODINI;
             ,RICODFIN=this.w_RICODFIN;
             ,RIFAMINI=this.w_RIFAMINI;
             ,RIFAMIFN=this.w_RIFAMIFN;
             ,RIGRUINI=this.w_RIGRUINI;
             ,RIGRUFIN=this.w_RIGRUFIN;
             ,RICATINI=this.w_RICATINI;
             ,RICATFIN=this.w_RICATFIN;
             ,RITIPGES=this.w_RITIPGES;
             ,RIMAGINI=this.w_RIMAGINI;
             ,RIMAGFIN=this.w_RIMAGFIN;
             ,RIFAPINI=this.w_RIFAPINI;
             ,RIFAPFIN=this.w_RIFAPFIN;
             ,RIPIAINI=this.w_RIPIAINI;
             ,RIPIAFIN=this.w_RIPIAFIN;
             ,RICODGRU=this.w_RICODGRU;
             ,RICOSORD=this.w_RICOSORD;
             ,RICOSBEN=this.w_RICOSBEN;
             ,RIPERINT=this.w_RIPERINT;
             ,RIPERIZA=this.w_RIPERIZA;
             ,RIPROVEN=this.w_RIPROVEN;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.RIORDINO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RIORDINO_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.RIORDINO_IDX,i_nConn)
      *
      * delete RIORDINO
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'RINUMRIO',this.w_RINUMRIO  ,'RICODESE',this.w_RICODESE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.RIORDINO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RIORDINO_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,5,.t.)
        if .o_INIESE<>.w_INIESE
            .w_RIDATINI = .w_INIESE
        endif
        if .o_FINESE<>.w_FINESE
            .w_RIDATFIN = .w_FINESE
        endif
        .DoRTCalc(8,9,.t.)
            .w_OBTEST = .w_RIDATINI
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .DoRTCalc(11,19,.t.)
        if .o_RICODINI<>.w_RICODINI
          .link_1_29('Full')
        endif
        .DoRTCalc(21,23,.t.)
        if .o_RIFAMINI<>.w_RIFAMINI
          .link_1_36('Full')
        endif
        .DoRTCalc(25,27,.t.)
        if .o_RIGRUINI<>.w_RIGRUINI
          .link_1_41('Full')
        endif
        .DoRTCalc(29,31,.t.)
        if .o_RICATINI<>.w_RICATINI
          .link_1_48('Full')
        endif
        .DoRTCalc(33,36,.t.)
        if .o_RIMAGINI<>.w_RIMAGINI
          .link_1_54('Full')
        endif
        .DoRTCalc(38,42,.t.)
        if .o_RICATINI<>.w_RICATINI
          .link_1_63('Full')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi .or. .op_RICODESE<>.w_RICODESE
           cp_AskTableProg(this,i_nConn,"SERIOR","i_codazi,w_RICODESE,w_RINUMRIO")
          .op_RINUMRIO = .w_RINUMRIO
        endif
        .op_codazi = .w_codazi
        .op_RICODESE = .w_RICODESE
      endwith
      this.DoRTCalc(44,49,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oRICODESE_1_3.enabled = this.oPgFrm.Page1.oPag.oRICODESE_1_3.mCond()
    this.oPgFrm.Page1.oPag.oRIDATINI_1_6.enabled = this.oPgFrm.Page1.oPag.oRIDATINI_1_6.mCond()
    this.oPgFrm.Page1.oPag.oRIDATFIN_1_7.enabled = this.oPgFrm.Page1.oPag.oRIDATFIN_1_7.mCond()
    this.oPgFrm.Page1.oPag.oRICOEFFI_1_8.enabled = this.oPgFrm.Page1.oPag.oRICOEFFI_1_8.mCond()
    this.oPgFrm.Page1.oPag.oRIDESRIO_1_10.enabled = this.oPgFrm.Page1.oPag.oRIDESRIO_1_10.mCond()
    this.oPgFrm.Page1.oPag.oRICODINI_1_26.enabled = this.oPgFrm.Page1.oPag.oRICODINI_1_26.mCond()
    this.oPgFrm.Page1.oPag.oRICODFIN_1_29.enabled = this.oPgFrm.Page1.oPag.oRICODFIN_1_29.mCond()
    this.oPgFrm.Page1.oPag.oRIFAMINI_1_32.enabled = this.oPgFrm.Page1.oPag.oRIFAMINI_1_32.mCond()
    this.oPgFrm.Page1.oPag.oRIFAMIFN_1_36.enabled = this.oPgFrm.Page1.oPag.oRIFAMIFN_1_36.mCond()
    this.oPgFrm.Page1.oPag.oRIGRUINI_1_38.enabled = this.oPgFrm.Page1.oPag.oRIGRUINI_1_38.mCond()
    this.oPgFrm.Page1.oPag.oRIGRUFIN_1_41.enabled = this.oPgFrm.Page1.oPag.oRIGRUFIN_1_41.mCond()
    this.oPgFrm.Page1.oPag.oRICATINI_1_44.enabled = this.oPgFrm.Page1.oPag.oRICATINI_1_44.mCond()
    this.oPgFrm.Page1.oPag.oRICATFIN_1_48.enabled = this.oPgFrm.Page1.oPag.oRICATFIN_1_48.mCond()
    this.oPgFrm.Page1.oPag.oRIMAGINI_1_51.enabled = this.oPgFrm.Page1.oPag.oRIMAGINI_1_51.mCond()
    this.oPgFrm.Page1.oPag.oRIMAGFIN_1_54.enabled = this.oPgFrm.Page1.oPag.oRIMAGFIN_1_54.mCond()
    this.oPgFrm.Page1.oPag.oRICOSORD_1_68.enabled = this.oPgFrm.Page1.oPag.oRICOSORD_1_68.mCond()
    this.oPgFrm.Page1.oPag.oRICOSBEN_1_69.enabled = this.oPgFrm.Page1.oPag.oRICOSBEN_1_69.mCond()
    this.oPgFrm.Page1.oPag.oRIPERINT_1_70.enabled = this.oPgFrm.Page1.oPag.oRIPERINT_1_70.mCond()
    this.oPgFrm.Page1.oPag.oRIPERIZA_1_71.enabled = this.oPgFrm.Page1.oPag.oRIPERIZA_1_71.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_22.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_23.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=AZIENDA
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZIENDA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZIENDA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZPERPQT";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_AZIENDA)
            select AZCODAZI,AZPERPQT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZIENDA = NVL(_Link_.AZCODAZI,space(5))
      this.w_DECQT = NVL(_Link_.AZPERPQT,0)
    else
      if i_cCtrl<>'Load'
        this.w_AZIENDA = space(5)
      endif
      this.w_DECQT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZIENDA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RICODESE
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RICODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_KES',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_RICODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_AZIENDA;
                     ,'ESCODESE',trim(this.w_RICODESE))
          select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RICODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RICODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oRICODESE_1_3'),i_cWhere,'GSAR_KES',"Elenco esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_AZIENDA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RICODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_RICODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_AZIENDA;
                       ,'ESCODESE',this.w_RICODESE)
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RICODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_INIESE = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_FINESE = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_RICODESE = space(4)
      endif
      this.w_INIESE = ctod("  /  /  ")
      this.w_FINESE = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RICODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RICODINI
  func Link_1_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RICODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_AAR',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_RICODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_RICODINI))
          select ARCODART,ARDESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RICODINI)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RICODINI) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oRICODINI_1_26'),i_cWhere,'GSMA_AAR',"Articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RICODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_RICODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_RICODINI)
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RICODINI = NVL(_Link_.ARCODART,space(41))
      this.w_DESINI = NVL(_Link_.ARDESART,space(41))
    else
      if i_cCtrl<>'Load'
        this.w_RICODINI = space(41)
      endif
      this.w_DESINI = space(41)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_RICODINI<=.w_RICODFIN or empty(.w_RICODFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice iniziale maggiore di quello finale")
        endif
        this.w_RICODINI = space(41)
        this.w_DESINI = space(41)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RICODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_26(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_26.ARCODART as ARCODART126"+ ",link_1_26.ARDESART as ARDESART126"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_26 on RIORDINO.RICODINI=link_1_26.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_26"
          i_cKey=i_cKey+'+" and RIORDINO.RICODINI=link_1_26.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=RICODFIN
  func Link_1_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RICODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_AAR',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_RICODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_RICODFIN))
          select ARCODART,ARDESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RICODFIN)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RICODFIN) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oRICODFIN_1_29'),i_cWhere,'GSMA_AAR',"Articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RICODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_RICODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_RICODFIN)
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RICODFIN = NVL(_Link_.ARCODART,space(41))
      this.w_DESFIN = NVL(_Link_.ARDESART,space(41))
    else
      if i_cCtrl<>'Load'
        this.w_RICODFIN = space(41)
      endif
      this.w_DESFIN = space(41)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_RICODINI<=.w_RICODFIN or empty(.w_RICODINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice iniziale maggiore di quello finale")
        endif
        this.w_RICODFIN = space(41)
        this.w_DESFIN = space(41)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RICODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_29(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_29.ARCODART as ARCODART129"+ ",link_1_29.ARDESART as ARDESART129"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_29 on RIORDINO.RICODFIN=link_1_29.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_29"
          i_cKey=i_cKey+'+" and RIORDINO.RICODFIN=link_1_29.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=RIFAMINI
  func Link_1_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RIFAMINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_RIFAMINI)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_RIFAMINI))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RIFAMINI)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RIFAMINI) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oRIFAMINI_1_32'),i_cWhere,'',"FAMIGLIE ARTICOLI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RIFAMINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_RIFAMINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_RIFAMINI)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RIFAMINI = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAI = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(40))
    else
      if i_cCtrl<>'Load'
        this.w_RIFAMINI = space(5)
      endif
      this.w_DESFAMAI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_RIFAMINI <= .w_RIFAMIFN OR EMPTY(.w_RIFAMIFN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice iniziale maggiore di quello finale")
        endif
        this.w_RIFAMINI = space(5)
        this.w_DESFAMAI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RIFAMINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_32(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.FAM_ARTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_32.FACODICE as FACODICE132"+ ","+cp_TransLinkFldName('link_1_32.FADESCRI')+" as FADESCRI132"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_32 on RIORDINO.RIFAMINI=link_1_32.FACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_32"
          i_cKey=i_cKey+'+" and RIORDINO.RIFAMINI=link_1_32.FACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=RIFAMIFN
  func Link_1_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RIFAMIFN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_RIFAMIFN)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_RIFAMIFN))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RIFAMIFN)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RIFAMIFN) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oRIFAMIFN_1_36'),i_cWhere,'',"FAMIGLIE ARTICOLI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RIFAMIFN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_RIFAMIFN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_RIFAMIFN)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RIFAMIFN = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAF = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(40))
    else
      if i_cCtrl<>'Load'
        this.w_RIFAMIFN = space(5)
      endif
      this.w_DESFAMAF = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_RIFAMINI <= .w_RIFAMIFN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice iniziale maggiore di quello finale")
        endif
        this.w_RIFAMIFN = space(5)
        this.w_DESFAMAF = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RIFAMIFN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_36(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.FAM_ARTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_36.FACODICE as FACODICE136"+ ","+cp_TransLinkFldName('link_1_36.FADESCRI')+" as FADESCRI136"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_36 on RIORDINO.RIFAMIFN=link_1_36.FACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_36"
          i_cKey=i_cKey+'+" and RIORDINO.RIFAMIFN=link_1_36.FACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=RIGRUINI
  func Link_1_38(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RIGRUINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_RIGRUINI)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_RIGRUINI))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RIGRUINI)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RIGRUINI) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oRIGRUINI_1_38'),i_cWhere,'',"GRUPPI MERCEOLOGICI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RIGRUINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_RIGRUINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_RIGRUINI)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RIGRUINI = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUI = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(40))
    else
      if i_cCtrl<>'Load'
        this.w_RIGRUINI = space(5)
      endif
      this.w_DESGRUI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_RIGRUINI <= .w_RIGRUFIN OR EMPTY(.w_RIGRUFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice iniziale maggiore di quello finale")
        endif
        this.w_RIGRUINI = space(5)
        this.w_DESGRUI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RIGRUINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_38(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.GRUMERC_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_38.GMCODICE as GMCODICE138"+ ","+cp_TransLinkFldName('link_1_38.GMDESCRI')+" as GMDESCRI138"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_38 on RIORDINO.RIGRUINI=link_1_38.GMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_38"
          i_cKey=i_cKey+'+" and RIORDINO.RIGRUINI=link_1_38.GMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=RIGRUFIN
  func Link_1_41(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RIGRUFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_RIGRUFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_RIGRUFIN))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RIGRUFIN)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RIGRUFIN) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oRIGRUFIN_1_41'),i_cWhere,'',"GRUPPI MERCEOLOGICI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RIGRUFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_RIGRUFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_RIGRUFIN)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RIGRUFIN = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUF = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(40))
    else
      if i_cCtrl<>'Load'
        this.w_RIGRUFIN = space(5)
      endif
      this.w_DESGRUF = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_RIGRUINI <= .w_RIGRUFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice iniziale maggiore di quello finale")
        endif
        this.w_RIGRUFIN = space(5)
        this.w_DESGRUF = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RIGRUFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_41(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.GRUMERC_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_41.GMCODICE as GMCODICE141"+ ","+cp_TransLinkFldName('link_1_41.GMDESCRI')+" as GMDESCRI141"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_41 on RIORDINO.RIGRUFIN=link_1_41.GMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_41"
          i_cKey=i_cKey+'+" and RIORDINO.RIGRUFIN=link_1_41.GMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=RICATINI
  func Link_1_44(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RICATINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_RICATINI)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_RICATINI))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RICATINI)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RICATINI) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oRICATINI_1_44'),i_cWhere,'',"CATEGORIE OMOGENEE",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RICATINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_RICATINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_RICATINI)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RICATINI = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATI = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(40))
    else
      if i_cCtrl<>'Load'
        this.w_RICATINI = space(5)
      endif
      this.w_DESCATI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_RICATINI<= .w_RICATFIN OR EMPTY(.w_RICATFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice iniziale maggiore di quello finale")
        endif
        this.w_RICATINI = space(5)
        this.w_DESCATI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RICATINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_44(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CATEGOMO_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_44.OMCODICE as OMCODICE144"+ ","+cp_TransLinkFldName('link_1_44.OMDESCRI')+" as OMDESCRI144"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_44 on RIORDINO.RICATINI=link_1_44.OMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_44"
          i_cKey=i_cKey+'+" and RIORDINO.RICATINI=link_1_44.OMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=RICATFIN
  func Link_1_48(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RICATFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_RICATFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_RICATFIN))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RICATFIN)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RICATFIN) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oRICATFIN_1_48'),i_cWhere,'',"CATEGORIE OMOGENEE",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RICATFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_RICATFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_RICATFIN)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RICATFIN = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATF = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(40))
    else
      if i_cCtrl<>'Load'
        this.w_RICATFIN = space(5)
      endif
      this.w_DESCATF = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_RICATINI<= .w_RICATFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice iniziale maggiore di quello finale")
        endif
        this.w_RICATFIN = space(5)
        this.w_DESCATF = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RICATFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_48(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CATEGOMO_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_48.OMCODICE as OMCODICE148"+ ","+cp_TransLinkFldName('link_1_48.OMDESCRI')+" as OMDESCRI148"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_48 on RIORDINO.RICATFIN=link_1_48.OMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_48"
          i_cKey=i_cKey+'+" and RIORDINO.RICATFIN=link_1_48.OMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=RIMAGINI
  func Link_1_51(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RIMAGINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_RIMAGINI)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_RIMAGINI))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RIMAGINI)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RIMAGINI) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oRIMAGINI_1_51'),i_cWhere,'GSAR_AMA',"MAGAZZINI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RIMAGINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_RIMAGINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_RIMAGINI)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RIMAGINI = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAGI = NVL(_Link_.MGDESMAG,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_RIMAGINI = space(5)
      endif
      this.w_DESMAGI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_RIMAGINI <= .w_RIMAGFIN OR EMPTY(.w_RIMAGFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice iniziale maggiore di quello finale")
        endif
        this.w_RIMAGINI = space(5)
        this.w_DESMAGI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RIMAGINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_51(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_51.MGCODMAG as MGCODMAG151"+ ",link_1_51.MGDESMAG as MGDESMAG151"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_51 on RIORDINO.RIMAGINI=link_1_51.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_51"
          i_cKey=i_cKey+'+" and RIORDINO.RIMAGINI=link_1_51.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=RIMAGFIN
  func Link_1_54(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RIMAGFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_RIMAGFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_RIMAGFIN))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RIMAGFIN)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RIMAGFIN) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oRIMAGFIN_1_54'),i_cWhere,'GSAR_AMA',"MAGAZZINI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RIMAGFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_RIMAGFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_RIMAGFIN)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RIMAGFIN = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAGF = NVL(_Link_.MGDESMAG,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_RIMAGFIN = space(5)
      endif
      this.w_DESMAGF = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_RIMAGINI <= .w_RIMAGFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice iniziale maggiore di quello finale")
        endif
        this.w_RIMAGFIN = space(5)
        this.w_DESMAGF = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RIMAGFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_54(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_54.MGCODMAG as MGCODMAG154"+ ",link_1_54.MGDESMAG as MGDESMAG154"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_54 on RIORDINO.RIMAGFIN=link_1_54.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_54"
          i_cKey=i_cKey+'+" and RIORDINO.RIMAGFIN=link_1_54.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=RICODGRU
  func Link_1_63(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_DOMA_IDX,3]
    i_lTable = "CAU_DOMA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_DOMA_IDX,2], .t., this.CAU_DOMA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_DOMA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RICODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_MCD',True,'CAU_DOMA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CSCAUGRU like "+cp_ToStrODBC(trim(this.w_RICODGRU)+"%");

          i_ret=cp_SQL(i_nConn,"select CSCAUGRU,CSCAUDES";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CSCAUGRU","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CSCAUGRU',trim(this.w_RICODGRU))
          select CSCAUGRU,CSCAUDES;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CSCAUGRU into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RICODGRU)==trim(_Link_.CSCAUGRU) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RICODGRU) and !this.bDontReportError
            deferred_cp_zoom('CAU_DOMA','*','CSCAUGRU',cp_AbsName(oSource.parent,'oRICODGRU_1_63'),i_cWhere,'GSMA_MCD',"GRUPPO CAUSALI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCAUGRU,CSCAUDES";
                     +" from "+i_cTable+" "+i_lTable+" where CSCAUGRU="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCAUGRU',oSource.xKey(1))
            select CSCAUGRU,CSCAUDES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RICODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCAUGRU,CSCAUDES";
                   +" from "+i_cTable+" "+i_lTable+" where CSCAUGRU="+cp_ToStrODBC(this.w_RICODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCAUGRU',this.w_RICODGRU)
            select CSCAUGRU,CSCAUDES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RICODGRU = NVL(_Link_.CSCAUGRU,space(5))
      this.w_DESGRU = NVL(_Link_.CSCAUDES,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_RICODGRU = space(5)
      endif
      this.w_DESGRU = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_DOMA_IDX,2])+'\'+cp_ToStr(_Link_.CSCAUGRU,1)
      cp_ShowWarn(i_cKey,this.CAU_DOMA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RICODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oRINUMRIO_1_2.value==this.w_RINUMRIO)
      this.oPgFrm.Page1.oPag.oRINUMRIO_1_2.value=this.w_RINUMRIO
    endif
    if not(this.oPgFrm.Page1.oPag.oRICODESE_1_3.value==this.w_RICODESE)
      this.oPgFrm.Page1.oPag.oRICODESE_1_3.value=this.w_RICODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oRIDATINI_1_6.value==this.w_RIDATINI)
      this.oPgFrm.Page1.oPag.oRIDATINI_1_6.value=this.w_RIDATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oRIDATFIN_1_7.value==this.w_RIDATFIN)
      this.oPgFrm.Page1.oPag.oRIDATFIN_1_7.value=this.w_RIDATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oRICOEFFI_1_8.value==this.w_RICOEFFI)
      this.oPgFrm.Page1.oPag.oRICOEFFI_1_8.value=this.w_RICOEFFI
    endif
    if not(this.oPgFrm.Page1.oPag.oRIDESRIO_1_10.value==this.w_RIDESRIO)
      this.oPgFrm.Page1.oPag.oRIDESRIO_1_10.value=this.w_RIDESRIO
    endif
    if not(this.oPgFrm.Page1.oPag.oRICODINI_1_26.value==this.w_RICODINI)
      this.oPgFrm.Page1.oPag.oRICODINI_1_26.value=this.w_RICODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINI_1_27.value==this.w_DESINI)
      this.oPgFrm.Page1.oPag.oDESINI_1_27.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oRICODFIN_1_29.value==this.w_RICODFIN)
      this.oPgFrm.Page1.oPag.oRICODFIN_1_29.value=this.w_RICODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIN_1_30.value==this.w_DESFIN)
      this.oPgFrm.Page1.oPag.oDESFIN_1_30.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oRIFAMINI_1_32.value==this.w_RIFAMINI)
      this.oPgFrm.Page1.oPag.oRIFAMINI_1_32.value=this.w_RIFAMINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAMAI_1_34.value==this.w_DESFAMAI)
      this.oPgFrm.Page1.oPag.oDESFAMAI_1_34.value=this.w_DESFAMAI
    endif
    if not(this.oPgFrm.Page1.oPag.oRIFAMIFN_1_36.value==this.w_RIFAMIFN)
      this.oPgFrm.Page1.oPag.oRIFAMIFN_1_36.value=this.w_RIFAMIFN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAMAF_1_37.value==this.w_DESFAMAF)
      this.oPgFrm.Page1.oPag.oDESFAMAF_1_37.value=this.w_DESFAMAF
    endif
    if not(this.oPgFrm.Page1.oPag.oRIGRUINI_1_38.value==this.w_RIGRUINI)
      this.oPgFrm.Page1.oPag.oRIGRUINI_1_38.value=this.w_RIGRUINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRUI_1_40.value==this.w_DESGRUI)
      this.oPgFrm.Page1.oPag.oDESGRUI_1_40.value=this.w_DESGRUI
    endif
    if not(this.oPgFrm.Page1.oPag.oRIGRUFIN_1_41.value==this.w_RIGRUFIN)
      this.oPgFrm.Page1.oPag.oRIGRUFIN_1_41.value=this.w_RIGRUFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRUF_1_43.value==this.w_DESGRUF)
      this.oPgFrm.Page1.oPag.oDESGRUF_1_43.value=this.w_DESGRUF
    endif
    if not(this.oPgFrm.Page1.oPag.oRICATINI_1_44.value==this.w_RICATINI)
      this.oPgFrm.Page1.oPag.oRICATINI_1_44.value=this.w_RICATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCATI_1_47.value==this.w_DESCATI)
      this.oPgFrm.Page1.oPag.oDESCATI_1_47.value=this.w_DESCATI
    endif
    if not(this.oPgFrm.Page1.oPag.oRICATFIN_1_48.value==this.w_RICATFIN)
      this.oPgFrm.Page1.oPag.oRICATFIN_1_48.value=this.w_RICATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oRITIPGES_1_49.RadioValue()==this.w_RITIPGES)
      this.oPgFrm.Page1.oPag.oRITIPGES_1_49.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCATF_1_50.value==this.w_DESCATF)
      this.oPgFrm.Page1.oPag.oDESCATF_1_50.value=this.w_DESCATF
    endif
    if not(this.oPgFrm.Page1.oPag.oRIMAGINI_1_51.value==this.w_RIMAGINI)
      this.oPgFrm.Page1.oPag.oRIMAGINI_1_51.value=this.w_RIMAGINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAGI_1_53.value==this.w_DESMAGI)
      this.oPgFrm.Page1.oPag.oDESMAGI_1_53.value=this.w_DESMAGI
    endif
    if not(this.oPgFrm.Page1.oPag.oRIMAGFIN_1_54.value==this.w_RIMAGFIN)
      this.oPgFrm.Page1.oPag.oRIMAGFIN_1_54.value=this.w_RIMAGFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAGF_1_56.value==this.w_DESMAGF)
      this.oPgFrm.Page1.oPag.oDESMAGF_1_56.value=this.w_DESMAGF
    endif
    if not(this.oPgFrm.Page1.oPag.oRICODGRU_1_63.value==this.w_RICODGRU)
      this.oPgFrm.Page1.oPag.oRICODGRU_1_63.value=this.w_RICODGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRU_1_64.value==this.w_DESGRU)
      this.oPgFrm.Page1.oPag.oDESGRU_1_64.value=this.w_DESGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oRICOSORD_1_68.value==this.w_RICOSORD)
      this.oPgFrm.Page1.oPag.oRICOSORD_1_68.value=this.w_RICOSORD
    endif
    if not(this.oPgFrm.Page1.oPag.oRICOSBEN_1_69.value==this.w_RICOSBEN)
      this.oPgFrm.Page1.oPag.oRICOSBEN_1_69.value=this.w_RICOSBEN
    endif
    if not(this.oPgFrm.Page1.oPag.oRIPERINT_1_70.value==this.w_RIPERINT)
      this.oPgFrm.Page1.oPag.oRIPERINT_1_70.value=this.w_RIPERINT
    endif
    if not(this.oPgFrm.Page1.oPag.oRIPERIZA_1_71.value==this.w_RIPERIZA)
      this.oPgFrm.Page1.oPag.oRIPERIZA_1_71.value=this.w_RIPERIZA
    endif
    if not(this.oPgFrm.Page1.oPag.oRIPROVEN_1_83.RadioValue()==this.w_RIPROVEN)
      this.oPgFrm.Page1.oPag.oRIPROVEN_1_83.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'RIORDINO')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_RIDATINI)) or not(.w_RIDATINI <= .w_RIDATFIN))  and (.cFunction<>'Edit')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRIDATINI_1_6.SetFocus()
            i_bnoObbl = !empty(.w_RIDATINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data iniziale maggiore della data finale")
          case   ((empty(.w_RIDATFIN)) or not(.w_RIDATINI <= .w_RIDATFIN))  and (.cFunction<>'Edit')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRIDATFIN_1_7.SetFocus()
            i_bnoObbl = !empty(.w_RIDATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data iniziale maggiore della data finale")
          case   not(0=<ABS(.w_RICOEFFI) and ABS(.w_RICOEFFI)<=1)  and (.cFunction<>'Edit')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRICOEFFI_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_RIDESRIO))  and (.cFunction<>'Edit')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRIDESRIO_1_10.SetFocus()
            i_bnoObbl = !empty(.w_RIDESRIO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_RICODINI<=.w_RICODFIN or empty(.w_RICODFIN))  and (.cFunction<>'Edit')  and not(empty(.w_RICODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRICODINI_1_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice iniziale maggiore di quello finale")
          case   not(.w_RICODINI<=.w_RICODFIN or empty(.w_RICODINI))  and (.cFunction<>'Edit')  and not(empty(.w_RICODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRICODFIN_1_29.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice iniziale maggiore di quello finale")
          case   not(.w_RIFAMINI <= .w_RIFAMIFN OR EMPTY(.w_RIFAMIFN))  and (.cFunction<>'Edit')  and not(empty(.w_RIFAMINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRIFAMINI_1_32.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice iniziale maggiore di quello finale")
          case   not(.w_RIFAMINI <= .w_RIFAMIFN)  and (.cFunction<>'Edit')  and not(empty(.w_RIFAMIFN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRIFAMIFN_1_36.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice iniziale maggiore di quello finale")
          case   not(.w_RIGRUINI <= .w_RIGRUFIN OR EMPTY(.w_RIGRUFIN))  and (.cFunction<>'Edit')  and not(empty(.w_RIGRUINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRIGRUINI_1_38.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice iniziale maggiore di quello finale")
          case   not(.w_RIGRUINI <= .w_RIGRUFIN)  and (.cFunction<>'Edit')  and not(empty(.w_RIGRUFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRIGRUFIN_1_41.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice iniziale maggiore di quello finale")
          case   not(.w_RICATINI<= .w_RICATFIN OR EMPTY(.w_RICATFIN))  and (.cFunction<>'Edit')  and not(empty(.w_RICATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRICATINI_1_44.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice iniziale maggiore di quello finale")
          case   not(.w_RICATINI<= .w_RICATFIN)  and (.cFunction<>'Edit')  and not(empty(.w_RICATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRICATFIN_1_48.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice iniziale maggiore di quello finale")
          case   not(.w_RIMAGINI <= .w_RIMAGFIN OR EMPTY(.w_RIMAGFIN))  and (.cFunction<>'Edit')  and not(empty(.w_RIMAGINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRIMAGINI_1_51.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice iniziale maggiore di quello finale")
          case   not(.w_RIMAGINI <= .w_RIMAGFIN)  and (.cFunction<>'Edit')  and not(empty(.w_RIMAGFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRIMAGFIN_1_54.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice iniziale maggiore di quello finale")
          case   ((empty(.w_RICOSORD)) or not(.w_RICOSORD >0))  and (.cFunction<>'Edit')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRICOSORD_1_68.SetFocus()
            i_bnoObbl = !empty(.w_RICOSORD)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_RICOSBEN)) or not(.w_RICOSBEN >0))  and (.cFunction<>'Edit')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRICOSBEN_1_69.SetFocus()
            i_bnoObbl = !empty(.w_RICOSBEN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_RIPERINT)) or not(.w_RIPERINT > 0 and .w_RIPERINT <= 100))  and (.cFunction<>'Edit')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRIPERINT_1_70.SetFocus()
            i_bnoObbl = !empty(.w_RIPERINT)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_RIPERIZA)) or not(.w_RIPERIZA > 0 and .w_RIPERIZA <= 100))  and (.cFunction<>'Edit')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRIPERIZA_1_71.SetFocus()
            i_bnoObbl = !empty(.w_RIPERIZA)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_INIESE = this.w_INIESE
    this.o_FINESE = this.w_FINESE
    this.o_RICODINI = this.w_RICODINI
    this.o_RIFAMINI = this.w_RIFAMINI
    this.o_RIGRUINI = this.w_RIGRUINI
    this.o_RICATINI = this.w_RICATINI
    this.o_RIMAGINI = this.w_RIMAGINI
    this.o_RIPIAINI = this.w_RIPIAINI
    return

enddefine

* --- Define pages as container
define class tgsma_ariPag1 as StdContainer
  Width  = 797
  height = 488
  stdWidth  = 797
  stdheight = 488
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oRINUMRIO_1_2 as StdField with uid="UGHWLBOTFR",rtseq=2,rtrep=.f.,;
    cFormVar = "w_RINUMRIO", cQueryName = "RINUMRIO",nZero=6,;
    bObbl = .f. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Numero progressivo del punto di riordino",;
    HelpContextID = 6311067,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=95, Top=21, cSayPict='"999999"', cGetPict='"999999"', InputMask=replicate('X',6)

  add object oRICODESE_1_3 as StdField with uid="CFQAALWSYZ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_RICODESE", cQueryName = "RINUMRIO,RICODESE",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Codice esercizio",;
    HelpContextID = 234290341,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=433, Top=21, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", cZoomOnZoom="GSAR_KES", oKey_1_1="ESCODAZI", oKey_1_2="this.w_AZIENDA", oKey_2_1="ESCODESE", oKey_2_2="this.w_RICODESE"

  func oRICODESE_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
   endif
  endfunc

  func oRICODESE_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oRICODESE_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRICODESE_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_AZIENDA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_AZIENDA)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oRICODESE_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_KES',"Elenco esercizi",'',this.parent.oContained
  endproc
  proc oRICODESE_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_KES()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ESCODAZI=w_AZIENDA
     i_obj.w_ESCODESE=this.parent.oContained.w_RICODESE
     i_obj.ecpSave()
  endproc

  add object oRIDATINI_1_6 as StdField with uid="KCLAEMNZCN",rtseq=6,rtrep=.f.,;
    cFormVar = "w_RIDATINI", cQueryName = "RIDATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data iniziale maggiore della data finale",;
    ToolTipText = "Data a partire dalla quale sono stati considerati i movimenti",;
    HelpContextID = 117117791,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=574, Top=21

  func oRIDATINI_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
   endif
  endfunc

  func oRIDATINI_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_RIDATINI <= .w_RIDATFIN)
    endwith
    return bRes
  endfunc

  add object oRIDATFIN_1_7 as StdField with uid="IYFHNZMNTY",rtseq=7,rtrep=.f.,;
    cFormVar = "w_RIDATFIN", cQueryName = "RIDATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data iniziale maggiore della data finale",;
    ToolTipText = "Data fino alla quale sono stati considerati i movimenti",;
    HelpContextID = 201649308,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=721, Top=21

  func oRIDATFIN_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
   endif
  endfunc

  func oRIDATFIN_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_RIDATINI <= .w_RIDATFIN)
    endwith
    return bRes
  endfunc

  add object oRICOEFFI_1_8 as StdField with uid="RRYKJTKVEI",rtseq=8,rtrep=.f.,;
    cFormVar = "w_RICOEFFI", cQueryName = "RICOEFFI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Coefficiente di correzione per il calcolo del punto di riordino",;
    HelpContextID = 216464545,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=333, Top=79, cSayPict='"99.99"', cGetPict='"99.99"'

  func oRICOEFFI_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
   endif
  endfunc

  func oRICOEFFI_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (0=<ABS(.w_RICOEFFI) and ABS(.w_RICOEFFI)<=1)
    endwith
    return bRes
  endfunc

  add object oRIDESRIO_1_10 as StdField with uid="NYGYYVMAKB",rtseq=9,rtrep=.f.,;
    cFormVar = "w_RIDESRIO", cQueryName = "RIDESRIO",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione associata al calcolo del punto di riordino",;
    HelpContextID = 1109147,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=95, Top=49, InputMask=replicate('X',40)

  func oRIDESRIO_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
   endif
  endfunc


  add object oObj_1_22 as cp_runprogram with uid="HXEGIAJMSL",left=-4, top=515, width=238,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSMA_BRI",;
    cEvent = "Record Inserted,Record Updated",;
    nPag=1;
    , HelpContextID = 216953882


  add object oObj_1_23 as cp_runprogram with uid="RYGBTTKRWY",left=-4, top=538, width=133,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSMA_BRE',;
    cEvent = "Delete start",;
    nPag=1;
    , ToolTipText = "Eliminazione figli articoli";
    , HelpContextID = 216953882

  add object oRICODINI_1_26 as StdField with uid="KZLVNVNYZD",rtseq=18,rtrep=.f.,;
    cFormVar = "w_RICODINI", cQueryName = "RICODINI",;
    bObbl = .f. , nPag = 1, value=space(41), bMultilanguage =  .f.,;
    sErrorMsg = "Codice iniziale maggiore di quello finale",;
    ToolTipText = "Codice articolo iniziale",;
    HelpContextID = 101253983,;
   bGlobalFont=.t.,;
    Height=21, Width=314, Left=113, Top=128, InputMask=replicate('X',41), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_AAR", oKey_1_1="ARCODART", oKey_1_2="this.w_RICODINI"

  func oRICODINI_1_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
   endif
  endfunc

  func oRICODINI_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_26('Part',this)
    endwith
    return bRes
  endfunc

  proc oRICODINI_1_26.ecpDrop(oSource)
    this.Parent.oContained.link_1_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRICODINI_1_26.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oRICODINI_1_26'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_AAR',"Articoli",'',this.parent.oContained
  endproc
  proc oRICODINI_1_26.mZoomOnZoom
    local i_obj
    i_obj=GSMA_AAR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_RICODINI
     i_obj.ecpSave()
  endproc

  add object oDESINI_1_27 as StdField with uid="GNKUAPTQYP",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(41), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione codice articolo iniziale",;
    HelpContextID = 111410742,;
   bGlobalFont=.t.,;
    Height=21, Width=358, Left=429, Top=128, InputMask=replicate('X',41)

  add object oRICODFIN_1_29 as StdField with uid="TJOPTWSAJR",rtseq=20,rtrep=.f.,;
    cFormVar = "w_RICODFIN", cQueryName = "RICODFIN",;
    bObbl = .f. , nPag = 1, value=space(41), bMultilanguage =  .f.,;
    sErrorMsg = "Codice iniziale maggiore di quello finale",;
    ToolTipText = "Codice articolo finale",;
    HelpContextID = 217513116,;
   bGlobalFont=.t.,;
    Height=21, Width=314, Left=113, Top=152, InputMask=replicate('X',41), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_AAR", oKey_1_1="ARCODART", oKey_1_2="this.w_RICODFIN"

  proc oRICODFIN_1_29.mDefault
    with this.Parent.oContained
      if empty(.w_RICODFIN)
        .w_RICODFIN = .w_RICODINI
      endif
    endwith
  endproc

  func oRICODFIN_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
   endif
  endfunc

  func oRICODFIN_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_29('Part',this)
    endwith
    return bRes
  endfunc

  proc oRICODFIN_1_29.ecpDrop(oSource)
    this.Parent.oContained.link_1_29('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRICODFIN_1_29.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oRICODFIN_1_29'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_AAR',"Articoli",'',this.parent.oContained
  endproc
  proc oRICODFIN_1_29.mZoomOnZoom
    local i_obj
    i_obj=GSMA_AAR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_RICODFIN
     i_obj.ecpSave()
  endproc

  add object oDESFIN_1_30 as StdField with uid="KMZPQMWUNH",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(41), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione codice articolo finale",;
    HelpContextID = 189857334,;
   bGlobalFont=.t.,;
    Height=21, Width=358, Left=429, Top=152, InputMask=replicate('X',41)

  add object oRIFAMINI_1_32 as StdField with uid="WSAXRUIGRH",rtseq=22,rtrep=.f.,;
    cFormVar = "w_RIFAMINI", cQueryName = "RIFAMINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice iniziale maggiore di quello finale",;
    ToolTipText = "Codice famiglia iniziale",;
    HelpContextID = 109785951,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=114, Top=176, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_RIFAMINI"

  func oRIFAMINI_1_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
   endif
  endfunc

  func oRIFAMINI_1_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_32('Part',this)
    endwith
    return bRes
  endfunc

  proc oRIFAMINI_1_32.ecpDrop(oSource)
    this.Parent.oContained.link_1_32('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRIFAMINI_1_32.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oRIFAMINI_1_32'),iif(empty(i_cWhere),.f.,i_cWhere),'',"FAMIGLIE ARTICOLI",'',this.parent.oContained
  endproc

  add object oDESFAMAI_1_34 as StdField with uid="OTJVWRHRIN",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESFAMAI", cQueryName = "DESFAMAI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione codice famiglia iniziale",;
    HelpContextID = 164691583,;
   bGlobalFont=.t.,;
    Height=21, Width=218, Left=181, Top=176, InputMask=replicate('X',40)

  add object oRIFAMIFN_1_36 as StdField with uid="WRJMQQUSTN",rtseq=24,rtrep=.f.,;
    cFormVar = "w_RIFAMIFN", cQueryName = "RIFAMIFN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice iniziale maggiore di quello finale",;
    ToolTipText = "Codice famiglia finale",;
    HelpContextID = 158649500,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=503, Top=176, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_RIFAMIFN"

  proc oRIFAMIFN_1_36.mDefault
    with this.Parent.oContained
      if empty(.w_RIFAMIFN)
        .w_RIFAMIFN = .w_RIFAMINI
      endif
    endwith
  endproc

  func oRIFAMIFN_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
   endif
  endfunc

  func oRIFAMIFN_1_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_36('Part',this)
    endwith
    return bRes
  endfunc

  proc oRIFAMIFN_1_36.ecpDrop(oSource)
    this.Parent.oContained.link_1_36('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRIFAMIFN_1_36.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oRIFAMIFN_1_36'),iif(empty(i_cWhere),.f.,i_cWhere),'',"FAMIGLIE ARTICOLI",'',this.parent.oContained
  endproc

  add object oDESFAMAF_1_37 as StdField with uid="RDKTLNSCJY",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESFAMAF", cQueryName = "DESFAMAF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione codice famiglia finale",;
    HelpContextID = 164691580,;
   bGlobalFont=.t.,;
    Height=21, Width=218, Left=569, Top=176, InputMask=replicate('X',40)

  add object oRIGRUINI_1_38 as StdField with uid="OGEWDVKEIX",rtseq=26,rtrep=.f.,;
    cFormVar = "w_RIGRUINI", cQueryName = "RIGRUINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice iniziale maggiore di quello finale",;
    ToolTipText = "Guppo merceologico iniziale",;
    HelpContextID = 119292767,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=114, Top=201, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_RIGRUINI"

  func oRIGRUINI_1_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
   endif
  endfunc

  func oRIGRUINI_1_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_38('Part',this)
    endwith
    return bRes
  endfunc

  proc oRIGRUINI_1_38.ecpDrop(oSource)
    this.Parent.oContained.link_1_38('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRIGRUINI_1_38.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oRIGRUINI_1_38'),iif(empty(i_cWhere),.f.,i_cWhere),'',"GRUPPI MERCEOLOGICI",'',this.parent.oContained
  endproc

  add object oDESGRUI_1_40 as StdField with uid="ETRKIIXCVX",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DESGRUI", cQueryName = "DESGRUI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione guppo merceologico iniziale",;
    HelpContextID = 220070346,;
   bGlobalFont=.t.,;
    Height=21, Width=218, Left=181, Top=201, InputMask=replicate('X',40)

  add object oRIGRUFIN_1_41 as StdField with uid="BNAMYZKGAK",rtseq=28,rtrep=.f.,;
    cFormVar = "w_RIGRUFIN", cQueryName = "RIGRUFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice iniziale maggiore di quello finale",;
    ToolTipText = "Gruppo merceologico finale",;
    HelpContextID = 199474332,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=503, Top=201, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_RIGRUFIN"

  proc oRIGRUFIN_1_41.mDefault
    with this.Parent.oContained
      if empty(.w_RIGRUFIN)
        .w_RIGRUFIN = .w_RIGRUINI
      endif
    endwith
  endproc

  func oRIGRUFIN_1_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
   endif
  endfunc

  func oRIGRUFIN_1_41.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_41('Part',this)
    endwith
    return bRes
  endfunc

  proc oRIGRUFIN_1_41.ecpDrop(oSource)
    this.Parent.oContained.link_1_41('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRIGRUFIN_1_41.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oRIGRUFIN_1_41'),iif(empty(i_cWhere),.f.,i_cWhere),'',"GRUPPI MERCEOLOGICI",'',this.parent.oContained
  endproc

  add object oDESGRUF_1_43 as StdField with uid="CRACSOXCVU",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESGRUF", cQueryName = "DESGRUF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descriizione gruppo merceologico finale",;
    HelpContextID = 220070346,;
   bGlobalFont=.t.,;
    Height=21, Width=218, Left=569, Top=201, InputMask=replicate('X',40)

  add object oRICATINI_1_44 as StdField with uid="DYZGBECYIH",rtseq=30,rtrep=.f.,;
    cFormVar = "w_RICATINI", cQueryName = "RICATINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice iniziale maggiore di quello finale",;
    ToolTipText = "Codice famiglia produzione iniziale",;
    HelpContextID = 117113695,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=114, Top=225, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_RICATINI"

  func oRICATINI_1_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
   endif
  endfunc

  func oRICATINI_1_44.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_44('Part',this)
    endwith
    return bRes
  endfunc

  proc oRICATINI_1_44.ecpDrop(oSource)
    this.Parent.oContained.link_1_44('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRICATINI_1_44.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oRICATINI_1_44'),iif(empty(i_cWhere),.f.,i_cWhere),'',"CATEGORIE OMOGENEE",'',this.parent.oContained
  endproc

  add object oDESCATI_1_47 as StdField with uid="LCTXWVEWBP",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DESCATI", cQueryName = "DESCATI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione codice famiglia iniziale",;
    HelpContextID = 254935498,;
   bGlobalFont=.t.,;
    Height=21, Width=218, Left=181, Top=225, InputMask=replicate('X',40)

  add object oRICATFIN_1_48 as StdField with uid="QMZPEBAUQL",rtseq=32,rtrep=.f.,;
    cFormVar = "w_RICATFIN", cQueryName = "RICATFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice iniziale maggiore di quello finale",;
    ToolTipText = "Codice famiglia produzione finale",;
    HelpContextID = 201653404,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=503, Top=225, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_RICATFIN"

  proc oRICATFIN_1_48.mDefault
    with this.Parent.oContained
      if empty(.w_RICATFIN)
        .w_RICATFIN = .w_RICATINI
      endif
    endwith
  endproc

  func oRICATFIN_1_48.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
   endif
  endfunc

  func oRICATFIN_1_48.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_48('Part',this)
    endwith
    return bRes
  endfunc

  proc oRICATFIN_1_48.ecpDrop(oSource)
    this.Parent.oContained.link_1_48('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRICATFIN_1_48.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oRICATFIN_1_48'),iif(empty(i_cWhere),.f.,i_cWhere),'',"CATEGORIE OMOGENEE",'',this.parent.oContained
  endproc


  add object oRITIPGES_1_49 as StdCombo with uid="NMKUCZBOSF",rtseq=33,rtrep=.f.,left=503,top=249,width=152,height=21;
    , HelpContextID = 188476567;
    , cFormVar="w_RITIPGES",RowSource=""+"a fabbisogno,"+"a scorta,"+"a consumo,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oRITIPGES_1_49.RadioValue()
    return(iif(this.value =1,'F',;
    iif(this.value =2,'S',;
    iif(this.value =3,'C',;
    iif(this.value =4,'T',;
    space(1))))))
  endfunc
  func oRITIPGES_1_49.GetRadio()
    this.Parent.oContained.w_RITIPGES = this.RadioValue()
    return .t.
  endfunc

  func oRITIPGES_1_49.SetRadio()
    this.Parent.oContained.w_RITIPGES=trim(this.Parent.oContained.w_RITIPGES)
    this.value = ;
      iif(this.Parent.oContained.w_RITIPGES=='F',1,;
      iif(this.Parent.oContained.w_RITIPGES=='S',2,;
      iif(this.Parent.oContained.w_RITIPGES=='C',3,;
      iif(this.Parent.oContained.w_RITIPGES=='T',4,;
      0))))
  endfunc

  add object oDESCATF_1_50 as StdField with uid="UEANTVTCCJ",rtseq=34,rtrep=.f.,;
    cFormVar = "w_DESCATF", cQueryName = "DESCATF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 254935498,;
   bGlobalFont=.t.,;
    Height=21, Width=218, Left=569, Top=225, InputMask=replicate('X',40)

  add object oRIMAGINI_1_51 as StdField with uid="YEXKWZAGMG",rtseq=35,rtrep=.f.,;
    cFormVar = "w_RIMAGINI", cQueryName = "RIMAGINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice iniziale maggiore di quello finale",;
    ToolTipText = "Codice magazzino iniziale",;
    HelpContextID = 103523167,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=114, Top=280, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_RIMAGINI"

  func oRIMAGINI_1_51.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
   endif
  endfunc

  func oRIMAGINI_1_51.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_51('Part',this)
    endwith
    return bRes
  endfunc

  proc oRIMAGINI_1_51.ecpDrop(oSource)
    this.Parent.oContained.link_1_51('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRIMAGINI_1_51.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oRIMAGINI_1_51'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"MAGAZZINI",'',this.parent.oContained
  endproc
  proc oRIMAGINI_1_51.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_RIMAGINI
     i_obj.ecpSave()
  endproc

  add object oDESMAGI_1_53 as StdField with uid="SMQGACSEAJ",rtseq=36,rtrep=.f.,;
    cFormVar = "w_DESMAGI", cQueryName = "DESMAGI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione codice magazzino iniziale",;
    HelpContextID = 203948490,;
   bGlobalFont=.t.,;
    Height=21, Width=218, Left=181, Top=280, InputMask=replicate('X',40)

  add object oRIMAGFIN_1_54 as StdField with uid="NJMQBQTCMI",rtseq=37,rtrep=.f.,;
    cFormVar = "w_RIMAGFIN", cQueryName = "RIMAGFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice iniziale maggiore di quello finale",;
    ToolTipText = "Codice magazzino finale",;
    HelpContextID = 215243932,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=114, Top=305, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_RIMAGFIN"

  proc oRIMAGFIN_1_54.mDefault
    with this.Parent.oContained
      if empty(.w_RIMAGFIN)
        .w_RIMAGFIN = .w_RIMAGINI
      endif
    endwith
  endproc

  func oRIMAGFIN_1_54.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
   endif
  endfunc

  func oRIMAGFIN_1_54.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_54('Part',this)
    endwith
    return bRes
  endfunc

  proc oRIMAGFIN_1_54.ecpDrop(oSource)
    this.Parent.oContained.link_1_54('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRIMAGFIN_1_54.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oRIMAGFIN_1_54'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"MAGAZZINI",'',this.parent.oContained
  endproc
  proc oRIMAGFIN_1_54.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_RIMAGFIN
     i_obj.ecpSave()
  endproc

  add object oDESMAGF_1_56 as StdField with uid="GYTUONEXTT",rtseq=38,rtrep=.f.,;
    cFormVar = "w_DESMAGF", cQueryName = "DESMAGF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione codice magazzino finale",;
    HelpContextID = 203948490,;
   bGlobalFont=.t.,;
    Height=21, Width=218, Left=181, Top=305, InputMask=replicate('X',40)

  add object oRICODGRU_1_63 as StdField with uid="MDUTKRZRDI",rtseq=43,rtrep=.f.,;
    cFormVar = "w_RICODGRU", cQueryName = "RICODGRU",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo causali",;
    HelpContextID = 67699563,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=503, Top=281, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_DOMA", cZoomOnZoom="GSMA_MCD", oKey_1_1="CSCAUGRU", oKey_1_2="this.w_RICODGRU"

  func oRICODGRU_1_63.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_63('Part',this)
    endwith
    return bRes
  endfunc

  proc oRICODGRU_1_63.ecpDrop(oSource)
    this.Parent.oContained.link_1_63('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRICODGRU_1_63.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_DOMA','*','CSCAUGRU',cp_AbsName(this.parent,'oRICODGRU_1_63'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_MCD',"GRUPPO CAUSALI",'',this.parent.oContained
  endproc
  proc oRICODGRU_1_63.mZoomOnZoom
    local i_obj
    i_obj=GSMA_MCD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CSCAUGRU=this.parent.oContained.w_RICODGRU
     i_obj.ecpSave()
  endproc

  add object oDESGRU_1_64 as StdField with uid="IDDOEMRCCX",rtseq=44,rtrep=.f.,;
    cFormVar = "w_DESGRU", cQueryName = "DESGRU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 48365110,;
   bGlobalFont=.t.,;
    Height=21, Width=218, Left=569, Top=281, InputMask=replicate('X',40)


  add object oBtn_1_65 as StdButton with uid="PYPQPVDIGP",left=745, top=443, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per lanciare l'elaborazione";
    , HelpContextID = 20109447;
    , caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_65.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oRICOSORD_1_68 as StdField with uid="PZGRSUSHOV",rtseq=45,rtrep=.f.,;
    cFormVar = "w_RICOSORD", cQueryName = "RICOSORD",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Costo medio unitario di un ordine (espresso in valuta di conto)",;
    HelpContextID = 217645914,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=636, Top=340, cSayPict="v_GU(20)", cGetPict="v_GU(20)"

  func oRICOSORD_1_68.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
   endif
  endfunc

  func oRICOSORD_1_68.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_RICOSORD >0)
    endwith
    return bRes
  endfunc

  add object oRICOSBEN_1_69 as StdField with uid="QDSXKCHOEP",rtseq=46,rtrep=.f.,;
    cFormVar = "w_RICOSBEN", cQueryName = "RICOSBEN",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Costo del bene (espresso in valuta di conto)",;
    HelpContextID = 457884,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=636, Top=365, cSayPict="v_GU(20)", cGetPict="v_GU(20)"

  func oRICOSBEN_1_69.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
   endif
  endfunc

  func oRICOSBEN_1_69.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_RICOSBEN >0)
    endwith
    return bRes
  endfunc

  add object oRIPERINT_1_70 as StdField with uid="BKDQPAVDBS",rtseq=47,rtrep=.f.,;
    cFormVar = "w_RIPERINT", cQueryName = "RIPERINT",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Interesse sul capitale immobilizzato",;
    HelpContextID = 115331946,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=636, Top=390, cSayPict='"999.99"', cGetPict='"999.99"'

  func oRIPERINT_1_70.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
   endif
  endfunc

  func oRIPERINT_1_70.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_RIPERINT > 0 and .w_RIPERINT <= 100)
    endwith
    return bRes
  endfunc

  add object oRIPERIZA_1_71 as StdField with uid="KVHCTXYVWQ",rtseq=48,rtrep=.f.,;
    cFormVar = "w_RIPERIZA", cQueryName = "RIPERIZA",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "incidenza del costo di gestione del magazzino",;
    HelpContextID = 153103529,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=636, Top=414, cSayPict='"999.99"', cGetPict='"999.99"'

  func oRIPERIZA_1_71.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
   endif
  endfunc

  func oRIPERIZA_1_71.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_RIPERIZA > 0 and .w_RIPERIZA <= 100)
    endwith
    return bRes
  endfunc


  add object oRIPROVEN_1_83 as StdCombo with uid="CUTJXHGOFX",rtseq=49,rtrep=.f.,left=114,top=359,width=152,height=21;
    , HelpContextID = 205728924;
    , cFormVar="w_RIPROVEN",RowSource=""+"Tutti,"+"Solo interna,"+"Solo esterna,"+"Conto lavoro", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oRIPROVEN_1_83.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'I',;
    iif(this.value =3,'E',;
    iif(this.value =4,'L',;
    space(1))))))
  endfunc
  func oRIPROVEN_1_83.GetRadio()
    this.Parent.oContained.w_RIPROVEN = this.RadioValue()
    return .t.
  endfunc

  func oRIPROVEN_1_83.SetRadio()
    this.Parent.oContained.w_RIPROVEN=trim(this.Parent.oContained.w_RIPROVEN)
    this.value = ;
      iif(this.Parent.oContained.w_RIPROVEN=='T',1,;
      iif(this.Parent.oContained.w_RIPROVEN=='I',2,;
      iif(this.Parent.oContained.w_RIPROVEN=='E',3,;
      iif(this.Parent.oContained.w_RIPROVEN=='L',4,;
      0))))
  endfunc

  add object oStr_1_9 as StdString with uid="XSBKWUHRMT",Visible=.t., Left=22, Top=23,;
    Alignment=1, Width=71, Height=15,;
    Caption="Numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="KACJSAGYFY",Visible=.t., Left=505, Top=23,;
    Alignment=1, Width=66, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="QJVDSJOBJY",Visible=.t., Left=657, Top=23,;
    Alignment=1, Width=63, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="EKENVBQRMC",Visible=.t., Left=360, Top=23,;
    Alignment=1, Width=71, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="MPUSKLZCNH",Visible=.t., Left=3, Top=51,;
    Alignment=1, Width=90, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="SRRCMUTRGQ",Visible=.t., Left=26, Top=99,;
    Alignment=0, Width=85, Height=18,;
    Caption="Codice articolo"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="IYPAPGBIBM",Visible=.t., Left=14, Top=128,;
    Alignment=1, Width=97, Height=18,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="URWPAUFIUW",Visible=.t., Left=14, Top=152,;
    Alignment=1, Width=97, Height=18,;
    Caption="A codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="ABDRUJYZBN",Visible=.t., Left=14, Top=176,;
    Alignment=1, Width=97, Height=18,;
    Caption="Da famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="BUWXVLDVUG",Visible=.t., Left=405, Top=176,;
    Alignment=1, Width=95, Height=18,;
    Caption="A famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="HSXSQARAJB",Visible=.t., Left=14, Top=201,;
    Alignment=1, Width=97, Height=18,;
    Caption="Da gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="JFUFHMCPMW",Visible=.t., Left=405, Top=201,;
    Alignment=1, Width=95, Height=18,;
    Caption="A gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="WSEIFWOPOD",Visible=.t., Left=14, Top=225,;
    Alignment=1, Width=97, Height=18,;
    Caption="Da cat. omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="ARLORWNBII",Visible=.t., Left=405, Top=225,;
    Alignment=1, Width=95, Height=18,;
    Caption="A cat. omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="LQUUQVVYSR",Visible=.t., Left=12, Top=280,;
    Alignment=1, Width=99, Height=18,;
    Caption="Da magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="FMWOCAGXHM",Visible=.t., Left=12, Top=305,;
    Alignment=1, Width=99, Height=18,;
    Caption="A magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_57 as StdString with uid="DBOUELFUDI",Visible=.t., Left=26, Top=254,;
    Alignment=0, Width=115, Height=18,;
    Caption="Altre selezioni"  ;
  , bGlobalFont=.t.

  add object oStr_1_62 as StdString with uid="XJBFXSXTLW",Visible=.t., Left=405, Top=281,;
    Alignment=1, Width=95, Height=18,;
    Caption="Gr.causali.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_72 as StdString with uid="BULYPMWQFO",Visible=.t., Left=491, Top=340,;
    Alignment=1, Width=141, Height=15,;
    Caption="Costo unitario ordine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_73 as StdString with uid="LKKHMBWIPV",Visible=.t., Left=517, Top=365,;
    Alignment=1, Width=115, Height=15,;
    Caption="Costo del bene:"  ;
  , bGlobalFont=.t.

  add object oStr_1_74 as StdString with uid="TYWFXTJVNZ",Visible=.t., Left=692, Top=392,;
    Alignment=0, Width=15, Height=15,;
    Caption="%"  ;
  , bGlobalFont=.t.

  add object oStr_1_75 as StdString with uid="ADXFOMSLRW",Visible=.t., Left=693, Top=415,;
    Alignment=0, Width=15, Height=15,;
    Caption="%"  ;
  , bGlobalFont=.t.

  add object oStr_1_76 as StdString with uid="LZVWIRTTZD",Visible=.t., Left=502, Top=391,;
    Alignment=1, Width=130, Height=15,;
    Caption="Interesse sul capitale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_77 as StdString with uid="JPREIVEOQO",Visible=.t., Left=481, Top=416,;
    Alignment=1, Width=151, Height=15,;
    Caption="Incidenza del magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_78 as StdString with uid="GALZROPHBO",Visible=.t., Left=475, Top=318,;
    Alignment=0, Width=287, Height=15,;
    Caption="Parametri per il calcolo del lotto di riordino"  ;
  , bGlobalFont=.t.

  add object oStr_1_81 as StdString with uid="PLTTUMZNDF",Visible=.t., Left=185, Top=79,;
    Alignment=1, Width=144, Height=15,;
    Caption="Coefficiente di correzione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_82 as StdString with uid="ZLCISZBILY",Visible=.t., Left=401, Top=80,;
    Alignment=0, Width=338, Height=15,;
    Caption="Valore calcolato=val.calcol.+ (val.calcol.X coeff. di correz.)"  ;
  , bGlobalFont=.t.

  add object oStr_1_84 as StdString with uid="CPBPMSZTJP",Visible=.t., Left=5, Top=360,;
    Alignment=1, Width=106, Height=17,;
    Caption="Provenienza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_85 as StdString with uid="UWCKJHVKFY",Visible=.t., Left=394, Top=252,;
    Alignment=1, Width=106, Height=18,;
    Caption="Tipo gestione:"  ;
  , bGlobalFont=.t.

  add object oBox_1_66 as StdBox with uid="XKDZJNUGTB",left=24, top=120, width=769,height=3

  add object oBox_1_67 as StdBox with uid="GASQFICHYH",left=24, top=273, width=769,height=3

  add object oBox_1_79 as StdBox with uid="YYOSVUUVAD",left=476, top=335, width=297,height=1

  add object oBox_1_80 as StdBox with uid="IEFCNYDWJF",left=468, top=311, width=325,height=131
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_ari','RIORDINO','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".RINUMRIO=RIORDINO.RINUMRIO";
  +" and "+i_cAliasName2+".RICODESE=RIORDINO.RICODESE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
