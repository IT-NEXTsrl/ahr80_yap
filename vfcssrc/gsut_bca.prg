* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bca                                                        *
*              Creazione azienda                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_195]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1997-07-30                                                      *
* Last revis.: 2015-03-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bca",oParentObject)
return(i_retval)

define class tgsut_bca as StdBatch
  * --- Local variables
  w_APPOAZI = space(5)
  w_ESCODESE = space(4)
  w_ESINIESE = ctod("  /  /  ")
  w_ESFINESE = ctod("  /  /  ")
  w_ESVALNAZ = space(3)
  w_ESCONCON = ctod("  /  /  ")
  w_ESCONTES = ctod("  /  /  ")
  w_ESCONVEN = ctod("  /  /  ")
  w_ESCONACQ = ctod("  /  /  ")
  w_ESCONMAG = ctod("  /  /  ")
  w_ESCONANA = ctod("  /  /  ")
  w_ESSTAINT = ctod("  /  /  ")
  w_ESDARLIG = 0
  w_ESAVELIG = 0
  w_ESPROLIG = 0
  w_ESFLSTAM = space(1)
  w_C65 = space(5)
  w_C66 = space(5)
  w_CALPRO = space(2)
  w_MOCODICE = space(10)
  w_AZI = space(5)
  w_DISERIAL = space(10)
  w_DINUMDIC = 0
  w_DINUMDOC = 0
  w_DI__ANNO = space(4)
  w_DITIPCON = space(1)
  w_DITIPIVA = space(1)
  w_DISERDIC = space(3)
  w_CURAZI = space(5)
  w_APPO = space(10)
  w_C1 = space(1)
  w_C16 = space(5)
  w_C31 = space(5)
  w_MAXLIV = 0
  w_C2 = space(1)
  w_C17 = space(5)
  w_C32 = space(5)
  w_CODEUR = space(3)
  w_C3 = space(1)
  w_C18 = space(5)
  w_C33 = space(5)
  w_CODLIR = space(3)
  w_C4 = space(1)
  w_C19 = space(5)
  w_C34 = space(5)
  w_OK = .f.
  w_C5 = space(5)
  w_C20 = space(5)
  w_C35 = space(5)
  w_OAZI = space(5)
  w_C6 = space(5)
  w_C21 = space(5)
  w_C36 = space(5)
  w_OUTE = 0
  w_C7 = space(5)
  w_C22 = 0
  w_C37 = space(5)
  w_TIPCON = space(1)
  w_C8 = space(5)
  w_C23 = 0
  w_C38 = space(5)
  w_CODICE = 0
  w_C9 = space(5)
  w_C24 = space(5)
  w_C39 = space(5)
  VABENE = .f.
  w_C10 = space(5)
  w_C25 = space(5)
  w_C40 = space(5)
  OLDAZI = space(3)
  w_C11 = space(5)
  w_C26 = space(5)
  w_C41 = space(1)
  CARCOD = space(1)
  w_C12 = space(5)
  w_C27 = space(5)
  w_C42 = space(1)
  w_C13 = space(5)
  w_C28 = space(5)
  w_C43 = space(1)
  w_C14 = space(5)
  w_C29 = space(5)
  w_C44 = space(5)
  w_C15 = space(5)
  w_C30 = space(5)
  w_C45 = space(15)
  w_C46 = space(15)
  w_C47 = space(15)
  w_C48 = space(15)
  w_C49 = space(5)
  w_C50 = space(5)
  w_C51 = space(5)
  w_C52 = space(15)
  w_C53 = space(15)
  w_C54 = space(15)
  w_C55 = space(15)
  w_C56 = space(15)
  w_C57 = space(15)
  w_C58 = space(15)
  w_C59 = space(15)
  w_C60 = space(15)
  w_C61 = space(15)
  w_C62 = space(15)
  w_C63 = space(15)
  w_C64 = space(15)
  * --- WorkFile variables
  AZIENDA_idx=0
  ESERCIZI_idx=0
  CONTROPA_idx=0
  UTE_AZI_idx=0
  PAR_PROV_idx=0
  MAGAZZIN_idx=0
  COC_MAST_idx=0
  TIP_ALLE_idx=0
  DIC_INTE_idx=0
  PROMODEL_idx=0
  AZBACKUP_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Crea una Nuova Azienda (Lanciato da GSUT_KCA)
    * --- cosa devo copiare in questo Batch
    * --- gli archivi
    * --- Attivit� e servizi
    * --- gli archivi di alterego
    this.Page_4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- controllo che non ci siano carattere -pericolosi- nel codice azienda
    * --- Il primo carattere non deve essere una cifra
    this.w_CODICE = 1
    this.VABENE = .t.
    this.oParentObject.w_CODAZI = alltrim(this.oParentObject.w_codazi)
    if VAL(left(this.oParentObject.w_CODAZI,1))>0 OR left(this.oParentObject.w_CODAZI,1)="0"
      ah_ErrorMsg("La prima lettera del codice azienda non deve essere numerica",,"")
      i_retcode = 'stop'
      return
    else
      FOR L_i = 1 TO LEN(this.oParentObject.w_CODAZI)
      * --- Controllo Caratteri 'Strani' o spazi
      if SUBSTR(this.oParentObject.w_CODAZI, L_i, 1) $ '&.$?*+-/\<>=|\!"�%/()^���[]��@�#���;:,�' Or Empty(SUBSTR(this.oParentObject.w_CODAZI, L_i, 1))
        ah_ErrorMsg("Il codice azienda contiene caratteri non consentiti (&.$? *+-\/<>=) o spazi",,"")
        i_retcode = 'stop'
        return
      endif
      ENDFOR
    endif
    * --- Problema record cancellati
    this.oParentObject.w_CODAZI = LEFT( this.oParentObject.w_CODAZI+SPACE(5),5)
    * --- LA DITTA XXX O xxx non pu� essere creata
    this.w_OK = UPPER(this.oParentObject.W_CODAZI)="XXX"
    do while this.w_codice<len( this.oParentObject.w_codazi )+1 and ( NOT this.w_ok )
      this.CARCOD = substr(this.oParentObject.w_CODAZI,this.w_CODICE,1)
      this.w_OK = between(this.CARCOD,"A","Z") or between(this.CARCOD,"a","z") or between(this.CARCOD,"0","9")
      this.w_CODICE = this.w_CODICE+1
    enddo
    if this.w_OK
      * --- se la maschera passa tutti i check
      if this.oParentObject.checkform()
        * --- creazione DataBaseVuoto
        this.w_OK = CP_CREATEAZI(this.oParentObject.w_CODAZI, this.oParentObject.w_RAGAZI)
        if this.w_ok
          * --- Try
          local bErr_039DE0C0
          bErr_039DE0C0=bTrsErr
          this.Try_039DE0C0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
            * --- cancello fisicamente gli archivi creati
            this.w_OK = CP_DELETEAZI(this.oParentObject.w_CODAZI)
            if I_ERROR="BASE"
              ah_ErrorMsg("IMPOSSIBILE CREARE AZIENDA: %1",,"", this.oParentObject.w_CODAZI )
            else
              ah_ErrorMsg("CREAZIONE AZIENDA %1 NON RIUSCITA",,"", alltrim(this.oParentObject.w_CODAZI) )
            endif
          endif
          bTrsErr=bTrsErr or bErr_039DE0C0
          * --- End
        else
          * --- Abbandona...
          ah_ErrorMsg("IMPOSSIBILE CREARE TABELLE AZIENDA: %1",,"", alltrim(this.oParentObject.w_CODAZI) )
        endif
      endif
    else
      * --- il codice azienda contiene caratteri proibiti dal sistema
      ah_ErrorMsg("IMPOSSIBILE CREARE AZIENDA: %1%0- Il codice azienda pu� contenere solo lettere e numeri",,"", alltrim(this.oParentObject.w_CODAZI) )
    endif
  endproc
  proc Try_039DE0C0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- la ditta non deve esistere altrimenti errore
    this.w_MAXLIV = g_MAXLIV
    * --- Se non copio le valute non copio le valute nei dati azienda
    if this.oParentObject.w_VENDI<>"S" And this.oParentObject.w_STLEGAL<>"S"
      this.w_CODEUR = ""
      this.w_CODLIR = ""
    else
      this.w_CODEUR = g_CODEUR
      this.w_CODLIR = g_CODLIR
    endif
    * --- Insert into AZIENDA
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.AZIENDA_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"AZCODAZI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODAZI),'AZIENDA','AZCODAZI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'AZCODAZI',this.oParentObject.w_CODAZI)
      insert into (i_cTable) (AZCODAZI &i_ccchkf. );
         values (;
           this.oParentObject.w_CODAZI;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Write into AZIENDA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AZRAGAZI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_RAGAZI),'AZIENDA','AZRAGAZI');
      +",AZFLBUNI ="+cp_NullLink(cp_ToStrODBC("N"),'AZIENDA','AZFLBUNI');
      +",AZTIPDEN ="+cp_NullLink(cp_ToStrODBC("M"),'AZIENDA','AZTIPDEN');
      +",AZSTAREG ="+cp_NullLink(cp_ToStrODBC("V"),'AZIENDA','AZSTAREG');
      +",AZNUMSCO ="+cp_NullLink(cp_ToStrODBC(2),'AZIENDA','AZNUMSCO');
      +",AZMAXLIV ="+cp_NullLink(cp_ToStrODBC(this.w_MAXLIV),'AZIENDA','AZMAXLIV');
      +",AZVALEUR ="+cp_NullLink(cp_ToStrODBC(this.w_CODEUR),'AZIENDA','AZVALEUR');
      +",AZVALLIR ="+cp_NullLink(cp_ToStrODBC(this.w_CODLIR),'AZIENDA','AZVALLIR');
      +",AZPERAZI ="+cp_NullLink(cp_ToStrODBC("N"),'AZIENDA','AZPERAZI');
      +",AZTRAEXP ="+cp_NullLink(cp_ToStrODBC("N"),'AZIENDA','AZTRAEXP');
      +",AZUNIUTE ="+cp_NullLink(cp_ToStrODBC(IIF(IsAlt(), "S", " ")),'AZIENDA','AZUNIUTE');
      +",AZQUADRA ="+cp_NullLink(cp_ToStrODBC(IIF(IsAlt(), "S", " ")),'AZIENDA','AZQUADRA');
      +",AZFLUNIV ="+cp_NullLink(cp_ToStrODBC(IIF(IsAlt(), "S", " ")),'AZIENDA','AZFLUNIV');
      +",AZPERPAR ="+cp_NullLink(cp_ToStrODBC(IIF(IsAlt(), "S", " ")),'AZIENDA','AZPERPAR');
      +",AZCONCAS ="+cp_NullLink(cp_ToStrODBC(IIF(IsAlt(), "S", " ")),'AZIENDA','AZCONCAS');
      +",AZDETCON ="+cp_NullLink(cp_ToStrODBC(IIF(IsAlt(), "S", " ")),'AZIENDA','AZDETCON');
      +",AZCFNUME ="+cp_NullLink(cp_ToStrODBC(IIF(IsAlt(), "S", " ")),'AZIENDA','AZCFNUME');
      +",AZPERPQT ="+cp_NullLink(cp_ToStrODBC(IIF(IsAlt(), 2, 0)),'AZIENDA','AZPERPQT');
          +i_ccchkf ;
      +" where ";
          +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      update (i_cTable) set;
          AZRAGAZI = this.oParentObject.w_RAGAZI;
          ,AZFLBUNI = "N";
          ,AZTIPDEN = "M";
          ,AZSTAREG = "V";
          ,AZNUMSCO = 2;
          ,AZMAXLIV = this.w_MAXLIV;
          ,AZVALEUR = this.w_CODEUR;
          ,AZVALLIR = this.w_CODLIR;
          ,AZPERAZI = "N";
          ,AZTRAEXP = "N";
          ,AZUNIUTE = IIF(IsAlt(), "S", " ");
          ,AZQUADRA = IIF(IsAlt(), "S", " ");
          ,AZFLUNIV = IIF(IsAlt(), "S", " ");
          ,AZPERPAR = IIF(IsAlt(), "S", " ");
          ,AZCONCAS = IIF(IsAlt(), "S", " ");
          ,AZDETCON = IIF(IsAlt(), "S", " ");
          ,AZCFNUME = IIF(IsAlt(), "S", " ");
          ,AZPERPQT = IIF(IsAlt(), 2, 0);
          &i_ccchkf. ;
       where;
          AZCODAZI = this.oParentObject.w_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Insert into AZBACKUP
    i_nConn=i_TableProp[this.AZBACKUP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZBACKUP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.AZBACKUP_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"AZCODAZI"+",AZLUNBCK"+",AZMARBCK"+",AZMERBCK"+",AZGIOBCK"+",AZVENBCK"+",AZORABCK"+",AZMINBCK"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODAZI),'AZBACKUP','AZCODAZI');
      +","+cp_NullLink(cp_ToStrODBC("S"),'AZBACKUP','AZLUNBCK');
      +","+cp_NullLink(cp_ToStrODBC("S"),'AZBACKUP','AZMARBCK');
      +","+cp_NullLink(cp_ToStrODBC("S"),'AZBACKUP','AZMERBCK');
      +","+cp_NullLink(cp_ToStrODBC("S"),'AZBACKUP','AZGIOBCK');
      +","+cp_NullLink(cp_ToStrODBC("S"),'AZBACKUP','AZVENBCK');
      +","+cp_NullLink(cp_ToStrODBC(15),'AZBACKUP','AZORABCK');
      +","+cp_NullLink(cp_ToStrODBC(0),'AZBACKUP','AZMINBCK');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'AZCODAZI',this.oParentObject.w_CODAZI,'AZLUNBCK',"S",'AZMARBCK',"S",'AZMERBCK',"S",'AZGIOBCK',"S",'AZVENBCK',"S",'AZORABCK',15,'AZMINBCK',0)
      insert into (i_cTable) (AZCODAZI,AZLUNBCK,AZMARBCK,AZMERBCK,AZGIOBCK,AZVENBCK,AZORABCK,AZMINBCK &i_ccchkf. );
         values (;
           this.oParentObject.w_CODAZI;
           ,"S";
           ,"S";
           ,"S";
           ,"S";
           ,"S";
           ,15;
           ,0;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    if IsAlt()
      * --- Try
      local bErr_039B2CB0
      bErr_039B2CB0=bTrsErr
      this.Try_039B2CB0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        ah_ErrorMsg("Impossibile registrare la nazione e la lingua nazionale nei dati studio: inserire manualmente",,"")
      endif
      bTrsErr=bTrsErr or bErr_039B2CB0
      * --- End
    endif
    if IsAlt()
      * --- Duplicazione esercizi
      this.w_APPOAZI = i_CODAZI
      * --- Select from ESERCIZI
      i_nConn=i_TableProp[this.ESERCIZI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select *  from "+i_cTable+" ESERCIZI ";
            +" where ESCODAZI="+cp_ToStrODBC(this.w_APPOAZI)+"";
             ,"_Curs_ESERCIZI")
      else
        select * from (i_cTable);
         where ESCODAZI=this.w_APPOAZI;
          into cursor _Curs_ESERCIZI
      endif
      if used('_Curs_ESERCIZI')
        select _Curs_ESERCIZI
        locate for 1=1
        do while not(eof())
        this.w_ESCODESE = _Curs_ESERCIZI.ESCODESE
        this.w_ESINIESE = _Curs_ESERCIZI.ESINIESE
        this.w_ESFINESE = _Curs_ESERCIZI.ESFINESE
        this.w_ESVALNAZ = _Curs_ESERCIZI.ESVALNAZ
        this.w_ESCONCON = _Curs_ESERCIZI.ESCONCON
        this.w_ESCONTES = _Curs_ESERCIZI.ESCONTES
        this.w_ESCONVEN = _Curs_ESERCIZI.ESCONVEN
        this.w_ESCONACQ = _Curs_ESERCIZI.ESCONACQ
        this.w_ESCONMAG = _Curs_ESERCIZI.ESCONMAG
        this.w_ESCONANA = _Curs_ESERCIZI.ESCONANA
        this.w_ESSTAINT = _Curs_ESERCIZI.ESSTAINT
        this.w_ESDARLIG = _Curs_ESERCIZI.ESDARLIG
        this.w_ESAVELIG = _Curs_ESERCIZI.ESAVELIG
        this.w_ESPROLIG = _Curs_ESERCIZI.ESPROLIG
        this.w_ESFLSTAM = _Curs_ESERCIZI.ESFLSTAM
        * --- Insert into ESERCIZI
        i_nConn=i_TableProp[this.ESERCIZI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ESERCIZI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"ESCODAZI"+",ESCODESE"+",ESINIESE"+",ESFINESE"+",ESVALNAZ"+",ESCONCON"+",ESCONTES"+",ESCONVEN"+",ESCONACQ"+",ESCONMAG"+",ESCONANA"+",ESSTAINT"+",ESDARLIG"+",ESAVELIG"+",ESPROLIG"+",ESFLSTAM"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODAZI),'ESERCIZI','ESCODAZI');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ESCODESE),'ESERCIZI','ESCODESE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ESINIESE),'ESERCIZI','ESINIESE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ESFINESE),'ESERCIZI','ESFINESE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ESVALNAZ),'ESERCIZI','ESVALNAZ');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ESCONCON),'ESERCIZI','ESCONCON');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ESCONTES),'ESERCIZI','ESCONTES');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ESCONVEN),'ESERCIZI','ESCONVEN');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ESCONACQ),'ESERCIZI','ESCONACQ');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ESCONMAG),'ESERCIZI','ESCONMAG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ESCONANA),'ESERCIZI','ESCONANA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ESSTAINT),'ESERCIZI','ESSTAINT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ESDARLIG),'ESERCIZI','ESDARLIG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ESAVELIG),'ESERCIZI','ESAVELIG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ESPROLIG),'ESERCIZI','ESPROLIG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ESFLSTAM),'ESERCIZI','ESFLSTAM');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'ESCODAZI',this.oParentObject.w_CODAZI,'ESCODESE',this.w_ESCODESE,'ESINIESE',this.w_ESINIESE,'ESFINESE',this.w_ESFINESE,'ESVALNAZ',this.w_ESVALNAZ,'ESCONCON',this.w_ESCONCON,'ESCONTES',this.w_ESCONTES,'ESCONVEN',this.w_ESCONVEN,'ESCONACQ',this.w_ESCONACQ,'ESCONMAG',this.w_ESCONMAG,'ESCONANA',this.w_ESCONANA,'ESSTAINT',this.w_ESSTAINT)
          insert into (i_cTable) (ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ,ESCONCON,ESCONTES,ESCONVEN,ESCONACQ,ESCONMAG,ESCONANA,ESSTAINT,ESDARLIG,ESAVELIG,ESPROLIG,ESFLSTAM &i_ccchkf. );
             values (;
               this.oParentObject.w_CODAZI;
               ,this.w_ESCODESE;
               ,this.w_ESINIESE;
               ,this.w_ESFINESE;
               ,this.w_ESVALNAZ;
               ,this.w_ESCONCON;
               ,this.w_ESCONTES;
               ,this.w_ESCONVEN;
               ,this.w_ESCONACQ;
               ,this.w_ESCONMAG;
               ,this.w_ESCONANA;
               ,this.w_ESSTAINT;
               ,this.w_ESDARLIG;
               ,this.w_ESAVELIG;
               ,this.w_ESPROLIG;
               ,this.w_ESFLSTAM;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
          select _Curs_ESERCIZI
          continue
        enddo
        use
      endif
    else
      * --- Insert into ESERCIZI
      i_nConn=i_TableProp[this.ESERCIZI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ESERCIZI_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"ESCODAZI"+",ESCODESE"+",ESINIESE"+",ESFINESE"+",ESVALNAZ"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODAZI),'ESERCIZI','ESCODAZI');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODESE),'ESERCIZI','ESCODESE');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_INIESE),'ESERCIZI','ESINIESE');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FINESE),'ESERCIZI','ESFINESE');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_VALESE),'ESERCIZI','ESVALNAZ');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'ESCODAZI',this.oParentObject.w_CODAZI,'ESCODESE',this.oParentObject.w_CODESE,'ESINIESE',this.oParentObject.w_INIESE,'ESFINESE',this.oParentObject.w_FINESE,'ESVALNAZ',this.oParentObject.w_VALESE)
        insert into (i_cTable) (ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ &i_ccchkf. );
           values (;
             this.oParentObject.w_CODAZI;
             ,this.oParentObject.w_CODESE;
             ,this.oParentObject.w_INIESE;
             ,this.oParentObject.w_FINESE;
             ,this.oParentObject.w_VALESE;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    ah_Msg("Azienda: %1 generata - inizio copia dati",.T.,.F.,.F., this.oParentObject.w_CODAZI )
    this.VABENE = .t.
    this.w_OAZI = i_CODAZI
    * --- Select from UTE_AZI
    i_nConn=i_TableProp[this.UTE_AZI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.UTE_AZI_idx,2],.t.,this.UTE_AZI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" UTE_AZI ";
          +" where UACODAZI="+cp_ToStrODBC(this.w_OAZI)+"";
           ,"_Curs_UTE_AZI")
    else
      select * from (i_cTable);
       where UACODAZI=this.w_OAZI;
        into cursor _Curs_UTE_AZI
    endif
    if used('_Curs_UTE_AZI')
      select _Curs_UTE_AZI
      locate for 1=1
      do while not(eof())
      this.w_OUTE = _Curs_UTE_AZI.UACODUTE
      if NVL(this.w_OUTE, 0)<>0
        if this.oParentObject.w_FLABUT="T" OR this.w_OUTE=1
          * --- Try
          local bErr_039B7570
          bErr_039B7570=bTrsErr
          this.Try_039B7570()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_039B7570
          * --- End
        endif
      endif
        select _Curs_UTE_AZI
        continue
      enddo
      use
    endif
    * --- Tabella Conversioni
    if this.oParentObject.w_CONVE="S"
      this.VABENE = TRASFARC(i_CODAZI, this.oParentObject.w_codazi, "CONVERSI")
    endif
    * --- archivi di base
    if this.VABENE
      if this.oParentObject.w_conta="S" OR this.oParentObject.w_MAGA="S" OR this.oParentObject.w_VENDI="S" OR this.oParentObject.w_SPEDI="S" OR this.oParentObject.w_ANAL="S" OR this.oParentObject.w_IMPO="S" OR this.oParentObject.w_CESPI="S" OR this.oParentObject.w_STATIS="S" OR this.oParentObject.w_ANABIL="S" OR this.oParentObject.w_STLEGAL="S"
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      if this.VABENE AND ISALT()
        this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"PAR_ALTE", "PACODAZI", i_codazi, "PACODAZI"),.F.)
      endif
      * --- non tutti perch� chi manca dipende da chi � nell'IF
      if this.VABENE
        this.w_APPO = this.oParentObject.w_mastri+this.oParentObject.w_pagame+this.oParentObject.w_agenti+this.oParentObject.w_listini+this.oParentObject.w_nomecla+this.oParentObject.w_cambi+this.oParentObject.w_caumaga+this.oParentObject.w_anal
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.oParentObject.w_CONTA="S" AND this.oParentObject.w_CAUCONTA="S" AND this.oParentObject.w_PAGAME="S" AND this.oParentObject.w_CONTI="S" and this.VABENE
          * --- Aggiorna le Contropartite
          this.w_C1 = " "
          this.w_C11 = " "
          this.w_C21 = " "
          this.w_C31 = " "
          this.w_C41 = " "
          this.w_C2 = " "
          this.w_C12 = " "
          this.w_C22 = 0
          this.w_C32 = " "
          this.w_C42 = " "
          this.w_C3 = " "
          this.w_C13 = " "
          this.w_C23 = 0
          this.w_C33 = " "
          this.w_C43 = " "
          this.w_C4 = " "
          this.w_C14 = " "
          this.w_C24 = " "
          this.w_C34 = " "
          this.w_C44 = " "
          this.w_C5 = " "
          this.w_C15 = " "
          this.w_C25 = " "
          this.w_C35 = " "
          this.w_C6 = " "
          this.w_C16 = " "
          this.w_C26 = " "
          this.w_C36 = " "
          this.w_C7 = " "
          this.w_C17 = " "
          this.w_C27 = " "
          this.w_C37 = " "
          this.w_C8 = " "
          this.w_C18 = " "
          this.w_C28 = " "
          this.w_C38 = " "
          this.w_C9 = " "
          this.w_C19 = " "
          this.w_C29 = " "
          this.w_C39 = " "
          this.w_C10 = " "
          this.w_C20 = " "
          this.w_C30 = " "
          this.w_C40 = " "
          this.w_C45 = " "
          this.w_C46 = " "
          this.w_C47 = " "
          this.w_C48 = " "
          this.w_C49 = " "
          this.w_C50 = " "
          this.w_C51 = " "
          this.w_C52 = " "
          this.w_C53 = " "
          this.w_C54 = " "
          this.w_C55 = " "
          this.w_C56 = " "
          this.w_C57 = " "
          this.w_C58 = " "
          this.w_C59 = " "
          this.w_C60 = " "
          this.w_C61 = " "
          this.w_C62 = " "
          this.w_C63 = " "
          this.w_C64 = " "
          this.w_C65 = " "
          this.w_C66 = " "
          * --- Read from CONTROPA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTROPA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2],.t.,this.CONTROPA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "*"+;
              " from "+i_cTable+" CONTROPA where ";
                  +"COCODAZI = "+cp_ToStrODBC(i_CODAZI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              *;
              from (i_cTable) where;
                  COCODAZI = i_CODAZI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_C1 = NVL(cp_ToDate(_read_.COTIPCON),cp_NullValue(_read_.COTIPCON))
            this.w_C2 = NVL(cp_ToDate(_read_.COCONCLI),cp_NullValue(_read_.COCONCLI))
            this.w_C3 = NVL(cp_ToDate(_read_.COCONINC),cp_NullValue(_read_.COCONINC))
            this.w_C4 = NVL(cp_ToDate(_read_.COCONIMB),cp_NullValue(_read_.COCONIMB))
            this.w_C5 = NVL(cp_ToDate(_read_.COCONTRA),cp_NullValue(_read_.COCONTRA))
            this.w_C6 = NVL(cp_ToDate(_read_.COCONBOL),cp_NullValue(_read_.COCONBOL))
            this.w_C7 = NVL(cp_ToDate(_read_.COCOIBOL),cp_NullValue(_read_.COCOIBOL))
            this.w_C8 = NVL(cp_ToDate(_read_.COCOIINC),cp_NullValue(_read_.COCOIINC))
            this.w_C9 = NVL(cp_ToDate(_read_.COCOIIMB),cp_NullValue(_read_.COCOIIMB))
            this.w_C10 = NVL(cp_ToDate(_read_.COCOITRA),cp_NullValue(_read_.COCOITRA))
            this.w_C11 = NVL(cp_ToDate(_read_.COSAABBA),cp_NullValue(_read_.COSAABBA))
            this.w_C12 = NVL(cp_ToDate(_read_.COSAABBP),cp_NullValue(_read_.COSAABBP))
            this.w_C13 = NVL(cp_ToDate(_read_.COSADIFC),cp_NullValue(_read_.COSADIFC))
            this.w_C14 = NVL(cp_ToDate(_read_.COSADIFN),cp_NullValue(_read_.COSADIFN))
            this.w_C15 = NVL(cp_ToDate(_read_.COSAPAGA),cp_NullValue(_read_.COSAPAGA))
            this.w_C16 = NVL(cp_ToDate(_read_.COCREBRE),cp_NullValue(_read_.COCREBRE))
            this.w_C17 = NVL(cp_ToDate(_read_.COCREMED),cp_NullValue(_read_.COCREMED))
            this.w_C18 = NVL(cp_ToDate(_read_.CODEBBRE),cp_NullValue(_read_.CODEBBRE))
            this.w_C19 = NVL(cp_ToDate(_read_.CODEBMED),cp_NullValue(_read_.CODEBMED))
            this.w_C20 = NVL(cp_ToDate(_read_.COCONBAN),cp_NullValue(_read_.COCONBAN))
            this.w_C21 = NVL(cp_ToDate(_read_.COCONSTR),cp_NullValue(_read_.COCONSTR))
            this.w_C22 = NVL(cp_ToDate(_read_.COSPESOL),cp_NullValue(_read_.COSPESOL))
            this.w_C23 = NVL(cp_ToDate(_read_.COINTSOL),cp_NullValue(_read_.COINTSOL))
            this.w_C24 = NVL(cp_ToDate(_read_.COARTDES),cp_NullValue(_read_.COARTDES))
            this.w_C25 = NVL(cp_ToDate(_read_.CODIFCON),cp_NullValue(_read_.CODIFCON))
            this.w_C26 = NVL(cp_ToDate(_read_.COCONARR),cp_NullValue(_read_.COCONARR))
            this.w_C27 = NVL(cp_ToDate(_read_.COCONRIT),cp_NullValue(_read_.COCONRIT))
            this.w_C28 = NVL(cp_ToDate(_read_.COENASAR),cp_NullValue(_read_.COENASAR))
            this.w_C29 = NVL(cp_ToDate(_read_.COCAURIT),cp_NullValue(_read_.COCAURIT))
            this.w_C30 = NVL(cp_ToDate(_read_.COCONEFA),cp_NullValue(_read_.COCONEFA))
            this.w_C31 = NVL(cp_ToDate(_read_.COCAUEFA),cp_NullValue(_read_.COCAUEFA))
            this.w_C32 = NVL(cp_ToDate(_read_.COCACINC),cp_NullValue(_read_.COCACINC))
            this.w_C33 = NVL(cp_ToDate(_read_.COCACPAG),cp_NullValue(_read_.COCACPAG))
            this.w_C34 = NVL(cp_ToDate(_read_.COCOCINC),cp_NullValue(_read_.COCOCINC))
            this.w_C35 = NVL(cp_ToDate(_read_.COCOCPAG),cp_NullValue(_read_.COCOCPAG))
            this.w_C36 = NVL(cp_ToDate(_read_.COFATEME),cp_NullValue(_read_.COFATEME))
            this.w_C37 = NVL(cp_ToDate(_read_.COFATRIC),cp_NullValue(_read_.COFATRIC))
            this.w_C38 = NVL(cp_ToDate(_read_.CORATATT),cp_NullValue(_read_.CORATATT))
            this.w_C39 = NVL(cp_ToDate(_read_.CORATPAS),cp_NullValue(_read_.CORATPAS))
            this.w_C40 = NVL(cp_ToDate(_read_.CORISATT),cp_NullValue(_read_.CORISATT))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from CONTROPA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTROPA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2],.t.,this.CONTROPA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CORISPAS,COIVADEB,COIVACRE,COIFCIVA,COSCACES,COCONCAF,COCONABF,COCONIVF,COCATCLG,COPAGRIC,COCAUICO,COACQINC,COACQIMB,COACQTRA,COACQBOL,COCORIPR,COGIRRIT,COCOIRVE,COCOPRVE,COCAURIL,COCONTRS,COCAUSLD,COCAUGIR,COEVICRE,COCAUSPL,COCONSPL"+;
              " from "+i_cTable+" CONTROPA where ";
                  +"COCODAZI = "+cp_ToStrODBC(i_CODAZI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CORISPAS,COIVADEB,COIVACRE,COIFCIVA,COSCACES,COCONCAF,COCONABF,COCONIVF,COCATCLG,COPAGRIC,COCAUICO,COACQINC,COACQIMB,COACQTRA,COACQBOL,COCORIPR,COGIRRIT,COCOIRVE,COCOPRVE,COCAURIL,COCONTRS,COCAUSLD,COCAUGIR,COEVICRE,COCAUSPL,COCONSPL;
              from (i_cTable) where;
                  COCODAZI = i_CODAZI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_C41 = NVL(cp_ToDate(_read_.CORISPAS),cp_NullValue(_read_.CORISPAS))
            this.w_C42 = NVL(cp_ToDate(_read_.COIVADEB),cp_NullValue(_read_.COIVADEB))
            this.w_C43 = NVL(cp_ToDate(_read_.COIVACRE),cp_NullValue(_read_.COIVACRE))
            this.w_C44 = NVL(cp_ToDate(_read_.COIFCIVA),cp_NullValue(_read_.COIFCIVA))
            this.w_C45 = NVL(cp_ToDate(_read_.COSCACES),cp_NullValue(_read_.COSCACES))
            this.w_C46 = NVL(cp_ToDate(_read_.COCONCAF),cp_NullValue(_read_.COCONCAF))
            this.w_C47 = NVL(cp_ToDate(_read_.COCONABF),cp_NullValue(_read_.COCONABF))
            this.w_C48 = NVL(cp_ToDate(_read_.COCONIVF),cp_NullValue(_read_.COCONIVF))
            this.w_C49 = NVL(cp_ToDate(_read_.COCATCLG),cp_NullValue(_read_.COCATCLG))
            this.w_C50 = NVL(cp_ToDate(_read_.COPAGRIC),cp_NullValue(_read_.COPAGRIC))
            this.w_C51 = NVL(cp_ToDate(_read_.COCAUICO),cp_NullValue(_read_.COCAUICO))
            this.w_C52 = NVL(cp_ToDate(_read_.COACQINC),cp_NullValue(_read_.COACQINC))
            this.w_C53 = NVL(cp_ToDate(_read_.COACQIMB),cp_NullValue(_read_.COACQIMB))
            this.w_C54 = NVL(cp_ToDate(_read_.COACQTRA),cp_NullValue(_read_.COACQTRA))
            this.w_C55 = NVL(cp_ToDate(_read_.COACQBOL),cp_NullValue(_read_.COACQBOL))
            this.w_C56 = NVL(cp_ToDate(_read_.COCORIPR),cp_NullValue(_read_.COCORIPR))
            this.w_C57 = NVL(cp_ToDate(_read_.COGIRRIT),cp_NullValue(_read_.COGIRRIT))
            this.w_C58 = NVL(cp_ToDate(_read_.COCOIRVE),cp_NullValue(_read_.COCOIRVE))
            this.w_C59 = NVL(cp_ToDate(_read_.COCOPRVE),cp_NullValue(_read_.COCOPRVE))
            this.w_C60 = NVL(cp_ToDate(_read_.COCAURIL),cp_NullValue(_read_.COCAURIL))
            this.w_C61 = NVL(cp_ToDate(_read_.COCONTRS),cp_NullValue(_read_.COCONTRS))
            this.w_C62 = NVL(cp_ToDate(_read_.COCAUSLD),cp_NullValue(_read_.COCAUSLD))
            this.w_C63 = NVL(cp_ToDate(_read_.COCAUGIR),cp_NullValue(_read_.COCAUGIR))
            this.w_C64 = NVL(cp_ToDate(_read_.COEVICRE),cp_NullValue(_read_.COEVICRE))
            this.w_C65 = NVL(cp_ToDate(_read_.COCAUSPL),cp_NullValue(_read_.COCAUSPL))
            this.w_C66 = NVL(cp_ToDate(_read_.COCONSPL),cp_NullValue(_read_.COCONSPL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_C24 = IIF(this.oParentObject.w_ARTICOLI="S",this.w_C24,"")
          * --- Try
          local bErr_039C5AE0
          bErr_039C5AE0=bTrsErr
          this.Try_039C5AE0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_039C5AE0
          * --- End
          * --- Try
          local bErr_039C5960
          bErr_039C5960=bTrsErr
          this.Try_039C5960()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            ah_ErrorMsg("Impossibile registrare le contropartite: inserire manualmente",,"")
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_039C5960
          * --- End
        endif
        if this.VABENE AND this.oParentObject.w_AGENTI="S"
          * --- Parametri Provvigioni
          * --- Read from PAR_PROV
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PAR_PROV_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROV_idx,2],.t.,this.PAR_PROV_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PPPREAZI,PPPREAGE,PPMAXESC,PPMINESC,PPMAXNES,PPMINNES,PPPERASS,PPIMPIN1,PPPERFI1,PPIMPIN2,PPPERFI2,PPPERFI3,PPCODIVA,PPIMPPL1,PPIMPPL2,PPPERPL1,PPPERPL2,PPPERPL3,PPCALPRO"+;
              " from "+i_cTable+" PAR_PROV where ";
                  +"PPCODAZI = "+cp_ToStrODBC(i_CODAZI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PPPREAZI,PPPREAGE,PPMAXESC,PPMINESC,PPMAXNES,PPMINNES,PPPERASS,PPIMPIN1,PPPERFI1,PPIMPIN2,PPPERFI2,PPPERFI3,PPCODIVA,PPIMPPL1,PPIMPPL2,PPPERPL1,PPPERPL2,PPPERPL3,PPCALPRO;
              from (i_cTable) where;
                  PPCODAZI = i_CODAZI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_C1 = NVL(cp_ToDate(_read_.PPPREAZI),cp_NullValue(_read_.PPPREAZI))
            this.w_C2 = NVL(cp_ToDate(_read_.PPPREAGE),cp_NullValue(_read_.PPPREAGE))
            this.w_C3 = NVL(cp_ToDate(_read_.PPMAXESC),cp_NullValue(_read_.PPMAXESC))
            this.w_C4 = NVL(cp_ToDate(_read_.PPMINESC),cp_NullValue(_read_.PPMINESC))
            this.w_C5 = NVL(cp_ToDate(_read_.PPMAXNES),cp_NullValue(_read_.PPMAXNES))
            this.w_C6 = NVL(cp_ToDate(_read_.PPMINNES),cp_NullValue(_read_.PPMINNES))
            this.w_C7 = NVL(cp_ToDate(_read_.PPPERASS),cp_NullValue(_read_.PPPERASS))
            this.w_C8 = NVL(cp_ToDate(_read_.PPIMPIN1),cp_NullValue(_read_.PPIMPIN1))
            this.w_C9 = NVL(cp_ToDate(_read_.PPPERFI1),cp_NullValue(_read_.PPPERFI1))
            this.w_C10 = NVL(cp_ToDate(_read_.PPIMPIN2),cp_NullValue(_read_.PPIMPIN2))
            this.w_C11 = NVL(cp_ToDate(_read_.PPPERFI2),cp_NullValue(_read_.PPPERFI2))
            this.w_C12 = NVL(cp_ToDate(_read_.PPPERFI3),cp_NullValue(_read_.PPPERFI3))
            this.w_C13 = NVL(cp_ToDate(_read_.PPCODIVA),cp_NullValue(_read_.PPCODIVA))
            this.w_C14 = NVL(cp_ToDate(_read_.PPIMPPL1),cp_NullValue(_read_.PPIMPPL1))
            this.w_C15 = NVL(cp_ToDate(_read_.PPIMPPL2),cp_NullValue(_read_.PPIMPPL2))
            this.w_C16 = NVL(cp_ToDate(_read_.PPPERPL1),cp_NullValue(_read_.PPPERPL1))
            this.w_C17 = NVL(cp_ToDate(_read_.PPPERPL2),cp_NullValue(_read_.PPPERPL2))
            this.w_C18 = NVL(cp_ToDate(_read_.PPPERPL3),cp_NullValue(_read_.PPPERPL3))
            this.w_CALPRO = NVL(cp_ToDate(_read_.PPCALPRO),cp_NullValue(_read_.PPCALPRO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_CALPRO = iif(empty(this.w_CALPRO), "DI", this.w_CALPRO)
          if i_Rows<>0
            * --- Try
            local bErr_039D9AD0
            bErr_039D9AD0=bTrsErr
            this.Try_039D9AD0()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_039D9AD0
            * --- End
            * --- Try
            local bErr_039D9080
            bErr_039D9080=bTrsErr
            this.Try_039D9080()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              ah_ErrorMsg("Impossibile registrare i parametri provvigioni: inserire manualmente",,"")
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_039D9080
            * --- End
          else
            * --- Inserisce una riga con il valore di default sul campo Calcolo provvigioni per evitare
            *     errori nel caso in cui si decida successivamente di gestire le provvigioni
            * --- Insert into PAR_PROV
            i_nConn=i_TableProp[this.PAR_PROV_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROV_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_PROV_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"PPCODAZI"+",PPCALPRO"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODAZI),'PAR_PROV','PPCODAZI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CALPRO),'PAR_PROV','PPCALPRO');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'PPCODAZI',this.oParentObject.w_CODAZI,'PPCALPRO',this.w_CALPRO)
              insert into (i_cTable) (PPCODAZI,PPCALPRO &i_ccchkf. );
                 values (;
                   this.oParentObject.w_CODAZI;
                   ,this.w_CALPRO;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
        endif
        * --- COPIA ARCHIVI DI BASE CESPITI
        if g_CESP="S" AND this.oParentObject.w_CESPI="S" AND this.VABENE
          * --- Parametri - Causali - Formule - Modelli Contabili
          do GSCE_BCA with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- COPIA ARCHIVI DISTINTA BASE 
        *     (le tabelle della distinta sono usate anche per Vendite funzioni avanzate)
        if (this.oParentObject.w_DISB="S" Or this.oParentObject.w_VEFA="S" ) AND this.VABENE
          * --- Parametri - Risorse - Coeff.Impiego - Reparti - Par. Variabili
          do GSAR1BAZ with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- COPIA ARCHIVI GESTIONE PROGETTI
        if g_COMM="S" AND this.oParentObject.w_COMM="S" AND this.VABENE
          * --- Famiglie attivit� - Attivita - Attivit� precedenti -  Costi - Relazioni struttura
          do GSPC_BCA with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
    endif
    if NOT this.VABENE
      * --- Raise
      i_Error="BASE"
      return
    else
      this.w_ok = ah_YesNo("Trasferimento riuscito%0-Per annullare premere no%0-Per confermare premere si")
      if this.w_ok
        * --- commit
        cp_EndTrs(.t.)
        this.oParentObject.w_CODAZI = SPACE(5)
        this.oParentObject.w_RAGAZI = SPACE(30)
        this.oParentObject.w_CODESE = SPACE(4)
      else
        * --- Raise
        i_Error="UTENTE"
        return
      endif
    endif
    return
  proc Try_039B2CB0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into AZIENDA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AZCODNAZ ="+cp_NullLink(cp_ToStrODBC("IT"),'AZIENDA','AZCODNAZ');
      +",AZCODLIN ="+cp_NullLink(cp_ToStrODBC("ITA"),'AZIENDA','AZCODLIN');
          +i_ccchkf ;
      +" where ";
          +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      update (i_cTable) set;
          AZCODNAZ = "IT";
          ,AZCODLIN = "ITA";
          &i_ccchkf. ;
       where;
          AZCODAZI = this.oParentObject.w_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_039B7570()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into UTE_AZI
    i_nConn=i_TableProp[this.UTE_AZI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.UTE_AZI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.UTE_AZI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"UACODAZI"+",UACODUTE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODAZI),'UTE_AZI','UACODAZI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OUTE),'UTE_AZI','UACODUTE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'UACODAZI',this.oParentObject.w_CODAZI,'UACODUTE',this.w_OUTE)
      insert into (i_cTable) (UACODAZI,UACODUTE &i_ccchkf. );
         values (;
           this.oParentObject.w_CODAZI;
           ,this.w_OUTE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_039C5AE0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into CONTROPA
    i_nConn=i_TableProp[this.CONTROPA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONTROPA_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"COCODAZI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODAZI),'CONTROPA','COCODAZI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'COCODAZI',this.oParentObject.w_CODAZI)
      insert into (i_cTable) (COCODAZI &i_ccchkf. );
         values (;
           this.oParentObject.w_CODAZI;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_039C5960()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into CONTROPA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CONTROPA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTROPA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"COTIPCON ="+cp_NullLink(cp_ToStrODBC("G"),'CONTROPA','COTIPCON');
      +",COCONCLI ="+cp_NullLink(cp_ToStrODBC(this.w_C2),'CONTROPA','COCONCLI');
      +",COCONINC ="+cp_NullLink(cp_ToStrODBC(this.w_C3),'CONTROPA','COCONINC');
      +",COCONIMB ="+cp_NullLink(cp_ToStrODBC(this.w_C4),'CONTROPA','COCONIMB');
      +",COCONTRA ="+cp_NullLink(cp_ToStrODBC(this.w_C5),'CONTROPA','COCONTRA');
      +",COCONBOL ="+cp_NullLink(cp_ToStrODBC(this.w_C6),'CONTROPA','COCONBOL');
      +",COCOIBOL ="+cp_NullLink(cp_ToStrODBC(this.w_C7),'CONTROPA','COCOIBOL');
      +",COCOIINC ="+cp_NullLink(cp_ToStrODBC(this.w_C8),'CONTROPA','COCOIINC');
      +",COCOIIMB ="+cp_NullLink(cp_ToStrODBC(this.w_C9),'CONTROPA','COCOIIMB');
      +",COCOITRA ="+cp_NullLink(cp_ToStrODBC(this.w_C10),'CONTROPA','COCOITRA');
      +",COSAABBA ="+cp_NullLink(cp_ToStrODBC(this.w_C11),'CONTROPA','COSAABBA');
      +",COSAABBP ="+cp_NullLink(cp_ToStrODBC(this.w_C12),'CONTROPA','COSAABBP');
      +",COSADIFC ="+cp_NullLink(cp_ToStrODBC(this.w_C13),'CONTROPA','COSADIFC');
      +",COSADIFN ="+cp_NullLink(cp_ToStrODBC(this.w_C14),'CONTROPA','COSADIFN');
      +",COSAPAGA ="+cp_NullLink(cp_ToStrODBC(this.w_C15),'CONTROPA','COSAPAGA');
      +",COCREBRE ="+cp_NullLink(cp_ToStrODBC(this.w_C16),'CONTROPA','COCREBRE');
      +",COCREMED ="+cp_NullLink(cp_ToStrODBC(this.w_C17),'CONTROPA','COCREMED');
      +",CODEBBRE ="+cp_NullLink(cp_ToStrODBC(this.w_C18),'CONTROPA','CODEBBRE');
      +",CODEBMED ="+cp_NullLink(cp_ToStrODBC(this.w_C19),'CONTROPA','CODEBMED');
      +",COCONBAN ="+cp_NullLink(cp_ToStrODBC(this.w_C20),'CONTROPA','COCONBAN');
          +i_ccchkf ;
      +" where ";
          +"COCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      update (i_cTable) set;
          COTIPCON = "G";
          ,COCONCLI = this.w_C2;
          ,COCONINC = this.w_C3;
          ,COCONIMB = this.w_C4;
          ,COCONTRA = this.w_C5;
          ,COCONBOL = this.w_C6;
          ,COCOIBOL = this.w_C7;
          ,COCOIINC = this.w_C8;
          ,COCOIIMB = this.w_C9;
          ,COCOITRA = this.w_C10;
          ,COSAABBA = this.w_C11;
          ,COSAABBP = this.w_C12;
          ,COSADIFC = this.w_C13;
          ,COSADIFN = this.w_C14;
          ,COSAPAGA = this.w_C15;
          ,COCREBRE = this.w_C16;
          ,COCREMED = this.w_C17;
          ,CODEBBRE = this.w_C18;
          ,CODEBMED = this.w_C19;
          ,COCONBAN = this.w_C20;
          &i_ccchkf. ;
       where;
          COCODAZI = this.oParentObject.w_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into CONTROPA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CONTROPA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTROPA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"COCONSTR ="+cp_NullLink(cp_ToStrODBC(this.w_C21),'CONTROPA','COCONSTR');
      +",COSPESOL ="+cp_NullLink(cp_ToStrODBC(this.w_C22),'CONTROPA','COSPESOL');
      +",COINTSOL ="+cp_NullLink(cp_ToStrODBC(this.w_C23),'CONTROPA','COINTSOL');
      +",COARTDES ="+cp_NullLink(cp_ToStrODBC(this.w_C24),'CONTROPA','COARTDES');
      +",CODIFCON ="+cp_NullLink(cp_ToStrODBC(this.w_C25),'CONTROPA','CODIFCON');
      +",COCONARR ="+cp_NullLink(cp_ToStrODBC(this.w_C26),'CONTROPA','COCONARR');
      +",COCONRIT ="+cp_NullLink(cp_ToStrODBC(this.w_C27),'CONTROPA','COCONRIT');
      +",COENASAR ="+cp_NullLink(cp_ToStrODBC(this.w_C28),'CONTROPA','COENASAR');
      +",COCAURIT ="+cp_NullLink(cp_ToStrODBC(this.w_C29),'CONTROPA','COCAURIT');
      +",COCONEFA ="+cp_NullLink(cp_ToStrODBC(this.w_C30),'CONTROPA','COCONEFA');
      +",COCAUEFA ="+cp_NullLink(cp_ToStrODBC(this.w_C31),'CONTROPA','COCAUEFA');
      +",COCACINC ="+cp_NullLink(cp_ToStrODBC(this.w_C32),'CONTROPA','COCACINC');
      +",COCACPAG ="+cp_NullLink(cp_ToStrODBC(this.w_C33),'CONTROPA','COCACPAG');
      +",COCOCINC ="+cp_NullLink(cp_ToStrODBC(this.w_C34),'CONTROPA','COCOCINC');
      +",COCOCPAG ="+cp_NullLink(cp_ToStrODBC(this.w_C35),'CONTROPA','COCOCPAG');
      +",COFATEME ="+cp_NullLink(cp_ToStrODBC(this.w_C36),'CONTROPA','COFATEME');
      +",COFATRIC ="+cp_NullLink(cp_ToStrODBC(this.w_C37),'CONTROPA','COFATRIC');
      +",CORATATT ="+cp_NullLink(cp_ToStrODBC(this.w_C38),'CONTROPA','CORATATT');
      +",CORATPAS ="+cp_NullLink(cp_ToStrODBC(this.w_C39),'CONTROPA','CORATPAS');
      +",CORISATT ="+cp_NullLink(cp_ToStrODBC(this.w_C40),'CONTROPA','CORISATT');
          +i_ccchkf ;
      +" where ";
          +"COCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      update (i_cTable) set;
          COCONSTR = this.w_C21;
          ,COSPESOL = this.w_C22;
          ,COINTSOL = this.w_C23;
          ,COARTDES = this.w_C24;
          ,CODIFCON = this.w_C25;
          ,COCONARR = this.w_C26;
          ,COCONRIT = this.w_C27;
          ,COENASAR = this.w_C28;
          ,COCAURIT = this.w_C29;
          ,COCONEFA = this.w_C30;
          ,COCAUEFA = this.w_C31;
          ,COCACINC = this.w_C32;
          ,COCACPAG = this.w_C33;
          ,COCOCINC = this.w_C34;
          ,COCOCPAG = this.w_C35;
          ,COFATEME = this.w_C36;
          ,COFATRIC = this.w_C37;
          ,CORATATT = this.w_C38;
          ,CORATPAS = this.w_C39;
          ,CORISATT = this.w_C40;
          &i_ccchkf. ;
       where;
          COCODAZI = this.oParentObject.w_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into CONTROPA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CONTROPA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTROPA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CORISPAS ="+cp_NullLink(cp_ToStrODBC(this.w_C41),'CONTROPA','CORISPAS');
      +",COIVADEB ="+cp_NullLink(cp_ToStrODBC(this.w_C42),'CONTROPA','COIVADEB');
      +",COIVACRE ="+cp_NullLink(cp_ToStrODBC(this.w_C43),'CONTROPA','COIVACRE');
      +",COIFCIVA ="+cp_NullLink(cp_ToStrODBC(this.w_C44),'CONTROPA','COIFCIVA');
      +",COSCACES ="+cp_NullLink(cp_ToStrODBC(this.w_C45),'CONTROPA','COSCACES');
      +",COCONCAF ="+cp_NullLink(cp_ToStrODBC(this.w_C46),'CONTROPA','COCONCAF');
      +",COCONABF ="+cp_NullLink(cp_ToStrODBC(this.w_C47),'CONTROPA','COCONABF');
      +",COCONIVF ="+cp_NullLink(cp_ToStrODBC(this.w_C48),'CONTROPA','COCONIVF');
      +",COCATCLG ="+cp_NullLink(cp_ToStrODBC(this.w_C49),'CONTROPA','COCATCLG');
      +",COPAGRIC ="+cp_NullLink(cp_ToStrODBC(this.w_C50),'CONTROPA','COPAGRIC');
      +",COCAUICO ="+cp_NullLink(cp_ToStrODBC(this.w_C51),'CONTROPA','COCAUICO');
      +",COACQINC ="+cp_NullLink(cp_ToStrODBC(this.w_C52),'CONTROPA','COACQINC');
      +",COACQIMB ="+cp_NullLink(cp_ToStrODBC(this.w_C53),'CONTROPA','COACQIMB');
      +",COACQTRA ="+cp_NullLink(cp_ToStrODBC(this.w_C54),'CONTROPA','COACQTRA');
      +",COACQBOL ="+cp_NullLink(cp_ToStrODBC(this.w_C55),'CONTROPA','COACQBOL');
      +",COCORIPR ="+cp_NullLink(cp_ToStrODBC(this.w_C56),'CONTROPA','COCORIPR');
      +",COGIRRIT ="+cp_NullLink(cp_ToStrODBC(this.w_C57),'CONTROPA','COGIRRIT');
      +",COCOIRVE ="+cp_NullLink(cp_ToStrODBC(this.w_C58),'CONTROPA','COCOIRVE');
      +",COCOPRVE ="+cp_NullLink(cp_ToStrODBC(this.w_C59),'CONTROPA','COCOPRVE');
      +",COCAURIL ="+cp_NullLink(cp_ToStrODBC(this.w_C60),'CONTROPA','COCAURIL');
      +",COCONTRS ="+cp_NullLink(cp_ToStrODBC(this.w_C61),'CONTROPA','COCONTRS');
      +",COCAUSLD ="+cp_NullLink(cp_ToStrODBC(this.w_C62),'CONTROPA','COCAUSLD');
      +",COCAUGIR ="+cp_NullLink(cp_ToStrODBC(this.w_C63),'CONTROPA','COCAUGIR');
      +",COEVICRE ="+cp_NullLink(cp_ToStrODBC(this.w_C64),'CONTROPA','COEVICRE');
      +",COCAUSPL ="+cp_NullLink(cp_ToStrODBC(this.w_C65),'CONTROPA','COCAUSPL');
      +",COCONSPL ="+cp_NullLink(cp_ToStrODBC(this.w_C66),'CONTROPA','COCONSPL');
          +i_ccchkf ;
      +" where ";
          +"COCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      update (i_cTable) set;
          CORISPAS = this.w_C41;
          ,COIVADEB = this.w_C42;
          ,COIVACRE = this.w_C43;
          ,COIFCIVA = this.w_C44;
          ,COSCACES = this.w_C45;
          ,COCONCAF = this.w_C46;
          ,COCONABF = this.w_C47;
          ,COCONIVF = this.w_C48;
          ,COCATCLG = this.w_C49;
          ,COPAGRIC = this.w_C50;
          ,COCAUICO = this.w_C51;
          ,COACQINC = this.w_C52;
          ,COACQIMB = this.w_C53;
          ,COACQTRA = this.w_C54;
          ,COACQBOL = this.w_C55;
          ,COCORIPR = this.w_C56;
          ,COGIRRIT = this.w_C57;
          ,COCOIRVE = this.w_C58;
          ,COCOPRVE = this.w_C59;
          ,COCAURIL = this.w_C60;
          ,COCONTRS = this.w_C61;
          ,COCAUSLD = this.w_C62;
          ,COCAUGIR = this.w_C63;
          ,COEVICRE = this.w_C64;
          ,COCAUSPL = this.w_C65;
          ,COCONSPL = this.w_C66;
          &i_ccchkf. ;
       where;
          COCODAZI = this.oParentObject.w_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_039D9AD0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PAR_PROV
    i_nConn=i_TableProp[this.PAR_PROV_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROV_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_PROV_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PPCODAZI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODAZI),'PAR_PROV','PPCODAZI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PPCODAZI',this.oParentObject.w_CODAZI)
      insert into (i_cTable) (PPCODAZI &i_ccchkf. );
         values (;
           this.oParentObject.w_CODAZI;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_039D9080()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into PAR_PROV
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_PROV_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROV_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_PROV_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PPPREAZI ="+cp_NullLink(cp_ToStrODBC(this.w_C1),'PAR_PROV','PPPREAZI');
      +",PPPREAGE ="+cp_NullLink(cp_ToStrODBC(this.w_C2),'PAR_PROV','PPPREAGE');
      +",PPMAXESC ="+cp_NullLink(cp_ToStrODBC(this.w_C3),'PAR_PROV','PPMAXESC');
      +",PPMINESC ="+cp_NullLink(cp_ToStrODBC(this.w_C4),'PAR_PROV','PPMINESC');
      +",PPMAXNES ="+cp_NullLink(cp_ToStrODBC(this.w_C5),'PAR_PROV','PPMAXNES');
      +",PPMINNES ="+cp_NullLink(cp_ToStrODBC(this.w_C6),'PAR_PROV','PPMINNES');
      +",PPPERASS ="+cp_NullLink(cp_ToStrODBC(this.w_C7),'PAR_PROV','PPPERASS');
      +",PPIMPIN1 ="+cp_NullLink(cp_ToStrODBC(this.w_C8),'PAR_PROV','PPIMPIN1');
      +",PPPERFI1 ="+cp_NullLink(cp_ToStrODBC(this.w_C9),'PAR_PROV','PPPERFI1');
      +",PPIMPIN2 ="+cp_NullLink(cp_ToStrODBC(this.w_C10),'PAR_PROV','PPIMPIN2');
      +",PPPERFI2 ="+cp_NullLink(cp_ToStrODBC(this.w_C11),'PAR_PROV','PPPERFI2');
      +",PPPERFI3 ="+cp_NullLink(cp_ToStrODBC(this.w_C12),'PAR_PROV','PPPERFI3');
      +",PPCODIVA ="+cp_NullLink(cp_ToStrODBC(this.w_C13),'PAR_PROV','PPCODIVA');
      +",PPIMPPL1 ="+cp_NullLink(cp_ToStrODBC(this.w_C14),'PAR_PROV','PPIMPPL1');
      +",PPIMPPL2 ="+cp_NullLink(cp_ToStrODBC(this.w_C15),'PAR_PROV','PPIMPPL2');
      +",PPPERPL1 ="+cp_NullLink(cp_ToStrODBC(this.w_C16),'PAR_PROV','PPPERPL1');
      +",PPPERPL2 ="+cp_NullLink(cp_ToStrODBC(this.w_C17),'PAR_PROV','PPPERPL2');
      +",PPPERPL3 ="+cp_NullLink(cp_ToStrODBC(this.w_C18),'PAR_PROV','PPPERPL3');
      +",PPCALPRO ="+cp_NullLink(cp_ToStrODBC(this.w_CALPRO),'PAR_PROV','PPCALPRO');
          +i_ccchkf ;
      +" where ";
          +"PPCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      update (i_cTable) set;
          PPPREAZI = this.w_C1;
          ,PPPREAGE = this.w_C2;
          ,PPMAXESC = this.w_C3;
          ,PPMINESC = this.w_C4;
          ,PPMAXNES = this.w_C5;
          ,PPMINNES = this.w_C6;
          ,PPPERASS = this.w_C7;
          ,PPIMPIN1 = this.w_C8;
          ,PPPERFI1 = this.w_C9;
          ,PPIMPIN2 = this.w_C10;
          ,PPPERFI2 = this.w_C11;
          ,PPPERFI3 = this.w_C12;
          ,PPCODIVA = this.w_C13;
          ,PPIMPPL1 = this.w_C14;
          ,PPIMPPL2 = this.w_C15;
          ,PPPERPL1 = this.w_C16;
          ,PPPERPL2 = this.w_C17;
          ,PPPERPL3 = this.w_C18;
          ,PPCALPRO = this.w_CALPRO;
          &i_ccchkf. ;
       where;
          PPCODAZI = this.oParentObject.w_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- COPIA LIBRERIA IMMAGINI
    if this.oParentObject.w_LIB_IMMA="S"
      * --- Se w_STLEGAL='S' allora gli archivi TIP_ALLE, CLA_ALLE, EXT_ENS, PROMCLAS e PRODCLAS devono essere trasferiti
      this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"TIP_ALLE")
      this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CLA_ALLE")
      this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"EXT_ENS")
      this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"PROMCLAS")
      this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"PRODCLAS")
    endif
    * --- COPIA ARCHIVI DI BASE CONTABILITA
    if this.oParentObject.W_CONTA="S" AND this.VABENE
      * --- CODICI IVA-CAT. CONTABILI CLI / FOR - CAT. CONTABILE ARTICOLI
      this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"VOCIIVA")
      this.VABENE = iif(this.vabene,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CACOCLFO"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CACOARTI"),.F.)
    endif
    * --- COPIA ARCHIVI DI BASE Spedizioni \ Intra
    if this.oParentObject.W_SPEDI="S" AND this.VABENE
      * --- Spedizioni - Porti - vettori - Aspetti esteriori - Tipi di Transazione - metodi di calcolo spese trasporto
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"METCALSP"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"MCALSCAI"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"MCALSCAP"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"MCALSCAC"),.F.)
      this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"MODASPED")
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"PORTI"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"VETTORI"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"ASPETTO"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"TIPITRAN"),.F.)
    endif
    * --- COPIA ARCHIVI DI BASE MAGAZZINO
    if this.oParentObject.W_MAGA="S" AND this.VABENE
      * --- Gruppi Merceologici - Ubicazioni - Cat. Omogenee -Tipi confezione-Unit� di Misura - Marchi
      this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"GRUMERC")
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"MAGAZZIN"),.F.)
       
 i_nConn=i_TableProp[this.MAGAZZIN_idx,3] 
 w_iRows = SQLExec(i_nConn,"UPDATE "+ALLTRIM(this.oParentObject.w_CODAZI)+"MAGAZZIN SET MGSTAINT=' ' ") 
 w_iRows = SQLExec(i_nConn,"UPDATE "+ALLTRIM(this.oParentObject.w_CODAZI)+"MAGAZZIN SET MGPRPAGM=0 ") 
 w_iRows = SQLExec(i_nConn,"UPDATE "+ALLTRIM(this.oParentObject.w_CODAZI)+"MAGAZZIN SET MGPREFIS='' ")
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"UBICAZIO"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CATEGOMO"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"TIPICONF"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"UNIMIS"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"MARCHI"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"TIP_MATE"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"STRUTABE"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CLA_RICA"),.F.)
    endif
    * --- COPIA ARCHIVI DI BASE VENDITE / ACQUISTI
    if this.oParentObject.W_VENDI="S" AND this.VABENE
      * --- Cat. Commerciali - Tipi Pagamenti - Gruppi Provigioni - Divise - cambi standard - nazioni -lingue - banche -zone
      this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CATECOMM")
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"MOD_PAGA"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"GRUPRO"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"VALUTE"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"NAZIONI"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"LINGUE"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"BAN_CHE"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"ZONE"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CAT_SCMA"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CLA_RIGD"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"FES_MAST"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"FES_DETT"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"COC_MAST"),.F.)
       
 i_nConn=i_TableProp[this.COC_MAST_idx,3] 
 w_iRows = SQLExec(i_nConn,"UPDATE "+ALLTRIM(this.oParentObject.w_CODAZI)+"COC_MAST SET BASALCRE=0") 
 w_iRows = SQLExec(i_nConn,"UPDATE "+ALLTRIM(this.oParentObject.w_CODAZI)+"COC_MAST SET BASALDEB=0")
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CAU_DIST"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"PAR_PROD"),.F.)
    endif
    * --- COPIA ARCHIVI PERIODICITA'
    if (this.oParentObject.w_VENDI="S" OR this.oParentObject.w_STLEGAL="S" ) AND this.VABENE
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"TAB_CALE"),.F.)
      this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"MODCLDAT")
    endif
    * --- COPIA ARCHIVI DI BASE STATISTICHE
    if this.oParentObject.w_STATIS="S" AND this.VABENE
      * --- Criteri di Estrazione - Causale di Selezione
      this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CRIMELAB")
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CRIDELAB"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CAU_SELE"),.F.)
    endif
    * --- COPIA ARCHIVI DI BASE ANALISI BILANCIO
    if this.oParentObject.w_ANABIL="S" AND this.VABENE
      * --- Regole di Elaborazione - Gruppi di Regole
      this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"REG_EXTM")
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"REG_EXTR"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"GRU_REGO"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"GRUDREGO"),.F.)
    endif
    * --- COPIA ARCHIVI IMPORT
    if this.oParentObject.w_IMPO="S" AND this.VABENE
      * --- Non Gestito
    endif
    * --- COPIA FRASI MODELLO
    if this.oParentObject.w_FRAMODE="S" AND this.VABENE
      this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CAT_MODE")
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"FRA_MODE"),.F.)
    endif
    * --- COPIA ARCHIVI DI BASE STUDIO LEGALE
    if this.oParentObject.w_STLEGAL="S" AND this.VABENE
      * --- Voci di Costo - Centri di Costo - Calendario di Ripartizione - Ripartizioni
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"TIPCOSTO"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"VOC_COST"),.F.)
      if this.oParentObject.w_tariffa="S" AND this.VABENE
        * --- Articoli - Tipi prestazioni
        this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"TIPI_PRE"),.F.)
        * --- Articoli - Note
        this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"ART_ICOL"),.F.)
        this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"NOT_ARTI"),.F.)
        * --- Codici artcoli + Traduzioni in Lingua
        this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"KEY_ARTI"),.F.)
        this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"TRADARTI"),.F.)
        * --- Articoli Alternativi
        this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"ART_ALTE"),.F.)
      endif
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"SET_RAGG"),.F.)
      if this.oParentObject.w_statpra="S" AND this.VABENE
        this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"PRA_STAT"),.F.)
      endif
      if this.oParentObject.w_tipipra="S" AND this.VABENE
        this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"PRA_TIPI"),.F.)
        this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"SET_TIPI"),.F.)
      endif
      if this.oParentObject.w_matepra="S" AND this.VABENE
        this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"PRA_MATE"),.F.)
        this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"SET_MATE"),.F.)
      endif
      if this.oParentObject.w_oggepra="S" AND this.VABENE
        this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"PRA_OGGE"),.F.)
      endif
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"TIP_CLIE"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"SET_TIPC"),.F.)
      if this.oParentObject.w_entipra="S" AND this.VABENE
        this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"PRA_ENTI"),.F.)
      endif
      if this.oParentObject.w_uffipra="S" AND this.VABENE
        this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"PRA_UFFI"),.F.)
      endif
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CAT_SOGG"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"OFFTIPAT"),.F.)
      if this.oParentObject.w_ubicpra="S" AND this.VABENE
        this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"PRA_UBIC"),.F.)
      endif
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"PRA_SEDI"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"PRA_DIPA"),.F.)
      if this.oParentObject.w_tariffa="S" AND this.oParentObject.w_tipiatt="S" AND this.VABENE
        this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CAUMATTI"),.F.)
        this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CAU_ATTI"),.F.)
      endif
      if this.oParentObject.w_tariffa="S" AND this.oParentObject.w_raggatt="S" AND this.VABENE
        this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"PRO_ITER"),.F.)
      endif
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"MANSIONI"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"DIPENDEN"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"PROVINCE"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"PROMODEL"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CAL_AZIE"),.F.)
      if this.oParentObject.w_tariffa="S" AND this.oParentObject.w_raggpre="S" AND this.VABENE
        this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"PRE_ITER"),.F.)
      endif
      * --- Gestione autonumber - PROMODEL
      this.w_AZI = this.oParentObject.w_CODAZI
      if this.VABENE
        * --- Select from PROMODEL
        i_nConn=i_TableProp[this.PROMODEL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PROMODEL_idx,2],.t.,this.PROMODEL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select Max(MOCODICE) As Prog  from "+i_cTable+" PROMODEL ";
               ,"_Curs_PROMODEL")
        else
          select Max(MOCODICE) As Prog from (i_cTable);
            into cursor _Curs_PROMODEL
        endif
        if used('_Curs_PROMODEL')
          select _Curs_PROMODEL
          locate for 1=1
          do while not(eof())
          this.w_MOCODICE = Nvl(_Curs_PROMODEL.Prog,1)
          cp_NextTableProg(this,i_nConn,"SEMDO","w_azi,w_MOCODICE")
            select _Curs_PROMODEL
            continue
          enddo
          use
        endif
      endif
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"PAR_PARC", "PRCODAZI", i_codazi, "PRCODAZI"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"PAR_AGEN", "PACODAZI", i_codazi, "PACODAZI"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"PAR_PRAT", "PACODAZI", i_codazi, "PACODAZI"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"PAR_ATTI", "PTCODAZI", i_codazi, "PTCODAZI"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"SAL_NOMI"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"TRI_BUTI"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"TAB_RITE", "TRCODAZI", i_codazi, "TRCODAZI"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"ANAG_PRO"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"PAR_OFFE", "POCODAZI", i_codazi, "POCODAZI"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"GRU_NOMI"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"ORI_NOMI"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"RUO_CONT"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"PRA_AL01"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"PRA_AL02"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"PRA_AL03"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"PRA_AL04"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"RUO_SOGI"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"TIP_RISE"),.F.)
      if this.oParentObject.w_raggtipi="S" AND this.VABENE
        this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"RAG_TIPI"),.F.)
      endif
      if this.oParentObject.w_classpra="S" AND this.VABENE
        this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CLA_PRAT"),.F.)
      endif
      * --- WP modelli (master - detail) e sezioni
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"WP_SEZIO"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"WP_MMODL"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"WP_DMODL"),.F.)
      if this.oParentObject.w_tariffa="S" AND this.VABENE
        * --- Tariffario
        this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"TAR_IFFE")
        this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"TAR_DETT"),.F.)
      endif
      * --- Se � installato il modulo Antiriciclaggio
      if g_ANTI="S"
        this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"TIP_PRES"),.F.)
        this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"ANT_DIVA"),.F.)
        this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"NAT_GIUR"),.F.)
        this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"PAR_ANTI", "PACODAZI", i_codazi, "PACODAZI"),.F.)
      endif
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- COPIA ARCHVI Livello Superiore (dipendono da archivi base)
    * --- PAGAMENTI (MASTER - DETAIL)
    if this.oParentObject.W_PAGAME="S" AND this.VABENE
      this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"PAG_AMEN")
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"PAG_2AME"),.F.)
    endif
    * --- MASTRI
    if this.oParentObject.W_MASTRI="S" AND this.VABENE
      this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"MASTRI")
    endif
    * --- CONTI DI TIPO -G-
    if this.oParentObject.W_CONTI="S" AND this.VABENE
      this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CONTI","ANTIPCON","G")
      if this.oParentObject.w_STLEGAL="S" AND this.VABENE
        * --- Colonne Cronologico
        this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"COLCRONO"),.F.)
      endif
    endif
    * --- SOTTOCONTO STUDIO
    if this.oParentObject.W_CONTI="S" AND this.VABENE
      this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"STUMPIAC")
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"STU_PIAC"),.F.)
    endif
    * --- AGENTI + Provvigioni
    if this.oParentObject.W_AGENTI="S" AND this.VABENE
      this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"AGENTI")
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"PRO_AGEN"),.F.)
    endif
    * --- Listini -Traduzioni
    if this.oParentObject.W_listini="S" AND this.VABENE
      this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"LISTINI")
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"TRADLIST"),.F.)
    endif
    * --- Tipologie operazioni IVA
    if ( this.oParentObject.w_CLIENTI="S" Or this.oParentObject.w_FORNI="S" Or this.oParentObject.w_ARTICOLI="S" ) AND this.VABENE
      this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"TIPCODIV")
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"ATTCOIVA"),.F.)
    endif
    * --- ARCHIVI VEFA 
    if (this.oParentObject.W_CLIENTI="S" OR this.oParentObject.W_FORNI="S" or this.oParentObject.W_caudocu="S") AND this.VABENE 
      * --- Gruppi Intestatari e Strutture EDI
      do GSAR_BAZ with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if (this.oParentObject.W_CLIENTI="S" OR this.oParentObject.W_FORNI="S") AND this.VABENE
      * --- Gruppi Intestatri EDI
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"GRU_INTE"),.F.)
    endif
    * --- CLIENTI
    this.w_AZI = this.oParentObject.w_CODAZI
    if this.oParentObject.W_CLIENTI="S" AND this.VABENE
      * --- Copio gruppi intestatari
      this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"GRP_DEFA")
      this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CONTI","ANTIPCON","C")
      if this.VABENE
        * --- Dettaglio pagamenti 
        this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"BAN_CONTI","CCTIPCON","C")
      endif
      * --- Dichiarazion d'intento
      if this.VABENE
        this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"DIC_INTE","DITIPCON","C")
      endif
      if this.VABENE
        this.w_DITIPCON = "C"
        * --- Select from DIC_INTE
        i_nConn=i_TableProp[this.DIC_INTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2],.t.,this.DIC_INTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select Max(DISERIAL) As Prog  from "+i_cTable+" DIC_INTE ";
               ,"_Curs_DIC_INTE")
        else
          select Max(DISERIAL) As Prog from (i_cTable);
            into cursor _Curs_DIC_INTE
        endif
        if used('_Curs_DIC_INTE')
          select _Curs_DIC_INTE
          locate for 1=1
          do while not(eof())
          this.w_DISERIAL = Nvl(_Curs_DIC_INTE.Prog,1)
          this.w_CURAZI = ALLTRIM(i_CODAZI)
          =cp_ChangeAzi( this.w_azi )
          cp_NextTableProg(this,i_nConn,"SEDIN","i_CODAZI,w_DISERIAL")
          =cp_ChangeAzi( this.w_CURAZI )
            select _Curs_DIC_INTE
            continue
          enddo
          use
        endif
        * --- Select from DIC_INTE
        i_nConn=i_TableProp[this.DIC_INTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2],.t.,this.DIC_INTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select DI__ANNO ,Max(DINUMDIC) As Prot, Max(DISERDIC) As Serdic  from "+i_cTable+" DIC_INTE ";
              +" where DITIPCON = 'C' ";
              +" group by DI__ANNO";
              +" order by DI__ANNO";
               ,"_Curs_DIC_INTE")
        else
          select DI__ANNO ,Max(DINUMDIC) As Prot, Max(DISERDIC) As Serdic from (i_cTable);
           where DITIPCON = "C" ;
           group by DI__ANNO;
           order by DI__ANNO;
            into cursor _Curs_DIC_INTE
        endif
        if used('_Curs_DIC_INTE')
          select _Curs_DIC_INTE
          locate for 1=1
          do while not(eof())
          this.w_DI__ANNO = Nvl(_Curs_DIC_INTE.DI__ANNO, g_CODESE)
          this.w_DISERDIC = NVL(_Curs_DIC_INTE.Serdic,space(3))
          this.w_DINUMDIC = Nvl(_Curs_DIC_INTE.Prot,1)
          this.w_CURAZI = ALLTRIM(i_CODAZI)
          =cp_ChangeAzi( this.w_azi )
          cp_NextTableProg(this,i_nConn,"PRDIN","i_CODAZI,w_DI__ANNO,w_DITIPCON,w_DISERDIC,w_DINUMDIC")
          =cp_ChangeAzi( this.w_CURAZI )
            select _Curs_DIC_INTE
            continue
          enddo
          use
        endif
      endif
      if this.oParentObject.w_SPEDI="S" AND this.VABENE
        * --- Sedi
        this.VABENE = TRASFARC(i_CODAZI, this.oParentObject.w_codazi, "DES_DIVE", "DDTIPCON", "C")
      endif
      if this.VABENE
        * --- Riferimenti
        this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CONTATTI","COTIPCON","C")
      endif
    endif
    * --- COPIA ARCHIVI DI BASE Analitica
    if this.oParentObject.W_ANAL="S" AND this.VABENE
      if this.oParentObject.w_STLEGAL<>"S"
        * --- Se w_STLEGAL='S' allora gli archivi TIP_COSTO e VOC_COST sono gi� stati copiati
        * --- Voci di Costo - Centri di Costo - Calendario di Ripartizione - Ripartizioni
        this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"TIPCOSTO")
        this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"VOC_COST"),.F.)
        if NOT ISALT()
          * --- Se w_STLEGAL='S' allora l'archivio CAN_TIER (Pratiche) non deve essere trasferito
          this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CAN_TIER"),.F.)
        endif
      endif
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CENCOST"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CAL_RIPA"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"RIPACENT"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"RIPACOMM"),.F.)
      if this.oParentObject.W_CONTI="S" AND this.VABENE
        * --- Ripartizioni Conti su Analitica
        this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"COLLCENT")
      endif
    endif
    * --- FORNITORI
    if this.oParentObject.W_FORNI="S" AND this.VABENE
      this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CONTI","ANTIPCON","F")
      if this.VABENE
        * --- Dettaglio pagamenti 
        this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"BAN_CONTI","CCTIPCON","F")
      endif
      * --- Dichiarazion d'intento
      if this.VABENE
        this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"DIC_INTE","DITIPCON","F")
      endif
      if this.VABENE
        this.w_DITIPCON = "F"
        * --- Select from DIC_INTE
        i_nConn=i_TableProp[this.DIC_INTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2],.t.,this.DIC_INTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select Max(DISERIAL) As Prog  from "+i_cTable+" DIC_INTE ";
               ,"_Curs_DIC_INTE")
        else
          select Max(DISERIAL) As Prog from (i_cTable);
            into cursor _Curs_DIC_INTE
        endif
        if used('_Curs_DIC_INTE')
          select _Curs_DIC_INTE
          locate for 1=1
          do while not(eof())
          this.w_DISERIAL = Nvl(_Curs_DIC_INTE.Prog,1)
          this.w_CURAZI = ALLTRIM(i_CODAZI)
          =cp_ChangeAzi( this.w_azi )
          cp_NextTableProg(this,i_nConn,"SEDIN","i_CODAZI,w_DISERIAL")
          =cp_ChangeAzi( this.w_CURAZI )
            select _Curs_DIC_INTE
            continue
          enddo
          use
        endif
        * --- Select from DIC_INTE
        i_nConn=i_TableProp[this.DIC_INTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2],.t.,this.DIC_INTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select DI__ANNO,Max(DINUMDIC) As Prot  from "+i_cTable+" DIC_INTE ";
              +" where DITIPCON = 'F' ";
              +" group by DI__ANNO";
              +" order by DI__ANNO";
               ,"_Curs_DIC_INTE")
        else
          select DI__ANNO,Max(DINUMDIC) As Prot from (i_cTable);
           where DITIPCON = "F" ;
           group by DI__ANNO;
           order by DI__ANNO;
            into cursor _Curs_DIC_INTE
        endif
        if used('_Curs_DIC_INTE')
          select _Curs_DIC_INTE
          locate for 1=1
          do while not(eof())
          this.w_DI__ANNO = Nvl(_Curs_DIC_INTE.DI__ANNO, g_CODESE)
          this.w_CURAZI = ALLTRIM(i_CODAZI)
          =cp_ChangeAzi( this.w_azi )
          this.w_DINUMDIC = Nvl(_Curs_DIC_INTE.Prot,1)
          cp_NextTableProg(this,i_nConn,"PRDIN","i_CODAZI,w_DI__ANNO,w_DITIPCON,w_DINUMDIC")
          =cp_ChangeAzi( this.w_CURAZI )
            select _Curs_DIC_INTE
            continue
          enddo
          use
        endif
        * --- Select from DIC_INTE
        i_nConn=i_TableProp[this.DIC_INTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2],.t.,this.DIC_INTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select DI__ANNO, DITIPIVA,Max(DINUMDOC) As Prot  from "+i_cTable+" DIC_INTE ";
              +" where DITIPCON = 'F'";
              +" group by DI__ANNO,DITIPIVA";
              +" order by DI__ANNO";
               ,"_Curs_DIC_INTE")
        else
          select DI__ANNO, DITIPIVA,Max(DINUMDOC) As Prot from (i_cTable);
           where DITIPCON = "F";
           group by DI__ANNO,DITIPIVA;
           order by DI__ANNO;
            into cursor _Curs_DIC_INTE
        endif
        if used('_Curs_DIC_INTE')
          select _Curs_DIC_INTE
          locate for 1=1
          do while not(eof())
          this.w_DINUMDOC = Nvl(_Curs_DIC_INTE.Prot,1)
          this.w_DI__ANNO = Nvl(_Curs_DIC_INTE.DI__ANNO, g_CODESE)
          this.w_DITIPIVA = _Curs_DIC_INTE.DITIPIVA
          this.w_CURAZI = ALLTRIM(i_CODAZI)
          =cp_ChangeAzi( this.w_azi )
          cp_NextTableProg(this,i_nConn,"PRDID","i_CODAZI,w_DI__ANNO,w_DITIPCON,w_DITIPIVA,w_DINUMDOC")
          =cp_ChangeAzi( this.w_CURAZI )
            select _Curs_DIC_INTE
            continue
          enddo
          use
        endif
      endif
      if this.oParentObject.w_SPEDI="S" AND this.VABENE
        * --- Sedi
        this.VABENE = TRASFARC(i_CODAZI, this.oParentObject.w_codazi, "DES_DIVE", "DDTIPCON", "F")
      endif
      if this.VABENE
        * --- Riferimenti
        this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CONTATTI","COTIPCON","F")
      endif
    endif
    * --- Nomenclatura
    if this.oParentObject.W_nomecla="S" AND this.VABENE
      this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"NOMENCLA")
    endif
    * --- Cambi Standard
    if this.oParentObject.W_CAMBI="S" AND this.VABENE
      this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CAM_BI")
    endif
    * --- Causali Magazzino - TRAD. MAGAZZINO
    if this.oParentObject.W_caumaga="S" AND this.VABENE
      this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CAM_AGAZ")
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"TRADCAMA"),.F.)
    endif
    * --- CAUSALI CONTABILI
    if this.oParentObject.W_cauconta="S" AND this.VABENE
      this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CAU_CONT")
      * --- Traduzioni
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"TRADCAUC"),.F.)
      * --- Automatismi IVA - Primanota
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CAUIVA1"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CAUPRI1"),.F.)
    endif
    * --- MODELLI CONTABILI
    if this.oParentObject.w_modconta = "S"
      * --- Modelli Contabili
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"MOD_CONT"),.F.)
      * --- Automatismi IVA - Primanota
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CAUIVA"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CAUPRI"),.F.)
    endif
    * --- Contropartite vendite / acquisti
    if this.oParentObject.W_contro="S" AND this.VABENE
      this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CONTVEAC")
    endif
    * --- Causali Documenti - Documenti collegati
    if this.oParentObject.W_caudocu="S" AND this.VABENE
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"MODMRIFE"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"MODDRIFE"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"TIP_DOCU"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"DOC_COLL"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"REPO_DOC"),.F.)
    endif
    * --- Strutture Intestatri EDI
    if g_VEFA="S" AND this.oParentObject.W_caudocu="S" AND this.VABENE
      if this.oParentObject.w_CLIENTI="S"
        this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"STR_INTE","SITIPCON","C")
      endif
      if this.oParentObject.w_FORNI="S"
        this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"STR_INTE","SITIPCON","F")
      endif
    endif
    * --- Messaggi
    if this.oParentObject.w_MESSAGGI="S" AND this.VABENE
      this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"MESSAGGI")
    endif
    * --- Causali Movimenti di Conto Corrente
    if this.oParentObject.w_CAUMOVCC="S" AND this.VABENE AND g_BANC="S"
      this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CCC_MAST")
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CCC_DETT"),.F.)
    endif
    * --- Provvigioni  (tabella provvigioni) - dopo gli agenti i parametri di provvigioni sono mono azienda - non serve trasefrirli
    if this.oParentObject.w_PROVVI="S" AND this.VABENE
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"TAB_PROV"),.F.)
    endif
    * --- Tabelle sconti e maggiorazioni - Anche categorie sconti e maggiorazioni
    if this.oParentObject.w_TBLSCO="S" AND this.VABENE
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"TAB_SCON"),.F.)
    endif
    * --- Articoli - assieme a tabelle varianti (Master / Detail), Famiglie Articoli, Cat. Analitica Articoli
    * --- Quest'ultima se non attiva l'analitica provoca la copia anche degli archivi di analitica
    if this.oParentObject.w_ARTICOLI="S" AND this.VABENE
      if this.oParentObject.w_FANAL<>"S"
        * --- Copia l'analitica - per copiarla mi servono le spedizioni - controllo di non averle gi� copiate
        if this.oParentObject.w_SPEDI<>"S"
          * --- Spedizioni - Porti - vettori - Aspetti esteriori - Tipi di Transazione
          this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"METCALSP"),.F.)
          this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"MCALSCAI"),.F.)
          this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"MODASPED")
          this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"PORTI"),.F.)
          this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"VETTORI"),.F.)
          this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"ASPETTO"),.F.)
          this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"TIPITRAN"),.F.)
        endif
        if this.oParentObject.w_STLEGAL<>"S"
          * --- Voci di Costo - Centri di Costo - Calendario di Ripartizione - Ripartizioni
          this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"TIPCOSTO")
          this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"VOC_COST"),.F.)
          * --- Se w_STLEGAL='S' allora l'archivio CAN_TIER (Pratiche) non deve essere trasferito
          this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CAN_TIER"),.F.)
        endif
        this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CENCOST"),.F.)
        this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CAL_RIPA"),.F.)
        this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"RIPACENT"),.F.)
      endif
      * --- Famiglia Articoli
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"FAM_ARTI"),.F.)
      * --- Reparti di Vendita (GPOS)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"REP_ARTI"),.F.)
      * --- Gruppi (OFFE)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"TIT_SEZI"),.F.)
      * --- SottoGruppi (OFFE)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"SOT_SEZI"),.F.)
      * --- Classi Matricole  (MADV)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CMT_MAST"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CMT_DETT"),.F.)
      * --- Tipi contributi  accessori
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"TIPCONTR"),.F.)
      * --- Classe materiali critici
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"MAT_CRI"),.F.)
      * --- Articoli - Note
      if this.oParentObject.w_STLEGAL<>"S"
        * --- Se w_STLEGAL='S' allora gli archivi TIPI_PRE, ART_ICOL e NOT_ARTI sono gi� stati trasferiti
        this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"TIPI_PRE"),.F.)
        this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"ART_ICOL"),.F.)
        this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"NOT_ARTI"),.F.)
      endif
      * --- Tipologie Colli
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"TIP_COLL"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CON_COLL"),.F.)
      * --- Listini Articoli - Scaglioni Listini
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"LIS_TINI"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"LIS_SCAG"),.F.)
      * --- Codici artcoli + Traduzioni in Lingua
      if this.oParentObject.w_STLEGAL<>"S"
        * --- Se w_STLEGAL='S' allora gli archivi KEY_ARTI e TRADARTI sono gi� stati trasferiti
        this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"KEY_ARTI"),.F.)
        this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"TRADARTI"),.F.)
      endif
      * --- Articoli Alternativi
      if this.oParentObject.w_STLEGAL<>"S"
        * --- Se w_STLEGAL='S' allora l'archivio ART_ALTE � gi� stato trasferito
        this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"ART_ALTE"),.F.)
      endif
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"SISMCOLL"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"SISDCOLL"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CATMCONT"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CATDCONT"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CONTRART"),.F.)
      if this.VABENE AND this.oParentObject.W_CLIENTI="S" 
        this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CONTRINT", "MCTIPINT","C" )
      endif
      if this.VABENE AND this.oParentObject.W_FORNI="S" 
        this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CONTRINT", "MCTIPINT","F" )
      endif
      * --- Per i saldi imballi gli articoli devono essere sicuramente riportati
      if this.VABENE And this.oParentObject.w_VEFA="S" And this.oParentObject.w_FORNI="S"
        * --- Trasferisco saldi imballi fornitori
        this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"SAL_IMBA", "SITIPCON","F" )
      endif
      if this.VABENE And this.oParentObject.w_VEFA="S" And this.oParentObject.w_CLIENTI="S"
        * --- Trasferisco saldi imballi articoli
        this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"SAL_IMBA", "SITIPCON","C" )
      endif
    endif
    * --- --Matricole. Le classi matricole CMT_MAST sono gi� copiate con gli articoli.
    if this.oParentObject.w_MATRICOLE = "S" AND this.oParentObject.w_ARTICOLI = "S" AND this.VABENE
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"MATRICOL"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"PROGMAT"),.F.)
    endif
    * --- Contratti (Master / Detail) - Scaglioni Contratti
    if this.oParentObject.w_CONTRA1="S" AND this.VABENE
      this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CON_TRAM")
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CON_TRAD"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CON_COSC"),.F.)
    endif
    * --- Dati articoli Magazzino - Anagrafica + movimentazione figlia
    if this.oParentObject.w_ARTMAG="S" AND this.VABENE
      this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"PAR_RIOR")
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"PAR_RIMA"),.F.)
    endif
    * --- Categorie Listini
    if this.oParentObject.w_CATELIST="S" AND this.VABENE
      this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CATMLIST")
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CATELIST"),.F.)
    endif
    * --- Parametri Listini
    if this.oParentObject.w_PARALIST="S" AND this.VABENE
      this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"PARMLIST")
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"PARALIST"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"PARATRAS"),.F.)
    endif
    * --- Strutture di Bilancio
    if this.oParentObject.w_SBIL="S" AND this.VABENE
      * --- Nel caso in cui non sono stati selezionati i clienti i conti o i fornitori copia solo la struttura
      this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"TIR_MAST")
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"TIR_DETT"),.F.)
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"RIGTOTAL"),.F.)
      if (this.oParentObject.w_CONTI="S" AND this.oParentObject.w_CLIENTI="S" AND this.oParentObject.w_FORNI="S" AND (this.oParentObject.w_ANAL="S" OR this.oParentObject.w_FANAL<>"S"))
        this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"VOCIRICL"),.F.)
        this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"DETTRICL"),.F.)
      endif
    endif
    * --- Tipologie e categorie attributi nominativi
    if this.VABENE
      this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"TIP_CATT")
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CAT_ATTR"),.F.)
    endif
    if g_AGEN="S" AND this.oParentObject.w_ATTSER="S"
      this.Page_5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Variabili Locali
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.oParentObject.w_MANS="S" AND this.VABENE
      this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"MANSIONI")
    endif
    this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"TIP_RISO"),.F.)
    this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"RIS_ORSE"),.F.)
    if this.oParentObject.w_DIPE="S" AND this.VABENE
      this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"DIPENDEN")
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"STRU_RIS"),.F.)
    endif
    if this.oParentObject.w_TIAT="S" AND this.VABENE
      this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"OFFTIPAT")
    endif
    if this.oParentObject.w_TPAT="S" AND this.VABENE
      this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CAUMATTI")
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CAU_ATTI"),.F.)
    endif
    if this.oParentObject.w_RAAT="S" AND this.VABENE
      this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"PRO_ITER")
    endif
    if this.oParentObject.w_RAPR="S" AND this.VABENE
      this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"PRE_ITER")
    endif
    if this.oParentObject.w_GRCP="S" AND this.VABENE
      this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"GRU_MAST")
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"GRU_DETT"),.F.)
    endif
    if this.oParentObject.w_MOEL="S" AND this.VABENE
      this.VABENE = TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"CAT_ELEM")
      this.VABENE = IIF(this.VABENE,TRASFARC(i_CODAZI,this.oParentObject.w_codazi,"MOD_ELEM"),.F.)
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,11)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='ESERCIZI'
    this.cWorkTables[3]='CONTROPA'
    this.cWorkTables[4]='UTE_AZI'
    this.cWorkTables[5]='PAR_PROV'
    this.cWorkTables[6]='MAGAZZIN'
    this.cWorkTables[7]='COC_MAST'
    this.cWorkTables[8]='TIP_ALLE'
    this.cWorkTables[9]='DIC_INTE'
    this.cWorkTables[10]='PROMODEL'
    this.cWorkTables[11]='AZBACKUP'
    return(this.OpenAllTables(11))

  proc CloseCursors()
    if used('_Curs_ESERCIZI')
      use in _Curs_ESERCIZI
    endif
    if used('_Curs_UTE_AZI')
      use in _Curs_UTE_AZI
    endif
    if used('_Curs_PROMODEL')
      use in _Curs_PROMODEL
    endif
    if used('_Curs_DIC_INTE')
      use in _Curs_DIC_INTE
    endif
    if used('_Curs_DIC_INTE')
      use in _Curs_DIC_INTE
    endif
    if used('_Curs_DIC_INTE')
      use in _Curs_DIC_INTE
    endif
    if used('_Curs_DIC_INTE')
      use in _Curs_DIC_INTE
    endif
    if used('_Curs_DIC_INTE')
      use in _Curs_DIC_INTE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
