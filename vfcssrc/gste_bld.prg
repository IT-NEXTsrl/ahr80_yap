* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bld                                                        *
*              Lancia maschera di drill down                                   *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][207]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-11-14                                                      *
* Last revis.: 2014-10-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bld",oParentObject)
return(i_retval)

define class tgste_bld as StdBatch
  * --- Local variables
  CFMSK = .NULL.
  w_COLSEL1 = 0
  w_ROWSEL1 = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato  da GSTE_KFL (Visualizza Cash Flow)
    *     Lancia maschera di drill down (GSTE_KFD)
    this.CFMSK = THIS.OPARENTOBJECT
    * --- Utilizzate per passarle alla maschera GSTE_KFD
    this.oParentObject.w_ROWSEL = ASCAN(this.CFMSK.CODCONTI, this.oParentObject.w_NUMCOR, -1, -1, 1, 7)
    this.oParentObject.w_ROWSEL = ROUND(this.oParentObject.w_ROWSEL/2,0)
    * --- Siccome l'array � composta da due colonne il valore che restituisce ASCAN
    *     deve essere diviso per 2
    if this.oParentObject.w_ROWSEL=0
      this.oParentObject.w_ROWSEL = this.CFMSK.w_CONTINUM + 1
    endif
    this.w_COLSEL1 = this.oParentObject.w_COLSEL
    this.w_ROWSEL1 = this.oParentObject.w_ROWSEL
    * --- Controllo che la riga selezionata non corrisponda ai totali
    if this.oParentObject.w_ROWSEL = this.CFMSK.w_CONTINUM + 1
      ah_errormsg("La funzionalit� non � disponibile selezionando il totale")
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_COLSEL <=2
      ah_errormsg("La funzionalit� � disponibile solo per i movimenti")
      i_retcode = 'stop'
      return
    endif
    if NOT EMPTY(this.oParentObject.w_TIPODATO)
      ah_errormsg("La funzionalit� � disponibile solo per righe di totale conto banca")
      i_retcode = 'stop'
      return
      i_retcode = 'stop'
      return
    endif
    do GSTE_KFD with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
