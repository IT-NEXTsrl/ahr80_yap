* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_suc                                                        *
*              Utilizzi crediti IVA                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_48]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-21                                                      *
* Last revis.: 2008-07-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_suc",oParentObject))

* --- Class definition
define class tgscg_suc as StdForm
  Top    = 50
  Left   = 97

  * --- Standard Properties
  Width  = 404
  Height = 137
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-07-08"
  HelpContextID=57288041
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Constant Properties
  _IDX = 0
  cPrg = "gscg_suc"
  cComment = "Utilizzi crediti IVA"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_ANNO = space(4)
  w_PERINI = 0
  w_PERFIN = 0
  w_UTIDETR = space(1)
  w_UTIF24 = space(1)
  w_UTIRIMB = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_sucPag1","gscg_suc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oANNO_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gscg_suc
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ANNO=space(4)
      .w_PERINI=0
      .w_PERFIN=0
      .w_UTIDETR=space(1)
      .w_UTIF24=space(1)
      .w_UTIRIMB=space(1)
        .w_ANNO = STR(YEAR(i_DATSYS),4,0)
        .w_PERINI = 1
        .w_PERFIN = IIF(g_TIPDEN='S',4,12)
        .w_UTIDETR = 'S'
        .w_UTIF24 = 'S'
        .w_UTIRIMB = 'S'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,6,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oANNO_1_1.value==this.w_ANNO)
      this.oPgFrm.Page1.oPag.oANNO_1_1.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oPERINI_1_2.value==this.w_PERINI)
      this.oPgFrm.Page1.oPag.oPERINI_1_2.value=this.w_PERINI
    endif
    if not(this.oPgFrm.Page1.oPag.oPERFIN_1_3.value==this.w_PERFIN)
      this.oPgFrm.Page1.oPag.oPERFIN_1_3.value=this.w_PERFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oUTIDETR_1_4.RadioValue()==this.w_UTIDETR)
      this.oPgFrm.Page1.oPag.oUTIDETR_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oUTIF24_1_5.RadioValue()==this.w_UTIF24)
      this.oPgFrm.Page1.oPag.oUTIF24_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oUTIRIMB_1_6.RadioValue()==this.w_UTIRIMB)
      this.oPgFrm.Page1.oPag.oUTIRIMB_1_6.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_ANNO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANNO_1_1.SetFocus()
            i_bnoObbl = !empty(.w_ANNO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.w_PERINI>=1 And .w_PERINI<= IIF(g_TIPDEN='S',4,12)) and (.w_PERINI<=.w_PERFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPERINI_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Numero periodo errato o maggiore del secondo")
          case   not((.w_PERFIN>=1 And .w_PERFIN<= IIF(g_TIPDEN='S',4,12)) and (.w_perini<=.w_perfin))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPERFIN_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Numero periodo errato o maggiore del secondo")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgscg_sucPag1 as StdContainer
  Width  = 400
  height = 137
  stdWidth  = 400
  stdheight = 137
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oANNO_1_1 as StdField with uid="LZGZRQJEAB",rtseq=1,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno solare di competenza da stampare",;
    HelpContextID = 51770106,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=89, Top=13, InputMask=replicate('X',4)

  add object oPERINI_1_2 as StdField with uid="HPWDYVAFHG",rtseq=2,rtrep=.f.,;
    cFormVar = "w_PERINI", cQueryName = "PERINI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Numero periodo errato o maggiore del secondo",;
    HelpContextID = 87800586,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=89, Top=38, cSayPict='"99"', cGetPict='"99"'

  func oPERINI_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_PERINI>=1 And .w_PERINI<= IIF(g_TIPDEN='S',4,12)) and (.w_PERINI<=.w_PERFIN))
    endwith
    return bRes
  endfunc

  add object oPERFIN_1_3 as StdField with uid="ZOBKBULTGM",rtseq=3,rtrep=.f.,;
    cFormVar = "w_PERFIN", cQueryName = "PERFIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Numero periodo errato o maggiore del secondo",;
    HelpContextID = 9353994,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=89, Top=63, cSayPict='"99"', cGetPict='"99"'

  func oPERFIN_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_PERFIN>=1 And .w_PERFIN<= IIF(g_TIPDEN='S',4,12)) and (.w_perini<=.w_perfin))
    endwith
    return bRes
  endfunc

  add object oUTIDETR_1_4 as StdCheck with uid="FPTJPGKMPW",rtseq=4,rtrep=.f.,left=159, top=13, caption="Utilizzo in detrazione IVA",;
    ToolTipText = "Se attivo: utilizzo in detrazione IVA",;
    HelpContextID = 86950982,;
    cFormVar="w_UTIDETR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oUTIDETR_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oUTIDETR_1_4.GetRadio()
    this.Parent.oContained.w_UTIDETR = this.RadioValue()
    return .t.
  endfunc

  func oUTIDETR_1_4.SetRadio()
    this.Parent.oContained.w_UTIDETR=trim(this.Parent.oContained.w_UTIDETR)
    this.value = ;
      iif(this.Parent.oContained.w_UTIDETR=='S',1,;
      0)
  endfunc

  add object oUTIF24_1_5 as StdCheck with uid="RWVUCEJGYC",rtseq=5,rtrep=.f.,left=159, top=38, caption="Utilizzo F24 - altri tributi",;
    ToolTipText = "Se attivo: utilizzo F24 - altri tributi",;
    HelpContextID = 201276346,;
    cFormVar="w_UTIF24", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oUTIF24_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oUTIF24_1_5.GetRadio()
    this.Parent.oContained.w_UTIF24 = this.RadioValue()
    return .t.
  endfunc

  func oUTIF24_1_5.SetRadio()
    this.Parent.oContained.w_UTIF24=trim(this.Parent.oContained.w_UTIF24)
    this.value = ;
      iif(this.Parent.oContained.w_UTIF24=='S',1,;
      0)
  endfunc

  add object oUTIRIMB_1_6 as StdCheck with uid="LRLURDRYWR",rtseq=6,rtrep=.f.,left=159, top=63, caption="Utilizzo per rimborsi",;
    ToolTipText = "Se attivo: utilizzo per rimborsi",;
    HelpContextID = 243057734,;
    cFormVar="w_UTIRIMB", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oUTIRIMB_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oUTIRIMB_1_6.GetRadio()
    this.Parent.oContained.w_UTIRIMB = this.RadioValue()
    return .t.
  endfunc

  func oUTIRIMB_1_6.SetRadio()
    this.Parent.oContained.w_UTIRIMB=trim(this.Parent.oContained.w_UTIRIMB)
    this.value = ;
      iif(this.Parent.oContained.w_UTIRIMB=='S',1,;
      0)
  endfunc


  add object oBtn_1_8 as StdButton with uid="IBNYUFBWRR",left=294, top=87, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per iniziare la stampa";
    , HelpContextID = 57259290;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      vx_exec("QUERY\GSCG_SUC.VQR",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_9 as StdButton with uid="YVVIPHEZOO",left=345, top=87, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 49970618;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_7 as StdString with uid="HLOPOKSXPB",Visible=.t., Left=45, Top=13,;
    Alignment=1, Width=40, Height=15,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="RZMELAFLUV",Visible=.t., Left=7, Top=38,;
    Alignment=1, Width=78, Height=18,;
    Caption="Da periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="BSONIOUNPF",Visible=.t., Left=10, Top=63,;
    Alignment=1, Width=75, Height=18,;
    Caption="A periodo:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_suc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
