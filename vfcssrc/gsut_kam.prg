* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kam                                                        *
*              Autenticazione invio E-mail                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_59]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-05-14                                                      *
* Last revis.: 2015-11-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_kam",oParentObject))

* --- Class definition
define class tgsut_kam as StdForm
  Top    = 77
  Left   = 100

  * --- Standard Properties
  Width  = 430
  Height = 191
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-11-26"
  HelpContextID=135707799
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=8

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsut_kam"
  cComment = "Autenticazione invio E-mail"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_AMTYPSEA = 0
  w_AM_EMAIL = space(254)
  w_AM_LOGIN = space(254)
  w_AMPASSWD = space(254)
  w_LOGIN = space(254)
  w_OLDPSW = space(254)
  w_PWDCL = space(1)
  o_PWDCL = space(1)
  w_PWDOK = .F.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kamPag1","gsut_kam",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oLOGIN_1_5
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AMTYPSEA=0
      .w_AM_EMAIL=space(254)
      .w_AM_LOGIN=space(254)
      .w_AMPASSWD=space(254)
      .w_LOGIN=space(254)
      .w_OLDPSW=space(254)
      .w_PWDCL=space(1)
      .w_PWDOK=.f.
      .w_AMTYPSEA=oParentObject.w_AMTYPSEA
      .w_AM_EMAIL=oParentObject.w_AM_EMAIL
      .w_AM_LOGIN=oParentObject.w_AM_LOGIN
      .w_AMPASSWD=oParentObject.w_AMPASSWD
      .w_PWDOK=oParentObject.w_PWDOK
      .oPgFrm.Page1.oPag.oObj_1_11.Calculate(iif(.w_PWDCL="S","","*"))
          .DoRTCalc(1,7,.f.)
        .w_PWDOK = IIF (.w_AMTYPSEA=4 , CifraCnf( ALLTRIM(.w_OLDPSW) , 'C' ) = ALLTRIM(.w_AMPASSWD)   ,.w_AM_LOGIN=.w_LOGIN AND CifraCnf( ALLTRIM(.w_OLDPSW) , 'C' ) = ALLTRIM(.w_AMPASSWD))
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsut_kam
    If .f. and type('oParentObject')='O' and type('oParentObject.w_UTCODICE')='N'
     This.w_UTENTE = oParentObject.w_UTCODICE
     This.Link_1_1('Full')
     This.NotifyEvent('w_UTENTE Changed')
     This.mCalc(.t.)
     This.SaveDependsOn()
    Endif
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_AMTYPSEA=.w_AMTYPSEA
      .oParentObject.w_AM_EMAIL=.w_AM_EMAIL
      .oParentObject.w_AM_LOGIN=.w_AM_LOGIN
      .oParentObject.w_AMPASSWD=.w_AMPASSWD
      .oParentObject.w_PWDOK=.w_PWDOK
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        if .o_PWDCL<>.w_PWDCL
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate(iif(.w_PWDCL="S","","*"))
        endif
        .DoRTCalc(1,7,.t.)
            .w_PWDOK = IIF (.w_AMTYPSEA=4 , CifraCnf( ALLTRIM(.w_OLDPSW) , 'C' ) = ALLTRIM(.w_AMPASSWD)   ,.w_AM_LOGIN=.w_LOGIN AND CifraCnf( ALLTRIM(.w_OLDPSW) , 'C' ) = ALLTRIM(.w_AMPASSWD))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate(iif(.w_PWDCL="S","","*"))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oOLDPSW_1_6.enabled = this.oPgFrm.Page1.oPag.oOLDPSW_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oLOGIN_1_5.visible=!this.oPgFrm.Page1.oPag.oLOGIN_1_5.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_9.visible=!this.oPgFrm.Page1.oPag.oStr_1_9.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_11.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oAM_EMAIL_1_2.value==this.w_AM_EMAIL)
      this.oPgFrm.Page1.oPag.oAM_EMAIL_1_2.value=this.w_AM_EMAIL
    endif
    if not(this.oPgFrm.Page1.oPag.oLOGIN_1_5.value==this.w_LOGIN)
      this.oPgFrm.Page1.oPag.oLOGIN_1_5.value=this.w_LOGIN
    endif
    if not(this.oPgFrm.Page1.oPag.oOLDPSW_1_6.value==this.w_OLDPSW)
      this.oPgFrm.Page1.oPag.oOLDPSW_1_6.value=this.w_OLDPSW
    endif
    if not(this.oPgFrm.Page1.oPag.oPWDCL_1_10.RadioValue()==this.w_PWDCL)
      this.oPgFrm.Page1.oPag.oPWDCL_1_10.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((.w_AM_LOGIN=.w_LOGIN OR  .w_AMTYPSEA=4) AND CifraCnf( ALLTRIM(.w_OLDPSW) , 'C' ) = ALLTRIM(.w_AMPASSWD))  and (NOT EMPTY(.w_LOGIN) OR  .w_AMTYPSEA=4)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOLDPSW_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Login e/o password non validi")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PWDCL = this.w_PWDCL
    return

enddefine

* --- Define pages as container
define class tgsut_kamPag1 as StdContainer
  Width  = 426
  height = 191
  stdWidth  = 426
  stdheight = 191
  resizeXpos=259
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oAM_EMAIL_1_2 as StdField with uid="CMQUBBVUSE",rtseq=2,rtrep=.f.,;
    cFormVar = "w_AM_EMAIL", cQueryName = "AM_EMAIL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo E-mail dell'utente",;
    HelpContextID = 238157394,;
   bGlobalFont=.t.,;
    Height=21, Width=350, Left=62, Top=20, InputMask=replicate('X',254)

  add object oLOGIN_1_5 as StdField with uid="SNUNNJBMYQ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_LOGIN", cQueryName = "LOGIN",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Login",;
    HelpContextID = 222593206,;
   bGlobalFont=.t.,;
    Height=21, Width=232, Left=180, Top=59, InputMask=replicate('X',254)

  func oLOGIN_1_5.mHide()
    with this.Parent.oContained
      return (.w_AMTYPSEA=4)
    endwith
  endfunc

  add object oOLDPSW_1_6 as StdField with uid="OKEPJEIKGG",rtseq=6,rtrep=.f.,;
    cFormVar = "w_OLDPSW", cQueryName = "OLDPSW",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    sErrorMsg = "Login e/o password non validi",;
    ToolTipText = "Password",;
    HelpContextID = 191148570,;
   bGlobalFont=.t.,;
    Height=21, Width=232, Left=180, Top=85, InputMask=replicate('X',254)

  func oOLDPSW_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_LOGIN) OR  .w_AMTYPSEA=4)
    endwith
   endif
  endfunc

  func oOLDPSW_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_AM_LOGIN=.w_LOGIN OR  .w_AMTYPSEA=4) AND CifraCnf( ALLTRIM(.w_OLDPSW) , 'C' ) = ALLTRIM(.w_AMPASSWD))
    endwith
    return bRes
  endfunc

  add object oPWDCL_1_10 as StdCheck with uid="ZOZEABRGYY",rtseq=7,rtrep=.f.,left=181, top=110, caption="Mostra password",;
    ToolTipText = "Visualizza la password in chiaro",;
    HelpContextID = 220092662,;
    cFormVar="w_PWDCL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPWDCL_1_10.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPWDCL_1_10.GetRadio()
    this.Parent.oContained.w_PWDCL = this.RadioValue()
    return .t.
  endfunc

  func oPWDCL_1_10.SetRadio()
    this.Parent.oContained.w_PWDCL=trim(this.Parent.oContained.w_PWDCL)
    this.value = ;
      iif(this.Parent.oContained.w_PWDCL=='S',1,;
      0)
  endfunc


  add object oObj_1_11 as cp_setobjprop with uid="KWUURUPULT",left=49, top=200, width=120,height=21,;
    caption='OLDPSW',;
   bGlobalFont=.t.,;
    cObj="w_OLDPSW",cProp="passwordchar",;
    nPag=1;
    , HelpContextID = 191148570


  add object oBtn_1_13 as StdButton with uid="APIWEFNJEL",left=313, top=135, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare login/password";
    , HelpContextID = 41781738;
    , Caption='\<OK';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_13.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_PWDOK)
      endwith
    endif
  endfunc


  add object oBtn_1_14 as StdButton with uid="ZJIUAZYUNY",left=368, top=135, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 41781738;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_7 as StdString with uid="FPPIBGTEZW",Visible=.t., Left=20, Top=85,;
    Alignment=1, Width=158, Height=18,;
    Caption="Password:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="VWXXLVYHWU",Visible=.t., Left=5, Top=20,;
    Alignment=1, Width=55, Height=18,;
    Caption="Account:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="FLGKZZFDXH",Visible=.t., Left=112, Top=59,;
    Alignment=1, Width=66, Height=18,;
    Caption="Login:"  ;
  , bGlobalFont=.t.

  func oStr_1_9.mHide()
    with this.Parent.oContained
      return (.w_AMTYPSEA=4)
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kam','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
