* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscp_kre                                                        *
*              Regola                                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_83]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-01-02                                                      *
* Last revis.: 2013-10-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscp_kre",oParentObject))

* --- Class definition
define class tgscp_kre as StdForm
  Top    = 2
  Left   = 6

  * --- Standard Properties
  Width  = 597
  Height = 400
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-10-16"
  HelpContextID=115971433
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=17

  * --- Constant Properties
  _IDX = 0
  cPrg = "gscp_kre"
  cComment = "Regola"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_RETABEL1 = space(2)
  w_CAMPO1_OM = space(30)
  w_CAMPO1_OD = space(30)
  w_CAMPO1_OD = space(30)
  w_CAMPO1_AR = space(30)
  w_CAMPO1_CL = space(30)
  w_REOPERAT = space(2)
  w_RETABEL2 = space(2)
  w_REVALORE = space(0)
  w_CAMPO2_OD = space(30)
  w_CAMPO2_OD = space(30)
  w_CAMPO2_CL = space(30)
  w_CAMPO2_AR = space(30)
  w_REDESSIN = space(40)
  w_RELIVCONF = 0
  w_RECAMPO1 = space(30)
  w_RECAMPO2 = space(30)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscp_krePag1","gscp_kre",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oRETABEL1_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_RETABEL1=space(2)
      .w_CAMPO1_OM=space(30)
      .w_CAMPO1_OD=space(30)
      .w_CAMPO1_OD=space(30)
      .w_CAMPO1_AR=space(30)
      .w_CAMPO1_CL=space(30)
      .w_REOPERAT=space(2)
      .w_RETABEL2=space(2)
      .w_REVALORE=space(0)
      .w_CAMPO2_OD=space(30)
      .w_CAMPO2_OD=space(30)
      .w_CAMPO2_CL=space(30)
      .w_CAMPO2_AR=space(30)
      .w_REDESSIN=space(40)
      .w_RELIVCONF=0
      .w_RECAMPO1=space(30)
      .w_RECAMPO2=space(30)
      .w_RETABEL1=oParentObject.w_RETABEL1
      .w_REOPERAT=oParentObject.w_REOPERAT
      .w_RETABEL2=oParentObject.w_RETABEL2
      .w_REVALORE=oParentObject.w_REVALORE
      .w_REDESSIN=oParentObject.w_REDESSIN
      .w_RELIVCONF=oParentObject.w_RELIVCONF
      .w_RECAMPO1=oParentObject.w_RECAMPO1
      .w_RECAMPO2=oParentObject.w_RECAMPO2
        .w_RETABEL1 = .w_RETABEL1
        .w_CAMPO1_OM = .w_RECAMPO1
        .w_CAMPO1_OD = .w_RECAMPO1
        .w_CAMPO1_OD = .w_RECAMPO1
        .w_CAMPO1_AR = .w_RECAMPO1
        .w_CAMPO1_CL = .w_RECAMPO1
        .w_REOPERAT = .w_REOPERAT
        .w_RETABEL2 = .w_RETABEL2
          .DoRTCalc(9,9,.f.)
        .w_CAMPO2_OD = .w_RECAMPO2
        .w_CAMPO2_OD = .w_RECAMPO2
        .w_CAMPO2_CL = .w_RECAMPO2
        .w_CAMPO2_AR = .w_RECAMPO2
          .DoRTCalc(14,15,.f.)
        .w_RECAMPO1 = iif(.w_RETABEL1='OM',.w_CAMPO1_OM,iif(.w_RETABEL1='OD',.w_CAMPO1_OD,iif(.w_RETABEL1='AR',.w_CAMPO1_AR,.w_CAMPO1_CL)))
        .w_RECAMPO2 = iif(.w_RETABEL2='OD',.w_CAMPO2_OD,iif(.w_RETABEL2='AR',.w_CAMPO2_AR,iif(.w_RETABEL2='CL',.w_CAMPO2_CL,'')))
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_27.enabled = this.oPgFrm.Page1.oPag.oBtn_1_27.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_RETABEL1=.w_RETABEL1
      .oParentObject.w_REOPERAT=.w_REOPERAT
      .oParentObject.w_RETABEL2=.w_RETABEL2
      .oParentObject.w_REVALORE=.w_REVALORE
      .oParentObject.w_REDESSIN=.w_REDESSIN
      .oParentObject.w_RELIVCONF=.w_RELIVCONF
      .oParentObject.w_RECAMPO1=.w_RECAMPO1
      .oParentObject.w_RECAMPO2=.w_RECAMPO2
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,15,.t.)
            .w_RECAMPO1 = iif(.w_RETABEL1='OM',.w_CAMPO1_OM,iif(.w_RETABEL1='OD',.w_CAMPO1_OD,iif(.w_RETABEL1='AR',.w_CAMPO1_AR,.w_CAMPO1_CL)))
            .w_RECAMPO2 = iif(.w_RETABEL2='OD',.w_CAMPO2_OD,iif(.w_RETABEL2='AR',.w_CAMPO2_AR,iif(.w_RETABEL2='CL',.w_CAMPO2_CL,'')))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCAMPO1_OM_1_2.enabled_(this.oPgFrm.Page1.oPag.oCAMPO1_OM_1_2.mCond())
    this.oPgFrm.Page1.oPag.oCAMPO1_OD_1_3.enabled_(this.oPgFrm.Page1.oPag.oCAMPO1_OD_1_3.mCond())
    this.oPgFrm.Page1.oPag.oCAMPO1_OD_1_4.enabled_(this.oPgFrm.Page1.oPag.oCAMPO1_OD_1_4.mCond())
    this.oPgFrm.Page1.oPag.oCAMPO1_AR_1_5.enabled_(this.oPgFrm.Page1.oPag.oCAMPO1_AR_1_5.mCond())
    this.oPgFrm.Page1.oPag.oCAMPO1_CL_1_6.enabled_(this.oPgFrm.Page1.oPag.oCAMPO1_CL_1_6.mCond())
    this.oPgFrm.Page1.oPag.oREVALORE_1_9.enabled = this.oPgFrm.Page1.oPag.oREVALORE_1_9.mCond()
    this.oPgFrm.Page1.oPag.oCAMPO2_OD_1_10.enabled_(this.oPgFrm.Page1.oPag.oCAMPO2_OD_1_10.mCond())
    this.oPgFrm.Page1.oPag.oCAMPO2_OD_1_11.enabled_(this.oPgFrm.Page1.oPag.oCAMPO2_OD_1_11.mCond())
    this.oPgFrm.Page1.oPag.oCAMPO2_CL_1_12.enabled_(this.oPgFrm.Page1.oPag.oCAMPO2_CL_1_12.mCond())
    this.oPgFrm.Page1.oPag.oCAMPO2_AR_1_13.enabled_(this.oPgFrm.Page1.oPag.oCAMPO2_AR_1_13.mCond())
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCAMPO1_OM_1_2.visible=!this.oPgFrm.Page1.oPag.oCAMPO1_OM_1_2.mHide()
    this.oPgFrm.Page1.oPag.oCAMPO1_OD_1_3.visible=!this.oPgFrm.Page1.oPag.oCAMPO1_OD_1_3.mHide()
    this.oPgFrm.Page1.oPag.oCAMPO1_OD_1_4.visible=!this.oPgFrm.Page1.oPag.oCAMPO1_OD_1_4.mHide()
    this.oPgFrm.Page1.oPag.oCAMPO1_AR_1_5.visible=!this.oPgFrm.Page1.oPag.oCAMPO1_AR_1_5.mHide()
    this.oPgFrm.Page1.oPag.oCAMPO1_CL_1_6.visible=!this.oPgFrm.Page1.oPag.oCAMPO1_CL_1_6.mHide()
    this.oPgFrm.Page1.oPag.oRETABEL2_1_8.visible=!this.oPgFrm.Page1.oPag.oRETABEL2_1_8.mHide()
    this.oPgFrm.Page1.oPag.oREVALORE_1_9.visible=!this.oPgFrm.Page1.oPag.oREVALORE_1_9.mHide()
    this.oPgFrm.Page1.oPag.oCAMPO2_OD_1_10.visible=!this.oPgFrm.Page1.oPag.oCAMPO2_OD_1_10.mHide()
    this.oPgFrm.Page1.oPag.oCAMPO2_OD_1_11.visible=!this.oPgFrm.Page1.oPag.oCAMPO2_OD_1_11.mHide()
    this.oPgFrm.Page1.oPag.oCAMPO2_CL_1_12.visible=!this.oPgFrm.Page1.oPag.oCAMPO2_CL_1_12.mHide()
    this.oPgFrm.Page1.oPag.oCAMPO2_AR_1_13.visible=!this.oPgFrm.Page1.oPag.oCAMPO2_AR_1_13.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_21.visible=!this.oPgFrm.Page1.oPag.oBtn_1_21.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_22.visible=!this.oPgFrm.Page1.oPag.oBtn_1_22.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_24.visible=!this.oPgFrm.Page1.oPag.oBtn_1_24.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_25.visible=!this.oPgFrm.Page1.oPag.oBtn_1_25.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_26.visible=!this.oPgFrm.Page1.oPag.oBtn_1_26.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_27.visible=!this.oPgFrm.Page1.oPag.oBtn_1_27.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oRETABEL1_1_1.RadioValue()==this.w_RETABEL1)
      this.oPgFrm.Page1.oPag.oRETABEL1_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMPO1_OM_1_2.RadioValue()==this.w_CAMPO1_OM)
      this.oPgFrm.Page1.oPag.oCAMPO1_OM_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMPO1_OD_1_3.RadioValue()==this.w_CAMPO1_OD)
      this.oPgFrm.Page1.oPag.oCAMPO1_OD_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMPO1_OD_1_4.RadioValue()==this.w_CAMPO1_OD)
      this.oPgFrm.Page1.oPag.oCAMPO1_OD_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMPO1_AR_1_5.RadioValue()==this.w_CAMPO1_AR)
      this.oPgFrm.Page1.oPag.oCAMPO1_AR_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMPO1_CL_1_6.RadioValue()==this.w_CAMPO1_CL)
      this.oPgFrm.Page1.oPag.oCAMPO1_CL_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oREOPERAT_1_7.RadioValue()==this.w_REOPERAT)
      this.oPgFrm.Page1.oPag.oREOPERAT_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRETABEL2_1_8.RadioValue()==this.w_RETABEL2)
      this.oPgFrm.Page1.oPag.oRETABEL2_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oREVALORE_1_9.value==this.w_REVALORE)
      this.oPgFrm.Page1.oPag.oREVALORE_1_9.value=this.w_REVALORE
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMPO2_OD_1_10.RadioValue()==this.w_CAMPO2_OD)
      this.oPgFrm.Page1.oPag.oCAMPO2_OD_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMPO2_OD_1_11.RadioValue()==this.w_CAMPO2_OD)
      this.oPgFrm.Page1.oPag.oCAMPO2_OD_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMPO2_CL_1_12.RadioValue()==this.w_CAMPO2_CL)
      this.oPgFrm.Page1.oPag.oCAMPO2_CL_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMPO2_AR_1_13.RadioValue()==this.w_CAMPO2_AR)
      this.oPgFrm.Page1.oPag.oCAMPO2_AR_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oREDESSIN_1_16.value==this.w_REDESSIN)
      this.oPgFrm.Page1.oPag.oREDESSIN_1_16.value=this.w_REDESSIN
    endif
    if not(this.oPgFrm.Page1.oPag.oRELIVCONF_1_18.RadioValue()==this.w_RELIVCONF)
      this.oPgFrm.Page1.oPag.oRELIVCONF_1_18.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgscp_krePag1 as StdContainer
  Width  = 593
  height = 400
  stdWidth  = 593
  stdheight = 400
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oRETABEL1_1_1 as StdCombo with uid="WHQZNCPYTC",rtseq=1,rtrep=.f.,left=84,top=56,width=144,height=22;
    , HelpContextID = 226691769;
    , cFormVar="w_RETABEL1",RowSource=""+"Ordini web master,"+"Ordini web dettaglio,"+"Articolo,"+"Cliente", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oRETABEL1_1_1.RadioValue()
    return(iif(this.value =1,'OM',;
    iif(this.value =2,'OD',;
    iif(this.value =3,'AR',;
    iif(this.value =4,'CL',;
    space(2))))))
  endfunc
  func oRETABEL1_1_1.GetRadio()
    this.Parent.oContained.w_RETABEL1 = this.RadioValue()
    return .t.
  endfunc

  func oRETABEL1_1_1.SetRadio()
    this.Parent.oContained.w_RETABEL1=trim(this.Parent.oContained.w_RETABEL1)
    this.value = ;
      iif(this.Parent.oContained.w_RETABEL1=='OM',1,;
      iif(this.Parent.oContained.w_RETABEL1=='OD',2,;
      iif(this.Parent.oContained.w_RETABEL1=='AR',3,;
      iif(this.Parent.oContained.w_RETABEL1=='CL',4,;
      0))))
  endfunc

  add object oCAMPO1_OM_1_2 as StdRadio with uid="PWHUOOXBGK",rtseq=2,rtrep=.f.,left=84, top=85, width=145,height=250;
    , cFormVar="w_CAMPO1_OM", ButtonCount=16, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oCAMPO1_OM_1_2.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="1^ Sconto cliente"
      this.Buttons(1).HelpContextID = 10779323
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="2^ Sconto cliente"
      this.Buttons(2).HelpContextID = 10779323
      this.Buttons(2).Top=15
      this.Buttons(3).Caption="Cod. agente"
      this.Buttons(3).HelpContextID = 10779323
      this.Buttons(3).Top=30
      this.Buttons(4).Caption="Cod. porto"
      this.Buttons(4).HelpContextID = 10779323
      this.Buttons(4).Top=45
      this.Buttons(5).Caption="Cod. spedizione"
      this.Buttons(5).HelpContextID = 10779323
      this.Buttons(5).Top=60
      this.Buttons(6).Caption="Cod. pagamento"
      this.Buttons(6).HelpContextID = 10779323
      this.Buttons(6).Top=75
      this.Buttons(7).Caption="Cod. valuta"
      this.Buttons(7).HelpContextID = 10779323
      this.Buttons(7).Top=90
      this.Buttons(8).Caption="Cod. vettore"
      this.Buttons(8).HelpContextID = 10779323
      this.Buttons(8).Top=105
      this.Buttons(9).Caption="Condizioni consegna"
      this.Buttons(9).HelpContextID = 10779323
      this.Buttons(9).Top=120
      this.Buttons(10).Caption="Data documento"
      this.Buttons(10).HelpContextID = 10779323
      this.Buttons(10).Top=135
      this.Buttons(11).Caption="Note"
      this.Buttons(11).HelpContextID = 10779323
      this.Buttons(11).Top=150
      this.Buttons(12).Caption="Spese incasso"
      this.Buttons(12).HelpContextID = 10779323
      this.Buttons(12).Top=165
      this.Buttons(13).Caption="Spese trasporto"
      this.Buttons(13).HelpContextID = 10779323
      this.Buttons(13).Top=180
      this.Buttons(14).Caption="Spese imballo"
      this.Buttons(14).HelpContextID = 10779323
      this.Buttons(14).Top=195
      this.Buttons(15).Caption="Totale ordine"
      this.Buttons(15).HelpContextID = 10779323
      this.Buttons(15).Top=210
      this.Buttons(16).Caption="Totale sconti"
      this.Buttons(16).HelpContextID = 10779323
      this.Buttons(16).Top=225
      this.SetAll("Width",143)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oCAMPO1_OM_1_2.RadioValue()
    return(iif(this.value =1,'ORSCOCL1',;
    iif(this.value =2,'ORSCOCL2',;
    iif(this.value =3,'ORCODAGE',;
    iif(this.value =4,'ORCODPOR',;
    iif(this.value =5,'ORCODSPE',;
    iif(this.value =6,'ORCODPAG',;
    iif(this.value =7,'ORCODVAL',;
    iif(this.value =8,'ORCODVET',;
    iif(this.value =9,'ORCONCON',;
    iif(this.value =10,'ORDATDOC',;
    iif(this.value =11,'OR__NOTE',;
    iif(this.value =12,'ORSPEINC',;
    iif(this.value =13,'ORSPETRA',;
    iif(this.value =14,'ORSPEIMB',;
    iif(this.value =15,'ORTOTORD',;
    iif(this.value =16,'ORSCONTI',;
    space(30))))))))))))))))))
  endfunc
  func oCAMPO1_OM_1_2.GetRadio()
    this.Parent.oContained.w_CAMPO1_OM = this.RadioValue()
    return .t.
  endfunc

  func oCAMPO1_OM_1_2.SetRadio()
    this.Parent.oContained.w_CAMPO1_OM=trim(this.Parent.oContained.w_CAMPO1_OM)
    this.value = ;
      iif(this.Parent.oContained.w_CAMPO1_OM=='ORSCOCL1',1,;
      iif(this.Parent.oContained.w_CAMPO1_OM=='ORSCOCL2',2,;
      iif(this.Parent.oContained.w_CAMPO1_OM=='ORCODAGE',3,;
      iif(this.Parent.oContained.w_CAMPO1_OM=='ORCODPOR',4,;
      iif(this.Parent.oContained.w_CAMPO1_OM=='ORCODSPE',5,;
      iif(this.Parent.oContained.w_CAMPO1_OM=='ORCODPAG',6,;
      iif(this.Parent.oContained.w_CAMPO1_OM=='ORCODVAL',7,;
      iif(this.Parent.oContained.w_CAMPO1_OM=='ORCODVET',8,;
      iif(this.Parent.oContained.w_CAMPO1_OM=='ORCONCON',9,;
      iif(this.Parent.oContained.w_CAMPO1_OM=='ORDATDOC',10,;
      iif(this.Parent.oContained.w_CAMPO1_OM=='OR__NOTE',11,;
      iif(this.Parent.oContained.w_CAMPO1_OM=='ORSPEINC',12,;
      iif(this.Parent.oContained.w_CAMPO1_OM=='ORSPETRA',13,;
      iif(this.Parent.oContained.w_CAMPO1_OM=='ORSPEIMB',14,;
      iif(this.Parent.oContained.w_CAMPO1_OM=='ORTOTORD',15,;
      iif(this.Parent.oContained.w_CAMPO1_OM=='ORSCONTI',16,;
      0))))))))))))))))
  endfunc

  func oCAMPO1_OM_1_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_RETABEL1='OM')
    endwith
   endif
  endfunc

  func oCAMPO1_OM_1_2.mHide()
    with this.Parent.oContained
      return (.w_RETABEL1<>'OM')
    endwith
  endfunc

  add object oCAMPO1_OD_1_3 as StdRadio with uid="ASJKTJLSXJ",rtseq=3,rtrep=.f.,left=84, top=85, width=159,height=189;
    , cFormVar="w_CAMPO1_OD", ButtonCount=11, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oCAMPO1_OD_1_3.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="1^ Sconto applicato"
      this.Buttons(1).HelpContextID = 10779467
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="2^ Sconto applicato"
      this.Buttons(2).HelpContextID = 10779467
      this.Buttons(2).Top=17
      this.Buttons(3).Caption="3^ Sconto applicato"
      this.Buttons(3).HelpContextID = 10779467
      this.Buttons(3).Top=34
      this.Buttons(4).Caption="4^ Sconto applicato"
      this.Buttons(4).HelpContextID = 10779467
      this.Buttons(4).Top=51
      this.Buttons(5).Caption="Cod. contratto"
      this.Buttons(5).HelpContextID = 10779467
      this.Buttons(5).Top=68
      this.Buttons(6).Caption="Cod. listino"
      this.Buttons(6).HelpContextID = 10779467
      this.Buttons(6).Top=85
      this.Buttons(7).Caption="Data evasione"
      this.Buttons(7).HelpContextID = 10779467
      this.Buttons(7).Top=102
      this.Buttons(8).Caption="Flag omaggio (S/N)"
      this.Buttons(8).HelpContextID = 10779467
      this.Buttons(8).Top=119
      this.Buttons(9).Caption="Prezzo"
      this.Buttons(9).HelpContextID = 10779467
      this.Buttons(9).Top=136
      this.Buttons(10).Caption="Quantit�"
      this.Buttons(10).HelpContextID = 10779467
      this.Buttons(10).Top=153
      this.Buttons(11).Caption="Quantit� in 1^ UM"
      this.Buttons(11).HelpContextID = 10779467
      this.Buttons(11).Top=170
      this.SetAll("Width",157)
      this.SetAll("Height",19)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oCAMPO1_OD_1_3.RadioValue()
    return(iif(this.value =1,'ORSCONT1',;
    iif(this.value =2,'ORSCONT2',;
    iif(this.value =3,'ORSCONT3',;
    iif(this.value =4,'ORSCONT4',;
    iif(this.value =5,'ORCONTRA',;
    iif(this.value =6,'ORCODLIS',;
    iif(this.value =7,'ORDATEVA',;
    iif(this.value =8,'ORFLOMAG',;
    iif(this.value =9,'ORPREZZO',;
    iif(this.value =10,'ORQTAMOV',;
    iif(this.value =11,'ORQTAUM1',;
    space(30)))))))))))))
  endfunc
  func oCAMPO1_OD_1_3.GetRadio()
    this.Parent.oContained.w_CAMPO1_OD = this.RadioValue()
    return .t.
  endfunc

  func oCAMPO1_OD_1_3.SetRadio()
    this.Parent.oContained.w_CAMPO1_OD=trim(this.Parent.oContained.w_CAMPO1_OD)
    this.value = ;
      iif(this.Parent.oContained.w_CAMPO1_OD=='ORSCONT1',1,;
      iif(this.Parent.oContained.w_CAMPO1_OD=='ORSCONT2',2,;
      iif(this.Parent.oContained.w_CAMPO1_OD=='ORSCONT3',3,;
      iif(this.Parent.oContained.w_CAMPO1_OD=='ORSCONT4',4,;
      iif(this.Parent.oContained.w_CAMPO1_OD=='ORCONTRA',5,;
      iif(this.Parent.oContained.w_CAMPO1_OD=='ORCODLIS',6,;
      iif(this.Parent.oContained.w_CAMPO1_OD=='ORDATEVA',7,;
      iif(this.Parent.oContained.w_CAMPO1_OD=='ORFLOMAG',8,;
      iif(this.Parent.oContained.w_CAMPO1_OD=='ORPREZZO',9,;
      iif(this.Parent.oContained.w_CAMPO1_OD=='ORQTAMOV',10,;
      iif(this.Parent.oContained.w_CAMPO1_OD=='ORQTAUM1',11,;
      0)))))))))))
  endfunc

  func oCAMPO1_OD_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_RETABEL1='OD')
    endwith
   endif
  endfunc

  func oCAMPO1_OD_1_3.mHide()
    with this.Parent.oContained
      return (.w_RETABEL1<>'OD' or isahe())
    endwith
  endfunc

  add object oCAMPO1_OD_1_4 as StdRadio with uid="NFOWZLGGZH",rtseq=4,rtrep=.f.,left=84, top=85, width=159,height=189;
    , cFormVar="w_CAMPO1_OD", ButtonCount=12, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oCAMPO1_OD_1_4.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="1^ Sconto applicato"
      this.Buttons(1).HelpContextID = 10779467
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="2^ Sconto applicato"
      this.Buttons(2).HelpContextID = 10779467
      this.Buttons(2).Top=15
      this.Buttons(3).Caption="3^ Sconto applicato"
      this.Buttons(3).HelpContextID = 10779467
      this.Buttons(3).Top=30
      this.Buttons(4).Caption="4^ Sconto applicato"
      this.Buttons(4).HelpContextID = 10779467
      this.Buttons(4).Top=45
      this.Buttons(5).Caption="Sconto a valore"
      this.Buttons(5).HelpContextID = 10779467
      this.Buttons(5).Top=60
      this.Buttons(6).Caption="Cod. contratto"
      this.Buttons(6).HelpContextID = 10779467
      this.Buttons(6).Top=75
      this.Buttons(7).Caption="Cod. listino"
      this.Buttons(7).HelpContextID = 10779467
      this.Buttons(7).Top=90
      this.Buttons(8).Caption="Data evasione"
      this.Buttons(8).HelpContextID = 10779467
      this.Buttons(8).Top=105
      this.Buttons(9).Caption="Flag omaggio (S/N)"
      this.Buttons(9).HelpContextID = 10779467
      this.Buttons(9).Top=120
      this.Buttons(10).Caption="Prezzo"
      this.Buttons(10).HelpContextID = 10779467
      this.Buttons(10).Top=135
      this.Buttons(11).Caption="Quantit�"
      this.Buttons(11).HelpContextID = 10779467
      this.Buttons(11).Top=150
      this.Buttons(12).Caption="Quantit� in 1^ UM"
      this.Buttons(12).HelpContextID = 10779467
      this.Buttons(12).Top=165
      this.SetAll("Width",157)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oCAMPO1_OD_1_4.RadioValue()
    return(iif(this.value =1,'ORSCONT1',;
    iif(this.value =2,'ORSCONT2',;
    iif(this.value =3,'ORSCONT3',;
    iif(this.value =4,'ORSCONT4',;
    iif(this.value =5,'ORVALSCO',;
    iif(this.value =6,'ORCONTRA',;
    iif(this.value =7,'ORCODLIS',;
    iif(this.value =8,'ORDATEVA',;
    iif(this.value =9,'ORFLOMAG',;
    iif(this.value =10,'ORPREZZO',;
    iif(this.value =11,'ORQTAMOV',;
    iif(this.value =12,'ORQTAUM1',;
    space(30))))))))))))))
  endfunc
  func oCAMPO1_OD_1_4.GetRadio()
    this.Parent.oContained.w_CAMPO1_OD = this.RadioValue()
    return .t.
  endfunc

  func oCAMPO1_OD_1_4.SetRadio()
    this.Parent.oContained.w_CAMPO1_OD=trim(this.Parent.oContained.w_CAMPO1_OD)
    this.value = ;
      iif(this.Parent.oContained.w_CAMPO1_OD=='ORSCONT1',1,;
      iif(this.Parent.oContained.w_CAMPO1_OD=='ORSCONT2',2,;
      iif(this.Parent.oContained.w_CAMPO1_OD=='ORSCONT3',3,;
      iif(this.Parent.oContained.w_CAMPO1_OD=='ORSCONT4',4,;
      iif(this.Parent.oContained.w_CAMPO1_OD=='ORVALSCO',5,;
      iif(this.Parent.oContained.w_CAMPO1_OD=='ORCONTRA',6,;
      iif(this.Parent.oContained.w_CAMPO1_OD=='ORCODLIS',7,;
      iif(this.Parent.oContained.w_CAMPO1_OD=='ORDATEVA',8,;
      iif(this.Parent.oContained.w_CAMPO1_OD=='ORFLOMAG',9,;
      iif(this.Parent.oContained.w_CAMPO1_OD=='ORPREZZO',10,;
      iif(this.Parent.oContained.w_CAMPO1_OD=='ORQTAMOV',11,;
      iif(this.Parent.oContained.w_CAMPO1_OD=='ORQTAUM1',12,;
      0))))))))))))
  endfunc

  func oCAMPO1_OD_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_RETABEL1='OD')
    endwith
   endif
  endfunc

  func oCAMPO1_OD_1_4.mHide()
    with this.Parent.oContained
      return (.w_RETABEL1<>'OD' or not isahe())
    endwith
  endfunc

  add object oCAMPO1_AR_1_5 as StdRadio with uid="LVEFIKNCZC",rtseq=5,rtrep=.f.,left=84, top=85, width=175,height=62;
    , cFormVar="w_CAMPO1_AR", ButtonCount=3, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oCAMPO1_AR_1_5.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Categ. contabile"
      this.Buttons(1).HelpContextID = 10779257
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Codice articolo"
      this.Buttons(2).HelpContextID = 10779257
      this.Buttons(2).Top=20
      this.Buttons(3).Caption="Gruppo merceologico"
      this.Buttons(3).HelpContextID = 10779257
      this.Buttons(3).Top=40
      this.SetAll("Width",173)
      this.SetAll("Height",22)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oCAMPO1_AR_1_5.RadioValue()
    return(iif(this.value =1,'ARCATCON',;
    iif(this.value =2,'ORCODART',;
    iif(this.value =3,'ARGRUMER',;
    space(30)))))
  endfunc
  func oCAMPO1_AR_1_5.GetRadio()
    this.Parent.oContained.w_CAMPO1_AR = this.RadioValue()
    return .t.
  endfunc

  func oCAMPO1_AR_1_5.SetRadio()
    this.Parent.oContained.w_CAMPO1_AR=trim(this.Parent.oContained.w_CAMPO1_AR)
    this.value = ;
      iif(this.Parent.oContained.w_CAMPO1_AR=='ARCATCON',1,;
      iif(this.Parent.oContained.w_CAMPO1_AR=='ORCODART',2,;
      iif(this.Parent.oContained.w_CAMPO1_AR=='ARGRUMER',3,;
      0)))
  endfunc

  func oCAMPO1_AR_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_RETABEL1='AR')
    endwith
   endif
  endfunc

  func oCAMPO1_AR_1_5.mHide()
    with this.Parent.oContained
      return (.w_RETABEL1<>'AR')
    endwith
  endfunc

  add object oCAMPO1_CL_1_6 as StdRadio with uid="ERWZZKKFDP",rtseq=6,rtrep=.f.,left=84, top=85, width=175,height=105;
    , cFormVar="w_CAMPO1_CL", ButtonCount=6, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oCAMPO1_CL_1_6.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="1^ Sconto / magg."
      this.Buttons(1).HelpContextID = 10779351
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="2^ Sconto / magg."
      this.Buttons(2).HelpContextID = 10779351
      this.Buttons(2).Top=17
      this.Buttons(3).Caption="Categ. commerciale"
      this.Buttons(3).HelpContextID = 10779351
      this.Buttons(3).Top=34
      this.Buttons(4).Caption="Codice cliente"
      this.Buttons(4).HelpContextID = 10779351
      this.Buttons(4).Top=51
      this.Buttons(5).Caption="Cod. pagamento"
      this.Buttons(5).HelpContextID = 10779351
      this.Buttons(5).Top=68
      this.Buttons(6).Caption="Fido disponibile"
      this.Buttons(6).HelpContextID = 10779351
      this.Buttons(6).Top=85
      this.SetAll("Width",173)
      this.SetAll("Height",19)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oCAMPO1_CL_1_6.RadioValue()
    return(iif(this.value =1,'AN1SCONT',;
    iif(this.value =2,'AN2SCONT',;
    iif(this.value =3,'ANCATCOM',;
    iif(this.value =4,'ORCODCON',;
    iif(this.value =5,'ANCODPAG',;
    iif(this.value =6,'VALFIDIS',;
    space(30))))))))
  endfunc
  func oCAMPO1_CL_1_6.GetRadio()
    this.Parent.oContained.w_CAMPO1_CL = this.RadioValue()
    return .t.
  endfunc

  func oCAMPO1_CL_1_6.SetRadio()
    this.Parent.oContained.w_CAMPO1_CL=trim(this.Parent.oContained.w_CAMPO1_CL)
    this.value = ;
      iif(this.Parent.oContained.w_CAMPO1_CL=='AN1SCONT',1,;
      iif(this.Parent.oContained.w_CAMPO1_CL=='AN2SCONT',2,;
      iif(this.Parent.oContained.w_CAMPO1_CL=='ANCATCOM',3,;
      iif(this.Parent.oContained.w_CAMPO1_CL=='ORCODCON',4,;
      iif(this.Parent.oContained.w_CAMPO1_CL=='ANCODPAG',5,;
      iif(this.Parent.oContained.w_CAMPO1_CL=='VALFIDIS',6,;
      0))))))
  endfunc

  func oCAMPO1_CL_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_RETABEL1='CL')
    endwith
   endif
  endfunc

  func oCAMPO1_CL_1_6.mHide()
    with this.Parent.oContained
      return (.w_RETABEL1<>'CL')
    endwith
  endfunc


  add object oREOPERAT_1_7 as StdCombo with uid="MDQXNZIZGL",rtseq=7,rtrep=.f.,left=267,top=154,width=142,height=22;
    , ToolTipText = "Operatore per il confronto dei valori";
    , HelpContextID = 263955818;
    , cFormVar="w_REOPERAT",RowSource=""+"uguale a,"+"diverso da,"+"maggiore di,"+"maggiore o uguale a,"+"minore di,"+"minore o uguale a,"+"� vuoto,"+"non � vuoto", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oREOPERAT_1_7.RadioValue()
    return(iif(this.value =1,'=',;
    iif(this.value =2,'<>',;
    iif(this.value =3,'>',;
    iif(this.value =4,'>=',;
    iif(this.value =5,'<',;
    iif(this.value =6,'<=',;
    iif(this.value =7,'E',;
    iif(this.value =8,'NE',;
    space(2))))))))))
  endfunc
  func oREOPERAT_1_7.GetRadio()
    this.Parent.oContained.w_REOPERAT = this.RadioValue()
    return .t.
  endfunc

  func oREOPERAT_1_7.SetRadio()
    this.Parent.oContained.w_REOPERAT=trim(this.Parent.oContained.w_REOPERAT)
    this.value = ;
      iif(this.Parent.oContained.w_REOPERAT=='=',1,;
      iif(this.Parent.oContained.w_REOPERAT=='<>',2,;
      iif(this.Parent.oContained.w_REOPERAT=='>',3,;
      iif(this.Parent.oContained.w_REOPERAT=='>=',4,;
      iif(this.Parent.oContained.w_REOPERAT=='<',5,;
      iif(this.Parent.oContained.w_REOPERAT=='<=',6,;
      iif(this.Parent.oContained.w_REOPERAT=='E',7,;
      iif(this.Parent.oContained.w_REOPERAT=='NE',8,;
      0))))))))
  endfunc


  add object oRETABEL2_1_8 as StdCombo with uid="CKFHCOOPRB",rtseq=8,rtrep=.f.,left=414,top=56,width=144,height=22;
    , HelpContextID = 226691768;
    , cFormVar="w_RETABEL2",RowSource=""+"Ordini web dettaglio,"+"Articolo,"+"Cliente,"+"Espressione", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oRETABEL2_1_8.RadioValue()
    return(iif(this.value =1,'OD',;
    iif(this.value =2,'AR',;
    iif(this.value =3,'CL',;
    iif(this.value =4,'EX',;
    space(2))))))
  endfunc
  func oRETABEL2_1_8.GetRadio()
    this.Parent.oContained.w_RETABEL2 = this.RadioValue()
    return .t.
  endfunc

  func oRETABEL2_1_8.SetRadio()
    this.Parent.oContained.w_RETABEL2=trim(this.Parent.oContained.w_RETABEL2)
    this.value = ;
      iif(this.Parent.oContained.w_RETABEL2=='OD',1,;
      iif(this.Parent.oContained.w_RETABEL2=='AR',2,;
      iif(this.Parent.oContained.w_RETABEL2=='CL',3,;
      iif(this.Parent.oContained.w_RETABEL2=='EX',4,;
      0))))
  endfunc

  func oRETABEL2_1_8.mHide()
    with this.Parent.oContained
      return (.w_REOPERAT='E'  or .w_REOPERAT='NE')
    endwith
  endfunc

  add object oREVALORE_1_9 as StdMemo with uid="GTVDQTVUJD",rtseq=9,rtrep=.f.,;
    cFormVar = "w_REVALORE", cQueryName = "REVALORE",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 220009819,;
   bGlobalFont=.t.,;
    Height=239, Width=144, Left=414, Top=85

  func oREVALORE_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_RETABEL2='EX')
    endwith
   endif
  endfunc

  func oREVALORE_1_9.mHide()
    with this.Parent.oContained
      return (.w_RETABEL2<>'EX' or .w_REOPERAT='E'  or .w_REOPERAT='NE')
    endwith
  endfunc

  add object oCAMPO2_OD_1_10 as StdRadio with uid="PIRYCKJEHL",rtseq=10,rtrep=.f.,left=414, top=85, width=125,height=93;
    , cFormVar="w_CAMPO2_OD", ButtonCount=5, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oCAMPO2_OD_1_10.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="1^ Sconto iniziale"
      this.Buttons(1).HelpContextID = 262437707
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="2^ Sconto iniziale"
      this.Buttons(2).HelpContextID = 262437707
      this.Buttons(2).Top=18
      this.Buttons(3).Caption="3^ Sconto iniziale"
      this.Buttons(3).HelpContextID = 262437707
      this.Buttons(3).Top=36
      this.Buttons(4).Caption="4^ Sconto iniziale"
      this.Buttons(4).HelpContextID = 262437707
      this.Buttons(4).Top=54
      this.Buttons(5).Caption="Prezzo"
      this.Buttons(5).HelpContextID = 262437707
      this.Buttons(5).Top=72
      this.SetAll("Width",123)
      this.SetAll("Height",20)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oCAMPO2_OD_1_10.RadioValue()
    return(iif(this.value =1,'ORSCOIN1',;
    iif(this.value =2,'ORSCOIN2',;
    iif(this.value =3,'ORSCOIN3',;
    iif(this.value =4,'ORSCOIN4',;
    iif(this.value =5,'ORPREZZ1',;
    space(30)))))))
  endfunc
  func oCAMPO2_OD_1_10.GetRadio()
    this.Parent.oContained.w_CAMPO2_OD = this.RadioValue()
    return .t.
  endfunc

  func oCAMPO2_OD_1_10.SetRadio()
    this.Parent.oContained.w_CAMPO2_OD=trim(this.Parent.oContained.w_CAMPO2_OD)
    this.value = ;
      iif(this.Parent.oContained.w_CAMPO2_OD=='ORSCOIN1',1,;
      iif(this.Parent.oContained.w_CAMPO2_OD=='ORSCOIN2',2,;
      iif(this.Parent.oContained.w_CAMPO2_OD=='ORSCOIN3',3,;
      iif(this.Parent.oContained.w_CAMPO2_OD=='ORSCOIN4',4,;
      iif(this.Parent.oContained.w_CAMPO2_OD=='ORPREZZ1',5,;
      0)))))
  endfunc

  func oCAMPO2_OD_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_RETABEL2='OD')
    endwith
   endif
  endfunc

  func oCAMPO2_OD_1_10.mHide()
    with this.Parent.oContained
      return (.w_RETABEL2<>'OD' or isahe())
    endwith
  endfunc

  add object oCAMPO2_OD_1_11 as StdRadio with uid="DQRZMXSHMN",rtseq=11,rtrep=.f.,left=414, top=85, width=125,height=93;
    , cFormVar="w_CAMPO2_OD", ButtonCount=6, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oCAMPO2_OD_1_11.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="1^ Sconto iniziale"
      this.Buttons(1).HelpContextID = 262437707
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="2^ Sconto iniziale"
      this.Buttons(2).HelpContextID = 262437707
      this.Buttons(2).Top=15
      this.Buttons(3).Caption="3^ Sconto iniziale"
      this.Buttons(3).HelpContextID = 262437707
      this.Buttons(3).Top=30
      this.Buttons(4).Caption="4^ Sconto iniziale"
      this.Buttons(4).HelpContextID = 262437707
      this.Buttons(4).Top=45
      this.Buttons(5).Caption="Sconto a valore"
      this.Buttons(5).HelpContextID = 262437707
      this.Buttons(5).Top=60
      this.Buttons(6).Caption="Prezzo"
      this.Buttons(6).HelpContextID = 262437707
      this.Buttons(6).Top=75
      this.SetAll("Width",123)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oCAMPO2_OD_1_11.RadioValue()
    return(iif(this.value =1,'ORSCOIN1',;
    iif(this.value =2,'ORSCOIN2',;
    iif(this.value =3,'ORSCOIN3',;
    iif(this.value =4,'ORSCOIN4',;
    iif(this.value =5,'ORSCOINV',;
    iif(this.value =6,'ORPREZZ1',;
    space(30))))))))
  endfunc
  func oCAMPO2_OD_1_11.GetRadio()
    this.Parent.oContained.w_CAMPO2_OD = this.RadioValue()
    return .t.
  endfunc

  func oCAMPO2_OD_1_11.SetRadio()
    this.Parent.oContained.w_CAMPO2_OD=trim(this.Parent.oContained.w_CAMPO2_OD)
    this.value = ;
      iif(this.Parent.oContained.w_CAMPO2_OD=='ORSCOIN1',1,;
      iif(this.Parent.oContained.w_CAMPO2_OD=='ORSCOIN2',2,;
      iif(this.Parent.oContained.w_CAMPO2_OD=='ORSCOIN3',3,;
      iif(this.Parent.oContained.w_CAMPO2_OD=='ORSCOIN4',4,;
      iif(this.Parent.oContained.w_CAMPO2_OD=='ORSCOINV',5,;
      iif(this.Parent.oContained.w_CAMPO2_OD=='ORPREZZ1',6,;
      0))))))
  endfunc

  func oCAMPO2_OD_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_RETABEL2='OD')
    endwith
   endif
  endfunc

  func oCAMPO2_OD_1_11.mHide()
    with this.Parent.oContained
      return (.w_RETABEL2<>'OD' or not isahe())
    endwith
  endfunc

  add object oCAMPO2_CL_1_12 as StdRadio with uid="WWZHLBYWUC",rtseq=12,rtrep=.f.,left=414, top=85, width=147,height=118;
    , cFormVar="w_CAMPO2_CL", ButtonCount=7, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oCAMPO2_CL_1_12.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="1^ Sconto / magg."
      this.Buttons(1).HelpContextID = 262437591
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="2^ Sconto / magg."
      this.Buttons(2).HelpContextID = 262437591
      this.Buttons(2).Top=16
      this.Buttons(3).Caption="Categ. commerciale"
      this.Buttons(3).HelpContextID = 262437591
      this.Buttons(3).Top=32
      this.Buttons(4).Caption="Codice cliente"
      this.Buttons(4).HelpContextID = 262437591
      this.Buttons(4).Top=48
      this.Buttons(5).Caption="Cod. pagamento"
      this.Buttons(5).HelpContextID = 262437591
      this.Buttons(5).Top=64
      this.Buttons(6).Caption="Fido disponibile"
      this.Buttons(6).HelpContextID = 262437591
      this.Buttons(6).Top=80
      this.Buttons(7).Caption="Massimo ordine"
      this.Buttons(7).HelpContextID = 262437591
      this.Buttons(7).Top=96
      this.SetAll("Width",145)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oCAMPO2_CL_1_12.RadioValue()
    return(iif(this.value =1,'AN1SCONT',;
    iif(this.value =2,'AN2SCONT',;
    iif(this.value =3,'ANCATCOM',;
    iif(this.value =4,'ORCODCON',;
    iif(this.value =5,'ANCODPAG',;
    iif(this.value =6,'VALFIDIS',;
    iif(this.value =7,'ANMAXORD',;
    space(30)))))))))
  endfunc
  func oCAMPO2_CL_1_12.GetRadio()
    this.Parent.oContained.w_CAMPO2_CL = this.RadioValue()
    return .t.
  endfunc

  func oCAMPO2_CL_1_12.SetRadio()
    this.Parent.oContained.w_CAMPO2_CL=trim(this.Parent.oContained.w_CAMPO2_CL)
    this.value = ;
      iif(this.Parent.oContained.w_CAMPO2_CL=='AN1SCONT',1,;
      iif(this.Parent.oContained.w_CAMPO2_CL=='AN2SCONT',2,;
      iif(this.Parent.oContained.w_CAMPO2_CL=='ANCATCOM',3,;
      iif(this.Parent.oContained.w_CAMPO2_CL=='ORCODCON',4,;
      iif(this.Parent.oContained.w_CAMPO2_CL=='ANCODPAG',5,;
      iif(this.Parent.oContained.w_CAMPO2_CL=='VALFIDIS',6,;
      iif(this.Parent.oContained.w_CAMPO2_CL=='ANMAXORD',7,;
      0)))))))
  endfunc

  func oCAMPO2_CL_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_RETABEL2='CL')
    endwith
   endif
  endfunc

  func oCAMPO2_CL_1_12.mHide()
    with this.Parent.oContained
      return (.w_RETABEL2<>'CL' or .w_REOPERAT='E'  or .w_REOPERAT='NE')
    endwith
  endfunc

  add object oCAMPO2_AR_1_13 as StdRadio with uid="DSSXWJHRYC",rtseq=13,rtrep=.f.,left=414, top=85, width=147,height=62;
    , cFormVar="w_CAMPO2_AR", ButtonCount=3, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oCAMPO2_AR_1_13.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Categ. contabile"
      this.Buttons(1).HelpContextID = 262437497
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Codice articolo"
      this.Buttons(2).HelpContextID = 262437497
      this.Buttons(2).Top=20
      this.Buttons(3).Caption="Gruppo merceologico"
      this.Buttons(3).HelpContextID = 262437497
      this.Buttons(3).Top=40
      this.SetAll("Width",145)
      this.SetAll("Height",22)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oCAMPO2_AR_1_13.RadioValue()
    return(iif(this.value =1,'ARCATCON',;
    iif(this.value =2,'ORCODART',;
    iif(this.value =3,'ARGRUMER',;
    space(30)))))
  endfunc
  func oCAMPO2_AR_1_13.GetRadio()
    this.Parent.oContained.w_CAMPO2_AR = this.RadioValue()
    return .t.
  endfunc

  func oCAMPO2_AR_1_13.SetRadio()
    this.Parent.oContained.w_CAMPO2_AR=trim(this.Parent.oContained.w_CAMPO2_AR)
    this.value = ;
      iif(this.Parent.oContained.w_CAMPO2_AR=='ARCATCON',1,;
      iif(this.Parent.oContained.w_CAMPO2_AR=='ORCODART',2,;
      iif(this.Parent.oContained.w_CAMPO2_AR=='ARGRUMER',3,;
      0)))
  endfunc

  func oCAMPO2_AR_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_RETABEL2='AR')
    endwith
   endif
  endfunc

  func oCAMPO2_AR_1_13.mHide()
    with this.Parent.oContained
      return (.w_RETABEL2<>'AR' or .w_REOPERAT='E'  or .w_REOPERAT='NE')
    endwith
  endfunc

  add object oREDESSIN_1_16 as StdField with uid="SBEGBTQDZC",rtseq=14,rtrep=.f.,;
    cFormVar = "w_REDESSIN", cQueryName = "REDESSIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 242223772,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=265, Top=15, InputMask=replicate('X',40)


  add object oRELIVCONF_1_18 as StdCombo with uid="DCNPCFADVN",rtseq=15,rtrep=.f.,left=225,top=359,width=75,height=22;
    , ToolTipText = "Se la condizione � vera, il nuovo livello di conformit� dell'ordine � il minimo tra questo valore e l'attuale";
    , HelpContextID = 29653444;
    , cFormVar="w_RELIVCONF",RowSource=""+"Verde,"+"Giallo,"+"Rosso,"+"Nero", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oRELIVCONF_1_18.RadioValue()
    return(iif(this.value =1,8,;
    iif(this.value =2,6,;
    iif(this.value =3,4,;
    iif(this.value =4,2,;
    0)))))
  endfunc
  func oRELIVCONF_1_18.GetRadio()
    this.Parent.oContained.w_RELIVCONF = this.RadioValue()
    return .t.
  endfunc

  func oRELIVCONF_1_18.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_RELIVCONF==8,1,;
      iif(this.Parent.oContained.w_RELIVCONF==6,2,;
      iif(this.Parent.oContained.w_RELIVCONF==4,3,;
      iif(this.Parent.oContained.w_RELIVCONF==2,4,;
      0))))
  endfunc


  add object oBtn_1_21 as StdButton with uid="ZMIZZNTUDJ",left=407, top=355, width=74,height=23,;
    caption="OK", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 115950874;
    , Caption='\<OK';
  , bGlobalFont=.t.

    proc oBtn_1_21.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_21.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_typebutton<>1)
     endwith
    endif
  endfunc


  add object oBtn_1_22 as StdButton with uid="JWIBHYOYEX",left=484, top=355, width=74,height=23,;
    caption="Annulla", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 206748934;
    , Caption='\<Annulla';
  , bGlobalFont=.t.

    proc oBtn_1_22.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_22.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_typebutton<>1)
     endwith
    endif
  endfunc


  add object oBtn_1_24 as StdButton with uid="QKVPYFEWXV",left=452, top=348, width=50,height=30,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 207619094;
  , bGlobalFont=.t.

    proc oBtn_1_24.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_24.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_typebutton<>10)
     endwith
    endif
  endfunc


  add object oBtn_1_25 as StdButton with uid="EQEYTXUCHR",left=508, top=348, width=50,height=30,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 108654010;
  , bGlobalFont=.t.

    proc oBtn_1_25.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_25.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_typebutton<>10)
     endwith
    endif
  endfunc


  add object oBtn_1_26 as StdButton with uid="RPQBTFGFJB",left=454, top=333, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 207619094;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_26.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_26.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_typebutton<>11)
     endwith
    endif
  endfunc


  add object oBtn_1_27 as StdButton with uid="WLJFRCCWNI",left=510, top=333, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 108654010;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_27.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_27.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_typebutton<>11)
     endwith
    endif
  endfunc

  add object oStr_1_14 as StdString with uid="KANBHPJMFY",Visible=.t., Left=50, Top=58,;
    Alignment=0, Width=31, Height=18,;
    Caption="Se"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="CBHKHSABAN",Visible=.t., Left=55, Top=361,;
    Alignment=0, Width=27, Height=19,;
    Caption="= >"  ;
    , FontName = "System", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_17 as StdString with uid="XYAWXMSKEC",Visible=.t., Left=89, Top=362,;
    Alignment=0, Width=118, Height=18,;
    Caption="livello di conformit�"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="IZCTEINTRT",Visible=.t., Left=3, Top=17,;
    Alignment=1, Width=250, Height=18,;
    Caption="Condizioni per l'applicabilit� della regola:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscp_kre','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
