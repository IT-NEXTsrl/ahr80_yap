* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_blb                                                        *
*              Lancio Backup                                                   *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_7]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-10-14                                                      *
* Last revis.: 2009-01-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_blb",oParentObject)
return(i_retval)

define class tgsut_blb as StdBatch
  * --- Local variables
  w_CONTA = 0
  w_LUN = space(1)
  w_MAR = space(1)
  w_MER = space(1)
  w_GIO = space(1)
  w_VEN = space(1)
  w_SAB = space(1)
  w_DOM = space(1)
  w_ORA = 0
  w_MIN = 0
  w_GIORNO = 0
  * --- WorkFile variables
  AHEUSRCO_idx=0
  AZIENDA_idx=0
  AZBACKUP_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica se esistono ancora utenti attivi sul database oltre all'utente attuale
    * --- Se utente amministratore verifico se lanciare il back up..
    if cp_IsAdministrator(.F.)
      this.w_GIORNO = Dow ( Date() )
      * --- Read from AZBACKUP
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AZBACKUP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AZBACKUP_idx,2],.t.,this.AZBACKUP_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AZLUNBCK,AZMARBCK,AZMERBCK,AZGIOBCK,AZVENBCK,AZSABBCK,AZDOMBCK,AZORABCK,AZMINBCK"+;
          " from "+i_cTable+" AZBACKUP where ";
              +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AZLUNBCK,AZMARBCK,AZMERBCK,AZGIOBCK,AZVENBCK,AZSABBCK,AZDOMBCK,AZORABCK,AZMINBCK;
          from (i_cTable) where;
              AZCODAZI = i_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_LUN = NVL(cp_ToDate(_read_.AZLUNBCK),cp_NullValue(_read_.AZLUNBCK))
        this.w_MAR = NVL(cp_ToDate(_read_.AZMARBCK),cp_NullValue(_read_.AZMARBCK))
        this.w_MER = NVL(cp_ToDate(_read_.AZMERBCK),cp_NullValue(_read_.AZMERBCK))
        this.w_GIO = NVL(cp_ToDate(_read_.AZGIOBCK),cp_NullValue(_read_.AZGIOBCK))
        this.w_VEN = NVL(cp_ToDate(_read_.AZVENBCK),cp_NullValue(_read_.AZVENBCK))
        this.w_SAB = NVL(cp_ToDate(_read_.AZSABBCK),cp_NullValue(_read_.AZSABBCK))
        this.w_DOM = NVL(cp_ToDate(_read_.AZDOMBCK),cp_NullValue(_read_.AZDOMBCK))
        this.w_ORA = NVL(cp_ToDate(_read_.AZORABCK),cp_NullValue(_read_.AZORABCK))
        this.w_MIN = NVL(cp_ToDate(_read_.AZMINBCK),cp_NullValue(_read_.AZMINBCK))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Verifico se ho passato l'rora....
      if HOUR(Datetime())*60+MINUTE(Datetime())>= this.w_ORA*60 + this.w_MIN
        * --- DOW restituisce 1 per Domenica, 2 per Luned�....
        if (this.w_GIORNO=1 And this.w_DOM="S") Or (this.w_GIORNO=2 And this.w_LUN="S") Or (this.w_GIORNO=3 And this.w_MAR="S") Or (this.w_GIORNO=4 And this.w_MER="S") Or (this.w_GIORNO=5 And this.w_GIO="S") Or (this.w_GIORNO=6 And this.w_VEN="S") Or (this.w_GIORNO=7 And this.w_SAB="S")
          * --- Select from AHEUSRCO
          i_nConn=i_TableProp[this.AHEUSRCO_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.AHEUSRCO_idx,2],.t.,this.AHEUSRCO_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" AHEUSRCO ";
                 ,"_Curs_AHEUSRCO")
          else
            select Count(*) As Conta from (i_cTable);
              into cursor _Curs_AHEUSRCO
          endif
          if used('_Curs_AHEUSRCO')
            select _Curs_AHEUSRCO
            locate for 1=1
            do while not(eof())
            this.w_CONTA = Nvl( _Curs_AHEUSRCO.CONTA , 0 )
              select _Curs_AHEUSRCO
              continue
            enddo
            use
          endif
          if this.w_CONTA=1
            * --- Lancia il back Up
            do GSUT_KBK with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='AHEUSRCO'
    this.cWorkTables[2]='AZIENDA'
    this.cWorkTables[3]='AZBACKUP'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_AHEUSRCO')
      use in _Curs_AHEUSRCO
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
