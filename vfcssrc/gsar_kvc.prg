* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_kvc                                                        *
*              Visualizza clienti e fornitori                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-12-04                                                      *
* Last revis.: 2014-06-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_kvc",oParentObject))

* --- Class definition
define class tgsar_kvc as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 653
  Height = 489+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-06-10"
  HelpContextID=48854889
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=29

  * --- Constant Properties
  _IDX = 0
  ZONE_IDX = 0
  NAZIONI_IDX = 0
  CACOCLFO_IDX = 0
  CATECOMM_IDX = 0
  LISTINI_IDX = 0
  PAG_AMEN_IDX = 0
  MASTRI_IDX = 0
  cPrg = "gsar_kvc"
  cComment = "Visualizza clienti e fornitori"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TIPO = space(1)
  o_TIPO = space(1)
  w_DATINIZ = ctod('  /  /  ')
  o_DATINIZ = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_obsodat1 = space(1)
  w_RAGSOC = space(60)
  w_LOCALI = space(30)
  w_PROVIN = space(2)
  w_PANNAZION = space(3)
  w_NOTE = space(0)
  w_PANCONSU = space(15)
  w_PANCONSU = space(15)
  w_CATCON = space(5)
  w_CATECOMM = space(3)
  w_CODLIS = space(5)
  w_PANCODZON = space(3)
  w_CODPAG = space(5)
  w_DESPAG = space(30)
  w_CODFIS = space(1)
  w_PARIVA = space(1)
  w_IRPEF_IRES = space(1)
  w_DESAPP = space(40)
  w_ANCODICE = space(15)
  w_ANTIPCON = space(1)
  w_RET = .F.
  w_DESCATCON = space(35)
  w_DESCATECOM = space(35)
  w_DESLIST = space(40)
  w_DESZONA = space(35)
  w_ARKVC_ZOOM = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_kvcPag1","gsar_kvc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Generale")
      .Pages(2).addobject("oPag","tgsar_kvcPag2","gsar_kvc",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Dettagli")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPO_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ARKVC_ZOOM = this.oPgFrm.Pages(1).oPag.ARKVC_ZOOM
    DoDefault()
    proc Destroy()
      this.w_ARKVC_ZOOM = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='ZONE'
    this.cWorkTables[2]='NAZIONI'
    this.cWorkTables[3]='CACOCLFO'
    this.cWorkTables[4]='CATECOMM'
    this.cWorkTables[5]='LISTINI'
    this.cWorkTables[6]='PAG_AMEN'
    this.cWorkTables[7]='MASTRI'
    return(this.OpenAllTables(7))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPO=space(1)
      .w_DATINIZ=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_obsodat1=space(1)
      .w_RAGSOC=space(60)
      .w_LOCALI=space(30)
      .w_PROVIN=space(2)
      .w_PANNAZION=space(3)
      .w_NOTE=space(0)
      .w_PANCONSU=space(15)
      .w_PANCONSU=space(15)
      .w_CATCON=space(5)
      .w_CATECOMM=space(3)
      .w_CODLIS=space(5)
      .w_PANCODZON=space(3)
      .w_CODPAG=space(5)
      .w_DESPAG=space(30)
      .w_CODFIS=space(1)
      .w_PARIVA=space(1)
      .w_IRPEF_IRES=space(1)
      .w_DESAPP=space(40)
      .w_ANCODICE=space(15)
      .w_ANTIPCON=space(1)
      .w_RET=.f.
      .w_DESCATCON=space(35)
      .w_DESCATECOM=space(35)
      .w_DESLIST=space(40)
      .w_DESZONA=space(35)
        .w_TIPO = 'T'
        .w_DATINIZ = i_datsys
        .w_OBTEST = .w_DATINIZ
          .DoRTCalc(4,4,.f.)
        .w_obsodat1 = 'N'
      .oPgFrm.Page1.oPag.ARKVC_ZOOM.Calculate()
        .DoRTCalc(6,9,.f.)
        if not(empty(.w_PANNAZION))
          .link_2_6('Full')
        endif
        .DoRTCalc(10,11,.f.)
        if not(empty(.w_PANCONSU))
          .link_2_11('Full')
        endif
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_PANCONSU))
          .link_2_12('Full')
        endif
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_CATCON))
          .link_2_14('Full')
        endif
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_CATECOMM))
          .link_2_16('Full')
        endif
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_CODLIS))
          .link_2_17('Full')
        endif
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_PANCODZON))
          .link_2_19('Full')
        endif
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_CODPAG))
          .link_2_24('Full')
        endif
          .DoRTCalc(18,18,.f.)
        .w_CODFIS = 'T'
        .w_PARIVA = 'T'
        .w_IRPEF_IRES = 'T'
          .DoRTCalc(22,22,.f.)
        .w_ANCODICE = .w_ARKVC_ZOOM.GetVar('ANCODICE')
        .w_ANTIPCON = .w_ARKVC_ZOOM.GetVar('ANTIPCON')
    endwith
    this.DoRTCalc(25,29,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_32.enabled = this.oPgFrm.Page2.oPag.oBtn_2_32.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_34.enabled = this.oPgFrm.Page2.oPag.oBtn_2_34.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_DATINIZ<>.w_DATINIZ
            .w_OBTEST = .w_DATINIZ
        endif
        .oPgFrm.Page1.oPag.ARKVC_ZOOM.Calculate()
        if .o_TIPO<>.w_TIPO
          .Calculate_RHTKMDTSHR()
        endif
        .DoRTCalc(4,22,.t.)
            .w_ANCODICE = .w_ARKVC_ZOOM.GetVar('ANCODICE')
            .w_ANTIPCON = .w_ARKVC_ZOOM.GetVar('ANTIPCON')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(25,29,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ARKVC_ZOOM.Calculate()
    endwith
  return

  proc Calculate_DOBRBAQOOH()
    with this
          * --- Attiva la prima pagina
          .oPgFrm.ActivePage = 1
    endwith
  endproc
  proc Calculate_RHTKMDTSHR()
    with this
          * --- Azzera categ. contabile
          .w_CATCON = IIF(.w_TIPO='F' AND ISALT(), space(5), .w_CATCON)
          .w_IRPEF_IRES = IIF(.w_TIPO='F', 'T', .w_IRPEF_IRES)
          .w_PANCONSU = space(15)
          .link_2_11('Full')
          .link_2_12('Full')
    endwith
  endproc
  proc Calculate_XFJIUGHTAT()
    with this
          * --- Doppio click
          .w_ret = opengest('A', IIF(.w_ANTIPCON='C', 'GSAR_ACL', 'GSAR_AFR'), 'ANTIPCON', .w_ANTIPCON, 'ANCODICE', .w_ANCODICE)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page2.oPag.oPANCONSU_2_11.enabled = this.oPgFrm.Page2.oPag.oPANCONSU_2_11.mCond()
    this.oPgFrm.Page2.oPag.oPANCONSU_2_12.enabled = this.oPgFrm.Page2.oPag.oPANCONSU_2_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_10.visible=!this.oPgFrm.Page1.oPag.oBtn_1_10.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_11.visible=!this.oPgFrm.Page1.oPag.oBtn_1_11.mHide()
    this.oPgFrm.Page2.oPag.oPANCONSU_2_11.visible=!this.oPgFrm.Page2.oPag.oPANCONSU_2_11.mHide()
    this.oPgFrm.Page2.oPag.oPANCONSU_2_12.visible=!this.oPgFrm.Page2.oPag.oPANCONSU_2_12.mHide()
    this.oPgFrm.Page2.oPag.oCATCON_2_14.visible=!this.oPgFrm.Page2.oPag.oCATCON_2_14.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_15.visible=!this.oPgFrm.Page2.oPag.oStr_2_15.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_30.visible=!this.oPgFrm.Page2.oPag.oStr_2_30.mHide()
    this.oPgFrm.Page2.oPag.oIRPEF_IRES_2_31.visible=!this.oPgFrm.Page2.oPag.oIRPEF_IRES_2_31.mHide()
    this.oPgFrm.Page2.oPag.oDESCATCON_2_35.visible=!this.oPgFrm.Page2.oPag.oDESCATCON_2_35.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Ricerca")
          .Calculate_DOBRBAQOOH()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.ARKVC_ZOOM.Event(cEvent)
        if lower(cEvent)==lower("w_ARKVC_ZOOM selected")
          .Calculate_XFJIUGHTAT()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PANNAZION
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NAZIONI_IDX,3]
    i_lTable = "NAZIONI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2], .t., this.NAZIONI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PANNAZION) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANZ',True,'NAZIONI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NACODNAZ like "+cp_ToStrODBC(trim(this.w_PANNAZION)+"%");

          i_ret=cp_SQL(i_nConn,"select NACODNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NACODNAZ","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NACODNAZ',trim(this.w_PANNAZION))
          select NACODNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NACODNAZ into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PANNAZION)==trim(_Link_.NACODNAZ) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PANNAZION) and !this.bDontReportError
            deferred_cp_zoom('NAZIONI','*','NACODNAZ',cp_AbsName(oSource.parent,'oPANNAZION_2_6'),i_cWhere,'GSAR_ANZ',"Nazioni",'GSAR_SCL.NAZIONI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',oSource.xKey(1))
            select NACODNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PANNAZION)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(this.w_PANNAZION);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',this.w_PANNAZION)
            select NACODNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PANNAZION = NVL(_Link_.NACODNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_PANNAZION = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])+'\'+cp_ToStr(_Link_.NACODNAZ,1)
      cp_ShowWarn(i_cKey,this.NAZIONI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PANNAZION Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PANCONSU
  func Link_2_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_lTable = "MASTRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2], .t., this.MASTRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PANCONSU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMC',True,'MASTRI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MCCODICE like "+cp_ToStrODBC(trim(this.w_PANCONSU)+"%");

          i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MCCODICE',trim(this.w_PANCONSU))
          select MCCODICE,MCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PANCONSU)==trim(_Link_.MCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStrODBC(trim(this.w_PANCONSU)+"%");

            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStr(trim(this.w_PANCONSU)+"%");

            select MCCODICE,MCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PANCONSU) and !this.bDontReportError
            deferred_cp_zoom('MASTRI','*','MCCODICE',cp_AbsName(oSource.parent,'oPANCONSU_2_11'),i_cWhere,'GSAR_AMC',"Mastri",'GSAR_SFR.MASTRI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',oSource.xKey(1))
            select MCCODICE,MCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PANCONSU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(this.w_PANCONSU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',this.w_PANCONSU)
            select MCCODICE,MCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PANCONSU = NVL(_Link_.MCCODICE,space(15))
      this.w_DESAPP = NVL(_Link_.MCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PANCONSU = space(15)
      endif
      this.w_DESAPP = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])+'\'+cp_ToStr(_Link_.MCCODICE,1)
      cp_ShowWarn(i_cKey,this.MASTRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PANCONSU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PANCONSU
  func Link_2_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_lTable = "MASTRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2], .t., this.MASTRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PANCONSU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMC',True,'MASTRI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MCCODICE like "+cp_ToStrODBC(trim(this.w_PANCONSU)+"%");

          i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MCCODICE',trim(this.w_PANCONSU))
          select MCCODICE,MCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PANCONSU)==trim(_Link_.MCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStrODBC(trim(this.w_PANCONSU)+"%");

            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStr(trim(this.w_PANCONSU)+"%");

            select MCCODICE,MCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PANCONSU) and !this.bDontReportError
            deferred_cp_zoom('MASTRI','*','MCCODICE',cp_AbsName(oSource.parent,'oPANCONSU_2_12'),i_cWhere,'GSAR_AMC',"Mastri",'GSAR_SCL.MASTRI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',oSource.xKey(1))
            select MCCODICE,MCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PANCONSU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(this.w_PANCONSU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',this.w_PANCONSU)
            select MCCODICE,MCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PANCONSU = NVL(_Link_.MCCODICE,space(15))
      this.w_DESAPP = NVL(_Link_.MCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PANCONSU = space(15)
      endif
      this.w_DESAPP = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])+'\'+cp_ToStr(_Link_.MCCODICE,1)
      cp_ShowWarn(i_cKey,this.MASTRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PANCONSU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATCON
  func Link_2_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CACOCLFO_IDX,3]
    i_lTable = "CACOCLFO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2], .t., this.CACOCLFO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AC2',True,'CACOCLFO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" C2CODICE like "+cp_ToStrODBC(trim(this.w_CATCON)+"%");

          i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by C2CODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'C2CODICE',trim(this.w_CATCON))
          select C2CODICE,C2DESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by C2CODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATCON)==trim(_Link_.C2CODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATCON) and !this.bDontReportError
            deferred_cp_zoom('CACOCLFO','*','C2CODICE',cp_AbsName(oSource.parent,'oCATCON_2_14'),i_cWhere,'GSAR_AC2',"Categorie contabili",'GSAR_SFR.CACOCLFO_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',oSource.xKey(1))
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(this.w_CATCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',this.w_CATCON)
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATCON = NVL(_Link_.C2CODICE,space(5))
      this.w_DESCATCON = NVL(_Link_.C2DESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATCON = space(5)
      endif
      this.w_DESCATCON = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])+'\'+cp_ToStr(_Link_.C2CODICE,1)
      cp_ShowWarn(i_cKey,this.CACOCLFO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATECOMM
  func Link_2_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATECOMM_IDX,3]
    i_lTable = "CATECOMM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2], .t., this.CATECOMM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATECOMM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACT',True,'CATECOMM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_CATECOMM)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_CATECOMM))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATECOMM)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATECOMM) and !this.bDontReportError
            deferred_cp_zoom('CATECOMM','*','CTCODICE',cp_AbsName(oSource.parent,'oCATECOMM_2_16'),i_cWhere,'GSAR_ACT',"Categorie commerciali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATECOMM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_CATECOMM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_CATECOMM)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATECOMM = NVL(_Link_.CTCODICE,space(3))
      this.w_DESCATECOM = NVL(_Link_.CTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATECOMM = space(3)
      endif
      this.w_DESCATECOM = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CATECOMM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATECOMM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODLIS
  func Link_2_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_CODLIS)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_CODLIS))
          select LSCODLIS,LSDESLIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oCODLIS_2_17'),i_cWhere,'GSAR_ALI',"Listini",'GSMA0KVL.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_CODLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_CODLIS)
            select LSCODLIS,LSDESLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLIST = NVL(_Link_.LSDESLIS,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODLIS = space(5)
      endif
      this.w_DESLIST = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PANCODZON
  func Link_2_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZONE_IDX,3]
    i_lTable = "ZONE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2], .t., this.ZONE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PANCODZON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AZO',True,'ZONE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ZOCODZON like "+cp_ToStrODBC(trim(this.w_PANCODZON)+"%");

          i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODTOBSO,ZODESZON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ZOCODZON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ZOCODZON',trim(this.w_PANCODZON))
          select ZOCODZON,ZODTOBSO,ZODESZON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ZOCODZON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PANCODZON)==trim(_Link_.ZOCODZON) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PANCODZON) and !this.bDontReportError
            deferred_cp_zoom('ZONE','*','ZOCODZON',cp_AbsName(oSource.parent,'oPANCODZON_2_19'),i_cWhere,'GSAR_AZO',"Zone",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODTOBSO,ZODESZON";
                     +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',oSource.xKey(1))
            select ZOCODZON,ZODTOBSO,ZODESZON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PANCODZON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODTOBSO,ZODESZON";
                   +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(this.w_PANCODZON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',this.w_PANCODZON)
            select ZOCODZON,ZODTOBSO,ZODESZON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PANCODZON = NVL(_Link_.ZOCODZON,space(3))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ZODTOBSO),ctod("  /  /  "))
      this.w_DESZONA = NVL(_Link_.ZODESZON,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PANCODZON = space(3)
      endif
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_DESZONA = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice zona inesistente oppure obsoleto")
        endif
        this.w_PANCODZON = space(3)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_DESZONA = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])+'\'+cp_ToStr(_Link_.ZOCODZON,1)
      cp_ShowWarn(i_cKey,this.ZONE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PANCODZON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODPAG
  func Link_2_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_lTable = "PAG_AMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2], .t., this.PAG_AMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APA',True,'PAG_AMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PACODICE like "+cp_ToStrODBC(trim(this.w_CODPAG)+"%");

          i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI,PADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PACODICE',trim(this.w_CODPAG))
          select PACODICE,PADESCRI,PADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODPAG)==trim(_Link_.PACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" PADESCRI like "+cp_ToStrODBC(trim(this.w_CODPAG)+"%");

            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI,PADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" PADESCRI like "+cp_ToStr(trim(this.w_CODPAG)+"%");

            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODPAG) and !this.bDontReportError
            deferred_cp_zoom('PAG_AMEN','*','PACODICE',cp_AbsName(oSource.parent,'oCODPAG_2_24'),i_cWhere,'GSAR_APA',"Pagamenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI,PADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',oSource.xKey(1))
            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI,PADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_CODPAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_CODPAG)
            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPAG = NVL(_Link_.PACODICE,space(5))
      this.w_DESPAG = NVL(_Link_.PADESCRI,space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.PADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODPAG = space(5)
      endif
      this.w_DESPAG = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice pagamento inesistente oppure obsoleto")
        endif
        this.w_CODPAG = space(5)
        this.w_DESPAG = space(30)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_AMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPO_1_1.RadioValue()==this.w_TIPO)
      this.oPgFrm.Page1.oPag.oTIPO_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINIZ_1_3.value==this.w_DATINIZ)
      this.oPgFrm.Page1.oPag.oDATINIZ_1_3.value=this.w_DATINIZ
    endif
    if not(this.oPgFrm.Page1.oPag.oobsodat1_1_6.RadioValue()==this.w_obsodat1)
      this.oPgFrm.Page1.oPag.oobsodat1_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oRAGSOC_2_1.value==this.w_RAGSOC)
      this.oPgFrm.Page2.oPag.oRAGSOC_2_1.value=this.w_RAGSOC
    endif
    if not(this.oPgFrm.Page2.oPag.oLOCALI_2_3.value==this.w_LOCALI)
      this.oPgFrm.Page2.oPag.oLOCALI_2_3.value=this.w_LOCALI
    endif
    if not(this.oPgFrm.Page2.oPag.oPROVIN_2_5.value==this.w_PROVIN)
      this.oPgFrm.Page2.oPag.oPROVIN_2_5.value=this.w_PROVIN
    endif
    if not(this.oPgFrm.Page2.oPag.oPANNAZION_2_6.value==this.w_PANNAZION)
      this.oPgFrm.Page2.oPag.oPANNAZION_2_6.value=this.w_PANNAZION
    endif
    if not(this.oPgFrm.Page2.oPag.oNOTE_2_10.value==this.w_NOTE)
      this.oPgFrm.Page2.oPag.oNOTE_2_10.value=this.w_NOTE
    endif
    if not(this.oPgFrm.Page2.oPag.oPANCONSU_2_11.value==this.w_PANCONSU)
      this.oPgFrm.Page2.oPag.oPANCONSU_2_11.value=this.w_PANCONSU
    endif
    if not(this.oPgFrm.Page2.oPag.oPANCONSU_2_12.value==this.w_PANCONSU)
      this.oPgFrm.Page2.oPag.oPANCONSU_2_12.value=this.w_PANCONSU
    endif
    if not(this.oPgFrm.Page2.oPag.oCATCON_2_14.value==this.w_CATCON)
      this.oPgFrm.Page2.oPag.oCATCON_2_14.value=this.w_CATCON
    endif
    if not(this.oPgFrm.Page2.oPag.oCATECOMM_2_16.value==this.w_CATECOMM)
      this.oPgFrm.Page2.oPag.oCATECOMM_2_16.value=this.w_CATECOMM
    endif
    if not(this.oPgFrm.Page2.oPag.oCODLIS_2_17.value==this.w_CODLIS)
      this.oPgFrm.Page2.oPag.oCODLIS_2_17.value=this.w_CODLIS
    endif
    if not(this.oPgFrm.Page2.oPag.oPANCODZON_2_19.value==this.w_PANCODZON)
      this.oPgFrm.Page2.oPag.oPANCODZON_2_19.value=this.w_PANCODZON
    endif
    if not(this.oPgFrm.Page2.oPag.oCODPAG_2_24.value==this.w_CODPAG)
      this.oPgFrm.Page2.oPag.oCODPAG_2_24.value=this.w_CODPAG
    endif
    if not(this.oPgFrm.Page2.oPag.oDESPAG_2_25.value==this.w_DESPAG)
      this.oPgFrm.Page2.oPag.oDESPAG_2_25.value=this.w_DESPAG
    endif
    if not(this.oPgFrm.Page2.oPag.oCODFIS_2_27.RadioValue()==this.w_CODFIS)
      this.oPgFrm.Page2.oPag.oCODFIS_2_27.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPARIVA_2_29.RadioValue()==this.w_PARIVA)
      this.oPgFrm.Page2.oPag.oPARIVA_2_29.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oIRPEF_IRES_2_31.RadioValue()==this.w_IRPEF_IRES)
      this.oPgFrm.Page2.oPag.oIRPEF_IRES_2_31.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDESAPP_2_33.value==this.w_DESAPP)
      this.oPgFrm.Page2.oPag.oDESAPP_2_33.value=this.w_DESAPP
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCATCON_2_35.value==this.w_DESCATCON)
      this.oPgFrm.Page2.oPag.oDESCATCON_2_35.value=this.w_DESCATCON
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCATECOM_2_36.value==this.w_DESCATECOM)
      this.oPgFrm.Page2.oPag.oDESCATECOM_2_36.value=this.w_DESCATECOM
    endif
    if not(this.oPgFrm.Page2.oPag.oDESLIST_2_37.value==this.w_DESLIST)
      this.oPgFrm.Page2.oPag.oDESLIST_2_37.value=this.w_DESLIST
    endif
    if not(this.oPgFrm.Page2.oPag.oDESZONA_2_38.value==this.w_DESZONA)
      this.oPgFrm.Page2.oPag.oDESZONA_2_38.value=this.w_DESZONA
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DATINIZ))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINIZ_1_3.SetFocus()
            i_bnoObbl = !empty(.w_DATINIZ)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_PANCODZON))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPANCODZON_2_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice zona inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_CODPAG))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODPAG_2_24.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice pagamento inesistente oppure obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPO = this.w_TIPO
    this.o_DATINIZ = this.w_DATINIZ
    return

enddefine

* --- Define pages as container
define class tgsar_kvcPag1 as StdContainer
  Width  = 649
  height = 489
  stdWidth  = 649
  stdheight = 489
  resizeXpos=577
  resizeYpos=380
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPO_1_1 as StdCombo with uid="AWOTDFPUPW",rtseq=1,rtrep=.f.,left=97,top=27,width=89,height=22;
    , ToolTipText = "Tipo intestatario";
    , HelpContextID = 43329738;
    , cFormVar="w_TIPO",RowSource=""+"Clienti,"+"Fornitori,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPO_1_1.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oTIPO_1_1.GetRadio()
    this.Parent.oContained.w_TIPO = this.RadioValue()
    return .t.
  endfunc

  func oTIPO_1_1.SetRadio()
    this.Parent.oContained.w_TIPO=trim(this.Parent.oContained.w_TIPO)
    this.value = ;
      iif(this.Parent.oContained.w_TIPO=='C',1,;
      iif(this.Parent.oContained.w_TIPO=='F',2,;
      iif(this.Parent.oContained.w_TIPO=='T',3,;
      0)))
  endfunc

  add object oDATINIZ_1_3 as StdField with uid="KXZLKOTRZF",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DATINIZ", cQueryName = "DATINIZ",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di riferimento sul controllo dell'obsolescenza",;
    HelpContextID = 79360458,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=374, Top=27

  add object oobsodat1_1_6 as StdCheck with uid="TANZHKUOWP",rtseq=5,rtrep=.f.,left=484, top=27, caption="Solo obsoleti",;
    ToolTipText = "Se attivo, visualizza solo obsoleti",;
    HelpContextID = 80552471,;
    cFormVar="w_obsodat1", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oobsodat1_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oobsodat1_1_6.GetRadio()
    this.Parent.oContained.w_obsodat1 = this.RadioValue()
    return .t.
  endfunc

  func oobsodat1_1_6.SetRadio()
    this.Parent.oContained.w_obsodat1=trim(this.Parent.oContained.w_obsodat1)
    this.value = ;
      iif(this.Parent.oContained.w_obsodat1=='S',1,;
      0)
  endfunc


  add object ARKVC_ZOOM as cp_zoombox with uid="NPTUYPIRAF",left=3, top=81, width=646,height=348,;
    caption='Object',;
   bGlobalFont=.t.,;
    cMenuFile="",bRetriveAllRows=.f.,cZoomFile="GSAR_KVC",bOptions=.f.,bAdvOptions=.f.,bQueryOnDblClick=.t.,cTable="CONTI",bReadOnly=.t.,bNoZoomGridShape=.f.,bQueryOnLoad=.f.,cZoomOnZoom="",;
    cEvent = "Ricerca",;
    nPag=1;
    , HelpContextID = 129142758


  add object oBtn_1_9 as StdButton with uid="PHEWMSSLEL",left=591, top=27, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=1;
    , ToolTipText = "Ricerca";
    , HelpContextID = 128067350;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      with this.Parent.oContained
        .NotifyEvent('Ricerca')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_10 as StdButton with uid="ARHYBOCNYM",left=15, top=439, width=48,height=45,;
    CpPicture="bmp\DETTAGLI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare il cliente/fornitore selezionato";
    , HelpContextID = 41476858;
    , caption='\<Visualizza';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      with this.Parent.oContained
        = opengest('A', IIF(.w_ANTIPCON='C', 'GSAR_ACL', 'GSAR_AFR'), 'ANTIPCON', .w_ANTIPCON, 'ANCODICE', .w_ANCODICE)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_10.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_ANCODICE))
      endwith
    endif
  endfunc

  func oBtn_1_10.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_ANCODICE))
     endwith
    endif
  endfunc


  add object oBtn_1_11 as StdButton with uid="QJTUZLZBDL",left=69, top=439, width=48,height=45,;
    CpPicture="bmp\Modifica.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per modificare il cliente/fornitore selezionato";
    , HelpContextID = 216415527;
    , caption='\<Modifica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      with this.Parent.oContained
        = opengest('M', IIF(.w_ANTIPCON='C', 'GSAR_ACL', 'GSAR_AFR'), 'ANTIPCON', .w_ANTIPCON, 'ANCODICE', .w_ANCODICE)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_11.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_ANCODICE))
      endwith
    endif
  endfunc

  func oBtn_1_11.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_ANCODICE))
     endwith
    endif
  endfunc


  add object oBtn_1_12 as StdButton with uid="TKCMXZXILU",left=591, top=439, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 41537466;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_2 as StdString with uid="XVUBYBVEQE",Visible=.t., Left=278, Top=28,;
    Alignment=1, Width=92, Height=18,;
    Caption="Data di controllo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="OUSKNANCJF",Visible=.t., Left=21, Top=28,;
    Alignment=1, Width=70, Height=18,;
    Caption="Intestatario:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsar_kvcPag2 as StdContainer
  Width  = 649
  height = 489
  stdWidth  = 649
  stdheight = 489
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oRAGSOC_2_1 as AH_SEARCHFLD with uid="XGRZEDLQSV",rtseq=6,rtrep=.f.,;
    cFormVar = "w_RAGSOC", cQueryName = "RAGSOC",;
    bObbl = .f. , nPag = 2, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Ragione sociale o parte di essa",;
    HelpContextID = 178372842,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=105, Top=30, InputMask=replicate('X',60)

  add object oLOCALI_2_3 as AH_SEARCHFLD with uid="HVRTTRPAYE",rtseq=7,rtrep=.f.,;
    cFormVar = "w_LOCALI", cQueryName = "LOCALI",;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Localit� o parte di essa",;
    HelpContextID = 82047818,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=105, Top=66, InputMask=replicate('X',30), bHasZoom = .t. 

  proc oLOCALI_2_3.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C"," ",".w_LOCALI")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oPROVIN_2_5 as StdField with uid="QWLVJQTZEM",rtseq=8,rtrep=.f.,;
    cFormVar = "w_PROVIN", cQueryName = "PROVIN",;
    bObbl = .f. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia",;
    HelpContextID = 118774,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=385, Top=66, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. 

  proc oPROVIN_2_5.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P"," "," ",".w_PROVIN")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oPANNAZION_2_6 as StdField with uid="AGVBJQHMYP",rtseq=9,rtrep=.f.,;
    cFormVar = "w_PANNAZION", cQueryName = "PANNAZION",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Nazione",;
    HelpContextID = 75910107,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=487, Top=66, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="NAZIONI", cZoomOnZoom="GSAR_ANZ", oKey_1_1="NACODNAZ", oKey_1_2="this.w_PANNAZION"

  func oPANNAZION_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oPANNAZION_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPANNAZION_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'NAZIONI','*','NACODNAZ',cp_AbsName(this.parent,'oPANNAZION_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANZ',"Nazioni",'GSAR_SCL.NAZIONI_VZM',this.parent.oContained
  endproc
  proc oPANNAZION_2_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NACODNAZ=this.parent.oContained.w_PANNAZION
     i_obj.ecpSave()
  endproc

  add object oNOTE_2_10 as AH_SEARCHMEMO with uid="YFIDICHIXZ",rtseq=10,rtrep=.f.,;
    cFormVar = "w_NOTE", cQueryName = "NOTE",;
    bObbl = .f. , nPag = 2, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Annotazioni o parte di esse",;
    HelpContextID = 43967274,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=105, Top=102

  add object oPANCONSU_2_11 as StdField with uid="EXMJWATBSW",rtseq=11,rtrep=.f.,;
    cFormVar = "w_PANCONSU", cQueryName = "PANCONSU",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Mastro",;
    HelpContextID = 5156683,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=106, Top=138, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="MASTRI", cZoomOnZoom="GSAR_AMC", oKey_1_1="MCCODICE", oKey_1_2="this.w_PANCONSU"

  func oPANCONSU_2_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPO='F')
    endwith
   endif
  endfunc

  func oPANCONSU_2_11.mHide()
    with this.Parent.oContained
      return (.w_TIPO<>'F')
    endwith
  endfunc

  func oPANCONSU_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oPANCONSU_2_11.ecpDrop(oSource)
    this.Parent.oContained.link_2_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPANCONSU_2_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MASTRI','*','MCCODICE',cp_AbsName(this.parent,'oPANCONSU_2_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMC',"Mastri",'GSAR_SFR.MASTRI_VZM',this.parent.oContained
  endproc
  proc oPANCONSU_2_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MCCODICE=this.parent.oContained.w_PANCONSU
     i_obj.ecpSave()
  endproc

  add object oPANCONSU_2_12 as StdField with uid="UCPMPLCJTH",rtseq=12,rtrep=.f.,;
    cFormVar = "w_PANCONSU", cQueryName = "PANCONSU",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Mastro di raggruppamento",;
    HelpContextID = 5156683,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=105, Top=138, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="MASTRI", cZoomOnZoom="GSAR_AMC", oKey_1_1="MCCODICE", oKey_1_2="this.w_PANCONSU"

  func oPANCONSU_2_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPO='C')
    endwith
   endif
  endfunc

  func oPANCONSU_2_12.mHide()
    with this.Parent.oContained
      return (.w_TIPO='F')
    endwith
  endfunc

  func oPANCONSU_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oPANCONSU_2_12.ecpDrop(oSource)
    this.Parent.oContained.link_2_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPANCONSU_2_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MASTRI','*','MCCODICE',cp_AbsName(this.parent,'oPANCONSU_2_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMC',"Mastri",'GSAR_SCL.MASTRI_VZM',this.parent.oContained
  endproc
  proc oPANCONSU_2_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MCCODICE=this.parent.oContained.w_PANCONSU
     i_obj.ecpSave()
  endproc

  add object oCATCON_2_14 as StdField with uid="QHNOVRYKXP",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CATCON", cQueryName = "CATCON",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Categoria contabile",;
    HelpContextID = 5180966,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=105, Top=174, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CACOCLFO", cZoomOnZoom="GSAR_AC2", oKey_1_1="C2CODICE", oKey_1_2="this.w_CATCON"

  func oCATCON_2_14.mHide()
    with this.Parent.oContained
      return (.w_TIPO='F' AND ISALT())
    endwith
  endfunc

  func oCATCON_2_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATCON_2_14.ecpDrop(oSource)
    this.Parent.oContained.link_2_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATCON_2_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CACOCLFO','*','C2CODICE',cp_AbsName(this.parent,'oCATCON_2_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AC2',"Categorie contabili",'GSAR_SFR.CACOCLFO_VZM',this.parent.oContained
  endproc
  proc oCATCON_2_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AC2()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_C2CODICE=this.parent.oContained.w_CATCON
     i_obj.ecpSave()
  endproc

  add object oCATECOMM_2_16 as StdField with uid="FNNUKUNBXH",rtseq=14,rtrep=.f.,;
    cFormVar = "w_CATECOMM", cQueryName = "CATECOMM",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Categoria commerciale",;
    HelpContextID = 258929037,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=105, Top=210, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CATECOMM", cZoomOnZoom="GSAR_ACT", oKey_1_1="CTCODICE", oKey_1_2="this.w_CATECOMM"

  func oCATECOMM_2_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATECOMM_2_16.ecpDrop(oSource)
    this.Parent.oContained.link_2_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATECOMM_2_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATECOMM','*','CTCODICE',cp_AbsName(this.parent,'oCATECOMM_2_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACT',"Categorie commerciali",'',this.parent.oContained
  endproc
  proc oCATECOMM_2_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_CATECOMM
     i_obj.ecpSave()
  endproc

  add object oCODLIS_2_17 as StdField with uid="FMDHNAULPH",rtseq=15,rtrep=.f.,;
    cFormVar = "w_CODLIS", cQueryName = "CODLIS",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice listino",;
    HelpContextID = 83303462,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=105, Top=246, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_CODLIS"

  func oCODLIS_2_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODLIS_2_17.ecpDrop(oSource)
    this.Parent.oContained.link_2_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODLIS_2_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oCODLIS_2_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"Listini",'GSMA0KVL.LISTINI_VZM',this.parent.oContained
  endproc
  proc oCODLIS_2_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LSCODLIS=this.parent.oContained.w_CODLIS
     i_obj.ecpSave()
  endproc

  add object oPANCODZON_2_19 as StdField with uid="INSDLAWTRZ",rtseq=16,rtrep=.f.,;
    cFormVar = "w_PANCODZON", cQueryName = "PANCODZON",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Codice zona inesistente oppure obsoleto",;
    ToolTipText = "Zona",;
    HelpContextID = 162614235,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=105, Top=282, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="ZONE", cZoomOnZoom="GSAR_AZO", oKey_1_1="ZOCODZON", oKey_1_2="this.w_PANCODZON"

  func oPANCODZON_2_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oPANCODZON_2_19.ecpDrop(oSource)
    this.Parent.oContained.link_2_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPANCODZON_2_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZONE','*','ZOCODZON',cp_AbsName(this.parent,'oPANCODZON_2_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AZO',"Zone",'',this.parent.oContained
  endproc
  proc oPANCODZON_2_19.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AZO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ZOCODZON=this.parent.oContained.w_PANCODZON
     i_obj.ecpSave()
  endproc

  add object oCODPAG_2_24 as StdField with uid="AJEOCEYFRL",rtseq=17,rtrep=.f.,;
    cFormVar = "w_CODPAG", cQueryName = "CODPAG",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice pagamento inesistente oppure obsoleto",;
    ToolTipText = "Codice pagamento",;
    HelpContextID = 126149594,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=105, Top=318, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="PAG_AMEN", cZoomOnZoom="GSAR_APA", oKey_1_1="PACODICE", oKey_1_2="this.w_CODPAG"

  func oCODPAG_2_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODPAG_2_24.ecpDrop(oSource)
    this.Parent.oContained.link_2_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODPAG_2_24.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PAG_AMEN','*','PACODICE',cp_AbsName(this.parent,'oCODPAG_2_24'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APA',"Pagamenti",'',this.parent.oContained
  endproc
  proc oCODPAG_2_24.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PACODICE=this.parent.oContained.w_CODPAG
     i_obj.ecpSave()
  endproc

  add object oDESPAG_2_25 as StdField with uid="RVIOIHAFPZ",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DESPAG", cQueryName = "DESPAG",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 126090698,;
   bGlobalFont=.t.,;
    Height=21, Width=373, Left=165, Top=318, InputMask=replicate('X',30)


  add object oCODFIS_2_27 as StdCombo with uid="FJCSDCFAES",rtseq=19,rtrep=.f.,left=105,top=359,width=79,height=22;
    , HelpContextID = 82910246;
    , cFormVar="w_CODFIS",RowSource=""+"Tutti,"+"Assente,"+"Valorizzato", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oCODFIS_2_27.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'A',;
    iif(this.value =3,'V',;
    space(1)))))
  endfunc
  func oCODFIS_2_27.GetRadio()
    this.Parent.oContained.w_CODFIS = this.RadioValue()
    return .t.
  endfunc

  func oCODFIS_2_27.SetRadio()
    this.Parent.oContained.w_CODFIS=trim(this.Parent.oContained.w_CODFIS)
    this.value = ;
      iif(this.Parent.oContained.w_CODFIS=='T',1,;
      iif(this.Parent.oContained.w_CODFIS=='A',2,;
      iif(this.Parent.oContained.w_CODFIS=='V',3,;
      0)))
  endfunc


  add object oPARIVA_2_29 as StdCombo with uid="GKYMTEVGYN",rtseq=20,rtrep=.f.,left=275,top=359,width=79,height=22;
    , HelpContextID = 205197578;
    , cFormVar="w_PARIVA",RowSource=""+"Tutti,"+"Assente,"+"Valorizzato", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oPARIVA_2_29.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'A',;
    iif(this.value =3,'V',;
    space(1)))))
  endfunc
  func oPARIVA_2_29.GetRadio()
    this.Parent.oContained.w_PARIVA = this.RadioValue()
    return .t.
  endfunc

  func oPARIVA_2_29.SetRadio()
    this.Parent.oContained.w_PARIVA=trim(this.Parent.oContained.w_PARIVA)
    this.value = ;
      iif(this.Parent.oContained.w_PARIVA=='T',1,;
      iif(this.Parent.oContained.w_PARIVA=='A',2,;
      iif(this.Parent.oContained.w_PARIVA=='V',3,;
      0)))
  endfunc


  add object oIRPEF_IRES_2_31 as StdCombo with uid="HFNNAYGTVI",rtseq=21,rtrep=.f.,left=459,top=359,width=79,height=22;
    , HelpContextID = 255772888;
    , cFormVar="w_IRPEF_IRES",RowSource=""+"Tutti,"+"Non attivo,"+"Attivo", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oIRPEF_IRES_2_31.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'N',;
    iif(this.value =3,'S',;
    space(1)))))
  endfunc
  func oIRPEF_IRES_2_31.GetRadio()
    this.Parent.oContained.w_IRPEF_IRES = this.RadioValue()
    return .t.
  endfunc

  func oIRPEF_IRES_2_31.SetRadio()
    this.Parent.oContained.w_IRPEF_IRES=trim(this.Parent.oContained.w_IRPEF_IRES)
    this.value = ;
      iif(this.Parent.oContained.w_IRPEF_IRES=='T',1,;
      iif(this.Parent.oContained.w_IRPEF_IRES=='N',2,;
      iif(this.Parent.oContained.w_IRPEF_IRES=='S',3,;
      0)))
  endfunc

  func oIRPEF_IRES_2_31.mHide()
    with this.Parent.oContained
      return (.w_TIPO='F')
    endwith
  endfunc


  add object oBtn_2_32 as StdButton with uid="TMRCZGTRFE",left=590, top=30, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=2;
    , ToolTipText = "Ricerca";
    , HelpContextID = 128067350;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_32.Click()
      with this.Parent.oContained
        .NotifyEvent('Ricerca')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESAPP_2_33 as StdField with uid="LINZYCPIKP",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DESAPP", cQueryName = "DESAPP",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 39649846,;
   bGlobalFont=.t.,;
    Height=21, Width=297, Left=241, Top=138, InputMask=replicate('X',40)


  add object oBtn_2_34 as StdButton with uid="IWSVPTEFDG",left=590, top=439, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 41537466;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_34.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCATCON_2_35 as StdField with uid="KICGVSQLWR",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DESCATCON", cQueryName = "DESCATCON",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 91162469,;
   bGlobalFont=.t.,;
    Height=21, Width=373, Left=165, Top=174, InputMask=replicate('X',35)

  func oDESCATCON_2_35.mHide()
    with this.Parent.oContained
      return (.w_TIPO='F' AND ISALT())
    endwith
  endfunc

  add object oDESCATECOM_2_36 as StdField with uid="FGAWKLAZZS",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DESCATECOM", cQueryName = "DESCATECOM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 91182185,;
   bGlobalFont=.t.,;
    Height=21, Width=373, Left=165, Top=210, InputMask=replicate('X',35)

  add object oDESLIST_2_37 as StdField with uid="HTQZWEQGZJ",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DESLIST", cQueryName = "DESLIST",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 83362358,;
   bGlobalFont=.t.,;
    Height=21, Width=373, Left=165, Top=246, InputMask=replicate('X',40)

  add object oDESZONA_2_38 as StdField with uid="BVUFAAWXBU",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESZONA", cQueryName = "DESZONA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 6685238,;
   bGlobalFont=.t.,;
    Height=21, Width=373, Left=165, Top=282, InputMask=replicate('X',35)

  add object oStr_2_2 as StdString with uid="PNJSMJLXUH",Visible=.t., Left=53, Top=66,;
    Alignment=1, Width=46, Height=18,;
    Caption="Localit�:"  ;
  , bGlobalFont=.t.

  add object oStr_2_4 as StdString with uid="AWGFOPHEQI",Visible=.t., Left=349, Top=66,;
    Alignment=1, Width=30, Height=18,;
    Caption="Prov.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_7 as StdString with uid="RDJAYBHXHJ",Visible=.t., Left=60, Top=282,;
    Alignment=1, Width=39, Height=15,;
    Caption="Zona:"  ;
  , bGlobalFont=.t.

  add object oStr_2_8 as StdString with uid="BPGJVPQSEI",Visible=.t., Left=423, Top=66,;
    Alignment=1, Width=60, Height=15,;
    Caption="Nazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_9 as StdString with uid="CHROHUGMQN",Visible=.t., Left=40, Top=30,;
    Alignment=1, Width=59, Height=18,;
    Caption="Rag. soc.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_13 as StdString with uid="BSGKMHWZHM",Visible=.t., Left=67, Top=102,;
    Alignment=1, Width=32, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_2_15 as StdString with uid="HGBTOEMDXR",Visible=.t., Left=19, Top=174,;
    Alignment=1, Width=80, Height=15,;
    Caption="Cat.contabile:"  ;
  , bGlobalFont=.t.

  func oStr_2_15.mHide()
    with this.Parent.oContained
      return (.w_TIPO='F' AND ISALT())
    endwith
  endfunc

  add object oStr_2_18 as StdString with uid="PKWGDEMLRM",Visible=.t., Left=-6, Top=210,;
    Alignment=1, Width=105, Height=18,;
    Caption="Cat.commerciale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_21 as StdString with uid="PNUJVVEEQQ",Visible=.t., Left=6, Top=138,;
    Alignment=1, Width=93, Height=18,;
    Caption="Mastro contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_2_22 as StdString with uid="SXUTVOFRRA",Visible=.t., Left=34, Top=246,;
    Alignment=1, Width=65, Height=18,;
    Caption="Cod. listino:"  ;
  , bGlobalFont=.t.

  add object oStr_2_23 as StdString with uid="THBKHHYACH",Visible=.t., Left=4, Top=318,;
    Alignment=1, Width=95, Height=18,;
    Caption="Cod. pagamento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_26 as StdString with uid="RJJRCKAYBC",Visible=.t., Left=30, Top=360,;
    Alignment=1, Width=68, Height=18,;
    Caption="Cod. fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_28 as StdString with uid="IFJFNCKTFW",Visible=.t., Left=211, Top=360,;
    Alignment=1, Width=58, Height=18,;
    Caption="Partita IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_2_30 as StdString with uid="KJOBWJXRIV",Visible=.t., Left=383, Top=360,;
    Alignment=1, Width=69, Height=18,;
    Caption="IRPEF/IRES:"  ;
  , bGlobalFont=.t.

  func oStr_2_30.mHide()
    with this.Parent.oContained
      return (.w_TIPO='F')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_kvc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
