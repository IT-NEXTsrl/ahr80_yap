* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bbp                                                        *
*              Controllo partite aperte                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-06-08                                                      *
* Last revis.: 2009-01-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bbp",oParentObject)
return(i_retval)

define class tgsar_bbp as StdBatch
  * --- Local variables
  w_MESG = space(500)
  * --- WorkFile variables
  OFF_NOMI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se � stata modificata la data di obsolescenza si avvisa l utente se ci sono dei beneficiari che lo hanno come conto collegato 
    this.bUpdateParentObject=.F.
    if this.oParentObject.w_NOFLGBEN="B"
      this.w_MESG = ""
      vq_exec("QUERY\GSAR_BBP.VQR",this,"__TMP__")
      if RECCOUNT ("__TMP__") >0
        if AH_YESNO ("Il conto associato al deb./cred. diverso � obsoleto.%0Si vuole visualizzare l'elenco delle partite aperte aventi questo conto collegato ?")
          SELECT "__TMP__"
          CP_CHPRN("QUERY\GSAR_BBP.frx","",this.oParentObject)
        endif
      else
        ah_errormsg("Il conto associato al deb./cred. diverso � obsoleto")
      endif
      USE IN SELECT (" __TMP__")
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='OFF_NOMI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
