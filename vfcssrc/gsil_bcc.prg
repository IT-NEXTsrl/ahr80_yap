* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsil_bcc                                                        *
*              Importa definizionecampi                                        *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-02-06                                                      *
* Last revis.: 2005-12-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsil_bcc",oParentObject)
return(i_retval)

define class tgsil_bcc as StdBatch
  * --- Local variables
  * --- WorkFile variables
  STRUTABE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if ah_yesno("Allineare la lista campi all'ultima importazione effettuata?")
      Select (this.oParentObject.cTrsName)
      Delete All
      * --- Select from gsil_bcc
      do vq_exec with 'gsil_bcc',this,'_Curs_gsil_bcc','',.f.,.t.
      if used('_Curs_gsil_bcc')
        select _Curs_gsil_bcc
        locate for 1=1
        do while not(eof())
        this.oParentObject.InitRow()
        this.oParentObject.w_STNOMTAB = _Curs_GSIL_BCC.STNOMTAB
        this.oParentObject.w_STNOMCAM = _Curs_GSIL_BCC.STNOMCAM
        this.oParentObject.w_STDESCAM = _Curs_GSIL_BCC.STDESCAM
        this.oParentObject.w_ST__TIPO = _Curs_GSIL_BCC.ST__TIPO
        this.oParentObject.w_ST__LUNG = _Curs_GSIL_BCC.ST__LUNG
        this.oParentObject.w_ST__NDEC = _Curs_GSIL_BCC.ST__NDEC
        this.oParentObject.TrsFromWork()
          select _Curs_gsil_bcc
          continue
        enddo
        use
      endif
      SELECT (this.oParentObject.cTrsName)
      GO TOP
      * --- Aggiorno la visualizzazione
      With this.oParentObject
      .WorkFromTrs()
      .Refresh()
      EndWith
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='STRUTABE'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_gsil_bcc')
      use in _Curs_gsil_bcc
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
