* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bfx                                                        *
*              Importazione da foglio Excel                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-01-04                                                      *
* Last revis.: 2017-10-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCursore,pTXTFile
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bfx",oParentObject,m.pCursore,m.pTXTFile)
return(i_retval)

define class tgsar_bfx as StdBatch
  * --- Local variables
  pCursore = space(10)
  pTXTFile = space(254)
  nConn = 0
  err = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Conterr� il valore della connessione ODBC
    this.err = 0
    * --- Creo la Connessione al Foglio Excel...
    if TYPE ("g_EXCELSTRINGCONN")="C" AND not empty (g_EXCELSTRINGCONN)
      this.nConn = SQLSTRINGCONNECT( STRTRAN ( STRTRAN(g_EXCELSTRINGCONN,"<NOMEFILE>", this.pTXTFile ), "<NOMEDIR>", JustPath(this.pTXTFILE) ) )
    else
      this.nConn = SQLSTRINGCONNECT("Driver=Microsoft Excel Driver (*.xls, *.xlsx, *.xlsm, *.xlsb);DBQ="+this.pTXTFile+";DefaultDir="+JustPath(this.pTXTFILE)+";DriverId=790;FIL=excel 8.0;MaxBufferSize=2048;PageTimeout=5;")
    endif
    if this.nConn>0
      * --- Eseguo la query...
      *     Il riferimento alla pagina del foglio Excel avviene sempre e solo per nome...
      *     A causa di problemi con le gestione degli handle dei processi aperti, la 
      *     lettura dei dati dal foglio Excel necessita che la pagina sorgente sia nominata
      *     come "Foglio1"
      *     Cambiando il nome alla pagina di riferimento, la connessione restituir� un errore.
      this.err = SQLEXEC(this.nConn, "select * from [Foglio1$]" ,this.pCursore)
      if this.err = -1
        this.err = SQLEXEC(this.nConn, "select * from [Tabella1$]" ,this.pCursore)
      endif
      * --- Chiusura della connessione al foglio Excel
      SQLCANCEL(this.nConn)
      SQLDISCONNECT(this.nConn)
    else
      this.err = -1
    endif
    i_retcode = 'stop'
    i_retval = this.Err
    return
  endproc


  proc Init(oParentObject,pCursore,pTXTFile)
    this.pCursore=pCursore
    this.pTXTFile=pTXTFile
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCursore,pTXTFile"
endproc
