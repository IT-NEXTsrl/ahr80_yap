* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_szm                                                        *
*              Visualizzazione documenti                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_116]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-15                                                      *
* Last revis.: 2014-07-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsve_szm",oParentObject))

* --- Class definition
define class tgsve_szm as StdForm
  Top    = 0
  Left   = 3

  * --- Standard Properties
  Width  = 784
  Height = 475+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-07-14"
  HelpContextID=26594711
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=59

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  ART_ICOL_IDX = 0
  TIP_DOCU_IDX = 0
  MAGAZZIN_IDX = 0
  CAM_AGAZ_IDX = 0
  CAN_TIER_IDX = 0
  AGENTI_IDX = 0
  ATTIVITA_IDX = 0
  VOC_COST_IDX = 0
  CENCOST_IDX = 0
  cPrg = "gsve_szm"
  cComment = "Visualizzazione documenti"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_CATDO1 = space(2)
  w_CATDO2 = space(2)
  w_CATDOC = space(2)
  w_FLVEAC = space(1)
  w_FILDOC = space(2)
  w_TIPDOC = space(5)
  w_DESDOC = space(35)
  w_TCATDOC = space(2)
  w_TFLVEAC = space(1)
  w_CODART = space(20)
  o_CODART = space(20)
  w_DESART = space(40)
  w_CODMAG = space(5)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_DOCINI = ctod('  /  /  ')
  w_DOCFIN = ctod('  /  /  ')
  w_SERIALE = space(10)
  w_TOTCAR = 0
  w_DESCAU = space(35)
  w_TOTSCA = 0
  w_NUMROW = 0
  w_NUMRIF = 0
  w_SERIALE = space(10)
  w_PARAME = space(3)
  w_DESAPP = space(30)
  w_CLIFOR = space(40)
  w_UNIMIS = space(3)
  w_MAGCLI = space(1)
  w_CLISEL = space(15)
  o_CLISEL = space(15)
  w_MAGFOR = space(1)
  w_FORSEL = space(15)
  w_MAGALT = space(1)
  w_DESCLI = space(40)
  w_DESFOR = space(40)
  w_CAUSEL = space(5)
  w_DESCAU2 = space(35)
  w_AGESEL = space(5)
  w_DESAGE = space(35)
  w_TIPATT = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_DESCOM = space(30)
  w_TIPVOC = space(1)
  w_VOCSEL = space(15)
  w_DESVOC = space(40)
  w_CENSEL = space(15)
  w_DESCEN = space(40)
  w_COMMESSA = space(15)
  w_DATOBSF = ctod('  /  /  ')
  w_TIPEVA = space(1)
  w_VISIBLE = .F.
  w_CATDO2 = space(2)
  w_SERDOCU = space(10)
  w_CONTRA = space(10)
  w_ELCODMOD = space(10)
  w_ELCODIMP = space(10)
  w_ELCODCOM = 0
  w_ELRINNOV = 0
  w_ZoomDoc = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_szmPag1","gsve_szm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(2).addobject("oPag","tgsve_szmPag2","gsve_szm",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni aggiuntive")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCATDO1_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomDoc = this.oPgFrm.Pages(1).oPag.ZoomDoc
    DoDefault()
    proc Destroy()
      this.w_ZoomDoc = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[10]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='TIP_DOCU'
    this.cWorkTables[4]='MAGAZZIN'
    this.cWorkTables[5]='CAM_AGAZ'
    this.cWorkTables[6]='CAN_TIER'
    this.cWorkTables[7]='AGENTI'
    this.cWorkTables[8]='ATTIVITA'
    this.cWorkTables[9]='VOC_COST'
    this.cWorkTables[10]='CENCOST'
    return(this.OpenAllTables(10))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_CATDO1=space(2)
      .w_CATDO2=space(2)
      .w_CATDOC=space(2)
      .w_FLVEAC=space(1)
      .w_FILDOC=space(2)
      .w_TIPDOC=space(5)
      .w_DESDOC=space(35)
      .w_TCATDOC=space(2)
      .w_TFLVEAC=space(1)
      .w_CODART=space(20)
      .w_DESART=space(40)
      .w_CODMAG=space(5)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_DOCINI=ctod("  /  /  ")
      .w_DOCFIN=ctod("  /  /  ")
      .w_SERIALE=space(10)
      .w_TOTCAR=0
      .w_DESCAU=space(35)
      .w_TOTSCA=0
      .w_NUMROW=0
      .w_NUMRIF=0
      .w_SERIALE=space(10)
      .w_PARAME=space(3)
      .w_DESAPP=space(30)
      .w_CLIFOR=space(40)
      .w_UNIMIS=space(3)
      .w_MAGCLI=space(1)
      .w_CLISEL=space(15)
      .w_MAGFOR=space(1)
      .w_FORSEL=space(15)
      .w_MAGALT=space(1)
      .w_DESCLI=space(40)
      .w_DESFOR=space(40)
      .w_CAUSEL=space(5)
      .w_DESCAU2=space(35)
      .w_AGESEL=space(5)
      .w_DESAGE=space(35)
      .w_TIPATT=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_DESCOM=space(30)
      .w_TIPVOC=space(1)
      .w_VOCSEL=space(15)
      .w_DESVOC=space(40)
      .w_CENSEL=space(15)
      .w_DESCEN=space(40)
      .w_COMMESSA=space(15)
      .w_DATOBSF=ctod("  /  /  ")
      .w_TIPEVA=space(1)
      .w_VISIBLE=.f.
      .w_CATDO2=space(2)
      .w_SERDOCU=space(10)
      .w_CONTRA=space(10)
      .w_ELCODMOD=space(10)
      .w_ELCODIMP=space(10)
      .w_ELCODCOM=0
      .w_ELRINNOV=0
        .w_CODAZI = i_CODAZI
        .w_CATDO1 = 'XX'
        .w_CATDO2 = 'XX'
        .w_CATDOC = IIF(g_ACQU='S', .w_CATDO1, IIF(.w_CATDO2='IF', 'DI', IIF(.w_CATDO2='DF', 'DT', .w_CATDO2)))
        .w_FLVEAC = IIF(g_ACQU='S', 'V', IIF(.w_CATDO2 $ 'DF-IF', 'A', IIF(.w_CATDO2='XX', ' ', 'V')))
        .w_FILDOC = IIF(.w_CATDOC='XX', SPACE(2), .w_CATDOC)
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_TIPDOC))
          .link_1_8('Full')
        endif
        .DoRTCalc(8,11,.f.)
        if not(empty(.w_CODART))
          .link_1_12('Full')
        endif
          .DoRTCalc(12,12,.f.)
        .w_CODMAG = iif(IsAlt(), space(5), g_MAGAZI)
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_CODMAG))
          .link_1_14('Full')
        endif
          .DoRTCalc(14,15,.f.)
        .w_DOCINI = g_INIESE
        .w_DOCFIN = g_FINESE
      .oPgFrm.Page1.oPag.ZoomDoc.Calculate()
        .w_SERIALE = .w_ZoomDoc.getVar('SERIAL')
        .w_TOTCAR = 0
        .w_DESCAU = .w_ZoomDoc.getVar('DESCAU')
        .w_TOTSCA = 0
        .w_NUMROW = .w_ZoomDoc.getVar('CPROWNUM')
        .w_NUMRIF = .w_ZoomDoc.getVar('NUMRIF')
        .w_SERIALE = .w_ZoomDoc.getVar('SERIAL')
        .w_PARAME = nvl(.w_ZoomDoc.getVar('FLVEAC'),"")+nvl(.w_ZoomDoc.getVar('CLADOC'),"")
          .DoRTCalc(26,26,.f.)
        .w_CLIFOR = .w_ZoomDoc.getVar('DESCON')
          .DoRTCalc(28,28,.f.)
        .w_MAGCLI = 'C'
        .DoRTCalc(30,30,.f.)
        if not(empty(.w_CLISEL))
          .link_2_2('Full')
        endif
        .w_MAGFOR = 'X'
        .DoRTCalc(32,32,.f.)
        if not(empty(.w_FORSEL))
          .link_2_4('Full')
        endif
        .w_MAGALT = iif(IsAlt() AND EMPTY(.w_CLISEL), 'N', 'X')
        .DoRTCalc(34,36,.f.)
        if not(empty(.w_CAUSEL))
          .link_2_8('Full')
        endif
        .DoRTCalc(37,38,.f.)
        if not(empty(.w_AGESEL))
          .link_2_10('Full')
        endif
          .DoRTCalc(39,39,.f.)
        .w_TIPATT = 'A'
        .w_OBTEST = i_INIDAT
      .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        .DoRTCalc(42,45,.f.)
        if not(empty(.w_VOCSEL))
          .link_2_20('Full')
        endif
        .DoRTCalc(46,47,.f.)
        if not(empty(.w_CENSEL))
          .link_2_22('Full')
        endif
        .DoRTCalc(48,49,.f.)
        if not(empty(.w_COMMESSA))
          .link_2_25('Full')
        endif
          .DoRTCalc(50,50,.f.)
        .w_TIPEVA = 'T'
        .w_VISIBLE = .T.
      .oPgFrm.Page1.oPag.oObj_1_45.Calculate()
        .w_CATDO2 = 'XX'
      .oPgFrm.Page1.oPag.oObj_1_51.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_59.Calculate()
    endwith
    this.DoRTCalc(54,59,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_46.enabled = this.oPgFrm.Page1.oPag.oBtn_1_46.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_47.enabled = this.oPgFrm.Page1.oPag.oBtn_1_47.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_48.enabled = this.oPgFrm.Page1.oPag.oBtn_1_48.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
            .w_CATDOC = IIF(g_ACQU='S', .w_CATDO1, IIF(.w_CATDO2='IF', 'DI', IIF(.w_CATDO2='DF', 'DT', .w_CATDO2)))
            .w_FLVEAC = IIF(g_ACQU='S', 'V', IIF(.w_CATDO2 $ 'DF-IF', 'A', IIF(.w_CATDO2='XX', ' ', 'V')))
            .w_FILDOC = IIF(.w_CATDOC='XX', SPACE(2), .w_CATDOC)
        .oPgFrm.Page1.oPag.ZoomDoc.Calculate()
        .DoRTCalc(7,17,.t.)
            .w_SERIALE = .w_ZoomDoc.getVar('SERIAL')
        .DoRTCalc(19,19,.t.)
            .w_DESCAU = .w_ZoomDoc.getVar('DESCAU')
        .DoRTCalc(21,21,.t.)
            .w_NUMROW = .w_ZoomDoc.getVar('CPROWNUM')
            .w_NUMRIF = .w_ZoomDoc.getVar('NUMRIF')
            .w_SERIALE = .w_ZoomDoc.getVar('SERIAL')
            .w_PARAME = nvl(.w_ZoomDoc.getVar('FLVEAC'),"")+nvl(.w_ZoomDoc.getVar('CLADOC'),"")
        .DoRTCalc(26,26,.t.)
            .w_CLIFOR = .w_ZoomDoc.getVar('DESCON')
        .DoRTCalc(28,32,.t.)
        if .o_CLISEL<>.w_CLISEL
            .w_MAGALT = iif(IsAlt() AND EMPTY(.w_CLISEL), 'N', 'X')
        endif
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_45.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_51.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_59.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(34,59,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomDoc.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_45.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_51.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_59.Calculate()
    endwith
  return

  proc Calculate_FHNOTOXVEF()
    with this
          * --- Carica temporaneo
          gsve_bvm(this;
              ,"InitV";
             )
    endwith
  endproc
  proc Calculate_TJQIARQJEM()
    with this
          * --- Done
          gsve_bvm(this;
              ,"DoneV";
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCATDO1_1_3.enabled = this.oPgFrm.Page1.oPag.oCATDO1_1_3.mCond()
    this.oPgFrm.Page1.oPag.oCATDO2_1_4.enabled = this.oPgFrm.Page1.oPag.oCATDO2_1_4.mCond()
    this.oPgFrm.Page2.oPag.oCLISEL_2_2.enabled = this.oPgFrm.Page2.oPag.oCLISEL_2_2.mCond()
    this.oPgFrm.Page2.oPag.oMAGFOR_2_3.enabled = this.oPgFrm.Page2.oPag.oMAGFOR_2_3.mCond()
    this.oPgFrm.Page2.oPag.oFORSEL_2_4.enabled = this.oPgFrm.Page2.oPag.oFORSEL_2_4.mCond()
    this.oPgFrm.Page2.oPag.oVOCSEL_2_20.enabled = this.oPgFrm.Page2.oPag.oVOCSEL_2_20.mCond()
    this.oPgFrm.Page2.oPag.oCENSEL_2_22.enabled = this.oPgFrm.Page2.oPag.oCENSEL_2_22.mCond()
    this.oPgFrm.Page2.oPag.oCOMMESSA_2_25.enabled = this.oPgFrm.Page2.oPag.oCOMMESSA_2_25.mCond()
    this.oPgFrm.Page1.oPag.oCATDO2_1_49.enabled = this.oPgFrm.Page1.oPag.oCATDO2_1_49.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_46.enabled = this.oPgFrm.Page1.oPag.oBtn_1_46.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_60.enabled = this.oPgFrm.Page1.oPag.oBtn_1_60.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCATDO1_1_3.visible=!this.oPgFrm.Page1.oPag.oCATDO1_1_3.mHide()
    this.oPgFrm.Page1.oPag.oCATDO2_1_4.visible=!this.oPgFrm.Page1.oPag.oCATDO2_1_4.mHide()
    this.oPgFrm.Page1.oPag.oCODMAG_1_14.visible=!this.oPgFrm.Page1.oPag.oCODMAG_1_14.mHide()
    this.oPgFrm.Page1.oPag.oTOTCAR_1_21.visible=!this.oPgFrm.Page1.oPag.oTOTCAR_1_21.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_22.visible=!this.oPgFrm.Page1.oPag.oStr_1_22.mHide()
    this.oPgFrm.Page1.oPag.oTOTSCA_1_24.visible=!this.oPgFrm.Page1.oPag.oTOTSCA_1_24.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_25.visible=!this.oPgFrm.Page1.oPag.oStr_1_25.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_28.visible=!this.oPgFrm.Page1.oPag.oStr_1_28.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_29.visible=!this.oPgFrm.Page1.oPag.oStr_1_29.mHide()
    this.oPgFrm.Page1.oPag.oDESAPP_1_36.visible=!this.oPgFrm.Page1.oPag.oDESAPP_1_36.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_37.visible=!this.oPgFrm.Page1.oPag.oStr_1_37.mHide()
    this.oPgFrm.Page1.oPag.oUNIMIS_1_40.visible=!this.oPgFrm.Page1.oPag.oUNIMIS_1_40.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_41.visible=!this.oPgFrm.Page1.oPag.oStr_1_41.mHide()
    this.oPgFrm.Page2.oPag.oMAGFOR_2_3.visible=!this.oPgFrm.Page2.oPag.oMAGFOR_2_3.mHide()
    this.oPgFrm.Page2.oPag.oFORSEL_2_4.visible=!this.oPgFrm.Page2.oPag.oFORSEL_2_4.mHide()
    this.oPgFrm.Page2.oPag.oDESFOR_2_7.visible=!this.oPgFrm.Page2.oPag.oDESFOR_2_7.mHide()
    this.oPgFrm.Page2.oPag.oCAUSEL_2_8.visible=!this.oPgFrm.Page2.oPag.oCAUSEL_2_8.mHide()
    this.oPgFrm.Page2.oPag.oDESCAU2_2_9.visible=!this.oPgFrm.Page2.oPag.oDESCAU2_2_9.mHide()
    this.oPgFrm.Page2.oPag.oAGESEL_2_10.visible=!this.oPgFrm.Page2.oPag.oAGESEL_2_10.mHide()
    this.oPgFrm.Page2.oPag.oDESAGE_2_11.visible=!this.oPgFrm.Page2.oPag.oDESAGE_2_11.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_13.visible=!this.oPgFrm.Page2.oPag.oStr_2_13.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_14.visible=!this.oPgFrm.Page2.oPag.oStr_2_14.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_17.visible=!this.oPgFrm.Page2.oPag.oStr_2_17.mHide()
    this.oPgFrm.Page2.oPag.oDESCOM_2_18.visible=!this.oPgFrm.Page2.oPag.oDESCOM_2_18.mHide()
    this.oPgFrm.Page2.oPag.oVOCSEL_2_20.visible=!this.oPgFrm.Page2.oPag.oVOCSEL_2_20.mHide()
    this.oPgFrm.Page2.oPag.oDESVOC_2_21.visible=!this.oPgFrm.Page2.oPag.oDESVOC_2_21.mHide()
    this.oPgFrm.Page2.oPag.oCENSEL_2_22.visible=!this.oPgFrm.Page2.oPag.oCENSEL_2_22.mHide()
    this.oPgFrm.Page2.oPag.oDESCEN_2_23.visible=!this.oPgFrm.Page2.oPag.oDESCEN_2_23.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_24.visible=!this.oPgFrm.Page2.oPag.oStr_2_24.mHide()
    this.oPgFrm.Page2.oPag.oCOMMESSA_2_25.visible=!this.oPgFrm.Page2.oPag.oCOMMESSA_2_25.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_26.visible=!this.oPgFrm.Page2.oPag.oStr_2_26.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_43.visible=!this.oPgFrm.Page1.oPag.oStr_1_43.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_30.visible=!this.oPgFrm.Page2.oPag.oStr_2_30.mHide()
    this.oPgFrm.Page1.oPag.oCATDO2_1_49.visible=!this.oPgFrm.Page1.oPag.oCATDO2_1_49.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_50.visible=!this.oPgFrm.Page1.oPag.oStr_1_50.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("FormLoad")
          .Calculate_FHNOTOXVEF()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.ZoomDoc.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_42.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_45.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_51.Event(cEvent)
        if lower(cEvent)==lower("Done")
          .Calculate_TJQIARQJEM()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_59.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=TIPDOC
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_TIPDOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_TIPDOC))
          select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPDOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TIPDOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oTIPDOC_1_8'),i_cWhere,'GSVE_ATD',"Tipi documenti",'GSVE1QZM.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_TIPDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_TIPDOC)
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESDOC = NVL(_Link_.TDDESDOC,space(35))
      this.w_TCATDOC = NVL(_Link_.TDCATDOC,space(2))
      this.w_TFLVEAC = NVL(_Link_.TDFLVEAC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TIPDOC = space(5)
      endif
      this.w_DESDOC = space(35)
      this.w_TCATDOC = space(2)
      this.w_TFLVEAC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_TFLVEAC=.w_FLVEAC OR .w_FLVEAC=' ') AND (.w_TCATDOC=.w_CATDOC OR (.w_CATDOC='XX' AND .w_TCATDOC<>'OR'))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento incongruente")
        endif
        this.w_TIPDOC = space(5)
        this.w_DESDOC = space(35)
        this.w_TCATDOC = space(2)
        this.w_TFLVEAC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODART
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODART)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODART))
          select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODART)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_CODART)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_CODART)+"%");

            select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODART) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODART_1_12'),i_cWhere,'GSMA_BZA',"Codici articoli/servizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODART)
            select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODART = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
      this.w_UNIMIS = NVL(_Link_.ARUNMIS1,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CODART = space(20)
      endif
      this.w_DESART = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_UNIMIS = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice articolo inesistente oppure obsoleto")
        endif
        this.w_CODART = space(20)
        this.w_DESART = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_UNIMIS = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAG
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_CODMAG))
          select MGCODMAG,MGDESMAG,MGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStr(trim(this.w_CODMAG)+"%");

            select MGCODMAG,MGDESMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCODMAG_1_14'),i_cWhere,'GSAR_AMA',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAG)
            select MGCODMAG,MGDESMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESAPP = NVL(_Link_.MGDESMAG,space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.MGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAG = space(5)
      endif
      this.w_DESAPP = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Magazzino inesistente oppure obsoleto")
        endif
        this.w_CODMAG = space(5)
        this.w_DESAPP = space(30)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLISEL
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLISEL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CLISEL)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MAGCLI);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_MAGCLI;
                     ,'ANCODICE',trim(this.w_CLISEL))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLISEL)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CLISEL)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MAGCLI);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CLISEL)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_MAGCLI);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CLISEL) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCLISEL_2_2'),i_cWhere,'GSAR_BZC',"Clienti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MAGCLI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice cliente inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_MAGCLI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLISEL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CLISEL);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MAGCLI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_MAGCLI;
                       ,'ANCODICE',this.w_CLISEL)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLISEL = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLI = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CLISEL = space(15)
      endif
      this.w_DESCLI = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice cliente inesistente oppure obsoleto")
        endif
        this.w_CLISEL = space(15)
        this.w_DESCLI = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLISEL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FORSEL
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FORSEL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFR',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_FORSEL)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MAGFOR);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_MAGFOR;
                     ,'ANCODICE',trim(this.w_FORSEL))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FORSEL)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_FORSEL)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MAGFOR);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_FORSEL)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_MAGFOR);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_FORSEL) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oFORSEL_2_4'),i_cWhere,'GSAR_AFR',"Fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MAGFOR<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_MAGFOR);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FORSEL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_FORSEL);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MAGFOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_MAGFOR;
                       ,'ANCODICE',this.w_FORSEL)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FORSEL = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFOR = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSF = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_FORSEL = space(15)
      endif
      this.w_DESFOR = space(40)
      this.w_DATOBSF = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSF) OR .w_DATOBSF>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente oppure obsoleto")
        endif
        this.w_FORSEL = space(15)
        this.w_DESFOR = space(40)
        this.w_DATOBSF = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FORSEL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUSEL
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUSEL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACM',True,'CAM_AGAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_CAUSEL)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_CAUSEL))
          select CMCODICE,CMDESCRI,CMDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAUSEL)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CMDESCRI like "+cp_ToStrODBC(trim(this.w_CAUSEL)+"%");

            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CMDESCRI like "+cp_ToStr(trim(this.w_CAUSEL)+"%");

            select CMCODICE,CMDESCRI,CMDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CAUSEL) and !this.bDontReportError
            deferred_cp_zoom('CAM_AGAZ','*','CMCODICE',cp_AbsName(oSource.parent,'oCAUSEL_2_8'),i_cWhere,'GSMA_ACM',"Causali magazzino",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI,CMDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUSEL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_CAUSEL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_CAUSEL)
            select CMCODICE,CMDESCRI,CMDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUSEL = NVL(_Link_.CMCODICE,space(5))
      this.w_DESCAU2 = NVL(_Link_.CMDESCRI,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CMDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CAUSEL = space(5)
      endif
      this.w_DESCAU2 = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale di magazzino inesistente oppure obsoleta")
        endif
        this.w_CAUSEL = space(5)
        this.w_DESCAU2 = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUSEL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AGESEL
  func Link_2_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AGESEL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_AGESEL)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_AGESEL))
          select AGCODAGE,AGDESAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AGESEL)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStrODBC(trim(this.w_AGESEL)+"%");

            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStr(trim(this.w_AGESEL)+"%");

            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_AGESEL) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oAGESEL_2_10'),i_cWhere,'GSAR_AGE',"Agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AGESEL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_AGESEL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_AGESEL)
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AGESEL = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGE = NVL(_Link_.AGDESAGE,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_AGESEL = space(5)
      endif
      this.w_DESAGE = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice agente inesistente oppure obsoleto")
        endif
        this.w_AGESEL = space(5)
        this.w_DESAGE = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AGESEL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VOCSEL
  func Link_2_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_COST_IDX,3]
    i_lTable = "VOC_COST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2], .t., this.VOC_COST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VOCSEL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_AVC',True,'VOC_COST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VCCODICE like "+cp_ToStrODBC(trim(this.w_VOCSEL)+"%");

          i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCTIPVOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VCCODICE',trim(this.w_VOCSEL))
          select VCCODICE,VCDESCRI,VCTIPVOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VOCSEL)==trim(_Link_.VCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VOCSEL) and !this.bDontReportError
            deferred_cp_zoom('VOC_COST','*','VCCODICE',cp_AbsName(oSource.parent,'oVOCSEL_2_20'),i_cWhere,'GSCA_AVC',"Voci di ricavo",'GSMARAAR.VOC_COST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCTIPVOC";
                     +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',oSource.xKey(1))
            select VCCODICE,VCDESCRI,VCTIPVOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VOCSEL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCTIPVOC";
                   +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(this.w_VOCSEL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',this.w_VOCSEL)
            select VCCODICE,VCDESCRI,VCTIPVOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VOCSEL = NVL(_Link_.VCCODICE,space(15))
      this.w_DESVOC = NVL(_Link_.VCDESCRI,space(40))
      this.w_TIPVOC = NVL(_Link_.VCTIPVOC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_VOCSEL = space(15)
      endif
      this.w_DESVOC = space(40)
      this.w_TIPVOC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPVOC<>'C'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice voce di ricavo incongruente")
        endif
        this.w_VOCSEL = space(15)
        this.w_DESVOC = space(40)
        this.w_TIPVOC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])+'\'+cp_ToStr(_Link_.VCCODICE,1)
      cp_ShowWarn(i_cKey,this.VOC_COST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VOCSEL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CENSEL
  func Link_2_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CENSEL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_CENSEL)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_CENSEL))
          select CC_CONTO,CCDESPIA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CENSEL)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CENSEL) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oCENSEL_2_22'),i_cWhere,'GSCA_ACC',"Centri di ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CENSEL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_CENSEL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_CENSEL)
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CENSEL = NVL(_Link_.CC_CONTO,space(15))
      this.w_DESCEN = NVL(_Link_.CCDESPIA,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CENSEL = space(15)
      endif
      this.w_DESCEN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CENSEL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COMMESSA
  func Link_2_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COMMESSA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_COMMESSA)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_COMMESSA))
          select CNCODCAN,CNDESCAN,CNDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COMMESSA)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_COMMESSA)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_COMMESSA)+"%");

            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COMMESSA) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCOMMESSA_2_25'),i_cWhere,'GSAR_ACN',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COMMESSA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_COMMESSA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_COMMESSA)
            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COMMESSA = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOM = NVL(_Link_.CNDESCAN,space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CNDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COMMESSA = space(15)
      endif
      this.w_DESCOM = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        endif
        this.w_COMMESSA = space(15)
        this.w_DESCOM = space(30)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COMMESSA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCATDO1_1_3.RadioValue()==this.w_CATDO1)
      this.oPgFrm.Page1.oPag.oCATDO1_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCATDO2_1_4.RadioValue()==this.w_CATDO2)
      this.oPgFrm.Page1.oPag.oCATDO2_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPDOC_1_8.value==this.w_TIPDOC)
      this.oPgFrm.Page1.oPag.oTIPDOC_1_8.value=this.w_TIPDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDOC_1_9.value==this.w_DESDOC)
      this.oPgFrm.Page1.oPag.oDESDOC_1_9.value=this.w_DESDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oCODART_1_12.value==this.w_CODART)
      this.oPgFrm.Page1.oPag.oCODART_1_12.value=this.w_CODART
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_13.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_13.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oCODMAG_1_14.value==this.w_CODMAG)
      this.oPgFrm.Page1.oPag.oCODMAG_1_14.value=this.w_CODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDOCINI_1_17.value==this.w_DOCINI)
      this.oPgFrm.Page1.oPag.oDOCINI_1_17.value=this.w_DOCINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDOCFIN_1_18.value==this.w_DOCFIN)
      this.oPgFrm.Page1.oPag.oDOCFIN_1_18.value=this.w_DOCFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTCAR_1_21.value==this.w_TOTCAR)
      this.oPgFrm.Page1.oPag.oTOTCAR_1_21.value=this.w_TOTCAR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAU_1_23.value==this.w_DESCAU)
      this.oPgFrm.Page1.oPag.oDESCAU_1_23.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTSCA_1_24.value==this.w_TOTSCA)
      this.oPgFrm.Page1.oPag.oTOTSCA_1_24.value=this.w_TOTSCA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESAPP_1_36.value==this.w_DESAPP)
      this.oPgFrm.Page1.oPag.oDESAPP_1_36.value=this.w_DESAPP
    endif
    if not(this.oPgFrm.Page1.oPag.oCLIFOR_1_38.value==this.w_CLIFOR)
      this.oPgFrm.Page1.oPag.oCLIFOR_1_38.value=this.w_CLIFOR
    endif
    if not(this.oPgFrm.Page1.oPag.oUNIMIS_1_40.value==this.w_UNIMIS)
      this.oPgFrm.Page1.oPag.oUNIMIS_1_40.value=this.w_UNIMIS
    endif
    if not(this.oPgFrm.Page2.oPag.oMAGCLI_2_1.RadioValue()==this.w_MAGCLI)
      this.oPgFrm.Page2.oPag.oMAGCLI_2_1.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCLISEL_2_2.value==this.w_CLISEL)
      this.oPgFrm.Page2.oPag.oCLISEL_2_2.value=this.w_CLISEL
    endif
    if not(this.oPgFrm.Page2.oPag.oMAGFOR_2_3.RadioValue()==this.w_MAGFOR)
      this.oPgFrm.Page2.oPag.oMAGFOR_2_3.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFORSEL_2_4.value==this.w_FORSEL)
      this.oPgFrm.Page2.oPag.oFORSEL_2_4.value=this.w_FORSEL
    endif
    if not(this.oPgFrm.Page2.oPag.oMAGALT_2_5.RadioValue()==this.w_MAGALT)
      this.oPgFrm.Page2.oPag.oMAGALT_2_5.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCLI_2_6.value==this.w_DESCLI)
      this.oPgFrm.Page2.oPag.oDESCLI_2_6.value=this.w_DESCLI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFOR_2_7.value==this.w_DESFOR)
      this.oPgFrm.Page2.oPag.oDESFOR_2_7.value=this.w_DESFOR
    endif
    if not(this.oPgFrm.Page2.oPag.oCAUSEL_2_8.value==this.w_CAUSEL)
      this.oPgFrm.Page2.oPag.oCAUSEL_2_8.value=this.w_CAUSEL
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAU2_2_9.value==this.w_DESCAU2)
      this.oPgFrm.Page2.oPag.oDESCAU2_2_9.value=this.w_DESCAU2
    endif
    if not(this.oPgFrm.Page2.oPag.oAGESEL_2_10.value==this.w_AGESEL)
      this.oPgFrm.Page2.oPag.oAGESEL_2_10.value=this.w_AGESEL
    endif
    if not(this.oPgFrm.Page2.oPag.oDESAGE_2_11.value==this.w_DESAGE)
      this.oPgFrm.Page2.oPag.oDESAGE_2_11.value=this.w_DESAGE
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCOM_2_18.value==this.w_DESCOM)
      this.oPgFrm.Page2.oPag.oDESCOM_2_18.value=this.w_DESCOM
    endif
    if not(this.oPgFrm.Page2.oPag.oVOCSEL_2_20.value==this.w_VOCSEL)
      this.oPgFrm.Page2.oPag.oVOCSEL_2_20.value=this.w_VOCSEL
    endif
    if not(this.oPgFrm.Page2.oPag.oDESVOC_2_21.value==this.w_DESVOC)
      this.oPgFrm.Page2.oPag.oDESVOC_2_21.value=this.w_DESVOC
    endif
    if not(this.oPgFrm.Page2.oPag.oCENSEL_2_22.value==this.w_CENSEL)
      this.oPgFrm.Page2.oPag.oCENSEL_2_22.value=this.w_CENSEL
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCEN_2_23.value==this.w_DESCEN)
      this.oPgFrm.Page2.oPag.oDESCEN_2_23.value=this.w_DESCEN
    endif
    if not(this.oPgFrm.Page2.oPag.oCOMMESSA_2_25.value==this.w_COMMESSA)
      this.oPgFrm.Page2.oPag.oCOMMESSA_2_25.value=this.w_COMMESSA
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPEVA_2_29.RadioValue()==this.w_TIPEVA)
      this.oPgFrm.Page2.oPag.oTIPEVA_2_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCATDO2_1_49.RadioValue()==this.w_CATDO2)
      this.oPgFrm.Page1.oPag.oCATDO2_1_49.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((.w_TFLVEAC=.w_FLVEAC OR .w_FLVEAC=' ') AND (.w_TCATDOC=.w_CATDOC OR (.w_CATDOC='XX' AND .w_TCATDOC<>'OR')))  and not(empty(.w_TIPDOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTIPDOC_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento incongruente")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_CODART))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODART_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice articolo inesistente oppure obsoleto")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(IsAlt())  and not(empty(.w_CODMAG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODMAG_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Magazzino inesistente oppure obsoleto")
          case   ((empty(.w_DOCINI)) or not(.w_DOCINI<=.w_DOCFIN OR EMPTY(.w_DOCFIN)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDOCINI_1_17.SetFocus()
            i_bnoObbl = !empty(.w_DOCINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   ((empty(.w_DOCFIN)) or not(.w_DOCINI<=.w_DOCFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDOCFIN_1_18.SetFocus()
            i_bnoObbl = !empty(.w_DOCFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and (.w_MAGCLI='C')  and not(empty(.w_CLISEL))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCLISEL_2_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice cliente inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSF) OR .w_DATOBSF>.w_OBTEST)  and not(g_ACQU='S' OR IsAlt())  and (.w_MAGFOR='F' AND g_ACQU<>'S')  and not(empty(.w_FORSEL))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oFORSEL_2_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice fornitore inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(IsAlt())  and not(empty(.w_CAUSEL))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCAUSEL_2_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale di magazzino inesistente oppure obsoleta")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(IsAlt())  and not(empty(.w_AGESEL))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oAGESEL_2_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice agente inesistente oppure obsoleto")
          case   not(.w_TIPVOC<>'C')  and not(NOT(g_COMM='S' OR g_PERCCR='S'))  and (g_COMM='S' OR g_PERCCR='S')  and not(empty(.w_VOCSEL))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oVOCSEL_2_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice voce di ricavo incongruente")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(NOT((g_COMM='S'  OR g_PRAT='S' OR (g_PERCCR='S' AND g_PERCAN='S'))))  and ((g_COMM='S'  OR g_PRAT='S' OR (g_PERCCR='S' AND g_PERCAN='S')))  and not(empty(.w_COMMESSA))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCOMMESSA_2_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODART = this.w_CODART
    this.o_CLISEL = this.w_CLISEL
    return

enddefine

* --- Define pages as container
define class tgsve_szmPag1 as StdContainer
  Width  = 780
  height = 476
  stdWidth  = 780
  stdheight = 476
  resizeXpos=475
  resizeYpos=294
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oCATDO1_1_3 as StdCombo with uid="QDJUHAEQBR",rtseq=2,rtrep=.f.,left=73,top=5,width=130,height=21;
    , ToolTipText = "Categoria documento di selezione";
    , HelpContextID = 137407706;
    , cFormVar="w_CATDO1",RowSource=""+"Tutti i documenti,"+"Documenti interni,"+"Doc. di trasporto,"+"Fatture,"+"Note di credito,"+"Ricevute fiscali", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCATDO1_1_3.RadioValue()
    return(iif(this.value =1,'XX',;
    iif(this.value =2,'DI',;
    iif(this.value =3,'DT',;
    iif(this.value =4,'FA',;
    iif(this.value =5,'NC',;
    iif(this.value =6,'RF',;
    space(2))))))))
  endfunc
  func oCATDO1_1_3.GetRadio()
    this.Parent.oContained.w_CATDO1 = this.RadioValue()
    return .t.
  endfunc

  func oCATDO1_1_3.SetRadio()
    this.Parent.oContained.w_CATDO1=trim(this.Parent.oContained.w_CATDO1)
    this.value = ;
      iif(this.Parent.oContained.w_CATDO1=='XX',1,;
      iif(this.Parent.oContained.w_CATDO1=='DI',2,;
      iif(this.Parent.oContained.w_CATDO1=='DT',3,;
      iif(this.Parent.oContained.w_CATDO1=='FA',4,;
      iif(this.Parent.oContained.w_CATDO1=='NC',5,;
      iif(this.Parent.oContained.w_CATDO1=='RF',6,;
      0))))))
  endfunc

  func oCATDO1_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_ACQU='S')
    endwith
   endif
  endfunc

  func oCATDO1_1_3.mHide()
    with this.Parent.oContained
      return (g_ACQU<>'S')
    endwith
  endfunc


  add object oCATDO2_1_4 as StdCombo with uid="UPJNZIKQML",rtseq=3,rtrep=.f.,left=73,top=5,width=130,height=21;
    , ToolTipText = "Categoria documento di selezione";
    , HelpContextID = 120630490;
    , cFormVar="w_CATDO2",RowSource=""+"Tutti i documenti,"+"Documenti interni,"+"Doc. di trasporto,"+"Fatture,"+"Note di credito,"+"Ricevute fiscali,"+"DDT a fornitore,"+"Carichi a fornitore", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCATDO2_1_4.RadioValue()
    return(iif(this.value =1,'XX',;
    iif(this.value =2,'DI',;
    iif(this.value =3,'DT',;
    iif(this.value =4,'FA',;
    iif(this.value =5,'NC',;
    iif(this.value =6,'RF',;
    iif(this.value =7,'DF',;
    iif(this.value =8,'IF',;
    space(2))))))))))
  endfunc
  func oCATDO2_1_4.GetRadio()
    this.Parent.oContained.w_CATDO2 = this.RadioValue()
    return .t.
  endfunc

  func oCATDO2_1_4.SetRadio()
    this.Parent.oContained.w_CATDO2=trim(this.Parent.oContained.w_CATDO2)
    this.value = ;
      iif(this.Parent.oContained.w_CATDO2=='XX',1,;
      iif(this.Parent.oContained.w_CATDO2=='DI',2,;
      iif(this.Parent.oContained.w_CATDO2=='DT',3,;
      iif(this.Parent.oContained.w_CATDO2=='FA',4,;
      iif(this.Parent.oContained.w_CATDO2=='NC',5,;
      iif(this.Parent.oContained.w_CATDO2=='RF',6,;
      iif(this.Parent.oContained.w_CATDO2=='DF',7,;
      iif(this.Parent.oContained.w_CATDO2=='IF',8,;
      0))))))))
  endfunc

  func oCATDO2_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_ACQU<>'S')
    endwith
   endif
  endfunc

  func oCATDO2_1_4.mHide()
    with this.Parent.oContained
      return (g_ACQU='S' OR IsAlt())
    endwith
  endfunc

  add object oTIPDOC_1_8 as StdField with uid="SOMWIURCTL",rtseq=7,rtrep=.f.,;
    cFormVar = "w_TIPDOC", cQueryName = "TIPDOC",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento incongruente",;
    ToolTipText = "Codice tipo documento di selezione (spazio = no selezione)",;
    HelpContextID = 103867338,;
   bGlobalFont=.t.,;
    Height=21, Width=61, Left=243, Top=7, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_TIPDOC"

  func oTIPDOC_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIPDOC_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIPDOC_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oTIPDOC_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Tipi documenti",'GSVE1QZM.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oTIPDOC_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_TIPDOC
     i_obj.ecpSave()
  endproc

  add object oDESDOC_1_9 as StdField with uid="OADXZRBATB",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESDOC", cQueryName = "DESDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 103856330,;
   bGlobalFont=.t.,;
    Height=21, Width=212, Left=308, Top=7, InputMask=replicate('X',35)

  add object oCODART_1_12 as StdField with uid="MGYUCPGMQK",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CODART", cQueryName = "CODART",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice articolo inesistente oppure obsoleto",;
    ToolTipText = "Articolo selezionato",;
    HelpContextID = 84188890,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=73, Top=35, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_CODART"

  func oCODART_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODART_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODART_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODART_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Codici articoli/servizi",'',this.parent.oContained
  endproc
  proc oCODART_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CODART
     i_obj.ecpSave()
  endproc

  add object oDESART_1_13 as StdField with uid="JRTLSAMPBU",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 84129994,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=231, Top=35, InputMask=replicate('X',40)

  add object oCODMAG_1_14 as StdField with uid="JMVUEQLIUN",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CODMAG", cQueryName = "CODMAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Magazzino inesistente oppure obsoleto",;
    ToolTipText = "Magazzino selezionato",;
    HelpContextID = 50896602,;
   bGlobalFont=.t.,;
    Height=21, Width=61, Left=73, Top=60, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMAG"

  func oCODMAG_1_14.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oCODMAG_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMAG_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAG_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCODMAG_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'',this.parent.oContained
  endproc
  proc oCODMAG_1_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_CODMAG
     i_obj.ecpSave()
  endproc

  add object oDOCINI_1_17 as StdField with uid="ZGFEWOOULR",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DOCINI", cQueryName = "DOCINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data documento di inizio selezione",;
    HelpContextID = 3976906,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=629, Top=7

  func oDOCINI_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DOCINI<=.w_DOCFIN OR EMPTY(.w_DOCFIN))
    endwith
    return bRes
  endfunc

  add object oDOCFIN_1_18 as StdField with uid="MSJYJXJTGQ",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DOCFIN", cQueryName = "DOCFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data documento di fine selezione",;
    HelpContextID = 193965770,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=629, Top=35

  func oDOCFIN_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DOCINI<=.w_DOCFIN)
    endwith
    return bRes
  endfunc


  add object ZoomDoc as cp_zoombox with uid="GEQMUJGKAI",left=3, top=83, width=770,height=296,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="DOC_DETT",cZoomFile="GSVE_SZM",bOptions=.f.,bQueryOnLoad=.f.,bAdvOptions=.t.,bReadOnly=.t.,cMenuFile="",cZoomOnZoom="GSZM_BCC",bRetriveAllRows=.f.,bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 63843098

  add object oTOTCAR_1_21 as StdField with uid="JFAYKUGITM",rtseq=19,rtrep=.f.,;
    cFormVar = "w_TOTCAR", cQueryName = "TOTCAR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale carichi dei movimenti selezionati",;
    HelpContextID = 135372234,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=476, Top=406, cSayPict="v_PQ(16)", cGetPict="v_GQ(16)"

  func oTOTCAR_1_21.mHide()
    with this.Parent.oContained
      return (.w_VISIBLE)
    endwith
  endfunc

  add object oDESCAU_1_23 as StdField with uid="ZUBRYRDOEK",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione del cliente o fornitore selezionato",;
    HelpContextID = 85047498,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=78, Top=382, InputMask=replicate('X',35)

  add object oTOTSCA_1_24 as StdField with uid="PWXLVIYGHT",rtseq=21,rtrep=.f.,;
    cFormVar = "w_TOTSCA", cQueryName = "TOTSCA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale scarichi dei movimenti selezionati",;
    HelpContextID = 149003722,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=596, Top=406, cSayPict="v_PQ(16)", cGetPict="v_GQ(16)"

  func oTOTSCA_1_24.mHide()
    with this.Parent.oContained
      return (.w_VISIBLE)
    endwith
  endfunc

  add object oDESAPP_1_36 as StdField with uid="KANYTXGEET",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DESAPP", cQueryName = "DESAPP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 153336010,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=139, Top=60, InputMask=replicate('X',30)

  func oDESAPP_1_36.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oCLIFOR_1_38 as StdField with uid="EROFXEWWWT",rtseq=27,rtrep=.f.,;
    cFormVar = "w_CLIFOR", cQueryName = "CLIFOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione del cliente o fornitore selezionato",;
    HelpContextID = 120541658,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=78, Top=406, InputMask=replicate('X',40)

  add object oUNIMIS_1_40 as StdField with uid="IXFGLWAOAL",rtseq=28,rtrep=.f.,;
    cFormVar = "w_UNIMIS", cQueryName = "UNIMIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 109596346,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=737, Top=406, InputMask=replicate('X',3)

  func oUNIMIS_1_40.mHide()
    with this.Parent.oContained
      return (.w_VISIBLE)
    endwith
  endfunc


  add object oObj_1_42 as cp_runprogram with uid="EJGYZWXGXP",left=612, top=484, width=164,height=25,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSVE_BVM("Ven")',;
    cEvent = "LanciaCLI",;
    nPag=1;
    , HelpContextID = 63843098


  add object oObj_1_45 as cp_runprogram with uid="CDCTMDAAOJ",left=1, top=484, width=324,height=20,;
    caption='GSVE_BZM(w_SERIALE w_PARAME)',;
   bGlobalFont=.t.,;
    prg="GSVE_BZM(w_SERIALE, w_PARAME)",;
    cEvent = "w_zoomdoc selected",;
    nPag=1;
    , HelpContextID = 149890410


  add object oBtn_1_46 as StdButton with uid="MBXAIGGBZE",left=12, top=431, width=48,height=45,;
    CpPicture="bmp\dettagli.BMP", caption="", nPag=1;
    , ToolTipText = "Visualizza il documento associato alla riga selezionata";
    , HelpContextID = 69391728;
    , caption='\<Visualizza';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_46.Click()
      with this.Parent.oContained
        GSVE_BZM(this.Parent.oContained,.w_SERIALE, .w_PARAME)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_46.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERIALE))
      endwith
    endif
  endfunc


  add object oBtn_1_47 as StdButton with uid="ISDICPRYHO",left=725, top=431, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 33912134;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_47.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_48 as StdButton with uid="PHEWMSSLEL",left=725, top=35, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=1;
    , ToolTipText = "Riesegue la ricerca con le nuove selezioni";
    , HelpContextID = 64918506;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_48.Click()
      with this.Parent.oContained
        GSVE_BVM(this.Parent.oContained,"Ven")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oCATDO2_1_49 as StdCombo with uid="MAIJRZNXIL",rtseq=53,rtrep=.f.,left=73,top=5,width=130,height=21;
    , ToolTipText = "Categoria documento di selezione";
    , HelpContextID = 120630490;
    , cFormVar="w_CATDO2",RowSource=""+"Tutti i documenti,"+"Altri documenti,"+"Fatture,"+"Note di credito", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCATDO2_1_49.RadioValue()
    return(iif(this.value =1,'XX',;
    iif(this.value =2,'DI',;
    iif(this.value =3,'FA',;
    iif(this.value =4,'NC',;
    space(2))))))
  endfunc
  func oCATDO2_1_49.GetRadio()
    this.Parent.oContained.w_CATDO2 = this.RadioValue()
    return .t.
  endfunc

  func oCATDO2_1_49.SetRadio()
    this.Parent.oContained.w_CATDO2=trim(this.Parent.oContained.w_CATDO2)
    this.value = ;
      iif(this.Parent.oContained.w_CATDO2=='XX',1,;
      iif(this.Parent.oContained.w_CATDO2=='DI',2,;
      iif(this.Parent.oContained.w_CATDO2=='FA',3,;
      iif(this.Parent.oContained.w_CATDO2=='NC',4,;
      0))))
  endfunc

  func oCATDO2_1_49.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_ACQU<>'S')
    endwith
   endif
  endfunc

  func oCATDO2_1_49.mHide()
    with this.Parent.oContained
      return (g_ACQU='S' OR NOT IsAlt())
    endwith
  endfunc


  add object oObj_1_51 as cp_runprogram with uid="LYMMNHLBMT",left=1, top=507, width=324,height=23,;
    caption='GSVE_BVM(V)',;
   bGlobalFont=.t.,;
    prg='GSVE_BVM("Ven")',;
    cEvent = "Lanciattivity",;
    nPag=1;
    , HelpContextID = 103584205


  add object oObj_1_59 as cp_outputCombo with uid="ZLERXIVPWT",left=210, top=431, width=380,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 63843098


  add object oBtn_1_60 as StdButton with uid="PYYBFJPVEW",left=673, top=431, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la stampa";
    , HelpContextID = 100051162;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_60.Click()
      with this.Parent.oContained
        gsve_bvm(this.Parent.oContained,"Stampa")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_60.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc

  add object oStr_1_22 as StdString with uid="AXVCKHHLKY",Visible=.t., Left=1, Top=35,;
    Alignment=1, Width=70, Height=15,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  func oStr_1_22.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_25 as StdString with uid="ITDTCOTAKS",Visible=.t., Left=1, Top=60,;
    Alignment=1, Width=70, Height=15,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  func oStr_1_25.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_28 as StdString with uid="QDJAYCNVSC",Visible=.t., Left=476, Top=387,;
    Alignment=0, Width=114, Height=15,;
    Caption="Totale carichi"  ;
  , bGlobalFont=.t.

  func oStr_1_28.mHide()
    with this.Parent.oContained
      return (.w_VISIBLE)
    endwith
  endfunc

  add object oStr_1_29 as StdString with uid="GWGUIIUDFR",Visible=.t., Left=596, Top=387,;
    Alignment=0, Width=114, Height=15,;
    Caption="Totale scarichi"  ;
  , bGlobalFont=.t.

  func oStr_1_29.mHide()
    with this.Parent.oContained
      return (.w_VISIBLE)
    endwith
  endfunc

  add object oStr_1_30 as StdString with uid="APHYIAFQKO",Visible=.t., Left=1, Top=7,;
    Alignment=1, Width=70, Height=15,;
    Caption="Categoria:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="WPBOLNVROY",Visible=.t., Left=203, Top=7,;
    Alignment=1, Width=38, Height=15,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="OOQMYWSQRY",Visible=.t., Left=523, Top=7,;
    Alignment=1, Width=102, Height=15,;
    Caption="Documenti dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="KTZRCRKILE",Visible=.t., Left=523, Top=35,;
    Alignment=1, Width=102, Height=15,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="GXBNLTESUO",Visible=.t., Left=1, Top=406,;
    Alignment=1, Width=76, Height=15,;
    Caption="Intestatario:"  ;
  , bGlobalFont=.t.

  func oStr_1_37.mHide()
    with this.Parent.oContained
      return (g_ACQU='S')
    endwith
  endfunc

  add object oStr_1_39 as StdString with uid="BBWFTTTQOK",Visible=.t., Left=1, Top=382,;
    Alignment=1, Width=76, Height=15,;
    Caption="Documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="EFSNEEACFA",Visible=.t., Left=711, Top=406,;
    Alignment=1, Width=25, Height=15,;
    Caption="in:"  ;
  , bGlobalFont=.t.

  func oStr_1_41.mHide()
    with this.Parent.oContained
      return (.w_VISIBLE)
    endwith
  endfunc

  add object oStr_1_43 as StdString with uid="HPKPFVFIIL",Visible=.t., Left=1, Top=406,;
    Alignment=1, Width=76, Height=15,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  func oStr_1_43.mHide()
    with this.Parent.oContained
      return (g_ACQU<>'S')
    endwith
  endfunc

  add object oStr_1_50 as StdString with uid="FQKFYNXBXF",Visible=.t., Left=3, Top=35,;
    Alignment=1, Width=68, Height=18,;
    Caption="Prestazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_50.mHide()
    with this.Parent.oContained
      return (NOT IsAlt())
    endwith
  endfunc

  add object oStr_1_61 as StdString with uid="HRTFFGQQPR",Visible=.t., Left=115, Top=431,;
    Alignment=1, Width=93, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsve_szmPag2 as StdContainer
  Width  = 780
  height = 476
  stdWidth  = 780
  stdheight = 476
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMAGCLI_2_1 as StdCheck with uid="LMZJFUBFCL",rtseq=29,rtrep=.f.,left=34, top=19, caption="Clienti",;
    ToolTipText = "Se attivo: seleziona movimenti riferiti a clienti",;
    HelpContextID = 6454330,;
    cFormVar="w_MAGCLI", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oMAGCLI_2_1.RadioValue()
    return(iif(this.value =1,'C',;
    'X'))
  endfunc
  func oMAGCLI_2_1.GetRadio()
    this.Parent.oContained.w_MAGCLI = this.RadioValue()
    return .t.
  endfunc

  func oMAGCLI_2_1.SetRadio()
    this.Parent.oContained.w_MAGCLI=trim(this.Parent.oContained.w_MAGCLI)
    this.value = ;
      iif(this.Parent.oContained.w_MAGCLI=='C',1,;
      0)
  endfunc

  func oMAGCLI_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CLISEL)
        bRes2=.link_2_2('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oCLISEL_2_2 as StdField with uid="YWNLSAVWWQ",rtseq=30,rtrep=.f.,;
    cFormVar = "w_CLISEL", cQueryName = "CLISEL",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice cliente inesistente oppure obsoleto",;
    ToolTipText = "Cliente di selezione",;
    HelpContextID = 230838746,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=124, Top=19, cSayPict="p_CLI", cGetPict="p_CLI", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_MAGCLI", oKey_2_1="ANCODICE", oKey_2_2="this.w_CLISEL"

  func oCLISEL_2_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MAGCLI='C')
    endwith
   endif
  endfunc

  func oCLISEL_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLISEL_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLISEL_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_MAGCLI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_MAGCLI)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCLISEL_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti",'',this.parent.oContained
  endproc
  proc oCLISEL_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_MAGCLI
     i_obj.w_ANCODICE=this.parent.oContained.w_CLISEL
     i_obj.ecpSave()
  endproc

  add object oMAGFOR_2_3 as StdCheck with uid="MOXFVFZFJW",rtseq=31,rtrep=.f.,left=34, top=49, caption="Fornitori",;
    ToolTipText = "Se attivo: seleziona movimenti riferiti a fornitori",;
    HelpContextID = 120552506,;
    cFormVar="w_MAGFOR", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oMAGFOR_2_3.RadioValue()
    return(iif(this.value =1,'F',;
    'X'))
  endfunc
  func oMAGFOR_2_3.GetRadio()
    this.Parent.oContained.w_MAGFOR = this.RadioValue()
    return .t.
  endfunc

  func oMAGFOR_2_3.SetRadio()
    this.Parent.oContained.w_MAGFOR=trim(this.Parent.oContained.w_MAGFOR)
    this.value = ;
      iif(this.Parent.oContained.w_MAGFOR=='F',1,;
      0)
  endfunc

  func oMAGFOR_2_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_ACQU<>'S')
    endwith
   endif
  endfunc

  func oMAGFOR_2_3.mHide()
    with this.Parent.oContained
      return (g_ACQU='S' OR IsAlt())
    endwith
  endfunc

  func oMAGFOR_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_FORSEL)
        bRes2=.link_2_4('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oFORSEL_2_4 as StdField with uid="ZOBUYDKUUJ",rtseq=32,rtrep=.f.,;
    cFormVar = "w_FORSEL", cQueryName = "FORSEL",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice fornitore inesistente oppure obsoleto",;
    ToolTipText = "Fornitore di selezione",;
    HelpContextID = 230801066,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=124, Top=49, cSayPict="p_CLI", cGetPict="p_CLI", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_AFR", oKey_1_1="ANTIPCON", oKey_1_2="this.w_MAGFOR", oKey_2_1="ANCODICE", oKey_2_2="this.w_FORSEL"

  func oFORSEL_2_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MAGFOR='F' AND g_ACQU<>'S')
    endwith
   endif
  endfunc

  func oFORSEL_2_4.mHide()
    with this.Parent.oContained
      return (g_ACQU='S' OR IsAlt())
    endwith
  endfunc

  func oFORSEL_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oFORSEL_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFORSEL_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_MAGFOR)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_MAGFOR)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oFORSEL_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFR',"Fornitori",'',this.parent.oContained
  endproc
  proc oFORSEL_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_MAGFOR
     i_obj.w_ANCODICE=this.parent.oContained.w_FORSEL
     i_obj.ecpSave()
  endproc

  add object oMAGALT_2_5 as StdCheck with uid="DLGHCPWYCW",rtseq=33,rtrep=.f.,left=124, top=80, caption="Altri movimenti",;
    ToolTipText = "Se attivo: seleziona movimenti non riferiti a clienti o fornitori",;
    HelpContextID = 90471482,;
    cFormVar="w_MAGALT", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oMAGALT_2_5.RadioValue()
    return(iif(this.value =1,'N',;
    'X'))
  endfunc
  func oMAGALT_2_5.GetRadio()
    this.Parent.oContained.w_MAGALT = this.RadioValue()
    return .t.
  endfunc

  func oMAGALT_2_5.SetRadio()
    this.Parent.oContained.w_MAGALT=trim(this.Parent.oContained.w_MAGALT)
    this.value = ;
      iif(this.Parent.oContained.w_MAGALT=='N',1,;
      0)
  endfunc

  add object oDESCLI_2_6 as StdField with uid="EPVIIPVNLO",rtseq=34,rtrep=.f.,;
    cFormVar = "w_DESCLI", cQueryName = "DESCLI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 6404298,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=263, Top=19, InputMask=replicate('X',40)

  add object oDESFOR_2_7 as StdField with uid="RTFMOGHEDO",rtseq=35,rtrep=.f.,;
    cFormVar = "w_DESFOR", cQueryName = "DESFOR",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 120502474,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=263, Top=49, InputMask=replicate('X',40)

  func oDESFOR_2_7.mHide()
    with this.Parent.oContained
      return (g_ACQU='S' OR IsAlt())
    endwith
  endfunc

  add object oCAUSEL_2_8 as StdField with uid="RVQPJLQPIQ",rtseq=36,rtrep=.f.,;
    cFormVar = "w_CAUSEL", cQueryName = "CAUSEL",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale di magazzino inesistente oppure obsoleta",;
    ToolTipText = "Causale di magazzino di selezione (vuoto=no selezione)",;
    HelpContextID = 230792410,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=124, Top=128, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAM_AGAZ", cZoomOnZoom="GSMA_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_CAUSEL"

  func oCAUSEL_2_8.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oCAUSEL_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAUSEL_2_8.ecpDrop(oSource)
    this.Parent.oContained.link_2_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCAUSEL_2_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAM_AGAZ','*','CMCODICE',cp_AbsName(this.parent,'oCAUSEL_2_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACM',"Causali magazzino",'',this.parent.oContained
  endproc
  proc oCAUSEL_2_8.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_CAUSEL
     i_obj.ecpSave()
  endproc

  add object oDESCAU2_2_9 as StdField with uid="RNIKZKWVIU",rtseq=37,rtrep=.f.,;
    cFormVar = "w_DESCAU2", cQueryName = "DESCAU2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 85047498,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=189, Top=128, InputMask=replicate('X',35)

  func oDESCAU2_2_9.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oAGESEL_2_10 as StdField with uid="AIOWLZDJQF",rtseq=38,rtrep=.f.,;
    cFormVar = "w_AGESEL", cQueryName = "AGESEL",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice agente inesistente oppure obsoleto",;
    ToolTipText = "Codice agente di selezione (vuoto=no selezione)",;
    HelpContextID = 230856442,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=124, Top=158, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_AGESEL"

  func oAGESEL_2_10.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oAGESEL_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oAGESEL_2_10.ecpDrop(oSource)
    this.Parent.oContained.link_2_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAGESEL_2_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oAGESEL_2_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"Agenti",'',this.parent.oContained
  endproc
  proc oAGESEL_2_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_AGESEL
     i_obj.ecpSave()
  endproc

  add object oDESAGE_2_11 as StdField with uid="PAXUTDMECM",rtseq=39,rtrep=.f.,;
    cFormVar = "w_DESAGE", cQueryName = "DESAGE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 78887114,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=189, Top=160, InputMask=replicate('X',35)

  func oDESAGE_2_11.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oDESCOM_2_18 as StdField with uid="IWHHZXRKES",rtseq=43,rtrep=.f.,;
    cFormVar = "w_DESCOM", cQueryName = "DESCOM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 204585162,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=261, Top=248, InputMask=replicate('X',30)

  func oDESCOM_2_18.mHide()
    with this.Parent.oContained
      return (NOT((g_COMM='S'  OR g_PRAT='S' OR (g_PERCCR='S' AND g_PERCAN='S'))))
    endwith
  endfunc

  add object oVOCSEL_2_20 as StdField with uid="BXYHJRRPSK",rtseq=45,rtrep=.f.,;
    cFormVar = "w_VOCSEL", cQueryName = "VOCSEL",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice voce di ricavo incongruente",;
    ToolTipText = "Codice voce di ricavo di selezione",;
    HelpContextID = 230862250,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=124, Top=188, cSayPict="p_MCE", cGetPict="p_MCE", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="VOC_COST", cZoomOnZoom="GSCA_AVC", oKey_1_1="VCCODICE", oKey_1_2="this.w_VOCSEL"

  func oVOCSEL_2_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COMM='S' OR g_PERCCR='S')
    endwith
   endif
  endfunc

  func oVOCSEL_2_20.mHide()
    with this.Parent.oContained
      return (NOT(g_COMM='S' OR g_PERCCR='S'))
    endwith
  endfunc

  func oVOCSEL_2_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oVOCSEL_2_20.ecpDrop(oSource)
    this.Parent.oContained.link_2_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVOCSEL_2_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_COST','*','VCCODICE',cp_AbsName(this.parent,'oVOCSEL_2_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_AVC',"Voci di ricavo",'GSMARAAR.VOC_COST_VZM',this.parent.oContained
  endproc
  proc oVOCSEL_2_20.mZoomOnZoom
    local i_obj
    i_obj=GSCA_AVC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VCCODICE=this.parent.oContained.w_VOCSEL
     i_obj.ecpSave()
  endproc

  add object oDESVOC_2_21 as StdField with uid="RRXGULMIJI",rtseq=46,rtrep=.f.,;
    cFormVar = "w_DESVOC", cQueryName = "DESVOC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 102676682,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=262, Top=188, InputMask=replicate('X',40)

  func oDESVOC_2_21.mHide()
    with this.Parent.oContained
      return (NOT(g_COMM='S' OR g_PERCCR='S'))
    endwith
  endfunc

  add object oCENSEL_2_22 as StdField with uid="LVBQXSXAFA",rtseq=47,rtrep=.f.,;
    cFormVar = "w_CENSEL", cQueryName = "CENSEL",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice centro di ricavo incongruente",;
    ToolTipText = "Codice centro di ricavo di selezione",;
    HelpContextID = 230820058,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=124, Top=218, cSayPict="p_CEN", cGetPict="p_CEN", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_CENSEL"

  func oCENSEL_2_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCCR='S')
    endwith
   endif
  endfunc

  func oCENSEL_2_22.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  func oCENSEL_2_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oCENSEL_2_22.ecpDrop(oSource)
    this.Parent.oContained.link_2_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCENSEL_2_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oCENSEL_2_22'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri di ricavo",'',this.parent.oContained
  endproc
  proc oCENSEL_2_22.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_CENSEL
     i_obj.ecpSave()
  endproc

  add object oDESCEN_2_23 as StdField with uid="RXROGRGSZG",rtseq=48,rtrep=.f.,;
    cFormVar = "w_DESCEN", cQueryName = "DESCEN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 198293706,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=262, Top=218, InputMask=replicate('X',40)

  func oDESCEN_2_23.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  add object oCOMMESSA_2_25 as StdField with uid="CRUWNASMFB",rtseq=49,rtrep=.f.,;
    cFormVar = "w_COMMESSA", cQueryName = "COMMESSA",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
    ToolTipText = "Codice commessa",;
    HelpContextID = 113774233,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=124, Top=248, cSayPict="p_MCE", cGetPict="p_MCE", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_COMMESSA"

  func oCOMMESSA_2_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((g_COMM='S'  OR g_PRAT='S' OR (g_PERCCR='S' AND g_PERCAN='S')))
    endwith
   endif
  endfunc

  func oCOMMESSA_2_25.mHide()
    with this.Parent.oContained
      return (NOT((g_COMM='S'  OR g_PRAT='S' OR (g_PERCCR='S' AND g_PERCAN='S'))))
    endwith
  endfunc

  func oCOMMESSA_2_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOMMESSA_2_25.ecpDrop(oSource)
    this.Parent.oContained.link_2_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOMMESSA_2_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCOMMESSA_2_25'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Commesse",'',this.parent.oContained
  endproc
  proc oCOMMESSA_2_25.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_COMMESSA
     i_obj.ecpSave()
  endproc


  add object oTIPEVA_2_29 as StdCombo with uid="RJUTOFBIMA",rtseq=51,rtrep=.f.,left=124,top=278,width=156,height=21;
    , ToolTipText = "Selezione: tutti i documenti, solo righe inevase, solo documenti con righe non evase totalmente";
    , HelpContextID = 130016202;
    , cFormVar="w_TIPEVA",RowSource=""+"Tutti,"+"Righe inevase,"+"Documenti inevasi", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTIPEVA_2_29.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'R',;
    iif(this.value =3,'D',;
    space(1)))))
  endfunc
  func oTIPEVA_2_29.GetRadio()
    this.Parent.oContained.w_TIPEVA = this.RadioValue()
    return .t.
  endfunc

  func oTIPEVA_2_29.SetRadio()
    this.Parent.oContained.w_TIPEVA=trim(this.Parent.oContained.w_TIPEVA)
    this.value = ;
      iif(this.Parent.oContained.w_TIPEVA=='T',1,;
      iif(this.Parent.oContained.w_TIPEVA=='R',2,;
      iif(this.Parent.oContained.w_TIPEVA=='D',3,;
      0)))
  endfunc

  add object oStr_2_13 as StdString with uid="OLGVQNXTHD",Visible=.t., Left=2, Top=158,;
    Alignment=1, Width=120, Height=15,;
    Caption="Agente:"  ;
  , bGlobalFont=.t.

  func oStr_2_13.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_2_14 as StdString with uid="EDJOGPKJEM",Visible=.t., Left=2, Top=128,;
    Alignment=1, Width=120, Height=15,;
    Caption="Causale magazzino:"  ;
  , bGlobalFont=.t.

  func oStr_2_14.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_2_17 as StdString with uid="SAHIBASGUJ",Visible=.t., Left=46, Top=248,;
    Alignment=1, Width=76, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  func oStr_2_17.mHide()
    with this.Parent.oContained
      return (NOT((g_COMM='S'  OR (g_PERCCR='S' AND g_PERCAN='S'))))
    endwith
  endfunc

  add object oStr_2_24 as StdString with uid="ACEQNVHEYA",Visible=.t., Left=2, Top=188,;
    Alignment=1, Width=120, Height=18,;
    Caption="Voce di ricavo:"  ;
  , bGlobalFont=.t.

  func oStr_2_24.mHide()
    with this.Parent.oContained
      return (NOT(g_COMM='S' OR g_PERCCR='S'))
    endwith
  endfunc

  add object oStr_2_26 as StdString with uid="LSOVIRIYTM",Visible=.t., Left=2, Top=218,;
    Alignment=1, Width=120, Height=18,;
    Caption="Centro di ricavo:"  ;
  , bGlobalFont=.t.

  func oStr_2_26.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  add object oStr_2_28 as StdString with uid="DVTJLGBIGM",Visible=.t., Left=64, Top=278,;
    Alignment=1, Width=58, Height=18,;
    Caption="Evasione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_30 as StdString with uid="EQDTEERMOA",Visible=.t., Left=81, Top=248,;
    Alignment=1, Width=41, Height=18,;
    Caption="Pratica:"  ;
  , bGlobalFont=.t.

  func oStr_2_30.mHide()
    with this.Parent.oContained
      return ((g_COMM='S'  OR (g_PERCCR='S' AND g_PERCAN='S')) OR g_PRAT='N')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_szm','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
