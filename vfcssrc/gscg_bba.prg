* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bba                                                        *
*              Carica codice banca e descriz                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_44]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-11                                                      *
* Last revis.: 2000-02-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOpz
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bba",oParentObject,m.pOpz)
return(i_retval)

define class tgscg_bba as StdBatch
  * --- Local variables
  pOpz = space(1)
  w_BACODBAN = space(15)
  w_BADESCRI = space(50)
  w_OBTEST = ctod("  /  /  ")
  * --- WorkFile variables
  COC_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carica il Codice Banca e Descrizione nell'archivio dei Dati IVA Annuali (da GSCG_MDI)
    this.w_BACODBAN = SPACE(15)
    this.w_BADESCRI = SPACE(50)
    do case
      case this.pOpz="C"
        * --- Ricerca Codice Banca e Descrizione
        vx_exec("QUERY\GSCG_QBA.VZM",this)
        if NOT EMPTY(this.w_BACODBAN)
          * --- Carica Codici
          this.oParentObject.w_IACODBAN = NVL(this.w_BACODBAN, SPACE(15))
          this.oParentObject.w_IADESBAN = NVL(this.w_BADESCRI, SPACE(50))
        endif
      case this.pOpz="V"
        * --- Ricerca Descrizione Banca
        * --- Read from COC_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.COC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2],.t.,this.COC_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "BADESCRI,BADTOBSO"+;
            " from "+i_cTable+" COC_MAST where ";
                +"BACODBAN = "+cp_ToStrODBC(this.oParentObject.w_IACODBAN);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            BADESCRI,BADTOBSO;
            from (i_cTable) where;
                BACODBAN = this.oParentObject.w_IACODBAN;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_BADESCRI = NVL(cp_ToDate(_read_.BADESCRI),cp_NullValue(_read_.BADESCRI))
          this.w_OBTEST = NVL(cp_ToDate(_read_.BADTOBSO),cp_NullValue(_read_.BADTOBSO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = 'C/C Versamento Inesistente!'
          return
        endif
        select (i_nOldArea)
        if i_Rows=0
          this.oParentObject.w_IACODBAN = ""
          this.oParentObject.w_IADESBAN = ""
          ah_ErrorMsg("C/C versamento inesistente",,"")
        else
          if NOT EMPTY(this.w_OBTEST) AND this.w_OBTEST<=i_DATSYS
            this.oParentObject.w_IACODBAN = ""
            this.oParentObject.w_IADESBAN = ""
            ah_ErrorMsg("C/C versamento obsoleto",,"")
          else
            this.oParentObject.w_IADESBAN = NVL(this.w_BADESCRI, SPACE(50))
          endif
        endif
    endcase
  endproc


  proc Init(oParentObject,pOpz)
    this.pOpz=pOpz
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='COC_MAST'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOpz"
endproc
