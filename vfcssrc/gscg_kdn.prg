* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_kdn                                                        *
*              Inserimento riga utilizzo                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2017-07-28                                                      *
* Last revis.: 2017-10-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_kdn",oParentObject))

* --- Class definition
define class tgscg_kdn as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 553
  Height = 183
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2017-10-12"
  HelpContextID=185981591
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  _IDX = 0
  cPrg = "gscg_kdn"
  cComment = "Inserimento riga utilizzo"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_AGGIORNA = space(1)
  w_DDIMPDIC = 0
  w_DDNOTOPE = space(100)
  w_DDCODUTE = 0
  w_DDDATOPE = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_kdnPag1","gscg_kdn",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDDIMPDIC_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AGGIORNA=space(1)
      .w_DDIMPDIC=0
      .w_DDNOTOPE=space(100)
      .w_DDCODUTE=0
      .w_DDDATOPE=ctod("  /  /  ")
      .w_AGGIORNA=oParentObject.w_AGGIORNA
      .w_DDIMPDIC=oParentObject.w_DDIMPDIC
      .w_DDNOTOPE=oParentObject.w_DDNOTOPE
      .w_DDCODUTE=oParentObject.w_DDCODUTE
      .w_DDDATOPE=oParentObject.w_DDDATOPE
        .w_AGGIORNA = .T.
          .DoRTCalc(2,3,.f.)
        .w_DDCODUTE = i_Codute
        .w_DDDATOPE = i_Datsys
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_AGGIORNA=.w_AGGIORNA
      .oParentObject.w_DDIMPDIC=.w_DDIMPDIC
      .oParentObject.w_DDNOTOPE=.w_DDNOTOPE
      .oParentObject.w_DDCODUTE=.w_DDCODUTE
      .oParentObject.w_DDDATOPE=.w_DDDATOPE
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,5,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDDIMPDIC_1_2.value==this.w_DDIMPDIC)
      this.oPgFrm.Page1.oPag.oDDIMPDIC_1_2.value=this.w_DDIMPDIC
    endif
    if not(this.oPgFrm.Page1.oPag.oDDNOTOPE_1_4.value==this.w_DDNOTOPE)
      this.oPgFrm.Page1.oPag.oDDNOTOPE_1_4.value=this.w_DDNOTOPE
    endif
    if not(this.oPgFrm.Page1.oPag.oDDCODUTE_1_8.value==this.w_DDCODUTE)
      this.oPgFrm.Page1.oPag.oDDCODUTE_1_8.value=this.w_DDCODUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oDDDATOPE_1_10.value==this.w_DDDATOPE)
      this.oPgFrm.Page1.oPag.oDDDATOPE_1_10.value=this.w_DDDATOPE
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgscg_kdnPag1 as StdContainer
  Width  = 549
  height = 183
  stdWidth  = 549
  stdheight = 183
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDDIMPDIC_1_2 as StdField with uid="WWKYXKBVLK",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DDIMPDIC", cQueryName = "DDIMPDIC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 73905017,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=121, Top=20, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oDDNOTOPE_1_4 as StdField with uid="IEWFYEEDNQ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DDNOTOPE", cQueryName = "DDNOTOPE",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 5635205,;
   bGlobalFont=.t.,;
    Height=21, Width=412, Left=121, Top=93, InputMask=replicate('X',100)


  add object oBtn_1_5 as StdButton with uid="APIWEFNJEL",left=441, top=132, width=48,height=45,;
    CpPicture="bmp\OK.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per confermare le scelte effettuate";
    , HelpContextID = 204263801;
    , tabstop=.f., caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_5.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_DDIMPDIC<>0)
      endwith
    endif
  endfunc


  add object oBtn_1_6 as StdButton with uid="ZJIUAZYUNY",left=491, top=132, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 193299014;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDDCODUTE_1_8 as StdField with uid="RNLLIRMOOG",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DDCODUTE", cQueryName = "DDCODUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 190229637,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=121, Top=57

  add object oDDDATOPE_1_10 as StdField with uid="JNHJHJINUB",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DDDATOPE", cQueryName = "DDDATOPE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 6593669,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=337, Top=57

  add object oStr_1_3 as StdString with uid="ZYAPVNNFAO",Visible=.t., Left=5, Top=22,;
    Alignment=1, Width=112, Height=18,;
    Caption="Importo riga:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="NRUYZDMPKQ",Visible=.t., Left=22, Top=59,;
    Alignment=1, Width=95, Height=18,;
    Caption="Codice utente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="KTBKEYGDYU",Visible=.t., Left=209, Top=59,;
    Alignment=1, Width=124, Height=18,;
    Caption="Data inserimento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="VEERJQXXGY",Visible=.t., Left=74, Top=96,;
    Alignment=1, Width=43, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_kdn','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
