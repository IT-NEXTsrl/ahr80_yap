* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_aiv                                                        *
*              Codici IVA                                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_52]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-20                                                      *
* Last revis.: 2017-08-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_aiv"))

* --- Class definition
define class tgsar_aiv as StdForm
  Top    = 3
  Left   = 10

  * --- Standard Properties
  Width  = 573
  Height = 567+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2017-08-25"
  HelpContextID=9009001
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=51

  * --- Constant Properties
  VOCIIVA_IDX = 0
  cFile = "VOCIIVA"
  cKeySelect = "IVCODIVA"
  cKeyWhere  = "IVCODIVA=this.w_IVCODIVA"
  cKeyWhereODBC = '"IVCODIVA="+cp_ToStrODBC(this.w_IVCODIVA)';

  cKeyWhereODBCqualified = '"VOCIIVA.IVCODIVA="+cp_ToStrODBC(this.w_IVCODIVA)';

  cPrg = "gsar_aiv"
  cComment = "Codici IVA"
  icon = "anag.ico"
  cAutoZoom = 'GSAR0AIV'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_IVCODIVA = space(5)
  w_IVDESIVA = space(35)
  w_IVDESSUP = space(100)
  w_IVPERIVA = 0
  o_IVPERIVA = 0
  w_IVPERIND = 0
  o_IVPERIND = 0
  w_IVPROIVA = space(1)
  w_IVTIPPLA = space(1)
  w_IVPLAIVA = space(1)
  o_IVPLAIVA = space(1)
  w_IVMACIVA = space(1)
  w_IVIMPEXP = space(1)
  w_IVBOLIVA = space(1)
  w_IVSPLPAY = space(1)
  w_IVPERCOM = 0
  w_IVTIPAGR = space(1)
  w_IVESINOD = space(1)
  w_IVREVCHA = space(1)
  o_IVREVCHA = space(1)
  w_IVREGMAR = space(1)
  o_IVREGMAR = space(1)
  w_IVPERMAR = 0
  w_IVFLVAFF = space(1)
  w_IVACQINT = space(1)
  o_IVACQINT = space(1)
  w_IVDATANA = 0
  w_IVDATANP = 0
  w_IVRIGQUE = 0
  w_IVTIPBEN = space(1)
  w_IVAUTCOM = space(1)
  w_IVFLGSER = space(1)
  w_IVRIGQUF = 0
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_IVFLGBSV = space(1)
  w_IVFLGBSA = space(1)
  w_IVCODINT = space(5)
  o_IVCODINT = space(5)
  w_IVCODINV = space(5)
  w_DESINT = space(35)
  w_IVDTOBSO = ctod('  /  /  ')
  w_IVDTINVA = ctod('  /  /  ')
  w_ACQINT = space(1)
  w_PERINT = 0
  w_PEIINT = 0
  w_OBTEST = space(10)
  w_DESINTV = space(35)
  w_ACQINTV = space(1)
  w_PERINTV = 0
  w_PEIINTV = 0
  w_IVCODAUV = space(5)
  w_DESINTA = space(35)
  w_AUTCOM = space(1)
  w_PERINTA = 0
  w_PEIINTA = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'VOCIIVA','gsar_aiv')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_aivPag1","gsar_aiv",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Codice IVA")
      .Pages(1).HelpContextID = 186024655
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oIVCODIVA_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='VOCIIVA'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.VOCIIVA_IDX,5],7]
    this.nPostItConn=i_TableProp[this.VOCIIVA_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_IVCODIVA = NVL(IVCODIVA,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_38_joined
    link_1_38_joined=.f.
    local link_1_39_joined
    link_1_39_joined=.f.
    local link_1_67_joined
    link_1_67_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from VOCIIVA where IVCODIVA=KeySet.IVCODIVA
    *
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('VOCIIVA')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "VOCIIVA.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' VOCIIVA '
      link_1_38_joined=this.AddJoinedLink_1_38(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_39_joined=this.AddJoinedLink_1_39(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_67_joined=this.AddJoinedLink_1_67(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'IVCODIVA',this.w_IVCODIVA  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESINT = space(35)
        .w_ACQINT = space(1)
        .w_PERINT = 0
        .w_PEIINT = 0
        .w_DESINTV = space(35)
        .w_ACQINTV = space(1)
        .w_PERINTV = 0
        .w_PEIINTV = 0
        .w_DESINTA = space(35)
        .w_AUTCOM = space(1)
        .w_PERINTA = 0
        .w_PEIINTA = 0
        .w_IVCODIVA = NVL(IVCODIVA,space(5))
        .w_IVDESIVA = NVL(IVDESIVA,space(35))
        .w_IVDESSUP = NVL(IVDESSUP,space(100))
        .w_IVPERIVA = NVL(IVPERIVA,0)
        .w_IVPERIND = NVL(IVPERIND,0)
        .w_IVPROIVA = NVL(IVPROIVA,space(1))
        .w_IVTIPPLA = NVL(IVTIPPLA,space(1))
        .w_IVPLAIVA = NVL(IVPLAIVA,space(1))
        .w_IVMACIVA = NVL(IVMACIVA,space(1))
        .w_IVIMPEXP = NVL(IVIMPEXP,space(1))
        .w_IVBOLIVA = NVL(IVBOLIVA,space(1))
        .w_IVSPLPAY = NVL(IVSPLPAY,space(1))
        .w_IVPERCOM = NVL(IVPERCOM,0)
        .w_IVTIPAGR = NVL(IVTIPAGR,space(1))
        .w_IVESINOD = NVL(IVESINOD,space(1))
        .w_IVREVCHA = NVL(IVREVCHA,space(1))
        .w_IVREGMAR = NVL(IVREGMAR,space(1))
        .w_IVPERMAR = NVL(IVPERMAR,0)
        .w_IVFLVAFF = NVL(IVFLVAFF,space(1))
        .w_IVACQINT = NVL(IVACQINT,space(1))
        .w_IVDATANA = NVL(IVDATANA,0)
        .w_IVDATANP = NVL(IVDATANP,0)
        .w_IVRIGQUE = NVL(IVRIGQUE,0)
        .w_IVTIPBEN = NVL(IVTIPBEN,space(1))
        .w_IVAUTCOM = NVL(IVAUTCOM,space(1))
        .w_IVFLGSER = NVL(IVFLGSER,space(1))
        .w_IVRIGQUF = NVL(IVRIGQUF,0)
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_IVFLGBSV = NVL(IVFLGBSV,space(1))
        .w_IVFLGBSA = NVL(IVFLGBSA,space(1))
        .w_IVCODINT = NVL(IVCODINT,space(5))
          if link_1_38_joined
            this.w_IVCODINT = NVL(IVCODIVA138,NVL(this.w_IVCODINT,space(5)))
            this.w_DESINT = NVL(IVDESIVA138,space(35))
            this.w_ACQINT = NVL(IVACQINT138,space(1))
            this.w_PERINT = NVL(IVPERIVA138,0)
            this.w_PEIINT = NVL(IVPERIND138,0)
          else
          .link_1_38('Load')
          endif
        .w_IVCODINV = NVL(IVCODINV,space(5))
          if link_1_39_joined
            this.w_IVCODINV = NVL(IVCODIVA139,NVL(this.w_IVCODINV,space(5)))
            this.w_DESINTV = NVL(IVDESIVA139,space(35))
            this.w_ACQINTV = NVL(IVACQINT139,space(1))
            this.w_PERINTV = NVL(IVPERIVA139,0)
            this.w_PEIINTV = NVL(IVPERIND139,0)
          else
          .link_1_39('Load')
          endif
        .w_IVDTOBSO = NVL(cp_ToDate(IVDTOBSO),ctod("  /  /  "))
        .w_IVDTINVA = NVL(cp_ToDate(IVDTINVA),ctod("  /  /  "))
        .w_OBTEST = i_datsys
        .w_IVCODAUV = NVL(IVCODAUV,space(5))
          if link_1_67_joined
            this.w_IVCODAUV = NVL(IVCODIVA167,NVL(this.w_IVCODAUV,space(5)))
            this.w_DESINTA = NVL(IVDESIVA167,space(35))
            this.w_PERINTA = NVL(IVPERIVA167,0)
            this.w_PEIINTA = NVL(IVPERIND167,0)
            this.w_AUTCOM = NVL(IVREVCHA167,space(1))
          else
          .link_1_67('Load')
          endif
        .oPgFrm.Page1.oPag.oObj_1_83.Calculate()
        cp_LoadRecExtFlds(this,'VOCIIVA')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_IVCODIVA = space(5)
      .w_IVDESIVA = space(35)
      .w_IVDESSUP = space(100)
      .w_IVPERIVA = 0
      .w_IVPERIND = 0
      .w_IVPROIVA = space(1)
      .w_IVTIPPLA = space(1)
      .w_IVPLAIVA = space(1)
      .w_IVMACIVA = space(1)
      .w_IVIMPEXP = space(1)
      .w_IVBOLIVA = space(1)
      .w_IVSPLPAY = space(1)
      .w_IVPERCOM = 0
      .w_IVTIPAGR = space(1)
      .w_IVESINOD = space(1)
      .w_IVREVCHA = space(1)
      .w_IVREGMAR = space(1)
      .w_IVPERMAR = 0
      .w_IVFLVAFF = space(1)
      .w_IVACQINT = space(1)
      .w_IVDATANA = 0
      .w_IVDATANP = 0
      .w_IVRIGQUE = 0
      .w_IVTIPBEN = space(1)
      .w_IVAUTCOM = space(1)
      .w_IVFLGSER = space(1)
      .w_IVRIGQUF = 0
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_IVFLGBSV = space(1)
      .w_IVFLGBSA = space(1)
      .w_IVCODINT = space(5)
      .w_IVCODINV = space(5)
      .w_DESINT = space(35)
      .w_IVDTOBSO = ctod("  /  /  ")
      .w_IVDTINVA = ctod("  /  /  ")
      .w_ACQINT = space(1)
      .w_PERINT = 0
      .w_PEIINT = 0
      .w_OBTEST = space(10)
      .w_DESINTV = space(35)
      .w_ACQINTV = space(1)
      .w_PERINTV = 0
      .w_PEIINTV = 0
      .w_IVCODAUV = space(5)
      .w_DESINTA = space(35)
      .w_AUTCOM = space(1)
      .w_PERINTA = 0
      .w_PEIINTA = 0
      if .cFunction<>"Filter"
          .DoRTCalc(1,4,.f.)
        .w_IVPERIND = 0
        .w_IVPROIVA = 'N'
        .w_IVTIPPLA = IIF(.w_IVPLAIVA = "S", "A", " ")
          .DoRTCalc(8,8,.f.)
        .w_IVMACIVA = 'N'
          .DoRTCalc(10,11,.f.)
        .w_IVSPLPAY = 'N'
          .DoRTCalc(13,13,.f.)
        .w_IVTIPAGR = 'N'
        .w_IVESINOD = iif(.w_IVPERIVA <> 0 or .w_IVREVCHA='S','N',.w_IVESINOD)
          .DoRTCalc(16,16,.f.)
        .w_IVREGMAR = ' '
        .w_IVPERMAR = iif(.w_IVREGMAR $ 'A-C',.w_IVPERMAR,0)
        .w_IVFLVAFF = 'N'
        .w_IVACQINT = 'N'
        .w_IVDATANA = 0
        .w_IVDATANP = 0
          .DoRTCalc(23,23,.f.)
        .w_IVTIPBEN = 'N'
          .DoRTCalc(25,25,.f.)
        .w_IVFLGSER = 'E'
          .DoRTCalc(27,31,.f.)
        .w_IVFLGBSV = 'N'
        .w_IVFLGBSA = 'N'
        .w_IVCODINT = SPACE(5)
        .DoRTCalc(34,34,.f.)
          if not(empty(.w_IVCODINT))
          .link_1_38('Full')
          endif
        .w_IVCODINV = SPACE(5)
        .DoRTCalc(35,35,.f.)
          if not(empty(.w_IVCODINV))
          .link_1_39('Full')
          endif
          .DoRTCalc(36,41,.f.)
        .w_OBTEST = i_datsys
          .DoRTCalc(43,46,.f.)
        .w_IVCODAUV = SPACE(5)
        .DoRTCalc(47,47,.f.)
          if not(empty(.w_IVCODAUV))
          .link_1_67('Full')
          endif
        .oPgFrm.Page1.oPag.oObj_1_83.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'VOCIIVA')
    this.DoRTCalc(48,51,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsar_aiv
    this.w_IVDATANA = 1
    this.w_IVDATANP = 1
    this.SetControlsValue()
    this.w_IVDATANA = 0
    this.w_IVDATANP = 0
    this.SetControlsValue()
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oIVCODIVA_1_1.enabled = i_bVal
      .Page1.oPag.oIVDESIVA_1_2.enabled = i_bVal
      .Page1.oPag.oIVDESSUP_1_3.enabled = i_bVal
      .Page1.oPag.oIVPERIVA_1_4.enabled = i_bVal
      .Page1.oPag.oIVPERIND_1_5.enabled = i_bVal
      .Page1.oPag.oIVPROIVA_1_6.enabled = i_bVal
      .Page1.oPag.oIVTIPPLA_1_7.enabled = i_bVal
      .Page1.oPag.oIVPLAIVA_1_8.enabled = i_bVal
      .Page1.oPag.oIVMACIVA_1_9.enabled = i_bVal
      .Page1.oPag.oIVIMPEXP_1_10.enabled = i_bVal
      .Page1.oPag.oIVBOLIVA_1_11.enabled = i_bVal
      .Page1.oPag.oIVSPLPAY_1_12.enabled = i_bVal
      .Page1.oPag.oIVPERCOM_1_13.enabled = i_bVal
      .Page1.oPag.oIVTIPAGR_1_14.enabled = i_bVal
      .Page1.oPag.oIVESINOD_1_15.enabled = i_bVal
      .Page1.oPag.oIVREVCHA_1_16.enabled = i_bVal
      .Page1.oPag.oIVREGMAR_1_17.enabled = i_bVal
      .Page1.oPag.oIVPERMAR_1_18.enabled = i_bVal
      .Page1.oPag.oIVFLVAFF_1_19.enabled = i_bVal
      .Page1.oPag.oIVACQINT_1_20.enabled = i_bVal
      .Page1.oPag.oIVDATANA_1_21.enabled = i_bVal
      .Page1.oPag.oIVDATANP_1_22.enabled = i_bVal
      .Page1.oPag.oIVTIPBEN_1_24.enabled = i_bVal
      .Page1.oPag.oIVAUTCOM_1_25.enabled = i_bVal
      .Page1.oPag.oIVFLGSER_1_26.enabled = i_bVal
      .Page1.oPag.oIVFLGBSV_1_35.enabled = i_bVal
      .Page1.oPag.oIVFLGBSA_1_36.enabled = i_bVal
      .Page1.oPag.oIVCODINT_1_38.enabled = i_bVal
      .Page1.oPag.oIVCODINV_1_39.enabled = i_bVal
      .Page1.oPag.oIVDTOBSO_1_41.enabled = i_bVal
      .Page1.oPag.oIVDTINVA_1_44.enabled = i_bVal
      .Page1.oPag.oIVCODAUV_1_67.enabled = i_bVal
      .Page1.oPag.oObj_1_83.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oIVCODIVA_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oIVCODIVA_1_1.enabled = .t.
        .Page1.oPag.oIVDESIVA_1_2.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'VOCIIVA',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IVCODIVA,"IVCODIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IVDESIVA,"IVDESIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IVDESSUP,"IVDESSUP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IVPERIVA,"IVPERIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IVPERIND,"IVPERIND",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IVPROIVA,"IVPROIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IVTIPPLA,"IVTIPPLA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IVPLAIVA,"IVPLAIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IVMACIVA,"IVMACIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IVIMPEXP,"IVIMPEXP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IVBOLIVA,"IVBOLIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IVSPLPAY,"IVSPLPAY",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IVPERCOM,"IVPERCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IVTIPAGR,"IVTIPAGR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IVESINOD,"IVESINOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IVREVCHA,"IVREVCHA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IVREGMAR,"IVREGMAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IVPERMAR,"IVPERMAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IVFLVAFF,"IVFLVAFF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IVACQINT,"IVACQINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IVDATANA,"IVDATANA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IVDATANP,"IVDATANP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IVRIGQUE,"IVRIGQUE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IVTIPBEN,"IVTIPBEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IVAUTCOM,"IVAUTCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IVFLGSER,"IVFLGSER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IVRIGQUF,"IVRIGQUF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IVFLGBSV,"IVFLGBSV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IVFLGBSA,"IVFLGBSA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IVCODINT,"IVCODINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IVCODINV,"IVCODINV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IVDTOBSO,"IVDTOBSO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IVDTINVA,"IVDTINVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IVCODAUV,"IVCODAUV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    i_lTable = "VOCIIVA"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.VOCIIVA_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSAR_SVI with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.VOCIIVA_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into VOCIIVA
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'VOCIIVA')
        i_extval=cp_InsertValODBCExtFlds(this,'VOCIIVA')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(IVCODIVA,IVDESIVA,IVDESSUP,IVPERIVA,IVPERIND"+;
                  ",IVPROIVA,IVTIPPLA,IVPLAIVA,IVMACIVA,IVIMPEXP"+;
                  ",IVBOLIVA,IVSPLPAY,IVPERCOM,IVTIPAGR,IVESINOD"+;
                  ",IVREVCHA,IVREGMAR,IVPERMAR,IVFLVAFF,IVACQINT"+;
                  ",IVDATANA,IVDATANP,IVRIGQUE,IVTIPBEN,IVAUTCOM"+;
                  ",IVFLGSER,IVRIGQUF,UTCC,UTCV,UTDC"+;
                  ",UTDV,IVFLGBSV,IVFLGBSA,IVCODINT,IVCODINV"+;
                  ",IVDTOBSO,IVDTINVA,IVCODAUV "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_IVCODIVA)+;
                  ","+cp_ToStrODBC(this.w_IVDESIVA)+;
                  ","+cp_ToStrODBC(this.w_IVDESSUP)+;
                  ","+cp_ToStrODBC(this.w_IVPERIVA)+;
                  ","+cp_ToStrODBC(this.w_IVPERIND)+;
                  ","+cp_ToStrODBC(this.w_IVPROIVA)+;
                  ","+cp_ToStrODBC(this.w_IVTIPPLA)+;
                  ","+cp_ToStrODBC(this.w_IVPLAIVA)+;
                  ","+cp_ToStrODBC(this.w_IVMACIVA)+;
                  ","+cp_ToStrODBC(this.w_IVIMPEXP)+;
                  ","+cp_ToStrODBC(this.w_IVBOLIVA)+;
                  ","+cp_ToStrODBC(this.w_IVSPLPAY)+;
                  ","+cp_ToStrODBC(this.w_IVPERCOM)+;
                  ","+cp_ToStrODBC(this.w_IVTIPAGR)+;
                  ","+cp_ToStrODBC(this.w_IVESINOD)+;
                  ","+cp_ToStrODBC(this.w_IVREVCHA)+;
                  ","+cp_ToStrODBC(this.w_IVREGMAR)+;
                  ","+cp_ToStrODBC(this.w_IVPERMAR)+;
                  ","+cp_ToStrODBC(this.w_IVFLVAFF)+;
                  ","+cp_ToStrODBC(this.w_IVACQINT)+;
                  ","+cp_ToStrODBC(this.w_IVDATANA)+;
                  ","+cp_ToStrODBC(this.w_IVDATANP)+;
                  ","+cp_ToStrODBC(this.w_IVRIGQUE)+;
                  ","+cp_ToStrODBC(this.w_IVTIPBEN)+;
                  ","+cp_ToStrODBC(this.w_IVAUTCOM)+;
                  ","+cp_ToStrODBC(this.w_IVFLGSER)+;
                  ","+cp_ToStrODBC(this.w_IVRIGQUF)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBC(this.w_IVFLGBSV)+;
                  ","+cp_ToStrODBC(this.w_IVFLGBSA)+;
                  ","+cp_ToStrODBCNull(this.w_IVCODINT)+;
                  ","+cp_ToStrODBCNull(this.w_IVCODINV)+;
                  ","+cp_ToStrODBC(this.w_IVDTOBSO)+;
                  ","+cp_ToStrODBC(this.w_IVDTINVA)+;
                  ","+cp_ToStrODBCNull(this.w_IVCODAUV)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'VOCIIVA')
        i_extval=cp_InsertValVFPExtFlds(this,'VOCIIVA')
        cp_CheckDeletedKey(i_cTable,0,'IVCODIVA',this.w_IVCODIVA)
        INSERT INTO (i_cTable);
              (IVCODIVA,IVDESIVA,IVDESSUP,IVPERIVA,IVPERIND,IVPROIVA,IVTIPPLA,IVPLAIVA,IVMACIVA,IVIMPEXP,IVBOLIVA,IVSPLPAY,IVPERCOM,IVTIPAGR,IVESINOD,IVREVCHA,IVREGMAR,IVPERMAR,IVFLVAFF,IVACQINT,IVDATANA,IVDATANP,IVRIGQUE,IVTIPBEN,IVAUTCOM,IVFLGSER,IVRIGQUF,UTCC,UTCV,UTDC,UTDV,IVFLGBSV,IVFLGBSA,IVCODINT,IVCODINV,IVDTOBSO,IVDTINVA,IVCODAUV  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_IVCODIVA;
                  ,this.w_IVDESIVA;
                  ,this.w_IVDESSUP;
                  ,this.w_IVPERIVA;
                  ,this.w_IVPERIND;
                  ,this.w_IVPROIVA;
                  ,this.w_IVTIPPLA;
                  ,this.w_IVPLAIVA;
                  ,this.w_IVMACIVA;
                  ,this.w_IVIMPEXP;
                  ,this.w_IVBOLIVA;
                  ,this.w_IVSPLPAY;
                  ,this.w_IVPERCOM;
                  ,this.w_IVTIPAGR;
                  ,this.w_IVESINOD;
                  ,this.w_IVREVCHA;
                  ,this.w_IVREGMAR;
                  ,this.w_IVPERMAR;
                  ,this.w_IVFLVAFF;
                  ,this.w_IVACQINT;
                  ,this.w_IVDATANA;
                  ,this.w_IVDATANP;
                  ,this.w_IVRIGQUE;
                  ,this.w_IVTIPBEN;
                  ,this.w_IVAUTCOM;
                  ,this.w_IVFLGSER;
                  ,this.w_IVRIGQUF;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_IVFLGBSV;
                  ,this.w_IVFLGBSA;
                  ,this.w_IVCODINT;
                  ,this.w_IVCODINV;
                  ,this.w_IVDTOBSO;
                  ,this.w_IVDTINVA;
                  ,this.w_IVCODAUV;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.VOCIIVA_IDX,i_nConn)
      *
      * update VOCIIVA
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'VOCIIVA')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " IVDESIVA="+cp_ToStrODBC(this.w_IVDESIVA)+;
             ",IVDESSUP="+cp_ToStrODBC(this.w_IVDESSUP)+;
             ",IVPERIVA="+cp_ToStrODBC(this.w_IVPERIVA)+;
             ",IVPERIND="+cp_ToStrODBC(this.w_IVPERIND)+;
             ",IVPROIVA="+cp_ToStrODBC(this.w_IVPROIVA)+;
             ",IVTIPPLA="+cp_ToStrODBC(this.w_IVTIPPLA)+;
             ",IVPLAIVA="+cp_ToStrODBC(this.w_IVPLAIVA)+;
             ",IVMACIVA="+cp_ToStrODBC(this.w_IVMACIVA)+;
             ",IVIMPEXP="+cp_ToStrODBC(this.w_IVIMPEXP)+;
             ",IVBOLIVA="+cp_ToStrODBC(this.w_IVBOLIVA)+;
             ",IVSPLPAY="+cp_ToStrODBC(this.w_IVSPLPAY)+;
             ",IVPERCOM="+cp_ToStrODBC(this.w_IVPERCOM)+;
             ",IVTIPAGR="+cp_ToStrODBC(this.w_IVTIPAGR)+;
             ",IVESINOD="+cp_ToStrODBC(this.w_IVESINOD)+;
             ",IVREVCHA="+cp_ToStrODBC(this.w_IVREVCHA)+;
             ",IVREGMAR="+cp_ToStrODBC(this.w_IVREGMAR)+;
             ",IVPERMAR="+cp_ToStrODBC(this.w_IVPERMAR)+;
             ",IVFLVAFF="+cp_ToStrODBC(this.w_IVFLVAFF)+;
             ",IVACQINT="+cp_ToStrODBC(this.w_IVACQINT)+;
             ",IVDATANA="+cp_ToStrODBC(this.w_IVDATANA)+;
             ",IVDATANP="+cp_ToStrODBC(this.w_IVDATANP)+;
             ",IVRIGQUE="+cp_ToStrODBC(this.w_IVRIGQUE)+;
             ",IVTIPBEN="+cp_ToStrODBC(this.w_IVTIPBEN)+;
             ",IVAUTCOM="+cp_ToStrODBC(this.w_IVAUTCOM)+;
             ",IVFLGSER="+cp_ToStrODBC(this.w_IVFLGSER)+;
             ",IVRIGQUF="+cp_ToStrODBC(this.w_IVRIGQUF)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",IVFLGBSV="+cp_ToStrODBC(this.w_IVFLGBSV)+;
             ",IVFLGBSA="+cp_ToStrODBC(this.w_IVFLGBSA)+;
             ",IVCODINT="+cp_ToStrODBCNull(this.w_IVCODINT)+;
             ",IVCODINV="+cp_ToStrODBCNull(this.w_IVCODINV)+;
             ",IVDTOBSO="+cp_ToStrODBC(this.w_IVDTOBSO)+;
             ",IVDTINVA="+cp_ToStrODBC(this.w_IVDTINVA)+;
             ",IVCODAUV="+cp_ToStrODBCNull(this.w_IVCODAUV)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'VOCIIVA')
        i_cWhere = cp_PKFox(i_cTable  ,'IVCODIVA',this.w_IVCODIVA  )
        UPDATE (i_cTable) SET;
              IVDESIVA=this.w_IVDESIVA;
             ,IVDESSUP=this.w_IVDESSUP;
             ,IVPERIVA=this.w_IVPERIVA;
             ,IVPERIND=this.w_IVPERIND;
             ,IVPROIVA=this.w_IVPROIVA;
             ,IVTIPPLA=this.w_IVTIPPLA;
             ,IVPLAIVA=this.w_IVPLAIVA;
             ,IVMACIVA=this.w_IVMACIVA;
             ,IVIMPEXP=this.w_IVIMPEXP;
             ,IVBOLIVA=this.w_IVBOLIVA;
             ,IVSPLPAY=this.w_IVSPLPAY;
             ,IVPERCOM=this.w_IVPERCOM;
             ,IVTIPAGR=this.w_IVTIPAGR;
             ,IVESINOD=this.w_IVESINOD;
             ,IVREVCHA=this.w_IVREVCHA;
             ,IVREGMAR=this.w_IVREGMAR;
             ,IVPERMAR=this.w_IVPERMAR;
             ,IVFLVAFF=this.w_IVFLVAFF;
             ,IVACQINT=this.w_IVACQINT;
             ,IVDATANA=this.w_IVDATANA;
             ,IVDATANP=this.w_IVDATANP;
             ,IVRIGQUE=this.w_IVRIGQUE;
             ,IVTIPBEN=this.w_IVTIPBEN;
             ,IVAUTCOM=this.w_IVAUTCOM;
             ,IVFLGSER=this.w_IVFLGSER;
             ,IVRIGQUF=this.w_IVRIGQUF;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,IVFLGBSV=this.w_IVFLGBSV;
             ,IVFLGBSA=this.w_IVFLGBSA;
             ,IVCODINT=this.w_IVCODINT;
             ,IVCODINV=this.w_IVCODINV;
             ,IVDTOBSO=this.w_IVDTOBSO;
             ,IVDTINVA=this.w_IVDTINVA;
             ,IVCODAUV=this.w_IVCODAUV;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.VOCIIVA_IDX,i_nConn)
      *
      * delete VOCIIVA
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'IVCODIVA',this.w_IVCODIVA  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
        if .o_IVPERIVA<>.w_IVPERIVA
            .w_IVPERIND = 0
        endif
        .DoRTCalc(6,6,.t.)
        if .o_IVPLAIVA<>.w_IVPLAIVA
            .w_IVTIPPLA = IIF(.w_IVPLAIVA = "S", "A", " ")
        endif
        .DoRTCalc(8,14,.t.)
        if .o_IVPERIVA <>.w_IVPERIVA .or. .o_IVREVCHA<>.w_IVREVCHA
            .w_IVESINOD = iif(.w_IVPERIVA <> 0 or .w_IVREVCHA='S','N',.w_IVESINOD)
        endif
        .DoRTCalc(16,17,.t.)
        if .o_IVREGMAR<>.w_IVREGMAR
            .w_IVPERMAR = iif(.w_IVREGMAR $ 'A-C',.w_IVPERMAR,0)
        endif
        .DoRTCalc(19,33,.t.)
        if .o_IVPERIVA<>.w_IVPERIVA.or. .o_IVPERIND<>.w_IVPERIND.or. .o_IVACQINT<>.w_IVACQINT
            .w_IVCODINT = SPACE(5)
          .link_1_38('Full')
        endif
        if .o_IVPERIVA<>.w_IVPERIVA.or. .o_IVPERIND<>.w_IVPERIND.or. .o_IVACQINT<>.w_IVACQINT.or. .o_IVCODINT<>.w_IVCODINT
            .w_IVCODINV = SPACE(5)
          .link_1_39('Full')
        endif
        .DoRTCalc(36,41,.t.)
            .w_OBTEST = i_datsys
        .DoRTCalc(43,46,.t.)
        if .o_IVPERIVA<>.w_IVPERIVA.or. .o_IVPERIND<>.w_IVPERIND.or. .o_IVACQINT<>.w_IVACQINT
            .w_IVCODAUV = SPACE(5)
          .link_1_67('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_83.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(48,51,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_83.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oIVPERIND_1_5.enabled = this.oPgFrm.Page1.oPag.oIVPERIND_1_5.mCond()
    this.oPgFrm.Page1.oPag.oIVBOLIVA_1_11.enabled = this.oPgFrm.Page1.oPag.oIVBOLIVA_1_11.mCond()
    this.oPgFrm.Page1.oPag.oIVESINOD_1_15.enabled = this.oPgFrm.Page1.oPag.oIVESINOD_1_15.mCond()
    this.oPgFrm.Page1.oPag.oIVPERMAR_1_18.enabled = this.oPgFrm.Page1.oPag.oIVPERMAR_1_18.mCond()
    this.oPgFrm.Page1.oPag.oIVCODINT_1_38.enabled = this.oPgFrm.Page1.oPag.oIVCODINT_1_38.mCond()
    this.oPgFrm.Page1.oPag.oIVCODINV_1_39.enabled = this.oPgFrm.Page1.oPag.oIVCODINV_1_39.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oIVTIPPLA_1_7.visible=!this.oPgFrm.Page1.oPag.oIVTIPPLA_1_7.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_60.visible=!this.oPgFrm.Page1.oPag.oStr_1_60.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_83.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=IVCODINT
  func Link_1_38(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IVCODINT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_IVCODINT)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVACQINT,IVPERIVA,IVPERIND";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_IVCODINT))
          select IVCODIVA,IVDESIVA,IVACQINT,IVPERIVA,IVPERIND;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IVCODINT)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStrODBC(trim(this.w_IVCODINT)+"%");

            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVACQINT,IVPERIVA,IVPERIND";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStr(trim(this.w_IVCODINT)+"%");

            select IVCODIVA,IVDESIVA,IVACQINT,IVPERIVA,IVPERIND;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_IVCODINT) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oIVCODINT_1_38'),i_cWhere,'GSAR_AIV',"Voci IVA",'GSAR1AIV.VOCIIVA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVACQINT,IVPERIVA,IVPERIND";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDESIVA,IVACQINT,IVPERIVA,IVPERIND;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IVCODINT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVACQINT,IVPERIVA,IVPERIND";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_IVCODINT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_IVCODINT)
            select IVCODIVA,IVDESIVA,IVACQINT,IVPERIVA,IVPERIND;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IVCODINT = NVL(_Link_.IVCODIVA,space(5))
      this.w_DESINT = NVL(_Link_.IVDESIVA,space(35))
      this.w_ACQINT = NVL(_Link_.IVACQINT,space(1))
      this.w_PERINT = NVL(_Link_.IVPERIVA,0)
      this.w_PEIINT = NVL(_Link_.IVPERIND,0)
    else
      if i_cCtrl<>'Load'
        this.w_IVCODINT = space(5)
      endif
      this.w_DESINT = space(35)
      this.w_ACQINT = space(1)
      this.w_PERINT = 0
      this.w_PEIINT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_IVCODINT) OR .w_IVPERIVA=.w_PERINT AND .w_IVPERIND=.w_PEIINT AND .w_ACQINT='A'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice IVA incongruente o obsoleto")
        endif
        this.w_IVCODINT = space(5)
        this.w_DESINT = space(35)
        this.w_ACQINT = space(1)
        this.w_PERINT = 0
        this.w_PEIINT = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IVCODINT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_38(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOCIIVA_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_38.IVCODIVA as IVCODIVA138"+ ",link_1_38.IVDESIVA as IVDESIVA138"+ ",link_1_38.IVACQINT as IVACQINT138"+ ",link_1_38.IVPERIVA as IVPERIVA138"+ ",link_1_38.IVPERIND as IVPERIND138"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_38 on VOCIIVA.IVCODINT=link_1_38.IVCODIVA"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_38"
          i_cKey=i_cKey+'+" and VOCIIVA.IVCODINT=link_1_38.IVCODIVA(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=IVCODINV
  func Link_1_39(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IVCODINV) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_IVCODINV)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVACQINT,IVPERIVA,IVPERIND";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_IVCODINV))
          select IVCODIVA,IVDESIVA,IVACQINT,IVPERIVA,IVPERIND;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IVCODINV)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStrODBC(trim(this.w_IVCODINV)+"%");

            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVACQINT,IVPERIVA,IVPERIND";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStr(trim(this.w_IVCODINV)+"%");

            select IVCODIVA,IVDESIVA,IVACQINT,IVPERIVA,IVPERIND;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_IVCODINV) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oIVCODINV_1_39'),i_cWhere,'GSAR_AIV',"Voci IVA",'GSAR1AIV.VOCIIVA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVACQINT,IVPERIVA,IVPERIND";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDESIVA,IVACQINT,IVPERIVA,IVPERIND;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IVCODINV)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVACQINT,IVPERIVA,IVPERIND";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_IVCODINV);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_IVCODINV)
            select IVCODIVA,IVDESIVA,IVACQINT,IVPERIVA,IVPERIND;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IVCODINV = NVL(_Link_.IVCODIVA,space(5))
      this.w_DESINTV = NVL(_Link_.IVDESIVA,space(35))
      this.w_ACQINTV = NVL(_Link_.IVACQINT,space(1))
      this.w_PERINTV = NVL(_Link_.IVPERIVA,0)
      this.w_PEIINTV = NVL(_Link_.IVPERIND,0)
    else
      if i_cCtrl<>'Load'
        this.w_IVCODINV = space(5)
      endif
      this.w_DESINTV = space(35)
      this.w_ACQINTV = space(1)
      this.w_PERINTV = 0
      this.w_PEIINTV = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_IVPERIVA=.w_PERINTV AND (.w_IVPERIND=.w_PEIINTV OR .w_PEIINTV=0) AND .w_ACQINTV='A'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice IVA incongruente o obsoleto")
        endif
        this.w_IVCODINV = space(5)
        this.w_DESINTV = space(35)
        this.w_ACQINTV = space(1)
        this.w_PERINTV = 0
        this.w_PEIINTV = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IVCODINV Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_39(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOCIIVA_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_39.IVCODIVA as IVCODIVA139"+ ",link_1_39.IVDESIVA as IVDESIVA139"+ ",link_1_39.IVACQINT as IVACQINT139"+ ",link_1_39.IVPERIVA as IVPERIVA139"+ ",link_1_39.IVPERIND as IVPERIND139"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_39 on VOCIIVA.IVCODINV=link_1_39.IVCODIVA"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_39"
          i_cKey=i_cKey+'+" and VOCIIVA.IVCODINV=link_1_39.IVCODIVA(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=IVCODAUV
  func Link_1_67(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IVCODAUV) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_IVCODAUV)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVPERIVA,IVPERIND,IVREVCHA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_IVCODAUV))
          select IVCODIVA,IVDESIVA,IVPERIVA,IVPERIND,IVREVCHA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IVCODAUV)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStrODBC(trim(this.w_IVCODAUV)+"%");

            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVPERIVA,IVPERIND,IVREVCHA";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStr(trim(this.w_IVCODAUV)+"%");

            select IVCODIVA,IVDESIVA,IVPERIVA,IVPERIND,IVREVCHA;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_IVCODAUV) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oIVCODAUV_1_67'),i_cWhere,'GSAR_AIV',"Voci IVA",'GSAR2AIV.VOCIIVA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVPERIVA,IVPERIND,IVREVCHA";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDESIVA,IVPERIVA,IVPERIND,IVREVCHA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IVCODAUV)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVPERIVA,IVPERIND,IVREVCHA";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_IVCODAUV);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_IVCODAUV)
            select IVCODIVA,IVDESIVA,IVPERIVA,IVPERIND,IVREVCHA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IVCODAUV = NVL(_Link_.IVCODIVA,space(5))
      this.w_DESINTA = NVL(_Link_.IVDESIVA,space(35))
      this.w_PERINTA = NVL(_Link_.IVPERIVA,0)
      this.w_PEIINTA = NVL(_Link_.IVPERIND,0)
      this.w_AUTCOM = NVL(_Link_.IVREVCHA,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_IVCODAUV = space(5)
      endif
      this.w_DESINTA = space(35)
      this.w_PERINTA = 0
      this.w_PEIINTA = 0
      this.w_AUTCOM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_IVPERIVA=.w_PERINTA AND .w_PEIINTA=0 AND NVL(.w_AUTCOM,' ')='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice IVA incongruente o non di tipo Reverse Charge o obsoleto")
        endif
        this.w_IVCODAUV = space(5)
        this.w_DESINTA = space(35)
        this.w_PERINTA = 0
        this.w_PEIINTA = 0
        this.w_AUTCOM = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IVCODAUV Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_67(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOCIIVA_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_67.IVCODIVA as IVCODIVA167"+ ",link_1_67.IVDESIVA as IVDESIVA167"+ ",link_1_67.IVPERIVA as IVPERIVA167"+ ",link_1_67.IVPERIND as IVPERIND167"+ ",link_1_67.IVREVCHA as IVREVCHA167"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_67 on VOCIIVA.IVCODAUV=link_1_67.IVCODIVA"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_67"
          i_cKey=i_cKey+'+" and VOCIIVA.IVCODAUV=link_1_67.IVCODIVA(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oIVCODIVA_1_1.value==this.w_IVCODIVA)
      this.oPgFrm.Page1.oPag.oIVCODIVA_1_1.value=this.w_IVCODIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oIVDESIVA_1_2.value==this.w_IVDESIVA)
      this.oPgFrm.Page1.oPag.oIVDESIVA_1_2.value=this.w_IVDESIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oIVDESSUP_1_3.value==this.w_IVDESSUP)
      this.oPgFrm.Page1.oPag.oIVDESSUP_1_3.value=this.w_IVDESSUP
    endif
    if not(this.oPgFrm.Page1.oPag.oIVPERIVA_1_4.value==this.w_IVPERIVA)
      this.oPgFrm.Page1.oPag.oIVPERIVA_1_4.value=this.w_IVPERIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oIVPERIND_1_5.value==this.w_IVPERIND)
      this.oPgFrm.Page1.oPag.oIVPERIND_1_5.value=this.w_IVPERIND
    endif
    if not(this.oPgFrm.Page1.oPag.oIVPROIVA_1_6.RadioValue()==this.w_IVPROIVA)
      this.oPgFrm.Page1.oPag.oIVPROIVA_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIVTIPPLA_1_7.RadioValue()==this.w_IVTIPPLA)
      this.oPgFrm.Page1.oPag.oIVTIPPLA_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIVPLAIVA_1_8.RadioValue()==this.w_IVPLAIVA)
      this.oPgFrm.Page1.oPag.oIVPLAIVA_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIVMACIVA_1_9.RadioValue()==this.w_IVMACIVA)
      this.oPgFrm.Page1.oPag.oIVMACIVA_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIVIMPEXP_1_10.RadioValue()==this.w_IVIMPEXP)
      this.oPgFrm.Page1.oPag.oIVIMPEXP_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIVBOLIVA_1_11.RadioValue()==this.w_IVBOLIVA)
      this.oPgFrm.Page1.oPag.oIVBOLIVA_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIVSPLPAY_1_12.RadioValue()==this.w_IVSPLPAY)
      this.oPgFrm.Page1.oPag.oIVSPLPAY_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIVPERCOM_1_13.value==this.w_IVPERCOM)
      this.oPgFrm.Page1.oPag.oIVPERCOM_1_13.value=this.w_IVPERCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oIVTIPAGR_1_14.RadioValue()==this.w_IVTIPAGR)
      this.oPgFrm.Page1.oPag.oIVTIPAGR_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIVESINOD_1_15.RadioValue()==this.w_IVESINOD)
      this.oPgFrm.Page1.oPag.oIVESINOD_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIVREVCHA_1_16.RadioValue()==this.w_IVREVCHA)
      this.oPgFrm.Page1.oPag.oIVREVCHA_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIVREGMAR_1_17.RadioValue()==this.w_IVREGMAR)
      this.oPgFrm.Page1.oPag.oIVREGMAR_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIVPERMAR_1_18.value==this.w_IVPERMAR)
      this.oPgFrm.Page1.oPag.oIVPERMAR_1_18.value=this.w_IVPERMAR
    endif
    if not(this.oPgFrm.Page1.oPag.oIVFLVAFF_1_19.RadioValue()==this.w_IVFLVAFF)
      this.oPgFrm.Page1.oPag.oIVFLVAFF_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIVACQINT_1_20.RadioValue()==this.w_IVACQINT)
      this.oPgFrm.Page1.oPag.oIVACQINT_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIVDATANA_1_21.RadioValue()==this.w_IVDATANA)
      this.oPgFrm.Page1.oPag.oIVDATANA_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIVDATANP_1_22.RadioValue()==this.w_IVDATANP)
      this.oPgFrm.Page1.oPag.oIVDATANP_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIVTIPBEN_1_24.RadioValue()==this.w_IVTIPBEN)
      this.oPgFrm.Page1.oPag.oIVTIPBEN_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIVAUTCOM_1_25.RadioValue()==this.w_IVAUTCOM)
      this.oPgFrm.Page1.oPag.oIVAUTCOM_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIVFLGSER_1_26.RadioValue()==this.w_IVFLGSER)
      this.oPgFrm.Page1.oPag.oIVFLGSER_1_26.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIVFLGBSV_1_35.RadioValue()==this.w_IVFLGBSV)
      this.oPgFrm.Page1.oPag.oIVFLGBSV_1_35.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIVFLGBSA_1_36.RadioValue()==this.w_IVFLGBSA)
      this.oPgFrm.Page1.oPag.oIVFLGBSA_1_36.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIVCODINT_1_38.value==this.w_IVCODINT)
      this.oPgFrm.Page1.oPag.oIVCODINT_1_38.value=this.w_IVCODINT
    endif
    if not(this.oPgFrm.Page1.oPag.oIVCODINV_1_39.value==this.w_IVCODINV)
      this.oPgFrm.Page1.oPag.oIVCODINV_1_39.value=this.w_IVCODINV
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINT_1_40.value==this.w_DESINT)
      this.oPgFrm.Page1.oPag.oDESINT_1_40.value=this.w_DESINT
    endif
    if not(this.oPgFrm.Page1.oPag.oIVDTOBSO_1_41.value==this.w_IVDTOBSO)
      this.oPgFrm.Page1.oPag.oIVDTOBSO_1_41.value=this.w_IVDTOBSO
    endif
    if not(this.oPgFrm.Page1.oPag.oIVDTINVA_1_44.value==this.w_IVDTINVA)
      this.oPgFrm.Page1.oPag.oIVDTINVA_1_44.value=this.w_IVDTINVA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINTV_1_62.value==this.w_DESINTV)
      this.oPgFrm.Page1.oPag.oDESINTV_1_62.value=this.w_DESINTV
    endif
    if not(this.oPgFrm.Page1.oPag.oIVCODAUV_1_67.value==this.w_IVCODAUV)
      this.oPgFrm.Page1.oPag.oIVCODAUV_1_67.value=this.w_IVCODAUV
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINTA_1_68.value==this.w_DESINTA)
      this.oPgFrm.Page1.oPag.oDESINTA_1_68.value=this.w_DESINTA
    endif
    cp_SetControlsValueExtFlds(this,'VOCIIVA')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_IVCODIVA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIVCODIVA_1_1.SetFocus()
            i_bnoObbl = !empty(.w_IVCODIVA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_IVPERIND=0 OR EMPTY(.w_IVPLAIVA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIVPLAIVA_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Percentuale IVA indetraibile incongruente")
          case   not(EMPTY(.w_IVCODINT) OR .w_IVPERIVA=.w_PERINT AND .w_IVPERIND=.w_PEIINT AND .w_ACQINT='A')  and (.w_IVACQINT='N' Or .w_IVACQINT='I')  and not(empty(.w_IVCODINT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIVCODINT_1_38.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice IVA incongruente o obsoleto")
          case   not(.w_IVPERIVA=.w_PERINTV AND (.w_IVPERIND=.w_PEIINTV OR .w_PEIINTV=0) AND .w_ACQINTV='A')  and ((.w_IVACQINT='N' Or .w_IVACQINT='I') AND NOT EMPTY(.w_IVCODINT))  and not(empty(.w_IVCODINV))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIVCODINV_1_39.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice IVA incongruente o obsoleto")
          case   not(.w_IVPERIVA=.w_PERINTA AND .w_PEIINTA=0 AND NVL(.w_AUTCOM,' ')='S')  and not(empty(.w_IVCODAUV))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIVCODAUV_1_67.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice IVA incongruente o non di tipo Reverse Charge o obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsar_aiv
      * Verifico se l'utente ha impostato il campo comunicazione fiscalit�
      * privilegiata a 'Beni' oppure 'Servizi' e la combo "Tipologia operazione IVA 
      * ciclo acquisti/vendite' a 'Cessione di beni/Prestazione di servizi' oppure 'Acquisto
      * di beni oppure acquisizione di servizi
      if  .w_IVFLGSER $ 'BS' And (.w_IVFLGBSV $ 'BS' OR .w_IVFLGBSA $ 'BS')
         if ah_yesno ('Attenzione: le operazioni interessate dalla%0Comunicazione fiscalit� privilegiata sono normalmente escluse%0dalla Comunicazione operazioni superiori a 3000 euro.%0Confermi ugualmente?')
           * Confermo il salvataggio del record
         else
           i_bnoChk = .f.
           i_bRes = .f.
      		 i_cErrorMsg = Ah_MsgFormat("Riconsiderare i parametri relativi a 'Fiscalit� privilegiata' e 'Comunicazione operazioni superiori a 3.000 euro'")
         endif
      endif
      if i_bRes
      	if (! empty(.w_IVPERMAR) and ! empty(.w_IVPERIVA)) or (.w_IVREGMAR # 'S' and not empty(.w_IVREGMAR) and empty(.w_IVPERMAR) and ! empty(.w_IVPERIVA))
      	   * Messaggio di warning
      	   ah_ErrorMsg("Attenzione: � presente una percentuale aliquota IVA oltre all'aliquota del margine.",48,'')
      	endif
      	if .w_IVREGMAR = 'C' and empty(.w_IVPERMAR)
      	   * Messaggio di warning
      	   ah_ErrorMsg("Attenzione: percentuale aliquota del margine � uguale a zero.",48,'')
      	endif
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_IVPERIVA = this.w_IVPERIVA
    this.o_IVPERIND = this.w_IVPERIND
    this.o_IVPLAIVA = this.w_IVPLAIVA
    this.o_IVREVCHA = this.w_IVREVCHA
    this.o_IVREGMAR = this.w_IVREGMAR
    this.o_IVACQINT = this.w_IVACQINT
    this.o_IVCODINT = this.w_IVCODINT
    return

enddefine

* --- Define pages as container
define class tgsar_aivPag1 as StdContainer
  Width  = 569
  height = 567
  stdWidth  = 569
  stdheight = 567
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oIVCODIVA_1_1 as StdField with uid="UUDVSAAPPV",rtseq=1,rtrep=.f.,;
    cFormVar = "w_IVCODIVA", cQueryName = "IVCODIVA",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 218764231,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=58, Left=65, Top=10, InputMask=replicate('X',5)

  add object oIVDESIVA_1_2 as StdField with uid="RYHPMHJAED",rtseq=2,rtrep=.f.,;
    cFormVar = "w_IVDESIVA", cQueryName = "IVDESIVA",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione dell'aliquota IVA",;
    HelpContextID = 233841607,;
   bGlobalFont=.t.,;
    Height=21, Width=406, Left=126, Top=10, InputMask=replicate('X',35)

  add object oIVDESSUP_1_3 as StdField with uid="GLYEBANOHV",rtseq=3,rtrep=.f.,;
    cFormVar = "w_IVDESSUP", cQueryName = "IVDESSUP",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 133178326,;
   bGlobalFont=.t.,;
    Height=21, Width=467, Left=100, Top=35, InputMask=replicate('X',100)

  add object oIVPERIVA_1_4 as StdField with uid="FIEKGRTGZZ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_IVPERIVA", cQueryName = "IVPERIVA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale aliquota IVA; in caso di esenzione = 0",;
    HelpContextID = 232842183,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=65, Top=62, cSayPict='"99.99"', cGetPict='"99.99"'

  add object oIVPERIND_1_5 as StdField with uid="QKUWEFZZCY",rtseq=5,rtrep=.f.,;
    cFormVar = "w_IVPERIND", cQueryName = "IVPERIND",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale di indetraibilit�",;
    HelpContextID = 35593270,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=258, Top=62, cSayPict='"999"', cGetPict='"999"'

  func oIVPERIND_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IVPERIVA<>0 And .w_IVPLAIVA<>'S')
    endwith
   endif
  endfunc


  add object oIVPROIVA_1_6 as StdCombo with uid="ARANVWBOKV",rtseq=6,rtrep=.f.,left=378,top=62,width=154,height=21;
    , ToolTipText = "Test se il codice IVA partecipa al calcolo del prorata";
    , HelpContextID = 230548423;
    , cFormVar="w_IVPROIVA",RowSource=""+"Operazione esente,"+"Partecipa al calcolo,"+"Non partecipa", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oIVPROIVA_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'E',;
    space(1)))))
  endfunc
  func oIVPROIVA_1_6.GetRadio()
    this.Parent.oContained.w_IVPROIVA = this.RadioValue()
    return .t.
  endfunc

  func oIVPROIVA_1_6.SetRadio()
    this.Parent.oContained.w_IVPROIVA=trim(this.Parent.oContained.w_IVPROIVA)
    this.value = ;
      iif(this.Parent.oContained.w_IVPROIVA=='S',1,;
      iif(this.Parent.oContained.w_IVPROIVA=='N',2,;
      iif(this.Parent.oContained.w_IVPROIVA=='E',3,;
      0)))
  endfunc


  add object oIVTIPPLA_1_7 as StdCombo with uid="HNAGWLNWMF",rtseq=7,rtrep=.f.,left=378,top=89,width=154,height=21;
    , ToolTipText = "Tipo acquisto plafond";
    , HelpContextID = 188406841;
    , cFormVar="w_IVTIPPLA",RowSource=""+"Acquisti interni e INTRA,"+"Importazioni", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oIVTIPPLA_1_7.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'I',;
    space(1))))
  endfunc
  func oIVTIPPLA_1_7.GetRadio()
    this.Parent.oContained.w_IVTIPPLA = this.RadioValue()
    return .t.
  endfunc

  func oIVTIPPLA_1_7.SetRadio()
    this.Parent.oContained.w_IVTIPPLA=trim(this.Parent.oContained.w_IVTIPPLA)
    this.value = ;
      iif(this.Parent.oContained.w_IVTIPPLA=='A',1,;
      iif(this.Parent.oContained.w_IVTIPPLA=='I',2,;
      0))
  endfunc

  func oIVTIPPLA_1_7.mHide()
    with this.Parent.oContained
      return (.w_IVPLAIVA <> "S")
    endwith
  endfunc

  add object oIVPLAIVA_1_8 as StdCheck with uid="QAIJRMCYEE",rtseq=8,rtrep=.f.,left=65, top=89, caption="Acquisti plafond",;
    ToolTipText = "Se attivo: il codice IVA partecipa al calcolo plafond",;
    HelpContextID = 215475143,;
    cFormVar="w_IVPLAIVA", bObbl = .f. , nPag = 1;
    ,sErrorMsg = "Percentuale IVA indetraibile incongruente";
   , bGlobalFont=.t.


  func oIVPLAIVA_1_8.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oIVPLAIVA_1_8.GetRadio()
    this.Parent.oContained.w_IVPLAIVA = this.RadioValue()
    return .t.
  endfunc

  func oIVPLAIVA_1_8.SetRadio()
    this.Parent.oContained.w_IVPLAIVA=trim(this.Parent.oContained.w_IVPLAIVA)
    this.value = ;
      iif(this.Parent.oContained.w_IVPLAIVA=='S',1,;
      0)
  endfunc

  func oIVPLAIVA_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_IVPERIND=0 OR EMPTY(.w_IVPLAIVA))
    endwith
    return bRes
  endfunc

  add object oIVMACIVA_1_9 as StdCheck with uid="KUVDGLBSQW",rtseq=9,rtrep=.f.,left=65, top=109, caption="Monte acquisti",;
    ToolTipText = "Se attivo: l'aliquota aggiorna il monte acquisti in ventilazione",;
    HelpContextID = 216839111,;
    cFormVar="w_IVMACIVA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oIVMACIVA_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oIVMACIVA_1_9.GetRadio()
    this.Parent.oContained.w_IVMACIVA = this.RadioValue()
    return .t.
  endfunc

  func oIVMACIVA_1_9.SetRadio()
    this.Parent.oContained.w_IVMACIVA=trim(this.Parent.oContained.w_IVMACIVA)
    this.value = ;
      iif(this.Parent.oContained.w_IVMACIVA=='S',1,;
      0)
  endfunc

  add object oIVIMPEXP_1_10 as StdCheck with uid="ZBVURXQDFJ",rtseq=10,rtrep=.f.,left=65, top=129, caption="Esportazioni",;
    ToolTipText = "Se attivo: il codice IVA partecipa al calcolo plafond variabile",;
    HelpContextID = 104303658,;
    cFormVar="w_IVIMPEXP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oIVIMPEXP_1_10.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oIVIMPEXP_1_10.GetRadio()
    this.Parent.oContained.w_IVIMPEXP = this.RadioValue()
    return .t.
  endfunc

  func oIVIMPEXP_1_10.SetRadio()
    this.Parent.oContained.w_IVIMPEXP=trim(this.Parent.oContained.w_IVIMPEXP)
    this.value = ;
      iif(this.Parent.oContained.w_IVIMPEXP=='S',1,;
      0)
  endfunc

  add object oIVBOLIVA_1_11 as StdCheck with uid="CMUJURMJVX",rtseq=11,rtrep=.f.,left=65, top=149, caption="Bollo su importi esenti",;
    ToolTipText = "Se attivo: il codice IVA � considerato ai fini del calcolo dei bolli su importi",;
    HelpContextID = 227148743,;
    cFormVar="w_IVBOLIVA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oIVBOLIVA_1_11.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oIVBOLIVA_1_11.GetRadio()
    this.Parent.oContained.w_IVBOLIVA = this.RadioValue()
    return .t.
  endfunc

  func oIVBOLIVA_1_11.SetRadio()
    this.Parent.oContained.w_IVBOLIVA=trim(this.Parent.oContained.w_IVBOLIVA)
    this.value = ;
      iif(this.Parent.oContained.w_IVBOLIVA=='S',1,;
      0)
  endfunc

  func oIVBOLIVA_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IVPERIVA=0)
    endwith
   endif
  endfunc

  add object oIVSPLPAY_1_12 as StdCheck with uid="LIJDQHEVGZ",rtseq=12,rtrep=.f.,left=65, top=169, caption="Non computa in liquidazione",;
    ToolTipText = "Se attivo, la relativa imposta non partecipa al saldo in fase di liquidazione",;
    HelpContextID = 76288991,;
    cFormVar="w_IVSPLPAY", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oIVSPLPAY_1_12.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oIVSPLPAY_1_12.GetRadio()
    this.Parent.oContained.w_IVSPLPAY = this.RadioValue()
    return .t.
  endfunc

  func oIVSPLPAY_1_12.SetRadio()
    this.Parent.oContained.w_IVSPLPAY=trim(this.Parent.oContained.w_IVSPLPAY)
    this.value = ;
      iif(this.Parent.oContained.w_IVSPLPAY=='S',1,;
      0)
  endfunc

  add object oIVPERCOM_1_13 as StdField with uid="NELPEMQZIR",rtseq=13,rtrep=.f.,;
    cFormVar = "w_IVPERCOM", cQueryName = "IVPERCOM",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale di compensazione IVA vendite utilizzata ai fini del calcolo della liquidazione IVA a regime agricolo",;
    HelpContextID = 132178899,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=378, Top=116, cSayPict='"99.99"', cGetPict='"99.99"'


  add object oIVTIPAGR_1_14 as StdCombo with uid="ADRLTJDHWM",rtseq=14,rtrep=.f.,left=378,top=144,width=154,height=21;
    , ToolTipText = "Utilizzato in caso di IVA acquisti a regime agricolo (agevolata o normale)";
    , HelpContextID = 171629608;
    , cFormVar="w_IVTIPAGR",RowSource=""+"Normale,"+"Agevolata", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oIVTIPAGR_1_14.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'A',;
    space(1))))
  endfunc
  func oIVTIPAGR_1_14.GetRadio()
    this.Parent.oContained.w_IVTIPAGR = this.RadioValue()
    return .t.
  endfunc

  func oIVTIPAGR_1_14.SetRadio()
    this.Parent.oContained.w_IVTIPAGR=trim(this.Parent.oContained.w_IVTIPAGR)
    this.value = ;
      iif(this.Parent.oContained.w_IVTIPAGR=='N',1,;
      iif(this.Parent.oContained.w_IVTIPAGR=='A',2,;
      0))
  endfunc

  add object oIVESINOD_1_15 as StdCheck with uid="EKAAFLFJOF",rtseq=15,rtrep=.f.,left=378, top=185, caption="Esigibilit� non differibile",;
    ToolTipText = "Attivabile solo per operazioni con aliquota = 0 e con gestione Reverse Charge",;
    HelpContextID = 39728074,;
    cFormVar="w_IVESINOD", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oIVESINOD_1_15.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oIVESINOD_1_15.GetRadio()
    this.Parent.oContained.w_IVESINOD = this.RadioValue()
    return .t.
  endfunc

  func oIVESINOD_1_15.SetRadio()
    this.Parent.oContained.w_IVESINOD=trim(this.Parent.oContained.w_IVESINOD)
    this.value = ;
      iif(this.Parent.oContained.w_IVESINOD=='S',1,;
      0)
  endfunc

  func oIVESINOD_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IVPERIVA = 0 OR .w_IVREVCHA='S')
    endwith
   endif
  endfunc

  add object oIVREVCHA_1_16 as StdCheck with uid="WLOZWEYSMV",rtseq=16,rtrep=.f.,left=378, top=165, caption="Reverse Charge",;
    ToolTipText = "Flag gestione Reverse Charge",;
    HelpContextID = 132054073,;
    cFormVar="w_IVREVCHA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oIVREVCHA_1_16.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oIVREVCHA_1_16.GetRadio()
    this.Parent.oContained.w_IVREVCHA = this.RadioValue()
    return .t.
  endfunc

  func oIVREVCHA_1_16.SetRadio()
    this.Parent.oContained.w_IVREVCHA=trim(this.Parent.oContained.w_IVREVCHA)
    this.value = ;
      iif(this.Parent.oContained.w_IVREVCHA=='S',1,;
      0)
  endfunc


  add object oIVREGMAR_1_17 as StdCombo with uid="OKCAQMWQBI",value=1,rtseq=17,rtrep=.f.,left=129,top=211,width=173,height=21;
    , ToolTipText = "Identifica se il codice IVA viene usato per acquisti,cessioni e rilevazione dei costi di ripristino dei beni usati";
    , HelpContextID = 19989464;
    , cFormVar="w_IVREGMAR",RowSource=""+"Non utilizzato,"+"Acquisto beni usati,"+"Spese accessorie beni usati,"+"Cessioni beni usati", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oIVREGMAR_1_17.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'A',;
    iif(this.value =3,'S',;
    iif(this.value =4,'C',;
    space(1))))))
  endfunc
  func oIVREGMAR_1_17.GetRadio()
    this.Parent.oContained.w_IVREGMAR = this.RadioValue()
    return .t.
  endfunc

  func oIVREGMAR_1_17.SetRadio()
    this.Parent.oContained.w_IVREGMAR=trim(this.Parent.oContained.w_IVREGMAR)
    this.value = ;
      iif(this.Parent.oContained.w_IVREGMAR=='',1,;
      iif(this.Parent.oContained.w_IVREGMAR=='A',2,;
      iif(this.Parent.oContained.w_IVREGMAR=='S',3,;
      iif(this.Parent.oContained.w_IVREGMAR=='C',4,;
      0))))
  endfunc

  add object oIVPERMAR_1_18 as StdField with uid="OPSLMWZAIF",rtseq=18,rtrep=.f.,;
    cFormVar = "w_IVPERMAR", cQueryName = "IVPERMAR",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale aliquota acquisti o cessioni beni usati",;
    HelpContextID = 31515608,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=378, Top=211, cSayPict='"99.99"', cGetPict='"99.99"'

  func oIVPERMAR_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IVREGMAR $ 'A-C')
    endwith
   endif
  endfunc


  add object oIVFLVAFF_1_19 as StdCombo with uid="OQSJIORXOK",rtseq=19,rtrep=.f.,left=129,top=260,width=143,height=21;
    , ToolTipText = "Determina compilazione dichiarazione periodica";
    , HelpContextID = 103236556;
    , cFormVar="w_IVFLVAFF",RowSource=""+"Normale,"+"Escluso,"+"Escluso V.A.", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oIVFLVAFF_1_19.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'E',;
    iif(this.value =3,'V',;
    space(1)))))
  endfunc
  func oIVFLVAFF_1_19.GetRadio()
    this.Parent.oContained.w_IVFLVAFF = this.RadioValue()
    return .t.
  endfunc

  func oIVFLVAFF_1_19.SetRadio()
    this.Parent.oContained.w_IVFLVAFF=trim(this.Parent.oContained.w_IVFLVAFF)
    this.value = ;
      iif(this.Parent.oContained.w_IVFLVAFF=='N',1,;
      iif(this.Parent.oContained.w_IVFLVAFF=='E',2,;
      iif(this.Parent.oContained.w_IVFLVAFF=='V',3,;
      0)))
  endfunc


  add object oIVACQINT_1_20 as StdCombo with uid="LKYLZQBAJT",rtseq=20,rtrep=.f.,left=378,top=260,width=154,height=21;
    , ToolTipText = "Eventuale suddivisione codici IVA per dichiarazione periodica";
    , HelpContextID = 36834342;
    , cFormVar="w_IVACQINT",RowSource=""+"No,"+"Acquisti INTRA,"+"Cessioni INTRA,"+"Importazioni oro,"+"Acq.+ imp. oro", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oIVACQINT_1_20.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'A',;
    iif(this.value =3,'C',;
    iif(this.value =4,'I',;
    iif(this.value =5,'O',;
    space(1)))))))
  endfunc
  func oIVACQINT_1_20.GetRadio()
    this.Parent.oContained.w_IVACQINT = this.RadioValue()
    return .t.
  endfunc

  func oIVACQINT_1_20.SetRadio()
    this.Parent.oContained.w_IVACQINT=trim(this.Parent.oContained.w_IVACQINT)
    this.value = ;
      iif(this.Parent.oContained.w_IVACQINT=='N',1,;
      iif(this.Parent.oContained.w_IVACQINT=='A',2,;
      iif(this.Parent.oContained.w_IVACQINT=='C',3,;
      iif(this.Parent.oContained.w_IVACQINT=='I',4,;
      iif(this.Parent.oContained.w_IVACQINT=='O',5,;
      0)))))
  endfunc


  add object oIVDATANA_1_21 as StdCombo with uid="IAEVPEDUFY",value=1,rtseq=21,rtrep=.f.,left=129,top=313,width=167,height=21;
    , ToolTipText = "Operazioni attive comunicazione annuale IVA";
    , HelpContextID = 168025145;
    , cFormVar="w_IVDATANA",RowSource=""+"Nessuno,"+"Oper. non imponibili,"+"Operazioni esenti,"+"Cessioni intracom. beni,"+"Cessioni intracom. servizi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oIVDATANA_1_21.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,1,;
    iif(this.value =3,2,;
    iif(this.value =4,3,;
    iif(this.value =5,4,;
    0))))))
  endfunc
  func oIVDATANA_1_21.GetRadio()
    this.Parent.oContained.w_IVDATANA = this.RadioValue()
    return .t.
  endfunc

  func oIVDATANA_1_21.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_IVDATANA==0,1,;
      iif(this.Parent.oContained.w_IVDATANA==1,2,;
      iif(this.Parent.oContained.w_IVDATANA==2,3,;
      iif(this.Parent.oContained.w_IVDATANA==3,4,;
      iif(this.Parent.oContained.w_IVDATANA==4,5,;
      0)))))
  endfunc


  add object oIVDATANP_1_22 as StdCombo with uid="BZBGGWHVXX",value=1,rtseq=22,rtrep=.f.,left=378,top=313,width=190,height=21;
    , ToolTipText = "Operazioni passive comunicazione annuale IVA";
    , HelpContextID = 168025130;
    , cFormVar="w_IVDATANP",RowSource=""+"Nessuno,"+"Oper. non imponibili,"+"Operazioni esenti,"+"Acquisti INTRA di beni,"+"Acquisti INTRA di servizi,"+"Imp. di oro e argen.,"+"Imp. INTRA oro e arg.,"+"Imp. rottami/mat. rec.,"+"Imp. INTRA rottami/mat. rec.", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oIVDATANP_1_22.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,1,;
    iif(this.value =3,2,;
    iif(this.value =4,3,;
    iif(this.value =5,6,;
    iif(this.value =6,4,;
    iif(this.value =7,5,;
    iif(this.value =8,7,;
    iif(this.value =9,8,;
    0))))))))))
  endfunc
  func oIVDATANP_1_22.GetRadio()
    this.Parent.oContained.w_IVDATANP = this.RadioValue()
    return .t.
  endfunc

  func oIVDATANP_1_22.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_IVDATANP==0,1,;
      iif(this.Parent.oContained.w_IVDATANP==1,2,;
      iif(this.Parent.oContained.w_IVDATANP==2,3,;
      iif(this.Parent.oContained.w_IVDATANP==3,4,;
      iif(this.Parent.oContained.w_IVDATANP==6,5,;
      iif(this.Parent.oContained.w_IVDATANP==4,6,;
      iif(this.Parent.oContained.w_IVDATANP==5,7,;
      iif(this.Parent.oContained.w_IVDATANP==7,8,;
      iif(this.Parent.oContained.w_IVDATANP==8,9,;
      0)))))))))
  endfunc


  add object oIVTIPBEN_1_24 as StdCombo with uid="UBDRAHSDSE",rtseq=24,rtrep=.f.,left=129,top=341,width=211,height=21;
    , ToolTipText = "Tipologia beni utilizzata per la compilazione della comunicazione annuale dati IVA ";
    , HelpContextID = 113583060;
    , cFormVar="w_IVTIPBEN",RowSource=""+"Beni destinati alla rivendita,"+"Beni ammortizzabili,"+"Beni strumentali non ammortizzabili,"+"Nessun cumulo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oIVTIPBEN_1_24.RadioValue()
    return(iif(this.value =1,'R',;
    iif(this.value =2,'A',;
    iif(this.value =3,'S',;
    iif(this.value =4,'N',;
    space(1))))))
  endfunc
  func oIVTIPBEN_1_24.GetRadio()
    this.Parent.oContained.w_IVTIPBEN = this.RadioValue()
    return .t.
  endfunc

  func oIVTIPBEN_1_24.SetRadio()
    this.Parent.oContained.w_IVTIPBEN=trim(this.Parent.oContained.w_IVTIPBEN)
    this.value = ;
      iif(this.Parent.oContained.w_IVTIPBEN=='R',1,;
      iif(this.Parent.oContained.w_IVTIPBEN=='A',2,;
      iif(this.Parent.oContained.w_IVTIPBEN=='S',3,;
      iif(this.Parent.oContained.w_IVTIPBEN=='N',4,;
      0))))
  endfunc

  add object oIVAUTCOM_1_25 as StdCheck with uid="DZMKJUMHVR",rtseq=25,rtrep=.f.,left=378, top=342, caption="Codice IVA per autofatture",;
    ToolTipText = "Codice IVA per autofatture",;
    HelpContextID = 135263187,;
    cFormVar="w_IVAUTCOM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oIVAUTCOM_1_25.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oIVAUTCOM_1_25.GetRadio()
    this.Parent.oContained.w_IVAUTCOM = this.RadioValue()
    return .t.
  endfunc

  func oIVAUTCOM_1_25.SetRadio()
    this.Parent.oContained.w_IVAUTCOM=trim(this.Parent.oContained.w_IVAUTCOM)
    this.value = ;
      iif(this.Parent.oContained.w_IVAUTCOM=='S',1,;
      0)
  endfunc


  add object oIVFLGSER_1_26 as StdCombo with uid="DDZRDTLDQZ",rtseq=26,rtrep=.f.,left=378,top=366,width=107,height=21;
    , ToolTipText = "Comunicazione fiscalit� privilegiata";
    , HelpContextID = 121062360;
    , cFormVar="w_IVFLGSER",RowSource=""+"Servizi,"+"Beni,"+"Escluso", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oIVFLGSER_1_26.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'B',;
    iif(this.value =3,'E',;
    space(1)))))
  endfunc
  func oIVFLGSER_1_26.GetRadio()
    this.Parent.oContained.w_IVFLGSER = this.RadioValue()
    return .t.
  endfunc

  func oIVFLGSER_1_26.SetRadio()
    this.Parent.oContained.w_IVFLGSER=trim(this.Parent.oContained.w_IVFLGSER)
    this.value = ;
      iif(this.Parent.oContained.w_IVFLGSER=='S',1,;
      iif(this.Parent.oContained.w_IVFLGSER=='B',2,;
      iif(this.Parent.oContained.w_IVFLGSER=='E',3,;
      0)))
  endfunc


  add object oIVFLGBSV_1_35 as StdCombo with uid="QKIFCVEXGP",rtseq=32,rtrep=.f.,left=378,top=408,width=190,height=21;
    , ToolTipText = "Tipologia operazione IVA ciclo vendite";
    , HelpContextID = 104285148;
    , cFormVar="w_IVFLGBSV",RowSource=""+"Non definito,"+"Escluso,"+"Cessione di beni,"+"Prestazione di servizi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oIVFLGBSV_1_35.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'E',;
    iif(this.value =3,'B',;
    iif(this.value =4,'S',;
    space(1))))))
  endfunc
  func oIVFLGBSV_1_35.GetRadio()
    this.Parent.oContained.w_IVFLGBSV = this.RadioValue()
    return .t.
  endfunc

  func oIVFLGBSV_1_35.SetRadio()
    this.Parent.oContained.w_IVFLGBSV=trim(this.Parent.oContained.w_IVFLGBSV)
    this.value = ;
      iif(this.Parent.oContained.w_IVFLGBSV=='N',1,;
      iif(this.Parent.oContained.w_IVFLGBSV=='E',2,;
      iif(this.Parent.oContained.w_IVFLGBSV=='B',3,;
      iif(this.Parent.oContained.w_IVFLGBSV=='S',4,;
      0))))
  endfunc


  add object oIVFLGBSA_1_36 as StdCombo with uid="UKQPUHXKTD",rtseq=33,rtrep=.f.,left=378,top=436,width=190,height=21;
    , ToolTipText = "Tipologia operazione IVA ciclo acquisti";
    , HelpContextID = 104285127;
    , cFormVar="w_IVFLGBSA",RowSource=""+"Non definito,"+"Escluso,"+"Acquisto di beni,"+"Acquisizione di servizi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oIVFLGBSA_1_36.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'E',;
    iif(this.value =3,'B',;
    iif(this.value =4,'S',;
    space(1))))))
  endfunc
  func oIVFLGBSA_1_36.GetRadio()
    this.Parent.oContained.w_IVFLGBSA = this.RadioValue()
    return .t.
  endfunc

  func oIVFLGBSA_1_36.SetRadio()
    this.Parent.oContained.w_IVFLGBSA=trim(this.Parent.oContained.w_IVFLGBSA)
    this.value = ;
      iif(this.Parent.oContained.w_IVFLGBSA=='N',1,;
      iif(this.Parent.oContained.w_IVFLGBSA=='E',2,;
      iif(this.Parent.oContained.w_IVFLGBSA=='B',3,;
      iif(this.Parent.oContained.w_IVFLGBSA=='S',4,;
      0))))
  endfunc

  add object oIVCODINT_1_38 as StdField with uid="NIMDLHXJKC",rtseq=34,rtrep=.f.,;
    cFormVar = "w_IVCODINT", cQueryName = "IVCODINT",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice IVA incongruente o obsoleto",;
    ToolTipText = "Codice IVA utilizzato in alternativa nei documenti di acquisto INTRA",;
    HelpContextID = 49671206,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=129, Top=469, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_IVCODINT"

  func oIVCODINT_1_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IVACQINT='N' Or .w_IVACQINT='I')
    endwith
   endif
  endfunc

  func oIVCODINT_1_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_38('Part',this)
    endwith
    return bRes
  endfunc

  proc oIVCODINT_1_38.ecpDrop(oSource)
    this.Parent.oContained.link_1_38('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIVCODINT_1_38.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oIVCODINT_1_38'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Voci IVA",'GSAR1AIV.VOCIIVA_VZM',this.parent.oContained
  endproc
  proc oIVCODINT_1_38.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_IVCODINT
     i_obj.ecpSave()
  endproc

  add object oIVCODINV_1_39 as StdField with uid="FESYUYGSGK",rtseq=35,rtrep=.f.,;
    cFormVar = "w_IVCODINV", cQueryName = "IVCODINV",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice IVA incongruente o obsoleto",;
    ToolTipText = "Codice IVA utilizzato in alternativa nei documenti di acquisto INTRA",;
    HelpContextID = 49671204,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=129, Top=493, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_IVCODINV"

  func oIVCODINV_1_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_IVACQINT='N' Or .w_IVACQINT='I') AND NOT EMPTY(.w_IVCODINT))
    endwith
   endif
  endfunc

  func oIVCODINV_1_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_39('Part',this)
    endwith
    return bRes
  endfunc

  proc oIVCODINV_1_39.ecpDrop(oSource)
    this.Parent.oContained.link_1_39('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIVCODINV_1_39.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oIVCODINV_1_39'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Voci IVA",'GSAR1AIV.VOCIIVA_VZM',this.parent.oContained
  endproc
  proc oIVCODINV_1_39.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_IVCODINV
     i_obj.ecpSave()
  endproc

  add object oDESINT_1_40 as StdField with uid="ITBYONJNLD",rtseq=36,rtrep=.f.,;
    cFormVar = "w_DESINT", cQueryName = "DESINT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 145031734,;
   bGlobalFont=.t.,;
    Height=21, Width=373, Left=194, Top=469, InputMask=replicate('X',35)

  add object oIVDTOBSO_1_41 as StdField with uid="DBYKUDWPGH",rtseq=37,rtrep=.f.,;
    cFormVar = "w_IVDTOBSO", cQueryName = "IVDTOBSO",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Ultima data di validit�",;
    HelpContextID = 113189845,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=488, Top=544, tabstop=.f.

  add object oIVDTINVA_1_44 as StdField with uid="VKCSYOXJEX",rtseq=38,rtrep=.f.,;
    cFormVar = "w_IVDTINVA", cQueryName = "IVDTINVA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio validit�",;
    HelpContextID = 228645945,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=129, Top=544, tabstop=.f.

  add object oDESINTV_1_62 as StdField with uid="JPPDSRSTFL",rtseq=43,rtrep=.f.,;
    cFormVar = "w_DESINTV", cQueryName = "DESINTV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 123403722,;
   bGlobalFont=.t.,;
    Height=21, Width=373, Left=194, Top=493, InputMask=replicate('X',35)

  add object oIVCODAUV_1_67 as StdField with uid="WHCWRZTUSP",rtseq=47,rtrep=.f.,;
    cFormVar = "w_IVCODAUV", cQueryName = "IVCODAUV",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice IVA incongruente o non di tipo Reverse Charge o obsoleto",;
    ToolTipText = "Codice IVA utilizzato in alternativa nei documenti di autofattura per il registro IVA vendite",;
    HelpContextID = 84546524,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=129, Top=517, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_IVCODAUV"

  func oIVCODAUV_1_67.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_67('Part',this)
    endwith
    return bRes
  endfunc

  proc oIVCODAUV_1_67.ecpDrop(oSource)
    this.Parent.oContained.link_1_67('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIVCODAUV_1_67.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oIVCODAUV_1_67'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Voci IVA",'GSAR2AIV.VOCIIVA_VZM',this.parent.oContained
  endproc
  proc oIVCODAUV_1_67.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_IVCODAUV
     i_obj.ecpSave()
  endproc

  add object oDESINTA_1_68 as StdField with uid="XSPRVMPLZJ",rtseq=48,rtrep=.f.,;
    cFormVar = "w_DESINTA", cQueryName = "DESINTA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 145031734,;
   bGlobalFont=.t.,;
    Height=21, Width=373, Left=194, Top=517, InputMask=replicate('X',35)


  add object oObj_1_83 as cp_runprogram with uid="ROAAWEFSEL",left=1, top=581, width=188,height=19,;
    caption='GSAR_BIV',;
   bGlobalFont=.t.,;
    prg="GSAR_BIV",;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 138612548

  add object oStr_1_28 as StdString with uid="UTMDCYLENS",Visible=.t., Left=10, Top=10,;
    Alignment=1, Width=49, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_29 as StdString with uid="NTKNHUGDBT",Visible=.t., Left=320, Top=62,;
    Alignment=1, Width=54, Height=15,;
    Caption="Prorata:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="OBSLIIOBRU",Visible=.t., Left=137, Top=62,;
    Alignment=1, Width=118, Height=15,;
    Caption="% Indetraibilit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="FEDICIDRTR",Visible=.t., Left=-4, Top=62,;
    Alignment=1, Width=63, Height=15,;
    Caption="Aliquota:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="SXFHMDJVCS",Visible=.t., Left=319, Top=545,;
    Alignment=1, Width=164, Height=15,;
    Caption="Data obsolescenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="BLCQEVICCK",Visible=.t., Left=1, Top=544,;
    Alignment=1, Width=124, Height=15,;
    Caption="Data validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="YYOGJEFZSD",Visible=.t., Left=299, Top=260,;
    Alignment=1, Width=75, Height=15,;
    Caption="VP1/VP2/VP3:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="KANFWMTZFL",Visible=.t., Left=69, Top=260,;
    Alignment=1, Width=56, Height=15,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="NFJSWVFASX",Visible=.t., Left=1, Top=469,;
    Alignment=1, Width=124, Height=15,;
    Caption="Cod.IVA acq.CEE:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="TSHPXFZGOF",Visible=.t., Left=9, Top=236,;
    Alignment=0, Width=197, Height=15,;
    Caption="Dichiarazione IVA periodica"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_54 as StdString with uid="KIGVOOZFOF",Visible=.t., Left=257, Top=116,;
    Alignment=1, Width=117, Height=18,;
    Caption="% Compensazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="YWFEAYRICD",Visible=.t., Left=273, Top=144,;
    Alignment=1, Width=101, Height=18,;
    Caption="Tipo acq.agricolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_57 as StdString with uid="ZMSRSSQNKU",Visible=.t., Left=9, Top=290,;
    Alignment=0, Width=197, Height=18,;
    Caption="Dichiarazione annuale dati IVA"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_58 as StdString with uid="BLIRSLSLSI",Visible=.t., Left=299, Top=313,;
    Alignment=1, Width=75, Height=15,;
    Caption="Op. passive:"  ;
  , bGlobalFont=.t.

  add object oStr_1_59 as StdString with uid="SXLAXSKTHC",Visible=.t., Left=1, Top=313,;
    Alignment=1, Width=124, Height=15,;
    Caption="Op. attive:"  ;
  , bGlobalFont=.t.

  add object oStr_1_60 as StdString with uid="AZMVSSISYV",Visible=.t., Left=329, Top=89,;
    Alignment=1, Width=45, Height=17,;
    Caption="Plafond:"  ;
  , bGlobalFont=.t.

  func oStr_1_60.mHide()
    with this.Parent.oContained
      return (.w_IVPLAIVA <> "S")
    endwith
  endfunc

  add object oStr_1_61 as StdString with uid="RVGRIKUOZE",Visible=.t., Left=1, Top=341,;
    Alignment=1, Width=124, Height=18,;
    Caption="Tipologia beni:"  ;
  , bGlobalFont=.t.

  add object oStr_1_63 as StdString with uid="DAGKEHYPLJ",Visible=.t., Left=1, Top=493,;
    Alignment=1, Width=124, Height=15,;
    Caption="Cod.IVA ven.CEE:"  ;
  , bGlobalFont=.t.

  add object oStr_1_69 as StdString with uid="TGQYGKHFYD",Visible=.t., Left=1, Top=519,;
    Alignment=1, Width=124, Height=15,;
    Caption="Cod.IVA ven.autof.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_73 as StdString with uid="ZHGCAAYQYW",Visible=.t., Left=9, Top=191,;
    Alignment=0, Width=197, Height=18,;
    Caption="Regime del margine"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_75 as StdString with uid="SXCBIAAJSW",Visible=.t., Left=22, Top=214,;
    Alignment=1, Width=103, Height=15,;
    Caption="Beni usati:"  ;
  , bGlobalFont=.t.

  add object oStr_1_76 as StdString with uid="NCFJWVMXIN",Visible=.t., Left=311, Top=214,;
    Alignment=1, Width=63, Height=15,;
    Caption="Aliquota:"  ;
  , bGlobalFont=.t.

  add object oStr_1_77 as StdString with uid="FUOGBRKXTI",Visible=.t., Left=149, Top=367,;
    Alignment=1, Width=225, Height=18,;
    Caption="Comunicazione fiscalit� privilegiata:"  ;
  , bGlobalFont=.t.

  add object oStr_1_78 as StdString with uid="ZVACPDOZTI",Visible=.t., Left=9, Top=387,;
    Alignment=0, Width=382, Height=18,;
    Caption="Comunicazione operazioni superiori a 3.000 euro"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_80 as StdString with uid="QCLWACSMAI",Visible=.t., Left=140, Top=410,;
    Alignment=1, Width=234, Height=18,;
    Caption="Tipologia operazione IVA ciclo vendite:"  ;
  , bGlobalFont=.t.

  add object oStr_1_81 as StdString with uid="NWANVUXUSZ",Visible=.t., Left=140, Top=438,;
    Alignment=1, Width=234, Height=18,;
    Caption="Tipologia operazione IVA ciclo acquisti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_82 as StdString with uid="VEVWFLUXAG",Visible=.t., Left=3, Top=36,;
    Alignment=1, Width=95, Height=18,;
    Caption="Descr.aggiuntiva:"  ;
  , bGlobalFont=.t.

  add object oBox_1_52 as StdBox with uid="THZBGDRSQX",left=4, top=254, width=563,height=2

  add object oBox_1_56 as StdBox with uid="XKDSXUGMZE",left=4, top=282, width=563,height=2

  add object oBox_1_74 as StdBox with uid="JSZFTVTJLS",left=4, top=206, width=563,height=2

  add object oBox_1_79 as StdBox with uid="BCHNBKQDNL",left=4, top=404, width=563,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_aiv','VOCIIVA','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".IVCODIVA=VOCIIVA.IVCODIVA";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
