* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsil_scl                                                        *
*              Stampa categorie listini                                        *
*                                                                              *
*      Author: Pollina Fabrizio                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-04-30                                                      *
* Last revis.: 2014-05-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsil_scl",oParentObject))

* --- Class definition
define class tgsil_scl as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 498
  Height = 158
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-05-29"
  HelpContextID=177614999
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=2

  * --- Constant Properties
  _IDX = 0
  CATMLIST_IDX = 0
  cPrg = "gsil_scl"
  cComment = "Stampa categorie listini"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CTCODINI = space(15)
  w_CTCODFIN = space(15)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsil_sclPag1","gsil_scl",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCTCODINI_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='CATMLIST'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CTCODINI=space(15)
      .w_CTCODFIN=space(15)
        .w_CTCODINI = this.oparentobject.w_CTCODCAT
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CTCODINI))
          .link_1_1('Full')
        endif
        .w_CTCODFIN = this.oparentobject.w_CTCODCAT
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CTCODFIN))
          .link_1_2('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_3.Calculate()
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_3.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,2,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_3.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_3.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CTCODINI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATMLIST_IDX,3]
    i_lTable = "CATMLIST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATMLIST_IDX,2], .t., this.CATMLIST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATMLIST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CTCODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATMLIST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODCAT like "+cp_ToStrODBC(trim(this.w_CTCODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODCAT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODCAT',trim(this.w_CTCODINI))
          select CTCODCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODCAT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CTCODINI)==trim(_Link_.CTCODCAT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CTCODINI) and !this.bDontReportError
            deferred_cp_zoom('CATMLIST','*','CTCODCAT',cp_AbsName(oSource.parent,'oCTCODINI_1_1'),i_cWhere,'',"Categorie listini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODCAT="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODCAT',oSource.xKey(1))
            select CTCODCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CTCODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODCAT="+cp_ToStrODBC(this.w_CTCODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODCAT',this.w_CTCODINI)
            select CTCODCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CTCODINI = NVL(_Link_.CTCODCAT,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_CTCODINI = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CTCODINI<=.w_CTCODFIN OR EMPTY(.w_CTCODFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("La categoria iniziale deve essere minore o uguale alla categoria di fine stampa")
        endif
        this.w_CTCODINI = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATMLIST_IDX,2])+'\'+cp_ToStr(_Link_.CTCODCAT,1)
      cp_ShowWarn(i_cKey,this.CATMLIST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CTCODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CTCODFIN
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATMLIST_IDX,3]
    i_lTable = "CATMLIST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATMLIST_IDX,2], .t., this.CATMLIST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATMLIST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CTCODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATMLIST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODCAT like "+cp_ToStrODBC(trim(this.w_CTCODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODCAT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODCAT',trim(this.w_CTCODFIN))
          select CTCODCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODCAT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CTCODFIN)==trim(_Link_.CTCODCAT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CTCODFIN) and !this.bDontReportError
            deferred_cp_zoom('CATMLIST','*','CTCODCAT',cp_AbsName(oSource.parent,'oCTCODFIN_1_2'),i_cWhere,'',"Categorie listini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODCAT="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODCAT',oSource.xKey(1))
            select CTCODCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CTCODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODCAT="+cp_ToStrODBC(this.w_CTCODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODCAT',this.w_CTCODFIN)
            select CTCODCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CTCODFIN = NVL(_Link_.CTCODCAT,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_CTCODFIN = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CTCODINI<=.w_CTCODFIN OR EMPTY(.w_CTCODFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("La categoria di fine stampa deve essere maggiore o uguale alla categoria iniziale")
        endif
        this.w_CTCODFIN = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATMLIST_IDX,2])+'\'+cp_ToStr(_Link_.CTCODCAT,1)
      cp_ShowWarn(i_cKey,this.CATMLIST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CTCODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCTCODINI_1_1.value==this.w_CTCODINI)
      this.oPgFrm.Page1.oPag.oCTCODINI_1_1.value=this.w_CTCODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCTCODFIN_1_2.value==this.w_CTCODFIN)
      this.oPgFrm.Page1.oPag.oCTCODFIN_1_2.value=this.w_CTCODFIN
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_CTCODINI<=.w_CTCODFIN OR EMPTY(.w_CTCODFIN))  and not(empty(.w_CTCODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCTCODINI_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La categoria iniziale deve essere minore o uguale alla categoria di fine stampa")
          case   not(.w_CTCODINI<=.w_CTCODFIN OR EMPTY(.w_CTCODFIN))  and not(empty(.w_CTCODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCTCODFIN_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La categoria di fine stampa deve essere maggiore o uguale alla categoria iniziale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsil_sclPag1 as StdContainer
  Width  = 494
  height = 158
  stdWidth  = 494
  stdheight = 158
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCTCODINI_1_1 as StdField with uid="ONEOPHZUTI",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CTCODINI", cQueryName = "CTCODINI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "La categoria iniziale deve essere minore o uguale alla categoria di fine stampa",;
    ToolTipText = "Categoria iniziale stampa",;
    HelpContextID = 136952175,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=103, Top=10, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CATMLIST", oKey_1_1="CTCODCAT", oKey_1_2="this.w_CTCODINI"

  func oCTCODINI_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCTCODINI_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCTCODINI_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATMLIST','*','CTCODCAT',cp_AbsName(this.parent,'oCTCODINI_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie listini",'',this.parent.oContained
  endproc

  add object oCTCODFIN_1_2 as StdField with uid="XIHWEYGFYG",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CTCODFIN", cQueryName = "CTCODFIN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "La categoria di fine stampa deve essere maggiore o uguale alla categoria iniziale",;
    ToolTipText = "Categoria di fine stampa",;
    HelpContextID = 86620532,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=103, Top=39, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CATMLIST", oKey_1_1="CTCODCAT", oKey_1_2="this.w_CTCODFIN"

  func oCTCODFIN_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCTCODFIN_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCTCODFIN_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATMLIST','*','CTCODCAT',cp_AbsName(this.parent,'oCTCODFIN_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie listini",'',this.parent.oContained
  endproc


  add object oObj_1_3 as cp_outputCombo with uid="ZLERXIVPWT",left=103, top=69, width=382,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 181258266


  add object oBtn_1_4 as StdButton with uid="DRHVUCZTHS",left=378, top=106, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la stampa";
    , HelpContextID = 268309994;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_4.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_5 as StdButton with uid="TSZFIRIFVB",left=435, top=106, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 268309994;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_6 as StdString with uid="HZRCTPJWXH",Visible=.t., Left=0, Top=71,;
    Alignment=1, Width=97, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="YYLXSXKXQH",Visible=.t., Left=9, Top=14,;
    Alignment=1, Width=88, Height=18,;
    Caption="Da categoria:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="QIHIVKYMRB",Visible=.t., Left=18, Top=41,;
    Alignment=1, Width=79, Height=18,;
    Caption="A categoria:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsil_scl','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
