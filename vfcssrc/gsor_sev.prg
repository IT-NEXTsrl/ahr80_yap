* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsor_sev                                                        *
*              Evadibilitą ordini                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30][VRS_113]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-04-18                                                      *
* Last revis.: 2015-03-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsor_sev",oParentObject))

* --- Class definition
define class tgsor_sev as StdForm
  Top    = 34
  Left   = 74

  * --- Standard Properties
  Width  = 549
  Height = 406
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-03-19"
  HelpContextID=57239913
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=32

  * --- Constant Properties
  _IDX = 0
  TIP_DOCU_IDX = 0
  CAM_AGAZ_IDX = 0
  CAU_CONT_IDX = 0
  CONTI_IDX = 0
  ART_ICOL_IDX = 0
  MAGAZZIN_IDX = 0
  AGENTI_IDX = 0
  PNT_MAST_IDX = 0
  cPrg = "gsor_sev"
  cComment = "Evadibilitą ordini"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CRITERIO = space(1)
  w_CODMAG = space(5)
  w_DESMAGA = space(35)
  w_CODICE1 = space(20)
  w_DESART1 = space(35)
  w_CODICE2 = space(20)
  w_BNUMORD = 0
  w_DAALFA = space(10)
  w_DATDOC1 = ctod('  /  /  ')
  w_ENUMORD = 0
  w_AALFA = space(10)
  w_DATDOC2 = ctod('  /  /  ')
  w_DESART2 = space(35)
  w_DATAEV1 = ctod('  /  /  ')
  w_DATAEV2 = ctod('  /  /  ')
  w_DISPONI = space(1)
  w_TIPCON = space(1)
  w_CODCON = space(15)
  w_CODAGE = space(5)
  w_CAUORD = space(5)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_ONUME = space(50)
  w_DESCRI1 = space(40)
  w_DESAGE = space(35)
  w_TIPART1 = space(2)
  w_TIPART2 = space(2)
  w_TDDESDOC = space(35)
  w_FLVEAC = space(1)
  w_CATDOC = space(2)
  w_MVCLADOC = space(2)
  w_FLORDAPE = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsor_sevPag1","gsor_sev",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCRITERIO_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[8]
    this.cWorkTables[1]='TIP_DOCU'
    this.cWorkTables[2]='CAM_AGAZ'
    this.cWorkTables[3]='CAU_CONT'
    this.cWorkTables[4]='CONTI'
    this.cWorkTables[5]='ART_ICOL'
    this.cWorkTables[6]='MAGAZZIN'
    this.cWorkTables[7]='AGENTI'
    this.cWorkTables[8]='PNT_MAST'
    return(this.OpenAllTables(8))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSOR_BEV with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CRITERIO=space(1)
      .w_CODMAG=space(5)
      .w_DESMAGA=space(35)
      .w_CODICE1=space(20)
      .w_DESART1=space(35)
      .w_CODICE2=space(20)
      .w_BNUMORD=0
      .w_DAALFA=space(10)
      .w_DATDOC1=ctod("  /  /  ")
      .w_ENUMORD=0
      .w_AALFA=space(10)
      .w_DATDOC2=ctod("  /  /  ")
      .w_DESART2=space(35)
      .w_DATAEV1=ctod("  /  /  ")
      .w_DATAEV2=ctod("  /  /  ")
      .w_DISPONI=space(1)
      .w_TIPCON=space(1)
      .w_CODCON=space(15)
      .w_CODAGE=space(5)
      .w_CAUORD=space(5)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_ONUME=space(50)
      .w_DESCRI1=space(40)
      .w_DESAGE=space(35)
      .w_TIPART1=space(2)
      .w_TIPART2=space(2)
      .w_TDDESDOC=space(35)
      .w_FLVEAC=space(1)
      .w_CATDOC=space(2)
      .w_MVCLADOC=space(2)
      .w_FLORDAPE=space(1)
        .w_CRITERIO = 'C'
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODMAG))
          .link_1_2('Full')
        endif
        .DoRTCalc(3,4,.f.)
        if not(empty(.w_CODICE1))
          .link_1_4('Full')
        endif
        .DoRTCalc(5,6,.f.)
        if not(empty(.w_CODICE2))
          .link_1_6('Full')
        endif
        .w_BNUMORD = 1
          .DoRTCalc(8,9,.f.)
        .w_ENUMORD = 999999999999999
          .DoRTCalc(11,14,.f.)
        .w_DATAEV2 = i_datsys
        .w_DISPONI = 'T'
        .w_TIPCON = 'C'
        .w_CODCON = SPACE(15)
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_CODCON))
          .link_1_18('Full')
        endif
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_CODAGE))
          .link_1_19('Full')
        endif
        .DoRTCalc(20,20,.f.)
        if not(empty(.w_CAUORD))
          .link_1_20('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
        .w_OBTEST = i_INIDAT
          .DoRTCalc(22,28,.f.)
        .w_FLVEAC = 'V'
        .w_CATDOC = 'OR'
        .w_MVCLADOC = "  "
        .w_FLORDAPE = " "
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,32,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_21.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODMAG
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_CODMAG))
          select MGCODMAG,MGDESMAG,MGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCODMAG_1_2'),i_cWhere,'GSAR_AMA',"Magazzini",'GSAR_AMA.MAGAZZIN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAG)
            select MGCODMAG,MGDESMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAGA = NVL(_Link_.MGDESMAG,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.MGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAG = space(5)
      endif
      this.w_DESMAGA = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale di magazzino inesistente oppure obsoleta")
        endif
        this.w_CODMAG = space(5)
        this.w_DESMAGA = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODICE1
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODICE1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODICE1)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODICE1))
          select ARCODART,ARDESART,ARTIPART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODICE1)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODICE1) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODICE1_1_4'),i_cWhere,'GSMA_BZA',"Articoli",'GSOR_SMS.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODICE1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODICE1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODICE1)
            select ARCODART,ARDESART,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODICE1 = NVL(_Link_.ARCODART,space(20))
      this.w_DESART1 = NVL(_Link_.ARDESART,space(35))
      this.w_TIPART1 = NVL(_Link_.ARTIPART,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_CODICE1 = space(20)
      endif
      this.w_DESART1 = space(35)
      this.w_TIPART1 = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_CODICE2) OR ((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND (.w_CODICE1<=.w_CODICE2))) AND (.w_TIPART1 $ 'PF-SE-MP-PH-MC-MA-IM-FS-FM' OR EMPTY(.w_CODICE1))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Articolo iniziale inesistente o obsoleto o non a quantitą")
        endif
        this.w_CODICE1 = space(20)
        this.w_DESART1 = space(35)
        this.w_TIPART1 = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODICE1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODICE2
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODICE2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODICE2)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODICE2))
          select ARCODART,ARDESART,ARTIPART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODICE2)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODICE2) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODICE2_1_6'),i_cWhere,'GSMA_BZA',"Articoli",'GSOR_SMS.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODICE2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODICE2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODICE2)
            select ARCODART,ARDESART,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODICE2 = NVL(_Link_.ARCODART,space(20))
      this.w_DESART2 = NVL(_Link_.ARDESART,space(35))
      this.w_TIPART2 = NVL(_Link_.ARTIPART,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_CODICE2 = space(20)
      endif
      this.w_DESART2 = space(35)
      this.w_TIPART2 = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_CODICE1) OR ((.w_CODICE1<=.w_CODICE2) AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))) AND (.w_TIPART2 $ 'PF-SE-MP-PH-MC-MA-IM-FS-FM' OR EMPTY(.w_CODICE2))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Articolo finale inesistente o obsoleto o non a quantitą")
        endif
        this.w_CODICE2 = space(20)
        this.w_DESART2 = space(35)
        this.w_TIPART2 = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODICE2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCON
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCON_1_18'),i_cWhere,'',"Clienti",'GSAR0ACL.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice intestatario inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRI1 = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON = space(15)
      endif
      this.w_DESCRI1 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice intestatario inesistente oppure obsoleto")
        endif
        this.w_CODCON = space(15)
        this.w_DESCRI1 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAGE
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_CODAGE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_CODAGE))
          select AGCODAGE,AGDESAGE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODAGE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODAGE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oCODAGE_1_19'),i_cWhere,'GSAR_AGE',"Agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_CODAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_CODAGE)
            select AGCODAGE,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAGE = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGE = NVL(_Link_.AGDESAGE,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODAGE = space(5)
      endif
      this.w_DESAGE = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUORD
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUORD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOR_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_CAUORD)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_CAUORD))
          select TDTIPDOC,TDDESDOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAUORD)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CAUORD) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oCAUORD_1_20'),i_cWhere,'GSOR_ATD',"Causali documenti",'GSOR_MDV.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUORD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_CAUORD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_CAUORD)
            select TDTIPDOC,TDDESDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUORD = NVL(_Link_.TDTIPDOC,space(5))
      this.w_TDDESDOC = NVL(_Link_.TDDESDOC,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CAUORD = space(5)
      endif
      this.w_TDDESDOC = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUORD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCRITERIO_1_1.RadioValue()==this.w_CRITERIO)
      this.oPgFrm.Page1.oPag.oCRITERIO_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODMAG_1_2.value==this.w_CODMAG)
      this.oPgFrm.Page1.oPag.oCODMAG_1_2.value=this.w_CODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAGA_1_3.value==this.w_DESMAGA)
      this.oPgFrm.Page1.oPag.oDESMAGA_1_3.value=this.w_DESMAGA
    endif
    if not(this.oPgFrm.Page1.oPag.oCODICE1_1_4.value==this.w_CODICE1)
      this.oPgFrm.Page1.oPag.oCODICE1_1_4.value=this.w_CODICE1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART1_1_5.value==this.w_DESART1)
      this.oPgFrm.Page1.oPag.oDESART1_1_5.value=this.w_DESART1
    endif
    if not(this.oPgFrm.Page1.oPag.oCODICE2_1_6.value==this.w_CODICE2)
      this.oPgFrm.Page1.oPag.oCODICE2_1_6.value=this.w_CODICE2
    endif
    if not(this.oPgFrm.Page1.oPag.oBNUMORD_1_7.value==this.w_BNUMORD)
      this.oPgFrm.Page1.oPag.oBNUMORD_1_7.value=this.w_BNUMORD
    endif
    if not(this.oPgFrm.Page1.oPag.oDAALFA_1_8.value==this.w_DAALFA)
      this.oPgFrm.Page1.oPag.oDAALFA_1_8.value=this.w_DAALFA
    endif
    if not(this.oPgFrm.Page1.oPag.oDATDOC1_1_9.value==this.w_DATDOC1)
      this.oPgFrm.Page1.oPag.oDATDOC1_1_9.value=this.w_DATDOC1
    endif
    if not(this.oPgFrm.Page1.oPag.oENUMORD_1_10.value==this.w_ENUMORD)
      this.oPgFrm.Page1.oPag.oENUMORD_1_10.value=this.w_ENUMORD
    endif
    if not(this.oPgFrm.Page1.oPag.oAALFA_1_11.value==this.w_AALFA)
      this.oPgFrm.Page1.oPag.oAALFA_1_11.value=this.w_AALFA
    endif
    if not(this.oPgFrm.Page1.oPag.oDATDOC2_1_12.value==this.w_DATDOC2)
      this.oPgFrm.Page1.oPag.oDATDOC2_1_12.value=this.w_DATDOC2
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART2_1_13.value==this.w_DESART2)
      this.oPgFrm.Page1.oPag.oDESART2_1_13.value=this.w_DESART2
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAEV1_1_14.value==this.w_DATAEV1)
      this.oPgFrm.Page1.oPag.oDATAEV1_1_14.value=this.w_DATAEV1
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAEV2_1_15.value==this.w_DATAEV2)
      this.oPgFrm.Page1.oPag.oDATAEV2_1_15.value=this.w_DATAEV2
    endif
    if not(this.oPgFrm.Page1.oPag.oDISPONI_1_16.RadioValue()==this.w_DISPONI)
      this.oPgFrm.Page1.oPag.oDISPONI_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCON_1_18.value==this.w_CODCON)
      this.oPgFrm.Page1.oPag.oCODCON_1_18.value=this.w_CODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oCODAGE_1_19.value==this.w_CODAGE)
      this.oPgFrm.Page1.oPag.oCODAGE_1_19.value=this.w_CODAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUORD_1_20.value==this.w_CAUORD)
      this.oPgFrm.Page1.oPag.oCAUORD_1_20.value=this.w_CAUORD
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI1_1_32.value==this.w_DESCRI1)
      this.oPgFrm.Page1.oPag.oDESCRI1_1_32.value=this.w_DESCRI1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESAGE_1_34.value==this.w_DESAGE)
      this.oPgFrm.Page1.oPag.oDESAGE_1_34.value=this.w_DESAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oTDDESDOC_1_42.value==this.w_TDDESDOC)
      this.oPgFrm.Page1.oPag.oTDDESDOC_1_42.value=this.w_TDDESDOC
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_CODMAG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODMAG_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale di magazzino inesistente oppure obsoleta")
          case   not((EMPTY(.w_CODICE2) OR ((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND (.w_CODICE1<=.w_CODICE2))) AND (.w_TIPART1 $ 'PF-SE-MP-PH-MC-MA-IM-FS-FM' OR EMPTY(.w_CODICE1)))  and not(empty(.w_CODICE1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODICE1_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Articolo iniziale inesistente o obsoleto o non a quantitą")
          case   not((EMPTY(.w_CODICE1) OR ((.w_CODICE1<=.w_CODICE2) AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))) AND (.w_TIPART2 $ 'PF-SE-MP-PH-MC-MA-IM-FS-FM' OR EMPTY(.w_CODICE2)))  and not(empty(.w_CODICE2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODICE2_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Articolo finale inesistente o obsoleto o non a quantitą")
          case   not((empty(.w_ENUMORD)) OR (.w_ENUMORD>=.w_BNUMORD))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oBNUMORD_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero documento di inizio selezione deve essere minore di quello di fine selezione")
          case   not(EMPTY(.w_DATDOC2) OR .w_DATDOC1<=.w_DATDOC2)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATDOC1_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data documento di inizio selezione deve essere minore di quella di fine selezione")
          case   not((.w_ENUMORD>=.w_BNUMORD) OR  ( empty(.w_BNUMORD)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oENUMORD_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero documento di inizio selezione deve essere minore di quello di fine selezione")
          case   not(EMPTY(.w_DATDOC1) OR .w_DATDOC1<=.w_DATDOC2)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATDOC2_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data documento di inizio selezione deve essere minore di quella di fine selezione")
          case   not(EMPTY(.w_DATAEV2) OR .w_DATAEV1<=.w_DATAEV2)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATAEV1_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data prevista evasione di inizio selezione deve essere minore di quella di fine selezione")
          case   ((empty(.w_DATAEV2)) or not(EMPTY(.w_DATAEV1) OR .w_DATAEV1<=.w_DATAEV2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATAEV2_1_15.SetFocus()
            i_bnoObbl = !empty(.w_DATAEV2)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data prevista evasione di inizio selezione deve essere minore di quella di fine selezione")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_CODCON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCON_1_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice intestatario inesistente oppure obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsor_sevPag1 as StdContainer
  Width  = 545
  height = 406
  stdWidth  = 545
  stdheight = 406
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oCRITERIO_1_1 as StdCombo with uid="KMUDUPISCP",rtseq=1,rtrep=.f.,left=121,top=11,width=153,height=21;
    , ToolTipText = "Contemporaneo: evadibilitą righe ordini progressiva; alternativo: evadibilitą righe ordini indipendente";
    , HelpContextID = 213942923;
    , cFormVar="w_CRITERIO",RowSource=""+"Contemporaneo,"+"Alternativo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCRITERIO_1_1.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'A',;
    space(1))))
  endfunc
  func oCRITERIO_1_1.GetRadio()
    this.Parent.oContained.w_CRITERIO = this.RadioValue()
    return .t.
  endfunc

  func oCRITERIO_1_1.SetRadio()
    this.Parent.oContained.w_CRITERIO=trim(this.Parent.oContained.w_CRITERIO)
    this.value = ;
      iif(this.Parent.oContained.w_CRITERIO=='C',1,;
      iif(this.Parent.oContained.w_CRITERIO=='A',2,;
      0))
  endfunc

  add object oCODMAG_1_2 as StdField with uid="HXEFGPKRGF",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODMAG", cQueryName = "CODMAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale di magazzino inesistente oppure obsoleta",;
    ToolTipText = "Codice del magazzino (vuoto = no selezione)",;
    HelpContextID = 133704230,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=121, Top=41, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMAG"

  func oCODMAG_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMAG_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAG_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCODMAG_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'GSAR_AMA.MAGAZZIN_VZM',this.parent.oContained
  endproc
  proc oCODMAG_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_CODMAG
     i_obj.ecpSave()
  endproc

  add object oDESMAGA_1_3 as StdField with uid="DVPTNPQKCC",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DESMAGA", cQueryName = "DESMAGA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 133763126,;
   bGlobalFont=.t.,;
    Height=21, Width=246, Left=185, Top=41, InputMask=replicate('X',35)

  add object oCODICE1_1_4 as StdField with uid="ZDCPTHKNHH",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CODICE1", cQueryName = "CODICE1",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Articolo iniziale inesistente o obsoleto o non a quantitą",;
    ToolTipText = "Codice articolo di inizio selezione",;
    HelpContextID = 101984806,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=121, Top=70, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_CODICE1"

  func oCODICE1_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODICE1_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODICE1_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODICE1_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Articoli",'GSOR_SMS.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oCODICE1_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CODICE1
     i_obj.ecpSave()
  endproc

  add object oDESART1_1_5 as StdField with uid="VAWWVEYWWU",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESART1", cQueryName = "DESART1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 100470838,;
   bGlobalFont=.t.,;
    Height=21, Width=259, Left=275, Top=70, InputMask=replicate('X',35)

  add object oCODICE2_1_6 as StdField with uid="BRINZWBVDE",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CODICE2", cQueryName = "CODICE2",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Articolo finale inesistente o obsoleto o non a quantitą",;
    ToolTipText = "Codice articolo di fine selezione",;
    HelpContextID = 101984806,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=121, Top=99, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_CODICE2"

  func oCODICE2_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODICE2_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODICE2_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODICE2_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Articoli",'GSOR_SMS.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oCODICE2_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CODICE2
     i_obj.ecpSave()
  endproc

  add object oBNUMORD_1_7 as StdField with uid="ZBNZTFUKTZ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_BNUMORD", cQueryName = "",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero documento di inizio selezione deve essere minore di quello di fine selezione",;
    ToolTipText = "Numero documento di inizio selezione",;
    HelpContextID = 64567574,;
   bGlobalFont=.t.,;
    Height=21, Width=134, Left=121, Top=126, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oBNUMORD_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_ENUMORD)) OR (.w_ENUMORD>=.w_BNUMORD))
    endwith
    return bRes
  endfunc

  add object oDAALFA_1_8 as StdField with uid="IQDHLHPBVF",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DAALFA", cQueryName = "DAALFA",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento di inizio selezione",;
    HelpContextID = 38202422,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=264, Top=126, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  add object oDATDOC1_1_9 as StdField with uid="KTUFDNNKXZ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DATDOC1", cQueryName = "",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data documento di inizio selezione deve essere minore di quella di fine selezione",;
    ToolTipText = "Data documento di inizio selezione",;
    HelpContextID = 80747574,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=403, Top=126

  func oDATDOC1_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_DATDOC2) OR .w_DATDOC1<=.w_DATDOC2)
    endwith
    return bRes
  endfunc

  add object oENUMORD_1_10 as StdField with uid="GDBIXIPPHU",rtseq=10,rtrep=.f.,;
    cFormVar = "w_ENUMORD", cQueryName = "",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero documento di inizio selezione deve essere minore di quello di fine selezione",;
    ToolTipText = "Numero documento di fine selezione",;
    HelpContextID = 64567622,;
   bGlobalFont=.t.,;
    Height=21, Width=134, Left=121, Top=154, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oENUMORD_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_ENUMORD>=.w_BNUMORD) OR  ( empty(.w_BNUMORD)))
    endwith
    return bRes
  endfunc

  add object oAALFA_1_11 as StdField with uid="HFHATSWFWM",rtseq=11,rtrep=.f.,;
    cFormVar = "w_AALFA", cQueryName = "AALFA",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento di fine selezione",;
    HelpContextID = 252601338,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=264, Top=154, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  add object oDATDOC2_1_12 as StdField with uid="VNSTMBLDUE",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DATDOC2", cQueryName = "",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data documento di inizio selezione deve essere minore di quella di fine selezione",;
    ToolTipText = "Data documento di fine selezione",;
    HelpContextID = 80747574,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=403, Top=154

  func oDATDOC2_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_DATDOC1) OR .w_DATDOC1<=.w_DATDOC2)
    endwith
    return bRes
  endfunc

  add object oDESART2_1_13 as StdField with uid="YYIDHMZLQD",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESART2", cQueryName = "DESART2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 100470838,;
   bGlobalFont=.t.,;
    Height=21, Width=259, Left=275, Top=99, InputMask=replicate('X',35)

  add object oDATAEV1_1_14 as StdField with uid="FDBRTVIQMX",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DATAEV1", cQueryName = "DATAEV1",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data prevista evasione di inizio selezione deve essere minore di quella di fine selezione",;
    ToolTipText = "Data prevista evasione di inizio selezione",;
    HelpContextID = 120396854,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=121, Top=183

  func oDATAEV1_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_DATAEV2) OR .w_DATAEV1<=.w_DATAEV2)
    endwith
    return bRes
  endfunc

  add object oDATAEV2_1_15 as StdField with uid="LTPSODFNJM",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DATAEV2", cQueryName = "DATAEV2",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data prevista evasione di inizio selezione deve essere minore di quella di fine selezione",;
    ToolTipText = "Data prevista evasione di fine selezione",;
    HelpContextID = 120396854,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=403, Top=183

  func oDATAEV2_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_DATAEV1) OR .w_DATAEV1<=.w_DATAEV2)
    endwith
    return bRes
  endfunc


  add object oDISPONI_1_16 as StdCombo with uid="KOIJJJPDWP",rtseq=16,rtrep=.f.,left=121,top=212,width=153,height=21;
    , ToolTipText = "Disponibilitą";
    , HelpContextID = 2354122;
    , cFormVar="w_DISPONI",RowSource=""+"Righe evadibili,"+"Righe non evadibili,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDISPONI_1_16.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oDISPONI_1_16.GetRadio()
    this.Parent.oContained.w_DISPONI = this.RadioValue()
    return .t.
  endfunc

  func oDISPONI_1_16.SetRadio()
    this.Parent.oContained.w_DISPONI=trim(this.Parent.oContained.w_DISPONI)
    this.value = ;
      iif(this.Parent.oContained.w_DISPONI=='E',1,;
      iif(this.Parent.oContained.w_DISPONI=='N',2,;
      iif(this.Parent.oContained.w_DISPONI=='T',3,;
      0)))
  endfunc

  add object oCODCON_1_18 as StdField with uid="EDDONEAZYD",rtseq=18,rtrep=.f.,;
    cFormVar = "w_CODCON", cQueryName = "CODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice intestatario inesistente oppure obsoleto",;
    ToolTipText = "Eventuale selezione su intestatario (vuoto = no selezione)",;
    HelpContextID = 265169446,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=121, Top=240, cSayPict="p_CLF", cGetPict="p_CLF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCON"

  func oCODCON_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCON_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCON_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCON_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Clienti",'GSAR0ACL.CONTI_VZM',this.parent.oContained
  endproc

  add object oCODAGE_1_19 as StdField with uid="QRSSCPNUDR",rtseq=19,rtrep=.f.,;
    cFormVar = "w_CODAGE", cQueryName = "CODAGE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale selezione su agente (vuoto = no selezione)",;
    HelpContextID = 105654822,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=121, Top=269, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_CODAGE"

  func oCODAGE_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODAGE_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODAGE_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oCODAGE_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"Agenti",'',this.parent.oContained
  endproc
  proc oCODAGE_1_19.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_CODAGE
     i_obj.ecpSave()
  endproc

  add object oCAUORD_1_20 as StdField with uid="RIEJFVURKY",rtseq=20,rtrep=.f.,;
    cFormVar = "w_CAUORD", cQueryName = "CAUORD",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale del documento",;
    HelpContextID = 101395494,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=121, Top=298, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSOR_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_CAUORD"

  func oCAUORD_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAUORD_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCAUORD_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oCAUORD_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOR_ATD',"Causali documenti",'GSOR_MDV.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oCAUORD_1_20.mZoomOnZoom
    local i_obj
    i_obj=GSOR_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_CAUORD
     i_obj.ecpSave()
  endproc


  add object oObj_1_21 as cp_outputCombo with uid="ZLERXIVPWT",left=121, top=327, width=414,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 120757734


  add object oBtn_1_22 as StdButton with uid="ADQGKKRZFG",left=442, top=354, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , HelpContextID = 84549670;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      with this.Parent.oContained
        do GSOR_BEV with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_23 as StdButton with uid="QDGAMAUXIL",left=492, top=354, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 49922490;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCRI1_1_32 as StdField with uid="OFSZWBAZUK",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESCRI1", cQueryName = "DESCRI1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 184487990,;
   bGlobalFont=.t.,;
    Height=21, Width=278, Left=256, Top=240, InputMask=replicate('X',40)

  add object oDESAGE_1_34 as StdField with uid="XWASPHVOQY",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESAGE", cQueryName = "DESAGE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 105713718,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=185, Top=269, InputMask=replicate('X',35)

  add object oTDDESDOC_1_42 as StdField with uid="PVBLWRNCBR",rtseq=28,rtrep=.f.,;
    cFormVar = "w_TDDESDOC", cQueryName = "TDDESDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 101720185,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=185, Top=298, InputMask=replicate('X',35)

  add object oStr_1_24 as StdString with uid="TCYVYQHQCU",Visible=.t., Left=20, Top=327,;
    Alignment=1, Width=99, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="KGDDXJAWXG",Visible=.t., Left=20, Top=70,;
    Alignment=1, Width=99, Height=15,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="ECVCUPVVYD",Visible=.t., Left=20, Top=99,;
    Alignment=1, Width=99, Height=15,;
    Caption="A codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="YMIFVOZCUF",Visible=.t., Left=20, Top=41,;
    Alignment=1, Width=99, Height=15,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="OFQPPOEWRC",Visible=.t., Left=34, Top=214,;
    Alignment=1, Width=85, Height=18,;
    Caption="Disponibilitą:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="YPJFGXUJDZ",Visible=.t., Left=59, Top=241,;
    Alignment=1, Width=60, Height=18,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="HUQWDPSGKF",Visible=.t., Left=57, Top=269,;
    Alignment=1, Width=62, Height=18,;
    Caption="Agente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="GKCYYDLUTD",Visible=.t., Left=5, Top=186,;
    Alignment=1, Width=114, Height=18,;
    Caption="Da data prev. ev.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="OVEVCZOYDL",Visible=.t., Left=303, Top=186,;
    Alignment=1, Width=96, Height=18,;
    Caption="A data prev. ev.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="KAGLKOLDWQ",Visible=.t., Left=22, Top=11,;
    Alignment=1, Width=97, Height=18,;
    Caption="Criterio evasione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="MGWWAADHII",Visible=.t., Left=19, Top=300,;
    Alignment=1, Width=100, Height=18,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="DKPVJOYCGC",Visible=.t., Left=39, Top=126,;
    Alignment=1, Width=80, Height=18,;
    Caption="Da doc. n.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="XCVVRPDOFL",Visible=.t., Left=50, Top=158,;
    Alignment=1, Width=69, Height=18,;
    Caption="A doc. n.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="DLGWUZZOTK",Visible=.t., Left=355, Top=126,;
    Alignment=1, Width=44, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="JGWMBFEVVL",Visible=.t., Left=355, Top=158,;
    Alignment=1, Width=44, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="YAAATJHSEK",Visible=.t., Left=253, Top=129,;
    Alignment=2, Width=14, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="BJWEUSWIMR",Visible=.t., Left=253, Top=158,;
    Alignment=2, Width=14, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsor_sev','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
