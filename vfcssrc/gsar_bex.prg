* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bex                                                        *
*              EXP/IMP TABELLE IMPORT DBF                                      *
*                                                                              *
*      Author: TAM SOFTWARE                                                    *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_414]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-10-12                                                      *
* Last revis.: 2014-04-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bex",oParentObject,m.pOper)
return(i_retval)

define class tgsar_bex as StdBatch
  * --- Local variables
  pOper = space(5)
  Messaggio = space(254)
  w_SCELTA = 0
  w_OK = .f.
  w_EXP_ARTICOL = space(1)
  w_nt = 0
  w_nConn = 0
  w_cTable = space(50)
  w_cTableName = space(50)
  w_cWhere = space(50)
  w_READ_RES = space(10)
  w_FLDFILTER = space(10)
  w_NUMFILTER = 0
  w_IMPERROR = .f.
  * --- WorkFile variables
  ART_ICOL_idx=0
  KEY_ARTI_idx=0
  NOT_ARTI_idx=0
  TAR_IFFE_idx=0
  TAR_DETT_idx=0
  CACOARTI_idx=0
  UNIMIS_idx=0
  CLA_RIGD_idx=0
  vociiva_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Export/Import Dati Tracciati, Destinazioni Valori Predefiniti e Trascodifiche DBF
    * --- Variabili maschera
    * --- Tipo operazione da effettuare (I=Import; E=Export)
    * --- Nomi e path dei files DBF
    * --- Variabili per le connessioni al database
    * --- Numero area di lavoro tabella
    * --- Numero connessione
    * --- Nome fisico tabella
    * --- Nome Logico Tabella
    * --- Filtro nella select in Export
    this.w_nt = 0
    this.w_nConn = 0
    this.w_cTable = ""
    this.w_cTableName = ""
    this.w_FLDFILTER = ""
    this.w_NUMFILTER = 0
    this.w_IMPERROR = .F.
    do case
      case this.pOPER=="SELEZ" Or this.pOPER=="DESEL" Or this.pOPER=="INVSE"
        this.oParentObject.w_SELUNI = ICASE(this.pOPER=="SELEZ","UN", this.pOPER=="DESEL", " ", IIF(this.oParentObject.w_SELUNI="UN", " ", "UN"))
        this.oParentObject.w_SELCAR = ICASE(this.pOPER=="SELEZ","CA", this.pOPER=="DESEL", " ", IIF(this.oParentObject.w_SELCAR="CA", " ", "CA"))
        this.oParentObject.w_SELART = ICASE(this.pOPER=="SELEZ","AR", this.pOPER=="DESEL", " ", IIF(this.oParentObject.w_SELART="AR", " ", "AR"))
        this.oParentObject.w_SELKEY = ICASE(this.pOPER=="SELEZ","KA", this.pOPER=="DESEL", " ", IIF(this.oParentObject.w_SELKEY="KA", " ", "KA"))
        this.oParentObject.w_SELTAR = ICASE(this.pOPER=="SELEZ","TA", this.pOPER=="DESEL", " ", IIF(this.oParentObject.w_SELTAR="TA", " ", "TA"))
        this.oParentObject.w_SELNOT = ICASE(this.pOPER=="SELEZ","NA", this.pOPER=="DESEL", " ", IIF(this.oParentObject.w_SELNOT="NA", " ", "NA"))
        this.oParentObject.w_SELIVA = ICASE(this.pOPER=="SELEZ","VI", this.pOPER=="DESEL", " ", IIF(this.oParentObject.w_SELIVA="VI", " ", "VI"))
        this.oParentObject.w_SELTIP = ICASE(this.pOPER=="SELEZ","TR", this.pOPER=="DESEL", " ", IIF(this.oParentObject.w_SELTIP="TR", " ", "TR"))
      case this.pOPER=="ELABO"
        this.oParentObject.w_MSG = ""
        this.oParentObject.oPGFRM.ActivePage=2
        if this.oParentObject.w_RADSELIE1 = "E"
          addMsgNL("Inizio esportazione tabelle %1" ,This,TTOC(DATETIME()))
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          * --- begin transaction
          cp_BeginTrs()
          addMsgNL("Inizio importazione tabelle %1" ,This,TTOC(DATETIME()))
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.w_IMPERROR or bTrsErr
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
          else
            * --- commit
            cp_EndTrs(.t.)
          endif
        endif
        if this.oParentObject.w_RADSELIE1="I"
          addMsg("Fine importazione tabelle %1" ,This,TTOC(DATETIME()))
          if this.w_IMPERROR or bTrsErr
            ah_ErrorMsg("Procedura di importazione terminata con errori",48,"")
          else
            ah_ErrorMsg("Procedura di importazione terminata con successo",64,"")
          endif
        else
          addMsg("Fine esportazione tabelle %1" ,This,TTOC(DATETIME()))
          ah_ErrorMsg("Procedura di esportazione terminata con successo",64,"")
        endif
    endcase
    * --- Chiusura cursore generico
    if used("Cursdbf")
      use in "Cursdbf"
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Export Tabelle
    * --- Scrittura files dbf
    this.w_SCELTA = 1
    this.w_FLDFILTER = ""
    this.w_cWhere = ""
    if this.oParentObject.w_SELUNI = "UN"
      Filedbf = alltrim(this.oParentObject.w_DBF1)
      addmsgNL("Export unit� di misura...", This)
      this.w_cTableName = "UNIMIS"
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.oParentObject.w_SELIVA = "VI"
      Filedbf = alltrim(this.oParentObject.w_DBF4)
      addmsgNL("Export vociiva...", This)
      this.w_cTableName = "VOCIIVA"
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.oParentObject.w_SELTIP = "TR"
      Filedbf = alltrim(this.oParentObject.w_DBF6)
      addmsgNL("Export tipologie righe documenti...", This)
      this.w_cTableName = "CLA_RIGD"
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.oParentObject.w_SELCAR = "CA"
      Filedbf = alltrim(this.oParentObject.w_DBF5)
      addmsgNL("Categorie contabili ...", This)
      this.w_cTableName = "CACOARTI"
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.oParentObject.w_SELART = "AR"
      Filedbf = alltrim(this.oParentObject.w_DBF2)
      addmsgNL("Export voci del tariffario...", This)
      this.w_cTableName = "ART_ICOL"
      this.w_EXP_ARTICOL = "S"
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_EXP_ARTICOL = " "
    endif
    if this.oParentObject.w_SELKEY = "KA"
      Filedbf = alltrim(this.oParentObject.w_DBF3)
      addmsgNL("Export codici di ricerca...", This)
      this.w_cTableName = "KEY_ARTI"
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.oParentObject.w_SELART = "AR" AND (this.oParentObject.w_TIPART<>"TT" OR NOT EMPTY(this.oParentObject.w_CODPRE) OR this.oParentObject.w_CLASSTARIF<>"K" OR NOT EMPTY(this.oParentObject.w_SOLOPAR))
        * --- Cancellazione da KEY_ARTI dove CACODART NOT IN ART_ICOL.ARCODART
        SELECT 0
        USE alltrim(this.oParentObject.w_DBF2) ALIAS ART_ICOL
        SELECT 0
        USE alltrim(this.oParentObject.w_DBF3) ALIAS KEY_ARTI
        SELECT KEY_ARTI.* FROM KEY_ARTI INNER JOIN ART_ICOL ON KEY_ARTI.CACODART=ART_ICOL.ARCODART INTO TABLE LEFT(alltrim(this.oParentObject.w_DBF3),(LEN(alltrim(this.oParentObject.w_DBF3))-12))+"APPOKEY.DBF"
        SELECT KEY_ARTI
        USE
        ERASE alltrim(this.oParentObject.w_DBF3)
        ERASE LEFT(alltrim(this.oParentObject.w_DBF3),(LEN(alltrim(this.oParentObject.w_DBF3))-3))+"FPT"
        SELECT * FROM APPOKEY INTO TABLE alltrim(this.oParentObject.w_DBF3)
        SELECT KEY_ARTI
        USE
        SELECT ART_ICOL
        USE
        SELECT APPOKEY
        USE
        ERASE LEFT(alltrim(this.oParentObject.w_DBF3),(LEN(alltrim(this.oParentObject.w_DBF3))-12))+"APPOKEY.DBF"
        ERASE LEFT(alltrim(this.oParentObject.w_DBF3),(LEN(alltrim(this.oParentObject.w_DBF3))-12))+"APPOKEY.FPT"
      endif
    endif
    if this.oParentObject.w_SELTAR = "TA"
      Filedbf = alltrim(this.oParentObject.w_DBF7)
      addmsgNL("Export tariffe...", This)
      if NOT EMPTY(this.oParentObject.w_CODLIS)
        this.w_cWhere = "TACODLIS = '" + Alltrim(this.oParentObject.w_CODLIS) + "'"
      endif
      this.w_cTableName = "TAR_IFFE"
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.oParentObject.w_SELART = "AR" AND (this.oParentObject.w_TIPART<>"TT" OR NOT EMPTY(this.oParentObject.w_CODPRE) OR this.oParentObject.w_CLASSTARIF<>"K" OR NOT EMPTY(this.oParentObject.w_SOLOPAR))
        * --- Cancellazione da TAR_IFFE dove TACODART NOT IN ART_ICOL.ARCODART
        SELECT 0
        USE alltrim(this.oParentObject.w_DBF2) ALIAS ART_ICOL
        SELECT 0
        USE alltrim(this.oParentObject.w_DBF7) ALIAS TAR_IFFE
        SELECT TAR_IFFE.* FROM TAR_IFFE INNER JOIN ART_ICOL ON TAR_IFFE.TACODART=ART_ICOL.ARCODART INTO TABLE LEFT(alltrim(this.oParentObject.w_DBF7),(LEN(alltrim(this.oParentObject.w_DBF7))-12))+"APPOTAR.DBF"
        SELECT TAR_IFFE
        USE
        ERASE alltrim(this.oParentObject.w_DBF7)
        ERASE LEFT(alltrim(this.oParentObject.w_DBF7),(LEN(alltrim(this.oParentObject.w_DBF7))-3))+"FPT"
        SELECT * FROM APPOTAR INTO TABLE alltrim(this.oParentObject.w_DBF7)
        SELECT TAR_IFFE
        USE
        SELECT ART_ICOL
        USE
        SELECT APPOTAR
        USE
        ERASE LEFT(alltrim(this.oParentObject.w_DBF7),(LEN(alltrim(this.oParentObject.w_DBF7))-12))+"APPOTAR.DBF"
        ERASE LEFT(alltrim(this.oParentObject.w_DBF7),(LEN(alltrim(this.oParentObject.w_DBF7))-12))+"APPOTAR.FPT"
      endif
      Filedbf = alltrim(this.oParentObject.w_DBF77)
      addmsgNL("Export dettaglio tariffe...", This)
      this.w_cTableName = "TAR_DETT"
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.oParentObject.w_SELART = "AR" AND (this.oParentObject.w_TIPART<>"TT" OR NOT EMPTY(this.oParentObject.w_CODPRE) OR this.oParentObject.w_CLASSTARIF<>"K" OR NOT EMPTY(this.oParentObject.w_SOLOPAR))
        * --- Cancellazione da TAR_DETT dove DECODART NOT IN ART_ICOL.ARCODART
        SELECT 0
        USE alltrim(this.oParentObject.w_DBF2) ALIAS ART_ICOL
        SELECT 0
        USE alltrim(this.oParentObject.w_DBF77) ALIAS TAR_DETT
        SELECT TAR_DETT.* FROM TAR_DETT INNER JOIN ART_ICOL ON TAR_DETT.DECODART=ART_ICOL.ARCODART INTO TABLE LEFT(alltrim(this.oParentObject.w_DBF77),(LEN(alltrim(this.oParentObject.w_DBF77))-12))+"APPODET.DBF"
        SELECT TAR_DETT
        USE
        ERASE alltrim(this.oParentObject.w_DBF77)
        ERASE LEFT(alltrim(this.oParentObject.w_DBF77),(LEN(alltrim(this.oParentObject.w_DBF77))-3))+"FPT"
        SELECT * FROM APPODET INTO TABLE alltrim(this.oParentObject.w_DBF77)
        SELECT TAR_DETT
        USE
        SELECT ART_ICOL
        USE
        SELECT APPODET
        USE
        ERASE LEFT(alltrim(this.oParentObject.w_DBF77),(LEN(alltrim(this.oParentObject.w_DBF77))-12))+"APPODET.DBF"
        ERASE LEFT(alltrim(this.oParentObject.w_DBF77),(LEN(alltrim(this.oParentObject.w_DBF77))-12))+"APPODET.FPT"
      endif
      if NOT EMPTY(this.oParentObject.w_CODLIS)
        * --- Cancellazione da TAR_DETT dove DECODART,DEROWNUM NOT IN TAR_IFFE.TACODART,CPROWNUM
        SELECT 0
        USE alltrim(this.oParentObject.w_DBF7) ALIAS TAR_IFFE
        SELECT 0
        USE alltrim(this.oParentObject.w_DBF77) ALIAS TAR_DETT
        SELECT TAR_DETT.* FROM TAR_DETT INNER JOIN TAR_IFFE ON TAR_DETT.DECODART=TAR_IFFE.TACODART AND TAR_DETT.DEROWNUM=TAR_IFFE.CPROWNUM INTO TABLE LEFT(alltrim(this.oParentObject.w_DBF77),(LEN(alltrim(this.oParentObject.w_DBF77))-12))+"APPOLIS.DBF"
        SELECT TAR_DETT
        USE
        ERASE alltrim(this.oParentObject.w_DBF77)
        ERASE LEFT(alltrim(this.oParentObject.w_DBF77),(LEN(alltrim(this.oParentObject.w_DBF77))-3))+"FPT"
        SELECT * FROM APPOLIS INTO TABLE alltrim(this.oParentObject.w_DBF77)
        SELECT TAR_DETT
        USE
        SELECT TAR_IFFE
        USE
        SELECT APPOLIS
        USE
        ERASE LEFT(alltrim(this.oParentObject.w_DBF77),(LEN(alltrim(this.oParentObject.w_DBF77))-12))+"APPOLIS.DBF"
        ERASE LEFT(alltrim(this.oParentObject.w_DBF77),(LEN(alltrim(this.oParentObject.w_DBF77))-12))+"APPOLIS.FPT"
      endif
    endif
    if this.oParentObject.w_SELNOT = "NA"
      Filedbf = alltrim(this.oParentObject.w_DBF8)
      addmsgNL("Note voci del tariffario...", This)
      this.w_cTableName = "NOT_ARTI"
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.oParentObject.w_SELART = "AR" AND this.oParentObject.w_TIPART<>"TT"
        * --- Cancellazione da NOT_ARTI dove ARCODART NOT IN ART_ICOL.ARCODART
        SELECT 0
        USE alltrim(this.oParentObject.w_DBF2) ALIAS ART_ICOL
        SELECT 0
        USE alltrim(this.oParentObject.w_DBF8) ALIAS NOT_ARTI
        SELECT NOT_ARTI.* FROM NOT_ARTI INNER JOIN ART_ICOL ON NOT_ARTI.ARCODART=ART_ICOL.ARCODART INTO TABLE LEFT(alltrim(this.oParentObject.w_DBF8),(LEN(alltrim(this.oParentObject.w_DBF8))-12))+"APPONOT.DBF"
        SELECT NOT_ARTI
        USE
        ERASE alltrim(this.oParentObject.w_DBF8)
        ERASE LEFT(alltrim(this.oParentObject.w_DBF8),(LEN(alltrim(this.oParentObject.w_DBF8))-3))+"FPT"
        SELECT * FROM APPONOT INTO TABLE alltrim(this.oParentObject.w_DBF8)
        SELECT NOT_ARTI
        USE
        SELECT ART_ICOL
        USE
        SELECT APPONOT
        USE
        ERASE LEFT(alltrim(this.oParentObject.w_DBF8),(LEN(alltrim(this.oParentObject.w_DBF8))-12))+"APPONOT.DBF"
        ERASE LEFT(alltrim(this.oParentObject.w_DBF8),(LEN(alltrim(this.oParentObject.w_DBF8))-12))+"APPONOT.FPT"
      endif
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Import Tabelle
    * --- Lettura files DBF
    this.w_cTableName = ""
    this.w_FLDFILTER = ""
    this.w_IMPERROR = .F.
    if this.oParentObject.w_SELUNI = "UN"
      addmsgNL("Import unit� di misura" ,This)
      Filedbf = alltrim(this.oParentObject.w_DBF1)
      this.w_cTableName = "UNIMIS"
      this.w_FLDFILTER = "UMCODICE"
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.oParentObject.w_SELIVA = "VI"
      addmsgNL("Import vociiva" ,This)
      Filedbf = alltrim(this.oParentObject.w_DBF4)
      this.w_cTableName = "VOCIIVA"
      this.w_FLDFILTER = "IVCODIVA"
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.oParentObject.w_SELTIP = "TR"
      addmsgNL("Import tipologie documenti" ,This)
      Filedbf = alltrim(this.oParentObject.w_DBF6)
      this.w_cTableName = "CLA_RIGD"
      this.w_FLDFILTER = "TRCODCLA"
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.oParentObject.w_SELCAR = "CA" and !this.w_IMPERROR
      addmsgNL("Import categorie contabili " ,This)
      Filedbf = alltrim(this.oParentObject.w_DBF5)
      this.w_cTableName = "CACOARTI"
      this.w_FLDFILTER = "C1CODICE"
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.oParentObject.w_SELART = "AR" and !this.w_IMPERROR
      addmsgNL("Import voci del tariffario" ,This)
      Filedbf = alltrim(this.oParentObject.w_DBF2)
      this.w_cTableName = "ART_ICOL"
      this.w_FLDFILTER = "ARCODART"
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.oParentObject.w_SELKEY = "KA" and !this.w_IMPERROR
      addmsgNL("Import codici di ricerca" ,This)
      Filedbf = alltrim(this.oParentObject.w_DBF3)
      this.w_cTableName = "KEY_ARTI"
      this.w_FLDFILTER = "CACODICE"
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.oParentObject.w_SELTAR = "TA" and !this.w_IMPERROR
      addmsgNL("Import tariffe" ,This)
      Filedbf = alltrim(this.oParentObject.w_DBF7)
      this.w_cTableName = "TAR_IFFE"
      this.w_FLDFILTER = "TACODART,CPROWNUM"
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if !this.w_IMPERROR
        addmsgNL("Import dettaglio tariffe" ,This)
        Filedbf = alltrim(this.oParentObject.w_DBF77)
        this.w_cTableName = "TAR_DETT"
        this.w_FLDFILTER = "DECODART,DEROWNUM,CPROWNUM"
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    if this.oParentObject.w_SELNOT = "NA" and !this.w_IMPERROR
      addmsgNL("Import note voci del tariffario" ,This)
      Filedbf = alltrim(this.oParentObject.w_DBF8)
      this.w_cTableName = "NOT_ARTI"
      this.w_FLDFILTER = "NOCODART"
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica esistenza file da esportare
    this.w_OK = .T.
    this.w_cTableName = ALLTRIM(this.w_cTableName)
    if file( Filedbf )
      this.w_OK = ah_YesNo("E' gi� presente il file:%1. Si desidera sovrascrivere?","",FileDBF)
    endif
    if this.w_OK
      * --- Se export del Tariffario
      if this.w_EXP_ARTICOL = "S"
        * --- Genera nome casuale da attribuire al cursore
        this.w_READ_RES = Sys(2015)
        vq_exec("query\GSAR_BEX.VQR", this, this.w_READ_RES)
      else
        this.w_cWhere = IIF(EMPTY(NVL(this.w_cWhere, " ")), "1=1", this.w_cWhere)
        this.w_READ_RES = READTABLE(this.w_cTableName,"*","","",.F.,this.w_cWhere)
      endif
      if EMPTY(this.w_READ_RES)
        ah_ErrorMsg("Errore durante la lettura dei dati della tabella %1", "" , "", this.w_cTableName)
      else
        select (this.w_READ_RES)
        go top
        copy to (Filedbf) TYPE FOX2X
        USE IN (this.w_READ_RES)
      endif
      this.w_cWhere = ""
      this.w_FLDFILTER = ""
    endif
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if file(Filedbf)
      use (Filedbf) alias CursDBF
      if USED("CursDBF") and reccount("CursDBF")<>0
        Select "CursDBF"
        go top
        do while !EOF()
          * --- Try
          local bErr_03A4A820
          bErr_03A4A820=bTrsErr
          this.Try_03A4A820()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            * --- Divido la stringa dei filtri divisa da virgole
            this.w_NUMFILTER = ALINES(ARRSPLIT, this.w_FLDFILTER, 5, ",")
            if this.w_NUMFILTER>0
              * --- Preparo l'array dei filtri
              PUBLIC ARRAY ARRFILTER(this.w_NUMFILTER ,2)
              do while this.w_NUMFILTER>0
                ARRFILTER(this.w_NUMFILTER,1)= ARRSPLIT(this.w_NUMFILTER)
                L_MAC = "IIF(VARTYPE(CursDBF."+ALLTRIM(ARRSPLIT(this.w_NUMFILTER))+")='C',ALLTRIM(CursDBF."+ALLTRIM(ARRSPLIT(this.w_numFILTER))+"),CursDBF."+ALLTRIM(ARRSPLIT(this.w_numFILTER))+")"
                ARRFILTER(this.w_NUMFILTER,2)= &L_MAC
                this.w_NUMFILTER = this.w_NUMFILTER - 1
              enddo
              if !EMPTY(writetable(this.w_cTableName, @ARRINS, @ARRFILTER, this, this.oParentObject.w_FLVERBOS))
                this.w_IMPERROR = .T.
                addmsgNL("Errere scrittura archivio:%0%1" ,This, MESSAGE())
                * --- transaction error
                bTrsErr=.t.
                i_TrsMsg=ah_MsgFormat("Errore aggiornamento tabella %1", this.w_cTableName)
              endif
            else
              this.w_IMPERROR = .T.
              * --- transaction error
              bTrsErr=.t.
              i_TrsMsg=ah_MsgFormat("Filtri aggiornamento tabelle %1 non specificati", this.w_cTableName)
            endif
          endif
          bTrsErr=bTrsErr or bErr_03A4A820
          * --- End
          SELECT "CursDBF"
          SKIP
        enddo
      endif
      RELEASE ARRINS
      RELEASE ARRFILTER
      RELEASE ARRSPLIT
      this.w_NUMFILTER = 0
      if USED("CursDBF")
        USE IN "CursDBF"
      endif
    else
      ah_ErrorMsg("Attenzione: il file '%1' � inesistente. Impossibile importare i relativi dati.", "" , "", Filedbf)
    endif
  endproc
  proc Try_03A4A820()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    PUBLIC ARRAY ARRINS(1,1)
    if curtoarr("CursDBF",0,@ARRINS)<0
      * --- Raise
      i_Error="CreaArrErr"
      return
    endif
    if !EMPTY(inserttable(this.w_cTableName, @ARRINS, iif(this.oParentObject.w_FLVERBOS,this," "), this.oParentObject.w_FLVERBOS))
      * --- Raise
      i_Error="InsErr"
      return
    endif
    return


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,9)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='KEY_ARTI'
    this.cWorkTables[3]='NOT_ARTI'
    this.cWorkTables[4]='TAR_IFFE'
    this.cWorkTables[5]='TAR_DETT'
    this.cWorkTables[6]='CACOARTI'
    this.cWorkTables[7]='UNIMIS'
    this.cWorkTables[8]='CLA_RIGD'
    this.cWorkTables[9]='vociiva'
    return(this.OpenAllTables(9))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
