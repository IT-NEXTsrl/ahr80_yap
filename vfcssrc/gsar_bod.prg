* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bod                                                        *
*              Elimina nominativo                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-03                                                      *
* Last revis.: 2007-07-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bod",oParentObject)
return(i_retval)

define class tgsar_bod as StdBatch
  * --- Local variables
  w_NUMVER = 0
  w_MESS = space(10)
  w_APPO = space(10)
  w_ALSERIAL = space(10)
  w_ALTIPALL = space(1)
  w_ALPATFIL = space(255)
  w_Periodo = ctod("  /  /  ")
  w_Pat = space(254)
  w_CritPeri = space(1)
  w_ArcNom = space(1)
  w_RagSoc = space(40)
  * --- WorkFile variables
  ALL_EGAT_idx=0
  ALL_ATTR_idx=0
  PAR_OFFE_idx=0
  OFF_ERTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimina Allegati in caso di cancellazione Nominativo (da GSAR_ANO)
    * --- Elimina eventuali allegati
    this.w_RagSoc = this.oParentObject.w_NODESCRI
    if NOT EMPTY(this.oParentObject.w_NOCODICE)
      * --- Select from ALL_EGAT
      i_nConn=i_TableProp[this.ALL_EGAT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ALL_EGAT_idx,2],.t.,this.ALL_EGAT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" ALL_EGAT ";
            +" where ALRIFNOM="+cp_ToStrODBC(this.oParentObject.w_NOCODICE)+"";
             ,"_Curs_ALL_EGAT")
      else
        select * from (i_cTable);
         where ALRIFNOM=this.oParentObject.w_NOCODICE;
          into cursor _Curs_ALL_EGAT
      endif
      if used('_Curs_ALL_EGAT')
        select _Curs_ALL_EGAT
        locate for 1=1
        do while not(eof())
        this.w_ALSERIAL = NVL(_Curs_ALL_EGAT.ALSERIAL, "")
        this.w_Periodo = CP_TODATE(_Curs_ALL_EGAT.ALDATREG)
        this.w_ALTIPALL = NVL(_Curs_ALL_EGAT.ALTIPALL, "L")
        this.w_ALPATFIL = NVL(_Curs_ALL_EGAT.ALPATFIL, "")
        if NOT EMPTY(this.w_ALSERIAL)
          * --- Delete from ALL_ATTR
          i_nConn=i_TableProp[this.ALL_ATTR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ALL_ATTR_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"AACODICE = "+cp_ToStrODBC(this.w_ALSERIAL);
                   )
          else
            delete from (i_cTable) where;
                  AACODICE = this.w_ALSERIAL;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
        endif
        if this.w_ALTIPALL="F" AND NOT EMPTY(this.w_ALPATFIL)
          * --- Elimina File Allegato
          this.w_Pat = ""
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_PAT = ALLTRIM(this.w_PAT)
          if NOT EMPTY(this.w_PAT)
            this.w_PAT = this.w_PAT + IIF(RIGHT(this.w_PAT, 1)="\", "", "\") + ALLTRIM(this.w_ALPATFIL)
            DELFIL = ALLTRIM(this.w_PAT)
            if FILE(DELFIL)
              DELETE FILE (DELFIL)
            endif
          endif
        endif
          select _Curs_ALL_EGAT
          continue
        enddo
        use
      endif
      * --- Delete from ALL_EGAT
      i_nConn=i_TableProp[this.ALL_EGAT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ALL_EGAT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"ALRIFNOM = "+cp_ToStrODBC(this.oParentObject.w_NOCODICE);
               )
      else
        delete from (i_cTable) where;
              ALRIFNOM = this.oParentObject.w_NOCODICE;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Periodo
    * --- Calcolo nome directory memorizzazione allegati
    * --- Read from PAR_OFFE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_OFFE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_OFFE_idx,2],.t.,this.PAR_OFFE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "POPATALN,POPERALN,POFLCAAR"+;
        " from "+i_cTable+" PAR_OFFE where ";
            +"POCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        POPATALN,POPERALN,POFLCAAR;
        from (i_cTable) where;
            POCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_Pat = NVL(cp_ToDate(_read_.POPATALN),cp_NullValue(_read_.POPATALN))
      this.w_CritPeri = NVL(cp_ToDate(_read_.POPERALN),cp_NullValue(_read_.POPERALN))
      this.w_ArcNom = NVL(cp_ToDate(_read_.POFLCAAR),cp_NullValue(_read_.POFLCAAR))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_ArcNom="S"
      this.w_Pat = alltrim(this.w_Pat)+alltrim(this.w_RagSoc)+"\" 
    else
      this.w_Pat = alltrim(this.w_Pat)
    endif
    do case
      case this.w_CritPeri = "M"
        * --- Raggruppamento per mesi
        this.w_Pat = this.w_Pat + right("00"+alltrim(str(month(this.w_Periodo),2,0)), 2) + str(year(this.w_Periodo),4,0)
      case this.w_CritPeri = "T"
        * --- Raggruppamento per trimestri
        do case
          case month(this.w_Periodo) > 0 .and. month(this.w_Periodo) < 4
            this.w_Pat = this.w_Pat + "T1" + str(year(this.w_Periodo),4,0)
          case month(this.w_Periodo) > 3 .and. month(this.w_Periodo) < 7
            this.w_Pat = this.w_Pat + "T2" + str(year(this.w_Periodo),4,0)
          case month(this.w_Periodo) > 6 .and. month(this.w_Periodo) < 10
            this.w_Pat = this.w_Pat + "T3" + str(year(this.w_Periodo),4,0)
          case month(this.w_Periodo) > 9
            this.w_Pat = this.w_Pat + "T4" + str(year(this.w_Periodo),4,0)
        endcase
      case this.w_CritPeri = "Q"
        * --- Raggruppamento per quadrimestri
        do case
          case month(this.w_Periodo) > 0 .and. month(this.w_Periodo) < 5
            this.w_Pat = this.w_Pat + "Q1" + str(year(this.w_Periodo),4,0)
          case month(this.w_Periodo) > 4 .and. month(this.w_Periodo) < 9
            this.w_Pat = this.w_Pat + "Q2" + str(year(this.w_Periodo),4,0)
          case month(this.w_Periodo) > 8
            this.w_Pat = this.w_Pat + "Q3" + str(year(this.w_Periodo),4,0)
        endcase
      case this.w_CritPeri = "S"
        * --- Raggruppamento per semestri
        this.w_Pat = this.w_Pat + iif( month(this.w_Periodo) >6 , "S2", "S1") + str(year(this.w_Periodo),4,0)
      case this.w_CritPeri = "A"
        * --- Raggruppamento per anni
        this.w_Pat = this.w_Pat + str(year(this.w_Periodo),4,0)
    endcase
    this.w_Pat = this.w_Pat+"\" 
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='ALL_EGAT'
    this.cWorkTables[2]='ALL_ATTR'
    this.cWorkTables[3]='PAR_OFFE'
    this.cWorkTables[4]='OFF_ERTE'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_ALL_EGAT')
      use in _Curs_ALL_EGAT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
