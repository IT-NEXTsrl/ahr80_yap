* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bzp                                                        *
*              Lancia primanota da altre proc                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_7]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-13                                                      *
* Last revis.: 2011-12-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pSERIAL
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bzp",oParentObject,m.pSERIAL)
return(i_retval)

define class tgsar_bzp as StdBatch
  * --- Local variables
  pSERIAL = space(10)
  w_PNOTA = .NULL.
  w_COMPET = space(4)
  w_MVSERIAL = space(10)
  w_SERIALP = space(10)
  w_ALFA = space(10)
  w_TIPMOV = space(2)
  w_NUMERO = 0
  w_DATA = ctod("  /  /  ")
  * --- WorkFile variables
  PNT_MAST_idx=0
  PNTSMAST_idx=0
  DOC_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- lancia la PrimaNota da Bottone (da Varie Procedure)
    * --- chiave della Prima Nota
    * --- Questo oggetto sar� definito come Prima Nota
    * --- Read from PNT_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PNT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2],.t.,this.PNT_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PNCOMPET"+;
        " from "+i_cTable+" PNT_MAST where ";
            +"PNSERIAL = "+cp_ToStrODBC(this.pSERIAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PNCOMPET;
        from (i_cTable) where;
            PNSERIAL = this.pSERIAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_COMPET = NVL(cp_ToDate(_read_.PNCOMPET),cp_NullValue(_read_.PNCOMPET))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if g_COGE="S" OR !Isalt()
      if NOT EMPTY(NVL(this.w_COMPET,space(4)))
        * --- definisco l'oggetto come appartenente alla classe della Prima Nota
        this.w_PNOTA = GSCG_MPN()
        * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
        if !(this.w_PNOTA.bSec1)
          Ah_ErrorMsg("Impossibile aprire la gestione collegata!",48,"")
          i_retcode = 'stop'
          return
        endif
        * --- inizializzo la chiave della Prima Nota
        this.w_PNOTA.w_PNSERIAL = this.pSERIAL
        * --- creo il curosre delle solo chiavi
        this.w_PNOTA.QueryKeySet("PNSERIAL='"+this.pSERIAL+ "'","")     
        * --- mi metto in interrogazione
        this.w_PNOTA.LoadRecwarn()     
        i_retcode = 'stop'
        i_retval = this.w_PNOTA
        return
      else
        * --- Read from PNTSMAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PNTSMAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PNTSMAST_idx,2],.t.,this.PNTSMAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PNCOMPET"+;
            " from "+i_cTable+" PNTSMAST where ";
                +"PNSERIAL = "+cp_ToStrODBC(this.pSERIAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PNCOMPET;
            from (i_cTable) where;
                PNSERIAL = this.pSERIAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_COMPET = NVL(cp_ToDate(_read_.PNCOMPET),cp_NullValue(_read_.PNCOMPET))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if NOT EMPTY(NVL(this.w_COMPET,space(4))) Or this.pSerial="@@@@@@@@@@"
          ah_ErrorMsg("Movimento di primanota storicizzato","!","")
        else
          ah_ErrorMsg("Movimento di primanota inesistente","!","")
        endif
      endif
    else
      this.w_SERIALP = this.pSERIAL
      * --- Read from DOC_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MVSERIAL,MVALFDOC,MVCLADOC,MVNUMDOC,MVDATDOC"+;
          " from "+i_cTable+" DOC_MAST where ";
              +"MVRIFCON = "+cp_ToStrODBC(this.pSERIAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MVSERIAL,MVALFDOC,MVCLADOC,MVNUMDOC,MVDATDOC;
          from (i_cTable) where;
              MVRIFCON = this.pSERIAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MVSERIAL = NVL(cp_ToDate(_read_.MVSERIAL),cp_NullValue(_read_.MVSERIAL))
        this.w_ALFA = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
        this.w_TIPMOV = NVL(cp_ToDate(_read_.MVCLADOC),cp_NullValue(_read_.MVCLADOC))
        this.w_NUMERO = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
        this.w_DATA = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      do GSAR_BZD with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  proc Init(oParentObject,pSERIAL)
    this.pSERIAL=pSERIAL
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='PNT_MAST'
    this.cWorkTables[2]='PNTSMAST'
    this.cWorkTables[3]='DOC_MAST'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pSERIAL"
endproc
