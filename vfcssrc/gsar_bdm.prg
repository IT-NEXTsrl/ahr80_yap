* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bdm                                                        *
*              Determina matricole - import e car.rapido                       *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_14]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-04-04                                                      *
* Last revis.: 2011-01-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_LCODICE,w_LCODLOT,w_LCODUBI,w_LCODMAG,w_LPREZZO,w_LUNIMIS,w_LSERRIF,w_LROWRIF,w_LCODCEN,w_LCODCOM,w_LCODATT,w_LCODMA2,w_LCODUB2,w_LUNILOG,w_OBJ_GEST,pCursor
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bdm",oParentObject,m.w_LCODICE,m.w_LCODLOT,m.w_LCODUBI,m.w_LCODMAG,m.w_LPREZZO,m.w_LUNIMIS,m.w_LSERRIF,m.w_LROWRIF,m.w_LCODCEN,m.w_LCODCOM,m.w_LCODATT,m.w_LCODMA2,m.w_LCODUB2,m.w_LUNILOG,m.w_OBJ_GEST,m.pCursor)
return(i_retval)

define class tgsar_bdm as StdBatch
  * --- Local variables
  w_LCODICE = space(20)
  w_LCODLOT = space(20)
  w_LCODUBI = space(20)
  w_LCODMAG = space(5)
  w_LPREZZO = 0
  w_LUNIMIS = space(3)
  w_LSERRIF = space(10)
  w_LROWRIF = 0
  w_LCODCEN = space(15)
  w_LCODCOM = space(15)
  w_LCODATT = space(15)
  w_LCODMA2 = space(5)
  w_LCODUB2 = space(20)
  w_LUNILOG = space(18)
  w_OBJ_GEST = .NULL.
  pCursor = space(50)
  w_TMPCARI = space(1)
  w_GSVE_MMT = .NULL.
  w_SERRIF = space(10)
  w_ROWRIF = 0
  w_NUMRIF = 0
  w_LCODLOT = space(20)
  w_LCODUBI = space(40)
  w_KEYSAL = space(20)
  w_CODMAT = space(40)
  w_FUNCTION = space(20)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carica nella gestione matricole l'elenco della matricole...
    * --- Lanciato da GSVE_BI3 e GSAR_BPG sia nel caso di caricamento
    *     rapido che nel caso di imposrt da car.rapido.
    *     Riceve come parametro i cirteri di selezione da applicare, quindi
    *     Codice di ricerca Articolo
    *     Codice lotto
    *     Codice Ubicazione
    *     Codice magazzino principale
    *     Prezzo
    *     Unit� di misura
    *     Seriale e Riga evasa
    *     Centro di costo
    *     Commessa
    *     Attivit� di commessa
    *     Gestione da popolare (GSxx_MDV)
    *     Cursore contenenente il dettaglio matricole (GSAR_MPG.CTRSNAME)
    * --- Imposto MTCARI (inserimento rapido matricole) = ' ' in modo che se fosse invece attivato nella 
    *     causale di magazzino, in fase di importazione documenti non venga aperta la maschera di inserimento.
    *     Mi segno anche il vecchio valore per poi reimpostarlo.
    this.w_TMPCARI = this.w_OBJ_GEST.w_MTCARI
    this.w_OBJ_GEST.w_MTCARI = " "
    this.w_OBJ_GEST.GSVE_MMT.LinkPCClick(.T.)     
    this.w_OBJ_GEST.GSVE_MMT.Hide()     
    this.w_GSVE_MMT = this.w_OBJ_GEST.GSVE_MMT.Cnt
    if Not empty(this.w_LSERRIF) and this.w_LROWRIF<>0
       
 Select * From ( this.pCursor ) ; 
 Where t_STATOROW="S" And Not Deleted() ; 
 And t_GPCODLOT= this.w_LCODLOT And t_GPCODUBI= this.w_LCODUBI ; 
 And t_GPCODMAG= this.w_LCODMAG And t_GPPREZZO= this.w_LPREZZO And t_GPUNIMIS= this.w_LUNIMIS ; 
 And t_GPSERRIF = this.w_LSERRIF And t_GPROWRIF= this.w_LROWRIF ; 
 And t_GPCODCEN= this.w_LCODCEN And t_GPCODCOM= this.w_LCODCOM And t_GPCODATT= this.w_LCODATT ; 
 And t_GPCODMA2= this.w_LCODMA2 And t_GPCODUB2= this.w_LCODUB2 And t_GPUNILOG= this.w_LUNILOG ; 
 Into Cursor Tmp_Matricole NoFilter
    else
       
 Select * From ( this.pCursor ) ; 
 Where t_STATOROW="S" And Not Deleted() ; 
 And t_GPCODICE= this.w_LCODICE And t_GPCODLOT= this.w_LCODLOT And t_GPCODUBI= this.w_LCODUBI ; 
 And t_GPCODMAG= this.w_LCODMAG And t_GPPREZZO= this.w_LPREZZO And t_GPUNIMIS= this.w_LUNIMIS ; 
 And t_GPSERRIF = this.w_LSERRIF And t_GPROWRIF= this.w_LROWRIF ; 
 And t_GPCODCEN= this.w_LCODCEN And t_GPCODCOM= this.w_LCODCOM And t_GPCODATT= this.w_LCODATT ; 
 And t_GPCODMA2= this.w_LCODMA2 And t_GPCODUB2= this.w_LCODUB2 And t_GPUNILOG= this.w_LUNILOG ; 
 Into Cursor Tmp_Matricole NoFilter
    endif
     
 Select Tmp_Matricole 
 Go Top
    do while Not Eof( "Tmp_Matricole" )
      * --- Assegno informazioni dal temporaneo
      this.w_CODMAT = NVL(t_GPCODMAT,SPACE(40))
      this.w_LCODLOT = NVL(t_GPCODLOT,SPACE(20))
      this.w_LCODUBI = NVL(t_GPCODUBI,SPACE(20))
      this.w_SERRIF = NVL(t_MTSERRIF,SPACE(10))
      this.w_ROWRIF = NVL(t_MTROWRIF,0)
      this.w_NUMRIF = NVL(t_MTRIFNUM,0)
      this.w_KEYSAL = NVL(t_MTKEYSAL,SPACE(40))
      this.w_GSVE_MMT.AddRow()     
      this.w_GSVE_MMT.InitSon()     
      this.w_GSVE_MMT.w_MTKEYSAL = this.w_KEYSAL
      this.w_GSVE_MMT.w_MTRIFNUM = this.w_NUMRIF
      this.w_GSVE_MMT.w_MTRIFSTO = this.w_NUMRIF
      this.w_GSVE_MMT.w_MTSERRIF = this.w_SERRIF
      this.w_GSVE_MMT.w_MTROWRIF = this.w_ROWRIF
      this.w_GSVE_MMT.w_MT__FLAG = IIF( Empty( this.w_SERRIF ) , " " , "+" )
      this.w_GSVE_MMT.w_MT_SALDO = 0
      this.w_GSVE_MMT.w_MTCODMAT = this.w_CODMAT
      this.w_GSVE_MMT.w_MTCODLOT = this.w_LCODLOT
      this.w_GSVE_MMT.w_MTCODUBI = this.w_LCODUBI
      this.w_GSVE_MMT.w_RIGA = IIF( Empty( this.w_CODMAT ) , 0 ,1 )
      this.w_GSVE_MMT.w_TOTALE = this.w_GSVE_MMT.w_TOTALE + this.w_GSVE_MMT.w_RIGA
      this.w_GSVE_MMT.TrsFromWork()     
       
 Select Tmp_Matricole 
 Skip
    enddo
     
 Use In Tmp_Matricole
    * --- Memorizzo la cFunction per risettarla alla fine
    this.w_GSVE_MMT.bUpdated = .t.
    this.w_FUNCTION = this.w_OBJ_GEST.GSVE_MMT.cFunction
    * --- Per evitare l'esecuzione della CheckForm
    this.w_OBJ_GEST.GSVE_MMT.cFunction = "Query"
    this.w_OBJ_GEST.GSVE_MMT.EcpSave()     
    this.w_OBJ_GEST.GSVE_MMT.cFunction = this.w_FUNCTION
    * --- Reimposto MTCARI come era in origine
    this.w_OBJ_GEST.w_MTCARI = this.w_TMPCARI
    this.w_OBJ_GEST.w_SHOWMMT = "S"
  endproc


  proc Init(oParentObject,w_LCODICE,w_LCODLOT,w_LCODUBI,w_LCODMAG,w_LPREZZO,w_LUNIMIS,w_LSERRIF,w_LROWRIF,w_LCODCEN,w_LCODCOM,w_LCODATT,w_LCODMA2,w_LCODUB2,w_LUNILOG,w_OBJ_GEST,pCursor)
    this.w_LCODICE=w_LCODICE
    this.w_LCODLOT=w_LCODLOT
    this.w_LCODUBI=w_LCODUBI
    this.w_LCODMAG=w_LCODMAG
    this.w_LPREZZO=w_LPREZZO
    this.w_LUNIMIS=w_LUNIMIS
    this.w_LSERRIF=w_LSERRIF
    this.w_LROWRIF=w_LROWRIF
    this.w_LCODCEN=w_LCODCEN
    this.w_LCODCOM=w_LCODCOM
    this.w_LCODATT=w_LCODATT
    this.w_LCODMA2=w_LCODMA2
    this.w_LCODUB2=w_LCODUB2
    this.w_LUNILOG=w_LUNILOG
    this.w_OBJ_GEST=w_OBJ_GEST
    this.pCursor=pCursor
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_LCODICE,w_LCODLOT,w_LCODUBI,w_LCODMAG,w_LPREZZO,w_LUNIMIS,w_LSERRIF,w_LROWRIF,w_LCODCEN,w_LCODCOM,w_LCODATT,w_LCODMA2,w_LCODUB2,w_LUNILOG,w_OBJ_GEST,pCursor"
endproc
