* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bca                                                        *
*              Storicizza dati anagrafici                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_28]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-04-10                                                      *
* Last revis.: 2014-11-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bca",oParentObject)
return(i_retval)

define class tgsar_bca as StdBatch
  * --- Local variables
  w_DESCR2 = space(40)
  w_DESCRI = space(40)
  w_INDIR2 = space(35)
  w_INDIRI = space(35)
  w_CAP = space(8)
  w_LOCALI = space(30)
  w_PROVIN = space(2)
  w_NAZION = space(3)
  w_INDWEB = space(254)
  w_EMAIL = space(254)
  w_EMPEC = space(254)
  w_CODFIS = space(16)
  w_PARIVA = space(12)
  w_TELEFO = space(18)
  w_TELFAX = space(18)
  w_DATINI = ctod("  /  /  ")
  w_DATFIN = ctod("  /  /  ")
  w_DESCR3 = space(60)
  w_DESCR4 = space(60)
  w_INDIR3 = space(35)
  w_INDIR4 = space(35)
  w_LOCAL2 = space(30)
  w_NUMCEL = space(18)
  w_CPROWORD = 0
  * --- WorkFile variables
  SED_STOR_idx=0
  CONTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Storicizza i dati anagrafici dei clienti/fornitori (Lanciato dal bottone Storicizza in GSAR_ACL/GSAR_AFR)
    if ah_yesno("Si desidera storicizzare i dati anagrafici?")
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANDESCRI,ANDESCR2,ANINDIRI,ANINDIR2,AN___CAP,ANLOCALI,ANPROVIN,ANNAZION,ANTELEFO,ANTELFAX,ANNUMCEL,ANCODFIS,ANPARIVA,ANINDWEB,AN_EMAIL,AN_EMPEC,ANDESCR3,ANDESCR4,ANINDIR3,ANINDIR4,ANLOCAL2"+;
          " from "+i_cTable+" CONTI where ";
              +"ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_ANCODICE);
              +" and ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_ANTIPCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANDESCRI,ANDESCR2,ANINDIRI,ANINDIR2,AN___CAP,ANLOCALI,ANPROVIN,ANNAZION,ANTELEFO,ANTELFAX,ANNUMCEL,ANCODFIS,ANPARIVA,ANINDWEB,AN_EMAIL,AN_EMPEC,ANDESCR3,ANDESCR4,ANINDIR3,ANINDIR4,ANLOCAL2;
          from (i_cTable) where;
              ANCODICE = this.oParentObject.w_ANCODICE;
              and ANTIPCON = this.oParentObject.w_ANTIPCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DESCRI = NVL(cp_ToDate(_read_.ANDESCRI),cp_NullValue(_read_.ANDESCRI))
        this.w_DESCR2 = NVL(cp_ToDate(_read_.ANDESCR2),cp_NullValue(_read_.ANDESCR2))
        this.w_INDIRI = NVL(cp_ToDate(_read_.ANINDIRI),cp_NullValue(_read_.ANINDIRI))
        this.w_INDIR2 = NVL(cp_ToDate(_read_.ANINDIR2),cp_NullValue(_read_.ANINDIR2))
        this.w_CAP = NVL(cp_ToDate(_read_.AN___CAP),cp_NullValue(_read_.AN___CAP))
        this.w_LOCALI = NVL(cp_ToDate(_read_.ANLOCALI),cp_NullValue(_read_.ANLOCALI))
        this.w_PROVIN = NVL(cp_ToDate(_read_.ANPROVIN),cp_NullValue(_read_.ANPROVIN))
        this.w_NAZION = NVL(cp_ToDate(_read_.ANNAZION),cp_NullValue(_read_.ANNAZION))
        this.w_TELEFO = NVL(cp_ToDate(_read_.ANTELEFO),cp_NullValue(_read_.ANTELEFO))
        this.w_TELFAX = NVL(cp_ToDate(_read_.ANTELFAX),cp_NullValue(_read_.ANTELFAX))
        this.w_NUMCEL = NVL(cp_ToDate(_read_.ANNUMCEL),cp_NullValue(_read_.ANNUMCEL))
        this.w_CODFIS = NVL(cp_ToDate(_read_.ANCODFIS),cp_NullValue(_read_.ANCODFIS))
        this.w_PARIVA = NVL(cp_ToDate(_read_.ANPARIVA),cp_NullValue(_read_.ANPARIVA))
        this.w_INDWEB = NVL(cp_ToDate(_read_.ANINDWEB),cp_NullValue(_read_.ANINDWEB))
        this.w_EMAIL = NVL(cp_ToDate(_read_.AN_EMAIL),cp_NullValue(_read_.AN_EMAIL))
        this.w_EMPEC = NVL(cp_ToDate(_read_.AN_EMPEC),cp_NullValue(_read_.AN_EMPEC))
        this.w_DESCR3 = NVL(cp_ToDate(_read_.ANDESCR3),cp_NullValue(_read_.ANDESCR3))
        this.w_DESCR4 = NVL(cp_ToDate(_read_.ANDESCR4),cp_NullValue(_read_.ANDESCR4))
        this.w_INDIR3 = NVL(cp_ToDate(_read_.ANINDIR3),cp_NullValue(_read_.ANINDIR3))
        this.w_INDIR4 = NVL(cp_ToDate(_read_.ANINDIR4),cp_NullValue(_read_.ANINDIR4))
        this.w_LOCAL2 = NVL(cp_ToDate(_read_.ANLOCAL2),cp_NullValue(_read_.ANLOCAL2))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_DATFIN = I_DATSYS
      VQ_EXEC("GSAR_MSA.VQR",this,"STORICO")
      if reccount("STORICO") > 0
         
 select STORICO 
 go top 
 scan
        this.w_DATINI = CP_TODATE(SSDATINI)
        this.w_CPROWORD = Nvl(CPROWORD,0) +10
        ENDSCAN
      else
        this.w_DATINI = i_INIDAT
        this.w_CPROWORD = 10
      endif
      if this.w_DATINI>this.w_DATFIN
        Ah_errormsg("Impossibile storicizzare!%0Intervallo di date con dati anagrafici gi� storicizzati")
        i_retcode = 'stop'
        return
      else
        * --- Try
        local bErr_038D8E70
        bErr_038D8E70=bTrsErr
        this.Try_038D8E70()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          Ah_errormsg("ERRORE - impossibile inserire record nelle tabelle SED_STOR")
        endif
        bTrsErr=bTrsErr or bErr_038D8E70
        * --- End
      endif
      if used("STORICO")
         
 select Storico 
 use
      endif
    endif
  endproc
  proc Try_038D8E70()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Insert into SED_STOR
    i_nConn=i_TableProp[this.SED_STOR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SED_STOR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SED_STOR_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SSTIPCON"+",SSCODICE"+",SSDATINI"+",SSDATFIN"+",SSDESCRI"+",SSDESCR2"+",SSINDIRI"+",SSINDIR2"+",SS___CAP"+",SSLOCALI"+",SSNAZION"+",SSCODFIS"+",SSPARIVA"+",SSPROVIN"+",SSTELEFO"+",SSTELFAX"+",SSNUMCEL"+",SSINDWEB"+",SS_EMAIL"+",SS_EMPEC"+",CPROWORD"+",SSDESCR3"+",SSDESCR4"+",SSINDIR3"+",SSINDIR4"+",SSLOCAL2"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ANTIPCON),'SED_STOR','SSTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ANCODICE),'SED_STOR','SSCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DATINI),'SED_STOR','SSDATINI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DATFIN),'SED_STOR','SSDATFIN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESCRI),'SED_STOR','SSDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESCR2),'SED_STOR','SSDESCR2');
      +","+cp_NullLink(cp_ToStrODBC(this.w_INDIRI),'SED_STOR','SSINDIRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_INDIR2),'SED_STOR','SSINDIR2');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAP),'SED_STOR','SS___CAP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LOCALI),'SED_STOR','SSLOCALI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NAZION),'SED_STOR','SSNAZION');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODFIS),'SED_STOR','SSCODFIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PARIVA),'SED_STOR','SSPARIVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PROVIN),'SED_STOR','SSPROVIN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TELEFO),'SED_STOR','SSTELEFO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TELFAX),'SED_STOR','SSTELFAX');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMCEL),'SED_STOR','SSNUMCEL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_INDWEB),'SED_STOR','SSINDWEB');
      +","+cp_NullLink(cp_ToStrODBC(this.w_EMAIL),'SED_STOR','SS_EMAIL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_EMPEC),'SED_STOR','SS_EMPEC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'SED_STOR','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESCR3),'SED_STOR','SSDESCR3');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESCR4),'SED_STOR','SSDESCR4');
      +","+cp_NullLink(cp_ToStrODBC(this.w_INDIR3),'SED_STOR','SSINDIR3');
      +","+cp_NullLink(cp_ToStrODBC(this.w_INDIR4),'SED_STOR','SSINDIR4');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LOCAL2),'SED_STOR','SSLOCAL2');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SSTIPCON',this.oParentObject.w_ANTIPCON,'SSCODICE',this.oParentObject.w_ANCODICE,'SSDATINI',this.w_DATINI,'SSDATFIN',this.w_DATFIN,'SSDESCRI',this.w_DESCRI,'SSDESCR2',this.w_DESCR2,'SSINDIRI',this.w_INDIRI,'SSINDIR2',this.w_INDIR2,'SS___CAP',this.w_CAP,'SSLOCALI',this.w_LOCALI,'SSNAZION',this.w_NAZION,'SSCODFIS',this.w_CODFIS)
      insert into (i_cTable) (SSTIPCON,SSCODICE,SSDATINI,SSDATFIN,SSDESCRI,SSDESCR2,SSINDIRI,SSINDIR2,SS___CAP,SSLOCALI,SSNAZION,SSCODFIS,SSPARIVA,SSPROVIN,SSTELEFO,SSTELFAX,SSNUMCEL,SSINDWEB,SS_EMAIL,SS_EMPEC,CPROWORD,SSDESCR3,SSDESCR4,SSINDIR3,SSINDIR4,SSLOCAL2 &i_ccchkf. );
         values (;
           this.oParentObject.w_ANTIPCON;
           ,this.oParentObject.w_ANCODICE;
           ,this.w_DATINI;
           ,this.w_DATFIN;
           ,this.w_DESCRI;
           ,this.w_DESCR2;
           ,this.w_INDIRI;
           ,this.w_INDIR2;
           ,this.w_CAP;
           ,this.w_LOCALI;
           ,this.w_NAZION;
           ,this.w_CODFIS;
           ,this.w_PARIVA;
           ,this.w_PROVIN;
           ,this.w_TELEFO;
           ,this.w_TELFAX;
           ,this.w_NUMCEL;
           ,this.w_INDWEB;
           ,this.w_EMAIL;
           ,this.w_EMPEC;
           ,this.w_CPROWORD;
           ,this.w_DESCR3;
           ,this.w_DESCR4;
           ,this.w_INDIR3;
           ,this.w_INDIR4;
           ,this.w_LOCAL2;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    Ah_errormsg("Dati anagrafici storicizzati con successo")
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='SED_STOR'
    this.cWorkTables[2]='CONTI'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
